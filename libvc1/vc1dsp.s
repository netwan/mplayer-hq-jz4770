.file	1 "vc1dsp.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.vc1_v_overlap_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_v_overlap_c
.type	vc1_v_overlap_c, @function
vc1_v_overlap_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$10,$5,1
addu	$12,$4,$5
subu	$10,$4,$10
addiu	$14,$4,8
addu	$5,$10,$5
li	$11,1			# 0x1
b	$L6
li	$13,-256			# 0xffffffffffffff00

$L10:
andi	$6,$8,0x00ff
addu	$3,$7,$3
sb	$6,0($5)
subu	$6,$0,$3
and	$7,$3,$13
beq	$7,$0,$L4
sra	$6,$6,31

$L11:
andi	$3,$6,0x00ff
addu	$2,$2,$15
sb	$3,0($4)
addiu	$4,$4,1
xori	$11,$11,0x1
sb	$2,0($12)
addiu	$10,$10,1
addiu	$5,$5,1
beq	$4,$14,$L12
addiu	$12,$12,1

$L6:
lbu	$9,0($10)
lbu	$15,0($12)
lbu	$8,0($5)
lbu	$7,0($4)
subu	$3,$9,$15
addu	$6,$8,$3
addiu	$3,$3,3
subu	$6,$6,$7
addu	$3,$3,$11
addiu	$6,$6,4
sra	$3,$3,3
subu	$6,$6,$11
andi	$2,$3,0x00ff
sra	$3,$6,3
subu	$9,$9,$2
subu	$6,$8,$3
subu	$8,$0,$6
sb	$9,0($10)
and	$9,$6,$13
bne	$9,$0,$L10
sra	$8,$8,31

andi	$6,$6,0x00ff
addu	$3,$7,$3
sb	$6,0($5)
subu	$6,$0,$3
and	$7,$3,$13
bne	$7,$0,$L11
sra	$6,$6,31

$L4:
andi	$3,$3,0x00ff
addu	$2,$2,$15
sb	$3,0($4)
addiu	$4,$4,1
xori	$11,$11,0x1
sb	$2,0($12)
addiu	$10,$10,1
addiu	$5,$5,1
bne	$4,$14,$L6
addiu	$12,$12,1

$L12:
j	$31
nop

.set	macro
.set	reorder
.end	vc1_v_overlap_c
.size	vc1_v_overlap_c, .-vc1_v_overlap_c
.section	.text.vc1_h_overlap_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_h_overlap_c
.type	vc1_h_overlap_c, @function
vc1_h_overlap_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$11,8			# 0x8
li	$10,1			# 0x1
b	$L18
li	$12,-256			# 0xffffffffffffff00

$L21:
andi	$6,$8,0x00ff
addu	$3,$7,$3
sb	$6,-1($4)
subu	$6,$0,$3
and	$7,$3,$12
beq	$7,$0,$L16
sra	$6,$6,31

$L22:
addu	$2,$2,$13
andi	$3,$6,0x00ff
addiu	$11,$11,-1
sb	$3,0($4)
xori	$10,$10,0x1
sb	$2,1($4)
beq	$11,$0,$L23
addu	$4,$4,$5

$L18:
lbu	$9,-2($4)
lbu	$13,1($4)
lbu	$8,-1($4)
lbu	$7,0($4)
subu	$3,$9,$13
addu	$6,$8,$3
addiu	$3,$3,3
subu	$6,$6,$7
addu	$3,$3,$10
addiu	$6,$6,4
sra	$3,$3,3
subu	$6,$6,$10
andi	$2,$3,0x00ff
sra	$3,$6,3
subu	$9,$9,$2
subu	$6,$8,$3
subu	$8,$0,$6
sb	$9,-2($4)
and	$9,$6,$12
bne	$9,$0,$L21
sra	$8,$8,31

andi	$6,$6,0x00ff
addu	$3,$7,$3
sb	$6,-1($4)
subu	$6,$0,$3
and	$7,$3,$12
bne	$7,$0,$L22
sra	$6,$6,31

$L16:
addu	$2,$2,$13
andi	$3,$3,0x00ff
addiu	$11,$11,-1
sb	$3,0($4)
xori	$10,$10,0x1
sb	$2,1($4)
bne	$11,$0,$L18
addu	$4,$4,$5

$L23:
j	$31
nop

.set	macro
.set	reorder
.end	vc1_h_overlap_c
.size	vc1_h_overlap_c, .-vc1_h_overlap_c
.section	.text.vc1_v_loop_filter8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_v_loop_filter8_c
.type	vc1_v_loop_filter8_c, @function
vc1_v_loop_filter8_c:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
subu	$8,$0,$5
lw	$15,%got(ff_cropTbl)($28)
sll	$11,$5,1
sll	$12,$8,2
addu	$13,$11,$5
sll	$8,$8,1
addu	$9,$4,$5
addu	$12,$4,$12
addiu	$sp,$sp,-24
addu	$10,$4,$8
addu	$5,$12,$5
sw	$20,20($sp)
addu	$8,$9,$8
sw	$19,16($sp)
addu	$13,$4,$13
sw	$18,12($sp)
addu	$11,$4,$11
sw	$17,8($sp)
addiu	$14,$4,8
sw	$16,4($sp)
addiu	$15,$15,1024
$L43:
lbu	$24,2($4)
lbu	$25,2($8)
lbu	$17,2($10)
lbu	$19,2($9)
subu	$7,$24,$25
subu	$3,$17,$19
sll	$2,$7,2
sll	$3,$3,1
addu	$2,$2,$7
addu	$2,$3,$2
addiu	$2,$2,4
sra	$16,$2,31
sra	$2,$2,3
xor	$2,$16,$2
subu	$2,$2,$16
slt	$3,$2,$6
beq	$3,$0,$L26
nop

lbu	$3,2($5)
lbu	$18,2($12)
lbu	$7,2($11)
subu	$3,$17,$3
lbu	$17,2($13)
subu	$18,$18,$25
sll	$20,$3,2
sll	$18,$18,1
addu	$3,$20,$3
subu	$19,$7,$19
addu	$3,$18,$3
sll	$7,$19,2
subu	$17,$24,$17
addiu	$3,$3,4
sll	$17,$17,1
addu	$7,$7,$19
sra	$3,$3,3
addu	$7,$17,$7
subu	$17,$0,$3
slt	$18,$3,0
addiu	$7,$7,4
movn	$3,$17,$18
sra	$7,$7,3
subu	$18,$0,$7
slt	$19,$7,0
slt	$17,$3,$2
bne	$17,$0,$L29
movn	$7,$18,$19

slt	$17,$7,$2
beq	$17,$0,$L26
nop

$L29:
subu	$24,$25,$24
sra	$17,$24,31
xor	$24,$17,$24
subu	$24,$24,$17
sra	$24,$24,1
beq	$24,$0,$L26
slt	$18,$3,$7

movz	$3,$7,$18
subu	$2,$3,$2
sll	$7,$2,2
addu	$2,$7,$2
sra	$3,$2,31
xor	$16,$3,$16
beq	$17,$16,$L61
xor	$2,$3,$2

$L31:
lbu	$18,0($4)
lbu	$19,0($8)
lbu	$2,0($10)
lbu	$3,0($9)
subu	$17,$18,$19
subu	$7,$2,$3
sll	$16,$17,2
sll	$7,$7,1
addu	$17,$16,$17
addu	$17,$7,$17
addiu	$17,$17,4
sra	$24,$17,31
sra	$17,$17,3
xor	$17,$24,$17
subu	$17,$17,$24
slt	$7,$17,$6
beq	$7,$0,$L35
nop

lbu	$7,0($5)
lbu	$16,0($12)
lbu	$20,0($11)
subu	$2,$2,$7
lbu	$7,0($13)
subu	$16,$16,$19
sll	$25,$2,2
sll	$16,$16,1
addu	$2,$25,$2
subu	$3,$20,$3
addu	$2,$16,$2
subu	$7,$18,$7
sll	$16,$3,2
addiu	$2,$2,4
sll	$7,$7,1
addu	$3,$16,$3
sra	$2,$2,3
addu	$3,$7,$3
subu	$7,$0,$2
slt	$16,$2,0
addiu	$3,$3,4
movn	$2,$7,$16
sra	$3,$3,3
subu	$16,$0,$3
slt	$20,$3,0
slt	$7,$2,$17
bne	$7,$0,$L34
movn	$3,$16,$20

slt	$7,$3,$17
beq	$7,$0,$L35
nop

$L34:
subu	$18,$19,$18
sra	$7,$18,31
xor	$18,$7,$18
subu	$18,$18,$7
sra	$18,$18,1
beq	$18,$0,$L35
slt	$16,$2,$3

movz	$2,$3,$16
subu	$17,$2,$17
sll	$2,$17,2
addu	$17,$2,$17
sra	$2,$17,31
xor	$24,$2,$24
beq	$7,$24,$L62
xor	$17,$2,$17

$L35:
lbu	$16,1($4)
lbu	$17,1($8)
lbu	$19,1($10)
lbu	$25,1($9)
subu	$7,$16,$17
subu	$3,$19,$25
sll	$2,$7,2
sll	$3,$3,1
addu	$2,$2,$7
addu	$2,$3,$2
addiu	$2,$2,4
sra	$18,$2,31
sra	$2,$2,3
xor	$2,$18,$2
subu	$2,$2,$18
slt	$3,$2,$6
beq	$3,$0,$L36
nop

lbu	$3,1($5)
lbu	$24,1($12)
lbu	$7,1($11)
subu	$3,$19,$3
lbu	$19,1($13)
subu	$24,$24,$17
sll	$20,$3,2
sll	$24,$24,1
addu	$3,$20,$3
subu	$25,$7,$25
addu	$3,$24,$3
subu	$19,$16,$19
sll	$7,$25,2
addiu	$3,$3,4
sll	$19,$19,1
addu	$7,$7,$25
sra	$3,$3,3
addu	$7,$19,$7
subu	$19,$0,$3
slt	$20,$3,0
addiu	$7,$7,4
movn	$3,$19,$20
sra	$7,$7,3
subu	$20,$0,$7
slt	$24,$7,0
slt	$19,$3,$2
bne	$19,$0,$L39
movn	$7,$20,$24

slt	$19,$7,$2
beq	$19,$0,$L36
nop

$L39:
subu	$16,$17,$16
sra	$19,$16,31
xor	$16,$19,$16
subu	$16,$16,$19
sra	$16,$16,1
beq	$16,$0,$L36
slt	$20,$3,$7

movz	$3,$7,$20
subu	$2,$3,$2
sll	$7,$2,2
addu	$2,$7,$2
sra	$3,$2,31
xor	$18,$3,$18
beq	$19,$18,$L63
xor	$2,$3,$2

$L36:
lbu	$16,3($4)
lbu	$17,3($8)
lbu	$19,3($10)
lbu	$25,3($9)
subu	$7,$16,$17
subu	$3,$19,$25
sll	$2,$7,2
sll	$3,$3,1
addu	$2,$2,$7
addu	$2,$3,$2
addiu	$2,$2,4
sra	$18,$2,31
sra	$2,$2,3
xor	$2,$18,$2
subu	$2,$2,$18
slt	$3,$2,$6
beq	$3,$0,$L26
nop

lbu	$3,3($5)
lbu	$24,3($12)
lbu	$7,3($11)
subu	$3,$19,$3
lbu	$19,3($13)
subu	$24,$24,$17
sll	$20,$3,2
sll	$24,$24,1
addu	$3,$20,$3
subu	$25,$7,$25
addu	$3,$24,$3
subu	$19,$16,$19
sll	$7,$25,2
addiu	$3,$3,4
sll	$19,$19,1
addu	$7,$7,$25
sra	$3,$3,3
addu	$7,$19,$7
subu	$19,$0,$3
slt	$20,$3,0
addiu	$7,$7,4
movn	$3,$19,$20
sra	$7,$7,3
subu	$20,$0,$7
slt	$24,$7,0
slt	$19,$3,$2
bne	$19,$0,$L42
movn	$7,$20,$24

slt	$19,$7,$2
beq	$19,$0,$L26
nop

$L42:
subu	$16,$17,$16
sra	$19,$16,31
xor	$16,$19,$16
subu	$16,$16,$19
sra	$16,$16,1
beq	$16,$0,$L26
slt	$20,$3,$7

movz	$3,$7,$20
subu	$2,$3,$2
sll	$7,$2,2
addu	$2,$7,$2
sra	$3,$2,31
xor	$18,$3,$18
beq	$19,$18,$L64
nop

$L26:
addiu	$4,$4,4
addiu	$10,$10,4
addiu	$9,$9,4
addiu	$8,$8,4
addiu	$12,$12,4
addiu	$5,$5,4
addiu	$13,$13,4
bne	$4,$14,$L43
addiu	$11,$11,4

lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,24

$L61:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$24,$2
movz	$24,$2,$3
xor	$24,$24,$17
subu	$17,$24,$17
subu	$25,$25,$17
addu	$25,$15,$25
lbu	$2,0($25)
sb	$2,2($8)
lbu	$2,2($4)
addu	$17,$17,$2
addu	$17,$15,$17
lbu	$2,0($17)
b	$L31
sb	$2,2($4)

$L62:
subu	$2,$17,$2
sra	$2,$2,3
slt	$3,$18,$2
movn	$2,$18,$3
xor	$2,$2,$7
subu	$7,$2,$7
subu	$19,$19,$7
addu	$19,$15,$19
lbu	$2,0($19)
sb	$2,0($8)
lbu	$2,0($4)
addu	$7,$7,$2
addu	$7,$15,$7
lbu	$2,0($7)
b	$L35
sb	$2,0($4)

$L64:
xor	$2,$3,$2
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$16,$2
movz	$16,$2,$3
xor	$16,$16,$19
subu	$19,$16,$19
subu	$17,$17,$19
addu	$17,$15,$17
lbu	$2,0($17)
sb	$2,3($8)
lbu	$2,3($4)
addu	$19,$19,$2
addu	$19,$15,$19
lbu	$2,0($19)
b	$L26
sb	$2,3($4)

$L63:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$16,$2
movz	$16,$2,$3
xor	$16,$16,$19
subu	$19,$16,$19
subu	$17,$17,$19
addu	$17,$15,$17
lbu	$2,0($17)
sb	$2,1($8)
lbu	$2,1($4)
addu	$19,$19,$2
addu	$19,$15,$19
lbu	$2,0($19)
b	$L36
sb	$2,1($4)

.set	macro
.set	reorder
.end	vc1_v_loop_filter8_c
.size	vc1_v_loop_filter8_c, .-vc1_v_loop_filter8_c
.section	.text.vc1_inv_trans_8x8_dc_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_8x8_dc_c
.type	vc1_inv_trans_8x8_dc_c, @function
vc1_inv_trans_8x8_dc_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lh	$3,0($6)
li	$12,8			# 0x8
sll	$2,$3,1
addu	$2,$2,$3
addiu	$2,$2,1
sra	$3,$2,1
sll	$2,$3,1
addu	$2,$2,$3
lw	$3,%got(ff_cropTbl)($28)
addiu	$2,$2,16
sra	$2,$2,5
addiu	$2,$2,1024
addu	$2,$3,$2
$L66:
lbu	$3,0($4)
addiu	$12,$12,-1
lbu	$11,1($4)
lbu	$10,2($4)
addu	$3,$2,$3
lbu	$9,3($4)
addu	$11,$2,$11
lbu	$8,4($4)
addu	$10,$2,$10
lbu	$7,5($4)
lbu	$13,0($3)
addu	$9,$2,$9
addu	$8,$2,$8
lbu	$6,6($4)
addu	$7,$2,$7
lbu	$3,7($4)
sb	$13,0($4)
addu	$6,$2,$6
lbu	$11,0($11)
addu	$3,$2,$3
sb	$11,1($4)
lbu	$10,0($10)
sb	$10,2($4)
lbu	$9,0($9)
sb	$9,3($4)
lbu	$8,0($8)
sb	$8,4($4)
lbu	$7,0($7)
sb	$7,5($4)
lbu	$6,0($6)
sb	$6,6($4)
lbu	$3,0($3)
sb	$3,7($4)
bne	$12,$0,$L66
addu	$4,$4,$5

j	$31
nop

.set	macro
.set	reorder
.end	vc1_inv_trans_8x8_dc_c
.size	vc1_inv_trans_8x8_dc_c, .-vc1_inv_trans_8x8_dc_c
.section	.text.vc1_inv_trans_8x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_8x8_c
.type	vc1_inv_trans_8x8_c, @function
vc1_inv_trans_8x8_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
addiu	$14,$4,128
sw	$fp,36($sp)
move	$2,$4
sw	$23,32($sp)
sw	$22,28($sp)
sw	$21,24($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
$L69:
lh	$20,2($2)
addiu	$2,$2,16
sll	$fp,$20,4
lh	$18,-10($2)
sll	$23,$20,3
lh	$6,-16($2)
subu	$13,$fp,$20
lh	$9,-8($2)
addu	$23,$23,$20
lh	$21,-6($2)
subu	$15,$0,$18
lh	$3,-4($2)
sll	$7,$18,4
lh	$17,-12($2)
addu	$11,$6,$9
subu	$9,$6,$9
lh	$19,-2($2)
sll	$6,$18,3
sll	$25,$11,2
sll	$24,$3,3
sll	$5,$17,3
sll	$10,$9,2
sll	$16,$3,1
sll	$9,$9,4
sll	$22,$17,1
subu	$7,$7,$18
sll	$8,$15,2
addu	$18,$6,$18
sll	$11,$11,4
sll	$6,$21,4
sll	$12,$21,3
sll	$15,$15,4
sll	$20,$20,2
addu	$7,$fp,$7
addu	$8,$13,$8
subu	$11,$11,$25
subu	$10,$9,$10
sll	$17,$17,4
subu	$16,$24,$16
subu	$22,$5,$22
sll	$9,$3,4
move	$13,$6
addu	$15,$23,$15
sll	$24,$21,2
sll	$23,$19,4
subu	$6,$6,$21
addu	$12,$12,$21
sll	$fp,$19,3
subu	$25,$20,$18
addu	$16,$17,$16
addiu	$11,$11,4
addiu	$10,$10,4
subu	$3,$22,$9
addu	$15,$15,$24
addu	$12,$7,$12
subu	$13,$8,$13
sll	$17,$19,2
addu	$fp,$fp,$19
subu	$9,$23,$19
addu	$24,$25,$6
addu	$8,$10,$3
subu	$5,$11,$16
addu	$9,$15,$9
addu	$7,$11,$16
subu	$3,$10,$3
addu	$12,$12,$17
subu	$13,$13,$fp
subu	$6,$24,$23
addu	$16,$7,$12
addu	$15,$8,$13
addu	$11,$3,$9
addu	$10,$5,$6
subu	$3,$3,$9
subu	$5,$5,$6
subu	$8,$8,$13
subu	$7,$7,$12
sra	$13,$16,3
sra	$12,$15,3
sra	$9,$11,3
sra	$6,$10,3
sh	$13,-16($2)
sra	$5,$5,3
sh	$12,-14($2)
sra	$3,$3,3
sh	$9,-12($2)
sra	$8,$8,3
sh	$6,-10($2)
sra	$7,$7,3
sh	$5,-8($2)
sh	$3,-6($2)
sh	$8,-4($2)
bne	$2,$14,$L69
sh	$7,-2($2)

addiu	$14,$4,16
$L70:
lh	$9,16($4)
addiu	$4,$4,2
sll	$21,$9,4
lh	$24,46($4)
sll	$20,$9,3
lh	$3,-2($4)
subu	$22,$21,$9
lh	$13,62($4)
addu	$20,$20,$9
lh	$18,78($4)
subu	$10,$0,$24
lh	$12,94($4)
sll	$8,$24,4
lh	$16,30($4)
addu	$11,$3,$13
subu	$13,$3,$13
lh	$17,110($4)
sll	$3,$24,3
sll	$2,$11,2
sll	$5,$13,2
sll	$23,$12,3
sll	$6,$16,3
sll	$7,$10,2
sll	$15,$12,1
sll	$19,$16,1
subu	$8,$8,$24
sll	$9,$9,2
addu	$24,$3,$24
sll	$11,$11,4
sll	$3,$18,4
sll	$13,$13,4
sll	$25,$18,3
sll	$10,$10,4
subu	$19,$6,$19
addu	$8,$21,$8
addu	$22,$22,$7
subu	$11,$11,$2
subu	$13,$13,$5
subu	$15,$23,$15
sll	$6,$12,4
move	$fp,$3
addu	$10,$20,$10
sll	$5,$18,2
subu	$12,$9,$24
subu	$3,$3,$18
sll	$16,$16,4
addu	$25,$25,$18
sll	$23,$17,3
sll	$20,$17,4
subu	$6,$19,$6
addu	$9,$12,$3
addiu	$11,$11,64
addu	$10,$10,$5
addiu	$13,$13,64
addu	$15,$16,$15
addu	$25,$8,$25
subu	$19,$22,$fp
sll	$21,$17,2
addu	$22,$23,$17
subu	$20,$20,$17
sll	$3,$17,4
addu	$8,$11,$15
subu	$3,$9,$3
addu	$7,$13,$6
subu	$5,$13,$6
subu	$2,$11,$15
addu	$6,$10,$20
addu	$21,$25,$21
subu	$22,$19,$22
subu	$11,$5,$6
subu	$12,$2,$3
subu	$10,$7,$22
subu	$9,$8,$21
addu	$5,$5,$6
addu	$2,$2,$3
addu	$8,$8,$21
addu	$7,$7,$22
sra	$8,$8,7
sra	$7,$7,7
sra	$5,$5,7
sra	$2,$2,7
sh	$8,-2($4)
addiu	$12,$12,1
sh	$7,14($4)
addiu	$11,$11,1
sh	$5,30($4)
addiu	$6,$10,1
sh	$2,46($4)
addiu	$3,$9,1
sra	$8,$12,7
sra	$7,$11,7
sra	$5,$6,7
sra	$2,$3,7
sh	$8,62($4)
sh	$7,78($4)
sh	$5,94($4)
bne	$4,$14,$L70
sh	$2,110($4)

lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	vc1_inv_trans_8x8_c
.size	vc1_inv_trans_8x8_c, .-vc1_inv_trans_8x8_c
.section	.text.vc1_inv_trans_8x4_dc_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_8x4_dc_c
.type	vc1_inv_trans_8x4_dc_c, @function
vc1_inv_trans_8x4_dc_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lh	$3,0($6)
li	$12,4			# 0x4
sll	$2,$3,1
addu	$2,$2,$3
addiu	$2,$2,1
sra	$3,$2,1
sll	$2,$3,4
addu	$2,$2,$3
lw	$3,%got(ff_cropTbl)($28)
addiu	$2,$2,64
sra	$2,$2,7
addiu	$2,$2,1024
addu	$2,$3,$2
$L75:
lbu	$3,0($4)
addiu	$12,$12,-1
lbu	$11,1($4)
lbu	$10,2($4)
addu	$3,$2,$3
lbu	$9,3($4)
addu	$11,$2,$11
lbu	$8,4($4)
addu	$10,$2,$10
lbu	$7,5($4)
lbu	$13,0($3)
addu	$9,$2,$9
addu	$8,$2,$8
lbu	$6,6($4)
addu	$7,$2,$7
lbu	$3,7($4)
sb	$13,0($4)
addu	$6,$2,$6
lbu	$11,0($11)
addu	$3,$2,$3
sb	$11,1($4)
lbu	$10,0($10)
sb	$10,2($4)
lbu	$9,0($9)
sb	$9,3($4)
lbu	$8,0($8)
sb	$8,4($4)
lbu	$7,0($7)
sb	$7,5($4)
lbu	$6,0($6)
sb	$6,6($4)
lbu	$3,0($3)
sb	$3,7($4)
bne	$12,$0,$L75
addu	$4,$4,$5

j	$31
nop

.set	macro
.set	reorder
.end	vc1_inv_trans_8x4_dc_c
.size	vc1_inv_trans_8x4_dc_c, .-vc1_inv_trans_8x4_dc_c
.section	.text.vc1_inv_trans_8x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_8x4_c
.type	vc1_inv_trans_8x4_c, @function
vc1_inv_trans_8x4_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$2,$6,64
addiu	$sp,$sp,-56
.cprestore	0
sw	$2,8($sp)
move	$2,$6
sw	$fp,52($sp)
sw	$23,48($sp)
sw	$22,44($sp)
sw	$21,40($sp)
sw	$20,36($sp)
sw	$19,32($sp)
sw	$18,28($sp)
sw	$17,24($sp)
sw	$16,20($sp)
sw	$5,60($sp)
$L78:
lh	$11,2($2)
addiu	$2,$2,16
sll	$19,$11,4
lh	$8,-10($2)
sll	$18,$11,3
lh	$9,-8($2)
subu	$20,$19,$11
lh	$7,-16($2)
addu	$18,$18,$11
lh	$14,-6($2)
subu	$13,$0,$8
lh	$5,-4($2)
sll	$24,$8,4
addu	$3,$7,$9
lh	$25,-12($2)
subu	$7,$7,$9
lh	$10,-2($2)
sll	$9,$8,3
sll	$fp,$3,2
sll	$22,$5,3
sll	$12,$5,1
subu	$24,$24,$8
sll	$15,$14,3
sll	$16,$13,2
sll	$11,$11,2
addu	$8,$9,$8
sll	$3,$3,4
sll	$9,$14,4
sll	$13,$13,4
subu	$12,$22,$12
addu	$15,$15,$14
addu	$13,$18,$13
sll	$23,$7,2
sll	$17,$25,1
sll	$7,$7,4
sll	$21,$25,3
subu	$3,$3,$fp
sll	$18,$14,2
subu	$8,$11,$8
subu	$14,$9,$14
sll	$25,$25,4
subu	$23,$7,$23
addu	$25,$25,$12
addu	$24,$19,$24
sll	$22,$10,4
sll	$19,$10,3
addiu	$fp,$3,4
addu	$8,$8,$14
subu	$17,$21,$17
sll	$5,$5,4
addu	$16,$20,$16
subu	$5,$17,$5
addiu	$23,$23,4
sll	$12,$10,2
addu	$19,$19,$10
addu	$18,$13,$18
subu	$3,$fp,$25
subu	$13,$22,$10
subu	$10,$8,$22
addu	$24,$24,$15
subu	$16,$16,$9
addu	$20,$23,$5
subu	$7,$23,$5
addu	$5,$3,$10
subu	$3,$3,$10
addu	$15,$fp,$25
addu	$24,$24,$12
subu	$16,$16,$19
addu	$13,$18,$13
sra	$3,$3,3
addu	$11,$15,$24
addu	$9,$20,$16
addu	$8,$7,$13
sh	$3,-8($2)
subu	$7,$7,$13
lw	$3,8($sp)
subu	$20,$20,$16
subu	$15,$15,$24
sra	$10,$11,3
sra	$9,$9,3
sra	$8,$8,3
sra	$5,$5,3
sh	$10,-16($2)
sra	$7,$7,3
sh	$9,-14($2)
sra	$20,$20,3
sh	$8,-12($2)
sra	$15,$15,3
sh	$5,-10($2)
sh	$7,-6($2)
sh	$20,-4($2)
bne	$2,$3,$L78
sh	$15,-2($2)

lw	$2,60($sp)
addiu	$15,$4,8
lw	$12,%got(ff_cropTbl)($28)
addu	$14,$4,$2
addiu	$12,$12,1024
addu	$13,$14,$2
addu	$5,$13,$2
$L79:
lh	$11,16($6)
addiu	$4,$4,1
lh	$16,0($6)
addiu	$14,$14,1
lh	$3,32($6)
addiu	$13,$13,1
sll	$10,$11,2
lh	$18,48($6)
sll	$9,$11,4
lbu	$17,-1($4)
addu	$2,$16,$3
subu	$9,$9,$10
sll	$19,$2,4
subu	$9,$9,$11
sll	$8,$18,1
sll	$10,$18,3
addu	$2,$19,$2
sll	$9,$9,1
addu	$10,$8,$10
addiu	$2,$2,64
addu	$10,$9,$10
sll	$8,$18,2
addu	$19,$2,$10
sll	$9,$18,4
sra	$19,$19,7
subu	$3,$16,$3
addu	$16,$17,$19
subu	$8,$9,$8
addu	$9,$12,$16
sll	$7,$3,4
sll	$16,$11,1
subu	$8,$8,$18
lbu	$9,0($9)
sll	$11,$11,3
addu	$7,$7,$3
sll	$8,$8,1
addu	$11,$16,$11
sb	$9,-1($4)
addiu	$3,$7,64
lbu	$9,-1($14)
subu	$7,$8,$11
addiu	$5,$5,1
subu	$8,$3,$7
addu	$3,$3,$7
sra	$7,$8,7
sra	$3,$3,7
addu	$7,$9,$7
subu	$2,$2,$10
addu	$7,$12,$7
sra	$2,$2,7
addiu	$6,$6,2
lbu	$7,0($7)
sb	$7,-1($14)
lbu	$7,-1($13)
addu	$3,$7,$3
addu	$3,$12,$3
lbu	$3,0($3)
sb	$3,-1($13)
lbu	$10,-1($5)
addu	$2,$10,$2
addu	$2,$12,$2
lbu	$2,0($2)
bne	$4,$15,$L79
sb	$2,-1($5)

lw	$fp,52($sp)
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	vc1_inv_trans_8x4_c
.size	vc1_inv_trans_8x4_c, .-vc1_inv_trans_8x4_c
.section	.text.vc1_inv_trans_4x8_dc_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_4x8_dc_c
.type	vc1_inv_trans_4x8_dc_c, @function
vc1_inv_trans_4x8_dc_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lh	$3,0($6)
li	$9,8			# 0x8
sll	$2,$3,4
addu	$2,$2,$3
addiu	$2,$2,4
sra	$2,$2,3
sll	$3,$2,2
sll	$2,$2,4
subu	$2,$2,$3
addiu	$2,$2,64
sra	$2,$2,7
addiu	$3,$2,1024
lw	$2,%got(ff_cropTbl)($28)
addu	$2,$2,$3
$L84:
lbu	$8,0($4)
addiu	$9,$9,-1
lbu	$7,1($4)
lbu	$6,2($4)
addu	$8,$2,$8
lbu	$3,3($4)
addu	$7,$2,$7
addu	$6,$2,$6
lbu	$8,0($8)
addu	$3,$2,$3
sb	$8,0($4)
lbu	$7,0($7)
sb	$7,1($4)
lbu	$6,0($6)
sb	$6,2($4)
lbu	$3,0($3)
sb	$3,3($4)
bne	$9,$0,$L84
addu	$4,$4,$5

j	$31
nop

.set	macro
.set	reorder
.end	vc1_inv_trans_4x8_dc_c
.size	vc1_inv_trans_4x8_dc_c, .-vc1_inv_trans_4x8_dc_c
.section	.text.vc1_inv_trans_4x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_4x8_c
.type	vc1_inv_trans_4x8_c, @function
vc1_inv_trans_4x8_c:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-64
addiu	$11,$6,128
.cprestore	0
move	$12,$6
sw	$fp,60($sp)
sw	$23,56($sp)
sw	$22,52($sp)
sw	$21,48($sp)
sw	$20,44($sp)
sw	$19,40($sp)
sw	$18,36($sp)
sw	$17,32($sp)
sw	$16,28($sp)
$L87:
lh	$13,2($12)
addiu	$12,$12,16
sll	$3,$13,2
lh	$15,-10($12)
sll	$8,$13,4
lh	$7,-12($12)
lh	$14,-16($12)
subu	$8,$8,$3
sll	$10,$15,4
sll	$9,$15,2
addu	$2,$14,$7
subu	$9,$10,$9
subu	$14,$14,$7
sll	$10,$13,1
sll	$3,$2,4
sll	$7,$14,4
subu	$8,$8,$13
sll	$16,$15,1
sll	$17,$15,3
subu	$9,$9,$15
sll	$13,$13,3
addu	$7,$7,$14
addu	$3,$3,$2
sll	$8,$8,1
sll	$9,$9,1
addu	$13,$10,$13
addu	$14,$16,$17
addiu	$2,$3,4
addiu	$3,$7,4
addu	$7,$8,$14
subu	$8,$9,$13
addu	$10,$2,$7
subu	$9,$3,$8
subu	$2,$2,$7
addu	$3,$3,$8
sra	$7,$9,3
sra	$8,$10,3
sra	$3,$3,3
sra	$2,$2,3
sh	$7,-14($12)
sh	$8,-16($12)
sh	$3,-12($12)
bne	$12,$11,$L87
sh	$2,-10($12)

addu	$fp,$4,$5
lw	$19,%got(ff_cropTbl)($28)
addiu	$2,$6,8
addu	$23,$fp,$5
addiu	$19,$19,1024
addu	$22,$23,$5
sw	$2,16($sp)
addu	$21,$22,$5
addu	$20,$21,$5
addu	$16,$20,$5
addu	$5,$16,$5
sw	$5,8($sp)
$L88:
lh	$14,48($6)
addiu	$6,$6,2
addiu	$4,$4,1
lbu	$10,-1($4)
addiu	$fp,$fp,1
subu	$2,$0,$14
lh	$3,62($6)
sll	$9,$14,4
lh	$5,-2($6)
addiu	$20,$20,1
lh	$13,78($6)
subu	$9,$9,$14
lh	$12,14($6)
addiu	$21,$21,1
lh	$25,94($6)
addiu	$16,$16,1
lh	$7,30($6)
sll	$8,$13,3
lh	$11,110($6)
sll	$24,$12,4
sw	$2,12($sp)
addu	$2,$5,$3
sll	$18,$25,3
sll	$17,$2,2
sll	$15,$25,1
sll	$2,$2,4
addu	$9,$24,$9
subu	$2,$2,$17
subu	$15,$18,$15
sll	$17,$7,4
addu	$8,$8,$13
addu	$15,$17,$15
addiu	$2,$2,64
sll	$17,$11,2
addu	$8,$9,$8
subu	$5,$5,$3
addu	$9,$2,$15
sll	$3,$7,1
addu	$8,$8,$17
sll	$18,$5,2
addu	$17,$9,$8
sw	$3,20($sp)
lw	$3,12($sp)
sll	$5,$5,4
sra	$17,$17,7
sll	$7,$7,3
addu	$17,$10,$17
subu	$10,$24,$12
sll	$24,$3,2
lw	$3,20($sp)
addu	$17,$19,$17
subu	$5,$5,$18
subu	$7,$7,$3
addu	$24,$10,$24
sll	$3,$25,4
lbu	$25,0($17)
sll	$10,$11,3
sll	$18,$13,4
addiu	$17,$5,64
subu	$3,$7,$3
sb	$25,-1($4)
addu	$5,$10,$11
lbu	$25,-1($fp)
subu	$18,$24,$18
addu	$10,$17,$3
subu	$18,$18,$5
lw	$5,12($sp)
sll	$24,$12,3
addu	$7,$10,$18
addu	$24,$24,$12
sra	$7,$7,7
sll	$5,$5,4
addu	$7,$25,$7
addu	$24,$24,$5
addu	$7,$19,$7
sll	$25,$13,2
sll	$5,$11,4
lbu	$7,0($7)
addu	$24,$24,$25
subu	$25,$5,$11
subu	$3,$17,$3
addu	$24,$24,$25
sb	$7,-1($fp)
sll	$7,$14,3
addu	$17,$3,$24
lbu	$5,0($23)
addu	$14,$7,$14
sra	$17,$17,7
sll	$12,$12,2
addu	$17,$5,$17
sll	$25,$13,4
addu	$17,$19,$17
subu	$12,$12,$14
subu	$13,$25,$13
lbu	$5,0($17)
sll	$11,$11,4
addu	$12,$12,$13
subu	$2,$2,$15
subu	$11,$12,$11
sb	$5,0($23)
subu	$3,$3,$24
addu	$7,$2,$11
lbu	$12,0($22)
subu	$2,$2,$11
sra	$7,$7,7
addiu	$2,$2,1
addu	$7,$12,$7
sra	$2,$2,7
addu	$7,$19,$7
addiu	$3,$3,1
subu	$10,$10,$18
lbu	$5,0($7)
sra	$3,$3,7
addiu	$10,$10,1
subu	$8,$9,$8
sra	$10,$10,7
sb	$5,0($22)
addiu	$8,$8,1
lbu	$5,-1($21)
addiu	$23,$23,1
sra	$8,$8,7
addu	$2,$5,$2
lw	$5,8($sp)
addu	$2,$19,$2
addiu	$5,$5,1
lbu	$2,0($2)
sw	$5,8($sp)
sb	$2,-1($21)
lbu	$2,-1($20)
addu	$2,$2,$3
addu	$2,$19,$2
lbu	$2,0($2)
sb	$2,-1($20)
lbu	$18,-1($16)
addu	$10,$18,$10
addu	$10,$19,$10
lbu	$2,0($10)
sb	$2,-1($16)
lbu	$9,-1($5)
addu	$8,$9,$8
addu	$8,$19,$8
lbu	$2,0($8)
sb	$2,-1($5)
lw	$2,16($sp)
bne	$6,$2,$L88
addiu	$22,$22,1

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	vc1_inv_trans_4x8_c
.size	vc1_inv_trans_4x8_c, .-vc1_inv_trans_4x8_c
.section	.text.vc1_inv_trans_4x4_dc_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_4x4_dc_c
.type	vc1_inv_trans_4x4_dc_c, @function
vc1_inv_trans_4x4_dc_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lh	$3,0($6)
li	$9,4			# 0x4
sll	$2,$3,4
addu	$2,$2,$3
addiu	$2,$2,4
sra	$3,$2,3
sll	$2,$3,4
addu	$2,$2,$3
addiu	$2,$2,64
sra	$2,$2,7
addiu	$3,$2,1024
lw	$2,%got(ff_cropTbl)($28)
addu	$2,$2,$3
$L93:
lbu	$8,0($4)
addiu	$9,$9,-1
lbu	$7,1($4)
lbu	$6,2($4)
addu	$8,$2,$8
lbu	$3,3($4)
addu	$7,$2,$7
addu	$6,$2,$6
lbu	$8,0($8)
addu	$3,$2,$3
sb	$8,0($4)
lbu	$7,0($7)
sb	$7,1($4)
lbu	$6,0($6)
sb	$6,2($4)
lbu	$3,0($3)
sb	$3,3($4)
bne	$9,$0,$L93
addu	$4,$4,$5

j	$31
nop

.set	macro
.set	reorder
.end	vc1_inv_trans_4x4_dc_c
.size	vc1_inv_trans_4x4_dc_c, .-vc1_inv_trans_4x4_dc_c
.section	.text.vc1_inv_trans_4x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_inv_trans_4x4_c
.type	vc1_inv_trans_4x4_c, @function
vc1_inv_trans_4x4_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
addiu	$11,$6,64
sw	$17,4($sp)
move	$2,$6
sw	$16,0($sp)
$L96:
lh	$13,2($2)
addiu	$2,$2,16
sll	$7,$13,2
lh	$15,-10($2)
sll	$9,$13,4
lh	$8,-12($2)
lh	$14,-16($2)
subu	$9,$9,$7
sll	$12,$15,4
sll	$10,$15,2
addu	$3,$14,$8
subu	$10,$12,$10
subu	$14,$14,$8
sll	$12,$13,1
sll	$7,$3,4
sll	$8,$14,4
subu	$9,$9,$13
sll	$16,$15,1
sll	$17,$15,3
subu	$10,$10,$15
sll	$13,$13,3
addu	$8,$8,$14
addu	$7,$7,$3
sll	$9,$9,1
sll	$10,$10,1
addu	$13,$12,$13
addu	$14,$16,$17
addiu	$3,$7,4
addiu	$7,$8,4
addu	$8,$9,$14
subu	$9,$10,$13
addu	$12,$3,$8
subu	$10,$7,$9
subu	$3,$3,$8
addu	$7,$7,$9
sra	$8,$10,3
sra	$9,$12,3
sra	$7,$7,3
sra	$3,$3,3
sh	$8,-14($2)
sh	$9,-16($2)
sh	$7,-12($2)
bne	$2,$11,$L96
sh	$3,-10($2)

addu	$14,$4,$5
lw	$12,%got(ff_cropTbl)($28)
addiu	$15,$4,4
addu	$13,$14,$5
addiu	$12,$12,1024
addu	$5,$13,$5
$L97:
lh	$11,16($6)
addiu	$4,$4,1
lh	$24,0($6)
addiu	$14,$14,1
lh	$3,32($6)
addiu	$13,$13,1
sll	$10,$11,2
lh	$16,48($6)
sll	$9,$11,4
lbu	$25,-1($4)
addu	$2,$24,$3
subu	$9,$9,$10
sll	$8,$16,1
sll	$17,$2,4
subu	$9,$9,$11
sll	$10,$16,3
addu	$2,$17,$2
sll	$9,$9,1
addu	$10,$8,$10
addiu	$2,$2,64
addu	$10,$9,$10
sll	$8,$16,2
addu	$17,$2,$10
sll	$9,$16,4
sra	$17,$17,7
subu	$3,$24,$3
addu	$24,$25,$17
subu	$8,$9,$8
addu	$9,$12,$24
sll	$7,$3,4
subu	$8,$8,$16
sll	$16,$11,1
lbu	$9,0($9)
sll	$11,$11,3
addu	$7,$7,$3
sll	$8,$8,1
addu	$11,$16,$11
sb	$9,-1($4)
addiu	$3,$7,64
lbu	$9,-1($14)
subu	$7,$8,$11
addiu	$5,$5,1
subu	$8,$3,$7
addu	$3,$3,$7
sra	$7,$8,7
sra	$3,$3,7
addu	$7,$9,$7
subu	$2,$2,$10
addu	$7,$12,$7
sra	$2,$2,7
addiu	$6,$6,2
lbu	$7,0($7)
sb	$7,-1($14)
lbu	$7,-1($13)
addu	$3,$7,$3
addu	$3,$12,$3
lbu	$3,0($3)
sb	$3,-1($13)
lbu	$10,-1($5)
addu	$2,$10,$2
addu	$2,$12,$2
lbu	$2,0($2)
bne	$4,$15,$L97
sb	$2,-1($5)

lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	vc1_inv_trans_4x4_c
.size	vc1_inv_trans_4x4_c, .-vc1_inv_trans_4x4_c
.section	.text.put_vc1_mspel_mc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc
.type	put_vc1_mspel_mc, @function
put_vc1_mspel_mc:
.frame	$sp,240,$31		# vars= 192, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-240
.cprestore	0
sw	$17,208($sp)
sw	$fp,236($sp)
sw	$23,232($sp)
sw	$22,228($sp)
sw	$21,224($sp)
sw	$20,220($sp)
sw	$19,216($sp)
sw	$18,212($sp)
sw	$16,204($sp)
lw	$8,256($sp)
bne	$8,$0,$L102
lw	$17,260($sp)

li	$16,8			# 0x8
li	$11,1			# 0x1
li	$13,-256			# 0xffffffffffffff00
li	$15,53			# 0x35
li	$14,2			# 0x2
li	$18,3			# 0x3
slt	$12,$7,2
$L103:
addiu	$10,$5,8
move	$9,$4
move	$8,$5
$L141:
beq	$7,$11,$L133
nop

bne	$12,$0,$L163
nop

beq	$7,$14,$L136
nop

bne	$7,$18,$L140
move	$2,$0

lbu	$22,-1($8)
lbu	$19,0($8)
lbu	$3,1($8)
sll	$2,$22,1
lbu	$21,2($8)
sll	$20,$19,1
sll	$19,$19,4
addu	$2,$2,$22
addu	$19,$20,$19
sll	$20,$21,2
subu	$2,$19,$2
mul	$19,$3,$15
addu	$2,$19,$2
subu	$2,$2,$20
addiu	$2,$2,32
subu	$2,$2,$17
sra	$2,$2,6
$L138:
and	$3,$2,$13
beq	$3,$0,$L139
nop

$L164:
subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L140:
addiu	$8,$8,1
sb	$2,0($9)
bne	$10,$8,$L141
addiu	$9,$9,1

addiu	$16,$16,-1
addu	$4,$4,$6
bne	$16,$0,$L103
addu	$5,$5,$6

$L101:
lw	$fp,236($sp)
lw	$23,232($sp)
lw	$22,228($sp)
lw	$21,224($sp)
lw	$20,220($sp)
lw	$19,216($sp)
lw	$18,212($sp)
lw	$17,208($sp)
lw	$16,204($sp)
j	$31
addiu	$sp,$sp,240

$L163:
bne	$7,$0,$L146
nop

b	$L140
lbu	$2,0($8)

$L136:
lbu	$3,0($8)
lbu	$21,1($8)
lbu	$2,-1($8)
sll	$19,$3,3
lbu	$22,2($8)
sll	$20,$21,3
addu	$3,$19,$3
addu	$19,$20,$21
subu	$3,$3,$2
addu	$2,$3,$19
subu	$2,$2,$22
addiu	$2,$2,8
subu	$2,$2,$17
sra	$2,$2,4
and	$3,$2,$13
bne	$3,$0,$L164
nop

$L139:
b	$L140
andi	$2,$2,0x00ff

$L146:
b	$L140
move	$2,$0

$L133:
lbu	$3,0($8)
lbu	$20,1($8)
lbu	$23,-1($8)
mul	$19,$3,$15
lbu	$2,2($8)
sll	$22,$20,1
sll	$3,$23,2
sll	$20,$20,4
sll	$21,$2,1
addu	$20,$22,$20
addu	$21,$21,$2
subu	$3,$19,$3
addu	$2,$3,$20
subu	$2,$2,$21
addiu	$2,$2,32
subu	$2,$2,$17
b	$L138
sra	$2,$2,6

$L102:
beq	$7,$0,$L104
li	$2,1			# 0x1

lw	$9,%got(shift_value.5621)($28)
sll	$3,$7,2
sll	$2,$8,2
sw	$17,196($sp)
sll	$10,$6,1
sw	$4,188($sp)
addiu	$9,$9,%lo(shift_value.5621)
sw	$7,192($sp)
addiu	$14,$10,-1
addu	$2,$2,$9
sw	$10,184($sp)
addu	$3,$3,$9
addu	$14,$5,$14
lw	$25,0($2)
li	$2,1			# 0x1
lw	$3,0($3)
addiu	$5,$sp,184
addiu	$21,$sp,8
addu	$13,$10,$6
addu	$25,$3,$25
subu	$15,$6,$10
sra	$25,$25,1
li	$16,2			# 0x2
addiu	$24,$25,-1
li	$18,3			# 0x3
sll	$24,$2,$24
li	$20,53			# 0x35
addu	$24,$24,$17
li	$19,1			# 0x1
addiu	$24,$24,-1
move	$17,$5
$L105:
lw	$5,184($sp)
subu	$11,$14,$13
addu	$9,$15,$14
addiu	$12,$14,11
subu	$10,$14,$5
move	$7,$21
move	$5,$14
$L110:
beq	$8,$16,$L107
nop

beq	$8,$18,$L108
move	$2,$0

bne	$8,$19,$L167
addu	$2,$24,$2

lbu	$4,0($10)
lbu	$22,0($9)
lbu	$3,0($11)
mul	$4,$4,$20
lbu	$fp,0($5)
sll	$2,$22,1
sll	$3,$3,2
sll	$22,$22,4
sll	$23,$fp,1
addu	$22,$2,$22
addu	$23,$23,$fp
subu	$3,$4,$3
addu	$2,$3,$22
subu	$2,$2,$23
$L106:
addu	$2,$24,$2
$L167:
addiu	$7,$7,2
sra	$2,$2,$25
addiu	$5,$5,1
addiu	$11,$11,1
addiu	$10,$10,1
sh	$2,-2($7)
bne	$12,$5,$L110
addiu	$9,$9,1

addiu	$21,$21,22
bne	$21,$17,$L105
addu	$14,$14,$6

lw	$17,196($sp)
addiu	$10,$sp,24
lw	$4,188($sp)
li	$2,64			# 0x40
lw	$7,192($sp)
addiu	$16,$sp,200
subu	$17,$2,$17
li	$12,2			# 0x2
li	$13,3			# 0x3
li	$15,53			# 0x35
li	$14,1			# 0x1
li	$11,-256			# 0xffffffffffffff00
addiu	$8,$10,-16
move	$9,$4
$L119:
beq	$7,$12,$L114
nop

beq	$7,$13,$L115
nop

beq	$7,$14,$L165
nop

move	$2,$0
$L113:
addu	$2,$17,$2
sra	$2,$2,7
and	$3,$2,$11
beq	$3,$0,$L117
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L118:
addiu	$8,$8,2
sb	$2,0($9)
bne	$10,$8,$L119
addiu	$9,$9,1

addiu	$10,$10,22
beq	$10,$16,$L101
addu	$4,$4,$6

move	$9,$4
b	$L119
addiu	$8,$10,-16

$L104:
li	$18,8			# 0x8
subu	$17,$2,$17
li	$13,2			# 0x2
li	$15,-256			# 0xffffffffffffff00
li	$14,3			# 0x3
li	$19,53			# 0x35
li	$16,1			# 0x1
li	$12,8			# 0x8
$L122:
move	$11,$5
subu	$10,$5,$6
addu	$5,$5,$6
move	$7,$0
move	$9,$5
$L130:
beq	$8,$13,$L124
addu	$22,$7,$6

beq	$8,$14,$L125
addu	$22,$5,$22

beq	$8,$16,$L166
nop

move	$2,$0
$L129:
addu	$3,$4,$7
addiu	$7,$7,1
sb	$2,0($3)
addiu	$11,$11,1
addiu	$10,$10,1
bne	$7,$12,$L130
addiu	$9,$9,1

addiu	$18,$18,-1
bne	$18,$0,$L122
addu	$4,$4,$6

lw	$fp,236($sp)
lw	$23,232($sp)
lw	$22,228($sp)
lw	$21,224($sp)
lw	$20,220($sp)
lw	$19,216($sp)
lw	$18,212($sp)
lw	$17,208($sp)
lw	$16,204($sp)
j	$31
addiu	$sp,$sp,240

$L117:
b	$L118
andi	$2,$2,0x00ff

$L108:
lbu	$fp,0($11)
lbu	$4,0($10)
lbu	$3,0($9)
sll	$2,$fp,1
lbu	$23,0($5)
sll	$22,$4,1
sll	$4,$4,4
addu	$2,$2,$fp
addu	$4,$22,$4
sll	$22,$23,2
subu	$2,$4,$2
mul	$4,$3,$20
addu	$2,$4,$2
b	$L106
subu	$2,$2,$22

$L107:
lbu	$3,0($10)
lbu	$2,0($9)
lbu	$fp,0($11)
sll	$4,$3,3
lbu	$22,0($5)
sll	$23,$2,3
addu	$3,$4,$3
addu	$4,$23,$2
subu	$3,$3,$fp
addu	$2,$3,$4
b	$L106
subu	$2,$2,$22

$L165:
lh	$3,2($8)
lh	$18,4($8)
lh	$21,0($8)
mul	$5,$3,$15
lh	$2,6($8)
sll	$20,$18,1
sll	$3,$21,2
sll	$18,$18,4
sll	$19,$2,1
addu	$18,$20,$18
addu	$19,$19,$2
subu	$3,$5,$3
addu	$2,$3,$18
b	$L113
subu	$2,$2,$19

$L115:
lh	$20,0($8)
lh	$5,2($8)
lh	$3,4($8)
sll	$2,$20,1
lh	$19,6($8)
sll	$18,$5,1
sll	$5,$5,4
addu	$2,$2,$20
addu	$5,$18,$5
sll	$18,$19,2
subu	$2,$5,$2
mul	$5,$3,$15
addu	$2,$5,$2
b	$L113
subu	$2,$2,$18

$L114:
lh	$3,2($8)
lh	$19,4($8)
lh	$2,0($8)
sll	$5,$3,3
lh	$20,6($8)
sll	$18,$19,3
addu	$3,$5,$3
addu	$5,$18,$19
subu	$3,$3,$2
addu	$2,$3,$5
b	$L113
subu	$2,$2,$20

$L124:
lbu	$3,0($11)
lbu	$23,0($9)
lbu	$24,0($10)
addu	$22,$5,$22
sll	$20,$3,3
sll	$21,$23,3
addu	$3,$20,$3
lbu	$22,0($22)
addu	$20,$21,$23
subu	$3,$3,$24
addu	$2,$3,$20
subu	$2,$2,$22
addiu	$2,$2,8
subu	$2,$2,$17
sra	$2,$2,4
$L127:
and	$3,$2,$15
beq	$3,$0,$L161
nop

subu	$2,$0,$2
sra	$2,$2,31
$L161:
b	$L129
andi	$2,$2,0x00ff

$L166:
lbu	$20,0($11)
addu	$22,$7,$6
lbu	$21,0($9)
addu	$22,$5,$22
lbu	$3,0($10)
mul	$20,$20,$19
sll	$23,$21,1
lbu	$2,0($22)
sll	$3,$3,2
sll	$21,$21,4
sll	$22,$2,1
addu	$21,$23,$21
subu	$3,$20,$3
addu	$20,$22,$2
addu	$2,$3,$21
subu	$2,$2,$20
addiu	$2,$2,32
subu	$2,$2,$17
b	$L127
sra	$2,$2,6

$L125:
lbu	$23,0($10)
lbu	$20,0($11)
lbu	$3,0($9)
sll	$2,$23,1
sll	$21,$20,1
sll	$20,$20,4
lbu	$22,0($22)
addu	$2,$2,$23
addu	$20,$21,$20
sll	$21,$22,2
subu	$2,$20,$2
mul	$20,$3,$19
addu	$2,$20,$2
subu	$2,$2,$21
addiu	$2,$2,32
subu	$2,$2,$17
b	$L127
sra	$2,$2,6

.set	macro
.set	reorder
.end	put_vc1_mspel_mc
.size	put_vc1_mspel_mc, .-put_vc1_mspel_mc
.section	.text.avg_vc1_mspel_mc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc
.type	avg_vc1_mspel_mc, @function
avg_vc1_mspel_mc:
.frame	$sp,240,$31		# vars= 192, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-240
.cprestore	0
sw	$17,208($sp)
sw	$fp,236($sp)
sw	$23,232($sp)
sw	$22,228($sp)
sw	$21,224($sp)
sw	$20,220($sp)
sw	$19,216($sp)
sw	$18,212($sp)
sw	$16,204($sp)
lw	$8,256($sp)
bne	$8,$0,$L169
lw	$17,260($sp)

li	$18,8			# 0x8
li	$12,1			# 0x1
li	$14,-256			# 0xffffffffffffff00
li	$16,53			# 0x35
li	$15,2			# 0x2
li	$19,3			# 0x3
slt	$13,$7,2
$L170:
move	$9,$4
move	$8,$5
b	$L208
addiu	$10,$5,8

$L201:
beq	$7,$15,$L203
nop

bne	$7,$19,$L207
move	$2,$0

lbu	$23,-1($8)
lbu	$21,0($8)
lbu	$11,1($8)
sll	$3,$23,1
lbu	$22,2($8)
sll	$2,$21,1
sll	$21,$21,4
addu	$3,$3,$23
addu	$21,$2,$21
sll	$2,$22,2
subu	$3,$21,$3
mul	$21,$11,$16
addu	$3,$21,$3
subu	$3,$3,$2
addiu	$3,$3,32
subu	$3,$3,$17
sra	$2,$3,6
$L205:
and	$3,$2,$14
beq	$3,$0,$L207
subu	$3,$0,$2

sra	$3,$3,31
andi	$2,$3,0xff
$L207:
addu	$2,$20,$2
addiu	$9,$9,1
addiu	$2,$2,1
addiu	$8,$8,1
sra	$2,$2,1
beq	$8,$10,$L233
sb	$2,-1($9)

$L208:
beq	$7,$12,$L200
lbu	$20,0($9)

beq	$13,$0,$L201
nop

bne	$7,$0,$L207
move	$2,$0

lbu	$2,0($8)
addiu	$9,$9,1
addiu	$8,$8,1
addu	$2,$20,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$8,$10,$L208
sb	$2,-1($9)

$L233:
addiu	$18,$18,-1
addu	$4,$4,$6
bne	$18,$0,$L170
addu	$5,$5,$6

lw	$fp,236($sp)
lw	$23,232($sp)
lw	$22,228($sp)
lw	$21,224($sp)
lw	$20,220($sp)
lw	$19,216($sp)
lw	$18,212($sp)
lw	$17,208($sp)
lw	$16,204($sp)
j	$31
addiu	$sp,$sp,240

$L200:
lbu	$11,0($8)
lbu	$21,1($8)
lbu	$3,-1($8)
mul	$11,$11,$16
lbu	$2,2($8)
sll	$23,$21,1
sll	$3,$3,2
sll	$21,$21,4
sll	$22,$2,1
addu	$21,$23,$21
addu	$22,$22,$2
subu	$3,$11,$3
addu	$2,$3,$21
subu	$2,$2,$22
addiu	$2,$2,32
subu	$2,$2,$17
b	$L205
sra	$2,$2,6

$L203:
lbu	$11,0($8)
lbu	$23,1($8)
lbu	$2,-1($8)
sll	$21,$11,3
lbu	$3,2($8)
sll	$22,$23,3
addu	$11,$21,$11
addu	$21,$22,$23
subu	$11,$11,$2
addu	$2,$11,$21
subu	$2,$2,$3
addiu	$2,$2,8
subu	$3,$2,$17
b	$L205
sra	$2,$3,4

$L169:
bne	$7,$0,$L234
sll	$3,$7,2

li	$2,1			# 0x1
li	$21,8			# 0x8
subu	$17,$2,$17
addu	$18,$5,$6
li	$15,2			# 0x2
li	$19,-256			# 0xffffffffffffff00
li	$16,3			# 0x3
li	$22,53			# 0x35
li	$20,1			# 0x1
$L189:
move	$11,$5
subu	$14,$5,$6
move	$13,$18
move	$5,$18
addu	$18,$18,$6
move	$10,$4
move	$12,$18
b	$L197
addiu	$23,$4,8

$L237:
beq	$8,$16,$L192
move	$2,$0

beq	$8,$20,$L235
nop

$L196:
addu	$2,$9,$2
addiu	$10,$10,1
addiu	$2,$2,1
addiu	$11,$11,1
sra	$2,$2,1
addiu	$14,$14,1
addiu	$13,$13,1
addiu	$12,$12,1
beq	$23,$10,$L236
sb	$2,-1($10)

$L197:
bne	$8,$15,$L237
lbu	$9,0($10)

lbu	$3,0($11)
lbu	$2,0($13)
lbu	$fp,0($14)
sll	$7,$3,3
lbu	$24,0($12)
sll	$25,$2,3
addu	$3,$7,$3
addu	$7,$25,$2
subu	$3,$3,$fp
addu	$2,$3,$7
subu	$2,$2,$24
addiu	$2,$2,8
subu	$2,$2,$17
sra	$2,$2,4
$L194:
subu	$3,$0,$2
and	$7,$2,$19
beq	$7,$0,$L196
sra	$3,$3,31

andi	$2,$3,0xff
addu	$2,$9,$2
addiu	$10,$10,1
addiu	$2,$2,1
addiu	$11,$11,1
sra	$2,$2,1
addiu	$14,$14,1
addiu	$13,$13,1
addiu	$12,$12,1
bne	$23,$10,$L197
sb	$2,-1($10)

$L236:
addiu	$21,$21,-1
bne	$21,$0,$L189
addu	$4,$4,$6

lw	$fp,236($sp)
lw	$23,232($sp)
lw	$22,228($sp)
lw	$21,224($sp)
lw	$20,220($sp)
lw	$19,216($sp)
lw	$18,212($sp)
lw	$17,208($sp)
lw	$16,204($sp)
j	$31
addiu	$sp,$sp,240

$L234:
lw	$9,%got(shift_value.5660)($28)
sll	$2,$8,2
sw	$17,196($sp)
sll	$10,$6,1
sw	$4,188($sp)
addiu	$9,$9,%lo(shift_value.5660)
sw	$7,192($sp)
addiu	$14,$10,-1
addu	$2,$2,$9
sw	$10,184($sp)
addu	$3,$3,$9
addu	$14,$5,$14
lw	$25,0($2)
li	$2,1			# 0x1
lw	$3,0($3)
addiu	$5,$sp,184
addiu	$21,$sp,8
addu	$13,$10,$6
addu	$25,$3,$25
subu	$15,$6,$10
sra	$25,$25,1
li	$16,2			# 0x2
addiu	$24,$25,-1
li	$18,3			# 0x3
sll	$24,$2,$24
li	$20,53			# 0x35
addu	$24,$24,$17
li	$19,1			# 0x1
addiu	$24,$24,-1
move	$17,$5
$L172:
lw	$9,184($sp)
subu	$11,$14,$13
addiu	$12,$14,11
move	$7,$21
subu	$10,$14,$9
addu	$9,$15,$14
move	$5,$14
$L177:
beq	$8,$16,$L174
nop

beq	$8,$18,$L175
move	$2,$0

bne	$8,$19,$L241
addu	$2,$24,$2

lbu	$4,0($10)
lbu	$22,0($9)
lbu	$3,0($11)
mul	$4,$4,$20
lbu	$fp,0($5)
sll	$2,$22,1
sll	$3,$3,2
sll	$22,$22,4
sll	$23,$fp,1
addu	$22,$2,$22
addu	$23,$23,$fp
subu	$3,$4,$3
addu	$2,$3,$22
subu	$2,$2,$23
$L173:
addu	$2,$24,$2
$L241:
addiu	$7,$7,2
sra	$2,$2,$25
addiu	$5,$5,1
addiu	$11,$11,1
addiu	$10,$10,1
sh	$2,-2($7)
bne	$12,$5,$L177
addiu	$9,$9,1

addiu	$21,$21,22
bne	$21,$17,$L172
addu	$14,$14,$6

lw	$17,196($sp)
li	$2,64			# 0x40
lw	$4,188($sp)
addiu	$11,$sp,24
lw	$7,192($sp)
addiu	$18,$sp,200
subu	$17,$2,$17
li	$13,2			# 0x2
li	$14,3			# 0x3
li	$16,53			# 0x35
li	$15,1			# 0x1
li	$12,-256			# 0xffffffffffffff00
$L179:
move	$10,$4
b	$L186
addiu	$9,$11,-16

$L240:
subu	$2,$0,$2
addiu	$10,$10,1
sra	$2,$2,31
addiu	$9,$9,2
andi	$2,$2,0x00ff
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
beq	$11,$9,$L238
sb	$2,-1($10)

$L186:
beq	$7,$13,$L181
lbu	$8,0($10)

beq	$7,$14,$L182
nop

beq	$7,$15,$L239
nop

move	$2,$0
$L180:
addu	$2,$17,$2
sra	$2,$2,7
and	$3,$2,$12
bne	$3,$0,$L240
nop

andi	$2,$2,0x00ff
addu	$2,$8,$2
addiu	$10,$10,1
addiu	$2,$2,1
addiu	$9,$9,2
sra	$2,$2,1
bne	$11,$9,$L186
sb	$2,-1($10)

$L238:
addiu	$11,$11,22
bne	$11,$18,$L179
addu	$4,$4,$6

lw	$fp,236($sp)
lw	$23,232($sp)
lw	$22,228($sp)
lw	$21,224($sp)
lw	$20,220($sp)
lw	$19,216($sp)
lw	$18,212($sp)
lw	$17,208($sp)
lw	$16,204($sp)
j	$31
addiu	$sp,$sp,240

$L175:
lbu	$fp,0($11)
lbu	$4,0($10)
lbu	$3,0($9)
sll	$2,$fp,1
lbu	$23,0($5)
sll	$22,$4,1
sll	$4,$4,4
addu	$2,$2,$fp
addu	$4,$22,$4
sll	$22,$23,2
subu	$2,$4,$2
mul	$4,$3,$20
addu	$2,$4,$2
b	$L173
subu	$2,$2,$22

$L174:
lbu	$3,0($10)
lbu	$2,0($9)
lbu	$fp,0($11)
sll	$4,$3,3
lbu	$22,0($5)
sll	$23,$2,3
addu	$3,$4,$3
addu	$4,$23,$2
subu	$3,$3,$fp
addu	$2,$3,$4
b	$L173
subu	$2,$2,$22

$L239:
lh	$3,2($9)
lh	$19,4($9)
lh	$22,0($9)
mul	$5,$3,$16
lh	$2,6($9)
sll	$21,$19,1
sll	$3,$22,2
sll	$19,$19,4
sll	$20,$2,1
addu	$19,$21,$19
addu	$20,$20,$2
subu	$3,$5,$3
addu	$2,$3,$19
b	$L180
subu	$2,$2,$20

$L182:
lh	$21,0($9)
lh	$20,2($9)
lh	$5,4($9)
sll	$3,$21,1
lh	$2,6($9)
sll	$19,$20,1
sll	$20,$20,4
addu	$21,$3,$21
addu	$3,$19,$20
mul	$19,$5,$16
subu	$3,$3,$21
sll	$2,$2,2
addu	$3,$19,$3
b	$L180
subu	$2,$3,$2

$L181:
lh	$21,2($9)
lh	$20,4($9)
lh	$3,0($9)
sll	$5,$21,3
lh	$2,6($9)
sll	$19,$20,3
addu	$5,$5,$21
addu	$19,$19,$20
subu	$3,$5,$3
addu	$3,$3,$19
b	$L180
subu	$2,$3,$2

$L235:
lbu	$7,0($11)
lbu	$24,0($13)
lbu	$3,0($14)
mul	$7,$7,$22
lbu	$fp,0($12)
sll	$2,$24,1
sll	$3,$3,2
sll	$24,$24,4
sll	$25,$fp,1
addu	$24,$2,$24
addu	$25,$25,$fp
subu	$3,$7,$3
addu	$2,$3,$24
subu	$2,$2,$25
addiu	$2,$2,32
subu	$2,$2,$17
b	$L194
sra	$2,$2,6

$L192:
lbu	$fp,0($14)
lbu	$24,0($11)
lbu	$7,0($13)
sll	$3,$fp,1
lbu	$25,0($12)
sll	$2,$24,1
sll	$24,$24,4
addu	$3,$3,$fp
addu	$24,$2,$24
sll	$2,$25,2
subu	$3,$24,$3
mul	$24,$7,$22
addu	$3,$24,$3
subu	$3,$3,$2
addiu	$3,$3,32
subu	$3,$3,$17
b	$L194
sra	$2,$3,6

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc
.size	avg_vc1_mspel_mc, .-avg_vc1_mspel_mc
.section	.text.put_vc1_mspel_mc10_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc10_c
.type	put_vc1_mspel_mc10_c, @function
put_vc1_mspel_mc10_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc10_c
.size	put_vc1_mspel_mc10_c, .-put_vc1_mspel_mc10_c
.section	.text.avg_vc1_mspel_mc10_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc10_c
.type	avg_vc1_mspel_mc10_c, @function
avg_vc1_mspel_mc10_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc10_c
.size	avg_vc1_mspel_mc10_c, .-avg_vc1_mspel_mc10_c
.section	.text.put_vc1_mspel_mc20_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc20_c
.type	put_vc1_mspel_mc20_c, @function
put_vc1_mspel_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc20_c
.size	put_vc1_mspel_mc20_c, .-put_vc1_mspel_mc20_c
.section	.text.avg_vc1_mspel_mc20_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc20_c
.type	avg_vc1_mspel_mc20_c, @function
avg_vc1_mspel_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc20_c
.size	avg_vc1_mspel_mc20_c, .-avg_vc1_mspel_mc20_c
.section	.text.put_vc1_mspel_mc30_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc30_c
.type	put_vc1_mspel_mc30_c, @function
put_vc1_mspel_mc30_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc30_c
.size	put_vc1_mspel_mc30_c, .-put_vc1_mspel_mc30_c
.section	.text.avg_vc1_mspel_mc30_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc30_c
.type	avg_vc1_mspel_mc30_c, @function
avg_vc1_mspel_mc30_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc30_c
.size	avg_vc1_mspel_mc30_c, .-avg_vc1_mspel_mc30_c
.section	.text.put_vc1_mspel_mc01_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc01_c
.type	put_vc1_mspel_mc01_c, @function
put_vc1_mspel_mc01_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
move	$7,$0

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc01_c
.size	put_vc1_mspel_mc01_c, .-put_vc1_mspel_mc01_c
.section	.text.avg_vc1_mspel_mc01_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc01_c
.type	avg_vc1_mspel_mc01_c, @function
avg_vc1_mspel_mc01_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
move	$7,$0

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc01_c
.size	avg_vc1_mspel_mc01_c, .-avg_vc1_mspel_mc01_c
.section	.text.put_vc1_mspel_mc11_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc11_c
.type	put_vc1_mspel_mc11_c, @function
put_vc1_mspel_mc11_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc11_c
.size	put_vc1_mspel_mc11_c, .-put_vc1_mspel_mc11_c
.section	.text.avg_vc1_mspel_mc11_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc11_c
.type	avg_vc1_mspel_mc11_c, @function
avg_vc1_mspel_mc11_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc11_c
.size	avg_vc1_mspel_mc11_c, .-avg_vc1_mspel_mc11_c
.section	.text.put_vc1_mspel_mc21_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc21_c
.type	put_vc1_mspel_mc21_c, @function
put_vc1_mspel_mc21_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc21_c
.size	put_vc1_mspel_mc21_c, .-put_vc1_mspel_mc21_c
.section	.text.avg_vc1_mspel_mc21_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc21_c
.type	avg_vc1_mspel_mc21_c, @function
avg_vc1_mspel_mc21_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc21_c
.size	avg_vc1_mspel_mc21_c, .-avg_vc1_mspel_mc21_c
.section	.text.put_vc1_mspel_mc31_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc31_c
.type	put_vc1_mspel_mc31_c, @function
put_vc1_mspel_mc31_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc31_c
.size	put_vc1_mspel_mc31_c, .-put_vc1_mspel_mc31_c
.section	.text.avg_vc1_mspel_mc31_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc31_c
.type	avg_vc1_mspel_mc31_c, @function
avg_vc1_mspel_mc31_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,1			# 0x1
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc31_c
.size	avg_vc1_mspel_mc31_c, .-avg_vc1_mspel_mc31_c
.section	.text.put_vc1_mspel_mc02_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc02_c
.type	put_vc1_mspel_mc02_c, @function
put_vc1_mspel_mc02_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
move	$7,$0

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc02_c
.size	put_vc1_mspel_mc02_c, .-put_vc1_mspel_mc02_c
.section	.text.avg_vc1_mspel_mc02_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc02_c
.type	avg_vc1_mspel_mc02_c, @function
avg_vc1_mspel_mc02_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
move	$7,$0

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc02_c
.size	avg_vc1_mspel_mc02_c, .-avg_vc1_mspel_mc02_c
.section	.text.put_vc1_mspel_mc12_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc12_c
.type	put_vc1_mspel_mc12_c, @function
put_vc1_mspel_mc12_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc12_c
.size	put_vc1_mspel_mc12_c, .-put_vc1_mspel_mc12_c
.section	.text.avg_vc1_mspel_mc12_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc12_c
.type	avg_vc1_mspel_mc12_c, @function
avg_vc1_mspel_mc12_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc12_c
.size	avg_vc1_mspel_mc12_c, .-avg_vc1_mspel_mc12_c
.section	.text.put_vc1_mspel_mc22_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc22_c
.type	put_vc1_mspel_mc22_c, @function
put_vc1_mspel_mc22_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc22_c
.size	put_vc1_mspel_mc22_c, .-put_vc1_mspel_mc22_c
.section	.text.avg_vc1_mspel_mc22_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc22_c
.type	avg_vc1_mspel_mc22_c, @function
avg_vc1_mspel_mc22_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc22_c
.size	avg_vc1_mspel_mc22_c, .-avg_vc1_mspel_mc22_c
.section	.text.put_vc1_mspel_mc32_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc32_c
.type	put_vc1_mspel_mc32_c, @function
put_vc1_mspel_mc32_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc32_c
.size	put_vc1_mspel_mc32_c, .-put_vc1_mspel_mc32_c
.section	.text.avg_vc1_mspel_mc32_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc32_c
.type	avg_vc1_mspel_mc32_c, @function
avg_vc1_mspel_mc32_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,2			# 0x2
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc32_c
.size	avg_vc1_mspel_mc32_c, .-avg_vc1_mspel_mc32_c
.section	.text.put_vc1_mspel_mc03_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc03_c
.type	put_vc1_mspel_mc03_c, @function
put_vc1_mspel_mc03_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
move	$7,$0

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc03_c
.size	put_vc1_mspel_mc03_c, .-put_vc1_mspel_mc03_c
.section	.text.avg_vc1_mspel_mc03_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc03_c
.type	avg_vc1_mspel_mc03_c, @function
avg_vc1_mspel_mc03_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
move	$7,$0

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc03_c
.size	avg_vc1_mspel_mc03_c, .-avg_vc1_mspel_mc03_c
.section	.text.put_vc1_mspel_mc13_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc13_c
.type	put_vc1_mspel_mc13_c, @function
put_vc1_mspel_mc13_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc13_c
.size	put_vc1_mspel_mc13_c, .-put_vc1_mspel_mc13_c
.section	.text.avg_vc1_mspel_mc13_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc13_c
.type	avg_vc1_mspel_mc13_c, @function
avg_vc1_mspel_mc13_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,1			# 0x1

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc13_c
.size	avg_vc1_mspel_mc13_c, .-avg_vc1_mspel_mc13_c
.section	.text.put_vc1_mspel_mc23_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc23_c
.type	put_vc1_mspel_mc23_c, @function
put_vc1_mspel_mc23_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc23_c
.size	put_vc1_mspel_mc23_c, .-put_vc1_mspel_mc23_c
.section	.text.avg_vc1_mspel_mc23_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc23_c
.type	avg_vc1_mspel_mc23_c, @function
avg_vc1_mspel_mc23_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,2			# 0x2

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc23_c
.size	avg_vc1_mspel_mc23_c, .-avg_vc1_mspel_mc23_c
.section	.text.put_vc1_mspel_mc33_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vc1_mspel_mc33_c
.type	put_vc1_mspel_mc33_c, @function
put_vc1_mspel_mc33_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(put_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(put_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vc1_mspel_mc33_c
.size	put_vc1_mspel_mc33_c, .-put_vc1_mspel_mc33_c
.section	.text.avg_vc1_mspel_mc33_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_vc1_mspel_mc33_c
.type	avg_vc1_mspel_mc33_c, @function
avg_vc1_mspel_mc33_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(avg_vc1_mspel_mc)($28)
addiu	$sp,$sp,-40
li	$2,3			# 0x3
sw	$7,20($sp)
addiu	$25,$25,%lo(avg_vc1_mspel_mc)
sw	$31,36($sp)
.cprestore	24
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,avg_vc1_mspel_mc
1:	jalr	$25
li	$7,3			# 0x3

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_vc1_mspel_mc33_c
.size	avg_vc1_mspel_mc33_c, .-avg_vc1_mspel_mc33_c
.section	.text.vc1_h_loop_filter16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_h_loop_filter16_c
.type	vc1_h_loop_filter16_c, @function
vc1_h_loop_filter16_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$7,$5,1
lw	$13,%got(ff_cropTbl)($28)
addiu	$sp,$sp,-8
addu	$7,$4,$7
sll	$11,$5,2
sw	$17,4($sp)
addu	$10,$7,$5
sw	$16,0($sp)
addu	$5,$4,$5
li	$12,4			# 0x4
addiu	$13,$13,1024
$L321:
lbu	$14,0($7)
lbu	$15,-1($7)
lbu	$17,-2($7)
lbu	$8,1($7)
subu	$9,$14,$15
subu	$3,$17,$8
sll	$2,$9,2
sll	$3,$3,1
addu	$2,$2,$9
addu	$2,$3,$2
addiu	$2,$2,4
sra	$25,$2,31
sra	$2,$2,3
xor	$2,$25,$2
subu	$2,$2,$25
slt	$3,$2,$6
beq	$3,$0,$L304
nop

lbu	$3,-3($7)
lbu	$24,-4($7)
lbu	$16,2($7)
subu	$3,$17,$3
lbu	$9,3($7)
subu	$24,$24,$15
sll	$17,$3,2
sll	$24,$24,1
addu	$3,$17,$3
subu	$8,$16,$8
addu	$3,$24,$3
sll	$16,$8,2
subu	$9,$14,$9
addiu	$3,$3,4
sll	$9,$9,1
addu	$8,$16,$8
sra	$3,$3,3
addu	$8,$9,$8
subu	$9,$0,$3
slt	$16,$3,0
addiu	$8,$8,4
movn	$3,$9,$16
sra	$8,$8,3
subu	$16,$0,$8
slt	$17,$8,0
slt	$9,$3,$2
bne	$9,$0,$L307
movn	$8,$16,$17

slt	$9,$8,$2
beq	$9,$0,$L304
nop

$L307:
subu	$9,$15,$14
sra	$16,$9,31
xor	$9,$16,$9
subu	$9,$9,$16
sra	$9,$9,1
beq	$9,$0,$L304
slt	$17,$3,$8

movz	$3,$8,$17
subu	$2,$3,$2
sll	$8,$2,2
addu	$2,$8,$2
sra	$3,$2,31
xor	$25,$3,$25
beq	$16,$25,$L339
xor	$2,$3,$2

$L309:
lbu	$17,0($4)
lbu	$15,-1($4)
lbu	$25,-2($4)
lbu	$24,1($4)
subu	$14,$17,$15
subu	$8,$25,$24
sll	$3,$14,2
sll	$8,$8,1
addu	$14,$3,$14
addu	$14,$8,$14
addiu	$14,$14,4
sra	$16,$14,31
sra	$14,$14,3
xor	$14,$16,$14
subu	$14,$14,$16
slt	$2,$14,$6
beq	$2,$0,$L313
nop

lbu	$2,-3($4)
lbu	$9,-4($4)
lbu	$3,2($4)
subu	$2,$25,$2
lbu	$8,3($4)
subu	$9,$9,$15
sll	$25,$2,2
sll	$9,$9,1
addu	$2,$25,$2
subu	$3,$3,$24
addu	$2,$9,$2
subu	$8,$17,$8
sll	$9,$3,2
addiu	$2,$2,4
sll	$8,$8,1
addu	$3,$9,$3
sra	$2,$2,3
addu	$3,$8,$3
subu	$8,$0,$2
slt	$9,$2,0
addiu	$3,$3,4
movn	$2,$8,$9
sra	$3,$3,3
subu	$9,$0,$3
slt	$24,$3,0
slt	$8,$2,$14
bne	$8,$0,$L312
movn	$3,$9,$24

slt	$8,$3,$14
beq	$8,$0,$L313
nop

$L312:
subu	$8,$15,$17
sra	$9,$8,31
xor	$8,$9,$8
subu	$8,$8,$9
sra	$8,$8,1
beq	$8,$0,$L313
slt	$24,$2,$3

movz	$2,$3,$24
subu	$14,$2,$14
sll	$2,$14,2
addu	$14,$2,$14
sra	$2,$14,31
xor	$16,$2,$16
beq	$9,$16,$L340
xor	$14,$2,$14

$L313:
lbu	$14,0($5)
lbu	$15,-1($5)
lbu	$9,-2($5)
lbu	$8,1($5)
subu	$16,$14,$15
subu	$3,$9,$8
sll	$2,$16,2
sll	$3,$3,1
addu	$2,$2,$16
addu	$2,$3,$2
addiu	$2,$2,4
sra	$24,$2,31
sra	$2,$2,3
xor	$2,$24,$2
subu	$2,$2,$24
slt	$3,$2,$6
beq	$3,$0,$L314
nop

lbu	$3,-3($5)
lbu	$16,-4($5)
lbu	$17,2($5)
subu	$3,$9,$3
lbu	$9,3($5)
subu	$16,$16,$15
sll	$25,$3,2
sll	$16,$16,1
addu	$3,$25,$3
subu	$8,$17,$8
addu	$3,$16,$3
subu	$9,$14,$9
sll	$16,$8,2
addiu	$3,$3,4
sll	$9,$9,1
addu	$8,$16,$8
sra	$3,$3,3
addu	$8,$9,$8
subu	$9,$0,$3
slt	$16,$3,0
addiu	$8,$8,4
movn	$3,$9,$16
sra	$8,$8,3
subu	$16,$0,$8
slt	$17,$8,0
slt	$9,$3,$2
bne	$9,$0,$L317
movn	$8,$16,$17

slt	$9,$8,$2
beq	$9,$0,$L314
nop

$L317:
subu	$9,$15,$14
sra	$16,$9,31
xor	$9,$16,$9
subu	$9,$9,$16
sra	$9,$9,1
beq	$9,$0,$L314
slt	$17,$3,$8

movz	$3,$8,$17
subu	$2,$3,$2
sll	$8,$2,2
addu	$2,$8,$2
sra	$3,$2,31
xor	$24,$3,$24
beq	$16,$24,$L341
xor	$2,$3,$2

$L314:
lbu	$14,0($10)
lbu	$15,-1($10)
lbu	$9,-2($10)
lbu	$8,1($10)
subu	$16,$14,$15
subu	$3,$9,$8
sll	$2,$16,2
sll	$3,$3,1
addu	$2,$2,$16
addu	$2,$3,$2
addiu	$2,$2,4
sra	$24,$2,31
sra	$2,$2,3
xor	$2,$24,$2
subu	$2,$2,$24
slt	$3,$2,$6
beq	$3,$0,$L304
nop

lbu	$3,-3($10)
lbu	$16,-4($10)
lbu	$17,2($10)
subu	$3,$9,$3
lbu	$9,3($10)
subu	$16,$16,$15
sll	$25,$3,2
sll	$16,$16,1
addu	$3,$25,$3
subu	$8,$17,$8
addu	$3,$16,$3
subu	$9,$14,$9
sll	$16,$8,2
addiu	$3,$3,4
sll	$9,$9,1
addu	$8,$16,$8
sra	$3,$3,3
addu	$8,$9,$8
subu	$9,$0,$3
slt	$16,$3,0
addiu	$8,$8,4
movn	$3,$9,$16
sra	$8,$8,3
subu	$16,$0,$8
slt	$17,$8,0
slt	$9,$3,$2
bne	$9,$0,$L320
movn	$8,$16,$17

slt	$9,$8,$2
beq	$9,$0,$L304
nop

$L320:
subu	$9,$15,$14
sra	$16,$9,31
xor	$9,$16,$9
subu	$9,$9,$16
sra	$9,$9,1
beq	$9,$0,$L304
slt	$17,$3,$8

movz	$3,$8,$17
subu	$2,$3,$2
sll	$8,$2,2
addu	$2,$8,$2
sra	$3,$2,31
xor	$24,$3,$24
beq	$16,$24,$L342
nop

$L304:
addiu	$12,$12,-1
addu	$4,$4,$11
addu	$7,$7,$11
addu	$10,$10,$11
bne	$12,$0,$L321
addu	$5,$5,$11

lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L339:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$9,$2
movn	$2,$9,$3
xor	$2,$2,$16
subu	$16,$2,$16
subu	$15,$15,$16
addu	$14,$14,$16
addu	$15,$13,$15
addu	$14,$13,$14
lbu	$2,0($15)
sb	$2,-1($7)
lbu	$2,0($14)
b	$L309
sb	$2,0($7)

$L340:
subu	$2,$14,$2
sra	$2,$2,3
slt	$3,$8,$2
movn	$2,$8,$3
xor	$2,$2,$9
subu	$9,$2,$9
subu	$15,$15,$9
addu	$9,$17,$9
addu	$15,$13,$15
addu	$9,$13,$9
lbu	$2,0($15)
sb	$2,-1($4)
lbu	$2,0($9)
b	$L313
sb	$2,0($4)

$L342:
xor	$2,$3,$2
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$9,$2
movn	$2,$9,$3
xor	$2,$2,$16
subu	$16,$2,$16
subu	$15,$15,$16
addu	$14,$14,$16
addu	$15,$13,$15
addu	$14,$13,$14
lbu	$2,0($15)
sb	$2,-1($10)
lbu	$2,0($14)
b	$L304
sb	$2,0($10)

$L341:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$9,$2
movn	$2,$9,$3
xor	$2,$2,$16
subu	$16,$2,$16
subu	$15,$15,$16
addu	$14,$14,$16
addu	$15,$13,$15
addu	$14,$13,$14
lbu	$2,0($15)
sb	$2,-1($5)
lbu	$2,0($14)
b	$L314
sb	$2,0($5)

.set	macro
.set	reorder
.end	vc1_h_loop_filter16_c
.size	vc1_h_loop_filter16_c, .-vc1_h_loop_filter16_c
.section	.text.vc1_h_loop_filter4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_h_loop_filter4_c
.type	vc1_h_loop_filter4_c, @function
vc1_h_loop_filter4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$9,$5,1
addu	$10,$4,$9
lbu	$14,0($10)
lbu	$13,-1($10)
lbu	$7,-2($10)
lbu	$11,1($10)
subu	$12,$14,$13
subu	$2,$7,$11
sll	$3,$12,2
sll	$2,$2,1
addu	$12,$3,$12
addu	$12,$2,$12
addiu	$12,$12,4
sra	$15,$12,31
sra	$12,$12,3
xor	$12,$15,$12
subu	$12,$12,$15
slt	$2,$12,$6
beq	$2,$0,$L379
nop

lbu	$2,-3($10)
lbu	$8,-4($10)
lbu	$3,2($10)
subu	$2,$7,$2
lbu	$7,3($10)
subu	$8,$8,$13
sll	$24,$2,2
sll	$8,$8,1
addu	$2,$24,$2
subu	$11,$3,$11
addu	$2,$8,$2
sll	$3,$11,2
subu	$7,$14,$7
addiu	$2,$2,4
sll	$7,$7,1
addu	$3,$3,$11
sra	$2,$2,3
addu	$3,$7,$3
subu	$7,$0,$2
slt	$8,$2,0
addiu	$3,$3,4
movn	$2,$7,$8
sra	$3,$3,3
subu	$8,$0,$3
slt	$11,$3,0
slt	$7,$2,$12
bne	$7,$0,$L348
movn	$3,$8,$11

slt	$7,$3,$12
beq	$7,$0,$L379
nop

$L348:
subu	$7,$13,$14
sra	$8,$7,31
xor	$7,$8,$7
subu	$7,$7,$8
sra	$7,$7,1
beq	$7,$0,$L379
slt	$11,$2,$3

movz	$2,$3,$11
subu	$12,$2,$12
sll	$2,$12,2
addu	$12,$2,$12
sra	$2,$12,31
xor	$15,$2,$15
beq	$8,$15,$L375
xor	$12,$2,$12

$L350:
lbu	$12,0($4)
lbu	$11,-1($4)
lbu	$2,-2($4)
lbu	$15,1($4)
subu	$10,$12,$11
subu	$7,$2,$15
sll	$3,$10,2
sll	$7,$7,1
addu	$10,$3,$10
addu	$10,$7,$10
addiu	$10,$10,4
sra	$13,$10,31
sra	$10,$10,3
xor	$10,$13,$10
subu	$10,$10,$13
slt	$3,$10,$6
beq	$3,$0,$L354
nop

lbu	$7,-3($4)
lbu	$8,-4($4)
lbu	$3,2($4)
subu	$2,$2,$7
lbu	$7,3($4)
subu	$8,$8,$11
sll	$14,$2,2
sll	$8,$8,1
addu	$2,$14,$2
subu	$3,$3,$15
addu	$2,$8,$2
subu	$7,$12,$7
sll	$8,$3,2
addiu	$2,$2,4
sll	$7,$7,1
addu	$3,$8,$3
sra	$2,$2,3
addu	$3,$7,$3
subu	$7,$0,$2
slt	$8,$2,0
addiu	$3,$3,4
movn	$2,$7,$8
sra	$3,$3,3
subu	$8,$0,$3
slt	$14,$3,0
slt	$7,$2,$10
bne	$7,$0,$L353
movn	$3,$8,$14

slt	$7,$3,$10
beq	$7,$0,$L354
nop

$L353:
subu	$7,$11,$12
sra	$8,$7,31
xor	$7,$8,$7
subu	$7,$7,$8
sra	$7,$7,1
beq	$7,$0,$L354
slt	$14,$2,$3

movz	$2,$3,$14
subu	$10,$2,$10
sll	$2,$10,2
addu	$10,$2,$10
sra	$2,$10,31
xor	$13,$2,$13
beq	$8,$13,$L376
xor	$10,$2,$10

$L354:
addu	$5,$4,$5
lbu	$10,0($5)
lbu	$8,-1($5)
lbu	$7,-2($5)
lbu	$12,1($5)
subu	$11,$10,$8
subu	$3,$7,$12
sll	$2,$11,2
sll	$3,$3,1
addu	$2,$2,$11
addu	$2,$3,$2
addiu	$2,$2,4
sra	$11,$2,31
sra	$2,$2,3
xor	$2,$11,$2
subu	$2,$2,$11
slt	$3,$2,$6
beq	$3,$0,$L355
nop

lbu	$3,-3($5)
lbu	$4,-4($5)
lbu	$13,2($5)
subu	$3,$7,$3
lbu	$7,3($5)
subu	$4,$4,$8
sll	$14,$3,2
sll	$4,$4,1
addu	$3,$14,$3
subu	$12,$13,$12
addu	$3,$4,$3
subu	$7,$10,$7
sll	$4,$12,2
addiu	$3,$3,4
sll	$7,$7,1
addu	$4,$4,$12
sra	$3,$3,3
addu	$4,$7,$4
subu	$7,$0,$3
slt	$12,$3,0
addiu	$4,$4,4
movn	$3,$7,$12
sra	$4,$4,3
subu	$12,$0,$4
slt	$13,$4,0
slt	$7,$3,$2
bne	$7,$0,$L358
movn	$4,$12,$13

slt	$7,$4,$2
beq	$7,$0,$L355
nop

$L358:
subu	$7,$8,$10
sra	$12,$7,31
xor	$7,$12,$7
subu	$7,$7,$12
sra	$7,$7,1
beq	$7,$0,$L355
slt	$13,$3,$4

movz	$3,$4,$13
subu	$2,$3,$2
sll	$4,$2,2
addu	$2,$4,$2
sra	$3,$2,31
xor	$11,$3,$11
beq	$12,$11,$L377
xor	$2,$3,$2

$L355:
addu	$5,$5,$9
lbu	$8,0($5)
lbu	$4,-1($5)
lbu	$7,-2($5)
lbu	$10,1($5)
subu	$9,$8,$4
subu	$3,$7,$10
sll	$2,$9,2
sll	$3,$3,1
addu	$2,$2,$9
addu	$2,$3,$2
addiu	$2,$2,4
sra	$9,$2,31
sra	$2,$2,3
xor	$2,$9,$2
subu	$2,$2,$9
slt	$6,$2,$6
beq	$6,$0,$L379
nop

lbu	$3,-3($5)
lbu	$6,-4($5)
lbu	$11,2($5)
subu	$3,$7,$3
lbu	$7,3($5)
subu	$6,$6,$4
sll	$12,$3,2
sll	$6,$6,1
addu	$3,$12,$3
subu	$10,$11,$10
addu	$3,$6,$3
subu	$7,$8,$7
sll	$6,$10,2
addiu	$3,$3,4
sll	$7,$7,1
addu	$6,$6,$10
sra	$3,$3,3
addu	$6,$7,$6
subu	$7,$0,$3
slt	$10,$3,0
addiu	$6,$6,4
movn	$3,$7,$10
sra	$6,$6,3
subu	$10,$0,$6
slt	$11,$6,0
slt	$7,$3,$2
bne	$7,$0,$L361
movn	$6,$10,$11

slt	$7,$6,$2
beq	$7,$0,$L379
nop

$L361:
subu	$7,$4,$8
sra	$10,$7,31
xor	$7,$10,$7
subu	$7,$7,$10
sra	$7,$7,1
beq	$7,$0,$L379
slt	$11,$3,$6

movz	$3,$6,$11
subu	$2,$3,$2
sll	$6,$2,2
addu	$2,$6,$2
sra	$3,$2,31
xor	$9,$3,$9
beq	$10,$9,$L378
xor	$2,$3,$2

$L379:
j	$31
nop

$L375:
lw	$11,%got(ff_cropTbl)($28)
subu	$2,$12,$2
sra	$2,$2,3
addiu	$11,$11,1024
slt	$3,$7,$2
movz	$7,$2,$3
xor	$7,$7,$8
subu	$8,$7,$8
subu	$13,$13,$8
addu	$14,$14,$8
addu	$13,$11,$13
addu	$14,$11,$14
lbu	$2,0($13)
sb	$2,-1($10)
lbu	$2,0($14)
b	$L350
sb	$2,0($10)

$L378:
lw	$6,%got(ff_cropTbl)($28)
subu	$2,$2,$3
sra	$2,$2,3
addiu	$6,$6,1024
slt	$3,$7,$2
movn	$2,$7,$3
xor	$2,$2,$10
subu	$10,$2,$10
subu	$4,$4,$10
addu	$8,$8,$10
addu	$4,$6,$4
addu	$8,$6,$8
lbu	$2,0($4)
sb	$2,-1($5)
lbu	$2,0($8)
j	$31
sb	$2,0($5)

$L377:
lw	$4,%got(ff_cropTbl)($28)
subu	$2,$2,$3
sra	$2,$2,3
addiu	$4,$4,1024
slt	$3,$7,$2
movn	$2,$7,$3
xor	$2,$2,$12
subu	$12,$2,$12
subu	$8,$8,$12
addu	$10,$10,$12
addu	$8,$4,$8
addu	$10,$4,$10
lbu	$2,0($8)
sb	$2,-1($5)
lbu	$2,0($10)
b	$L355
sb	$2,0($5)

$L376:
subu	$2,$10,$2
lw	$10,%got(ff_cropTbl)($28)
sra	$2,$2,3
addiu	$10,$10,1024
slt	$3,$7,$2
movn	$2,$7,$3
xor	$2,$2,$8
subu	$8,$2,$8
subu	$11,$11,$8
addu	$8,$12,$8
addu	$11,$10,$11
addu	$8,$10,$8
lbu	$2,0($11)
sb	$2,-1($4)
lbu	$2,0($8)
b	$L354
sb	$2,0($4)

.set	macro
.set	reorder
.end	vc1_h_loop_filter4_c
.size	vc1_h_loop_filter4_c, .-vc1_h_loop_filter4_c
.section	.text.vc1_h_loop_filter8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_h_loop_filter8_c
.type	vc1_h_loop_filter8_c, @function
vc1_h_loop_filter8_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$7,$5,1
lw	$14,%got(ff_cropTbl)($28)
addiu	$sp,$sp,-16
addu	$7,$4,$7
sll	$11,$5,2
sw	$18,12($sp)
addu	$10,$7,$5
sw	$17,8($sp)
sw	$16,4($sp)
addu	$5,$4,$5
li	$12,2			# 0x2
addiu	$14,$14,1024
b	$L399
li	$13,1			# 0x1

$L401:
li	$12,1			# 0x1
$L399:
lbu	$15,0($7)
lbu	$24,-1($7)
lbu	$9,-2($7)
lbu	$8,1($7)
subu	$16,$15,$24
subu	$3,$9,$8
sll	$2,$16,2
sll	$3,$3,1
addu	$2,$2,$16
addu	$2,$3,$2
addiu	$2,$2,4
sra	$25,$2,31
sra	$2,$2,3
xor	$2,$25,$2
subu	$2,$2,$25
slt	$3,$2,$6
beq	$3,$0,$L382
nop

lbu	$3,-3($7)
lbu	$16,-4($7)
lbu	$17,2($7)
subu	$3,$9,$3
lbu	$9,3($7)
subu	$16,$16,$24
sll	$18,$3,2
sll	$16,$16,1
addu	$3,$18,$3
subu	$8,$17,$8
addu	$3,$16,$3
subu	$9,$15,$9
sll	$16,$8,2
addiu	$3,$3,4
sll	$9,$9,1
addu	$8,$16,$8
sra	$3,$3,3
addu	$8,$9,$8
subu	$9,$0,$3
slt	$16,$3,0
addiu	$8,$8,4
movn	$3,$9,$16
sra	$8,$8,3
subu	$16,$0,$8
slt	$17,$8,0
slt	$9,$3,$2
bne	$9,$0,$L385
movn	$8,$16,$17

slt	$9,$8,$2
beq	$9,$0,$L382
nop

$L385:
subu	$9,$24,$15
sra	$16,$9,31
xor	$9,$16,$9
subu	$9,$9,$16
sra	$9,$9,1
beq	$9,$0,$L382
slt	$17,$3,$8

movz	$3,$8,$17
subu	$2,$3,$2
sll	$8,$2,2
addu	$2,$8,$2
sra	$3,$2,31
xor	$25,$3,$25
beq	$16,$25,$L417
xor	$2,$3,$2

$L387:
lbu	$24,0($4)
lbu	$16,-1($4)
lbu	$25,-2($4)
lbu	$18,1($4)
subu	$15,$24,$16
subu	$8,$25,$18
sll	$2,$15,2
sll	$8,$8,1
addu	$15,$2,$15
addu	$15,$8,$15
addiu	$15,$15,4
sra	$17,$15,31
sra	$15,$15,3
xor	$15,$17,$15
subu	$15,$15,$17
slt	$2,$15,$6
beq	$2,$0,$L391
nop

lbu	$2,-3($4)
lbu	$9,-4($4)
lbu	$3,2($4)
subu	$2,$25,$2
lbu	$8,3($4)
subu	$9,$9,$16
sll	$25,$2,2
sll	$9,$9,1
addu	$2,$25,$2
subu	$3,$3,$18
addu	$2,$9,$2
subu	$8,$24,$8
sll	$9,$3,2
addiu	$2,$2,4
sll	$8,$8,1
addu	$3,$9,$3
sra	$2,$2,3
addu	$3,$8,$3
subu	$8,$0,$2
slt	$9,$2,0
addiu	$3,$3,4
movn	$2,$8,$9
sra	$3,$3,3
subu	$9,$0,$3
slt	$18,$3,0
slt	$8,$2,$15
bne	$8,$0,$L390
movn	$3,$9,$18

slt	$8,$3,$15
beq	$8,$0,$L391
nop

$L390:
subu	$8,$16,$24
sra	$9,$8,31
xor	$8,$9,$8
subu	$8,$8,$9
sra	$8,$8,1
beq	$8,$0,$L391
slt	$18,$2,$3

movz	$2,$3,$18
subu	$15,$2,$15
sll	$2,$15,2
addu	$15,$2,$15
sra	$2,$15,31
xor	$17,$2,$17
beq	$9,$17,$L418
xor	$15,$2,$15

$L391:
lbu	$15,0($5)
lbu	$16,-1($5)
lbu	$9,-2($5)
lbu	$8,1($5)
subu	$17,$15,$16
subu	$3,$9,$8
sll	$2,$17,2
sll	$3,$3,1
addu	$2,$2,$17
addu	$2,$3,$2
addiu	$2,$2,4
sra	$17,$2,31
sra	$2,$2,3
xor	$2,$17,$2
subu	$2,$2,$17
slt	$3,$2,$6
beq	$3,$0,$L392
nop

lbu	$3,-3($5)
lbu	$24,-4($5)
lbu	$18,2($5)
subu	$3,$9,$3
lbu	$9,3($5)
subu	$24,$24,$16
sll	$25,$3,2
sll	$24,$24,1
addu	$3,$25,$3
subu	$8,$18,$8
addu	$3,$24,$3
sll	$18,$8,2
subu	$9,$15,$9
addiu	$3,$3,4
sll	$9,$9,1
addu	$8,$18,$8
sra	$3,$3,3
addu	$8,$9,$8
subu	$9,$0,$3
slt	$18,$3,0
addiu	$8,$8,4
movn	$3,$9,$18
sra	$8,$8,3
subu	$18,$0,$8
slt	$24,$8,0
slt	$9,$3,$2
bne	$9,$0,$L395
movn	$8,$18,$24

slt	$9,$8,$2
beq	$9,$0,$L392
nop

$L395:
subu	$9,$16,$15
sra	$18,$9,31
xor	$9,$18,$9
subu	$9,$9,$18
sra	$9,$9,1
beq	$9,$0,$L392
slt	$24,$3,$8

movz	$3,$8,$24
subu	$2,$3,$2
sll	$8,$2,2
addu	$2,$8,$2
sra	$3,$2,31
xor	$17,$3,$17
beq	$18,$17,$L419
xor	$2,$3,$2

$L392:
lbu	$15,0($10)
lbu	$16,-1($10)
lbu	$9,-2($10)
lbu	$8,1($10)
subu	$17,$15,$16
subu	$3,$9,$8
sll	$2,$17,2
sll	$3,$3,1
addu	$2,$2,$17
addu	$2,$3,$2
addiu	$2,$2,4
sra	$17,$2,31
sra	$2,$2,3
xor	$2,$17,$2
subu	$2,$2,$17
slt	$3,$2,$6
beq	$3,$0,$L382
nop

lbu	$3,-3($10)
lbu	$24,-4($10)
lbu	$18,2($10)
subu	$3,$9,$3
lbu	$9,3($10)
subu	$24,$24,$16
sll	$25,$3,2
sll	$24,$24,1
addu	$3,$25,$3
subu	$8,$18,$8
addu	$3,$24,$3
sll	$18,$8,2
subu	$9,$15,$9
addiu	$3,$3,4
sll	$9,$9,1
addu	$8,$18,$8
sra	$3,$3,3
addu	$8,$9,$8
subu	$9,$0,$3
slt	$18,$3,0
addiu	$8,$8,4
movn	$3,$9,$18
sra	$8,$8,3
subu	$18,$0,$8
slt	$24,$8,0
slt	$9,$3,$2
bne	$9,$0,$L398
movn	$8,$18,$24

slt	$9,$8,$2
beq	$9,$0,$L382
nop

$L398:
subu	$9,$16,$15
sra	$18,$9,31
xor	$9,$18,$9
subu	$9,$9,$18
sra	$9,$9,1
beq	$9,$0,$L382
slt	$24,$3,$8

movz	$3,$8,$24
subu	$2,$3,$2
sll	$8,$2,2
addu	$2,$8,$2
sra	$3,$2,31
xor	$17,$3,$17
beq	$18,$17,$L420
nop

$L382:
addu	$4,$4,$11
addu	$7,$7,$11
addu	$10,$10,$11
bne	$12,$13,$L401
addu	$5,$5,$11

lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

$L417:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$9,$2
movz	$9,$2,$3
xor	$9,$9,$16
subu	$16,$9,$16
subu	$24,$24,$16
addu	$15,$15,$16
addu	$24,$14,$24
addu	$15,$14,$15
lbu	$2,0($24)
sb	$2,-1($7)
lbu	$2,0($15)
b	$L387
sb	$2,0($7)

$L418:
subu	$2,$15,$2
sra	$2,$2,3
slt	$3,$8,$2
movn	$2,$8,$3
xor	$2,$2,$9
subu	$9,$2,$9
subu	$16,$16,$9
addu	$9,$24,$9
addu	$16,$14,$16
addu	$9,$14,$9
lbu	$2,0($16)
sb	$2,-1($4)
lbu	$2,0($9)
b	$L391
sb	$2,0($4)

$L420:
xor	$2,$3,$2
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$9,$2
movn	$2,$9,$3
xor	$2,$2,$18
subu	$18,$2,$18
subu	$16,$16,$18
addu	$15,$15,$18
addu	$16,$14,$16
addu	$15,$14,$15
lbu	$2,0($16)
sb	$2,-1($10)
lbu	$2,0($15)
b	$L382
sb	$2,0($10)

$L419:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$9,$2
movn	$2,$9,$3
xor	$2,$2,$18
subu	$18,$2,$18
subu	$16,$16,$18
addu	$15,$15,$18
addu	$16,$14,$16
addu	$15,$14,$15
lbu	$2,0($16)
sb	$2,-1($5)
lbu	$2,0($15)
b	$L392
sb	$2,0($5)

.set	macro
.set	reorder
.end	vc1_h_loop_filter8_c
.size	vc1_h_loop_filter8_c, .-vc1_h_loop_filter8_c
.section	.text.vc1_v_loop_filter16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_v_loop_filter16_c
.type	vc1_v_loop_filter16_c, @function
vc1_v_loop_filter16_c:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$11,$5,1
lw	$15,%got(ff_cropTbl)($28)
subu	$8,$0,$5
sll	$12,$5,2
addu	$13,$11,$5
sll	$8,$8,1
addu	$9,$4,$5
subu	$12,$4,$12
addiu	$sp,$sp,-24
addu	$10,$4,$8
addu	$5,$12,$5
sw	$20,20($sp)
addu	$8,$9,$8
sw	$19,16($sp)
addu	$13,$4,$13
sw	$18,12($sp)
addu	$11,$4,$11
sw	$17,8($sp)
addiu	$14,$4,16
sw	$16,4($sp)
addiu	$15,$15,1024
$L440:
lbu	$24,2($4)
lbu	$25,2($8)
lbu	$17,2($10)
lbu	$19,2($9)
subu	$7,$24,$25
subu	$3,$17,$19
sll	$2,$7,2
sll	$3,$3,1
addu	$2,$2,$7
addu	$2,$3,$2
addiu	$2,$2,4
sra	$16,$2,31
sra	$2,$2,3
xor	$2,$16,$2
subu	$2,$2,$16
slt	$3,$2,$6
beq	$3,$0,$L423
nop

lbu	$3,2($5)
lbu	$18,2($12)
lbu	$7,2($11)
subu	$3,$17,$3
lbu	$17,2($13)
subu	$18,$18,$25
sll	$20,$3,2
sll	$18,$18,1
addu	$3,$20,$3
subu	$19,$7,$19
addu	$3,$18,$3
sll	$7,$19,2
subu	$17,$24,$17
addiu	$3,$3,4
sll	$17,$17,1
addu	$7,$7,$19
sra	$3,$3,3
addu	$7,$17,$7
subu	$17,$0,$3
slt	$18,$3,0
addiu	$7,$7,4
movn	$3,$17,$18
sra	$7,$7,3
subu	$18,$0,$7
slt	$19,$7,0
slt	$17,$3,$2
bne	$17,$0,$L426
movn	$7,$18,$19

slt	$17,$7,$2
beq	$17,$0,$L423
nop

$L426:
subu	$24,$25,$24
sra	$17,$24,31
xor	$24,$17,$24
subu	$24,$24,$17
sra	$24,$24,1
beq	$24,$0,$L423
slt	$18,$3,$7

movz	$3,$7,$18
subu	$2,$3,$2
sll	$7,$2,2
addu	$2,$7,$2
sra	$3,$2,31
xor	$16,$3,$16
beq	$17,$16,$L458
xor	$2,$3,$2

$L428:
lbu	$18,0($4)
lbu	$19,0($8)
lbu	$2,0($10)
lbu	$3,0($9)
subu	$17,$18,$19
subu	$7,$2,$3
sll	$16,$17,2
sll	$7,$7,1
addu	$17,$16,$17
addu	$17,$7,$17
addiu	$17,$17,4
sra	$24,$17,31
sra	$17,$17,3
xor	$17,$24,$17
subu	$17,$17,$24
slt	$7,$17,$6
beq	$7,$0,$L432
nop

lbu	$7,0($5)
lbu	$16,0($12)
lbu	$20,0($11)
subu	$2,$2,$7
lbu	$7,0($13)
subu	$16,$16,$19
sll	$25,$2,2
sll	$16,$16,1
addu	$2,$25,$2
subu	$3,$20,$3
addu	$2,$16,$2
subu	$7,$18,$7
sll	$16,$3,2
addiu	$2,$2,4
sll	$7,$7,1
addu	$3,$16,$3
sra	$2,$2,3
addu	$3,$7,$3
subu	$7,$0,$2
slt	$16,$2,0
addiu	$3,$3,4
movn	$2,$7,$16
sra	$3,$3,3
subu	$16,$0,$3
slt	$20,$3,0
slt	$7,$2,$17
bne	$7,$0,$L431
movn	$3,$16,$20

slt	$7,$3,$17
beq	$7,$0,$L432
nop

$L431:
subu	$18,$19,$18
sra	$7,$18,31
xor	$18,$7,$18
subu	$18,$18,$7
sra	$18,$18,1
beq	$18,$0,$L432
slt	$16,$2,$3

movz	$2,$3,$16
subu	$17,$2,$17
sll	$2,$17,2
addu	$17,$2,$17
sra	$2,$17,31
xor	$24,$2,$24
beq	$7,$24,$L459
xor	$17,$2,$17

$L432:
lbu	$16,1($4)
lbu	$17,1($8)
lbu	$19,1($10)
lbu	$25,1($9)
subu	$7,$16,$17
subu	$3,$19,$25
sll	$2,$7,2
sll	$3,$3,1
addu	$2,$2,$7
addu	$2,$3,$2
addiu	$2,$2,4
sra	$18,$2,31
sra	$2,$2,3
xor	$2,$18,$2
subu	$2,$2,$18
slt	$3,$2,$6
beq	$3,$0,$L433
nop

lbu	$3,1($5)
lbu	$24,1($12)
lbu	$7,1($11)
subu	$3,$19,$3
lbu	$19,1($13)
subu	$24,$24,$17
sll	$20,$3,2
sll	$24,$24,1
addu	$3,$20,$3
subu	$25,$7,$25
addu	$3,$24,$3
subu	$19,$16,$19
sll	$7,$25,2
addiu	$3,$3,4
sll	$19,$19,1
addu	$7,$7,$25
sra	$3,$3,3
addu	$7,$19,$7
subu	$19,$0,$3
slt	$20,$3,0
addiu	$7,$7,4
movn	$3,$19,$20
sra	$7,$7,3
subu	$20,$0,$7
slt	$24,$7,0
slt	$19,$3,$2
bne	$19,$0,$L436
movn	$7,$20,$24

slt	$19,$7,$2
beq	$19,$0,$L433
nop

$L436:
subu	$16,$17,$16
sra	$19,$16,31
xor	$16,$19,$16
subu	$16,$16,$19
sra	$16,$16,1
beq	$16,$0,$L433
slt	$20,$3,$7

movz	$3,$7,$20
subu	$2,$3,$2
sll	$7,$2,2
addu	$2,$7,$2
sra	$3,$2,31
xor	$18,$3,$18
beq	$19,$18,$L460
xor	$2,$3,$2

$L433:
lbu	$16,3($4)
lbu	$17,3($8)
lbu	$19,3($10)
lbu	$25,3($9)
subu	$7,$16,$17
subu	$3,$19,$25
sll	$2,$7,2
sll	$3,$3,1
addu	$2,$2,$7
addu	$2,$3,$2
addiu	$2,$2,4
sra	$18,$2,31
sra	$2,$2,3
xor	$2,$18,$2
subu	$2,$2,$18
slt	$3,$2,$6
beq	$3,$0,$L423
nop

lbu	$3,3($5)
lbu	$24,3($12)
lbu	$7,3($11)
subu	$3,$19,$3
lbu	$19,3($13)
subu	$24,$24,$17
sll	$20,$3,2
sll	$24,$24,1
addu	$3,$20,$3
subu	$25,$7,$25
addu	$3,$24,$3
subu	$19,$16,$19
sll	$7,$25,2
addiu	$3,$3,4
sll	$19,$19,1
addu	$7,$7,$25
sra	$3,$3,3
addu	$7,$19,$7
subu	$19,$0,$3
slt	$20,$3,0
addiu	$7,$7,4
movn	$3,$19,$20
sra	$7,$7,3
subu	$20,$0,$7
slt	$24,$7,0
slt	$19,$3,$2
bne	$19,$0,$L439
movn	$7,$20,$24

slt	$19,$7,$2
beq	$19,$0,$L423
nop

$L439:
subu	$16,$17,$16
sra	$19,$16,31
xor	$16,$19,$16
subu	$16,$16,$19
sra	$16,$16,1
beq	$16,$0,$L423
slt	$20,$3,$7

movz	$3,$7,$20
subu	$2,$3,$2
sll	$7,$2,2
addu	$2,$7,$2
sra	$3,$2,31
xor	$18,$3,$18
beq	$19,$18,$L461
nop

$L423:
addiu	$4,$4,4
addiu	$10,$10,4
addiu	$9,$9,4
addiu	$8,$8,4
addiu	$12,$12,4
addiu	$5,$5,4
addiu	$13,$13,4
bne	$4,$14,$L440
addiu	$11,$11,4

lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,24

$L458:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$24,$2
movz	$24,$2,$3
xor	$24,$24,$17
subu	$17,$24,$17
subu	$25,$25,$17
addu	$25,$15,$25
lbu	$2,0($25)
sb	$2,2($8)
lbu	$2,2($4)
addu	$17,$17,$2
addu	$17,$15,$17
lbu	$2,0($17)
b	$L428
sb	$2,2($4)

$L459:
subu	$2,$17,$2
sra	$2,$2,3
slt	$3,$18,$2
movn	$2,$18,$3
xor	$2,$2,$7
subu	$7,$2,$7
subu	$19,$19,$7
addu	$19,$15,$19
lbu	$2,0($19)
sb	$2,0($8)
lbu	$2,0($4)
addu	$7,$7,$2
addu	$7,$15,$7
lbu	$2,0($7)
b	$L432
sb	$2,0($4)

$L461:
xor	$2,$3,$2
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$16,$2
movz	$16,$2,$3
xor	$16,$16,$19
subu	$19,$16,$19
subu	$17,$17,$19
addu	$17,$15,$17
lbu	$2,0($17)
sb	$2,3($8)
lbu	$2,3($4)
addu	$19,$19,$2
addu	$19,$15,$19
lbu	$2,0($19)
b	$L423
sb	$2,3($4)

$L460:
subu	$2,$2,$3
sra	$2,$2,3
slt	$3,$16,$2
movz	$16,$2,$3
xor	$16,$16,$19
subu	$19,$16,$19
subu	$17,$17,$19
addu	$17,$15,$17
lbu	$2,0($17)
sb	$2,1($8)
lbu	$2,1($4)
addu	$19,$19,$2
addu	$19,$15,$19
lbu	$2,0($19)
b	$L433
sb	$2,1($4)

.set	macro
.set	reorder
.end	vc1_v_loop_filter16_c
.size	vc1_v_loop_filter16_c, .-vc1_v_loop_filter16_c
.section	.text.vc1_v_loop_filter4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_v_loop_filter4_c
.type	vc1_v_loop_filter4_c, @function
vc1_v_loop_filter4_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$7,$4,2
lbu	$13,2($4)
subu	$12,$0,$5
addu	$2,$7,$5
sll	$12,$12,1
addu	$25,$2,$12
lbu	$3,0($2)
addu	$2,$7,$12
lbu	$15,0($25)
lbu	$2,0($2)
subu	$14,$13,$15
subu	$8,$2,$3
sll	$9,$14,2
sll	$8,$8,1
addu	$14,$9,$14
addu	$14,$8,$14
addiu	$14,$14,4
sra	$24,$14,31
sra	$14,$14,3
xor	$14,$24,$14
subu	$14,$14,$24
slt	$8,$14,$6
beq	$8,$0,$L496
nop

sll	$8,$5,2
addiu	$sp,$sp,-16
subu	$8,$0,$8
sw	$16,4($sp)
sll	$9,$5,1
addu	$11,$7,$8
sw	$17,8($sp)
sw	$18,12($sp)
addu	$17,$7,$9
addu	$16,$11,$5
addu	$10,$9,$5
lbu	$11,0($11)
lbu	$17,0($17)
lbu	$16,0($16)
addu	$7,$7,$10
subu	$11,$11,$15
subu	$3,$17,$3
subu	$2,$2,$16
lbu	$7,0($7)
sll	$11,$11,1
sll	$16,$2,2
subu	$7,$13,$7
addu	$2,$16,$2
sll	$16,$3,2
addu	$2,$11,$2
sll	$7,$7,1
addiu	$2,$2,4
addu	$3,$16,$3
sra	$2,$2,3
addu	$3,$7,$3
subu	$7,$0,$2
slt	$11,$2,0
addiu	$3,$3,4
movn	$2,$7,$11
sra	$3,$3,3
subu	$11,$0,$3
slt	$16,$3,0
slt	$7,$2,$14
bne	$7,$0,$L467
movn	$3,$11,$16

slt	$7,$3,$14
bne	$7,$0,$L500
subu	$7,$15,$13

$L462:
lw	$18,12($sp)
$L501:
lw	$17,8($sp)
$L503:
lw	$16,4($sp)
addiu	$sp,$sp,16
$L496:
j	$31
nop

$L467:
subu	$7,$15,$13
$L500:
sra	$11,$7,31
xor	$7,$11,$7
subu	$7,$7,$11
sra	$7,$7,1
beq	$7,$0,$L501
lw	$18,12($sp)

slt	$13,$2,$3
movz	$2,$3,$13
subu	$14,$2,$14
sll	$2,$14,2
addu	$14,$2,$14
sra	$2,$14,31
xor	$24,$2,$24
beq	$11,$24,$L497
xor	$14,$2,$14

$L469:
addu	$2,$4,$5
lbu	$14,0($4)
addu	$3,$4,$12
addu	$25,$2,$12
lbu	$2,0($2)
lbu	$16,0($3)
lbu	$15,0($25)
subu	$13,$16,$2
subu	$11,$14,$15
sll	$3,$13,1
sll	$7,$11,2
addu	$13,$7,$11
addu	$13,$3,$13
addiu	$13,$13,4
sra	$24,$13,31
sra	$13,$13,3
xor	$13,$24,$13
subu	$13,$13,$24
slt	$3,$13,$6
beq	$3,$0,$L502
addiu	$7,$4,1

addu	$3,$4,$8
addu	$17,$4,$9
addu	$11,$3,$5
lbu	$7,0($3)
addu	$3,$4,$10
lbu	$17,0($17)
lbu	$11,0($11)
subu	$7,$7,$15
lbu	$3,0($3)
subu	$2,$17,$2
subu	$16,$16,$11
sll	$11,$7,1
sll	$7,$16,2
subu	$3,$14,$3
addu	$7,$7,$16
sll	$17,$2,2
addu	$7,$11,$7
sll	$3,$3,1
addiu	$7,$7,4
addu	$2,$17,$2
sra	$7,$7,3
addu	$2,$3,$2
subu	$3,$0,$7
slt	$11,$7,0
addiu	$2,$2,4
movn	$7,$3,$11
sra	$2,$2,3
subu	$11,$0,$2
slt	$16,$2,0
slt	$3,$7,$13
bne	$3,$0,$L472
movn	$2,$11,$16

slt	$3,$2,$13
beq	$3,$0,$L473
nop

$L472:
subu	$14,$15,$14
sra	$3,$14,31
xor	$14,$3,$14
subu	$14,$14,$3
sra	$14,$14,1
beq	$14,$0,$L473
slt	$11,$7,$2

movn	$2,$7,$11
subu	$13,$2,$13
sll	$2,$13,2
addu	$13,$2,$13
sra	$2,$13,31
xor	$24,$2,$24
beq	$3,$24,$L498
xor	$13,$2,$13

$L473:
addiu	$7,$4,1
$L502:
lbu	$13,1($4)
addu	$2,$7,$5
addu	$3,$7,$12
addu	$15,$2,$12
lbu	$16,0($2)
lbu	$25,0($3)
lbu	$14,0($15)
subu	$2,$25,$16
subu	$11,$13,$14
sll	$3,$2,1
sll	$2,$11,2
addu	$2,$2,$11
addu	$2,$3,$2
addiu	$2,$2,4
sra	$24,$2,31
sra	$2,$2,3
xor	$2,$24,$2
subu	$2,$2,$24
slt	$3,$2,$6
beq	$3,$0,$L474
addu	$3,$7,$8

addu	$18,$7,$9
addu	$17,$3,$5
lbu	$11,0($3)
addu	$7,$7,$10
lbu	$3,0($18)
lbu	$17,0($17)
subu	$11,$11,$14
lbu	$7,0($7)
subu	$16,$3,$16
subu	$17,$25,$17
sll	$25,$11,1
sll	$11,$17,2
sll	$3,$16,2
addu	$11,$11,$17
subu	$7,$13,$7
addu	$11,$25,$11
sll	$7,$7,1
addiu	$11,$11,4
addu	$3,$3,$16
sra	$11,$11,3
addu	$3,$7,$3
subu	$7,$0,$11
slt	$16,$11,0
addiu	$3,$3,4
movn	$11,$7,$16
sra	$3,$3,3
subu	$16,$0,$3
slt	$17,$3,0
slt	$7,$11,$2
bne	$7,$0,$L477
movn	$3,$16,$17

slt	$7,$3,$2
beq	$7,$0,$L474
nop

$L477:
subu	$13,$14,$13
sra	$7,$13,31
xor	$13,$7,$13
subu	$13,$13,$7
sra	$13,$13,1
beq	$13,$0,$L474
slt	$16,$11,$3

movn	$3,$11,$16
subu	$2,$3,$2
sll	$11,$2,2
addu	$2,$11,$2
sra	$3,$2,31
xor	$24,$3,$24
beq	$7,$24,$L499
xor	$2,$3,$2

$L474:
addiu	$14,$4,3
lbu	$7,3($4)
addu	$2,$14,$5
addu	$3,$14,$12
addu	$12,$2,$12
lbu	$24,0($2)
lbu	$15,0($3)
lbu	$11,0($12)
subu	$2,$15,$24
subu	$13,$7,$11
sll	$3,$2,1
sll	$2,$13,2
addu	$2,$2,$13
addu	$2,$3,$2
addiu	$2,$2,4
sra	$13,$2,31
sra	$2,$2,3
xor	$2,$13,$2
subu	$2,$2,$13
slt	$6,$2,$6
beq	$6,$0,$L501
lw	$18,12($sp)

addu	$8,$14,$8
addu	$9,$14,$9
addu	$5,$8,$5
lbu	$3,0($8)
addu	$10,$14,$10
lbu	$8,0($9)
lbu	$5,0($5)
subu	$3,$3,$11
lbu	$6,0($10)
subu	$24,$8,$24
subu	$15,$15,$5
sll	$5,$3,1
sll	$3,$15,2
sll	$8,$24,2
addu	$3,$3,$15
subu	$6,$7,$6
addu	$3,$5,$3
sll	$6,$6,1
addiu	$3,$3,4
addu	$5,$8,$24
sra	$3,$3,3
addu	$5,$6,$5
subu	$6,$0,$3
slt	$8,$3,0
addiu	$5,$5,4
movn	$3,$6,$8
sra	$5,$5,3
subu	$8,$0,$5
slt	$9,$5,0
slt	$6,$3,$2
bne	$6,$0,$L480
movn	$5,$8,$9

slt	$6,$5,$2
beq	$6,$0,$L501
nop

$L480:
subu	$7,$11,$7
sra	$6,$7,31
xor	$7,$6,$7
subu	$7,$7,$6
sra	$7,$7,1
beq	$7,$0,$L501
lw	$18,12($sp)

slt	$8,$3,$5
movz	$3,$5,$8
subu	$2,$3,$2
sll	$5,$2,2
addu	$2,$5,$2
sra	$3,$2,31
xor	$13,$3,$13
bne	$6,$13,$L503
lw	$17,8($sp)

xor	$2,$3,$2
lw	$5,%got(ff_cropTbl)($28)
subu	$2,$2,$3
sra	$2,$2,3
addiu	$5,$5,1024
slt	$3,$7,$2
movn	$2,$7,$3
xor	$2,$2,$6
subu	$6,$2,$6
subu	$11,$11,$6
addu	$11,$5,$11
lbu	$2,0($11)
sb	$2,0($12)
lbu	$2,3($4)
addu	$6,$6,$2
addu	$6,$5,$6
lbu	$2,0($6)
b	$L462
sb	$2,3($4)

$L497:
lw	$13,%got(ff_cropTbl)($28)
subu	$2,$14,$2
sra	$2,$2,3
addiu	$13,$13,1024
slt	$3,$7,$2
movn	$2,$7,$3
xor	$2,$2,$11
subu	$11,$2,$11
subu	$15,$15,$11
addu	$15,$13,$15
lbu	$2,0($15)
sb	$2,0($25)
lbu	$2,2($4)
addu	$11,$11,$2
addu	$11,$13,$11
lbu	$2,0($11)
b	$L469
sb	$2,2($4)

$L499:
lw	$11,%got(ff_cropTbl)($28)
subu	$2,$2,$3
sra	$2,$2,3
addiu	$11,$11,1024
slt	$3,$13,$2
movn	$2,$13,$3
xor	$2,$2,$7
subu	$7,$2,$7
subu	$14,$14,$7
addu	$14,$11,$14
lbu	$2,0($14)
sb	$2,0($15)
lbu	$2,1($4)
addu	$7,$7,$2
addu	$7,$11,$7
lbu	$2,0($7)
b	$L474
sb	$2,1($4)

$L498:
lw	$11,%got(ff_cropTbl)($28)
subu	$2,$13,$2
sra	$2,$2,3
addiu	$11,$11,1024
slt	$7,$14,$2
movn	$2,$14,$7
xor	$2,$2,$3
subu	$3,$2,$3
subu	$15,$15,$3
addu	$15,$11,$15
lbu	$2,0($15)
sb	$2,0($25)
lbu	$2,0($4)
addu	$3,$3,$2
addu	$3,$11,$3
lbu	$2,0($3)
b	$L473
sb	$2,0($4)

.set	macro
.set	reorder
.end	vc1_v_loop_filter4_c
.size	vc1_v_loop_filter4_c, .-vc1_v_loop_filter4_c
.section	.text.unlikely.ff_vc1dsp_init,"ax",@progbits
.align	2
.globl	ff_vc1dsp_init
.set	nomips16
.set	nomicromips
.ent	ff_vc1dsp_init
.type	ff_vc1dsp_init, @function
ff_vc1dsp_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(vc1_inv_trans_8x8_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_8x8_c)
sw	$2,2808($4)
lw	$2,%got(vc1_inv_trans_4x8_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_4x8_c)
sw	$2,2816($4)
lw	$2,%got(vc1_inv_trans_8x4_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_8x4_c)
sw	$2,2812($4)
lw	$2,%got(vc1_inv_trans_4x4_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_4x4_c)
sw	$2,2820($4)
lw	$2,%got(vc1_inv_trans_8x8_dc_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_8x8_dc_c)
sw	$2,2824($4)
lw	$2,%got(vc1_inv_trans_4x8_dc_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_4x8_dc_c)
sw	$2,2832($4)
lw	$2,%got(vc1_inv_trans_8x4_dc_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_8x4_dc_c)
sw	$2,2828($4)
lw	$2,%got(vc1_inv_trans_4x4_dc_c)($28)
addiu	$2,$2,%lo(vc1_inv_trans_4x4_dc_c)
sw	$2,2836($4)
lw	$2,%got(vc1_h_overlap_c)($28)
addiu	$2,$2,%lo(vc1_h_overlap_c)
sw	$2,2844($4)
lw	$2,%got(vc1_v_overlap_c)($28)
addiu	$2,$2,%lo(vc1_v_overlap_c)
sw	$2,2840($4)
lw	$2,%got(vc1_v_loop_filter4_c)($28)
addiu	$2,$2,%lo(vc1_v_loop_filter4_c)
sw	$2,2848($4)
lw	$2,%got(vc1_h_loop_filter4_c)($28)
addiu	$2,$2,%lo(vc1_h_loop_filter4_c)
sw	$2,2852($4)
lw	$2,%got(vc1_v_loop_filter8_c)($28)
addiu	$2,$2,%lo(vc1_v_loop_filter8_c)
sw	$2,2856($4)
lw	$2,%got(vc1_h_loop_filter8_c)($28)
addiu	$2,$2,%lo(vc1_h_loop_filter8_c)
sw	$2,2860($4)
lw	$2,%got(vc1_v_loop_filter16_c)($28)
addiu	$2,$2,%lo(vc1_v_loop_filter16_c)
sw	$2,2864($4)
lw	$2,%got(vc1_h_loop_filter16_c)($28)
addiu	$2,$2,%lo(vc1_h_loop_filter16_c)
sw	$2,2868($4)
lw	$2,%got(ff_put_pixels8x8_c)($28)
sw	$2,2872($4)
lw	$2,%got(put_vc1_mspel_mc10_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc10_c)
sw	$2,2876($4)
lw	$2,%got(put_vc1_mspel_mc20_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc20_c)
sw	$2,2880($4)
lw	$2,%got(put_vc1_mspel_mc30_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc30_c)
sw	$2,2884($4)
lw	$2,%got(put_vc1_mspel_mc01_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc01_c)
sw	$2,2888($4)
lw	$2,%got(put_vc1_mspel_mc11_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc11_c)
sw	$2,2892($4)
lw	$2,%got(put_vc1_mspel_mc21_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc21_c)
sw	$2,2896($4)
lw	$2,%got(put_vc1_mspel_mc31_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc31_c)
sw	$2,2900($4)
lw	$2,%got(put_vc1_mspel_mc02_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc02_c)
sw	$2,2904($4)
lw	$2,%got(put_vc1_mspel_mc12_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc12_c)
sw	$2,2908($4)
lw	$2,%got(put_vc1_mspel_mc22_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc22_c)
sw	$2,2912($4)
lw	$2,%got(put_vc1_mspel_mc32_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc32_c)
sw	$2,2916($4)
lw	$2,%got(put_vc1_mspel_mc03_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc03_c)
sw	$2,2920($4)
lw	$2,%got(put_vc1_mspel_mc13_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc13_c)
sw	$2,2924($4)
lw	$2,%got(put_vc1_mspel_mc23_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc23_c)
sw	$2,2928($4)
lw	$2,%got(put_vc1_mspel_mc33_c)($28)
addiu	$2,$2,%lo(put_vc1_mspel_mc33_c)
sw	$2,2932($4)
lw	$2,%got(ff_avg_pixels8x8_c)($28)
sw	$2,2936($4)
lw	$2,%got(avg_vc1_mspel_mc10_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc10_c)
sw	$2,2940($4)
lw	$2,%got(avg_vc1_mspel_mc20_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc20_c)
sw	$2,2944($4)
lw	$2,%got(avg_vc1_mspel_mc30_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc30_c)
sw	$2,2948($4)
lw	$2,%got(avg_vc1_mspel_mc01_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc01_c)
sw	$2,2952($4)
lw	$2,%got(avg_vc1_mspel_mc11_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc11_c)
sw	$2,2956($4)
lw	$2,%got(avg_vc1_mspel_mc21_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc21_c)
sw	$2,2960($4)
lw	$2,%got(avg_vc1_mspel_mc31_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc31_c)
sw	$2,2964($4)
lw	$2,%got(avg_vc1_mspel_mc02_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc02_c)
sw	$2,2968($4)
lw	$2,%got(avg_vc1_mspel_mc12_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc12_c)
sw	$2,2972($4)
lw	$2,%got(avg_vc1_mspel_mc22_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc22_c)
sw	$2,2976($4)
lw	$2,%got(avg_vc1_mspel_mc32_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc32_c)
sw	$2,2980($4)
lw	$2,%got(avg_vc1_mspel_mc03_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc03_c)
sw	$2,2984($4)
lw	$2,%got(avg_vc1_mspel_mc13_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc13_c)
sw	$2,2988($4)
lw	$2,%got(avg_vc1_mspel_mc23_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc23_c)
sw	$2,2992($4)
lw	$2,%got(avg_vc1_mspel_mc33_c)($28)
addiu	$2,$2,%lo(avg_vc1_mspel_mc33_c)
j	$31
sw	$2,2996($4)

.set	macro
.set	reorder
.end	ff_vc1dsp_init
.size	ff_vc1dsp_init, .-ff_vc1dsp_init
.rdata
.align	2
.type	shift_value.5621, @object
.size	shift_value.5621, 16
shift_value.5621:
.word	0
.word	5
.word	1
.word	5
.align	2
.type	shift_value.5660, @object
.size	shift_value.5660, 16
shift_value.5660:
.word	0
.word	5
.word	1
.word	5
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
