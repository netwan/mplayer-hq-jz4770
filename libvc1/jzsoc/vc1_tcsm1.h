/*****************************************************************************
 *
 * JZ4760 TCSM1 Space Seperate
 *
 * $Id: vc1_tcsm1.h,v 1.7 2011/06/29 07:05:56 dqliu Exp $
 *
 ****************************************************************************/

#ifndef __VC1_TCSM1_H__
#define __VC1_TCSM1_H__

#define TCSM1_BANK0 0xF4000000
#define TCSM1_BANK1 0xF4001000
#define TCSM1_BANK2 0xF4002000
#define TCSM1_BANK3 0xF4003000
#define TCSM1_BANK4 0xF4004000

volatile unsigned char *tcsm1_base;
#define TCSM1_PADDR(a)         ((((unsigned)(a)) & 0xFFFF) | 0x132C0000)
#define TCSM1_VCADDR(a)        (tcsm1_base + ((a) & 0xFFFF))

#define VC1_P1_MAIN (TCSM1_BANK0)

#define TCSM1_CMD_LEN  (8 << 2)
#define TCSM1_MBNUM_WP          (TCSM1_BANK4)
#define TCSM1_MBNUM_RP          (TCSM1_MBNUM_WP+4)
#define TCSM1_ADDR_RP           (TCSM1_MBNUM_RP+4)
#define TCSM1_DCORE_SHARE_ADDR  (TCSM1_ADDR_RP+4)
#define TCSM1_FIRST_MBLEN       (TCSM1_DCORE_SHARE_ADDR+4)

#define DFRM_BUF_LEN            (((sizeof(struct VC1_Frame_GlbARGs)+31)/32)*32)
#define TCSM1_DFRM_BUF          (TCSM1_MBNUM_WP+TCSM1_CMD_LEN)//must be cache align

#define TASK_BUF_LEN            ((sizeof(struct VC1_MB_DecARGs) + 3) & 0xFFFFFFFC)
#define TASK_BUF0               (TCSM1_DFRM_BUF+DFRM_BUF_LEN)
#define TASK_BUF1               (TASK_BUF0+TASK_BUF_LEN)
#define TASK_BUF2               (TASK_BUF1+TASK_BUF_LEN)

#define VC1_FIFO_DEP  8

#define MC_CHN_ONELEN  (14<<2)
#define MC_CHN_DEP  VC1_FIFO_DEP
#define MC_CHN_LEN  (MC_CHN_ONELEN*MC_CHN_DEP)

#define TCSM1_MOTION_DHA        (TASK_BUF2+TASK_BUF_LEN)
#define TCSM1_MOTION_DSA        (TCSM1_MOTION_DHA + MC_CHN_LEN)

#define VMAU_CHN_ONELEN  (9<<2)
#define VMAU_CHN_DEP  VC1_FIFO_DEP
#define VMAU_CHN_LEN  (VMAU_CHN_ONELEN*VMAU_CHN_DEP)
#define VMAU_CHN_BASE           (TCSM1_MOTION_DSA+0x4)
#define VMAU_END_FLAG           (VMAU_CHN_BASE+VMAU_CHN_LEN)

#define GP0_DES_CHAIN_LEN  ((2*4)<<2)
#define DDMA_GP0_DES_CHAIN      (VMAU_END_FLAG+4)
#define GP1_DES_CHAIN_LEN  ((4*5)<<2)
#define DDMA_GP1_DES_CHAIN      (DDMA_GP0_DES_CHAIN+GP0_DES_CHAIN_LEN)
#define DDMA_GP1_DES_CHAIN1     (DDMA_GP1_DES_CHAIN+GP1_DES_CHAIN_LEN)

#define TCSM1_GP1_POLL_END      (DDMA_GP1_DES_CHAIN1 + GP1_DES_CHAIN_LEN)
#define TCSM1_GP0_POLL_END      (TCSM1_GP1_POLL_END + 4)

#define MSRC_ONE_LEN  (384<<1)
#define MSRC_DEP  VC1_FIFO_DEP
#define MSRC_BUF_LEN  (MSRC_ONE_LEN*MSRC_DEP)
#define TCSM1_MSRC_BUF          (TCSM1_GP0_POLL_END + 4)

#define DOUT_Y_STRD  (16)
#define DOUT_C_STRD  (8)
#define VC1_DYBUF_LEN           (DOUT_Y_STRD*16)
#define VC1_DCBUF_LEN           (DOUT_Y_STRD* 8)
#define VC1_DBUF_LEN            (VC1_DYBUF_LEN + VC1_DCBUF_LEN)
/*mc previous recon buffer*/
#define DOUT_YBUF0              (TCSM1_MSRC_BUF + MSRC_BUF_LEN)
#define DOUT_CBUF0              (DOUT_YBUF0 + VC1_DYBUF_LEN)

#define DOUT_YBUF1              (DOUT_CBUF0 + VC1_DCBUF_LEN)
#define DOUT_CBUF1              (DOUT_YBUF1 + VC1_DYBUF_LEN)

#define DOUT_YBUF2              (DOUT_CBUF1 + VC1_DCBUF_LEN)
#define DOUT_CBUF2              (DOUT_YBUF2 + VC1_DYBUF_LEN)

#define ROTA_Y_OFFSET  (16 * 16)
#define ROTA_C_OFFSET  (16 * 8)
#define ROTA_Y_BUF              (DOUT_CBUF2 + VC1_DCBUF_LEN)
#define ROTA_C_BUF              (ROTA_Y_BUF + ROTA_Y_OFFSET)

#define ROTA_Y_BUF1             (ROTA_C_BUF + ROTA_C_OFFSET)
#define ROTA_C_BUF1             (ROTA_Y_BUF1 + ROTA_Y_OFFSET)

#define TCSM1_PMON_BUF_LEN  (18 << 2)
#define TCSM1_PMON_BUF          (ROTA_C_BUF1 + ROTA_C_OFFSET)

#define EDGE_WIDTH  32
#endif
