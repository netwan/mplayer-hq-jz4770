/*****************************************************************************
 *
 * JZC VC1 Decoder Architecture
 *
 ****************************************************************************/

#ifndef __JZC_VC1_ARCH_H__
#define __JZC_VC1_ARCH_H__

typedef struct VC1_Frame_Ptr{
  uint8_t *ptr[2];
}VC1_Frame_Ptr;

typedef struct VC1_Frame_GlbARGs{
  uint8_t mb_width;
  uint8_t mb_height;
  uint8_t pq;
  uint8_t halfpq;

  uint8_t overlap;      // 
  uint8_t pict_type;    //
  uint8_t profile;      //
  uint8_t bi_type;      //
  uint8_t rangeredfrm;  //
  uint8_t fastuvmc;
  uint8_t mspel;
  uint8_t use_ic;
  uint8_t flags;
  uint8_t lowres;

  uint16_t pquantizer;
  uint16_t linesize;
  uint16_t uvlinesize;
  uint8_t *last_data[2];
  uint8_t *next_data[2];

  VC1_Frame_Ptr current_picture_save;
  VC1_Frame_Ptr rota_current_picture;
}VC1_Frame_GlbARGs;

#define FRAME_T_CC_LINE ((sizeof(struct VC1_Frame_GlbARGs)+31)/32)

typedef struct VC1_MB_DecARGs{
  uint16_t next_dma_len;
  uint8_t mb_x;
  uint8_t mb_y;
  
  int16_t real_num;
  uint8_t idct_row[6];   //intra:12

  //uint8_t pb_val[6];
  //int16_t mq;

  uint32_t mau_mcbp;
  uint32_t mau_acbp;
  uint32_t mau_qp;

  uint8_t vc1_fourmv;
  uint8_t mv_mode;
  //uint8_t idct_row_4x4[24];
  //uint8_t idct_row_8x4[12];

  uint8_t vc1_intra[6];
  //uint8_t ttblock[6];

  //char subblockpat[6];
  uint8_t vc1_skipped;
  uint8_t vc1_direct;

  uint8_t vc1_mb_has_coeffs[2];
  uint8_t bfmv_type;
  //uint8_t pb_val[6];
  uint8_t vc1_b_mc_skipped;
  uint8_t chroma_ret;
  uint8_t bintra;
  //uint8_t dump[2];
  short tx, ty;
  short vc1_mv[4][2];
  DCTELEM mb[6*64];
}VC1_MB_DecARGs;

#endif /*__JZC_VC1_ARCH_H__*/

