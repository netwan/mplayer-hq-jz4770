#define VC1_P1_USE_PADDR

#include "jzsys.h"
#include "jzasm.h"
#include "jzmedia.h"
#include "vc1_p1_type.h"
#include "jz4760_2ddma_hw.h"
#include "vc1_dcore.h"
#include "vc1_tcsm0.h"
#include "vc1_tcsm1.h"
#include "../../libjzcommon/jz4760e_aux_pmon.h"
#include "t_vmau.h"
//#include "debug1.h"

#define VC1_STOP_P1()						\
  ({								\
    ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=0;	\
    *((volatile int *)(TCSM0_PADDR(TCSM0_P1_TASK_DONE))) = 0x1;	\
    i_nop;							\
    i_nop;							\
    i_wait();							\
  })

#include "vc1_idct.c"
#include "vc1_p1_mc.c"

#define __p1_text __attribute__ ((__section__ (".p1_text")))
#define __p1_main __attribute__ ((__section__ (".p1_main")))
#define __p1_data __attribute__ ((__section__ (".p1_data")))

#ifdef JZC_PMON_P1
PMON_CREAT(mc_polling);
PMON_CREAT(mc_cfg);
PMON_CREAT(idct);
PMON_CREAT(gp0);
PMON_CREAT(gp1);
PMON_CREAT(gp2);
#endif

uint32_t current_picture_ptr[2];
VC1_Frame_GlbARGs *dFRM;
VC1_MB_DecARGs *dMB;

uint8_t *rota_buf1, *rota_buf2,*rota_buf3, *rota_buf4;
uint8_t luty[256],lutuv[256];
//int mc_flag,last_mc_flag;
#define XCHG2(a,b,t)   t=a;a=b;b=t
#define XCHG3(a,b,c,t)   t=a;a=b;b=c;c=t
#undef JZC_ROTA90_OPT
__p1_main int main() {

  //enable MXU 
  S32I2M(xr16, 0x3);
  int xchg_tmp;
  int i, j, k, count, off, dMBs;
  int linesize, uvlinesize, mb_size, block_size;
  uint8_t *dout_yptr[3], *dout_cptr[3];
#ifdef JZC_ROTA90_OPT
  int rota_mx;
  uint32_t rota_picture_ptr[2];
  uint32_t rota_picture_ptr1[2];
#endif
  DCTELEM *idct, *blk;
  uint8_t *mc_uv;
  int *gp0_chain_ptr, *gp1_chain_ptr, *gp1_chain_ptr1,*pmon_ptr;
  VC1_MB_DecARGs *dMB_L, *dMB_N;
  uint8_t mv_mode2;
  uint32_t fifo_index = 1;
  fifo_index++;

  uint32_t motion_dha,next_motion_dha;
  uint32_t msrc_buf,next_msrc_buf;
  VMAU_CHN_REG *mau_reg_ptr,*next_mau_reg_ptr;

  volatile int *tcsm0_gp0_poll_end ,*tcsm0_gp1_poll_end;
  volatile int *tcsm1_gp0_poll_end ,*tcsm1_gp1_poll_end;

  motion_dha = TCSM1_MOTION_DHA;
  next_motion_dha = TCSM1_MOTION_DHA + MC_CHN_ONELEN;
  msrc_buf = TCSM1_MSRC_BUF;
  next_msrc_buf = TCSM1_MSRC_BUF + MSRC_ONE_LEN;

  mau_reg_ptr = VMAU_CHN_BASE;
  next_mau_reg_ptr = VMAU_CHN_BASE + VMAU_CHN_ONELEN;

  tcsm1_gp0_poll_end  = (volatile int*)(TCSM1_GP0_POLL_END);
  *tcsm1_gp0_poll_end  = 1;

  tcsm0_gp0_poll_end  = (volatile int*)TCSM0_PADDR(TCSM0_GP0_POLL_END);
  *tcsm0_gp0_poll_end  = 0;

  tcsm1_gp1_poll_end  = (volatile int*)(TCSM1_GP1_POLL_END);
  *tcsm1_gp1_poll_end  = 0;

  tcsm0_gp1_poll_end  = (volatile int*)TCSM0_PADDR(TCSM0_GP1_POLL_END);
  *tcsm0_gp1_poll_end  = 0;

  const uint32_t scale_table[8]={
    0x8040200,0x9090808,0xb0b0a0a,0xd0d0c0c,0xf0f0e0e,0x11111010,0x13131212,0x15151414,
  };
  count = 0;
  volatile int *mbnum_wp=TCSM1_MBNUM_WP;
  volatile int *mbnum_rp=TCSM1_MBNUM_RP;
  volatile int *addr_rp=TCSM1_ADDR_RP;

  *mbnum_rp=0;
  *addr_rp=TCSM0_PADDR(TCSM0_TASK_FIFO);

  dFRM=TCSM1_DFRM_BUF;
  mb_size= 4 - dFRM->lowres;
  block_size = 8>>dFRM->lowres;
  //last_mc_flag = 0;

  dMB = TASK_BUF1;//curr mb
  dMB_L = TASK_BUF0;
  dMB_N = TASK_BUF2;
  gp0_chain_ptr = DDMA_GP0_DES_CHAIN;
  gp0_chain_ptr[2]=GP_STRD(64,GP_FRM_NML,64);
  set_gp0_dha(TCSM1_PADDR(DDMA_GP0_DES_CHAIN));

  dout_yptr[0] = DOUT_YBUF0;
  dout_yptr[1] = DOUT_YBUF1;
  dout_yptr[2] = DOUT_YBUF2;

  dout_cptr[0] = DOUT_CBUF0;
  dout_cptr[1] = DOUT_CBUF1;
  dout_cptr[2] = DOUT_CBUF2;

#ifdef JZC_ROTA90_OPT
  rota_buf1 = ROTA_Y_BUF;
  rota_buf2 = ROTA_C_BUF;

  rota_buf3 = ROTA_Y_BUF1;
  rota_buf4 = ROTA_C_BUF1;
#endif

  gp1_chain_ptr = DDMA_GP1_DES_CHAIN;
  gp1_chain_ptr1 = DDMA_GP1_DES_CHAIN1;

  gp1_chain_ptr[2]=GP_STRD(256,GP_FRM_NML,256);
  gp1_chain_ptr[3]=GP_UNIT(GP_TAG_LK,256,256);

  gp1_chain_ptr[6]=GP_STRD(128,GP_FRM_NML,128);
  gp1_chain_ptr[7]=GP_UNIT(GP_TAG_LK,128,128);

  gp1_chain_ptr1[2]=GP_STRD(256,GP_FRM_NML,256);
  gp1_chain_ptr1[3]=GP_UNIT(GP_TAG_LK,256,256);
  
  gp1_chain_ptr1[6]=GP_STRD(128,GP_FRM_NML,128);
  gp1_chain_ptr1[7]=GP_UNIT(GP_TAG_LK,128,128);

#ifdef JZC_PMON_P1
  pmon_ptr=(int*)TCSM1_PMON_BUF;
  PMON_CLEAR(mc_polling);
  PMON_CLEAR(mc_cfg);
  PMON_CLEAR(gp1);
  PMON_CLEAR(gp0);
  PMON_CLEAR(gp2);
  PMON_CLEAR(idct);
#endif

  while(*mbnum_wp<=*mbnum_rp+2);//wait until the first two mb is ready

  gp0_chain_ptr[0]=addr_rp[0];
  gp0_chain_ptr[1]=TCSM1_PADDR(dMB);
  gp0_chain_ptr[3]=GP_UNIT(GP_TAG_UL,64,(*(int*)TCSM1_FIRST_MBLEN));
  set_gp0_dcs();
  poll_gp0_end();

  (*mbnum_rp)++;
  *addr_rp+=*(int*)TCSM1_FIRST_MBLEN;
  ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=*addr_rp;

  gp0_chain_ptr[0]=addr_rp[0];
  gp0_chain_ptr[1]=TCSM1_PADDR(dMB_N);
  gp0_chain_ptr[3]=GP_UNIT(GP_TAG_LK,64,dMB->next_dma_len);

  gp0_chain_ptr[4] = TCSM0_PADDR(TCSM0_GP0_POLL_END);
  gp0_chain_ptr[5] = TCSM1_PADDR(TCSM1_GP0_POLL_END);
  gp0_chain_ptr[6] = GP_STRD(4,GP_FRM_NML,4);
  gp0_chain_ptr[7] = GP_UNIT(GP_TAG_UL,4,4);

  set_gp0_dcs();

  uint32_t is_iframe=dFRM->pict_type == I_TYPE || (dFRM->pict_type == B_TYPE && dFRM->bi_type);
  volatile uint32_t *qt_tbl =VMAU_V_BASE + VMAU_QT_BASE;
  qt_tbl[0]=scale_table[0];
  qt_tbl[1]=scale_table[1];
  qt_tbl[2]=scale_table[2];
  qt_tbl[3]=scale_table[3];
  if(is_iframe){
    int tbl_index=dMB->mau_qp & 0x10;
    if(tbl_index&&(dFRM->profile != PROFILE_ADVANCED)){
      tbl_index=(mau_reg_ptr->quant_para & 0xF)>>2;
      qt_tbl[tbl_index]=scale_table[tbl_index+4];
    }
  }else{
    *(uint32_t *)motion_dha=0;
    SET_REG1_DSA(TCSM1_PADDR(TCSM1_MOTION_DSA));
    SET_REG1_BINFO(0,0,0,0,dFRM->mspel? (IS_ILUT0): (IS_ILUT1 | 1),0,0,0,0,0,0,0,0);
    SET_REG1_DDC(TCSM1_PADDR(motion_dha) | 0x1);
  }
  *(volatile uint32_t *)VMAU_END_FLAG = 1;

  current_picture_ptr[0] = dFRM->current_picture_save.ptr[0]-256*3;
  current_picture_ptr[1] = dFRM->current_picture_save.ptr[1]-128*3;

  while (dMB->real_num > 0){

    current_picture_ptr[0]+=256;
    current_picture_ptr[1]+=128;
    int tbl_index;
    //mc_flag = 0;
    int last_mb_y,last_mb_x;
    *(uint32_t *)next_motion_dha = 0;
    volatile int *tdd = (int *)motion_dha;
    int tkn = 0;
    tdd ++;
#ifdef JZC_PMON_P1
    PMON_ON(mc_cfg);
#endif

    if (dFRM->pict_type == P_TYPE){
      if(!dMB->vc1_fourmv) {
	if (((!dMB->vc1_skipped) && (!dMB->vc1_intra[0])) || (dMB->vc1_skipped)){
	  vc1_mc_1mv_hw(0,0,tdd, &tkn);
	  //mc_flag = 1;
	}
      }else{
	for (i=0; i<4; i++) {
	  if (((!dMB->vc1_skipped) && (!dMB->vc1_intra[i])) || (dMB->vc1_skipped)) {
	    vc1_mc_4mv_luma_hw(i, tdd, &tkn);
	  }
	}
	tdd[2*tkn-1] |= (1<<TDD_DOE_SFT) | (1 << 24);
	vc1_mc_4mv_chroma_hw(tdd, &tkn);
	//mc_flag = 1;
      }
    }
    else if (dFRM->pict_type == B_TYPE && (!(dFRM->bi_type))){
      if (!dMB->vc1_b_mc_skipped){
	if(dFRM->use_ic) {
	  mv_mode2 = dMB->mv_mode;
	  dMB->mv_mode = MV_PMODE_INTENSITY_COMP;
	}
	if(dMB->vc1_direct || (dMB->bfmv_type == BMV_TYPE_INTERPOLATED)) {
	  vc1_mc_1mv_hw_b(1, 0, tdd, &tkn);
          //mc_flag = 1;
	} else {
	  if(dFRM->use_ic && (dMB->bfmv_type == BMV_TYPE_BACKWARD)) dMB->mv_mode = mv_mode2;
	  vc1_mc_1mv_hw(0, (dMB->bfmv_type == BMV_TYPE_BACKWARD),tdd, &tkn);
          //mc_flag = 1;
	}
	if(dFRM->use_ic) dMB->mv_mode = mv_mode2;
      }
    }

    tdd[2*tkn] = TDD_CFG(1,/*vld*/
		     1,/*lk*/
		     REG1_DDC/*cidx*/);
    tdd[2*tkn+1] = TCSM1_PADDR(next_motion_dha);
    tdd[2*tkn+2] = TDD_SYNC(1,/*vld*/
			   1,/*lk*/
			   1, /*crst*/
			   0xFFFF/*id*/);
    tdd[-1] = TDD_HEAD(1,/*vld*/
		       1,/*lk*/
		       SubPel[VC1_QPEL]-1,/*ch1pel*/
		       SubPel[VC1_QPEL]-1,/*ch2pel*/ 
		       TDD_POS_SPEC,/*posmd*/
		       TDD_MV_SPEC,/*mvmd*/
		       tkn,/*tkn*/
		       dMB->mb_y,/*mby*/
		       dMB->mb_x/*mbx*/);
#ifdef JZC_PMON_P1
    PMON_OFF(mc_cfg);
#endif

#if 0
    if(count>0)
      {
	while(*(volatile int *)TCSM1_MOTION_DSA != 0x8000FFFF);
	*(volatile int *)TCSM1_MOTION_DSA = 0;
      }
#endif

    uint16_t *dst=msrc_buf-16;
    {
      uint16_t *src;
      if(is_iframe)
	src=((uint32_t)dMB) + 24;
      else
	src=dMB->mb;
      src-=8;
      uint32_t cbp_mask = VC1_8x8;
      for(i=0;i<6;i++){
	for(j=0;j<dMB->idct_row[i];j++){
	  S32LDI(xr1,src,0x10);
	  S32LDD(xr2,src,0x4);
	  S32LDD(xr3,src,0x8);
	  S32LDD(xr4,src,0xc);
	  S32SDI(xr1,dst,0x10);
	  S32STD(xr2,dst,0x4);
	  S32STD(xr3,dst,0x8);
	  S32STD(xr4,dst,0xc);
	}
	if((dMB->mau_acbp&cbp_mask)==cbp_mask)
	  for(j=dMB->idct_row[i];j<8;j++){
	    S32SDI(xr0,dst,0x10);
	    S32STD(xr0,dst,0x4);
	    S32STD(xr0,dst,0x8);
	    S32STD(xr0,dst,0xc);
	  }
	cbp_mask<<=2;
      }
    }

#ifdef VMAU_OPT
    next_mau_reg_ptr->main_cbp=dMB->mau_mcbp;
    next_mau_reg_ptr->quant_para=dMB->mau_qp;
    next_mau_reg_ptr->main_addr=TCSM1_PADDR(msrc_buf);
    //next_mau_reg_ptr->ncchn_addr=TCSM1_PADDR(next_mau_reg_ptr);
    next_mau_reg_ptr->main_len = (uint32_t)dst-msrc_buf+16;
    next_mau_reg_ptr->id=1;
    //next_mau_reg_ptr->dec_incr=0;
    next_mau_reg_ptr->aux_cbp=dMB->mau_acbp;
    //next_mau_reg_ptr->y_pred_mode[0]=0;
    //next_mau_reg_ptr->y_pred_mode[1]=0;
    //next_mau_reg_ptr->c_pred_mode[0]=0;
#endif

    if(count > 0)
      {
#ifdef JZC_PMON_P1
	PMON_ON(idct);
#endif
#if 0
	if (dFRM->pict_type == B_TYPE && (!(dFRM->bi_type))){
	  if (!dMB_L->vc1_b_mc_skipped){
	    if (dMB_L->vc1_skipped) {
	      goto end;
	    } else if (!dMB_L->vc1_direct) {
	      if(!dMB_L->vc1_mb_has_coeffs[0] && !dMB_L->bintra)
		goto end;
	      if(!(dMB_L->bintra && !dMB_L->vc1_mb_has_coeffs[0]))
		if(dMB_L->bfmv_type == BMV_TYPE_INTERPOLATED)
		  if(!dMB_L->vc1_mb_has_coeffs[1])
		    goto end;
	    }
	  }
	}
#endif
	//begin idct and add residual
#ifdef VMAU_OPT
	while(*(volatile uint32_t *)VMAU_END_FLAG == 0);
	if(!is_iframe){
	  volatile uint32_t *addr =VMAU_V_BASE + VMAU_QT_BASE;
	  addr[0]=scale_table[0];
	  addr[1]=scale_table[1];
	  addr[2]=scale_table[2];
	  addr[3]=scale_table[3];
	  tbl_index=mau_reg_ptr->quant_para & (0x10 << VC1_MQT_SFT);
	  if(tbl_index){
	    tbl_index=(mau_reg_ptr->quant_para & (0xF << VC1_MQT_SFT))>>(VC1_MQT_SFT+2);
	    addr[tbl_index]=scale_table[tbl_index+4];
	  }
	}
	write_reg(VMAU_V_BASE+VMAU_SLV_DEC_YADDR,TCSM1_PADDR(dout_yptr[1]));
	write_reg(VMAU_V_BASE+VMAU_SLV_DEC_UADDR,TCSM1_PADDR(dout_cptr[1]));
	write_reg(VMAU_V_BASE+VMAU_SLV_DEC_VADDR,TCSM1_PADDR(dout_cptr[1])+8);
	write_reg(VMAU_V_BASE+VMAU_SLV_NCCHN_ADDR,TCSM1_PADDR(mau_reg_ptr));
	write_reg(VMAU_V_BASE+VMAU_SLV_DEC_DONE, TCSM1_PADDR(VMAU_END_FLAG));
	*(volatile uint32_t *)VMAU_END_FLAG = 0;
	write_reg(VMAU_V_BASE+VMAU_SLV_GBL_RUN, 1); //run
#else
	if(dFRM->pict_type == I_TYPE || (dFRM->pict_type == B_TYPE && dFRM->bi_type)){

	}else{
	  if( !((dFRM->pict_type == P_TYPE) && (dMB_L->vc1_skipped))) {

	  }
	}
#endif
#ifdef JZC_PMON_P1
	PMON_OFF(idct);
#endif
      }
  end:
    if(count > 1)
      {
#ifdef JZC_PMON_P1
	PMON_ON(gp2);
#endif

#ifdef JZC_ROTA90_OPT
	int strd = 16;
	uint8_t *t1,*t2,*t3,*t4;
	for(i = 0;i < 4;i++){
	  t1 = dout_yptr[0] + 16*i*4 - 4;
	  t2 = rota_buf1 + 12 - 4*i - strd;
	  for(j = 0; j < 4; j++){
	    S32LDI (xr1, t1, 0x4);       //xr1: x3  ,x2  ,x1  ,x0
	    S32LDD (xr2, t1, 0x10);      //xr2: x19 ,x18 ,x17 ,x16
	    S32LDD (xr3, t1, 0x20);      //xr3: x35 ,x34 ,x33 ,x32
	    S32LDD (xr4, t1, 0x30);      //xr4: x51 ,x50 ,x49 ,x48

	    S32SFL(xr2,xr2,xr1,xr1,ptn0); //xr2:x19,x3,x18,x2 xr1:x17,x1,x16,x0
	    S32SFL(xr4,xr4,xr3,xr3,ptn0); //xr4:x51,x35,x50,x34 xr3:x49,x33,x48,x32
	    S32SFL(xr4,xr4,xr2,xr2,ptn3); //xr4:x51,x35,x19,x3 xr2:x50,x34,x18,x2
	    S32SFL(xr3,xr3,xr1,xr1,ptn3); //xr3:x49,x33,x17,x1 xr1:x48,x32,x16,x0
	    S32SDIVR(xr1, t2, strd, 0x0);
	    S32SDIVR(xr3, t2, strd, 0x0);
	    S32SDIVR(xr2, t2, strd, 0x0);
	    S32SDIVR(xr4, t2, strd, 0x0);
	  }
	}

	strd = 16;
	for(i = 0;i < 2;i++){
	  t1 = dout_cptr[0] + 16*i*4 - 4;
	  t2 = rota_buf2 + 4 - 4*i - strd;
	  t3 = t1 + 8;
	  t4 = t2 + 8;
	  for(j = 0; j < 2; j++){
	    S32LDI (xr1, t1, 0x4);       //xr1: x3  ,x2  ,x1  ,x0
	    S32LDD (xr2, t1, 0x10);      //xr2: x19 ,x18 ,x17 ,x16
	    S32LDD (xr3, t1, 0x20);      //xr3: x35 ,x34 ,x33 ,x32
	    S32LDD (xr4, t1, 0x30);      //xr4: x51 ,x50 ,x49 ,x48

	    S32LDI (xr5, t3, 0x4);       //xr5: x3  ,x2  ,x1  ,x0
	    S32LDD (xr6, t3, 0x10);      //xr6: x19 ,x18 ,x17 ,x16
	    S32LDD (xr7, t3, 0x20);      //xr7: x35 ,x34 ,x33 ,x32
	    S32LDD (xr8, t3, 0x30);      //xr8: x51 ,x50 ,x49 ,x48

	    S32SFL(xr2,xr2,xr1,xr1,ptn0); //xr2:x19,x3,x18,x2 xr1:x17,x1,x16,x0
	    S32SFL(xr4,xr4,xr3,xr3,ptn0); //xr4:x51,x35,x50,x34 xr3:x49,x33,x48,x32
	    S32SFL(xr4,xr4,xr2,xr2,ptn3); //xr4:x51,x35,x19,x3 xr2:x50,x34,x18,x2
	    S32SFL(xr3,xr3,xr1,xr1,ptn3); //xr3:x49,x33,x17,x1 xr1:x48,x32,x16,x0

	    S32SFL(xr6,xr6,xr5,xr5,ptn0); //xr6:x19,x3,x18,x2 xr5:x17,x1,x16,x0
	    S32SFL(xr8,xr8,xr7,xr7,ptn0); //xr8:x51,x35,x50,x34 xr7:x49,x33,x48,x32
	    S32SFL(xr8,xr8,xr6,xr6,ptn3); //xr8:x51,x35,x19,x3 xr6:x50,x34,x18,x2
	    S32SFL(xr7,xr7,xr5,xr5,ptn3); //xr7:x49,x33,x17,x1 xr5:x48,x32,x16,x0

	    S32SDIVR(xr1, t2, strd, 0x0);
	    S32SDIVR(xr3, t2, strd, 0x0);
	    S32SDIVR(xr2, t2, strd, 0x0);
	    S32SDIVR(xr4, t2, strd, 0x0);

	    S32SDIVR(xr5, t4, strd, 0x0);
	    S32SDIVR(xr7, t4, strd, 0x0);
	    S32SDIVR(xr6, t4, strd, 0x0);
	    S32SDIVR(xr8, t4, strd, 0x0);
	  }
	}
#endif
#ifdef JZC_PMON_P1
	PMON_OFF(gp2);
#endif
	gp1_chain_ptr[0]=TCSM1_PADDR(dout_yptr[0]);
	gp1_chain_ptr[1]=(current_picture_ptr[0]);
	gp1_chain_ptr[4]=TCSM1_PADDR(dout_cptr[0]);
	gp1_chain_ptr[5]=(current_picture_ptr[1]);

#ifdef JZC_ROTA90_OPT
	rota_mx = dFRM->mb_height - 1 - last_mb_y;
	rota_picture_ptr[0] = dFRM->rota_current_picture.ptr[0] + (last_mb_x*dFRM->mb_height*256 + rota_mx*256);
	rota_picture_ptr[1] = dFRM->rota_current_picture.ptr[1] + (last_mb_x*dFRM->mb_height*128 + rota_mx*128);
#endif

   	uint32_t src_ptr,dst_ptr,src_cptr,dst_cptr;
#ifdef JZC_ROTA90_OPT
	gp1_chain_ptr[8]=TCSM1_PADDR(rota_buf1);
	gp1_chain_ptr[9]=(rota_picture_ptr[0]);
	gp1_chain_ptr[10]=GP_STRD(256,GP_FRM_NML,256);
	gp1_chain_ptr[11]=GP_UNIT(GP_TAG_LK,256,256);

	gp1_chain_ptr[12]=TCSM1_PADDR(rota_buf2);
	gp1_chain_ptr[13]=(rota_picture_ptr[1]);
	gp1_chain_ptr[14]=GP_STRD(128,GP_FRM_NML,128);
	gp1_chain_ptr[15]=GP_UNIT(GP_TAG_LK,128,128);

	gp1_chain_ptr[16] = TCSM0_PADDR((int)tcsm0_gp1_poll_end);
	gp1_chain_ptr[17] = TCSM1_PADDR((int)tcsm1_gp1_poll_end);
	gp1_chain_ptr[18] = GP_STRD(4,GP_FRM_NML,4);
	gp1_chain_ptr[19] = GP_UNIT(GP_TAG_UL,4,4);
#else
	gp1_chain_ptr[8] = TCSM0_PADDR((int)tcsm0_gp1_poll_end);
	gp1_chain_ptr[9] = TCSM1_PADDR((int)tcsm1_gp1_poll_end);
	gp1_chain_ptr[10] = GP_STRD(4,GP_FRM_NML,4);
	gp1_chain_ptr[11] = GP_UNIT(GP_TAG_UL,4,4);
#endif
	set_gp1_dha(TCSM1_PADDR(gp1_chain_ptr));
#ifdef JZC_PMON_P1
	PMON_ON(gp1);
#endif
	//poll_gp1_end();
	while(*tcsm1_gp1_poll_end);
	*tcsm1_gp1_poll_end = 1;

#ifdef JZC_PMON_P1
	PMON_OFF(gp1);
#endif
	set_gp1_dcs();
      }//if(count)
    last_mb_y=dMB_L->mb_y;
    last_mb_x=dMB_L->mb_x;

    //poll_gp0_end();
    while(*tcsm1_gp0_poll_end);
    *tcsm1_gp0_poll_end = 1;

    (*mbnum_rp)++;
    *addr_rp += dMB->next_dma_len;

    if((int)(*addr_rp)>=(TCSM0_PADDR(TCSM0_END)-TASK_BUF_LEN))
      *addr_rp=TCSM0_PADDR(TCSM0_TASK_FIFO);
    ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=*addr_rp;

#ifdef JZC_PMON_P1
    PMON_ON(gp0);
#endif
    while(*mbnum_wp<=*mbnum_rp+2);//wait until the next next mb is ready

#ifdef JZC_PMON_P1
    PMON_OFF(gp0);
#endif

    gp0_chain_ptr[0]=addr_rp[0];
    gp0_chain_ptr[1]=TCSM1_PADDR(dMB_L);
    gp0_chain_ptr[3]=GP_UNIT(GP_TAG_LK,64,dMB_N->next_dma_len);

    gp0_chain_ptr[4] = TCSM0_PADDR(TCSM0_GP0_POLL_END);
    gp0_chain_ptr[5] = TCSM1_PADDR(TCSM1_GP0_POLL_END);
    gp0_chain_ptr[6] = GP_STRD(4,GP_FRM_NML,4);
    gp0_chain_ptr[7] = GP_UNIT(GP_TAG_UL,4,4);

    set_gp0_dcs();
    count ++;
    //last_mc_flag = mc_flag;
    XCHG3(dMB_L,dMB,dMB_N,xchg_tmp);
    XCHG3(dout_yptr[0],dout_yptr[1],dout_yptr[2],xchg_tmp);
    XCHG3(dout_cptr[0],dout_cptr[1],dout_cptr[2],xchg_tmp);

    motion_dha = next_motion_dha;
    mau_reg_ptr = next_mau_reg_ptr;
    msrc_buf = next_msrc_buf;
    if(fifo_index == VC1_FIFO_DEP){
      next_motion_dha=TCSM1_MOTION_DHA;
      next_msrc_buf = TCSM1_MSRC_BUF;
      next_mau_reg_ptr = VMAU_CHN_BASE;
      fifo_index = 0;
    }else{
      next_motion_dha +=MC_CHN_ONELEN;
      next_msrc_buf += MSRC_ONE_LEN;
      next_mau_reg_ptr = (uint32_t)next_mau_reg_ptr+VMAU_CHN_ONELEN;
    }
    fifo_index++;
#ifdef JZC_ROTA90_OPT
    XCHG2(rota_buf1,rota_buf3,xchg_tmp);
    XCHG2(rota_buf2,rota_buf4,xchg_tmp);
#endif
    XCHG2(gp1_chain_ptr,gp1_chain_ptr1,xchg_tmp);
  }
  *(uint32_t *)motion_dha = TDD_HEAD(1,/*vld*/
				     0,/*lk*/
				     SubPel[VC1_QPEL]-1,/*ch1pel*/
				     SubPel[VC1_QPEL]-1,/*ch2pel*/ 
				     TDD_POS_SPEC,/*posmd*/
				     TDD_MV_SPEC,/*mvmd*/ 
				     0,/*tkn*/
				     0,/*mby*/
				     0/*mbx*/);

  while(*(volatile uint32_t *)VMAU_END_FLAG == 0);
  while(*tcsm1_gp0_poll_end);
  poll_gp1_end();
#ifdef JZC_PMON_P1
  pmon_ptr[0]=mc_cfg_pmon_val_useless_insn;
  pmon_ptr[1]=mc_polling_pmon_val_useless_insn;
  pmon_ptr[2]=idct_pmon_val_useless_insn;
  pmon_ptr[3]=gp0_pmon_val_useless_insn;
  pmon_ptr[4]=gp1_pmon_val_useless_insn;
  pmon_ptr[5]=gp2_pmon_val_useless_insn;
 
  pmon_ptr[6]=mc_cfg_pmon_val_piperdy;
  pmon_ptr[7]=mc_polling_pmon_val_piperdy;
  pmon_ptr[8]=idct_pmon_val_piperdy;
  pmon_ptr[9]=gp0_pmon_val_piperdy;
  pmon_ptr[10]=gp1_pmon_val_piperdy;
  pmon_ptr[11]=gp2_pmon_val_piperdy;

  pmon_ptr[12]=mc_cfg_pmon_val_cclk;
  pmon_ptr[13]=mc_polling_pmon_val_cclk;
  pmon_ptr[14]=idct_pmon_val_cclk;
  pmon_ptr[15]=gp0_pmon_val_cclk;
  pmon_ptr[16]=gp1_pmon_val_cclk;
  pmon_ptr[17]=gp2_pmon_val_cclk;
#endif

  *((volatile int *)TCSM0_PADDR(TCSM0_P1_TASK_DONE)) = 0x1;
  i_nop;
  i_nop;
  i_wait();
}
