.file	1 "vc1_p1.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vc1_mc_1mv_hw.constprop.1
.type	vc1_mc_1mv_hw.constprop.1, @function
vc1_mc_1mv_hw.constprop.1:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(dFRM)
bne	$4,$0,$L3
lw	$14,%lo(dFRM)($2)

lw	$2,20($14)
beq	$2,$0,$L26
nop

$L3:
lui	$2,%hi(dMB)
lw	$12,%lo(dMB)($2)
addiu	$2,$4,10
sll	$2,$2,2
addu	$2,$12,$2
lh	$9,4($2)
sll	$2,$4,2
addu	$2,$12,$2
lh	$15,46($2)
andi	$8,$9,0x3
xori	$11,$8,0x3
sltu	$11,$11,1
addu	$11,$9,$11
sra	$11,$11,1
andi	$2,$15,0x3
xori	$10,$2,0x3
sltu	$10,$10,1
addu	$10,$15,$10
lbu	$3,9($14)
beq	$3,$0,$L5
sra	$10,$10,1

bltz	$11,$L7
andi	$3,$11,0x1

subu	$3,$0,$3
$L7:
addu	$11,$11,$3
bltz	$10,$L9
andi	$3,$10,0x1

subu	$3,$0,$3
$L9:
addu	$10,$10,$3
$L5:
lbu	$3,10($14)
bne	$3,$0,$L25
sll	$2,$2,2

andi	$13,$15,0x2
sll	$2,$13,2
andi	$8,$9,0x2
or	$13,$2,$8
$L11:
lw	$2,0($6)
sll	$2,$2,3
addu	$2,$5,$2
andi	$9,$9,0xffff
sll	$15,$15,16
or	$9,$9,$15
sw	$9,0($2)
lw	$2,0($6)
sll	$2,$2,3
addu	$2,$5,$2
andi	$4,$4,0x1
sll	$3,$4,30
lbu	$9,8($14)
andi	$9,$9,0x1
sll	$9,$9,27
lbu	$7,25($12)
li	$4,67108864			# 0x4000000
xori	$7,$7,0x4
move	$8,$0
movz	$8,$4,$7
move	$7,$8
li	$8,50331648			# 0x3000000
addiu	$8,$8,240
or	$8,$9,$8
or	$8,$8,$3
lbu	$9,10($14)
li	$15,786432			# 0xc0000
movn	$15,$0,$9
or	$8,$8,$13
or	$7,$8,$7
or	$7,$7,$15
sw	$7,4($2)
lw	$7,0($6)
addiu	$7,$7,1
sw	$7,0($6)
sll	$7,$7,3
addu	$7,$5,$7
andi	$2,$11,0xffff
sll	$8,$10,16
or	$2,$2,$8
sw	$2,0($7)
lw	$2,0($6)
sll	$2,$2,3
addu	$5,$5,$2
lbu	$7,8($14)
andi	$7,$7,0x1
sll	$8,$7,27
lbu	$2,25($12)
xori	$2,$2,0x4
move	$9,$0
movz	$9,$4,$2
li	$4,33619968			# 0x2010000
addiu	$4,$4,240
or	$7,$8,$4
or	$4,$7,$3
andi	$3,$11,0x3
sll	$3,$3,1
or	$3,$4,$3
andi	$2,$10,0x3
sll	$2,$2,21
or	$2,$3,$2
or	$2,$2,$9
sw	$2,4($5)
lw	$2,0($6)
addiu	$2,$2,1
sw	$2,0($6)
lw	$2,12($12)
li	$3,603979776			# 0x24000000
or	$2,$2,$3
sw	$2,12($12)
$L26:
j	$31
nop

$L25:
j	$L11
or	$13,$2,$8

.set	macro
.set	reorder
.end	vc1_mc_1mv_hw.constprop.1
.size	vc1_mc_1mv_hw.constprop.1, .-vc1_mc_1mv_hw.constprop.1
.section	.p1_main,"ax",@progbits
.align	2
.globl	main
.set	nomips16
.set	nomicromips
.ent	main
.type	main, @function
main:
.frame	$sp,168,$31		# vars= 112, regs= 10/0, args= 16, gp= 0
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
la	$sp, 0xF400BFFC
addiu	$sp,$sp,-168
sw	$31,164($sp)
sw	$fp,160($sp)
sw	$23,156($sp)
sw	$22,152($sp)
sw	$21,148($sp)
sw	$20,144($sp)
sw	$19,140($sp)
sw	$18,136($sp)
sw	$17,132($sp)
sw	$16,128($sp)
li	$2,3			# 0x3
#APP
# 53 "vc1_p1.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
li	$2,-201326592			# 0xfffffffff4000000
li	$3,1			# 0x1
sw	$3,19904($2)
li	$3,321585152			# 0x132b0000
sw	$0,12($3)
sw	$0,19900($2)
sw	$0,8($3)
li	$4,134479872			# 0x8040000
addiu	$4,$4,512
sw	$4,16($sp)
li	$4,151584768			# 0x9090000
addiu	$4,$4,2056
sw	$4,20($sp)
li	$4,185270272			# 0xb0b0000
addiu	$4,$4,2570
sw	$4,24($sp)
li	$4,218955776			# 0xd0d0000
addiu	$4,$4,3084
sw	$4,28($sp)
li	$4,252641280			# 0xf0f0000
addiu	$4,$4,3598
sw	$4,32($sp)
li	$4,286326784			# 0x11110000
addiu	$4,$4,4112
sw	$4,36($sp)
li	$4,320012288			# 0x13130000
addiu	$4,$4,4626
sw	$4,40($sp)
li	$4,353697792			# 0x15150000
addiu	$4,$4,5140
sw	$4,44($sp)
sw	$0,16388($2)
addiu	$3,$3,8192
sw	$3,16392($2)
addiu	$3,$2,16416
lui	$4,%hi(dFRM)
sw	$3,%lo(dFRM)($4)
lui	$17,%hi(dMB)
addiu	$3,$2,17308
sw	$3,%lo(dMB)($17)
li	$3,4194304			# 0x400000
addiu	$3,$3,64
sw	$3,19716($2)
li	$3,321650688			# 0x132c0000
addiu	$3,$3,19708
li	$4,320929792			# 0x13210000
sw	$3,0($4)
li	$4,16777216			# 0x1000000
addiu	$4,$4,256
sw	$4,19748($2)
sw	$4,19752($2)
li	$3,8388608			# 0x800000
addiu	$3,$3,128
sw	$3,19764($2)
sw	$3,19768($2)
sw	$4,19828($2)
sw	$4,19832($2)
sw	$3,19844($2)
sw	$3,19848($2)
li	$4,-201326592			# 0xfffffffff4000000
$L190:
lw	$3,16384($4)
lw	$2,16388($4)
addiu	$2,$2,2
slt	$2,$2,$3
beq	$2,$0,$L190
lw	$2,16392($4)
sw	$2,19708($4)
li	$2,321650688			# 0x132c0000
addiu	$2,$2,17308
sw	$2,19712($4)
lhu	$2,16400($4)
li	$3,-2143289344			# 0xffffffff80400000
or	$2,$2,$3
sw	$2,19720($4)
li	$3,1			# 0x1
li	$2,320929792			# 0x13210000
sw	$3,4($2)
li	$3,320929792			# 0x13210000
addiu	$4,$3,4
$L191:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L191
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

lw	$3,16388($2)
addiu	$3,$3,1
sw	$3,16388($2)
lw	$3,16392($2)
lw	$5,16400($2)
addu	$3,$3,$5
sw	$3,16392($2)
lw	$3,16392($2)
li	$5,321585152			# 0x132b0000
sw	$3,4($5)
lw	$3,16392($2)
sw	$3,19708($2)
li	$3,321650688			# 0x132c0000
addiu	$6,$3,18136
sw	$6,19712($2)
lhu	$6,17308($2)
li	$7,4194304			# 0x400000
or	$6,$6,$7
sw	$6,19720($2)
addiu	$5,$5,12
sw	$5,19724($2)
addiu	$3,$3,19904
sw	$3,19728($2)
li	$3,262144			# 0x40000
addiu	$3,$3,4
sw	$3,19732($2)
li	$3,-2147221504			# 0xffffffff80040000
addiu	$3,$3,4
sw	$3,19736($2)
li	$5,1			# 0x1
sw	$5,0($4)
lbu	$3,16421($2)
.set	noreorder
.set	nomacro
beq	$3,$5,$L30
addiu	$2,$2,16416
.set	macro
.set	reorder

li	$4,3			# 0x3
beq	$3,$4,$L174
li	$2,321388544			# 0x13280000
$L201:
ori	$2,$2,0x8000
lw	$3,16($sp)
sw	$3,0($2)
addiu	$3,$2,4
lw	$4,20($sp)
sw	$4,4($2)
lw	$2,24($sp)
sw	$2,4($3)
lw	$2,28($sp)
sw	$2,8($3)
li	$2,-201326592			# 0xfffffffff4000000
sw	$0,18964($2)
li	$3,321650688			# 0x132c0000
addiu	$4,$3,19412
li	$2,321191936			# 0x13250000
addiu	$5,$2,88
#APP
# 199 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
lui	$5,%hi(dFRM)
lw	$4,%lo(dFRM)($5)
lbu	$4,10($4)
li	$5,196608			# 0x30000
movn	$5,$0,$4
move	$4,$5
addiu	$5,$2,48
#APP
# 200 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$3,18965
addiu	$2,$2,84
#APP
# 201 "vc1_p1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
sw	$0,68($sp)
$L32:
li	$18,-201326592			# 0xfffffffff4000000
li	$2,1			# 0x1
sw	$2,19704($18)
lui	$11,%hi(dFRM)
lw	$5,%lo(dFRM)($11)
lw	$4,36($5)
addiu	$4,$4,-768
lui	$10,%hi(current_picture_ptr)
sw	$4,%lo(current_picture_ptr)($10)
lw	$6,40($5)
addiu	$6,$6,-384
addiu	$13,$10,%lo(current_picture_ptr)
sw	$6,4($13)
lw	$9,%lo(dMB)($17)
lh	$2,4($9)
.set	noreorder
.set	nomacro
blez	$2,$L108
addiu	$12,$18,26052
.set	macro
.set	reorder

sw	$12,60($sp)
addiu	$21,$18,26436
sw	$21,56($sp)
addiu	$2,$18,26820
sw	$2,84($sp)
addiu	$15,$18,26308
addiu	$25,$18,26692
addiu	$3,$18,27076
sw	$3,80($sp)
addiu	$20,$18,19452
addiu	$7,$18,19416
sw	$7,72($sp)
addiu	$11,$18,20676
sw	$11,64($sp)
addiu	$16,$18,19908
addiu	$21,$18,19020
addiu	$23,$18,18964
li	$24,2			# 0x2
addiu	$22,$18,18136
addiu	$14,$18,16480
addiu	$12,$18,19820
sw	$12,76($sp)
addiu	$18,$18,19740
move	$fp,$0
addiu	$4,$4,256
sw	$4,%lo(current_picture_ptr)($10)
addiu	$6,$6,128
sw	$6,4($13)
sw	$21,88($sp)
sw	$0,0($21)
sw	$0,48($sp)
lbu	$3,5($5)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L175
addiu	$19,$23,4
.set	macro
.set	reorder

$L34:
lw	$2,4($5)
li	$12,-16777216			# 0xffffffffff000000
ori	$12,$12,0xff00
and	$2,$2,$12
li	$3,768			# 0x300
beq	$2,$3,$L176
$L114:
move	$8,$0
$L197:
li	$2,8			# 0x8
move	$3,$0
addiu	$4,$23,8
$L37:
addu	$3,$19,$3
li	$9,-805306368			# 0xffffffffd0000000
ori	$9,$9,0x54
sw	$9,0($3)
andi	$3,$21,0xffff
li	$5,321650688			# 0x132c0000
or	$3,$3,$5
sw	$3,0($4)
addu	$2,$19,$2
li	$3,-402653184			# 0xffffffffe8000000
ori	$3,$3,0xffff
sw	$3,0($2)
lw	$6,%lo(dMB)($17)
lbu	$3,2($6)
li	$2,-880803840			# 0xffffffffcb800000
or	$2,$3,$2
lbu	$3,3($6)
sll	$3,$3,8
or	$2,$2,$3
or	$8,$2,$8
sw	$8,0($23)
lw	$11,68($sp)
.set	noreorder
.set	nomacro
beq	$11,$0,$L79
addiu	$2,$16,-16
.set	macro
.set	reorder

addiu	$3,$6,24
$L80:
addiu	$3,$3,-16
li	$8,3			# 0x3
move	$7,$0
li	$9,8			# 0x8
li	$11,6			# 0x6
addu	$4,$6,$7
lbu	$5,6($4)
.set	noreorder
.set	nomacro
beq	$5,$0,$L177
move	$4,$0
.set	macro
.set	reorder

$L143:
#APP
# 298 "vc1_p1.c" 1
.word	0b01110000011000000001000001010100	#S32LDI XR1,$3,16
# 0 "" 2
# 299 "vc1_p1.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 300 "vc1_p1.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 301 "vc1_p1.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 302 "vc1_p1.c" 1
.word	0b01110000010000000001000001010101	#S32SDI XR1,$2,16
# 0 "" 2
# 303 "vc1_p1.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 304 "vc1_p1.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 305 "vc1_p1.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
#NO_APP
addiu	$4,$4,1
lw	$6,%lo(dMB)($17)
addu	$5,$6,$7
lbu	$5,6($5)
slt	$12,$4,$5
bne	$12,$0,$L143
lw	$4,16($6)
and	$4,$8,$4
.set	noreorder
.set	nomacro
beq	$4,$8,$L178
slt	$4,$5,8
.set	macro
.set	reorder

$L83:
addiu	$7,$7,1
$L188:
.set	noreorder
.set	nomacro
beq	$7,$11,$L85
sll	$8,$8,2
.set	macro
.set	reorder

$L179:
lw	$6,%lo(dMB)($17)
addu	$4,$6,$7
lbu	$5,6($4)
.set	noreorder
.set	nomacro
bne	$5,$0,$L143
move	$4,$0
.set	macro
.set	reorder

$L177:
lw	$4,16($6)
and	$4,$8,$4
beq	$4,$8,$L84
addiu	$7,$7,1
.set	noreorder
.set	nomacro
bne	$7,$11,$L179
sll	$8,$8,2
.set	macro
.set	reorder

$L85:
lw	$3,%lo(dMB)($17)
lw	$4,12($3)
sw	$4,0($20)
lw	$4,20($3)
sw	$4,4($20)
andi	$4,$16,0xffff
li	$5,321650688			# 0x132c0000
or	$4,$4,$5
sw	$4,8($20)
subu	$2,$2,$16
addiu	$2,$2,16
sh	$2,16($20)
li	$2,1			# 0x1
sb	$2,18($20)
lw	$2,16($3)
.set	noreorder
.set	nomacro
beq	$fp,$0,$L95
sw	$2,20($20)
.set	macro
.set	reorder

li	$3,-201326592			# 0xfffffffff4000000
$L152:
lw	$2,19704($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L152
lw	$12,68($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$12,$0,$L192
lhu	$4,56($sp)
.set	macro
.set	reorder

li	$2,321388544			# 0x13280000
ori	$2,$2,0x8000
lw	$3,16($sp)
sw	$3,0($2)
lw	$3,20($sp)
sw	$3,4($2)
lw	$3,24($sp)
sw	$3,8($2)
lw	$3,28($sp)
sw	$3,12($2)
lw	$7,72($sp)
lw	$3,4($7)
li	$4,65536			# 0x10000
and	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L192
lhu	$4,56($sp)
.set	macro
.set	reorder

andi	$3,$3,0xf000
srl	$3,$3,14
sll	$4,$3,2
addu	$2,$4,$2
addiu	$3,$3,4
sll	$3,$3,2
addu	$3,$sp,$3
lw	$3,16($3)
sw	$3,0($2)
lhu	$4,56($sp)
$L192:
li	$2,321650688			# 0x132c0000
or	$4,$4,$2
li	$3,321388544			# 0x13280000
addiu	$5,$3,104
#APP
# 368 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
andi	$4,$25,0xffff
or	$4,$4,$2
addiu	$5,$3,108
#APP
# 369 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$4,8
addiu	$5,$3,112
#APP
# 370 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
lhu	$4,72($sp)
or	$4,$4,$2
addiu	$5,$3,12
#APP
# 371 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,19704
addiu	$5,$3,88
#APP
# 372 "vc1_p1.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
li	$4,-201326592			# 0xfffffffff4000000
sw	$0,19704($4)
li	$4,1			# 0x1
addiu	$3,$3,64
#APP
# 374 "vc1_p1.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$fp,$4,$L171
lhu	$3,60($sp)
.set	macro
.set	reorder

or	$3,$3,$2
sw	$3,0($18)
lw	$3,%lo(current_picture_ptr)($10)
sw	$3,4($18)
andi	$3,$15,0xffff
or	$3,$3,$2
sw	$3,16($18)
lw	$3,4($13)
sw	$3,20($18)
li	$3,321585152			# 0x132b0000
addiu	$3,$3,8
sw	$3,32($18)
addiu	$3,$2,19900
sw	$3,36($18)
li	$3,262144			# 0x40000
addiu	$3,$3,4
sw	$3,40($18)
li	$3,-2147221504			# 0xffffffff80040000
addiu	$3,$3,4
sw	$3,44($18)
andi	$3,$18,0xffff
or	$2,$3,$2
li	$3,320995328			# 0x13220000
sw	$2,0($3)
li	$2,-201326592			# 0xfffffffff4000000
addiu	$4,$2,19900
$L193:
lw	$3,19900($2)
bne	$3,$0,$L193
li	$2,1			# 0x1
sw	$2,0($4)
li	$3,320995328			# 0x13220000
sw	$2,4($3)
$L171:
lw	$3,%lo(dMB)($17)
$L95:
li	$2,-201326592			# 0xfffffffff4000000
$L194:
lw	$4,19904($2)
.set	noreorder
.set	nomacro
bne	$4,$0,$L194
li	$4,1			# 0x1
.set	macro
.set	reorder

sw	$4,19904($2)
lw	$4,16388($2)
addiu	$4,$4,1
sw	$4,16388($2)
lw	$4,16392($2)
lhu	$5,0($3)
addu	$4,$4,$5
sw	$4,16392($2)
lw	$4,16392($2)
li	$5,321585152			# 0x132b0000
addiu	$6,$5,15556
sltu	$4,$4,$6
.set	noreorder
.set	nomacro
bne	$4,$0,$L96
addiu	$5,$5,8192
.set	macro
.set	reorder

sw	$5,16392($2)
$L96:
li	$2,-201326592			# 0xfffffffff4000000
lw	$4,16392($2)
li	$2,321585152			# 0x132b0000
sw	$4,4($2)
li	$2,-201326592			# 0xfffffffff4000000
$L195:
lw	$5,16384($2)
lw	$4,16388($2)
addiu	$4,$4,2
slt	$4,$4,$5
beq	$4,$0,$L195
lw	$4,16392($2)
sw	$4,19708($2)
andi	$5,$14,0xffff
li	$4,321650688			# 0x132c0000
or	$5,$5,$4
sw	$5,19712($2)
lhu	$5,0($22)
li	$6,4194304			# 0x400000
or	$5,$5,$6
sw	$5,19720($2)
li	$5,321585152			# 0x132b0000
addiu	$5,$5,12
sw	$5,19724($2)
addiu	$4,$4,19904
sw	$4,19728($2)
li	$4,262144			# 0x40000
addiu	$4,$4,4
sw	$4,19732($2)
li	$4,-2147221504			# 0xffffffff80040000
addiu	$4,$4,4
sw	$4,19736($2)
li	$5,1			# 0x1
li	$4,320929792			# 0x13210000
sw	$5,4($4)
addiu	$fp,$fp,1
li	$4,8			# 0x8
.set	noreorder
.set	nomacro
beq	$24,$4,$L120
sw	$22,%lo(dMB)($17)
.set	macro
.set	reorder

addiu	$2,$21,56
lw	$9,64($sp)
addiu	$7,$9,768
addiu	$8,$20,36
lh	$4,4($22)
.set	noreorder
.set	nomacro
blez	$4,$L33
addiu	$24,$24,1
.set	macro
.set	reorder

$L180:
lw	$4,%lo(current_picture_ptr)($10)
lw	$6,4($13)
lui	$11,%hi(dFRM)
lw	$5,%lo(dFRM)($11)
sw	$20,72($sp)
move	$9,$22
lw	$11,84($sp)
lw	$12,60($sp)
sw	$12,84($sp)
lw	$12,56($sp)
sw	$12,60($sp)
sw	$11,56($sp)
lw	$11,80($sp)
sw	$15,80($sp)
move	$15,$25
move	$25,$11
move	$20,$8
lw	$16,64($sp)
sw	$7,64($sp)
move	$23,$21
move	$21,$2
move	$22,$14
move	$14,$3
move	$2,$18
lw	$18,76($sp)
sw	$2,76($sp)
addiu	$4,$4,256
sw	$4,%lo(current_picture_ptr)($10)
addiu	$6,$6,128
sw	$6,4($13)
sw	$21,88($sp)
sw	$0,0($21)
sw	$0,48($sp)
lbu	$3,5($5)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$3,$2,$L34
addiu	$19,$23,4
.set	macro
.set	reorder

$L175:
lbu	$2,24($9)
bne	$2,$0,$L35
lbu	$2,32($9)
.set	noreorder
.set	nomacro
bne	$2,$0,$L196
move	$4,$0
.set	macro
.set	reorder

lbu	$2,26($9)
bne	$2,$0,$L114
$L196:
move	$5,$19
addiu	$6,$sp,48
sw	$10,104($sp)
sw	$13,108($sp)
sw	$14,112($sp)
sw	$15,120($sp)
sw	$24,124($sp)
.set	noreorder
.set	nomacro
jal	vc1_mc_1mv_hw.constprop.1
sw	$25,116($sp)
.set	macro
.set	reorder

lw	$5,48($sp)
sll	$3,$5,3
addiu	$4,$3,4
addiu	$2,$3,8
andi	$5,$5,0x7f
sll	$8,$5,16
addu	$4,$19,$4
lw	$10,104($sp)
lw	$13,108($sp)
lw	$14,112($sp)
lw	$15,120($sp)
lw	$24,124($sp)
.set	noreorder
.set	nomacro
j	$L37
lw	$25,116($sp)
.set	macro
.set	reorder

$L178:
beq	$4,$0,$L83
$L84:
#APP
# 309 "vc1_p1.c" 1
.word	0b01110000010000000001000000010101	#S32SDI XR0,$2,16
# 0 "" 2
# 310 "vc1_p1.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 311 "vc1_p1.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 312 "vc1_p1.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
#NO_APP
addiu	$5,$5,1
bne	$5,$9,$L84
.set	noreorder
.set	nomacro
j	$L188
addiu	$7,$7,1
.set	macro
.set	reorder

$L79:
.set	noreorder
.set	nomacro
j	$L80
addiu	$3,$6,60
.set	macro
.set	reorder

$L120:
li	$24,1			# 0x1
addiu	$8,$2,19416
addiu	$7,$2,19908
lh	$4,4($22)
.set	noreorder
.set	nomacro
bgtz	$4,$L180
addiu	$2,$2,18964
.set	macro
.set	reorder

$L33:
li	$2,-1954545664			# 0xffffffff8b800000
lw	$21,88($sp)
sw	$2,0($21)
li	$3,-201326592			# 0xfffffffff4000000
$L100:
lw	$2,19704($3)
beq	$2,$0,$L100
li	$3,-201326592			# 0xfffffffff4000000
$L142:
lw	$2,19904($3)
bne	$2,$0,$L142
li	$3,320995328			# 0x13220000
$L102:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L102
li	$2,321585152			# 0x132b0000
.set	macro
.set	reorder

li	$3,1			# 0x1
sw	$3,0($2)
#APP
# 602 "vc1_p1.c" 1
nop	#i_nop
# 0 "" 2
# 603 "vc1_p1.c" 1
nop	#i_nop
# 0 "" 2
# 604 "vc1_p1.c" 1
wait	#i_wait
# 0 "" 2
#NO_APP
move	$2,$0
lw	$31,164($sp)
lw	$fp,160($sp)
lw	$23,156($sp)
lw	$22,152($sp)
lw	$21,148($sp)
lw	$20,144($sp)
lw	$19,140($sp)
lw	$18,136($sp)
lw	$17,132($sp)
lw	$16,128($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,168
.set	macro
.set	reorder

$L176:
lbu	$2,37($9)
.set	noreorder
.set	nomacro
bne	$2,$0,$L197
move	$8,$0
.set	macro
.set	reorder

lbu	$2,11($5)
beq	$2,$0,$L54
lbu	$2,25($9)
sw	$2,100($sp)
li	$2,4			# 0x4
sb	$2,25($9)
$L54:
lbu	$2,33($9)
.set	noreorder
.set	nomacro
bne	$2,$0,$L55
li	$2,2			# 0x2
.set	macro
.set	reorder

lbu	$4,36($9)
beq	$4,$2,$L55
lbu	$2,11($5)
bne	$2,$0,$L181
$L77:
sltu	$4,$4,1
move	$5,$19
addiu	$6,$sp,48
sw	$10,104($sp)
sw	$13,108($sp)
sw	$14,112($sp)
sw	$15,120($sp)
sw	$24,124($sp)
.set	noreorder
.set	nomacro
jal	vc1_mc_1mv_hw.constprop.1
sw	$25,116($sp)
.set	macro
.set	reorder

lw	$25,116($sp)
lw	$24,124($sp)
lw	$15,120($sp)
lw	$14,112($sp)
lw	$13,108($sp)
lw	$10,104($sp)
$L58:
lui	$3,%hi(dFRM)
$L189:
lw	$2,%lo(dFRM)($3)
lbu	$2,11($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L198
lw	$5,48($sp)
.set	macro
.set	reorder

lw	$2,%lo(dMB)($17)
lw	$7,100($sp)
sb	$7,25($2)
lw	$5,48($sp)
$L198:
sll	$3,$5,3
addiu	$4,$3,4
addiu	$2,$3,8
andi	$5,$5,0x7f
sll	$8,$5,16
.set	noreorder
.set	nomacro
j	$L37
addu	$4,$19,$4
.set	macro
.set	reorder

$L35:
lw	$2,20($5)
sw	$2,92($sp)
addiu	$12,$9,44
move	$3,$0
move	$8,$0
move	$11,$0
sw	$21,96($sp)
$L44:
lbu	$2,32($9)
.set	noreorder
.set	nomacro
bne	$2,$0,$L199
lw	$4,92($sp)
.set	macro
.set	reorder

addu	$2,$9,$11
lbu	$2,26($2)
bne	$2,$0,$L39
$L199:
beq	$4,$0,$L39
lh	$2,0($12)
lbu	$4,10($5)
.set	noreorder
.set	nomacro
beq	$4,$0,$L40
lh	$3,2($12)
.set	macro
.set	reorder

andi	$4,$3,0x3
sll	$4,$4,2
andi	$6,$2,0x3
or	$4,$4,$6
$L41:
sll	$6,$8,3
addu	$7,$19,$6
andi	$2,$2,0xffff
sll	$3,$3,16
or	$3,$2,$3
sw	$3,0($7)
move	$6,$7
lbu	$2,8($5)
andi	$2,$2,0x1
sll	$2,$2,27
lbu	$3,25($9)
xori	$3,$3,0x4
li	$7,67108864			# 0x4000000
movn	$7,$0,$3
move	$3,$7
ori	$2,$2,0xa0
lbu	$7,10($5)
li	$21,786432			# 0xc0000
movn	$21,$0,$7
move	$7,$21
andi	$21,$11,0x2
sll	$21,$21,10
or	$2,$2,$21
andi	$21,$11,0x1
sll	$21,$21,1
sll	$21,$21,8
or	$2,$2,$21
or	$2,$2,$4
or	$3,$2,$3
or	$7,$3,$7
sw	$7,4($6)
addiu	$3,$8,1
lw	$2,12($9)
li	$4,67108864			# 0x4000000
or	$2,$2,$4
sw	$2,12($9)
move	$8,$3
$L39:
addiu	$11,$11,1
li	$7,4			# 0x4
.set	noreorder
.set	nomacro
bne	$11,$7,$L44
addiu	$12,$12,4
.set	macro
.set	reorder

lw	$21,96($sp)
sw	$3,48($sp)
sll	$6,$8,3
addiu	$2,$6,-4
addu	$2,$19,$2
lw	$3,0($2)
li	$4,50331648			# 0x3000000
or	$3,$3,$4
sw	$3,0($2)
lw	$11,92($sp)
.set	noreorder
.set	nomacro
beq	$11,$0,$L182
addiu	$4,$6,4
.set	macro
.set	reorder

lbu	$2,38($9)
.set	noreorder
.set	nomacro
beq	$2,$0,$L46
addu	$2,$19,$6
.set	macro
.set	reorder

sw	$0,0($2)
addiu	$4,$6,4
addu	$4,$19,$4
li	$2,240			# 0xf0
.set	noreorder
.set	nomacro
beq	$8,$0,$L183
sw	$2,0($4)
.set	macro
.set	reorder

addiu	$8,$8,1
.set	noreorder
.set	nomacro
j	$L169
sw	$8,48($sp)
.set	macro
.set	reorder

$L40:
andi	$4,$3,0x2
sll	$4,$4,2
andi	$6,$2,0x2
.set	noreorder
.set	nomacro
j	$L41
or	$4,$4,$6
.set	macro
.set	reorder

$L46:
lh	$3,40($9)
andi	$2,$3,0x3
xori	$2,$2,0x3
sltu	$2,$2,1
addu	$2,$3,$2
sra	$4,$2,1
lh	$3,42($9)
andi	$2,$3,0x3
xori	$2,$2,0x3
sltu	$2,$2,1
addu	$2,$3,$2
sra	$7,$2,1
lbu	$2,9($5)
.set	noreorder
.set	nomacro
beq	$2,$0,$L200
addu	$11,$19,$6
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$4,$L50
andi	$2,$4,0x1
.set	macro
.set	reorder

subu	$2,$0,$2
$L50:
addu	$4,$4,$2
.set	noreorder
.set	nomacro
bltz	$7,$L52
andi	$2,$7,0x1
.set	macro
.set	reorder

subu	$2,$0,$2
$L52:
addu	$7,$7,$2
addu	$11,$19,$6
$L200:
andi	$2,$4,0xffff
sll	$3,$7,16
or	$2,$2,$3
sw	$2,0($11)
move	$12,$11
lbu	$2,8($5)
andi	$2,$2,0x1
sll	$5,$2,27
lbu	$2,25($9)
xori	$2,$2,0x4
li	$3,67108864			# 0x4000000
move	$11,$0
movz	$11,$3,$2
li	$2,33619968			# 0x2010000
addiu	$2,$2,240
or	$3,$5,$2
andi	$2,$4,0x3
sll	$2,$2,1
or	$2,$3,$2
andi	$3,$7,0x3
sll	$3,$3,21
or	$2,$2,$3
or	$2,$2,$11
sw	$2,4($12)
addiu	$8,$8,1
sw	$8,48($sp)
lw	$2,12($9)
li	$3,536870912			# 0x20000000
or	$2,$2,$3
sw	$2,12($9)
$L169:
addiu	$3,$6,8
addiu	$4,$6,12
addiu	$2,$6,16
andi	$8,$8,0x7f
sll	$8,$8,16
.set	noreorder
.set	nomacro
j	$L37
addu	$4,$19,$4
.set	macro
.set	reorder

$L55:
lw	$2,20($5)
.set	noreorder
.set	nomacro
beq	$2,$0,$L184
lw	$4,%lo(dMB)($17)
.set	macro
.set	reorder

$L202:
lh	$8,44($4)
lh	$9,46($4)
andi	$11,$8,0x3
xori	$2,$11,0x3
sltu	$2,$2,1
addu	$2,$8,$2
sra	$6,$2,1
andi	$3,$9,0x3
xori	$2,$3,0x3
sltu	$2,$2,1
addu	$2,$9,$2
sra	$7,$2,1
lbu	$2,9($5)
beq	$2,$0,$L59
.set	noreorder
.set	nomacro
bltz	$6,$L61
andi	$2,$6,0x1
.set	macro
.set	reorder

subu	$2,$0,$2
$L61:
addu	$6,$6,$2
.set	noreorder
.set	nomacro
bltz	$7,$L63
andi	$2,$7,0x1
.set	macro
.set	reorder

subu	$2,$0,$2
$L63:
addu	$7,$7,$2
$L59:
lbu	$2,10($5)
.set	noreorder
.set	nomacro
bne	$2,$0,$L185
sll	$2,$3,2
.set	macro
.set	reorder

andi	$2,$9,0x2
sll	$2,$2,2
andi	$3,$8,0x2
or	$11,$2,$3
$L65:
andi	$2,$8,0xffff
sll	$3,$9,16
or	$2,$2,$3
sw	$2,4($23)
lbu	$2,8($5)
andi	$2,$2,0x1
sll	$8,$2,27
lbu	$2,25($4)
xori	$2,$2,0x4
li	$3,67108864			# 0x4000000
move	$9,$0
movz	$9,$3,$2
li	$3,-2147483648			# 0xffffffff80000000
ori	$3,$3,0xf0
or	$8,$8,$3
lbu	$3,10($5)
li	$2,786432			# 0xc0000
movn	$2,$0,$3
move	$3,$2
or	$2,$8,$11
or	$2,$2,$9
or	$2,$2,$3
sw	$2,8($23)
lh	$11,48($4)
lh	$12,50($4)
andi	$9,$11,0x3
sw	$9,92($sp)
xori	$2,$9,0x3
sltu	$2,$2,1
addu	$2,$11,$2
sra	$8,$2,1
andi	$3,$12,0x3
xori	$2,$3,0x3
sltu	$2,$2,1
addu	$2,$12,$2
sra	$9,$2,1
lbu	$2,9($5)
beq	$2,$0,$L68
.set	noreorder
.set	nomacro
bltz	$8,$L186
andi	$2,$8,0x1
.set	macro
.set	reorder

$L70:
addu	$8,$8,$2
.set	noreorder
.set	nomacro
bltz	$9,$L187
andi	$2,$9,0x1
.set	macro
.set	reorder

$L72:
addu	$9,$9,$2
$L68:
lbu	$2,10($5)
.set	noreorder
.set	nomacro
beq	$2,$0,$L73
andi	$2,$12,0x2
.set	macro
.set	reorder

sll	$2,$3,2
lw	$3,92($sp)
or	$2,$2,$3
sw	$2,92($sp)
$L74:
andi	$2,$11,0xffff
sll	$3,$12,16
or	$2,$2,$3
sw	$2,12($23)
lbu	$2,8($5)
andi	$2,$2,0x1
sll	$11,$2,27
lbu	$3,10($5)
li	$2,786432			# 0xc0000
movn	$2,$0,$3
move	$3,$2
li	$2,-1023410176			# 0xffffffffc3000000
addiu	$2,$2,240
or	$2,$11,$2
lw	$11,92($sp)
or	$2,$2,$11
or	$2,$2,$3
sw	$2,16($23)
andi	$2,$6,0xffff
sll	$3,$7,16
or	$2,$2,$3
sw	$2,20($23)
lbu	$2,8($5)
andi	$2,$2,0x1
sll	$11,$2,27
lbu	$2,25($4)
xori	$2,$2,0x4
li	$3,67108864			# 0x4000000
move	$12,$0
movz	$12,$3,$2
li	$2,-2147418112			# 0xffffffff80010000
addiu	$2,$2,240
or	$3,$11,$2
andi	$2,$6,0x3
sll	$2,$2,1
or	$2,$3,$2
andi	$3,$7,0x3
sll	$3,$3,21
or	$2,$2,$3
or	$2,$2,$12
sw	$2,24($23)
andi	$2,$8,0xffff
sll	$3,$9,16
or	$2,$2,$3
sw	$2,28($23)
lbu	$3,8($5)
andi	$3,$3,0x1
sll	$3,$3,27
li	$2,-1040121856			# 0xffffffffc2010000
addiu	$2,$2,240
or	$3,$3,$2
andi	$2,$8,0x3
sll	$2,$2,1
or	$3,$3,$2
andi	$2,$9,0x3
sll	$2,$2,21
or	$2,$3,$2
sw	$2,32($23)
li	$2,4			# 0x4
sw	$2,48($sp)
lw	$2,12($4)
li	$3,603979776			# 0x24000000
or	$2,$2,$3
.set	noreorder
.set	nomacro
j	$L58
sw	$2,12($4)
.set	macro
.set	reorder

$L183:
addiu	$2,$6,8
.set	noreorder
.set	nomacro
j	$L37
move	$3,$6
.set	macro
.set	reorder

$L181:
.set	noreorder
.set	nomacro
bne	$4,$0,$L77
lw	$12,100($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L77
sb	$12,25($9)
.set	macro
.set	reorder

$L185:
.set	noreorder
.set	nomacro
j	$L65
or	$11,$2,$11
.set	macro
.set	reorder

$L174:
lbu	$2,7($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L201
li	$2,321388544			# 0x13280000
.set	macro
.set	reorder

$L30:
li	$3,321388544			# 0x13280000
ori	$3,$3,0x8000
lw	$2,16($sp)
sw	$2,0($3)
lw	$4,20($sp)
sw	$4,4($3)
lw	$4,24($sp)
sw	$4,8($3)
lw	$4,28($sp)
sw	$4,12($3)
li	$4,-201326592			# 0xfffffffff4000000
lw	$2,17328($4)
andi	$2,$2,0x10
.set	noreorder
.set	nomacro
bne	$2,$0,$L105
li	$2,3			# 0x3
.set	macro
.set	reorder

li	$7,1			# 0x1
.set	noreorder
.set	nomacro
j	$L32
sw	$7,68($sp)
.set	macro
.set	reorder

$L73:
sll	$2,$2,2
andi	$3,$11,0x2
or	$3,$2,$3
.set	noreorder
.set	nomacro
j	$L74
sw	$3,92($sp)
.set	macro
.set	reorder

$L105:
lbu	$5,16422($4)
.set	noreorder
.set	nomacro
beq	$5,$2,$L107
li	$9,1			# 0x1
.set	macro
.set	reorder

lw	$2,19420($4)
andi	$2,$2,0xf
srl	$2,$2,2
sll	$4,$2,2
addu	$3,$4,$3
addiu	$2,$2,4
sll	$2,$2,2
addu	$2,$sp,$2
lw	$2,16($2)
sw	$2,0($3)
li	$7,1			# 0x1
.set	noreorder
.set	nomacro
j	$L32
sw	$7,68($sp)
.set	macro
.set	reorder

$L184:
lw	$2,28($5)
bne	$2,$0,$L202
.set	noreorder
.set	nomacro
j	$L189
lui	$3,%hi(dFRM)
.set	macro
.set	reorder

$L187:
.set	noreorder
.set	nomacro
j	$L72
subu	$2,$0,$2
.set	macro
.set	reorder

$L182:
addiu	$2,$6,8
andi	$8,$8,0x7f
sll	$8,$8,16
move	$3,$6
.set	noreorder
.set	nomacro
j	$L37
addu	$4,$19,$4
.set	macro
.set	reorder

$L108:
addiu	$18,$18,18964
.set	noreorder
.set	nomacro
j	$L33
sw	$18,88($sp)
.set	macro
.set	reorder

$L107:
.set	noreorder
.set	nomacro
j	$L32
sw	$9,68($sp)
.set	macro
.set	reorder

$L186:
.set	noreorder
.set	nomacro
j	$L70
subu	$2,$0,$2
.set	macro
.set	reorder

.end	main
.size	main, .-main

.comm	lutuv,256,4

.comm	luty,256,4

.comm	rota_buf4,4,4

.comm	rota_buf3,4,4

.comm	rota_buf2,4,4

.comm	rota_buf1,4,4

.comm	dMB,4,4

.comm	dFRM,4,4

.comm	current_picture_ptr,8,4

.comm	total_work,4,4

.comm	total_tran,4,4

.comm	motion_iwta,4,4

.comm	motion_doutc,4,4

.comm	motion_douty,4,4

.comm	motion_dsa,4,4

.comm	motion_dha,4,4

.comm	motion_buf,4,4
.globl	OFFTAB
.data
.align	2
.type	OFFTAB, @object
.size	OFFTAB, 16
OFFTAB:
.word	0
.word	4
.word	32
.word	36

.comm	tcsm1_base,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
