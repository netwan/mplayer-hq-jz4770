.file	1 "vc1_parser.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.vc1_split,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_split
.type	vc1_split, @function
vc1_split:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$6,$L5
move	$9,$0

li	$2,-1			# 0xffffffffffffffff
move	$3,$0
li	$8,-256			# 0xffffffffffffff00
b	$L4
li	$7,256			# 0x100

$L3:
addiu	$3,$3,1
$L12:
beq	$3,$6,$L5
nop

$L4:
addu	$4,$5,$3
sll	$2,$2,8
lbu	$4,0($4)
or	$2,$4,$2
and	$4,$2,$8
bne	$4,$7,$L3
addiu	$4,$2,-270

sltu	$4,$4,2
bne	$4,$0,$L6
nop

beq	$9,$0,$L12
addiu	$3,$3,1

addiu	$3,$3,-1
j	$31
addiu	$2,$3,-3

$L6:
addiu	$3,$3,1
bne	$3,$6,$L4
li	$9,1			# 0x1

$L5:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	vc1_split
.size	vc1_split, .-vc1_split
.section	.text.vc1_parse,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_parse
.type	vc1_parse, @function
vc1_parse:
.frame	$sp,88,$31		# vars= 24, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,176($4)
addiu	$sp,$sp,-88
.cprestore	16
andi	$2,$2,0x1
sw	$22,72($sp)
sw	$21,68($sp)
move	$22,$4
sw	$31,84($sp)
move	$21,$5
sw	$fp,80($sp)
sw	$23,76($sp)
sw	$20,64($sp)
sw	$19,60($sp)
sw	$18,56($sp)
sw	$17,52($sp)
sw	$16,48($sp)
sw	$6,96($sp)
sw	$7,100($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L14
lw	$4,0($4)
.set	macro
.set	reorder

lw	$3,108($sp)
move	$17,$4
move	$9,$3
sw	$3,40($sp)
$L15:
lw	$11,104($sp)
addiu	$4,$9,8
lw	$25,%call16(av_mallocz)($28)
addu	$fp,$11,$9
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sw	$11,44($sp)
.set	macro
.set	reorder

lw	$11,44($sp)
move	$23,$2
li	$2,1			# 0x1
lw	$28,16($sp)
sw	$21,40($17)
sltu	$3,$11,$fp
.set	noreorder
.set	nomacro
beq	$3,$0,$L53
sw	$2,11988($17)
.set	macro
.set	reorder

li	$19,16711680			# 0xff0000
li	$18,-16777216			# 0xffffffffff000000
addiu	$19,$19,255
addiu	$20,$17,40
ori	$18,$18,0xff00
$L67:
addiu	$3,$11,4
subu	$2,$fp,$3
slt	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$0,$L83
move	$2,$fp
.set	macro
.set	reorder

sltu	$2,$3,$fp
.set	noreorder
.set	nomacro
beq	$2,$0,$L58
li	$2,-256			# 0xffffffffffffff00
.set	macro
.set	reorder

lbu	$5,4($11)
addiu	$6,$11,5
li	$8,-256			# 0xffffffffffffff00
or	$5,$5,$2
.set	noreorder
.set	nomacro
b	$L34
li	$7,256			# 0x100
.set	macro
.set	reorder

$L33:
lbu	$4,-1($2)
or	$5,$4,$5
and	$4,$5,$8
.set	noreorder
.set	nomacro
beq	$4,$7,$L32
addiu	$16,$6,-3
.set	macro
.set	reorder

move	$6,$2
$L34:
sltu	$4,$6,$fp
addiu	$2,$6,1
.set	noreorder
.set	nomacro
bne	$4,$0,$L33
sll	$5,$5,8
.set	macro
.set	reorder

$L58:
move	$2,$fp
$L83:
move	$16,$fp
$L30:
subu	$2,$2,$11
addiu	$13,$2,-4
slt	$4,$13,4
.set	noreorder
.set	nomacro
beq	$4,$0,$L35
li	$14,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$13,$L36
addu	$2,$11,$2
.set	macro
.set	reorder

move	$5,$23
$L37:
addiu	$3,$3,1
lbu	$4,-1($3)
addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$3,$2,$L37
sb	$4,-1($5)
.set	macro
.set	reorder

sll	$12,$13,3
sra	$2,$12,3
$L54:
addu	$2,$23,$2
move	$3,$23
$L43:
sw	$3,24($sp)
sw	$12,36($sp)
sw	$2,28($sp)
.set	noreorder
.set	nomacro
blez	$13,$L45
sw	$0,32($sp)
.set	macro
.set	reorder

#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($11)  
lwr $2, 0($11)  

# 0 "" 2
#NO_APP
srl	$3,$2,8
sll	$2,$2,8
and	$3,$3,$19
and	$2,$2,$18
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
li	$3,270			# 0x10e
.set	noreorder
.set	nomacro
beq	$2,$3,$L46
li	$3,271			# 0x10f
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L47
li	$3,269			# 0x10d
.set	macro
.set	reorder

beq	$2,$3,$L77
$L45:
sltu	$2,$16,$fp
.set	noreorder
.set	nomacro
beq	$2,$0,$L84
lw	$25,%call16(av_free)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L67
move	$11,$16
.set	macro
.set	reorder

$L35:
li	$6,1			# 0x1
move	$5,$0
addiu	$4,$2,-5
move	$8,$23
$L41:
lbu	$2,0($3)
.set	noreorder
.set	nomacro
beq	$2,$14,$L78
move	$12,$6
.set	macro
.set	reorder

$L39:
sb	$2,0($8)
$L40:
addiu	$5,$5,1
addiu	$3,$3,1
slt	$2,$5,$13
addiu	$6,$6,1
.set	noreorder
.set	nomacro
bne	$2,$0,$L41
addiu	$8,$8,1
.set	macro
.set	reorder

sll	$12,$12,3
sra	$2,$12,3
bltz	$2,$L44
$L42:
bgez	$12,$L54
$L44:
move	$2,$0
move	$12,$0
.set	noreorder
.set	nomacro
b	$L43
move	$3,$0
.set	macro
.set	reorder

$L78:
slt	$15,$5,2
bne	$15,$0,$L39
lbu	$15,-1($3)
bne	$15,$0,$L39
lbu	$15,-2($3)
.set	noreorder
.set	nomacro
bne	$15,$0,$L39
slt	$24,$5,$4
.set	macro
.set	reorder

beq	$24,$0,$L39
lbu	$15,1($3)
sltu	$24,$15,4
beq	$24,$0,$L39
sb	$15,0($8)
addiu	$3,$3,1
.set	noreorder
.set	nomacro
b	$L40
addiu	$5,$5,1
.set	macro
.set	reorder

$L32:
.set	noreorder
.set	nomacro
b	$L30
move	$2,$16
.set	macro
.set	reorder

$L77:
lw	$2,11208($17)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L49
lw	$25,%call16(vc1_parse_frame_header_adv)($28)
.set	macro
.set	reorder

lw	$25,%call16(vc1_parse_frame_header)($28)
addiu	$5,$sp,24
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_parse_frame_header
1:	jalr	$25
move	$4,$20
.set	macro
.set	reorder

li	$3,7			# 0x7
lw	$2,2944($17)
.set	noreorder
.set	nomacro
beq	$2,$3,$L79
lw	$28,16($sp)
.set	macro
.set	reorder

$L51:
sw	$2,32($22)
sltu	$2,$16,$fp
.set	noreorder
.set	nomacro
bne	$2,$0,$L67
move	$11,$16
.set	macro
.set	reorder

$L53:
lw	$25,%call16(av_free)($28)
$L84:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$3,104($sp)
lw	$4,96($sp)
lw	$2,40($sp)
sw	$3,0($4)
lw	$3,108($sp)
lw	$4,100($sp)
sw	$3,0($4)
$L66:
lw	$31,84($sp)
lw	$fp,80($sp)
lw	$23,76($sp)
lw	$22,72($sp)
lw	$21,68($sp)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L47:
lw	$25,%call16(vc1_decode_sequence_header)($28)
$L71:
addiu	$6,$sp,24
move	$4,$21
.set	noreorder
.set	nomacro
jalr	$25
move	$5,$20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L45
lw	$28,16($sp)
.set	macro
.set	reorder

$L46:
.set	noreorder
.set	nomacro
b	$L71
lw	$25,%call16(vc1_decode_entry_point)($28)
.set	macro
.set	reorder

$L49:
addiu	$5,$sp,24
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_parse_frame_header_adv
1:	jalr	$25
move	$4,$20
.set	macro
.set	reorder

li	$3,7			# 0x7
lw	$2,2944($17)
.set	noreorder
.set	nomacro
bne	$2,$3,$L51
lw	$28,16($sp)
.set	macro
.set	reorder

$L79:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
b	$L45
sw	$2,32($22)
.set	macro
.set	reorder

$L36:
sll	$12,$13,3
sra	$2,$12,3
bgez	$2,$L42
sw	$0,24($sp)
sw	$0,36($sp)
sw	$0,28($sp)
.set	noreorder
.set	nomacro
b	$L45
sw	$0,32($sp)
.set	macro
.set	reorder

$L14:
lw	$9,20($4)
lw	$6,104($sp)
lw	$5,108($sp)
.set	noreorder
.set	nomacro
bne	$9,$0,$L16
lw	$2,16($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$5,$L85
li	$3,-100			# 0xffffffffffffff9c
.set	macro
.set	reorder

lbu	$3,0($6)
sll	$2,$2,8
or	$2,$3,$2
addiu	$3,$2,-268
sltu	$3,$3,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L20
move	$16,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L80
li	$16,1			# 0x1
.set	macro
.set	reorder

$L22:
lbu	$2,0($3)
or	$2,$7,$2
addiu	$3,$2,-268
sltu	$3,$3,2
bne	$3,$0,$L81
$L20:
addiu	$16,$16,1
sll	$7,$2,8
.set	noreorder
.set	nomacro
bne	$16,$5,$L22
addu	$3,$6,$16
.set	macro
.set	reorder

$L23:
li	$3,-100			# 0xffffffffffffff9c
$L85:
sw	$9,20($4)
sw	$2,16($4)
.set	noreorder
.set	nomacro
b	$L25
sw	$3,40($sp)
.set	macro
.set	reorder

$L16:
.set	noreorder
.set	nomacro
bne	$5,$0,$L21
move	$16,$0
.set	macro
.set	reorder

sw	$0,40($sp)
$L25:
lw	$25,%call16(ff_combine_frame)($28)
addiu	$6,$sp,104
lw	$5,40($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_combine_frame
1:	jalr	$25
addiu	$7,$sp,108
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L82
lw	$28,16($sp)
.set	macro
.set	reorder

lw	$9,108($sp)
.set	noreorder
.set	nomacro
b	$L15
lw	$17,0($22)
.set	macro
.set	reorder

$L80:
li	$9,1			# 0x1
$L21:
slt	$3,$16,$5
.set	noreorder
.set	nomacro
beq	$3,$0,$L23
li	$8,-256			# 0xffffffffffffff00
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L26
li	$7,256			# 0x100
.set	macro
.set	reorder

$L24:
addiu	$16,$16,1
$L86:
.set	noreorder
.set	nomacro
beq	$16,$5,$L85
li	$3,-100			# 0xffffffffffffff9c
.set	macro
.set	reorder

$L26:
addu	$3,$6,$16
sll	$2,$2,8
lbu	$3,0($3)
or	$2,$3,$2
and	$3,$2,$8
.set	noreorder
.set	nomacro
bne	$3,$7,$L24
addiu	$3,$2,-267
.set	macro
.set	reorder

sltu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L86
addiu	$16,$16,1
.set	macro
.set	reorder

addiu	$16,$16,-1
addiu	$16,$16,-3
sw	$0,20($4)
li	$2,-1			# 0xffffffffffffffff
sw	$16,40($sp)
.set	noreorder
.set	nomacro
b	$L25
sw	$2,16($4)
.set	macro
.set	reorder

$L81:
addiu	$16,$16,1
.set	noreorder
.set	nomacro
b	$L21
li	$9,1			# 0x1
.set	macro
.set	reorder

$L82:
lw	$3,96($sp)
lw	$2,108($sp)
sw	$0,0($3)
lw	$3,100($sp)
.set	noreorder
.set	nomacro
b	$L66
sw	$0,0($3)
.set	macro
.set	reorder

.end	vc1_parse
.size	vc1_parse, .-vc1_parse
.globl	vc1_parser
.section	.data.rel,"aw",@progbits
.align	2
.type	vc1_parser, @object
.size	vc1_parser, 44
vc1_parser:
.word	73
.space	16
.word	12000
.word	0
.word	vc1_parse
.word	ff_parse1_close
.word	vc1_split
.space	4

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
