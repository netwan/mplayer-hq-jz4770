.file	1 "vc1.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.decode_colskip,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_colskip
.type	decode_colskip, @function
decode_colskip:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
blez	$5,$L17
addu	$12,$4,$5

$L10:
lw	$3,8($8)
lw	$5,0($8)
srl	$9,$3,3
andi	$10,$3,0x7
addu	$9,$5,$9
addiu	$3,$3,1
lbu	$2,0($9)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L3
sw	$3,8($8)

blez	$6,$L7
nop

move	$10,$0
b	$L9
move	$9,$4

$L14:
lw	$3,8($8)
lw	$5,0($8)
$L9:
srl	$2,$3,3
andi	$11,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
addiu	$10,$10,1
lbu	$2,0($5)
sw	$3,8($8)
sll	$2,$2,$11
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,0($9)
bne	$10,$6,$L14
addu	$9,$9,$7

$L7:
addiu	$4,$4,1
bne	$4,$12,$L10
nop

$L17:
j	$31
nop

$L3:
blez	$6,$L7
move	$3,$0

move	$2,$4
$L8:
addiu	$3,$3,1
sb	$0,0($2)
bne	$3,$6,$L8
addu	$2,$2,$7

addiu	$4,$4,1
bne	$4,$12,$L10
nop

j	$31
nop

.set	macro
.set	reorder
.end	decode_colskip
.size	decode_colskip, .-decode_colskip
.section	.text.decode_rowskip,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_rowskip
.type	decode_rowskip, @function
decode_rowskip:
.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
.cprestore	16
sw	$21,48($sp)
move	$21,$7
sw	$20,44($sp)
move	$20,$6
sw	$19,40($sp)
move	$19,$5
sw	$18,36($sp)
move	$18,$0
sw	$17,32($sp)
move	$17,$4
sw	$16,28($sp)
sw	$31,52($sp)
blez	$6,$L18
lw	$16,72($sp)

$L27:
lw	$3,8($16)
lw	$8,0($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L20
sw	$3,8($16)

blez	$19,$L22
nop

addu	$4,$17,$19
b	$L24
move	$9,$17

$L30:
lw	$3,8($16)
lw	$8,0($16)
$L24:
srl	$2,$3,3
andi	$10,$3,0x7
addu	$8,$8,$2
addiu	$3,$3,1
addiu	$9,$9,1
lbu	$2,0($8)
sw	$3,8($16)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
bne	$4,$9,$L30
sb	$2,-1($9)

$L22:
addiu	$18,$18,1
bne	$18,$20,$L27
addu	$17,$17,$21

$L18:
lw	$31,52($sp)
$L31:
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,56

$L20:
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$18,$18,1
move	$4,$17
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$6,$19

addu	$17,$17,$21
bne	$18,$20,$L27
lw	$28,16($sp)

b	$L31
lw	$31,52($sp)

.set	macro
.set	reorder
.end	decode_rowskip
.size	decode_rowskip, .-decode_rowskip
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"invalid NORM-6 VLC\012\000"
.section	.text.bitplane_decoding,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	bitplane_decoding
.type	bitplane_decoding, @function
bitplane_decoding:
.frame	$sp,80,$31		# vars= 8, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-80
lw	$2,%got(ff_vc1_imode_vlc)($28)
lw	$11,10332($6)
sw	$16,40($sp)
lw	$16,10340($6)
lw	$13,4($2)
sw	$22,64($sp)
srl	$2,$16,3
sw	$21,60($sp)
addiu	$9,$16,1
sw	$20,56($sp)
addu	$2,$11,$2
sw	$19,52($sp)
srl	$3,$9,3
sw	$18,48($sp)
sw	$17,44($sp)
move	$17,$6
.cprestore	24
addu	$3,$11,$3
sw	$31,76($sp)
andi	$14,$9,0x7
sw	$fp,72($sp)
andi	$16,$16,0x7
sw	$23,68($sp)
move	$19,$4
lbu	$12,0($2)
lw	$18,160($6)
lw	$20,164($6)
lw	$21,168($6)
sll	$16,$12,$16
sw	$9,10340($6)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$8,$6,8
sll	$10,$6,8
li	$6,16711680			# 0xff0000
andi	$16,$16,0x00ff
addiu	$6,$6,255
srl	$16,$16,7
and	$7,$8,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$6,$10,$6
or	$6,$7,$6
sll	$3,$6,16
srl	$2,$6,16
or	$2,$3,$2
sll	$2,$2,$14
srl	$2,$2,28
sll	$3,$2,2
addu	$3,$13,$3
lh	$2,2($3)
lh	$22,0($3)
addu	$9,$9,$2
andi	$2,$22,0xffff
sltu	$3,$2,7
sw	$9,10340($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L33
sw	$0,0($5)
.set	macro
.set	reorder

sll	$3,$2,2
lw	$2,%got($L35)($28)
addiu	$2,$2,%lo($L35)
addu	$2,$2,$3
lw	$2,0($2)
addu	$2,$2,$28
j	$2
.rdata
.align	2
.align	2
$L35:
.gpword	$L34
.gpword	$L36
.gpword	$L36
.gpword	$L37
.gpword	$L37
.gpword	$L38
.gpword	$L39
.section	.text.bitplane_decoding
$L58:
addiu	$23,$23,3
lw	$fp,36($sp)
slt	$2,$23,$20
.set	noreorder
.set	nomacro
bne	$2,$0,$L53
addu	$12,$12,$fp
.set	macro
.set	reorder

$L51:
.set	noreorder
.set	nomacro
beq	$14,$0,$L118
li	$2,2			# 0x2
.set	macro
.set	reorder

lw	$25,%got(decode_colskip)($28)
addiu	$17,$17,10332
li	$5,1			# 0x1
move	$4,$19
addiu	$25,$25,%lo(decode_colskip)
sw	$17,16($sp)
move	$6,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_colskip
1:	jalr	$25
move	$7,$21
.set	macro
.set	reorder

$L33:
li	$2,2			# 0x2
$L118:
.set	noreorder
.set	nomacro
beq	$22,$2,$L70
li	$2,4			# 0x4
.set	macro
.set	reorder

beq	$22,$2,$L70
.set	noreorder
.set	nomacro
beq	$16,$0,$L116
lw	$31,76($sp)
.set	macro
.set	reorder

mul	$3,$21,$20
$L117:
.set	noreorder
.set	nomacro
blez	$3,$L116
lw	$31,76($sp)
.set	macro
.set	reorder

addu	$3,$19,$3
$L83:
lbu	$2,0($19)
addiu	$19,$19,1
sltu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$19,$3,$L83
sb	$2,-1($19)
.set	macro
.set	reorder

$L74:
lw	$31,76($sp)
$L116:
sll	$2,$22,1
lw	$fp,72($sp)
addu	$2,$2,$16
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L70:
lbu	$2,0($19)
slt	$7,$18,2
xor	$2,$16,$2
.set	noreorder
.set	nomacro
bne	$7,$0,$L76
sb	$2,0($19)
.set	macro
.set	reorder

addiu	$2,$19,1
addu	$5,$19,$18
$L75:
lbu	$4,0($2)
addiu	$2,$2,1
lbu	$3,-2($2)
xor	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$2,$5,$L75
sb	$3,-1($2)
.set	macro
.set	reorder

$L76:
slt	$2,$20,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L74
addiu	$2,$21,1
.set	macro
.set	reorder

subu	$5,$0,$21
addu	$19,$19,$2
li	$8,1			# 0x1
addiu	$18,$18,-1
$L78:
addu	$3,$19,$5
lbu	$4,-1($19)
lbu	$2,-1($3)
xor	$2,$2,$4
.set	noreorder
.set	nomacro
bne	$7,$0,$L82
sb	$2,-1($19)
.set	macro
.set	reorder

addu	$6,$18,$19
.set	noreorder
.set	nomacro
b	$L81
move	$2,$19
.set	macro
.set	reorder

$L111:
lbu	$3,0($2)
addiu	$2,$2,1
xor	$3,$16,$3
.set	noreorder
.set	nomacro
beq	$2,$6,$L82
sb	$3,-1($2)
.set	macro
.set	reorder

$L81:
addu	$3,$2,$5
lbu	$4,-1($2)
lbu	$3,0($3)
bne	$3,$4,$L111
lbu	$4,0($2)
addiu	$2,$2,1
xor	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$2,$6,$L81
sb	$3,-1($2)
.set	macro
.set	reorder

$L82:
addiu	$8,$8,1
.set	noreorder
.set	nomacro
bne	$8,$20,$L78
addu	$19,$19,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L116
lw	$31,76($sp)
.set	macro
.set	reorder

$L39:
lw	$25,%got(decode_colskip)($28)
addiu	$17,$17,10332
move	$5,$18
move	$6,$20
addiu	$25,$25,%lo(decode_colskip)
sw	$17,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_colskip
1:	jalr	$25
move	$7,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L116
lw	$31,76($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L117
mul	$3,$21,$20
.set	macro
.set	reorder

$L34:
lw	$31,76($sp)
li	$3,1			# 0x1
move	$2,$16
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
sw	$3,0($5)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L36:
mul	$13,$20,$18
andi	$2,$13,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L85
move	$10,$0
.set	macro
.set	reorder

lw	$3,10340($17)
addiu	$8,$4,1
li	$10,1			# 0x1
srl	$2,$3,3
andi	$4,$3,0x7
addu	$11,$11,$2
addiu	$3,$3,1
lbu	$2,0($11)
sw	$3,10340($17)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,0($19)
$L41:
slt	$2,$10,$13
.set	noreorder
.set	nomacro
beq	$2,$0,$L33
li	$12,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$14,%got(ff_vc1_norm2_vlc)($28)
li	$11,-16777216			# 0xffffffffff000000
addiu	$12,$12,255
subu	$15,$21,$18
move	$6,$10
.set	noreorder
.set	nomacro
b	$L46
ori	$11,$11,0xff00
.set	macro
.set	reorder

$L114:
sra	$2,$2,1
addiu	$6,$6,2
addiu	$8,$9,1
.set	noreorder
.set	nomacro
beq	$18,$6,$L113
sb	$2,0($9)
.set	macro
.set	reorder

$L45:
addiu	$10,$10,2
slt	$2,$10,$13
.set	noreorder
.set	nomacro
beq	$2,$0,$L118
li	$2,2			# 0x2
.set	macro
.set	reorder

$L46:
lw	$5,10340($17)
addiu	$7,$6,1
lw	$2,10332($17)
addiu	$9,$8,1
srl	$3,$5,3
andi	$23,$5,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$4,$2,8
srl	$3,$3,8
and	$4,$4,$11
and	$3,$3,$12
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
lw	$3,4($14)
sll	$2,$2,$23
srl	$2,$2,29
sll	$2,$2,2
addu	$2,$3,$2
lh	$3,2($2)
lh	$2,0($2)
addu	$5,$5,$3
andi	$3,$2,0x1
sw	$5,10340($17)
.set	noreorder
.set	nomacro
bne	$7,$18,$L114
sb	$3,0($8)
.set	macro
.set	reorder

sra	$2,$2,1
addu	$9,$9,$15
li	$6,1			# 0x1
addiu	$8,$9,1
.set	noreorder
.set	nomacro
bne	$18,$6,$L45
sb	$2,0($9)
.set	macro
.set	reorder

$L113:
addu	$8,$8,$15
.set	noreorder
.set	nomacro
b	$L45
move	$6,$0
.set	macro
.set	reorder

$L37:
li	$2,1431633920			# 0x55550000
sra	$3,$20,31
addiu	$2,$2,21846
mult	$20,$2
mfhi	$4
subu	$3,$4,$3
sll	$4,$3,1
addu	$3,$4,$3
.set	noreorder
.set	nomacro
bne	$20,$3,$L48
mult	$18,$2
.set	macro
.set	reorder

sra	$3,$18,31
mfhi	$2
subu	$2,$2,$3
sll	$3,$2,1
addu	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$18,$2,$L49
andi	$fp,$20,0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$20,$L51
andi	$14,$18,0x1
.set	macro
.set	reorder

sll	$2,$21,1
lw	$13,%got(ff_vc1_norm6_vlc)($28)
addu	$15,$14,$21
addu	$3,$2,$21
li	$11,16711680			# 0xff0000
slt	$4,$14,$18
li	$10,-16777216			# 0xffffffffff000000
sw	$3,36($sp)
addu	$12,$19,$2
sw	$4,32($sp)
move	$23,$0
subu	$24,$14,$2
subu	$15,$15,$2
addiu	$11,$11,255
ori	$10,$10,0xff00
$L53:
lw	$25,32($sp)
.set	noreorder
.set	nomacro
beq	$25,$0,$L58
addu	$7,$24,$12
.set	macro
.set	reorder

addu	$6,$15,$12
.set	noreorder
.set	nomacro
b	$L57
move	$5,$14
.set	macro
.set	reorder

$L55:
addu	$3,$3,$4
.set	noreorder
.set	nomacro
bltz	$2,$L110
sw	$3,10340($17)
.set	macro
.set	reorder

sra	$25,$2,1
sra	$9,$2,2
sra	$8,$2,3
sra	$4,$2,4
sra	$3,$2,5
addu	$fp,$12,$5
andi	$2,$2,0x1
addiu	$5,$5,2
andi	$25,$25,0x1
sb	$2,0($7)
andi	$9,$9,0x1
andi	$8,$8,0x1
sb	$25,1($7)
andi	$4,$4,0x1
sb	$9,0($6)
andi	$3,$3,0x1
sb	$8,1($6)
slt	$2,$5,$18
sb	$4,0($fp)
sb	$3,1($fp)
addiu	$7,$7,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L58
addiu	$6,$6,2
.set	macro
.set	reorder

$L57:
lw	$4,10340($17)
lw	$8,10332($17)
lw	$9,4($13)
srl	$2,$4,3
andi	$25,$4,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$fp,$3,8
sll	$2,$3,8
and	$fp,$fp,$11
and	$2,$2,$10
or	$3,$fp,$2
sll	$2,$3,16
srl	$3,$3,16
or	$2,$2,$3
sll	$2,$2,$25
srl	$2,$2,23
sll	$2,$2,2
addu	$2,$9,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L55
lh	$2,0($2)
.set	macro
.set	reorder

addiu	$4,$4,9
srl	$fp,$4,3
andi	$25,$4,0x7
addu	$8,$8,$fp
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($8)  
lwr $fp, 0($8)  

# 0 "" 2
#NO_APP
move	$8,$fp
sll	$8,$8,8
srl	$fp,$fp,8
and	$8,$8,$10
and	$fp,$fp,$11
or	$fp,$fp,$8
sll	$8,$fp,16
srl	$fp,$fp,16
or	$fp,$8,$fp
sll	$fp,$fp,$25
srl	$3,$fp,$3
addu	$2,$3,$2
sll	$2,$2,2
addu	$9,$9,$2
lh	$2,0($9)
.set	noreorder
.set	nomacro
b	$L55
lh	$3,2($9)
.set	macro
.set	reorder

$L38:
lw	$25,%got(decode_rowskip)($28)
addiu	$17,$17,10332
move	$5,$18
move	$6,$20
addiu	$25,$25,%lo(decode_rowskip)
sw	$17,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_rowskip
1:	jalr	$25
move	$7,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L116
lw	$31,76($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L117
mul	$3,$21,$20
.set	macro
.set	reorder

$L48:
andi	$fp,$20,0x1
sra	$3,$18,31
mfhi	$2
mul	$4,$fp,$21
subu	$2,$2,$3
slt	$3,$fp,$20
sll	$23,$2,1
addu	$14,$4,$19
addu	$2,$23,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L60
subu	$23,$18,$2
.set	macro
.set	reorder

$L84:
sll	$2,$21,1
lw	$13,%got(ff_vc1_norm6_vlc)($28)
li	$12,16711680			# 0xff0000
li	$7,-16777216			# 0xffffffffff000000
addu	$14,$14,$23
sw	$2,32($sp)
addiu	$12,$12,255
move	$15,$fp
slt	$24,$23,$18
ori	$7,$7,0xff00
$L62:
.set	noreorder
.set	nomacro
beq	$24,$0,$L67
addu	$9,$14,$21
.set	macro
.set	reorder

move	$4,$14
.set	noreorder
.set	nomacro
b	$L66
move	$11,$23
.set	macro
.set	reorder

$L64:
addu	$3,$3,$8
.set	noreorder
.set	nomacro
bltz	$2,$L110
sw	$3,10340($17)
.set	macro
.set	reorder

$L65:
sra	$3,$2,5
sra	$10,$2,1
sra	$8,$2,2
sra	$6,$2,3
sra	$5,$2,4
andi	$2,$2,0x1
addiu	$11,$11,3
sb	$2,0($4)
andi	$10,$10,0x1
andi	$2,$3,0x1
andi	$8,$8,0x1
sb	$10,1($4)
andi	$6,$6,0x1
andi	$5,$5,0x1
sb	$8,2($4)
slt	$3,$11,$18
sb	$6,0($9)
sb	$5,1($9)
addiu	$4,$4,3
sb	$2,2($9)
.set	noreorder
.set	nomacro
beq	$3,$0,$L67
addiu	$9,$9,3
.set	macro
.set	reorder

$L66:
lw	$8,10340($17)
lw	$6,10332($17)
lw	$10,4($13)
srl	$2,$8,3
andi	$5,$8,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($2)  
lwr $25, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$25,8
sll	$2,$25,8
and	$3,$3,$12
and	$2,$2,$7
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$5
srl	$2,$2,23
sll	$2,$2,2
addu	$2,$10,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L64
lh	$2,0($2)
.set	macro
.set	reorder

addiu	$8,$8,9
srl	$25,$8,3
andi	$5,$8,0x7
addu	$6,$6,$25
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($6)  
lwr $25, 0($6)  

# 0 "" 2
#NO_APP
move	$6,$25
sll	$6,$6,8
srl	$25,$25,8
and	$6,$6,$7
and	$25,$25,$12
or	$25,$25,$6
sll	$6,$25,16
srl	$25,$25,16
or	$25,$6,$25
sll	$5,$25,$5
srl	$3,$5,$3
addu	$2,$3,$2
sll	$2,$2,2
addu	$10,$10,$2
lh	$3,2($10)
lh	$2,0($10)
addu	$3,$3,$8
.set	noreorder
.set	nomacro
bgez	$2,$L65
sw	$3,10340($17)
.set	macro
.set	reorder

$L110:
lw	$6,%got($LC0)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,76($sp)
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L67:
addiu	$15,$15,2
lw	$3,32($sp)
slt	$2,$15,$20
.set	noreorder
.set	nomacro
bne	$2,$0,$L62
addu	$14,$14,$3
.set	macro
.set	reorder

$L60:
.set	noreorder
.set	nomacro
bne	$23,$0,$L115
lw	$25,%got(decode_colskip)($28)
.set	macro
.set	reorder

$L68:
.set	noreorder
.set	nomacro
beq	$fp,$0,$L33
lw	$25,%got(decode_rowskip)($28)
.set	macro
.set	reorder

addiu	$17,$17,10332
addu	$4,$19,$23
subu	$5,$18,$23
li	$6,1			# 0x1
sw	$17,16($sp)
addiu	$25,$25,%lo(decode_rowskip)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_rowskip
1:	jalr	$25
move	$7,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L118
li	$2,2			# 0x2
.set	macro
.set	reorder

$L85:
.set	noreorder
.set	nomacro
b	$L41
move	$8,$4
.set	macro
.set	reorder

$L49:
mul	$3,$fp,$21
slt	$2,$fp,$20
move	$23,$0
.set	noreorder
.set	nomacro
bne	$2,$0,$L84
addu	$14,$3,$19
.set	macro
.set	reorder

b	$L68
$L115:
addiu	$2,$17,10332
move	$4,$19
move	$5,$23
addiu	$25,$25,%lo(decode_colskip)
sw	$2,16($sp)
move	$6,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_colskip
1:	jalr	$25
move	$7,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L68
lw	$28,24($sp)
.set	macro
.set	reorder

.end	bitplane_decoding
.size	bitplane_decoding, .-bitplane_decoding
.section	.text.vop_dquant_decoding,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vop_dquant_decoding
.type	vop_dquant_decoding, @function
vop_dquant_decoding:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$3,11188($4)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L132
li	$9,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$3,10340($4)
lw	$7,10332($4)
srl	$5,$3,3
andi	$8,$3,0x7
addu	$5,$7,$5
addiu	$6,$3,1
lbu	$2,0($5)
sw	$6,10340($4)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L133
sb	$2,11240($4)
.set	macro
.set	reorder

$L122:
.set	noreorder
.set	nomacro
j	$31
move	$2,$0
.set	macro
.set	reorder

$L133:
srl	$2,$6,3
li	$10,16711680			# 0xff0000
addu	$2,$7,$2
li	$9,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$5
addiu	$10,$10,255
srl	$5,$5,8
sll	$2,$2,8
ori	$9,$9,0xff00
and	$5,$5,$10
and	$2,$2,$9
or	$2,$5,$2
sll	$5,$2,16
srl	$2,$2,16
andi	$6,$6,0x7
or	$2,$5,$2
sll	$2,$2,$6
addiu	$5,$3,3
srl	$2,$2,30
sw	$5,10340($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L124
sb	$2,11241($4)
.set	macro
.set	reorder

sltu	$6,$2,3
.set	noreorder
.set	nomacro
bne	$6,$0,$L125
li	$6,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$6,$L124
srl	$2,$5,3
.set	macro
.set	reorder

andi	$5,$5,0x7
addu	$2,$7,$2
addiu	$3,$3,4
lbu	$2,0($2)
sw	$3,10340($4)
sll	$5,$2,$5
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
bne	$5,$0,$L124
sb	$5,11243($4)
.set	macro
.set	reorder

sb	$0,11300($4)
$L124:
lw	$2,11240($4)
li	$5,-16777216			# 0xffffffffff000000
li	$3,768			# 0x300
ori	$5,$5,0xff00
and	$2,$2,$5
.set	noreorder
.set	nomacro
beq	$2,$3,$L122
li	$9,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$8,10340($4)
addiu	$9,$9,255
srl	$3,$8,3
andi	$10,$8,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$6,8
sll	$6,$6,8
and	$3,$3,$9
and	$6,$6,$5
or	$2,$3,$6
sll	$3,$2,16
srl	$2,$2,16
addiu	$6,$8,3
or	$2,$3,$2
sll	$2,$2,$10
li	$3,7			# 0x7
srl	$2,$2,29
.set	noreorder
.set	nomacro
beq	$2,$3,$L134
sw	$6,10340($4)
.set	macro
.set	reorder

$L128:
lbu	$3,11228($4)
addiu	$3,$3,1
addu	$2,$3,$2
sb	$2,11229($4)
.set	noreorder
.set	nomacro
j	$31
move	$2,$0
.set	macro
.set	reorder

$L125:
srl	$2,$5,3
andi	$5,$5,0x7
addu	$2,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
srl	$6,$8,8
sll	$8,$8,8
and	$6,$6,$10
and	$2,$8,$9
or	$2,$6,$2
sll	$6,$2,16
srl	$2,$2,16
addiu	$3,$3,5
or	$2,$6,$2
sll	$5,$2,$5
sw	$3,10340($4)
srl	$5,$5,30
.set	noreorder
.set	nomacro
b	$L124
sb	$5,11242($4)
.set	macro
.set	reorder

$L132:
lw	$6,10340($4)
lw	$7,10332($4)
li	$5,-16777216			# 0xffffffffff000000
addiu	$9,$9,255
srl	$2,$6,3
ori	$5,$5,0xff00
addu	$2,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$2,$2,8
srl	$3,$3,8
and	$2,$2,$5
and	$3,$3,$9
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$6,0x7
or	$2,$3,$2
sll	$2,$2,$8
addiu	$3,$6,3
srl	$2,$2,29
li	$8,7			# 0x7
.set	noreorder
.set	nomacro
bne	$2,$8,$L128
sw	$3,10340($4)
.set	macro
.set	reorder

srl	$2,$3,3
andi	$3,$3,0x7
addu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($7)  
lwr $2, 0($7)  

# 0 "" 2
#NO_APP
move	$7,$2
sll	$7,$7,8
srl	$2,$2,8
and	$5,$7,$5
and	$9,$2,$9
or	$5,$9,$5
sll	$7,$5,16
srl	$5,$5,16
addiu	$2,$6,8
or	$5,$7,$5
sll	$3,$5,$3
sw	$2,10340($4)
srl	$3,$3,27
.set	noreorder
.set	nomacro
b	$L122
sb	$3,11229($4)
.set	macro
.set	reorder

$L134:
srl	$2,$6,3
andi	$6,$6,0x7
addu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($7)  
lwr $3, 0($7)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$9,$2,$9
and	$5,$3,$5
or	$5,$9,$5
sll	$2,$5,16
srl	$5,$5,16
addiu	$8,$8,8
or	$5,$2,$5
sll	$6,$5,$6
sw	$8,10340($4)
srl	$6,$6,27
.set	noreorder
.set	nomacro
b	$L122
sb	$6,11229($4)
.set	macro
.set	reorder

.end	vop_dquant_decoding
.size	vop_dquant_decoding, .-vop_dquant_decoding
.section	.text.motion_init,"ax",@progbits
.align	2
.align	5
.globl	motion_init
.set	nomips16
.set	nomicromips
.ent	motion_init
.type	motion_init, @function
motion_init:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
sll	$3,$5,4
lw	$11,%got(mc_base)($28)
sll	$6,$4,6
sll	$2,$4,4
sll	$5,$5,6
addu	$2,$2,$6
addu	$5,$3,$5
lw	$3,%got(IntpFMT)($28)
sll	$6,$2,3
sll	$9,$5,3
addiu	$3,$3,%lo(IntpFMT)
subu	$2,$6,$2
subu	$9,$9,$5
addiu	$sp,$sp,-16
addu	$2,$3,$2
addu	$9,$3,$9
sw	$18,12($sp)
move	$10,$0
sw	$17,8($sp)
li	$13,128			# 0x80
sw	$16,4($sp)
$L136:
lbu	$7,5($2)
addiu	$15,$10,1280
lbu	$24,0($2)
lbu	$12,1($2)
lbu	$8,33($2)
sll	$7,$7,31
lbu	$18,7($2)
andi	$24,$24,0x3
lbu	$5,25($2)
sll	$24,$24,28
andi	$8,$8,0x1
lbu	$17,27($2)
andi	$6,$12,0x1
lbu	$14,29($2)
or	$7,$8,$7
lbu	$8,31($2)
sll	$6,$6,27
lw	$16,0($11)
andi	$18,$18,0x1
lw	$12,%got(mc_base)($28)
or	$7,$7,$24
sll	$18,$18,24
or	$6,$7,$6
sll	$5,$5,16
andi	$7,$17,0xf
or	$6,$6,$18
sll	$7,$7,8
andi	$3,$14,0x1
or	$5,$6,$5
sll	$3,$3,2
andi	$6,$8,0x1
or	$5,$5,$7
sll	$6,$6,1
or	$3,$5,$3
or	$3,$3,$6
addu	$15,$16,$15
#APP
# 116 "vc1.c" 1
sw	 $3,0($15)	#i_sw
# 0 "" 2
#NO_APP
lbu	$6,6($2)
addiu	$24,$10,1284
lbu	$7,2($2)
lbu	$8,34($2)
lbu	$18,3($2)
sll	$6,$6,31
lbu	$25,4($2)
andi	$7,$7,0x1
lbu	$17,8($2)
andi	$8,$8,0x1
sll	$7,$7,27
lbu	$5,26($2)
andi	$18,$18,0x1
lbu	$16,28($2)
or	$8,$8,$6
lbu	$15,30($2)
sll	$18,$18,26
lbu	$14,32($2)
andi	$6,$25,0x1
lw	$25,0($11)
or	$7,$8,$7
sll	$6,$6,25
andi	$8,$17,0x1
or	$7,$7,$18
sll	$8,$8,24
or	$6,$7,$6
sll	$5,$5,16
andi	$7,$16,0xf
or	$6,$6,$8
sll	$7,$7,8
andi	$3,$15,0x1
or	$5,$6,$5
sll	$3,$3,2
andi	$6,$14,0x1
or	$5,$5,$7
sll	$6,$6,1
or	$3,$5,$3
or	$3,$3,$6
addu	$24,$25,$24
#APP
# 116 "vc1.c" 1
sw	 $3,0($24)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,16($2)
addiu	$14,$10,1028
lbu	$3,15($2)
lbu	$6,13($2)
lbu	$8,14($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$8,8
or	$3,$5,$3
or	$3,$3,$6
addu	$5,$7,$14
#APP
# 137 "vc1.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,12($2)
addiu	$14,$10,1024
lbu	$3,11($2)
lbu	$6,9($2)
lbu	$8,10($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$8,8
or	$3,$5,$3
or	$3,$3,$6
addu	$5,$7,$14
#APP
# 137 "vc1.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,24($2)
addiu	$14,$10,1156
lbu	$3,23($2)
lbu	$6,21($2)
lbu	$8,22($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$8,8
or	$3,$5,$3
or	$3,$3,$6
addu	$5,$7,$14
#APP
# 147 "vc1.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,20($2)
addiu	$14,$10,1152
lbu	$3,19($2)
lbu	$6,17($2)
lbu	$8,18($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$8,8
or	$3,$5,$3
or	$3,$3,$6
addu	$5,$7,$14
#APP
# 147 "vc1.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lbu	$7,7($9)
addiu	$14,$10,3328
lbu	$6,27($9)
lbu	$16,5($9)
lbu	$3,9($9)
sll	$7,$7,15
andi	$6,$6,0x7
lbu	$5,25($9)
lbu	$8,10($9)
sll	$6,$6,12
andi	$7,$7,0xffff
lw	$15,0($11)
sll	$16,$16,31
or	$6,$7,$6
andi	$7,$3,0x7
or	$6,$6,$16
sll	$3,$7,9
andi	$5,$5,0x3f
andi	$7,$8,0x7
or	$5,$6,$5
sll	$6,$7,6
or	$3,$5,$3
or	$3,$3,$6
addu	$5,$15,$14
#APP
# 158 "vc1.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lbu	$7,8($9)
addiu	$14,$10,3332
lbu	$6,28($9)
lbu	$16,6($9)
lbu	$3,17($9)
sll	$7,$7,15
andi	$6,$6,0x7
lbu	$5,26($9)
lbu	$8,18($9)
sll	$6,$6,12
andi	$7,$7,0xffff
lw	$15,0($11)
sll	$16,$16,31
or	$6,$7,$6
andi	$7,$3,0x7
or	$6,$6,$16
sll	$3,$7,9
andi	$5,$5,0x3f
andi	$7,$8,0x7
or	$5,$6,$5
sll	$6,$7,6
or	$3,$5,$3
or	$3,$3,$6
addu	$5,$15,$14
#APP
# 158 "vc1.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$10,$10,8
addiu	$2,$2,35
.set	noreorder
.set	nomacro
bne	$10,$13,$L136
addiu	$9,$9,35
.set	macro
.set	reorder

lw	$2,0($12)
li	$3,7			# 0x7
addiu	$2,$2,4
#APP
# 174 "vc1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($12)
addiu	$2,$2,2052
#APP
# 177 "vc1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,131072			# 0x20000
lw	$3,0($12)
ori	$2,$2,0xff89
#APP
# 180 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(SubPel)($28)
lw	$5,%got(AryFMT)($28)
lw	$3,0($12)
addiu	$2,$2,%lo(SubPel)
addiu	$5,$5,%lo(AryFMT)
addu	$2,$4,$2
addu	$4,$4,$5
addiu	$3,$3,48
lb	$2,0($2)
lbu	$4,0($4)
addiu	$2,$2,-1
sll	$4,$4,31
sll	$2,$2,14
andi	$2,$2,0xffff
or	$2,$2,$4
#APP
# 201 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($12)
move	$3,$0
addiu	$2,$2,2096
#APP
# 214 "vc1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,16
.set	macro
.set	reorder

.end	motion_init
.size	motion_init, .-motion_init
.section	.text.motion_config_vc1,"ax",@progbits
.align	2
.align	5
.globl	motion_config_vc1
.set	nomips16
.set	nomicromips
.ent	motion_config_vc1
.type	motion_config_vc1, @function
motion_config_vc1:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-40
li	$2,131072			# 0x20000
sw	$16,24($sp)
.cprestore	16
ori	$2,$2,0xff89
sw	$17,28($sp)
move	$17,$4
sw	$31,36($sp)
sw	$18,32($sp)
lw	$16,%got(mc_base)($28)
lw	$3,0($16)
#APP
# 239 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
move	$18,$0
addiu	$2,$2,768
#APP
# 260 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,288($4)
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,772
#APP
# 260 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,896
#APP
# 261 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,888($17)
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,900
#APP
# 261 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,2816
#APP
# 262 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,292($17)
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,2820
#APP
# 262 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,2944
#APP
# 263 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,892($17)
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,2948
#APP
# 263 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$6,%got(its_scale)($28)
li	$2,100663296			# 0x6000000
lw	$3,%got(its_rnd_y)($28)
lw	$4,0($16)
lbu	$5,0($6)
lhu	$3,0($3)
addiu	$4,$4,32
sll	$5,$5,16
or	$3,$3,$2
or	$3,$3,$5
#APP
# 265 "vc1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,%got(its_rnd_c)($28)
lbu	$5,0($6)
lw	$4,0($16)
lhu	$3,0($3)
sll	$5,$5,16
addiu	$4,$4,2080
or	$2,$3,$2
or	$2,$2,$5
#APP
# 270 "vc1.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
li	$3,134217728			# 0x8000000
addiu	$2,$2,36
#APP
# 276 "vc1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,44
#APP
# 284 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,2084
#APP
# 285 "vc1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,2088
#APP
# 293 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
addiu	$2,$2,2092
#APP
# 296 "vc1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,192($17)
lw	$3,0($16)
addiu	$4,$2,15
slt	$5,$2,0
movn	$2,$4,$5
addiu	$3,$3,76
sra	$2,$2,4
andi	$2,$2,0xfff
sll	$2,$2,16
ori	$2,$2,0x10
#APP
# 297 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,164($17)
lw	$2,160($17)
lw	$4,0($16)
sll	$3,$3,4
sll	$2,$2,4
andi	$3,$3,0xff0
sll	$3,$3,16
andi	$2,$2,0xff0
or	$2,$3,$2
addiu	$3,$4,80
#APP
# 298 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,192($17)
lw	$3,0($16)
addiu	$4,$2,15
slt	$5,$2,0
movn	$2,$4,$5
addiu	$3,$3,2124
sra	$2,$2,4
andi	$2,$2,0xfff
sll	$2,$2,16
ori	$2,$2,0x8
#APP
# 299 "vc1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

.end	motion_config_vc1
.size	motion_config_vc1, .-motion_config_vc1
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"Header: %0X\012\000"
.align	2
$LC2:
.ascii	"WMV3 Complex Profile is not fully supported\012\000"
.align	2
$LC3:
.ascii	"Reserved LEVEL %i\012\000"
.align	2
$LC4:
.ascii	"Only 4:2:0 chroma format supported\012\000"
.align	2
$LC5:
.ascii	"Advanced Profile level %i:\012frmrtq_postproc=%i, bitrtq"
.ascii	"_postproc=%i\012LoopFilter=%i, ChromaFormat=%i, Pulldown"
.ascii	"=%i, Interlace: %i\012TFCTRflag=%i, FINTERPflag=%i\012\000"
.align	2
$LC6:
.ascii	"Progressive Segmented Frame mode: not supported (yet)\012"
.ascii	"\000"
.align	2
$LC7:
.ascii	"Display extended info:\012\000"
.align	2
$LC8:
.ascii	"Display dimensions: %ix%i\012\000"
.align	2
$LC9:
.ascii	"Aspect: %i:%i\012\000"
.align	2
$LC10:
.ascii	"Reserved RES_SM=%i is forbidden\012\000"
.align	2
$LC11:
.ascii	"LOOPFILTER shall not be enabled in Simple Profile\012\000"
.align	2
$LC12:
.ascii	"FASTUVMC unavailable in Simple Profile\012\000"
.align	2
$LC13:
.ascii	"Extended MVs unavailable in Simple Profile\012\000"
.align	2
$LC14:
.ascii	"1 for reserved RES_TRANSTAB is forbidden\012\000"
.align	2
$LC15:
.ascii	"RANGERED should be set to 0 in Simple Profile\012\000"
.align	2
$LC16:
.ascii	"Old WMV3 version detected, some frames may be decoded in"
.ascii	"correctly\012\000"
.align	2
$LC17:
.ascii	"Profile %i:\012frmrtq_postproc=%i, bitrtq_postproc=%i\012"
.ascii	"LoopFilter=%i, MultiRes=%i, FastUVMC=%i, Extended MV=%i\012"
.ascii	"Rangered=%i, VSTransform=%i, Overlap=%i, SyncMarker=%i\012"
.ascii	"DQuant=%i, Quantizer mode=%i, Max B frames=%i\012\000"
.section	.text.vc1_decode_sequence_header,"ax",@progbits
.align	2
.align	5
.globl	vc1_decode_sequence_header
.set	nomips16
.set	nomicromips
.ent	vc1_decode_sequence_header
.type	vc1_decode_sequence_header, @function
vc1_decode_sequence_header:
.frame	$sp,104,$31		# vars= 0, regs= 6/0, args= 72, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-104
lw	$7,8($6)
li	$3,16711680			# 0xff0000
lw	$25,%call16(av_log)($28)
sw	$16,80($sp)
move	$16,$6
sw	$19,92($sp)
srl	$2,$7,3
sw	$20,96($sp)
li	$19,-16777216			# 0xffffffffff000000
lw	$6,0($6)
addiu	$20,$3,255
ori	$19,$19,0xff00
sw	$17,84($sp)
andi	$7,$7,0x7
.cprestore	72
addu	$2,$6,$2
sw	$18,88($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
srl	$8,$6,8
sw	$31,100($sp)
sll	$6,$6,8
and	$8,$8,$20
and	$6,$6,$19
or	$6,$8,$6
sll	$2,$6,16
srl	$6,$6,16
move	$17,$5
or	$2,$2,$6
lw	$6,%got($LC1)($28)
sll	$7,$2,$7
li	$5,48			# 0x30
addiu	$6,$6,%lo($LC1)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$18,$4
.set	macro
.set	reorder

lw	$6,8($16)
lw	$3,0($16)
lw	$28,72($sp)
srl	$2,$6,3
andi	$7,$6,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$20
and	$5,$4,$19
or	$5,$3,$5
sll	$3,$5,16
srl	$5,$5,16
addiu	$6,$6,2
or	$2,$3,$5
sll	$2,$2,$7
li	$3,2			# 0x2
sw	$6,8($16)
srl	$2,$2,30
.set	noreorder
.set	nomacro
beq	$2,$3,$L194
sw	$2,11168($17)
.set	macro
.set	reorder

$L144:
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L195
lw	$2,%got(vc1_scantableA)($28)
.set	macro
.set	reorder

li	$4,16711680			# 0xff0000
lw	$5,8($16)
li	$8,-16777216			# 0xffffffffff000000
addiu	$9,$4,255
ori	$8,$8,0xff00
sw	$2,11232($17)
srl	$3,$5,3
lw	$2,%got(vc1_scantableB)($28)
andi	$6,$5,0x7
addiu	$4,$5,2
sw	$2,11236($17)
lw	$2,0($16)
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sw	$4,8($16)
sll	$5,$5,8
and	$3,$3,$9
and	$5,$5,$8
or	$5,$3,$5
sll	$3,$5,16
srl	$5,$5,16
or	$7,$3,$5
sll	$7,$7,$6
srl	$7,$7,30
.set	noreorder
.set	nomacro
bne	$7,$0,$L196
sw	$7,11080($17)
.set	macro
.set	reorder

lw	$4,8($16)
srl	$3,$4,3
andi	$7,$4,0x7
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$6,$5,8
sll	$3,$5,8
and	$6,$6,$9
and	$3,$3,$8
or	$5,$6,$3
sll	$6,$5,16
srl	$5,$5,16
addiu	$4,$4,3
or	$5,$6,$5
sll	$5,$5,$7
sw	$4,8($16)
srl	$5,$5,29
sw	$5,11172($17)
lw	$6,8($16)
srl	$4,$6,3
andi	$7,$6,0x7
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$9
and	$8,$5,$8
or	$3,$4,$8
sll	$4,$3,16
srl	$3,$3,16
addiu	$6,$6,5
or	$3,$4,$3
sll	$3,$3,$7
sw	$6,8($16)
srl	$3,$3,27
sw	$3,11176($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L197
sw	$3,9900($17)
.set	macro
.set	reorder

$L162:
lw	$3,0($17)
lw	$3,700($3)
slt	$3,$3,48
bne	$3,$0,$L163
sw	$0,9900($17)
$L163:
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11084($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11088($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L164
sw	$3,11092($17)
.set	macro
.set	reorder

lw	$5,%got(ff_simple_idct)($28)
lw	$2,%got(ff_simple_idct44_add)($28)
lw	$4,%got(ff_simple_idct84_add)($28)
lw	$3,%got(ff_simple_idct48_add)($28)
sw	$5,5768($17)
sw	$2,5780($17)
sw	$4,5772($17)
sw	$3,5776($17)
sw	$4,5788($17)
sw	$3,5792($17)
sw	$2,5796($17)
lw	$5,%got(ff_simple_idct_add)($28)
sw	$5,5784($17)
lw	$2,0($16)
$L164:
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
lw	$4,11168($17)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L198
sw	$3,11180($17)
.set	macro
.set	reorder

$L165:
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
lw	$4,11168($17)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L166
sw	$3,11184($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L199
lw	$6,%got($LC13)($28)
.set	macro
.set	reorder

$L166:
lw	$7,8($16)
srl	$3,$7,3
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
srl	$5,$8,8
addiu	$3,$3,255
sll	$6,$8,8
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
andi	$5,$7,0x7
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
addiu	$7,$7,2
or	$3,$4,$3
sll	$3,$3,$5
sw	$7,8($16)
srl	$3,$3,30
sw	$3,11188($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11192($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L200
sw	$3,11096($17)
.set	macro
.set	reorder

lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11196($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,10092($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L201
sw	$3,11100($17)
.set	macro
.set	reorder

$L168:
lw	$8,8($16)
li	$4,16711680			# 0xff0000
li	$5,-16777216			# 0xffffffffff000000
addiu	$10,$4,255
srl	$3,$8,3
ori	$9,$5,0xff00
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$6,$7,8
sll	$7,$7,8
and	$6,$6,$10
and	$7,$7,$9
or	$5,$6,$7
sll	$6,$5,16
srl	$5,$5,16
andi	$3,$8,0x7
or	$5,$6,$5
sll	$5,$5,$3
addiu	$8,$8,3
srl	$5,$5,29
sw	$8,8($16)
sw	$5,112($18)
sw	$5,72($17)
lw	$6,8($16)
srl	$4,$6,3
andi	$7,$6,0x7
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$10
and	$5,$5,$9
or	$3,$4,$5
sll	$4,$3,16
srl	$3,$3,16
addiu	$6,$6,2
or	$3,$4,$3
sll	$3,$3,$7
sw	$6,8($16)
srl	$3,$3,30
sw	$3,11200($17)
lw	$4,8($16)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$2,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,8($16)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11204($17)
lw	$3,8($16)
srl	$5,$3,3
andi	$4,$3,0x7
addu	$2,$2,$5
addiu	$3,$3,1
lbu	$2,0($2)
sw	$3,8($16)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L202
sw	$2,11104($17)
.set	macro
.set	reorder

$L169:
lw	$2,11092($17)
beq	$2,$0,$L203
$L170:
lw	$2,112($18)
li	$5,48			# 0x30
lw	$20,11172($17)
move	$4,$18
lw	$19,11176($17)
lw	$16,9900($17)
lw	$15,11088($17)
lw	$14,11180($17)
lw	$13,11184($17)
lw	$12,11100($17)
lw	$11,11192($17)
lw	$10,11196($17)
lw	$9,10092($17)
lw	$8,11188($17)
lw	$3,11200($17)
lw	$6,%got($LC17)($28)
lw	$7,11168($17)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC17)
sw	$2,64($sp)
sw	$20,16($sp)
sw	$19,20($sp)
sw	$16,24($sp)
sw	$15,28($sp)
sw	$14,32($sp)
sw	$13,36($sp)
sw	$12,40($sp)
sw	$11,44($sp)
sw	$10,48($sp)
sw	$9,52($sp)
sw	$8,56($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,60($sp)
.set	macro
.set	reorder

move	$2,$0
$L148:
lw	$31,100($sp)
lw	$20,96($sp)
lw	$19,92($sp)
lw	$18,88($sp)
lw	$17,84($sp)
lw	$16,80($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,104
.set	macro
.set	reorder

$L198:
.set	noreorder
.set	nomacro
bne	$3,$0,$L165
lw	$6,%got($LC12)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC12)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L148
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L203:
lw	$2,8($16)
addiu	$2,$2,16
.set	noreorder
.set	nomacro
b	$L170
sw	$2,8($16)
.set	macro
.set	reorder

$L201:
lw	$3,11168($17)
.set	noreorder
.set	nomacro
bne	$3,$0,$L168
lw	$6,%got($LC15)($28)
.set	macro
.set	reorder

li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC15)
.set	macro
.set	reorder

lw	$28,72($sp)
.set	noreorder
.set	nomacro
b	$L168
lw	$2,0($16)
.set	macro
.set	reorder

$L197:
lw	$3,11168($17)
.set	noreorder
.set	nomacro
bne	$3,$0,$L162
lw	$6,%got($LC11)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)
.set	macro
.set	reorder

lw	$28,72($sp)
.set	noreorder
.set	nomacro
b	$L162
lw	$2,0($16)
.set	macro
.set	reorder

$L202:
lw	$6,%got($LC16)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L169
lw	$28,72($sp)
.set	macro
.set	reorder

$L195:
li	$2,1			# 0x1
sw	$2,11104($17)
lw	$2,%got(ff_vc1_adv_progressive_8x4_zz)($28)
lw	$8,8($16)
sw	$2,11232($17)
srl	$3,$8,3
lw	$2,%got(ff_vc1_adv_progressive_4x8_zz)($28)
andi	$7,$8,0x7
addiu	$8,$8,3
sw	$2,11236($17)
lw	$2,0($16)
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
sw	$8,8($16)
srl	$5,$4,8
addiu	$3,$3,255
sll	$6,$4,8
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$7,$3,$7
srl	$7,$7,29
sltu	$3,$7,5
.set	noreorder
.set	nomacro
beq	$3,$0,$L204
sw	$7,11112($17)
.set	macro
.set	reorder

$L146:
lw	$5,8($16)
li	$19,16711680			# 0xff0000
li	$18,-16777216			# 0xffffffffff000000
addiu	$19,$19,255
srl	$3,$5,3
ori	$18,$18,0xff00
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$7,8
sll	$3,$7,8
and	$4,$4,$19
and	$3,$3,$18
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$6,$5,0x7
or	$3,$4,$3
sll	$3,$3,$6
addiu	$5,$5,2
srl	$3,$3,30
li	$4,1			# 0x1
sw	$5,8($16)
.set	noreorder
.set	nomacro
bne	$3,$4,$L205
sw	$3,11116($17)
.set	macro
.set	reorder

lw	$7,8($16)
li	$5,48			# 0x30
lw	$8,0($17)
lw	$25,%call16(av_log)($28)
srl	$4,$7,3
andi	$9,$7,0x7
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$6,8
sll	$6,$6,8
and	$4,$4,$19
and	$6,$6,$18
or	$3,$4,$6
lw	$6,%got($LC5)($28)
sll	$4,$3,16
srl	$3,$3,16
addiu	$7,$7,3
or	$3,$4,$3
sll	$3,$3,$9
sw	$7,8($16)
move	$4,$8
srl	$3,$3,29
addiu	$6,$6,%lo($LC5)
sw	$3,11172($17)
lw	$10,8($16)
srl	$7,$10,3
andi	$11,$10,0x7
addu	$7,$2,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($7)  
lwr $9, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$9,8
sll	$9,$9,8
and	$7,$7,$19
and	$9,$9,$18
or	$3,$7,$9
sll	$7,$3,16
srl	$3,$3,16
addiu	$10,$10,5
or	$3,$7,$3
sll	$3,$3,$11
sw	$10,8($16)
srl	$3,$3,27
sw	$3,11176($17)
lw	$7,8($16)
srl	$9,$7,3
andi	$10,$7,0x7
addu	$9,$2,$9
addiu	$7,$7,1
lbu	$3,0($9)
sw	$7,8($16)
sll	$3,$3,$10
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11120($17)
lw	$10,8($16)
srl	$7,$10,3
andi	$11,$10,0x7
addu	$7,$2,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($7)  
lwr $9, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$9,8
sll	$9,$9,8
and	$7,$7,$19
and	$9,$9,$18
or	$9,$7,$9
sll	$7,$9,16
srl	$9,$9,16
addiu	$10,$10,12
or	$3,$7,$9
sll	$3,$3,$11
sw	$10,8($16)
srl	$3,$3,20
addiu	$3,$3,1
sll	$3,$3,1
sw	$3,660($8)
lw	$10,8($16)
srl	$7,$10,3
andi	$11,$10,0x7
addu	$7,$2,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($7)  
lwr $9, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$9,8
sll	$9,$9,8
and	$7,$7,$19
and	$9,$9,$18
or	$9,$7,$9
sll	$7,$9,16
srl	$9,$9,16
addiu	$10,$10,12
or	$3,$7,$9
sll	$3,$3,$11
sw	$10,8($16)
srl	$3,$3,20
lw	$7,660($8)
addiu	$3,$3,1
sll	$3,$3,1
sw	$7,40($8)
sw	$3,664($8)
sw	$3,44($8)
lw	$7,8($16)
srl	$9,$7,3
andi	$10,$7,0x7
addu	$9,$2,$9
addiu	$7,$7,1
lbu	$3,0($9)
sw	$7,8($16)
sll	$3,$3,$10
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11124($17)
lw	$7,8($16)
srl	$9,$7,3
andi	$10,$7,0x7
addu	$9,$2,$9
addiu	$7,$7,1
lbu	$3,0($9)
sw	$7,8($16)
sll	$3,$3,$10
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11128($17)
lw	$7,8($16)
srl	$9,$7,3
andi	$10,$7,0x7
addu	$9,$2,$9
addiu	$7,$7,1
lbu	$3,0($9)
sw	$7,8($16)
sll	$3,$3,$10
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,11132($17)
lw	$3,8($16)
srl	$9,$3,3
andi	$7,$3,0x7
addu	$2,$2,$9
addiu	$3,$3,1
lbu	$2,0($2)
sw	$3,8($16)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11204($17)
lw	$2,8($16)
addiu	$2,$2,1
sw	$2,8($16)
lw	$15,660($8)
lw	$14,664($8)
lw	$3,11132($17)
lw	$2,11204($17)
lw	$13,11172($17)
lw	$12,11176($17)
lw	$11,9900($17)
lw	$10,11116($17)
lw	$9,11124($17)
lw	$8,11128($17)
lw	$7,11112($17)
sw	$15,180($17)
sw	$14,184($17)
sw	$3,40($sp)
sw	$13,16($sp)
sw	$12,20($sp)
sw	$11,24($sp)
sw	$10,28($sp)
sw	$9,32($sp)
sw	$8,36($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,44($sp)
.set	macro
.set	reorder

lw	$3,8($16)
lw	$5,0($16)
lw	$28,72($sp)
srl	$4,$3,3
andi	$6,$3,0x7
addu	$4,$5,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L206
sw	$2,11164($17)
.set	macro
.set	reorder

li	$2,7			# 0x7
lw	$4,0($17)
sw	$2,112($4)
sw	$2,72($17)
lw	$2,8($16)
srl	$6,$2,3
andi	$7,$2,0x7
addu	$6,$5,$6
addiu	$2,$2,1
lbu	$3,0($6)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L207
sw	$2,8($16)
.set	macro
.set	reorder

$L150:
srl	$3,$2,3
andi	$6,$2,0x7
addu	$3,$5,$3
addiu	$4,$2,1
lbu	$3,0($3)
sw	$4,8($16)
sll	$2,$3,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L158
sw	$2,11160($17)
.set	macro
.set	reorder

$L159:
move	$2,$0
$L208:
lw	$31,100($sp)
lw	$20,96($sp)
lw	$19,92($sp)
lw	$18,88($sp)
lw	$17,84($sp)
lw	$16,80($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,104
.set	macro
.set	reorder

$L158:
lw	$6,8($16)
srl	$2,$6,3
addu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
#NO_APP
srl	$4,$2,8
sll	$5,$2,8
li	$2,16711680			# 0xff0000
addiu	$2,$2,255
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$4,$6,0x7
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$6,$6,5
or	$2,$3,$2
sll	$2,$2,$4
sw	$6,8($16)
srl	$2,$2,27
sw	$2,11880($17)
lw	$3,8($16)
addiu	$3,$3,8
sw	$3,8($16)
lw	$2,11880($17)
.set	noreorder
.set	nomacro
blez	$2,$L159
move	$4,$0
.set	macro
.set	reorder

addiu	$3,$3,32
$L210:
addiu	$4,$4,1
sw	$3,8($16)
lw	$2,11880($17)
slt	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L210
addiu	$3,$3,32
.set	macro
.set	reorder

addiu	$3,$3,-32
.set	noreorder
.set	nomacro
b	$L208
move	$2,$0
.set	macro
.set	reorder

$L194:
lw	$6,%got($LC2)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC2)
.set	macro
.set	reorder

lw	$28,72($sp)
.set	noreorder
.set	nomacro
b	$L144
lw	$2,11168($17)
.set	macro
.set	reorder

$L204:
lw	$6,%got($LC3)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

lw	$28,72($sp)
.set	noreorder
.set	nomacro
b	$L146
lw	$2,0($16)
.set	macro
.set	reorder

$L207:
lw	$6,%got($LC7)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

li	$5,48			# 0x30
lw	$7,8($16)
lw	$8,0($16)
lw	$9,0($17)
srl	$3,$7,3
lw	$28,72($sp)
andi	$10,$7,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
lw	$25,%call16(av_log)($28)
sll	$6,$4,8
and	$3,$3,$19
and	$6,$6,$18
or	$6,$3,$6
sll	$3,$6,16
srl	$6,$6,16
addiu	$7,$7,14
or	$2,$3,$6
lw	$6,%got($LC8)($28)
sll	$2,$2,$10
sw	$7,8($16)
move	$4,$9
srl	$2,$2,18
addiu	$6,$6,%lo($LC8)
addiu	$2,$2,1
move	$7,$2
sw	$2,40($9)
lw	$10,8($16)
srl	$3,$10,3
andi	$11,$10,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$8,8
sll	$8,$8,8
and	$3,$3,$19
and	$8,$8,$18
or	$8,$3,$8
sll	$3,$8,16
srl	$8,$8,16
addiu	$10,$10,14
or	$2,$3,$8
sll	$2,$2,$11
sw	$10,8($16)
srl	$2,$2,18
addiu	$2,$2,1
sw	$2,44($9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$3,8($16)
lw	$6,0($16)
lw	$28,72($sp)
srl	$4,$3,3
andi	$7,$3,0x7
addu	$4,$6,$4
addiu	$5,$3,1
lbu	$2,0($4)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L209
sw	$5,8($16)
.set	macro
.set	reorder

$L155:
lw	$4,0($17)
lw	$7,392($4)
lw	$2,396($4)
$L152:
lw	$6,%got($LC9)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC9)
.set	macro
.set	reorder

lw	$4,8($16)
lw	$5,0($16)
lw	$28,72($sp)
srl	$6,$4,3
andi	$7,$4,0x7
addu	$6,$5,$6
addiu	$3,$4,1
lbu	$2,0($6)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L156
sw	$3,8($16)
.set	macro
.set	reorder

srl	$6,$3,3
andi	$3,$3,0x7
addu	$6,$5,$6
addiu	$7,$4,2
lbu	$2,0($6)
sll	$2,$2,$3
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L157
sw	$7,8($16)
.set	macro
.set	reorder

lw	$8,0($17)
li	$2,32			# 0x20
li	$7,16711680			# 0xff0000
sw	$2,32($8)
addiu	$7,$7,255
lw	$6,8($16)
srl	$3,$6,3
andi	$9,$6,0x7
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
addiu	$6,$6,16
ori	$7,$7,0xff00
and	$4,$4,$7
or	$4,$3,$4
sw	$6,8($16)
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$9
srl	$2,$2,16
addiu	$2,$2,1
sw	$2,36($8)
lw	$3,8($16)
$L156:
srl	$6,$3,3
$L211:
andi	$4,$3,0x7
addu	$6,$5,$6
addiu	$2,$3,1
lbu	$6,0($6)
sll	$4,$6,$4
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L150
sw	$2,8($16)
.set	macro
.set	reorder

srl	$4,$2,3
li	$8,16711680			# 0xff0000
li	$7,-16777216			# 0xffffffffff000000
addiu	$8,$8,255
addu	$4,$5,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$6,8
sll	$6,$6,8
ori	$7,$7,0xff00
and	$6,$6,$7
and	$4,$4,$8
or	$4,$4,$6
sll	$6,$4,16
srl	$4,$4,16
andi	$2,$2,0x7
or	$4,$6,$4
sll	$4,$4,$2
addiu	$3,$3,9
srl	$4,$4,24
sw	$3,8($16)
sw	$4,11148($17)
lw	$6,8($16)
srl	$2,$6,3
andi	$9,$6,0x7
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$2,$2,8
srl	$3,$3,8
and	$2,$2,$7
and	$3,$3,$8
or	$3,$3,$2
sll	$4,$3,16
srl	$3,$3,16
addiu	$6,$6,8
or	$3,$4,$3
sll	$3,$3,$9
sw	$6,8($16)
srl	$3,$3,24
sw	$3,11152($17)
lw	$4,8($16)
srl	$3,$4,3
andi	$6,$4,0x7
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sll	$3,$3,8
srl	$2,$2,8
and	$7,$3,$7
and	$2,$2,$8
or	$2,$2,$7
sll	$3,$2,16
srl	$2,$2,16
addiu	$4,$4,8
or	$2,$3,$2
sll	$2,$2,$6
sw	$4,8($16)
srl	$2,$2,24
sw	$2,11156($17)
.set	noreorder
.set	nomacro
b	$L150
lw	$2,8($16)
.set	macro
.set	reorder

$L209:
srl	$2,$5,3
andi	$5,$5,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$19
and	$4,$4,$18
or	$2,$2,$4
sll	$4,$2,16
srl	$2,$2,16
addiu	$7,$3,5
or	$2,$4,$2
sll	$2,$2,$5
srl	$2,$2,28
.set	noreorder
.set	nomacro
beq	$2,$0,$L155
sw	$7,8($16)
.set	macro
.set	reorder

slt	$4,$2,14
beq	$4,$0,$L154
lw	$3,%got(ff_vc1_pixel_aspect)($28)
sll	$2,$2,3
lw	$4,0($17)
addu	$2,$3,$2
lw	$5,0($2)
lw	$3,4($2)
move	$7,$5
sw	$5,392($4)
move	$2,$3
.set	noreorder
.set	nomacro
b	$L152
sw	$3,396($4)
.set	macro
.set	reorder

$L196:
lw	$6,%got($LC10)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L148
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L205:
lw	$6,%got($LC4)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L148
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L157:
srl	$2,$7,3
li	$6,16711680			# 0xff0000
li	$8,-16777216			# 0xffffffffff000000
addu	$2,$5,$2
addiu	$6,$6,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
ori	$8,$8,0xff00
and	$3,$3,$8
and	$2,$2,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$7,$7,0x7
or	$2,$3,$2
addiu	$9,$4,10
sll	$2,$2,$7
addiu	$3,$4,14
srl	$2,$2,24
sw	$9,8($16)
srl	$4,$9,3
addu	$4,$5,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($4)  
lwr $7, 0($4)  

# 0 "" 2
#NO_APP
sw	$3,8($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L156
move	$4,$7
.set	macro
.set	reorder

sltu	$7,$2,8
.set	noreorder
.set	nomacro
beq	$7,$0,$L156
sll	$7,$4,8
.set	macro
.set	reorder

srl	$4,$4,8
and	$8,$7,$8
and	$6,$4,$6
or	$6,$8,$6
sll	$4,$6,16
srl	$6,$6,16
andi	$9,$9,0x7
or	$4,$4,$6
sll	$9,$4,$9
srl	$4,$9,28
.set	noreorder
.set	nomacro
beq	$4,$0,$L211
srl	$6,$3,3
.set	macro
.set	reorder

sltu	$6,$4,3
.set	noreorder
.set	nomacro
beq	$6,$0,$L211
srl	$6,$3,3
.set	macro
.set	reorder

addiu	$2,$2,-1
lw	$6,0($17)
addiu	$3,$4,-1
lw	$4,%got(ff_vc1_fps_nr)($28)
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$4,$2
lw	$4,%got(ff_vc1_fps_dr)($28)
lw	$2,0($2)
addu	$3,$4,$3
li	$4,1000			# 0x3e8
mul	$2,$2,$4
lw	$3,0($3)
sw	$3,32($6)
sw	$2,36($6)
.set	noreorder
.set	nomacro
b	$L156
lw	$3,8($16)
.set	macro
.set	reorder

$L154:
li	$4,15			# 0xf
.set	noreorder
.set	nomacro
bne	$2,$4,$L155
addiu	$9,$3,13
.set	macro
.set	reorder

lw	$4,0($17)
srl	$2,$7,3
srl	$5,$9,3
addu	$2,$6,$2
addu	$6,$6,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
sw	$9,8($16)
srl	$2,$5,8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($6)  
lwr $8, 0($6)  

# 0 "" 2
#NO_APP
move	$6,$8
sll	$8,$5,8
srl	$5,$6,8
sll	$6,$6,8
and	$2,$2,$19
and	$8,$8,$18
and	$19,$5,$19
and	$5,$6,$18
or	$2,$2,$8
or	$5,$19,$5
sll	$6,$2,16
srl	$8,$2,16
sll	$2,$5,16
srl	$5,$5,16
or	$6,$6,$8
or	$2,$2,$5
andi	$7,$7,0x7
andi	$5,$9,0x7
sll	$7,$6,$7
sll	$5,$2,$5
srl	$6,$7,24
srl	$5,$5,24
addiu	$3,$3,21
move	$2,$5
move	$7,$6
sw	$3,8($16)
sw	$6,392($4)
.set	noreorder
.set	nomacro
b	$L152
sw	$5,396($4)
.set	macro
.set	reorder

$L200:
lw	$6,%got($LC14)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L148
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L199:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L148
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L206:
lw	$6,%got($LC6)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L148
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	vc1_decode_sequence_header
.size	vc1_decode_sequence_header, .-vc1_decode_sequence_header
.section	.rodata.str1.4
.align	2
$LC18:
.ascii	"Entry point: %08X\012\000"
.align	2
$LC19:
.ascii	"Luma scaling is not supported, expect wrong picture\012\000"
.align	2
$LC20:
.ascii	"Chroma scaling is not supported, expect wrong picture\012"
.ascii	"\000"
.align	2
$LC21:
.ascii	"Entry point info:\012BrokenLink=%i, ClosedEntry=%i, Pans"
.ascii	"canFlag=%i\012RefDist=%i, Postproc=%i, FastUVMC=%i, ExtM"
.ascii	"V=%i\012DQuant=%i, VSTransform=%i, Overlap=%i, Qmode=%i\012"
.ascii	"\000"
.section	.text.vc1_decode_entry_point,"ax",@progbits
.align	2
.align	5
.globl	vc1_decode_entry_point
.set	nomips16
.set	nomicromips
.ent	vc1_decode_entry_point
.type	vc1_decode_entry_point, @function
vc1_decode_entry_point:
.frame	$sp,88,$31		# vars= 0, regs= 6/0, args= 56, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$11,8($6)
addiu	$sp,$sp,-88
li	$3,16711680			# 0xff0000
lw	$7,0($6)
sw	$20,80($sp)
addiu	$10,$11,16
sw	$19,76($sp)
addiu	$20,$3,255
sw	$16,64($sp)
srl	$3,$11,3
sw	$17,68($sp)
li	$19,-16777216			# 0xffffffffff000000
.cprestore	56
move	$16,$6
sw	$18,72($sp)
addu	$3,$7,$3
sw	$31,84($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($3)  
lwr $9, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$10,3
lw	$25,%call16(av_log)($28)
srl	$6,$9,8
sll	$9,$9,8
ori	$19,$19,0xff00
addu	$2,$7,$2
and	$6,$6,$20
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
and	$9,$9,$19
srl	$7,$8,8
sll	$8,$8,8
or	$9,$6,$9
sll	$6,$9,16
srl	$2,$9,16
and	$7,$7,$20
and	$8,$8,$19
or	$8,$7,$8
or	$3,$6,$2
lw	$6,%got($LC18)($28)
sll	$7,$8,16
andi	$2,$11,0x7
srl	$8,$8,16
sll	$2,$3,$2
or	$8,$7,$8
andi	$7,$10,0x7
sll	$7,$8,$7
srl	$2,$2,16
srl	$7,$7,16
sll	$2,$2,16
move	$17,$5
addiu	$6,$6,%lo($LC18)
li	$5,48			# 0x30
or	$7,$2,$7
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$18,$4
.set	macro
.set	reorder

lw	$3,8($16)
lw	$4,0($16)
lw	$28,56($sp)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11945($17)
lw	$3,8($16)
lw	$4,0($16)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11946($17)
lw	$3,8($16)
lw	$6,0($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11136($17)
lw	$3,8($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11140($17)
lw	$3,8($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,9900($17)
lw	$3,8($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11180($17)
lw	$3,8($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11184($17)
lw	$3,8($16)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$4,8
sll	$2,$4,8
and	$5,$5,$20
and	$2,$2,$19
or	$4,$5,$2
sll	$2,$4,16
srl	$4,$4,16
addiu	$3,$3,2
or	$4,$2,$4
sll	$4,$4,$7
sw	$3,8($16)
srl	$4,$4,30
sw	$4,11188($17)
lw	$3,8($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11192($17)
lw	$3,8($16)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11196($17)
lw	$5,8($16)
srl	$2,$5,3
addiu	$7,$5,2
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sw	$7,8($16)
sll	$4,$4,8
lw	$7,11160($17)
and	$3,$3,$20
and	$19,$4,$19
or	$2,$3,$19
sll	$3,$2,16
srl	$2,$2,16
andi	$5,$5,0x7
or	$2,$3,$2
sll	$2,$2,$5
srl	$2,$2,30
.set	noreorder
.set	nomacro
bne	$7,$0,$L213
sw	$2,11200($17)
.set	macro
.set	reorder

$L215:
lw	$3,8($16)
srl	$5,$3,3
andi	$4,$3,0x7
addu	$5,$6,$5
addiu	$2,$3,1
lbu	$5,0($5)
sll	$4,$5,$4
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L235
sw	$2,8($16)
.set	macro
.set	reorder

$L217:
lw	$3,11184($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L237
srl	$3,$2,3
.set	macro
.set	reorder

andi	$4,$2,0x7
addu	$3,$6,$3
addiu	$2,$2,1
lbu	$3,0($3)
sw	$2,8($16)
sll	$2,$3,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,11144($17)
lw	$2,8($16)
srl	$3,$2,3
$L237:
andi	$4,$2,0x7
addu	$6,$6,$3
addiu	$2,$2,1
lbu	$3,0($6)
sw	$2,8($16)
sll	$2,$3,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L236
sb	$2,11920($17)
.set	macro
.set	reorder

$L219:
lw	$3,8($16)
lw	$4,0($16)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L220
sb	$2,11921($17)
.set	macro
.set	reorder

lw	$6,%got($LC20)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC20)
.set	macro
.set	reorder

lw	$6,8($16)
lw	$3,0($16)
lw	$28,56($sp)
srl	$2,$6,3
andi	$7,$6,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sll	$5,$3,8
li	$3,16711680			# 0xff0000
srl	$4,$4,8
addiu	$3,$3,255
addiu	$6,$6,3
and	$3,$4,$3
li	$4,-16777216			# 0xffffffffff000000
sw	$6,8($16)
ori	$4,$4,0xff00
and	$4,$5,$4
or	$2,$3,$4
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$7
srl	$2,$2,29
sb	$2,11923($17)
$L220:
lbu	$15,11946($17)
li	$5,48			# 0x30
lw	$14,11136($17)
move	$4,$18
lw	$13,11140($17)
lw	$12,9900($17)
lw	$11,11180($17)
lw	$10,11184($17)
lw	$9,11188($17)
lw	$8,11192($17)
lw	$3,11196($17)
lw	$2,11200($17)
lw	$6,%got($LC21)($28)
lbu	$7,11945($17)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC21)
sw	$15,16($sp)
sw	$14,20($sp)
sw	$13,24($sp)
sw	$12,28($sp)
sw	$11,32($sp)
sw	$10,36($sp)
sw	$9,40($sp)
sw	$8,44($sp)
sw	$3,48($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,52($sp)
.set	macro
.set	reorder

move	$2,$0
lw	$31,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L213:
lw	$2,11880($17)
.set	noreorder
.set	nomacro
blez	$2,$L215
move	$5,$0
.set	macro
.set	reorder

lw	$3,8($16)
addiu	$3,$3,8
$L238:
addiu	$5,$5,1
sw	$3,8($16)
lw	$2,11880($17)
slt	$2,$5,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L238
addiu	$3,$3,8
.set	macro
.set	reorder

addiu	$3,$3,-8
srl	$5,$3,3
andi	$4,$3,0x7
addu	$5,$6,$5
addiu	$2,$3,1
lbu	$5,0($5)
sll	$4,$5,$4
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L217
sw	$2,8($16)
.set	macro
.set	reorder

$L235:
srl	$4,$2,3
li	$9,16711680			# 0xff0000
addu	$4,$6,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($4)  
lwr $7, 0($4)  

# 0 "" 2
#NO_APP
li	$4,-16777216			# 0xffffffffff000000
addiu	$9,$9,255
srl	$5,$7,8
sll	$7,$7,8
ori	$8,$4,0xff00
and	$5,$5,$9
and	$7,$7,$8
or	$7,$5,$7
sll	$5,$7,16
srl	$7,$7,16
andi	$2,$2,0x7
or	$4,$5,$7
sll	$2,$4,$2
addiu	$3,$3,13
srl	$2,$2,20
addiu	$2,$2,1
sw	$3,8($16)
sll	$2,$2,1
sw	$2,660($18)
lw	$7,8($16)
srl	$3,$7,3
andi	$10,$7,0x7
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$9
and	$4,$5,$8
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
addiu	$7,$7,12
or	$2,$3,$4
sll	$2,$2,$10
sw	$7,8($16)
srl	$2,$2,20
addiu	$2,$2,1
sll	$2,$2,1
sw	$2,664($18)
.set	noreorder
.set	nomacro
b	$L217
lw	$2,8($16)
.set	macro
.set	reorder

$L236:
lw	$6,%got($LC19)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC19)
.set	macro
.set	reorder

lw	$6,8($16)
lw	$3,0($16)
lw	$28,56($sp)
srl	$2,$6,3
andi	$7,$6,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
srl	$4,$2,8
addiu	$3,$3,255
sll	$5,$2,8
and	$3,$4,$3
li	$4,-16777216			# 0xffffffffff000000
addiu	$6,$6,3
ori	$4,$4,0xff00
and	$4,$5,$4
or	$2,$3,$4
sw	$6,8($16)
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$7
srl	$2,$2,29
.set	noreorder
.set	nomacro
b	$L219
sb	$2,11922($17)
.set	macro
.set	reorder

.end	vc1_decode_entry_point
.size	vc1_decode_entry_point, .-vc1_decode_entry_point
.section	.rodata.str1.4
.align	2
$LC22:
.ascii	"MB MV Type plane encoding: Imode: %i, Invert: %i\012\000"
.align	2
$LC23:
.ascii	"MB Skip plane encoding: Imode: %i, Invert: %i\012\000"
.align	2
$LC24:
.ascii	"VOP DQuant info\012\000"
.align	2
$LC25:
.ascii	"MB Direct Type plane encoding: Imode: %i, Invert: %i\012"
.ascii	"\000"
.section	.text.vc1_parse_frame_header,"ax",@progbits
.align	2
.align	5
.globl	vc1_parse_frame_header
.set	nomips16
.set	nomicromips
.ent	vc1_parse_frame_header
.type	vc1_parse_frame_header, @function
vc1_parse_frame_header:
.frame	$sp,56,$31		# vars= 0, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-56
lw	$2,11204($4)
.cprestore	24
sw	$31,52($sp)
sw	$19,48($sp)
sw	$18,44($sp)
sw	$17,40($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L240
sw	$16,36($sp)
.set	macro
.set	reorder

lw	$3,8($5)
lw	$6,0($5)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$6,$6,$2
addiu	$3,$3,1
lbu	$2,0($6)
sw	$3,8($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11861($4)
$L240:
lw	$2,8($5)
addiu	$2,$2,2
sw	$2,8($5)
sb	$0,11860($4)
lw	$2,11100($4)
beq	$2,$0,$L241
lw	$3,8($5)
lw	$6,0($5)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$6,$6,$2
addiu	$3,$3,1
lbu	$2,0($6)
sw	$3,8($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11860($4)
$L241:
lw	$3,8($5)
lw	$7,0($5)
lw	$9,0($4)
srl	$6,$3,3
andi	$8,$3,0x7
addu	$6,$7,$6
addiu	$3,$3,1
lbu	$2,0($6)
sw	$3,8($5)
lw	$3,112($9)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L242
sw	$2,2904($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L243
li	$3,2			# 0x2
.set	macro
.set	reorder

lw	$3,8($5)
srl	$6,$3,3
andi	$8,$3,0x7
addu	$6,$7,$6
addiu	$3,$3,1
lbu	$2,0($6)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L363
sw	$3,8($5)
.set	macro
.set	reorder

li	$2,3			# 0x3
sw	$0,11928($4)
sw	$2,2904($4)
lw	$9,8($5)
lw	$2,%got(ff_vc1_bfraction_vlc)($28)
srl	$3,$9,3
addu	$7,$7,$3
lw	$10,4($2)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($7)  
lwr $3, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$3,8
sll	$8,$3,8
li	$3,16711680			# 0xff0000
andi	$2,$9,0x7
addiu	$3,$3,255
and	$6,$7,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$8,$3
or	$3,$6,$3
sll	$6,$3,16
srl	$3,$3,16
or	$3,$6,$3
sll	$2,$3,$2
srl	$2,$2,25
sll	$2,$2,2
addu	$3,$10,$2
lh	$2,0($3)
lh	$3,2($3)
andi	$2,$2,0x00ff
addu	$9,$9,$3
lw	$3,%got(ff_vc1_bfraction_lut)($28)
sll	$6,$2,1
addu	$3,$3,$6
sw	$9,8($5)
sb	$2,11944($4)
lh	$2,0($3)
.set	noreorder
.set	nomacro
bne	$2,$0,$L364
sh	$2,11298($4)
.set	macro
.set	reorder

li	$2,7			# 0x7
sw	$2,2904($4)
$L250:
lw	$2,8($5)
addiu	$2,$2,7
sw	$2,8($5)
$L375:
lw	$18,11948($4)
beq	$18,$0,$L253
$L312:
move	$18,$0
$L351:
lw	$31,52($sp)
move	$2,$18
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L242:
li	$3,1			# 0x1
sw	$0,11928($4)
li	$6,2			# 0x2
movn	$3,$6,$2
move	$2,$3
sw	$3,2904($4)
$L249:
li	$3,1			# 0x1
$L380:
beq	$2,$3,$L250
$L252:
lw	$18,11948($4)
bne	$18,$0,$L312
$L253:
lw	$2,0($5)
sw	$0,11856($4)
lw	$9,8($5)
srl	$3,$9,3
andi	$10,$9,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$7,$3,8
addiu	$2,$2,255
sll	$8,$3,8
and	$6,$7,$2
li	$2,-16777216			# 0xffffffffff000000
addiu	$9,$9,5
ori	$2,$2,0xff00
and	$2,$8,$2
or	$2,$6,$2
sll	$6,$2,16
srl	$2,$2,16
or	$3,$6,$2
sll	$3,$3,$10
srl	$3,$3,27
.set	noreorder
.set	nomacro
beq	$3,$0,$L297
sw	$9,8($5)
.set	macro
.set	reorder

lw	$2,11200($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L365
lw	$6,%got(ff_vc1_pquant_table)($28)
.set	macro
.set	reorder

addu	$6,$6,$3
lbu	$6,32($6)
sb	$6,11228($4)
li	$6,2			# 0x2
.set	noreorder
.set	nomacro
bne	$2,$6,$L366
li	$2,1			# 0x1
.set	macro
.set	reorder

sb	$0,11309($4)
$L257:
sltu	$2,$3,9
.set	noreorder
.set	nomacro
beq	$2,$0,$L259
sw	$3,11268($4)
.set	macro
.set	reorder

$L372:
lw	$3,8($5)
lw	$6,0($5)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$6,$6,$2
addiu	$3,$3,1
lbu	$2,0($6)
sw	$3,8($5)
lw	$3,11200($4)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11300($4)
li	$2,1			# 0x1
beq	$3,$2,$L367
$L261:
lw	$2,11184($4)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L262
sb	$0,11240($4)
.set	macro
.set	reorder

$L373:
lbu	$2,11308($4)
srl	$8,$2,1
addiu	$6,$2,9
addiu	$7,$2,7
addu	$6,$6,$8
sll	$7,$3,$7
addiu	$8,$6,-1
addiu	$2,$2,8
sll	$3,$3,$8
$L263:
lw	$8,11088($4)
sw	$6,11212($4)
sw	$2,11216($4)
sw	$3,11220($4)
.set	noreorder
.set	nomacro
beq	$8,$0,$L267
sw	$7,11224($4)
.set	macro
.set	reorder

lw	$2,2904($4)
li	$3,3			# 0x3
beq	$2,$3,$L268
lw	$8,8($5)
lw	$2,0($5)
srl	$3,$8,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($2)  
lwr $25, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$6,$25,8
addiu	$2,$2,255
sll	$7,$25,8
and	$3,$6,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$6,$8,0x7
ori	$2,$2,0xff00
and	$2,$7,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$8,$8,2
or	$2,$3,$2
sll	$2,$2,$6
sw	$8,8($5)
srl	$2,$2,30
sb	$2,11301($4)
$L267:
lw	$2,11084($4)
.set	noreorder
.set	nomacro
bne	$2,$0,$L269
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,2904($4)
$L268:
sw	$0,11932($4)
$L271:
addiu	$3,$2,-1
sltu	$3,$3,2
bne	$3,$0,$L368
$L272:
li	$3,2			# 0x2
move	$17,$5
.set	noreorder
.set	nomacro
beq	$2,$3,$L274
move	$16,$4
.set	macro
.set	reorder

li	$4,3			# 0x3
bne	$2,$4,$L273
lbu	$2,11228($16)
sltu	$4,$2,5
.set	noreorder
.set	nomacro
beq	$4,$0,$L302
sltu	$2,$2,13
.set	macro
.set	reorder

sw	$0,11316($16)
$L303:
lw	$3,8($17)
addiu	$5,$16,11332
lw	$7,0($17)
move	$6,$16
lw	$19,%got(bitplane_decoding)($28)
srl	$2,$3,3
lw	$4,11324($16)
andi	$8,$3,0x7
addu	$7,$7,$2
addiu	$3,$3,1
addiu	$19,$19,%lo(bitplane_decoding)
lbu	$2,0($7)
move	$25,$19
sw	$3,8($17)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11208($16)
sw	$2,10044($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
sw	$2,10328($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L297
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC25)($28)
andi	$3,$2,0x1
lw	$4,0($16)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
sra	$7,$2,1
addiu	$6,$6,%lo($LC25)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

addiu	$5,$16,11336
lw	$4,2836($16)
$L361:
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L297
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC23)($28)
andi	$3,$2,0x1
lw	$4,0($16)
sra	$7,$2,1
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
addiu	$6,$6,%lo($LC23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

li	$3,16711680			# 0xff0000
lw	$9,8($17)
li	$4,-16777216			# 0xffffffffff000000
lw	$8,0($17)
addiu	$10,$3,255
ori	$4,$4,0xff00
lw	$28,24($sp)
srl	$2,$9,3
andi	$3,$9,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
srl	$6,$7,8
sll	$7,$7,8
and	$6,$6,$10
and	$7,$7,$4
or	$5,$6,$7
sll	$6,$5,16
srl	$5,$5,16
addiu	$9,$9,2
or	$5,$6,$5
sll	$5,$5,$3
sw	$9,8($17)
srl	$5,$5,30
sw	$5,10272($16)
lw	$6,8($17)
srl	$7,$6,3
andi	$9,$6,0x7
addu	$7,$8,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($7)  
lwr $5, 0($7)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$10
and	$4,$5,$4
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
addiu	$6,$6,2
or	$2,$3,$4
lw	$3,%got(ff_vc1_cbpcy_p_vlc)($28)
sll	$2,$2,$9
sw	$6,8($17)
srl	$2,$2,30
lw	$4,11188($16)
sll	$2,$2,4
addu	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L369
sw	$2,11312($16)
.set	macro
.set	reorder

$L305:
lw	$2,11192($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L300
sw	$0,11252($16)
.set	macro
.set	reorder

lw	$3,8($17)
lw	$4,0($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L370
sb	$2,11256($16)
.set	macro
.set	reorder

$L273:
lw	$2,11932($16)
beq	$2,$0,$L371
$L307:
lw	$3,2904($16)
li	$2,7			# 0x7
.set	noreorder
.set	nomacro
bne	$3,$2,$L312
li	$2,3			# 0x3
.set	macro
.set	reorder

lw	$31,52($sp)
lw	$19,48($sp)
lw	$17,40($sp)
sw	$2,2904($16)
li	$2,1			# 0x1
sw	$2,11928($16)
move	$2,$18
lw	$18,44($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L243:
sw	$0,11928($4)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
b	$L249
sw	$3,2904($4)
.set	macro
.set	reorder

$L365:
slt	$2,$3,9
sw	$3,11268($4)
sb	$2,11309($4)
lw	$2,%got(ff_vc1_pquant_table)($28)
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,11228($4)
sltu	$2,$3,9
bne	$2,$0,$L372
$L259:
lw	$3,11200($4)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L261
sb	$0,11300($4)
.set	macro
.set	reorder

$L367:
lw	$3,8($5)
lw	$6,0($5)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$6,$6,$2
addiu	$3,$3,1
lbu	$2,0($6)
sw	$3,8($5)
li	$3,1			# 0x1
sb	$0,11240($4)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11309($4)
lw	$2,11184($4)
bne	$2,$3,$L373
$L262:
lw	$9,0($5)
move	$2,$0
lw	$6,8($5)
li	$10,3			# 0x3
$L264:
srl	$7,$6,3
andi	$8,$6,0x7
addu	$7,$9,$7
addiu	$6,$6,1
lbu	$3,0($7)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L374
sw	$6,8($5)
.set	macro
.set	reorder

addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$10,$L264
li	$8,3			# 0x3
.set	macro
.set	reorder

li	$7,1024			# 0x400
li	$3,4096			# 0x1000
li	$2,11			# 0xb
sb	$8,11308($4)
.set	noreorder
.set	nomacro
b	$L263
li	$6,13			# 0xd
.set	macro
.set	reorder

$L363:
li	$3,1			# 0x1
sw	$0,11928($4)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L380
sw	$3,2904($4)
.set	macro
.set	reorder

$L269:
lw	$2,2904($4)
.set	noreorder
.set	nomacro
beq	$2,$3,$L270
li	$3,7			# 0x7
.set	macro
.set	reorder

bne	$2,$3,$L268
$L270:
lw	$2,8($5)
lw	$6,0($5)
srl	$3,$2,3
andi	$7,$2,0x7
addu	$6,$6,$3
addiu	$2,$2,1
lbu	$3,0($6)
sw	$2,8($5)
lw	$2,2904($4)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
b	$L271
sw	$3,11932($4)
.set	macro
.set	reorder

$L364:
lw	$3,2904($4)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L250
li	$2,7			# 0x7
.set	macro
.set	reorder

bne	$3,$2,$L252
lw	$2,8($5)
addiu	$2,$2,7
.set	noreorder
.set	nomacro
b	$L375
sw	$2,8($5)
.set	macro
.set	reorder

$L274:
lbu	$6,11228($4)
sltu	$3,$6,5
beq	$3,$0,$L276
sw	$0,11316($4)
sltu	$6,$6,13
$L277:
lw	$9,0($17)
move	$5,$0
lw	$3,8($17)
li	$8,1			# 0x1
li	$10,4			# 0x4
srl	$4,$3,3
$L377:
andi	$7,$3,0x7
addu	$4,$9,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$8,$L280
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$5,$10,$L377
srl	$4,$3,3
.set	macro
.set	reorder

$L280:
sll	$7,$6,2
li	$3,4			# 0x4
addu	$6,$7,$6
addu	$5,$6,$5
lw	$6,%got(ff_vc1_mv_pmode_table)($28)
addu	$5,$6,$5
lbu	$2,0($5)
.set	noreorder
.set	nomacro
beq	$2,$3,$L376
sb	$2,11208($16)
.set	macro
.set	reorder

andi	$3,$2,0xfd
.set	noreorder
.set	nomacro
bne	$3,$0,$L378
li	$3,1			# 0x1
.set	macro
.set	reorder

$L313:
.set	noreorder
.set	nomacro
bne	$2,$0,$L290
sw	$0,10044($16)
.set	macro
.set	reorder

lhu	$3,11208($16)
$L291:
move	$4,$0
sw	$4,10328($16)
$L381:
li	$4,772			# 0x304
.set	noreorder
.set	nomacro
beq	$3,$4,$L295
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L295
move	$5,$0
.set	macro
.set	reorder

lw	$6,168($16)
lw	$2,164($16)
lw	$25,%call16(memset)($28)
lw	$4,11320($16)
mul	$6,$6,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$0,11328($16)
.set	macro
.set	reorder

addiu	$5,$16,11336
lw	$28,24($sp)
lw	$4,2836($16)
lw	$19,%got(bitplane_decoding)($28)
.set	noreorder
.set	nomacro
b	$L361
addiu	$19,$19,%lo(bitplane_decoding)
.set	macro
.set	reorder

$L300:
li	$2,1			# 0x1
sb	$2,11256($16)
lw	$2,11932($16)
bne	$2,$0,$L307
$L371:
lw	$5,8($17)
lw	$4,0($17)
srl	$7,$5,3
andi	$8,$5,0x7
addu	$7,$4,$7
addiu	$6,$5,1
lbu	$3,0($7)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L318
sw	$6,8($17)
.set	macro
.set	reorder

srl	$3,$6,3
andi	$6,$6,0x7
addu	$3,$4,$3
addiu	$5,$5,2
lbu	$3,0($3)
sw	$5,8($17)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
addiu	$3,$3,1
$L308:
lw	$5,2904($16)
sw	$3,11244($16)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$5,$3,$L309
li	$3,7			# 0x7
.set	macro
.set	reorder

beq	$5,$3,$L309
$L310:
lw	$3,8($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
b	$L307
sw	$2,10284($16)
.set	macro
.set	reorder

$L368:
.set	noreorder
.set	nomacro
b	$L272
sw	$0,11852($4)
.set	macro
.set	reorder

$L366:
.set	noreorder
.set	nomacro
b	$L257
sb	$2,11309($4)
.set	macro
.set	reorder

$L295:
lw	$19,%got(bitplane_decoding)($28)
addiu	$5,$16,11328
lw	$4,11320($16)
addiu	$19,$19,%lo(bitplane_decoding)
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L297
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC22)($28)
andi	$3,$2,0x1
lw	$4,0($16)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
sra	$7,$2,1
addiu	$6,$6,%lo($LC22)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

addiu	$5,$16,11336
.set	noreorder
.set	nomacro
b	$L361
lw	$4,2836($16)
.set	macro
.set	reorder

$L369:
lw	$6,%got($LC24)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC24)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$25,%got(vop_dquant_decoding)($28)
addiu	$25,$25,%lo(vop_dquant_decoding)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vop_dquant_decoding
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L305
lw	$28,24($sp)
.set	macro
.set	reorder

$L376:
lw	$9,0($17)
move	$5,$0
lw	$3,8($17)
li	$8,1			# 0x1
li	$10,3			# 0x3
srl	$4,$3,3
$L379:
andi	$6,$3,0x7
addu	$4,$9,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$8,$L284
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$5,$10,$L379
srl	$4,$3,3
.set	macro
.set	reorder

$L284:
lw	$2,%got(ff_vc1_mv_pmode_table2)($28)
addu	$7,$7,$5
li	$3,16711680			# 0xff0000
addu	$7,$2,$7
addiu	$8,$3,255
li	$2,-16777216			# 0xffffffffff000000
lbu	$3,0($7)
ori	$2,$2,0xff00
sb	$3,11209($16)
lw	$7,8($17)
lw	$3,0($17)
srl	$4,$7,3
andi	$9,$7,0x7
addu	$3,$3,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$6,$5,8
and	$5,$4,$8
and	$6,$6,$2
or	$4,$5,$6
sll	$5,$4,16
srl	$4,$4,16
addiu	$7,$7,6
or	$4,$5,$4
sll	$4,$4,$9
sw	$7,8($17)
srl	$4,$4,26
sb	$4,11296($16)
lw	$5,8($17)
lw	$3,0($17)
srl	$4,$5,3
addiu	$6,$5,6
addu	$3,$3,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sw	$6,8($17)
sll	$4,$4,8
and	$3,$3,$8
and	$2,$4,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
li	$6,1			# 0x1
andi	$5,$5,0x7
or	$2,$3,$2
sll	$2,$2,$5
sw	$6,11852($16)
lbu	$6,11296($16)
srl	$2,$2,26
.set	noreorder
.set	nomacro
bne	$6,$0,$L286
sb	$2,11297($16)
.set	macro
.set	reorder

subu	$3,$0,$2
sltu	$2,$2,32
sll	$3,$3,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L316
addiu	$3,$3,16320
.set	macro
.set	reorder

addiu	$3,$3,8192
li	$4,16416			# 0x4020
li	$6,192			# 0xc0
$L287:
lw	$8,%got(its_scale)($28)
addiu	$3,$3,32
lbu	$2,11208($16)
li	$5,-3			# 0xfffffffffffffffd
sb	$6,0($8)
lw	$6,%got(its_rnd_y)($28)
and	$7,$2,$5
sh	$3,0($6)
lw	$3,%got(its_rnd_c)($28)
.set	noreorder
.set	nomacro
beq	$7,$0,$L313
sh	$4,0($3)
.set	macro
.set	reorder

li	$3,4			# 0x4
.set	noreorder
.set	nomacro
bne	$2,$3,$L378
li	$3,1			# 0x1
.set	macro
.set	reorder

lbu	$3,11209($16)
and	$5,$5,$3
.set	noreorder
.set	nomacro
bne	$5,$0,$L292
li	$3,1			# 0x1
.set	macro
.set	reorder

sw	$0,10044($16)
$L290:
lhu	$3,11208($16)
li	$4,4			# 0x4
.set	noreorder
.set	nomacro
beq	$3,$4,$L291
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L381
sw	$4,10328($16)
.set	macro
.set	reorder

$L302:
.set	noreorder
.set	nomacro
beq	$2,$0,$L304
li	$2,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L303
sw	$2,11316($16)
.set	macro
.set	reorder

$L276:
sltu	$6,$6,13
beq	$6,$0,$L278
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L277
sw	$2,11316($4)
.set	macro
.set	reorder

$L292:
$L378:
.set	noreorder
.set	nomacro
b	$L290
sw	$3,10044($16)
.set	macro
.set	reorder

$L278:
.set	noreorder
.set	nomacro
b	$L277
sw	$2,11316($4)
.set	macro
.set	reorder

$L304:
.set	noreorder
.set	nomacro
b	$L303
sw	$3,11316($16)
.set	macro
.set	reorder

$L318:
.set	noreorder
.set	nomacro
b	$L308
move	$3,$0
.set	macro
.set	reorder

$L309:
lw	$5,8($17)
srl	$7,$5,3
andi	$8,$5,0x7
addu	$7,$4,$7
addiu	$6,$5,1
lbu	$3,0($7)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L311
sw	$6,8($17)
.set	macro
.set	reorder

srl	$2,$6,3
andi	$6,$6,0x7
addu	$2,$4,$2
addiu	$5,$5,2
lbu	$2,0($2)
sw	$5,8($17)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,1
$L311:
.set	noreorder
.set	nomacro
b	$L310
sw	$2,11248($16)
.set	macro
.set	reorder

$L370:
lw	$6,8($17)
lw	$2,0($17)
srl	$3,$6,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$4,$3,8
addiu	$2,$2,255
sll	$5,$3,8
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$4,$6,0x7
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$6,$6,2
or	$2,$3,$2
lw	$3,%got(ff_vc1_ttfrm_to_tt)($28)
sll	$2,$2,$4
sw	$6,8($17)
srl	$2,$2,30
sll	$2,$2,2
addu	$2,$3,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
b	$L273
sw	$2,11252($16)
.set	macro
.set	reorder

$L297:
.set	noreorder
.set	nomacro
b	$L351
li	$18,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L286:
sltu	$3,$2,32
.set	noreorder
.set	nomacro
bne	$3,$0,$L288
addiu	$5,$6,32
.set	macro
.set	reorder

li	$3,32			# 0x20
addiu	$2,$2,-64
subu	$4,$3,$6
sll	$3,$2,6
sll	$4,$4,7
andi	$6,$5,0x00ff
addiu	$4,$4,32
.set	noreorder
.set	nomacro
b	$L287
andi	$4,$4,0xffff
.set	macro
.set	reorder

$L374:
andi	$8,$2,0x00ff
addiu	$3,$2,9
srl	$6,$8,1
sb	$8,11308($4)
li	$7,1			# 0x1
addu	$6,$3,$6
addiu	$9,$2,7
addiu	$3,$6,-1
addiu	$2,$2,8
sll	$3,$7,$3
.set	noreorder
.set	nomacro
b	$L263
sll	$7,$7,$9
.set	macro
.set	reorder

$L316:
li	$4,16416			# 0x4020
.set	noreorder
.set	nomacro
b	$L287
li	$6,192			# 0xc0
.set	macro
.set	reorder

$L288:
sll	$3,$2,6
li	$2,32			# 0x20
subu	$4,$2,$6
andi	$6,$5,0x00ff
sll	$4,$4,7
addiu	$4,$4,32
.set	noreorder
.set	nomacro
b	$L287
andi	$4,$4,0xffff
.set	macro
.set	reorder

.end	vc1_parse_frame_header
.size	vc1_parse_frame_header, .-vc1_parse_frame_header
.section	.rodata.str1.4
.align	2
$LC26:
.ascii	"Interlaced frames/fields support is not implemented\012\000"
.align	2
$LC27:
.ascii	"ACPRED plane encoding: Imode: %i, Invert: %i\012\000"
.align	2
$LC28:
.ascii	"CONDOVER plane encoding: Imode: %i, Invert: %i\012\000"
.section	.text.vc1_parse_frame_header_adv,"ax",@progbits
.align	2
.align	5
.globl	vc1_parse_frame_header_adv
.set	nomips16
.set	nomicromips
.ent	vc1_parse_frame_header_adv
.type	vc1_parse_frame_header_adv, @function
vc1_parse_frame_header_adv:
.frame	$sp,56,$31		# vars= 0, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-56
lw	$2,11128($4)
.cprestore	24
sw	$17,40($sp)
move	$17,$5
sw	$16,36($sp)
move	$16,$4
sw	$31,52($sp)
sw	$19,48($sp)
sw	$18,44($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L383
sw	$0,11924($4)
.set	macro
.set	reorder

$L472:
lw	$6,0($17)
move	$5,$0
lw	$3,8($17)
li	$8,4			# 0x4
srl	$4,$3,3
$L544:
andi	$7,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L527
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$5,$8,$L544
srl	$4,$3,3
.set	macro
.set	reorder

li	$2,2			# 0x2
move	$18,$0
sw	$2,2904($16)
li	$2,1			# 0x1
sw	$2,11924($16)
lw	$31,52($sp)
$L543:
move	$2,$18
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L383:
lw	$3,8($5)
lw	$6,0($5)
srl	$5,$3,3
andi	$7,$3,0x7
addu	$5,$6,$5
addiu	$4,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L385
sw	$4,8($17)
.set	macro
.set	reorder

srl	$2,$4,3
addiu	$3,$3,2
addu	$6,$6,$2
andi	$4,$4,0x7
lbu	$2,0($6)
sw	$3,8($17)
lw	$3,11952($16)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$4,$3,1
addiu	$2,$2,1
sw	$4,11952($16)
.set	noreorder
.set	nomacro
beq	$3,$0,$L386
sb	$2,11862($16)
.set	macro
.set	reorder

$L403:
lw	$31,52($sp)
li	$18,-1			# 0xffffffffffffffff
lw	$19,48($sp)
move	$2,$18
lw	$17,40($sp)
lw	$18,44($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L385:
.set	noreorder
.set	nomacro
b	$L472
sb	$0,11862($16)
.set	macro
.set	reorder

$L527:
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$5,$3,$L391
li	$4,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$5,$4,$L392
xori	$2,$5,0x1
.set	macro
.set	reorder

move	$5,$3
movz	$5,$4,$2
sw	$5,2904($16)
$L394:
lw	$2,11132($16)
beq	$2,$0,$L395
lw	$2,8($17)
addiu	$2,$2,8
sw	$2,8($17)
$L395:
lw	$2,11124($16)
beq	$2,$0,$L396
lw	$2,11128($16)
beq	$2,$0,$L397
lw	$2,11164($16)
bne	$2,$0,$L397
lw	$3,8($17)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$6,$6,$4
addiu	$3,$3,1
lbu	$2,0($6)
sw	$3,8($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11866($16)
lw	$3,8($17)
lw	$4,0($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11865($16)
lw	$6,0($17)
$L396:
sw	$0,11856($16)
lw	$3,8($17)
addiu	$2,$3,1
sw	$2,8($17)
lw	$4,11128($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L399
srl	$4,$2,3
.set	macro
.set	reorder

andi	$2,$2,0x7
addu	$6,$6,$4
addiu	$4,$3,2
lbu	$3,0($6)
sw	$4,8($17)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11876($16)
lw	$2,8($17)
lw	$6,0($17)
$L399:
lw	$3,11204($16)
.set	noreorder
.set	nomacro
beq	$3,$0,$L400
srl	$3,$2,3
.set	macro
.set	reorder

andi	$4,$2,0x7
addu	$6,$6,$3
addiu	$2,$2,1
lbu	$3,0($6)
sw	$2,8($17)
sll	$2,$3,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11861($16)
lw	$2,8($17)
lw	$6,0($17)
$L400:
lw	$4,2904($16)
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$4,$3,$L528
srl	$3,$2,3
.set	macro
.set	reorder

$L401:
srl	$3,$2,3
addu	$6,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($6)  
lwr $3, 0($6)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
sll	$6,$3,8
li	$3,16711680			# 0xff0000
addiu	$3,$3,255
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
andi	$5,$2,0x7
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
addiu	$2,$2,5
or	$3,$4,$3
sll	$3,$3,$5
srl	$3,$3,27
.set	noreorder
.set	nomacro
beq	$3,$0,$L403
sw	$2,8($17)
.set	macro
.set	reorder

lw	$2,11200($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L404
sw	$3,11268($16)
.set	macro
.set	reorder

slt	$2,$3,9
sb	$2,11309($16)
lw	$2,%got(ff_vc1_pquant_table)($28)
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,11228($16)
$L405:
sltu	$3,$3,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L407
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$3,8($17)
lw	$4,0($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($17)
lw	$3,11200($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11300($16)
li	$2,1			# 0x1
beq	$3,$2,$L529
$L409:
lw	$2,11120($16)
bne	$2,$0,$L530
$L410:
lw	$3,2904($16)
addiu	$2,$3,-1
sltu	$2,$2,2
bne	$2,$0,$L531
$L411:
lw	$18,11948($16)
.set	noreorder
.set	nomacro
bne	$18,$0,$L471
li	$2,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$2,$L414
slt	$2,$3,3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L532
li	$2,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$2,$L417
li	$2,7			# 0x7
.set	macro
.set	reorder

bne	$3,$2,$L517
$L416:
lw	$19,%got(bitplane_decoding)($28)
addiu	$5,$16,11892
lw	$4,11888($16)
addiu	$19,$19,%lo(bitplane_decoding)
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L403
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC27)($28)
andi	$3,$2,0x1
lw	$25,%call16(av_log)($28)
sra	$7,$2,1
lw	$4,0($16)
li	$5,48			# 0x30
addiu	$6,$6,%lo($LC27)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

lw	$2,11196($16)
lw	$28,24($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L517
sb	$0,11904($16)
.set	macro
.set	reorder

lbu	$2,11228($16)
sltu	$2,$2,9
bne	$2,$0,$L533
$L517:
lw	$5,0($17)
$L418:
lw	$3,8($17)
srl	$6,$3,3
andi	$7,$3,0x7
addu	$6,$5,$6
addiu	$4,$3,1
lbu	$2,0($6)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L481
sw	$4,8($17)
.set	macro
.set	reorder

srl	$2,$4,3
andi	$4,$4,0x7
addu	$2,$5,$2
addiu	$3,$3,2
lbu	$2,0($2)
sw	$3,8($17)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,1
$L464:
lw	$3,2904($16)
sw	$2,11244($16)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L465
li	$2,7			# 0x7
.set	macro
.set	reorder

beq	$3,$2,$L465
lw	$3,8($17)
srl	$4,$3,3
andi	$6,$3,0x7
addu	$5,$5,$4
addiu	$3,$3,1
lbu	$2,0($5)
sw	$3,8($17)
lw	$3,2904($16)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10284($16)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L468
li	$2,7			# 0x7
.set	macro
.set	reorder

$L546:
beq	$3,$2,$L534
$L514:
sw	$0,11928($16)
$L471:
move	$18,$0
$L537:
lw	$31,52($sp)
move	$2,$18
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L534:
lw	$2,11188($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L545
lw	$6,%got($LC24)($28)
.set	macro
.set	reorder

$L475:
li	$2,3			# 0x3
lw	$31,52($sp)
lw	$19,48($sp)
lw	$17,40($sp)
sw	$2,2904($16)
li	$2,1			# 0x1
sw	$2,11928($16)
move	$2,$18
lw	$18,44($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L407:
lw	$3,11200($16)
.set	noreorder
.set	nomacro
bne	$3,$2,$L409
sb	$0,11300($16)
.set	macro
.set	reorder

$L529:
lw	$3,8($17)
lw	$4,0($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11309($16)
lw	$2,11120($16)
beq	$2,$0,$L410
$L530:
lw	$6,8($17)
lw	$2,0($17)
srl	$3,$6,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$4,$3,8
addiu	$2,$2,255
sll	$5,$3,8
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$4,$6,0x7
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$6,$6,2
or	$2,$3,$2
sll	$2,$2,$4
sw	$6,8($17)
srl	$2,$2,30
lw	$3,2904($16)
sb	$2,11877($16)
addiu	$2,$3,-1
sltu	$2,$2,2
beq	$2,$0,$L411
$L531:
.set	noreorder
.set	nomacro
b	$L411
sw	$0,11852($16)
.set	macro
.set	reorder

$L404:
lw	$4,%got(ff_vc1_pquant_table)($28)
addu	$4,$4,$3
lbu	$4,32($4)
sb	$4,11228($16)
li	$4,2			# 0x2
.set	noreorder
.set	nomacro
bne	$2,$4,$L535
li	$2,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L405
sb	$0,11309($16)
.set	macro
.set	reorder

$L397:
lw	$7,8($17)
srl	$2,$7,3
addu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($6)  
lwr $2, 0($6)  

# 0 "" 2
#NO_APP
srl	$4,$2,8
sll	$5,$2,8
li	$2,16711680			# 0xff0000
addiu	$2,$2,255
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$4,$7,0x7
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$7,$7,2
or	$2,$3,$2
sll	$2,$2,$4
sw	$7,8($17)
srl	$2,$2,30
sb	$2,11865($16)
.set	noreorder
.set	nomacro
b	$L396
lw	$6,0($17)
.set	macro
.set	reorder

$L528:
addu	$6,$6,$3
lw	$3,%got(ff_vc1_bfraction_vlc)($28)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($6)  
lwr $4, 0($6)  

# 0 "" 2
#NO_APP
srl	$6,$4,8
sll	$7,$4,8
li	$4,16711680			# 0xff0000
lw	$8,4($3)
andi	$3,$2,0x7
addiu	$4,$4,255
and	$5,$6,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$4,$7,$4
or	$4,$5,$4
sll	$5,$4,16
srl	$4,$4,16
or	$4,$5,$4
sll	$3,$4,$3
srl	$3,$3,25
sll	$3,$3,2
addu	$4,$8,$3
lh	$3,0($4)
lh	$5,2($4)
lw	$4,%got(ff_vc1_bfraction_lut)($28)
andi	$3,$3,0x00ff
sll	$6,$3,1
addu	$2,$2,$5
addu	$4,$4,$6
sw	$2,8($17)
sb	$3,11944($16)
lh	$2,0($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L402
sh	$2,11298($16)
.set	macro
.set	reorder

lw	$2,8($17)
.set	noreorder
.set	nomacro
b	$L401
lw	$6,0($17)
.set	macro
.set	reorder

$L481:
.set	noreorder
.set	nomacro
b	$L464
move	$2,$0
.set	macro
.set	reorder

$L465:
lw	$3,8($17)
srl	$6,$3,3
andi	$7,$3,0x7
addu	$6,$5,$6
addiu	$4,$3,1
lbu	$2,0($6)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L482
sw	$4,8($17)
.set	macro
.set	reorder

srl	$2,$4,3
andi	$4,$4,0x7
addu	$2,$5,$2
addiu	$3,$3,2
lbu	$2,0($2)
sw	$3,8($17)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,1
sw	$2,11248($16)
$L540:
lw	$3,8($17)
srl	$4,$3,3
andi	$6,$3,0x7
addu	$5,$5,$4
addiu	$3,$3,1
lbu	$2,0($5)
sw	$3,8($17)
lw	$3,2904($16)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10284($16)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L546
li	$2,7			# 0x7
.set	macro
.set	reorder

$L468:
lw	$2,11188($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L514
lw	$6,%got($LC24)($28)
.set	macro
.set	reorder

$L545:
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC24)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$25,%got(vop_dquant_decoding)($28)
addiu	$25,$25,%lo(vop_dquant_decoding)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vop_dquant_decoding
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$2,7			# 0x7
lw	$3,2904($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L475
sw	$0,11928($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L537
move	$18,$0
.set	macro
.set	reorder

$L535:
.set	noreorder
.set	nomacro
b	$L405
sb	$2,11309($16)
.set	macro
.set	reorder

$L392:
li	$2,7			# 0x7
.set	noreorder
.set	nomacro
b	$L394
sw	$2,2904($16)
.set	macro
.set	reorder

$L391:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L394
sw	$2,2904($16)
.set	macro
.set	reorder

$L417:
lw	$2,11184($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L453
li	$3,128			# 0x80
.set	macro
.set	reorder

lw	$7,0($17)
move	$4,$0
lw	$3,8($17)
li	$8,3			# 0x3
$L454:
srl	$5,$3,3
andi	$6,$3,0x7
addu	$5,$7,$5
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L538
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$4,$8,$L454
li	$6,4096			# 0x1000
.set	macro
.set	reorder

li	$7,3			# 0x3
li	$3,1024			# 0x400
li	$4,11			# 0xb
sb	$7,11308($16)
.set	noreorder
.set	nomacro
b	$L457
li	$5,13			# 0xd
.set	macro
.set	reorder

$L532:
li	$2,1			# 0x1
beq	$3,$2,$L416
.set	noreorder
.set	nomacro
b	$L418
lw	$5,0($17)
.set	macro
.set	reorder

$L414:
lw	$2,11184($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L423
li	$2,128			# 0x80
.set	macro
.set	reorder

lw	$7,0($17)
move	$4,$0
lw	$3,8($17)
li	$8,3			# 0x3
$L424:
srl	$5,$3,3
andi	$6,$3,0x7
addu	$5,$7,$5
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L539
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$4,$8,$L424
li	$6,3			# 0x3
.set	macro
.set	reorder

li	$2,1024			# 0x400
li	$5,4096			# 0x1000
li	$4,11			# 0xb
sb	$6,11308($16)
.set	noreorder
.set	nomacro
b	$L427
li	$3,13			# 0xd
.set	macro
.set	reorder

$L402:
li	$2,7			# 0x7
lw	$6,0($17)
sw	$2,2904($16)
.set	noreorder
.set	nomacro
b	$L401
lw	$2,8($17)
.set	macro
.set	reorder

$L482:
move	$2,$0
.set	noreorder
.set	nomacro
b	$L540
sw	$2,11248($16)
.set	macro
.set	reorder

$L453:
sb	$0,11308($16)
li	$6,256			# 0x100
li	$4,8			# 0x8
li	$5,9			# 0x9
$L457:
lbu	$2,11228($16)
sw	$4,11216($16)
sw	$5,11212($16)
sltu	$4,$2,5
sw	$6,11220($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L458
sw	$3,11224($16)
.set	macro
.set	reorder

sw	$0,11316($16)
$L459:
lw	$3,8($17)
addiu	$5,$16,11332
lw	$7,0($17)
move	$6,$16
lw	$19,%got(bitplane_decoding)($28)
srl	$2,$3,3
lw	$4,11324($16)
andi	$8,$3,0x7
addu	$7,$7,$2
addiu	$3,$3,1
addiu	$19,$19,%lo(bitplane_decoding)
lbu	$2,0($7)
move	$25,$19
sw	$3,8($17)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,11208($16)
sw	$2,10044($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
sw	$2,10328($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L403
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC25)($28)
andi	$3,$2,0x1
lw	$4,0($16)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
sra	$7,$2,1
addiu	$6,$6,%lo($LC25)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

addiu	$5,$16,11336
lw	$4,2836($16)
$L524:
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L403
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC23)($28)
andi	$3,$2,0x1
lw	$4,0($16)
sra	$7,$2,1
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
addiu	$6,$6,%lo($LC23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

li	$3,16711680			# 0xff0000
lw	$8,8($17)
li	$4,-16777216			# 0xffffffffff000000
lw	$7,0($17)
addiu	$9,$3,255
ori	$4,$4,0xff00
lw	$28,24($sp)
srl	$3,$8,3
andi	$10,$8,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$6,8
sll	$6,$6,8
and	$5,$2,$9
and	$6,$6,$4
or	$2,$5,$6
sll	$5,$2,16
srl	$2,$2,16
addiu	$8,$8,2
or	$2,$5,$2
sll	$2,$2,$10
sw	$8,8($17)
srl	$2,$2,30
sw	$2,10272($16)
lw	$6,8($17)
srl	$2,$6,3
andi	$8,$6,0x7
addu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($7)  
lwr $5, 0($7)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$9
and	$4,$5,$4
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
addiu	$6,$6,2
or	$2,$3,$4
lw	$3,%got(ff_vc1_cbpcy_p_vlc)($28)
sll	$2,$2,$8
sw	$6,8($17)
srl	$2,$2,30
lw	$4,11188($16)
sll	$2,$2,4
addu	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L541
sw	$2,11312($16)
.set	macro
.set	reorder

$L461:
lw	$2,11192($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L462
sw	$0,11252($16)
.set	macro
.set	reorder

lw	$3,8($17)
lw	$4,0($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L517
sb	$2,11256($16)
.set	macro
.set	reorder

lw	$4,8($17)
li	$6,16711680			# 0xff0000
lw	$5,0($17)
addiu	$6,$6,255
srl	$2,$4,3
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$4,0x7
or	$2,$3,$2
lw	$3,%got(ff_vc1_ttfrm_to_tt)($28)
sll	$2,$2,$6
addiu	$4,$4,2
srl	$2,$2,30
sll	$2,$2,2
sw	$4,8($17)
addu	$2,$3,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
b	$L418
sw	$2,11252($16)
.set	macro
.set	reorder

$L462:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L517
sb	$2,11256($16)
.set	macro
.set	reorder

$L541:
lw	$6,%got($LC24)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC24)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$25,%got(vop_dquant_decoding)($28)
addiu	$25,$25,%lo(vop_dquant_decoding)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vop_dquant_decoding
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L461
lw	$28,24($sp)
.set	macro
.set	reorder

$L458:
sltu	$2,$2,13
.set	noreorder
.set	nomacro
beq	$2,$0,$L460
li	$2,2			# 0x2
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L459
sw	$2,11316($16)
.set	macro
.set	reorder

$L423:
sb	$0,11308($16)
li	$5,256			# 0x100
li	$4,8			# 0x8
li	$3,9			# 0x9
$L427:
lbu	$6,11228($16)
sw	$3,11212($16)
sw	$4,11216($16)
sltu	$3,$6,5
sw	$5,11220($16)
.set	noreorder
.set	nomacro
beq	$3,$0,$L428
sw	$2,11224($16)
.set	macro
.set	reorder

sw	$0,11316($16)
sltu	$6,$6,13
$L429:
lw	$9,0($17)
move	$5,$0
lw	$3,8($17)
li	$8,1			# 0x1
li	$10,4			# 0x4
srl	$4,$3,3
$L547:
andi	$7,$3,0x7
addu	$4,$9,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$8,$L432
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$5,$10,$L547
srl	$4,$3,3
.set	macro
.set	reorder

$L432:
sll	$7,$6,2
lw	$2,%got(ff_vc1_mv_pmode_table)($28)
li	$3,4			# 0x4
addu	$6,$7,$6
addu	$5,$6,$5
addu	$5,$2,$5
lbu	$2,0($5)
.set	noreorder
.set	nomacro
beq	$2,$3,$L542
sb	$2,11208($16)
.set	macro
.set	reorder

andi	$3,$2,0xfd
.set	noreorder
.set	nomacro
bne	$3,$0,$L548
li	$3,1			# 0x1
.set	macro
.set	reorder

$L473:
.set	noreorder
.set	nomacro
bne	$2,$0,$L442
sw	$0,10044($16)
.set	macro
.set	reorder

lhu	$3,11208($16)
$L443:
move	$4,$0
sw	$4,10328($16)
$L550:
li	$4,772			# 0x304
.set	noreorder
.set	nomacro
beq	$3,$4,$L447
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L447
move	$5,$0
.set	macro
.set	reorder

lw	$6,168($16)
lw	$2,164($16)
lw	$25,%call16(memset)($28)
lw	$4,11320($16)
mul	$6,$6,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$0,11328($16)
.set	macro
.set	reorder

addiu	$5,$16,11336
lw	$28,24($sp)
lw	$4,2836($16)
lw	$19,%got(bitplane_decoding)($28)
.set	noreorder
.set	nomacro
b	$L524
addiu	$19,$19,%lo(bitplane_decoding)
.set	macro
.set	reorder

$L447:
lw	$19,%got(bitplane_decoding)($28)
addiu	$5,$16,11328
lw	$4,11320($16)
addiu	$19,$19,%lo(bitplane_decoding)
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L403
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC22)($28)
andi	$3,$2,0x1
lw	$4,0($16)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
sra	$7,$2,1
addiu	$6,$6,%lo($LC22)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

addiu	$5,$16,11336
.set	noreorder
.set	nomacro
b	$L524
lw	$4,2836($16)
.set	macro
.set	reorder

$L542:
lw	$9,0($17)
move	$5,$0
lw	$3,8($17)
li	$8,1			# 0x1
li	$10,3			# 0x3
srl	$4,$3,3
$L549:
andi	$6,$3,0x7
addu	$4,$9,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$8,$L436
sw	$3,8($17)
.set	macro
.set	reorder

addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$5,$10,$L549
srl	$4,$3,3
.set	macro
.set	reorder

$L436:
lw	$2,%got(ff_vc1_mv_pmode_table2)($28)
addu	$7,$7,$5
li	$3,-16777216			# 0xffffffffff000000
addu	$7,$2,$7
li	$2,16711680			# 0xff0000
ori	$6,$3,0xff00
addiu	$8,$2,255
lbu	$2,0($7)
sb	$2,11209($16)
lw	$5,8($17)
lw	$2,0($17)
srl	$3,$5,3
andi	$7,$5,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$8
and	$4,$4,$6
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
addiu	$5,$5,6
or	$3,$4,$3
sll	$3,$3,$7
sw	$5,8($17)
srl	$3,$3,26
sb	$3,11296($16)
lw	$5,8($17)
lw	$2,0($17)
srl	$3,$5,3
addiu	$4,$5,6
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$4,8($17)
sll	$3,$3,8
lbu	$4,11296($16)
and	$2,$2,$8
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$5,$5,0x7
or	$2,$3,$2
sll	$2,$2,$5
srl	$2,$2,26
.set	noreorder
.set	nomacro
bne	$4,$0,$L438
sb	$2,11297($16)
.set	macro
.set	reorder

subu	$3,$0,$2
sltu	$2,$2,32
sll	$3,$3,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L478
addiu	$3,$3,16320
.set	macro
.set	reorder

addiu	$3,$3,8192
li	$4,16416			# 0x4020
li	$5,192			# 0xc0
$L439:
lw	$8,%got(its_scale)($28)
addiu	$3,$3,32
lbu	$2,11208($16)
li	$6,-3			# 0xfffffffffffffffd
sb	$5,0($8)
lw	$5,%got(its_rnd_y)($28)
and	$7,$2,$6
sh	$3,0($5)
lw	$3,%got(its_rnd_c)($28)
sh	$4,0($3)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L473
sw	$3,11852($16)
.set	macro
.set	reorder

li	$3,4			# 0x4
.set	noreorder
.set	nomacro
bne	$2,$3,$L548
li	$3,1			# 0x1
.set	macro
.set	reorder

lbu	$3,11209($16)
and	$6,$6,$3
.set	noreorder
.set	nomacro
bne	$6,$0,$L444
li	$3,1			# 0x1
.set	macro
.set	reorder

sw	$0,10044($16)
$L442:
lhu	$3,11208($16)
li	$4,4			# 0x4
.set	noreorder
.set	nomacro
beq	$3,$4,$L443
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L550
sw	$4,10328($16)
.set	macro
.set	reorder

$L428:
sltu	$6,$6,13
.set	noreorder
.set	nomacro
beq	$6,$0,$L430
li	$2,2			# 0x2
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L429
sw	$2,11316($16)
.set	macro
.set	reorder

$L386:
lw	$6,%got($LC26)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$18,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC26)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L543
lw	$31,52($sp)
.set	macro
.set	reorder

$L444:
$L548:
.set	noreorder
.set	nomacro
b	$L442
sw	$3,10044($16)
.set	macro
.set	reorder

$L460:
.set	noreorder
.set	nomacro
b	$L459
sw	$2,11316($16)
.set	macro
.set	reorder

$L430:
.set	noreorder
.set	nomacro
b	$L429
sw	$2,11316($16)
.set	macro
.set	reorder

$L533:
lw	$3,8($17)
lw	$6,0($17)
srl	$5,$3,3
andi	$7,$3,0x7
addu	$5,$6,$5
addiu	$4,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L421
sw	$4,8($17)
.set	macro
.set	reorder

srl	$2,$4,3
andi	$4,$4,0x7
addu	$6,$6,$2
addiu	$3,$3,2
lbu	$2,0($6)
sw	$3,8($17)
li	$3,2			# 0x2
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$3,$L517
sb	$2,11904($16)
.set	macro
.set	reorder

lw	$4,11896($16)
addiu	$5,$16,11900
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,bitplane_decoding
1:	jalr	$25
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L403
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got($LC28)($28)
andi	$3,$2,0x1
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
lw	$4,0($16)
sra	$7,$2,1
addiu	$6,$6,%lo($LC28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L418
lw	$5,0($17)
.set	macro
.set	reorder

$L438:
sltu	$3,$2,32
.set	noreorder
.set	nomacro
bne	$3,$0,$L440
addiu	$5,$4,32
.set	macro
.set	reorder

li	$3,32			# 0x20
addiu	$2,$2,-64
subu	$4,$3,$4
andi	$5,$5,0x00ff
sll	$4,$4,7
sll	$3,$2,6
addiu	$4,$4,32
.set	noreorder
.set	nomacro
b	$L439
andi	$4,$4,0xffff
.set	macro
.set	reorder

$L478:
li	$4,16416			# 0x4020
.set	noreorder
.set	nomacro
b	$L439
li	$5,192			# 0xc0
.set	macro
.set	reorder

$L440:
sll	$3,$2,6
li	$2,32			# 0x20
andi	$5,$5,0x00ff
subu	$4,$2,$4
sll	$4,$4,7
addiu	$4,$4,32
.set	noreorder
.set	nomacro
b	$L439
andi	$4,$4,0xffff
.set	macro
.set	reorder

$L538:
andi	$7,$4,0x00ff
addiu	$3,$4,9
srl	$5,$7,1
sb	$7,11308($16)
li	$2,1			# 0x1
addu	$5,$3,$5
addiu	$3,$4,7
addiu	$6,$5,-1
addiu	$4,$4,8
sll	$6,$2,$6
.set	noreorder
.set	nomacro
b	$L457
sll	$3,$2,$3
.set	macro
.set	reorder

$L539:
andi	$6,$4,0x00ff
addiu	$3,$4,9
srl	$5,$6,1
sb	$6,11308($16)
li	$2,1			# 0x1
addu	$3,$3,$5
addiu	$7,$4,7
addiu	$5,$3,-1
addiu	$4,$4,8
sll	$5,$2,$5
.set	noreorder
.set	nomacro
b	$L427
sll	$2,$2,$7
.set	macro
.set	reorder

$L421:
sb	$0,11904($16)
.set	noreorder
.set	nomacro
b	$L418
lw	$5,0($17)
.set	macro
.set	reorder

.end	vc1_parse_frame_header_adv
.size	vc1_parse_frame_header_adv, .-vc1_parse_frame_header_adv

.comm	its_rnd_c,2,2

.comm	its_rnd_y,2,2

.comm	its_scale,1,1

.comm	tcsm1_base,4,4
.rdata
.align	2
.type	IntpFMT, @object
.size	IntpFMT, 6720
IntpFMT:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.space	7
.byte	0
.space	7
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	7
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	SubPel, @object
.size	SubPel, 12
SubPel:
.byte	1
.byte	2
.byte	2
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.align	2
.type	AryFMT, @object
.size	AryFMT, 16
AryFMT:
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0

.comm	mc_base,4,4

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
