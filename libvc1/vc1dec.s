.file	1 "vc1dec.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.vc1_pred_b_mv,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_pred_b_mv
.type	vc1_pred_b_mv, @function
vc1_pred_b_mv:
.frame	$sp,96,$31		# vars= 48, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
lw	$8,10044($4)
li	$2,1			# 0x1
lw	$3,0($5)
addiu	$sp,$sp,-96
subu	$8,$2,$8
sw	$fp,92($sp)
sw	$20,76($sp)
sll	$3,$3,$8
sw	$19,72($sp)
sw	$23,88($sp)
sw	$22,84($sp)
sw	$21,80($sp)
sw	$18,68($sp)
sw	$17,64($sp)
sw	$16,60($sp)
lw	$19,11220($4)
lw	$20,11224($4)
sw	$3,0($5)
lw	$3,10044($4)
lw	$8,0($6)
subu	$3,$2,$3
sll	$3,$8,$3
sw	$3,0($6)
lw	$3,10044($4)
lw	$8,4($5)
subu	$3,$2,$3
sll	$3,$8,$3
sw	$3,4($5)
lw	$8,10044($4)
lw	$3,4($6)
subu	$2,$2,$8
sll	$2,$3,$2
sw	$2,4($6)
lw	$2,8004($4)
lw	$fp,172($4)
.set	noreorder
.set	nomacro
bne	$2,$0,$L66
lw	$10,8012($4)
.set	macro
.set	reorder

lw	$3,988($4)
sll	$13,$10,2
lh	$21,11298($4)
lw	$8,10044($4)
addu	$3,$3,$13
lh	$2,0($3)
lh	$3,2($3)
mul	$22,$2,$21
sll	$2,$2,8
mul	$21,$21,$3
sll	$3,$3,8
.set	noreorder
.set	nomacro
beq	$8,$0,$L67
subu	$2,$22,$2
.set	macro
.set	reorder

lw	$14,160($4)
addiu	$22,$22,128
lw	$24,7992($4)
li	$8,-60			# 0xffffffffffffffc4
subu	$3,$21,$3
sll	$12,$14,6
sll	$11,$24,6
addiu	$12,$12,-4
sra	$9,$22,8
subu	$8,$8,$11
sw	$12,12($sp)
addiu	$2,$2,128
lw	$15,12($sp)
addiu	$21,$21,128
addiu	$3,$3,128
slt	$12,$9,$8
sra	$21,$21,8
sra	$2,$2,8
sra	$3,$3,8
.set	noreorder
.set	nomacro
bne	$12,$0,$L48
subu	$17,$15,$11
.set	macro
.set	reorder

$L71:
slt	$22,$17,$9
lw	$16,7996($4)
movn	$9,$17,$22
lw	$25,164($4)
sll	$12,$16,6
move	$22,$9
li	$9,-60			# 0xffffffffffffffc4
sll	$15,$25,6
sw	$22,7268($4)
subu	$9,$9,$12
addiu	$15,$15,-4
slt	$23,$21,$9
.set	noreorder
.set	nomacro
bne	$23,$0,$L49
subu	$18,$15,$12
.set	macro
.set	reorder

$L72:
slt	$23,$18,$21
movn	$21,$18,$23
slt	$23,$2,$8
.set	noreorder
.set	nomacro
bne	$23,$0,$L50
sw	$21,7272($4)
.set	macro
.set	reorder

$L73:
slt	$23,$17,$2
movn	$2,$17,$23
slt	$23,$3,$9
.set	noreorder
.set	nomacro
bne	$23,$0,$L51
sw	$2,7300($4)
.set	macro
.set	reorder

$L74:
slt	$23,$18,$3
movn	$3,$18,$23
.set	noreorder
.set	nomacro
bne	$7,$0,$L68
sw	$3,7304($4)
.set	macro
.set	reorder

$L10:
lw	$21,112($sp)
addiu	$7,$21,-1
sltu	$7,$7,2
.set	noreorder
.set	nomacro
bne	$7,$0,$L69
lw	$22,112($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$22,$0,$L27
sll	$7,$19,1
.set	macro
.set	reorder

lw	$23,2184($4)
addiu	$2,$13,-8
addiu	$3,$14,-1
sll	$22,$20,1
sw	$2,28($sp)
sw	$23,16($sp)
sw	$3,24($sp)
sw	$7,40($sp)
$L25:
lw	$23,28($sp)
li	$3,2			# 0x2
lw	$7,2188($4)
lw	$21,24($sp)
addu	$2,$7,$23
li	$23,-2			# 0xfffffffffffffffe
xor	$21,$24,$21
movz	$3,$23,$21
sw	$3,8($sp)
.set	noreorder
.set	nomacro
bne	$24,$0,$L30
lw	$3,10296($4)
.set	macro
.set	reorder

sh	$0,2($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L44
sh	$0,0($2)
.set	macro
.set	reorder

lw	$2,11168($4)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L70
sll	$14,$14,5
.set	macro
.set	reorder

sll	$25,$25,5
sll	$16,$16,5
addiu	$14,$14,-4
addiu	$25,$25,-4
move	$3,$0
move	$10,$0
.set	noreorder
.set	nomacro
b	$L35
move	$23,$0
.set	macro
.set	reorder

$L67:
lw	$14,160($4)
addiu	$22,$22,255
lw	$24,7992($4)
subu	$3,$21,$3
sra	$22,$22,9
sll	$12,$14,6
sll	$11,$24,6
addiu	$12,$12,-4
li	$8,-60			# 0xffffffffffffffc4
addiu	$2,$2,255
sw	$12,12($sp)
addiu	$21,$21,255
addiu	$3,$3,255
lw	$15,12($sp)
sll	$9,$22,1
subu	$8,$8,$11
sra	$21,$21,9
sra	$2,$2,9
sra	$3,$3,9
slt	$12,$9,$8
sll	$21,$21,1
sll	$2,$2,1
sll	$3,$3,1
.set	noreorder
.set	nomacro
beq	$12,$0,$L71
subu	$17,$15,$11
.set	macro
.set	reorder

$L48:
lw	$16,7996($4)
li	$9,-60			# 0xffffffffffffffc4
lw	$25,164($4)
move	$22,$8
sll	$12,$16,6
sw	$22,7268($4)
sll	$15,$25,6
subu	$9,$9,$12
addiu	$15,$15,-4
slt	$23,$21,$9
.set	noreorder
.set	nomacro
beq	$23,$0,$L72
subu	$18,$15,$12
.set	macro
.set	reorder

$L49:
move	$21,$9
slt	$23,$2,$8
.set	noreorder
.set	nomacro
beq	$23,$0,$L73
sw	$21,7272($4)
.set	macro
.set	reorder

$L50:
move	$2,$8
slt	$23,$3,$9
.set	noreorder
.set	nomacro
beq	$23,$0,$L74
sw	$2,7300($4)
.set	macro
.set	reorder

$L51:
move	$3,$9
.set	noreorder
.set	nomacro
beq	$7,$0,$L10
sw	$3,7304($4)
.set	macro
.set	reorder

$L68:
lw	$5,2184($4)
lw	$4,2188($4)
lw	$fp,92($sp)
addu	$5,$5,$13
lw	$23,88($sp)
addu	$13,$4,$13
lw	$20,76($sp)
lw	$19,72($sp)
lw	$18,68($sp)
lw	$17,64($sp)
lw	$16,60($sp)
sh	$22,0($5)
sh	$21,2($5)
sh	$2,0($13)
sh	$3,2($13)
lw	$22,84($sp)
lw	$21,80($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L66:
lw	$2,2184($4)
sll	$10,$10,2
lw	$3,2188($4)
lw	$fp,92($sp)
addu	$2,$2,$10
lw	$23,88($sp)
addu	$10,$3,$10
lw	$22,84($sp)
lw	$21,80($sp)
lw	$20,76($sp)
lw	$19,72($sp)
lw	$18,68($sp)
lw	$17,64($sp)
lw	$16,60($sp)
sh	$0,2($10)
sh	$0,0($10)
sh	$0,2($2)
sh	$0,0($2)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L27:
lw	$8,2184($4)
sw	$8,16($sp)
lw	$7,2188($4)
$L28:
lw	$15,16($sp)
lw	$6,7268($4)
lw	$4,7272($4)
addu	$5,$15,$13
lw	$fp,92($sp)
addu	$13,$7,$13
lw	$23,88($sp)
lw	$22,84($sp)
lw	$21,80($sp)
lw	$20,76($sp)
lw	$19,72($sp)
lw	$18,68($sp)
lw	$17,64($sp)
lw	$16,60($sp)
sh	$6,0($5)
sh	$4,2($5)
sh	$2,0($13)
sh	$3,2($13)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L44:
sll	$3,$fp,1
$L79:
li	$fp,1			# 0x1
subu	$10,$10,$3
sll	$3,$10,2
.set	noreorder
.set	nomacro
beq	$14,$fp,$L75
addu	$3,$7,$3
.set	macro
.set	reorder

lw	$21,8($sp)
lh	$fp,0($3)
addu	$10,$21,$10
sll	$10,$10,2
addu	$10,$7,$10
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000111100000000001101111	#S32I2M XR1 $30
# 0 "" 2
#NO_APP
lh	$23,0($10)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000101110000000010101111	#S32I2M XR2,$23
# 0 "" 2
#NO_APP
lh	$23,0($2)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000101110000000011101111	#S32I2M XR3,$23
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000101110000000111101110	#S32M2I XR7, $23
# 0 "" 2
#NO_APP
lh	$3,2($3)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000000110000000001101111	#S32I2M XR1,$3
# 0 "" 2
#NO_APP
lh	$3,2($10)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000000110000000010101111	#S32I2M XR2,$3
# 0 "" 2
#NO_APP
lh	$2,2($2)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000010100000000111101110	#S32M2I XR7, $10
# 0 "" 2
#NO_APP
$L33:
lw	$2,11168($4)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L34
sll	$14,$14,5
.set	macro
.set	reorder

sll	$3,$24,5
addu	$24,$3,$23
sll	$25,$25,5
slt	$2,$24,-28
sll	$16,$16,5
addiu	$14,$14,-4
.set	noreorder
.set	nomacro
beq	$2,$0,$L35
addiu	$25,$25,-4
.set	macro
.set	reorder

li	$23,-28			# 0xffffffffffffffe4
li	$24,-28			# 0xffffffffffffffe4
subu	$23,$23,$3
$L35:
addu	$2,$16,$10
slt	$8,$2,-28
beq	$8,$0,$L36
li	$10,-28			# 0xffffffffffffffe4
li	$2,-28			# 0xffffffffffffffe4
subu	$10,$10,$16
$L36:
subu	$3,$14,$3
subu	$16,$25,$16
slt	$14,$14,$24
slt	$25,$25,$2
movn	$23,$3,$14
movn	$10,$16,$25
$L38:
lw	$5,4($5)
addiu	$3,$22,-1
lw	$8,40($sp)
addu	$23,$23,$5
addiu	$2,$8,-1
addu	$23,$23,$19
and	$2,$23,$2
subu	$2,$2,$19
sw	$2,7300($4)
lw	$5,4($6)
addu	$5,$10,$5
addu	$5,$5,$20
and	$3,$5,$3
subu	$3,$3,$20
.set	noreorder
.set	nomacro
b	$L28
sw	$3,7304($4)
.set	macro
.set	reorder

$L70:
move	$10,$0
move	$23,$0
$L34:
addu	$11,$23,$11
addu	$12,$10,$12
li	$14,-60			# 0xffffffffffffffc4
slt	$3,$11,-60
slt	$2,$12,-60
movn	$11,$14,$3
movn	$12,$14,$2
movn	$23,$8,$3
movn	$10,$9,$2
slt	$12,$15,$12
lw	$3,12($sp)
movn	$10,$18,$12
slt	$11,$3,$11
.set	noreorder
.set	nomacro
b	$L38
movn	$23,$17,$11
.set	macro
.set	reorder

$L30:
.set	noreorder
.set	nomacro
beq	$3,$0,$L79
sll	$3,$fp,1
.set	macro
.set	reorder

lh	$23,0($2)
.set	noreorder
.set	nomacro
b	$L33
lh	$10,2($2)
.set	macro
.set	reorder

$L69:
lw	$22,2184($4)
addiu	$23,$14,-1
addiu	$7,$13,-8
sw	$23,24($sp)
sw	$22,16($sp)
xor	$22,$24,$23
sw	$7,28($sp)
lw	$21,16($sp)
addu	$23,$21,$7
li	$7,-2			# 0xfffffffffffffffe
li	$21,2			# 0x2
movz	$21,$7,$22
lw	$7,10296($4)
.set	noreorder
.set	nomacro
bne	$24,$0,$L13
sw	$21,20($sp)
.set	macro
.set	reorder

sh	$0,2($23)
.set	noreorder
.set	nomacro
beq	$7,$0,$L42
sh	$0,0($23)
.set	macro
.set	reorder

lw	$7,11168($4)
slt	$7,$7,3
.set	noreorder
.set	nomacro
beq	$7,$0,$L76
sll	$7,$14,5
.set	macro
.set	reorder

sw	$0,40($sp)
sll	$21,$25,5
sw	$0,32($sp)
sll	$22,$16,5
sw	$0,20($sp)
addiu	$7,$7,-4
addiu	$21,$21,-4
sw	$22,36($sp)
move	$22,$0
sw	$7,8($sp)
.set	noreorder
.set	nomacro
b	$L18
sw	$21,44($sp)
.set	macro
.set	reorder

$L13:
bne	$7,$0,$L77
$L42:
sll	$7,$fp,1
lw	$21,16($sp)
subu	$7,$10,$7
sll	$22,$7,2
addu	$21,$21,$22
li	$22,1			# 0x1
.set	noreorder
.set	nomacro
beq	$14,$22,$L78
sw	$21,8($sp)
.set	macro
.set	reorder

lw	$22,8($sp)
lw	$21,20($sp)
lh	$22,0($22)
addu	$7,$21,$7
sll	$7,$7,2
sw	$22,20($sp)
lw	$22,16($sp)
addu	$21,$22,$7
lw	$7,20($sp)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
lh	$7,0($21)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
lh	$7,0($23)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001110000000011101111	#S32I2M XR3,$7
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000101100000000111101110	#S32M2I XR7, $22
# 0 "" 2
#NO_APP
sw	$22,20($sp)
lw	$22,8($sp)
lh	$7,2($22)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
lh	$7,2($21)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
lh	$7,2($23)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001110000000011101111	#S32I2M XR3,$7
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000101110000000111101110	#S32M2I XR7, $23
# 0 "" 2
#NO_APP
sw	$23,32($sp)
$L16:
lw	$7,11168($4)
slt	$7,$7,3
.set	noreorder
.set	nomacro
beq	$7,$0,$L80
lw	$7,20($sp)
.set	macro
.set	reorder

lw	$21,20($sp)
sll	$22,$24,5
sll	$23,$14,5
addu	$21,$22,$21
addiu	$23,$23,-4
sw	$21,40($sp)
sll	$21,$25,5
lw	$7,40($sp)
addiu	$21,$21,-4
sw	$23,8($sp)
slt	$7,$7,-28
sw	$21,44($sp)
sw	$7,48($sp)
sll	$7,$16,5
lw	$21,48($sp)
.set	noreorder
.set	nomacro
beq	$21,$0,$L18
sw	$7,36($sp)
.set	macro
.set	reorder

li	$7,-28			# 0xffffffffffffffe4
li	$23,-28			# 0xffffffffffffffe4
subu	$7,$7,$22
sw	$23,40($sp)
sw	$7,20($sp)
$L18:
lw	$7,36($sp)
lw	$23,32($sp)
addu	$21,$7,$23
slt	$7,$21,-28
.set	noreorder
.set	nomacro
beq	$7,$0,$L81
lw	$7,8($sp)
.set	macro
.set	reorder

lw	$23,36($sp)
li	$7,-28			# 0xffffffffffffffe4
li	$21,-28			# 0xffffffffffffffe4
subu	$7,$7,$23
sw	$7,32($sp)
lw	$7,8($sp)
$L81:
lw	$23,36($sp)
subu	$22,$7,$22
sw	$22,48($sp)
lw	$22,44($sp)
subu	$22,$22,$23
sw	$22,36($sp)
lw	$22,40($sp)
slt	$23,$7,$22
lw	$7,44($sp)
lw	$22,20($sp)
slt	$21,$7,$21
lw	$7,48($sp)
movn	$22,$7,$23
lw	$23,36($sp)
sw	$22,20($sp)
lw	$22,32($sp)
movn	$22,$23,$21
sw	$22,32($sp)
$L21:
sll	$22,$19,1
lw	$7,0($5)
sw	$22,40($sp)
sll	$22,$20,1
lw	$23,40($sp)
addiu	$21,$23,-1
lw	$23,20($sp)
addu	$7,$23,$7
addiu	$23,$22,-1
addu	$7,$7,$19
and	$7,$7,$21
lw	$21,32($sp)
subu	$7,$7,$19
sw	$23,8($sp)
sw	$7,7268($4)
lw	$7,0($6)
addu	$7,$21,$7
lw	$21,112($sp)
addu	$23,$7,$20
lw	$7,8($sp)
and	$23,$23,$7
subu	$23,$23,$20
li	$7,2			# 0x2
.set	noreorder
.set	nomacro
beq	$21,$7,$L25
sw	$23,7272($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L28
lw	$7,2188($4)
.set	macro
.set	reorder

$L77:
lh	$7,0($23)
lh	$23,2($23)
sw	$7,20($sp)
.set	noreorder
.set	nomacro
b	$L16
sw	$23,32($sp)
.set	macro
.set	reorder

$L78:
lh	$22,0($21)
lh	$23,2($21)
sw	$22,20($sp)
.set	noreorder
.set	nomacro
b	$L16
sw	$23,32($sp)
.set	macro
.set	reorder

$L76:
sw	$0,32($sp)
sw	$0,20($sp)
lw	$7,20($sp)
$L80:
lw	$22,32($sp)
addu	$21,$7,$11
addu	$7,$22,$12
slt	$23,$21,-60
slt	$22,$7,-60
sw	$23,36($sp)
sw	$22,8($sp)
li	$22,-60			# 0xffffffffffffffc4
movn	$21,$22,$23
lw	$23,8($sp)
movn	$7,$22,$23
lw	$22,20($sp)
lw	$23,36($sp)
slt	$7,$15,$7
movn	$22,$8,$23
lw	$23,32($sp)
sw	$22,20($sp)
lw	$22,12($sp)
slt	$21,$22,$21
lw	$22,8($sp)
movn	$23,$9,$22
sw	$23,32($sp)
lw	$23,20($sp)
movn	$23,$17,$21
lw	$21,32($sp)
movn	$21,$18,$7
sw	$23,20($sp)
.set	noreorder
.set	nomacro
b	$L21
sw	$21,32($sp)
.set	macro
.set	reorder

$L75:
lh	$23,0($3)
.set	noreorder
.set	nomacro
b	$L33
lh	$10,2($3)
.set	macro
.set	reorder

.end	vc1_pred_b_mv
.size	vc1_pred_b_mv, .-vc1_pred_b_mv
.section	.text.unlikely.vc1_decode_end,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	vc1_decode_end
.type	vc1_decode_end, @function
vc1_decode_end:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
lw	$25,%call16(av_freep)($28)
sw	$16,24($sp)
sw	$31,28($sp)
.cprestore	16
lw	$16,136($4)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11908

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11912

lw	$28,16($sp)
lw	$25,%call16(MPV_common_end)($28)
.reloc	1f,R_MIPS_JALR,MPV_common_end
1:	jalr	$25
move	$4,$16

lw	$28,16($sp)
lw	$25,%call16(ff_intrax8_common_end)($28)
.reloc	1f,R_MIPS_JALR,ff_intrax8_common_end
1:	jalr	$25
addiu	$4,$16,10584

move	$2,$0
lw	$28,16($sp)
lw	$31,28($sp)
lw	$16,24($sp)
lw	$3,%got(use_jz_buf)($28)
sw	$0,0($3)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	vc1_decode_end
.size	vc1_decode_end, .-vc1_decode_end
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"vc1 len of aux task = %d\012\000"
.align	2
$LC1:
.ascii	"Extra data: %i bits left, value: %X\012\000"
.align	2
$LC2:
.ascii	"Read %i bits in overflow\012\000"
.align	2
$LC3:
.ascii	"Extradata size too small: %i\012\000"
.align	2
$LC4:
.ascii	"Incomplete extradata\012\000"
.section	.text.unlikely.vc1_decode_init,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	vc1_decode_init
.type	vc1_decode_init, @function
vc1_decode_init:
.frame	$sp,144,$31		# vars= 40, regs= 10/0, args= 56, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-144
lw	$3,%got(crc_code_rota)($28)
lw	$5,%got(vpu_base)($28)
li	$2,64			# 0x40
.cprestore	56
move	$6,$0
sw	$17,108($sp)
move	$17,$4
sw	$16,104($sp)
sw	$31,140($sp)
sw	$fp,136($sp)
sw	$23,132($sp)
sw	$22,128($sp)
sw	$21,124($sp)
sw	$20,120($sp)
sw	$19,116($sp)
sw	$18,112($sp)
sh	$0,0($3)
lw	$3,%got(tlb_i)($28)
lw	$16,136($4)
li	$4,96			# 0x60
sw	$0,0($3)
$L85:
lw	$3,0($5)
addu	$3,$3,$2
#APP
# 3716 "vc1dec.c" 1
sw	 $6,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$4,$L85
lw	$25,%call16(motion_init)($28)
.set	macro
.set	reorder

li	$4,8			# 0x8
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_init
1:	jalr	$25
li	$5,3			# 0x3
.set	macro
.set	reorder

li	$2,3			# 0x3
lw	$28,56($sp)
#APP
# 3724 "vc1dec.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
li	$2,-201326592			# 0xfffffffff4000000
addiu	$3,$2,16384
$L86:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L86
lw	$2,%got(tcsm1_base)($28)
li	$3,49152			# 0xc000
move	$20,$2
lw	$2,0($2)
addu	$3,$2,$3
$L87:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L87
lw	$2,%got(sram_base)($28)
lw	$2,0($2)
addiu	$3,$2,12288
$L88:
sw	$0,0($2)
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$3,$L88
lw	$25,%call16(av_malloc)($28)
.set	macro
.set	reorder

lw	$18,%got(vc1_auxcodes_len)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
li	$4,524288			# 0x80000
.set	macro
.set	reorder

li	$3,1			# 0x1
lw	$28,56($sp)
move	$19,$2
lw	$2,%got(aux_base)($28)
lw	$4,%got($LC0)($28)
lw	$25,%call16(printf)($28)
lw	$2,0($2)
addiu	$4,$4,%lo($LC0)
sw	$3,0($2)
li	$2,-201326592			# 0xfffffffff4000000
sw	$0,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
lw	$5,0($18)
.set	macro
.set	reorder

move	$6,$0
lw	$28,56($sp)
move	$8,$18
lw	$10,0($20)
lw	$9,%got(vc1_aux_task_codes)($28)
$L89:
lw	$3,0($8)
sll	$5,$6,2
addu	$7,$9,$5
srl	$3,$3,2
addu	$5,$10,$5
sltu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L165
addiu	$6,$6,1
.set	macro
.set	reorder

lw	$3,0($7)
.set	noreorder
.set	nomacro
b	$L89
sw	$3,0($5)
.set	macro
.set	reorder

$L165:
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$19
.set	macro
.set	reorder

li	$2,-2147483648			# 0xffffffff80000000
lw	$28,56($sp)
addiu	$3,$2,16384
$L91:
#APP
# 3772 "vc1dec.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L91
#APP
# 3772 "vc1dec.c" 1
sync
# 0 "" 2
# 3773 "vc1dec.c" 1
sync
# 0 "" 2
#NO_APP
lw	$2,%got(sde_base)($28)
li	$3,257			# 0x101
lw	$14,%got(vc1_last_decode_table)($28)
move	$18,$0
lw	$13,%got(vc1_index_decode_table)($28)
li	$23,740			# 0x2e4
lw	$12,%got(vc1_last_delta_level_table)($28)
li	$22,44			# 0x2c
lw	$11,%got(vc1_delta_level_table)($28)
addiu	$14,$14,%lo(vc1_last_decode_table)
lw	$2,0($2)
addiu	$13,$13,%lo(vc1_index_decode_table)
lw	$4,%got(residual_init_table)($28)
addiu	$12,$12,%lo(vc1_last_delta_level_table)
lw	$7,%got(cfg_all_table)($28)
addiu	$11,$11,%lo(vc1_delta_level_table)
li	$21,10			# 0xa
sw	$3,48($2)
li	$20,57			# 0x39
li	$19,1024			# 0x400
li	$15,8			# 0x8
$L93:
mul	$9,$18,$23
lw	$2,4($7)
mul	$10,$18,$21
lw	$3,12($7)
andi	$6,$18,0x1
move	$5,$19
sll	$8,$18,2
movz	$5,$0,$6
addiu	$2,$2,-1
addu	$8,$14,$8
addu	$31,$9,$13
mul	$9,$18,$22
move	$6,$5
lw	$8,0($8)
sll	$5,$18,5
sw	$31,4($4)
sll	$3,$3,8
subu	$5,$5,$18
sll	$2,$2,4
ori	$3,$3,0x9
addu	$fp,$9,$12
lw	$9,%got(vc1_last_delta_run_table)($28)
sll	$6,$6,9
sll	$8,$8,1
addiu	$9,$9,%lo(vc1_last_delta_run_table)
sw	$fp,8($4)
addu	$5,$11,$5
addu	$25,$10,$9
lw	$9,%got(vc1_delta_run_table)($28)
mul	$10,$18,$20
sw	$8,0($4)
or	$2,$3,$2
sw	$5,12($4)
addiu	$9,$9,%lo(vc1_delta_run_table)
sw	$25,16($4)
or	$2,$2,$6
addiu	$18,$18,1
sw	$2,24($4)
addiu	$7,$7,16
addiu	$4,$4,28
addu	$24,$10,$9
.set	noreorder
.set	nomacro
bne	$18,$15,$L93
sw	$24,-8($4)
.set	macro
.set	reorder

lw	$25,%call16(jz4740_alloc_frame)($28)
li	$4,256			# 0x100
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
li	$5,1048576			# 0x100000
.set	macro
.set	reorder

li	$4,32			# 0x20
lw	$28,56($sp)
li	$5,96			# 0x60
lw	$3,%got(buf2_pre_alloc_buf)($28)
lw	$25,%call16(jz4740_alloc_frame)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
sw	$2,%lo(buf2_pre_alloc_buf)($3)
.set	macro
.set	reorder

lw	$28,56($sp)
lw	$3,28($17)
lw	$4,%got(left_buf)($28)
.set	noreorder
.set	nomacro
beq	$3,$0,$L164
sw	$2,0($4)
.set	macro
.set	reorder

lw	$2,24($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L95
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,12($17)
andi	$2,$2,0x2000
.set	noreorder
.set	nomacro
bne	$2,$0,$L97
move	$4,$17
.set	macro
.set	reorder

lw	$2,132($17)
lw	$25,496($17)
.set	noreorder
.set	nomacro
jalr	$25
lw	$5,48($2)
.set	macro
.set	reorder

lw	$28,56($sp)
.set	noreorder
.set	nomacro
b	$L98
sw	$2,52($17)
.set	macro
.set	reorder

$L97:
sw	$18,52($17)
$L98:
lw	$2,132($17)
lw	$25,%call16(ff_find_hwaccel)($28)
lw	$5,52($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_find_hwaccel
1:	jalr	$25
lw	$4,8($2)
.set	macro
.set	reorder

lw	$3,364($17)
lw	$28,56($sp)
sw	$2,872($17)
.set	noreorder
.set	nomacro
bne	$3,$0,$L99
sw	$17,0($16)
.set	macro
.set	reorder

li	$2,19			# 0x13
sw	$2,364($17)
$L99:
lw	$25,%call16(ff_msmpeg4_vc1_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_msmpeg4_vc1_init
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L164
lw	$28,56($sp)
.set	macro
.set	reorder

lw	$2,%got(done.7468)($28)
sw	$0,11912($16)
sw	$0,11908($16)
lw	$2,%lo(done.7468)($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L168
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$3,%got(ff_vc1_bfraction_codes)($28)
li	$19,1			# 0x1
lw	$2,%got(ff_vc1_bfraction_vlc)($28)
li	$18,4			# 0x4
lw	$25,%call16(init_vlc_sparse)($28)
li	$5,7			# 0x7
lw	$7,%got(ff_vc1_bfraction_bits)($28)
li	$6,23			# 0x17
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.7471)($28)
move	$22,$0
sw	$19,16($sp)
sw	$19,20($sp)
addiu	$3,$3,%lo(table.7471)
sw	$19,28($sp)
sw	$19,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,128			# 0x80
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)
.set	macro
.set	reorder

li	$5,3			# 0x3
lw	$28,56($sp)
li	$6,4			# 0x4
sw	$19,16($sp)
sw	$19,20($sp)
sw	$19,28($sp)
lw	$3,%got(ff_vc1_norm2_codes)($28)
lw	$2,%got(ff_vc1_norm2_vlc)($28)
lw	$25,%call16(init_vlc_sparse)($28)
lw	$7,%got(ff_vc1_norm2_bits)($28)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.7472)($28)
sw	$19,32($sp)
sw	$0,36($sp)
addiu	$3,$3,%lo(table.7472)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,8			# 0x8
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)
.set	macro
.set	reorder

li	$3,2			# 0x2
lw	$28,56($sp)
li	$5,9			# 0x9
sw	$19,16($sp)
li	$6,64			# 0x40
sw	$3,28($sp)
sw	$3,32($sp)
lw	$3,%got(table.7473)($28)
lw	$9,%got(ff_vc1_norm6_codes)($28)
lw	$2,%got(ff_vc1_norm6_vlc)($28)
addiu	$3,$3,%lo(table.7473)
lw	$25,%call16(init_vlc_sparse)($28)
lw	$7,%got(ff_vc1_norm6_bits)($28)
sw	$19,20($sp)
move	$4,$2
sw	$9,24($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,556			# 0x22c
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$28,56($sp)
li	$6,7			# 0x7
sw	$19,16($sp)
sw	$19,20($sp)
sw	$19,28($sp)
lw	$3,%got(ff_vc1_imode_codes)($28)
lw	$2,%got(ff_vc1_imode_vlc)($28)
lw	$12,%got(ff_vc1_ttmb_vlc)($28)
lw	$8,%got(ff_vc1_subblkpat_vlc)($28)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.7474)($28)
lw	$25,%call16(init_vlc_sparse)($28)
lw	$7,%got(ff_vc1_imode_bits)($28)
addiu	$3,$3,%lo(table.7474)
lw	$23,%got(ff_vc1_ttblk_vlc)($28)
sw	$19,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,16			# 0x10
sw	$3,12($2)
sw	$8,96($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$12,88($sp)
.set	macro
.set	reorder

lw	$28,56($sp)
lw	$12,88($sp)
lw	$8,96($sp)
lw	$3,%got(vlc_offs)($28)
lw	$19,%got(vlc_table.7470)($28)
addiu	$21,$3,%lo(vlc_offs)
addiu	$19,$19,%lo(vlc_table.7470)
move	$20,$21
$L101:
lhu	$18,2($20)
li	$fp,1			# 0x1
lhu	$2,0($20)
sll	$4,$22,5
lw	$24,%got(ff_vc1_ttmb_codes)($28)
li	$13,4			# 0x4
lw	$3,%got(ff_vc1_ttmb_bits)($28)
sll	$7,$22,4
sll	$5,$2,2
lw	$25,%call16(init_vlc_sparse)($28)
subu	$2,$18,$2
sw	$0,36($sp)
addu	$5,$19,$5
sw	$fp,16($sp)
addu	$4,$24,$4
sw	$fp,20($sp)
sw	$0,40($sp)
addu	$7,$3,$7
sw	$2,12($12)
li	$2,2			# 0x2
sw	$5,4($12)
li	$6,16			# 0x10
li	$5,8			# 0x8
sw	$8,96($sp)
sw	$4,24($sp)
move	$4,$12
sw	$13,48($sp)
addiu	$20,$20,6
sw	$12,88($sp)
sw	$13,84($sp)
sw	$0,44($sp)
sw	$2,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

sll	$7,$22,3
lw	$28,56($sp)
sll	$2,$18,2
lhu	$14,-2($20)
li	$6,8			# 0x8
lw	$12,88($sp)
addu	$2,$19,$2
lw	$13,84($sp)
addiu	$22,$22,1
lw	$5,%got(ff_vc1_ttblk_codes)($28)
subu	$18,$14,$18
lw	$9,%got(ff_vc1_ttblk_bits)($28)
addiu	$12,$12,16
lw	$25,%call16(init_vlc_sparse)($28)
addu	$4,$5,$7
sw	$fp,16($sp)
addu	$7,$9,$7
sw	$fp,20($sp)
li	$5,5			# 0x5
sw	$4,24($sp)
move	$4,$23
sw	$18,12($23)
addiu	$23,$23,16
sw	$2,-12($23)
move	$18,$19
sw	$12,88($sp)
sw	$13,48($sp)
sw	$14,92($sp)
sw	$fp,28($sp)
sw	$fp,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$0,44($sp)
.set	macro
.set	reorder

sll	$2,$22,4
lw	$28,56($sp)
li	$6,15			# 0xf
lhu	$4,0($20)
subu	$2,$2,$22
lw	$14,92($sp)
addiu	$2,$2,-15
lw	$8,96($sp)
lw	$7,%got(ff_vc1_subblkpat_bits)($28)
sll	$5,$14,2
lw	$13,84($sp)
subu	$14,$4,$14
lw	$4,%got(ff_vc1_subblkpat_codes)($28)
addu	$5,$19,$5
lw	$25,%call16(init_vlc_sparse)($28)
sw	$fp,16($sp)
addu	$7,$7,$2
addu	$4,$4,$2
sw	$5,4($8)
li	$5,6			# 0x6
sw	$fp,20($sp)
sw	$14,12($8)
sw	$4,24($sp)
move	$4,$8
addiu	$8,$8,16
sw	$fp,28($sp)
sw	$fp,32($sp)
sw	$0,36($sp)
sw	$8,96($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$13,48($sp)
.set	macro
.set	reorder

li	$2,3			# 0x3
lw	$28,56($sp)
lw	$8,96($sp)
.set	noreorder
.set	nomacro
bne	$22,$2,$L101
lw	$12,88($sp)
.set	macro
.set	reorder

lw	$14,%got(ff_vc1_4mv_block_pattern_vlc)($28)
move	$fp,$0
lw	$23,%got(ff_vc1_cbpcy_p_vlc)($28)
move	$22,$21
lw	$20,%got(ff_vc1_mv_diff_vlc)($28)
$L102:
lhu	$2,18($22)
sll	$7,$fp,4
lhu	$11,20($22)
li	$8,1			# 0x1
lw	$10,%got(ff_vc1_4mv_block_pattern_codes)($28)
li	$19,4			# 0x4
sll	$4,$2,2
lw	$24,%got(ff_vc1_4mv_block_pattern_bits)($28)
subu	$2,$11,$2
lw	$25,%call16(init_vlc_sparse)($28)
addu	$15,$10,$7
sw	$8,16($sp)
addu	$4,$18,$4
sw	$8,20($sp)
sw	$8,28($sp)
addu	$7,$24,$7
sw	$0,36($sp)
li	$5,6			# 0x6
sw	$4,4($14)
move	$4,$14
sw	$2,12($14)
addiu	$14,$14,16
sw	$15,24($sp)
li	$15,2			# 0x2
li	$6,16			# 0x10
sw	$8,32($sp)
sw	$14,92($sp)
addiu	$22,$22,6
sw	$8,96($sp)
sw	$11,84($sp)
sw	$15,88($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$19,48($sp)
.set	macro
.set	reorder

sll	$2,$fp,7
lw	$11,84($sp)
sll	$7,$fp,6
lw	$28,56($sp)
li	$5,8			# 0x8
lw	$8,96($sp)
li	$6,64			# 0x40
sll	$4,$11,2
lw	$15,88($sp)
lhu	$24,16($22)
addu	$4,$18,$4
lw	$3,%got(ff_vc1_cbpcy_p_codes)($28)
lw	$25,%call16(init_vlc_sparse)($28)
sw	$8,16($sp)
subu	$11,$24,$11
sw	$8,20($sp)
addu	$2,$3,$2
sw	$15,28($sp)
sw	$4,4($23)
sw	$2,24($sp)
sw	$11,12($23)
sw	$15,32($sp)
sw	$8,96($sp)
sw	$15,88($sp)
sw	$24,84($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$19,48($sp)
lw	$4,%got(ff_vc1_cbpcy_p_bits)($28)
addu	$7,$4,$7
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

li	$6,146			# 0x92
lhu	$4,18($22)
li	$7,73			# 0x49
lw	$28,56($sp)
mul	$3,$fp,$6
lw	$24,84($sp)
li	$6,73			# 0x49
lw	$8,96($sp)
addiu	$23,$23,16
lw	$15,88($sp)
sll	$5,$24,2
lw	$2,%got(ff_vc1_mv_diff_codes)($28)
subu	$24,$4,$24
lw	$25,%call16(init_vlc_sparse)($28)
mul	$4,$fp,$7
sw	$8,16($sp)
addu	$2,$3,$2
sw	$8,20($sp)
lw	$3,%got(ff_vc1_mv_diff_bits)($28)
addu	$5,$18,$5
addiu	$fp,$fp,1
sw	$5,4($20)
li	$5,8			# 0x8
sw	$15,28($sp)
addu	$7,$4,$3
sw	$24,12($20)
move	$4,$20
sw	$2,24($sp)
sw	$15,32($sp)
addiu	$20,$20,16
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$19,48($sp)
.set	macro
.set	reorder

lw	$28,56($sp)
.set	noreorder
.set	nomacro
bne	$fp,$19,$L102
lw	$14,92($sp)
.set	macro
.set	reorder

lw	$20,%got(vc1_ac_tables)($28)
move	$23,$0
lw	$19,%got(ff_vc1_ac_coeff_table)($28)
li	$22,8			# 0x8
li	$fp,4			# 0x4
addiu	$20,$20,%lo(vc1_ac_tables)
$L103:
lw	$5,%got(vc1_ac_sizes)($28)
addiu	$7,$20,4
lhu	$8,42($21)
addiu	$21,$21,2
lhu	$4,42($21)
addiu	$5,$5,%lo(vc1_ac_sizes)
lw	$25,%call16(init_vlc_sparse)($28)
sll	$2,$8,2
addu	$6,$5,$23
subu	$4,$4,$8
addu	$2,$18,$2
lw	$6,0($6)
li	$5,8			# 0x8
sw	$20,24($sp)
addiu	$23,$23,4
sw	$22,16($sp)
addiu	$20,$20,1488
sw	$fp,20($sp)
sw	$4,12($19)
move	$4,$19
sw	$2,4($19)
addiu	$19,$19,16
sw	$22,28($sp)
sw	$fp,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$fp,48($sp)
.set	macro
.set	reorder

li	$6,32			# 0x20
.set	noreorder
.set	nomacro
bne	$23,$6,$L103
lw	$28,56($sp)
.set	macro
.set	reorder

li	$2,1			# 0x1
lw	$9,%got(done.7468)($28)
sw	$2,%lo(done.7468)($9)
li	$2,-1			# 0xffffffffffffffff
$L168:
lw	$3,2788($16)
li	$6,32800			# 0x8020
sb	$0,11308($16)
sb	$2,11228($16)
lw	$2,%got(vmau_base)($28)
lw	$5,0($2)
li	$2,32768			# 0x8000
addu	$6,$5,$6
addu	$2,$5,$2
$L104:
lw	$4,0($3)
#APP
# 3824 "vc1dec.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$6,$L104
addiu	$3,$3,4
.set	macro
.set	reorder

lw	$8,2792($16)
move	$2,$0
li	$7,32800			# 0x8020
li	$6,32			# 0x20
addu	$4,$8,$2
$L169:
addu	$3,$2,$7
lw	$4,0($4)
addu	$3,$5,$3
#APP
# 3830 "vc1dec.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$6,$L169
addu	$4,$8,$2
.set	macro
.set	reorder

lw	$2,44($17)
lw	$4,40($17)
lw	$3,224($17)
sw	$2,664($17)
li	$2,74			# 0x4a
.set	noreorder
.set	nomacro
bne	$3,$2,$L106
sw	$4,660($17)
.set	macro
.set	reorder

lw	$2,28($17)
sll	$2,$2,3
sra	$3,$2,3
.set	noreorder
.set	nomacro
bltz	$3,$L133
lw	$4,24($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L170
addu	$3,$4,$3
.set	macro
.set	reorder

$L133:
move	$3,$0
move	$2,$0
move	$4,$0
addu	$3,$4,$3
$L170:
lw	$25,%call16(vc1_decode_sequence_header)($28)
addiu	$6,$sp,64
sw	$4,64($sp)
sw	$2,76($sp)
move	$4,$17
move	$5,$16
sw	$3,68($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_sequence_header
1:	jalr	$25
sw	$0,72($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L164
lw	$28,56($sp)
.set	macro
.set	reorder

lw	$7,28($17)
lw	$2,72($sp)
sll	$7,$7,3
subu	$7,$7,$2
.set	noreorder
.set	nomacro
blez	$7,$L108
lw	$3,64($sp)
.set	macro
.set	reorder

srl	$4,$2,3
li	$5,16711680			# 0xff0000
lw	$6,%got($LC1)($28)
lw	$25,%call16(av_log)($28)
addu	$3,$3,$4
addiu	$5,$5,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$5
li	$5,-16777216			# 0xffffffffff000000
addiu	$6,$6,%lo($LC1)
ori	$5,$5,0xff00
and	$4,$4,$5
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$2,0x7
or	$3,$4,$3
sll	$3,$3,$5
subu	$4,$0,$7
addu	$2,$7,$2
srl	$3,$3,$4
li	$5,32			# 0x20
sw	$2,72($sp)
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L109
lw	$28,56($sp)
.set	macro
.set	reorder

$L108:
.set	noreorder
.set	nomacro
beq	$7,$0,$L109
lw	$6,%got($LC2)($28)
.set	macro
.set	reorder

li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
subu	$7,$0,$7
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L109
lw	$28,56($sp)
.set	macro
.set	reorder

$L106:
lw	$7,28($17)
lw	$18,24($17)
slt	$2,$7,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L111
addu	$23,$18,$7
.set	macro
.set	reorder

lw	$6,%got($LC3)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L95
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L111:
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
addiu	$4,$7,8
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
lw	$28,56($sp)
li	$6,-256			# 0xffffffffffffff00
li	$5,256			# 0x100
move	$fp,$2
$L112:
sltu	$4,$18,$23
addiu	$2,$18,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L166
sll	$3,$3,8
.set	macro
.set	reorder

lbu	$4,-1($2)
or	$3,$4,$3
and	$4,$3,$6
.set	noreorder
.set	nomacro
bne	$4,$5,$L134
addiu	$4,$18,-3
.set	macro
.set	reorder

$L115:
li	$19,16711680			# 0xff0000
sw	$0,80($sp)
li	$18,-16777216			# 0xffffffffff000000
move	$21,$0
addiu	$19,$19,255
ori	$18,$18,0xff00
.set	noreorder
.set	nomacro
b	$L113
addiu	$20,$sp,64
.set	macro
.set	reorder

$L134:
.set	noreorder
.set	nomacro
b	$L112
move	$18,$2
.set	macro
.set	reorder

$L166:
.set	noreorder
.set	nomacro
b	$L115
move	$4,$23
.set	macro
.set	reorder

$L138:
li	$21,1			# 0x1
$L120:
move	$4,$22
$L113:
sltu	$2,$4,$23
.set	noreorder
.set	nomacro
beq	$2,$0,$L167
addiu	$8,$4,4
.set	macro
.set	reorder

subu	$2,$23,$8
slt	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$0,$L116
move	$22,$23
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
li	$11,-256			# 0xffffffffffffff00
li	$6,256			# 0x100
move	$5,$8
$L118:
sltu	$9,$5,$23
addiu	$7,$5,1
.set	noreorder
.set	nomacro
beq	$9,$0,$L135
sll	$3,$3,8
.set	macro
.set	reorder

lbu	$2,-1($7)
or	$3,$2,$3
and	$9,$3,$11
.set	noreorder
.set	nomacro
bne	$9,$6,$L136
addiu	$22,$5,-3
.set	macro
.set	reorder

$L116:
subu	$6,$22,$4
addiu	$2,$6,-4
.set	noreorder
.set	nomacro
blez	$2,$L120
slt	$3,$2,4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L121
li	$3,1			# 0x1
.set	macro
.set	reorder

move	$3,$0
$L122:
addu	$5,$4,$3
addu	$6,$fp,$3
addiu	$3,$3,1
lbu	$7,4($5)
slt	$5,$3,$2
.set	noreorder
.set	nomacro
bne	$5,$0,$L122
sb	$7,0($6)
.set	macro
.set	reorder

$L123:
sll	$2,$2,3
sra	$3,$2,3
.set	noreorder
.set	nomacro
bgez	$3,$L127
move	$6,$fp
.set	macro
.set	reorder

move	$3,$0
move	$2,$0
move	$6,$0
$L127:
addu	$5,$6,$3
sw	$2,76($sp)
sw	$0,72($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($4)  
lwr $2, 0($4)  

# 0 "" 2
#NO_APP
srl	$3,$2,8
sw	$6,64($sp)
sll	$2,$2,8
and	$3,$3,$19
and	$2,$2,$18
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
li	$3,270			# 0x10e
.set	noreorder
.set	nomacro
beq	$2,$3,$L128
sw	$5,68($sp)
.set	macro
.set	reorder

li	$3,271			# 0x10f
.set	noreorder
.set	nomacro
bne	$2,$3,$L113
move	$4,$22
.set	macro
.set	reorder

lw	$25,%call16(vc1_decode_sequence_header)($28)
move	$4,$17
move	$5,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_sequence_header
1:	jalr	$25
move	$6,$20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L138
lw	$28,56($sp)
.set	macro
.set	reorder

$L163:
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

$L164:
.set	noreorder
.set	nomacro
b	$L95
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L136:
.set	noreorder
.set	nomacro
b	$L118
move	$5,$7
.set	macro
.set	reorder

$L135:
.set	noreorder
.set	nomacro
b	$L116
move	$22,$23
.set	macro
.set	reorder

$L121:
move	$5,$0
li	$11,3			# 0x3
addiu	$12,$6,-5
$L126:
lbu	$7,0($8)
addu	$9,$fp,$3
.set	noreorder
.set	nomacro
bne	$7,$11,$L124
move	$6,$3
.set	macro
.set	reorder

slt	$13,$5,2
bne	$13,$0,$L124
lbu	$13,-1($8)
bne	$13,$0,$L124
lbu	$13,-2($8)
.set	noreorder
.set	nomacro
bne	$13,$0,$L124
slt	$14,$5,$12
.set	macro
.set	reorder

beq	$14,$0,$L124
lbu	$13,1($8)
sltu	$14,$13,4
beq	$14,$0,$L124
sb	$13,-1($9)
addiu	$8,$8,1
.set	noreorder
.set	nomacro
b	$L125
addiu	$5,$5,1
.set	macro
.set	reorder

$L124:
sb	$7,-1($9)
$L125:
addiu	$5,$5,1
addiu	$8,$8,1
slt	$7,$5,$2
.set	noreorder
.set	nomacro
bne	$7,$0,$L126
addiu	$3,$3,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L123
move	$2,$6
.set	macro
.set	reorder

$L128:
lw	$25,%call16(vc1_decode_entry_point)($28)
move	$4,$17
move	$5,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_entry_point
1:	jalr	$25
move	$6,$20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L163
lw	$28,56($sp)
.set	macro
.set	reorder

li	$10,1			# 0x1
.set	noreorder
.set	nomacro
b	$L120
sw	$10,80($sp)
.set	macro
.set	reorder

$L167:
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$21,$0,$L131
lw	$28,56($sp)
.set	macro
.set	reorder

lw	$24,80($sp)
bne	$24,$0,$L109
$L131:
lw	$6,%got($LC4)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L95
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L109:
lw	$2,664($17)
lw	$4,168($16)
lw	$3,660($17)
addiu	$2,$2,15
lw	$5,112($17)
lw	$25,%call16(av_malloc)($28)
sra	$2,$2,4
addiu	$3,$3,15
mul	$4,$2,$4
sltu	$5,$0,$5
sra	$3,$3,4
xori	$6,$5,0x1
sw	$5,264($17)
sw	$2,164($16)
sw	$6,10096($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
sw	$3,160($16)
.set	macro
.set	reorder

lw	$3,164($16)
lw	$4,168($16)
lw	$28,56($sp)
sw	$2,11320($16)
lw	$25,%call16(av_malloc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
mul	$4,$4,$3
.set	macro
.set	reorder

lw	$3,164($16)
lw	$4,168($16)
lw	$28,56($sp)
sw	$2,11324($16)
lw	$25,%call16(av_malloc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
mul	$4,$4,$3
.set	macro
.set	reorder

lw	$3,164($16)
lw	$4,168($16)
lw	$28,56($sp)
sw	$2,11888($16)
lw	$25,%call16(av_malloc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
mul	$4,$4,$3
.set	macro
.set	reorder

lw	$28,56($sp)
lw	$4,168($16)
sw	$2,11896($16)
lw	$25,%call16(av_malloc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
sll	$4,$4,3
.set	macro
.set	reorder

lw	$3,164($16)
lw	$6,168($16)
lw	$28,56($sp)
addiu	$5,$3,1
lw	$4,172($16)
sll	$3,$3,1
sw	$2,11936($16)
mul	$5,$6,$5
sll	$6,$6,2
lw	$25,%call16(av_malloc)($28)
addiu	$3,$3,1
addu	$2,$2,$6
sw	$2,11940($16)
sll	$2,$5,1
mul	$5,$3,$4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
addu	$4,$5,$2
.set	macro
.set	reorder

addiu	$4,$16,10584
lw	$8,164($16)
move	$5,$16
lw	$3,172($16)
lw	$7,168($16)
sll	$6,$8,1
lw	$28,56($sp)
addiu	$8,$8,1
sw	$2,11280($16)
addiu	$6,$6,1
addiu	$9,$7,1
mul	$10,$3,$6
lw	$25,%call16(ff_intrax8_common_init)($28)
addiu	$3,$3,1
addu	$3,$2,$3
sw	$3,11284($16)
addu	$6,$10,$9
addu	$2,$2,$6
mul	$6,$7,$8
sw	$2,11288($16)
addu	$7,$6,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_intrax8_common_init
1:	jalr	$25
sw	$7,11292($16)
.set	macro
.set	reorder

li	$4,1			# 0x1
lw	$28,56($sp)
move	$2,$0
lw	$3,%got(use_jz_buf)($28)
sw	$4,0($3)
$L95:
lw	$31,140($sp)
lw	$fp,136($sp)
lw	$23,132($sp)
lw	$22,128($sp)
lw	$21,124($sp)
lw	$20,120($sp)
lw	$19,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,144
.set	macro
.set	reorder

.end	vc1_decode_init
.size	vc1_decode_init, .-vc1_decode_init
.section	.rodata.str1.4
.align	2
$LC5:
.ascii	"Illegal DC VLC\012\000"
.section	.text.vc1_decode_intra_block,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_decode_intra_block
.type	vc1_decode_intra_block, @function
vc1_decode_intra_block:
.frame	$sp,144,$31		# vars= 80, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-144
sw	$fp,136($sp)
sw	$16,104($sp)
move	$16,$4
sw	$20,120($sp)
lw	$fp,7996($4)
lw	$4,168($4)
lw	$20,%got(dMB)($28)
lw	$2,7992($16)
mul	$9,$fp,$4
sw	$18,112($sp)
.cprestore	16
lw	$18,0($20)
lw	$3,11272($16)
lw	$8,11276($16)
addu	$18,$18,$6
sw	$19,116($sp)
sw	$31,140($sp)
addu	$fp,$9,$2
sw	$23,132($sp)
li	$2,1			# 0x1
sw	$22,128($sp)
sw	$21,124($sp)
sw	$17,108($sp)
lw	$19,2824($16)
sb	$2,6($18)
#APP
# 2592 "vc1dec.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
lw	$2,10284($16)
slt	$11,$6,4
.set	noreorder
.set	nomacro
beq	$11,$0,$L172
sll	$4,$2,4
.set	macro
.set	reorder

lw	$2,%got(ff_msmp4_dc_luma_vlc)($28)
addu	$2,$2,$4
lw	$9,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L313
lh	$21,0($4)
.set	macro
.set	reorder

$L175:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bltz	$21,$L314
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$21,$0,$L180
lw	$10,160($sp)
.set	macro
.set	reorder

li	$4,31			# 0x1f
slt	$2,$10,32
movz	$10,$4,$2
slt	$4,$10,0
move	$2,$10
movn	$2,$0,$4
li	$4,119			# 0x77
.set	noreorder
.set	nomacro
beq	$21,$4,$L315
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$4,$L316
li	$4,2			# 0x2
.set	macro
.set	reorder

beq	$2,$4,$L317
$L183:
subu	$2,$0,$21
$L338:
li	$4,13			# 0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 2624 "vc1dec.c" 1
movn	$21,$2,$4	#i_movn
# 0 "" 2
#NO_APP
$L180:
lw	$14,7996($16)
$L339:
sll	$17,$6,2
lw	$9,168($16)
addu	$17,$16,$17
lw	$23,7992($16)
lw	$13,2728($16)
mul	$2,$9,$14
lw	$10,2172($16)
lw	$12,8012($17)
sll	$12,$12,1
addu	$12,$13,$12
addu	$14,$2,$23
lw	$2,8036($17)
addu	$15,$10,$14
lh	$13,-2($12)
nor	$4,$0,$2
sll	$4,$4,1
sll	$2,$2,1
addu	$4,$12,$4
subu	$2,$12,$2
lh	$24,0($4)
lh	$22,0($2)
.set	noreorder
.set	nomacro
bne	$8,$0,$L318
lb	$2,0($15)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L256
addiu	$4,$6,-2
.set	macro
.set	reorder

li	$4,1			# 0x1
move	$22,$0
$L252:
addu	$21,$21,$22
sltu	$2,$3,1
sll	$21,$21,16
sra	$21,$21,16
sh	$21,0($12)
li	$12,1			# 0x1
sh	$21,0($5)
#APP
# 2643 "vc1dec.c" 1
movn	$4,$12,$2	#i_movn
# 0 "" 2
#NO_APP
move	$12,$0
sltu	$13,$8,1
#APP
# 2644 "vc1dec.c" 1
movn	$4,$12,$13	#i_movn
# 0 "" 2
#NO_APP
movz	$13,$0,$2
#APP
# 2645 "vc1dec.c" 1
movn	$19,$12,$13	#i_movn
# 0 "" 2
#NO_APP
lw	$2,%got(left_buf)($28)
.set	noreorder
.set	nomacro
beq	$11,$0,$L195
lw	$13,0($2)
.set	macro
.set	reorder

sra	$11,$6,1
sll	$11,$11,4
$L196:
sll	$14,$23,4
lw	$12,2812($16)
sll	$23,$23,6
andi	$2,$6,0x5
sll	$2,$2,3
subu	$23,$23,$14
addu	$13,$13,$11
addu	$23,$2,$23
sll	$23,$23,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L319
addu	$12,$12,$23
.set	macro
.set	reorder

addu	$2,$10,$fp
lb	$21,0($2)
.set	noreorder
.set	nomacro
bne	$8,$0,$L253
lb	$2,-1($2)
.set	macro
.set	reorder

move	$11,$0
move	$23,$13
$L254:
move	$8,$0
#APP
# 2654 "vc1dec.c" 1
movn	$8,$2,$11	#i_movn
# 0 "" 2
#NO_APP
subu	$2,$fp,$9
addu	$10,$10,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L263
lb	$2,0($10)
.set	macro
.set	reorder

beq	$3,$0,$L263
slt	$fp,$fp,$9
xori	$fp,$fp,0x1
$L199:
#APP
# 2655 "vc1dec.c" 1
movn	$8,$2,$fp	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$4,$0,$L320
li	$2,1			# 0x1
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$6,$2,$L341
li	$2,1			# 0x1
.set	macro
.set	reorder

xori	$2,$6,0x3
$L343:
sltu	$2,$2,1
#APP
# 2656 "vc1dec.c" 1
movn	$8,$21,$2	#i_movn
# 0 "" 2
#NO_APP
bne	$7,$0,$L321
$L203:
beq	$19,$0,$L241
beq	$8,$0,$L245
beq	$21,$8,$L245
lbu	$7,11228($16)
.set	noreorder
.set	nomacro
beq	$7,$21,$L322
sll	$3,$21,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$8,$7,$L323
sll	$2,$8,1
.set	macro
.set	reorder

move	$7,$0
$L247:
addu	$7,$2,$7
addiu	$3,$3,-2
addiu	$2,$7,-1
lw	$7,%got(ff_vc1_dqscale)($28)
sll	$3,$3,2
addiu	$9,$23,16
addu	$7,$7,$3
li	$8,131072			# 0x20000
addiu	$3,$23,2
lw	$7,0($7)
mul	$7,$2,$7
$L248:
lh	$2,0($3)
addiu	$3,$3,2
mul	$10,$2,$7
addu	$2,$10,$8
sra	$2,$2,18
.set	noreorder
.set	nomacro
bne	$3,$9,$L248
sh	$2,-2($3)
.set	macro
.set	reorder

$L245:
beq	$4,$0,$L324
lh	$2,2($13)
sh	$2,16($5)
#APP
# 2828 "vc1dec.c" 1
.word	0b01110001101000000000010001010000	#S32LDD XR1,$13,4
# 0 "" 2
# 2829 "vc1dec.c" 1
.word	0b01110001101000000000100010010000	#S32LDD XR2,$13,8
# 0 "" 2
# 2830 "vc1dec.c" 1
.word	0b01110001101000000000110011010000	#S32LDD XR3,$13,12
# 0 "" 2
# 2832 "vc1dec.c" 1
.word	0b01110000101000000100000001101011	#S16STD XR1,$5,32,PTN0
# 0 "" 2
# 2833 "vc1dec.c" 1
.word	0b01110000101010000110000001101011	#S16STD XR1,$5,48,PTN1
# 0 "" 2
# 2834 "vc1dec.c" 1
.word	0b01110000101000001000000010101011	#S16STD XR2,$5,64,PTN0
# 0 "" 2
# 2835 "vc1dec.c" 1
.word	0b01110000101010001010000010101011	#S16STD XR2,$5,80,PTN1
# 0 "" 2
# 2836 "vc1dec.c" 1
.word	0b01110000101000001100000011101011	#S16STD XR3,$5,96,PTN0
# 0 "" 2
# 2837 "vc1dec.c" 1
.word	0b01110000101010001110000011101011	#S16STD XR3,$5,112,PTN1
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
li	$7,8			# 0x8
sll	$2,$3,4
$L342:
addu	$2,$5,$2
lh	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L249
sll	$4,$3,3
.set	macro
.set	reorder

#APP
# 2840 "vc1dec.c" 1
.word	0b01110000000001000000001010101111	#S32I2M XR10,$4
# 0 "" 2
# 2841 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L249:
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$3,$7,$L342
sll	$2,$3,4
.set	macro
.set	reorder

#APP
# 2869 "vc1dec.c" 1
.word	0b01110001100000000000000000010001	#S32STD XR0,$12,0
# 0 "" 2
# 2870 "vc1dec.c" 1
.word	0b01110001100000000000010000010001	#S32STD XR0,$12,4
# 0 "" 2
# 2871 "vc1dec.c" 1
.word	0b01110001100000000000100000010001	#S32STD XR0,$12,8
# 0 "" 2
# 2872 "vc1dec.c" 1
.word	0b01110001100000000000110000010001	#S32STD XR0,$12,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L240
li	$19,63			# 0x3f
.set	macro
.set	reorder

$L172:
lw	$2,%got(ff_msmp4_dc_chroma_vlc)($28)
addu	$2,$2,$4
lw	$9,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bgez	$2,$L175
lh	$21,0($4)
.set	macro
.set	reorder

$L313:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$10,$2,4
ori	$2,$10,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addu	$4,$4,$21
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bgez	$2,$L175
lh	$21,0($4)
.set	macro
.set	reorder

ori	$10,$10,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$10,$2,4
ori	$2,$10,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addu	$4,$4,$21
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bgez	$2,$L175
lh	$21,0($4)
.set	macro
.set	reorder

ori	$10,$10,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$21,$2,$21
sll	$2,$21,2
addu	$9,$9,$2
lh	$21,0($9)
.set	noreorder
.set	nomacro
b	$L175
lh	$2,2($9)
.set	macro
.set	reorder

$L318:
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$6,$4,$L187
li	$4,3			# 0x3
.set	macro
.set	reorder

beq	$6,$4,$L188
lb	$4,-1($15)
beq	$4,$0,$L189
beq	$2,$4,$L189
lw	$15,2788($16)
addu	$25,$15,$2
addu	$15,$15,$4
lbu	$4,0($25)
lbu	$15,0($15)
addiu	$4,$4,-1
mul	$15,$13,$15
lw	$13,%got(ff_vc1_dqscale)($28)
sll	$4,$4,2
addu	$4,$13,$4
lw	$13,0($4)
li	$4,131072			# 0x20000
mul	$25,$15,$13
addu	$13,$25,$4
sra	$13,$13,18
$L189:
beq	$3,$0,$L305
addiu	$4,$6,-2
sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L251
subu	$4,$14,$9
.set	macro
.set	reorder

addu	$4,$10,$4
lb	$4,0($4)
beq	$4,$0,$L190
$L259:
beq	$2,$4,$L190
lw	$15,2788($16)
addu	$25,$15,$2
addu	$15,$15,$4
lbu	$4,0($25)
lbu	$15,0($15)
addiu	$4,$4,-1
mul	$15,$22,$15
lw	$22,%got(ff_vc1_dqscale)($28)
sll	$4,$4,2
addu	$4,$22,$4
lw	$22,0($4)
li	$4,131072			# 0x20000
mul	$25,$15,$22
addu	$22,$25,$4
sra	$22,$22,18
$L190:
.set	noreorder
.set	nomacro
bne	$8,$0,$L251
move	$4,$0
.set	macro
.set	reorder

b	$L252
$L188:
bne	$3,$0,$L251
$L305:
move	$22,$13
.set	noreorder
.set	nomacro
b	$L252
li	$4,1			# 0x1
.set	macro
.set	reorder

$L320:
.set	noreorder
.set	nomacro
bne	$6,$2,$L343
xori	$2,$6,0x3
.set	macro
.set	reorder

li	$2,1			# 0x1
$L341:
#APP
# 2656 "vc1dec.c" 1
movn	$8,$21,$2	#i_movn
# 0 "" 2
#NO_APP
beq	$7,$0,$L203
$L321:
lbu	$2,11228($16)
sltu	$2,$2,8
bne	$2,$0,$L266
lbu	$2,11240($16)
sltu	$2,$0,$2
sw	$2,40($sp)
$L204:
lw	$9,164($sp)
sll	$2,$9,5
sll	$3,$9,2
subu	$3,$2,$3
lw	$2,%got(residual_init_table)($28)
addu	$2,$2,$3
lw	$10,8($2)
lw	$11,12($2)
lw	$25,16($2)
lw	$3,20($2)
lw	$24,0($2)
lw	$15,4($2)
lw	$14,24($2)
sw	$10,24($sp)
sw	$11,28($sp)
sw	$25,32($sp)
sw	$3,36($sp)
#APP
# 1546 "vc1dec.c" 1
mtc0	$14,$21,0
# 0 "" 2
# 1547 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1551 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$7,1			# 0x1
lw	$25,%got(wmv1_scantable)($28)
li	$fp,-1026			# 0xfffffffffffffbfe
.set	noreorder
.set	nomacro
b	$L228
li	$22,2			# 0x2
.set	macro
.set	reorder

$L325:
slt	$2,$2,$24
addu	$9,$15,$9
xori	$2,$2,0x1
lb	$3,0($9)
lb	$9,1($9)
$L206:
addu	$7,$7,$3
slt	$3,$7,64
beq	$3,$0,$L226
$L327:
.set	noreorder
.set	nomacro
bne	$2,$0,$L227
lw	$3,%got(wmv1_scantable)($28)
.set	macro
.set	reorder

#APP
# 1607 "vc1dec.c" 1
mtc0	$14,$21,0
# 0 "" 2
# 1608 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$25,$7
addiu	$7,$7,1
lbu	$3,0($2)
#APP
# 1610 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1611 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$5,$3
sh	$9,0($3)
#APP
# 1613 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L228:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L325
sll	$9,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L267
move	$11,$0
.set	macro
.set	reorder

li	$9,13			# 0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
#NO_APP
subu	$11,$22,$11
beq	$11,$22,$L326
#APP
# 1563 "vc1dec.c" 1
mtc0	$14,$21,0
# 0 "" 2
# 1564 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1565 "vc1dec.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
and	$2,$10,$fp
sll	$9,$2,1
slt	$2,$2,$24
addu	$9,$15,$9
andi	$10,$10,0x1
xori	$2,$2,0x1
lb	$3,0($9)
.set	noreorder
.set	nomacro
bne	$11,$0,$L209
lb	$9,1($9)
.set	macro
.set	reorder

$L330:
.set	noreorder
.set	nomacro
beq	$2,$0,$L210
lw	$11,28($sp)
.set	macro
.set	reorder

lw	$11,24($sp)
addu	$11,$11,$3
lbu	$11,0($11)
sw	$11,44($sp)
lw	$11,44($sp)
addu	$9,$9,$11
$L212:
subu	$11,$0,$9
#APP
# 1579 "vc1dec.c" 1
movn	$9,$11,$10	#i_movn
# 0 "" 2
#NO_APP
addu	$7,$7,$3
$L337:
slt	$3,$7,64
bne	$3,$0,$L327
$L226:
#APP
# 2670 "vc1dec.c" 1
.word	0b01110000000000100000001011101110	#S32M2I XR11, $2
# 0 "" 2
#NO_APP
sra	$2,$2,3
addiu	$2,$2,1
andi	$2,$2,0x00ff
sb	$2,6($18)
beq	$19,$0,$L230
beq	$8,$0,$L231
.set	noreorder
.set	nomacro
beq	$21,$8,$L231
li	$2,3			# 0x3
.set	macro
.set	reorder

lbu	$7,11228($16)
sll	$3,$21,1
movz	$2,$0,$4
.set	noreorder
.set	nomacro
beq	$7,$21,$L328
move	$4,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$8,$7,$L329
sll	$2,$8,1
.set	macro
.set	reorder

move	$7,$0
$L234:
addiu	$3,$3,-2
addu	$2,$2,$7
lw	$7,%got(ff_vc1_dqscale)($28)
sll	$3,$3,2
addiu	$2,$2,-1
addu	$3,$7,$3
addiu	$9,$23,2
li	$8,1			# 0x1
lw	$11,0($3)
li	$15,131072			# 0x20000
li	$14,8			# 0x8
mul	$11,$2,$11
$L236:
lh	$2,0($9)
sll	$7,$8,$4
sll	$3,$7,1
mul	$10,$2,$11
addu	$3,$5,$3
addu	$2,$10,$15
lhu	$10,0($3)
sra	$2,$2,18
addu	$2,$2,$10
sll	$2,$2,16
sra	$2,$2,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L235
sh	$2,0($3)
.set	macro
.set	reorder

#APP
# 2683 "vc1dec.c" 1
.word	0b01110000000001110000001010101111	#S32I2M XR10,$7
# 0 "" 2
# 2684 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L235:
addiu	$8,$8,1
.set	noreorder
.set	nomacro
bne	$8,$14,$L236
addiu	$9,$9,2
.set	macro
.set	reorder

$L230:
lh	$2,16($5)
$L340:
sh	$2,2($13)
lh	$2,2($5)
sh	$2,2($12)
#APP
# 2786 "vc1dec.c" 1
.word	0b01110000101000000100000110101010	#S16LDD XR6,$5,32,PTN0
# 0 "" 2
# 2787 "vc1dec.c" 1
.word	0b01110000101010000110000110101010	#S16LDD XR6,$5,48,PTN1
# 0 "" 2
# 2788 "vc1dec.c" 1
.word	0b01110000101000001000000111101010	#S16LDD XR7,$5,64,PTN0
# 0 "" 2
# 2789 "vc1dec.c" 1
.word	0b01110000101010001010000111101010	#S16LDD XR7,$5,80,PTN1
# 0 "" 2
# 2790 "vc1dec.c" 1
.word	0b01110000101000001100001000101010	#S16LDD XR8,$5,96,PTN0
# 0 "" 2
# 2791 "vc1dec.c" 1
.word	0b01110000101010001110001000101010	#S16LDD XR8,$5,112,PTN1
# 0 "" 2
# 2793 "vc1dec.c" 1
.word	0b01110001101000000000010110010001	#S32STD XR6,$13,4
# 0 "" 2
# 2794 "vc1dec.c" 1
.word	0b01110001101000000000100111010001	#S32STD XR7,$13,8
# 0 "" 2
# 2795 "vc1dec.c" 1
.word	0b01110001101000000000111000010001	#S32STD XR8,$13,12
# 0 "" 2
# 2797 "vc1dec.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2798 "vc1dec.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2799 "vc1dec.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2801 "vc1dec.c" 1
.word	0b01110001100000000000010010010001	#S32STD XR2,$12,4
# 0 "" 2
# 2802 "vc1dec.c" 1
.word	0b01110001100000000000100011010001	#S32STD XR3,$12,8
# 0 "" 2
# 2803 "vc1dec.c" 1
.word	0b01110001100000000000110100010001	#S32STD XR4,$12,12
# 0 "" 2
#NO_APP
li	$3,63			# 0x3f
li	$2,1			# 0x1
movz	$3,$2,$19
.set	noreorder
.set	nomacro
b	$L240
move	$19,$3
.set	macro
.set	reorder

$L263:
.set	noreorder
.set	nomacro
b	$L199
move	$fp,$0
.set	macro
.set	reorder

$L319:
addu	$2,$10,$fp
move	$23,$12
move	$11,$0
lb	$21,0($2)
.set	noreorder
.set	nomacro
b	$L254
lb	$2,-1($2)
.set	macro
.set	reorder

$L195:
.set	noreorder
.set	nomacro
b	$L196
sll	$11,$6,4
.set	macro
.set	reorder

$L241:
#APP
# 2933 "vc1dec.c" 1
.word	0b01110001101000000000000000010001	#S32STD XR0,$13,0
# 0 "" 2
# 2934 "vc1dec.c" 1
.word	0b01110001101000000000010000010001	#S32STD XR0,$13,4
# 0 "" 2
# 2935 "vc1dec.c" 1
.word	0b01110001101000000000100000010001	#S32STD XR0,$13,8
# 0 "" 2
# 2936 "vc1dec.c" 1
.word	0b01110001101000000000110000010001	#S32STD XR0,$13,12
# 0 "" 2
# 2938 "vc1dec.c" 1
.word	0b01110001100000000000000000010001	#S32STD XR0,$12,0
# 0 "" 2
# 2939 "vc1dec.c" 1
.word	0b01110001100000000000010000010001	#S32STD XR0,$12,4
# 0 "" 2
# 2940 "vc1dec.c" 1
.word	0b01110001100000000000100000010001	#S32STD XR0,$12,8
# 0 "" 2
# 2941 "vc1dec.c" 1
.word	0b01110001100000000000110000010001	#S32STD XR0,$12,12
# 0 "" 2
#NO_APP
li	$19,1			# 0x1
$L240:
lw	$2,0($20)
#APP
# 2944 "vc1dec.c" 1
.word	0b01110000000000110000001011101110	#S32M2I XR11, $3
# 0 "" 2
#NO_APP
sra	$3,$3,3
addu	$6,$2,$6
addiu	$3,$3,1
move	$2,$0
andi	$3,$3,0x00ff
sb	$3,6($6)
sw	$19,8680($17)
$L300:
lw	$31,140($sp)
lw	$fp,136($sp)
lw	$23,132($sp)
lw	$22,128($sp)
lw	$21,124($sp)
lw	$20,120($sp)
lw	$19,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,144
.set	macro
.set	reorder

$L256:
sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L252
move	$4,$0
.set	macro
.set	reorder

subu	$4,$14,$9
addu	$4,$10,$4
lb	$4,0($4)
beq	$4,$0,$L190
b	$L259
$L251:
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$6,$4,$L191
move	$15,$24
.set	macro
.set	reorder

li	$4,1			# 0x1
$L346:
.set	noreorder
.set	nomacro
beq	$6,$4,$L192
li	$4,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$4,$L193
addiu	$14,$14,-1
.set	macro
.set	reorder

$L192:
subu	$14,$14,$9
$L193:
addu	$14,$10,$14
lb	$4,0($14)
.set	noreorder
.set	nomacro
beq	$4,$0,$L344
lw	$25,%call16(abs)($28)
.set	macro
.set	reorder

beq	$2,$4,$L344
lw	$14,2788($16)
addu	$2,$14,$2
addu	$4,$14,$4
lbu	$2,0($2)
lbu	$15,0($4)
lw	$4,%got(ff_vc1_dqscale)($28)
addiu	$2,$2,-1
mul	$24,$24,$15
sll	$2,$2,2
addu	$2,$4,$2
lw	$15,0($2)
li	$2,131072			# 0x20000
mul	$4,$24,$15
addu	$15,$4,$2
sra	$15,$15,18
$L191:
lw	$25,%call16(abs)($28)
$L344:
subu	$4,$22,$15
sw	$3,56($sp)
sw	$5,80($sp)
sw	$6,84($sp)
sw	$7,88($sp)
sw	$8,60($sp)
sw	$9,68($sp)
sw	$10,76($sp)
sw	$11,92($sp)
sw	$12,72($sp)
sw	$13,64($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
sw	$15,52($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$13,64($sp)
lw	$15,52($sp)
sw	$2,52($sp)
lw	$25,%call16(abs)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
subu	$4,$15,$13
.set	macro
.set	reorder

lw	$14,52($sp)
lw	$13,64($sp)
lw	$28,16($sp)
slt	$2,$2,$14
lw	$3,56($sp)
sltu	$4,$2,1
movz	$22,$13,$2
lw	$5,80($sp)
lw	$6,84($sp)
lw	$7,88($sp)
lw	$8,60($sp)
lw	$9,68($sp)
lw	$10,76($sp)
lw	$11,92($sp)
.set	noreorder
.set	nomacro
b	$L252
lw	$12,72($sp)
.set	macro
.set	reorder

$L253:
sltu	$11,$0,$fp
.set	noreorder
.set	nomacro
b	$L254
move	$23,$13
.set	macro
.set	reorder

$L324:
lh	$2,2($12)
sh	$2,2($5)
#APP
# 2888 "vc1dec.c" 1
.word	0b01110001100000000000010001010000	#S32LDD XR1,$12,4
# 0 "" 2
# 2889 "vc1dec.c" 1
.word	0b01110001100000000000100010010000	#S32LDD XR2,$12,8
# 0 "" 2
# 2890 "vc1dec.c" 1
.word	0b01110001100000000000110011010000	#S32LDD XR3,$12,12
# 0 "" 2
# 2892 "vc1dec.c" 1
.word	0b01110000101000000000010001010001	#S32STD XR1,$5,4
# 0 "" 2
# 2893 "vc1dec.c" 1
.word	0b01110000101000000000100010010001	#S32STD XR2,$5,8
# 0 "" 2
# 2894 "vc1dec.c" 1
.word	0b01110000101000000000110011010001	#S32STD XR3,$5,12
# 0 "" 2
# 2919 "vc1dec.c" 1
.word	0b01110001101000000000000000010001	#S32STD XR0,$13,0
# 0 "" 2
# 2920 "vc1dec.c" 1
.word	0b01110001101000000000010000010001	#S32STD XR0,$13,4
# 0 "" 2
# 2921 "vc1dec.c" 1
.word	0b01110001101000000000100000010001	#S32STD XR0,$13,8
# 0 "" 2
# 2922 "vc1dec.c" 1
.word	0b01110001101000000000110000010001	#S32STD XR0,$13,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L240
li	$19,63			# 0x3f
.set	macro
.set	reorder

$L266:
li	$7,1			# 0x1
.set	noreorder
.set	nomacro
b	$L204
sw	$7,40($sp)
.set	macro
.set	reorder

$L317:
li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$21,$21,1
addiu	$21,$21,-1
.set	noreorder
.set	nomacro
b	$L183
addu	$21,$21,$2
.set	macro
.set	reorder

$L267:
#APP
# 1563 "vc1dec.c" 1
mtc0	$14,$21,0
# 0 "" 2
# 1564 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1565 "vc1dec.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
and	$2,$10,$fp
sll	$9,$2,1
slt	$2,$2,$24
addu	$9,$15,$9
andi	$10,$10,0x1
xori	$2,$2,0x1
lb	$3,0($9)
.set	noreorder
.set	nomacro
beq	$11,$0,$L330
lb	$9,1($9)
.set	macro
.set	reorder

$L209:
.set	noreorder
.set	nomacro
beq	$2,$0,$L307
lw	$11,36($sp)
.set	macro
.set	reorder

lw	$11,32($sp)
$L307:
addu	$11,$11,$9
lbu	$11,0($11)
addiu	$11,$11,1
andi	$11,$11,0x00ff
sw	$11,44($sp)
lw	$11,44($sp)
addu	$3,$3,$11
subu	$11,$0,$9
#APP
# 1579 "vc1dec.c" 1
movn	$9,$11,$10	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L337
addu	$7,$7,$3
.set	macro
.set	reorder

$L227:
addu	$7,$3,$7
lbu	$2,0($7)
#APP
# 1617 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1618 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L226
sh	$9,0($2)
.set	macro
.set	reorder

$L315:
.set	noreorder
.set	nomacro
beq	$2,$4,$L332
li	$4,2			# 0x2
.set	macro
.set	reorder

beq	$2,$4,$L333
li	$2,125			# 0x7d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$21,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L338
subu	$2,$0,$21
.set	macro
.set	reorder

$L187:
beq	$3,$0,$L305
subu	$4,$14,$9
addu	$4,$10,$4
lb	$4,0($4)
.set	noreorder
.set	nomacro
bne	$4,$0,$L259
move	$15,$24
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L346
li	$4,1			# 0x1
.set	macro
.set	reorder

$L316:
li	$2,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$21,$21,2
li	$4,13			# 0xd
addiu	$21,$21,-3
addu	$21,$21,$2
subu	$2,$0,$21
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 2624 "vc1dec.c" 1
movn	$21,$2,$4	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L339
lw	$14,7996($16)
.set	macro
.set	reorder

$L231:
beq	$4,$0,$L238
lhu	$2,2($13)
lhu	$3,16($5)
addu	$2,$2,$3
sll	$2,$2,16
sra	$2,$2,16
sh	$2,16($5)
#APP
# 2710 "vc1dec.c" 1
.word	0b01110000101000000100000110101010	#S16LDD XR6,$5,32,PTN0
# 0 "" 2
# 2711 "vc1dec.c" 1
.word	0b01110000101010000110000110101010	#S16LDD XR6,$5,48,PTN1
# 0 "" 2
# 2712 "vc1dec.c" 1
.word	0b01110000101000001000000111101010	#S16LDD XR7,$5,64,PTN0
# 0 "" 2
# 2713 "vc1dec.c" 1
.word	0b01110000101010001010000111101010	#S16LDD XR7,$5,80,PTN1
# 0 "" 2
# 2714 "vc1dec.c" 1
.word	0b01110000101000001100001000101010	#S16LDD XR8,$5,96,PTN0
# 0 "" 2
# 2715 "vc1dec.c" 1
.word	0b01110000101010001110001000101010	#S16LDD XR8,$5,112,PTN1
# 0 "" 2
# 2717 "vc1dec.c" 1
.word	0b01110001101000000000010001010000	#S32LDD XR1,$13,4
# 0 "" 2
# 2718 "vc1dec.c" 1
.word	0b01110001101000000000100010010000	#S32LDD XR2,$13,8
# 0 "" 2
# 2719 "vc1dec.c" 1
.word	0b01110001101000000000110011010000	#S32LDD XR3,$13,12
# 0 "" 2
# 2721 "vc1dec.c" 1
.word	0b01110000000000000101101001001110	#Q16ADD XR9,XR6,XR1,XR0,AA,WW
# 0 "" 2
# 2722 "vc1dec.c" 1
.word	0b01110000000000001001111010001110	#Q16ADD XR10,XR7,XR2,XR0,AA,WW
# 0 "" 2
# 2723 "vc1dec.c" 1
.word	0b01110000000000001110000100001110	#Q16ADD XR4,XR8,XR3,XR0,AA,WW
# 0 "" 2
#NO_APP
li	$3,524288			# 0x80000
#APP
# 2725 "vc1dec.c" 1
.word	0b01110000000000110000000101101111	#S32I2M XR5,$3
# 0 "" 2
# 2726 "vc1dec.c" 1
.word	0b01110000010000000001010110110000	#D32SLL XR6,XR5,XR0,XR0,1
# 0 "" 2
#NO_APP
li	$3,8			# 0x8
movz	$3,$0,$2
#APP
# 2727 "vc1dec.c" 1
.word	0b01110000000000110000001100101111	#S32I2M XR12,$3
# 0 "" 2
# 2728 "vc1dec.c" 1
.word	0b01110000101000000100001001101011	#S16STD XR9,$5,32,PTN0
# 0 "" 2
# 2729 "vc1dec.c" 1
.word	0b01110000101010000110001001101011	#S16STD XR9,$5,48,PTN1
# 0 "" 2
# 2730 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 2731 "vc1dec.c" 1
.word	0b01110000000011010110011100111001	#D16MOVN XR12,XR9,XR5
# 0 "" 2
# 2733 "vc1dec.c" 1
.word	0b01110000101000001000001010101011	#S16STD XR10,$5,64,PTN0
# 0 "" 2
# 2734 "vc1dec.c" 1
.word	0b01110000101010001010001010101011	#S16STD XR10,$5,80,PTN1
# 0 "" 2
# 2735 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 2736 "vc1dec.c" 1
.word	0b01110000000011010110101100111001	#D16MOVN XR12,XR10,XR5
# 0 "" 2
# 2738 "vc1dec.c" 1
.word	0b01110000101000001100000100101011	#S16STD XR4,$5,96,PTN0
# 0 "" 2
# 2739 "vc1dec.c" 1
.word	0b01110000101010001110000100101011	#S16STD XR4,$5,112,PTN1
# 0 "" 2
# 2740 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 2741 "vc1dec.c" 1
.word	0b01110000000011010101001100111001	#D16MOVN XR12,XR4,XR5
# 0 "" 2
# 2742 "vc1dec.c" 1
.word	0b01110011001010110000001100111101	#S32SFL XR12,XR0,XR12,XR10,PTN3
# 0 "" 2
# 2743 "vc1dec.c" 1
.word	0b01110000000000101011001100000011	#S32MAX XR12,XR12,XR10
# 0 "" 2
# 2744 "vc1dec.c" 1
.word	0b01110000000000110010111011000011	#S32MAX XR11,XR11,XR12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L340
lh	$2,16($5)
.set	macro
.set	reorder

$L210:
addu	$11,$11,$3
lbu	$11,0($11)
sw	$11,44($sp)
lw	$11,44($sp)
.set	noreorder
.set	nomacro
b	$L212
addu	$9,$9,$11
.set	macro
.set	reorder

$L238:
lhu	$2,2($12)
lhu	$3,2($5)
addu	$2,$2,$3
sh	$2,2($5)
#APP
# 2757 "vc1dec.c" 1
.word	0b01110001100000000000010110010000	#S32LDD XR6,$12,4
# 0 "" 2
# 2758 "vc1dec.c" 1
.word	0b01110001100000000000100111010000	#S32LDD XR7,$12,8
# 0 "" 2
# 2759 "vc1dec.c" 1
.word	0b01110001100000000000111000010000	#S32LDD XR8,$12,12
# 0 "" 2
# 2761 "vc1dec.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2762 "vc1dec.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2763 "vc1dec.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2765 "vc1dec.c" 1
.word	0b01110000000000011000100010001110	#Q16ADD XR2,XR2,XR6,XR0,AA,WW
# 0 "" 2
# 2766 "vc1dec.c" 1
.word	0b01110000000000011100110011001110	#Q16ADD XR3,XR3,XR7,XR0,AA,WW
# 0 "" 2
# 2767 "vc1dec.c" 1
.word	0b01110000000000100001000100001110	#Q16ADD XR4,XR4,XR8,XR0,AA,WW
# 0 "" 2
# 2769 "vc1dec.c" 1
.word	0b01110000101000000000010010010001	#S32STD XR2,$5,4
# 0 "" 2
# 2770 "vc1dec.c" 1
.word	0b01110000101000000000100011010001	#S32STD XR3,$5,8
# 0 "" 2
# 2771 "vc1dec.c" 1
.word	0b01110000101000000000110100010001	#S32STD XR4,$5,12
# 0 "" 2
#NO_APP
li	$2,7			# 0x7
#APP
# 2773 "vc1dec.c" 1
.word	0b01110000000000100000001100101111	#S32I2M XR12,$2
# 0 "" 2
# 2774 "vc1dec.c" 1
.word	0b01110000000000110010111011000011	#S32MAX XR11,XR11,XR12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L340
lh	$2,16($5)
.set	macro
.set	reorder

$L328:
lbu	$9,11300($16)
sll	$2,$8,1
move	$7,$0
.set	noreorder
.set	nomacro
b	$L234
addu	$3,$3,$9
.set	macro
.set	reorder

$L332:
li	$2,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,29			# 0x1d
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$21,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
.set	noreorder
.set	nomacro
b	$L183
or	$21,$2,$21
.set	macro
.set	reorder

$L323:
.set	noreorder
.set	nomacro
b	$L247
lbu	$7,11300($16)
.set	macro
.set	reorder

$L322:
lbu	$9,11300($16)
sll	$2,$8,1
move	$7,$0
.set	noreorder
.set	nomacro
b	$L247
addu	$3,$3,$9
.set	macro
.set	reorder

$L326:
lw	$2,10312($16)
li	$9,13			# 0xd
lw	$3,10316($16)
sw	$2,100($sp)
sw	$3,44($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$10,100($sp)
.set	noreorder
.set	nomacro
beq	$10,$0,$L215
lw	$11,40($sp)
.set	macro
.set	reorder

addiu	$11,$10,-1
andi	$11,$11,0x7
sll	$11,$11,4
ori	$11,$11,0xd
$L216:
lw	$9,44($sp)
slt	$3,$9,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L334
li	$10,125			# 0x7d
.set	macro
.set	reorder

addiu	$3,$9,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L223:
li	$10,13			# 0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
lw	$10,100($sp)
sw	$9,48($sp)
slt	$9,$10,9
beq	$9,$0,$L335
#APP
# 90 "vlc_bs.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
$L225:
subu	$10,$0,$9
lw	$11,48($sp)
#APP
# 1598 "vc1dec.c" 1
movn	$9,$10,$11	#i_movn
# 0 "" 2
#NO_APP
lw	$10,100($sp)
lw	$11,44($sp)
sw	$10,10312($16)
.set	noreorder
.set	nomacro
b	$L206
sw	$11,10316($16)
.set	macro
.set	reorder

$L333:
li	$2,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,13			# 0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$21,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,1
.set	noreorder
.set	nomacro
b	$L183
or	$21,$2,$21
.set	macro
.set	reorder

$L215:
.set	noreorder
.set	nomacro
beq	$11,$0,$L268
move	$10,$0
.set	macro
.set	reorder

li	$3,45			# 0x2d
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$9,$0,$L218
sw	$9,100($sp)
.set	macro
.set	reorder

addiu	$11,$9,-1
andi	$11,$11,0x7
sll	$11,$11,4
ori	$11,$11,0xd
$L219:
li	$3,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L216
sw	$3,44($sp)
.set	macro
.set	reorder

$L329:
.set	noreorder
.set	nomacro
b	$L234
lbu	$7,11300($16)
.set	macro
.set	reorder

$L268:
li	$3,13			# 0xd
$L345:
#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
li	$9,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$9,$L336
addiu	$11,$10,1
.set	macro
.set	reorder

addiu	$10,$10,1
li	$11,6			# 0x6
.set	noreorder
.set	nomacro
bne	$10,$11,$L345
li	$3,13			# 0xd
.set	macro
.set	reorder

li	$10,8			# 0x8
li	$11,125			# 0x7d
.set	noreorder
.set	nomacro
b	$L219
sw	$10,100($sp)
.set	macro
.set	reorder

$L314:
lw	$6,%got($LC5)($28)
li	$5,16			# 0x10
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L300
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L218:
li	$10,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$10,7
addiu	$10,$10,8
andi	$11,$11,0x7
sll	$11,$11,4
sw	$10,100($sp)
.set	noreorder
.set	nomacro
b	$L219
ori	$11,$11,0xd
.set	macro
.set	reorder

$L335:
li	$9,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$10,-8
sll	$11,$9,$11
#APP
# 84 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L225
or	$9,$11,$9
.set	macro
.set	reorder

$L334:
#APP
# 76 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$9,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$9,$9,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,$9
#APP
# 84 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L223
or	$3,$3,$9
.set	macro
.set	reorder

$L336:
addiu	$10,$10,2
sll	$11,$11,4
sw	$10,100($sp)
.set	noreorder
.set	nomacro
b	$L219
ori	$11,$11,0xd
.set	macro
.set	reorder

.end	vc1_decode_intra_block
.size	vc1_decode_intra_block, .-vc1_decode_intra_block
.section	.text.vc1_decode_i_block_adv,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_decode_i_block_adv
.type	vc1_decode_i_block_adv, @function
vc1_decode_i_block_adv:
.frame	$sp,136,$31		# vars= 72, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-136
lw	$3,11272($4)
sw	$23,124($sp)
sw	$21,116($sp)
lw	$23,168($4)
lw	$21,7996($4)
sw	$20,112($sp)
.cprestore	16
mul	$2,$23,$21
sw	$fp,128($sp)
sw	$22,120($sp)
sw	$16,96($sp)
move	$16,$4
sw	$31,132($sp)
sw	$19,108($sp)
sw	$18,104($sp)
sw	$17,100($sp)
lw	$20,7992($4)
lw	$22,11276($4)
lw	$fp,2824($4)
addu	$21,$2,$20
#APP
# 2179 "vc1dec.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
lw	$2,10284($4)
slt	$8,$6,4
.set	noreorder
.set	nomacro
beq	$8,$0,$L348
sll	$4,$2,4
.set	macro
.set	reorder

lw	$2,%got(ff_msmp4_dc_luma_vlc)($28)
addu	$2,$2,$4
lw	$9,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L496
lh	$19,0($4)
.set	macro
.set	reorder

$L351:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bltz	$19,$L497
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$19,$0,$L525
sll	$17,$6,2
.set	macro
.set	reorder

li	$2,119			# 0x77
.set	noreorder
.set	nomacro
beq	$19,$2,$L498
lw	$10,156($sp)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$10,$2,$L499
lw	$14,156($sp)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$14,$2,$L500
li	$2,13			# 0xd
.set	macro
.set	reorder

$L359:
li	$2,13			# 0xd
$L524:
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$2,$0,$2
subu	$4,$0,$2
xor	$19,$4,$19
addu	$19,$19,$2
$L356:
sll	$17,$6,2
$L525:
lw	$12,2728($16)
lw	$10,2172($16)
addu	$17,$16,$17
addu	$9,$10,$21
lw	$11,8012($17)
lw	$2,8036($17)
lb	$13,0($9)
sll	$11,$11,1
nor	$4,$0,$2
addu	$11,$12,$11
sll	$4,$4,1
sll	$2,$2,1
addu	$4,$11,$4
lh	$14,-2($11)
subu	$2,$11,$2
lh	$15,0($4)
.set	noreorder
.set	nomacro
beq	$22,$0,$L362
lh	$12,0($2)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$6,$2,$L363
li	$2,3			# 0x3
.set	macro
.set	reorder

beq	$6,$2,$L364
lb	$2,-1($9)
beq	$2,$0,$L365
beq	$13,$2,$L365
lw	$4,2788($16)
addu	$18,$4,$13
addu	$4,$4,$2
lbu	$2,0($18)
lbu	$4,0($4)
addiu	$2,$2,-1
mul	$4,$14,$4
lw	$14,%got(ff_vc1_dqscale)($28)
sll	$2,$2,2
addu	$2,$14,$2
lw	$14,0($2)
li	$2,131072			# 0x20000
mul	$18,$4,$14
addu	$14,$18,$2
sra	$14,$14,18
$L365:
.set	noreorder
.set	nomacro
beq	$3,$0,$L435
addiu	$2,$6,-2
.set	macro
.set	reorder

$L529:
sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L366
subu	$2,$21,$23
.set	macro
.set	reorder

addu	$2,$10,$2
lb	$2,0($2)
bne	$2,$0,$L439
$L366:
.set	noreorder
.set	nomacro
bne	$22,$0,$L526
li	$2,3			# 0x3
.set	macro
.set	reorder

move	$18,$0
$L436:
addu	$12,$19,$12
move	$19,$fp
sll	$12,$12,16
sra	$12,$12,16
sh	$12,0($11)
sh	$12,0($5)
$L428:
lw	$2,%got(left_buf)($28)
.set	noreorder
.set	nomacro
beq	$8,$0,$L370
lw	$12,0($2)
.set	macro
.set	reorder

sra	$4,$6,1
sll	$4,$4,4
$L371:
sll	$2,$20,4
lw	$11,2812($16)
sll	$20,$20,6
lb	$25,0($9)
andi	$8,$6,0x5
sll	$8,$8,3
subu	$2,$20,$2
addu	$4,$12,$4
addu	$2,$8,$2
sll	$2,$2,1
.set	noreorder
.set	nomacro
beq	$18,$0,$L372
addu	$11,$11,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$22,$0,$L527
move	$24,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$21,$0,$L528
xori	$2,$6,0x1
.set	macro
.set	reorder

lb	$24,-1($9)
movz	$24,$25,$2
$L433:
xori	$2,$6,0x3
.set	noreorder
.set	nomacro
beq	$7,$0,$L376
movz	$24,$25,$2
.set	macro
.set	reorder

$L504:
.set	noreorder
.set	nomacro
beq	$fp,$0,$L446
lw	$12,%got(wmv1_scantable)($28)
.set	macro
.set	reorder

bne	$18,$0,$L501
lbu	$2,11228($16)
sltu	$2,$2,8
.set	noreorder
.set	nomacro
beq	$2,$0,$L502
addiu	$12,$12,128
.set	macro
.set	reorder

$L448:
li	$7,1			# 0x1
.set	noreorder
.set	nomacro
b	$L378
sw	$7,32($sp)
.set	macro
.set	reorder

$L348:
lw	$2,%got(ff_msmp4_dc_chroma_vlc)($28)
addu	$2,$2,$4
lw	$9,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bgez	$2,$L351
lh	$19,0($4)
.set	macro
.set	reorder

$L496:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$10,$2,4
ori	$2,$10,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addu	$4,$4,$19
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bgez	$2,$L351
lh	$19,0($4)
.set	macro
.set	reorder

ori	$10,$10,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$10,$2,4
ori	$2,$10,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$4,$2,$19
sll	$4,$4,2
addu	$4,$9,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bgez	$2,$L351
lh	$19,0($4)
.set	macro
.set	reorder

ori	$10,$10,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$19,$2,$19
sll	$2,$19,2
addu	$9,$9,$2
lh	$19,0($9)
.set	noreorder
.set	nomacro
b	$L351
lh	$2,2($9)
.set	macro
.set	reorder

$L362:
.set	noreorder
.set	nomacro
bne	$3,$0,$L529
addiu	$2,$6,-2
.set	macro
.set	reorder

sll	$2,$19,16
li	$18,1			# 0x1
sra	$2,$2,16
move	$19,$0
sh	$2,0($11)
.set	noreorder
.set	nomacro
b	$L428
sh	$2,0($5)
.set	macro
.set	reorder

$L527:
xori	$2,$6,0x1
$L528:
.set	noreorder
.set	nomacro
b	$L433
movz	$24,$25,$2
.set	macro
.set	reorder

$L372:
.set	noreorder
.set	nomacro
beq	$3,$0,$L374
move	$24,$0
.set	macro
.set	reorder

slt	$2,$21,$23
beq	$2,$0,$L503
$L374:
li	$2,2			# 0x2
bne	$6,$2,$L433
.set	noreorder
.set	nomacro
bne	$7,$0,$L504
move	$24,$25
.set	macro
.set	reorder

$L376:
beq	$19,$0,$L416
beq	$24,$0,$L420
.set	noreorder
.set	nomacro
beq	$25,$24,$L420
sll	$2,$25,1
.set	macro
.set	reorder

lbu	$7,11228($16)
move	$8,$11
.set	noreorder
.set	nomacro
beq	$7,$25,$L505
movn	$8,$4,$18
.set	macro
.set	reorder

move	$3,$0
$L422:
addu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$24,$7,$L506
sll	$2,$24,1
.set	macro
.set	reorder

move	$7,$0
$L423:
addiu	$3,$3,-2
addu	$2,$2,$7
lw	$7,%got(ff_vc1_dqscale)($28)
sll	$3,$3,2
addiu	$2,$2,-1
addu	$7,$7,$3
li	$9,131072			# 0x20000
addiu	$3,$8,2
lw	$7,0($7)
addiu	$8,$8,16
mul	$7,$2,$7
$L424:
lh	$2,0($3)
addiu	$3,$3,2
mul	$10,$2,$7
addu	$2,$10,$9
sra	$2,$2,18
.set	noreorder
.set	nomacro
bne	$3,$8,$L424
sh	$2,-2($3)
.set	macro
.set	reorder

$L420:
beq	$18,$0,$L507
lh	$2,2($4)
sh	$2,16($5)
#APP
# 2447 "vc1dec.c" 1
.word	0b01110000100000000000010001010000	#S32LDD XR1,$4,4
# 0 "" 2
# 2448 "vc1dec.c" 1
.word	0b01110000100000000000100010010000	#S32LDD XR2,$4,8
# 0 "" 2
# 2449 "vc1dec.c" 1
.word	0b01110000100000000000110011010000	#S32LDD XR3,$4,12
# 0 "" 2
# 2451 "vc1dec.c" 1
.word	0b01110000101000000100000001101011	#S16STD XR1,$5,32,PTN0
# 0 "" 2
# 2452 "vc1dec.c" 1
.word	0b01110000101010000110000001101011	#S16STD XR1,$5,48,PTN1
# 0 "" 2
# 2453 "vc1dec.c" 1
.word	0b01110000101000001000000010101011	#S16STD XR2,$5,64,PTN0
# 0 "" 2
# 2454 "vc1dec.c" 1
.word	0b01110000101010001010000010101011	#S16STD XR2,$5,80,PTN1
# 0 "" 2
# 2455 "vc1dec.c" 1
.word	0b01110000101000001100000011101011	#S16STD XR3,$5,96,PTN0
# 0 "" 2
# 2456 "vc1dec.c" 1
.word	0b01110000101010001110000011101011	#S16STD XR3,$5,112,PTN1
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
li	$7,8			# 0x8
sll	$2,$3,4
$L530:
addu	$2,$5,$2
lh	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L425
sll	$4,$3,3
.set	macro
.set	reorder

#APP
# 2459 "vc1dec.c" 1
.word	0b01110000000001000000001010101111	#S32I2M XR10,$4
# 0 "" 2
# 2460 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L425:
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$3,$7,$L530
sll	$2,$3,4
.set	macro
.set	reorder

#APP
# 2489 "vc1dec.c" 1
.word	0b01110001011000000000000000010001	#S32STD XR0,$11,0
# 0 "" 2
# 2490 "vc1dec.c" 1
.word	0b01110001011000000000010000010001	#S32STD XR0,$11,4
# 0 "" 2
# 2491 "vc1dec.c" 1
.word	0b01110001011000000000100000010001	#S32STD XR0,$11,8
# 0 "" 2
# 2492 "vc1dec.c" 1
.word	0b01110001011000000000110000010001	#S32STD XR0,$11,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L415
li	$19,63			# 0x3f
.set	macro
.set	reorder

$L370:
.set	noreorder
.set	nomacro
b	$L371
sll	$4,$6,4
.set	macro
.set	reorder

$L416:
#APP
# 2553 "vc1dec.c" 1
.word	0b01110000100000000000000000010001	#S32STD XR0,$4,0
# 0 "" 2
# 2554 "vc1dec.c" 1
.word	0b01110000100000000000010000010001	#S32STD XR0,$4,4
# 0 "" 2
# 2555 "vc1dec.c" 1
.word	0b01110000100000000000100000010001	#S32STD XR0,$4,8
# 0 "" 2
# 2556 "vc1dec.c" 1
.word	0b01110000100000000000110000010001	#S32STD XR0,$4,12
# 0 "" 2
# 2558 "vc1dec.c" 1
.word	0b01110001011000000000000000010001	#S32STD XR0,$11,0
# 0 "" 2
# 2559 "vc1dec.c" 1
.word	0b01110001011000000000010000010001	#S32STD XR0,$11,4
# 0 "" 2
# 2560 "vc1dec.c" 1
.word	0b01110001011000000000100000010001	#S32STD XR0,$11,8
# 0 "" 2
# 2561 "vc1dec.c" 1
.word	0b01110001011000000000110000010001	#S32STD XR0,$11,12
# 0 "" 2
#NO_APP
li	$19,1			# 0x1
$L415:
lw	$2,%got(dMB)($28)
lw	$2,0($2)
#APP
# 2564 "vc1dec.c" 1
.word	0b01110000000000110000001011101110	#S32M2I XR11, $3
# 0 "" 2
#NO_APP
sra	$3,$3,3
addu	$6,$2,$6
addiu	$3,$3,1
move	$2,$0
andi	$3,$3,0x00ff
sb	$3,6($6)
sw	$19,8680($17)
$L486:
lw	$31,132($sp)
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

$L446:
addiu	$12,$12,64
$L377:
lbu	$2,11228($16)
sltu	$2,$2,8
bne	$2,$0,$L448
$L502:
lbu	$2,11240($16)
sltu	$2,$0,$2
sw	$2,32($sp)
$L378:
lw	$8,152($sp)
sll	$2,$8,5
sll	$3,$8,2
subu	$3,$2,$3
lw	$2,%got(residual_init_table)($28)
addu	$2,$2,$3
lw	$9,8($2)
lw	$10,12($2)
lw	$15,0($2)
lw	$14,4($2)
lw	$fp,16($2)
lw	$23,20($2)
lw	$13,24($2)
sw	$9,24($sp)
sw	$10,28($sp)
#APP
# 1546 "vc1dec.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 1547 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1551 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$7,1			# 0x1
li	$21,-1026			# 0xfffffffffffffbfe
li	$22,13			# 0xd
.set	noreorder
.set	nomacro
b	$L402
li	$20,2			# 0x2
.set	macro
.set	reorder

$L508:
slt	$2,$2,$15
addu	$8,$14,$8
xori	$2,$2,0x1
lb	$3,0($8)
lb	$8,1($8)
$L380:
addu	$7,$7,$3
slt	$3,$7,64
beq	$3,$0,$L400
$L510:
bne	$2,$0,$L401
#APP
# 1607 "vc1dec.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 1608 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$12,$7
addiu	$7,$7,1
lbu	$3,0($2)
#APP
# 1610 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1611 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$5,$3
sh	$8,0($3)
#APP
# 1613 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L402:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L508
sll	$8,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L449
move	$10,$0
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$22,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
subu	$10,$20,$10
beq	$10,$20,$L509
#APP
# 1563 "vc1dec.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 1564 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1565 "vc1dec.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
and	$2,$9,$21
sll	$8,$2,1
slt	$2,$2,$15
addu	$8,$14,$8
andi	$9,$9,0x1
xori	$2,$2,0x1
lb	$3,0($8)
.set	noreorder
.set	nomacro
bne	$10,$0,$L383
lb	$8,1($8)
.set	macro
.set	reorder

$L513:
.set	noreorder
.set	nomacro
beq	$2,$0,$L384
lw	$10,28($sp)
.set	macro
.set	reorder

lw	$10,24($sp)
addu	$10,$10,$3
lbu	$10,0($10)
sw	$10,36($sp)
lw	$10,36($sp)
addu	$8,$8,$10
$L386:
subu	$10,$0,$8
#APP
# 1579 "vc1dec.c" 1
movn	$8,$10,$9	#i_movn
# 0 "" 2
#NO_APP
addu	$7,$7,$3
$L522:
slt	$3,$7,64
bne	$3,$0,$L510
$L400:
beq	$19,$0,$L404
beq	$24,$0,$L405
beq	$25,$24,$L405
lbu	$7,11228($16)
.set	noreorder
.set	nomacro
beq	$7,$25,$L511
sll	$2,$25,1
.set	macro
.set	reorder

move	$3,$0
$L406:
addu	$2,$2,$3
.set	noreorder
.set	nomacro
beq	$24,$7,$L512
sll	$3,$24,1
.set	macro
.set	reorder

move	$8,$0
$L407:
addu	$8,$3,$8
addiu	$2,$2,-2
.set	noreorder
.set	nomacro
beq	$18,$0,$L408
addiu	$8,$8,-1
.set	macro
.set	reorder

lw	$3,%got(ff_vc1_dqscale)($28)
sll	$2,$2,2
addiu	$10,$4,2
li	$9,1			# 0x1
addu	$2,$3,$2
li	$15,131072			# 0x20000
li	$14,8			# 0x8
lw	$13,0($2)
$L410:
lh	$2,0($10)
sll	$3,$9,4
sll	$12,$9,3
addu	$3,$5,$3
mul	$2,$2,$8
lhu	$7,0($3)
mul	$16,$2,$13
addu	$2,$16,$15
sra	$2,$2,18
addu	$2,$2,$7
sll	$2,$2,16
sra	$2,$2,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L409
sh	$2,0($3)
.set	macro
.set	reorder

#APP
# 2274 "vc1dec.c" 1
.word	0b01110000000011000000001010101111	#S32I2M XR10,$12
# 0 "" 2
# 2275 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L409:
addiu	$9,$9,1
.set	noreorder
.set	nomacro
bne	$9,$14,$L410
addiu	$10,$10,2
.set	macro
.set	reorder

$L404:
lh	$2,16($5)
$L523:
sh	$2,2($4)
lh	$2,2($5)
sh	$2,2($11)
#APP
# 2359 "vc1dec.c" 1
.word	0b01110000101000000100000110101010	#S16LDD XR6,$5,32,PTN0
# 0 "" 2
# 2360 "vc1dec.c" 1
.word	0b01110000101010000110000110101010	#S16LDD XR6,$5,48,PTN1
# 0 "" 2
# 2361 "vc1dec.c" 1
.word	0b01110000101000001000000111101010	#S16LDD XR7,$5,64,PTN0
# 0 "" 2
# 2362 "vc1dec.c" 1
.word	0b01110000101010001010000111101010	#S16LDD XR7,$5,80,PTN1
# 0 "" 2
# 2363 "vc1dec.c" 1
.word	0b01110000101000001100001000101010	#S16LDD XR8,$5,96,PTN0
# 0 "" 2
# 2364 "vc1dec.c" 1
.word	0b01110000101010001110001000101010	#S16LDD XR8,$5,112,PTN1
# 0 "" 2
# 2366 "vc1dec.c" 1
.word	0b01110000100000000000010110010001	#S32STD XR6,$4,4
# 0 "" 2
# 2367 "vc1dec.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 2368 "vc1dec.c" 1
.word	0b01110000100000000000111000010001	#S32STD XR8,$4,12
# 0 "" 2
# 2370 "vc1dec.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2371 "vc1dec.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2372 "vc1dec.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2374 "vc1dec.c" 1
.word	0b01110001011000000000010010010001	#S32STD XR2,$11,4
# 0 "" 2
# 2375 "vc1dec.c" 1
.word	0b01110001011000000000100011010001	#S32STD XR3,$11,8
# 0 "" 2
# 2376 "vc1dec.c" 1
.word	0b01110001011000000000110100010001	#S32STD XR4,$11,12
# 0 "" 2
#NO_APP
li	$3,63			# 0x3f
li	$2,1			# 0x1
movz	$3,$2,$19
.set	noreorder
.set	nomacro
b	$L415
move	$19,$3
.set	macro
.set	reorder

$L449:
#APP
# 1563 "vc1dec.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 1564 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1565 "vc1dec.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
and	$2,$9,$21
sll	$8,$2,1
slt	$2,$2,$15
addu	$8,$14,$8
andi	$9,$9,0x1
xori	$2,$2,0x1
lb	$3,0($8)
.set	noreorder
.set	nomacro
beq	$10,$0,$L513
lb	$8,1($8)
.set	macro
.set	reorder

$L383:
.set	noreorder
.set	nomacro
beq	$2,$0,$L387
addu	$10,$23,$8
.set	macro
.set	reorder

addu	$10,$fp,$8
lbu	$10,0($10)
addiu	$10,$10,1
andi	$10,$10,0x00ff
addu	$3,$3,$10
$L516:
subu	$10,$0,$8
#APP
# 1579 "vc1dec.c" 1
movn	$8,$10,$9	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L522
addu	$7,$7,$3
.set	macro
.set	reorder

$L401:
addu	$7,$12,$7
lbu	$2,0($7)
#APP
# 1617 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1618 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L400
sh	$8,0($2)
.set	macro
.set	reorder

$L364:
.set	noreorder
.set	nomacro
beq	$3,$0,$L435
li	$2,3			# 0x3
.set	macro
.set	reorder

$L526:
.set	noreorder
.set	nomacro
beq	$6,$2,$L367
move	$18,$15
.set	macro
.set	reorder

$L440:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$6,$2,$L441
li	$4,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$4,$L369
addiu	$2,$21,-1
.set	macro
.set	reorder

$L368:
subu	$2,$2,$23
$L369:
addu	$2,$10,$2
lb	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L531
lw	$25,%call16(abs)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$13,$2,$L532
subu	$4,$12,$18
.set	macro
.set	reorder

lw	$4,2788($16)
addu	$13,$4,$13
addu	$4,$4,$2
lbu	$2,0($13)
lbu	$18,0($4)
lw	$4,%got(ff_vc1_dqscale)($28)
addiu	$2,$2,-1
mul	$15,$15,$18
sll	$2,$2,2
addu	$2,$4,$2
lw	$18,0($2)
li	$2,131072			# 0x20000
mul	$4,$15,$18
addu	$18,$4,$2
sra	$18,$18,18
$L367:
lw	$25,%call16(abs)($28)
$L531:
subu	$4,$12,$18
$L532:
sw	$3,44($sp)
sw	$5,64($sp)
sw	$6,68($sp)
sw	$7,72($sp)
sw	$8,76($sp)
sw	$9,80($sp)
sw	$10,60($sp)
sw	$11,52($sp)
sw	$12,56($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
sw	$14,48($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$14,48($sp)
lw	$25,%call16(abs)($28)
subu	$4,$18,$14
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
move	$18,$2
.set	macro
.set	reorder

lw	$12,56($sp)
slt	$2,$2,$18
lw	$14,48($sp)
sltu	$18,$2,1
lw	$28,16($sp)
lw	$3,44($sp)
movz	$12,$14,$2
lw	$5,64($sp)
lw	$6,68($sp)
lw	$7,72($sp)
lw	$8,76($sp)
lw	$9,80($sp)
lw	$10,60($sp)
.set	noreorder
.set	nomacro
b	$L436
lw	$11,52($sp)
.set	macro
.set	reorder

$L363:
.set	noreorder
.set	nomacro
beq	$3,$0,$L435
subu	$2,$21,$23
.set	macro
.set	reorder

addu	$2,$10,$2
lb	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L440
move	$18,$15
.set	macro
.set	reorder

$L439:
beq	$13,$2,$L366
lw	$4,2788($16)
addu	$18,$4,$13
addu	$4,$4,$2
lbu	$2,0($18)
lbu	$4,0($4)
addiu	$2,$2,-1
mul	$4,$12,$4
lw	$12,%got(ff_vc1_dqscale)($28)
sll	$2,$2,2
addu	$2,$12,$2
lw	$12,0($2)
li	$2,131072			# 0x20000
mul	$18,$4,$12
addu	$12,$18,$2
.set	noreorder
.set	nomacro
b	$L366
sra	$12,$12,18
.set	macro
.set	reorder

$L435:
addu	$14,$19,$14
li	$18,1			# 0x1
sll	$14,$14,16
move	$19,$fp
sra	$14,$14,16
sh	$14,0($11)
.set	noreorder
.set	nomacro
b	$L428
sh	$14,0($5)
.set	macro
.set	reorder

$L387:
lbu	$10,0($10)
addiu	$10,$10,1
andi	$10,$10,0x00ff
.set	noreorder
.set	nomacro
b	$L516
addu	$3,$3,$10
.set	macro
.set	reorder

$L384:
addu	$10,$10,$3
lbu	$10,0($10)
sw	$10,36($sp)
lw	$10,36($sp)
.set	noreorder
.set	nomacro
b	$L386
addu	$8,$8,$10
.set	macro
.set	reorder

$L405:
beq	$18,$0,$L413
lhu	$2,2($4)
lhu	$3,16($5)
addu	$2,$2,$3
sll	$2,$2,16
sra	$2,$2,16
sh	$2,16($5)
#APP
# 2286 "vc1dec.c" 1
.word	0b01110000101000000100000110101010	#S16LDD XR6,$5,32,PTN0
# 0 "" 2
# 2287 "vc1dec.c" 1
.word	0b01110000101010000110000110101010	#S16LDD XR6,$5,48,PTN1
# 0 "" 2
# 2288 "vc1dec.c" 1
.word	0b01110000101000001000000111101010	#S16LDD XR7,$5,64,PTN0
# 0 "" 2
# 2289 "vc1dec.c" 1
.word	0b01110000101010001010000111101010	#S16LDD XR7,$5,80,PTN1
# 0 "" 2
# 2290 "vc1dec.c" 1
.word	0b01110000101000001100001000101010	#S16LDD XR8,$5,96,PTN0
# 0 "" 2
# 2291 "vc1dec.c" 1
.word	0b01110000101010001110001000101010	#S16LDD XR8,$5,112,PTN1
# 0 "" 2
# 2293 "vc1dec.c" 1
.word	0b01110000100000000000010001010000	#S32LDD XR1,$4,4
# 0 "" 2
# 2294 "vc1dec.c" 1
.word	0b01110000100000000000100010010000	#S32LDD XR2,$4,8
# 0 "" 2
# 2295 "vc1dec.c" 1
.word	0b01110000100000000000110011010000	#S32LDD XR3,$4,12
# 0 "" 2
# 2297 "vc1dec.c" 1
.word	0b01110000000000000101101001001110	#Q16ADD XR9,XR6,XR1,XR0,AA,WW
# 0 "" 2
# 2298 "vc1dec.c" 1
.word	0b01110000000000001001111010001110	#Q16ADD XR10,XR7,XR2,XR0,AA,WW
# 0 "" 2
# 2299 "vc1dec.c" 1
.word	0b01110000000000001110000100001110	#Q16ADD XR4,XR8,XR3,XR0,AA,WW
# 0 "" 2
#NO_APP
li	$3,524288			# 0x80000
#APP
# 2301 "vc1dec.c" 1
.word	0b01110000000000110000000101101111	#S32I2M XR5,$3
# 0 "" 2
# 2302 "vc1dec.c" 1
.word	0b01110000010000000001010110110000	#D32SLL XR6,XR5,XR0,XR0,1
# 0 "" 2
#NO_APP
li	$3,8			# 0x8
movz	$3,$0,$2
#APP
# 2303 "vc1dec.c" 1
.word	0b01110000000000110000001100101111	#S32I2M XR12,$3
# 0 "" 2
# 2304 "vc1dec.c" 1
.word	0b01110000101000000100001001101011	#S16STD XR9,$5,32,PTN0
# 0 "" 2
# 2305 "vc1dec.c" 1
.word	0b01110000101010000110001001101011	#S16STD XR9,$5,48,PTN1
# 0 "" 2
# 2306 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 2307 "vc1dec.c" 1
.word	0b01110000000011010110011100111001	#D16MOVN XR12,XR9,XR5
# 0 "" 2
# 2309 "vc1dec.c" 1
.word	0b01110000101000001000001010101011	#S16STD XR10,$5,64,PTN0
# 0 "" 2
# 2310 "vc1dec.c" 1
.word	0b01110000101010001010001010101011	#S16STD XR10,$5,80,PTN1
# 0 "" 2
# 2311 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 2312 "vc1dec.c" 1
.word	0b01110000000011010110101100111001	#D16MOVN XR12,XR10,XR5
# 0 "" 2
# 2314 "vc1dec.c" 1
.word	0b01110000101000001100000100101011	#S16STD XR4,$5,96,PTN0
# 0 "" 2
# 2315 "vc1dec.c" 1
.word	0b01110000101010001110000100101011	#S16STD XR4,$5,112,PTN1
# 0 "" 2
# 2316 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 2317 "vc1dec.c" 1
.word	0b01110000000011010101001100111001	#D16MOVN XR12,XR4,XR5
# 0 "" 2
# 2318 "vc1dec.c" 1
.word	0b01110011001010110000001100111101	#S32SFL XR12,XR0,XR12,XR10,PTN3
# 0 "" 2
# 2319 "vc1dec.c" 1
.word	0b01110000000000101011001100000011	#S32MAX XR12,XR12,XR10
# 0 "" 2
# 2320 "vc1dec.c" 1
.word	0b01110000000000110010111011000011	#S32MAX XR11,XR11,XR12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L523
lh	$2,16($5)
.set	macro
.set	reorder

$L507:
lh	$2,2($11)
sh	$2,2($5)
#APP
# 2508 "vc1dec.c" 1
.word	0b01110001011000000000010001010000	#S32LDD XR1,$11,4
# 0 "" 2
# 2509 "vc1dec.c" 1
.word	0b01110001011000000000100010010000	#S32LDD XR2,$11,8
# 0 "" 2
# 2510 "vc1dec.c" 1
.word	0b01110001011000000000110011010000	#S32LDD XR3,$11,12
# 0 "" 2
# 2512 "vc1dec.c" 1
.word	0b01110000101000000000010001010001	#S32STD XR1,$5,4
# 0 "" 2
# 2513 "vc1dec.c" 1
.word	0b01110000101000000000100010010001	#S32STD XR2,$5,8
# 0 "" 2
# 2514 "vc1dec.c" 1
.word	0b01110000101000000000110011010001	#S32STD XR3,$5,12
# 0 "" 2
# 2539 "vc1dec.c" 1
.word	0b01110000100000000000000000010001	#S32STD XR0,$4,0
# 0 "" 2
# 2540 "vc1dec.c" 1
.word	0b01110000100000000000010000010001	#S32STD XR0,$4,4
# 0 "" 2
# 2541 "vc1dec.c" 1
.word	0b01110000100000000000100000010001	#S32STD XR0,$4,8
# 0 "" 2
# 2542 "vc1dec.c" 1
.word	0b01110000100000000000110000010001	#S32STD XR0,$4,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L415
li	$19,63			# 0x3f
.set	macro
.set	reorder

$L503:
subu	$21,$21,$23
addu	$10,$10,$21
.set	noreorder
.set	nomacro
b	$L374
lb	$24,0($10)
.set	macro
.set	reorder

$L501:
.set	noreorder
.set	nomacro
b	$L377
addiu	$12,$12,192
.set	macro
.set	reorder

$L500:
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$19,$19,1
addiu	$19,$19,-1
.set	noreorder
.set	nomacro
b	$L359
addu	$19,$19,$2
.set	macro
.set	reorder

$L498:
lw	$4,156($sp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$4,$2,$L517
lw	$9,156($sp)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$9,$2,$L518
li	$2,125			# 0x7d
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$19,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L524
li	$2,13			# 0xd
.set	macro
.set	reorder

$L499:
li	$2,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$19,$19,2
addiu	$19,$19,-3
addu	$19,$19,$2
li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$2,$0,$2
subu	$4,$0,$2
xor	$19,$4,$19
.set	noreorder
.set	nomacro
b	$L356
addu	$19,$19,$2
.set	macro
.set	reorder

$L413:
lhu	$2,2($11)
lhu	$3,2($5)
addu	$2,$2,$3
sh	$2,2($5)
#APP
# 2333 "vc1dec.c" 1
.word	0b01110001011000000000010110010000	#S32LDD XR6,$11,4
# 0 "" 2
# 2334 "vc1dec.c" 1
.word	0b01110001011000000000100111010000	#S32LDD XR7,$11,8
# 0 "" 2
# 2335 "vc1dec.c" 1
.word	0b01110001011000000000111000010000	#S32LDD XR8,$11,12
# 0 "" 2
# 2337 "vc1dec.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2338 "vc1dec.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2339 "vc1dec.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2341 "vc1dec.c" 1
.word	0b01110000000000011000100010001110	#Q16ADD XR2,XR2,XR6,XR0,AA,WW
# 0 "" 2
# 2342 "vc1dec.c" 1
.word	0b01110000000000011100110011001110	#Q16ADD XR3,XR3,XR7,XR0,AA,WW
# 0 "" 2
# 2343 "vc1dec.c" 1
.word	0b01110000000000100001000100001110	#Q16ADD XR4,XR4,XR8,XR0,AA,WW
# 0 "" 2
# 2345 "vc1dec.c" 1
.word	0b01110000101000000000010010010001	#S32STD XR2,$5,4
# 0 "" 2
# 2346 "vc1dec.c" 1
.word	0b01110000101000000000100011010001	#S32STD XR3,$5,8
# 0 "" 2
# 2347 "vc1dec.c" 1
.word	0b01110000101000000000110100010001	#S32STD XR4,$5,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L523
lh	$2,16($5)
.set	macro
.set	reorder

$L509:
lw	$2,10312($16)
lw	$3,10316($16)
sw	$2,88($sp)
sw	$3,36($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$22,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$8,88($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L389
lw	$9,32($sp)
.set	macro
.set	reorder

addiu	$10,$8,-1
andi	$10,$10,0x7
sll	$10,$10,4
ori	$10,$10,0xd
$L390:
lw	$8,36($sp)
slt	$3,$8,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L519
li	$9,125			# 0x7d
.set	macro
.set	reorder

addiu	$3,$8,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L397:
#APP
# 90 "vlc_bs.c" 1
mtc0	$22,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
sw	$9,40($sp)
lw	$9,88($sp)
slt	$8,$9,9
beq	$8,$0,$L520
#APP
# 90 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
$L399:
subu	$9,$0,$8
lw	$10,40($sp)
#APP
# 1598 "vc1dec.c" 1
movn	$8,$9,$10	#i_movn
# 0 "" 2
#NO_APP
lw	$9,88($sp)
lw	$10,36($sp)
sw	$9,10312($16)
.set	noreorder
.set	nomacro
b	$L380
sw	$10,10316($16)
.set	macro
.set	reorder

$L441:
.set	noreorder
.set	nomacro
b	$L368
move	$2,$21
.set	macro
.set	reorder

$L408:
lw	$3,%got(ff_vc1_dqscale)($28)
sll	$2,$2,2
addiu	$7,$5,2
addiu	$9,$11,2
addu	$2,$3,$2
addiu	$13,$5,16
li	$12,131072			# 0x20000
lw	$10,0($2)
$L412:
lh	$2,0($9)
addiu	$7,$7,2
lhu	$3,-2($7)
addiu	$9,$9,2
mul	$2,$2,$8
mul	$14,$2,$10
addu	$2,$14,$12
sra	$2,$2,18
addu	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$7,$13,$L412
sh	$2,-2($7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L523
lh	$2,16($5)
.set	macro
.set	reorder

$L389:
.set	noreorder
.set	nomacro
beq	$9,$0,$L450
li	$10,45			# 0x2d
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$0,$L392
sw	$3,88($sp)
.set	macro
.set	reorder

addiu	$10,$3,-1
andi	$10,$10,0x7
sll	$10,$10,4
ori	$10,$10,0xd
$L393:
li	$3,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L390
sw	$3,36($sp)
.set	macro
.set	reorder

$L517:
li	$2,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,29			# 0x1d
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$19,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
.set	noreorder
.set	nomacro
b	$L359
or	$19,$2,$19
.set	macro
.set	reorder

$L518:
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,13			# 0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$19,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,1
.set	noreorder
.set	nomacro
b	$L359
or	$19,$2,$19
.set	macro
.set	reorder

$L512:
.set	noreorder
.set	nomacro
b	$L407
lbu	$8,11300($16)
.set	macro
.set	reorder

$L511:
.set	noreorder
.set	nomacro
b	$L406
lbu	$3,11300($16)
.set	macro
.set	reorder

$L506:
.set	noreorder
.set	nomacro
b	$L423
lbu	$7,11300($16)
.set	macro
.set	reorder

$L505:
.set	noreorder
.set	nomacro
b	$L422
lbu	$3,11300($16)
.set	macro
.set	reorder

$L450:
move	$9,$0
$L391:
#APP
# 102 "vlc_bs.c" 1
mtc0	$22,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
li	$8,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$8,$L521
addiu	$10,$9,1
.set	macro
.set	reorder

addiu	$9,$9,1
li	$10,6			# 0x6
.set	noreorder
.set	nomacro
bne	$9,$10,$L391
li	$10,125			# 0x7d
.set	macro
.set	reorder

li	$9,8			# 0x8
.set	noreorder
.set	nomacro
b	$L393
sw	$9,88($sp)
.set	macro
.set	reorder

$L392:
li	$8,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
addiu	$10,$9,7
addiu	$9,$9,8
andi	$10,$10,0x7
sll	$10,$10,4
sw	$9,88($sp)
.set	noreorder
.set	nomacro
b	$L393
ori	$10,$10,0xd
.set	macro
.set	reorder

$L497:
lw	$6,%got($LC5)($28)
li	$5,16			# 0x10
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L486
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L520:
li	$8,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
addiu	$10,$9,-8
sll	$10,$8,$10
#APP
# 84 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L399
or	$8,$10,$8
.set	macro
.set	reorder

$L519:
#APP
# 76 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$8,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$8,$8,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,$8
#APP
# 84 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L397
or	$3,$3,$8
.set	macro
.set	reorder

$L521:
addiu	$9,$9,2
sll	$10,$10,4
sw	$9,88($sp)
.set	noreorder
.set	nomacro
b	$L393
ori	$10,$10,0xd
.set	macro
.set	reorder

.end	vc1_decode_i_block_adv
.size	vc1_decode_i_block_adv, .-vc1_decode_i_block_adv
.section	.text.vc1_decode_p_block_vlc.isra.7,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_decode_p_block_vlc.isra.7
.type	vc1_decode_p_block_vlc.isra.7, @function
vc1_decode_p_block_vlc.isra.7:
.frame	$sp,88,$31		# vars= 40, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-88
.cprestore	0
sw	$fp,84($sp)
sw	$23,80($sp)
sw	$22,76($sp)
sw	$21,72($sp)
sw	$20,68($sp)
sw	$19,64($sp)
sw	$18,60($sp)
sw	$17,56($sp)
sw	$16,52($sp)
#APP
# 2961 "vc1dec.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
li	$2,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
beq	$7,$2,$L534
li	$3,79			# 0x4f
.set	macro
.set	reorder

andi	$2,$7,0x7
$L535:
li	$3,7			# 0x7
.set	noreorder
.set	nomacro
beq	$2,$3,$L721
li	$3,95			# 0x5f
.set	macro
.set	reorder

bne	$2,$0,$L722
$L670:
move	$11,$0
$L547:
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L550
sltu	$7,$2,4
.set	macro
.set	reorder

beq	$7,$0,$L551
.set	noreorder
.set	nomacro
bne	$2,$0,$L672
sll	$8,$6,2
.set	macro
.set	reorder

li	$2,1			# 0x1
sll	$7,$6,1
sll	$2,$2,$8
lw	$8,%got(dMB)($28)
sll	$3,$3,$7
lw	$15,0($8)
lw	$7,12($15)
or	$2,$2,$7
sw	$2,12($15)
lw	$2,16($15)
or	$3,$3,$2
sw	$3,16($15)
lbu	$2,11228($4)
sltu	$2,$2,8
bne	$2,$0,$L673
lbu	$2,11240($4)
sltu	$2,$0,$2
sw	$2,8($sp)
$L555:
lw	$2,%got(vc1_hw_codingset2)($28)
lw	$3,0($2)
sll	$2,$3,2
sll	$3,$3,5
subu	$3,$3,$2
lw	$2,%got(residual_init_table)($28)
addu	$2,$2,$3
lw	$13,0($2)
lw	$12,4($2)
lw	$20,8($2)
lw	$19,12($2)
lw	$18,16($2)
lw	$17,20($2)
lw	$11,24($2)
#APP
# 1645 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 1646 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1650 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
move	$7,$0
lw	$14,%got(wmv1_scantable)($28)
li	$25,-1026			# 0xfffffffffffffbfe
li	$16,13			# 0xd
li	$24,2			# 0x2
li	$fp,29			# 0x1d
li	$21,1			# 0x1
.set	noreorder
.set	nomacro
b	$L579
li	$23,6			# 0x6
.set	macro
.set	reorder

$L723:
slt	$2,$2,$13
addu	$8,$12,$8
xori	$2,$2,0x1
lb	$3,0($8)
lb	$8,1($8)
$L557:
addu	$7,$7,$3
slt	$3,$7,64
beq	$3,$0,$L577
$L725:
.set	noreorder
.set	nomacro
bne	$2,$0,$L578
lw	$3,%got(wmv1_scantable)($28)
.set	macro
.set	reorder

#APP
# 1706 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
#NO_APP
addu	$2,$14,$7
addiu	$7,$7,1
lbu	$3,0($2)
#APP
# 1708 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1709 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
# 1710 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
# 1711 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$5,$3
sh	$8,0($3)
$L579:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L723
sll	$8,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L674
move	$10,$0
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$16,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
subu	$10,$24,$10
beq	$10,$24,$L724
#APP
# 1662 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 1663 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1664 "vc1dec.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
and	$2,$9,$25
sll	$8,$2,1
slt	$2,$2,$13
addu	$8,$12,$8
andi	$9,$9,0x1
xori	$2,$2,0x1
lb	$3,0($8)
.set	noreorder
.set	nomacro
bne	$10,$0,$L560
lb	$8,1($8)
.set	macro
.set	reorder

$L735:
.set	noreorder
.set	nomacro
beq	$2,$0,$L561
addu	$10,$19,$3
.set	macro
.set	reorder

addu	$10,$20,$3
lbu	$10,0($10)
addu	$8,$8,$10
$L563:
subu	$10,$0,$8
#APP
# 1678 "vc1dec.c" 1
movn	$8,$10,$9	#i_movn
# 0 "" 2
#NO_APP
addu	$7,$7,$3
$L759:
slt	$3,$7,64
bne	$3,$0,$L725
$L577:
#APP
# 3004 "vc1dec.c" 1
.word	0b01110000000000100000001011101110	#S32M2I XR11, $2
# 0 "" 2
#NO_APP
sra	$2,$2,3
lw	$3,%got(dma_len)($28)
addu	$6,$15,$6
addiu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L711
li	$25,15			# 0xf
.set	macro
.set	reorder

$L722:
lbu	$3,11256($4)
.set	noreorder
.set	nomacro
bne	$3,$0,$L761
li	$3,13			# 0xd
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
beq	$7,$3,$L539
andi	$7,$7,0x8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$0,$L539
lw	$3,104($sp)
.set	macro
.set	reorder

$L763:
.set	noreorder
.set	nomacro
bne	$3,$0,$L541
li	$3,13			# 0xd
.set	macro
.set	reorder

$L761:
#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$7,$0,$L543
move	$11,$0
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$11,1
beq	$11,$0,$L666
xori	$11,$11,0x3
$L543:
addiu	$3,$2,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L667
addiu	$3,$2,-4
.set	macro
.set	reorder

li	$7,6			# 0x6
andi	$3,$3,0x00ff
sltu	$3,$3,2
.set	noreorder
.set	nomacro
b	$L547
movn	$2,$7,$3
.set	macro
.set	reorder

$L721:
#APP
# 137 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
lw	$7,11316($4)
sll	$3,$3,2
sll	$8,$7,4
lw	$7,%got(ff_vc1_subblkpat_vlc)($28)
addu	$7,$7,$8
lw	$7,4($7)
addu	$7,$7,$3
lh	$3,2($7)
addiu	$3,$3,-1
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
lh	$11,0($7)
addiu	$11,$11,1
.set	noreorder
.set	nomacro
b	$L547
nor	$11,$0,$11
.set	macro
.set	reorder

$L534:
lw	$2,11316($4)
#APP
# 137 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
lw	$8,%got(ff_vc1_ttblk_vlc)($28)
sll	$9,$2,4
sll	$3,$3,2
addu	$8,$8,$9
lw	$8,4($8)
addu	$8,$8,$3
lh	$3,2($8)
addiu	$3,$3,-1
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
lh	$3,0($8)
sll	$2,$2,3
addu	$2,$2,$3
lw	$3,%got(ff_vc1_ttblk_to_tt)($28)
sll	$2,$2,2
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L535
lbu	$2,0($2)
.set	macro
.set	reorder

$L551:
li	$3,6			# 0x6
.set	noreorder
.set	nomacro
beq	$2,$3,$L553
li	$3,7			# 0x7
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$3,$L672
lw	$3,%got(dMB)($28)
.set	macro
.set	reorder

sll	$7,$6,2
lw	$17,%got(ff_vc1_simple_progressive_4x4_zz)($28)
nor	$2,$0,$11
addiu	$7,$7,3
li	$13,3			# 0x3
lw	$fp,0($3)
move	$24,$0
lw	$3,%got(vc1_hw_codingset2)($28)
li	$22,1			# 0x1
sw	$7,8($sp)
li	$23,-1026			# 0xfffffffffffffbfe
li	$21,2			# 0x2
lw	$7,16($fp)
li	$20,-1			# 0xffffffffffffffff
lw	$3,0($3)
andi	$25,$2,0xf
move	$19,$5
sll	$18,$3,2
sw	$7,16($fp)
sll	$3,$3,5
subu	$3,$3,$18
lw	$18,%got(residual_init_table)($28)
addu	$18,$18,$3
sll	$2,$22,$13
$L762:
and	$2,$2,$11
.set	noreorder
.set	nomacro
beq	$2,$0,$L726
lw	$3,8($sp)
.set	macro
.set	reorder

$L580:
addiu	$13,$13,-1
.set	noreorder
.set	nomacro
bne	$13,$20,$L762
sll	$2,$22,$13
.set	macro
.set	reorder

lw	$3,%got(dma_len)($28)
sra	$2,$24,3
addu	$6,$fp,$6
$L711:
andi	$2,$2,0x00ff
lw	$4,0($3)
sb	$2,6($6)
lbu	$2,6($6)
lw	$fp,84($sp)
lw	$23,80($sp)
andi	$2,$2,0x00ff
lw	$22,76($sp)
addu	$2,$2,$4
lw	$21,72($sp)
lw	$20,68($sp)
lw	$19,64($sp)
lw	$18,60($sp)
lw	$17,56($sp)
lw	$16,52($sp)
sw	$2,0($3)
move	$2,$25
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L726:
sll	$10,$24,1
addu	$10,$19,$10
subu	$2,$3,$13
lw	$3,12($fp)
sll	$2,$22,$2
or	$2,$2,$3
sw	$2,12($fp)
lbu	$2,11228($4)
sltu	$2,$2,8
bne	$2,$0,$L677
lbu	$2,11240($4)
sltu	$2,$0,$2
sw	$2,24($sp)
$L581:
lw	$7,8($18)
lw	$8,12($18)
lw	$9,16($18)
lw	$15,0($18)
lw	$14,4($18)
lw	$16,20($18)
lw	$12,24($18)
sw	$7,12($sp)
sw	$8,16($sp)
sw	$9,20($sp)
#APP
# 1645 "vc1dec.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 1646 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1650 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L605
move	$5,$0
.set	macro
.set	reorder

$L727:
slt	$2,$2,$15
addu	$7,$14,$7
xori	$2,$2,0x1
lb	$3,0($7)
lb	$7,1($7)
$L583:
addu	$5,$5,$3
slt	$3,$5,16
beq	$3,$0,$L603
$L729:
bne	$2,$0,$L604
#APP
# 1706 "vc1dec.c" 1
mtc0	$12,$21,0
# 0 "" 2
#NO_APP
addu	$2,$17,$5
addiu	$5,$5,1
lbu	$3,0($2)
#APP
# 1708 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1709 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
# 1710 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
# 1711 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$10,$3
sh	$7,0($3)
$L605:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L727
sll	$7,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L678
move	$9,$0
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
subu	$9,$21,$9
beq	$9,$21,$L728
#APP
# 1662 "vc1dec.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 1663 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1664 "vc1dec.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
and	$2,$8,$23
sll	$7,$2,1
slt	$2,$2,$15
addu	$7,$14,$7
andi	$8,$8,0x1
xori	$2,$2,0x1
lb	$3,0($7)
.set	noreorder
.set	nomacro
bne	$9,$0,$L586
lb	$7,1($7)
.set	macro
.set	reorder

$L733:
.set	noreorder
.set	nomacro
beq	$2,$0,$L587
lw	$9,16($sp)
.set	macro
.set	reorder

lw	$9,12($sp)
addu	$9,$9,$3
lbu	$9,0($9)
sw	$9,28($sp)
lw	$9,28($sp)
addu	$7,$7,$9
$L589:
subu	$9,$0,$7
#APP
# 1678 "vc1dec.c" 1
movn	$7,$9,$8	#i_movn
# 0 "" 2
#NO_APP
addu	$5,$5,$3
$L758:
slt	$3,$5,16
bne	$3,$0,$L729
$L603:
.set	noreorder
.set	nomacro
b	$L580
addiu	$24,$24,16
.set	macro
.set	reorder

$L666:
.set	noreorder
.set	nomacro
b	$L543
move	$11,$0
.set	macro
.set	reorder

$L677:
li	$5,1			# 0x1
.set	noreorder
.set	nomacro
b	$L581
sw	$5,24($sp)
.set	macro
.set	reorder

$L673:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L555
sw	$2,8($sp)
.set	macro
.set	reorder

$L672:
move	$25,$0
lw	$fp,84($sp)
lw	$23,80($sp)
lw	$22,76($sp)
move	$2,$25
lw	$21,72($sp)
lw	$20,68($sp)
lw	$19,64($sp)
lw	$18,60($sp)
lw	$17,56($sp)
lw	$16,52($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L539:
lw	$3,11104($4)
.set	noreorder
.set	nomacro
beq	$3,$0,$L763
lw	$3,104($sp)
.set	macro
.set	reorder

$L541:
addiu	$3,$2,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L545
li	$3,1			# 0x1
.set	macro
.set	reorder

li	$11,2			# 0x2
xori	$2,$2,0x2
movz	$11,$3,$2
li	$3,12			# 0xc
li	$7,3			# 0x3
move	$25,$3
movn	$25,$7,$2
$L662:
li	$2,2			# 0x2
lw	$14,11232($4)
sll	$3,$6,1
li	$17,1			# 0x1
sll	$3,$2,$3
lw	$2,%got(dMB)($28)
li	$18,1			# 0x1
sll	$fp,$6,2
move	$23,$0
lw	$24,0($2)
li	$20,-1026			# 0xfffffffffffffbfe
lw	$2,%got(vc1_hw_codingset2)($28)
li	$21,13			# 0xd
li	$22,2			# 0x2
move	$16,$4
lw	$8,16($24)
move	$15,$5
lw	$2,0($2)
or	$3,$3,$8
sll	$7,$2,2
sll	$2,$2,5
sw	$3,16($24)
lw	$3,%got(residual_init_table)($28)
subu	$2,$2,$7
addu	$19,$3,$2
sll	$2,$18,$17
and	$2,$2,$11
beq	$2,$0,$L730
$L607:
.set	noreorder
.set	nomacro
beq	$17,$0,$L710
addiu	$fp,$fp,2
.set	macro
.set	reorder

move	$17,$0
sll	$2,$18,$17
and	$2,$2,$11
bne	$2,$0,$L607
$L730:
lw	$3,12($24)
sll	$2,$18,$fp
sll	$9,$23,1
or	$2,$2,$3
addu	$9,$15,$9
sw	$2,12($24)
lbu	$2,11228($16)
sltu	$2,$2,8
bne	$2,$0,$L681
lbu	$2,11240($16)
sltu	$2,$0,$2
sw	$2,24($sp)
$L608:
lw	$2,8($19)
lw	$3,12($19)
lw	$5,16($19)
lw	$7,20($19)
lw	$13,0($19)
lw	$12,4($19)
lw	$10,24($19)
sw	$2,8($sp)
sw	$3,12($sp)
sw	$5,16($sp)
sw	$7,20($sp)
#APP
# 1645 "vc1dec.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 1646 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1650 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L632
move	$4,$0
.set	macro
.set	reorder

$L731:
slt	$2,$2,$13
addu	$5,$12,$5
xori	$2,$2,0x1
lb	$3,0($5)
lb	$5,1($5)
$L610:
addu	$4,$4,$3
$L757:
slt	$3,$4,32
beq	$3,$0,$L630
bne	$2,$0,$L631
#APP
# 1706 "vc1dec.c" 1
mtc0	$10,$21,0
# 0 "" 2
#NO_APP
addu	$2,$14,$4
addiu	$4,$4,1
lbu	$3,0($2)
#APP
# 1708 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1709 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
# 1710 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
# 1711 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$9,$3
sh	$5,0($3)
$L632:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L731
sll	$5,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L611
move	$8,$0
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
subu	$8,$22,$8
beq	$8,$22,$L732
$L611:
#APP
# 1662 "vc1dec.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 1663 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1664 "vc1dec.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
and	$2,$7,$20
sll	$5,$2,1
slt	$2,$2,$13
addu	$5,$12,$5
andi	$7,$7,0x1
xori	$2,$2,0x1
lb	$3,0($5)
.set	noreorder
.set	nomacro
bne	$8,$0,$L613
lb	$5,1($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L614
lw	$8,12($sp)
.set	macro
.set	reorder

lw	$8,8($sp)
addu	$8,$8,$3
lbu	$8,0($8)
sw	$8,28($sp)
lw	$8,28($sp)
addu	$5,$5,$8
$L616:
subu	$8,$0,$5
#APP
# 1678 "vc1dec.c" 1
movn	$5,$8,$7	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L757
addu	$4,$4,$3
.set	macro
.set	reorder

$L710:
sra	$2,$23,3
lw	$3,%got(dma_len)($28)
.set	noreorder
.set	nomacro
b	$L711
addu	$6,$24,$6
.set	macro
.set	reorder

$L678:
#APP
# 1662 "vc1dec.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 1663 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1664 "vc1dec.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
and	$2,$8,$23
sll	$7,$2,1
slt	$2,$2,$15
addu	$7,$14,$7
andi	$8,$8,0x1
xori	$2,$2,0x1
lb	$3,0($7)
.set	noreorder
.set	nomacro
beq	$9,$0,$L733
lb	$7,1($7)
.set	macro
.set	reorder

$L586:
.set	noreorder
.set	nomacro
beq	$2,$0,$L590
addu	$9,$16,$7
.set	macro
.set	reorder

lw	$9,20($sp)
addu	$9,$9,$7
lbu	$9,0($9)
addiu	$9,$9,1
andi	$9,$9,0x00ff
sw	$9,28($sp)
lw	$9,28($sp)
addu	$3,$3,$9
$L738:
subu	$9,$0,$7
#APP
# 1678 "vc1dec.c" 1
movn	$7,$9,$8	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L758
addu	$5,$5,$3
.set	macro
.set	reorder

$L631:
addu	$4,$14,$4
lbu	$2,0($4)
#APP
# 1722 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1723 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addu	$9,$9,$2
sh	$5,0($9)
$L630:
.set	noreorder
.set	nomacro
b	$L607
addiu	$23,$23,32
.set	macro
.set	reorder

$L674:
#APP
# 1662 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 1663 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1664 "vc1dec.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
and	$2,$9,$25
sll	$8,$2,1
slt	$2,$2,$13
addu	$8,$12,$8
andi	$9,$9,0x1
xori	$2,$2,0x1
lb	$3,0($8)
.set	noreorder
.set	nomacro
beq	$10,$0,$L735
lb	$8,1($8)
.set	macro
.set	reorder

$L560:
.set	noreorder
.set	nomacro
beq	$2,$0,$L564
addu	$10,$17,$8
.set	macro
.set	reorder

addu	$10,$18,$8
lbu	$10,0($10)
addiu	$10,$10,1
andi	$10,$10,0x00ff
addu	$3,$3,$10
$L739:
subu	$10,$0,$8
#APP
# 1678 "vc1dec.c" 1
movn	$8,$10,$9	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L759
addu	$7,$7,$3
.set	macro
.set	reorder

$L604:
addu	$5,$17,$5
lbu	$2,0($5)
#APP
# 1722 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1723 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addiu	$24,$24,16
addu	$10,$10,$2
.set	noreorder
.set	nomacro
b	$L580
sh	$7,0($10)
.set	macro
.set	reorder

$L578:
addu	$7,$3,$7
lbu	$2,0($7)
#APP
# 1722 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1723 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addu	$5,$5,$2
.set	noreorder
.set	nomacro
b	$L577
sh	$8,0($5)
.set	macro
.set	reorder

$L667:
.set	noreorder
.set	nomacro
b	$L547
li	$2,3			# 0x3
.set	macro
.set	reorder

$L613:
.set	noreorder
.set	nomacro
bne	$2,$0,$L708
lw	$8,16($sp)
.set	macro
.set	reorder

lw	$8,20($sp)
$L708:
addu	$8,$8,$5
lbu	$8,0($8)
addiu	$8,$8,1
andi	$8,$8,0x00ff
sw	$8,28($sp)
lw	$8,28($sp)
addu	$3,$3,$8
subu	$8,$0,$5
#APP
# 1678 "vc1dec.c" 1
movn	$5,$8,$7	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L757
addu	$4,$4,$3
.set	macro
.set	reorder

$L590:
lbu	$9,0($9)
addiu	$9,$9,1
andi	$9,$9,0x00ff
sw	$9,28($sp)
lw	$9,28($sp)
.set	noreorder
.set	nomacro
b	$L738
addu	$3,$3,$9
.set	macro
.set	reorder

$L587:
addu	$9,$9,$3
lbu	$9,0($9)
sw	$9,28($sp)
lw	$9,28($sp)
.set	noreorder
.set	nomacro
b	$L589
addu	$7,$7,$9
.set	macro
.set	reorder

$L681:
li	$10,1			# 0x1
.set	noreorder
.set	nomacro
b	$L608
sw	$10,24($sp)
.set	macro
.set	reorder

$L564:
lbu	$10,0($10)
addiu	$10,$10,1
andi	$10,$10,0x00ff
.set	noreorder
.set	nomacro
b	$L739
addu	$3,$3,$10
.set	macro
.set	reorder

$L561:
lbu	$10,0($10)
.set	noreorder
.set	nomacro
b	$L563
addu	$8,$8,$10
.set	macro
.set	reorder

$L614:
addu	$8,$8,$3
lbu	$8,0($8)
sw	$8,28($sp)
lw	$8,28($sp)
.set	noreorder
.set	nomacro
b	$L616
addu	$5,$5,$8
.set	macro
.set	reorder

$L728:
lw	$2,10312($4)
li	$7,13			# 0xd
lw	$3,10316($4)
sw	$2,40($sp)
sw	$3,28($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$8,40($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L592
lw	$9,24($sp)
.set	macro
.set	reorder

addiu	$9,$8,-1
andi	$9,$9,0x7
sll	$9,$9,4
ori	$9,$9,0xd
$L593:
lw	$7,28($sp)
slt	$3,$7,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L740
li	$8,125			# 0x7d
.set	macro
.set	reorder

addiu	$3,$7,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L600:
li	$8,13			# 0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
lw	$8,40($sp)
sw	$7,32($sp)
slt	$7,$8,9
beq	$7,$0,$L741
#APP
# 90 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
$L602:
subu	$8,$0,$7
lw	$9,32($sp)
#APP
# 1697 "vc1dec.c" 1
movn	$7,$8,$9	#i_movn
# 0 "" 2
#NO_APP
lw	$8,40($sp)
lw	$9,28($sp)
sw	$8,10312($4)
.set	noreorder
.set	nomacro
b	$L583
sw	$9,10316($4)
.set	macro
.set	reorder

$L724:
lw	$3,10316($4)
lw	$9,10312($4)
sw	$3,20($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$16,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$9,$0,$L566
lw	$8,8($sp)
.set	macro
.set	reorder

addiu	$8,$9,-1
andi	$8,$8,0x7
sll	$8,$8,4
ori	$8,$8,0xd
$L567:
lw	$10,20($sp)
slt	$3,$10,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L742
li	$22,125			# 0x7d
.set	macro
.set	reorder

addiu	$3,$10,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L574:
#APP
# 90 "vlc_bs.c" 1
mtc0	$16,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$22,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$22,$21,1
# 0 "" 2
#NO_APP
sw	$22,12($sp)
slt	$22,$9,9
.set	noreorder
.set	nomacro
beq	$22,$0,$L743
li	$10,125			# 0x7d
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
$L576:
subu	$22,$0,$8
lw	$10,12($sp)
#APP
# 1697 "vc1dec.c" 1
movn	$8,$22,$10	#i_movn
# 0 "" 2
#NO_APP
lw	$22,20($sp)
sw	$9,10312($4)
.set	noreorder
.set	nomacro
b	$L557
sw	$22,10316($4)
.set	macro
.set	reorder

$L545:
addiu	$3,$2,-4
andi	$3,$3,0x00ff
sltu	$3,$3,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L670
li	$3,1			# 0x1
.set	macro
.set	reorder

li	$11,2			# 0x2
xori	$2,$2,0x5
movz	$11,$3,$2
li	$3,10			# 0xa
li	$7,5			# 0x5
move	$25,$3
movn	$25,$7,$2
$L663:
li	$2,1			# 0x1
lw	$14,11236($4)
sll	$3,$6,1
li	$17,1			# 0x1
sll	$3,$2,$3
lw	$2,%got(dMB)($28)
li	$18,1			# 0x1
sll	$fp,$6,2
move	$23,$0
lw	$24,0($2)
li	$20,-1026			# 0xfffffffffffffbfe
lw	$2,%got(vc1_hw_codingset2)($28)
li	$21,13			# 0xd
li	$22,2			# 0x2
move	$16,$4
lw	$8,16($24)
move	$15,$5
lw	$2,0($2)
or	$3,$3,$8
sll	$7,$2,2
sll	$2,$2,5
sw	$3,16($24)
lw	$3,%got(residual_init_table)($28)
subu	$2,$2,$7
addu	$19,$3,$2
sll	$2,$18,$17
and	$2,$2,$11
beq	$2,$0,$L744
$L634:
.set	noreorder
.set	nomacro
beq	$17,$0,$L710
addiu	$fp,$fp,2
.set	macro
.set	reorder

move	$17,$0
sll	$2,$18,$17
and	$2,$2,$11
bne	$2,$0,$L634
$L744:
lw	$3,12($24)
sll	$2,$18,$fp
sll	$9,$23,1
or	$2,$2,$3
addu	$9,$15,$9
sw	$2,12($24)
lbu	$2,11228($16)
sltu	$2,$2,8
bne	$2,$0,$L686
lbu	$2,11240($16)
sltu	$2,$0,$2
sw	$2,24($sp)
$L635:
lw	$2,8($19)
lw	$3,12($19)
lw	$5,16($19)
lw	$7,20($19)
lw	$13,0($19)
lw	$12,4($19)
lw	$10,24($19)
sw	$2,8($sp)
sw	$3,12($sp)
sw	$5,16($sp)
sw	$7,20($sp)
#APP
# 1645 "vc1dec.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 1646 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1650 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L659
move	$4,$0
.set	macro
.set	reorder

$L745:
slt	$2,$2,$13
addu	$5,$12,$5
xori	$2,$2,0x1
lb	$3,0($5)
lb	$5,1($5)
$L637:
addu	$4,$4,$3
$L760:
slt	$3,$4,32
beq	$3,$0,$L657
bne	$2,$0,$L658
#APP
# 1706 "vc1dec.c" 1
mtc0	$10,$21,0
# 0 "" 2
#NO_APP
addu	$2,$14,$4
addiu	$4,$4,1
lbu	$3,0($2)
#APP
# 1708 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1709 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
# 1710 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
# 1711 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$9,$3
sh	$5,0($3)
$L659:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L745
sll	$5,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L638
move	$8,$0
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
subu	$8,$22,$8
beq	$8,$22,$L746
$L638:
#APP
# 1662 "vc1dec.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 1663 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1664 "vc1dec.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
and	$2,$7,$20
sll	$5,$2,1
slt	$2,$2,$13
addu	$5,$12,$5
andi	$7,$7,0x1
xori	$2,$2,0x1
lb	$3,0($5)
.set	noreorder
.set	nomacro
bne	$8,$0,$L640
lb	$5,1($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L641
lw	$8,12($sp)
.set	macro
.set	reorder

lw	$8,8($sp)
addu	$8,$8,$3
lbu	$8,0($8)
sw	$8,28($sp)
lw	$8,28($sp)
addu	$5,$5,$8
$L643:
subu	$8,$0,$5
#APP
# 1678 "vc1dec.c" 1
movn	$5,$8,$7	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L760
addu	$4,$4,$3
.set	macro
.set	reorder

$L658:
addu	$4,$14,$4
lbu	$2,0($4)
#APP
# 1722 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1723 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addu	$9,$9,$2
sh	$5,0($9)
$L657:
.set	noreorder
.set	nomacro
b	$L634
addiu	$23,$23,32
.set	macro
.set	reorder

$L640:
.set	noreorder
.set	nomacro
beq	$2,$0,$L709
lw	$8,20($sp)
.set	macro
.set	reorder

lw	$8,16($sp)
$L709:
addu	$8,$8,$5
lbu	$8,0($8)
addiu	$8,$8,1
andi	$8,$8,0x00ff
sw	$8,28($sp)
lw	$8,28($sp)
addu	$3,$3,$8
subu	$8,$0,$5
#APP
# 1678 "vc1dec.c" 1
movn	$5,$8,$7	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L760
addu	$4,$4,$3
.set	macro
.set	reorder

$L686:
li	$10,1			# 0x1
.set	noreorder
.set	nomacro
b	$L635
sw	$10,24($sp)
.set	macro
.set	reorder

$L641:
addu	$8,$8,$3
lbu	$8,0($8)
sw	$8,28($sp)
lw	$8,28($sp)
.set	noreorder
.set	nomacro
b	$L643
addu	$5,$5,$8
.set	macro
.set	reorder

$L746:
lw	$2,10312($16)
lw	$3,10316($16)
sw	$2,40($sp)
sw	$3,28($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$5,40($sp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L646
lw	$7,24($sp)
.set	macro
.set	reorder

addiu	$8,$5,-1
andi	$8,$8,0x7
sll	$8,$8,4
ori	$8,$8,0xd
$L647:
lw	$7,28($sp)
slt	$3,$7,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L747
lw	$5,28($sp)
.set	macro
.set	reorder

addiu	$3,$5,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L654:
#APP
# 90 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
sw	$7,32($sp)
lw	$7,40($sp)
slt	$5,$7,9
beq	$5,$0,$L748
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
$L656:
subu	$7,$0,$5
lw	$8,32($sp)
#APP
# 1697 "vc1dec.c" 1
movn	$5,$7,$8	#i_movn
# 0 "" 2
#NO_APP
lw	$7,40($sp)
lw	$8,28($sp)
sw	$7,10312($16)
.set	noreorder
.set	nomacro
b	$L637
sw	$8,10316($16)
.set	macro
.set	reorder

$L592:
.set	noreorder
.set	nomacro
beq	$9,$0,$L679
move	$8,$0
.set	macro
.set	reorder

li	$3,45			# 0x2d
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$7,$0,$L595
sw	$7,40($sp)
.set	macro
.set	reorder

addiu	$9,$7,-1
andi	$9,$9,0x7
sll	$9,$9,4
ori	$9,$9,0xd
$L596:
li	$8,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L593
sw	$3,28($sp)
.set	macro
.set	reorder

$L732:
lw	$2,10312($16)
lw	$3,10316($16)
sw	$2,40($sp)
sw	$3,28($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$5,40($sp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L619
lw	$7,24($sp)
.set	macro
.set	reorder

addiu	$8,$5,-1
andi	$8,$8,0x7
sll	$8,$8,4
ori	$8,$8,0xd
$L620:
lw	$7,28($sp)
slt	$3,$7,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L749
lw	$5,28($sp)
.set	macro
.set	reorder

addiu	$3,$5,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L627:
#APP
# 90 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
sw	$7,32($sp)
lw	$7,40($sp)
slt	$5,$7,9
beq	$5,$0,$L750
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
$L629:
subu	$7,$0,$5
lw	$8,32($sp)
#APP
# 1697 "vc1dec.c" 1
movn	$5,$7,$8	#i_movn
# 0 "" 2
#NO_APP
lw	$7,40($sp)
lw	$8,28($sp)
sw	$7,10312($16)
.set	noreorder
.set	nomacro
b	$L610
sw	$8,10316($16)
.set	macro
.set	reorder

$L566:
.set	noreorder
.set	nomacro
beq	$8,$0,$L675
move	$9,$0
.set	macro
.set	reorder

li	$9,45			# 0x2d
#APP
# 90 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
beq	$9,$0,$L569
addiu	$8,$9,-1
andi	$8,$8,0x7
sll	$8,$8,4
ori	$8,$8,0xd
$L570:
#APP
# 90 "vlc_bs.c" 1
mtc0	$fp,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
addiu	$10,$10,3
.set	noreorder
.set	nomacro
b	$L567
sw	$10,20($sp)
.set	macro
.set	reorder

$L679:
li	$3,13			# 0xd
$L764:
#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$22,$L751
addiu	$9,$8,1
.set	macro
.set	reorder

addiu	$8,$8,1
li	$9,6			# 0x6
.set	noreorder
.set	nomacro
bne	$8,$9,$L764
li	$3,13			# 0xd
.set	macro
.set	reorder

li	$7,8			# 0x8
li	$9,125			# 0x7d
.set	noreorder
.set	nomacro
b	$L596
sw	$7,40($sp)
.set	macro
.set	reorder

$L646:
.set	noreorder
.set	nomacro
beq	$7,$0,$L688
move	$7,$0
.set	macro
.set	reorder

li	$8,45			# 0x2d
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$3,$0,$L752
sw	$3,40($sp)
.set	macro
.set	reorder

li	$5,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$8,$7,7
addiu	$7,$7,8
andi	$8,$8,0x7
sll	$8,$8,4
sw	$7,40($sp)
ori	$8,$8,0xd
$L650:
li	$5,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L647
sw	$3,28($sp)
.set	macro
.set	reorder

$L619:
.set	noreorder
.set	nomacro
beq	$7,$0,$L683
move	$7,$0
.set	macro
.set	reorder

li	$8,45			# 0x2d
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$3,$0,$L753
sw	$3,40($sp)
.set	macro
.set	reorder

li	$5,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$8,$7,7
addiu	$7,$7,8
andi	$8,$8,0x7
sll	$8,$8,4
sw	$7,40($sp)
ori	$8,$8,0xd
$L623:
li	$5,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L620
sw	$3,28($sp)
.set	macro
.set	reorder

$L675:
$L568:
#APP
# 102 "vlc_bs.c" 1
mtc0	$16,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$21,$L754
addiu	$8,$9,1
.set	macro
.set	reorder

addiu	$9,$9,1
.set	noreorder
.set	nomacro
bne	$9,$23,$L568
li	$8,125			# 0x7d
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L570
li	$9,8			# 0x8
.set	macro
.set	reorder

$L688:
$L648:
#APP
# 102 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$18,$L755
addiu	$8,$7,1
.set	macro
.set	reorder

addiu	$7,$7,1
li	$8,6			# 0x6
.set	noreorder
.set	nomacro
bne	$7,$8,$L648
li	$3,8			# 0x8
.set	macro
.set	reorder

li	$8,125			# 0x7d
.set	noreorder
.set	nomacro
b	$L650
sw	$3,40($sp)
.set	macro
.set	reorder

$L683:
$L621:
#APP
# 102 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$18,$L756
addiu	$8,$7,1
.set	macro
.set	reorder

addiu	$7,$7,1
li	$8,6			# 0x6
.set	noreorder
.set	nomacro
bne	$7,$8,$L621
li	$3,8			# 0x8
.set	macro
.set	reorder

li	$8,125			# 0x7d
.set	noreorder
.set	nomacro
b	$L623
sw	$3,40($sp)
.set	macro
.set	reorder

$L595:
li	$8,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
addiu	$9,$8,7
addiu	$8,$8,8
andi	$9,$9,0x7
sll	$9,$9,4
sw	$8,40($sp)
.set	noreorder
.set	nomacro
b	$L596
ori	$9,$9,0xd
.set	macro
.set	reorder

$L569:
#APP
# 90 "vlc_bs.c" 1
mtc0	$fp,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
addiu	$8,$9,7
addiu	$9,$9,8
andi	$8,$8,0x7
sll	$8,$8,4
.set	noreorder
.set	nomacro
b	$L570
ori	$8,$8,0xd
.set	macro
.set	reorder

$L740:
#APP
# 76 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$7,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$7,$7,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,$7
#APP
# 84 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L600
or	$3,$3,$7
.set	macro
.set	reorder

$L741:
li	$7,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
addiu	$9,$8,-8
sll	$9,$7,$9
#APP
# 84 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L602
or	$7,$9,$7
.set	macro
.set	reorder

$L742:
#APP
# 76 "vlc_bs.c" 1
mtc0	$22,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$22,$10,-1
andi	$22,$22,0x7
sll	$22,$22,4
ori	$22,$22,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$22,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$22,$21,1
# 0 "" 2
#NO_APP
addiu	$22,$10,-8
sll	$3,$3,$22
#APP
# 84 "vlc_bs.c" 1
mfc0	$22,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L574
or	$3,$3,$22
.set	macro
.set	reorder

$L743:
#APP
# 76 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$22,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
addiu	$22,$9,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
sll	$22,$8,$22
#APP
# 84 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L576
or	$8,$22,$8
.set	macro
.set	reorder

$L753:
addiu	$8,$3,-1
andi	$8,$8,0x7
sll	$8,$8,4
.set	noreorder
.set	nomacro
b	$L623
ori	$8,$8,0xd
.set	macro
.set	reorder

$L752:
addiu	$8,$3,-1
andi	$8,$8,0x7
sll	$8,$8,4
.set	noreorder
.set	nomacro
b	$L650
ori	$8,$8,0xd
.set	macro
.set	reorder

$L747:
li	$3,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$7,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$7,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,$5
#APP
# 84 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L654
or	$3,$3,$5
.set	macro
.set	reorder

$L749:
li	$3,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$7,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$7,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,$5
#APP
# 84 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L627
or	$3,$3,$5
.set	macro
.set	reorder

$L751:
addiu	$8,$8,2
sll	$9,$9,4
sw	$8,40($sp)
.set	noreorder
.set	nomacro
b	$L596
ori	$9,$9,0xd
.set	macro
.set	reorder

$L754:
addiu	$9,$9,2
sll	$8,$8,4
.set	noreorder
.set	nomacro
b	$L570
ori	$8,$8,0xd
.set	macro
.set	reorder

$L750:
li	$5,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
addiu	$8,$7,-8
sll	$8,$5,$8
#APP
# 84 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L629
or	$5,$8,$5
.set	macro
.set	reorder

$L748:
li	$5,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
addiu	$8,$7,-8
sll	$8,$5,$8
#APP
# 84 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L656
or	$5,$8,$5
.set	macro
.set	reorder

$L756:
addiu	$7,$7,2
sll	$8,$8,4
sw	$7,40($sp)
.set	noreorder
.set	nomacro
b	$L623
ori	$8,$8,0xd
.set	macro
.set	reorder

$L755:
addiu	$7,$7,2
sll	$8,$8,4
sw	$7,40($sp)
.set	noreorder
.set	nomacro
b	$L650
ori	$8,$8,0xd
.set	macro
.set	reorder

$L550:
andi	$3,$11,0x1
sll	$2,$3,1
li	$7,6			# 0x6
addu	$2,$2,$3
andi	$3,$11,0x2
mul	$8,$3,$7
addu	$2,$8,$2
nor	$2,$0,$2
.set	noreorder
.set	nomacro
b	$L662
andi	$25,$2,0xf
.set	macro
.set	reorder

$L553:
sll	$2,$11,2
addu	$2,$2,$11
nor	$2,$0,$2
.set	noreorder
.set	nomacro
b	$L663
andi	$25,$2,0xf
.set	macro
.set	reorder

.end	vc1_decode_p_block_vlc.isra.7
.size	vc1_decode_p_block_vlc.isra.7, .-vc1_decode_p_block_vlc.isra.7
.section	.text.vc1_decode_b_mb_vlc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_decode_b_mb_vlc
.type	vc1_decode_b_mb_vlc, @function
vc1_decode_b_mb_vlc:
.frame	$sp,176,$31		# vars= 104, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$6,7996($4)
addiu	$sp,$sp,-176
lw	$5,168($4)
lw	$8,11252($4)
lw	$3,7992($4)
mul	$7,$6,$5
lw	$2,11332($4)
sw	$19,148($sp)
.cprestore	24
sw	$31,172($sp)
sw	$fp,168($sp)
sw	$23,164($sp)
sw	$22,160($sp)
sw	$21,156($sp)
addu	$6,$7,$3
sw	$20,152($sp)
sw	$18,144($sp)
sw	$17,140($sp)
sw	$16,136($sp)
sw	$8,48($sp)
sw	$0,8004($4)
lw	$19,%got(dma_len)($28)
lbu	$16,11228($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L766
sw	$0,0($19)
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
andi	$2,$2,0x00ff
$L767:
lw	$3,11336($4)
.set	noreorder
.set	nomacro
beq	$3,$0,$L768
li	$3,13			# 0xd
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
andi	$8,$8,0x00ff
$L769:
addiu	$fp,$4,8012
lw	$22,%got(dMB)($28)
move	$12,$0
sw	$0,36($sp)
li	$14,6			# 0x6
sw	$0,32($sp)
sw	$0,44($sp)
move	$11,$fp
sw	$0,40($sp)
$L770:
lw	$5,0($11)
addiu	$11,$11,4
lw	$3,11284($4)
addu	$3,$3,$5
sb	$0,0($3)
lw	$5,-4($11)
lw	$7,2728($4)
lw	$3,0($22)
sll	$5,$5,1
addu	$3,$3,$12
addu	$5,$7,$5
addiu	$12,$12,1
sh	$0,0($5)
sb	$0,6($3)
.set	noreorder
.set	nomacro
bne	$12,$14,$L770
move	$7,$0
.set	macro
.set	reorder

lw	$5,2172($4)
addu	$5,$5,$6
sb	$0,0($5)
lw	$5,%got(dMB)($28)
lw	$5,0($5)
addiu	$5,$5,56
#APP
# 3461 "vc1dec.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
li	$7,24			# 0x18
$L771:
#APP
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
# 3461 "vc1dec.c" 1
.word	0b01110000101000000000010001010101	#S32SDI XR1,$5,4
# 0 "" 2
#NO_APP
addiu	$7,$7,-1
bne	$7,$0,$L771
lw	$5,0($22)
sb	$2,33($5)
sb	$8,32($5)
sb	$0,37($5)
.set	noreorder
.set	nomacro
beq	$2,$0,$L772
move	$5,$0
.set	macro
.set	reorder

lw	$13,8004($4)
move	$15,$0
$L773:
addiu	$14,$4,8036
.set	noreorder
.set	nomacro
b	$L798
move	$12,$fp
.set	macro
.set	reorder

$L1273:
lw	$13,8004($4)
$L798:
lw	$7,0($12)
addiu	$12,$12,4
lw	$3,11284($4)
addu	$3,$3,$7
.set	noreorder
.set	nomacro
bne	$12,$14,$L1273
sb	$13,0($3)
.set	macro
.set	reorder

beq	$8,$0,$L799
.set	noreorder
.set	nomacro
bne	$2,$0,$L1116
li	$3,2			# 0x2
.set	macro
.set	reorder

andi	$3,$5,0x00ff
$L800:
lw	$13,10044($4)
li	$11,1			# 0x1
lw	$9,40($sp)
lw	$12,32($sp)
subu	$6,$11,$13
lw	$10,44($sp)
lw	$11,36($sp)
lw	$8,11220($4)
sll	$9,$9,$6
lw	$14,172($4)
sll	$12,$12,$6
sll	$10,$10,$6
lw	$7,8004($4)
sll	$11,$11,$6
lw	$25,11224($4)
lw	$16,8012($4)
sw	$8,48($sp)
sw	$14,60($sp)
sw	$9,40($sp)
sw	$12,32($sp)
sw	$10,44($sp)
.set	noreorder
.set	nomacro
bne	$7,$0,$L1274
sw	$11,36($sp)
.set	macro
.set	reorder

lw	$7,988($4)
sll	$15,$16,2
lh	$8,11298($4)
addu	$7,$7,$15
.set	noreorder
.set	nomacro
beq	$13,$0,$L1275
lh	$6,0($7)
.set	macro
.set	reorder

lh	$13,2($7)
mul	$14,$6,$8
lw	$18,160($4)
sll	$7,$6,8
lw	$21,7992($4)
mul	$8,$8,$13
sll	$17,$18,6
sll	$6,$13,8
addiu	$17,$17,-4
subu	$7,$14,$7
sll	$19,$21,6
addiu	$14,$14,128
sw	$17,68($sp)
li	$13,-60			# 0xffffffffffffffc4
lw	$20,68($sp)
subu	$6,$8,$6
sra	$14,$14,8
subu	$13,$13,$19
addiu	$7,$7,128
addiu	$8,$8,128
addiu	$6,$6,128
slt	$17,$14,$13
sra	$8,$8,8
sra	$7,$7,8
sra	$6,$6,8
.set	noreorder
.set	nomacro
bne	$17,$0,$L1117
subu	$23,$20,$19
.set	macro
.set	reorder

$L1294:
slt	$17,$23,$14
movn	$14,$23,$17
move	$17,$14
$L805:
lw	$24,7996($4)
li	$14,-60			# 0xffffffffffffffc4
lw	$fp,164($4)
sw	$17,7268($4)
sll	$20,$24,6
sw	$24,52($sp)
sll	$24,$fp,6
subu	$14,$14,$20
sw	$fp,56($sp)
addiu	$24,$24,-4
sw	$24,64($sp)
slt	$24,$8,$14
lw	$fp,64($sp)
sw	$24,72($sp)
subu	$24,$fp,$20
lw	$fp,72($sp)
.set	noreorder
.set	nomacro
bne	$fp,$0,$L1118
slt	$fp,$24,$8
.set	macro
.set	reorder

movn	$8,$24,$fp
$L806:
slt	$fp,$7,$13
.set	noreorder
.set	nomacro
bne	$fp,$0,$L1119
sw	$8,7272($4)
.set	macro
.set	reorder

slt	$fp,$23,$7
movn	$7,$23,$fp
$L807:
slt	$fp,$6,$14
.set	noreorder
.set	nomacro
bne	$fp,$0,$L1120
sw	$7,7300($4)
.set	macro
.set	reorder

slt	$fp,$24,$6
movn	$6,$24,$fp
.set	noreorder
.set	nomacro
bne	$2,$0,$L1276
sw	$6,7304($4)
.set	macro
.set	reorder

$L809:
addiu	$2,$5,-1
sltu	$2,$2,2
bne	$2,$0,$L1277
.set	noreorder
.set	nomacro
bne	$5,$0,$L826
lw	$12,48($sp)
.set	macro
.set	reorder

addiu	$7,$15,-8
lw	$6,2184($4)
addiu	$9,$18,-1
sll	$fp,$25,1
sll	$12,$12,1
sw	$7,72($sp)
sw	$9,88($sp)
sw	$6,80($sp)
sw	$12,76($sp)
$L824:
lw	$2,88($sp)
li	$5,-2			# 0xfffffffffffffffe
lw	$9,2188($4)
lw	$12,72($sp)
xor	$6,$21,$2
li	$2,2			# 0x2
addu	$7,$9,$12
movn	$5,$2,$6
.set	noreorder
.set	nomacro
bne	$21,$0,$L829
lw	$2,10296($4)
.set	macro
.set	reorder

sh	$0,2($7)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1061
sh	$0,0($7)
.set	macro
.set	reorder

lw	$2,11168($4)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1278
lw	$12,56($sp)
.set	macro
.set	reorder

sll	$7,$18,5
lw	$13,52($sp)
move	$5,$0
addiu	$7,$7,-4
sll	$6,$12,5
move	$2,$0
sll	$12,$13,5
addiu	$6,$6,-4
.set	noreorder
.set	nomacro
b	$L834
move	$13,$0
.set	macro
.set	reorder

$L772:
.set	noreorder
.set	nomacro
beq	$8,$0,$L1279
move	$15,$0
.set	macro
.set	reorder

li	$5,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
beq	$7,$0,$L793
$L1293:
#APP
# 102 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$7,$0,$L795
addiu	$5,$7,1
.set	macro
.set	reorder

li	$7,2			# 0x2
beq	$5,$7,$L796
beq	$5,$0,$L793
.set	noreorder
.set	nomacro
b	$L773
lw	$13,8004($4)
.set	macro
.set	reorder

$L799:
.set	noreorder
.set	nomacro
beq	$2,$0,$L842
move	$23,$4
.set	macro
.set	reorder

lw	$2,11312($4)
lw	$7,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$7,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1280
lh	$17,0($4)
.set	macro
.set	reorder

$L844:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lbu	$2,11240($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L845
li	$4,3			# 0x3
.set	macro
.set	reorder

lbu	$2,11241($23)
.set	noreorder
.set	nomacro
beq	$2,$4,$L1281
li	$7,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$7,$L1282
li	$7,1			# 0x1
.set	macro
.set	reorder

beq	$2,$7,$L1283
bne	$2,$0,$L845
lw	$7,7992($23)
.set	noreorder
.set	nomacro
bne	$7,$0,$L1284
li	$2,8			# 0x8
.set	macro
.set	reorder

lbu	$16,11229($23)
li	$4,4			# 0x4
li	$7,2			# 0x2
$L857:
beq	$7,$0,$L859
lw	$7,10296($23)
bne	$7,$0,$L1088
$L859:
beq	$4,$0,$L860
lw	$4,160($23)
lw	$7,7992($23)
addiu	$4,$4,-1
beq	$7,$4,$L1104
$L860:
beq	$2,$0,$L845
$L1103:
lw	$2,164($23)
lw	$4,7996($23)
addiu	$2,$2,-1
beq	$4,$2,$L1254
$L845:
lw	$2,2172($23)
$L1351:
sw	$0,8004($23)
addu	$6,$2,$6
sb	$16,0($6)
lbu	$2,11256($23)
bne	$2,$0,$L861
lw	$2,11316($23)
sll	$4,$2,4
lw	$2,%got(ff_vc1_ttmb_vlc)($28)
addu	$2,$2,$4
lw	$6,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$6,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L862
lh	$3,0($4)
.set	macro
.set	reorder

sw	$3,48($sp)
$L863:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
$L861:
lw	$2,8004($23)
lw	$7,10044($23)
lw	$12,8012($23)
sw	$0,36($sp)
sw	$0,44($sp)
sw	$0,32($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1285
sw	$0,40($sp)
.set	macro
.set	reorder

lw	$4,988($23)
sll	$12,$12,2
lh	$6,11298($23)
addu	$4,$4,$12
.set	noreorder
.set	nomacro
bne	$7,$0,$L866
lh	$2,0($4)
.set	macro
.set	reorder

lh	$7,2($4)
mul	$11,$2,$6
sll	$4,$2,8
mul	$6,$6,$7
sll	$2,$7,8
subu	$4,$11,$4
addiu	$11,$11,255
addiu	$4,$4,255
subu	$2,$6,$2
addiu	$6,$6,255
addiu	$2,$2,255
sra	$11,$11,9
sra	$7,$6,9
sra	$4,$4,9
sra	$2,$2,9
sll	$11,$11,1
sll	$7,$7,1
sll	$4,$4,1
sll	$2,$2,1
$L867:
lw	$14,7992($23)
li	$13,-60			# 0xffffffffffffffc4
lw	$6,160($23)
sll	$14,$14,6
sll	$6,$6,6
subu	$13,$13,$14
addiu	$6,$6,-4
slt	$15,$11,$13
.set	noreorder
.set	nomacro
bne	$15,$0,$L1123
subu	$14,$6,$14
.set	macro
.set	reorder

slt	$15,$14,$11
movn	$11,$14,$15
move	$15,$11
$L868:
lw	$18,7996($23)
li	$6,-60			# 0xffffffffffffffc4
lw	$11,164($23)
sw	$15,7268($23)
sll	$18,$18,6
sll	$11,$11,6
subu	$6,$6,$18
addiu	$11,$11,-4
slt	$20,$7,$6
.set	noreorder
.set	nomacro
bne	$20,$0,$L1124
subu	$11,$11,$18
.set	macro
.set	reorder

slt	$18,$11,$7
movn	$7,$11,$18
$L869:
slt	$18,$4,$13
.set	noreorder
.set	nomacro
bne	$18,$0,$L870
sw	$7,7272($23)
.set	macro
.set	reorder

slt	$13,$14,$4
movz	$14,$4,$13
move	$13,$14
$L870:
slt	$4,$2,$6
.set	noreorder
.set	nomacro
bne	$4,$0,$L871
sw	$13,7300($23)
.set	macro
.set	reorder

slt	$6,$11,$2
movz	$11,$2,$6
move	$6,$11
$L871:
lw	$4,2184($23)
lw	$2,2188($23)
sw	$6,7304($23)
addu	$4,$4,$12
addu	$12,$2,$12
sh	$15,0($4)
sh	$7,2($4)
sh	$13,0($12)
sh	$6,2($12)
$L865:
lw	$4,0($22)
andi	$5,$5,0x00ff
sb	$5,36($4)
$L872:
lw	$5,8004($23)
$L1347:
lw	$2,16($4)
.set	noreorder
.set	nomacro
beq	$5,$0,$L1349
or	$5,$2,$5
.set	macro
.set	reorder

li	$5,458752			# 0x70000
ori	$5,$5,0xe000
or	$5,$2,$5
$L1349:
lw	$21,%got(vc1_decode_intra_block)($28)
andi	$2,$16,0x3f
sw	$5,16($4)
sll	$2,$2,12
lw	$5,20($4)
li	$8,1			# 0x1
move	$20,$0
li	$18,5			# 0x5
or	$2,$2,$5
sw	$8,52($sp)
sw	$2,20($4)
move	$2,$fp
move	$fp,$20
.set	noreorder
.set	nomacro
b	$L1057
move	$20,$2
.set	macro
.set	reorder

$L1286:
addiu	$2,$fp,-2
sw	$0,11276($23)
sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1050
sw	$0,11272($23)
.set	macro
.set	reorder

lw	$2,10296($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1354
li	$9,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

$L1050:
lw	$2,0($20)
lw	$6,24($20)
lw	$4,11284($23)
subu	$2,$2,$6
addu	$2,$4,$2
lbu	$2,0($2)
sw	$2,11272($23)
li	$9,-3			# 0xfffffffffffffffd
$L1354:
li	$2,1			# 0x1
and	$4,$fp,$9
beq	$4,$2,$L1052
lw	$2,7992($23)
beq	$2,$0,$L1053
$L1052:
lw	$4,0($20)
lw	$2,11284($23)
addu	$2,$2,$4
lbu	$2,-1($2)
sw	$2,11276($23)
$L1053:
lw	$2,0($19)
andi	$10,$fp,0x4
lw	$9,11260($23)
addiu	$25,$21,%lo(vc1_decode_intra_block)
lw	$11,11264($23)
move	$4,$23
sll	$2,$2,4
sw	$16,16($sp)
move	$6,$fp
movn	$9,$11,$10
addu	$5,$5,$2
addiu	$5,$5,60
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_intra_block
1:	jalr	$25
sw	$9,20($sp)
.set	macro
.set	reorder

sll	$4,$fp,2
lw	$2,0($22)
li	$6,1			# 0x1
sll	$9,$fp,1
lw	$28,24($sp)
sll	$6,$6,$4
lw	$7,0($19)
addu	$4,$2,$fp
lbu	$5,6($4)
li	$4,3			# 0x3
sll	$4,$4,$9
lw	$9,12($2)
andi	$5,$5,0x00ff
addu	$5,$5,$7
or	$6,$6,$9
sw	$6,12($2)
lw	$6,16($2)
sw	$5,0($19)
or	$4,$4,$6
sw	$4,16($2)
$L1055:
addiu	$fp,$fp,1
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
beq	$fp,$2,$L765
addiu	$20,$20,4
.set	macro
.set	reorder

$L1057:
lw	$4,0($20)
subu	$7,$18,$fp
lw	$5,2728($23)
lw	$2,11284($23)
sra	$7,$17,$7
lw	$6,8004($23)
move	$9,$4
sll	$4,$4,1
addu	$2,$2,$9
addu	$4,$5,$4
andi	$7,$7,0x1
sh	$0,0($4)
sb	$6,0($2)
lw	$5,0($22)
lbu	$4,8004($23)
addu	$2,$5,$fp
sb	$4,26($2)
lw	$4,8004($23)
bne	$4,$0,$L1286
.set	noreorder
.set	nomacro
bne	$7,$0,$L1287
lw	$11,%got(vc1_decode_p_block_vlc.isra.7)($28)
.set	macro
.set	reorder

sb	$0,6($2)
addiu	$fp,$fp,1
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
bne	$fp,$2,$L1057
addiu	$20,$20,4
.set	macro
.set	reorder

$L765:
lw	$31,172($sp)
$L1346:
lw	$fp,168($sp)
lw	$23,164($sp)
lw	$22,160($sp)
lw	$21,156($sp)
lw	$20,152($sp)
lw	$19,148($sp)
lw	$18,144($sp)
lw	$17,140($sp)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,176
.set	macro
.set	reorder

$L768:
lw	$3,2836($4)
addu	$3,$3,$6
.set	noreorder
.set	nomacro
b	$L769
lbu	$8,0($3)
.set	macro
.set	reorder

$L766:
lw	$2,11324($4)
addu	$2,$2,$6
.set	noreorder
.set	nomacro
b	$L767
lbu	$2,0($2)
.set	macro
.set	reorder

$L842:
lw	$2,0($22)
andi	$4,$15,0x00ff
sb	$4,34($2)
lbu	$4,8004($23)
sb	$4,39($2)
.set	noreorder
.set	nomacro
beq	$15,$0,$L1288
li	$4,2			# 0x2
.set	macro
.set	reorder

bne	$5,$4,$L985
lw	$4,10272($23)
sll	$7,$4,4
lw	$4,%got(ff_vc1_mv_diff_vlc)($28)
addu	$4,$4,$7
li	$7,127			# 0x7f
lw	$12,4($4)
#APP
# 137 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
sll	$7,$7,2
addu	$7,$12,$7
lh	$4,2($7)
.set	noreorder
.set	nomacro
bltz	$4,$L1289
lh	$7,0($7)
.set	macro
.set	reorder

$L968:
addiu	$4,$4,-1
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addiu	$17,$7,1
slt	$4,$17,37
.set	noreorder
.set	nomacro
beq	$4,$0,$L1290
li	$14,1			# 0x1
.set	macro
.set	reorder

move	$14,$0
move	$12,$0
$L969:
.set	noreorder
.set	nomacro
bne	$17,$0,$L970
sw	$0,8004($23)
.set	macro
.set	reorder

sw	$0,32($sp)
sw	$0,40($sp)
$L971:
sb	$14,35($2)
.set	noreorder
.set	nomacro
bne	$12,$0,$L985
lw	$25,%got(vc1_pred_b_mv)($28)
.set	macro
.set	reorder

li	$16,2			# 0x2
addiu	$5,$sp,40
addiu	$6,$sp,32
move	$7,$0
sw	$16,16($sp)
addiu	$25,$25,%lo(vc1_pred_b_mv)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_pred_b_mv
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$2,0($22)
sb	$16,36($2)
.set	noreorder
.set	nomacro
b	$L1346
lw	$31,172($sp)
.set	macro
.set	reorder

$L1116:
.set	noreorder
.set	nomacro
b	$L800
li	$5,2			# 0x2
.set	macro
.set	reorder

$L1279:
lw	$5,10272($4)
sll	$7,$5,4
lw	$5,%got(ff_vc1_mv_diff_vlc)($28)
addu	$5,$5,$7
lw	$12,4($5)
li	$5,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
sll	$7,$7,2
addu	$7,$12,$7
lh	$5,2($7)
.set	noreorder
.set	nomacro
bltz	$5,$L1291
lh	$11,0($7)
.set	macro
.set	reorder

$L776:
addiu	$5,$5,-1
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addiu	$7,$11,1
slt	$5,$7,37
beq	$5,$0,$L1292
move	$15,$0
.set	noreorder
.set	nomacro
bne	$7,$0,$L778
sw	$0,8004($4)
.set	macro
.set	reorder

$L1295:
move	$5,$0
sw	$0,32($sp)
sw	$0,40($sp)
sw	$7,36($sp)
sw	$5,44($sp)
$L1302:
li	$5,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
bne	$7,$0,$L1293
$L793:
lh	$5,11298($4)
lw	$13,8004($4)
.set	noreorder
.set	nomacro
b	$L773
slt	$5,$5,128
.set	macro
.set	reorder

$L1275:
lh	$13,2($7)
mul	$14,$6,$8
lw	$18,160($4)
sll	$7,$6,8
lw	$21,7992($4)
mul	$8,$8,$13
sll	$17,$18,6
sll	$6,$13,8
addiu	$17,$17,-4
subu	$7,$14,$7
addiu	$14,$14,255
sll	$19,$21,6
sw	$17,68($sp)
subu	$6,$8,$6
lw	$20,68($sp)
sra	$14,$14,9
li	$13,-60			# 0xffffffffffffffc4
addiu	$7,$7,255
addiu	$8,$8,255
addiu	$6,$6,255
sll	$14,$14,1
subu	$13,$13,$19
sra	$8,$8,9
sra	$7,$7,9
sra	$6,$6,9
slt	$17,$14,$13
sll	$8,$8,1
sll	$7,$7,1
sll	$6,$6,1
.set	noreorder
.set	nomacro
beq	$17,$0,$L1294
subu	$23,$20,$19
.set	macro
.set	reorder

$L1117:
.set	noreorder
.set	nomacro
b	$L805
move	$17,$13
.set	macro
.set	reorder

$L1292:
addiu	$7,$11,-36
sw	$0,8004($4)
.set	noreorder
.set	nomacro
beq	$7,$0,$L1295
li	$15,1			# 0x1
.set	macro
.set	reorder

$L778:
li	$5,35			# 0x23
.set	noreorder
.set	nomacro
beq	$7,$5,$L1296
li	$5,36			# 0x24
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$5,$L1297
sra	$17,$7,31
.set	macro
.set	reorder

li	$5,715784192			# 0x2aaa0000
lw	$14,10044($4)
ori	$5,$5,0xaaab
mult	$7,$5
mfhi	$5
subu	$5,$5,$17
sll	$11,$5,3
sll	$12,$5,1
subu	$12,$11,$12
lw	$11,%got(size_table)($28)
subu	$12,$7,$12
.set	noreorder
.set	nomacro
beq	$14,$0,$L1298
sll	$18,$12,2
.set	macro
.set	reorder

addiu	$11,$11,%lo(size_table)
addu	$12,$11,$18
lw	$12,0($12)
.set	noreorder
.set	nomacro
blez	$12,$L1299
lw	$13,%got(offset_table)($28)
.set	macro
.set	reorder

$L1084:
slt	$5,$12,9
.set	noreorder
.set	nomacro
beq	$5,$0,$L1300
addiu	$5,$12,-1
.set	macro
.set	reorder

sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$20,$21,1
# 0 "" 2
#NO_APP
andi	$21,$20,0x1
subu	$3,$0,$21
sra	$20,$20,1
$L789:
lw	$13,%got(offset_table)($28)
li	$12,715784192			# 0x2aaa0000
ori	$12,$12,0xaaab
addiu	$13,$13,%lo(offset_table)
mult	$7,$12
addu	$18,$13,$18
lw	$7,0($18)
mfhi	$12
addu	$7,$20,$7
xor	$7,$7,$3
addu	$5,$7,$21
subu	$12,$12,$17
.set	noreorder
.set	nomacro
beq	$14,$0,$L1083
sw	$5,40($sp)
.set	macro
.set	reorder

move	$14,$0
$L790:
sll	$12,$12,2
addu	$11,$11,$12
lw	$7,0($11)
subu	$11,$7,$14
.set	noreorder
.set	nomacro
blez	$11,$L1115
move	$7,$0
.set	macro
.set	reorder

slt	$7,$11,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1301
li	$7,125			# 0x7d
.set	macro
.set	reorder

addiu	$7,$11,-1
sll	$7,$7,4
ori	$7,$7,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
#NO_APP
andi	$14,$11,0x1
subu	$7,$0,$14
sra	$11,$11,1
$L791:
addu	$12,$13,$12
lw	$12,0($12)
addu	$11,$11,$12
xor	$7,$11,$7
addu	$7,$7,$14
sw	$7,32($sp)
$L1331:
sw	$5,44($sp)
.set	noreorder
.set	nomacro
b	$L1302
sw	$7,36($sp)
.set	macro
.set	reorder

$L1288:
lw	$4,8004($23)
beq	$4,$0,$L1303
lbu	$2,11240($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L927
li	$2,3			# 0x3
.set	macro
.set	reorder

lbu	$4,11241($23)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1304
li	$7,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$7,$L1305
li	$7,1			# 0x1
.set	macro
.set	reorder

beq	$4,$7,$L1306
bne	$4,$0,$L927
lw	$7,7992($23)
.set	noreorder
.set	nomacro
bne	$7,$0,$L1307
li	$2,8			# 0x8
.set	macro
.set	reorder

lbu	$16,11229($23)
li	$4,4			# 0x4
li	$7,2			# 0x2
$L923:
beq	$7,$0,$L925
lw	$7,10296($23)
bne	$7,$0,$L1092
$L925:
beq	$4,$0,$L926
lw	$4,160($23)
lw	$7,7992($23)
addiu	$4,$4,-1
beq	$7,$4,$L1107
$L926:
beq	$2,$0,$L927
$L1106:
lw	$2,164($23)
lw	$4,7996($23)
addiu	$2,$2,-1
beq	$4,$2,$L1255
$L927:
lw	$2,2172($23)
$L1353:
addu	$6,$2,$6
li	$2,13			# 0xd
sb	$16,0($6)
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
lw	$4,10044($23)
li	$2,1			# 0x1
lw	$11,32($sp)
lw	$6,40($sp)
subu	$2,$2,$4
lw	$18,11220($23)
lw	$20,11224($23)
sll	$11,$11,$2
lw	$17,8012($23)
sll	$6,$6,$2
sw	$7,2824($23)
sw	$11,56($sp)
lw	$11,44($sp)
lw	$7,56($sp)
sw	$6,52($sp)
sll	$11,$11,$2
lw	$3,52($sp)
lw	$6,8004($23)
sw	$7,32($sp)
sw	$11,68($sp)
lw	$11,36($sp)
lw	$8,68($sp)
sw	$3,40($sp)
sll	$11,$11,$2
lw	$2,172($23)
sw	$8,44($sp)
sw	$11,64($sp)
sw	$2,76($sp)
.set	noreorder
.set	nomacro
bne	$6,$0,$L1308
sw	$11,36($sp)
.set	macro
.set	reorder

lw	$7,988($23)
sll	$9,$17,2
lh	$11,11298($23)
addu	$7,$7,$9
sw	$9,124($sp)
.set	noreorder
.set	nomacro
bne	$4,$0,$L930
lh	$2,0($7)
.set	macro
.set	reorder

lh	$4,2($7)
mul	$7,$2,$11
sll	$2,$2,8
mul	$6,$11,$4
sll	$4,$4,8
subu	$2,$7,$2
addiu	$7,$7,255
addiu	$2,$2,255
subu	$4,$6,$4
addiu	$11,$6,255
addiu	$4,$4,255
sra	$7,$7,9
sra	$11,$11,9
sra	$2,$2,9
sra	$4,$4,9
sll	$7,$7,1
sll	$11,$11,1
sll	$2,$2,1
sll	$4,$4,1
$L931:
lw	$6,160($23)
li	$14,-60			# 0xffffffffffffffc4
lw	$12,7992($23)
sll	$13,$6,6
sll	$3,$12,6
addiu	$13,$13,-4
subu	$14,$14,$3
sw	$3,60($sp)
sw	$13,88($sp)
slt	$13,$7,$14
lw	$8,88($sp)
.set	noreorder
.set	nomacro
bne	$13,$0,$L1133
subu	$21,$8,$3
.set	macro
.set	reorder

slt	$13,$21,$7
movn	$7,$21,$13
$L932:
lw	$10,164($23)
li	$15,-60			# 0xffffffffffffffc4
lw	$8,7996($23)
sw	$7,7268($23)
sll	$13,$10,6
sll	$9,$8,6
addiu	$13,$13,-4
subu	$15,$15,$9
sw	$9,80($sp)
sw	$13,92($sp)
slt	$13,$11,$15
lw	$24,92($sp)
.set	noreorder
.set	nomacro
bne	$13,$0,$L1134
subu	$3,$24,$9
.set	macro
.set	reorder

slt	$13,$3,$11
movn	$11,$3,$13
$L933:
slt	$13,$2,$14
.set	noreorder
.set	nomacro
bne	$13,$0,$L1135
sw	$11,7272($23)
.set	macro
.set	reorder

slt	$13,$21,$2
movn	$2,$21,$13
$L934:
slt	$13,$4,$15
.set	noreorder
.set	nomacro
bne	$13,$0,$L1136
sw	$2,7300($23)
.set	macro
.set	reorder

slt	$13,$3,$4
movn	$4,$3,$13
$L935:
addiu	$13,$5,-1
sltu	$13,$13,2
.set	noreorder
.set	nomacro
bne	$13,$0,$L1309
sw	$4,7304($23)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$5,$0,$L951
lw	$13,2184($23)
.set	macro
.set	reorder

lw	$24,124($sp)
addiu	$25,$6,-1
sll	$2,$20,1
addiu	$24,$24,-8
sw	$25,108($sp)
sll	$25,$18,1
sw	$2,52($sp)
sw	$24,104($sp)
$L950:
lw	$4,108($sp)
li	$2,2			# 0x2
lw	$9,104($sp)
li	$24,-2			# 0xfffffffffffffffe
lw	$5,2188($23)
xor	$4,$12,$4
sw	$4,56($sp)
addu	$4,$5,$9
lw	$9,56($sp)
movn	$24,$2,$9
lw	$2,10296($23)
.set	noreorder
.set	nomacro
bne	$12,$0,$L955
sw	$24,56($sp)
.set	macro
.set	reorder

sh	$0,2($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1075
sh	$0,0($4)
.set	macro
.set	reorder

lw	$2,11168($23)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1310
move	$4,$0
.set	macro
.set	reorder

sll	$6,$6,5
sll	$14,$10,5
sll	$21,$8,5
addiu	$6,$6,-4
addiu	$14,$14,-4
move	$15,$0
move	$2,$0
$L960:
addu	$17,$21,$4
slt	$3,$17,-28
beq	$3,$0,$L961
li	$4,-28			# 0xffffffffffffffe4
li	$17,-28			# 0xffffffffffffffe4
subu	$4,$4,$21
$L961:
subu	$15,$6,$15
subu	$21,$14,$21
slt	$6,$6,$12
slt	$14,$14,$17
movn	$2,$15,$6
movn	$4,$21,$14
$L963:
lw	$12,68($sp)
addiu	$25,$25,-1
lw	$14,64($sp)
lw	$15,52($sp)
addu	$2,$12,$2
addu	$4,$14,$4
addu	$2,$18,$2
addu	$4,$20,$4
addiu	$3,$15,-1
and	$2,$2,$25
and	$4,$4,$3
subu	$2,$2,$18
subu	$4,$4,$20
sw	$2,7300($23)
sw	$4,7304($23)
$L953:
lw	$17,124($sp)
addu	$13,$13,$17
addu	$5,$5,$17
sh	$7,0($13)
sh	$11,2($13)
sh	$2,0($5)
sh	$4,2($5)
$L929:
lw	$4,0($22)
li	$2,1			# 0x1
move	$17,$0
sb	$2,37($4)
.set	noreorder
.set	nomacro
b	$L1347
lw	$5,8004($23)
.set	macro
.set	reorder

$L1285:
lw	$2,2184($23)
sll	$12,$12,2
lw	$4,2188($23)
addu	$2,$2,$12
addu	$12,$4,$12
sh	$0,2($12)
sh	$0,0($12)
sh	$0,2($2)
.set	noreorder
.set	nomacro
b	$L865
sh	$0,0($2)
.set	macro
.set	reorder

$L1291:
li	$7,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
nor	$5,$0,$5
sll	$5,$5,4
ori	$5,$5,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addu	$11,$5,$11
sll	$5,$11,2
addu	$12,$12,$5
lh	$11,0($12)
.set	noreorder
.set	nomacro
b	$L776
lh	$5,2($12)
.set	macro
.set	reorder

$L1280:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$17,$2,$17
sll	$2,$17,2
addu	$7,$7,$2
lh	$17,0($7)
.set	noreorder
.set	nomacro
b	$L844
lh	$2,2($7)
.set	macro
.set	reorder

$L829:
bne	$2,$0,$L1311
$L1061:
lw	$6,60($sp)
sll	$2,$6,1
subu	$16,$16,$2
sll	$2,$16,2
addu	$6,$9,$2
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$18,$2,$L1312
addu	$16,$16,$5
.set	macro
.set	reorder

lh	$5,0($6)
sll	$2,$16,2
addu	$12,$9,$2
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001010000000001101111	#S32I2M XR1,$5
# 0 "" 2
#NO_APP
lh	$2,0($12)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
lh	$2,0($7)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000000100000000111101110	#S32M2I XR7, $2
# 0 "" 2
#NO_APP
lh	$5,2($6)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001010000000001101111	#S32I2M XR1,$5
# 0 "" 2
#NO_APP
lh	$5,2($12)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001010000000010101111	#S32I2M XR2,$5
# 0 "" 2
#NO_APP
lh	$5,2($7)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001010000000011101111	#S32I2M XR3,$5
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001010000000111101110	#S32M2I XR7, $5
# 0 "" 2
#NO_APP
$L832:
lw	$6,11168($4)
slt	$6,$6,3
.set	noreorder
.set	nomacro
beq	$6,$0,$L833
lw	$12,56($sp)
.set	macro
.set	reorder

sll	$13,$21,5
lw	$16,52($sp)
sll	$7,$18,5
addu	$21,$13,$2
sll	$6,$12,5
slt	$14,$21,-28
sll	$12,$16,5
addiu	$7,$7,-4
.set	noreorder
.set	nomacro
beq	$14,$0,$L834
addiu	$6,$6,-4
.set	macro
.set	reorder

li	$2,-28			# 0xffffffffffffffe4
li	$21,-28			# 0xffffffffffffffe4
subu	$2,$2,$13
$L834:
addu	$14,$12,$5
slt	$16,$14,-28
beq	$16,$0,$L835
li	$5,-28			# 0xffffffffffffffe4
li	$14,-28			# 0xffffffffffffffe4
subu	$5,$5,$12
$L835:
subu	$13,$7,$13
subu	$12,$6,$12
slt	$7,$7,$21
slt	$6,$6,$14
movn	$2,$13,$7
movn	$5,$12,$6
$L837:
addu	$7,$10,$2
lw	$24,48($sp)
lw	$2,76($sp)
addu	$5,$11,$5
addiu	$6,$fp,-1
addu	$7,$24,$7
addiu	$10,$2,-1
addu	$2,$25,$5
and	$7,$7,$10
and	$6,$2,$6
subu	$7,$7,$24
subu	$6,$6,$25
sw	$7,7300($4)
sw	$6,7304($4)
$L827:
lw	$4,80($sp)
addu	$9,$9,$15
addu	$2,$4,$15
sh	$17,0($2)
sh	$8,2($2)
sh	$7,0($9)
sh	$6,2($9)
lw	$2,0($22)
sb	$3,36($2)
lw	$31,172($sp)
$L1348:
lw	$fp,168($sp)
lw	$23,164($sp)
lw	$22,160($sp)
lw	$21,156($sp)
lw	$20,152($sp)
lw	$19,148($sp)
lw	$18,144($sp)
lw	$17,140($sp)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,176
.set	macro
.set	reorder

$L1287:
lw	$2,0($19)
move	$4,$23
move	$6,$fp
lw	$10,52($sp)
sll	$2,$2,4
lw	$7,48($sp)
addiu	$25,$11,%lo(vc1_decode_p_block_vlc.isra.7)
addu	$5,$5,$2
sw	$10,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_p_block_vlc.isra.7
1:	jalr	$25
addiu	$5,$5,60
.set	macro
.set	reorder

lbu	$2,11256($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1149
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$12,48($sp)
li	$13,-1			# 0xffffffffffffffff
sw	$0,52($sp)
slt	$2,$12,8
movz	$13,$12,$2
.set	noreorder
.set	nomacro
b	$L1055
sw	$13,48($sp)
.set	macro
.set	reorder

$L1274:
lw	$2,2184($4)
sll	$16,$16,2
lw	$4,2188($4)
addu	$2,$2,$16
addu	$4,$4,$16
sh	$0,2($4)
sh	$0,0($4)
sh	$0,2($2)
sh	$0,0($2)
lw	$2,0($22)
sb	$3,36($2)
.set	noreorder
.set	nomacro
b	$L1348
lw	$31,172($sp)
.set	macro
.set	reorder

$L1149:
.set	noreorder
.set	nomacro
b	$L1055
sw	$0,52($sp)
.set	macro
.set	reorder

$L1120:
move	$6,$14
.set	noreorder
.set	nomacro
beq	$2,$0,$L809
sw	$6,7304($4)
.set	macro
.set	reorder

$L1276:
lw	$2,2188($4)
lw	$5,2184($4)
addu	$4,$5,$15
addu	$15,$2,$15
lw	$2,0($22)
sh	$17,0($4)
sh	$8,2($4)
sh	$7,0($15)
sh	$6,2($15)
sb	$3,36($2)
.set	noreorder
.set	nomacro
b	$L1348
lw	$31,172($sp)
.set	macro
.set	reorder

$L1119:
.set	noreorder
.set	nomacro
b	$L807
move	$7,$13
.set	macro
.set	reorder

$L1118:
.set	noreorder
.set	nomacro
b	$L806
move	$8,$14
.set	macro
.set	reorder

$L985:
lw	$15,10044($23)
li	$12,1			# 0x1
lw	$11,44($sp)
lw	$4,40($sp)
subu	$2,$12,$15
lw	$7,32($sp)
lw	$3,11220($23)
sll	$9,$11,$2
lw	$11,36($sp)
lw	$10,172($23)
sll	$4,$4,$2
sll	$7,$7,$2
lw	$13,8004($23)
sll	$11,$11,$2
lw	$8,11224($23)
lw	$18,8012($23)
sw	$3,128($sp)
sw	$10,96($sp)
sw	$4,40($sp)
sw	$7,32($sp)
sw	$9,44($sp)
.set	noreorder
.set	nomacro
bne	$13,$0,$L1314
sw	$11,36($sp)
.set	macro
.set	reorder

lw	$12,988($23)
sll	$14,$18,2
lh	$13,11298($23)
addu	$12,$12,$14
lh	$2,0($12)
.set	noreorder
.set	nomacro
beq	$15,$0,$L1315
lh	$12,2($12)
.set	macro
.set	reorder

mul	$15,$2,$13
mul	$13,$13,$12
sll	$2,$2,8
sll	$10,$12,8
subu	$2,$15,$2
subu	$10,$13,$10
addiu	$21,$13,128
addiu	$15,$15,128
addiu	$2,$2,128
addiu	$10,$10,128
sra	$13,$15,8
sra	$24,$21,8
sra	$25,$2,8
sra	$3,$10,8
$L989:
lw	$2,160($23)
li	$12,-60			# 0xffffffffffffffc4
lw	$10,7992($23)
sll	$17,$2,6
sll	$15,$10,6
addiu	$17,$17,-4
subu	$12,$12,$15
sw	$17,76($sp)
slt	$17,$13,$12
lw	$20,76($sp)
subu	$20,$20,$15
.set	noreorder
.set	nomacro
bne	$17,$0,$L1142
sw	$20,64($sp)
.set	macro
.set	reorder

slt	$17,$20,$13
movz	$20,$13,$17
sw	$20,60($sp)
$L990:
lw	$13,164($23)
lw	$21,7996($23)
lw	$17,60($sp)
sll	$20,$13,6
sw	$13,56($sp)
li	$13,-60			# 0xffffffffffffffc4
addiu	$20,$20,-4
sw	$21,52($sp)
sw	$17,7268($23)
sll	$17,$21,6
sw	$20,88($sp)
subu	$13,$13,$17
lw	$21,88($sp)
slt	$20,$24,$13
subu	$21,$21,$17
.set	noreorder
.set	nomacro
bne	$20,$0,$L1143
sw	$21,80($sp)
.set	macro
.set	reorder

lw	$20,80($sp)
slt	$21,$21,$24
movz	$20,$24,$21
move	$21,$20
$L991:
slt	$20,$25,$12
.set	noreorder
.set	nomacro
bne	$20,$0,$L1144
sw	$21,7272($23)
.set	macro
.set	reorder

lw	$24,64($sp)
slt	$20,$24,$25
movz	$24,$25,$20
sw	$24,68($sp)
$L992:
lw	$25,68($sp)
slt	$20,$3,$13
.set	noreorder
.set	nomacro
bne	$20,$0,$L1145
sw	$25,7300($23)
.set	macro
.set	reorder

lw	$24,80($sp)
slt	$20,$24,$3
movz	$24,$3,$20
sw	$24,72($sp)
$L993:
lw	$25,72($sp)
addiu	$20,$5,-1
sltu	$20,$20,2
.set	noreorder
.set	nomacro
bne	$20,$0,$L1316
sw	$25,7304($23)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$5,$0,$L1010
lw	$7,128($sp)
.set	macro
.set	reorder

addiu	$3,$14,-8
lw	$25,2184($23)
addiu	$4,$2,-1
sll	$20,$8,1
sll	$7,$7,1
sw	$3,108($sp)
sw	$4,112($sp)
sw	$25,92($sp)
sw	$7,84($sp)
sw	$20,100($sp)
$L1008:
lw	$4,108($sp)
li	$7,-2			# 0xfffffffffffffffe
lw	$24,2188($23)
lw	$3,112($sp)
addu	$20,$24,$4
li	$4,2			# 0x2
xor	$25,$10,$3
movn	$7,$4,$25
.set	noreorder
.set	nomacro
bne	$10,$0,$L1013
lw	$4,10296($23)
.set	macro
.set	reorder

sh	$0,2($20)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1079
sh	$0,0($20)
.set	macro
.set	reorder

lw	$4,11168($23)
slt	$4,$4,3
.set	noreorder
.set	nomacro
beq	$4,$0,$L1317
move	$7,$0
.set	macro
.set	reorder

lw	$4,56($sp)
sll	$2,$2,5
lw	$7,52($sp)
move	$13,$0
addiu	$2,$2,-4
sll	$12,$4,5
sll	$17,$7,5
addiu	$12,$12,-4
move	$7,$0
move	$4,$0
$L1018:
addu	$15,$17,$7
slt	$18,$15,-28
beq	$18,$0,$L1019
li	$7,-28			# 0xffffffffffffffe4
li	$15,-28			# 0xffffffffffffffe4
subu	$7,$7,$17
$L1019:
subu	$13,$2,$13
subu	$17,$12,$17
slt	$2,$2,$10
slt	$12,$12,$15
movn	$4,$13,$2
movn	$7,$17,$12
$L1021:
addu	$2,$11,$7
lw	$12,84($sp)
lw	$11,128($sp)
addu	$4,$9,$4
lw	$13,100($sp)
addu	$2,$8,$2
addiu	$7,$12,-1
addu	$4,$11,$4
addiu	$9,$13,-1
and	$4,$4,$7
and	$2,$2,$9
subu	$4,$4,$11
subu	$2,$2,$8
sw	$4,68($sp)
sw	$2,72($sp)
sw	$4,7300($23)
sw	$2,7304($23)
$L1011:
lw	$15,92($sp)
lw	$17,60($sp)
lw	$18,68($sp)
lw	$20,72($sp)
addu	$4,$15,$14
addu	$14,$24,$14
lw	$2,0($22)
sh	$17,0($4)
sh	$21,2($4)
sh	$18,0($14)
.set	noreorder
.set	nomacro
b	$L987
sh	$20,2($14)
.set	macro
.set	reorder

$L796:
lw	$13,8004($4)
sw	$0,32($sp)
.set	noreorder
.set	nomacro
b	$L773
sw	$0,40($sp)
.set	macro
.set	reorder

$L795:
lh	$5,11298($4)
lw	$13,8004($4)
slt	$5,$5,128
.set	noreorder
.set	nomacro
b	$L773
xori	$5,$5,0x1
.set	macro
.set	reorder

$L1123:
.set	noreorder
.set	nomacro
b	$L868
move	$15,$13
.set	macro
.set	reorder

$L1124:
.set	noreorder
.set	nomacro
b	$L869
move	$7,$6
.set	macro
.set	reorder

$L866:
lh	$7,2($4)
mul	$11,$2,$6
sll	$4,$2,8
mul	$6,$6,$7
sll	$2,$7,8
subu	$4,$11,$4
addiu	$11,$11,128
addiu	$4,$4,128
subu	$2,$6,$2
addiu	$6,$6,128
addiu	$2,$2,128
sra	$11,$11,8
sra	$7,$6,8
sra	$4,$4,8
.set	noreorder
.set	nomacro
b	$L867
sra	$2,$2,8
.set	macro
.set	reorder

$L1314:
lw	$4,2184($23)
sll	$18,$18,2
lw	$7,2188($23)
lw	$2,0($22)
addu	$4,$4,$18
addu	$7,$7,$18
sh	$0,2($7)
sh	$0,0($7)
sh	$0,2($4)
sh	$0,0($4)
sb	$12,37($2)
$L987:
andi	$5,$5,0x00ff
sb	$5,36($2)
lw	$2,8004($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1025
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sw	$2,2824($23)
$L1025:
lw	$2,11312($23)
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1318
lh	$17,0($4)
.set	macro
.set	reorder

$L1027:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lbu	$2,11240($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1028
li	$2,3			# 0x3
.set	macro
.set	reorder

lbu	$4,11241($23)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1319
li	$5,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$5,$L1320
li	$5,1			# 0x1
.set	macro
.set	reorder

beq	$4,$5,$L1321
bne	$4,$0,$L1028
lw	$5,7992($23)
.set	noreorder
.set	nomacro
bne	$5,$0,$L1322
li	$2,8			# 0x8
.set	macro
.set	reorder

lbu	$16,11229($23)
li	$4,4			# 0x4
li	$5,2			# 0x2
$L1040:
beq	$5,$0,$L1042
lw	$5,10296($23)
bne	$5,$0,$L1101
$L1042:
beq	$4,$0,$L1043
lw	$4,160($23)
lw	$5,7992($23)
addiu	$4,$4,-1
beq	$5,$4,$L1110
$L1043:
beq	$2,$0,$L1028
$L1109:
lw	$2,164($23)
lw	$4,7996($23)
addiu	$2,$2,-1
beq	$4,$2,$L1257
$L1028:
lw	$2,2172($23)
$L1352:
addu	$6,$2,$6
sb	$16,0($6)
lbu	$2,11256($23)
bne	$2,$0,$L1044
lw	$2,8004($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1045
li	$5,458752			# 0x70000
.set	macro
.set	reorder

lw	$2,11316($23)
sll	$4,$2,4
lw	$2,%got(ff_vc1_ttmb_vlc)($28)
addu	$2,$2,$4
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1046
lh	$3,0($4)
.set	macro
.set	reorder

sw	$3,48($sp)
$L1047:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$4,0($22)
move	$5,$0
lw	$2,16($4)
.set	noreorder
.set	nomacro
b	$L1349
or	$5,$2,$5
.set	macro
.set	reorder

$L1044:
.set	noreorder
.set	nomacro
b	$L872
lw	$4,0($22)
.set	macro
.set	reorder

$L1277:
addiu	$17,$15,-8
lw	$2,2184($4)
addiu	$8,$18,-1
sw	$17,72($sp)
xor	$17,$21,$8
sw	$8,88($sp)
sw	$2,80($sp)
lw	$8,72($sp)
addu	$fp,$2,$8
li	$2,2			# 0x2
li	$8,-2			# 0xfffffffffffffffe
movz	$2,$8,$17
sw	$2,76($sp)
.set	noreorder
.set	nomacro
beq	$21,$0,$L1323
lw	$2,10296($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L1355
lw	$17,60($sp)
.set	macro
.set	reorder

lh	$8,0($fp)
lh	$17,2($fp)
sw	$8,108($sp)
$L815:
lw	$2,11168($4)
$L1350:
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1356
lw	$fp,108($sp)
.set	macro
.set	reorder

sll	$2,$18,5
sll	$fp,$21,5
sw	$2,84($sp)
lw	$2,108($sp)
addu	$8,$fp,$2
slt	$2,$8,-28
sw	$2,100($sp)
lw	$2,52($sp)
sll	$2,$2,5
sw	$2,92($sp)
lw	$2,84($sp)
addiu	$2,$2,-4
sw	$2,96($sp)
lw	$2,56($sp)
sll	$2,$2,5
addiu	$2,$2,-4
sw	$2,84($sp)
lw	$2,100($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1357
lw	$2,92($sp)
.set	macro
.set	reorder

li	$2,-28			# 0xffffffffffffffe4
li	$8,-28			# 0xffffffffffffffe4
subu	$2,$2,$fp
sw	$2,108($sp)
$L817:
lw	$2,92($sp)
$L1357:
addu	$2,$2,$17
sw	$2,100($sp)
slt	$2,$2,-28
.set	noreorder
.set	nomacro
beq	$2,$0,$L1358
lw	$2,96($sp)
.set	macro
.set	reorder

li	$17,-28			# 0xffffffffffffffe4
lw	$2,92($sp)
sw	$17,100($sp)
subu	$17,$17,$2
lw	$2,96($sp)
$L1358:
subu	$fp,$2,$fp
lw	$2,92($sp)
sw	$fp,104($sp)
lw	$fp,84($sp)
subu	$fp,$fp,$2
lw	$2,84($sp)
sw	$fp,92($sp)
lw	$fp,96($sp)
slt	$8,$fp,$8
lw	$fp,100($sp)
slt	$2,$2,$fp
lw	$fp,104($sp)
sw	$2,84($sp)
lw	$2,108($sp)
movn	$2,$fp,$8
lw	$8,92($sp)
sw	$2,108($sp)
lw	$2,84($sp)
movn	$17,$8,$2
$L820:
lw	$2,48($sp)
lw	$fp,108($sp)
lw	$8,48($sp)
sll	$2,$2,1
addu	$9,$9,$fp
sll	$fp,$25,1
sw	$2,76($sp)
addu	$2,$12,$17
lw	$12,76($sp)
addu	$9,$8,$9
addu	$2,$25,$2
addiu	$8,$12,-1
addiu	$12,$fp,-1
and	$9,$9,$8
and	$8,$2,$12
lw	$2,48($sp)
subu	$8,$8,$25
subu	$17,$9,$2
li	$2,2			# 0x2
sw	$8,7272($4)
.set	noreorder
.set	nomacro
beq	$5,$2,$L824
sw	$17,7268($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L827
lw	$9,2188($4)
.set	macro
.set	reorder

$L1315:
mul	$15,$2,$13
mul	$13,$13,$12
sll	$10,$12,8
sll	$2,$2,8
subu	$2,$15,$2
subu	$10,$13,$10
addiu	$15,$15,255
addiu	$13,$13,255
addiu	$2,$2,255
addiu	$10,$10,255
sra	$21,$13,9
sra	$12,$15,9
sra	$2,$2,9
sra	$10,$10,9
sll	$13,$12,1
sll	$24,$21,1
sll	$25,$2,1
.set	noreorder
.set	nomacro
b	$L989
sll	$3,$10,1
.set	macro
.set	reorder

$L1290:
addiu	$17,$7,-36
.set	noreorder
.set	nomacro
b	$L969
li	$12,1			# 0x1
.set	macro
.set	reorder

$L1045:
lw	$4,0($22)
ori	$5,$5,0xe000
lw	$2,16($4)
.set	noreorder
.set	nomacro
b	$L1349
or	$5,$2,$5
.set	macro
.set	reorder

$L1298:
addiu	$11,$11,%lo(size_table)
xori	$12,$12,0x5
addu	$13,$11,$18
sltu	$12,$12,1
lw	$13,0($13)
subu	$12,$13,$12
.set	noreorder
.set	nomacro
bgtz	$12,$L1084
lw	$13,%got(offset_table)($28)
.set	macro
.set	reorder

move	$12,$5
addiu	$13,$13,%lo(offset_table)
addu	$18,$13,$18
lw	$5,0($18)
sw	$5,40($sp)
$L1083:
xori	$14,$12,0x5
.set	noreorder
.set	nomacro
b	$L790
sltu	$14,$14,1
.set	macro
.set	reorder

$L1311:
lh	$2,0($7)
.set	noreorder
.set	nomacro
b	$L832
lh	$5,2($7)
.set	macro
.set	reorder

$L1323:
sh	$0,2($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1059
sh	$0,0($fp)
.set	macro
.set	reorder

lw	$2,11168($4)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1324
lw	$fp,56($sp)
.set	macro
.set	reorder

sll	$2,$18,5
lw	$8,52($sp)
addiu	$2,$2,-4
sw	$0,108($sp)
sll	$17,$fp,5
sll	$8,$8,5
sw	$2,96($sp)
addiu	$2,$17,-4
move	$fp,$0
sw	$8,92($sp)
move	$17,$0
sw	$2,84($sp)
.set	noreorder
.set	nomacro
b	$L817
move	$8,$0
.set	macro
.set	reorder

$L1308:
lw	$2,2184($23)
sll	$17,$17,2
lw	$4,2188($23)
addu	$2,$2,$17
addu	$4,$4,$17
sh	$0,2($4)
sh	$0,0($4)
sh	$0,2($2)
.set	noreorder
.set	nomacro
b	$L929
sh	$0,0($2)
.set	macro
.set	reorder

$L1320:
lbu	$4,11242($23)
li	$2,1			# 0x1
sll	$2,$2,$4
$L1035:
andi	$4,$2,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L1039
andi	$4,$2,0x4
.set	macro
.set	reorder

andi	$5,$2,0x2
.set	noreorder
.set	nomacro
b	$L1040
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1154:
li	$2,8			# 0x8
$L1088:
.set	noreorder
.set	nomacro
b	$L859
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1254:
.set	noreorder
.set	nomacro
b	$L845
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1284:
lw	$2,10296($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1154
li	$4,4			# 0x4
.set	macro
.set	reorder

lw	$4,160($23)
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$7,$4,$L1103
li	$2,8			# 0x8
.set	macro
.set	reorder

$L1104:
.set	noreorder
.set	nomacro
b	$L860
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1156:
li	$2,8			# 0x8
$L1101:
.set	noreorder
.set	nomacro
b	$L1042
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1312:
lh	$2,0($6)
.set	noreorder
.set	nomacro
b	$L832
lh	$5,2($6)
.set	macro
.set	reorder

$L1321:
lbu	$4,11242($23)
sll	$4,$2,$4
li	$2,-2004353024			# 0xffffffff88880000
sra	$5,$4,31
ori	$2,$2,0x8889
mult	$4,$2
mfhi	$2
addu	$2,$2,$4
sra	$2,$2,3
subu	$2,$2,$5
sll	$5,$2,4
subu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L1035
subu	$2,$4,$2
.set	macro
.set	reorder

$L1257:
.set	noreorder
.set	nomacro
b	$L1028
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1322:
lw	$2,10296($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1156
li	$4,4			# 0x4
.set	macro
.set	reorder

lw	$4,160($23)
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$5,$4,$L1109
li	$2,8			# 0x8
.set	macro
.set	reorder

$L1110:
.set	noreorder
.set	nomacro
b	$L1043
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1155:
li	$2,8			# 0x8
$L1092:
.set	noreorder
.set	nomacro
b	$L925
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1255:
.set	noreorder
.set	nomacro
b	$L927
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1307:
lw	$2,10296($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1155
li	$4,4			# 0x4
.set	macro
.set	reorder

lw	$4,160($23)
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$7,$4,$L1106
li	$2,8			# 0x8
.set	macro
.set	reorder

$L1107:
.set	noreorder
.set	nomacro
b	$L926
lbu	$16,11229($23)
.set	macro
.set	reorder

$L1278:
move	$5,$0
$L833:
addu	$19,$19,$2
lw	$18,68($sp)
addu	$20,$20,$5
lw	$21,64($sp)
li	$12,-60			# 0xffffffffffffffc4
slt	$7,$19,-60
slt	$6,$20,-60
movn	$19,$12,$7
movn	$20,$12,$6
movn	$2,$13,$7
slt	$19,$18,$19
movn	$5,$14,$6
slt	$20,$21,$20
movn	$2,$23,$19
.set	noreorder
.set	nomacro
b	$L837
movn	$5,$24,$20
.set	macro
.set	reorder

$L1142:
.set	noreorder
.set	nomacro
b	$L990
sw	$12,60($sp)
.set	macro
.set	reorder

$L1145:
.set	noreorder
.set	nomacro
b	$L993
sw	$13,72($sp)
.set	macro
.set	reorder

$L1144:
.set	noreorder
.set	nomacro
b	$L992
sw	$12,68($sp)
.set	macro
.set	reorder

$L1143:
.set	noreorder
.set	nomacro
b	$L991
move	$21,$13
.set	macro
.set	reorder

$L826:
lw	$13,2184($4)
lw	$9,2188($4)
.set	noreorder
.set	nomacro
b	$L827
sw	$13,80($sp)
.set	macro
.set	reorder

$L970:
li	$4,35			# 0x23
.set	noreorder
.set	nomacro
beq	$17,$4,$L1325
li	$4,36			# 0x24
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$17,$4,$L1326
li	$4,715784192			# 0x2aaa0000
.set	macro
.set	reorder

lw	$15,10044($23)
sra	$18,$17,31
ori	$4,$4,0xaaab
mult	$17,$4
mfhi	$4
subu	$7,$4,$18
sll	$11,$7,1
sll	$4,$7,3
subu	$4,$4,$11
lw	$11,%got(size_table)($28)
subu	$4,$17,$4
.set	noreorder
.set	nomacro
bne	$15,$0,$L978
sll	$20,$4,2
.set	macro
.set	reorder

addiu	$11,$11,%lo(size_table)
xori	$4,$4,0x5
addu	$13,$11,$20
sltu	$4,$4,1
lw	$13,0($13)
subu	$4,$13,$4
.set	noreorder
.set	nomacro
blez	$4,$L979
lw	$13,%got(offset_table)($28)
.set	macro
.set	reorder

$L1097:
slt	$7,$4,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1327
li	$7,125			# 0x7d
.set	macro
.set	reorder

addiu	$4,$4,-1
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
andi	$21,$7,0x1
subu	$3,$0,$21
sra	$7,$7,1
$L981:
lw	$13,%got(offset_table)($28)
li	$4,715784192			# 0x2aaa0000
ori	$4,$4,0xaaab
addiu	$13,$13,%lo(offset_table)
mult	$17,$4
addu	$20,$13,$20
lw	$20,0($20)
mfhi	$17
addu	$7,$7,$20
xor	$7,$7,$3
addu	$7,$7,$21
subu	$4,$17,$18
.set	noreorder
.set	nomacro
beq	$15,$0,$L1096
sw	$7,40($sp)
.set	macro
.set	reorder

move	$15,$0
$L982:
sll	$4,$4,2
addu	$11,$11,$4
lw	$7,0($11)
subu	$11,$7,$15
.set	noreorder
.set	nomacro
blez	$11,$L1141
move	$7,$0
.set	macro
.set	reorder

slt	$7,$11,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1328
li	$7,125			# 0x7d
.set	macro
.set	reorder

addiu	$7,$11,-1
sll	$7,$7,4
ori	$7,$7,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
andi	$11,$7,0x1
subu	$15,$0,$11
sra	$7,$7,1
$L983:
addu	$13,$13,$4
lw	$4,0($13)
addu	$7,$7,$4
xor	$4,$7,$15
addu	$4,$4,$11
.set	noreorder
.set	nomacro
b	$L971
sw	$4,32($sp)
.set	macro
.set	reorder

$L979:
move	$4,$7
addiu	$13,$13,%lo(offset_table)
addu	$20,$13,$20
lw	$7,0($20)
sw	$7,40($sp)
$L1096:
xori	$15,$4,0x5
.set	noreorder
.set	nomacro
b	$L982
sltu	$15,$15,1
.set	macro
.set	reorder

$L1318:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$17,$2,$17
sll	$2,$17,2
addu	$5,$5,$2
lh	$17,0($5)
.set	noreorder
.set	nomacro
b	$L1027
lh	$2,2($5)
.set	macro
.set	reorder

$L1281:
lbu	$2,11243($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L847
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bne	$2,$0,$L1254
.set	noreorder
.set	nomacro
b	$L845
lbu	$16,11228($23)
.set	macro
.set	reorder

$L1282:
lbu	$4,11242($23)
li	$2,1			# 0x1
sll	$2,$2,$4
$L852:
andi	$4,$2,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L856
andi	$4,$2,0x4
.set	macro
.set	reorder

andi	$7,$2,0x2
.set	noreorder
.set	nomacro
b	$L857
andi	$2,$2,0x8
.set	macro
.set	reorder

$L856:
lw	$4,7992($23)
.set	noreorder
.set	nomacro
beq	$4,$0,$L858
andi	$7,$2,0x2
.set	macro
.set	reorder

andi	$4,$2,0x4
.set	noreorder
.set	nomacro
b	$L857
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1039:
lw	$4,7992($23)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1041
andi	$5,$2,0x2
.set	macro
.set	reorder

andi	$4,$2,0x4
.set	noreorder
.set	nomacro
b	$L1040
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1296:
lw	$5,11212($4)
lw	$11,10044($4)
addiu	$5,$5,-1
addu	$5,$5,$11
slt	$7,$5,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1329
li	$7,125			# 0x7d
.set	macro
.set	reorder

addiu	$5,$5,-1
andi	$5,$5,0x7
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
$L782:
lw	$7,11216($4)
addiu	$7,$7,-1
addu	$11,$11,$7
slt	$7,$11,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1330
sw	$5,40($sp)
.set	macro
.set	reorder

addiu	$7,$11,-1
andi	$7,$7,0x7
sll	$7,$7,4
ori	$7,$7,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1331
sw	$7,32($sp)
.set	macro
.set	reorder

$L1303:
lw	$3,8012($23)
lw	$7,10044($23)
lw	$6,988($23)
lw	$9,172($23)
sw	$3,52($sp)
li	$3,1			# 0x1
lw	$4,52($sp)
subu	$3,$3,$7
lw	$17,11220($23)
lw	$18,11224($23)
sll	$8,$4,2
lw	$4,40($sp)
sw	$9,96($sp)
addu	$6,$6,$8
sll	$4,$4,$3
sw	$4,72($sp)
lw	$4,32($sp)
lw	$10,72($sp)
sll	$4,$4,$3
sw	$10,40($sp)
sw	$4,76($sp)
lw	$4,44($sp)
lw	$11,76($sp)
sll	$4,$4,$3
sw	$11,32($sp)
sw	$4,88($sp)
lw	$4,36($sp)
lw	$12,88($sp)
sll	$4,$4,$3
lh	$3,0($6)
lh	$6,2($6)
sw	$12,44($sp)
sw	$4,92($sp)
lw	$13,92($sp)
lh	$4,11298($23)
.set	noreorder
.set	nomacro
bne	$7,$0,$L875
sw	$13,36($sp)
.set	macro
.set	reorder

mul	$7,$3,$4
mul	$4,$4,$6
sll	$3,$3,8
sll	$6,$6,8
subu	$3,$7,$3
subu	$6,$4,$6
addiu	$7,$7,255
addiu	$4,$4,255
addiu	$3,$3,255
addiu	$6,$6,255
sra	$9,$4,9
sra	$7,$7,9
sra	$3,$3,9
sra	$4,$6,9
sll	$7,$7,1
sll	$9,$9,1
sll	$3,$3,1
sll	$4,$4,1
$L876:
lw	$13,7992($23)
li	$15,-60			# 0xffffffffffffffc4
lw	$6,160($23)
sll	$10,$13,6
sw	$10,48($sp)
sll	$10,$6,6
lw	$11,48($sp)
addiu	$10,$10,-4
subu	$15,$15,$11
sw	$10,56($sp)
lw	$12,56($sp)
slt	$10,$7,$15
.set	noreorder
.set	nomacro
bne	$10,$0,$L1127
subu	$19,$12,$11
.set	macro
.set	reorder

slt	$10,$19,$7
movn	$7,$19,$10
$L877:
lw	$10,164($23)
li	$16,-60			# 0xffffffffffffffc4
lw	$11,7996($23)
sw	$7,7268($23)
sll	$14,$10,6
sll	$12,$11,6
addiu	$14,$14,-4
subu	$16,$16,$12
sw	$14,68($sp)
slt	$14,$9,$16
lw	$21,68($sp)
.set	noreorder
.set	nomacro
bne	$14,$0,$L1128
subu	$20,$21,$12
.set	macro
.set	reorder

slt	$14,$20,$9
movn	$9,$20,$14
$L878:
slt	$14,$3,$15
.set	noreorder
.set	nomacro
bne	$14,$0,$L1129
sw	$9,7272($23)
.set	macro
.set	reorder

slt	$14,$19,$3
movn	$3,$19,$14
$L879:
slt	$14,$4,$16
.set	noreorder
.set	nomacro
bne	$14,$0,$L1130
sw	$3,7300($23)
.set	macro
.set	reorder

slt	$14,$20,$4
movn	$4,$20,$14
$L880:
addiu	$14,$5,-1
sltu	$14,$14,2
.set	noreorder
.set	nomacro
bne	$14,$0,$L1332
sw	$4,7304($23)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$5,$0,$L896
lw	$14,2184($23)
.set	macro
.set	reorder

addiu	$fp,$6,-1
addiu	$25,$8,-8
sll	$22,$17,1
sw	$fp,80($sp)
sll	$fp,$18,1
sw	$25,60($sp)
$L895:
lw	$3,80($sp)
lw	$24,60($sp)
lw	$21,2188($23)
xor	$25,$13,$3
li	$3,2			# 0x2
addu	$4,$21,$24
li	$24,-2			# 0xfffffffffffffffe
movz	$3,$24,$25
sw	$3,64($sp)
.set	noreorder
.set	nomacro
bne	$13,$0,$L900
lw	$3,10296($23)
.set	macro
.set	reorder

sh	$0,2($4)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1067
sh	$0,0($4)
.set	macro
.set	reorder

lw	$3,11168($23)
slt	$3,$3,3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1333
move	$4,$0
.set	macro
.set	reorder

sll	$12,$6,5
sll	$6,$10,5
sll	$11,$11,5
addiu	$12,$12,-4
addiu	$6,$6,-4
move	$15,$0
move	$3,$0
$L905:
addu	$10,$11,$4
slt	$16,$10,-28
beq	$16,$0,$L906
li	$4,-28			# 0xffffffffffffffe4
li	$10,-28			# 0xffffffffffffffe4
subu	$4,$4,$11
$L906:
subu	$15,$12,$15
subu	$11,$6,$11
slt	$12,$12,$13
slt	$6,$6,$10
movn	$3,$15,$12
movn	$4,$11,$6
$L908:
lw	$15,88($sp)
addiu	$22,$22,-1
lw	$16,92($sp)
addiu	$fp,$fp,-1
addu	$3,$15,$3
addu	$4,$16,$4
addu	$3,$17,$3
addu	$4,$18,$4
and	$3,$3,$22
and	$4,$4,$fp
subu	$3,$3,$17
subu	$4,$4,$18
sw	$3,7300($23)
sw	$4,7304($23)
$L898:
addu	$14,$14,$8
addu	$21,$21,$8
andi	$5,$5,0x00ff
sh	$7,0($14)
sh	$9,2($14)
sh	$3,0($21)
sh	$4,2($21)
sb	$5,36($2)
.set	noreorder
.set	nomacro
b	$L1346
lw	$31,172($sp)
.set	macro
.set	reorder

$L862:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$2,$3
sll	$2,$2,2
addu	$6,$6,$2
lh	$3,0($6)
lh	$2,2($6)
.set	noreorder
.set	nomacro
b	$L863
sw	$3,48($sp)
.set	macro
.set	reorder

$L858:
andi	$4,$2,0x4
lbu	$16,11229($23)
.set	noreorder
.set	nomacro
b	$L857
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1283:
lbu	$2,11242($23)
sll	$4,$4,$2
li	$2,-2004353024			# 0xffffffff88880000
sra	$7,$4,31
ori	$2,$2,0x8889
mult	$4,$2
mfhi	$2
addu	$2,$2,$4
sra	$2,$2,3
subu	$2,$2,$7
sll	$7,$2,4
subu	$2,$7,$2
.set	noreorder
.set	nomacro
b	$L852
subu	$2,$4,$2
.set	macro
.set	reorder

$L1324:
move	$17,$0
sw	$0,108($sp)
lw	$fp,108($sp)
$L1356:
addu	$2,$20,$17
addu	$fp,$19,$fp
sw	$2,96($sp)
slt	$8,$fp,-60
sw	$fp,84($sp)
slt	$fp,$2,-60
li	$2,-60			# 0xffffffffffffffc4
sw	$8,76($sp)
sw	$fp,92($sp)
lw	$fp,84($sp)
movz	$2,$fp,$8
lw	$fp,96($sp)
lw	$8,92($sp)
sw	$2,84($sp)
li	$2,-60			# 0xffffffffffffffc4
movz	$2,$fp,$8
lw	$8,76($sp)
lw	$fp,68($sp)
sw	$2,96($sp)
lw	$2,108($sp)
movn	$2,$13,$8
lw	$8,92($sp)
sw	$2,108($sp)
lw	$2,84($sp)
movn	$17,$14,$8
slt	$fp,$fp,$2
lw	$8,96($sp)
lw	$2,64($sp)
sw	$fp,76($sp)
slt	$fp,$2,$8
lw	$2,108($sp)
lw	$8,76($sp)
movn	$17,$24,$fp
movn	$2,$23,$8
.set	noreorder
.set	nomacro
b	$L820
sw	$2,108($sp)
.set	macro
.set	reorder

$L1059:
lw	$17,60($sp)
$L1355:
lw	$8,80($sp)
sll	$2,$17,1
subu	$2,$16,$2
sll	$17,$2,2
addu	$17,$8,$17
li	$8,1			# 0x1
.set	noreorder
.set	nomacro
beq	$18,$8,$L1334
lw	$8,76($sp)
.set	macro
.set	reorder

addu	$2,$2,$8
lh	$8,0($17)
sll	$2,$2,2
sw	$8,84($sp)
lw	$8,80($sp)
addu	$2,$8,$2
sw	$2,76($sp)
lw	$2,84($sp)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
#NO_APP
lw	$8,76($sp)
lh	$2,0($8)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
lh	$2,0($fp)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000000100000000111101110	#S32M2I XR7, $2
# 0 "" 2
#NO_APP
sw	$2,108($sp)
lh	$2,2($17)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
#NO_APP
lh	$2,2($8)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
lh	$2,2($fp)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000100010000000111101110	#S32M2I XR7, $17
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1350
lw	$2,11168($4)
.set	macro
.set	reorder

$L1297:
li	$7,1			# 0x1
sw	$0,40($sp)
sw	$0,32($sp)
li	$13,1			# 0x1
sw	$0,44($sp)
move	$5,$0
sw	$7,8004($4)
.set	noreorder
.set	nomacro
b	$L773
sw	$0,36($sp)
.set	macro
.set	reorder

$L1289:
li	$11,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
#NO_APP
nor	$4,$0,$4
sll	$4,$4,4
ori	$4,$4,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addu	$7,$4,$7
sll	$4,$7,2
addu	$4,$12,$4
lh	$7,0($4)
.set	noreorder
.set	nomacro
b	$L968
lh	$4,2($4)
.set	macro
.set	reorder

$L1079:
lw	$25,96($sp)
$L1360:
sll	$4,$25,1
subu	$18,$18,$4
sll	$4,$18,2
addu	$25,$24,$4
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$4,$L1335
addu	$18,$18,$7
.set	macro
.set	reorder

lh	$7,0($25)
sll	$4,$18,2
addu	$18,$24,$4
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
lh	$4,0($18)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001000000000010101111	#S32I2M XR2,$4
# 0 "" 2
#NO_APP
lh	$4,0($20)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001000000000011101111	#S32I2M XR3,$4
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001000000000111101110	#S32M2I XR7, $4
# 0 "" 2
#NO_APP
lh	$7,2($25)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
lh	$7,2($18)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
lh	$7,2($20)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001110000000011101111	#S32I2M XR3,$7
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001110000000111101110	#S32M2I XR7, $7
# 0 "" 2
#NO_APP
$L1016:
lw	$18,11168($23)
slt	$18,$18,3
.set	noreorder
.set	nomacro
beq	$18,$0,$L1359
addu	$20,$17,$7
.set	macro
.set	reorder

sll	$13,$10,5
lw	$3,56($sp)
lw	$18,52($sp)
sll	$2,$2,5
addu	$10,$13,$4
sll	$12,$3,5
slt	$15,$10,-28
sll	$17,$18,5
addiu	$2,$2,-4
.set	noreorder
.set	nomacro
beq	$15,$0,$L1018
addiu	$12,$12,-4
.set	macro
.set	reorder

li	$4,-28			# 0xffffffffffffffe4
li	$10,-28			# 0xffffffffffffffe4
.set	noreorder
.set	nomacro
b	$L1018
subu	$4,$4,$13
.set	macro
.set	reorder

$L1319:
lbu	$2,11243($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1030
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bne	$2,$0,$L1257
.set	noreorder
.set	nomacro
b	$L1028
lbu	$16,11228($23)
.set	macro
.set	reorder

$L847:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,7			# 0x7
beq	$2,$4,$L850
lbu	$16,11228($23)
.set	noreorder
.set	nomacro
b	$L845
addu	$16,$16,$2
.set	macro
.set	reorder

$L1013:
.set	noreorder
.set	nomacro
beq	$4,$0,$L1360
lw	$25,96($sp)
.set	macro
.set	reorder

lh	$4,0($20)
.set	noreorder
.set	nomacro
b	$L1016
lh	$7,2($20)
.set	macro
.set	reorder

$L1136:
.set	noreorder
.set	nomacro
b	$L935
move	$4,$15
.set	macro
.set	reorder

$L1135:
.set	noreorder
.set	nomacro
b	$L934
move	$2,$14
.set	macro
.set	reorder

$L1134:
.set	noreorder
.set	nomacro
b	$L933
move	$11,$15
.set	macro
.set	reorder

$L1133:
.set	noreorder
.set	nomacro
b	$L932
move	$7,$14
.set	macro
.set	reorder

$L930:
lh	$6,2($7)
mul	$7,$2,$11
sll	$2,$2,8
mul	$4,$11,$6
sll	$6,$6,8
subu	$2,$7,$2
addiu	$7,$7,128
addiu	$2,$2,128
subu	$6,$4,$6
addiu	$11,$4,128
addiu	$4,$6,128
sra	$7,$7,8
sra	$11,$11,8
sra	$2,$2,8
.set	noreorder
.set	nomacro
b	$L931
sra	$4,$4,8
.set	macro
.set	reorder

$L1115:
move	$11,$0
.set	noreorder
.set	nomacro
b	$L791
move	$14,$0
.set	macro
.set	reorder

$L1041:
andi	$4,$2,0x4
lbu	$16,11229($23)
.set	noreorder
.set	nomacro
b	$L1040
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1317:
addu	$20,$17,$7
$L1359:
lw	$25,76($sp)
addu	$18,$15,$4
lw	$3,64($sp)
li	$17,-60			# 0xffffffffffffffc4
lw	$10,80($sp)
slt	$15,$18,-60
slt	$2,$20,-60
movn	$18,$17,$15
movn	$20,$17,$2
movn	$4,$12,$15
slt	$15,$25,$18
movn	$7,$13,$2
lw	$2,88($sp)
movn	$4,$3,$15
slt	$20,$2,$20
.set	noreorder
.set	nomacro
b	$L1021
movn	$7,$10,$20
.set	macro
.set	reorder

$L1316:
addiu	$20,$2,-1
lw	$3,2184($23)
addiu	$21,$14,-8
xor	$25,$10,$20
sw	$20,112($sp)
li	$20,2			# 0x2
addu	$24,$3,$21
sw	$21,108($sp)
li	$21,-2			# 0xfffffffffffffffe
sw	$3,92($sp)
move	$3,$20
lw	$20,10296($23)
.set	noreorder
.set	nomacro
bne	$10,$0,$L996
movz	$3,$21,$25
.set	macro
.set	reorder

sh	$0,2($24)
.set	noreorder
.set	nomacro
beq	$20,$0,$L1077
sh	$0,0($24)
.set	macro
.set	reorder

lw	$20,11168($23)
slt	$20,$20,3
.set	noreorder
.set	nomacro
beq	$20,$0,$L1336
move	$21,$0
.set	macro
.set	reorder

sll	$20,$2,5
lw	$25,56($sp)
lw	$3,52($sp)
addiu	$20,$20,-4
sw	$0,124($sp)
sll	$21,$25,5
sll	$3,$3,5
sw	$20,84($sp)
addiu	$20,$21,-4
move	$21,$0
sw	$3,120($sp)
move	$3,$0
sw	$20,100($sp)
move	$20,$0
$L1001:
lw	$24,120($sp)
$L1362:
addu	$24,$24,$21
slt	$25,$24,-28
.set	noreorder
.set	nomacro
beq	$25,$0,$L1002
sw	$24,104($sp)
.set	macro
.set	reorder

lw	$24,120($sp)
li	$25,-28			# 0xffffffffffffffe4
li	$21,-28			# 0xffffffffffffffe4
subu	$21,$21,$24
sw	$25,104($sp)
$L1002:
lw	$25,84($sp)
lw	$24,120($sp)
subu	$3,$25,$3
sw	$3,60($sp)
lw	$3,100($sp)
subu	$3,$3,$24
lw	$24,124($sp)
sw	$3,116($sp)
slt	$3,$25,$24
lw	$25,100($sp)
lw	$24,104($sp)
slt	$25,$25,$24
lw	$24,116($sp)
sw	$25,100($sp)
lw	$25,60($sp)
movn	$20,$25,$3
lw	$3,100($sp)
movn	$21,$24,$3
$L1004:
addu	$21,$7,$21
lw	$3,128($sp)
sll	$7,$8,1
addu	$4,$4,$20
lw	$20,128($sp)
sll	$3,$3,1
sw	$7,100($sp)
addu	$24,$20,$4
addu	$4,$8,$21
lw	$21,100($sp)
addiu	$7,$3,-1
sw	$3,84($sp)
addiu	$20,$21,-1
and	$7,$24,$7
lw	$24,128($sp)
and	$4,$4,$20
subu	$21,$4,$8
subu	$7,$7,$24
li	$4,2			# 0x2
sw	$21,7272($23)
sw	$7,60($sp)
.set	noreorder
.set	nomacro
beq	$5,$4,$L1008
sw	$7,7268($23)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1011
lw	$24,2188($23)
.set	macro
.set	reorder

$L1130:
.set	noreorder
.set	nomacro
b	$L880
move	$4,$16
.set	macro
.set	reorder

$L1129:
.set	noreorder
.set	nomacro
b	$L879
move	$3,$15
.set	macro
.set	reorder

$L1128:
.set	noreorder
.set	nomacro
b	$L878
move	$9,$16
.set	macro
.set	reorder

$L1127:
.set	noreorder
.set	nomacro
b	$L877
move	$7,$15
.set	macro
.set	reorder

$L1077:
lw	$25,96($sp)
$L1364:
sll	$20,$25,1
lw	$25,92($sp)
subu	$20,$18,$20
sll	$21,$20,2
addu	$21,$25,$21
li	$25,1			# 0x1
beq	$2,$25,$L1337
addu	$20,$20,$3
lh	$3,0($21)
sll	$25,$20,2
lw	$20,92($sp)
addu	$25,$20,$25
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000000110000000001101111	#S32I2M XR1,$3
# 0 "" 2
#NO_APP
lh	$20,0($25)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000101000000000010101111	#S32I2M XR2,$20
# 0 "" 2
#NO_APP
lh	$20,0($24)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000101000000000011101111	#S32I2M XR3,$20
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000101000000000111101110	#S32M2I XR7, $20
# 0 "" 2
#NO_APP
lh	$21,2($21)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000101010000000001101111	#S32I2M XR1,$21
# 0 "" 2
#NO_APP
lh	$21,2($25)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000101010000000010101111	#S32I2M XR2,$21
# 0 "" 2
#NO_APP
lh	$21,2($24)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000101010000000011101111	#S32I2M XR3,$21
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000101010000000111101110	#S32M2I XR7, $21
# 0 "" 2
#NO_APP
$L999:
lw	$24,11168($23)
slt	$24,$24,3
.set	noreorder
.set	nomacro
beq	$24,$0,$L1361
addu	$25,$15,$20
.set	macro
.set	reorder

sll	$3,$10,5
addu	$24,$3,$20
slt	$25,$24,-28
sw	$24,124($sp)
sw	$25,104($sp)
sll	$25,$2,5
lw	$24,52($sp)
addiu	$25,$25,-4
sll	$24,$24,5
sw	$25,84($sp)
sw	$24,120($sp)
lw	$24,56($sp)
sll	$25,$24,5
addiu	$25,$25,-4
sw	$25,100($sp)
lw	$25,104($sp)
.set	noreorder
.set	nomacro
beq	$25,$0,$L1362
lw	$24,120($sp)
.set	macro
.set	reorder

li	$20,-28			# 0xffffffffffffffe4
sw	$20,124($sp)
.set	noreorder
.set	nomacro
b	$L1001
subu	$20,$20,$3
.set	macro
.set	reorder

$L1075:
lw	$24,76($sp)
$L1363:
sll	$2,$24,1
subu	$17,$17,$2
sll	$2,$17,2
addu	$24,$5,$2
li	$2,1			# 0x1
beq	$6,$2,$L1338
lw	$2,56($sp)
lh	$9,0($24)
addu	$17,$17,$2
sll	$2,$17,2
addu	$17,$5,$2
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000010010000000001101111	#S32I2M XR1,$9
# 0 "" 2
#NO_APP
lh	$2,0($17)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
lh	$2,0($4)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000000100000000111101110	#S32M2I XR7, $2
# 0 "" 2
#NO_APP
lh	$24,2($24)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000110000000000001101111	#S32I2M XR1,$24
# 0 "" 2
#NO_APP
lh	$17,2($17)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000100010000000010101111	#S32I2M XR2,$17
# 0 "" 2
#NO_APP
lh	$4,2($4)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001000000000011101111	#S32I2M XR3,$4
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001000000000111101110	#S32M2I XR7, $4
# 0 "" 2
#NO_APP
$L958:
lw	$17,11168($23)
slt	$17,$17,3
.set	noreorder
.set	nomacro
beq	$17,$0,$L959
sll	$6,$6,5
.set	macro
.set	reorder

sll	$15,$12,5
addu	$12,$15,$2
sll	$14,$10,5
slt	$17,$12,-28
sll	$21,$8,5
addiu	$6,$6,-4
.set	noreorder
.set	nomacro
beq	$17,$0,$L960
addiu	$14,$14,-4
.set	macro
.set	reorder

li	$2,-28			# 0xffffffffffffffe4
li	$12,-28			# 0xffffffffffffffe4
.set	noreorder
.set	nomacro
b	$L960
subu	$2,$2,$15
.set	macro
.set	reorder

$L1010:
lw	$24,2184($23)
sw	$24,92($sp)
.set	noreorder
.set	nomacro
b	$L1011
lw	$24,2188($23)
.set	macro
.set	reorder

$L978:
addiu	$11,$11,%lo(size_table)
addu	$4,$11,$20
lw	$4,0($4)
.set	noreorder
.set	nomacro
bgtz	$4,$L1097
lw	$13,%got(offset_table)($28)
.set	macro
.set	reorder

move	$4,$7
move	$15,$0
addiu	$13,$13,%lo(offset_table)
addu	$20,$13,$20
lw	$7,0($20)
.set	noreorder
.set	nomacro
b	$L982
sw	$7,40($sp)
.set	macro
.set	reorder

$L1325:
lw	$4,11212($23)
lw	$11,10044($23)
addiu	$4,$4,-1
addu	$4,$4,$11
slt	$7,$4,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1339
li	$7,125			# 0x7d
.set	macro
.set	reorder

addiu	$4,$4,-1
andi	$4,$4,0x7
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
$L974:
lw	$4,11216($23)
sw	$7,40($sp)
addiu	$4,$4,-1
addu	$4,$11,$4
slt	$7,$4,9
.set	noreorder
.set	nomacro
beq	$7,$0,$L1340
li	$7,125			# 0x7d
.set	macro
.set	reorder

addiu	$4,$4,-1
andi	$4,$4,0x7
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L971
sw	$4,32($sp)
.set	macro
.set	reorder

$L1030:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,7			# 0x7
beq	$2,$4,$L1033
lbu	$16,11228($23)
.set	noreorder
.set	nomacro
b	$L1028
addu	$16,$16,$2
.set	macro
.set	reorder

$L875:
mul	$7,$3,$4
mul	$4,$4,$6
sll	$3,$3,8
sll	$6,$6,8
subu	$3,$7,$3
subu	$6,$4,$6
addiu	$9,$4,128
addiu	$7,$7,128
addiu	$3,$3,128
addiu	$4,$6,128
sra	$7,$7,8
sra	$9,$9,8
sra	$3,$3,8
.set	noreorder
.set	nomacro
b	$L876
sra	$4,$4,8
.set	macro
.set	reorder

$L1326:
li	$4,1			# 0x1
sw	$0,40($sp)
sw	$0,32($sp)
.set	noreorder
.set	nomacro
b	$L971
sw	$4,8004($23)
.set	macro
.set	reorder

$L1310:
$L959:
lw	$6,60($sp)
li	$24,-60			# 0xffffffffffffffc4
lw	$8,80($sp)
lw	$9,88($sp)
addu	$12,$6,$2
lw	$10,92($sp)
addu	$6,$8,$4
slt	$8,$12,-60
slt	$17,$6,-60
movn	$12,$24,$8
movn	$6,$24,$17
movn	$2,$14,$8
slt	$12,$9,$12
movn	$4,$15,$17
slt	$6,$10,$6
movn	$2,$21,$12
.set	noreorder
.set	nomacro
b	$L963
movn	$4,$3,$6
.set	macro
.set	reorder

$L1305:
lbu	$4,11242($23)
li	$2,1			# 0x1
sll	$2,$2,$4
$L918:
andi	$4,$2,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L922
andi	$4,$2,0x4
.set	macro
.set	reorder

andi	$7,$2,0x2
.set	noreorder
.set	nomacro
b	$L923
andi	$2,$2,0x8
.set	macro
.set	reorder

$L922:
lw	$4,7992($23)
.set	noreorder
.set	nomacro
beq	$4,$0,$L924
andi	$7,$2,0x2
.set	macro
.set	reorder

andi	$4,$2,0x4
.set	noreorder
.set	nomacro
b	$L923
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1304:
lbu	$2,11243($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L913
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bne	$2,$0,$L1255
.set	noreorder
.set	nomacro
b	$L927
lbu	$16,11228($23)
.set	macro
.set	reorder

$L924:
andi	$4,$2,0x4
lbu	$16,11229($23)
.set	noreorder
.set	nomacro
b	$L923
andi	$2,$2,0x8
.set	macro
.set	reorder

$L955:
.set	noreorder
.set	nomacro
beq	$2,$0,$L1363
lw	$24,76($sp)
.set	macro
.set	reorder

lh	$2,0($4)
.set	noreorder
.set	nomacro
b	$L958
lh	$4,2($4)
.set	macro
.set	reorder

$L996:
.set	noreorder
.set	nomacro
beq	$20,$0,$L1364
lw	$25,96($sp)
.set	macro
.set	reorder

lh	$20,0($24)
.set	noreorder
.set	nomacro
b	$L999
lh	$21,2($24)
.set	macro
.set	reorder

$L1334:
lh	$fp,0($17)
lh	$17,2($17)
.set	noreorder
.set	nomacro
b	$L815
sw	$fp,108($sp)
.set	macro
.set	reorder

$L1309:
lw	$7,124($sp)
addiu	$25,$6,-1
lw	$13,2184($23)
xor	$24,$12,$25
addiu	$7,$7,-8
sw	$25,108($sp)
li	$25,-2			# 0xfffffffffffffffe
addu	$11,$13,$7
sw	$7,104($sp)
li	$7,2			# 0x2
move	$9,$7
lw	$7,10296($23)
.set	noreorder
.set	nomacro
bne	$12,$0,$L938
movz	$9,$25,$24
.set	macro
.set	reorder

sh	$0,2($11)
.set	noreorder
.set	nomacro
beq	$7,$0,$L1073
sh	$0,0($11)
.set	macro
.set	reorder

lw	$7,11168($23)
slt	$7,$7,3
.set	noreorder
.set	nomacro
beq	$7,$0,$L1341
move	$11,$0
.set	macro
.set	reorder

sll	$7,$6,5
sw	$0,120($sp)
sll	$11,$10,5
addiu	$7,$7,-4
sll	$24,$8,5
move	$9,$0
sw	$7,96($sp)
addiu	$7,$11,-4
move	$11,$0
sw	$24,116($sp)
sw	$7,84($sp)
move	$7,$0
$L943:
lw	$24,116($sp)
$L1366:
addu	$24,$24,$11
slt	$25,$24,-28
.set	noreorder
.set	nomacro
beq	$25,$0,$L944
sw	$24,100($sp)
.set	macro
.set	reorder

lw	$24,116($sp)
li	$25,-28			# 0xffffffffffffffe4
li	$11,-28			# 0xffffffffffffffe4
subu	$11,$11,$24
sw	$25,100($sp)
$L944:
lw	$25,96($sp)
lw	$24,116($sp)
subu	$9,$25,$9
sw	$9,72($sp)
lw	$9,84($sp)
subu	$9,$9,$24
lw	$24,120($sp)
sw	$9,112($sp)
slt	$9,$25,$24
lw	$25,84($sp)
lw	$24,100($sp)
slt	$25,$25,$24
lw	$24,112($sp)
sw	$25,84($sp)
lw	$25,72($sp)
movn	$7,$25,$9
lw	$9,84($sp)
movn	$11,$24,$9
$L946:
lw	$9,52($sp)
sll	$25,$18,1
lw	$24,56($sp)
addu	$7,$9,$7
sll	$9,$20,1
addu	$7,$18,$7
addu	$11,$24,$11
sw	$9,52($sp)
addiu	$24,$9,-1
sw	$7,72($sp)
addu	$11,$20,$11
lw	$9,72($sp)
addiu	$7,$25,-1
and	$11,$11,$24
subu	$11,$11,$20
and	$7,$9,$7
subu	$7,$7,$18
li	$24,2			# 0x2
sw	$11,7272($23)
.set	noreorder
.set	nomacro
beq	$5,$24,$L950
sw	$7,7268($23)
.set	macro
.set	reorder

$L951:
.set	noreorder
.set	nomacro
b	$L953
lw	$5,2188($23)
.set	macro
.set	reorder

$L1336:
addu	$25,$15,$20
$L1361:
addu	$3,$17,$21
slt	$24,$25,-60
sw	$25,100($sp)
slt	$25,$3,-60
sw	$3,104($sp)
li	$3,-60			# 0xffffffffffffffc4
sw	$25,84($sp)
lw	$25,100($sp)
sw	$24,60($sp)
movz	$3,$25,$24
lw	$25,104($sp)
lw	$24,84($sp)
sw	$3,100($sp)
li	$3,-60			# 0xffffffffffffffc4
movz	$3,$25,$24
lw	$25,100($sp)
lw	$24,76($sp)
sw	$3,104($sp)
lw	$3,60($sp)
slt	$24,$24,$25
lw	$25,88($sp)
sw	$24,60($sp)
movn	$20,$12,$3
lw	$3,84($sp)
movn	$21,$13,$3
lw	$3,104($sp)
slt	$24,$25,$3
lw	$25,60($sp)
lw	$3,64($sp)
movn	$20,$3,$25
lw	$25,80($sp)
.set	noreorder
.set	nomacro
b	$L1004
movn	$21,$25,$24
.set	macro
.set	reorder

$L1073:
lw	$24,76($sp)
$L1370:
li	$25,1			# 0x1
sll	$7,$24,1
subu	$7,$17,$7
sll	$24,$7,2
.set	noreorder
.set	nomacro
beq	$6,$25,$L1342
addu	$24,$13,$24
.set	macro
.set	reorder

addu	$7,$7,$9
lh	$9,0($24)
sll	$7,$7,2
addu	$25,$13,$7
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000010010000000001101111	#S32I2M XR1,$9
# 0 "" 2
#NO_APP
lh	$7,0($25)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
lh	$7,0($11)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001110000000011101111	#S32I2M XR3,$7
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001110000000111101110	#S32M2I XR7, $7
# 0 "" 2
#NO_APP
lh	$24,2($24)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000110000000000001101111	#S32I2M XR1,$24
# 0 "" 2
#NO_APP
lh	$24,2($25)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000110000000000010101111	#S32I2M XR2,$24
# 0 "" 2
#NO_APP
lh	$11,2($11)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000010110000000011101111	#S32I2M XR3,$11
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000010110000000111101110	#S32M2I XR7, $11
# 0 "" 2
#NO_APP
$L941:
lw	$24,11168($23)
slt	$24,$24,3
.set	noreorder
.set	nomacro
beq	$24,$0,$L1365
lw	$25,60($sp)
.set	macro
.set	reorder

sll	$9,$12,5
addu	$24,$9,$7
slt	$25,$24,-28
sw	$24,120($sp)
sll	$24,$8,5
sw	$25,100($sp)
sll	$25,$6,5
addiu	$25,$25,-4
sw	$24,116($sp)
sw	$25,96($sp)
sll	$25,$10,5
addiu	$25,$25,-4
sw	$25,84($sp)
lw	$25,100($sp)
.set	noreorder
.set	nomacro
beq	$25,$0,$L1366
lw	$24,116($sp)
.set	macro
.set	reorder

li	$7,-28			# 0xffffffffffffffe4
sw	$7,120($sp)
.set	noreorder
.set	nomacro
b	$L943
subu	$7,$7,$9
.set	macro
.set	reorder

$L1141:
move	$15,$0
.set	noreorder
.set	nomacro
b	$L983
move	$11,$0
.set	macro
.set	reorder

$L1300:
li	$5,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addiu	$13,$12,-1
andi	$13,$13,0x7
sll	$13,$13,4
ori	$13,$13,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$20,$21,1
# 0 "" 2
#NO_APP
addiu	$12,$12,-8
sll	$5,$5,$12
or	$20,$5,$20
andi	$21,$20,0x1
subu	$3,$0,$21
.set	noreorder
.set	nomacro
b	$L789
sra	$20,$20,1
.set	macro
.set	reorder

$L850:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$16,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1351
lw	$2,2172($23)
.set	macro
.set	reorder

$L1067:
lw	$25,96($sp)
$L1367:
lw	$24,52($sp)
sll	$3,$25,1
li	$25,1			# 0x1
subu	$3,$24,$3
sll	$24,$3,2
.set	noreorder
.set	nomacro
beq	$6,$25,$L1343
addu	$24,$21,$24
.set	macro
.set	reorder

lw	$25,64($sp)
addu	$3,$3,$25
lh	$25,0($24)
sll	$3,$3,2
sw	$25,52($sp)
addu	$25,$21,$3
lw	$3,52($sp)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000000110000000001101111	#S32I2M XR1,$3
# 0 "" 2
#NO_APP
lh	$3,0($25)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000000110000000010101111	#S32I2M XR2,$3
# 0 "" 2
#NO_APP
lh	$3,0($4)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000000110000000011101111	#S32I2M XR3,$3
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000000110000000111101110	#S32M2I XR7, $3
# 0 "" 2
#NO_APP
lh	$24,2($24)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000110000000000001101111	#S32I2M XR1,$24
# 0 "" 2
#NO_APP
lh	$24,2($25)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000110000000000010101111	#S32I2M XR2,$24
# 0 "" 2
#NO_APP
lh	$4,2($4)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001000000000011101111	#S32I2M XR3,$4
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001000000000111101110	#S32M2I XR7, $4
# 0 "" 2
#NO_APP
$L903:
lw	$24,11168($23)
slt	$24,$24,3
.set	noreorder
.set	nomacro
beq	$24,$0,$L904
sll	$11,$11,5
.set	macro
.set	reorder

sll	$15,$13,5
sll	$12,$6,5
addu	$13,$15,$3
sll	$6,$10,5
slt	$16,$13,-28
addiu	$12,$12,-4
.set	noreorder
.set	nomacro
beq	$16,$0,$L905
addiu	$6,$6,-4
.set	macro
.set	reorder

li	$3,-28			# 0xffffffffffffffe4
li	$13,-28			# 0xffffffffffffffe4
.set	noreorder
.set	nomacro
b	$L905
subu	$3,$3,$15
.set	macro
.set	reorder

$L900:
.set	noreorder
.set	nomacro
beq	$3,$0,$L1367
lw	$25,96($sp)
.set	macro
.set	reorder

lh	$3,0($4)
.set	noreorder
.set	nomacro
b	$L903
lh	$4,2($4)
.set	macro
.set	reorder

$L1333:
$L904:
lw	$10,48($sp)
addu	$12,$12,$4
li	$13,-60			# 0xffffffffffffffc4
addu	$6,$10,$3
slt	$10,$12,-60
slt	$11,$6,-60
movn	$12,$13,$10
movn	$6,$13,$11
movn	$3,$15,$11
movn	$4,$16,$10
lw	$11,56($sp)
lw	$13,68($sp)
slt	$6,$11,$6
slt	$12,$13,$12
movn	$3,$19,$6
.set	noreorder
.set	nomacro
b	$L908
movn	$4,$20,$12
.set	macro
.set	reorder

$L1332:
addiu	$22,$6,-1
lw	$24,96($sp)
li	$9,-2			# 0xfffffffffffffffe
lw	$fp,52($sp)
li	$7,2			# 0x2
lw	$14,2184($23)
xor	$21,$13,$22
sw	$22,80($sp)
movz	$7,$9,$21
sll	$22,$24,1
addiu	$25,$8,-8
subu	$9,$fp,$22
addu	$21,$14,$25
addu	$7,$9,$7
sw	$25,60($sp)
sll	$9,$9,2
sll	$7,$7,2
addu	$9,$14,$9
addu	$22,$14,$7
.set	noreorder
.set	nomacro
bne	$13,$0,$L883
lw	$7,10296($23)
.set	macro
.set	reorder

sh	$0,2($21)
.set	noreorder
.set	nomacro
beq	$7,$0,$L1065
sh	$0,0($21)
.set	macro
.set	reorder

lw	$7,11168($23)
slt	$7,$7,3
.set	noreorder
.set	nomacro
beq	$7,$0,$L1344
move	$9,$0
.set	macro
.set	reorder

sll	$7,$6,5
sll	$25,$10,5
addiu	$7,$7,-4
addiu	$25,$25,-4
sll	$24,$11,5
sw	$7,64($sp)
move	$22,$0
sw	$25,100($sp)
move	$21,$0
move	$7,$0
$L888:
addu	$fp,$24,$9
slt	$25,$fp,-28
.set	noreorder
.set	nomacro
beq	$25,$0,$L1368
lw	$25,64($sp)
.set	macro
.set	reorder

li	$9,-28			# 0xffffffffffffffe4
li	$fp,-28			# 0xffffffffffffffe4
subu	$9,$9,$24
$L1368:
subu	$21,$25,$21
lw	$25,100($sp)
subu	$24,$25,$24
slt	$25,$25,$fp
sw	$24,84($sp)
lw	$24,64($sp)
lw	$fp,84($sp)
slt	$22,$24,$22
movn	$7,$21,$22
movn	$9,$fp,$25
$L891:
lw	$21,72($sp)
sll	$22,$17,1
lw	$24,76($sp)
sll	$fp,$18,1
addu	$7,$21,$7
addu	$9,$24,$9
addiu	$21,$fp,-1
addu	$24,$17,$7
addu	$9,$18,$9
addiu	$7,$22,-1
and	$9,$9,$21
and	$7,$24,$7
subu	$7,$7,$17
subu	$9,$9,$18
li	$21,2			# 0x2
sw	$7,7268($23)
.set	noreorder
.set	nomacro
beq	$5,$21,$L895
sw	$9,7272($23)
.set	macro
.set	reorder

$L896:
.set	noreorder
.set	nomacro
b	$L898
lw	$21,2188($23)
.set	macro
.set	reorder

$L1335:
lh	$4,0($25)
.set	noreorder
.set	nomacro
b	$L1016
lh	$7,2($25)
.set	macro
.set	reorder

$L1301:
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$14,$11,-1
andi	$14,$14,0x7
sll	$14,$14,4
ori	$14,$14,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$14,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$14,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$14,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$11,-8
sll	$7,$7,$11
or	$11,$7,$14
andi	$14,$11,0x1
subu	$7,$0,$14
.set	noreorder
.set	nomacro
b	$L791
sra	$11,$11,1
.set	macro
.set	reorder

$L1046:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$2,$3
sll	$2,$2,2
addu	$2,$5,$2
lh	$3,0($2)
lh	$2,2($2)
.set	noreorder
.set	nomacro
b	$L1047
sw	$3,48($sp)
.set	macro
.set	reorder

$L1306:
lbu	$4,11242($23)
sll	$4,$2,$4
li	$2,-2004353024			# 0xffffffff88880000
sra	$7,$4,31
ori	$2,$2,0x8889
mult	$4,$2
mfhi	$2
addu	$2,$2,$4
sra	$2,$2,3
subu	$2,$2,$7
sll	$7,$2,4
subu	$2,$7,$2
.set	noreorder
.set	nomacro
b	$L918
subu	$2,$4,$2
.set	macro
.set	reorder

$L1065:
li	$7,1			# 0x1
beq	$6,$7,$L1345
lh	$7,0($9)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
lh	$7,0($22)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
lh	$7,0($21)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000001110000000011101111	#S32I2M XR3,$7
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000001110000000111101110	#S32M2I XR7, $7
# 0 "" 2
#NO_APP
lh	$9,2($9)
#APP
# 480 "vc1dec.c" 1
.word	0b01110000000010010000000001101111	#S32I2M XR1,$9
# 0 "" 2
#NO_APP
lh	$9,2($22)
#APP
# 481 "vc1dec.c" 1
.word	0b01110000000010010000000010101111	#S32I2M XR2,$9
# 0 "" 2
#NO_APP
lh	$9,2($21)
#APP
# 482 "vc1dec.c" 1
.word	0b01110000000010010000000011101111	#S32I2M XR3,$9
# 0 "" 2
# 484 "vc1dec.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 485 "vc1dec.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 486 "vc1dec.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 487 "vc1dec.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 489 "vc1dec.c" 1
.word	0b01110000000010010000000111101110	#S32M2I XR7, $9
# 0 "" 2
#NO_APP
$L886:
lw	$21,11168($23)
slt	$21,$21,3
.set	noreorder
.set	nomacro
beq	$21,$0,$L1369
lw	$21,48($sp)
.set	macro
.set	reorder

sll	$25,$6,5
sll	$21,$13,5
addiu	$25,$25,-4
addu	$22,$21,$7
sll	$fp,$10,5
slt	$24,$22,-28
sw	$25,64($sp)
addiu	$25,$fp,-4
sw	$24,84($sp)
sw	$25,100($sp)
lw	$25,84($sp)
.set	noreorder
.set	nomacro
beq	$25,$0,$L888
sll	$24,$11,5
.set	macro
.set	reorder

li	$7,-28			# 0xffffffffffffffe4
li	$22,-28			# 0xffffffffffffffe4
.set	noreorder
.set	nomacro
b	$L888
subu	$7,$7,$21
.set	macro
.set	reorder

$L1344:
lw	$21,48($sp)
$L1369:
li	$fp,-60			# 0xffffffffffffffc4
addu	$22,$21,$7
addu	$21,$12,$9
slt	$25,$22,-60
slt	$24,$21,-60
movn	$22,$fp,$25
movn	$21,$fp,$24
movn	$7,$15,$25
movn	$9,$16,$24
lw	$25,56($sp)
lw	$fp,68($sp)
slt	$22,$25,$22
slt	$21,$fp,$21
movn	$7,$19,$22
.set	noreorder
.set	nomacro
b	$L891
movn	$9,$20,$21
.set	macro
.set	reorder

$L883:
beq	$7,$0,$L1065
lh	$7,0($21)
.set	noreorder
.set	nomacro
b	$L886
lh	$9,2($21)
.set	macro
.set	reorder

$L1338:
lh	$2,0($24)
.set	noreorder
.set	nomacro
b	$L958
lh	$4,2($24)
.set	macro
.set	reorder

$L913:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,7			# 0x7
beq	$2,$4,$L916
lbu	$16,11228($23)
.set	noreorder
.set	nomacro
b	$L927
addu	$16,$16,$2
.set	macro
.set	reorder

$L1033:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$16,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1352
lw	$2,2172($23)
.set	macro
.set	reorder

$L938:
.set	noreorder
.set	nomacro
beq	$7,$0,$L1370
lw	$24,76($sp)
.set	macro
.set	reorder

lh	$7,0($11)
.set	noreorder
.set	nomacro
b	$L941
lh	$11,2($11)
.set	macro
.set	reorder

$L1337:
lh	$20,0($21)
.set	noreorder
.set	nomacro
b	$L999
lh	$21,2($21)
.set	macro
.set	reorder

$L1341:
lw	$25,60($sp)
$L1365:
lw	$9,80($sp)
addu	$25,$25,$7
addu	$9,$9,$11
slt	$24,$25,-60
sw	$25,84($sp)
slt	$25,$9,-60
sw	$9,100($sp)
li	$9,-60			# 0xffffffffffffffc4
sw	$25,96($sp)
lw	$25,84($sp)
sw	$24,72($sp)
movz	$9,$25,$24
lw	$25,100($sp)
lw	$24,96($sp)
sw	$9,84($sp)
li	$9,-60			# 0xffffffffffffffc4
movz	$9,$25,$24
lw	$25,84($sp)
lw	$24,88($sp)
sw	$9,100($sp)
lw	$9,72($sp)
slt	$24,$24,$25
lw	$25,92($sp)
sw	$24,72($sp)
movn	$7,$14,$9
lw	$9,96($sp)
movn	$11,$15,$9
lw	$9,100($sp)
slt	$24,$25,$9
lw	$25,72($sp)
movn	$11,$3,$24
.set	noreorder
.set	nomacro
b	$L946
movn	$7,$21,$25
.set	macro
.set	reorder

$L1327:
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$13,$4,-1
andi	$13,$13,0x7
sll	$13,$13,4
ori	$13,$13,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$4,-8
sll	$4,$7,$4
or	$4,$4,$13
andi	$21,$4,0x1
subu	$3,$0,$21
.set	noreorder
.set	nomacro
b	$L981
sra	$7,$4,1
.set	macro
.set	reorder

$L1330:
li	$7,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$12,$11,-1
andi	$12,$12,0x7
sll	$12,$12,4
ori	$12,$12,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$12,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$12,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$11,-8
sll	$7,$7,$11
or	$7,$7,$12
.set	noreorder
.set	nomacro
b	$L1331
sw	$7,32($sp)
.set	macro
.set	reorder

$L1329:
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$12,$21,1
# 0 "" 2
#NO_APP
addiu	$7,$5,-1
andi	$7,$7,0x7
sll	$7,$7,4
ori	$7,$7,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$5,-8
sll	$5,$12,$5
.set	noreorder
.set	nomacro
b	$L782
or	$5,$5,$13
.set	macro
.set	reorder

$L1328:
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$15,$11,-1
andi	$15,$15,0x7
sll	$15,$15,4
ori	$15,$15,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$15,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$15,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$15,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$11,-8
sll	$7,$7,$11
or	$7,$7,$15
andi	$11,$7,0x1
subu	$15,$0,$11
.set	noreorder
.set	nomacro
b	$L983
sra	$7,$7,1
.set	macro
.set	reorder

$L1342:
lh	$7,0($24)
.set	noreorder
.set	nomacro
b	$L941
lh	$11,2($24)
.set	macro
.set	reorder

$L1345:
lh	$7,0($9)
.set	noreorder
.set	nomacro
b	$L886
lh	$9,2($9)
.set	macro
.set	reorder

$L1343:
lh	$3,0($24)
.set	noreorder
.set	nomacro
b	$L903
lh	$4,2($24)
.set	macro
.set	reorder

$L916:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$16,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1353
lw	$2,2172($23)
.set	macro
.set	reorder

$L1299:
move	$12,$5
move	$14,$0
addiu	$13,$13,%lo(offset_table)
addu	$18,$13,$18
lw	$5,0($18)
.set	noreorder
.set	nomacro
b	$L790
sw	$5,40($sp)
.set	macro
.set	reorder

$L1340:
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$11,$4,-1
andi	$11,$11,0x7
sll	$11,$11,4
ori	$11,$11,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$11,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$4,-8
sll	$4,$7,$4
or	$4,$4,$11
.set	noreorder
.set	nomacro
b	$L971
sw	$4,32($sp)
.set	macro
.set	reorder

$L1339:
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$13,$4,-1
andi	$13,$13,0x7
sll	$13,$13,4
ori	$13,$13,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$4,-8
sll	$4,$7,$4
.set	noreorder
.set	nomacro
b	$L974
or	$7,$4,$13
.set	macro
.set	reorder

.end	vc1_decode_b_mb_vlc
.size	vc1_decode_b_mb_vlc, .-vc1_decode_b_mb_vlc
.section	.text.vc1_pred_mv,"ax",@progbits
.align	2
.align	5
.globl	vc1_pred_mv
.set	nomips16
.set	nomicromips
.ent	vc1_pred_mv
.type	vc1_pred_mv, @function
vc1_pred_mv:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$2,$5,2002
lw	$8,2184($4)
addiu	$sp,$sp,-16
lw	$11,8004($4)
sll	$2,$2,2
lw	$14,10044($4)
lw	$13,172($4)
addu	$2,$4,$2
lw	$10,32($sp)
sw	$18,12($sp)
sw	$17,8($sp)
lw	$3,4($2)
lw	$2,2188($4)
sw	$16,4($sp)
sll	$9,$3,2
.set	noreorder
.set	nomacro
beq	$11,$0,$L1372
addu	$8,$8,$9
.set	macro
.set	reorder

addu	$2,$2,$9
#APP
# 684 "vc1dec.c" 1
.word	0b01110001000000000000000000010001	#S32STD XR0,$8,0
# 0 "" 2
# 685 "vc1dec.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$10,$0,$L1430
lw	$18,12($sp)
.set	macro
.set	reorder

#APP
# 687 "vc1dec.c" 1
.word	0b01110001000000000000010000010001	#S32STD XR0,$8,4
# 0 "" 2
# 688 "vc1dec.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
#NO_APP
sll	$13,$13,2
addu	$8,$8,$13
#APP
# 689 "vc1dec.c" 1
.word	0b01110001000000000000000000010001	#S32STD XR0,$8,0
# 0 "" 2
# 690 "vc1dec.c" 1
.word	0b01110001000000000000010000010001	#S32STD XR0,$8,4
# 0 "" 2
#NO_APP
addu	$2,$2,$13
#APP
# 691 "vc1dec.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 692 "vc1dec.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
#NO_APP
lw	$18,12($sp)
$L1430:
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,16
.set	macro
.set	reorder

$L1372:
sll	$11,$13,2
addiu	$15,$8,-4
.set	noreorder
.set	nomacro
beq	$10,$0,$L1375
subu	$2,$8,$11
.set	macro
.set	reorder

lw	$25,160($4)
li	$16,-1			# 0xffffffffffffffff
lw	$24,7992($4)
addiu	$9,$25,-1
xor	$12,$24,$9
li	$9,2			# 0x2
movz	$9,$16,$12
$L1377:
subu	$9,$9,$13
lw	$12,10296($4)
sll	$9,$9,2
.set	noreorder
.set	nomacro
beq	$12,$0,$L1386
addu	$9,$8,$9
.set	macro
.set	reorder

addiu	$16,$5,-2
sltu	$16,$16,2
.set	noreorder
.set	nomacro
bne	$16,$0,$L1431
li	$16,1			# 0x1
.set	macro
.set	reorder

$L1387:
bne	$24,$0,$L1390
li	$9,1			# 0x1
beq	$5,$9,$L1390
#APP
# 736 "vc1dec.c" 1
.word	0b01110000000110000000001110101111	#S32I2M XR14,$24
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1427
li	$9,-28			# 0xffffffffffffffe4
.set	macro
.set	reorder

$L1375:
li	$9,1			# 0x1
.set	noreorder
.set	nomacro
beq	$5,$9,$L1379
slt	$9,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$9,$0,$L1422
li	$9,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$5,$9,$L1383
li	$9,1			# 0x1
.set	macro
.set	reorder

li	$9,3			# 0x3
bne	$5,$9,$L1378
li	$9,-1			# 0xffffffffffffffff
$L1383:
subu	$9,$9,$13
lw	$12,10296($4)
lw	$24,7992($4)
sll	$9,$9,2
lw	$25,160($4)
addu	$9,$8,$9
$L1386:
li	$16,1			# 0x1
$L1431:
beq	$25,$16,$L1423
#APP
# 724 "vc1dec.c" 1
.word	0b01110000010000000000001110010000	#S32LDD XR14,$2,0
# 0 "" 2
# 725 "vc1dec.c" 1
.word	0b01110001001000000000001101010000	#S32LDD XR13,$9,0
# 0 "" 2
# 726 "vc1dec.c" 1
.word	0b01110001111000000000001100010000	#S32LDD XR12,$15,0
# 0 "" 2
# 728 "vc1dec.c" 1
.word	0b01110000000010110111100100000011	#D16MAX XR4,XR14,XR13
# 0 "" 2
# 729 "vc1dec.c" 1
.word	0b01110000000011110111100101000011	#D16MIN XR5,XR14,XR13
# 0 "" 2
# 730 "vc1dec.c" 1
.word	0b01110000000011110001000100000011	#D16MIN XR4,XR4,XR12
# 0 "" 2
# 731 "vc1dec.c" 1
.word	0b01110000000010010101001110000011	#D16MAX XR14,XR4,XR5
# 0 "" 2
#NO_APP
li	$9,-28			# 0xffffffffffffffe4
$L1427:
li	$17,-60			# 0xffffffffffffffc4
xori	$16,$5,0x1
movz	$17,$9,$10
sltu	$16,$16,1
move	$9,$0
li	$18,32			# 0x20
#APP
# 743 "vc1dec.c" 1
movn	$9,$18,$16	#i_movn
# 0 "" 2
#NO_APP
xori	$16,$5,0x2
li	$18,2097152			# 0x200000
sltu	$16,$16,1
#APP
# 744 "vc1dec.c" 1
movn	$9,$18,$16	#i_movn
# 0 "" 2
#NO_APP
xori	$16,$5,0x3
addiu	$18,$18,32
sltu	$16,$16,1
#APP
# 745 "vc1dec.c" 1
movn	$9,$18,$16	#i_movn
# 0 "" 2
#NO_APP
li	$16,4			# 0x4
#APP
# 746 "vc1dec.c" 1
.word	0b01110000000100000000001111101111	#S32I2M XR15,$16
# 0 "" 2
# 747 "vc1dec.c" 1
.word	0b01110000000110000000001101101111	#S32I2M XR13,$24
# 0 "" 2
#NO_APP
lw	$16,7996($4)
#APP
# 748 "vc1dec.c" 1
.word	0b01110000000100000000001100101111	#S32I2M XR12,$16
# 0 "" 2
# 749 "vc1dec.c" 1
.word	0b01110011001111111111110000111101	#S32SFL XR0,XR15,XR15,XR15,PTN3
# 0 "" 2
# 750 "vc1dec.c" 1
.word	0b01110011001101110111000000111101	#S32SFL XR0,XR12,XR13,XR13,PTN3
# 0 "" 2
# 752 "vc1dec.c" 1
.word	0b01110000000110010000001010101111	#S32I2M XR10,$25
# 0 "" 2
#NO_APP
lw	$4,164($4)
#APP
# 753 "vc1dec.c" 1
.word	0b01110000000001000000001001101111	#S32I2M XR9,$4
# 0 "" 2
# 754 "vc1dec.c" 1
.word	0b01110011001010101010010000111101	#S32SFL XR0,XR9,XR10,XR10,PTN3
# 0 "" 2
# 755 "vc1dec.c" 1
.word	0b01110001101101110110101010110100	#Q16SLL XR10,XR10,XR13,XR13,6
# 0 "" 2
# 757 "vc1dec.c" 1
.word	0b01110000000010010000001011101111	#S32I2M XR11,$9
# 0 "" 2
# 758 "vc1dec.c" 1
.word	0b01110000000000101111011101001110	#Q16ADD XR13,XR13,XR11,XR0,AA,WW
# 0 "" 2
# 759 "vc1dec.c" 1
.word	0b01110011000000111110101100001110	#Q16ADD XR12,XR10,XR15,XR0,SS,WW
# 0 "" 2
# 761 "vc1dec.c" 1
.word	0b01110000000100010000001011101111	#S32I2M XR11,$17
# 0 "" 2
# 762 "vc1dec.c" 1
.word	0b01110011001011101110110000111101	#S32SFL XR0,XR11,XR11,XR11,PTN3
# 0 "" 2
# 763 "vc1dec.c" 1
.word	0b01110000000000111011011010001110	#Q16ADD XR10,XR13,XR14,XR0,AA,WW
# 0 "" 2
# 764 "vc1dec.c" 1
.word	0b01110000000001101110101001000110	#D16SLT XR9,XR10,XR11
# 0 "" 2
# 765 "vc1dec.c" 1
.word	0b01110011000000110110111000001110	#Q16ADD XR8,XR11,XR13,XR0,SS,WW
# 0 "" 2
# 766 "vc1dec.c" 1
.word	0b01110011000000110111000111001110	#Q16ADD XR7,XR12,XR13,XR0,SS,WW
# 0 "" 2
# 767 "vc1dec.c" 1
.word	0b01110000000011100010011110111001	#D16MOVN XR14,XR9,XR8
# 0 "" 2
# 768 "vc1dec.c" 1
.word	0b01110000000000111011011010001110	#Q16ADD XR10,XR13,XR14,XR0,AA,WW
# 0 "" 2
# 769 "vc1dec.c" 1
.word	0b01110000000001101011000110000110	#D16SLT XR6,XR12,XR10
# 0 "" 2
# 770 "vc1dec.c" 1
.word	0b01110000000011011101101110111001	#D16MOVN XR14,XR6,XR7
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$12,$0,$L1393
addiu	$4,$5,-2
.set	macro
.set	reorder

sltu	$4,$4,2
bne	$4,$0,$L1393
$L1394:
li	$4,1			# 0x1
$L1428:
subu	$2,$4,$14
#APP
# 813 "vc1dec.c" 1
.word	0b01110000000010010000001110101110	#S32M2I XR14, $9
# 0 "" 2
# 814 "vc1dec.c" 1
.word	0b01110000000001000000001110101110	#S32M2I XR14, $4
# 0 "" 2
#NO_APP
sll	$7,$7,$2
lhu	$14,36($sp)
sll	$6,$6,$2
lw	$2,%got(dMB)($28)
lhu	$13,40($sp)
sll	$9,$9,16
addu	$6,$14,$6
sra	$9,$9,16
lw	$12,0($2)
addu	$3,$13,$7
sll	$15,$14,1
sra	$7,$4,16
sll	$2,$13,1
addiu	$4,$15,-1
addu	$7,$3,$7
addu	$6,$6,$9
addiu	$2,$2,-1
and	$3,$6,$4
and	$2,$7,$2
subu	$3,$3,$14
subu	$2,$2,$13
addiu	$4,$5,10
sll	$3,$3,16
sll	$2,$2,16
sll	$5,$5,2
sll	$4,$4,2
sra	$3,$3,16
sra	$2,$2,16
addu	$5,$12,$5
addu	$12,$12,$4
sh	$3,0($8)
sh	$3,4($12)
sh	$2,2($8)
sh	$2,46($5)
.set	noreorder
.set	nomacro
beq	$10,$0,$L1430
lw	$18,12($sp)
.set	macro
.set	reorder

#APP
# 820 "vc1dec.c" 1
.word	0b01110001000000000000001110010000	#S32LDD XR14,$8,0
# 0 "" 2
# 821 "vc1dec.c" 1
.word	0b01110001000000000000011110010001	#S32STD XR14,$8,4
# 0 "" 2
#NO_APP
addu	$8,$8,$11
#APP
# 822 "vc1dec.c" 1
.word	0b01110001000000000000001110010001	#S32STD XR14,$8,0
# 0 "" 2
# 823 "vc1dec.c" 1
.word	0b01110001000000000000011110010001	#S32STD XR14,$8,4
# 0 "" 2
#NO_APP
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,16
.set	macro
.set	reorder

$L1393:
.set	noreorder
.set	nomacro
beq	$24,$0,$L1424
li	$4,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

lw	$4,44($sp)
subu	$13,$3,$13
$L1432:
addu	$13,$4,$13
lbu	$4,0($13)
bne	$4,$0,$L1425
#APP
# 779 "vc1dec.c" 1
.word	0b01110000010000000000001111010000	#S32LDD XR15,$2,0
# 0 "" 2
# 780 "vc1dec.c" 1
.word	0b01110011000000111111101101001110	#Q16ADD XR13,XR14,XR15,XR0,SS,WW
# 0 "" 2
# 781 "vc1dec.c" 1
.word	0b01110000000010110111011011000111	#D16CPS XR11,XR13,XR13
# 0 "" 2
# 782 "vc1dec.c" 1
.word	0b01110000110000101110111010001110	#Q16ADD XR10,XR11,XR11,XR0,AA,XW
# 0 "" 2
#NO_APP
$L1397:
#APP
# 785 "vc1dec.c" 1
.word	0b01110000000001000000001010101110	#S32M2I XR10, $4
# 0 "" 2
#NO_APP
sra	$4,$4,16
slt	$4,$4,33
.set	noreorder
.set	nomacro
beq	$4,$0,$L1421
lw	$4,44($sp)
.set	macro
.set	reorder

addu	$3,$4,$3
lbu	$3,-1($3)
beq	$3,$0,$L1400
#APP
# 794 "vc1dec.c" 1
.word	0b01110000000010111011101011000111	#D16CPS XR11,XR14,XR14
# 0 "" 2
# 795 "vc1dec.c" 1
.word	0b01110000110000101110111010001110	#Q16ADD XR10,XR11,XR11,XR0,AA,XW
# 0 "" 2
#NO_APP
$L1401:
#APP
# 803 "vc1dec.c" 1
.word	0b01110000000000110000001010101110	#S32M2I XR10, $3
# 0 "" 2
#NO_APP
sra	$3,$3,16
slt	$3,$3,33
.set	noreorder
.set	nomacro
bne	$3,$0,$L1428
li	$4,1			# 0x1
.set	macro
.set	reorder

$L1421:
li	$3,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
beq	$3,$0,$L1402
#APP
# 806 "vc1dec.c" 1
.word	0b01110000010000000000001110010000	#S32LDD XR14,$2,0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1428
li	$4,1			# 0x1
.set	macro
.set	reorder

$L1424:
li	$9,1			# 0x1
and	$4,$5,$4
.set	noreorder
.set	nomacro
bne	$4,$9,$L1394
lw	$4,44($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1432
subu	$13,$3,$13
.set	macro
.set	reorder

$L1425:
#APP
# 775 "vc1dec.c" 1
.word	0b01110000000010111011101011000111	#D16CPS XR11,XR14,XR14
# 0 "" 2
# 776 "vc1dec.c" 1
.word	0b01110000110000101110111010001110	#Q16ADD XR10,XR11,XR11,XR0,AA,XW
# 0 "" 2
#NO_APP
b	$L1397
$L1422:
bne	$5,$0,$L1378
lw	$24,7992($4)
blez	$24,$L1426
lw	$12,10296($4)
.set	noreorder
.set	nomacro
bne	$12,$0,$L1390
lw	$25,160($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1404
li	$9,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1390:
#APP
# 734 "vc1dec.c" 1
.word	0b01110001111000000000001110010000	#S32LDD XR14,$15,0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1427
li	$9,-28			# 0xffffffffffffffe4
.set	macro
.set	reorder

$L1423:
#APP
# 722 "vc1dec.c" 1
.word	0b01110000010000000000001110010000	#S32LDD XR14,$2,0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1427
li	$9,-28			# 0xffffffffffffffe4
.set	macro
.set	reorder

$L1402:
#APP
# 808 "vc1dec.c" 1
.word	0b01110001111000000000001110010000	#S32LDD XR14,$15,0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1428
li	$4,1			# 0x1
.set	macro
.set	reorder

$L1400:
#APP
# 798 "vc1dec.c" 1
.word	0b01110001111000000000001111010000	#S32LDD XR15,$15,0
# 0 "" 2
# 799 "vc1dec.c" 1
.word	0b01110011000000111111101101001110	#Q16ADD XR13,XR14,XR15,XR0,SS,WW
# 0 "" 2
# 800 "vc1dec.c" 1
.word	0b01110000000010110111011011000111	#D16CPS XR11,XR13,XR13
# 0 "" 2
# 801 "vc1dec.c" 1
.word	0b01110000110000101110111010001110	#Q16ADD XR10,XR11,XR11,XR0,AA,XW
# 0 "" 2
#NO_APP
b	$L1401
$L1379:
lw	$25,160($4)
li	$12,-1			# 0xffffffffffffffff
lw	$24,7992($4)
addiu	$9,$25,-1
xor	$9,$24,$9
movn	$12,$5,$9
move	$9,$12
$L1385:
lw	$12,10296($4)
bne	$12,$0,$L1387
$L1404:
subu	$9,$9,$13
move	$12,$0
sll	$9,$9,2
.set	noreorder
.set	nomacro
b	$L1386
addu	$9,$8,$9
.set	macro
.set	reorder

$L1378:
lw	$24,7992($4)
move	$9,$0
.set	noreorder
.set	nomacro
b	$L1377
lw	$25,160($4)
.set	macro
.set	reorder

$L1426:
lw	$25,160($4)
.set	noreorder
.set	nomacro
b	$L1385
li	$9,1			# 0x1
.set	macro
.set	reorder

.end	vc1_pred_mv
.size	vc1_pred_mv, .-vc1_pred_mv
.section	.text.vc1_decode_p_mb_vlc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_decode_p_mb_vlc
.type	vc1_decode_p_mb_vlc, @function
vc1_decode_p_mb_vlc:
.frame	$sp,136,$31		# vars= 56, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-136
sw	$19,108($sp)
sw	$16,96($sp)
move	$16,$4
lw	$19,7996($4)
lw	$4,168($4)
lw	$5,11252($16)
lw	$3,7992($16)
mul	$6,$19,$4
lw	$2,11328($16)
.cprestore	32
sw	$31,132($sp)
sw	$fp,128($sp)
sw	$23,124($sp)
sw	$22,120($sp)
sw	$21,116($sp)
sw	$20,112($sp)
sw	$18,104($sp)
sw	$17,100($sp)
sw	$5,72($sp)
addu	$5,$6,$3
lbu	$3,11228($16)
sw	$5,64($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1434
sw	$3,56($sp)
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
andi	$5,$5,0x00ff
$L1435:
lw	$2,11336($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1436
lw	$18,%got(dMB)($28)
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
lw	$18,%got(dMB)($28)
andi	$4,$4,0x00ff
lw	$2,0($18)
sb	$5,24($2)
sb	$4,32($2)
.set	noreorder
.set	nomacro
bne	$4,$0,$L1438
addiu	$2,$2,56
.set	macro
.set	reorder

$L1772:
#APP
# 3224 "vc1dec.c" 1
.word	0b01110000000001000000000001101111	#S32I2M XR1,$4
# 0 "" 2
#NO_APP
li	$4,24			# 0x18
$L1439:
#APP
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3224 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
#NO_APP
addiu	$4,$4,-1
bne	$4,$0,$L1439
beq	$5,$0,$L1719
lw	$2,11312($16)
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1512
lh	$21,0($4)
.set	macro
.set	reorder

sw	$21,68($sp)
$L1513:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addiu	$22,$16,8012
addiu	$23,$sp,40
addiu	$20,$sp,48
move	$fp,$0
move	$21,$0
move	$17,$0
move	$10,$20
move	$9,$23
move	$19,$22
$L1534:
lw	$5,0($19)
li	$2,5			# 0x5
lw	$6,2728($16)
slt	$4,$17,4
lw	$3,68($sp)
subu	$2,$2,$17
sll	$5,$5,1
sra	$2,$3,$2
addu	$5,$6,$5
andi	$2,$2,0x1
sh	$0,0($5)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1514
sw	$0,8004($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L1613
move	$15,$0
.set	macro
.set	reorder

lw	$2,10272($16)
lw	$4,%got(ff_vc1_mv_diff_vlc)($28)
sll	$2,$2,4
addu	$2,$4,$2
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1720
lh	$4,0($4)
.set	macro
.set	reorder

$L1517:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$4,1
slt	$5,$2,37
bne	$5,$0,$L1614
addiu	$2,$4,-36
.set	noreorder
.set	nomacro
beq	$2,$0,$L1615
li	$15,1			# 0x1
.set	macro
.set	reorder

$L1739:
li	$4,35			# 0x23
.set	noreorder
.set	nomacro
beq	$2,$4,$L1721
li	$4,36			# 0x24
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$4,$L1722
li	$5,715784192			# 0x2aaa0000
.set	macro
.set	reorder

lw	$24,10044($16)
sra	$25,$2,31
ori	$5,$5,0xaaab
mult	$2,$5
mfhi	$5
subu	$5,$5,$25
sll	$6,$5,1
sll	$4,$5,3
subu	$4,$4,$6
subu	$4,$2,$4
.set	noreorder
.set	nomacro
beq	$24,$0,$L1723
sll	$6,$4,2
.set	macro
.set	reorder

lw	$4,%got(size_table)($28)
addiu	$4,$4,%lo(size_table)
addu	$7,$4,$6
lw	$7,0($7)
.set	noreorder
.set	nomacro
blez	$7,$L1724
lw	$3,%got(offset_table)($28)
.set	macro
.set	reorder

$L1587:
slt	$5,$7,9
beq	$5,$0,$L1725
addiu	$5,$7,-1
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
$L1527:
lw	$3,%got(offset_table)($28)
sra	$7,$5,1
andi	$5,$5,0x1
addiu	$3,$3,%lo(offset_table)
addu	$6,$3,$6
lw	$31,0($6)
li	$6,715784192			# 0x2aaa0000
ori	$6,$6,0xaaab
mult	$2,$6
addu	$7,$7,$31
lw	$31,%got(offset_table)($28)
subu	$2,$0,$5
mfhi	$3
xor	$7,$7,$2
addu	$6,$5,$7
addiu	$31,$31,%lo(offset_table)
.set	noreorder
.set	nomacro
beq	$24,$0,$L1586
subu	$5,$3,$25
.set	macro
.set	reorder

move	$7,$0
$L1528:
sll	$5,$5,2
addu	$4,$4,$5
lw	$2,0($4)
subu	$2,$2,$7
.set	noreorder
.set	nomacro
blez	$2,$L1617
move	$4,$0
.set	macro
.set	reorder

slt	$4,$2,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L1726
li	$4,125			# 0x7d
.set	macro
.set	reorder

addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
andi	$7,$2,0x1
subu	$24,$0,$7
sra	$4,$2,1
$L1529:
addu	$5,$31,$5
lw	$5,0($5)
addu	$4,$4,$5
xor	$4,$4,$24
.set	noreorder
.set	nomacro
b	$L1515
addu	$7,$4,$7
.set	macro
.set	reorder

$L1436:
lw	$2,2836($16)
lw	$3,64($sp)
addu	$2,$2,$3
lbu	$4,0($2)
lw	$2,0($18)
sb	$5,24($2)
sb	$4,32($2)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1772
addiu	$2,$2,56
.set	macro
.set	reorder

$L1438:
.set	noreorder
.set	nomacro
bne	$5,$0,$L1574
addiu	$4,$16,8012
.set	macro
.set	reorder

sw	$0,8004($16)
move	$2,$0
li	$8,6			# 0x6
$L1575:
lw	$6,0($4)
addiu	$4,$4,4
lw	$5,11284($16)
addu	$5,$5,$6
sb	$0,0($5)
lw	$7,-4($4)
lw	$6,2728($16)
lw	$5,0($18)
sll	$7,$7,1
addu	$5,$5,$2
addu	$6,$6,$7
addiu	$2,$2,1
sh	$0,0($6)
sb	$0,6($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L1575
move	$5,$0
.set	macro
.set	reorder

lw	$2,64($sp)
lw	$3,2192($16)
move	$6,$0
lw	$25,%call16(vc1_pred_mv)($28)
move	$7,$0
sll	$4,$2,2
lw	$2,2172($16)
addu	$3,$3,$4
lw	$4,64($sp)
addu	$19,$2,$4
li	$2,2048			# 0x800
move	$4,$16
sw	$2,0($3)
li	$2,1			# 0x1
sb	$0,0($19)
sw	$2,16($sp)
lw	$8,11220($16)
lw	$3,11224($16)
lw	$2,11284($16)
sw	$8,20($sp)
sw	$3,24($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_pred_mv
1:	jalr	$25
sw	$2,28($sp)
.set	macro
.set	reorder

$L1650:
lw	$31,132($sp)
$L1774:
move	$2,$0
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

$L1434:
lw	$2,11320($16)
lw	$3,64($sp)
addu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L1435
lbu	$5,0($2)
.set	macro
.set	reorder

$L1719:
lw	$2,10272($16)
sll	$4,$2,4
lw	$2,%got(ff_vc1_mv_diff_vlc)($28)
addu	$2,$2,$4
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1728
lh	$4,0($4)
.set	macro
.set	reorder

$L1442:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addiu	$6,$4,1
slt	$2,$6,37
.set	noreorder
.set	nomacro
bne	$2,$0,$L1773
move	$20,$0
.set	macro
.set	reorder

addiu	$6,$4,-36
li	$20,1			# 0x1
$L1443:
$L1773:
.set	noreorder
.set	nomacro
beq	$6,$0,$L1607
sw	$0,8004($16)
.set	macro
.set	reorder

li	$2,35			# 0x23
.set	noreorder
.set	nomacro
beq	$6,$2,$L1729
li	$2,36			# 0x24
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$2,$L1730
li	$4,715784192			# 0x2aaa0000
.set	macro
.set	reorder

lw	$7,10044($16)
sra	$8,$6,31
ori	$4,$4,0xaaab
mult	$6,$4
mfhi	$4
subu	$2,$4,$8
sll	$4,$2,3
sll	$5,$2,1
subu	$5,$4,$5
lw	$4,%got(size_table)($28)
subu	$5,$6,$5
.set	noreorder
.set	nomacro
bne	$7,$0,$L1451
sll	$10,$5,2
.set	macro
.set	reorder

addiu	$4,$4,%lo(size_table)
xori	$5,$5,0x5
sltu	$9,$5,1
addu	$5,$4,$10
lw	$5,0($5)
subu	$5,$5,$9
.set	noreorder
.set	nomacro
blez	$5,$L1452
lw	$31,%got(offset_table)($28)
.set	macro
.set	reorder

$L1580:
slt	$2,$5,9
.set	noreorder
.set	nomacro
beq	$2,$0,$L1731
li	$2,125			# 0x7d
.set	macro
.set	reorder

addiu	$5,$5,-1
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
$L1454:
lw	$31,%got(offset_table)($28)
li	$5,715784192			# 0x2aaa0000
sra	$11,$9,1
ori	$5,$5,0xaaab
mult	$6,$5
addiu	$31,$31,%lo(offset_table)
andi	$9,$9,0x1
addu	$10,$31,$10
mfhi	$2
subu	$6,$0,$9
lw	$5,0($10)
addu	$5,$11,$5
xor	$6,$5,$6
subu	$2,$2,$8
.set	noreorder
.set	nomacro
beq	$7,$0,$L1579
addu	$6,$9,$6
.set	macro
.set	reorder

move	$5,$0
$L1455:
sll	$2,$2,2
addu	$4,$4,$2
lw	$4,0($4)
subu	$5,$4,$5
.set	noreorder
.set	nomacro
blez	$5,$L1609
move	$4,$0
.set	macro
.set	reorder

slt	$4,$5,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L1732
li	$4,125			# 0x7d
.set	macro
.set	reorder

addiu	$4,$5,-1
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
andi	$5,$4,0x1
subu	$7,$0,$5
sra	$4,$4,1
$L1456:
addu	$2,$31,$2
lw	$2,0($2)
addu	$2,$4,$2
xor	$2,$2,$7
.set	noreorder
.set	nomacro
b	$L1444
addu	$7,$2,$5
.set	macro
.set	reorder

$L1613:
move	$7,$0
move	$6,$0
$L1515:
lw	$31,11220($16)
move	$4,$16
lw	$24,11224($16)
move	$5,$17
lw	$3,11284($16)
lw	$25,%call16(vc1_pred_mv)($28)
sw	$2,76($sp)
sw	$9,84($sp)
sw	$10,80($sp)
sw	$15,88($sp)
sw	$3,28($sp)
sw	$0,16($sp)
sw	$31,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_pred_mv
1:	jalr	$25
sw	$24,24($sp)
.set	macro
.set	reorder

lw	$15,88($sp)
lw	$9,84($sp)
lw	$6,8004($16)
lw	$4,0($18)
lw	$28,32($sp)
sb	$15,0($9)
andi	$5,$6,0x00ff
lw	$10,80($sp)
addu	$4,$4,$17
addu	$21,$21,$6
sb	$5,26($4)
sb	$5,0($10)
lw	$2,76($sp)
$L1514:
andi	$4,$17,0x4
.set	noreorder
.set	nomacro
bne	$4,$0,$L1531
slt	$4,$21,3
.set	macro
.set	reorder

lbu	$4,0($10)
lw	$2,11284($16)
$L1766:
lw	$5,0($19)
addu	$2,$2,$5
.set	noreorder
.set	nomacro
bne	$fp,$0,$L1618
sb	$4,0($2)
.set	macro
.set	reorder

lbu	$11,0($9)
sltu	$4,$4,1
and	$fp,$4,$11
$L1533:
lw	$2,0($18)
addiu	$19,$19,4
addiu	$9,$9,1
addiu	$10,$10,1
addu	$2,$2,$17
addiu	$17,$17,1
sb	$0,6($2)
li	$2,6			# 0x6
bne	$17,$2,$L1534
bne	$21,$0,$L1535
.set	noreorder
.set	nomacro
beq	$fp,$0,$L1774
lw	$31,132($sp)
.set	macro
.set	reorder

$L1535:
lbu	$2,11240($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1733
li	$4,3			# 0x3
.set	macro
.set	reorder

$L1536:
lw	$2,2172($16)
li	$4,1			# 0x1
lw	$3,64($sp)
li	$7,6			# 0x6
li	$10,-3			# 0xfffffffffffffffd
li	$9,1			# 0x1
addu	$19,$2,$3
lw	$3,56($sp)
move	$5,$22
sb	$3,0($19)
$L1552:
addu	$2,$20,$4
lbu	$2,-1($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1553
addiu	$6,$4,-1
.set	macro
.set	reorder

lw	$2,10296($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1554
addiu	$2,$4,-3
.set	macro
.set	reorder

sltu	$2,$2,2
bne	$2,$0,$L1554
$L1555:
lw	$2,7992($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1557
and	$6,$6,$10
.set	macro
.set	reorder

beq	$6,$9,$L1734
$L1553:
beq	$4,$7,$L1735
$L1559:
addiu	$4,$4,1
.set	noreorder
.set	nomacro
b	$L1552
addiu	$5,$5,4
.set	macro
.set	reorder

$L1735:
sw	$0,2824($16)
$L1578:
lbu	$2,11256($16)
beq	$2,$0,$L1736
$L1561:
lw	$5,0($18)
$L1768:
move	$fp,$0
lw	$3,56($sp)
li	$17,-3			# 0xfffffffffffffffd
lw	$21,%got(dma_len)($28)
lw	$19,%got(vc1_decode_intra_block)($28)
andi	$2,$3,0x3f
lw	$4,20($5)
sll	$2,$2,12
li	$3,1			# 0x1
or	$2,$2,$4
sw	$3,60($sp)
sw	$2,20($5)
.set	noreorder
.set	nomacro
b	$L1764
addu	$4,$20,$fp
.set	macro
.set	reorder

$L1737:
sw	$0,11276($16)
sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1565
sw	$0,11272($16)
.set	macro
.set	reorder

lw	$2,10296($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1775
li	$2,1			# 0x1
.set	macro
.set	reorder

$L1565:
lw	$2,0($22)
lw	$6,24($22)
lw	$4,11284($16)
subu	$2,$2,$6
addu	$2,$4,$2
lbu	$2,0($2)
sw	$2,11272($16)
li	$2,1			# 0x1
$L1775:
and	$4,$fp,$17
beq	$4,$2,$L1567
lw	$2,7992($16)
beq	$2,$0,$L1568
$L1567:
lw	$4,0($22)
lw	$2,11284($16)
addu	$2,$2,$4
lbu	$2,-1($2)
sw	$2,11276($16)
$L1568:
lw	$2,0($21)
andi	$11,$fp,0x4
lw	$10,11260($16)
addiu	$25,$19,%lo(vc1_decode_intra_block)
lw	$14,11264($16)
move	$4,$16
sll	$2,$2,4
lw	$3,56($sp)
lbu	$7,0($23)
move	$6,$fp
movn	$10,$14,$11
addu	$5,$5,$2
addiu	$5,$5,60
sw	$3,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_intra_block
1:	jalr	$25
sw	$10,20($sp)
.set	macro
.set	reorder

sll	$2,$fp,2
lw	$6,0($18)
li	$5,1			# 0x1
sll	$10,$fp,1
lw	$28,32($sp)
sll	$5,$5,$2
lw	$7,0($21)
addu	$2,$6,$fp
lbu	$4,6($2)
li	$2,3			# 0x3
sll	$2,$2,$10
lw	$10,12($6)
andi	$4,$4,0x00ff
addu	$4,$4,$7
or	$5,$5,$10
sw	$5,12($6)
lw	$5,16($6)
sw	$4,0($21)
or	$2,$2,$5
sw	$2,16($6)
$L1570:
addiu	$fp,$fp,1
$L1765:
li	$2,6			# 0x6
addiu	$23,$23,1
.set	noreorder
.set	nomacro
beq	$fp,$2,$L1650
addiu	$22,$22,4
.set	macro
.set	reorder

lw	$5,0($18)
addu	$4,$20,$fp
$L1764:
lw	$7,16($5)
addiu	$2,$fp,13
lbu	$6,0($4)
sltu	$4,$0,$6
sll	$2,$4,$2
sw	$6,8004($16)
or	$2,$2,$7
sw	$2,16($5)
.set	noreorder
.set	nomacro
bne	$6,$0,$L1737
addiu	$2,$fp,-2
.set	macro
.set	reorder

lbu	$2,0($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1738
move	$4,$16
.set	macro
.set	reorder

addu	$5,$5,$fp
sb	$0,6($5)
.set	noreorder
.set	nomacro
b	$L1765
addiu	$fp,$fp,1
.set	macro
.set	reorder

$L1618:
.set	noreorder
.set	nomacro
b	$L1533
li	$fp,1			# 0x1
.set	macro
.set	reorder

$L1531:
lw	$5,0($18)
xori	$4,$4,0x1
sb	$2,0($9)
addu	$2,$5,$17
sb	$4,0($10)
sb	$4,26($2)
.set	noreorder
.set	nomacro
b	$L1766
lw	$2,11284($16)
.set	macro
.set	reorder

$L1554:
lw	$2,0($5)
lw	$14,24($5)
lw	$8,11284($16)
subu	$2,$2,$14
addu	$2,$8,$2
lbu	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1555
li	$2,13			# 0xd
.set	macro
.set	reorder

$L1776:
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1578
sw	$2,2824($16)
.set	macro
.set	reorder

$L1557:
lw	$2,11284($16)
lw	$6,0($5)
addu	$2,$2,$6
lbu	$2,-1($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1776
li	$2,13			# 0xd
.set	macro
.set	reorder

bne	$4,$7,$L1559
.set	noreorder
.set	nomacro
b	$L1578
sw	$0,2824($16)
.set	macro
.set	reorder

$L1614:
.set	noreorder
.set	nomacro
bne	$2,$0,$L1739
move	$15,$0
.set	macro
.set	reorder

$L1615:
li	$2,1			# 0x1
move	$7,$0
.set	noreorder
.set	nomacro
b	$L1515
move	$6,$0
.set	macro
.set	reorder

$L1738:
lw	$2,0($21)
lw	$3,60($sp)
move	$6,$fp
lw	$7,72($sp)
sll	$2,$2,4
addu	$5,$5,$2
lw	$2,%got(vc1_decode_p_block_vlc.isra.7)($28)
sw	$3,16($sp)
addiu	$25,$2,%lo(vc1_decode_p_block_vlc.isra.7)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_p_block_vlc.isra.7
1:	jalr	$25
addiu	$5,$5,60
.set	macro
.set	reorder

lbu	$2,11256($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1619
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$3,72($sp)
li	$4,-1			# 0xffffffffffffffff
sw	$0,60($sp)
slt	$2,$3,8
movz	$4,$3,$2
.set	noreorder
.set	nomacro
b	$L1570
sw	$4,72($sp)
.set	macro
.set	reorder

$L1720:
li	$6,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$2,$4
sll	$2,$2,2
addu	$5,$5,$2
lh	$4,0($5)
.set	noreorder
.set	nomacro
b	$L1517
lh	$2,2($5)
.set	macro
.set	reorder

$L1607:
move	$7,$0
move	$6,$0
$L1444:
lw	$3,64($sp)
$L1771:
lw	$2,2192($16)
sll	$4,$3,2
addu	$2,$2,$4
li	$4,8			# 0x8
sw	$4,0($2)
$L1450:
lw	$2,11284($16)
li	$17,1			# 0x1
lw	$9,11220($16)
move	$5,$0
lw	$8,11224($16)
move	$4,$16
lw	$25,%call16(vc1_pred_mv)($28)
sw	$2,28($sp)
sw	$17,16($sp)
sw	$9,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_pred_mv
1:	jalr	$25
sw	$8,24($sp)
.set	macro
.set	reorder

lw	$2,8004($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1458
lw	$28,32($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$20,$0,$L1740
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sw	$2,2824($16)
$L1477:
lw	$2,11312($16)
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1741
lh	$17,0($4)
.set	macro
.set	reorder

$L1480:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lbu	$2,11240($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1476
li	$2,3			# 0x3
.set	macro
.set	reorder

lbu	$4,11241($16)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1742
li	$5,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$5,$L1743
li	$5,1			# 0x1
.set	macro
.set	reorder

beq	$4,$5,$L1744
bne	$4,$0,$L1476
lw	$5,7992($16)
.set	noreorder
.set	nomacro
bne	$5,$0,$L1745
li	$2,8			# 0x8
.set	macro
.set	reorder

lbu	$3,11229($16)
li	$4,4			# 0x4
li	$5,2			# 0x2
sw	$3,56($sp)
$L1492:
beq	$5,$0,$L1494
lw	$5,10296($16)
bne	$5,$0,$L1593
$L1494:
beq	$4,$0,$L1495
lw	$4,160($16)
lw	$5,7992($16)
addiu	$4,$4,-1
beq	$5,$4,$L1601
$L1495:
beq	$2,$0,$L1476
$L1600:
lw	$2,164($16)
lw	$4,7996($16)
addiu	$2,$2,-1
bne	$4,$2,$L1476
$L1709:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1476
sw	$3,56($sp)
.set	macro
.set	reorder

$L1458:
bne	$20,$0,$L1477
lbu	$3,11228($16)
move	$17,$0
sw	$3,56($sp)
$L1476:
lw	$2,2172($16)
lw	$3,64($sp)
addu	$19,$2,$3
lw	$3,56($sp)
sb	$3,0($19)
lbu	$2,11256($16)
bne	$2,$0,$L1496
$L1754:
lw	$2,8004($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1497
li	$5,458752			# 0x70000
.set	macro
.set	reorder

beq	$20,$0,$L1498
lw	$2,11316($16)
sll	$4,$2,4
lw	$2,%got(ff_vc1_ttmb_vlc)($28)
addu	$2,$2,$4
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1499
lh	$22,0($4)
.set	macro
.set	reorder

sw	$22,72($sp)
$L1500:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
$L1498:
lw	$2,0($18)
move	$5,$0
lw	$4,16($2)
.set	noreorder
.set	nomacro
b	$L1767
or	$5,$4,$5
.set	macro
.set	reorder

$L1736:
beq	$fp,$0,$L1561
lw	$2,11316($16)
sll	$4,$2,4
lw	$2,%got(ff_vc1_ttmb_vlc)($28)
addu	$2,$2,$4
lw	$5,4($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sll	$4,$4,2
addu	$4,$5,$4
lh	$2,2($4)
.set	noreorder
.set	nomacro
bltz	$2,$L1562
lh	$3,0($4)
.set	macro
.set	reorder

sw	$3,72($sp)
$L1563:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1768
lw	$5,0($18)
.set	macro
.set	reorder

$L1733:
lbu	$2,11241($16)
.set	noreorder
.set	nomacro
beq	$2,$4,$L1746
li	$5,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$5,$L1747
li	$5,1			# 0x1
.set	macro
.set	reorder

beq	$2,$5,$L1748
bne	$2,$0,$L1536
lw	$4,7992($16)
.set	noreorder
.set	nomacro
bne	$4,$0,$L1749
li	$2,8			# 0x8
.set	macro
.set	reorder

lbu	$3,11229($16)
li	$4,4			# 0x4
li	$5,2			# 0x2
sw	$3,56($sp)
$L1548:
beq	$5,$0,$L1550
lw	$5,10296($16)
bne	$5,$0,$L1595
$L1550:
beq	$4,$0,$L1551
lw	$4,160($16)
lw	$5,7992($16)
addiu	$4,$4,-1
beq	$5,$4,$L1604
$L1551:
beq	$2,$0,$L1536
$L1603:
lw	$2,164($16)
lw	$4,7996($16)
addiu	$2,$2,-1
bne	$4,$2,$L1536
$L1710:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1536
sw	$3,56($sp)
.set	macro
.set	reorder

$L1619:
.set	noreorder
.set	nomacro
b	$L1570
sw	$0,60($sp)
.set	macro
.set	reorder

$L1723:
lw	$3,%got(size_table)($28)
xori	$4,$4,0x5
sltu	$31,$4,1
addiu	$3,$3,%lo(size_table)
addu	$7,$3,$6
lw	$7,0($7)
subu	$7,$7,$31
.set	noreorder
.set	nomacro
bgtz	$7,$L1587
move	$4,$3
.set	macro
.set	reorder

lw	$2,%got(offset_table)($28)
addiu	$2,$2,%lo(offset_table)
addu	$6,$2,$6
move	$31,$2
lw	$6,0($6)
$L1586:
xori	$2,$5,0x5
.set	noreorder
.set	nomacro
b	$L1528
sltu	$7,$2,1
.set	macro
.set	reorder

$L1740:
lbu	$2,11240($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1777
li	$2,13			# 0xd
.set	macro
.set	reorder

lbu	$4,11241($16)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$4,$2,$L1750
li	$5,2			# 0x2
.set	macro
.set	reorder

beq	$4,$5,$L1751
beq	$4,$17,$L1752
.set	noreorder
.set	nomacro
bne	$4,$0,$L1777
li	$2,13			# 0xd
.set	macro
.set	reorder

lw	$4,7992($16)
.set	noreorder
.set	nomacro
bne	$4,$0,$L1753
li	$17,8			# 0x8
.set	macro
.set	reorder

lbu	$3,11229($16)
li	$2,4			# 0x4
li	$4,2			# 0x2
sw	$3,56($sp)
$L1472:
beq	$4,$0,$L1474
lw	$4,10296($16)
bne	$4,$0,$L1591
$L1474:
beq	$2,$0,$L1475
lw	$2,160($16)
lw	$4,7992($16)
addiu	$2,$2,-1
beq	$4,$2,$L1598
$L1475:
.set	noreorder
.set	nomacro
beq	$17,$0,$L1777
li	$2,13			# 0xd
.set	macro
.set	reorder

$L1597:
lw	$2,164($16)
lw	$4,7996($16)
addiu	$2,$2,-1
beq	$4,$2,$L1708
$L1460:
li	$2,13			# 0xd
$L1777:
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$3,64($sp)
sw	$2,2824($16)
lw	$2,2172($16)
addu	$19,$2,$3
lw	$3,56($sp)
sb	$3,0($19)
lbu	$2,11256($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1754
move	$17,$0
.set	macro
.set	reorder

$L1496:
lw	$2,0($18)
lw	$5,8004($16)
lw	$4,16($2)
.set	noreorder
.set	nomacro
beq	$5,$0,$L1767
or	$5,$4,$5
.set	macro
.set	reorder

li	$5,458752			# 0x70000
ori	$5,$5,0xe000
or	$5,$4,$5
$L1767:
lw	$3,56($sp)
lw	$23,%got(dma_len)($28)
addiu	$fp,$16,8012
sw	$5,16($2)
li	$21,1			# 0x1
lw	$5,20($2)
andi	$4,$3,0x3f
sll	$4,$4,12
lw	$20,%got(vc1_decode_intra_block)($28)
move	$22,$0
or	$4,$4,$5
li	$19,5			# 0x5
sw	$4,20($2)
.set	noreorder
.set	nomacro
b	$L1769
lbu	$4,8004($16)
.set	macro
.set	reorder

$L1755:
addiu	$2,$22,-2
sw	$0,11276($16)
sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1503
sw	$0,11272($16)
.set	macro
.set	reorder

lw	$2,10296($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1778
li	$2,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

$L1503:
lw	$2,0($fp)
lw	$5,24($fp)
lw	$4,11284($16)
subu	$2,$2,$5
addu	$2,$4,$2
lbu	$2,0($2)
sw	$2,11272($16)
li	$2,-3			# 0xfffffffffffffffd
$L1778:
li	$4,1			# 0x1
and	$2,$22,$2
beq	$2,$4,$L1505
lw	$2,7992($16)
beq	$2,$0,$L1506
$L1505:
lw	$4,0($fp)
lw	$2,11284($16)
addu	$2,$2,$4
lbu	$2,-1($2)
sw	$2,11276($16)
$L1506:
lw	$5,0($23)
andi	$12,$22,0x4
lw	$2,11260($16)
addiu	$25,$20,%lo(vc1_decode_intra_block)
lw	$13,11264($16)
move	$4,$16
lw	$11,0($18)
sll	$5,$5,4
lw	$3,56($sp)
move	$6,$22
movn	$2,$13,$12
addu	$5,$11,$5
addiu	$5,$5,60
sw	$3,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_intra_block
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

sll	$2,$22,2
lw	$6,0($18)
li	$5,1			# 0x1
sll	$11,$22,1
lw	$28,32($sp)
sll	$5,$5,$2
lw	$7,0($23)
addu	$2,$6,$22
lbu	$4,6($2)
li	$2,3			# 0x3
sll	$2,$2,$11
lw	$11,12($6)
andi	$4,$4,0x00ff
addu	$4,$4,$7
or	$5,$5,$11
sw	$5,12($6)
lw	$5,16($6)
sw	$4,0($23)
or	$2,$2,$5
sw	$2,16($6)
$L1508:
addiu	$22,$22,1
$L1770:
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
beq	$22,$2,$L1650
addiu	$fp,$fp,4
.set	macro
.set	reorder

lw	$2,0($18)
lbu	$4,8004($16)
$L1769:
addu	$2,$2,$22
subu	$7,$19,$22
sra	$7,$17,$7
sb	$4,26($2)
lw	$5,0($fp)
lw	$4,2728($16)
lw	$2,11284($16)
lw	$6,8004($16)
sll	$11,$5,1
addu	$2,$2,$5
addu	$4,$4,$11
sh	$0,0($4)
sb	$6,0($2)
lw	$2,8004($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1755
andi	$7,$7,0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$7,$0,$L1756
move	$4,$16
.set	macro
.set	reorder

lw	$2,0($18)
addu	$2,$2,$22
sb	$0,6($2)
.set	noreorder
.set	nomacro
b	$L1770
addiu	$22,$22,1
.set	macro
.set	reorder

$L1729:
lw	$2,11212($16)
lw	$4,10044($16)
addiu	$2,$2,-1
addu	$2,$2,$4
slt	$5,$2,9
.set	noreorder
.set	nomacro
beq	$5,$0,$L1757
li	$5,125			# 0x7d
.set	macro
.set	reorder

addiu	$2,$2,-1
andi	$2,$2,0x7
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
$L1447:
lw	$2,11216($16)
addiu	$2,$2,-1
addu	$2,$4,$2
slt	$4,$2,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L1758
li	$4,125			# 0x7d
.set	macro
.set	reorder

addiu	$2,$2,-1
andi	$2,$2,0x7
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1771
lw	$3,64($sp)
.set	macro
.set	reorder

$L1452:
addiu	$31,$31,%lo(offset_table)
addu	$10,$31,$10
lw	$6,0($10)
$L1579:
xori	$5,$2,0x5
.set	noreorder
.set	nomacro
b	$L1455
sltu	$5,$5,1
.set	macro
.set	reorder

$L1756:
lw	$5,0($23)
lw	$2,0($18)
move	$6,$22
lw	$7,72($sp)
sll	$5,$5,4
sw	$21,16($sp)
addu	$5,$2,$5
lw	$2,%got(vc1_decode_p_block_vlc.isra.7)($28)
addiu	$25,$2,%lo(vc1_decode_p_block_vlc.isra.7)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_p_block_vlc.isra.7
1:	jalr	$25
addiu	$5,$5,60
.set	macro
.set	reorder

lbu	$2,11256($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1611
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$3,72($sp)
li	$4,-1			# 0xffffffffffffffff
move	$21,$0
slt	$2,$3,8
movz	$4,$3,$2
.set	noreorder
.set	nomacro
b	$L1508
sw	$4,72($sp)
.set	macro
.set	reorder

$L1611:
.set	noreorder
.set	nomacro
b	$L1508
move	$21,$0
.set	macro
.set	reorder

$L1721:
lw	$4,11212($16)
lw	$2,10044($16)
addiu	$4,$4,-1
addu	$4,$4,$2
slt	$5,$4,9
.set	noreorder
.set	nomacro
beq	$5,$0,$L1759
li	$5,125			# 0x7d
.set	macro
.set	reorder

addiu	$4,$4,-1
andi	$4,$4,0x7
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
$L1521:
lw	$4,11216($16)
addiu	$4,$4,-1
addu	$2,$2,$4
slt	$4,$2,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L1760
li	$4,125			# 0x7d
.set	macro
.set	reorder

addiu	$2,$2,-1
andi	$2,$2,0x7
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1515
li	$2,1			# 0x1
.set	macro
.set	reorder

$L1728:
li	$6,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$4,$2,$4
sll	$2,$4,2
addu	$5,$5,$2
lh	$4,0($5)
.set	noreorder
.set	nomacro
b	$L1442
lh	$2,2($5)
.set	macro
.set	reorder

$L1512:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$21,$2,$21
sll	$2,$21,2
addu	$5,$5,$2
lh	$2,0($5)
sw	$2,68($sp)
.set	noreorder
.set	nomacro
b	$L1513
lh	$2,2($5)
.set	macro
.set	reorder

$L1497:
lw	$2,0($18)
ori	$5,$5,0xe000
lw	$4,16($2)
.set	noreorder
.set	nomacro
b	$L1767
or	$5,$4,$5
.set	macro
.set	reorder

$L1722:
li	$4,1			# 0x1
li	$2,1			# 0x1
move	$7,$0
sw	$4,8004($16)
.set	noreorder
.set	nomacro
b	$L1515
move	$6,$0
.set	macro
.set	reorder

$L1617:
move	$24,$0
move	$7,$0
.set	noreorder
.set	nomacro
b	$L1529
move	$2,$0
.set	macro
.set	reorder

$L1451:
addiu	$4,$4,%lo(size_table)
addu	$5,$4,$10
lw	$5,0($5)
.set	noreorder
.set	nomacro
bgtz	$5,$L1580
lw	$31,%got(offset_table)($28)
.set	macro
.set	reorder

move	$5,$0
addiu	$31,$31,%lo(offset_table)
addu	$10,$31,$10
.set	noreorder
.set	nomacro
b	$L1455
lw	$6,0($10)
.set	macro
.set	reorder

$L1747:
lbu	$4,11242($16)
li	$2,1			# 0x1
sll	$2,$2,$4
$L1543:
andi	$4,$2,0x1
.set	noreorder
.set	nomacro
beq	$4,$0,$L1761
andi	$4,$2,0x4
.set	macro
.set	reorder

lw	$4,7992($16)
.set	noreorder
.set	nomacro
bne	$4,$0,$L1762
andi	$5,$2,0x2
.set	macro
.set	reorder

lbu	$3,11229($16)
andi	$4,$2,0x4
andi	$2,$2,0x8
.set	noreorder
.set	nomacro
b	$L1548
sw	$3,56($sp)
.set	macro
.set	reorder

$L1746:
lbu	$2,11243($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1538
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bne	$2,$0,$L1710
lbu	$3,11228($16)
.set	noreorder
.set	nomacro
b	$L1536
sw	$3,56($sp)
.set	macro
.set	reorder

$L1574:
lw	$5,2172($16)
lw	$3,64($sp)
move	$2,$0
li	$8,6			# 0x6
sw	$0,8004($16)
addu	$5,$5,$3
sb	$0,0($5)
$L1576:
lw	$6,0($4)
addiu	$4,$4,4
lw	$5,11284($16)
addu	$5,$5,$6
sb	$0,0($5)
lw	$7,-4($4)
lw	$6,2728($16)
lw	$5,0($18)
sll	$7,$7,1
addu	$5,$5,$2
addu	$6,$6,$7
addiu	$2,$2,1
sh	$0,0($6)
sb	$0,6($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L1576
move	$17,$0
.set	macro
.set	reorder

li	$18,4			# 0x4
$L1577:
lw	$8,11220($16)
move	$5,$17
lw	$3,11224($16)
move	$6,$0
lw	$2,11284($16)
move	$7,$0
lw	$25,%call16(vc1_pred_mv)($28)
addiu	$17,$17,1
sw	$0,16($sp)
move	$4,$16
sw	$8,20($sp)
sw	$3,24($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_pred_mv
1:	jalr	$25
sw	$2,28($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$17,$18,$L1577
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$2,2172($16)
lw	$3,64($sp)
addu	$19,$2,$3
.set	noreorder
.set	nomacro
b	$L1650
sb	$0,0($19)
.set	macro
.set	reorder

$L1762:
andi	$4,$2,0x4
.set	noreorder
.set	nomacro
b	$L1548
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1626:
li	$2,8			# 0x8
li	$4,4			# 0x4
$L1595:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1550
sw	$3,56($sp)
.set	macro
.set	reorder

$L1761:
andi	$5,$2,0x2
.set	noreorder
.set	nomacro
b	$L1548
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1538:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,7			# 0x7
beq	$2,$4,$L1541
lbu	$3,11228($16)
addu	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L1536
sw	$3,56($sp)
.set	macro
.set	reorder

$L1749:
lw	$2,10296($16)
bne	$2,$0,$L1626
lw	$5,160($16)
addiu	$5,$5,-1
.set	noreorder
.set	nomacro
bne	$4,$5,$L1603
li	$2,8			# 0x8
.set	macro
.set	reorder

$L1604:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1551
sw	$3,56($sp)
.set	macro
.set	reorder

$L1743:
lbu	$4,11242($16)
li	$2,1			# 0x1
sll	$2,$2,$4
$L1487:
andi	$4,$2,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L1491
andi	$4,$2,0x4
.set	macro
.set	reorder

andi	$5,$2,0x2
.set	noreorder
.set	nomacro
b	$L1492
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1625:
li	$2,8			# 0x8
$L1593:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1494
sw	$3,56($sp)
.set	macro
.set	reorder

$L1744:
lbu	$4,11242($16)
sll	$4,$2,$4
li	$2,-2004353024			# 0xffffffff88880000
sra	$5,$4,31
ori	$2,$2,0x8889
mult	$4,$2
mfhi	$2
addu	$2,$2,$4
sra	$2,$2,3
subu	$2,$2,$5
sll	$5,$2,4
subu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L1487
subu	$2,$4,$2
.set	macro
.set	reorder

$L1499:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$2,$22
sll	$2,$2,2
addu	$5,$5,$2
lh	$3,0($5)
lh	$2,2($5)
.set	noreorder
.set	nomacro
b	$L1500
sw	$3,72($sp)
.set	macro
.set	reorder

$L1750:
lbu	$2,11243($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1462
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bne	$2,$0,$L1708
lbu	$3,11228($16)
.set	noreorder
.set	nomacro
b	$L1460
sw	$3,56($sp)
.set	macro
.set	reorder

$L1745:
lw	$2,10296($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1625
li	$4,4			# 0x4
.set	macro
.set	reorder

lw	$4,160($16)
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$5,$4,$L1600
li	$2,8			# 0x8
.set	macro
.set	reorder

$L1601:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1495
sw	$3,56($sp)
.set	macro
.set	reorder

$L1624:
li	$17,8			# 0x8
$L1591:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1474
sw	$3,56($sp)
.set	macro
.set	reorder

$L1708:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1460
sw	$3,56($sp)
.set	macro
.set	reorder

$L1753:
lw	$2,10296($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1624
li	$2,4			# 0x4
.set	macro
.set	reorder

lw	$2,160($16)
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
bne	$4,$2,$L1597
li	$17,8			# 0x8
.set	macro
.set	reorder

$L1598:
lbu	$3,11229($16)
.set	noreorder
.set	nomacro
b	$L1475
sw	$3,56($sp)
.set	macro
.set	reorder

$L1741:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$17,$2,$17
sll	$2,$17,2
addu	$5,$5,$2
lh	$17,0($5)
.set	noreorder
.set	nomacro
b	$L1480
lh	$2,2($5)
.set	macro
.set	reorder

$L1730:
lw	$2,64($sp)
li	$9,1			# 0x1
lw	$8,8012($16)
move	$7,$0
lw	$5,2192($16)
move	$6,$0
sll	$4,$2,2
lw	$2,2188($16)
sll	$8,$8,2
sw	$9,8004($16)
addu	$4,$5,$4
addu	$2,$2,$8
sh	$0,0($2)
sh	$0,2($2)
.set	noreorder
.set	nomacro
b	$L1450
sw	$9,0($4)
.set	macro
.set	reorder

$L1491:
lw	$4,7992($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1493
andi	$5,$2,0x2
.set	macro
.set	reorder

andi	$4,$2,0x4
.set	noreorder
.set	nomacro
b	$L1492
andi	$2,$2,0x8
.set	macro
.set	reorder

$L1748:
lbu	$2,11242($16)
sll	$4,$4,$2
li	$2,-2004353024			# 0xffffffff88880000
sra	$5,$4,31
ori	$2,$2,0x8889
mult	$4,$2
mfhi	$2
addu	$2,$2,$4
sra	$2,$2,3
subu	$2,$2,$5
sll	$5,$2,4
subu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L1543
subu	$2,$4,$2
.set	macro
.set	reorder

$L1609:
move	$7,$0
.set	noreorder
.set	nomacro
b	$L1456
move	$5,$0
.set	macro
.set	reorder

$L1562:
li	$4,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$2,$3
sll	$2,$2,2
addu	$5,$5,$2
lh	$3,0($5)
lh	$2,2($5)
.set	noreorder
.set	nomacro
b	$L1563
sw	$3,72($sp)
.set	macro
.set	reorder

$L1493:
lbu	$3,11229($16)
andi	$4,$2,0x4
andi	$2,$2,0x8
.set	noreorder
.set	nomacro
b	$L1492
sw	$3,56($sp)
.set	macro
.set	reorder

$L1742:
lbu	$2,11243($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1482
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bne	$2,$0,$L1709
lbu	$3,11228($16)
.set	noreorder
.set	nomacro
b	$L1476
sw	$3,56($sp)
.set	macro
.set	reorder

$L1726:
#APP
# 76 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$2,-1
andi	$4,$4,0x7
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$2,-8
sll	$2,$7,$2
or	$2,$2,$4
andi	$7,$2,0x1
subu	$24,$0,$7
.set	noreorder
.set	nomacro
b	$L1529
sra	$4,$2,1
.set	macro
.set	reorder

$L1725:
li	$5,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$31,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$7,-1
andi	$5,$5,0x7
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addiu	$7,$7,-8
sll	$7,$31,$7
.set	noreorder
.set	nomacro
b	$L1527
or	$5,$7,$5
.set	macro
.set	reorder

$L1751:
lbu	$2,11242($16)
sll	$17,$17,$2
$L1467:
andi	$2,$17,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L1763
andi	$2,$17,0x4
.set	macro
.set	reorder

lw	$2,7992($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1473
andi	$4,$17,0x2
.set	macro
.set	reorder

andi	$2,$17,0x4
.set	noreorder
.set	nomacro
b	$L1472
andi	$17,$17,0x8
.set	macro
.set	reorder

$L1760:
#APP
# 76 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$2,-1
andi	$4,$4,0x7
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$2,-8
li	$2,1			# 0x1
sll	$4,$5,$4
.set	noreorder
.set	nomacro
b	$L1515
or	$7,$4,$7
.set	macro
.set	reorder

$L1473:
lbu	$3,11229($16)
andi	$2,$17,0x4
andi	$17,$17,0x8
.set	noreorder
.set	nomacro
b	$L1472
sw	$3,56($sp)
.set	macro
.set	reorder

$L1763:
andi	$4,$17,0x2
.set	noreorder
.set	nomacro
b	$L1472
andi	$17,$17,0x8
.set	macro
.set	reorder

$L1759:
#APP
# 76 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$4,-1
andi	$5,$5,0x7
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$4,-8
sll	$4,$7,$4
.set	noreorder
.set	nomacro
b	$L1521
or	$6,$4,$6
.set	macro
.set	reorder

$L1752:
lbu	$4,11242($16)
sll	$4,$2,$4
li	$2,-2004353024			# 0xffffffff88880000
sra	$17,$4,31
ori	$2,$2,0x8889
mult	$4,$2
mfhi	$2
addu	$2,$2,$4
sra	$2,$2,3
subu	$17,$2,$17
sll	$2,$17,4
subu	$17,$2,$17
.set	noreorder
.set	nomacro
b	$L1467
subu	$17,$4,$17
.set	macro
.set	reorder

$L1482:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,7			# 0x7
beq	$2,$4,$L1485
lbu	$3,11228($16)
addu	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L1476
sw	$3,56($sp)
.set	macro
.set	reorder

$L1732:
#APP
# 76 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addiu	$7,$5,-1
andi	$7,$7,0x7
sll	$7,$7,4
ori	$7,$7,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$5,-8
sll	$4,$4,$5
or	$4,$4,$7
andi	$5,$4,0x1
subu	$7,$0,$5
.set	noreorder
.set	nomacro
b	$L1456
sra	$4,$4,1
.set	macro
.set	reorder

$L1731:
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$5,-1
andi	$2,$2,0x7
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$5,-8
sll	$5,$9,$5
.set	noreorder
.set	nomacro
b	$L1454
or	$9,$5,$2
.set	macro
.set	reorder

$L1758:
#APP
# 76 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
addiu	$4,$2,-1
andi	$4,$4,0x7
sll	$4,$4,4
ori	$4,$4,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$2,-8
sll	$2,$7,$2
.set	noreorder
.set	nomacro
b	$L1444
or	$7,$2,$4
.set	macro
.set	reorder

$L1462:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,7			# 0x7
beq	$2,$4,$L1465
lbu	$3,11228($16)
addu	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L1460
sw	$3,56($sp)
.set	macro
.set	reorder

$L1541:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1536
sw	$3,56($sp)
.set	macro
.set	reorder

$L1757:
#APP
# 76 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
addiu	$5,$2,-1
andi	$5,$5,0x7
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$2,-8
sll	$2,$6,$2
.set	noreorder
.set	nomacro
b	$L1447
or	$6,$2,$5
.set	macro
.set	reorder

$L1465:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1460
sw	$3,56($sp)
.set	macro
.set	reorder

$L1485:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1476
sw	$3,56($sp)
.set	macro
.set	reorder

$L1734:
lw	$2,11284($16)
lw	$6,0($5)
addu	$2,$2,$6
lbu	$2,-1($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1776
li	$2,13			# 0xd
.set	macro
.set	reorder

addiu	$4,$4,1
.set	noreorder
.set	nomacro
b	$L1552
addiu	$5,$5,4
.set	macro
.set	reorder

$L1724:
move	$7,$0
addiu	$3,$3,%lo(offset_table)
addu	$6,$3,$6
move	$31,$3
.set	noreorder
.set	nomacro
b	$L1528
lw	$6,0($6)
.set	macro
.set	reorder

.end	vc1_decode_p_mb_vlc
.size	vc1_decode_p_mb_vlc, .-vc1_decode_p_mb_vlc
.section	.rodata.str1.4
.align	2
$LC6:
.ascii	"Sliced decoding is not implemented (yet)\012\000"
.align	2
$LC7:
.ascii	"Error in WVC1 interlaced frame\012\000"
.align	2
$LC8:
.ascii	"Interlaced WVC1 support is not implemented\012\000"
.align	2
$LC9:
.ascii	"Bits overconsumption: %i > %i\012\000"
.align	2
$LC10:
.ascii	"Bits overconsumption: %i > %i at %ix%i\012\000"
.align	2
$LC11:
.ascii	"s->current_picture.pict_type == s->current_picture_ptr->"
.ascii	"pict_type\000"
.align	2
$LC12:
.ascii	"vc1dec.c\000"
.align	2
$LC13:
.ascii	"s->current_picture.pict_type == s->pict_type\000"
.section	.text.vc1_decode_frame,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vc1_decode_frame
.type	vc1_decode_frame, @function
vc1_decode_frame:
.frame	$sp,272,$31		# vars= 192, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-272
sw	$fp,264($sp)
.cprestore	32
sw	$23,260($sp)
sw	$19,244($sp)
sw	$31,268($sp)
sw	$22,256($sp)
sw	$21,252($sp)
sw	$20,248($sp)
sw	$18,240($sp)
sw	$17,236($sp)
sw	$16,232($sp)
lw	$fp,20($7)
lw	$19,16($7)
.set	noreorder
.set	nomacro
bne	$fp,$0,$L1780
lw	$23,136($4)
.set	macro
.set	reorder

lw	$2,10096($23)
bne	$2,$0,$L2064
lw	$3,2692($23)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1779
addiu	$4,$3,208
.set	macro
.set	reorder

$L1782:
lw	$10,0($3)
addiu	$3,$3,16
addiu	$5,$5,16
lw	$9,-12($3)
lw	$8,-8($3)
lw	$7,-4($3)
sw	$10,-16($5)
sw	$9,-12($5)
sw	$8,-8($5)
.set	noreorder
.set	nomacro
bne	$3,$4,$L1782
sw	$7,-4($5)
.set	macro
.set	reorder

lw	$4,0($3)
lw	$3,4($3)
sw	$4,0($5)
sw	$3,4($5)
li	$3,216			# 0xd8
sw	$0,2692($23)
sw	$3,0($6)
$L1779:
lw	$31,268($sp)
$L2299:
lw	$fp,264($sp)
lw	$23,260($sp)
lw	$22,256($sp)
lw	$21,252($sp)
lw	$20,248($sp)
lw	$19,244($sp)
lw	$18,240($sp)
lw	$17,236($sp)
lw	$16,232($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

$L1780:
lw	$2,2696($23)
move	$16,$4
sw	$6,148($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1783
sw	$5,144($sp)
.set	macro
.set	reorder

lw	$2,0($2)
beq	$2,$0,$L1784
$L1783:
lw	$25,%call16(ff_find_unused_picture)($28)
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_find_unused_picture
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

sll	$3,$2,3
lw	$4,200($23)
sll	$2,$2,5
lw	$28,32($sp)
addu	$2,$3,$2
sll	$3,$2,4
subu	$2,$3,$2
addu	$2,$4,$2
sw	$2,2696($23)
$L1784:
lw	$2,0($23)
lw	$2,132($2)
lw	$2,32($2)
andi	$2,$2,0x80
beq	$2,$0,$L1785
lw	$2,11168($23)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1786
li	$2,40			# 0x28
.set	macro
.set	reorder

li	$2,39			# 0x27
sw	$2,52($16)
$L1785:
lw	$3,224($16)
li	$2,73			# 0x49
.set	noreorder
.set	nomacro
beq	$3,$2,$L2309
lw	$4,%got(buf2_pre_alloc_buf)($28)
.set	macro
.set	reorder

sll	$3,$fp,3
$L2310:
sra	$2,$3,3
bltz	$2,$L2074
bltz	$3,$L2074
addu	$2,$19,$2
move	$4,$19
$L1837:
sw	$4,10332($23)
move	$17,$19
sw	$3,10344($23)
sw	$2,10336($23)
sw	$0,10340($23)
$L1836:
lw	$2,11168($23)
slt	$2,$2,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L2219
lw	$25,%call16(vc1_parse_frame_header_adv)($28)
.set	macro
.set	reorder

lw	$25,%call16(vc1_parse_frame_header)($28)
$L2219:
addiu	$5,$23,10332
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$23
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
beq	$2,$3,$L2226
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$2,2904($23)
lw	$4,2688($23)
xori	$3,$2,0x1
sltu	$3,$3,1
sw	$2,2140($23)
.set	noreorder
.set	nomacro
beq	$4,$0,$L2243
sw	$3,2136($23)
.set	macro
.set	reorder

lw	$4,128($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1843
li	$3,3			# 0x3
.set	macro
.set	reorder

beq	$2,$3,$L2226
lw	$3,708($16)
slt	$5,$3,8
bne	$5,$0,$L1846
$L2052:
slt	$5,$3,32
$L2313:
.set	noreorder
.set	nomacro
bne	$5,$0,$L1846
li	$5,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$5,$L2244
slt	$5,$3,48
.set	macro
.set	reorder

lw	$31,268($sp)
$L2295:
move	$2,$fp
lw	$4,%got(last_skip_frame)($28)
lw	$fp,264($sp)
lw	$23,260($sp)
lw	$22,256($sp)
lw	$21,252($sp)
lw	$20,248($sp)
lw	$19,244($sp)
lw	$18,240($sp)
lw	$17,236($sp)
lw	$16,232($sp)
addiu	$sp,$sp,272
.set	noreorder
.set	nomacro
j	$31
sw	$3,%lo(last_skip_frame)($4)
.set	macro
.set	reorder

$L2064:
lw	$31,268($sp)
move	$2,$0
lw	$fp,264($sp)
lw	$23,260($sp)
lw	$22,256($sp)
lw	$21,252($sp)
lw	$20,248($sp)
lw	$19,244($sp)
lw	$18,240($sp)
lw	$17,236($sp)
lw	$16,232($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

$L1786:
lw	$3,224($16)
sw	$2,52($16)
li	$2,73			# 0x49
.set	noreorder
.set	nomacro
bne	$3,$2,$L2310
sll	$3,$fp,3
.set	macro
.set	reorder

lw	$4,%got(buf2_pre_alloc_buf)($28)
$L2309:
li	$3,16711680			# 0xff0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($19)  
lwr $2, 0($19)  

# 0 "" 2
#NO_APP
addiu	$8,$3,255
srl	$3,$2,8
lw	$22,%lo(buf2_pre_alloc_buf)($4)
li	$4,-16777216			# 0xffffffffff000000
sll	$2,$2,8
ori	$5,$4,0xff00
and	$3,$3,$8
and	$2,$2,$5
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
li	$4,-256			# 0xffffffffffffff00
or	$2,$3,$2
li	$3,256			# 0x100
and	$2,$2,$4
.set	noreorder
.set	nomacro
beq	$2,$3,$L2245
addu	$20,$19,$fp
.set	macro
.set	reorder

lw	$2,11128($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L2311
slt	$2,$fp,4
.set	macro
.set	reorder

lbu	$3,0($19)
li	$2,192			# 0xc0
andi	$5,$3,0xc0
.set	noreorder
.set	nomacro
beq	$5,$2,$L2246
slt	$2,$fp,4
.set	macro
.set	reorder

$L2311:
.set	noreorder
.set	nomacro
beq	$2,$0,$L1831
move	$2,$19
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$fp,$L2070
move	$2,$22
.set	macro
.set	reorder

addu	$5,$22,$fp
move	$3,$19
$L1832:
addiu	$3,$3,1
lbu	$4,-1($3)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$5,$L1832
sb	$4,-1($2)
.set	macro
.set	reorder

$L2070:
move	$21,$fp
$L2220:
move	$17,$19
$L1813:
sll	$21,$21,3
sra	$2,$21,3
bltz	$2,$L2072
bltz	$21,$L2072
addu	$2,$22,$2
$L1789:
sw	$22,10332($23)
sw	$21,10344($23)
sw	$2,10336($23)
.set	noreorder
.set	nomacro
b	$L1836
sw	$0,10340($23)
.set	macro
.set	reorder

$L2074:
move	$2,$0
move	$3,$0
.set	noreorder
.set	nomacro
b	$L1837
move	$4,$0
.set	macro
.set	reorder

$L1817:
addiu	$5,$2,-3
beq	$8,$5,$L1815
li	$7,16711680			# 0xff0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 0($2)  
lwr $4, -3($2)  

# 0 "" 2
#NO_APP
addiu	$7,$7,255
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$4,$4,$7
or	$2,$2,$4
sll	$4,$2,16
srl	$2,$2,16
or	$2,$4,$2
li	$4,268			# 0x10c
.set	noreorder
.set	nomacro
bne	$2,$4,$L1815
subu	$4,$5,$19
.set	macro
.set	reorder

slt	$2,$4,4
beq	$2,$0,$L1822
blez	$4,$L1826
.set	noreorder
.set	nomacro
b	$L1827
addu	$4,$22,$4
.set	macro
.set	reorder

$L2247:
lbu	$3,0($6)
addiu	$6,$6,1
$L1827:
addiu	$22,$22,1
.set	noreorder
.set	nomacro
bne	$22,$4,$L2247
sb	$3,-1($22)
.set	macro
.set	reorder

$L1826:
lw	$2,11952($23)
addiu	$3,$2,1
.set	noreorder
.set	nomacro
bne	$2,$0,$L2226
sw	$3,11952($23)
.set	macro
.set	reorder

lw	$6,%got($LC8)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC8)
.set	macro
.set	reorder

$L2226:
li	$2,-1			# 0xffffffffffffffff
$L2251:
lw	$31,268($sp)
lw	$fp,264($sp)
lw	$23,260($sp)
lw	$22,256($sp)
lw	$21,252($sp)
lw	$20,248($sp)
lw	$19,244($sp)
lw	$18,240($sp)
lw	$17,236($sp)
lw	$16,232($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

$L1843:
lw	$3,708($16)
slt	$5,$3,8
.set	noreorder
.set	nomacro
beq	$5,$0,$L2053
li	$5,3			# 0x3
.set	macro
.set	reorder

lw	$5,%got(last_skip_frame)($28)
$L2314:
lw	$4,%lo(last_skip_frame)($5)
slt	$4,$4,32
bne	$4,$0,$L1847
bne	$3,$0,$L1847
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L2248
move	$2,$fp
.set	macro
.set	reorder

b	$L1779
$L1847:
lw	$4,9812($23)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1850
sw	$3,%lo(last_skip_frame)($5)
.set	macro
.set	reorder

li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L1779
move	$2,$fp
.set	macro
.set	reorder

sw	$0,9812($23)
$L1850:
lw	$25,%call16(MPV_frame_start)($28)
move	$4,$23
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_frame_start
1:	jalr	$25
move	$5,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L2226
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$2,872($16)
addiu	$4,$23,3856
addiu	$3,$23,3984
sw	$4,7964($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1851
sw	$3,7968($23)
.set	macro
.set	reorder

lw	$25,24($2)
move	$4,$16
move	$5,$19
.set	noreorder
.set	nomacro
jalr	$25
move	$6,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L2226
move	$4,$16
.set	macro
.set	reorder

lw	$2,872($16)
addu	$6,$19,$fp
subu	$6,$6,$17
lw	$25,28($2)
.set	noreorder
.set	nomacro
jalr	$25
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L2251
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,872($16)
lw	$25,32($2)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L2226
lw	$28,32($sp)
.set	macro
.set	reorder

$L1852:
lw	$25,%call16(MPV_frame_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_frame_end
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$2,2696($23)
lw	$3,2140($23)
lw	$4,52($2)
.set	noreorder
.set	nomacro
bne	$3,$4,$L2249
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$4,2904($23)
.set	noreorder
.set	nomacro
bne	$3,$4,$L2250
lw	$4,%got($LC13)($28)
.set	macro
.set	reorder

li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$4,$L2312
lw	$3,144($sp)
.set	macro
.set	reorder

lw	$3,10096($23)
.set	noreorder
.set	nomacro
bne	$3,$0,$L2038
lw	$3,144($sp)
.set	macro
.set	reorder

lw	$2,2688($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L2043
addiu	$4,$2,208
.set	macro
.set	reorder

$L2045:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L2045
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
$L2298:
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
lw	$2,2688($23)
beq	$2,$0,$L2046
$L2044:
li	$2,216			# 0xd8
lw	$8,148($sp)
lw	$25,%call16(ff_print_debug_info)($28)
move	$4,$23
lw	$5,144($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_print_debug_info
1:	jalr	$25
sw	$2,0($8)
.set	macro
.set	reorder

$L2043:
li	$2,-2147483648			# 0xffffffff80000000
addiu	$3,$2,16384
$L2047:
#APP
# 4806 "vc1dec.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L2047
#APP
# 4806 "vc1dec.c" 1
sync
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1779
move	$2,$fp
.set	macro
.set	reorder

$L2053:
.set	noreorder
.set	nomacro
bne	$2,$5,$L2313
slt	$5,$3,32
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2295
lw	$31,268($sp)
.set	macro
.set	reorder

$L2243:
li	$3,3			# 0x3
beq	$2,$3,$L2226
lw	$3,2916($23)
bne	$3,$0,$L2226
lw	$3,708($16)
slt	$5,$3,8
.set	noreorder
.set	nomacro
beq	$5,$0,$L2052
lw	$4,128($16)
.set	macro
.set	reorder

$L1846:
slt	$4,$4,5
$L2296:
.set	noreorder
.set	nomacro
bne	$4,$0,$L2314
lw	$5,%got(last_skip_frame)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2251
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L2244:
.set	noreorder
.set	nomacro
beq	$5,$0,$L2295
lw	$31,268($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2296
slt	$4,$4,5
.set	macro
.set	reorder

$L1831:
li	$7,3			# 0x3
lbu	$6,0($2)
li	$5,1			# 0x1
move	$3,$0
addiu	$9,$fp,-1
.set	noreorder
.set	nomacro
beq	$6,$7,$L2252
move	$4,$22
.set	macro
.set	reorder

$L1833:
move	$21,$5
$L2315:
sb	$6,0($4)
$L1834:
addiu	$3,$3,1
addiu	$2,$2,1
slt	$6,$3,$fp
addiu	$5,$5,1
.set	noreorder
.set	nomacro
beq	$6,$0,$L2220
addiu	$4,$4,1
.set	macro
.set	reorder

lbu	$6,0($2)
bne	$6,$7,$L1833
$L2252:
slt	$8,$3,2
bne	$8,$0,$L1833
lbu	$8,-1($2)
bne	$8,$0,$L1833
lbu	$8,-2($2)
.set	noreorder
.set	nomacro
bne	$8,$0,$L1833
slt	$8,$3,$9
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$8,$0,$L2315
move	$21,$5
.set	macro
.set	reorder

lbu	$8,1($2)
sltu	$10,$8,4
beq	$10,$0,$L2315
sb	$8,0($4)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L1834
addiu	$3,$3,1
.set	macro
.set	reorder

$L2246:
.set	noreorder
.set	nomacro
bne	$2,$0,$L2316
lw	$6,%got($LC7)($28)
.set	macro
.set	reorder

addu	$8,$19,$fp
sltu	$2,$19,$8
.set	noreorder
.set	nomacro
beq	$2,$0,$L2316
li	$10,-256			# 0xffffffffffffff00
.set	macro
.set	reorder

addiu	$6,$19,1
li	$9,256			# 0x100
or	$4,$3,$4
move	$2,$6
$L1820:
.set	noreorder
.set	nomacro
beq	$2,$8,$L1815
sll	$4,$4,8
.set	macro
.set	reorder

lbu	$7,0($2)
or	$4,$7,$4
and	$7,$4,$10
.set	noreorder
.set	nomacro
beq	$7,$9,$L1817
addiu	$5,$2,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1820
move	$2,$5
.set	macro
.set	reorder

$L2248:
lw	$2,9812($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1850
sw	$0,%lo(last_skip_frame)($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1850
sw	$0,9812($23)
.set	macro
.set	reorder

$L2072:
move	$2,$0
move	$21,$0
.set	noreorder
.set	nomacro
b	$L1789
move	$22,$0
.set	macro
.set	reorder

$L2245:
sltu	$2,$19,$20
.set	noreorder
.set	nomacro
beq	$2,$0,$L2065
addiu	$7,$23,10332
.set	macro
.set	reorder

move	$21,$0
move	$6,$19
move	$17,$19
move	$15,$19
move	$14,$8
move	$13,$5
move	$19,$7
$L1812:
addiu	$10,$6,4
subu	$2,$20,$10
slt	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$0,$L2317
subu	$5,$20,$6
.set	macro
.set	reorder

sltu	$2,$10,$20
.set	noreorder
.set	nomacro
beq	$2,$0,$L2317
li	$4,-256			# 0xffffffffffffff00
.set	macro
.set	reorder

lbu	$2,4($6)
addiu	$3,$6,5
li	$5,-256			# 0xffffffffffffff00
or	$2,$2,$4
.set	noreorder
.set	nomacro
b	$L1796
li	$4,256			# 0x100
.set	macro
.set	reorder

$L1793:
lbu	$9,0($3)
or	$2,$9,$2
and	$9,$2,$5
.set	noreorder
.set	nomacro
beq	$9,$4,$L1792
addiu	$8,$3,1
.set	macro
.set	reorder

move	$3,$8
$L1796:
sltu	$8,$3,$20
.set	noreorder
.set	nomacro
bne	$8,$0,$L1793
sll	$2,$2,8
.set	macro
.set	reorder

subu	$5,$20,$6
$L2317:
addiu	$4,$5,-4
.set	noreorder
.set	nomacro
blez	$4,$L2217
move	$18,$20
.set	macro
.set	reorder

$L1795:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($6)  
lwr $2, 0($6)  

# 0 "" 2
#NO_APP
srl	$3,$2,8
sll	$2,$2,8
and	$3,$3,$14
and	$2,$2,$13
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
li	$3,269			# 0x10d
.set	noreorder
.set	nomacro
beq	$2,$3,$L1797
li	$3,270			# 0x10e
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L1798
li	$3,267			# 0x10b
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L2253
lw	$6,%got($LC6)($28)
.set	macro
.set	reorder

move	$6,$18
$L1794:
sltu	$3,$18,$20
bne	$3,$0,$L1812
$L2217:
.set	noreorder
.set	nomacro
b	$L1813
move	$19,$15
.set	macro
.set	reorder

$L2253:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1779
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1792:
addiu	$18,$3,-3
subu	$5,$18,$6
addiu	$4,$5,-4
bgtz	$4,$L1795
.set	noreorder
.set	nomacro
b	$L1794
move	$6,$18
.set	macro
.set	reorder

$L1798:
slt	$2,$4,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L1806
move	$2,$0
.set	macro
.set	reorder

$L1807:
addu	$3,$6,$2
addu	$5,$22,$2
addiu	$2,$2,1
lbu	$8,4($3)
slt	$3,$2,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L1807
sb	$8,0($5)
.set	macro
.set	reorder

move	$21,$4
sll	$3,$21,3
$L2297:
sra	$2,$3,3
bltz	$2,$L2254
addu	$2,$22,$2
move	$5,$22
$L2048:
lw	$25,%call16(vc1_decode_entry_point)($28)
move	$6,$19
sw	$5,10332($23)
move	$4,$16
sw	$3,10344($23)
move	$5,$23
sw	$2,10336($23)
sw	$0,10340($23)
sw	$13,216($sp)
sw	$14,212($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_entry_point
1:	jalr	$25
sw	$15,220($sp)
.set	macro
.set	reorder

move	$6,$18
lw	$28,32($sp)
lw	$13,216($sp)
lw	$14,212($sp)
.set	noreorder
.set	nomacro
b	$L1794
lw	$15,220($sp)
.set	macro
.set	reorder

$L1797:
lw	$2,872($16)
beq	$2,$0,$L2255
move	$17,$6
$L1800:
slt	$2,$4,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L1801
move	$2,$0
.set	macro
.set	reorder

$L1802:
addu	$3,$6,$2
addu	$5,$22,$2
addiu	$2,$2,1
lbu	$8,4($3)
slt	$3,$2,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L1802
sb	$8,0($5)
.set	macro
.set	reorder

move	$6,$18
.set	noreorder
.set	nomacro
b	$L1794
move	$21,$4
.set	macro
.set	reorder

$L1801:
li	$6,1			# 0x1
li	$9,3			# 0x3
addiu	$5,$5,-5
move	$3,$22
$L1805:
lbu	$8,0($10)
.set	noreorder
.set	nomacro
beq	$8,$9,$L2256
slt	$11,$2,2
.set	macro
.set	reorder

move	$21,$6
$L2318:
sb	$8,0($3)
$L1804:
addiu	$2,$2,1
addiu	$10,$10,1
slt	$8,$2,$4
addiu	$6,$6,1
.set	noreorder
.set	nomacro
bne	$8,$0,$L1805
addiu	$3,$3,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1794
move	$6,$18
.set	macro
.set	reorder

$L2256:
.set	noreorder
.set	nomacro
bne	$11,$0,$L2318
move	$21,$6
.set	macro
.set	reorder

lbu	$11,-1($10)
bne	$11,$0,$L2318
lbu	$11,-2($10)
.set	noreorder
.set	nomacro
bne	$11,$0,$L2318
slt	$11,$2,$5
.set	macro
.set	reorder

beq	$11,$0,$L2318
lbu	$11,1($10)
sltu	$12,$11,4
beq	$12,$0,$L2318
sb	$11,0($3)
addiu	$10,$10,1
.set	noreorder
.set	nomacro
b	$L1804
addiu	$2,$2,1
.set	macro
.set	reorder

$L1806:
li	$6,1			# 0x1
li	$9,3			# 0x3
addiu	$5,$5,-5
move	$3,$22
$L1811:
lbu	$8,0($10)
.set	noreorder
.set	nomacro
beq	$8,$9,$L2257
slt	$11,$2,2
.set	macro
.set	reorder

move	$21,$6
$L2319:
sb	$8,0($3)
$L1810:
addiu	$2,$2,1
addiu	$10,$10,1
slt	$8,$2,$4
addiu	$6,$6,1
.set	noreorder
.set	nomacro
bne	$8,$0,$L1811
addiu	$3,$3,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2297
sll	$3,$21,3
.set	macro
.set	reorder

$L2257:
.set	noreorder
.set	nomacro
bne	$11,$0,$L2319
move	$21,$6
.set	macro
.set	reorder

lbu	$11,-1($10)
bne	$11,$0,$L2319
lbu	$11,-2($10)
.set	noreorder
.set	nomacro
bne	$11,$0,$L2319
slt	$11,$2,$5
.set	macro
.set	reorder

beq	$11,$0,$L2319
lbu	$11,1($10)
sltu	$12,$11,4
beq	$12,$0,$L2319
sb	$11,0($3)
addiu	$10,$10,1
.set	noreorder
.set	nomacro
b	$L1810
addiu	$2,$2,1
.set	macro
.set	reorder

$L1815:
lw	$6,%got($LC7)($28)
$L2316:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1779
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L2254:
move	$2,$0
move	$3,$0
.set	noreorder
.set	nomacro
b	$L2048
move	$5,$0
.set	macro
.set	reorder

$L2038:
$L2312:
addiu	$4,$2,208
$L2040:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L2040
sw	$5,-4($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2298
lw	$4,0($2)
.set	macro
.set	reorder

$L1851:
lw	$25,%call16(ff_er_frame_start)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_frame_start
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

sll	$3,$fp,3
lw	$2,2904($23)
lw	$28,32($sp)
sw	$3,11076($23)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L1853
sw	$0,10312($23)
.set	macro
.set	reorder

li	$3,3			# 0x3
beq	$2,$3,$L2258
$L1854:
lw	$2,11244($23)
li	$3,1			# 0x1
beq	$2,$3,$L1862
li	$4,2			# 0x2
beq	$2,$4,$L1863
beq	$2,$0,$L2259
$L1861:
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L1867
li	$3,2			# 0x2
.set	macro
.set	reorder

beq	$2,$3,$L1868
beq	$2,$0,$L1866
$L1869:
lw	$5,%got(tcsm1_base)($28)
addiu	$3,$sp,40
li	$2,1			# 0x1
lw	$25,%call16(motion_config_vc1)($28)
move	$4,$23
lw	$22,%got(task_fifo_wp)($28)
lw	$21,%got(task_fifo_wp_d2)($28)
lw	$16,0($5)
sw	$3,204($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_config_vc1
1:	jalr	$25
sw	$2,10296($23)
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
lw	$28,32($sp)
addiu	$16,$16,16416
addiu	$3,$2,8192
lw	$6,%got(tcsm1_base)($28)
lw	$7,%got(tcsm0_fifo_rp)($28)
lw	$8,%got(tcsm1_fifo_wp)($28)
lw	$9,%got(task_fifo_wp_d1)($28)
lw	$5,0($6)
addiu	$6,$2,4
sw	$3,0($22)
sw	$3,0($9)
addiu	$4,$5,16384
sw	$0,16384($5)
sw	$6,0($7)
sw	$3,0($21)
sw	$4,0($8)
sw	$0,4($2)
lw	$14,160($23)
lw	$13,164($23)
lbu	$12,11228($23)
lbu	$11,11300($23)
lw	$10,11196($23)
lw	$9,2904($23)
lw	$8,11168($23)
lw	$7,11928($23)
lbu	$6,11860($23)
lw	$5,11180($23)
lw	$4,10328($23)
lw	$2,64($23)
lw	$3,11852($23)
sb	$14,40($sp)
sb	$13,41($sp)
sb	$12,42($sp)
sb	$11,43($sp)
sb	$10,44($sp)
sb	$9,45($sp)
sb	$8,46($sp)
sb	$7,47($sp)
sb	$6,48($sp)
sb	$5,49($sp)
sb	$4,50($sp)
sb	$3,51($sp)
sb	$2,52($sp)
lw	$3,0($23)
lbu	$6,11309($23)
lw	$5,192($23)
lw	$2,196($23)
lw	$3,656($3)
lw	$25,%call16(get_phy_addr)($28)
lw	$4,288($23)
sh	$6,54($sp)
sh	$5,56($sp)
sb	$3,53($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sh	$2,58($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,292($23)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,60($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,888($23)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,64($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,892($23)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,68($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,2088($23)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,72($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,2092($23)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,76($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
sw	$2,80($sp)
lw	$2,%got(rota_y)($28)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

lw	$28,32($sp)
sw	$2,84($sp)
lw	$2,%got(rota_c)($28)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

li	$6,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,40
move	$4,$16
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$2,88($sp)
.set	macro
.set	reorder

lw	$16,11932($23)
.set	noreorder
.set	nomacro
bne	$16,$0,$L2260
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$4,2904($23)
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$4,$3,$L2261
lw	$5,%got(vc1_hw_codingset1)($28)
.set	macro
.set	reorder

$L2327:
lw	$4,11260($23)
lw	$2,0($5)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1878
lw	$2,%got(cfg_all_table)($28)
.set	macro
.set	reorder

sll	$7,$4,4
sll	$6,$4,2
sw	$4,0($5)
sll	$3,$4,6
addu	$7,$2,$7
lw	$2,%got(sde_base)($28)
subu	$3,$3,$6
addu	$4,$3,$4
lw	$3,0($7)
lw	$5,0($2)
lw	$2,%got(hw_all_table)($28)
sll	$4,$4,5
.set	noreorder
.set	nomacro
blez	$3,$L1878
addu	$4,$2,$4
.set	macro
.set	reorder

addiu	$5,$5,8192
move	$6,$0
$L1879:
lw	$2,0($4)
addiu	$5,$5,4
addiu	$6,$6,1
sw	$2,-4($5)
lw	$2,0($7)
addiu	$3,$2,1
srl	$2,$3,31
addu	$2,$2,$3
sra	$2,$2,1
slt	$2,$6,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1879
addiu	$4,$4,4
.set	macro
.set	reorder

$L1878:
lw	$5,%got(vc1_hw_codingset2)($28)
lw	$4,11264($23)
lw	$2,0($5)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1877
lw	$2,%got(cfg_all_table)($28)
.set	macro
.set	reorder

sll	$7,$4,4
sll	$6,$4,2
sw	$4,0($5)
sll	$3,$4,6
addu	$7,$2,$7
lw	$2,%got(sde_base)($28)
subu	$3,$3,$6
addu	$4,$3,$4
lw	$3,0($7)
lw	$5,0($2)
lw	$2,%got(hw_all_table)($28)
sll	$4,$4,5
.set	noreorder
.set	nomacro
blez	$3,$L1877
addu	$4,$2,$4
.set	macro
.set	reorder

addiu	$5,$5,10240
move	$6,$0
$L1881:
lw	$2,0($4)
addiu	$5,$5,4
addiu	$6,$6,1
sw	$2,-4($5)
lw	$2,0($7)
addiu	$3,$2,1
srl	$2,$3,31
addu	$2,$2,$3
sra	$2,$2,1
slt	$2,$6,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1881
addiu	$4,$4,4
.set	macro
.set	reorder

$L1877:
li	$2,-2147483648			# 0xffffffff80000000
addiu	$3,$2,16384
$L1880:
#APP
# 4379 "vc1dec.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L1880
#APP
# 4379 "vc1dec.c" 1
sync
# 0 "" 2
#NO_APP
lw	$11,%got(aux_base)($28)
li	$3,1			# 0x1
li	$5,-201326592			# 0xfffffffff4000000
lw	$4,10332($23)
lw	$25,%call16(get_phy_addr)($28)
lw	$2,0($11)
sw	$3,0($2)
sw	$0,0($5)
sw	$3,0($2)
li	$3,2			# 0x2
sw	$3,0($2)
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$28,32($sp)
andi	$3,$2,0x3
lw	$4,10340($23)
sll	$3,$3,3
addu	$3,$3,$4
lw	$13,%got(vc1_hw_bs_buffer)($28)
srl	$4,$3,3
sw	$2,0($13)
addu	$2,$4,$2
li	$4,-4			# 0xfffffffffffffffc
and	$2,$2,$4
#APP
# 4397 "vc1dec.c" 1
mtc0	$2,$21,2
# 0 "" 2
#NO_APP
andi	$2,$3,0x1f
sll	$2,$2,5
li	$3,65536			# 0x10000
or	$2,$2,$3
#APP
# 4399 "vc1dec.c" 1
mtc0	$2,$21,3
# 0 "" 2
#NO_APP
$L1882:
#APP
# 4402 "vc1dec.c" 1
mfc0	$2,$21,3
# 0 "" 2
#NO_APP
and	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L1882
lw	$2,%got(vmau_base)($28)
.set	macro
.set	reorder

li	$4,4			# 0x4
lw	$3,0($2)
addiu	$3,$3,64
#APP
# 4411 "vc1dec.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($2)
li	$4,3			# 0x3
addiu	$3,$3,80
#APP
# 4412 "vc1dec.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($2)
li	$3,1048576			# 0x100000
addiu	$3,$3,16
addiu	$4,$4,116
#APP
# 4413 "vc1dec.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($2)
li	$3,1			# 0x1
addiu	$2,$2,68
#APP
# 4414 "vc1dec.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lbu	$4,11860($23)
li	$3,4096			# 0x1000
lw	$2,11092($23)
movz	$3,$0,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L1884
sw	$3,164($sp)
.set	macro
.set	reorder

lw	$3,11084($23)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1884
lw	$8,164($sp)
.set	macro
.set	reorder

li	$3,524288			# 0x80000
lbu	$5,11228($23)
or	$8,$8,$3
sltu	$4,$5,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L2057
sw	$8,196($sp)
.set	macro
.set	reorder

lw	$4,164($sp)
$L1888:
move	$6,$0
lbu	$2,11300($23)
$L2329:
andi	$5,$5,0x3f
lbu	$3,11309($23)
lw	$7,11168($23)
andi	$2,$2,0x3f
andi	$3,$3,0x1
sll	$3,$3,30
sll	$2,$2,6
or	$2,$2,$3
li	$3,-2147483648			# 0xffffffff80000000
or	$2,$2,$3
or	$5,$2,$5
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$7,$2,$L1890
sw	$5,156($sp)
.set	macro
.set	reorder

li	$2,1048576			# 0x100000
or	$4,$6,$4
addiu	$2,$2,4095
or	$2,$4,$2
sw	$2,188($sp)
$L1891:
lw	$2,164($23)
lw	$20,%got(dMB)($28)
.set	noreorder
.set	nomacro
blez	$2,$L1892
sw	$0,7996($23)
.set	macro
.set	reorder

sw	$fp,208($sp)
move	$fp,$23
$L1895:
lw	$25,%call16(jz_init_block_index)($28)
move	$4,$fp
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz_init_block_index
1:	jalr	$25
sw	$0,7992($fp)
.set	macro
.set	reorder

li	$3,3			# 0x3
lw	$28,32($sp)
lw	$4,%got(left_buf)($28)
lw	$2,0($4)
addiu	$2,$2,-4
$L1893:
#APP
# 4437 "vc1dec.c" 1
pref 30,4($2)
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 4437 "vc1dec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
bne	$3,$0,$L1893
lw	$8,7992($fp)
lw	$2,160($fp)
slt	$2,$8,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L2025
move	$23,$20
.set	macro
.set	reorder

lw	$5,0($22)
lw	$19,%got(dma_len)($28)
$L2024:
lw	$12,8012($fp)
addiu	$4,$5,828
lw	$11,8016($fp)
andi	$7,$5,0xffff
lw	$10,8020($fp)
andi	$4,$4,0xffff
lw	$9,8024($fp)
addiu	$12,$12,2
lw	$3,8028($fp)
addiu	$11,$11,2
lw	$2,8032($fp)
addiu	$10,$10,2
lw	$13,%got(tcsm0_fifo_rp)($28)
addiu	$9,$9,2
addiu	$3,$3,1
sw	$12,8012($fp)
addiu	$2,$2,1
sw	$11,8016($fp)
sw	$10,8020($fp)
sw	$9,8024($fp)
sw	$3,8028($fp)
sw	$2,8032($fp)
.set	noreorder
.set	nomacro
b	$L1897
lw	$6,0($13)
.set	macro
.set	reorder

$L2262:
.set	noreorder
.set	nomacro
bne	$2,$0,$L2320
li	$2,1			# 0x1
.set	macro
.set	reorder

$L1897:
lw	$2,0($6)
andi	$2,$2,0xffff
sltu	$3,$2,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L2262
sltu	$2,$2,$7
.set	macro
.set	reorder

li	$2,1			# 0x1
$L2320:
sw	$5,0($23)
andi	$8,$8,0x00ff
sw	$0,0($19)
li	$3,1			# 0x1
sh	$2,4($5)
sb	$8,2($5)
lbu	$2,7996($fp)
sb	$2,3($5)
lw	$2,2904($fp)
beq	$2,$3,$L1898
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L2263
li	$3,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L2264
lw	$3,156($sp)
.set	macro
.set	reorder

$L1900:
lw	$3,164($sp)
move	$4,$fp
lw	$9,%got(vc1_decode_b_mb_vlc)($28)
lw	$8,156($sp)
sw	$0,12($5)
addiu	$25,$9,%lo(vc1_decode_b_mb_vlc)
sw	$3,16($5)
sw	$8,20($5)
.reloc	1f,R_MIPS_JALR,vc1_decode_b_mb_vlc
1:	jalr	$25
lbu	$3,11208($fp)
lw	$2,0($23)
lw	$28,32($sp)
sb	$3,25($2)
lh	$6,7268($fp)
lh	$5,7272($fp)
lh	$4,7300($fp)
lh	$3,7304($fp)
sh	$6,44($2)
sh	$5,46($2)
sh	$4,48($2)
sh	$3,50($2)
$L1994:
#APP
# 4498 "vc1dec.c" 1
mfc0	$4,$21,3
# 0 "" 2
#NO_APP
srl	$4,$4,5
#APP
# 4499 "vc1dec.c" 1
mfc0	$3,$21,2
# 0 "" 2
#NO_APP
lw	$11,%got(vc1_hw_bs_buffer)($28)
andi	$2,$4,0x1f
srl	$2,$2,3
lw	$5,2904($fp)
andi	$4,$4,0x7
lw	$6,0($11)
subu	$3,$3,$6
addu	$2,$3,$2
sll	$2,$2,3
addu	$2,$2,$4
sw	$2,10340($fp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$5,$2,$L2014
li	$2,3			# 0x3
.set	macro
.set	reorder

beq	$5,$2,$L2265
$L2015:
lw	$2,0($19)
lw	$5,0($22)
sll	$2,$2,4
addiu	$2,$2,60
addu	$5,$5,$2
sw	$5,0($22)
$L2016:
lw	$13,%got(task_fifo_wp_d1)($28)
lw	$6,0($21)
lw	$3,7992($fp)
lw	$4,0($13)
subu	$2,$5,$4
sra	$2,$2,2
sll	$2,$2,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L2017
sh	$2,0($6)
.set	macro
.set	reorder

lw	$3,7996($fp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L2017
lw	$6,%got(tcsm1_base)($28)
.set	macro
.set	reorder

lw	$3,0($6)
sw	$2,16400($3)
$L2017:
andi	$2,$5,0xffff
addiu	$2,$2,828
sltu	$2,$2,16384
.set	noreorder
.set	nomacro
bne	$2,$0,$L2321
lw	$7,%got(tcsm1_fifo_wp)($28)
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
addiu	$2,$2,8192
move	$5,$2
sw	$2,0($22)
$L2321:
lw	$8,%got(task_fifo_wp_d1)($28)
sw	$4,0($21)
lw	$3,0($7)
sw	$5,0($8)
lw	$2,0($3)
addiu	$2,$2,1
sw	$2,0($3)
li	$3,1			# 0x1
lw	$2,2904($fp)
.set	noreorder
.set	nomacro
beq	$2,$3,$L2019
li	$3,3			# 0x3
.set	macro
.set	reorder

beq	$2,$3,$L2266
$L2020:
lw	$3,10340($fp)
lw	$2,11076($fp)
slt	$2,$2,$3
bne	$2,$0,$L2023
bltz	$3,$L2023
$L2021:
lw	$8,7992($fp)
lw	$2,160($fp)
addiu	$8,$8,1
slt	$2,$8,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L2024
sw	$8,7992($fp)
.set	macro
.set	reorder

move	$20,$23
$L2025:
lw	$2,7996($fp)
lw	$3,164($fp)
sw	$0,10296($fp)
addiu	$2,$2,1
slt	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L1895
sw	$2,7996($fp)
.set	macro
.set	reorder

move	$23,$fp
lw	$fp,208($sp)
$L1892:
lw	$11,%got(tcsm0_fifo_rp)($28)
move	$7,$0
lw	$2,%got(tcsm1_fifo_wp)($28)
li	$16,2			# 0x2
lw	$13,%got(task_fifo_wp_d1)($28)
li	$15,1			# 0x1
lw	$3,0($22)
li	$14,-1			# 0xffffffffffffffff
lw	$8,0($11)
li	$10,1			# 0x1
lw	$11,0($21)
li	$9,6			# 0x6
lw	$4,0($2)
li	$17,60			# 0x3c
lw	$6,0($13)
li	$12,3			# 0x3
li	$13,-201326592			# 0xfffffffff4000000
$L2032:
andi	$19,$3,0xffff
.set	noreorder
.set	nomacro
b	$L2027
addiu	$18,$19,828
.set	macro
.set	reorder

$L2267:
bne	$2,$0,$L2026
$L2027:
lw	$2,0($8)
andi	$2,$2,0xffff
sltu	$5,$2,$18
.set	noreorder
.set	nomacro
bne	$5,$0,$L2267
sltu	$2,$2,$19
.set	macro
.set	reorder

$L2026:
beq	$7,$16,$L2268
sh	$15,4($3)
move	$2,$0
$L2308:
sw	$0,12($3)
sw	$0,16($3)
sb	$0,24($3)
sb	$0,32($3)
addu	$5,$3,$2
$L2322:
addiu	$2,$2,1
sb	$0,6($5)
sb	$10,26($5)
.set	noreorder
.set	nomacro
bne	$2,$9,$L2322
addu	$5,$3,$2
.set	macro
.set	reorder

lw	$18,0($4)
addiu	$2,$3,60
addiu	$19,$13,8192
sh	$17,0($11)
andi	$5,$2,0xffff
addiu	$5,$5,828
addiu	$18,$18,1
addiu	$7,$7,1
sltu	$5,$5,16384
movz	$2,$19,$5
move	$11,$6
sw	$18,0($4)
beq	$7,$12,$L2269
move	$6,$2
.set	noreorder
.set	nomacro
b	$L2032
move	$3,$2
.set	macro
.set	reorder

$L2266:
lw	$2,11928($fp)
beq	$2,$0,$L2020
$L2019:
lw	$2,10340($fp)
lw	$3,11076($fp)
slt	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L2021
li	$3,112			# 0x70
.set	macro
.set	reorder

lw	$2,7996($fp)
lw	$7,7992($fp)
move	$5,$0
lw	$25,%call16(ff_er_add_slice)($28)
move	$6,$0
move	$4,$fp
sw	$3,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$28,32($sp)
lw	$2,11076($fp)
lw	$4,0($fp)
lw	$7,10340($fp)
lw	$6,%got($LC9)($28)
lw	$25,%call16(av_log)($28)
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC9)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2299
lw	$31,268($sp)
.set	macro
.set	reorder

$L2265:
lw	$2,11928($fp)
beq	$2,$0,$L2015
$L2014:
lw	$2,0($19)
lw	$5,0($22)
sll	$2,$2,4
addu	$5,$5,$2
.set	noreorder
.set	nomacro
b	$L2016
sw	$5,0($22)
.set	macro
.set	reorder

$L2263:
lw	$2,11928($fp)
beq	$2,$0,$L1900
$L1898:
lw	$4,168($fp)
addiu	$3,$5,24
lw	$7,7996($fp)
lw	$6,7992($fp)
lw	$9,188($sp)
mul	$8,$7,$4
lw	$11,156($sp)
lw	$2,2192($fp)
sw	$3,152($sp)
lw	$3,2188($fp)
lw	$13,152($sp)
addu	$4,$8,$6
li	$6,1114112			# 0x110000
sw	$13,0($22)
addiu	$6,$6,4369
sw	$6,12($5)
sw	$9,16($5)
sw	$11,20($5)
sll	$5,$4,2
lbu	$8,11228($fp)
addu	$2,$2,$5
li	$5,1			# 0x1
sw	$8,160($sp)
sw	$5,0($2)
lw	$2,8012($fp)
lw	$5,%got(ff_msmp4_mb_i_vlc)($28)
sll	$2,$2,2
addu	$2,$3,$2
lw	$5,4($5)
sh	$0,0($2)
sh	$0,2($2)
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,2
addu	$3,$5,$3
lh	$2,2($3)
.set	noreorder
.set	nomacro
bltz	$2,$L1901
lh	$3,0($3)
.set	macro
.set	reorder

move	$18,$3
$L1902:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$3,11168($fp)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L1903
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sw	$2,2824($fp)
lw	$3,152($sp)
$L2304:
addiu	$2,$3,-4
move	$3,$0
#APP
# 3630 "vc1dec.c" 1
.word	0b01110000000000110000000001101111	#S32I2M XR1,$3
# 0 "" 2
#NO_APP
li	$3,24			# 0x18
$L1924:
#APP
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 3630 "vc1dec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
bne	$3,$0,$L1924
addiu	$20,$fp,8012
.set	macro
.set	reorder

move	$17,$0
$L1993:
li	$2,5			# 0x5
slt	$12,$17,4
subu	$2,$2,$17
sra	$7,$18,$2
.set	noreorder
.set	nomacro
beq	$12,$0,$L1925
andi	$7,$7,0x1
.set	macro
.set	reorder

lw	$6,0($20)
lw	$8,172($fp)
lw	$3,2804($fp)
addiu	$4,$6,-1
subu	$5,$6,$8
subu	$4,$4,$8
addu	$5,$3,$5
addu	$4,$3,$4
addu	$3,$3,$6
lbu	$5,0($5)
lbu	$4,0($4)
lbu	$6,-1($3)
xor	$4,$4,$5
movz	$5,$6,$4
xor	$7,$7,$5
sll	$2,$7,$2
sb	$7,0($3)
lw	$3,11168($fp)
or	$18,$18,$2
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L2060
lw	$11,152($sp)
.set	macro
.set	reorder

lw	$4,0($19)
lw	$5,11260($fp)
sll	$4,$4,4
addu	$4,$11,$4
$L2059:
#APP
# 1839 "vc1dec.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
lw	$2,10284($fp)
.set	noreorder
.set	nomacro
beq	$12,$0,$L1933
sll	$3,$2,4
.set	macro
.set	reorder

lw	$2,%got(ff_msmp4_dc_luma_vlc)($28)
addu	$2,$2,$3
lw	$8,4($2)
$L1934:
li	$2,127			# 0x7f
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
addu	$2,$8,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L2270
lh	$2,0($2)
.set	macro
.set	reorder

$L1936:
addiu	$3,$3,-1
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bltz	$2,$L2271
lw	$6,%got($LC5)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L1941
li	$3,119			# 0x77
.set	macro
.set	reorder

lbu	$10,11228($fp)
lw	$3,0($20)
$L2301:
lw	$6,2780($fp)
lw	$9,2784($fp)
lw	$8,2728($fp)
sll	$3,$3,1
lw	$11,11196($fp)
movn	$9,$6,$12
addu	$8,$8,$3
#APP
# 1339 "vc1dec.c" 1
.word	0b01110001000101111111110011101010	#S16LDD XR3,$8,-2,PTN2
# 0 "" 2
#NO_APP
lw	$3,24($20)
subu	$3,$0,$3
sll	$3,$3,1
addu	$6,$8,$3
#APP
# 1340 "vc1dec.c" 1
.word	0b01110000110100000000000001101010	#S16LDD XR1,$6,0,PTN2
# 0 "" 2
#NO_APP
addiu	$3,$3,-2
addu	$3,$8,$3
#APP
# 1341 "vc1dec.c" 1
.word	0b01110000011100000000000010101010	#S16LDD XR2,$3,0,PTN2
# 0 "" 2
#NO_APP
lw	$3,10296($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1949
andi	$3,$17,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L1949
slt	$6,$10,9
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$6,$0,$L2323
lw	$13,%got(dcpred.7594)($28)
.set	macro
.set	reorder

bne	$11,$0,$L1951
$L2323:
sll	$3,$9,1
addiu	$13,$13,%lo(dcpred.7594)
addu	$3,$3,$13
lhu	$3,0($3)
$L1951:
#APP
# 1345 "vc1dec.c" 1
.word	0b01110000000000110000000001101111	#S32I2M XR1,$3
# 0 "" 2
# 1346 "vc1dec.c" 1
.word	0b01110000000100000100000010111001	#S32MOVZ XR2,XR0,XR1
# 0 "" 2
#NO_APP
$L1949:
lw	$6,7992($fp)
.set	noreorder
.set	nomacro
bne	$6,$0,$L1952
li	$3,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

li	$13,1			# 0x1
and	$3,$17,$3
.set	noreorder
.set	nomacro
beq	$3,$13,$L1952
slt	$10,$10,9
.set	macro
.set	reorder

bne	$10,$0,$L1953
.set	noreorder
.set	nomacro
beq	$11,$0,$L2324
lw	$3,%got(dcpred.7594)($28)
.set	macro
.set	reorder

move	$3,$0
$L1954:
#APP
# 1350 "vc1dec.c" 1
.word	0b01110000000000110000000010101111	#S32I2M XR2,$3
# 0 "" 2
# 1351 "vc1dec.c" 1
.word	0b01110000000100001000000011111001	#S32MOVZ XR3,XR0,XR2
# 0 "" 2
#NO_APP
$L1952:
#APP
# 1354 "vc1dec.c" 1
.word	0b01110011000000001000010100011000	#D32ADD XR4,XR1,XR2,XR0,SS
# 0 "" 2
# 1355 "vc1dec.c" 1
.word	0b01110011000000001000110101011000	#D32ADD XR5,XR3,XR2,XR0,SS
# 0 "" 2
# 1356 "vc1dec.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 1357 "vc1dec.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 1358 "vc1dec.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 1359 "vc1dec.c" 1
.word	0b01110000000101000101010011111001	#S32MOVN XR3,XR5,XR1
# 0 "" 2
# 1360 "vc1dec.c" 1
.word	0b01110000000000110000000011101110	#S32M2I XR3, $3
# 0 "" 2
# 1361 "vc1dec.c" 1
.word	0b01110000000011110000000101101110	#S32M2I XR5, $15
# 0 "" 2
#NO_APP
addu	$2,$2,$3
sltu	$15,$15,1
sll	$2,$2,16
sra	$2,$2,16
sh	$2,0($8)
.set	noreorder
.set	nomacro
beq	$7,$0,$L1955
sh	$2,0($4)
.set	macro
.set	reorder

lw	$16,2824($fp)
.set	noreorder
.set	nomacro
beq	$16,$0,$L2088
lw	$10,%got(wmv1_scantable)($28)
.set	macro
.set	reorder

beq	$15,$0,$L2089
addiu	$10,$10,192
$L1956:
lbu	$2,11228($fp)
sltu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$0,$L2090
li	$7,1			# 0x1
.set	macro
.set	reorder

lbu	$2,11240($fp)
sltu	$2,$0,$2
sw	$2,184($sp)
$L1957:
sll	$2,$5,2
sll	$5,$5,5
subu	$5,$5,$2
lw	$2,%got(residual_init_table)($28)
addu	$2,$2,$5
lw	$11,16($2)
lw	$8,8($2)
lw	$9,12($2)
lw	$3,20($2)
lw	$14,0($2)
lw	$13,4($2)
sw	$11,176($sp)
sw	$8,168($sp)
sw	$9,172($sp)
sw	$3,180($sp)
lw	$11,24($2)
#APP
# 1546 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 1547 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1551 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$5,1			# 0x1
li	$31,-1026			# 0xfffffffffffffbfe
li	$25,13			# 0xd
.set	noreorder
.set	nomacro
b	$L1981
li	$24,2			# 0x2
.set	macro
.set	reorder

$L2272:
slt	$2,$2,$14
addu	$7,$13,$7
xori	$2,$2,0x1
lb	$3,0($7)
lb	$7,1($7)
$L1959:
addu	$5,$5,$3
$L2300:
slt	$3,$5,64
beq	$3,$0,$L1979
bne	$2,$0,$L1980
#APP
# 1607 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 1608 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$10,$5
addiu	$5,$5,1
lbu	$3,0($2)
#APP
# 1610 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1611 "vc1dec.c" 1
.word	0b01110000000000110000001010101111	#S32I2M XR10,$3
# 0 "" 2
#NO_APP
sll	$3,$3,1
addu	$3,$4,$3
sh	$7,0($3)
#APP
# 1613 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L1981:
andi	$3,$2,0x400
.set	noreorder
.set	nomacro
beq	$3,$0,$L2272
sll	$7,$2,1
.set	macro
.set	reorder

andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L1960
move	$9,$0
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
subu	$9,$24,$9
beq	$9,$24,$L2273
$L1960:
#APP
# 1563 "vc1dec.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 1564 "vc1dec.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1565 "vc1dec.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
and	$2,$8,$31
sll	$7,$2,1
slt	$2,$2,$14
addu	$7,$13,$7
andi	$8,$8,0x1
xori	$2,$2,0x1
lb	$3,0($7)
.set	noreorder
.set	nomacro
bne	$9,$0,$L1962
lb	$7,1($7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L1963
lw	$9,172($sp)
.set	macro
.set	reorder

lw	$9,168($sp)
addu	$9,$9,$3
lbu	$9,0($9)
sw	$9,192($sp)
lw	$9,192($sp)
addu	$7,$7,$9
$L1965:
subu	$9,$0,$7
#APP
# 1579 "vc1dec.c" 1
movn	$7,$9,$8	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L2300
addu	$5,$5,$3
.set	macro
.set	reorder

$L1933:
lw	$2,%got(ff_msmp4_dc_chroma_vlc)($28)
addu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L1934
lw	$8,4($2)
.set	macro
.set	reorder

$L1941:
.set	noreorder
.set	nomacro
beq	$2,$3,$L2274
lbu	$10,11228($fp)
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$10,$3,$L2275
li	$3,29			# 0x1d
.set	macro
.set	reorder

li	$3,2			# 0x2
beq	$10,$3,$L2276
$L1945:
subu	$3,$0,$2
$L2302:
li	$6,13			# 0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
# 1865 "vc1dec.c" 1
movn	$2,$3,$6	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L2301
lw	$3,0($20)
.set	macro
.set	reorder

$L1980:
addu	$5,$10,$5
lbu	$2,0($5)
#APP
# 1617 "vc1dec.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 1618 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
sll	$2,$2,1
addu	$2,$4,$2
sh	$7,0($2)
$L1979:
lw	$11,%got(left_buf)($28)
.set	noreorder
.set	nomacro
beq	$12,$0,$L1982
lw	$8,0($11)
.set	macro
.set	reorder

sra	$7,$17,1
sll	$7,$7,4
$L1983:
sll	$2,$6,4
lw	$3,2812($fp)
sll	$6,$6,6
andi	$5,$17,0x5
subu	$2,$6,$2
sll	$5,$5,3
addu	$6,$8,$7
addu	$2,$5,$2
sll	$2,$2,1
.set	noreorder
.set	nomacro
beq	$16,$0,$L1984
addu	$2,$3,$2
.set	macro
.set	reorder

beq	$15,$0,$L1985
lhu	$3,2($6)
lhu	$5,16($4)
addu	$3,$3,$5
sh	$3,16($4)
#APP
# 1919 "vc1dec.c" 1
.word	0b01110000100000000100000110101010	#S16LDD XR6,$4,32,PTN0
# 0 "" 2
# 1920 "vc1dec.c" 1
.word	0b01110000100010000110000110101010	#S16LDD XR6,$4,48,PTN1
# 0 "" 2
# 1921 "vc1dec.c" 1
.word	0b01110000100000001000000111101010	#S16LDD XR7,$4,64,PTN0
# 0 "" 2
# 1922 "vc1dec.c" 1
.word	0b01110000100010001010000111101010	#S16LDD XR7,$4,80,PTN1
# 0 "" 2
# 1923 "vc1dec.c" 1
.word	0b01110000100000001100001000101010	#S16LDD XR8,$4,96,PTN0
# 0 "" 2
# 1924 "vc1dec.c" 1
.word	0b01110000100010001110001000101010	#S16LDD XR8,$4,112,PTN1
# 0 "" 2
# 1926 "vc1dec.c" 1
.word	0b01110000110000000000010001010000	#S32LDD XR1,$6,4
# 0 "" 2
# 1927 "vc1dec.c" 1
.word	0b01110000110000000000100010010000	#S32LDD XR2,$6,8
# 0 "" 2
# 1928 "vc1dec.c" 1
.word	0b01110000110000000000110011010000	#S32LDD XR3,$6,12
# 0 "" 2
# 1930 "vc1dec.c" 1
.word	0b01110000000000000101101001001110	#Q16ADD XR9,XR6,XR1,XR0,AA,WW
# 0 "" 2
# 1931 "vc1dec.c" 1
.word	0b01110000000000001001111010001110	#Q16ADD XR10,XR7,XR2,XR0,AA,WW
# 0 "" 2
# 1932 "vc1dec.c" 1
.word	0b01110000000000001110000100001110	#Q16ADD XR4,XR8,XR3,XR0,AA,WW
# 0 "" 2
#NO_APP
li	$3,524288			# 0x80000
#APP
# 1934 "vc1dec.c" 1
.word	0b01110000000000110000000101101111	#S32I2M XR5,$3
# 0 "" 2
# 1935 "vc1dec.c" 1
.word	0b01110000010000000001010110110000	#D32SLL XR6,XR5,XR0,XR0,1
# 0 "" 2
# 1936 "vc1dec.c" 1
.word	0b01110000000000110000001100101111	#S32I2M XR12,$3
# 0 "" 2
# 1937 "vc1dec.c" 1
.word	0b01110000100000000100001001101011	#S16STD XR9,$4,32,PTN0
# 0 "" 2
# 1938 "vc1dec.c" 1
.word	0b01110000100010000110001001101011	#S16STD XR9,$4,48,PTN1
# 0 "" 2
# 1939 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 1940 "vc1dec.c" 1
.word	0b01110000000011010110011100111001	#D16MOVN XR12,XR9,XR5
# 0 "" 2
# 1942 "vc1dec.c" 1
.word	0b01110000100000001000001010101011	#S16STD XR10,$4,64,PTN0
# 0 "" 2
# 1943 "vc1dec.c" 1
.word	0b01110000100010001010001010101011	#S16STD XR10,$4,80,PTN1
# 0 "" 2
# 1944 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 1945 "vc1dec.c" 1
.word	0b01110000000011010110101100111001	#D16MOVN XR12,XR10,XR5
# 0 "" 2
# 1947 "vc1dec.c" 1
.word	0b01110000100000001100000100101011	#S16STD XR4,$4,96,PTN0
# 0 "" 2
# 1948 "vc1dec.c" 1
.word	0b01110000100010001110000100101011	#S16STD XR4,$4,112,PTN1
# 0 "" 2
# 1949 "vc1dec.c" 1
.word	0b01110000100000010101100101001110	#Q16ADD XR5,XR6,XR5,XR0,AA,HW
# 0 "" 2
# 1950 "vc1dec.c" 1
.word	0b01110000000011010101001100111001	#D16MOVN XR12,XR4,XR5
# 0 "" 2
# 1951 "vc1dec.c" 1
.word	0b01110011001010110000001100111101	#S32SFL XR12,XR0,XR12,XR10,PTN3
# 0 "" 2
# 1952 "vc1dec.c" 1
.word	0b01110000000000101011001100000011	#S32MAX XR12,XR12,XR10
# 0 "" 2
# 1953 "vc1dec.c" 1
.word	0b01110000000000110010111011000011	#S32MAX XR11,XR11,XR12
# 0 "" 2
#NO_APP
$L1984:
lh	$3,16($4)
$L2303:
sh	$3,2($6)
lh	$3,2($4)
sh	$3,2($2)
#APP
# 1992 "vc1dec.c" 1
.word	0b01110000100000000100000110101010	#S16LDD XR6,$4,32,PTN0
# 0 "" 2
# 1993 "vc1dec.c" 1
.word	0b01110000100010000110000110101010	#S16LDD XR6,$4,48,PTN1
# 0 "" 2
# 1994 "vc1dec.c" 1
.word	0b01110000100000001000000111101010	#S16LDD XR7,$4,64,PTN0
# 0 "" 2
# 1995 "vc1dec.c" 1
.word	0b01110000100010001010000111101010	#S16LDD XR7,$4,80,PTN1
# 0 "" 2
# 1996 "vc1dec.c" 1
.word	0b01110000100000001100001000101010	#S16LDD XR8,$4,96,PTN0
# 0 "" 2
# 1997 "vc1dec.c" 1
.word	0b01110000100010001110001000101010	#S16LDD XR8,$4,112,PTN1
# 0 "" 2
# 1999 "vc1dec.c" 1
.word	0b01110000110000000000010110010001	#S32STD XR6,$6,4
# 0 "" 2
# 2000 "vc1dec.c" 1
.word	0b01110000110000000000100111010001	#S32STD XR7,$6,8
# 0 "" 2
# 2001 "vc1dec.c" 1
.word	0b01110000110000000000111000010001	#S32STD XR8,$6,12
# 0 "" 2
# 2003 "vc1dec.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 2004 "vc1dec.c" 1
.word	0b01110000100000000000100011010000	#S32LDD XR3,$4,8
# 0 "" 2
# 2005 "vc1dec.c" 1
.word	0b01110000100000000000110100010000	#S32LDD XR4,$4,12
# 0 "" 2
# 2007 "vc1dec.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2008 "vc1dec.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2009 "vc1dec.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
#NO_APP
lw	$2,2824($fp)
li	$3,63			# 0x3f
li	$4,1			# 0x1
movn	$4,$3,$2
$L1992:
lw	$3,0($23)
#APP
# 2151 "vc1dec.c" 1
.word	0b01110000000000100000001011101110	#S32M2I XR11, $2
# 0 "" 2
#NO_APP
sra	$2,$2,3
addu	$3,$3,$17
addiu	$2,$2,1
andi	$2,$2,0x00ff
sb	$2,6($3)
sw	$4,668($20)
$L1940:
lbu	$2,6($3)
lw	$3,0($19)
andi	$2,$2,0x00ff
addu	$2,$2,$3
sw	$2,0($19)
$L1932:
addiu	$17,$17,1
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
bne	$17,$2,$L1993
addiu	$20,$20,4
.set	macro
.set	reorder

b	$L1994
$L1955:
lw	$9,%got(left_buf)($28)
.set	noreorder
.set	nomacro
beq	$12,$0,$L2277
lw	$7,0($9)
.set	macro
.set	reorder

sra	$5,$17,1
sll	$5,$5,4
$L1987:
sll	$2,$6,4
lw	$8,2812($fp)
sll	$6,$6,6
lw	$9,2824($fp)
andi	$3,$17,0x5
subu	$2,$6,$2
sll	$3,$3,3
addu	$5,$7,$5
addu	$2,$3,$2
sll	$2,$2,1
.set	noreorder
.set	nomacro
beq	$9,$0,$L1988
addu	$6,$8,$2
.set	macro
.set	reorder

beq	$15,$0,$L1989
lh	$2,2($5)
sh	$2,16($4)
#APP
# 2034 "vc1dec.c" 1
.word	0b01110000101000000000010001010000	#S32LDD XR1,$5,4
# 0 "" 2
# 2035 "vc1dec.c" 1
.word	0b01110000101000000000100010010000	#S32LDD XR2,$5,8
# 0 "" 2
# 2036 "vc1dec.c" 1
.word	0b01110000101000000000110011010000	#S32LDD XR3,$5,12
# 0 "" 2
# 2038 "vc1dec.c" 1
.word	0b01110000100000000100000001101011	#S16STD XR1,$4,32,PTN0
# 0 "" 2
# 2039 "vc1dec.c" 1
.word	0b01110000100010000110000001101011	#S16STD XR1,$4,48,PTN1
# 0 "" 2
# 2040 "vc1dec.c" 1
.word	0b01110000100000001000000010101011	#S16STD XR2,$4,64,PTN0
# 0 "" 2
# 2041 "vc1dec.c" 1
.word	0b01110000100010001010000010101011	#S16STD XR2,$4,80,PTN1
# 0 "" 2
# 2042 "vc1dec.c" 1
.word	0b01110000100000001100000011101011	#S16STD XR3,$4,96,PTN0
# 0 "" 2
# 2043 "vc1dec.c" 1
.word	0b01110000100010001110000011101011	#S16STD XR3,$4,112,PTN1
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
li	$7,8			# 0x8
sll	$2,$3,4
$L2325:
addu	$2,$4,$2
lh	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1990
sll	$5,$3,3
.set	macro
.set	reorder

#APP
# 2046 "vc1dec.c" 1
.word	0b01110000000001010000001010101111	#S32I2M XR10,$5
# 0 "" 2
# 2047 "vc1dec.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
$L1990:
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$3,$7,$L2325
sll	$2,$3,4
.set	macro
.set	reorder

#APP
# 2075 "vc1dec.c" 1
.word	0b01110000110000000000000000010001	#S32STD XR0,$6,0
# 0 "" 2
# 2076 "vc1dec.c" 1
.word	0b01110000110000000000010000010001	#S32STD XR0,$6,4
# 0 "" 2
# 2077 "vc1dec.c" 1
.word	0b01110000110000000000100000010001	#S32STD XR0,$6,8
# 0 "" 2
# 2078 "vc1dec.c" 1
.word	0b01110000110000000000110000010001	#S32STD XR0,$6,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1992
li	$4,63			# 0x3f
.set	macro
.set	reorder

$L1925:
sll	$2,$7,$2
lw	$3,11168($fp)
or	$18,$18,$2
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L2060
lw	$9,152($sp)
.set	macro
.set	reorder

lw	$4,0($19)
lw	$5,11264($fp)
sll	$4,$4,4
.set	noreorder
.set	nomacro
b	$L2059
addu	$4,$9,$4
.set	macro
.set	reorder

$L2060:
lw	$2,10296($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1928
li	$2,1			# 0x1
.set	macro
.set	reorder

addiu	$2,$17,-2
sltu	$2,$2,2
$L1928:
lw	$3,7992($fp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L2085
sw	$2,11272($fp)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$17,$2,$L1930
xori	$3,$17,0x3
.set	macro
.set	reorder

sltu	$3,$3,1
$L1929:
lw	$5,0($19)
lw	$2,11260($fp)
lw	$4,11264($fp)
lw	$8,152($sp)
sll	$5,$5,4
sw	$3,11276($fp)
movz	$2,$4,$12
addu	$5,$8,$5
$L1931:
lw	$11,%got(vc1_decode_i_block_adv)($28)
move	$4,$fp
lw	$9,160($sp)
move	$6,$17
sw	$2,16($sp)
addiu	$25,$11,%lo(vc1_decode_i_block_adv)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vc1_decode_i_block_adv
1:	jalr	$25
sw	$9,20($sp)
.set	macro
.set	reorder

lw	$3,0($23)
lw	$4,0($19)
lw	$28,32($sp)
addu	$3,$3,$17
lbu	$2,6($3)
andi	$2,$2,0x00ff
addu	$2,$2,$4
.set	noreorder
.set	nomacro
b	$L1932
sw	$2,0($19)
.set	macro
.set	reorder

$L1953:
lw	$3,%got(dcpred.7594)($28)
$L2324:
sll	$9,$9,1
addiu	$3,$3,%lo(dcpred.7594)
addu	$9,$9,$3
.set	noreorder
.set	nomacro
b	$L1954
lhu	$3,0($9)
.set	macro
.set	reorder

$L2270:
li	$9,125			# 0x7d
#APP
# 194 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 195 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
nor	$6,$0,$3
sll	$6,$6,4
ori	$6,$6,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$2
sll	$2,$2,2
addu	$2,$8,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L1936
lh	$2,0($2)
.set	macro
.set	reorder

#APP
# 203 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 204 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$6,$3,4
ori	$3,$6,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$2
sll	$2,$2,2
addu	$2,$8,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L1936
lh	$2,0($2)
.set	macro
.set	reorder

ori	$6,$6,0xd
#APP
# 147 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$3,$3,4
ori	$3,$3,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$2
sll	$3,$2,2
addu	$3,$8,$3
lh	$2,0($3)
.set	noreorder
.set	nomacro
b	$L1936
lh	$3,2($3)
.set	macro
.set	reorder

$L1962:
.set	noreorder
.set	nomacro
beq	$2,$0,$L2222
lw	$9,180($sp)
.set	macro
.set	reorder

lw	$9,176($sp)
$L2222:
addu	$9,$9,$7
lbu	$9,0($9)
addiu	$9,$9,1
andi	$9,$9,0x00ff
sw	$9,192($sp)
lw	$9,192($sp)
addu	$3,$3,$9
subu	$9,$0,$7
#APP
# 1579 "vc1dec.c" 1
movn	$7,$9,$8	#i_movn
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L2300
addu	$5,$5,$3
.set	macro
.set	reorder

$L2090:
.set	noreorder
.set	nomacro
b	$L1957
sw	$7,184($sp)
.set	macro
.set	reorder

$L2088:
.set	noreorder
.set	nomacro
b	$L1956
addiu	$10,$10,64
.set	macro
.set	reorder

$L1982:
.set	noreorder
.set	nomacro
b	$L1983
sll	$7,$17,4
.set	macro
.set	reorder

$L2277:
.set	noreorder
.set	nomacro
b	$L1987
sll	$5,$17,4
.set	macro
.set	reorder

$L1988:
#APP
# 2139 "vc1dec.c" 1
.word	0b01110000101000000000000000010001	#S32STD XR0,$5,0
# 0 "" 2
# 2140 "vc1dec.c" 1
.word	0b01110000101000000000010000010001	#S32STD XR0,$5,4
# 0 "" 2
# 2141 "vc1dec.c" 1
.word	0b01110000101000000000100000010001	#S32STD XR0,$5,8
# 0 "" 2
# 2142 "vc1dec.c" 1
.word	0b01110000101000000000110000010001	#S32STD XR0,$5,12
# 0 "" 2
# 2144 "vc1dec.c" 1
.word	0b01110000110000000000000000010001	#S32STD XR0,$6,0
# 0 "" 2
# 2145 "vc1dec.c" 1
.word	0b01110000110000000000010000010001	#S32STD XR0,$6,4
# 0 "" 2
# 2146 "vc1dec.c" 1
.word	0b01110000110000000000100000010001	#S32STD XR0,$6,8
# 0 "" 2
# 2147 "vc1dec.c" 1
.word	0b01110000110000000000110000010001	#S32STD XR0,$6,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1992
move	$4,$0
.set	macro
.set	reorder

$L2085:
.set	noreorder
.set	nomacro
b	$L1929
li	$3,1			# 0x1
.set	macro
.set	reorder

$L2264:
move	$4,$fp
lw	$25,%got(vc1_decode_p_mb_vlc)($28)
lw	$13,196($sp)
sw	$0,12($5)
addiu	$25,$25,%lo(vc1_decode_p_mb_vlc)
sw	$13,16($5)
sw	$3,20($5)
.reloc	1f,R_MIPS_JALR,vc1_decode_p_mb_vlc
1:	jalr	$25
lw	$28,32($sp)
lbu	$3,11208($fp)
lw	$2,%got(dMB)($28)
lw	$2,0($2)
sb	$3,25($2)
lbu	$3,24($2)
andi	$3,$3,0x00ff
bne	$3,$0,$L1995
lbu	$3,32($2)
andi	$3,$3,0x00ff
bne	$3,$0,$L1996
lbu	$3,26($2)
andi	$3,$3,0x00ff
bne	$3,$0,$L1994
$L1996:
lw	$3,288($fp)
beq	$3,$0,$L1994
lw	$4,8012($fp)
lw	$3,2188($fp)
lhu	$5,44($2)
sll	$4,$4,2
addu	$3,$3,$4
sh	$5,0($3)
lhu	$2,46($2)
.set	noreorder
.set	nomacro
b	$L1994
sh	$2,2($3)
.set	macro
.set	reorder

$L2274:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$10,$2,$L2278
li	$2,125			# 0x7d
.set	macro
.set	reorder

li	$2,2			# 0x2
beq	$10,$2,$L2279
li	$2,125			# 0x7d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L2302
subu	$3,$0,$2
.set	macro
.set	reorder

$L1963:
addu	$9,$9,$3
lbu	$9,0($9)
sw	$9,192($sp)
lw	$9,192($sp)
.set	noreorder
.set	nomacro
b	$L1965
addu	$7,$7,$9
.set	macro
.set	reorder

$L1989:
lh	$2,2($6)
sh	$2,2($4)
#APP
# 2094 "vc1dec.c" 1
.word	0b01110000110000000000010001010000	#S32LDD XR1,$6,4
# 0 "" 2
# 2095 "vc1dec.c" 1
.word	0b01110000110000000000100010010000	#S32LDD XR2,$6,8
# 0 "" 2
# 2096 "vc1dec.c" 1
.word	0b01110000110000000000110011010000	#S32LDD XR3,$6,12
# 0 "" 2
# 2098 "vc1dec.c" 1
.word	0b01110000100000000000010001010001	#S32STD XR1,$4,4
# 0 "" 2
# 2099 "vc1dec.c" 1
.word	0b01110000100000000000100010010001	#S32STD XR2,$4,8
# 0 "" 2
# 2100 "vc1dec.c" 1
.word	0b01110000100000000000110011010001	#S32STD XR3,$4,12
# 0 "" 2
# 2125 "vc1dec.c" 1
.word	0b01110000101000000000000000010001	#S32STD XR0,$5,0
# 0 "" 2
# 2126 "vc1dec.c" 1
.word	0b01110000101000000000010000010001	#S32STD XR0,$5,4
# 0 "" 2
# 2127 "vc1dec.c" 1
.word	0b01110000101000000000100000010001	#S32STD XR0,$5,8
# 0 "" 2
# 2128 "vc1dec.c" 1
.word	0b01110000101000000000110000010001	#S32STD XR0,$5,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1992
li	$4,63			# 0x3f
.set	macro
.set	reorder

$L1985:
lhu	$3,2($4)
lhu	$5,2($2)
addu	$3,$3,$5
sh	$3,2($4)
#APP
# 1966 "vc1dec.c" 1
.word	0b01110000010000000000010110010000	#S32LDD XR6,$2,4
# 0 "" 2
# 1967 "vc1dec.c" 1
.word	0b01110000010000000000100111010000	#S32LDD XR7,$2,8
# 0 "" 2
# 1968 "vc1dec.c" 1
.word	0b01110000010000000000111000010000	#S32LDD XR8,$2,12
# 0 "" 2
# 1970 "vc1dec.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 1971 "vc1dec.c" 1
.word	0b01110000100000000000100011010000	#S32LDD XR3,$4,8
# 0 "" 2
# 1972 "vc1dec.c" 1
.word	0b01110000100000000000110100010000	#S32LDD XR4,$4,12
# 0 "" 2
# 1974 "vc1dec.c" 1
.word	0b01110000000000011000100010001110	#Q16ADD XR2,XR2,XR6,XR0,AA,WW
# 0 "" 2
# 1975 "vc1dec.c" 1
.word	0b01110000000000011100110011001110	#Q16ADD XR3,XR3,XR7,XR0,AA,WW
# 0 "" 2
# 1976 "vc1dec.c" 1
.word	0b01110000000000100001000100001110	#Q16ADD XR4,XR4,XR8,XR0,AA,WW
# 0 "" 2
# 1978 "vc1dec.c" 1
.word	0b01110000100000000000010010010001	#S32STD XR2,$4,4
# 0 "" 2
# 1979 "vc1dec.c" 1
.word	0b01110000100000000000100011010001	#S32STD XR3,$4,8
# 0 "" 2
# 1980 "vc1dec.c" 1
.word	0b01110000100000000000110100010001	#S32STD XR4,$4,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L2303
lh	$3,16($4)
.set	macro
.set	reorder

$L2089:
.set	noreorder
.set	nomacro
b	$L1956
addiu	$10,$10,128
.set	macro
.set	reorder

$L2275:
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
addiu	$6,$2,-3
.set	noreorder
.set	nomacro
b	$L1945
addu	$2,$6,$3
.set	macro
.set	reorder

$L1995:
sb	$0,38($2)
lw	$3,288($fp)
beq	$3,$0,$L1994
lw	$3,64($fp)
andi	$3,$3,0x2000
.set	noreorder
.set	nomacro
bne	$3,$0,$L1994
move	$5,$0
.set	macro
.set	reorder

lw	$12,11284($fp)
addiu	$11,$sp,124
addiu	$10,$sp,108
addiu	$8,$sp,92
li	$9,4			# 0x4
$L1999:
addiu	$4,$5,10
sll	$6,$5,2
sll	$4,$4,2
addu	$16,$fp,$3
addu	$4,$2,$4
addu	$6,$2,$6
addu	$15,$11,$3
lhu	$7,4($4)
addu	$14,$10,$3
lw	$4,8012($16)
addu	$13,$8,$3
lhu	$6,46($6)
addiu	$5,$5,1
sll	$7,$7,16
addu	$4,$12,$4
sll	$6,$6,16
sra	$7,$7,16
lbu	$4,0($4)
sra	$6,$6,16
addiu	$3,$3,4
sw	$7,0($15)
sw	$6,0($14)
.set	noreorder
.set	nomacro
bne	$5,$9,$L1999
sw	$4,0($13)
.set	macro
.set	reorder

lw	$6,100($sp)
lw	$4,104($sp)
lw	$7,96($sp)
lw	$9,92($sp)
sll	$3,$6,2
sll	$4,$4,3
sll	$10,$7,1
or	$3,$4,$3
or	$3,$3,$9
or	$3,$3,$10
.set	noreorder
.set	nomacro
bne	$3,$0,$L2000
lw	$11,%got(count.8094)($28)
.set	macro
.set	reorder

addiu	$3,$2,44
#APP
# 3968 "vc1dec.c" 1
.word	0b01110000011000000000000001010000	#S32LDD XR1,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,48
#APP
# 3969 "vc1dec.c" 1
.word	0b01110000011000000000000010010000	#S32LDD XR2,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,52
#APP
# 3970 "vc1dec.c" 1
.word	0b01110000011000000000000011010000	#S32LDD XR3,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,56
#APP
# 3971 "vc1dec.c" 1
.word	0b01110000011000000000000100010000	#S32LDD XR4,$3,0
# 0 "" 2
# 3972 "vc1dec.c" 1
.word	0b01110000000010001000010101000011	#D16MAX XR5,XR1,XR2
# 0 "" 2
# 3973 "vc1dec.c" 1
.word	0b01110000000010010000110110000011	#D16MAX XR6,XR3,XR4
# 0 "" 2
# 3975 "vc1dec.c" 1
.word	0b01110000000011001000010111000011	#D16MIN XR7,XR1,XR2
# 0 "" 2
# 3976 "vc1dec.c" 1
.word	0b01110000000011010000111000000011	#D16MIN XR8,XR3,XR4
# 0 "" 2
# 3978 "vc1dec.c" 1
.word	0b01110000000011011001010001000011	#D16MIN XR1,XR5,XR6
# 0 "" 2
# 3979 "vc1dec.c" 1
.word	0b01110000000010100001110010000011	#D16MAX XR2,XR7,XR8
# 0 "" 2
# 3981 "vc1dec.c" 1
.word	0b01110001000000001000010011001110	#Q16ADD XR3,XR1,XR2,XR0,AS,WW
# 0 "" 2
# 3982 "vc1dec.c" 1
.word	0b01110000000001000000000011101110	#S32M2I XR3, $4
# 0 "" 2
# 3983 "vc1dec.c" 1
.word	0b01110000000000110000000011101110	#S32M2I XR3, $3
# 0 "" 2
#NO_APP
sll	$6,$4,16
sra	$5,$3,16
sra	$6,$6,16
srl	$3,$5,31
srl	$4,$6,31
addu	$3,$3,$5
addu	$4,$4,$6
sra	$3,$3,1
sra	$4,$4,1
$L2001:
lw	$6,8012($fp)
lw	$5,2188($fp)
sll	$6,$6,2
addu	$5,$5,$6
sh	$4,0($5)
sh	$3,2($5)
sh	$4,40($2)
sh	$3,42($2)
b	$L1994
$L2276:
li	$3,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,1
addiu	$6,$2,-1
.set	noreorder
.set	nomacro
b	$L1945
addu	$2,$6,$3
.set	macro
.set	reorder

$L1903:
lw	$2,11892($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1905
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sw	$2,2824($fp)
$L1906:
lbu	$3,11904($fp)
li	$2,2			# 0x2
beq	$3,$2,$L2280
$L1907:
lbu	$2,11240($fp)
$L2306:
.set	noreorder
.set	nomacro
beq	$2,$0,$L1908
li	$2,3			# 0x3
.set	macro
.set	reorder

lbu	$3,11241($fp)
.set	noreorder
.set	nomacro
beq	$3,$2,$L2281
li	$5,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$5,$L2282
li	$5,1			# 0x1
.set	macro
.set	reorder

beq	$3,$5,$L2283
bne	$3,$0,$L1908
lw	$2,7992($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L2101
li	$2,15			# 0xf
.set	macro
.set	reorder

$L1920:
lw	$3,10296($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L2307
andi	$3,$2,0x4
.set	macro
.set	reorder

lbu	$8,11229($fp)
sw	$8,160($sp)
$L2307:
.set	noreorder
.set	nomacro
beq	$3,$0,$L1923
andi	$2,$2,0x8
.set	macro
.set	reorder

lw	$3,160($fp)
lw	$5,7992($fp)
addiu	$3,$3,-1
beq	$5,$3,$L2284
$L1923:
beq	$2,$0,$L1908
lw	$2,164($fp)
lw	$3,7996($fp)
addiu	$2,$2,-1
beq	$3,$2,$L2285
$L1908:
lw	$3,2172($fp)
lw	$13,160($sp)
addu	$4,$3,$4
andi	$2,$13,0x3f
sb	$13,0($4)
sll	$2,$2,12
lw	$4,2788($fp)
lw	$3,2792($fp)
lw	$5,0($23)
addu	$4,$4,$13
addu	$3,$3,$13
lbu	$6,0($4)
lw	$4,20($5)
sw	$6,2780($fp)
or	$2,$2,$4
lbu	$3,0($3)
sw	$3,2784($fp)
sw	$2,20($5)
.set	noreorder
.set	nomacro
b	$L2304
lw	$3,152($sp)
.set	macro
.set	reorder

$L2284:
lbu	$9,11229($fp)
.set	noreorder
.set	nomacro
b	$L1923
sw	$9,160($sp)
.set	macro
.set	reorder

$L1901:
li	$6,125			# 0x7d
#APP
# 147 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 148 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 137 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 138 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 139 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$2,$2,$3
sll	$2,$2,2
addu	$5,$5,$2
lh	$18,0($5)
.set	noreorder
.set	nomacro
b	$L1902
lh	$2,2($5)
.set	macro
.set	reorder

$L2273:
lw	$2,10312($fp)
lw	$3,10316($fp)
sw	$2,228($sp)
sw	$3,192($sp)
#APP
# 90 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$7,228($sp)
.set	noreorder
.set	nomacro
beq	$7,$0,$L1968
lw	$8,184($sp)
.set	macro
.set	reorder

addiu	$9,$7,-1
andi	$9,$9,0x7
sll	$9,$9,4
ori	$9,$9,0xd
$L1969:
lw	$7,192($sp)
slt	$3,$7,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L2286
li	$8,125			# 0x7d
.set	macro
.set	reorder

addiu	$3,$7,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
$L1976:
#APP
# 90 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
sw	$8,200($sp)
lw	$8,228($sp)
slt	$7,$8,9
beq	$7,$0,$L2287
#APP
# 90 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
$L1978:
subu	$8,$0,$7
lw	$9,200($sp)
#APP
# 1598 "vc1dec.c" 1
movn	$7,$8,$9	#i_movn
# 0 "" 2
#NO_APP
lw	$8,228($sp)
lw	$9,192($sp)
sw	$8,10312($fp)
.set	noreorder
.set	nomacro
b	$L1959
sw	$9,10316($fp)
.set	macro
.set	reorder

$L2278:
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
li	$2,29			# 0x1d
#APP
# 82 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,2
.set	noreorder
.set	nomacro
b	$L1945
or	$2,$3,$2
.set	macro
.set	reorder

$L1884:
lbu	$5,11228($23)
sltu	$3,$5,9
.set	noreorder
.set	nomacro
bne	$3,$0,$L2081
lw	$8,164($sp)
.set	macro
.set	reorder

lw	$3,11196($23)
.set	noreorder
.set	nomacro
bne	$3,$0,$L2082
lw	$9,164($sp)
.set	macro
.set	reorder

lw	$3,164($sp)
sw	$3,196($sp)
$L1886:
move	$4,$0
$L1887:
lw	$11,164($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1888
or	$4,$4,$11
.set	macro
.set	reorder

$L2062:
lw	$2,11084($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1888
li	$6,524288			# 0x80000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2329
lbu	$2,11300($23)
.set	macro
.set	reorder

$L1905:
lw	$2,11888($fp)
addu	$2,$2,$4
lbu	$2,0($2)
.set	noreorder
.set	nomacro
b	$L1906
sw	$2,2824($fp)
.set	macro
.set	reorder

$L2255:
lw	$2,0($23)
lw	$2,132($2)
lw	$2,32($2)
andi	$2,$2,0x80
.set	noreorder
.set	nomacro
b	$L1800
movn	$17,$6,$2
.set	macro
.set	reorder

$L1968:
.set	noreorder
.set	nomacro
beq	$8,$0,$L2092
li	$9,45			# 0x2d
.set	macro
.set	reorder

#APP
# 90 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$0,$L1971
sw	$3,228($sp)
.set	macro
.set	reorder

addiu	$9,$3,-1
andi	$9,$9,0x7
sll	$9,$9,4
ori	$9,$9,0xd
$L1972:
li	$3,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L1969
sw	$3,192($sp)
.set	macro
.set	reorder

$L2279:
li	$2,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
li	$2,13			# 0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 84 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,1
.set	noreorder
.set	nomacro
b	$L1945
or	$2,$3,$2
.set	macro
.set	reorder

$L2023:
lw	$2,7996($fp)
li	$3,112			# 0x70
lw	$7,7992($fp)
move	$5,$0
lw	$25,%call16(ff_er_add_slice)($28)
move	$6,$0
move	$4,$fp
sw	$3,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$2,11076($fp)
lw	$4,0($fp)
lw	$7,10340($fp)
lw	$28,32($sp)
sw	$2,16($sp)
lw	$2,7992($fp)
lw	$6,%got($LC10)($28)
lw	$25,%call16(av_log)($28)
sw	$2,20($sp)
lw	$2,7996($fp)
addiu	$6,$6,%lo($LC10)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2299
lw	$31,268($sp)
.set	macro
.set	reorder

$L2046:
lw	$2,10096($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L2044
li	$2,-2147483648			# 0xffffffff80000000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2047
addiu	$3,$2,16384
.set	macro
.set	reorder

$L2000:
sll	$4,$3,2
li	$10,1			# 0x1
addiu	$11,$11,%lo(count.8094)
addu	$4,$4,$11
lw	$4,0($4)
beq	$4,$10,$L2288
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$4,$3,$L2289
move	$3,$0
.set	macro
.set	reorder

sb	$10,38($2)
.set	noreorder
.set	nomacro
b	$L2001
move	$4,$0
.set	macro
.set	reorder

$L2280:
lw	$2,11900($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1907
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L2306
lbu	$2,11240($fp)
.set	macro
.set	reorder

$L2081:
.set	noreorder
.set	nomacro
b	$L1886
sw	$8,196($sp)
.set	macro
.set	reorder

$L1822:
li	$5,3			# 0x3
move	$2,$0
.set	noreorder
.set	nomacro
beq	$3,$5,$L2290
addiu	$8,$4,-1
.set	macro
.set	reorder

$L1828:
sb	$3,0($22)
$L1829:
addiu	$2,$2,1
slt	$3,$2,$4
.set	noreorder
.set	nomacro
beq	$3,$0,$L1826
addiu	$22,$22,1
.set	macro
.set	reorder

lbu	$3,1($19)
move	$19,$6
.set	noreorder
.set	nomacro
bne	$3,$5,$L1828
addiu	$6,$6,1
.set	macro
.set	reorder

$L2290:
slt	$7,$2,2
bne	$7,$0,$L1828
lbu	$7,-1($19)
bne	$7,$0,$L1828
lbu	$7,-2($19)
.set	noreorder
.set	nomacro
bne	$7,$0,$L1828
slt	$7,$2,$8
.set	macro
.set	reorder

beq	$7,$0,$L1828
lbu	$7,1($19)
sltu	$9,$7,4
beq	$9,$0,$L1828
move	$19,$6
sb	$7,0($22)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L1829
addiu	$6,$6,1
.set	macro
.set	reorder

$L2258:
lw	$2,11928($23)
beq	$2,$0,$L1854
$L1853:
lw	$3,11248($23)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L1856
li	$2,2			# 0x2
.set	macro
.set	reorder

beq	$3,$2,$L1857
.set	noreorder
.set	nomacro
bne	$3,$0,$L1855
li	$4,6			# 0x6
.set	macro
.set	reorder

lw	$3,11268($23)
slt	$3,$3,9
movn	$2,$4,$3
sw	$2,11260($23)
$L1855:
lw	$3,11168($23)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L2326
li	$3,1			# 0x1
.set	macro
.set	reorder

lbu	$4,11228($23)
lw	$3,2788($23)
lw	$2,2792($23)
addu	$3,$3,$4
addu	$2,$2,$4
lbu	$3,0($3)
sw	$3,2780($23)
lbu	$2,0($2)
sw	$2,2784($23)
li	$3,1			# 0x1
$L2326:
lw	$2,11244($23)
sw	$0,7996($23)
sw	$0,7992($23)
.set	noreorder
.set	nomacro
b	$L1861
sw	$3,8004($23)
.set	macro
.set	reorder

$L2092:
move	$8,$0
$L1970:
#APP
# 102 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
li	$7,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$7,$L2291
addiu	$9,$8,1
.set	macro
.set	reorder

addiu	$8,$8,1
li	$9,6			# 0x6
.set	noreorder
.set	nomacro
bne	$8,$9,$L1970
li	$9,125			# 0x7d
.set	macro
.set	reorder

li	$8,8			# 0x8
.set	noreorder
.set	nomacro
b	$L1972
sw	$8,228($sp)
.set	macro
.set	reorder

$L2065:
move	$2,$22
move	$17,$19
.set	noreorder
.set	nomacro
b	$L1789
move	$21,$0
.set	macro
.set	reorder

$L2261:
lw	$2,11924($23)
beq	$2,$0,$L2327
lw	$2,164($23)
li	$3,112			# 0x70
lw	$7,160($23)
move	$5,$0
lw	$25,%call16(ff_er_add_slice)($28)
move	$6,$0
addiu	$2,$2,-1
sw	$3,20($sp)
addiu	$7,$7,-1
move	$4,$23
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$2,164($23)
lw	$28,32($sp)
.set	noreorder
.set	nomacro
blez	$2,$L2035
sw	$0,7996($23)
.set	macro
.set	reorder

$L2149:
lw	$6,192($23)
lw	$7,196($23)
lw	$4,2092($23)
mul	$3,$16,$6
lw	$2,2088($23)
mul	$8,$16,$7
lw	$5,288($23)
lw	$25,%call16(memcpy)($28)
addu	$2,$2,$3
addu	$16,$8,$4
addu	$5,$5,$3
move	$4,$2
sw	$2,8060($23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$16,8064($23)
.set	macro
.set	reorder

lw	$2,196($23)
lw	$5,7996($23)
lw	$28,32($sp)
lw	$3,292($23)
move	$6,$2
mul	$7,$2,$5
lw	$4,8064($23)
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$7,$3
.set	macro
.set	reorder

lw	$16,7996($23)
lw	$2,164($23)
lw	$28,32($sp)
addiu	$16,$16,1
slt	$2,$16,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L2149
sw	$16,7996($23)
.set	macro
.set	reorder

$L2035:
li	$2,2			# 0x2
sw	$2,2904($23)
$L1872:
lw	$25,%call16(ff_er_frame_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_frame_end
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1852
lw	$28,32($sp)
.set	macro
.set	reorder

$L2259:
lw	$3,11268($23)
li	$2,6			# 0x6
slt	$3,$3,9
movz	$2,$4,$3
sw	$2,11260($23)
$L1866:
lw	$2,11268($23)
li	$4,7			# 0x7
slt	$3,$2,9
li	$2,3			# 0x3
movn	$2,$4,$3
.set	noreorder
.set	nomacro
b	$L1869
sw	$2,11264($23)
.set	macro
.set	reorder

$L1863:
li	$2,4			# 0x4
sw	$2,11260($23)
$L1868:
li	$2,5			# 0x5
.set	noreorder
.set	nomacro
b	$L1869
sw	$2,11264($23)
.set	macro
.set	reorder

$L1862:
sw	$0,11260($23)
$L1867:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L1869
sw	$2,11264($23)
.set	macro
.set	reorder

$L2282:
lbu	$3,11242($fp)
li	$2,1			# 0x1
sll	$2,$2,$3
$L1915:
andi	$3,$2,0x1
.set	noreorder
.set	nomacro
beq	$3,$0,$L2328
andi	$3,$2,0x2
.set	macro
.set	reorder

lw	$3,7992($fp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L2328
andi	$3,$2,0x2
.set	macro
.set	reorder

lbu	$3,11229($fp)
sw	$3,160($sp)
$L1919:
andi	$3,$2,0x2
$L2328:
.set	noreorder
.set	nomacro
bne	$3,$0,$L1920
andi	$3,$2,0x4
.set	macro
.set	reorder

b	$L2307
$L2281:
lbu	$2,11243($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1910
li	$2,45			# 0x2d
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 102 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 103 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 104 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L1911
lbu	$9,11229($fp)
.set	noreorder
.set	nomacro
b	$L1908
sw	$9,160($sp)
.set	macro
.set	reorder

$L1890:
li	$2,458752			# 0x70000
lw	$13,164($sp)
lw	$3,156($sp)
ori	$2,$2,0xefff
or	$2,$13,$2
sw	$2,188($sp)
li	$2,536870912			# 0x20000000
or	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L1891
sw	$3,156($sp)
.set	macro
.set	reorder

$L2082:
li	$4,458752			# 0x70000
ori	$4,$4,0xe000
.set	noreorder
.set	nomacro
b	$L1887
sw	$9,196($sp)
.set	macro
.set	reorder

$L1971:
li	$7,29			# 0x1d
#APP
# 90 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
addiu	$9,$8,7
addiu	$8,$8,8
andi	$9,$9,0x7
sll	$9,$9,4
sw	$8,228($sp)
.set	noreorder
.set	nomacro
b	$L1972
ori	$9,$9,0xd
.set	macro
.set	reorder

$L2286:
#APP
# 76 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$3,$7,-1
andi	$3,$3,0x7
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 82 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$7,$7,-8
#APP
# 78 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,$7
#APP
# 84 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1976
or	$3,$3,$7
.set	macro
.set	reorder

$L2283:
lbu	$3,11242($fp)
sll	$3,$2,$3
li	$2,-2004353024			# 0xffffffff88880000
sra	$5,$3,31
ori	$2,$2,0x8889
mult	$3,$2
mfhi	$2
addu	$2,$2,$3
sra	$2,$2,3
subu	$2,$2,$5
sll	$5,$2,4
subu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L1915
subu	$2,$3,$2
.set	macro
.set	reorder

$L2287:
li	$7,125			# 0x7d
#APP
# 76 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 77 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 78 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
# 82 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 83 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
addiu	$9,$8,-8
sll	$9,$7,$9
#APP
# 84 "vlc_bs.c" 1
mfc0	$7,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1978
or	$7,$9,$7
.set	macro
.set	reorder

$L2271:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC5)
.set	macro
.set	reorder

lw	$3,0($23)
lw	$28,32($sp)
.set	noreorder
.set	nomacro
b	$L1940
addu	$3,$3,$17
.set	macro
.set	reorder

$L2288:
li	$6,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$6,$L2004
slt	$6,$3,3
.set	macro
.set	reorder

bne	$6,$0,$L2292
beq	$3,$5,$L2007
li	$4,8			# 0x8
bne	$3,$4,$L2003
addiu	$3,$2,44
#APP
# 4002 "vc1dec.c" 1
.word	0b01110000011000000000001110010000	#S32LDD XR14,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,48
#APP
# 4003 "vc1dec.c" 1
.word	0b01110000011000000000001101010000	#S32LDD XR13,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,52
#APP
# 4004 "vc1dec.c" 1
.word	0b01110000011000000000001100010000	#S32LDD XR12,$3,0
# 0 "" 2
#NO_APP
$L2003:
#APP
# 4007 "vc1dec.c" 1
.word	0b01110000000010110111100100000011	#D16MAX XR4,XR14,XR13
# 0 "" 2
# 4008 "vc1dec.c" 1
.word	0b01110000000011110111100101000011	#D16MIN XR5,XR14,XR13
# 0 "" 2
# 4009 "vc1dec.c" 1
.word	0b01110000000011110001000100000011	#D16MIN XR4,XR4,XR12
# 0 "" 2
# 4010 "vc1dec.c" 1
.word	0b01110000000010010101000100000011	#D16MAX XR4,XR4,XR5
# 0 "" 2
# 4011 "vc1dec.c" 1
.word	0b01110000000001000000000100101110	#S32M2I XR4, $4
# 0 "" 2
# 4012 "vc1dec.c" 1
.word	0b01110000000000110000000100101110	#S32M2I XR4, $3
# 0 "" 2
#NO_APP
sll	$4,$4,16
sra	$3,$3,16
.set	noreorder
.set	nomacro
b	$L2001
sra	$4,$4,16
.set	macro
.set	reorder

$L2289:
.set	noreorder
.set	nomacro
beq	$9,$0,$L2330
sll	$5,$3,2
.set	macro
.set	reorder

beq	$7,$0,$L2096
move	$3,$4
movn	$3,$0,$6
$L2011:
sll	$5,$3,2
$L2330:
li	$6,4			# 0x4
addiu	$3,$3,1
.set	noreorder
.set	nomacro
b	$L2013
addu	$8,$8,$5
.set	macro
.set	reorder

$L2294:
.set	noreorder
.set	nomacro
beq	$3,$6,$L2293
addiu	$8,$8,4
.set	macro
.set	reorder

$L2013:
lw	$4,4($8)
.set	noreorder
.set	nomacro
bne	$4,$0,$L2294
addiu	$3,$3,1
.set	macro
.set	reorder

addiu	$3,$3,-1
$L2012:
addiu	$13,$sp,40
sll	$3,$3,2
addu	$4,$13,$5
addu	$3,$13,$3
lw	$6,84($4)
lw	$7,84($3)
lw	$5,68($4)
lw	$3,68($3)
addu	$6,$6,$7
addu	$5,$5,$3
srl	$4,$6,31
srl	$3,$5,31
addu	$4,$4,$6
addu	$3,$3,$5
sra	$4,$4,1
sra	$3,$3,1
sll	$4,$4,16
sll	$3,$3,16
sra	$4,$4,16
.set	noreorder
.set	nomacro
b	$L2001
sra	$3,$3,16
.set	macro
.set	reorder

$L1857:
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
b	$L1855
sw	$2,11260($23)
.set	macro
.set	reorder

$L1856:
.set	noreorder
.set	nomacro
b	$L1855
sw	$0,11260($23)
.set	macro
.set	reorder

$L2260:
lbu	$2,11228($23)
addiu	$4,$23,10584
lbu	$6,11309($23)
lbu	$3,11300($23)
sll	$5,$2,1
lw	$25,%call16(ff_intrax8_decode_picture)($28)
movn	$2,$0,$6
addu	$5,$5,$3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_intrax8_decode_picture
1:	jalr	$25
move	$6,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1872
lw	$28,32($sp)
.set	macro
.set	reorder

$L2101:
lbu	$3,11229($fp)
.set	noreorder
.set	nomacro
b	$L1919
sw	$3,160($sp)
.set	macro
.set	reorder

$L1910:
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$3,7			# 0x7
beq	$2,$3,$L1913
lbu	$3,11228($fp)
addu	$3,$2,$3
.set	noreorder
.set	nomacro
b	$L1908
sw	$3,160($sp)
.set	macro
.set	reorder

$L2285:
lbu	$11,11229($fp)
.set	noreorder
.set	nomacro
b	$L1908
sw	$11,160($sp)
.set	macro
.set	reorder

$L2057:
lw	$4,11196($23)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1886
lw	$8,164($sp)
.set	macro
.set	reorder

addiu	$3,$3,-8192
.set	noreorder
.set	nomacro
b	$L2062
or	$4,$8,$3
.set	macro
.set	reorder

$L2291:
addiu	$8,$8,2
sll	$9,$9,4
sw	$8,228($sp)
.set	noreorder
.set	nomacro
b	$L1972
ori	$9,$9,0xd
.set	macro
.set	reorder

$L1911:
lbu	$11,11228($fp)
.set	noreorder
.set	nomacro
b	$L1908
sw	$11,160($sp)
.set	macro
.set	reorder

$L2292:
bne	$3,$4,$L2003
addiu	$3,$2,48
$L2223:
#APP
# 3992 "vc1dec.c" 1
.word	0b01110000011000000000001110010000	#S32LDD XR14,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,52
#APP
# 3993 "vc1dec.c" 1
.word	0b01110000011000000000001101010000	#S32LDD XR13,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,56
#APP
# 3994 "vc1dec.c" 1
.word	0b01110000011000000000001100010000	#S32LDD XR12,$3,0
# 0 "" 2
#NO_APP
b	$L2003
$L2293:
.set	noreorder
.set	nomacro
b	$L2012
move	$3,$0
.set	macro
.set	reorder

$L2269:
lw	$5,0($4)
li	$7,-201326592			# 0xfffffffff4000000
lw	$9,%got(task_fifo_wp_d1)($28)
sw	$2,0($22)
sw	$3,0($20)
addiu	$3,$5,1
sw	$2,0($9)
li	$2,60			# 0x3c
sw	$6,0($21)
sh	$2,0($6)
sw	$3,0($4)
lw	$2,0($4)
addiu	$2,$2,1
sw	$2,0($4)
lw	$2,0($4)
addiu	$2,$2,1
sw	$2,0($4)
$L2033:
lw	$2,0($7)
.set	noreorder
.set	nomacro
beq	$2,$0,$L2033
lw	$11,%got(aux_base)($28)
.set	macro
.set	reorder

li	$3,1			# 0x1
lw	$25,%call16(ff_er_add_slice)($28)
move	$5,$0
move	$6,$0
move	$4,$23
lw	$2,0($11)
sw	$3,0($2)
li	$3,112			# 0x70
lw	$2,164($23)
lw	$7,160($23)
sw	$3,20($sp)
addiu	$2,$2,-1
addiu	$7,$7,-1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1872
lw	$28,32($sp)
.set	macro
.set	reorder

$L2007:
addiu	$3,$2,44
#APP
# 3997 "vc1dec.c" 1
.word	0b01110000011000000000001110010000	#S32LDD XR14,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,48
#APP
# 3998 "vc1dec.c" 1
.word	0b01110000011000000000001101010000	#S32LDD XR13,$3,0
# 0 "" 2
#NO_APP
addiu	$3,$2,56
#APP
# 3999 "vc1dec.c" 1
.word	0b01110000011000000000001100010000	#S32LDD XR12,$3,0
# 0 "" 2
#NO_APP
b	$L2003
$L2004:
.set	noreorder
.set	nomacro
b	$L2223
addiu	$3,$2,44
.set	macro
.set	reorder

$L1913:
li	$2,77			# 0x4d
#APP
# 90 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 91 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 92 "vlc_bs.c" 1
mfc0	$13,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1908
sw	$13,160($sp)
.set	macro
.set	reorder

$L2096:
.set	noreorder
.set	nomacro
b	$L2011
li	$3,1			# 0x1
.set	macro
.set	reorder

$L2268:
sh	$14,4($3)
.set	noreorder
.set	nomacro
b	$L2308
move	$2,$0
.set	macro
.set	reorder

$L1930:
lw	$5,0($19)
lw	$13,152($sp)
lw	$2,11260($fp)
sll	$5,$5,4
sw	$17,11276($fp)
.set	noreorder
.set	nomacro
b	$L1931
addu	$5,$13,$5
.set	macro
.set	reorder

$L2250:
li	$6,4626			# 0x1212
lw	$25,%call16(__assert)($28)
addiu	$4,$4,%lo($LC13)
$L2227:
lw	$5,%got($LC12)($28)
lw	$7,%got(__PRETTY_FUNCTION__.8249)($28)
addiu	$5,$5,%lo($LC12)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__assert
1:	jalr	$25
addiu	$7,$7,%lo(__PRETTY_FUNCTION__.8249)
.set	macro
.set	reorder

$L2249:
lw	$4,%got($LC11)($28)
li	$6,4625			# 0x1211
lw	$25,%call16(__assert)($28)
.set	noreorder
.set	nomacro
b	$L2227
addiu	$4,$4,%lo($LC11)
.set	macro
.set	reorder

.end	vc1_decode_frame
.size	vc1_decode_frame, .-vc1_decode_frame
.section	.text.vc1_fluash_buffer,"ax",@progbits
.align	2
.align	5
.globl	vc1_fluash_buffer
.set	nomips16
.set	nomicromips
.ent	vc1_fluash_buffer
.type	vc1_fluash_buffer, @function
vc1_fluash_buffer:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$18,32($sp)
li	$18,19200			# 0x4b00
sw	$16,24($sp)
move	$16,$0
sw	$17,28($sp)
move	$17,$4
sw	$31,36($sp)
$L2333:
lw	$2,200($17)
addu	$2,$2,$16
move	$5,$2
lw	$2,0($2)
beq	$2,$0,$L2332
addiu	$16,$16,600

lw	$2,0($17)
lw	$25,260($2)
jalr	$25
move	$4,$2

$L2332:
bne	$16,$18,$L2333
lw	$31,36($sp)

lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	vc1_fluash_buffer
.size	vc1_fluash_buffer, .-vc1_fluash_buffer
.section	.text.flush_dpb,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	flush_dpb
.type	flush_dpb, @function
flush_dpb:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
sw	$16,24($sp)
lw	$16,136($4)
.cprestore	16
sw	$31,28($sp)
lw	$2,2696($16)
beq	$2,$0,$L2340
nop

sw	$0,2696($16)
$L2340:
lw	$2,2688($16)
beq	$2,$0,$L2341
nop

sw	$0,2688($16)
$L2341:
lw	$2,2692($16)
beq	$2,$0,$L2355
lw	$25,%call16(vc1_fluash_buffer)($28)

sw	$0,2692($16)
$L2355:
.reloc	1f,R_MIPS_JALR,vc1_fluash_buffer
1:	jalr	$25
move	$4,$16

lw	$3,200($16)
addiu	$2,$3,80
addiu	$3,$3,19280
$L2343:
sw	$0,0($2)
addiu	$2,$2,600
bne	$2,$3,$L2343
lw	$31,28($sp)

lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	flush_dpb
.size	flush_dpb, .-flush_dpb
.local	vlc_table.7470
.comm	vlc_table.7470,46184,4
.local	table.7474
.comm	table.7474,64,4
.local	table.7473
.comm	table.7473,2224,4
.local	table.7472
.comm	table.7472,32,4
.local	table.7471
.comm	table.7471,512,4
.local	done.7468
.comm	done.7468,4,4
.rdata
.align	2
.type	count.8094, @object
.size	count.8094, 64
count.8094:
.word	0
.word	1
.word	1
.word	2
.word	1
.word	2
.word	2
.word	3
.word	1
.word	2
.word	2
.word	3
.word	2
.word	3
.word	3
.word	4
.align	2
.type	dcpred.7594, @object
.size	dcpred.7594, 64
dcpred.7594:
.half	-1
.half	1024
.half	512
.half	341
.half	256
.half	205
.half	171
.half	146
.half	128
.half	114
.half	102
.half	93
.half	85
.half	79
.half	73
.half	68
.half	64
.half	60
.half	57
.half	54
.half	51
.half	49
.half	47
.half	45
.half	43
.half	41
.half	39
.half	38
.half	37
.half	35
.half	34
.half	33
.align	2
.type	__PRETTY_FUNCTION__.8249, @object
.size	__PRETTY_FUNCTION__.8249, 17
__PRETTY_FUNCTION__.8249:
.ascii	"vc1_decode_frame\000"
.globl	wmv3_decoder
.section	.rodata.str1.4
.align	2
$LC14:
.ascii	"wmv3\000"
.align	2
$LC15:
.ascii	"Windows Media Video 9\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	wmv3_decoder, @object
.size	wmv3_decoder, 72
wmv3_decoder:
.word	$LC14
.word	0
.word	74
.word	11960
.word	vc1_decode_init
.word	0
.word	vc1_decode_end
.word	vc1_decode_frame
.word	34
.space	4
.word	flush_dpb
.space	4
.word	ff_hwaccel_pixfmt_list_420
.word	$LC15
.space	16
.globl	vc1_decoder
.section	.rodata.str1.4
.align	2
$LC16:
.ascii	"vc1\000"
.align	2
$LC17:
.ascii	"SMPTE VC-1\000"
.section	.data.rel
.align	2
.type	vc1_decoder, @object
.size	vc1_decoder, 72
vc1_decoder:
.word	$LC16
.word	0
.word	73
.word	11960
.word	vc1_decode_init
.word	0
.word	vc1_decode_end
.word	vc1_decode_frame
.word	34
.space	4
.word	flush_dpb
.space	4
.word	ff_hwaccel_pixfmt_list_420
.word	$LC17
.space	16
.local	last_skip_frame
.comm	last_skip_frame,4,4
.rdata
.align	2
.type	offset_table, @object
.size	offset_table, 24
offset_table:
.word	0
.word	1
.word	3
.word	7
.word	15
.word	31
.align	2
.type	size_table, @object
.size	size_table, 24
size_table:
.word	0
.word	2
.word	3
.word	4
.word	5
.word	8

.comm	left_buf,4,4

.comm	residual_init_table,224,4
.align	2
.type	vlc_offs, @object
.size	vlc_offs, 60
vlc_offs:
.half	0
.half	274
.half	306
.half	370
.half	626
.half	658
.half	722
.half	986
.half	1018
.half	1082
.half	1146
.half	1434
.half	1772
.half	1836
.half	2156
.half	2486
.half	2550
.half	2846
.half	3174
.half	3238
.half	3550
.half	3948
.half	5312
.half	6440
.half	6938
.half	8010
.half	8334
.half	8658
.half	9682
.half	11546

.comm	dma_len,4,4
.local	buf2_pre_alloc_buf
.comm	buf2_pre_alloc_buf,4,4

.comm	crc_code_rota,2,2

.comm	rota_c,4,4

.comm	rota_y,4,4

.comm	sde_base,4,4
.globl	hw_all_table
.data
.align	2
.type	hw_all_table, @object
.size	hw_all_table, 15616
hw_all_table:
.half	8230
.half	8230
.half	8230
.half	8230
.half	8231
.half	8231
.half	8231
.half	8231
.half	-28080
.half	-26336
.half	-28064
.half	-26320
.half	10308
.half	10308
.half	10309
.half	10309
.half	-28032
.half	-28024
.half	-26288
.half	-28008
.half	-26272
.half	-27976
.half	-27968
.half	-26256
.half	12402
.half	12403
.half	-26224
.half	-27896
.half	12296
.half	12297
.half	-26208
.half	-26176
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	10280
.half	10280
.half	10281
.half	10281
.half	-27824
.half	-27816
.half	-26160
.half	-26128
.half	10246
.half	10246
.half	10247
.half	10247
.half	10332
.half	10332
.half	10333
.half	10333
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	12548
.half	12549
.half	-27792
.half	-27784
.half	10490
.half	10490
.half	10491
.half	10491
.half	8196
.half	8196
.half	8196
.half	8196
.half	8197
.half	8197
.half	8197
.half	8197
.half	8430
.half	8430
.half	8430
.half	8430
.half	8431
.half	8431
.half	8431
.half	8431
.half	-27776
.half	-27768
.half	12414
.half	12415
.half	-26096
.half	-26064
.half	12556
.half	12557
.half	2102
.half	2102
.half	2102
.half	2102
.half	2102
.half	2102
.half	2102
.half	2102
.half	2103
.half	2103
.half	2103
.half	2103
.half	2103
.half	2103
.half	2103
.half	2103
.half	6286
.half	6286
.half	6287
.half	6287
.half	6508
.half	6508
.half	6509
.half	6509
.half	6372
.half	6372
.half	6373
.half	6373
.half	8282
.half	8283
.half	8472
.half	8473
.half	6230
.half	6230
.half	6231
.half	6231
.half	8256
.half	8257
.half	8398
.half	8399
.half	4352
.half	4352
.half	4352
.half	4352
.half	4353
.half	4353
.half	4353
.half	4353
.half	4280
.half	4280
.half	4280
.half	4280
.half	4281
.half	4281
.half	4281
.half	4281
.half	6346
.half	6346
.half	6347
.half	6347
.half	6418
.half	6418
.half	6419
.half	6419
.half	4200
.half	4200
.half	4200
.half	4200
.half	4201
.half	4201
.half	4201
.half	4201
.half	6206
.half	6206
.half	6207
.half	6207
.half	8304
.half	8305
.half	8402
.half	8403
.half	2260
.half	2260
.half	2260
.half	2260
.half	2260
.half	2260
.half	2260
.half	2260
.half	2261
.half	2261
.half	2261
.half	2261
.half	2261
.half	2261
.half	2261
.half	2261
.half	4290
.half	4290
.half	4290
.half	4290
.half	4291
.half	4291
.half	4291
.half	4291
.half	6378
.half	6378
.half	6379
.half	6379
.half	8428
.half	8429
.half	8258
.half	8259
.half	2388
.half	2388
.half	2388
.half	2388
.half	2388
.half	2388
.half	2388
.half	2388
.half	2389
.half	2389
.half	2389
.half	2389
.half	2389
.half	2389
.half	2389
.half	2389
.half	8316
.half	8317
.half	8388
.half	8389
.half	6294
.half	6294
.half	6295
.half	6295
.half	4400
.half	4400
.half	4400
.half	4400
.half	4401
.half	4401
.half	4401
.half	4401
.half	4320
.half	4320
.half	4320
.half	4320
.half	4321
.half	4321
.half	4321
.half	4321
.half	4180
.half	4180
.half	4180
.half	4180
.half	4181
.half	4181
.half	4181
.half	4181
.half	4212
.half	4212
.half	4213
.half	4213
.half	-28072
.half	-24448
.half	6218
.half	6219
.half	2060
.half	2060
.half	2060
.half	2060
.half	2061
.half	2061
.half	2061
.half	2061
.half	4282
.half	4282
.half	4283
.half	4283
.half	-28056
.half	-28048
.half	-28040
.half	-26304
.half	2338
.half	2338
.half	2338
.half	2338
.half	2339
.half	2339
.half	2339
.half	2339
.half	6410
.half	6411
.half	6178
.half	6179
.half	4126
.half	4126
.half	4127
.half	4127
.half	2384
.half	2384
.half	2384
.half	2384
.half	2385
.half	2385
.half	2385
.half	2385
.half	4276
.half	4276
.half	4277
.half	4277
.half	6336
.half	6337
.half	-28016
.half	-24416
.half	5490
.half	5490
.half	5491
.half	5491
.half	4112
.half	4112
.half	4113
.half	4113
.half	6290
.half	6291
.half	-28000
.half	-27992
.half	6406
.half	6407
.half	6342
.half	6343
.half	-24384
.half	-27984
.half	6414
.half	6415
.half	4224
.half	4224
.half	4225
.half	4225
.half	6164
.half	6165
.half	-27960
.half	-24352
.half	-27952
.half	-27944
.half	-27936
.half	-26240
.half	2346
.half	2346
.half	2346
.half	2346
.half	2347
.half	2347
.half	2347
.half	2347
.half	4342
.half	4342
.half	4343
.half	4343
.half	6374
.half	6375
.half	6232
.half	6233
.half	2312
.half	2312
.half	2312
.half	2312
.half	2313
.half	2313
.half	2313
.half	2313
.half	-27928
.half	-27920
.half	-27912
.half	-27904
.half	4192
.half	4192
.half	4193
.half	4193
.half	2216
.half	2216
.half	2216
.half	2216
.half	2217
.half	2217
.half	2217
.half	2217
.half	-27888
.half	-26192
.half	-27880
.half	-27872
.half	4414
.half	4414
.half	4415
.half	4415
.half	4418
.half	4418
.half	4419
.half	4419
.half	-27864
.half	-27856
.half	-27848
.half	-24320
.half	6278
.half	6279
.half	6266
.half	6267
.half	4274
.half	4274
.half	4275
.half	4275
.half	2332
.half	2332
.half	2332
.half	2332
.half	2333
.half	2333
.half	2333
.half	2333
.half	4234
.half	4234
.half	4235
.half	4235
.half	6332
.half	6333
.half	-27840
.half	-27832
.half	2062
.half	2062
.half	2062
.half	2062
.half	2063
.half	2063
.half	2063
.half	2063
.half	-26144
.half	-27808
.half	6470
.half	6471
.half	6242
.half	6243
.half	6194
.half	6195
.half	2350
.half	2350
.half	2350
.half	2350
.half	2351
.half	2351
.half	2351
.half	2351
.half	2168
.half	2168
.half	2168
.half	2168
.half	2169
.half	2169
.half	2169
.half	2169
.half	4404
.half	4404
.half	4405
.half	4405
.half	6302
.half	6303
.half	6376
.half	6377
.half	2354
.half	2354
.half	2354
.half	2354
.half	2355
.half	2355
.half	2355
.half	2355
.half	6306
.half	6307
.half	-27800
.half	-26112
.half	4114
.half	4114
.half	4115
.half	4115
.half	2336
.half	2336
.half	2336
.half	2336
.half	2337
.half	2337
.half	2337
.half	2337
.half	6512
.half	6513
.half	6402
.half	6403
.half	6310
.half	6311
.half	6392
.half	6393
.half	-27760
.half	-27752
.half	6298
.half	6299
.half	-27744
.half	-27736
.half	6166
.half	6167
.half	6472
.half	6473
.half	-26080
.half	-27728
.half	4338
.half	4338
.half	4339
.half	4339
.half	6252
.half	6253
.half	6254
.half	6255
.half	4156
.half	4156
.half	4157
.half	4157
.half	4202
.half	4202
.half	4203
.half	4203
.half	4456
.half	4456
.half	4457
.half	4457
.half	4144
.half	4144
.half	4145
.half	4145
.half	-27720
.half	-26048
.half	-27712
.half	-27704
.half	2358
.half	2358
.half	2358
.half	2358
.half	2359
.half	2359
.half	2359
.half	2359
.half	4396
.half	4396
.half	4397
.half	4397
.half	4128
.half	4128
.half	4129
.half	4129
.half	4420
.half	4420
.half	4421
.half	4421
.half	6180
.half	6181
.half	6510
.half	6511
.half	42
.half	42
.half	42
.half	42
.half	43
.half	43
.half	43
.half	43
.half	118
.half	118
.half	118
.half	118
.half	119
.half	119
.half	119
.half	119
.half	4348
.half	4349
.half	4142
.half	4143
.half	2334
.half	2334
.half	2335
.half	2335
.half	76
.half	76
.half	76
.half	76
.half	77
.half	77
.half	77
.half	77
.half	4252
.half	4253
.half	4452
.half	4453
.half	4228
.half	4229
.half	4178
.half	4179
.half	2390
.half	2390
.half	2391
.half	2391
.half	2392
.half	2392
.half	2393
.half	2393
.half	2092
.half	2092
.half	2093
.half	2093
.half	2208
.half	2208
.half	2209
.half	2209
.half	70
.half	70
.half	70
.half	70
.half	71
.half	71
.half	71
.half	71
.half	4388
.half	4389
.half	4446
.half	4447
.half	2074
.half	2074
.half	2075
.half	2075
.half	136
.half	136
.half	136
.half	136
.half	137
.half	137
.half	137
.half	137
.half	4316
.half	4317
.half	4314
.half	4315
.half	4154
.half	4155
.half	4408
.half	4409
.half	332
.half	332
.half	332
.half	332
.half	333
.half	333
.half	333
.half	333
.half	2386
.half	2386
.half	2387
.half	2387
.half	2262
.half	2262
.half	2263
.half	2263
.half	10
.half	10
.half	10
.half	10
.half	11
.half	11
.half	11
.half	11
.half	2342
.half	2342
.half	2343
.half	2343
.half	2362
.half	2362
.half	2363
.half	2363
.half	24
.half	24
.half	24
.half	24
.half	25
.half	25
.half	25
.half	25
.half	176
.half	176
.half	176
.half	176
.half	177
.half	177
.half	177
.half	177
.half	170
.half	170
.half	170
.half	170
.half	171
.half	171
.half	171
.half	171
.half	254
.half	254
.half	254
.half	254
.half	255
.half	255
.half	255
.half	255
.half	130
.half	130
.half	130
.half	130
.half	131
.half	131
.half	131
.half	131
.half	330
.half	330
.half	330
.half	330
.half	331
.half	331
.half	331
.half	331
.half	2196
.half	2196
.half	2197
.half	2197
.half	2128
.half	2128
.half	2129
.half	2129
.half	182
.half	182
.half	182
.half	182
.half	183
.half	183
.half	183
.half	183
.half	144
.half	144
.half	144
.half	144
.half	145
.half	145
.half	145
.half	145
.half	52
.half	52
.half	52
.half	52
.half	53
.half	53
.half	53
.half	53
.half	204
.half	204
.half	204
.half	204
.half	205
.half	205
.half	205
.half	205
.half	4322
.half	4323
.half	4318
.half	4319
.half	4450
.half	4451
.half	4458
.half	4459
.half	2400
.half	2400
.half	2401
.half	2401
.half	4392
.half	4393
.half	4268
.half	4269
.half	2076
.half	2076
.half	2077
.half	2077
.half	2320
.half	2320
.half	2321
.half	2321
.half	2394
.half	2394
.half	2395
.half	2395
.half	2104
.half	2104
.half	2105
.half	2105
.half	2150
.half	2150
.half	2151
.half	2151
.half	2248
.half	2248
.half	2249
.half	2249
.half	334
.half	334
.half	334
.half	334
.half	335
.half	335
.half	335
.half	335
.half	276
.half	276
.half	276
.half	276
.half	277
.half	277
.half	277
.half	277
.half	240
.half	240
.half	240
.half	240
.half	241
.half	241
.half	241
.half	241
.half	100
.half	100
.half	100
.half	100
.half	101
.half	101
.half	101
.half	101
.half	244
.half	244
.half	244
.half	244
.half	245
.half	245
.half	245
.half	245
.half	2120
.half	2120
.half	2121
.half	2121
.half	2222
.half	2222
.half	2223
.half	2223
.half	282
.half	282
.half	282
.half	282
.half	283
.half	283
.half	283
.half	283
.half	94
.half	94
.half	94
.half	94
.half	95
.half	95
.half	95
.half	95
.half	152
.half	152
.half	152
.half	152
.half	153
.half	153
.half	153
.half	153
.half	316
.half	316
.half	316
.half	316
.half	317
.half	317
.half	317
.half	317
.half	140
.half	140
.half	140
.half	140
.half	141
.half	141
.half	141
.half	141
.half	208
.half	208
.half	208
.half	208
.half	209
.half	209
.half	209
.half	209
.half	2264
.half	2264
.half	2265
.half	2265
.half	2396
.half	2396
.half	2397
.half	2397
.half	278
.half	278
.half	278
.half	278
.half	279
.half	279
.half	279
.half	279
.half	78
.half	78
.half	78
.half	78
.half	79
.half	79
.half	79
.half	79
.half	190
.half	190
.half	190
.half	190
.half	191
.half	191
.half	191
.half	191
.half	2212
.half	2212
.half	2213
.half	2213
.half	4454
.half	4455
.half	4416
.half	4417
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	-28128
.half	-26176
.half	-24448
.half	-24416
.half	10308
.half	10308
.half	10309
.half	10309
.half	8194
.half	8194
.half	8194
.half	8194
.half	8195
.half	8195
.half	8195
.half	8195
.half	8238
.half	8238
.half	8238
.half	8238
.half	8239
.half	8239
.half	8239
.half	8239
.half	12294
.half	12295
.half	-28064
.half	-28056
.half	10244
.half	10244
.half	10245
.half	10245
.half	6342
.half	6342
.half	6342
.half	6342
.half	6342
.half	6342
.half	6342
.half	6342
.half	6343
.half	6343
.half	6343
.half	6343
.half	6343
.half	6343
.half	6343
.half	6343
.half	-28048
.half	-24384
.half	-28000
.half	-24352
.half	10474
.half	10474
.half	10475
.half	10475
.half	8408
.half	8408
.half	8408
.half	8408
.half	8409
.half	8409
.half	8409
.half	8409
.half	-27992
.half	-24320
.half	12386
.half	12387
.half	12488
.half	12489
.half	12542
.half	12543
.half	-27976
.half	-27968
.half	-24288
.half	-24256
.half	-24224
.half	-27872
.half	12548
.half	12549
.half	10324
.half	10324
.half	10325
.half	10325
.half	10482
.half	10482
.half	10483
.half	10483
.half	8418
.half	8418
.half	8418
.half	8418
.half	8419
.half	8419
.half	8419
.half	8419
.half	12396
.half	12397
.half	-24192
.half	-24160
.half	10488
.half	10488
.half	10489
.half	10489
.half	10288
.half	10288
.half	10289
.half	10289
.half	12556
.half	12557
.half	12296
.half	12297
.half	6438
.half	6438
.half	6439
.half	6439
.half	-28120
.half	-28112
.half	-28104
.half	-28096
.half	4168
.half	4168
.half	4168
.half	4168
.half	4169
.half	4169
.half	4169
.half	4169
.half	2134
.half	2134
.half	2134
.half	2134
.half	2134
.half	2134
.half	2134
.half	2134
.half	2135
.half	2135
.half	2135
.half	2135
.half	2135
.half	2135
.half	2135
.half	2135
.half	2324
.half	2324
.half	2324
.half	2324
.half	2324
.half	2324
.half	2324
.half	2324
.half	2325
.half	2325
.half	2325
.half	2325
.half	2325
.half	2325
.half	2325
.half	2325
.half	8502
.half	8503
.half	-28088
.half	-26160
.half	8334
.half	8335
.half	-28080
.half	-28072
.half	4196
.half	4196
.half	4196
.half	4196
.half	4197
.half	4197
.half	4197
.half	4197
.half	6302
.half	6302
.half	6303
.half	6303
.half	8294
.half	8295
.half	-28040
.half	-28032
.half	6162
.half	6162
.half	6163
.half	6163
.half	-28024
.half	-28016
.half	-26144
.half	-28008
.half	2188
.half	2188
.half	2188
.half	2188
.half	2188
.half	2188
.half	2188
.half	2188
.half	2189
.half	2189
.half	2189
.half	2189
.half	2189
.half	2189
.half	2189
.half	2189
.half	6444
.half	6444
.half	6445
.half	6445
.half	8214
.half	8215
.half	8376
.half	8377
.half	6442
.half	6442
.half	6443
.half	6443
.half	6388
.half	6388
.half	6389
.half	6389
.half	4206
.half	4206
.half	4206
.half	4206
.half	4207
.half	4207
.half	4207
.half	4207
.half	6448
.half	6448
.half	6449
.half	6449
.half	6264
.half	6264
.half	6265
.half	6265
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	8524
.half	8525
.half	8504
.half	8505
.half	-26128
.half	-27984
.half	8282
.half	8283
.half	6348
.half	6348
.half	6349
.half	6349
.half	6218
.half	6218
.half	6219
.half	6219
.half	4398
.half	4398
.half	4398
.half	4398
.half	4399
.half	4399
.half	4399
.half	4399
.half	5456
.half	5456
.half	5456
.half	5456
.half	5457
.half	5457
.half	5457
.half	5457
.half	8304
.half	8305
.half	-27960
.half	-26112
.half	-27952
.half	-27944
.half	8362
.half	8363
.half	4332
.half	4332
.half	4332
.half	4332
.half	4333
.half	4333
.half	4333
.half	4333
.half	2100
.half	2100
.half	2100
.half	2100
.half	2100
.half	2100
.half	2100
.half	2100
.half	2101
.half	2101
.half	2101
.half	2101
.half	2101
.half	2101
.half	2101
.half	2101
.half	4112
.half	4112
.half	4112
.half	4112
.half	4113
.half	4113
.half	4113
.half	4113
.half	8448
.half	8449
.half	-27936
.half	-27928
.half	8328
.half	8329
.half	-27920
.half	-27912
.half	4388
.half	4388
.half	4388
.half	4388
.half	4389
.half	4389
.half	4389
.half	4389
.half	6164
.half	6164
.half	6165
.half	6165
.half	6272
.half	6272
.half	6273
.half	6273
.half	-27904
.half	-27896
.half	8216
.half	8217
.half	-27888
.half	-27880
.half	8268
.half	8269
.half	4150
.half	4150
.half	4150
.half	4150
.half	4151
.half	4151
.half	4151
.half	4151
.half	2250
.half	2250
.half	2250
.half	2250
.half	2250
.half	2250
.half	2250
.half	2250
.half	2251
.half	2251
.half	2251
.half	2251
.half	2251
.half	2251
.half	2251
.half	2251
.half	6394
.half	6394
.half	6395
.half	6395
.half	6200
.half	6200
.half	6201
.half	6201
.half	8508
.half	8509
.half	-27864
.half	-27856
.half	8250
.half	8251
.half	-27848
.half	-27840
.half	6364
.half	6364
.half	6365
.half	6365
.half	6306
.half	6306
.half	6307
.half	6307
.half	-27832
.half	-27824
.half	8398
.half	8399
.half	8500
.half	8501
.half	8366
.half	8367
.half	4392
.half	4392
.half	4392
.half	4392
.half	4393
.half	4393
.half	4393
.half	4393
.half	6310
.half	6310
.half	6311
.half	6311
.half	-26096
.half	-27816
.half	-27808
.half	-27800
.half	4242
.half	4242
.half	4243
.half	4243
.half	6232
.half	6233
.half	6296
.half	6297
.half	2174
.half	2174
.half	2174
.half	2174
.half	2175
.half	2175
.half	2175
.half	2175
.half	2078
.half	2078
.half	2078
.half	2078
.half	2079
.half	2079
.half	2079
.half	2079
.half	6240
.half	6241
.half	6368
.half	6369
.half	4374
.half	4374
.half	4375
.half	4375
.half	2162
.half	2162
.half	2162
.half	2162
.half	2163
.half	2163
.half	2163
.half	2163
.half	6358
.half	6359
.half	6186
.half	6187
.half	4418
.half	4418
.half	4419
.half	4419
.half	2338
.half	2338
.half	2338
.half	2338
.half	2339
.half	2339
.half	2339
.half	2339
.half	6384
.half	6385
.half	6340
.half	6341
.half	4348
.half	4348
.half	4349
.half	4349
.half	4328
.half	4328
.half	4329
.half	4329
.half	6402
.half	6403
.half	6300
.half	6301
.half	4134
.half	4134
.half	4135
.half	4135
.half	6188
.half	6189
.half	6320
.half	6321
.half	4136
.half	4136
.half	4137
.half	4137
.half	6226
.half	6227
.half	6316
.half	6317
.half	4212
.half	4212
.half	4213
.half	4213
.half	4264
.half	4264
.half	4265
.half	4265
.half	264
.half	264
.half	264
.half	264
.half	265
.half	265
.half	265
.half	265
.half	306
.half	306
.half	306
.half	306
.half	307
.half	307
.half	307
.half	307
.half	2126
.half	2126
.half	2127
.half	2127
.half	2270
.half	2270
.half	2271
.half	2271
.half	2372
.half	2372
.half	2373
.half	2373
.half	2152
.half	2152
.half	2153
.half	2153
.half	262
.half	262
.half	262
.half	262
.half	263
.half	263
.half	263
.half	263
.half	4234
.half	4235
.half	4190
.half	4191
.half	2178
.half	2178
.half	2179
.half	2179
.half	148
.half	148
.half	148
.half	148
.half	149
.half	149
.half	149
.half	149
.half	4416
.half	4417
.half	4176
.half	4177
.half	2202
.half	2202
.half	2203
.half	2203
.half	2266
.half	2266
.half	2267
.half	2267
.half	2328
.half	2328
.half	2329
.half	2329
.half	4110
.half	4111
.half	4324
.half	4325
.half	2332
.half	2332
.half	2333
.half	2333
.half	70
.half	70
.half	70
.half	70
.half	71
.half	71
.half	71
.half	71
.half	230
.half	230
.half	230
.half	230
.half	231
.half	231
.half	231
.half	231
.half	26
.half	26
.half	26
.half	26
.half	27
.half	27
.half	27
.half	27
.half	2256
.half	2256
.half	2257
.half	2257
.half	4132
.half	4133
.half	4160
.half	4161
.half	178
.half	178
.half	178
.half	178
.half	179
.half	179
.half	179
.half	179
.half	2322
.half	2322
.half	2323
.half	2323
.half	2080
.half	2080
.half	2081
.half	2081
.half	50
.half	50
.half	50
.half	50
.half	51
.half	51
.half	51
.half	51
.half	2336
.half	2336
.half	2337
.half	2337
.half	2182
.half	2182
.half	2183
.half	2183
.half	266
.half	266
.half	266
.half	266
.half	267
.half	267
.half	267
.half	267
.half	118
.half	118
.half	118
.half	118
.half	119
.half	119
.half	119
.half	119
.half	272
.half	272
.half	272
.half	272
.half	273
.half	273
.half	273
.half	273
.half	2110
.half	2110
.half	2111
.half	2111
.half	2378
.half	2378
.half	2379
.half	2379
.half	28
.half	28
.half	28
.half	28
.half	29
.half	29
.half	29
.half	29
.half	318
.half	318
.half	318
.half	318
.half	319
.half	319
.half	319
.half	319
.half	270
.half	270
.half	270
.half	270
.half	271
.half	271
.half	271
.half	271
.half	186
.half	186
.half	186
.half	186
.half	187
.half	187
.half	187
.half	187
.half	4284
.half	4285
.half	4308
.half	4309
.half	2294
.half	2294
.half	2295
.half	2295
.half	238
.half	238
.half	238
.half	238
.half	239
.half	239
.half	239
.half	239
.half	4228
.half	4229
.half	4202
.half	4203
.half	2192
.half	2192
.half	2193
.half	2193
.half	4424
.half	4425
.half	4162
.half	4163
.half	2374
.half	2374
.half	2375
.half	2375
.half	60
.half	60
.half	60
.half	60
.half	61
.half	61
.half	61
.half	61
.half	2082
.half	2082
.half	2083
.half	2083
.half	4286
.half	4287
.half	4288
.half	4289
.half	10
.half	10
.half	10
.half	10
.half	11
.half	11
.half	11
.half	11
.half	2212
.half	2212
.half	2213
.half	2213
.half	2330
.half	2330
.half	2331
.half	2331
.half	286
.half	286
.half	286
.half	286
.half	287
.half	287
.half	287
.half	287
.half	314
.half	314
.half	314
.half	314
.half	315
.half	315
.half	315
.half	315
.half	92
.half	92
.half	92
.half	92
.half	93
.half	93
.half	93
.half	93
.half	182
.half	182
.half	182
.half	182
.half	183
.half	183
.half	183
.half	183
.half	180
.half	180
.half	180
.half	180
.half	181
.half	181
.half	181
.half	181
.half	2258
.half	2258
.half	2259
.half	2259
.half	4246
.half	4247
.half	4290
.half	4291
.half	122
.half	122
.half	122
.half	122
.half	123
.half	123
.half	123
.half	123
.half	2208
.half	2208
.half	2209
.half	2209
.half	4430
.half	4431
.half	4220
.half	4221
.space	192
.half	14528
.half	14529
.half	-22272
.half	-27952
.half	14372
.half	14373
.half	-27944
.half	-27936
.half	-27928
.half	-22208
.half	-27920
.half	-27912
.half	14438
.half	14439
.half	-24128
.half	-27904
.half	8224
.half	8224
.half	8224
.half	8224
.half	8224
.half	8224
.half	8224
.half	8224
.half	8225
.half	8225
.half	8225
.half	8225
.half	8225
.half	8225
.half	8225
.half	8225
.half	10294
.half	10294
.half	10294
.half	10294
.half	10295
.half	10295
.half	10295
.half	10295
.half	14392
.half	14393
.half	-27896
.half	-27888
.half	15624
.half	15625
.half	-27880
.half	-22144
.half	14346
.half	14347
.half	-24096
.half	-27872
.half	-27864
.half	-25920
.half	14446
.half	14447
.half	12372
.half	12372
.half	12373
.half	12373
.half	-24064
.half	-24032
.half	14534
.half	14535
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	12296
.half	12296
.half	12297
.half	12297
.half	14508
.half	14509
.half	14538
.half	14539
.half	10274
.half	10274
.half	10274
.half	10274
.half	10275
.half	10275
.half	10275
.half	10275
.half	10418
.half	10418
.half	10418
.half	10418
.half	10419
.half	10419
.half	10419
.half	10419
.half	-27856
.half	-24000
.half	-27848
.half	-23968
.half	14408
.half	14409
.half	-27840
.half	-23936
.half	8362
.half	8362
.half	8362
.half	8362
.half	8362
.half	8362
.half	8362
.half	8362
.half	8363
.half	8363
.half	8363
.half	8363
.half	8363
.half	8363
.half	8363
.half	8363
.half	10246
.half	10246
.half	10246
.half	10246
.half	10247
.half	10247
.half	10247
.half	10247
.half	10310
.half	10310
.half	10310
.half	10310
.half	10311
.half	10311
.half	10311
.half	10311
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	-27832
.half	-27824
.half	14452
.half	14453
.half	12474
.half	12474
.half	12475
.half	12475
.half	-27816
.half	-27808
.half	-27800
.half	-23904
.half	12382
.half	12382
.half	12383
.half	12383
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	4208
.half	4208
.half	4208
.half	4208
.half	4208
.half	4208
.half	4208
.half	4208
.half	4209
.half	4209
.half	4209
.half	4209
.half	4209
.half	4209
.half	4209
.half	4209
.half	4156
.half	4156
.half	4156
.half	4156
.half	4156
.half	4156
.half	4156
.half	4156
.half	4157
.half	4157
.half	4157
.half	4157
.half	4157
.half	4157
.half	4157
.half	4157
.half	6242
.half	6242
.half	6242
.half	6242
.half	6243
.half	6243
.half	6243
.half	6243
.half	6386
.half	6386
.half	6386
.half	6386
.half	6387
.half	6387
.half	6387
.half	6387
.half	6280
.half	6280
.half	6280
.half	6280
.half	6281
.half	6281
.half	6281
.half	6281
.half	10494
.half	10495
.half	10270
.half	10271
.half	10502
.half	10503
.half	10308
.half	10309
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2088
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	2089
.half	6326
.half	6326
.half	6326
.half	6326
.half	6327
.half	6327
.half	6327
.half	6327
.half	8220
.half	8220
.half	8221
.half	8221
.half	10424
.half	10425
.half	10348
.half	10349
.half	8282
.half	8282
.half	8283
.half	8283
.half	10408
.half	10409
.half	10292
.half	10293
.half	10472
.half	10473
.half	10332
.half	10333
.half	8324
.half	8324
.half	8325
.half	8325
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2122
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	2123
.half	10322
.half	10323
.half	10456
.half	10457
.half	8444
.half	8444
.half	8445
.half	8445
.half	6286
.half	6286
.half	6286
.half	6286
.half	6287
.half	6287
.half	6287
.half	6287
.half	8350
.half	8350
.half	8351
.half	8351
.half	8356
.half	8356
.half	8357
.half	8357
.half	6304
.half	6304
.half	6304
.half	6304
.half	6305
.half	6305
.half	6305
.half	6305
.half	2144
.half	2144
.half	2144
.half	2144
.half	2144
.half	2144
.half	2144
.half	2144
.half	2145
.half	2145
.half	2145
.half	2145
.half	2145
.half	2145
.half	2145
.half	2145
.half	6392
.half	6392
.half	6393
.half	6393
.half	8240
.half	8241
.half	8442
.half	8443
.half	8272
.half	8273
.half	8448
.half	8449
.half	6168
.half	6168
.half	6169
.half	6169
.half	6250
.half	6250
.half	6251
.half	6251
.half	6190
.half	6190
.half	6191
.half	6191
.half	6380
.half	6380
.half	6381
.half	6381
.half	8452
.half	8453
.half	8330
.half	8331
.half	4252
.half	4252
.half	4252
.half	4252
.half	4253
.half	4253
.half	4253
.half	4253
.half	4248
.half	4248
.half	4248
.half	4248
.half	4249
.half	4249
.half	4249
.half	4249
.half	4116
.half	4116
.half	4116
.half	4116
.half	4117
.half	4117
.half	4117
.half	4117
.half	6208
.half	6208
.half	6209
.half	6209
.half	6390
.half	6390
.half	6391
.half	6391
.half	6294
.half	6294
.half	6295
.half	6295
.half	8258
.half	8259
.half	8400
.half	8401
.half	4172
.half	4172
.half	4172
.half	4172
.half	4173
.half	4173
.half	4173
.half	4173
.half	2152
.half	2152
.half	2152
.half	2152
.half	2152
.half	2152
.half	2152
.half	2152
.half	2153
.half	2153
.half	2153
.half	2153
.half	2153
.half	2153
.half	2153
.half	2153
.half	4244
.half	4244
.half	4244
.half	4244
.half	4245
.half	4245
.half	4245
.half	4245
.half	8242
.half	8243
.half	8450
.half	8451
.half	6170
.half	6170
.half	6171
.half	6171
.half	4284
.half	4284
.half	4284
.half	4284
.half	4285
.half	4285
.half	4285
.half	4285
.half	4226
.half	4226
.half	4226
.half	4226
.half	4227
.half	4227
.half	4227
.half	4227
.half	6270
.half	6270
.half	6271
.half	6271
.half	6306
.half	6306
.half	6307
.half	6307
.half	8388
.half	8389
.half	8346
.half	8347
.half	8404
.half	8405
.half	8420
.half	8421
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	6348
.half	6348
.half	6349
.half	6349
.half	6320
.half	6320
.half	6321
.half	6321
.half	6344
.half	6344
.half	6345
.half	6345
.half	8292
.half	8293
.half	8312
.half	8313
.half	2066
.half	2066
.half	2066
.half	2066
.half	2066
.half	2066
.half	2066
.half	2066
.half	2067
.half	2067
.half	2067
.half	2067
.half	2067
.half	2067
.half	2067
.half	2067
.half	4140
.half	4140
.half	4140
.half	4140
.half	4141
.half	4141
.half	4141
.half	4141
.half	6310
.half	6310
.half	6311
.half	6311
.half	8412
.half	8413
.half	8416
.half	8417
.half	2090
.half	2090
.half	2090
.half	2090
.half	2090
.half	2090
.half	2090
.half	2090
.half	2091
.half	2091
.half	2091
.half	2091
.half	2091
.half	2091
.half	2091
.half	2091
.half	6258
.half	6258
.half	6259
.half	6259
.half	8382
.half	8383
.half	8336
.half	8337
.half	4340
.half	4340
.half	4340
.half	4340
.half	4341
.half	4341
.half	4341
.half	4341
.half	2288
.half	2288
.half	2288
.half	2288
.half	2289
.half	2289
.half	2289
.half	2289
.half	6206
.half	6207
.half	6222
.half	6223
.half	4214
.half	4214
.half	4215
.half	4215
.half	206
.half	206
.half	206
.half	206
.half	207
.half	207
.half	207
.half	207
.half	210
.half	210
.half	210
.half	210
.half	211
.half	211
.half	211
.half	211
.half	122
.half	122
.half	122
.half	122
.half	123
.half	123
.half	123
.half	123
.half	12
.half	12
.half	12
.half	12
.half	13
.half	13
.half	13
.half	13
.half	2194
.half	2194
.half	2195
.half	2195
.half	2188
.half	2188
.half	2189
.half	2189
.half	38
.half	38
.half	38
.half	38
.half	39
.half	39
.half	39
.half	39
.half	218
.half	218
.half	218
.half	218
.half	219
.half	219
.half	219
.half	219
.half	214
.half	214
.half	214
.half	214
.half	215
.half	215
.half	215
.half	215
.half	2064
.half	2064
.half	2065
.half	2065
.half	4290
.half	4291
.half	4220
.half	4221
.half	234
.half	234
.half	234
.half	234
.half	235
.half	235
.half	235
.half	235
.half	222
.half	222
.half	222
.half	222
.half	223
.half	223
.half	223
.half	223
.half	128
.half	128
.half	128
.half	128
.half	129
.half	129
.half	129
.half	129
.half	226
.half	226
.half	226
.half	226
.half	227
.half	227
.half	227
.half	227
.half	14
.half	14
.half	14
.half	14
.half	15
.half	15
.half	15
.half	15
.half	58
.half	58
.half	58
.half	58
.half	59
.half	59
.half	59
.half	59
.half	134
.half	134
.half	134
.half	134
.half	135
.half	135
.half	135
.half	135
.half	86
.half	86
.half	86
.half	86
.half	87
.half	87
.half	87
.half	87
.half	180
.half	180
.half	180
.half	180
.half	181
.half	181
.half	181
.half	181
.half	4184
.half	4185
.half	4118
.half	4119
.half	2222
.half	2222
.half	2223
.half	2223
.half	230
.half	230
.half	230
.half	230
.half	231
.half	231
.half	231
.half	231
.space	192
.half	-24448
.half	-28216
.half	-24416
.half	-24384
.half	12344
.half	12345
.half	-28112
.half	-24352
.half	12488
.half	12489
.half	-28096
.half	-24320
.half	12492
.half	12493
.half	12500
.half	12501
.half	10426
.half	10426
.half	10427
.half	10427
.half	-28048
.half	-28040
.half	-28032
.half	-28024
.half	-24288
.half	-27968
.half	12496
.half	12497
.half	10286
.half	10286
.half	10287
.half	10287
.half	8372
.half	8372
.half	8372
.half	8372
.half	8373
.half	8373
.half	8373
.half	8373
.half	8364
.half	8364
.half	8364
.half	8364
.half	8365
.half	8365
.half	8365
.half	8365
.half	10436
.half	10436
.half	10437
.half	10437
.half	12352
.half	12353
.half	-27960
.half	-27952
.half	10432
.half	10432
.half	10433
.half	10433
.half	12360
.half	12361
.half	-26192
.half	-27944
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6144
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	6145
.half	10242
.half	10242
.half	10243
.half	10243
.half	-27936
.half	-24256
.half	-24224
.half	-27832
.half	8220
.half	8220
.half	8220
.half	8220
.half	8221
.half	8221
.half	8221
.half	8221
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4258
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	4259
.half	6402
.half	6402
.half	6403
.half	6403
.half	8326
.half	8327
.half	8242
.half	8243
.half	8274
.half	8275
.half	-28224
.half	-26240
.half	8280
.half	8281
.half	8226
.half	8227
.half	4206
.half	4206
.half	4206
.half	4206
.half	4207
.half	4207
.half	4207
.half	4207
.half	4222
.half	4222
.half	4222
.half	4222
.half	4223
.half	4223
.half	4223
.half	4223
.half	2146
.half	2146
.half	2146
.half	2146
.half	2146
.half	2146
.half	2146
.half	2146
.half	2147
.half	2147
.half	2147
.half	2147
.half	2147
.half	2147
.half	2147
.half	2147
.half	6394
.half	6394
.half	6395
.half	6395
.half	-28208
.half	-28200
.half	-28192
.half	-28184
.half	4344
.half	4344
.half	4344
.half	4344
.half	4345
.half	4345
.half	4345
.half	4345
.half	-28176
.half	-28168
.half	8334
.half	8335
.half	-28160
.half	-28152
.half	-28144
.half	-28136
.half	5416
.half	5416
.half	5416
.half	5416
.half	5417
.half	5417
.half	5417
.half	5417
.half	6326
.half	6326
.half	6327
.half	6327
.half	8348
.half	8349
.half	-28128
.half	-28120
.half	6210
.half	6210
.half	6211
.half	6211
.half	6218
.half	6218
.half	6219
.half	6219
.half	4218
.half	4218
.half	4218
.half	4218
.half	4219
.half	4219
.half	4219
.half	4219
.half	6400
.half	6400
.half	6401
.half	6401
.half	-28104
.half	-26224
.half	8380
.half	8381
.half	2284
.half	2284
.half	2284
.half	2284
.half	2284
.half	2284
.half	2284
.half	2284
.half	2285
.half	2285
.half	2285
.half	2285
.half	2285
.half	2285
.half	2285
.half	2285
.half	6404
.half	6404
.half	6405
.half	6405
.half	-28088
.half	-28080
.half	-28072
.half	-28064
.half	4340
.half	4340
.half	4340
.half	4340
.half	4341
.half	4341
.half	4341
.half	4341
.half	4358
.half	4358
.half	4358
.half	4358
.half	4359
.half	4359
.half	4359
.half	4359
.half	6408
.half	6408
.half	6409
.half	6409
.half	-26208
.half	-28056
.half	8204
.half	8205
.half	8292
.half	8293
.half	8470
.half	8471
.half	8286
.half	8287
.half	8328
.half	8329
.half	4154
.half	4154
.half	4154
.half	4154
.half	4155
.half	4155
.half	4155
.half	4155
.half	4128
.half	4128
.half	4128
.half	4128
.half	4129
.half	4129
.half	4129
.half	4129
.half	-28016
.half	-28008
.half	8304
.half	8305
.half	-28000
.half	-27992
.half	-27984
.half	-27976
.half	4104
.half	4104
.half	4104
.half	4104
.half	4105
.half	4105
.half	4105
.half	4105
.half	-27928
.half	-27920
.half	-27912
.half	-27904
.half	6310
.half	6310
.half	6311
.half	6311
.half	2290
.half	2290
.half	2290
.half	2290
.half	2290
.half	2290
.half	2290
.half	2290
.half	2291
.half	2291
.half	2291
.half	2291
.half	2291
.half	2291
.half	2291
.half	2291
.half	-27896
.half	-27888
.half	-27880
.half	-27872
.half	8390
.half	8391
.half	8458
.half	8459
.half	-27864
.half	-27856
.half	-27848
.half	-27840
.half	6396
.half	6396
.half	6397
.half	6397
.half	2164
.half	2164
.half	2164
.half	2164
.half	2164
.half	2164
.half	2164
.half	2164
.half	2165
.half	2165
.half	2165
.half	2165
.half	2165
.half	2165
.half	2165
.half	2165
.half	2216
.half	2216
.half	2216
.half	2216
.half	2217
.half	2217
.half	2217
.half	2217
.half	4314
.half	4314
.half	4315
.half	4315
.half	6382
.half	6383
.half	6322
.half	6323
.half	2258
.half	2258
.half	2258
.half	2258
.half	2259
.half	2259
.half	2259
.half	2259
.half	6334
.half	6335
.half	6252
.half	6253
.half	4118
.half	4118
.half	4119
.half	4119
.half	2132
.half	2132
.half	2132
.half	2132
.half	2133
.half	2133
.half	2133
.half	2133
.half	4150
.half	4150
.half	4151
.half	4151
.half	6170
.half	6171
.half	6188
.half	6189
.half	4270
.half	4270
.half	4271
.half	4271
.half	6154
.half	6155
.half	6398
.half	6399
.half	2152
.half	2152
.half	2152
.half	2152
.half	2153
.half	2153
.half	2153
.half	2153
.half	194
.half	194
.half	194
.half	194
.half	195
.half	195
.half	195
.half	195
.half	216
.half	216
.half	216
.half	216
.half	217
.half	217
.half	217
.half	217
.half	270
.half	270
.half	270
.half	270
.half	271
.half	271
.half	271
.half	271
.half	60
.half	60
.half	60
.half	60
.half	61
.half	61
.half	61
.half	61
.half	2198
.half	2198
.half	2199
.half	2199
.half	4330
.half	4331
.half	4326
.half	4327
.half	138
.half	138
.half	138
.half	138
.half	139
.half	139
.half	139
.half	139
.half	118
.half	118
.half	118
.half	118
.half	119
.half	119
.half	119
.half	119
.half	140
.half	140
.half	140
.half	140
.half	141
.half	141
.half	141
.half	141
.half	2254
.half	2254
.half	2255
.half	2255
.half	2262
.half	2262
.half	2263
.half	2263
.half	14
.half	14
.half	14
.half	14
.half	15
.half	15
.half	15
.half	15
.half	144
.half	144
.half	144
.half	144
.half	145
.half	145
.half	145
.half	145
.half	2086
.half	2086
.half	2087
.half	2087
.half	2340
.half	2340
.half	2341
.half	2341
.half	154
.half	154
.half	154
.half	154
.half	155
.half	155
.half	155
.half	155
.half	2172
.half	2172
.half	2173
.half	2173
.half	4384
.half	4385
.half	4158
.half	4159
.half	80
.half	80
.half	80
.half	80
.half	81
.half	81
.half	81
.half	81
.half	268
.half	268
.half	268
.half	268
.half	269
.half	269
.half	269
.half	269
.half	224
.half	224
.half	224
.half	224
.half	225
.half	225
.half	225
.half	225
.half	106
.half	106
.half	106
.half	106
.half	107
.half	107
.half	107
.half	107
.half	158
.half	158
.half	158
.half	158
.half	159
.half	159
.half	159
.half	159
.half	36
.half	36
.half	36
.half	36
.half	37
.half	37
.half	37
.half	37
.half	4198
.half	4199
.half	4378
.half	4379
.half	2176
.half	2176
.half	2177
.half	2177
.half	2324
.half	2324
.half	2325
.half	2325
.half	4138
.half	4139
.half	4266
.half	4267
.half	220
.half	220
.half	220
.half	220
.half	221
.half	221
.half	221
.half	221
.half	232
.half	232
.half	232
.half	232
.half	233
.half	233
.half	233
.half	233
.half	228
.half	228
.half	228
.half	228
.half	229
.half	229
.half	229
.half	229
.half	4
.half	4
.half	4
.half	4
.half	5
.half	5
.half	5
.half	5
.half	2066
.half	2066
.half	2067
.half	2067
.half	2194
.half	2194
.half	2195
.half	2195
.half	2196
.half	2196
.half	2197
.half	2197
.half	4256
.half	4257
.half	4216
.half	4217
.half	202
.half	202
.half	202
.half	202
.half	203
.half	203
.half	203
.half	203
.half	152
.half	152
.half	152
.half	152
.half	153
.half	153
.half	153
.half	153
.half	2338
.half	2338
.half	2339
.half	2339
.half	2138
.half	2138
.half	2139
.half	2139
.half	52
.half	52
.half	52
.half	52
.half	53
.half	53
.half	53
.half	53
.half	86
.half	86
.half	86
.half	86
.half	87
.half	87
.half	87
.half	87
.half	4342
.half	4343
.half	4226
.half	4227
.half	2096
.half	2096
.half	2097
.half	2097
.half	30
.half	30
.half	30
.half	30
.half	31
.half	31
.half	31
.half	31
.half	2288
.half	2288
.half	2289
.half	2289
.half	2054
.half	2054
.half	2055
.half	2055
.half	164
.half	164
.half	164
.half	164
.half	165
.half	165
.half	165
.half	165
.half	2232
.half	2232
.half	2233
.half	2233
.half	4192
.half	4193
.half	4380
.half	4381
.half	176
.half	176
.half	176
.half	176
.half	177
.half	177
.half	177
.half	177
.half	16
.half	16
.half	16
.half	16
.half	17
.half	17
.half	17
.half	17
.half	2322
.half	2322
.half	2323
.half	2323
.half	2328
.half	2328
.half	2329
.half	2329
.half	2180
.half	2180
.half	2181
.half	2181
.half	2162
.half	2162
.half	2163
.half	2163
.half	272
.half	272
.half	272
.half	272
.half	273
.half	273
.half	273
.half	273
.half	226
.half	226
.half	226
.half	226
.half	227
.half	227
.half	227
.half	227
.half	76
.half	76
.half	76
.half	76
.half	77
.half	77
.half	77
.half	77
.half	2088
.half	2088
.half	2089
.half	2089
.half	4120
.half	4121
.half	4166
.half	4167
.half	68
.half	68
.half	68
.half	68
.half	69
.half	69
.half	69
.half	69
.half	2342
.half	2342
.half	2343
.half	2343
.half	4382
.half	4383
.half	4174
.half	4175
.half	2270
.half	2270
.half	2271
.half	2271
.half	2068
.half	2068
.half	2069
.half	2069
.half	92
.half	92
.half	92
.half	92
.half	93
.half	93
.half	93
.half	93
.space	256
.half	-26336
.half	-28352
.half	-28344
.half	-28336
.half	-26320
.half	-24320
.half	15564
.half	15565
.half	-28328
.half	-28320
.half	-28312
.half	-28304
.half	-28296
.half	-28288
.half	-28280
.half	-28272
.half	-28264
.half	-28256
.half	-28248
.half	-28240
.half	-28232
.half	-28224
.half	-28216
.half	-28208
.half	-28200
.half	-28192
.half	-28184
.half	-28176
.half	-28168
.half	-28160
.half	-28152
.half	-28144
.half	14500
.half	14501
.half	14496
.half	14497
.half	14440
.half	14441
.half	14504
.half	14505
.half	14446
.half	14447
.half	14412
.half	14413
.half	14394
.half	14395
.half	14352
.half	14353
.half	12424
.half	12424
.half	12425
.half	12425
.half	12386
.half	12386
.half	12387
.half	12387
.half	12444
.half	12444
.half	12445
.half	12445
.half	12438
.half	12438
.half	12439
.half	12439
.half	12380
.half	12380
.half	12381
.half	12381
.half	12372
.half	12372
.half	12373
.half	12373
.half	12302
.half	12302
.half	12303
.half	12303
.half	12300
.half	12300
.half	12301
.half	12301
.half	12344
.half	12344
.half	12345
.half	12345
.half	12298
.half	12298
.half	12299
.half	12299
.half	10314
.half	10314
.half	10314
.half	10314
.half	10315
.half	10315
.half	10315
.half	10315
.half	10248
.half	10248
.half	10248
.half	10248
.half	10249
.half	10249
.half	10249
.half	10249
.half	10246
.half	10246
.half	10246
.half	10246
.half	10247
.half	10247
.half	10247
.half	10247
.half	8326
.half	8326
.half	8326
.half	8326
.half	8326
.half	8326
.half	8326
.half	8326
.half	8327
.half	8327
.half	8327
.half	8327
.half	8327
.half	8327
.half	8327
.half	8327
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	8246
.half	8246
.half	8246
.half	8246
.half	8246
.half	8246
.half	8246
.half	8246
.half	8247
.half	8247
.half	8247
.half	8247
.half	8247
.half	8247
.half	8247
.half	8247
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8240
.half	8241
.half	8242
.half	8243
.half	8244
.half	8245
.half	8262
.half	8263
.half	8300
.half	8301
.half	8264
.half	8265
.half	8274
.half	8275
.half	8306
.half	8307
.half	8324
.half	8325
.half	8340
.half	8341
.half	8362
.half	8363
.half	8366
.half	8367
.half	8388
.half	8389
.half	8390
.half	8391
.half	8392
.half	8393
.half	8394
.half	8395
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	6290
.half	6291
.half	6288
.half	6289
.half	6186
.half	6187
.half	6184
.half	6185
.half	6188
.half	6189
.half	6190
.half	6191
.half	6212
.half	6213
.half	6266
.half	6267
.half	6306
.half	6307
.half	6310
.half	6311
.half	6336
.half	6337
.half	6338
.half	6339
.half	4254
.half	4255
.half	4250
.half	4251
.half	4238
.half	4239
.half	4226
.half	4227
.half	4198
.half	4199
.half	4214
.half	4215
.half	4192
.half	4193
.half	4186
.half	4187
.half	4176
.half	4177
.half	4162
.half	4163
.half	4134
.half	4135
.half	4132
.half	4133
.half	4130
.half	4131
.half	4128
.half	4129
.half	2238
.half	2238
.half	2239
.half	2239
.half	2236
.half	2236
.half	2237
.half	2237
.half	2234
.half	2234
.half	2235
.half	2235
.half	2232
.half	2232
.half	2233
.half	2233
.half	2230
.half	2230
.half	2231
.half	2231
.half	2200
.half	2200
.half	2201
.half	2201
.half	2188
.half	2188
.half	2189
.half	2189
.half	2176
.half	2176
.half	2177
.half	2177
.half	2174
.half	2174
.half	2175
.half	2175
.half	2160
.half	2160
.half	2161
.half	2161
.half	2154
.half	2154
.half	2155
.half	2155
.half	2148
.half	2148
.half	2149
.half	2149
.half	2136
.half	2136
.half	2137
.half	2137
.half	2126
.half	2126
.half	2127
.half	2127
.half	2112
.half	2112
.half	2113
.half	2113
.half	2110
.half	2110
.half	2111
.half	2111
.half	2078
.half	2078
.half	2079
.half	2079
.half	2142
.half	2142
.half	2143
.half	2143
.half	2076
.half	2076
.half	2077
.half	2077
.half	2074
.half	2074
.half	2075
.half	2075
.half	2072
.half	2072
.half	2073
.half	2073
.half	178
.half	178
.half	178
.half	178
.half	179
.half	179
.half	179
.half	179
.half	176
.half	176
.half	176
.half	176
.half	177
.half	177
.half	177
.half	177
.half	172
.half	172
.half	172
.half	172
.half	173
.half	173
.half	173
.half	173
.half	138
.half	138
.half	138
.half	138
.half	139
.half	139
.half	139
.half	139
.half	124
.half	124
.half	124
.half	124
.half	125
.half	125
.half	125
.half	125
.half	120
.half	120
.half	120
.half	120
.half	121
.half	121
.half	121
.half	121
.half	116
.half	116
.half	116
.half	116
.half	117
.half	117
.half	117
.half	117
.half	180
.half	180
.half	180
.half	180
.half	181
.half	181
.half	181
.half	181
.half	86
.half	86
.half	86
.half	86
.half	87
.half	87
.half	87
.half	87
.half	60
.half	60
.half	60
.half	60
.half	61
.half	61
.half	61
.half	61
.half	22
.half	22
.half	22
.half	22
.half	23
.half	23
.half	23
.half	23
.half	20
.half	20
.half	20
.half	20
.half	21
.half	21
.half	21
.half	21
.half	18
.half	18
.half	18
.half	18
.half	19
.half	19
.half	19
.half	19
.space	880
.half	-26336
.half	-28352
.half	-28344
.half	-28336
.half	-26320
.half	-24320
.half	15564
.half	15565
.half	-28328
.half	-28320
.half	-28312
.half	-28304
.half	-28296
.half	-28288
.half	-28280
.half	-28272
.half	-28264
.half	-28256
.half	-28248
.half	-28240
.half	-28232
.half	-28224
.half	-28216
.half	-28208
.half	-28200
.half	-28192
.half	-28184
.half	-28176
.half	-28168
.half	-28160
.half	-28152
.half	-28144
.half	14474
.half	14475
.half	14472
.half	14473
.half	14470
.half	14471
.half	14468
.half	14469
.half	14422
.half	14423
.half	14420
.half	14421
.half	14416
.half	14417
.half	14342
.half	14343
.half	12418
.half	12418
.half	12419
.half	12419
.half	12416
.half	12416
.half	12417
.half	12417
.half	12414
.half	12414
.half	12415
.half	12415
.half	12410
.half	12410
.half	12411
.half	12411
.half	12364
.half	12364
.half	12365
.half	12365
.half	12360
.half	12360
.half	12361
.half	12361
.half	12356
.half	12356
.half	12357
.half	12357
.half	12350
.half	12350
.half	12351
.half	12351
.half	12314
.half	12314
.half	12315
.half	12315
.half	12292
.half	12292
.half	12293
.half	12293
.half	10296
.half	10296
.half	10296
.half	10296
.half	10297
.half	10297
.half	10297
.half	10297
.half	10290
.half	10290
.half	10290
.half	10290
.half	10291
.half	10291
.half	10291
.half	10291
.half	10284
.half	10284
.half	10284
.half	10284
.half	10285
.half	10285
.half	10285
.half	10285
.half	8308
.half	8308
.half	8308
.half	8308
.half	8308
.half	8308
.half	8308
.half	8308
.half	8309
.half	8309
.half	8309
.half	8309
.half	8309
.half	8309
.half	8309
.half	8309
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6168
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	6169
.half	8228
.half	8228
.half	8228
.half	8228
.half	8228
.half	8228
.half	8228
.half	8228
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8194
.half	8194
.half	8194
.half	8194
.half	8194
.half	8194
.half	8194
.half	8194
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8226
.half	8227
.half	8234
.half	8235
.half	8246
.half	8247
.half	8252
.half	8253
.half	8258
.half	8259
.half	8274
.half	8275
.half	8304
.half	8305
.half	8306
.half	8307
.half	8380
.half	8381
.half	8382
.half	8383
.half	8384
.half	8385
.half	8386
.half	8387
.half	8388
.half	8389
.half	8390
.half	8391
.half	8392
.half	8393
.half	8394
.half	8395
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	6268
.half	6269
.half	6264
.half	6265
.half	6164
.half	6165
.half	6162
.half	6163
.half	6166
.half	6167
.half	6176
.half	6177
.half	6252
.half	6253
.half	6254
.half	6255
.half	6324
.half	6325
.half	6326
.half	6327
.half	6328
.half	6329
.half	6330
.half	6331
.half	4274
.half	4275
.half	4272
.half	4273
.half	4270
.half	4271
.half	4268
.half	4269
.half	4174
.half	4175
.half	4170
.half	4171
.half	4166
.half	4167
.half	4160
.half	4161
.half	4154
.half	4155
.half	4144
.half	4145
.half	4136
.half	4137
.half	4126
.half	4127
.half	4112
.half	4113
.half	4110
.half	4111
.half	2218
.half	2218
.half	2219
.half	2219
.half	2216
.half	2216
.half	2217
.half	2217
.half	2214
.half	2214
.half	2215
.half	2215
.half	2212
.half	2212
.half	2213
.half	2213
.half	2210
.half	2210
.half	2211
.half	2211
.half	2208
.half	2208
.half	2209
.half	2209
.half	2206
.half	2206
.half	2207
.half	2207
.half	2204
.half	2204
.half	2205
.half	2205
.half	2166
.half	2166
.half	2167
.half	2167
.half	2154
.half	2154
.half	2155
.half	2155
.half	2152
.half	2152
.half	2153
.half	2153
.half	2150
.half	2150
.half	2151
.half	2151
.half	2148
.half	2148
.half	2149
.half	2149
.half	2146
.half	2146
.half	2147
.half	2147
.half	2144
.half	2144
.half	2145
.half	2145
.half	2142
.half	2142
.half	2143
.half	2143
.half	2140
.half	2140
.half	2141
.half	2141
.half	2100
.half	2100
.half	2101
.half	2101
.half	2094
.half	2094
.half	2095
.half	2095
.half	2060
.half	2060
.half	2061
.half	2061
.half	2058
.half	2058
.half	2059
.half	2059
.half	154
.half	154
.half	154
.half	154
.half	155
.half	155
.half	155
.half	155
.half	152
.half	152
.half	152
.half	152
.half	153
.half	153
.half	153
.half	153
.half	150
.half	150
.half	150
.half	150
.half	151
.half	151
.half	151
.half	151
.half	148
.half	148
.half	148
.half	148
.half	149
.half	149
.half	149
.half	149
.half	146
.half	146
.half	146
.half	146
.half	147
.half	147
.half	147
.half	147
.half	144
.half	144
.half	144
.half	144
.half	145
.half	145
.half	145
.half	145
.half	142
.half	142
.half	142
.half	142
.half	143
.half	143
.half	143
.half	143
.half	140
.half	140
.half	140
.half	140
.half	141
.half	141
.half	141
.half	141
.half	90
.half	90
.half	90
.half	90
.half	91
.half	91
.half	91
.half	91
.half	88
.half	88
.half	88
.half	88
.half	89
.half	89
.half	89
.half	89
.half	38
.half	38
.half	38
.half	38
.half	39
.half	39
.half	39
.half	39
.half	28
.half	28
.half	28
.half	28
.half	29
.half	29
.half	29
.half	29
.half	8
.half	8
.half	8
.half	8
.half	9
.half	9
.half	9
.half	9
.space	880
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	10392
.half	10392
.half	10392
.half	10392
.half	10393
.half	10393
.half	10393
.half	10393
.half	-28416
.half	-28408
.half	14534
.half	14535
.half	14364
.half	14365
.half	-28368
.half	-28312
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8199
.half	8199
.half	8199
.half	8199
.half	8199
.half	8199
.half	8199
.half	8199
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	6147
.half	10354
.half	10354
.half	10354
.half	10354
.half	10355
.half	10355
.half	10355
.half	10355
.half	12306
.half	12306
.half	12307
.half	12307
.half	-28304
.half	-28280
.half	14362
.half	14363
.half	8304
.half	8304
.half	8304
.half	8304
.half	8304
.half	8304
.half	8304
.half	8304
.half	8305
.half	8305
.half	8305
.half	8305
.half	8305
.half	8305
.half	8305
.half	8305
.half	12460
.half	12460
.half	12461
.half	12461
.half	12404
.half	12404
.half	12405
.half	12405
.half	-28256
.half	-28248
.half	-28240
.half	-28200
.half	-28192
.half	-28184
.half	-28176
.half	-28136
.half	10250
.half	10250
.half	10250
.half	10250
.half	10251
.half	10251
.half	10251
.half	10251
.half	12304
.half	12304
.half	12305
.half	12305
.half	-28128
.half	-28104
.half	14360
.half	14361
.half	-28096
.half	-28072
.half	-28064
.half	-28056
.half	14454
.half	14455
.half	14490
.half	14491
.half	-28048
.half	-27992
.half	14522
.half	14523
.half	-27984
.half	-27960
.half	14358
.half	14359
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	10248
.half	10248
.half	10248
.half	10248
.half	10249
.half	10249
.half	10249
.half	10249
.half	12302
.half	12302
.half	12303
.half	12303
.half	-27952
.half	-27928
.half	-27872
.half	-27864
.half	14588
.half	14589
.half	-27856
.half	-27816
.half	15684
.half	15685
.half	14356
.half	14357
.half	-27808
.half	-27768
.half	-27744
.half	-27736
.half	12300
.half	12300
.half	12301
.half	12301
.half	216
.half	216
.half	216
.half	216
.half	217
.half	217
.half	217
.half	217
.half	-28400
.half	-28392
.half	-28384
.half	-28376
.half	4164
.half	4165
.half	4230
.half	4231
.half	2152
.half	2152
.half	2153
.half	2153
.half	2218
.half	2218
.half	2219
.half	2219
.half	268
.half	268
.half	268
.half	268
.half	269
.half	269
.half	269
.half	269
.half	4336
.half	4337
.half	4386
.half	4387
.half	2154
.half	2154
.half	2155
.half	2155
.half	316
.half	316
.half	316
.half	316
.half	317
.half	317
.half	317
.half	317
.half	2176
.half	2176
.half	2177
.half	2177
.half	-28360
.half	-28352
.half	-28344
.half	-28320
.half	86
.half	86
.half	86
.half	86
.half	87
.half	87
.half	87
.half	87
.half	140
.half	140
.half	140
.half	140
.half	141
.half	141
.half	141
.half	141
.half	-28336
.half	-28328
.half	4382
.half	4383
.half	2326
.half	2326
.half	2327
.half	2327
.half	298
.half	298
.half	298
.half	298
.half	299
.half	299
.half	299
.half	299
.half	270
.half	270
.half	270
.half	270
.half	271
.half	271
.half	271
.half	271
.half	192
.half	192
.half	192
.half	192
.half	193
.half	193
.half	193
.half	193
.half	38
.half	38
.half	38
.half	38
.half	39
.half	39
.half	39
.half	39
.half	2248
.half	2248
.half	2249
.half	2249
.half	-28296
.half	-28288
.half	4162
.half	4163
.half	2284
.half	2284
.half	2285
.half	2285
.half	2242
.half	2242
.half	2243
.half	2243
.half	224
.half	224
.half	224
.half	224
.half	225
.half	225
.half	225
.half	225
.half	4400
.half	4401
.half	-28272
.half	-28264
.half	2098
.half	2098
.half	2099
.half	2099
.half	84
.half	84
.half	84
.half	84
.half	85
.half	85
.half	85
.half	85
.half	2150
.half	2150
.half	2151
.half	2151
.half	2196
.half	2196
.half	2197
.half	2197
.half	4160
.half	4161
.half	4274
.half	4275
.half	2224
.half	2224
.half	2225
.half	2225
.half	36
.half	36
.half	36
.half	36
.half	37
.half	37
.half	37
.half	37
.half	-28232
.half	-28224
.half	-28216
.half	-28208
.half	2302
.half	2302
.half	2303
.half	2303
.half	2194
.half	2194
.half	2195
.half	2195
.half	2268
.half	2268
.half	2269
.half	2269
.half	82
.half	82
.half	82
.half	82
.half	83
.half	83
.half	83
.half	83
.half	256
.half	256
.half	256
.half	256
.half	257
.half	257
.half	257
.half	257
.half	2252
.half	2252
.half	2253
.half	2253
.half	2368
.half	2368
.half	2369
.half	2369
.half	2332
.half	2332
.half	2333
.half	2333
.half	4228
.half	4229
.half	4286
.half	4287
.half	266
.half	266
.half	266
.half	266
.half	267
.half	267
.half	267
.half	267
.half	122
.half	122
.half	122
.half	122
.half	123
.half	123
.half	123
.half	123
.half	-28168
.half	-28160
.half	-28152
.half	-28144
.half	2096
.half	2096
.half	2097
.half	2097
.half	138
.half	138
.half	138
.half	138
.half	139
.half	139
.half	139
.half	139
.half	2322
.half	2322
.half	2323
.half	2323
.half	2148
.half	2148
.half	2149
.half	2149
.half	80
.half	80
.half	80
.half	80
.half	81
.half	81
.half	81
.half	81
.half	180
.half	180
.half	180
.half	180
.half	181
.half	181
.half	181
.half	181
.half	156
.half	156
.half	156
.half	156
.half	157
.half	157
.half	157
.half	157
.half	-28120
.half	-28112
.half	4158
.half	4159
.half	2174
.half	2174
.half	2175
.half	2175
.half	4302
.half	4303
.half	4292
.half	4293
.half	2216
.half	2216
.half	2217
.half	2217
.half	242
.half	242
.half	242
.half	242
.half	243
.half	243
.half	243
.half	243
.half	4330
.half	4331
.half	4392
.half	4393
.half	2206
.half	2206
.half	2207
.half	2207
.half	2270
.half	2270
.half	2271
.half	2271
.half	-28088
.half	-28080
.half	4156
.half	4157
.half	78
.half	78
.half	78
.half	78
.half	79
.half	79
.half	79
.half	79
.half	2146
.half	2146
.half	2147
.half	2147
.half	2144
.half	2144
.half	2145
.half	2145
.half	34
.half	34
.half	34
.half	34
.half	35
.half	35
.half	35
.half	35
.half	2328
.half	2328
.half	2329
.half	2329
.half	2094
.half	2094
.half	2095
.half	2095
.half	174
.half	174
.half	174
.half	174
.half	175
.half	175
.half	175
.half	175
.half	-28040
.half	-28032
.half	4306
.half	4307
.half	4396
.half	4397
.half	-28008
.half	-28000
.half	164
.half	164
.half	164
.half	164
.half	165
.half	165
.half	165
.half	165
.half	4378
.half	4379
.half	4322
.half	4323
.half	-28024
.half	-28016
.half	4360
.half	4361
.half	2296
.half	2296
.half	2297
.half	2297
.half	4414
.half	4415
.half	4406
.half	4407
.half	244
.half	244
.half	244
.half	244
.half	245
.half	245
.half	245
.half	245
.half	202
.half	202
.half	202
.half	202
.half	203
.half	203
.half	203
.half	203
.half	2142
.half	2142
.half	2143
.half	2143
.half	4310
.half	4311
.half	4354
.half	4355
.half	208
.half	208
.half	208
.half	208
.half	209
.half	209
.half	209
.half	209
.half	-27976
.half	-27968
.half	4154
.half	4155
.half	2092
.half	2092
.half	2093
.half	2093
.half	312
.half	312
.half	312
.half	312
.half	313
.half	313
.half	313
.half	313
.half	76
.half	76
.half	76
.half	76
.half	77
.half	77
.half	77
.half	77
.half	32
.half	32
.half	32
.half	32
.half	33
.half	33
.half	33
.half	33
.half	-27944
.half	-27936
.half	4226
.half	4227
.half	4388
.half	4389
.half	4256
.half	4257
.half	2192
.half	2192
.half	2193
.half	2193
.half	2278
.half	2278
.half	2279
.half	2279
.half	2294
.half	2294
.half	2295
.half	2295
.half	2230
.half	2230
.half	2231
.half	2231
.half	2236
.half	2236
.half	2237
.half	2237
.half	-27920
.half	-27912
.half	-27888
.half	-27880
.half	74
.half	74
.half	74
.half	74
.half	75
.half	75
.half	75
.half	75
.half	-27904
.half	-27896
.half	4206
.half	4207
.half	2140
.half	2140
.half	2141
.half	2141
.half	294
.half	294
.half	294
.half	294
.half	295
.half	295
.half	295
.half	295
.half	302
.half	302
.half	302
.half	302
.half	303
.half	303
.half	303
.half	303
.half	136
.half	136
.half	136
.half	136
.half	137
.half	137
.half	137
.half	137
.half	72
.half	72
.half	72
.half	72
.half	73
.half	73
.half	73
.half	73
.half	2324
.half	2324
.half	2325
.half	2325
.half	2090
.half	2090
.half	2091
.half	2091
.half	30
.half	30
.half	30
.half	30
.half	31
.half	31
.half	31
.half	31
.half	4152
.half	4153
.half	-27848
.half	-27840
.half	2172
.half	2172
.half	2173
.half	2173
.half	2138
.half	2138
.half	2139
.half	2139
.half	2190
.half	2190
.half	2191
.half	2191
.half	4246
.half	4247
.half	-27832
.half	-27824
.half	2136
.half	2136
.half	2137
.half	2137
.half	2362
.half	2362
.half	2363
.half	2363
.half	2354
.half	2354
.half	2355
.half	2355
.half	232
.half	232
.half	232
.half	232
.half	233
.half	233
.half	233
.half	233
.half	120
.half	120
.half	120
.half	120
.half	121
.half	121
.half	121
.half	121
.half	-27800
.half	-27792
.half	4150
.half	4151
.half	4324
.half	4325
.half	-27784
.half	-27776
.half	70
.half	70
.half	70
.half	70
.half	71
.half	71
.half	71
.half	71
.half	238
.half	238
.half	238
.half	238
.half	239
.half	239
.half	239
.half	239
.half	4418
.half	4419
.half	4204
.half	4205
.half	2214
.half	2214
.half	2215
.half	2215
.half	308
.half	308
.half	308
.half	308
.half	309
.half	309
.half	309
.half	309
.half	2088
.half	2088
.half	2089
.half	2089
.half	-27760
.half	-27752
.half	4384
.half	4385
.half	218
.half	218
.half	218
.half	218
.half	219
.half	219
.half	219
.half	219
.half	4346
.half	4347
.half	4280
.half	4281
.half	2260
.half	2260
.half	2261
.half	2261
.half	260
.half	260
.half	260
.half	260
.half	261
.half	261
.half	261
.half	261
.half	2320
.half	2320
.half	2321
.half	2321
.half	-27728
.half	-27720
.half	4148
.half	4149
.half	262
.half	262
.half	262
.half	262
.half	263
.half	263
.half	263
.half	263
.half	162
.half	162
.half	162
.half	162
.half	163
.half	163
.half	163
.half	163
.space	32
.half	6146
.half	6146
.half	6146
.half	6146
.half	6147
.half	6147
.half	6147
.half	6147
.half	10246
.half	10247
.half	-24512
.half	-28176
.half	-28168
.half	-24480
.half	10358
.half	10359
.half	-28144
.half	-24448
.half	-28104
.half	-28096
.half	10306
.half	10307
.half	-24416
.half	-27944
.half	6208
.half	6208
.half	6208
.half	6208
.half	6209
.half	6209
.half	6209
.half	6209
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	8282
.half	8282
.half	8283
.half	8283
.half	-24384
.half	-27864
.half	10346
.half	10347
.half	-24352
.half	-27800
.half	-24320
.half	-24288
.half	10244
.half	10245
.half	-24256
.half	-27704
.half	2300
.half	2300
.half	2300
.half	2300
.half	2300
.half	2300
.half	2300
.half	2300
.half	2301
.half	2301
.half	2301
.half	2301
.half	2301
.half	2301
.half	2301
.half	2301
.half	8496
.half	8497
.half	-28208
.half	-28200
.half	8374
.half	8375
.half	-28192
.half	-28184
.half	6318
.half	6318
.half	6319
.half	6319
.half	6166
.half	6166
.half	6167
.half	6167
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2060
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	2061
.half	-28160
.half	-28152
.half	8492
.half	8493
.half	6364
.half	6364
.half	6365
.half	6365
.half	4360
.half	4360
.half	4360
.half	4360
.half	4361
.half	4361
.half	4361
.half	4361
.half	4190
.half	4190
.half	4190
.half	4190
.half	4191
.half	4191
.half	4191
.half	4191
.half	4258
.half	4258
.half	4258
.half	4258
.half	4259
.half	4259
.half	4259
.half	4259
.half	-28136
.half	-28128
.half	8420
.half	8421
.half	-28120
.half	-28112
.half	8488
.half	8489
.half	4112
.half	4112
.half	4112
.half	4112
.half	4113
.half	4113
.half	4113
.half	4113
.half	6428
.half	6428
.half	6429
.half	6429
.half	8220
.half	8221
.half	-28088
.half	-26272
.half	-28032
.half	-28024
.half	8330
.half	8331
.half	8268
.half	8269
.half	-26256
.half	-27984
.half	6424
.half	6424
.half	6425
.half	6425
.half	6164
.half	6164
.half	6165
.half	6165
.half	-27976
.half	-27968
.half	8218
.half	8219
.half	8484
.half	8485
.half	-27960
.half	-27952
.half	2192
.half	2192
.half	2192
.half	2192
.half	2192
.half	2192
.half	2192
.half	2192
.half	2193
.half	2193
.half	2193
.half	2193
.half	2193
.half	2193
.half	2193
.half	2193
.half	-27936
.half	-27928
.half	-26240
.half	-27904
.half	-27896
.half	-27888
.half	-27880
.half	-27872
.half	6420
.half	6420
.half	6421
.half	6421
.half	6314
.half	6314
.half	6315
.half	6315
.half	2058
.half	2058
.half	2058
.half	2058
.half	2058
.half	2058
.half	2058
.half	2058
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	4204
.half	4204
.half	4204
.half	4204
.half	4205
.half	4205
.half	4205
.half	4205
.half	-26224
.half	-27808
.half	8370
.half	8371
.half	6416
.half	6416
.half	6417
.half	6417
.half	4252
.half	4252
.half	4252
.half	4252
.half	4253
.half	4253
.half	4253
.half	4253
.half	-26208
.half	-27792
.half	8302
.half	8303
.half	6162
.half	6162
.half	6163
.half	6163
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2286
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	2287
.half	6264
.half	6264
.half	6265
.half	6265
.half	6216
.half	6216
.half	6217
.half	6217
.half	-26192
.half	-27784
.half	8216
.half	8217
.half	6412
.half	6412
.half	6413
.half	6413
.half	2116
.half	2116
.half	2116
.half	2116
.half	2116
.half	2116
.half	2116
.half	2116
.half	2117
.half	2117
.half	2117
.half	2117
.half	2117
.half	2117
.half	2117
.half	2117
.half	8288
.half	8289
.half	8322
.half	8323
.half	6310
.half	6310
.half	6311
.half	6311
.half	-27776
.half	-27768
.half	8266
.half	8267
.half	8480
.half	8481
.half	-27760
.half	-26176
.half	2140
.half	2140
.half	2140
.half	2140
.half	2140
.half	2140
.half	2140
.half	2140
.half	2141
.half	2141
.half	2141
.half	2141
.half	2141
.half	2141
.half	2141
.half	2141
.half	6286
.half	6287
.half	-28080
.half	-28040
.half	4154
.half	4154
.half	4155
.half	4155
.half	2206
.half	2206
.half	2206
.half	2206
.half	2207
.half	2207
.half	2207
.half	2207
.half	2180
.half	2180
.half	2180
.half	2180
.half	2181
.half	2181
.half	2181
.half	2181
.half	4260
.half	4260
.half	4261
.half	4261
.half	6316
.half	6317
.half	-28016
.half	-27992
.half	2288
.half	2288
.half	2288
.half	2288
.half	2289
.half	2289
.half	2289
.half	2289
.half	6270
.half	6271
.half	-27920
.half	-27912
.half	4182
.half	4182
.half	4183
.half	4183
.half	4150
.half	4150
.half	4151
.half	4151
.half	-27856
.half	-27816
.half	6278
.half	6279
.half	2148
.half	2148
.half	2148
.half	2148
.half	2149
.half	2149
.half	2149
.half	2149
.half	2270
.half	2270
.half	2270
.half	2270
.half	2271
.half	2271
.half	2271
.half	2271
.half	6398
.half	6399
.half	6292
.half	6293
.half	4236
.half	4236
.half	4237
.half	4237
.half	2250
.half	2250
.half	2250
.half	2250
.half	2251
.half	2251
.half	2251
.half	2251
.half	4148
.half	4148
.half	4149
.half	4149
.half	6312
.half	6313
.half	6260
.half	6261
.half	4180
.half	4180
.half	4181
.half	4181
.half	-27752
.half	-27728
.half	-27720
.half	-27712
.half	2246
.half	2246
.half	2246
.half	2246
.half	2247
.half	2247
.half	2247
.half	2247
.half	80
.half	80
.half	80
.half	80
.half	81
.half	81
.half	81
.half	81
.half	2098
.half	2098
.half	2099
.half	2099
.half	4320
.half	4321
.half	4346
.half	4347
.half	38
.half	38
.half	38
.half	38
.half	39
.half	39
.half	39
.half	39
.half	4200
.half	4201
.half	4158
.half	4159
.half	4184
.half	4185
.half	4312
.half	4313
.half	232
.half	232
.half	232
.half	232
.half	233
.half	233
.half	233
.half	233
.half	136
.half	136
.half	136
.half	136
.half	137
.half	137
.half	137
.half	137
.half	194
.half	194
.half	194
.half	194
.half	195
.half	195
.half	195
.half	195
.half	324
.half	324
.half	324
.half	324
.half	325
.half	325
.half	325
.half	325
.half	2198
.half	2198
.half	2199
.half	2199
.half	2296
.half	2296
.half	2297
.half	2297
.half	4156
.half	4157
.half	4442
.half	4443
.half	2258
.half	2258
.half	2259
.half	2259
.half	320
.half	320
.half	320
.half	320
.half	321
.half	321
.half	321
.half	321
.half	2096
.half	2096
.half	2097
.half	2097
.half	2388
.half	2388
.half	2389
.half	2389
.half	234
.half	234
.half	234
.half	234
.half	235
.half	235
.half	235
.half	235
.half	8
.half	8
.half	8
.half	8
.half	9
.half	9
.half	9
.half	9
.half	226
.half	226
.half	226
.half	226
.half	227
.half	227
.half	227
.half	227
.half	36
.half	36
.half	36
.half	36
.half	37
.half	37
.half	37
.half	37
.half	-28072
.half	-28064
.half	4382
.half	4383
.half	2326
.half	2326
.half	2327
.half	2327
.half	196
.half	196
.half	196
.half	196
.half	197
.half	197
.half	197
.half	197
.half	-28056
.half	-28048
.half	4410
.half	4411
.half	2354
.half	2354
.half	2355
.half	2355
.half	318
.half	318
.half	318
.half	318
.half	319
.half	319
.half	319
.half	319
.half	2386
.half	2386
.half	2387
.half	2387
.half	2382
.half	2382
.half	2383
.half	2383
.half	266
.half	266
.half	266
.half	266
.half	267
.half	267
.half	267
.half	267
.half	190
.half	190
.half	190
.half	190
.half	191
.half	191
.half	191
.half	191
.half	112
.half	112
.half	112
.half	112
.half	113
.half	113
.half	113
.half	113
.half	-28008
.half	-28000
.half	4284
.half	4285
.half	2318
.half	2318
.half	2319
.half	2319
.half	290
.half	290
.half	290
.half	290
.half	291
.half	291
.half	291
.half	291
.half	200
.half	200
.half	200
.half	200
.half	201
.half	201
.half	201
.half	201
.half	160
.half	160
.half	160
.half	160
.half	161
.half	161
.half	161
.half	161
.half	2094
.half	2094
.half	2095
.half	2095
.half	2130
.half	2130
.half	2131
.half	2131
.half	122
.half	122
.half	122
.half	122
.half	123
.half	123
.half	123
.half	123
.half	2384
.half	2384
.half	2385
.half	2385
.half	4326
.half	4327
.half	4310
.half	4311
.half	316
.half	316
.half	316
.half	316
.half	317
.half	317
.half	317
.half	317
.half	186
.half	186
.half	186
.half	186
.half	187
.half	187
.half	187
.half	187
.half	2292
.half	2292
.half	2293
.half	2293
.half	4356
.half	4357
.half	4110
.half	4111
.half	2092
.half	2092
.half	2093
.half	2093
.half	4440
.half	4441
.half	4152
.half	4153
.half	34
.half	34
.half	34
.half	34
.half	35
.half	35
.half	35
.half	35
.half	176
.half	176
.half	176
.half	176
.half	177
.half	177
.half	177
.half	177
.half	274
.half	274
.half	274
.half	274
.half	275
.half	275
.half	275
.half	275
.half	146
.half	146
.half	146
.half	146
.half	147
.half	147
.half	147
.half	147
.half	78
.half	78
.half	78
.half	78
.half	79
.half	79
.half	79
.half	79
.half	2254
.half	2254
.half	2255
.half	2255
.half	2376
.half	2376
.half	2377
.half	2377
.half	312
.half	312
.half	312
.half	312
.half	313
.half	313
.half	313
.half	313
.half	2090
.half	2090
.half	2091
.half	2091
.half	2380
.half	2380
.half	2381
.half	2381
.half	128
.half	128
.half	128
.half	128
.half	129
.half	129
.half	129
.half	129
.half	-27848
.half	-27840
.half	4280
.half	4281
.half	2228
.half	2228
.half	2229
.half	2229
.half	192
.half	192
.half	192
.half	192
.half	193
.half	193
.half	193
.half	193
.half	4406
.half	4407
.half	-27832
.half	-27824
.half	2252
.half	2252
.half	2253
.half	2253
.half	322
.half	322
.half	322
.half	322
.half	323
.half	323
.half	323
.half	323
.half	4438
.half	4439
.half	4422
.half	4423
.half	2378
.half	2378
.half	2379
.half	2379
.half	262
.half	262
.half	262
.half	262
.half	263
.half	263
.half	263
.half	263
.half	32
.half	32
.half	32
.half	32
.half	33
.half	33
.half	33
.half	33
.half	4352
.half	4353
.half	4166
.half	4167
.half	3420
.half	3420
.half	3421
.half	3421
.half	308
.half	308
.half	308
.half	308
.half	309
.half	309
.half	309
.half	309
.half	98
.half	98
.half	98
.half	98
.half	99
.half	99
.half	99
.half	99
.half	2200
.half	2200
.half	2201
.half	2201
.half	4342
.half	4343
.half	4210
.half	4211
.half	2088
.half	2088
.half	2089
.half	2089
.half	4198
.half	4199
.half	4220
.half	4221
.half	30
.half	30
.half	30
.half	30
.half	31
.half	31
.half	31
.half	31
.half	4378
.half	4379
.half	-27744
.half	-27736
.half	2290
.half	2290
.half	2291
.half	2291
.half	294
.half	294
.half	294
.half	294
.half	295
.half	295
.half	295
.half	295
.half	4304
.half	4305
.half	4394
.half	4395
.half	4308
.half	4309
.half	4398
.half	4399
.half	236
.half	236
.half	236
.half	236
.half	237
.half	237
.half	237
.half	237
.half	154
.half	154
.half	154
.half	154
.half	155
.half	155
.half	155
.half	155
.half	258
.half	258
.half	258
.half	258
.half	259
.half	259
.half	259
.half	259
.half	218
.half	218
.half	218
.half	218
.half	219
.half	219
.half	219
.half	219
.globl	cfg_all_table
.align	2
.type	cfg_all_table, @object
.size	cfg_all_table, 128
cfg_all_table:
.word	976
.word	7
.word	372
.word	0
.word	880
.word	7
.word	338
.word	0
.word	880
.word	8
.word	266
.word	0
.word	848
.word	7
.word	298
.word	0
.word	536
.word	8
.word	206
.word	0
.word	536
.word	8
.word	206
.word	0
.word	960
.word	8
.word	326
.word	0
.word	976
.word	6
.word	350
.word	0
.globl	vc1_hw_codingset2
.align	2
.type	vc1_hw_codingset2, @object
.size	vc1_hw_codingset2, 4
vc1_hw_codingset2:
.word	171
.globl	vc1_hw_codingset1
.align	2
.type	vc1_hw_codingset1, @object
.size	vc1_hw_codingset1, 4
vc1_hw_codingset1:
.word	171

.comm	vc1_hw_bs_buffer,4,4

.comm	sram_base,4,4

.comm	tcsm1_word_bank_backup,24,4

.comm	vpu_base,4,4

.comm	tlb_i,4,4

.comm	tcsm0_fifo_rp,4,4

.comm	tcsm1_fifo_wp,4,4

.comm	task_fifo_wp_d2,4,4

.comm	task_fifo_wp_d1,4,4

.comm	task_fifo_wp,4,4

.comm	dMB,4,4

.comm	frame_info_mem,4,4

.comm	task_fifo_mem,4,4

.comm	tcsm1_base,4,4

.comm	vmau_base,4,4
.rdata
.align	2
.type	vc1_last_delta_run_table, @object
.size	vc1_last_delta_run_table, 80
vc1_last_delta_run_table:
.byte	-1
.byte	37
.byte	15
.byte	4
.byte	3
.byte	1
.byte	0
.space	3
.byte	-1
.byte	36
.byte	14
.byte	6
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	26
.byte	13
.byte	3
.byte	1
.space	5
.byte	-1
.byte	43
.byte	15
.byte	3
.byte	1
.byte	0
.space	4
.byte	-1
.byte	20
.byte	6
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	-1
.byte	40
.byte	1
.byte	0
.space	6
.byte	-1
.byte	16
.byte	14
.byte	2
.byte	0
.space	5
.byte	-1
.byte	30
.byte	28
.byte	3
.byte	0
.space	5
.align	2
.type	vc1_delta_run_table, @object
.size	vc1_delta_run_table, 456
vc1_delta_run_table:
.byte	-1
.byte	30
.byte	17
.byte	15
.byte	9
.byte	5
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.space	37
.byte	-1
.byte	26
.byte	16
.byte	11
.byte	7
.byte	5
.byte	3
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	33
.byte	-1
.byte	20
.byte	15
.byte	13
.byte	6
.byte	4
.byte	3
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	40
.byte	-1
.byte	29
.byte	15
.byte	12
.byte	5
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	42
.byte	-1
.byte	14
.byte	9
.byte	7
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	29
.byte	-1
.byte	26
.byte	10
.byte	6
.byte	2
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	44
.byte	-1
.byte	14
.byte	13
.byte	9
.byte	6
.byte	5
.byte	4
.byte	3
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	22
.byte	9
.byte	6
.byte	4
.byte	3
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	24
.align	2
.type	vc1_last_delta_level_table, @object
.size	vc1_last_delta_level_table, 352
vc1_last_delta_level_table:
.byte	6
.byte	5
.byte	4
.byte	4
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	6
.byte	9
.byte	5
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	7
.byte	4
.byte	4
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	17
.byte	5
.byte	4
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	8
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	23
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	3
.byte	4
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.space	27
.byte	4
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.space	13
.align	2
.type	vc1_delta_level_table, @object
.size	vc1_delta_level_table, 248
vc1_delta_level_table:
.byte	19
.byte	15
.byte	12
.byte	11
.byte	6
.byte	5
.byte	4
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	23
.byte	11
.byte	8
.byte	7
.byte	5
.byte	5
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	4
.byte	16
.byte	11
.byte	8
.byte	7
.byte	5
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	10
.byte	14
.byte	9
.byte	5
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	1
.byte	27
.byte	10
.byte	5
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	16
.byte	12
.byte	6
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	4
.byte	56
.byte	20
.byte	10
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.space	16
.byte	32
.byte	13
.byte	8
.byte	6
.byte	5
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.space	6
.align	2
.type	vc1_index_decode_table, @object
.size	vc1_index_decode_table, 5920
vc1_index_decode_table:
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	0
.byte	15
.byte	0
.byte	-15
.byte	0
.byte	16
.byte	0
.byte	-16
.byte	0
.byte	17
.byte	0
.byte	-17
.byte	0
.byte	18
.byte	0
.byte	-18
.byte	0
.byte	19
.byte	0
.byte	-19
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	1
.byte	10
.byte	1
.byte	-10
.byte	1
.byte	11
.byte	1
.byte	-11
.byte	1
.byte	12
.byte	1
.byte	-12
.byte	1
.byte	13
.byte	1
.byte	-13
.byte	1
.byte	14
.byte	1
.byte	-14
.byte	1
.byte	15
.byte	1
.byte	-15
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	2
.byte	6
.byte	2
.byte	-6
.byte	2
.byte	7
.byte	2
.byte	-7
.byte	2
.byte	8
.byte	2
.byte	-8
.byte	2
.byte	9
.byte	2
.byte	-9
.byte	2
.byte	10
.byte	2
.byte	-10
.byte	2
.byte	11
.byte	2
.byte	-11
.byte	2
.byte	12
.byte	2
.byte	-12
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	3
.byte	5
.byte	3
.byte	-5
.byte	3
.byte	6
.byte	3
.byte	-6
.byte	3
.byte	7
.byte	3
.byte	-7
.byte	3
.byte	8
.byte	3
.byte	-8
.byte	3
.byte	9
.byte	3
.byte	-9
.byte	3
.byte	10
.byte	3
.byte	-10
.byte	3
.byte	11
.byte	3
.byte	-11
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	4
.byte	4
.byte	4
.byte	-4
.byte	4
.byte	5
.byte	4
.byte	-5
.byte	4
.byte	6
.byte	4
.byte	-6
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	5
.byte	4
.byte	5
.byte	-4
.byte	5
.byte	5
.byte	5
.byte	-5
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	6
.byte	4
.byte	6
.byte	-4
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	7
.byte	4
.byte	7
.byte	-4
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	8
.byte	3
.byte	8
.byte	-3
.byte	8
.byte	4
.byte	8
.byte	-4
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	9
.byte	3
.byte	9
.byte	-3
.byte	9
.byte	4
.byte	9
.byte	-4
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	10
.byte	3
.byte	10
.byte	-3
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	11
.byte	3
.byte	11
.byte	-3
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	12
.byte	3
.byte	12
.byte	-3
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	13
.byte	3
.byte	13
.byte	-3
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	14
.byte	3
.byte	14
.byte	-3
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	15
.byte	3
.byte	15
.byte	-3
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	16
.byte	2
.byte	16
.byte	-2
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	17
.byte	2
.byte	17
.byte	-2
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	30
.byte	1
.byte	30
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	30
.byte	1
.byte	30
.byte	-1
.byte	31
.byte	1
.byte	31
.byte	-1
.byte	32
.byte	1
.byte	32
.byte	-1
.byte	33
.byte	1
.byte	33
.byte	-1
.byte	34
.byte	1
.byte	34
.byte	-1
.byte	35
.byte	1
.byte	35
.byte	-1
.byte	36
.byte	1
.byte	36
.byte	-1
.byte	37
.byte	1
.byte	37
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	0
.byte	15
.byte	0
.byte	-15
.byte	0
.byte	16
.byte	0
.byte	-16
.byte	0
.byte	17
.byte	0
.byte	-17
.byte	0
.byte	18
.byte	0
.byte	-18
.byte	0
.byte	19
.byte	0
.byte	-19
.byte	0
.byte	20
.byte	0
.byte	-20
.byte	0
.byte	21
.byte	0
.byte	-21
.byte	0
.byte	22
.byte	0
.byte	-22
.byte	0
.byte	23
.byte	0
.byte	-23
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	1
.byte	10
.byte	1
.byte	-10
.byte	1
.byte	11
.byte	1
.byte	-11
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	2
.byte	6
.byte	2
.byte	-6
.byte	2
.byte	7
.byte	2
.byte	-7
.byte	2
.byte	8
.byte	2
.byte	-8
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	3
.byte	5
.byte	3
.byte	-5
.byte	3
.byte	6
.byte	3
.byte	-6
.byte	3
.byte	7
.byte	3
.byte	-7
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	4
.byte	4
.byte	4
.byte	-4
.byte	4
.byte	5
.byte	4
.byte	-5
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	5
.byte	4
.byte	5
.byte	-4
.byte	5
.byte	5
.byte	5
.byte	-5
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	6
.byte	4
.byte	6
.byte	-4
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	7
.byte	4
.byte	7
.byte	-4
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	8
.byte	3
.byte	8
.byte	-3
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	9
.byte	3
.byte	9
.byte	-3
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	10
.byte	3
.byte	10
.byte	-3
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	11
.byte	3
.byte	11
.byte	-3
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	16
.byte	2
.byte	16
.byte	-2
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	30
.byte	1
.byte	30
.byte	-1
.byte	31
.byte	1
.byte	31
.byte	-1
.byte	32
.byte	1
.byte	32
.byte	-1
.byte	33
.byte	1
.byte	33
.byte	-1
.byte	34
.byte	1
.byte	34
.byte	-1
.byte	35
.byte	1
.byte	35
.byte	-1
.byte	36
.byte	1
.byte	36
.byte	-1
.space	68
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	0
.byte	15
.byte	0
.byte	-15
.byte	0
.byte	16
.byte	0
.byte	-16
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	1
.byte	10
.byte	1
.byte	-10
.byte	1
.byte	11
.byte	1
.byte	-11
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	2
.byte	6
.byte	2
.byte	-6
.byte	2
.byte	7
.byte	2
.byte	-7
.byte	2
.byte	8
.byte	2
.byte	-8
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	3
.byte	5
.byte	3
.byte	-5
.byte	3
.byte	6
.byte	3
.byte	-6
.byte	3
.byte	7
.byte	3
.byte	-7
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	4
.byte	4
.byte	4
.byte	-4
.byte	4
.byte	5
.byte	4
.byte	-5
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	5
.byte	4
.byte	5
.byte	-4
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	6
.byte	4
.byte	6
.byte	-4
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	8
.byte	3
.byte	8
.byte	-3
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	9
.byte	3
.byte	9
.byte	-3
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	10
.byte	3
.byte	10
.byte	-3
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	11
.byte	3
.byte	11
.byte	-3
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	12
.byte	3
.byte	12
.byte	-3
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	13
.byte	3
.byte	13
.byte	-3
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.space	212
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	4
.byte	4
.byte	4
.byte	-4
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	5
.byte	4
.byte	5
.byte	-4
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	8
.byte	3
.byte	8
.byte	-3
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	9
.byte	3
.byte	9
.byte	-3
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	10
.byte	3
.byte	10
.byte	-3
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	11
.byte	3
.byte	11
.byte	-3
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	12
.byte	3
.byte	12
.byte	-3
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	30
.byte	1
.byte	30
.byte	-1
.byte	31
.byte	1
.byte	31
.byte	-1
.byte	32
.byte	1
.byte	32
.byte	-1
.byte	33
.byte	1
.byte	33
.byte	-1
.byte	34
.byte	1
.byte	34
.byte	-1
.byte	35
.byte	1
.byte	35
.byte	-1
.byte	36
.byte	1
.byte	36
.byte	-1
.byte	37
.byte	1
.byte	37
.byte	-1
.byte	38
.byte	1
.byte	38
.byte	-1
.byte	39
.byte	1
.byte	39
.byte	-1
.byte	40
.byte	1
.byte	40
.byte	-1
.byte	41
.byte	1
.byte	41
.byte	-1
.byte	42
.byte	1
.byte	42
.byte	-1
.byte	43
.byte	1
.byte	43
.byte	-1
.space	148
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	0
.byte	15
.byte	0
.byte	-15
.byte	0
.byte	16
.byte	0
.byte	-16
.byte	0
.byte	17
.byte	0
.byte	-17
.byte	0
.byte	18
.byte	0
.byte	-18
.byte	0
.byte	19
.byte	0
.byte	-19
.byte	0
.byte	20
.byte	0
.byte	-20
.byte	0
.byte	21
.byte	0
.byte	-21
.byte	0
.byte	22
.byte	0
.byte	-22
.byte	0
.byte	23
.byte	0
.byte	-23
.byte	0
.byte	24
.byte	0
.byte	-24
.byte	0
.byte	25
.byte	0
.byte	-25
.byte	0
.byte	26
.byte	0
.byte	-26
.byte	0
.byte	27
.byte	0
.byte	-27
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	1
.byte	10
.byte	1
.byte	-10
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.space	332
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	30
.byte	1
.byte	30
.byte	-1
.byte	31
.byte	1
.byte	31
.byte	-1
.byte	32
.byte	1
.byte	32
.byte	-1
.byte	33
.byte	1
.byte	33
.byte	-1
.byte	34
.byte	1
.byte	34
.byte	-1
.byte	35
.byte	1
.byte	35
.byte	-1
.byte	36
.byte	1
.byte	36
.byte	-1
.byte	37
.byte	1
.byte	37
.byte	-1
.byte	38
.byte	1
.byte	38
.byte	-1
.byte	39
.byte	1
.byte	39
.byte	-1
.byte	40
.byte	1
.byte	40
.byte	-1
.space	332
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	0
.byte	15
.byte	0
.byte	-15
.byte	0
.byte	16
.byte	0
.byte	-16
.byte	0
.byte	17
.byte	0
.byte	-17
.byte	0
.byte	18
.byte	0
.byte	-18
.byte	0
.byte	19
.byte	0
.byte	-19
.byte	0
.byte	20
.byte	0
.byte	-20
.byte	0
.byte	21
.byte	0
.byte	-21
.byte	0
.byte	22
.byte	0
.byte	-22
.byte	0
.byte	23
.byte	0
.byte	-23
.byte	0
.byte	24
.byte	0
.byte	-24
.byte	0
.byte	25
.byte	0
.byte	-25
.byte	0
.byte	26
.byte	0
.byte	-26
.byte	0
.byte	27
.byte	0
.byte	-27
.byte	0
.byte	28
.byte	0
.byte	-28
.byte	0
.byte	29
.byte	0
.byte	-29
.byte	0
.byte	30
.byte	0
.byte	-30
.byte	0
.byte	31
.byte	0
.byte	-31
.byte	0
.byte	32
.byte	0
.byte	-32
.byte	0
.byte	33
.byte	0
.byte	-33
.byte	0
.byte	34
.byte	0
.byte	-34
.byte	0
.byte	35
.byte	0
.byte	-35
.byte	0
.byte	36
.byte	0
.byte	-36
.byte	0
.byte	37
.byte	0
.byte	-37
.byte	0
.byte	38
.byte	0
.byte	-38
.byte	0
.byte	39
.byte	0
.byte	-39
.byte	0
.byte	40
.byte	0
.byte	-40
.byte	0
.byte	41
.byte	0
.byte	-41
.byte	0
.byte	42
.byte	0
.byte	-42
.byte	0
.byte	43
.byte	0
.byte	-43
.byte	0
.byte	44
.byte	0
.byte	-44
.byte	0
.byte	45
.byte	0
.byte	-45
.byte	0
.byte	46
.byte	0
.byte	-46
.byte	0
.byte	47
.byte	0
.byte	-47
.byte	0
.byte	48
.byte	0
.byte	-48
.byte	0
.byte	49
.byte	0
.byte	-49
.byte	0
.byte	50
.byte	0
.byte	-50
.byte	0
.byte	51
.byte	0
.byte	-51
.byte	0
.byte	52
.byte	0
.byte	-52
.byte	0
.byte	53
.byte	0
.byte	-53
.byte	0
.byte	54
.byte	0
.byte	-54
.byte	0
.byte	55
.byte	0
.byte	-55
.byte	0
.byte	56
.byte	0
.byte	-56
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	1
.byte	10
.byte	1
.byte	-10
.byte	1
.byte	11
.byte	1
.byte	-11
.byte	1
.byte	12
.byte	1
.byte	-12
.byte	1
.byte	13
.byte	1
.byte	-13
.byte	1
.byte	14
.byte	1
.byte	-14
.byte	1
.byte	15
.byte	1
.byte	-15
.byte	1
.byte	16
.byte	1
.byte	-16
.byte	1
.byte	17
.byte	1
.byte	-17
.byte	1
.byte	18
.byte	1
.byte	-18
.byte	1
.byte	19
.byte	1
.byte	-19
.byte	1
.byte	20
.byte	1
.byte	-20
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	2
.byte	6
.byte	2
.byte	-6
.byte	2
.byte	7
.byte	2
.byte	-7
.byte	2
.byte	8
.byte	2
.byte	-8
.byte	2
.byte	9
.byte	2
.byte	-9
.byte	2
.byte	10
.byte	2
.byte	-10
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	3
.byte	5
.byte	3
.byte	-5
.byte	3
.byte	6
.byte	3
.byte	-6
.byte	3
.byte	7
.byte	3
.byte	-7
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	4
.byte	4
.byte	4
.byte	-4
.byte	4
.byte	5
.byte	4
.byte	-5
.byte	4
.byte	6
.byte	4
.byte	-6
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	5
.byte	4
.byte	5
.byte	-4
.byte	5
.byte	5
.byte	5
.byte	-5
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	6
.byte	4
.byte	6
.byte	-4
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	8
.byte	3
.byte	8
.byte	-3
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	9
.byte	3
.byte	9
.byte	-3
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	16
.byte	1
.byte	16
.byte	-1
.space	92
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	0
.byte	5
.byte	0
.byte	-5
.byte	0
.byte	6
.byte	0
.byte	-6
.byte	0
.byte	7
.byte	0
.byte	-7
.byte	0
.byte	8
.byte	0
.byte	-8
.byte	0
.byte	9
.byte	0
.byte	-9
.byte	0
.byte	10
.byte	0
.byte	-10
.byte	0
.byte	11
.byte	0
.byte	-11
.byte	0
.byte	12
.byte	0
.byte	-12
.byte	0
.byte	13
.byte	0
.byte	-13
.byte	0
.byte	14
.byte	0
.byte	-14
.byte	0
.byte	15
.byte	0
.byte	-15
.byte	0
.byte	16
.byte	0
.byte	-16
.byte	0
.byte	17
.byte	0
.byte	-17
.byte	0
.byte	18
.byte	0
.byte	-18
.byte	0
.byte	19
.byte	0
.byte	-19
.byte	0
.byte	20
.byte	0
.byte	-20
.byte	0
.byte	21
.byte	0
.byte	-21
.byte	0
.byte	22
.byte	0
.byte	-22
.byte	0
.byte	23
.byte	0
.byte	-23
.byte	0
.byte	24
.byte	0
.byte	-24
.byte	0
.byte	25
.byte	0
.byte	-25
.byte	0
.byte	26
.byte	0
.byte	-26
.byte	0
.byte	27
.byte	0
.byte	-27
.byte	0
.byte	28
.byte	0
.byte	-28
.byte	0
.byte	29
.byte	0
.byte	-29
.byte	0
.byte	30
.byte	0
.byte	-30
.byte	0
.byte	31
.byte	0
.byte	-31
.byte	0
.byte	32
.byte	0
.byte	-32
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	1
.byte	4
.byte	1
.byte	-4
.byte	1
.byte	5
.byte	1
.byte	-5
.byte	1
.byte	6
.byte	1
.byte	-6
.byte	1
.byte	7
.byte	1
.byte	-7
.byte	1
.byte	8
.byte	1
.byte	-8
.byte	1
.byte	9
.byte	1
.byte	-9
.byte	1
.byte	10
.byte	1
.byte	-10
.byte	1
.byte	11
.byte	1
.byte	-11
.byte	1
.byte	12
.byte	1
.byte	-12
.byte	1
.byte	13
.byte	1
.byte	-13
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	2
.byte	4
.byte	2
.byte	-4
.byte	2
.byte	5
.byte	2
.byte	-5
.byte	2
.byte	6
.byte	2
.byte	-6
.byte	2
.byte	7
.byte	2
.byte	-7
.byte	2
.byte	8
.byte	2
.byte	-8
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	3
.byte	4
.byte	3
.byte	-4
.byte	3
.byte	5
.byte	3
.byte	-5
.byte	3
.byte	6
.byte	3
.byte	-6
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	4
.byte	3
.byte	4
.byte	-3
.byte	4
.byte	4
.byte	4
.byte	-4
.byte	4
.byte	5
.byte	4
.byte	-5
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	5
.byte	3
.byte	5
.byte	-3
.byte	5
.byte	4
.byte	5
.byte	-4
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	6
.byte	3
.byte	6
.byte	-3
.byte	6
.byte	4
.byte	6
.byte	-4
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	7
.byte	3
.byte	7
.byte	-3
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	8
.byte	3
.byte	8
.byte	-3
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	9
.byte	3
.byte	9
.byte	-3
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	16
.byte	2
.byte	16
.byte	-2
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	17
.byte	2
.byte	17
.byte	-2
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	18
.byte	2
.byte	18
.byte	-2
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	19
.byte	2
.byte	19
.byte	-2
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	20
.byte	2
.byte	20
.byte	-2
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	21
.byte	2
.byte	21
.byte	-2
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	22
.byte	2
.byte	22
.byte	-2
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	0
.byte	2
.byte	0
.byte	-2
.byte	0
.byte	3
.byte	0
.byte	-3
.byte	0
.byte	4
.byte	0
.byte	-4
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	2
.byte	1
.byte	-2
.byte	1
.byte	3
.byte	1
.byte	-3
.byte	2
.byte	1
.byte	2
.byte	-1
.byte	2
.byte	2
.byte	2
.byte	-2
.byte	2
.byte	3
.byte	2
.byte	-3
.byte	3
.byte	1
.byte	3
.byte	-1
.byte	3
.byte	2
.byte	3
.byte	-2
.byte	3
.byte	3
.byte	3
.byte	-3
.byte	4
.byte	1
.byte	4
.byte	-1
.byte	4
.byte	2
.byte	4
.byte	-2
.byte	5
.byte	1
.byte	5
.byte	-1
.byte	5
.byte	2
.byte	5
.byte	-2
.byte	6
.byte	1
.byte	6
.byte	-1
.byte	6
.byte	2
.byte	6
.byte	-2
.byte	7
.byte	1
.byte	7
.byte	-1
.byte	7
.byte	2
.byte	7
.byte	-2
.byte	8
.byte	1
.byte	8
.byte	-1
.byte	8
.byte	2
.byte	8
.byte	-2
.byte	9
.byte	1
.byte	9
.byte	-1
.byte	9
.byte	2
.byte	9
.byte	-2
.byte	10
.byte	1
.byte	10
.byte	-1
.byte	10
.byte	2
.byte	10
.byte	-2
.byte	11
.byte	1
.byte	11
.byte	-1
.byte	11
.byte	2
.byte	11
.byte	-2
.byte	12
.byte	1
.byte	12
.byte	-1
.byte	12
.byte	2
.byte	12
.byte	-2
.byte	13
.byte	1
.byte	13
.byte	-1
.byte	13
.byte	2
.byte	13
.byte	-2
.byte	14
.byte	1
.byte	14
.byte	-1
.byte	14
.byte	2
.byte	14
.byte	-2
.byte	15
.byte	1
.byte	15
.byte	-1
.byte	15
.byte	2
.byte	15
.byte	-2
.byte	16
.byte	1
.byte	16
.byte	-1
.byte	16
.byte	2
.byte	16
.byte	-2
.byte	17
.byte	1
.byte	17
.byte	-1
.byte	17
.byte	2
.byte	17
.byte	-2
.byte	18
.byte	1
.byte	18
.byte	-1
.byte	18
.byte	2
.byte	18
.byte	-2
.byte	19
.byte	1
.byte	19
.byte	-1
.byte	19
.byte	2
.byte	19
.byte	-2
.byte	20
.byte	1
.byte	20
.byte	-1
.byte	20
.byte	2
.byte	20
.byte	-2
.byte	21
.byte	1
.byte	21
.byte	-1
.byte	21
.byte	2
.byte	21
.byte	-2
.byte	22
.byte	1
.byte	22
.byte	-1
.byte	22
.byte	2
.byte	22
.byte	-2
.byte	23
.byte	1
.byte	23
.byte	-1
.byte	23
.byte	2
.byte	23
.byte	-2
.byte	24
.byte	1
.byte	24
.byte	-1
.byte	24
.byte	2
.byte	24
.byte	-2
.byte	25
.byte	1
.byte	25
.byte	-1
.byte	25
.byte	2
.byte	25
.byte	-2
.byte	26
.byte	1
.byte	26
.byte	-1
.byte	26
.byte	2
.byte	26
.byte	-2
.byte	27
.byte	1
.byte	27
.byte	-1
.byte	27
.byte	2
.byte	27
.byte	-2
.byte	28
.byte	1
.byte	28
.byte	-1
.byte	28
.byte	2
.byte	28
.byte	-2
.byte	29
.byte	1
.byte	29
.byte	-1
.byte	30
.byte	1
.byte	30
.byte	-1
.space	44
.align	2
.type	vc1_last_decode_table, @object
.size	vc1_last_decode_table, 32
vc1_last_decode_table:
.word	119
.word	99
.word	85
.word	81
.word	67
.word	58
.word	126
.word	109
.align	2
.type	vc1_ac_tables, @object
.size	vc1_ac_tables, 11904
vc1_ac_tables:
.word	1
.word	2
.word	5
.word	3
.word	13
.word	4
.word	18
.word	5
.word	14
.word	6
.word	21
.word	7
.word	19
.word	8
.word	63
.word	8
.word	75
.word	9
.word	287
.word	9
.word	184
.word	10
.word	995
.word	10
.word	370
.word	11
.word	589
.word	12
.word	986
.word	12
.word	733
.word	13
.word	8021
.word	13
.word	1465
.word	14
.word	16046
.word	14
.word	0
.word	4
.word	16
.word	5
.word	8
.word	7
.word	32
.word	8
.word	41
.word	9
.word	500
.word	9
.word	563
.word	10
.word	480
.word	11
.word	298
.word	12
.word	989
.word	12
.word	1290
.word	13
.word	7977
.word	13
.word	2626
.word	14
.word	4722
.word	15
.word	5943
.word	15
.word	3
.word	5
.word	17
.word	7
.word	196
.word	8
.word	75
.word	10
.word	180
.word	11
.word	2004
.word	11
.word	837
.word	12
.word	727
.word	13
.word	1983
.word	13
.word	2360
.word	14
.word	3003
.word	14
.word	2398
.word	15
.word	19
.word	5
.word	120
.word	7
.word	105
.word	9
.word	562
.word	10
.word	1121
.word	11
.word	1004
.word	12
.word	1312
.word	13
.word	7978
.word	13
.word	15952
.word	14
.word	15953
.word	14
.word	5254
.word	15
.word	12
.word	6
.word	36
.word	9
.word	148
.word	11
.word	2240
.word	12
.word	3849
.word	14
.word	7920
.word	15
.word	61
.word	6
.word	83
.word	9
.word	416
.word	11
.word	726
.word	13
.word	3848
.word	14
.word	19
.word	7
.word	124
.word	9
.word	1985
.word	11
.word	1196
.word	14
.word	27
.word	7
.word	160
.word	10
.word	836
.word	12
.word	3961
.word	14
.word	121
.word	7
.word	993
.word	10
.word	724
.word	13
.word	8966
.word	14
.word	33
.word	8
.word	572
.word	10
.word	4014
.word	12
.word	9182
.word	14
.word	53
.word	8
.word	373
.word	11
.word	1971
.word	13
.word	197
.word	8
.word	372
.word	11
.word	1925
.word	13
.word	72
.word	9
.word	419
.word	11
.word	1182
.word	13
.word	44
.word	9
.word	250
.word	10
.word	2006
.word	11
.word	146
.word	10
.word	1484
.word	13
.word	7921
.word	15
.word	163
.word	10
.word	1005
.word	12
.word	2366
.word	14
.word	482
.word	11
.word	4723
.word	15
.word	1988
.word	11
.word	5255
.word	15
.word	657
.word	12
.word	659
.word	12
.word	3978
.word	12
.word	1289
.word	13
.word	1288
.word	13
.word	1933
.word	13
.word	1982
.word	13
.word	1932
.word	13
.word	1198
.word	14
.word	3002
.word	14
.word	8967
.word	14
.word	2970
.word	14
.word	5942
.word	15
.word	14
.word	4
.word	69
.word	7
.word	499
.word	9
.word	1146
.word	11
.word	1500
.word	13
.word	9183
.word	14
.word	25
.word	5
.word	40
.word	9
.word	374
.word	11
.word	1181
.word	13
.word	9181
.word	14
.word	48
.word	6
.word	162
.word	10
.word	751
.word	12
.word	1464
.word	14
.word	63
.word	6
.word	165
.word	10
.word	987
.word	12
.word	2367
.word	14
.word	68
.word	7
.word	1995
.word	11
.word	2399
.word	15
.word	99
.word	7
.word	963
.word	12
.word	21
.word	8
.word	2294
.word	12
.word	23
.word	8
.word	1176
.word	13
.word	44
.word	8
.word	1970
.word	13
.word	47
.word	8
.word	8020
.word	13
.word	141
.word	8
.word	1981
.word	13
.word	142
.word	8
.word	4482
.word	13
.word	251
.word	8
.word	1291
.word	13
.word	45
.word	8
.word	1984
.word	11
.word	121
.word	9
.word	8031
.word	13
.word	122
.word	9
.word	8022
.word	13
.word	561
.word	10
.word	996
.word	10
.word	417
.word	11
.word	323
.word	11
.word	503
.word	11
.word	367
.word	12
.word	658
.word	12
.word	743
.word	12
.word	364
.word	12
.word	365
.word	12
.word	988
.word	12
.word	3979
.word	12
.word	1177
.word	13
.word	984
.word	12
.word	1934
.word	13
.word	725
.word	13
.word	8030
.word	13
.word	7979
.word	13
.word	1935
.word	13
.word	1197
.word	14
.word	16047
.word	14
.word	9180
.word	14
.word	74
.word	9
.word	0
.word	3
.word	3
.word	4
.word	11
.word	5
.word	20
.word	6
.word	63
.word	6
.word	93
.word	7
.word	162
.word	8
.word	172
.word	9
.word	366
.word	9
.word	522
.word	10
.word	738
.word	10
.word	1074
.word	11
.word	1481
.word	11
.word	2087
.word	12
.word	2900
.word	12
.word	1254
.word	13
.word	4191
.word	13
.word	5930
.word	13
.word	8370
.word	14
.word	11598
.word	14
.word	14832
.word	14
.word	16757
.word	15
.word	23198
.word	15
.word	4
.word	4
.word	30
.word	5
.word	66
.word	7
.word	182
.word	8
.word	371
.word	9
.word	917
.word	10
.word	1838
.word	11
.word	2964
.word	12
.word	5796
.word	13
.word	8371
.word	14
.word	11845
.word	14
.word	5
.word	5
.word	64
.word	7
.word	73
.word	9
.word	655
.word	10
.word	1483
.word	11
.word	1162
.word	13
.word	2525
.word	14
.word	29666
.word	15
.word	24
.word	5
.word	37
.word	8
.word	138
.word	10
.word	1307
.word	11
.word	3679
.word	12
.word	2505
.word	14
.word	5020
.word	15
.word	41
.word	6
.word	79
.word	9
.word	1042
.word	11
.word	1165
.word	13
.word	11841
.word	14
.word	56
.word	6
.word	270
.word	9
.word	1448
.word	11
.word	4188
.word	13
.word	14834
.word	14
.word	88
.word	7
.word	543
.word	10
.word	3710
.word	12
.word	14847
.word	14
.word	35
.word	8
.word	739
.word	10
.word	1253
.word	13
.word	11840
.word	14
.word	161
.word	8
.word	1470
.word	11
.word	2504
.word	14
.word	131
.word	8
.word	314
.word	11
.word	5921
.word	13
.word	68
.word	9
.word	630
.word	12
.word	14838
.word	14
.word	139
.word	10
.word	1263
.word	13
.word	23195
.word	15
.word	520
.word	10
.word	7422
.word	13
.word	921
.word	10
.word	7348
.word	13
.word	926
.word	10
.word	14835
.word	14
.word	1451
.word	11
.word	29667
.word	15
.word	1847
.word	11
.word	23199
.word	15
.word	2093
.word	12
.word	3689
.word	12
.word	3688
.word	12
.word	1075
.word	11
.word	2939
.word	12
.word	11768
.word	14
.word	11862
.word	14
.word	11863
.word	14
.word	14839
.word	14
.word	20901
.word	15
.word	3
.word	3
.word	42
.word	6
.word	228
.word	8
.word	654
.word	10
.word	1845
.word	11
.word	4184
.word	13
.word	7418
.word	13
.word	11769
.word	14
.word	16756
.word	15
.word	9
.word	4
.word	84
.word	8
.word	920
.word	10
.word	1163
.word	13
.word	5021
.word	15
.word	13
.word	4
.word	173
.word	9
.word	2086
.word	12
.word	11596
.word	14
.word	17
.word	5
.word	363
.word	9
.word	2943
.word	12
.word	20900
.word	15
.word	25
.word	5
.word	539
.word	10
.word	5885
.word	13
.word	29
.word	5
.word	916
.word	10
.word	10451
.word	14
.word	43
.word	6
.word	1468
.word	11
.word	23194
.word	15
.word	47
.word	6
.word	583
.word	12
.word	16
.word	7
.word	2613
.word	12
.word	62
.word	6
.word	2938
.word	12
.word	89
.word	7
.word	4190
.word	13
.word	38
.word	8
.word	2511
.word	14
.word	85
.word	8
.word	7349
.word	13
.word	87
.word	8
.word	3675
.word	12
.word	160
.word	8
.word	5224
.word	13
.word	368
.word	9
.word	144
.word	10
.word	462
.word	9
.word	538
.word	10
.word	536
.word	10
.word	360
.word	9
.word	542
.word	10
.word	580
.word	12
.word	1846
.word	11
.word	312
.word	11
.word	1305
.word	11
.word	3678
.word	12
.word	1836
.word	11
.word	2901
.word	12
.word	2524
.word	14
.word	8379
.word	14
.word	1164
.word	13
.word	5923
.word	13
.word	11844
.word	14
.word	5797
.word	13
.word	1304
.word	11
.word	14846
.word	14
.word	361
.word	9
.space	136
.word	1
.word	2
.word	6
.word	3
.word	15
.word	4
.word	22
.word	5
.word	32
.word	6
.word	24
.word	7
.word	8
.word	8
.word	154
.word	8
.word	86
.word	9
.word	318
.word	9
.word	240
.word	10
.word	933
.word	10
.word	119
.word	11
.word	495
.word	11
.word	154
.word	12
.word	93
.word	13
.word	1
.word	4
.word	17
.word	5
.word	2
.word	7
.word	11
.word	8
.word	18
.word	9
.word	470
.word	9
.word	638
.word	10
.word	401
.word	11
.word	234
.word	12
.word	988
.word	12
.word	315
.word	13
.word	4
.word	5
.word	20
.word	7
.word	158
.word	8
.word	9
.word	10
.word	428
.word	11
.word	482
.word	11
.word	970
.word	12
.word	95
.word	13
.word	23
.word	5
.word	78
.word	7
.word	94
.word	9
.word	243
.word	10
.word	429
.word	11
.word	236
.word	12
.word	1520
.word	13
.word	14
.word	6
.word	225
.word	8
.word	932
.word	10
.word	156
.word	12
.word	317
.word	13
.word	59
.word	6
.word	28
.word	9
.word	20
.word	11
.word	2494
.word	12
.word	6
.word	7
.word	122
.word	9
.word	400
.word	11
.word	311
.word	13
.word	27
.word	7
.word	8
.word	10
.word	1884
.word	11
.word	113
.word	7
.word	215
.word	10
.word	2495
.word	12
.word	7
.word	8
.word	175
.word	10
.word	1228
.word	11
.word	52
.word	8
.word	613
.word	10
.word	159
.word	12
.word	224
.word	8
.word	22
.word	11
.word	807
.word	12
.word	21
.word	9
.word	381
.word	11
.word	3771
.word	12
.word	20
.word	9
.word	246
.word	10
.word	484
.word	11
.word	203
.word	10
.word	2461
.word	12
.word	202
.word	10
.word	764
.word	12
.word	383
.word	11
.word	1229
.word	11
.word	765
.word	12
.word	1278
.word	11
.word	314
.word	13
.word	10
.word	4
.word	66
.word	7
.word	467
.word	9
.word	1245
.word	11
.word	18
.word	5
.word	232
.word	8
.word	76
.word	11
.word	310
.word	13
.word	57
.word	6
.word	612
.word	10
.word	3770
.word	12
.word	0
.word	7
.word	174
.word	10
.word	2460
.word	12
.word	31
.word	7
.word	1246
.word	11
.word	67
.word	7
.word	1244
.word	11
.word	3
.word	8
.word	971
.word	12
.word	6
.word	8
.word	2462
.word	12
.word	42
.word	8
.word	1521
.word	13
.word	15
.word	8
.word	2558
.word	12
.word	51
.word	8
.word	2559
.word	12
.word	152
.word	8
.word	2463
.word	12
.word	234
.word	8
.word	316
.word	13
.word	46
.word	8
.word	402
.word	11
.word	310
.word	9
.word	106
.word	9
.word	21
.word	11
.word	943
.word	10
.word	483
.word	11
.word	116
.word	11
.word	235
.word	12
.word	761
.word	12
.word	92
.word	13
.word	237
.word	12
.word	989
.word	12
.word	806
.word	12
.word	94
.word	13
.word	22
.word	7
.space	424
.word	4
.word	3
.word	20
.word	5
.word	23
.word	7
.word	127
.word	8
.word	340
.word	9
.word	498
.word	10
.word	191
.word	11
.word	101
.word	12
.word	2730
.word	12
.word	1584
.word	13
.word	5527
.word	13
.word	951
.word	14
.word	11042
.word	14
.word	3046
.word	15
.word	11
.word	4
.word	55
.word	7
.word	98
.word	9
.word	7
.word	11
.word	358
.word	12
.word	206
.word	13
.word	5520
.word	13
.word	1526
.word	14
.word	3047
.word	15
.word	7
.word	5
.word	109
.word	8
.word	3
.word	11
.word	799
.word	12
.word	1522
.word	14
.word	2
.word	6
.word	97
.word	9
.word	85
.word	12
.word	479
.word	14
.word	26
.word	6
.word	30
.word	10
.word	2761
.word	12
.word	11043
.word	14
.word	30
.word	6
.word	31
.word	10
.word	2755
.word	12
.word	11051
.word	14
.word	6
.word	7
.word	4
.word	11
.word	760
.word	13
.word	25
.word	7
.word	6
.word	11
.word	1597
.word	13
.word	87
.word	7
.word	386
.word	11
.word	10914
.word	14
.word	4
.word	8
.word	384
.word	11
.word	1436
.word	14
.word	125
.word	8
.word	356
.word	12
.word	1901
.word	15
.word	2
.word	9
.word	397
.word	11
.word	5505
.word	13
.word	173
.word	8
.word	96
.word	12
.word	3175
.word	14
.word	28
.word	9
.word	238
.word	13
.word	3
.word	9
.word	719
.word	13
.word	217
.word	9
.word	5504
.word	13
.word	2
.word	11
.word	387
.word	11
.word	87
.word	12
.word	97
.word	12
.word	49
.word	11
.word	102
.word	12
.word	1585
.word	13
.word	1586
.word	13
.word	172
.word	13
.word	797
.word	12
.word	118
.word	12
.word	58
.word	11
.word	357
.word	12
.word	3174
.word	14
.word	3
.word	2
.word	84
.word	7
.word	683
.word	10
.word	22
.word	13
.word	1527
.word	14
.word	5
.word	4
.word	248
.word	9
.word	2729
.word	12
.word	95
.word	15
.word	4
.word	4
.word	28
.word	10
.word	5456
.word	13
.word	4
.word	5
.word	119
.word	11
.word	1900
.word	15
.word	14
.word	5
.word	10
.word	12
.word	12
.word	5
.word	1378
.word	11
.word	4
.word	6
.word	796
.word	12
.word	6
.word	6
.word	200
.word	13
.word	13
.word	6
.word	474
.word	13
.word	7
.word	6
.word	201
.word	13
.word	1
.word	7
.word	46
.word	14
.word	20
.word	7
.word	5526
.word	13
.word	10
.word	7
.word	2754
.word	12
.word	22
.word	7
.word	347
.word	14
.word	21
.word	7
.word	346
.word	14
.word	15
.word	8
.word	94
.word	15
.word	126
.word	8
.word	171
.word	8
.word	45
.word	9
.word	216
.word	9
.word	11
.word	9
.word	20
.word	10
.word	691
.word	10
.word	499
.word	10
.word	58
.word	10
.word	0
.word	10
.word	88
.word	10
.word	46
.word	9
.word	94
.word	10
.word	1379
.word	11
.word	236
.word	12
.word	84
.word	12
.word	2753
.word	12
.word	5462
.word	13
.word	762
.word	13
.word	385
.word	11
.word	5463
.word	13
.word	1437
.word	14
.word	10915
.word	14
.word	11050
.word	14
.word	478
.word	14
.word	1596
.word	13
.word	207
.word	13
.word	5524
.word	13
.word	13
.word	9
.space	296
.word	2
.word	2
.word	6
.word	3
.word	15
.word	4
.word	13
.word	5
.word	12
.word	5
.word	21
.word	6
.word	19
.word	6
.word	18
.word	6
.word	23
.word	7
.word	31
.word	8
.word	30
.word	8
.word	29
.word	8
.word	37
.word	9
.word	36
.word	9
.word	35
.word	9
.word	33
.word	9
.word	33
.word	10
.word	32
.word	10
.word	15
.word	10
.word	14
.word	10
.word	7
.word	11
.word	6
.word	11
.word	32
.word	11
.word	33
.word	11
.word	80
.word	12
.word	81
.word	12
.word	82
.word	12
.word	14
.word	4
.word	20
.word	6
.word	22
.word	7
.word	28
.word	8
.word	32
.word	9
.word	31
.word	9
.word	13
.word	10
.word	34
.word	11
.word	83
.word	12
.word	85
.word	12
.word	11
.word	5
.word	21
.word	7
.word	30
.word	9
.word	12
.word	10
.word	86
.word	12
.word	17
.word	6
.word	27
.word	8
.word	29
.word	9
.word	11
.word	10
.word	16
.word	6
.word	34
.word	9
.word	10
.word	10
.word	13
.word	6
.word	28
.word	9
.word	8
.word	10
.word	18
.word	7
.word	27
.word	9
.word	84
.word	12
.word	20
.word	7
.word	26
.word	9
.word	87
.word	12
.word	25
.word	8
.word	9
.word	10
.word	24
.word	8
.word	35
.word	11
.word	23
.word	8
.word	25
.word	9
.word	24
.word	9
.word	7
.word	10
.word	88
.word	12
.word	7
.word	4
.word	12
.word	6
.word	22
.word	8
.word	23
.word	9
.word	6
.word	10
.word	5
.word	11
.word	4
.word	11
.word	89
.word	12
.word	15
.word	6
.word	22
.word	9
.word	5
.word	10
.word	14
.word	6
.word	4
.word	10
.word	17
.word	7
.word	36
.word	11
.word	16
.word	7
.word	37
.word	11
.word	19
.word	7
.word	90
.word	12
.word	21
.word	8
.word	91
.word	12
.word	20
.word	8
.word	19
.word	8
.word	26
.word	8
.word	21
.word	9
.word	20
.word	9
.word	19
.word	9
.word	18
.word	9
.word	17
.word	9
.word	38
.word	11
.word	39
.word	11
.word	92
.word	12
.word	93
.word	12
.word	94
.word	12
.word	95
.word	12
.word	3
.word	7
.space	664
.word	2
.word	2
.word	15
.word	4
.word	21
.word	6
.word	23
.word	7
.word	31
.word	8
.word	37
.word	9
.word	36
.word	9
.word	33
.word	10
.word	32
.word	10
.word	7
.word	11
.word	6
.word	11
.word	32
.word	11
.word	6
.word	3
.word	20
.word	6
.word	30
.word	8
.word	15
.word	10
.word	33
.word	11
.word	80
.word	12
.word	14
.word	4
.word	29
.word	8
.word	14
.word	10
.word	81
.word	12
.word	13
.word	5
.word	35
.word	9
.word	13
.word	10
.word	12
.word	5
.word	34
.word	9
.word	82
.word	12
.word	11
.word	5
.word	12
.word	10
.word	83
.word	12
.word	19
.word	6
.word	11
.word	10
.word	84
.word	12
.word	18
.word	6
.word	10
.word	10
.word	17
.word	6
.word	9
.word	10
.word	16
.word	6
.word	8
.word	10
.word	22
.word	7
.word	85
.word	12
.word	21
.word	7
.word	20
.word	7
.word	28
.word	8
.word	27
.word	8
.word	33
.word	9
.word	32
.word	9
.word	31
.word	9
.word	30
.word	9
.word	29
.word	9
.word	28
.word	9
.word	27
.word	9
.word	26
.word	9
.word	34
.word	11
.word	35
.word	11
.word	86
.word	12
.word	87
.word	12
.word	7
.word	4
.word	25
.word	9
.word	5
.word	11
.word	15
.word	6
.word	4
.word	11
.word	14
.word	6
.word	13
.word	6
.word	12
.word	6
.word	19
.word	7
.word	18
.word	7
.word	17
.word	7
.word	16
.word	7
.word	26
.word	8
.word	25
.word	8
.word	24
.word	8
.word	23
.word	8
.word	22
.word	8
.word	21
.word	8
.word	20
.word	8
.word	19
.word	8
.word	24
.word	9
.word	23
.word	9
.word	22
.word	9
.word	21
.word	9
.word	20
.word	9
.word	19
.word	9
.word	18
.word	9
.word	17
.word	9
.word	7
.word	10
.word	6
.word	10
.word	5
.word	10
.word	4
.word	10
.word	36
.word	11
.word	37
.word	11
.word	38
.word	11
.word	39
.word	11
.word	88
.word	12
.word	89
.word	12
.word	90
.word	12
.word	91
.word	12
.word	92
.word	12
.word	93
.word	12
.word	94
.word	12
.word	95
.word	12
.word	3
.word	7
.space	664
.word	0
.word	2
.word	3
.word	3
.word	13
.word	4
.word	5
.word	4
.word	28
.word	5
.word	22
.word	5
.word	63
.word	6
.word	58
.word	6
.word	46
.word	6
.word	34
.word	6
.word	123
.word	7
.word	103
.word	7
.word	95
.word	7
.word	71
.word	7
.word	38
.word	7
.word	239
.word	8
.word	205
.word	8
.word	193
.word	8
.word	169
.word	8
.word	79
.word	8
.word	498
.word	9
.word	477
.word	9
.word	409
.word	9
.word	389
.word	9
.word	349
.word	9
.word	283
.word	9
.word	1007
.word	10
.word	993
.word	10
.word	968
.word	10
.word	817
.word	10
.word	771
.word	10
.word	753
.word	10
.word	672
.word	10
.word	563
.word	10
.word	294
.word	10
.word	1984
.word	11
.word	1903
.word	11
.word	1900
.word	11
.word	1633
.word	11
.word	1540
.word	11
.word	1394
.word	11
.word	1361
.word	11
.word	1130
.word	11
.word	628
.word	11
.word	3879
.word	12
.word	3876
.word	12
.word	3803
.word	12
.word	3214
.word	12
.word	3083
.word	12
.word	3082
.word	12
.word	2787
.word	12
.word	2262
.word	12
.word	1168
.word	12
.word	1173
.word	12
.word	7961
.word	13
.word	7605
.word	13
.word	9
.word	4
.word	16
.word	5
.word	41
.word	6
.word	98
.word	7
.word	243
.word	8
.word	173
.word	8
.word	485
.word	9
.word	377
.word	9
.word	156
.word	9
.word	945
.word	10
.word	686
.word	10
.word	295
.word	10
.word	1902
.word	11
.word	1392
.word	11
.word	629
.word	11
.word	3877
.word	12
.word	3776
.word	12
.word	2720
.word	12
.word	2263
.word	12
.word	7756
.word	13
.word	8
.word	5
.word	99
.word	7
.word	175
.word	8
.word	379
.word	9
.word	947
.word	10
.word	2013
.word	11
.word	1600
.word	11
.word	3981
.word	12
.word	3009
.word	12
.word	1169
.word	12
.word	40
.word	6
.word	195
.word	8
.word	337
.word	9
.word	673
.word	10
.word	1395
.word	11
.word	3779
.word	12
.word	7989
.word	13
.word	101
.word	7
.word	474
.word	9
.word	687
.word	10
.word	631
.word	11
.word	2249
.word	12
.word	6017
.word	13
.word	37
.word	7
.word	280
.word	9
.word	1606
.word	11
.word	2726
.word	12
.word	6016
.word	13
.word	201
.word	8
.word	801
.word	10
.word	3995
.word	12
.word	6430
.word	13
.word	72
.word	8
.word	1996
.word	11
.word	2721
.word	12
.word	384
.word	9
.word	1125
.word	11
.word	6405
.word	13
.word	994
.word	10
.word	3777
.word	12
.word	15515
.word	14
.word	756
.word	10
.word	2248
.word	12
.word	1985
.word	11
.word	2344
.word	13
.word	1505
.word	11
.word	12813
.word	14
.word	3778
.word	12
.word	25624
.word	15
.word	7988
.word	13
.word	120
.word	7
.word	341
.word	9
.word	1362
.word	11
.word	6431
.word	13
.word	250
.word	8
.word	2012
.word	11
.word	6407
.word	13
.word	172
.word	8
.word	585
.word	11
.word	5041
.word	14
.word	502
.word	9
.word	2786
.word	12
.word	476
.word	9
.word	1261
.word	12
.word	388
.word	9
.word	6404
.word	13
.word	342
.word	9
.word	2521
.word	13
.word	999
.word	10
.word	2345
.word	13
.word	946
.word	10
.word	15208
.word	14
.word	757
.word	10
.word	5040
.word	14
.word	802
.word	10
.word	15209
.word	14
.word	564
.word	10
.word	31029
.word	15
.word	1991
.word	11
.word	51251
.word	16
.word	1632
.word	11
.word	31028
.word	15
.word	587
.word	11
.word	51250
.word	16
.word	2727
.word	12
.word	7960
.word	13
.word	122
.word	7
.space	184
.word	2
.word	2
.word	0
.word	3
.word	30
.word	5
.word	4
.word	5
.word	18
.word	6
.word	112
.word	7
.word	26
.word	7
.word	95
.word	8
.word	71
.word	8
.word	467
.word	9
.word	181
.word	9
.word	87
.word	9
.word	949
.word	10
.word	365
.word	10
.word	354
.word	10
.word	1998
.word	11
.word	1817
.word	11
.word	1681
.word	11
.word	710
.word	11
.word	342
.word	11
.word	3986
.word	12
.word	3374
.word	12
.word	3360
.word	12
.word	1438
.word	12
.word	1128
.word	12
.word	678
.word	12
.word	7586
.word	13
.word	7264
.word	13
.word	6723
.word	13
.word	2845
.word	13
.word	2240
.word	13
.word	1373
.word	13
.word	3
.word	3
.word	10
.word	5
.word	119
.word	7
.word	229
.word	8
.word	473
.word	9
.word	997
.word	10
.word	358
.word	10
.word	1684
.word	11
.word	338
.word	11
.word	1439
.word	12
.word	7996
.word	13
.word	6731
.word	13
.word	1374
.word	13
.word	12
.word	4
.word	125
.word	7
.word	68
.word	8
.word	992
.word	10
.word	1897
.word	11
.word	3633
.word	12
.word	7974
.word	13
.word	1372
.word	13
.word	27
.word	5
.word	226
.word	8
.word	933
.word	10
.word	713
.word	11
.word	7971
.word	13
.word	15175
.word	14
.word	7
.word	5
.word	472
.word	9
.word	728
.word	11
.word	7975
.word	13
.word	13460
.word	14
.word	53
.word	6
.word	993
.word	10
.word	1436
.word	12
.word	14531
.word	14
.word	12
.word	6
.word	357
.word	10
.word	7459
.word	13
.word	5688
.word	14
.word	104
.word	7
.word	1683
.word	11
.word	14917
.word	14
.word	32
.word	7
.word	3984
.word	12
.word	31990
.word	15
.word	232
.word	8
.word	1423
.word	12
.word	11503
.word	15
.word	69
.word	8
.word	2874
.word	13
.word	497
.word	9
.word	15174
.word	14
.word	423
.word	9
.word	5750
.word	14
.word	86
.word	9
.word	26922
.word	15
.word	909
.word	10
.word	58121
.word	16
.word	170
.word	10
.word	116241
.word	17
.word	735
.word	11
.word	46009
.word	17
.word	712
.word	11
.word	232480
.word	18
.word	432
.word	11
.word	91024
.word	18
.word	3999
.word	12
.word	92017
.word	18
.word	3792
.word	12
.word	464963
.word	19
.word	3370
.word	12
.word	1023628
.word	20
.word	1121
.word	12
.word	1023630
.word	20
.word	2919
.word	13
.word	1375
.word	13
.word	63
.word	6
.word	109
.word	9
.word	3728
.word	12
.word	1358
.word	13
.word	19
.word	6
.word	281
.word	10
.word	2918
.word	13
.word	11
.word	6
.word	565
.word	11
.word	31989
.word	15
.word	117
.word	7
.word	3364
.word	12
.word	63977
.word	16
.word	46
.word	7
.word	7970
.word	13
.word	33
.word	7
.word	1359
.word	13
.word	20
.word	7
.word	14916
.word	14
.word	228
.word	8
.word	31991
.word	15
.word	94
.word	8
.word	29061
.word	15
.word	55
.word	8
.word	11379
.word	15
.word	475
.word	9
.word	23005
.word	16
.word	455
.word	9
.word	26923
.word	15
.word	422
.word	9
.word	22757
.word	16
.word	180
.word	9
.word	127952
.word	17
.word	176
.word	9
.word	45513
.word	17
.word	998
.word	10
.word	92016
.word	18
.word	366
.word	10
.word	255906
.word	18
.word	283
.word	10
.word	1023629
.word	20
.word	217
.word	10
.word	1023631
.word	20
.word	168
.word	10
.word	182051
.word	19
.word	1865
.word	11
.word	929924
.word	20
.word	1686
.word	11
.word	364101
.word	20
.word	734
.word	11
.word	728200
.word	21
.word	561
.word	11
.word	1859850
.word	21
.word	433
.word	11
.word	7439405
.word	23
.word	3371
.word	12
.word	3719703
.word	22
.word	3375
.word	12
.word	1456403
.word	22
.word	1458
.word	12
.word	1456402
.word	22
.word	1129
.word	12
.word	7439404
.word	23
.word	6722
.word	13
.word	2241
.word	13
.word	115
.word	7
.space	88
.align	2
.type	vc1_ac_sizes, @object
.size	vc1_ac_sizes, 32
vc1_ac_sizes:
.word	186
.word	169
.word	133
.word	149
.word	103
.word	103
.word	163
.word	175

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
