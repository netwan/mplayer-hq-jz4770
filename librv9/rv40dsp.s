.file	1 "rv40dsp.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 3
.abicalls
.section	.text.put_rv40_qpel8_h_lowpass,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_h_lowpass
.type	put_rv40_qpel8_h_lowpass, @function
put_rv40_qpel8_h_lowpass:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$12,24($sp)
lw	$11,28($sp)
lw	$13,32($sp)
blez	$12,$L1
lw	$10,36($sp)

lw	$8,%got(ff_cropTbl)($28)
addiu	$2,$10,-1
li	$9,1			# 0x1
move	$14,$0
sll	$9,$9,$2
addiu	$8,$8,1024
$L3:
lbu	$2,2($5)
addiu	$14,$14,1
lbu	$25,-1($5)
lbu	$24,-2($5)
lbu	$15,3($5)
addu	$25,$25,$2
lbu	$2,0($5)
lbu	$3,1($5)
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$2,3($5)
lbu	$25,0($5)
lbu	$24,-1($5)
lbu	$15,4($5)
addu	$25,$25,$2
lbu	$2,1($5)
lbu	$3,2($5)
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$2,4($5)
lbu	$25,1($5)
lbu	$24,0($5)
lbu	$15,5($5)
addu	$25,$25,$2
lbu	$2,2($5)
lbu	$3,3($5)
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,2($4)
lbu	$2,5($5)
lbu	$25,2($5)
lbu	$24,1($5)
lbu	$15,6($5)
addu	$25,$25,$2
lbu	$2,3($5)
lbu	$3,4($5)
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,3($4)
lbu	$2,2($5)
lbu	$15,7($5)
lbu	$16,6($5)
lbu	$24,3($5)
addu	$15,$2,$15
lbu	$2,4($5)
lbu	$3,5($5)
addu	$24,$24,$16
sll	$16,$24,2
addu	$24,$16,$24
subu	$15,$15,$24
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,4($4)
lbu	$2,7($5)
lbu	$25,4($5)
lbu	$24,3($5)
lbu	$15,8($5)
addu	$25,$25,$2
lbu	$2,5($5)
lbu	$3,6($5)
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,5($4)
lbu	$2,8($5)
lbu	$25,5($5)
lbu	$24,4($5)
lbu	$15,9($5)
addu	$25,$25,$2
lbu	$2,6($5)
lbu	$3,7($5)
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,6($4)
lbu	$2,9($5)
lbu	$25,6($5)
lbu	$24,5($5)
lbu	$15,10($5)
addu	$25,$25,$2
lbu	$2,7($5)
lbu	$3,8($5)
addu	$5,$5,$7
sll	$16,$25,2
addu	$15,$24,$15
addu	$25,$16,$25
subu	$15,$15,$25
mtlo	$15
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$10
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,7($4)
bne	$14,$12,$L3
addu	$4,$4,$6

$L1:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_rv40_qpel8_h_lowpass
.size	put_rv40_qpel8_h_lowpass, .-put_rv40_qpel8_h_lowpass
.section	.text.put_rv40_qpel16_h_lowpass,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_h_lowpass
.type	put_rv40_qpel16_h_lowpass, @function
put_rv40_qpel16_h_lowpass:
.frame	$sp,88,$31		# vars= 8, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-88
li	$2,8			# 0x8
sw	$16,48($sp)
sw	$23,76($sp)
sw	$21,68($sp)
sw	$19,60($sp)
lw	$16,%got(put_rv40_qpel8_h_lowpass)($28)
lw	$23,108($sp)
lw	$21,112($sp)
lw	$19,116($sp)
addiu	$16,$16,%lo(put_rv40_qpel8_h_lowpass)
.cprestore	32
sw	$31,84($sp)
move	$25,$16
sw	$fp,80($sp)
sw	$22,72($sp)
move	$22,$6
sw	$20,64($sp)
move	$20,$7
sw	$18,56($sp)
move	$18,$4
sw	$17,52($sp)
move	$17,$5
sw	$2,16($sp)
sw	$23,20($sp)
sw	$21,24($sp)
sw	$19,28($sp)
sw	$2,40($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
lw	$fp,104($sp)

addiu	$4,$18,8
lw	$2,40($sp)
addiu	$5,$17,8
move	$6,$22
sw	$23,20($sp)
move	$7,$20
sw	$21,24($sp)
sw	$19,28($sp)
move	$25,$16
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
addiu	$fp,$fp,-8

sll	$3,$20,3
sll	$2,$22,3
sw	$23,20($sp)
addu	$17,$17,$3
sw	$fp,16($sp)
addu	$18,$18,$2
sw	$21,24($sp)
move	$5,$17
sw	$19,28($sp)
move	$4,$18
move	$6,$22
move	$25,$16
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$20

addiu	$4,$18,8
lw	$28,32($sp)
addiu	$5,$17,8
lw	$31,84($sp)
move	$6,$22
lw	$18,56($sp)
move	$7,$20
lw	$22,72($sp)
move	$25,$16
lw	$20,64($sp)
lw	$17,52($sp)
lw	$16,48($sp)
sw	$fp,104($sp)
sw	$23,108($sp)
sw	$21,112($sp)
sw	$19,116($sp)
lw	$fp,80($sp)
lw	$23,76($sp)
lw	$21,68($sp)
lw	$19,60($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jr	$25
addiu	$sp,$sp,88

.set	macro
.set	reorder
.end	put_rv40_qpel16_h_lowpass
.size	put_rv40_qpel16_h_lowpass, .-put_rv40_qpel16_h_lowpass
.section	.text.put_rv40_qpel8_mc10_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc10_c
.type	put_rv40_qpel8_mc10_c, @function
put_rv40_qpel8_mc10_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,8			# 0x8
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,20			# 0x14
sw	$2,24($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
sw	$2,28($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc10_c
.size	put_rv40_qpel8_mc10_c, .-put_rv40_qpel8_mc10_c
.section	.text.put_rv40_qpel8_mc20_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc20_c
.type	put_rv40_qpel8_mc20_c, @function
put_rv40_qpel8_mc20_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-48
li	$3,8			# 0x8
sw	$2,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,24($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
move	$7,$6
.cprestore	32
sw	$3,16($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
sw	$2,28($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc20_c
.size	put_rv40_qpel8_mc20_c, .-put_rv40_qpel8_mc20_c
.section	.text.put_rv40_qpel8_mc30_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc30_c
.type	put_rv40_qpel8_mc30_c, @function
put_rv40_qpel8_mc30_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,8			# 0x8
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,52			# 0x34
sw	$2,24($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
sw	$2,28($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc30_c
.size	put_rv40_qpel8_mc30_c, .-put_rv40_qpel8_mc30_c
.section	.text.put_rv40_qpel16_mc10_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc10_c
.type	put_rv40_qpel16_mc10_c, @function
put_rv40_qpel16_mc10_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,16			# 0x10
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,20			# 0x14
sw	$2,24($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
sw	$2,28($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc10_c
.size	put_rv40_qpel16_mc10_c, .-put_rv40_qpel16_mc10_c
.section	.text.put_rv40_qpel16_mc20_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc20_c
.type	put_rv40_qpel16_mc20_c, @function
put_rv40_qpel16_mc20_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-48
li	$3,16			# 0x10
sw	$2,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,24($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
move	$7,$6
.cprestore	32
sw	$3,16($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
sw	$2,28($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc20_c
.size	put_rv40_qpel16_mc20_c, .-put_rv40_qpel16_mc20_c
.section	.text.put_rv40_qpel16_mc30_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc30_c
.type	put_rv40_qpel16_mc30_c, @function
put_rv40_qpel16_mc30_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,16			# 0x10
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,52			# 0x34
sw	$2,24($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
sw	$2,28($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc30_c
.size	put_rv40_qpel16_mc30_c, .-put_rv40_qpel16_mc30_c
.section	.text.put_rv40_chroma_mc4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_chroma_mc4_c
.type	put_rv40_chroma_mc4_c, @function
put_rv40_chroma_mc4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,20($sp)
li	$10,8			# 0x8
lw	$11,16($sp)
sra	$3,$2,1
sra	$9,$11,1
sll	$3,$3,2
mul	$13,$11,$2
addu	$3,$3,$9
subu	$8,$10,$2
sll	$9,$3,2
lw	$3,%got(rv40_bias)($28)
subu	$12,$10,$11
addiu	$3,$3,%lo(rv40_bias)
mul	$2,$12,$2
addu	$3,$9,$3
mul	$9,$12,$8
mul	$8,$11,$8
beq	$13,$0,$L23
lw	$10,0($3)

blez	$7,$L33
addu	$3,$5,$6

move	$14,$0
addiu	$5,$5,1
$L25:
lbu	$11,0($5)
addiu	$14,$14,1
lbu	$24,-1($5)
lbu	$15,0($3)
lbu	$12,1($3)
mult	$11,$8
madd	$24,$9
madd	$15,$2
madd	$12,$13
mflo	$11
addu	$11,$11,$10
sra	$11,$11,6
sb	$11,0($4)
lbu	$11,1($5)
lbu	$24,0($5)
lbu	$15,1($3)
lbu	$12,2($3)
mult	$11,$8
madd	$24,$9
madd	$15,$2
madd	$12,$13
mflo	$11
addu	$11,$11,$10
sra	$11,$11,6
sb	$11,1($4)
lbu	$11,2($5)
lbu	$24,1($5)
lbu	$15,2($3)
lbu	$12,3($3)
mult	$11,$8
madd	$24,$9
madd	$15,$2
madd	$12,$13
mflo	$11
addu	$11,$11,$10
sra	$11,$11,6
sb	$11,2($4)
lbu	$11,3($5)
lbu	$24,2($5)
addu	$5,$5,$6
lbu	$15,3($3)
lbu	$12,4($3)
mult	$11,$8
madd	$24,$9
madd	$15,$2
madd	$12,$13
addu	$3,$3,$6
mflo	$11
addu	$11,$11,$10
sra	$11,$11,6
sb	$11,3($4)
bne	$14,$7,$L25
addu	$4,$4,$6

$L33:
j	$31
nop

$L23:
li	$3,1			# 0x1
addu	$8,$8,$2
blez	$7,$L33
movn	$3,$6,$2

addu	$2,$5,$3
move	$12,$0
$L28:
lbu	$3,0($2)
addiu	$12,$12,1
lbu	$11,0($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,0($4)
lbu	$3,1($2)
lbu	$11,1($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,1($4)
lbu	$3,2($2)
lbu	$11,2($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,2($4)
lbu	$3,3($2)
addu	$2,$2,$6
lbu	$11,3($5)
addu	$5,$5,$6
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,3($4)
bne	$12,$7,$L28
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_rv40_chroma_mc4_c
.size	put_rv40_chroma_mc4_c, .-put_rv40_chroma_mc4_c
.section	.text.put_rv40_chroma_mc8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_chroma_mc8_c
.type	put_rv40_chroma_mc8_c, @function
put_rv40_chroma_mc8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,20($sp)
li	$10,8			# 0x8
lw	$12,16($sp)
sra	$3,$2,1
sra	$9,$12,1
sll	$3,$3,2
mul	$11,$12,$2
addu	$3,$3,$9
subu	$8,$10,$2
sll	$9,$3,2
lw	$3,%got(rv40_bias)($28)
subu	$13,$10,$12
addiu	$3,$3,%lo(rv40_bias)
mul	$2,$13,$2
addu	$3,$9,$3
mul	$9,$13,$8
mul	$8,$12,$8
beq	$11,$0,$L35
lw	$10,0($3)

blez	$7,$L45
addu	$3,$5,$6

move	$14,$0
$L37:
lbu	$12,1($5)
addiu	$14,$14,1
lbu	$24,0($5)
lbu	$15,0($3)
lbu	$13,1($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,0($4)
lbu	$12,2($5)
lbu	$24,1($5)
lbu	$15,1($3)
lbu	$13,2($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,1($4)
lbu	$12,3($5)
lbu	$24,2($5)
lbu	$15,2($3)
lbu	$13,3($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,2($4)
lbu	$12,4($5)
lbu	$24,3($5)
lbu	$15,3($3)
lbu	$13,4($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,3($4)
lbu	$12,5($5)
lbu	$24,4($5)
lbu	$15,4($3)
lbu	$13,5($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,4($4)
lbu	$12,6($5)
lbu	$24,5($5)
lbu	$15,5($3)
lbu	$13,6($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,5($4)
lbu	$12,7($5)
lbu	$24,6($5)
lbu	$15,6($3)
mult	$12,$8
lbu	$13,7($3)
madd	$24,$9
madd	$15,$2
madd	$13,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,6($4)
lbu	$12,8($5)
lbu	$24,7($5)
addu	$5,$5,$6
lbu	$15,7($3)
lbu	$13,8($3)
mult	$12,$8
madd	$24,$9
madd	$15,$2
madd	$13,$11
addu	$3,$3,$6
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
sb	$12,7($4)
bne	$14,$7,$L37
addu	$4,$4,$6

$L45:
j	$31
nop

$L35:
li	$3,1			# 0x1
addu	$8,$8,$2
blez	$7,$L45
movn	$3,$6,$2

addu	$2,$5,$3
move	$12,$0
$L40:
lbu	$3,0($2)
addiu	$12,$12,1
lbu	$11,0($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,0($4)
lbu	$3,1($2)
lbu	$11,1($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,1($4)
lbu	$3,2($2)
lbu	$11,2($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,2($4)
lbu	$3,3($2)
lbu	$11,3($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,3($4)
lbu	$3,4($2)
lbu	$11,4($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,4($4)
lbu	$3,5($2)
lbu	$11,5($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,5($4)
lbu	$3,6($2)
lbu	$11,6($5)
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,6($4)
lbu	$3,7($2)
addu	$2,$2,$6
lbu	$11,7($5)
addu	$5,$5,$6
mult	$3,$8
madd	$11,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
sb	$3,7($4)
bne	$12,$7,$L40
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_rv40_chroma_mc8_c
.size	put_rv40_chroma_mc8_c, .-put_rv40_chroma_mc8_c
.section	.text.avg_rv40_chroma_mc4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_chroma_mc4_c
.type	avg_rv40_chroma_mc4_c, @function
avg_rv40_chroma_mc4_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
li	$10,8			# 0x8
sw	$17,4($sp)
lw	$2,28($sp)
lw	$3,24($sp)
sw	$16,0($sp)
sra	$8,$2,1
sra	$9,$3,1
sll	$8,$8,2
mul	$13,$3,$2
addu	$8,$8,$9
subu	$11,$10,$3
sll	$9,$8,2
lw	$8,%got(rv40_bias)($28)
subu	$10,$10,$2
mul	$2,$11,$2
addiu	$8,$8,%lo(rv40_bias)
mul	$14,$3,$10
addu	$8,$9,$8
lw	$9,0($8)
beq	$13,$0,$L47
mul	$8,$11,$10

blez	$7,$L57
lw	$17,4($sp)

addu	$3,$5,$6
move	$15,$0
addiu	$5,$5,1
$L49:
lbu	$10,0($5)
addiu	$15,$15,1
lbu	$16,-1($5)
lbu	$12,0($3)
lbu	$11,1($3)
mult	$10,$14
madd	$16,$8
lbu	$25,0($4)
madd	$12,$2
lbu	$24,1($4)
madd	$11,$13
lbu	$12,2($4)
lbu	$11,3($4)
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$25,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,0($4)
lbu	$10,1($5)
lbu	$17,0($5)
lbu	$16,1($3)
lbu	$25,2($3)
mult	$10,$14
madd	$17,$8
madd	$16,$2
madd	$25,$13
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$24,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,1($4)
lbu	$10,2($5)
lbu	$16,1($5)
lbu	$25,2($3)
lbu	$24,3($3)
mult	$10,$14
madd	$16,$8
madd	$25,$2
madd	$24,$13
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$12,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,2($4)
lbu	$10,3($5)
lbu	$25,2($5)
addu	$5,$5,$6
lbu	$24,3($3)
lbu	$12,4($3)
mult	$10,$14
madd	$25,$8
madd	$24,$2
madd	$12,$13
addu	$3,$3,$6
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$11,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,3($4)
bne	$15,$7,$L49
addu	$4,$4,$6

$L46:
lw	$17,4($sp)
$L57:
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L47:
li	$10,1			# 0x1
addu	$3,$14,$2
blez	$7,$L46
movn	$10,$6,$2

addu	$2,$5,$10
move	$14,$0
$L52:
lbu	$10,0($2)
addiu	$14,$14,1
lbu	$11,0($5)
lbu	$15,0($4)
mult	$10,$3
lbu	$13,1($4)
madd	$11,$8
lbu	$12,2($4)
lbu	$11,3($4)
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$15,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,0($4)
lbu	$10,1($2)
lbu	$15,1($5)
mult	$10,$3
madd	$15,$8
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$13,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,1($4)
lbu	$10,2($2)
lbu	$13,2($5)
mult	$10,$3
madd	$13,$8
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$12,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,2($4)
lbu	$10,3($2)
addu	$2,$2,$6
lbu	$12,3($5)
addu	$5,$5,$6
mult	$10,$3
madd	$12,$8
mflo	$10
addu	$10,$10,$9
sra	$10,$10,6
addu	$10,$11,$10
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,3($4)
bne	$14,$7,$L52
addu	$4,$4,$6

lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	avg_rv40_chroma_mc4_c
.size	avg_rv40_chroma_mc4_c, .-avg_rv40_chroma_mc4_c
.section	.text.avg_rv40_chroma_mc8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_chroma_mc8_c
.type	avg_rv40_chroma_mc8_c, @function
avg_rv40_chroma_mc8_c:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-16
li	$10,8			# 0x8
sw	$19,12($sp)
lw	$2,36($sp)
lw	$12,32($sp)
sw	$18,8($sp)
sra	$3,$2,1
sw	$17,4($sp)
sra	$9,$12,1
sw	$16,0($sp)
sll	$3,$3,2
mul	$11,$12,$2
addu	$3,$3,$9
subu	$8,$10,$2
sll	$9,$3,2
lw	$3,%got(rv40_bias)($28)
subu	$13,$10,$12
addiu	$3,$3,%lo(rv40_bias)
mul	$2,$13,$2
addu	$3,$9,$3
mul	$9,$13,$8
mul	$8,$12,$8
beq	$11,$0,$L59
lw	$10,0($3)

blez	$7,$L69
lw	$19,12($sp)

addu	$3,$5,$6
move	$15,$0
$L61:
lbu	$12,1($5)
addiu	$15,$15,1
lbu	$16,0($5)
lbu	$14,0($3)
lbu	$13,1($3)
mult	$12,$8
madd	$16,$9
lbu	$25,0($4)
madd	$14,$2
lbu	$24,1($4)
madd	$13,$11
lbu	$17,2($4)
lbu	$14,3($4)
lbu	$13,4($4)
lbu	$16,5($4)
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$25,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,0($4)
lbu	$12,2($5)
lbu	$19,1($5)
lbu	$18,1($3)
lbu	$25,2($3)
mult	$12,$8
madd	$19,$9
madd	$18,$2
madd	$25,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$24,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,1($4)
lbu	$12,3($5)
lbu	$18,2($5)
lbu	$25,2($3)
lbu	$24,3($3)
mult	$12,$8
madd	$18,$9
madd	$25,$2
madd	$24,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$17,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,2($4)
lbu	$12,4($5)
lbu	$25,3($5)
lbu	$24,3($3)
lbu	$17,4($3)
mult	$12,$8
madd	$25,$9
madd	$24,$2
madd	$17,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$14,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,3($4)
lbu	$12,5($5)
lbu	$24,4($5)
lbu	$17,4($3)
lbu	$14,5($3)
mult	$12,$8
madd	$24,$9
madd	$17,$2
madd	$14,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$13,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,4($4)
lbu	$12,6($5)
lbu	$14,5($5)
lbu	$13,5($3)
mult	$12,$8
lbu	$17,6($3)
madd	$14,$9
lbu	$14,6($4)
madd	$13,$2
lbu	$13,7($4)
madd	$17,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$16,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,5($4)
lbu	$12,7($5)
lbu	$24,6($5)
lbu	$17,6($3)
lbu	$16,7($3)
mult	$12,$8
madd	$24,$9
madd	$17,$2
madd	$16,$11
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$14,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,6($4)
lbu	$12,8($5)
lbu	$17,7($5)
addu	$5,$5,$6
lbu	$16,7($3)
lbu	$14,8($3)
mult	$12,$8
madd	$17,$9
madd	$16,$2
madd	$14,$11
addu	$3,$3,$6
mflo	$12
addu	$12,$12,$10
sra	$12,$12,6
addu	$12,$13,$12
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,7($4)
bne	$15,$7,$L61
addu	$4,$4,$6

$L58:
lw	$19,12($sp)
$L69:
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

$L59:
li	$3,1			# 0x1
addu	$8,$8,$2
blez	$7,$L58
movn	$3,$6,$2

addu	$2,$5,$3
move	$14,$0
$L64:
lbu	$3,0($2)
addiu	$14,$14,1
lbu	$11,0($5)
lbu	$17,0($4)
mult	$3,$8
lbu	$16,1($4)
madd	$11,$9
lbu	$25,2($4)
lbu	$24,3($4)
lbu	$15,4($4)
lbu	$13,5($4)
mflo	$3
lbu	$12,6($4)
lbu	$11,7($4)
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$17,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,0($4)
lbu	$3,1($2)
lbu	$17,1($5)
mult	$3,$8
madd	$17,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$16,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,1($4)
lbu	$3,2($2)
lbu	$16,2($5)
mult	$3,$8
madd	$16,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$25,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,2($4)
lbu	$3,3($2)
lbu	$25,3($5)
mult	$3,$8
madd	$25,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$24,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,3($4)
lbu	$3,4($2)
lbu	$24,4($5)
mult	$3,$8
madd	$24,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$15,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,4($4)
lbu	$3,5($2)
lbu	$15,5($5)
mult	$3,$8
madd	$15,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$13,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,5($4)
lbu	$3,6($2)
lbu	$13,6($5)
mult	$3,$8
madd	$13,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$12,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,6($4)
lbu	$3,7($2)
addu	$2,$2,$6
lbu	$12,7($5)
addu	$5,$5,$6
mult	$3,$8
madd	$12,$9
mflo	$3
addu	$3,$3,$10
sra	$3,$3,6
addu	$3,$11,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,7($4)
bne	$14,$7,$L64
addu	$4,$4,$6

lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	avg_rv40_chroma_mc8_c
.size	avg_rv40_chroma_mc8_c, .-avg_rv40_chroma_mc8_c
.section	.text.avg_rv40_qpel8_v_lowpass.constprop.2,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_v_lowpass.constprop.2
.type	avg_rv40_qpel8_v_lowpass.constprop.2, @function
avg_rv40_qpel8_v_lowpass.constprop.2:
.frame	$sp,80,$31		# vars= 32, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-80
subu	$3,$0,$7
sw	$23,72($sp)
addu	$23,$4,$6
sw	$fp,76($sp)
sll	$3,$3,1
addu	$fp,$23,$6
sw	$19,56($sp)
sw	$18,52($sp)
li	$18,1			# 0x1
addu	$2,$fp,$6
sw	$17,48($sp)
lw	$19,104($sp)
lw	$17,%got(ff_cropTbl)($28)
sw	$2,8($sp)
lw	$8,8($sp)
addiu	$2,$19,-1
sw	$21,64($sp)
addiu	$17,$17,1024
sll	$18,$18,$2
sw	$20,60($sp)
addu	$8,$8,$6
.cprestore	0
addiu	$2,$4,8
sw	$22,68($sp)
addu	$9,$8,$6
sw	$16,44($sp)
sw	$8,12($sp)
addu	$15,$9,$6
sw	$3,36($sp)
sw	$9,16($sp)
addu	$6,$15,$6
sw	$2,32($sp)
sw	$15,20($sp)
lw	$21,96($sp)
lw	$20,100($sp)
sw	$6,24($sp)
$L71:
lw	$3,36($sp)
addu	$2,$5,$7
lbu	$8,0($5)
addiu	$4,$4,1
lbu	$14,-1($4)
addiu	$23,$23,1
addu	$6,$5,$3
addu	$3,$2,$7
addu	$10,$6,$7
lbu	$9,0($6)
addiu	$5,$5,1
lbu	$6,0($2)
addu	$2,$3,$7
lbu	$10,0($10)
addiu	$fp,$fp,1
lbu	$3,0($3)
addu	$16,$2,$7
lbu	$2,0($2)
addu	$11,$10,$3
lbu	$13,0($16)
addu	$9,$9,$2
sll	$12,$11,2
addu	$15,$8,$2
addu	$11,$12,$11
sll	$22,$15,2
subu	$11,$9,$11
addu	$10,$10,$13
mtlo	$11
addu	$15,$22,$15
addu	$16,$16,$7
subu	$15,$10,$15
addu	$25,$6,$13
lbu	$12,0($16)
addu	$16,$16,$7
sll	$24,$25,2
madd	$8,$21
madd	$6,$20
lbu	$10,0($16)
addu	$16,$16,$7
addu	$8,$8,$12
addu	$24,$24,$25
mflo	$11
mtlo	$15
addu	$15,$16,$7
lbu	$16,0($16)
subu	$24,$8,$24
addu	$25,$2,$10
addu	$11,$11,$18
madd	$6,$21
sra	$11,$11,$19
madd	$3,$20
addu	$11,$17,$11
addu	$6,$6,$10
lbu	$11,0($11)
mflo	$9
mtlo	$24
addu	$14,$14,$11
lbu	$11,0($15)
addu	$15,$15,$7
addiu	$14,$14,1
lbu	$22,0($15)
addu	$15,$15,$7
addu	$9,$9,$18
sra	$14,$14,1
lbu	$15,0($15)
sra	$9,$9,$19
madd	$3,$21
addu	$9,$17,$9
sb	$14,-1($4)
madd	$2,$20
lbu	$14,-1($23)
sw	$15,28($sp)
addu	$15,$3,$12
lbu	$8,0($9)
addu	$3,$3,$16
sll	$9,$15,2
mflo	$24
addu	$9,$9,$15
addu	$14,$14,$8
subu	$6,$6,$9
addiu	$14,$14,1
mtlo	$6
sra	$14,$14,1
addu	$24,$24,$18
addu	$9,$13,$16
sb	$14,-1($23)
sra	$24,$24,$19
sll	$14,$25,2
lbu	$8,-1($fp)
addu	$24,$17,$24
madd	$2,$21
madd	$13,$20
addu	$14,$14,$25
lbu	$24,0($24)
addu	$2,$2,$11
subu	$3,$3,$14
mflo	$6
mtlo	$3
lw	$3,8($sp)
addu	$8,$8,$24
addu	$14,$12,$11
addiu	$8,$8,1
sra	$8,$8,1
addu	$6,$6,$18
madd	$13,$21
sra	$6,$6,$19
sb	$8,-1($fp)
madd	$12,$20
lbu	$8,0($3)
addu	$6,$17,$6
addu	$13,$13,$22
addu	$22,$10,$22
lbu	$15,0($6)
sll	$6,$9,2
mflo	$3
addu	$6,$6,$9
lw	$9,8($sp)
addu	$8,$8,$15
subu	$2,$2,$6
lw	$6,12($sp)
addiu	$8,$8,1
mtlo	$2
lw	$2,12($sp)
sra	$8,$8,1
addu	$3,$3,$18
sra	$3,$3,$19
sb	$8,0($9)
lbu	$8,0($2)
addu	$3,$17,$3
madd	$12,$21
madd	$10,$20
lbu	$15,0($3)
sll	$3,$14,2
addu	$3,$3,$14
mflo	$2
subu	$13,$13,$3
addu	$8,$8,$15
mtlo	$13
addiu	$8,$8,1
sra	$8,$8,1
addu	$2,$2,$18
sra	$2,$2,$19
sb	$8,0($6)
madd	$10,$21
lw	$3,28($sp)
addu	$2,$17,$2
lw	$9,16($sp)
madd	$16,$20
lw	$15,20($sp)
addu	$12,$12,$3
lbu	$8,0($2)
sll	$2,$22,2
lbu	$6,0($9)
addiu	$9,$9,1
mflo	$10
addu	$22,$2,$22
addu	$6,$6,$8
lw	$8,12($sp)
subu	$12,$12,$22
addiu	$6,$6,1
mtlo	$12
sra	$6,$6,1
addu	$10,$10,$18
addiu	$8,$8,1
sra	$10,$10,$19
sb	$6,-1($9)
lbu	$2,0($15)
addu	$10,$17,$10
lw	$6,8($sp)
madd	$16,$21
sw	$8,12($sp)
madd	$11,$20
sw	$9,16($sp)
lbu	$3,0($10)
addiu	$6,$6,1
mflo	$11
sw	$6,8($sp)
addu	$2,$2,$3
lw	$3,20($sp)
lw	$6,32($sp)
addiu	$2,$2,1
addiu	$3,$3,1
sra	$2,$2,1
addu	$11,$11,$18
sw	$3,20($sp)
sb	$2,0($15)
lw	$15,24($sp)
sra	$11,$11,$19
addu	$11,$17,$11
addiu	$15,$15,1
lbu	$2,-1($15)
lbu	$3,0($11)
sw	$15,24($sp)
addu	$2,$2,$3
addiu	$2,$2,1
sra	$2,$2,1
bne	$4,$6,$L71
sb	$2,-1($15)

lw	$fp,76($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	avg_rv40_qpel8_v_lowpass.constprop.2
.size	avg_rv40_qpel8_v_lowpass.constprop.2, .-avg_rv40_qpel8_v_lowpass.constprop.2
.section	.text.avg_rv40_qpel16_v_lowpass.constprop.1,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_v_lowpass.constprop.1
.type	avg_rv40_qpel16_v_lowpass.constprop.1, @function
avg_rv40_qpel16_v_lowpass.constprop.1:
.frame	$sp,80,$31		# vars= 0, regs= 9/0, args= 32, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-80
sw	$16,44($sp)
sw	$23,72($sp)
sw	$22,68($sp)
sw	$20,60($sp)
lw	$16,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
lw	$23,96($sp)
lw	$22,100($sp)
lw	$20,104($sp)
addiu	$16,$16,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.cprestore	32
sw	$31,76($sp)
move	$25,$16
sw	$21,64($sp)
move	$21,$6
sw	$19,56($sp)
move	$19,$7
sw	$18,52($sp)
move	$18,$4
sw	$17,48($sp)
move	$17,$5
sw	$23,16($sp)
sw	$22,20($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$20,24($sp)

addiu	$4,$18,8
addiu	$5,$17,8
sw	$23,16($sp)
move	$6,$21
sw	$22,20($sp)
move	$7,$19
move	$25,$16
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$20,24($sp)

sll	$3,$19,3
sll	$2,$21,3
sw	$23,16($sp)
addu	$17,$17,$3
sw	$22,20($sp)
addu	$18,$18,$2
sw	$20,24($sp)
move	$5,$17
move	$4,$18
move	$6,$21
move	$25,$16
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
move	$7,$19

addiu	$4,$18,8
lw	$28,32($sp)
addiu	$5,$17,8
lw	$31,76($sp)
move	$6,$21
lw	$18,52($sp)
move	$7,$19
lw	$21,64($sp)
move	$25,$16
lw	$19,56($sp)
lw	$17,48($sp)
lw	$16,44($sp)
sw	$23,96($sp)
sw	$22,100($sp)
sw	$20,104($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$20,60($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jr	$25
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	avg_rv40_qpel16_v_lowpass.constprop.1
.size	avg_rv40_qpel16_v_lowpass.constprop.1, .-avg_rv40_qpel16_v_lowpass.constprop.1
.section	.text.avg_rv40_qpel16_mc23_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc23_c
.type	avg_rv40_qpel16_mc23_c, @function
avg_rv40_qpel16_mc23_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-392
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$17,380($sp)
li	$3,21			# 0x15
li	$17,20			# 0x14
sw	$18,384($sp)
subu	$5,$5,$2
sw	$16,376($sp)
li	$2,5			# 0x5
sw	$31,388($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,16			# 0x10
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,16($sp)
sw	$2,20($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
move	$6,$16
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$2,24($sp)

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc23_c
.size	avg_rv40_qpel16_mc23_c, .-avg_rv40_qpel16_mc23_c
.section	.text.avg_rv40_qpel16_mc13_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc13_c
.type	avg_rv40_qpel16_mc13_c, @function
avg_rv40_qpel16_mc13_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$19,388($sp)
li	$19,20			# 0x14
sw	$18,384($sp)
subu	$5,$5,$2
sw	$17,380($sp)
li	$2,21			# 0x15
li	$18,52			# 0x34
sw	$20,392($sp)
li	$17,6			# 0x6
sw	$16,376($sp)
move	$20,$4
sw	$31,396($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$17,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc13_c
.size	avg_rv40_qpel16_mc13_c, .-avg_rv40_qpel16_mc13_c
.section	.text.avg_rv40_qpel16_mc03_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc03_c
.type	avg_rv40_qpel16_mc03_c, @function
avg_rv40_qpel16_mc03_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc03_c
.size	avg_rv40_qpel16_mc03_c, .-avg_rv40_qpel16_mc03_c
.section	.text.avg_rv40_qpel16_mc32_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc32_c
.type	avg_rv40_qpel16_mc32_c, @function
avg_rv40_qpel16_mc32_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,21			# 0x15
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-392
sll	$2,$6,1
sw	$17,380($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,384($sp)
li	$2,6			# 0x6
sw	$16,376($sp)
move	$18,$4
sw	$31,388($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,24($sp)
li	$6,16			# 0x10
sw	$17,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
move	$6,$16

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc32_c
.size	avg_rv40_qpel16_mc32_c, .-avg_rv40_qpel16_mc32_c
.section	.text.avg_rv40_qpel16_mc22_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc22_c
.type	avg_rv40_qpel16_mc22_c, @function
avg_rv40_qpel16_mc22_c:
.frame	$sp,400,$31		# vars= 336, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$18,388($sp)
li	$18,5			# 0x5
sw	$16,380($sp)
subu	$5,$5,$2
li	$16,20			# 0x14
sw	$19,392($sp)
li	$2,21			# 0x15
sw	$17,384($sp)
move	$19,$4
sw	$31,396($sp)
move	$17,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$16,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$16,24($sp)
sw	$18,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$17

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$16,16($sp)
move	$4,$19
sw	$16,20($sp)
move	$6,$17
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$18,24($sp)

lw	$31,396($sp)
lw	$19,392($sp)
lw	$18,388($sp)
lw	$17,384($sp)
lw	$16,380($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc22_c
.size	avg_rv40_qpel16_mc22_c, .-avg_rv40_qpel16_mc22_c
.section	.text.avg_rv40_qpel16_mc12_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc12_c
.type	avg_rv40_qpel16_mc12_c, @function
avg_rv40_qpel16_mc12_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,21			# 0x15
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-392
sll	$2,$6,1
sw	$17,380($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,384($sp)
li	$2,6			# 0x6
sw	$16,376($sp)
move	$18,$4
sw	$31,388($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,20($sp)
li	$6,16			# 0x10
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
move	$6,$16

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc12_c
.size	avg_rv40_qpel16_mc12_c, .-avg_rv40_qpel16_mc12_c
.section	.text.avg_rv40_qpel16_mc02_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc02_c
.type	avg_rv40_qpel16_mc02_c, @function
avg_rv40_qpel16_mc02_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$sp,$sp,-48
sw	$2,16($sp)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
sw	$2,20($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
.cprestore	32
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
move	$7,$6

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc02_c
.size	avg_rv40_qpel16_mc02_c, .-avg_rv40_qpel16_mc02_c
.section	.text.avg_rv40_qpel16_mc31_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc31_c
.type	avg_rv40_qpel16_mc31_c, @function
avg_rv40_qpel16_mc31_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$19,388($sp)
li	$19,52			# 0x34
sw	$18,384($sp)
subu	$5,$5,$2
sw	$17,380($sp)
li	$2,21			# 0x15
li	$18,20			# 0x14
sw	$20,392($sp)
li	$17,6			# 0x6
sw	$16,376($sp)
move	$20,$4
sw	$31,396($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$17,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc31_c
.size	avg_rv40_qpel16_mc31_c, .-avg_rv40_qpel16_mc31_c
.section	.text.avg_rv40_qpel16_mc21_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc21_c
.type	avg_rv40_qpel16_mc21_c, @function
avg_rv40_qpel16_mc21_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-392
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$17,380($sp)
li	$3,21			# 0x15
li	$17,20			# 0x14
sw	$18,384($sp)
subu	$5,$5,$2
sw	$16,376($sp)
li	$2,5			# 0x5
sw	$31,388($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,16			# 0x10
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,20($sp)
sw	$2,16($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
move	$6,$16
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$2,24($sp)

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc21_c
.size	avg_rv40_qpel16_mc21_c, .-avg_rv40_qpel16_mc21_c
.section	.text.avg_rv40_qpel16_mc11_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc11_c
.type	avg_rv40_qpel16_mc11_c, @function
avg_rv40_qpel16_mc11_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$19,388($sp)
li	$19,52			# 0x34
sw	$18,384($sp)
subu	$5,$5,$2
sw	$17,380($sp)
li	$2,21			# 0x15
li	$18,20			# 0x14
sw	$20,392($sp)
li	$17,6			# 0x6
sw	$16,376($sp)
move	$20,$4
sw	$31,396($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$19,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$18,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$17,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc11_c
.size	avg_rv40_qpel16_mc11_c, .-avg_rv40_qpel16_mc11_c
.section	.text.avg_rv40_qpel16_mc01_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc01_c
.type	avg_rv40_qpel16_mc01_c, @function
avg_rv40_qpel16_mc01_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,52			# 0x34
lw	$25,%got(avg_rv40_qpel16_v_lowpass.constprop.1)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(avg_rv40_qpel16_v_lowpass.constprop.1)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_v_lowpass.constprop.1
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc01_c
.size	avg_rv40_qpel16_mc01_c, .-avg_rv40_qpel16_mc01_c
.section	.text.avg_rv40_qpel8_mc23_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc23_c
.type	avg_rv40_qpel8_mc23_c, @function
avg_rv40_qpel8_mc23_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-160
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$17,148($sp)
li	$3,13			# 0xd
li	$17,20			# 0x14
sw	$18,152($sp)
subu	$5,$5,$2
sw	$16,144($sp)
li	$2,5			# 0x5
sw	$31,156($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,8			# 0x8
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,16($sp)
sw	$2,20($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
move	$6,$16
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$2,24($sp)

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc23_c
.size	avg_rv40_qpel8_mc23_c, .-avg_rv40_qpel8_mc23_c
.section	.text.avg_rv40_qpel8_mc13_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc13_c
.type	avg_rv40_qpel8_mc13_c, @function
avg_rv40_qpel8_mc13_c:
.frame	$sp,168,$31		# vars= 104, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$19,156($sp)
li	$19,20			# 0x14
sw	$18,152($sp)
subu	$5,$5,$2
sw	$17,148($sp)
li	$2,13			# 0xd
li	$18,52			# 0x34
sw	$20,160($sp)
li	$17,6			# 0x6
sw	$16,144($sp)
move	$20,$4
sw	$31,164($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$17,24($sp)

lw	$31,164($sp)
lw	$20,160($sp)
lw	$19,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc13_c
.size	avg_rv40_qpel8_mc13_c, .-avg_rv40_qpel8_mc13_c
.section	.text.avg_rv40_qpel8_mc03_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc03_c
.type	avg_rv40_qpel8_mc03_c, @function
avg_rv40_qpel8_mc03_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc03_c
.size	avg_rv40_qpel8_mc03_c, .-avg_rv40_qpel8_mc03_c
.section	.text.avg_rv40_qpel8_mc32_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc32_c
.type	avg_rv40_qpel8_mc32_c, @function
avg_rv40_qpel8_mc32_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,13			# 0xd
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-160
sll	$2,$6,1
sw	$17,148($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,152($sp)
li	$2,6			# 0x6
sw	$16,144($sp)
move	$18,$4
sw	$31,156($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,24($sp)
li	$6,8			# 0x8
sw	$17,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
move	$6,$16

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc32_c
.size	avg_rv40_qpel8_mc32_c, .-avg_rv40_qpel8_mc32_c
.section	.text.avg_rv40_qpel8_mc22_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc22_c
.type	avg_rv40_qpel8_mc22_c, @function
avg_rv40_qpel8_mc22_c:
.frame	$sp,168,$31		# vars= 104, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$18,156($sp)
li	$18,5			# 0x5
sw	$16,148($sp)
subu	$5,$5,$2
li	$16,20			# 0x14
sw	$19,160($sp)
li	$2,13			# 0xd
sw	$17,152($sp)
move	$19,$4
sw	$31,164($sp)
move	$17,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$16,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$16,24($sp)
sw	$18,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$17

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$16,16($sp)
move	$4,$19
sw	$16,20($sp)
move	$6,$17
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$18,24($sp)

lw	$31,164($sp)
lw	$19,160($sp)
lw	$18,156($sp)
lw	$17,152($sp)
lw	$16,148($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc22_c
.size	avg_rv40_qpel8_mc22_c, .-avg_rv40_qpel8_mc22_c
.section	.text.avg_rv40_qpel8_mc12_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc12_c
.type	avg_rv40_qpel8_mc12_c, @function
avg_rv40_qpel8_mc12_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,13			# 0xd
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-160
sll	$2,$6,1
sw	$17,148($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,152($sp)
li	$2,6			# 0x6
sw	$16,144($sp)
move	$18,$4
sw	$31,156($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,20($sp)
li	$6,8			# 0x8
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
move	$6,$16

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc12_c
.size	avg_rv40_qpel8_mc12_c, .-avg_rv40_qpel8_mc12_c
.section	.text.avg_rv40_qpel8_mc02_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc02_c
.type	avg_rv40_qpel8_mc02_c, @function
avg_rv40_qpel8_mc02_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$sp,$sp,-48
sw	$2,16($sp)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
sw	$2,20($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
.cprestore	32
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
move	$7,$6

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc02_c
.size	avg_rv40_qpel8_mc02_c, .-avg_rv40_qpel8_mc02_c
.section	.text.avg_rv40_qpel8_mc31_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc31_c
.type	avg_rv40_qpel8_mc31_c, @function
avg_rv40_qpel8_mc31_c:
.frame	$sp,168,$31		# vars= 104, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$19,156($sp)
li	$19,52			# 0x34
sw	$18,152($sp)
subu	$5,$5,$2
sw	$17,148($sp)
li	$2,13			# 0xd
li	$18,20			# 0x14
sw	$20,160($sp)
li	$17,6			# 0x6
sw	$16,144($sp)
move	$20,$4
sw	$31,164($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$17,24($sp)

lw	$31,164($sp)
lw	$20,160($sp)
lw	$19,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc31_c
.size	avg_rv40_qpel8_mc31_c, .-avg_rv40_qpel8_mc31_c
.section	.text.avg_rv40_qpel8_mc21_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc21_c
.type	avg_rv40_qpel8_mc21_c, @function
avg_rv40_qpel8_mc21_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-160
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$17,148($sp)
li	$3,13			# 0xd
li	$17,20			# 0x14
sw	$18,152($sp)
subu	$5,$5,$2
sw	$16,144($sp)
li	$2,5			# 0x5
sw	$31,156($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,8			# 0x8
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,20($sp)
sw	$2,16($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
move	$6,$16
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$2,24($sp)

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc21_c
.size	avg_rv40_qpel8_mc21_c, .-avg_rv40_qpel8_mc21_c
.section	.text.avg_rv40_qpel8_mc11_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc11_c
.type	avg_rv40_qpel8_mc11_c, @function
avg_rv40_qpel8_mc11_c:
.frame	$sp,168,$31		# vars= 104, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$19,156($sp)
li	$19,52			# 0x34
sw	$18,152($sp)
subu	$5,$5,$2
sw	$17,148($sp)
li	$2,13			# 0xd
li	$18,20			# 0x14
sw	$20,160($sp)
li	$17,6			# 0x6
sw	$16,144($sp)
move	$20,$4
sw	$31,164($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$19,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$18,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$17,24($sp)

lw	$31,164($sp)
lw	$20,160($sp)
lw	$19,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc11_c
.size	avg_rv40_qpel8_mc11_c, .-avg_rv40_qpel8_mc11_c
.section	.text.avg_rv40_qpel8_mc01_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc01_c
.type	avg_rv40_qpel8_mc01_c, @function
avg_rv40_qpel8_mc01_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,52			# 0x34
lw	$25,%got(avg_rv40_qpel8_v_lowpass.constprop.2)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(avg_rv40_qpel8_v_lowpass.constprop.2)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_v_lowpass.constprop.2
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc01_c
.size	avg_rv40_qpel8_mc01_c, .-avg_rv40_qpel8_mc01_c
.section	.text.avg_rv40_qpel8_h_lowpass.constprop.3,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_h_lowpass.constprop.3
.type	avg_rv40_qpel8_h_lowpass.constprop.3, @function
avg_rv40_qpel8_h_lowpass.constprop.3:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-16
lw	$9,%got(ff_cropTbl)($28)
li	$8,1			# 0x1
li	$12,8			# 0x8
sw	$19,12($sp)
lw	$10,40($sp)
addiu	$9,$9,1024
lw	$11,32($sp)
lw	$13,36($sp)
addiu	$2,$10,-1
sw	$18,8($sp)
sw	$17,4($sp)
sll	$8,$8,$2
sw	$16,0($sp)
$L121:
lbu	$15,-1($5)
addiu	$12,$12,-1
lbu	$18,2($5)
lbu	$14,-2($5)
lbu	$17,3($5)
addu	$18,$15,$18
lbu	$2,0($5)
lbu	$3,1($5)
sll	$19,$18,2
lbu	$16,0($4)
addu	$17,$14,$17
lbu	$15,1($4)
addu	$18,$19,$18
lbu	$14,2($4)
lbu	$25,3($4)
subu	$17,$17,$18
mtlo	$17
madd	$2,$11
madd	$3,$13
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$16,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,0($4)
lbu	$2,3($5)
lbu	$18,0($5)
lbu	$17,-1($5)
lbu	$16,4($5)
addu	$18,$18,$2
lbu	$2,1($5)
lbu	$3,2($5)
sll	$19,$18,2
addu	$16,$17,$16
addu	$18,$19,$18
subu	$16,$16,$18
mtlo	$16
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$15,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,1($4)
lbu	$2,4($5)
lbu	$17,1($5)
lbu	$16,0($5)
lbu	$15,5($5)
addu	$17,$17,$2
lbu	$2,2($5)
lbu	$3,3($5)
sll	$18,$17,2
addu	$15,$16,$15
addu	$17,$18,$17
subu	$15,$15,$17
mtlo	$15
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$14,$14,$2
addiu	$14,$14,1
sra	$14,$14,1
sb	$14,2($4)
lbu	$2,5($5)
lbu	$15,2($5)
lbu	$14,1($5)
lbu	$17,6($5)
addu	$15,$15,$2
lbu	$2,3($5)
lbu	$3,4($5)
sll	$18,$15,2
lbu	$16,4($4)
addu	$17,$14,$17
lbu	$24,5($4)
addu	$18,$18,$15
lbu	$15,6($4)
lbu	$14,7($4)
subu	$17,$17,$18
mtlo	$17
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$25,$25,$2
addiu	$25,$25,1
sra	$25,$25,1
sb	$25,3($4)
lbu	$2,6($5)
lbu	$18,3($5)
lbu	$17,2($5)
lbu	$25,7($5)
addu	$18,$18,$2
lbu	$2,4($5)
lbu	$3,5($5)
sll	$19,$18,2
addu	$25,$17,$25
addu	$18,$19,$18
subu	$25,$25,$18
mtlo	$25
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$16,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,4($4)
lbu	$2,7($5)
lbu	$17,4($5)
lbu	$25,3($5)
lbu	$16,8($5)
addu	$17,$17,$2
lbu	$2,5($5)
lbu	$3,6($5)
sll	$18,$17,2
addu	$16,$25,$16
addu	$17,$18,$17
subu	$16,$16,$17
mtlo	$16
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$24,$24,$2
addiu	$24,$24,1
sra	$24,$24,1
sb	$24,5($4)
lbu	$2,8($5)
lbu	$17,5($5)
lbu	$24,4($5)
lbu	$16,9($5)
addu	$17,$17,$2
lbu	$2,6($5)
lbu	$3,7($5)
sll	$18,$17,2
addu	$16,$24,$16
addu	$17,$18,$17
subu	$16,$16,$17
mtlo	$16
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$15,$15,$2
addiu	$15,$15,1
sra	$15,$15,1
sb	$15,6($4)
lbu	$2,5($5)
lbu	$15,10($5)
lbu	$16,6($5)
lbu	$17,9($5)
addu	$15,$2,$15
lbu	$2,7($5)
lbu	$3,8($5)
addu	$5,$5,$7
addu	$16,$16,$17
sll	$17,$16,2
addu	$16,$17,$16
subu	$15,$15,$16
mtlo	$15
madd	$11,$2
madd	$13,$3
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$10
addu	$2,$9,$2
lbu	$2,0($2)
addu	$14,$14,$2
addiu	$14,$14,1
sra	$14,$14,1
sb	$14,7($4)
bne	$12,$0,$L121
addu	$4,$4,$6

lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	avg_rv40_qpel8_h_lowpass.constprop.3
.size	avg_rv40_qpel8_h_lowpass.constprop.3, .-avg_rv40_qpel8_h_lowpass.constprop.3
.section	.text.avg_rv40_qpel16_h_lowpass.constprop.0,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_h_lowpass.constprop.0
.type	avg_rv40_qpel16_h_lowpass.constprop.0, @function
avg_rv40_qpel16_h_lowpass.constprop.0:
.frame	$sp,80,$31		# vars= 0, regs= 9/0, args= 32, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-80
sw	$16,44($sp)
sw	$23,72($sp)
sw	$22,68($sp)
sw	$20,60($sp)
lw	$16,%got(avg_rv40_qpel8_h_lowpass.constprop.3)($28)
lw	$23,96($sp)
lw	$22,100($sp)
lw	$20,104($sp)
addiu	$16,$16,%lo(avg_rv40_qpel8_h_lowpass.constprop.3)
.cprestore	32
sw	$31,76($sp)
move	$25,$16
sw	$21,64($sp)
move	$21,$6
sw	$19,56($sp)
move	$19,$7
sw	$18,52($sp)
move	$18,$4
sw	$17,48($sp)
move	$17,$5
sw	$23,16($sp)
sw	$22,20($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jalr	$25
sw	$20,24($sp)

addiu	$4,$18,8
addiu	$5,$17,8
sw	$23,16($sp)
move	$6,$21
sw	$22,20($sp)
move	$7,$19
move	$25,$16
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jalr	$25
sw	$20,24($sp)

sll	$3,$19,3
sll	$2,$21,3
sw	$23,16($sp)
addu	$17,$17,$3
sw	$22,20($sp)
addu	$18,$18,$2
sw	$20,24($sp)
move	$5,$17
move	$4,$18
move	$6,$21
move	$25,$16
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jalr	$25
move	$7,$19

addiu	$4,$18,8
lw	$28,32($sp)
addiu	$5,$17,8
lw	$31,76($sp)
move	$6,$21
lw	$18,52($sp)
move	$7,$19
lw	$21,64($sp)
move	$25,$16
lw	$19,56($sp)
lw	$17,48($sp)
lw	$16,44($sp)
sw	$23,96($sp)
sw	$22,100($sp)
sw	$20,104($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$20,60($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jr	$25
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	avg_rv40_qpel16_h_lowpass.constprop.0
.size	avg_rv40_qpel16_h_lowpass.constprop.0, .-avg_rv40_qpel16_h_lowpass.constprop.0
.section	.text.avg_rv40_qpel16_mc30_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc30_c
.type	avg_rv40_qpel16_mc30_c, @function
avg_rv40_qpel16_mc30_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel16_h_lowpass.constprop.0)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(avg_rv40_qpel16_h_lowpass.constprop.0)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_h_lowpass.constprop.0
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc30_c
.size	avg_rv40_qpel16_mc30_c, .-avg_rv40_qpel16_mc30_c
.section	.text.avg_rv40_qpel16_mc20_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc20_c
.type	avg_rv40_qpel16_mc20_c, @function
avg_rv40_qpel16_mc20_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel16_h_lowpass.constprop.0)($28)
addiu	$sp,$sp,-48
sw	$2,16($sp)
addiu	$25,$25,%lo(avg_rv40_qpel16_h_lowpass.constprop.0)
sw	$2,20($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
.cprestore	32
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_h_lowpass.constprop.0
1:	jalr	$25
move	$7,$6

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc20_c
.size	avg_rv40_qpel16_mc20_c, .-avg_rv40_qpel16_mc20_c
.section	.text.avg_rv40_qpel16_mc10_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc10_c
.type	avg_rv40_qpel16_mc10_c, @function
avg_rv40_qpel16_mc10_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,52			# 0x34
lw	$25,%got(avg_rv40_qpel16_h_lowpass.constprop.0)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(avg_rv40_qpel16_h_lowpass.constprop.0)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel16_h_lowpass.constprop.0
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel16_mc10_c
.size	avg_rv40_qpel16_mc10_c, .-avg_rv40_qpel16_mc10_c
.section	.text.avg_rv40_qpel8_mc30_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc30_c
.type	avg_rv40_qpel8_mc30_c, @function
avg_rv40_qpel8_mc30_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel8_h_lowpass.constprop.3)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(avg_rv40_qpel8_h_lowpass.constprop.3)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc30_c
.size	avg_rv40_qpel8_mc30_c, .-avg_rv40_qpel8_mc30_c
.section	.text.avg_rv40_qpel8_mc20_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc20_c
.type	avg_rv40_qpel8_mc20_c, @function
avg_rv40_qpel8_mc20_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(avg_rv40_qpel8_h_lowpass.constprop.3)($28)
addiu	$sp,$sp,-48
sw	$2,16($sp)
addiu	$25,$25,%lo(avg_rv40_qpel8_h_lowpass.constprop.3)
sw	$2,20($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
.cprestore	32
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jalr	$25
move	$7,$6

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc20_c
.size	avg_rv40_qpel8_mc20_c, .-avg_rv40_qpel8_mc20_c
.section	.text.avg_rv40_qpel8_mc10_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc10_c
.type	avg_rv40_qpel8_mc10_c, @function
avg_rv40_qpel8_mc10_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,52			# 0x34
lw	$25,%got(avg_rv40_qpel8_h_lowpass.constprop.3)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(avg_rv40_qpel8_h_lowpass.constprop.3)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,avg_rv40_qpel8_h_lowpass.constprop.3
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_rv40_qpel8_mc10_c
.size	avg_rv40_qpel8_mc10_c, .-avg_rv40_qpel8_mc10_c
.section	.text.put_rv40_qpel8_v_lowpass.constprop.5,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_v_lowpass.constprop.5
.type	put_rv40_qpel8_v_lowpass.constprop.5, @function
put_rv40_qpel8_v_lowpass.constprop.5:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-64
subu	$3,$0,$7
sw	$19,40($sp)
sw	$18,36($sp)
li	$18,1			# 0x1
sw	$17,32($sp)
sll	$3,$3,1
lw	$19,88($sp)
lw	$17,%got(ff_cropTbl)($28)
sw	$21,48($sp)
addiu	$2,$19,-1
sw	$20,44($sp)
sw	$23,56($sp)
addiu	$17,$17,1024
sll	$18,$18,$2
sw	$22,52($sp)
addiu	$2,$5,8
.cprestore	0
sw	$fp,60($sp)
move	$22,$5
sw	$16,28($sp)
move	$23,$4
sw	$3,20($sp)
sw	$2,16($sp)
lw	$21,80($sp)
lw	$20,84($sp)
$L139:
lw	$5,20($sp)
addu	$2,$22,$7
addu	$24,$23,$6
lbu	$9,0($22)
addu	$4,$2,$7
addu	$3,$22,$5
lbu	$15,0($2)
addu	$8,$24,$6
addu	$5,$3,$7
lbu	$13,0($4)
addu	$25,$8,$6
lbu	$3,0($3)
addu	$2,$4,$7
sw	$8,8($sp)
lbu	$5,0($5)
addiu	$22,$22,1
sw	$25,12($sp)
addu	$4,$2,$7
lbu	$2,0($2)
addiu	$23,$23,1
addu	$25,$5,$13
lbu	$12,0($4)
addu	$4,$4,$7
sll	$8,$25,2
addu	$3,$3,$2
addu	$25,$8,$25
lbu	$11,0($4)
addu	$10,$9,$2
subu	$25,$3,$25
sll	$8,$10,2
mtlo	$25
addu	$3,$5,$12
addu	$5,$8,$10
addu	$10,$15,$12
subu	$5,$3,$5
sll	$3,$10,2
addu	$4,$4,$7
madd	$9,$21
madd	$15,$20
addu	$9,$9,$11
lbu	$8,0($4)
addu	$3,$3,$10
addu	$16,$13,$11
mflo	$25
mtlo	$5
subu	$3,$9,$3
sll	$9,$16,2
addu	$4,$4,$7
addu	$9,$9,$16
addu	$25,$25,$18
lbu	$14,0($4)
madd	$15,$21
madd	$13,$20
addu	$15,$15,$8
sra	$25,$25,$19
subu	$9,$15,$9
mflo	$5
mtlo	$3
addu	$25,$17,$25
addu	$4,$4,$7
lbu	$fp,0($25)
addu	$25,$2,$8
lbu	$10,0($4)
addu	$4,$4,$7
addu	$5,$5,$18
madd	$13,$21
madd	$2,$20
lbu	$16,0($4)
sra	$5,$5,$19
addu	$13,$13,$14
addu	$15,$17,$5
mflo	$3
mtlo	$9
sll	$5,$25,2
addu	$4,$4,$7
addu	$25,$5,$25
subu	$13,$13,$25
lbu	$4,0($4)
sb	$fp,-1($23)
addu	$3,$3,$18
madd	$2,$21
lbu	$fp,0($15)
madd	$12,$20
addu	$15,$12,$14
addu	$2,$2,$10
sb	$fp,0($24)
sll	$24,$15,2
mflo	$9
mtlo	$13
lw	$13,8($sp)
addu	$15,$24,$15
sra	$3,$3,$19
subu	$15,$2,$15
addu	$3,$17,$3
addu	$9,$9,$18
madd	$12,$21
lbu	$fp,0($3)
madd	$11,$20
addu	$3,$11,$10
addu	$12,$12,$16
sb	$fp,0($13)
sll	$fp,$3,2
mflo	$5
lw	$13,12($sp)
mtlo	$15
addu	$3,$fp,$3
addu	$16,$8,$16
subu	$3,$12,$3
sll	$2,$16,2
sra	$9,$9,$19
addu	$16,$2,$16
madd	$11,$21
madd	$8,$20
addu	$11,$11,$4
addu	$9,$17,$9
subu	$11,$11,$16
mflo	$15
mtlo	$3
lbu	$9,0($9)
addu	$5,$5,$18
addu	$2,$13,$6
sra	$5,$5,$19
sb	$9,0($13)
addu	$5,$17,$5
addu	$15,$15,$18
madd	$8,$21
madd	$14,$20
lbu	$5,0($5)
sra	$15,$15,$19
addu	$15,$17,$15
mflo	$3
sb	$5,0($2)
mtlo	$11
lbu	$4,0($15)
addu	$2,$2,$6
sb	$4,0($2)
addu	$2,$2,$6
addu	$3,$3,$18
madd	$14,$21
madd	$10,$20
sra	$3,$3,$19
addu	$3,$17,$3
mflo	$10
lbu	$4,0($3)
addu	$3,$2,$6
sb	$4,0($2)
addu	$10,$10,$18
sra	$10,$10,$19
addu	$10,$17,$10
lbu	$2,0($10)
sb	$2,0($3)
lw	$8,16($sp)
bne	$22,$8,$L139
lw	$fp,60($sp)

lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_rv40_qpel8_v_lowpass.constprop.5
.size	put_rv40_qpel8_v_lowpass.constprop.5, .-put_rv40_qpel8_v_lowpass.constprop.5
.section	.text.put_rv40_qpel16_v_lowpass.constprop.4,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_v_lowpass.constprop.4
.type	put_rv40_qpel16_v_lowpass.constprop.4, @function
put_rv40_qpel16_v_lowpass.constprop.4:
.frame	$sp,80,$31		# vars= 0, regs= 9/0, args= 32, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-80
sw	$16,44($sp)
sw	$23,72($sp)
sw	$22,68($sp)
sw	$20,60($sp)
lw	$16,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
lw	$23,96($sp)
lw	$22,100($sp)
lw	$20,104($sp)
addiu	$16,$16,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.cprestore	32
sw	$31,76($sp)
move	$25,$16
sw	$21,64($sp)
move	$21,$6
sw	$19,56($sp)
move	$19,$7
sw	$18,52($sp)
move	$18,$4
sw	$17,48($sp)
move	$17,$5
sw	$23,16($sp)
sw	$22,20($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$20,24($sp)

addiu	$4,$18,8
addiu	$5,$17,8
sw	$23,16($sp)
move	$6,$21
sw	$22,20($sp)
move	$7,$19
move	$25,$16
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$20,24($sp)

sll	$3,$19,3
sll	$2,$21,3
sw	$23,16($sp)
addu	$17,$17,$3
sw	$22,20($sp)
addu	$18,$18,$2
sw	$20,24($sp)
move	$5,$17
move	$4,$18
move	$6,$21
move	$25,$16
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
move	$7,$19

addiu	$4,$18,8
lw	$28,32($sp)
addiu	$5,$17,8
lw	$31,76($sp)
move	$6,$21
lw	$18,52($sp)
move	$7,$19
lw	$21,64($sp)
move	$25,$16
lw	$19,56($sp)
lw	$17,48($sp)
lw	$16,44($sp)
sw	$23,96($sp)
sw	$22,100($sp)
sw	$20,104($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$20,60($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jr	$25
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	put_rv40_qpel16_v_lowpass.constprop.4
.size	put_rv40_qpel16_v_lowpass.constprop.4, .-put_rv40_qpel16_v_lowpass.constprop.4
.section	.text.put_rv40_qpel16_mc23_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc23_c
.type	put_rv40_qpel16_mc23_c, @function
put_rv40_qpel16_mc23_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-392
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$17,380($sp)
li	$3,21			# 0x15
li	$17,20			# 0x14
sw	$18,384($sp)
subu	$5,$5,$2
sw	$16,376($sp)
li	$2,5			# 0x5
sw	$31,388($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,16			# 0x10
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,16($sp)
sw	$2,20($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
move	$6,$16
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$2,24($sp)

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc23_c
.size	put_rv40_qpel16_mc23_c, .-put_rv40_qpel16_mc23_c
.section	.text.put_rv40_qpel16_mc13_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc13_c
.type	put_rv40_qpel16_mc13_c, @function
put_rv40_qpel16_mc13_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$19,388($sp)
li	$19,20			# 0x14
sw	$18,384($sp)
subu	$5,$5,$2
sw	$17,380($sp)
li	$2,21			# 0x15
li	$18,52			# 0x34
sw	$20,392($sp)
li	$17,6			# 0x6
sw	$16,376($sp)
move	$20,$4
sw	$31,396($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$17,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc13_c
.size	put_rv40_qpel16_mc13_c, .-put_rv40_qpel16_mc13_c
.section	.text.put_rv40_qpel16_mc03_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc03_c
.type	put_rv40_qpel16_mc03_c, @function
put_rv40_qpel16_mc03_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc03_c
.size	put_rv40_qpel16_mc03_c, .-put_rv40_qpel16_mc03_c
.section	.text.put_rv40_qpel16_mc32_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc32_c
.type	put_rv40_qpel16_mc32_c, @function
put_rv40_qpel16_mc32_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,21			# 0x15
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-392
sll	$2,$6,1
sw	$17,380($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,384($sp)
li	$2,6			# 0x6
sw	$16,376($sp)
move	$18,$4
sw	$31,388($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,24($sp)
li	$6,16			# 0x10
sw	$17,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
move	$6,$16

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc32_c
.size	put_rv40_qpel16_mc32_c, .-put_rv40_qpel16_mc32_c
.section	.text.put_rv40_qpel16_mc22_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc22_c
.type	put_rv40_qpel16_mc22_c, @function
put_rv40_qpel16_mc22_c:
.frame	$sp,400,$31		# vars= 336, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$18,388($sp)
li	$18,5			# 0x5
sw	$16,380($sp)
subu	$5,$5,$2
li	$16,20			# 0x14
sw	$19,392($sp)
li	$2,21			# 0x15
sw	$17,384($sp)
move	$19,$4
sw	$31,396($sp)
move	$17,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$16,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$16,24($sp)
sw	$18,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$17

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$16,16($sp)
move	$4,$19
sw	$16,20($sp)
move	$6,$17
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$18,24($sp)

lw	$31,396($sp)
lw	$19,392($sp)
lw	$18,388($sp)
lw	$17,384($sp)
lw	$16,380($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc22_c
.size	put_rv40_qpel16_mc22_c, .-put_rv40_qpel16_mc22_c
.section	.text.put_rv40_qpel16_mc12_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc12_c
.type	put_rv40_qpel16_mc12_c, @function
put_rv40_qpel16_mc12_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,21			# 0x15
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
addiu	$sp,$sp,-392
sll	$2,$6,1
sw	$17,380($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,384($sp)
li	$2,6			# 0x6
sw	$16,376($sp)
move	$18,$4
sw	$31,388($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,20($sp)
li	$6,16			# 0x10
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
move	$6,$16

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc12_c
.size	put_rv40_qpel16_mc12_c, .-put_rv40_qpel16_mc12_c
.section	.text.put_rv40_qpel16_mc02_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc02_c
.type	put_rv40_qpel16_mc02_c, @function
put_rv40_qpel16_mc02_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$sp,$sp,-48
sw	$2,16($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
sw	$2,20($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
.cprestore	32
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
move	$7,$6

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc02_c
.size	put_rv40_qpel16_mc02_c, .-put_rv40_qpel16_mc02_c
.section	.text.put_rv40_qpel16_mc31_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc31_c
.type	put_rv40_qpel16_mc31_c, @function
put_rv40_qpel16_mc31_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$19,388($sp)
li	$19,52			# 0x34
sw	$18,384($sp)
subu	$5,$5,$2
sw	$17,380($sp)
li	$2,21			# 0x15
li	$18,20			# 0x14
sw	$20,392($sp)
li	$17,6			# 0x6
sw	$16,376($sp)
move	$20,$4
sw	$31,396($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$17,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc31_c
.size	put_rv40_qpel16_mc31_c, .-put_rv40_qpel16_mc31_c
.section	.text.put_rv40_qpel16_mc21_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc21_c
.type	put_rv40_qpel16_mc21_c, @function
put_rv40_qpel16_mc21_c:
.frame	$sp,392,$31		# vars= 336, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-392
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$17,380($sp)
li	$3,21			# 0x15
li	$17,20			# 0x14
sw	$18,384($sp)
subu	$5,$5,$2
sw	$16,376($sp)
li	$2,5			# 0x5
sw	$31,388($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,16			# 0x10
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,72
li	$7,16			# 0x10
sw	$17,20($sp)
sw	$2,16($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
move	$6,$16
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$2,24($sp)

lw	$31,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,392

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc21_c
.size	put_rv40_qpel16_mc21_c, .-put_rv40_qpel16_mc21_c
.section	.text.put_rv40_qpel16_mc11_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc11_c
.type	put_rv40_qpel16_mc11_c, @function
put_rv40_qpel16_mc11_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-400
lw	$25,%got(put_rv40_qpel16_h_lowpass)($28)
sll	$2,$6,1
sw	$19,388($sp)
li	$19,52			# 0x34
sw	$18,384($sp)
subu	$5,$5,$2
sw	$17,380($sp)
li	$2,21			# 0x15
li	$18,20			# 0x14
sw	$20,392($sp)
li	$17,6			# 0x6
sw	$16,376($sp)
move	$20,$4
sw	$31,396($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,16			# 0x10
sw	$19,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel16_h_lowpass)
sw	$18,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,72
lw	$28,32($sp)
li	$7,16			# 0x10
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$17,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc11_c
.size	put_rv40_qpel16_mc11_c, .-put_rv40_qpel16_mc11_c
.section	.text.put_rv40_qpel16_mc01_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc01_c
.type	put_rv40_qpel16_mc01_c, @function
put_rv40_qpel16_mc01_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,52			# 0x34
lw	$25,%got(put_rv40_qpel16_v_lowpass.constprop.4)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(put_rv40_qpel16_v_lowpass.constprop.4)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel16_v_lowpass.constprop.4
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel16_mc01_c
.size	put_rv40_qpel16_mc01_c, .-put_rv40_qpel16_mc01_c
.section	.text.put_rv40_qpel8_mc23_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc23_c
.type	put_rv40_qpel8_mc23_c, @function
put_rv40_qpel8_mc23_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-160
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$17,148($sp)
li	$3,13			# 0xd
li	$17,20			# 0x14
sw	$18,152($sp)
subu	$5,$5,$2
sw	$16,144($sp)
li	$2,5			# 0x5
sw	$31,156($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,8			# 0x8
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,16($sp)
sw	$2,20($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
move	$6,$16
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$2,24($sp)

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc23_c
.size	put_rv40_qpel8_mc23_c, .-put_rv40_qpel8_mc23_c
.section	.text.put_rv40_qpel8_mc13_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc13_c
.type	put_rv40_qpel8_mc13_c, @function
put_rv40_qpel8_mc13_c:
.frame	$sp,168,$31		# vars= 104, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$19,156($sp)
li	$19,20			# 0x14
sw	$18,152($sp)
subu	$5,$5,$2
sw	$17,148($sp)
li	$2,13			# 0xd
li	$18,52			# 0x34
sw	$20,160($sp)
li	$17,6			# 0x6
sw	$16,144($sp)
move	$20,$4
sw	$31,164($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$17,24($sp)

lw	$31,164($sp)
lw	$20,160($sp)
lw	$19,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc13_c
.size	put_rv40_qpel8_mc13_c, .-put_rv40_qpel8_mc13_c
.section	.text.put_rv40_qpel8_mc03_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc03_c
.type	put_rv40_qpel8_mc03_c, @function
put_rv40_qpel8_mc03_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,52			# 0x34
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc03_c
.size	put_rv40_qpel8_mc03_c, .-put_rv40_qpel8_mc03_c
.section	.text.put_rv40_qpel8_mc32_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc32_c
.type	put_rv40_qpel8_mc32_c, @function
put_rv40_qpel8_mc32_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,13			# 0xd
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-160
sll	$2,$6,1
sw	$17,148($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,152($sp)
li	$2,6			# 0x6
sw	$16,144($sp)
move	$18,$4
sw	$31,156($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,24($sp)
li	$6,8			# 0x8
sw	$17,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
move	$6,$16

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc32_c
.size	put_rv40_qpel8_mc32_c, .-put_rv40_qpel8_mc32_c
.section	.text.put_rv40_qpel8_mc22_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc22_c
.type	put_rv40_qpel8_mc22_c, @function
put_rv40_qpel8_mc22_c:
.frame	$sp,168,$31		# vars= 104, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$18,156($sp)
li	$18,5			# 0x5
sw	$16,148($sp)
subu	$5,$5,$2
li	$16,20			# 0x14
sw	$19,160($sp)
li	$2,13			# 0xd
sw	$17,152($sp)
move	$19,$4
sw	$31,164($sp)
move	$17,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$16,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$16,24($sp)
sw	$18,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$17

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$16,16($sp)
move	$4,$19
sw	$16,20($sp)
move	$6,$17
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$18,24($sp)

lw	$31,164($sp)
lw	$19,160($sp)
lw	$18,156($sp)
lw	$17,152($sp)
lw	$16,148($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc22_c
.size	put_rv40_qpel8_mc22_c, .-put_rv40_qpel8_mc22_c
.section	.text.put_rv40_qpel8_mc12_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc12_c
.type	put_rv40_qpel8_mc12_c, @function
put_rv40_qpel8_mc12_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,13			# 0xd
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
addiu	$sp,$sp,-160
sll	$2,$6,1
sw	$17,148($sp)
li	$17,20			# 0x14
sw	$3,16($sp)
subu	$5,$5,$2
li	$3,52			# 0x34
sw	$18,152($sp)
li	$2,6			# 0x6
sw	$16,144($sp)
move	$18,$4
sw	$31,156($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$3,20($sp)
li	$6,8			# 0x8
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,5			# 0x5
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,16($sp)
sw	$17,20($sp)
move	$4,$18
sw	$2,24($sp)
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
move	$6,$16

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc12_c
.size	put_rv40_qpel8_mc12_c, .-put_rv40_qpel8_mc12_c
.section	.text.put_rv40_qpel8_mc02_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc02_c
.type	put_rv40_qpel8_mc02_c, @function
put_rv40_qpel8_mc02_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,20			# 0x14
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$sp,$sp,-48
sw	$2,16($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
sw	$2,20($sp)
li	$2,5			# 0x5
sw	$31,44($sp)
.cprestore	32
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
move	$7,$6

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc02_c
.size	put_rv40_qpel8_mc02_c, .-put_rv40_qpel8_mc02_c
.section	.text.put_rv40_qpel8_mc31_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc31_c
.type	put_rv40_qpel8_mc31_c, @function
put_rv40_qpel8_mc31_c:
.frame	$sp,168,$31		# vars= 104, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$19,156($sp)
li	$19,52			# 0x34
sw	$18,152($sp)
subu	$5,$5,$2
sw	$17,148($sp)
li	$2,13			# 0xd
li	$18,20			# 0x14
sw	$20,160($sp)
li	$17,6			# 0x6
sw	$16,144($sp)
move	$20,$4
sw	$31,164($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$18,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$19,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$17,24($sp)

lw	$31,164($sp)
lw	$20,160($sp)
lw	$19,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc31_c
.size	put_rv40_qpel8_mc31_c, .-put_rv40_qpel8_mc31_c
.section	.text.put_rv40_qpel8_mc21_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc21_c
.type	put_rv40_qpel8_mc21_c, @function
put_rv40_qpel8_mc21_c:
.frame	$sp,160,$31		# vars= 104, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-160
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$17,148($sp)
li	$3,13			# 0xd
li	$17,20			# 0x14
sw	$18,152($sp)
subu	$5,$5,$2
sw	$16,144($sp)
li	$2,5			# 0x5
sw	$31,156($sp)
move	$16,$6
.cprestore	32
move	$18,$4
sw	$3,16($sp)
addiu	$4,$sp,40
sw	$17,20($sp)
li	$6,8			# 0x8
sw	$17,24($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$2,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

li	$2,52			# 0x34
lw	$28,32($sp)
addiu	$5,$sp,56
li	$7,8			# 0x8
sw	$17,20($sp)
sw	$2,16($sp)
li	$2,6			# 0x6
move	$4,$18
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
move	$6,$16
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$2,24($sp)

lw	$31,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,160

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc21_c
.size	put_rv40_qpel8_mc21_c, .-put_rv40_qpel8_mc21_c
.section	.text.put_rv40_qpel8_mc11_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc11_c
.type	put_rv40_qpel8_mc11_c, @function
put_rv40_qpel8_mc11_c:
.frame	$sp,168,$31		# vars= 104, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
lw	$25,%got(put_rv40_qpel8_h_lowpass)($28)
sll	$2,$6,1
sw	$19,156($sp)
li	$19,52			# 0x34
sw	$18,152($sp)
subu	$5,$5,$2
sw	$17,148($sp)
li	$2,13			# 0xd
li	$18,20			# 0x14
sw	$20,160($sp)
li	$17,6			# 0x6
sw	$16,144($sp)
move	$20,$4
sw	$31,164($sp)
move	$16,$6
.cprestore	32
addiu	$4,$sp,40
sw	$2,16($sp)
li	$6,8			# 0x8
sw	$19,20($sp)
addiu	$25,$25,%lo(put_rv40_qpel8_h_lowpass)
sw	$18,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_h_lowpass
1:	jalr	$25
move	$7,$16

addiu	$5,$sp,56
lw	$28,32($sp)
li	$7,8			# 0x8
sw	$19,16($sp)
move	$4,$20
sw	$18,20($sp)
move	$6,$16
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$17,24($sp)

lw	$31,164($sp)
lw	$20,160($sp)
lw	$19,156($sp)
lw	$18,152($sp)
lw	$17,148($sp)
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,168

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc11_c
.size	put_rv40_qpel8_mc11_c, .-put_rv40_qpel8_mc11_c
.section	.text.put_rv40_qpel8_mc01_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc01_c
.type	put_rv40_qpel8_mc01_c, @function
put_rv40_qpel8_mc01_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,52			# 0x34
lw	$25,%got(put_rv40_qpel8_v_lowpass.constprop.5)($28)
addiu	$sp,$sp,-48
move	$7,$6
sw	$2,16($sp)
li	$2,20			# 0x14
addiu	$25,$25,%lo(put_rv40_qpel8_v_lowpass.constprop.5)
sw	$31,44($sp)
.cprestore	32
sw	$2,20($sp)
li	$2,6			# 0x6
.reloc	1f,R_MIPS_JALR,put_rv40_qpel8_v_lowpass.constprop.5
1:	jalr	$25
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_rv40_qpel8_mc01_c
.size	put_rv40_qpel8_mc01_c, .-put_rv40_qpel8_mc01_c
.section	.text.ff_rv40dsp_init,"ax",@progbits
.align	2
.align	5
.globl	ff_rv40dsp_init
.set	nomips16
.set	nomicromips
.ent	ff_rv40dsp_init
.type	ff_rv40dsp_init, @function
ff_rv40dsp_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,1488($4)
lw	$5,1744($4)
lw	$3,1552($4)
sw	$2,3572($4)
lw	$2,%got(put_rv40_qpel16_mc10_c)($28)
sw	$5,3828($4)
addiu	$2,$2,%lo(put_rv40_qpel16_mc10_c)
sw	$2,3576($4)
lw	$2,%got(put_rv40_qpel16_mc20_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc20_c)
sw	$2,3580($4)
lw	$2,%got(put_rv40_qpel16_mc30_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc30_c)
sw	$2,3584($4)
lw	$2,%got(put_rv40_qpel16_mc01_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc01_c)
sw	$2,3588($4)
lw	$2,%got(put_rv40_qpel16_mc11_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc11_c)
sw	$2,3592($4)
lw	$2,%got(put_rv40_qpel16_mc21_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc21_c)
sw	$2,3596($4)
lw	$2,%got(put_rv40_qpel16_mc31_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc31_c)
sw	$2,3600($4)
lw	$2,%got(put_rv40_qpel16_mc02_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc02_c)
sw	$2,3604($4)
lw	$2,%got(put_rv40_qpel16_mc12_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc12_c)
sw	$2,3608($4)
lw	$2,%got(put_rv40_qpel16_mc22_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc22_c)
sw	$2,3612($4)
lw	$2,%got(put_rv40_qpel16_mc32_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc32_c)
sw	$2,3616($4)
lw	$2,%got(put_rv40_qpel16_mc03_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc03_c)
sw	$2,3620($4)
lw	$2,%got(put_rv40_qpel16_mc13_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc13_c)
sw	$2,3624($4)
lw	$2,%got(put_rv40_qpel16_mc23_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel16_mc23_c)
sw	$2,3628($4)
lw	$2,%got(avg_rv40_qpel16_mc10_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc10_c)
sw	$2,3832($4)
lw	$2,%got(avg_rv40_qpel16_mc20_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc20_c)
sw	$2,3836($4)
lw	$2,%got(avg_rv40_qpel16_mc30_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc30_c)
sw	$2,3840($4)
lw	$2,%got(avg_rv40_qpel16_mc01_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc01_c)
sw	$2,3844($4)
lw	$2,%got(avg_rv40_qpel16_mc11_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc11_c)
sw	$2,3848($4)
lw	$2,%got(avg_rv40_qpel16_mc21_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc21_c)
sw	$2,3852($4)
lw	$2,%got(avg_rv40_qpel16_mc31_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc31_c)
sw	$2,3856($4)
lw	$2,%got(avg_rv40_qpel16_mc02_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc02_c)
sw	$2,3860($4)
lw	$2,%got(avg_rv40_qpel16_mc12_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc12_c)
sw	$2,3864($4)
lw	$2,%got(avg_rv40_qpel16_mc22_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc22_c)
sw	$2,3868($4)
lw	$2,%got(avg_rv40_qpel16_mc32_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc32_c)
sw	$2,3872($4)
lw	$2,%got(avg_rv40_qpel16_mc03_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc03_c)
sw	$2,3876($4)
lw	$2,%got(avg_rv40_qpel16_mc13_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc13_c)
sw	$2,3880($4)
lw	$2,%got(avg_rv40_qpel16_mc23_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc23_c)
sw	$2,3884($4)
lw	$2,%got(put_rv40_qpel8_mc10_c)($28)
sw	$3,3636($4)
addiu	$2,$2,%lo(put_rv40_qpel8_mc10_c)
sw	$2,3640($4)
lw	$2,%got(put_rv40_qpel8_mc20_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc20_c)
sw	$2,3644($4)
lw	$2,%got(put_rv40_qpel8_mc30_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc30_c)
sw	$2,3648($4)
lw	$2,%got(put_rv40_qpel8_mc01_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc01_c)
sw	$2,3652($4)
lw	$2,%got(put_rv40_qpel8_mc11_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc11_c)
sw	$2,3656($4)
lw	$2,%got(put_rv40_qpel8_mc21_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc21_c)
sw	$2,3660($4)
lw	$2,%got(put_rv40_qpel8_mc31_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc31_c)
sw	$2,3664($4)
lw	$2,%got(put_rv40_qpel8_mc02_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc02_c)
sw	$2,3668($4)
lw	$2,%got(put_rv40_qpel8_mc12_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc12_c)
sw	$2,3672($4)
lw	$2,%got(put_rv40_qpel8_mc22_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc22_c)
sw	$2,3676($4)
lw	$2,%got(put_rv40_qpel8_mc32_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc32_c)
sw	$2,3680($4)
lw	$2,%got(put_rv40_qpel8_mc03_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc03_c)
sw	$2,3684($4)
lw	$2,%got(put_rv40_qpel8_mc13_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc13_c)
sw	$2,3688($4)
lw	$2,%got(put_rv40_qpel8_mc23_c)($28)
addiu	$2,$2,%lo(put_rv40_qpel8_mc23_c)
sw	$2,3692($4)
lw	$2,%got(avg_rv40_qpel8_mc10_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc10_c)
sw	$2,3896($4)
lw	$2,%got(avg_rv40_qpel8_mc20_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc20_c)
sw	$2,3900($4)
lw	$2,%got(avg_rv40_qpel8_mc30_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc30_c)
sw	$2,3904($4)
lw	$2,%got(avg_rv40_qpel8_mc01_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc01_c)
sw	$2,3908($4)
lw	$2,%got(avg_rv40_qpel8_mc11_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc11_c)
sw	$2,3912($4)
lw	$2,%got(avg_rv40_qpel8_mc21_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc21_c)
sw	$2,3916($4)
lw	$2,%got(avg_rv40_qpel8_mc31_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc31_c)
sw	$2,3920($4)
lw	$2,%got(avg_rv40_qpel8_mc02_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc02_c)
sw	$2,3924($4)
lw	$2,%got(avg_rv40_qpel8_mc12_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc12_c)
sw	$2,3928($4)
lw	$2,%got(avg_rv40_qpel8_mc22_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc22_c)
sw	$2,3932($4)
lw	$2,%got(avg_rv40_qpel8_mc32_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc32_c)
sw	$2,3936($4)
lw	$2,%got(avg_rv40_qpel8_mc03_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc03_c)
sw	$2,3940($4)
lw	$2,%got(avg_rv40_qpel8_mc13_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc13_c)
sw	$2,3944($4)
lw	$2,%got(avg_rv40_qpel8_mc23_c)($28)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc23_c)
sw	$2,3948($4)
lw	$2,%got(put_rv40_chroma_mc8_c)($28)
addiu	$2,$2,%lo(put_rv40_chroma_mc8_c)
sw	$2,4084($4)
lw	$2,%got(put_rv40_chroma_mc4_c)($28)
addiu	$2,$2,%lo(put_rv40_chroma_mc4_c)
sw	$2,4088($4)
lw	$2,%got(avg_rv40_chroma_mc8_c)($28)
addiu	$2,$2,%lo(avg_rv40_chroma_mc8_c)
sw	$2,4096($4)
lw	$2,1808($4)
sw	$2,3892($4)
lw	$2,%got(avg_rv40_chroma_mc4_c)($28)
addiu	$2,$2,%lo(avg_rv40_chroma_mc4_c)
j	$31
sw	$2,4100($4)

.set	macro
.set	reorder
.end	ff_rv40dsp_init
.size	ff_rv40dsp_init, .-ff_rv40dsp_init
.rdata
.align	2
.type	rv40_bias, @object
.size	rv40_bias, 64
rv40_bias:
.word	0
.word	16
.word	32
.word	16
.word	32
.word	28
.word	32
.word	28
.word	0
.word	32
.word	16
.word	32
.word	32
.word	28
.word	32
.word	28
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
