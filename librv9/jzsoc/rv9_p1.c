#define P1_USE_PADDR

#include "../../libjzcommon/jzsys.h"
#include "../../libjzcommon/jzasm.h"
#include "../../libjzcommon/jzmedia.h"
#include "rv9_p1_type.h"
#include "../../libjzcommon/jz4760e_2ddma_hw.h"
#include "rv9_dcore.h"
#include "rv9_tcsm0.h"
#include "rv9_tcsm1.h"
#include "jz4760e_rv_dblk.h"
#include "../../libjzcommon/jz4760e_dcsc.h"
#include "t_vmau.h"
//#include "debug0.h"

#if 1
#define RV9_STOP_P1()						\
  ({								\
    ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=0;	\
    *((volatile int *)(TCSM0_PADDR(TCSM0_P1_TASK_DONE))) = 0x1;	\
    i_nop;							\
    i_nop;							\
    i_wait();							\
  })
#endif
//volatile int *dbg_ptr;
//volatile unsigned char intraup_y[1284];
//volatile unsigned char intraup_u[644];
//volatile unsigned char intraup_v[644];
int mc_flag=0,last_mc_flag=0;
#include "rv9_p1_mc.c"

#define __p1_text __attribute__ ((__section__ (".p1_text")))
#define __p1_main __attribute__ ((__section__ (".p1_main")))
#define __p1_data __attribute__ ((__section__ (".p1_data")))

__p1_main int main() {
  S32I2M(xr16, 0x3);
  uint8_t top_left_tmp[3];
  int *gp0_chain_ptr, *gp1_chain_ptr, *gp1_chain_ptr1;
  int i,j, k, count, blknum, blkoff, chain;
  RV9_MB_DecARGs *dMB,*dMB_L,*dMB_N,*dMB_NN;
  RV9_Slice_GlbARGs *dSlice, *dSlice_ex;
  int y_mb_line, uv_mb_line, tag3, lastline = 0;
  uint32_t src_ptr,dst_ptr,src_cptr,dst_cptr;
  RV9_Frame_LPtr *Lpicture;
  uint8_t *src1, *dst1, *src2, *dst2, *src3, *dst3, *src4, *dst4;
  RV9_XCH2_T *XCH2_T0, *XCH2_T1;
  VMAU_CHN_REG *mau_reg_ptr;
  mau_reg_ptr = VMAU_CHN_BASE;

#ifdef JZC_ROTA90_OPT
  int tag1, tag2;
  int rota_mx, rota_my, rota_up_mx, rota_up_my;
  uint8_t *rota_ydst, *rota_cdst, *rota_up_ydst;
  int rota_y_mb_line, rota_uv_mb_line;  
#endif

  uint8_t *dblk_upout_addr0 = TCSM1_DBLK_UPOUT_Y0;
  uint8_t *dblk_upout_addr1 = TCSM1_DBLK_UPOUT_Y1;

  *(volatile int*)DBLK_END_FLAG = DBLK_SYN_VALUE;
  *(volatile int*)TCSM1_MOTION_DSA = (0x80000000 | 0xFFFF);
  uint8_t *motion_dha = TCSM1_MOTION_DHA;
  uint8_t *next_motion_dha = TCSM1_MOTION_DHA+MC_CHN_ONELEN;
  uint8_t *motion_dsa = TCSM1_MOTION_DSA;

  int xchg_tmp; 
  volatile int *mbnum_wp=TCSM1_MBNUM_WP;
  int mbnum_rp;
  unsigned int addr_rp;

  unsigned char * recon_ptr0=RECON_PREVIOUS_YBUF0;
  unsigned char * recon_ptr1=RECON_PREVIOUS_YBUF1;
  unsigned char * recon_ptr2=RECON_PREVIOUS_YBUF2;

#undef JZC_ROTA90_OPT
  uint8_t *dblk_mb_ydst1 = TCSM1_DBLK_MBOUT_YDES;
  uint8_t *dblk_mb_ydst2 = TCSM1_DBLK_MBOUT_YDES1;
  uint8_t *dblk_mb_ydst3 = TCSM1_DBLK_MBOUT_YDES2;

  uint8_t *dblk_mb_udst1 = TCSM1_DBLK_MBOUT_UDES;
  uint8_t *dblk_mb_udst2 = TCSM1_DBLK_MBOUT_UDES1;
  uint8_t *dblk_mb_udst3 = TCSM1_DBLK_MBOUT_UDES2;

#ifdef JZC_ROTA90_OPT
  uint8_t *rota_mb_ybuf1 = TCSM1_ROTA_MB_YBUF1;
  uint8_t *rota_mb_ybuf2 = TCSM1_ROTA_MB_YBUF2;
  uint8_t *rota_mb_ybuf3 = TCSM1_ROTA_MB_YBUF3;
  uint8_t *rota_mb_cbuf1 = TCSM1_ROTA_MB_CBUF1;
  uint8_t *rota_mb_cbuf2 = TCSM1_ROTA_MB_CBUF2;
  uint8_t *rota_mb_cbuf3 = TCSM1_ROTA_MB_CBUF3;
  uint8_t *rota_upmb_ybuf1 = TCSM1_UP_ROTA_YBUF1;
  uint8_t *rota_upmb_ybuf2 = TCSM1_UP_ROTA_YBUF2;
#endif
  uint8_t *up_ydst;

  uint8_t  mb_x = 0;
  uint8_t  mb_x_d1=0, mb_x_d2=0, mb_x_d3=0;
  uint8_t  mb_y=0;
  uint8_t  mb_y_d1=0, mb_y_d2=0, mb_y_d3=0;

  volatile int *tcsm0_gp0_poll_end ,*tcsm0_gp1_poll_end;
  volatile int *tcsm1_gp0_poll_end ,*tcsm1_gp1_poll_end;

  tcsm1_gp0_poll_end  = (volatile int*)(TCSM1_GP0_POLL_END);
  *tcsm1_gp0_poll_end  = 0;
  tcsm0_gp0_poll_end  = (volatile int*)TCSM0_PADDR(TCSM0_GP0_POLL_END);
  *tcsm0_gp0_poll_end  = 0;

  tcsm1_gp1_poll_end  = (volatile int*)(TCSM1_GP1_POLL_END);
  *tcsm1_gp1_poll_end  = 0;
  tcsm0_gp1_poll_end  = (volatile int*)TCSM0_PADDR(TCSM0_GP1_POLL_END);
  *tcsm0_gp1_poll_end  = 0;

  last_mc_flag = 0;
  mc_flag = 0;
  //*(int*)DBLK_END_FLAG = DBLK_SYN_VALUE;

  mbnum_rp=0;
  addr_rp=TCSM0_PADDR(TCSM0_TASK_FIFO);

  dSlice    = (int*)TCSM1_SLICE_BUF0;
  dSlice_ex = (int*)TCSM1_SLICE_BUF1;

#ifdef JZC_DBLK_OPT
  int intra, inter16x16;
  XCH2_T0 = TCSM1_XCH2_T_BUF0;
  XCH2_T1 = TCSM1_XCH2_T_BUF1;
#endif

  dMB   = TASK_BUF1;//curr mb
  dMB_L = TASK_BUF0;
  dMB_N = TASK_BUF2;
  dMB_NN = TASK_BUF3;
  gp0_chain_ptr = DDMA_GP0_DES_CHAIN;
  gp1_chain_ptr = DDMA_GP1_DES_CHAIN;
  gp1_chain_ptr1= DDMA_GP1_DES_CHAIN1;

  gp1_chain_ptr[6]=GP_STRD(128,GP_FRM_NML,128);
  gp1_chain_ptr[7]=GP_UNIT(GP_TAG_LK,128,128);
  gp1_chain_ptr[10]=GP_STRD(64,GP_FRM_NML,64);
  gp1_chain_ptr[11]=GP_UNIT(GP_TAG_LK,64,64);

  gp1_chain_ptr1[6]=GP_STRD(128,GP_FRM_NML,128);
  gp1_chain_ptr1[7]=GP_UNIT(GP_TAG_LK,128,128);
  gp1_chain_ptr1[10]=GP_STRD(64,GP_FRM_NML,64);
  gp1_chain_ptr1[11]=GP_UNIT(GP_TAG_LK,64,64);

  gp0_chain_ptr[2]=GP_STRD(64,GP_FRM_NML,64);
  set_gp0_dha(TCSM1_PADDR(DDMA_GP0_DES_CHAIN));

  while(*mbnum_wp<=mbnum_rp+1);//wait until the first two mb is ready

  ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=addr_rp;
  gp0_chain_ptr[0]=addr_rp;
  gp0_chain_ptr[1]=TCSM1_PADDR(dMB);
  gp0_chain_ptr[3]=GP_UNIT(GP_TAG_UL,TASK_BUF_LEN,TASK_BUF_LEN);
  set_gp0_dcs();
#ifdef JZC_VMAU_OPT
  unsigned int tmp = ((REAL & MAU_VIDEO_MSK) << MAU_VIDEO_SFT);
  write_reg(VMAU_V_BASE+VMAU_SLV_GBL_RUN, VMAU_RESET);
  write_reg(VMAU_V_BASE+VMAU_SLV_VIDEO_TYPE, tmp);

  write_reg(VMAU_V_BASE+VMAU_SLV_DEC_STR,(VMAU_COUT_STR<<16)|VMAU_YOUT_STR);
  write_reg(VMAU_V_BASE+VMAU_SLV_Y_GS, dSlice->mb_width*16);
  write_reg(VMAU_V_BASE+VMAU_SLV_GBL_CTR, VMAU_CTRL_FIFO_M);

  *((volatile uint32_t *)VMAU_END_FLAG) = 1;
#endif
  poll_gp0_end();

  mbnum_rp++;
  addr_rp += TASK_BUF_LEN;

  gp0_chain_ptr[0]=addr_rp;
  gp0_chain_ptr[1]=TCSM1_PADDR(dMB_N);
  gp0_chain_ptr[3]=GP_UNIT(GP_TAG_UL,TASK_BUF_LEN,TASK_BUF_LEN);
  set_gp0_dcs();

  SET_REG1_DSA(TCSM1_PADDR((int)motion_dsa));
  if(dSlice->si_type){
    if(dMB->mbtype > RV34_MB_TYPE_INTRA16x16)
      {
	rv40_decode_mv_aux(dSlice, dMB, motion_dha);
	motion_dsa[0] = 0x0;
      }
  }

  if(mc_flag){
    SET_REG1_DDC(TCSM1_PADDR((int)motion_dha) | 0x1);
    last_mc_flag=mc_flag;
  }
  poll_gp0_end();

  mbnum_rp++;
  addr_rp += TASK_BUF_LEN;
  while(*mbnum_wp<=mbnum_rp);//wait until the third mb is ready
  ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=addr_rp;
  gp0_chain_ptr[0]=addr_rp;
  gp0_chain_ptr[1]=TCSM1_PADDR(dMB_NN);
  gp0_chain_ptr[3]=GP_UNIT(GP_TAG_LK,TASK_BUF_LEN,TASK_BUF_LEN);

  gp0_chain_ptr[4] = TCSM0_PADDR(TCSM0_GP0_POLL_END);
  gp0_chain_ptr[5] = TCSM1_PADDR(TCSM1_GP0_POLL_END);
  gp0_chain_ptr[6] = GP_STRD(4,GP_FRM_NML,4);
  gp0_chain_ptr[7] = GP_UNIT(GP_TAG_UL,4,4);
  *tcsm1_gp0_poll_end = 1;
  set_gp0_dcs();

  count = 0;

  y_mb_line = dSlice->linesize;
  uv_mb_line = dSlice->uvlinesize;
#ifdef JZC_ROTA90_OPT
  rota_y_mb_line = dSlice->mb_height * 256;
  rota_uv_mb_line = dSlice->mb_height * 128;
#endif
  Lpicture = &dSlice->line_current_picture;
  while((dMB->new_slice>=0) && (dMB->er_slice >=0)) {
    if(dMB->new_slice==1){
      XCHG2(dSlice,dSlice_ex,xchg_tmp);
    }
    mc_flag = 0;
#ifdef JZC_DBLK_OPT
    intra      = (int)(IS_INTRA_MBTYPE(dMB->mbtype)); // intra means intra4x4 and intra16x16
    inter16x16 = (dMB->mbtype == RV34_MB_P_MIX16x16);
#endif
    chain = 0;
    mb_x_d3=mb_x_d2;
    mb_x_d2=mb_x_d1;
    mb_x_d1=mb_x;
    mb_x= dMB->mb_x;
    mb_y_d3=mb_y_d2;
    mb_y_d2=mb_y_d1;
    mb_y_d1=mb_y;
    mb_y= dMB->mb_y;

    if(dSlice->si_type){
      if(dMB_N->mbtype > RV34_MB_TYPE_INTRA16x16)
	{
	  rv40_decode_mv_aux(dSlice, dMB_N, next_motion_dha);
	}
    }

    if(last_mc_flag){
      while(*(volatile int *)motion_dsa != (0x80000000 | 0xFFFF) );
      last_mc_flag = 0;
    }
    if(mc_flag){
      motion_dsa[0] = 0x0;
      SET_REG1_DDC(TCSM1_PADDR(next_motion_dha) | 0x1);
      last_mc_flag = mc_flag;
    }

#ifdef JZC_VMAU_OPT
    {
      while(*(volatile uint32_t *)VMAU_END_FLAG == 0);
      mau_reg_ptr->main_cbp = (dMB->mau_mcbp<<24)|dMB->cbp;
      mau_reg_ptr->quant_para=(dMB->qscale & QUANT_QP_MSK) << QUANT_QP_SFT;
      mau_reg_ptr->main_addr=TCSM1_PADDR(dMB->block16);
      mau_reg_ptr->ncchn_addr=TCSM1_PADDR(mau_reg_ptr);
      mau_reg_ptr->main_len = dMB->mau_mlen<<1;
      mau_reg_ptr->id=1;
      mau_reg_ptr->dec_incr=0;
      mau_reg_ptr->aux_cbp=0;
      mau_reg_ptr->y_pred_mode[0]=dMB->mau_ypm[0];
      mau_reg_ptr->y_pred_mode[1]=dMB->mau_ypm[1];
      mau_reg_ptr->c_pred_mode[0]=dMB->mau_cpm;
      mau_reg_ptr->top_lr_avalid = dMB->mau_na;

      write_reg(VMAU_V_BASE+VMAU_SLV_DEC_YADDR,TCSM1_PADDR(recon_ptr0));
      write_reg(VMAU_V_BASE+VMAU_SLV_DEC_UADDR,TCSM1_PADDR(recon_ptr0+20*16));
      write_reg(VMAU_V_BASE+VMAU_SLV_DEC_VADDR,TCSM1_PADDR(recon_ptr0+20*16+12*8));
      write_reg(VMAU_V_BASE+VMAU_SLV_NCCHN_ADDR,TCSM1_PADDR(mau_reg_ptr));
      write_reg(VMAU_V_BASE+VMAU_SLV_DEC_DONE, TCSM1_PADDR(VMAU_END_FLAG));
      *(volatile uint32_t *)VMAU_END_FLAG = 0;
      write_reg(VMAU_V_BASE+VMAU_SLV_GBL_RUN, 1); //run
    }
#endif

    if (count > 1){ /*LEFT copy*/
      while (*(volatile int*)DBLK_END_FLAG != DBLK_SYN_VALUE);
    }

    if (count>1){
#ifdef JZC_ROTA90_OPT
      tag1 = (mb_y_d2+1>=dSlice->mb_height) ? 1 : 0;
      tag2 = tag1 ? 0 : 4;

      dst2 = dblk_mb_ydst3 + 16*(16-tag2) + 12;
      dst1 = rota_mb_ybuf3 + 192 - 4*tag1 ;
      for(i=0; i<4; i++){
        src1 = XCH2_T0->dblk_out_addr + 20*(16-tag2) + i*4;

        for(j=0; j<3+tag1; j++){
	  S32LDI(xr1,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr1:x15,x14,x13,x12
          S32LDI(xr2,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr2:x11,x10,x9,x8
	  S32LDI(xr3,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr3:x7,x6,x5,x4
          S32LDI(xr4,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr4:x3,x2,x1,x0

	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr9,xr8,xr6,xr10,ptn3); //xr9:x2,x6,x10,x14 xr10:x0,x4,x8,x12	    
	  S32SFL(xr11,xr7,xr5,xr12,ptn3); //xr11:x3,x7,x11,x15 xr12:x1,x5,x9,x13

          S32SDI(xr1,dst2,-0x10);
          S32SDI(xr2,dst2,-0x10);
          S32SDI(xr3,dst2,-0x10);
	  S32SDI(xr4,dst2,-0x10);

          S32SDI(xr10,dst1,0x4);
	  S32STD(xr12,dst1,0x10);
	  S32STD(xr9, dst1,0x20);
	  S32STD(xr11,dst1,0x30); 
        }
	dst2 = dblk_mb_ydst1 + 16*(16-tag2) + i*4;
	dst1 = rota_mb_ybuf1 + i*64 - 4*tag1;
      }

      for(i=0; i<2; i++){
        src1 = XCH2_T0->dblk_out_addr_c + 96 + i*4;
	src2 = src1 + TCSM1_DBLK_MBOUT_UV_OFFSET;
        if (i){
	  dst3 = dblk_mb_udst1 + 128;
          dst4 = dst3 + 8;
          dst1 = rota_mb_cbuf1 - 4;
          dst2 = dst1 + 8;
        }else{
          dst3 = dblk_mb_udst3 + 132;
          dst4 = dst3 + 8;
          dst1 = rota_mb_cbuf3 + 60;
          dst2 = dst1 + 8;
        }

        for(j=0; j<2; j++){
	  S32LDI(xr1,src1,-TCSM1_DBLK_MBOUT_STRD_C);
          S32LDI(xr2,src1,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr3,src1,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr4,src1,-TCSM1_DBLK_MBOUT_STRD_C);

	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr9,xr8,xr6,xr10,ptn3); //xr9:x2,x6,x10,x14 xr10:x0,x4,x8,x12	    
	  S32SFL(xr11,xr7,xr5,xr12,ptn3); //xr11:x3,x7,x11,x15 xr12:x1,x5,x9,x13
          
          S32SDI(xr1,dst3,-0x10);
	  S32SDI(xr2,dst3,-0x10);
	  S32SDI(xr3,dst3,-0x10);
	  S32SDI(xr4,dst3,-0x10);

	  S32SDI(xr10,dst1,0x4);
	  S32STD(xr12,dst1,0x10);
	  S32STD(xr9, dst1,0x20);
	  S32STD(xr11,dst1,0x30);          

	  S32LDI(xr1,src2,-TCSM1_DBLK_MBOUT_STRD_C);
          S32LDI(xr2,src2,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr3,src2,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr4,src2,-TCSM1_DBLK_MBOUT_STRD_C);

	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr9,xr8,xr6,xr10,ptn3); //xr9:x2,x6,x10,x14 xr10:x0,x4,x8,x12	    
	  S32SFL(xr11,xr7,xr5,xr12,ptn3); //xr11:x3,x7,x11,x15 xr12:x1,x5,x9,x13
          
          S32SDI(xr1,dst4,-0x10);
	  S32SDI(xr2,dst4,-0x10);
	  S32SDI(xr3,dst4,-0x10);
	  S32SDI(xr4,dst4,-0x10);

	  S32SDI(xr10,dst2,0x4);
	  S32STD(xr12,dst2,0x10);
	  S32STD(xr9, dst2,0x20);
	  S32STD(xr11,dst2,0x30);          
        }
      }
#else
      tag1 = (mb_y_d2+1>=dSlice->mb_height) ? 1 : 0;
      tag2 = tag1 ? 0 : 4;

      dst2 = dblk_mb_ydst3 + 16*(16-tag2) + 12;
      for(i=0; i<4; i++){
        src1 = XCH2_T0->dblk_out_addr + 20*(16-tag2) + i*4;

        for(j=0; j<3+tag1; j++){
	  S32LDI(xr1,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr1:x15,x14,x13,x12
          S32LDI(xr2,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr2:x11,x10,x9,x8
	  S32LDI(xr3,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr3:x7,x6,x5,x4
          S32LDI(xr4,src1,-TCSM1_DBLK_MBOUT_STRD_Y);  //xr4:x3,x2,x1,x0

	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr9,xr8,xr6,xr10,ptn3); //xr9:x2,x6,x10,x14 xr10:x0,x4,x8,x12	    
	  S32SFL(xr11,xr7,xr5,xr12,ptn3); //xr11:x3,x7,x11,x15 xr12:x1,x5,x9,x13

          S32SDI(xr1,dst2,-0x10);
          S32SDI(xr2,dst2,-0x10);
          S32SDI(xr3,dst2,-0x10);
	  S32SDI(xr4,dst2,-0x10);
        }
	dst2 = dblk_mb_ydst1 + 16*(16-tag2) + i*4;
      }

      for(i=0; i<2; i++){
        src1 = XCH2_T0->dblk_out_addr_c + 96 + i*4;
	src2 = src1 + TCSM1_DBLK_MBOUT_UV_OFFSET;
        if (i){
	  dst3 = dblk_mb_udst1 + 128;
          dst4 = dst3 + 8;
        }else{
          dst3 = dblk_mb_udst3 + 132;
          dst4 = dst3 + 8;
        }

        for(j=0; j<2; j++){
	  S32LDI(xr1,src1,-TCSM1_DBLK_MBOUT_STRD_C);
          S32LDI(xr2,src1,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr3,src1,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr4,src1,-TCSM1_DBLK_MBOUT_STRD_C);

	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr9,xr8,xr6,xr10,ptn3); //xr9:x2,x6,x10,x14 xr10:x0,x4,x8,x12	    
	  S32SFL(xr11,xr7,xr5,xr12,ptn3); //xr11:x3,x7,x11,x15 xr12:x1,x5,x9,x13
          
          S32SDI(xr1,dst3,-0x10);
	  S32SDI(xr2,dst3,-0x10);
	  S32SDI(xr3,dst3,-0x10);
	  S32SDI(xr4,dst3,-0x10);

	  S32LDI(xr1,src2,-TCSM1_DBLK_MBOUT_STRD_C);
          S32LDI(xr2,src2,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr3,src2,-TCSM1_DBLK_MBOUT_STRD_C);
	  S32LDI(xr4,src2,-TCSM1_DBLK_MBOUT_STRD_C);

	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr9,xr8,xr6,xr10,ptn3); //xr9:x2,x6,x10,x14 xr10:x0,x4,x8,x12	    
	  S32SFL(xr11,xr7,xr5,xr12,ptn3); //xr11:x3,x7,x11,x15 xr12:x1,x5,x9,x13
          
          S32SDI(xr1,dst4,-0x10);
	  S32SDI(xr2,dst4,-0x10);
	  S32SDI(xr3,dst4,-0x10);
	  S32SDI(xr4,dst4,-0x10);
        }
      }
#endif

#ifdef JZC_ROTA90_OPT
      if (mb_y_d2){
	for(i=0; i<4; i++){
          src1 = dblk_upout_addr1 + 64 + i*4;      
	  dst1 = rota_upmb_ybuf1 + i*16 - 4; 
	  S32LDI (xr1,src1,-0x10);       //xr1: x15,x14,x13,x12
	  S32LDI (xr2,src1,-0x10);       //xr2: x11,x10,x9,x8
	  S32LDI (xr3,src1,-0x10);       //xr3: x7,x6,x5,x4
	  S32LDI (xr4,src1,-0x10);       //xr4: x3,x2,x1,x0
	  
	  S32SFL(xr5,xr2,xr1,xr6,ptn2); //xr5:x11,x15,x9,x13 xr6:x10,x14,x8,x12
	  S32SFL(xr7,xr4,xr3,xr8,ptn2); //xr7:x3,x7,x1,x5 xr8:x2,x6,x0,x4	
	  S32SFL(xr3,xr8,xr6,xr4,ptn3); //xr3:x2,x6,x10,x14 xr4:x0,x4,x8,x12	    
	  S32SFL(xr1,xr7,xr5,xr2,ptn3); //xr1:x3,x7,x11,x15 xr2:x1,x5,x9,x13
	  
	  S32SDI(xr4,dst1,0x4);
	  S32STD(xr2,dst1,0x4);
	  S32STD(xr3,dst1,0x8);
	  S32STD(xr1,dst1,0xC);
	}
      }
#endif
      
      if (count > 2){
	tag3 = (mb_y_d3 + 1 == dSlice->mb_height) ? 1 : 0;
#ifdef JZC_ROTA90_OPT
	rota_mx = dSlice->mb_height - 1 - mb_y_d3;
	rota_my = mb_x_d3;
	rota_ydst = Lpicture->rota_y_ptr + rota_my*rota_y_mb_line + rota_mx*256;
	rota_cdst = Lpicture->rota_uv_ptr + rota_my*rota_uv_mb_line + rota_mx * 128;
#endif
	dst3 = Lpicture->y_ptr + mb_y_d3 * y_mb_line + mb_x_d3 * 256;
	dst4 = Lpicture->uv_ptr + mb_y_d3 * uv_mb_line + mb_x_d3 * 128;

	if (mb_y_d2){
	  up_ydst = Lpicture->y_ptr + (mb_y_d2 - 1) * y_mb_line + mb_x_d2 * 256;
#ifdef JZC_ROTA90_OPT
	  rota_up_mx = dSlice->mb_height - mb_y_d2;
	  rota_up_my = mb_x_d2;
          rota_up_ydst = Lpicture->rota_y_ptr + rota_up_my * rota_y_mb_line + rota_up_mx*256;
#endif
        }else{
          up_ydst = Lpicture->y_ptr - 256;
#ifdef JZC_ROTA90_OPT
          rota_up_ydst = up_ydst;
#endif
        }

	gp1_chain_ptr[0]=TCSM1_PADDR(dblk_mb_ydst3);
	gp1_chain_ptr[1]=(dst3);
        gp1_chain_ptr[2]=GP_STRD(192+tag3*64,GP_FRM_NML,tag3*64);
	gp1_chain_ptr[3]=GP_UNIT(GP_TAG_LK,192+tag3*64,192+tag3*64);  

	gp1_chain_ptr[4]=TCSM1_PADDR(dblk_mb_udst3);
	gp1_chain_ptr[5]=(dst4);	    
	gp1_chain_ptr[8]=TCSM1_PADDR(dblk_upout_addr1);
	gp1_chain_ptr[9]=(up_ydst + 16*12);

#ifdef JZC_ROTA90_OPT
	gp1_chain_ptr[12]=TCSM1_PADDR(rota_mb_ybuf3);
	gp1_chain_ptr[13]=(rota_ydst);
	gp1_chain_ptr[14]=GP_STRD(256,GP_FRM_NML,256);
	gp1_chain_ptr[15]=GP_UNIT(GP_TAG_LK,256,256);
	    
	gp1_chain_ptr[16]=TCSM1_PADDR(rota_mb_cbuf3);
	gp1_chain_ptr[17]=(rota_cdst);
	gp1_chain_ptr[18]=GP_STRD(128,GP_FRM_NML,128);
	gp1_chain_ptr[19]=GP_UNIT(GP_TAG_LK,128,128);
         
	gp1_chain_ptr[20]=TCSM1_PADDR(rota_upmb_ybuf1);
	gp1_chain_ptr[21]=(rota_up_ydst);
	gp1_chain_ptr[22]=GP_STRD(4,GP_FRM_NML,16);
	gp1_chain_ptr[23]=GP_UNIT(GP_TAG_LK,4,4*16);

	gp1_chain_ptr[24] = TCSM0_PADDR((int)tcsm0_gp1_poll_end);
	gp1_chain_ptr[25] = TCSM1_PADDR((int)tcsm1_gp1_poll_end);
	gp1_chain_ptr[26] = GP_STRD(4,GP_FRM_NML,4);
	gp1_chain_ptr[27] = GP_UNIT(GP_TAG_UL,4,4);
#else
	gp1_chain_ptr[12] = TCSM0_PADDR((int)tcsm0_gp1_poll_end);
	gp1_chain_ptr[13] = TCSM1_PADDR((int)tcsm1_gp1_poll_end);
	gp1_chain_ptr[14] = GP_STRD(4,GP_FRM_NML,4);
	gp1_chain_ptr[15] = GP_UNIT(GP_TAG_UL,4,4);
#endif
	set_gp1_dha(TCSM1_PADDR(gp1_chain_ptr));
	while(*tcsm1_gp1_poll_end);
	*tcsm1_gp1_poll_end = 1;
	set_gp1_dcs();
      }
    }

    if (count){ /*LEFT copy*/
      src1 = (uint8_t *)(XCH2_T0->dblk_out_addr + 16 - TCSM1_DBLK_MBOUT_STRD_Y);
      dst1 = (uint8_t *)(recon_ptr2 - 4 - PREVIOUS_LUMA_STRIDE);
      src2 = (uint8_t *)(XCH2_T0->dblk_out_addr_c + 8 - TCSM1_DBLK_MBOUT_STRD_C);
      dst2 = (uint8_t *)(recon_ptr2 + PREVIOUS_OFFSET_U - 4 - PREVIOUS_CHROMA_STRIDE);
      for(i=0;i<4;i++){
	S32LDI(xr1, src1, TCSM1_DBLK_MBOUT_STRD_Y);
	S32LDI(xr2, src2, TCSM1_DBLK_MBOUT_STRD_C);
	S32LDI(xr3, src1, TCSM1_DBLK_MBOUT_STRD_Y);
	S32LDI(xr4, src2, TCSM1_DBLK_MBOUT_STRD_C);
	S32LDI(xr5, src1, TCSM1_DBLK_MBOUT_STRD_Y);
	S32LDI(xr6, src2, TCSM1_DBLK_MBOUT_STRD_C);
	S32LDI(xr7, src1, TCSM1_DBLK_MBOUT_STRD_Y);
	S32LDI(xr8, src2, TCSM1_DBLK_MBOUT_STRD_C);

	S32SDI(xr1, dst1, PREVIOUS_LUMA_STRIDE);
	S32SDI(xr2, dst2, PREVIOUS_CHROMA_STRIDE);
	S32SDI(xr3, dst1, PREVIOUS_LUMA_STRIDE);
	S32SDI(xr4, dst2, PREVIOUS_CHROMA_STRIDE);
	S32SDI(xr5, dst1, PREVIOUS_LUMA_STRIDE);
	S32SDI(xr6, dst2, PREVIOUS_CHROMA_STRIDE);
	S32SDI(xr7, dst1, PREVIOUS_LUMA_STRIDE);
	S32SDI(xr8, dst2, PREVIOUS_CHROMA_STRIDE);
      }
      SET_DBLK_DDMA_DHA(TCSM1_PADDR(XCH2_T1->dblk_des_ptr));
      *(volatile int*)DBLK_END_FLAG=0;
      SET_DBLK_DDMA_DCS(0x1);
    }

    //dblk config
    {
      unsigned char left_mbs = (dMB->mb_x == 0) || (dMB->qscale == 100); //qp==100 for row_parity pseudo MB
      unsigned char top_mbs = (dMB->mb_y == 0);

      unsigned int * node = (unsigned int *)XCH2_T0->dblk_des_ptr;
      unsigned  dblk_bp = dMB->cbp;
      i_movn(dblk_bp, (dMB->cbp | 0xffff), inter16x16);
      i_movn(dblk_bp, (dMB->cbp | 0xffffff), intra);
      node[2] = TCSM1_PADDR((unsigned int)dblk_upout_addr1 + 136);
      node[3] = TCSM1_PADDR((unsigned int)dblk_upout_addr1 + 128);
      node[4] = TCSM1_PADDR((unsigned int)dblk_upout_addr1);
      
      node[5] = TCSM1_PADDR((unsigned int)recon_ptr0 + PREVIOUS_OFFSET_V);
      node[6] = TCSM1_PADDR((unsigned int)recon_ptr0 + PREVIOUS_OFFSET_U);
      node[7] = TCSM1_PADDR((unsigned int)recon_ptr0);
      
      node[8] = dMB->mvd_left | (dMB->mvd_above<<16);
      node[9] = dMB->mvd;
      node[10] = dMB->dcbp_above | (dMB->mbtype_above<<24);

      node[11] = dMB->dcbp_left | (dMB->mbtype_left<<24);
      node[12] = dblk_bp | (dMB->mbtype << 24);
      node[13] = (2 | (left_mbs<<2)|(top_mbs<<3)|((dSlice->pict_type-1)<<4) | (dMB->qscale<<16)|(dSlice->refqp<<24));
    }

    while(*tcsm1_gp0_poll_end);
    *tcsm1_gp0_poll_end = 1;     
    //poll_gp0_end();
    mbnum_rp++;
    addr_rp += TASK_BUF_LEN;
    if(addr_rp>(TCSM0_PADDR(TCSM0_END)-TASK_BUF_LEN))
      addr_rp=TCSM0_PADDR(TCSM0_TASK_FIFO);

    while(*mbnum_wp<=mbnum_rp);//wait until next mb is ready
    ((volatile int *)TCSM0_PADDR(TCSM0_P1_FIFO_RP))[0]=addr_rp;
    gp0_chain_ptr[0]=addr_rp;
    gp0_chain_ptr[1]=TCSM1_PADDR(dMB_L);
    gp0_chain_ptr[3]=GP_UNIT(GP_TAG_LK,TASK_BUF_LEN,TASK_BUF_LEN);

    gp0_chain_ptr[4] = TCSM0_PADDR(TCSM0_GP0_POLL_END);
    gp0_chain_ptr[5] = TCSM1_PADDR(TCSM1_GP0_POLL_END);
    gp0_chain_ptr[6] = GP_STRD(4,GP_FRM_NML,4);
    gp0_chain_ptr[7] = GP_UNIT(GP_TAG_UL,4,4);
    set_gp0_dcs();

    count++;
#ifdef JZC_DBLK_OPT
    XCHG2(XCH2_T0,XCH2_T1,xchg_tmp);
#endif

    XCHG2(motion_dha,next_motion_dha,xchg_tmp);
    XCHG3(recon_ptr2,recon_ptr0,recon_ptr1,xchg_tmp);
    XCHG4(dMB_L,dMB,dMB_N,dMB_NN,xchg_tmp);
    XCHG2(gp1_chain_ptr, gp1_chain_ptr1,xchg_tmp);
    XCHG2(dblk_upout_addr0,dblk_upout_addr1,xchg_tmp);

#ifdef JZC_ROTA90_OPT
    XCHG3(rota_mb_ybuf3,rota_mb_ybuf1,rota_mb_ybuf2,xchg_tmp);
    XCHG3(rota_mb_cbuf3,rota_mb_cbuf1,rota_mb_cbuf2,xchg_tmp);
    XCHG3(dblk_mb_ydst3,dblk_mb_ydst1,dblk_mb_ydst2,xchg_tmp);
    XCHG3(dblk_mb_udst3,dblk_mb_udst1,dblk_mb_udst2,xchg_tmp);
    XCHG2(rota_upmb_ybuf1,rota_upmb_ybuf2,xchg_tmp);
#else
    XCHG3(dblk_mb_ydst3,dblk_mb_ydst1,dblk_mb_ydst2,xchg_tmp);
    XCHG3(dblk_mb_udst3,dblk_mb_udst1,dblk_mb_udst2,xchg_tmp);
#endif

  }
  poll_gp0_end(); 
  poll_gp1_end();  
  
  *((volatile int *)TCSM0_PADDR(TCSM0_P1_TASK_DONE)) = 0x1;
  i_nop;
  i_nop;
  i_nop;
  i_nop;
  i_wait();

}
