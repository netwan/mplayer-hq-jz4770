.file	1 "rv9_p1.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	rv40_mc_2mv_hw
.type	rv40_mc_2mv_hw, @function
rv40_mc_2mv_hw:
.frame	$sp,8,$31		# vars= 8, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
lw	$9,40($sp)
lw	$2,44($sp)
sw	$9,0($sp)
beq	$9,$0,$L6
sw	$2,4($sp)

sltu	$2,$0,$2
$L2:
sll	$14,$2,31
lw	$2,32($sp)
andi	$2,$2,0x3
sll	$10,$2,10
lw	$3,36($sp)
andi	$3,$3,0x3
sll	$11,$3,8
lw	$3,24($sp)
sll	$3,$3,6
andi	$3,$3,0xff
lw	$8,28($sp)
andi	$8,$8,0x3
sll	$8,$8,4
move	$12,$sp
sll	$5,$5,2
addiu	$5,$5,50
addu	$4,$4,$5
move	$5,$9
move	$13,$0
li	$2,65536			# 0x10000
or	$8,$8,$2
or	$3,$8,$3
or	$3,$3,$11
or	$2,$3,$10
or	$2,$2,$14
li	$11,1			# 0x1
$L5:
beq	$5,$0,$L3
nop

lh	$3,-2($4)
lh	$9,0($4)
lw	$8,0($7)
sll	$8,$8,3
addu	$8,$6,$8
andi	$5,$3,0xffff
sll	$10,$9,16
or	$5,$5,$10
sw	$5,0($8)
lw	$10,0($7)
sll	$10,$10,3
addu	$10,$6,$10
sll	$8,$13,30
or	$8,$2,$8
andi	$3,$3,0x7
or	$5,$8,$3
andi	$3,$9,0x7
sll	$3,$3,20
or	$3,$5,$3
sw	$3,4($10)
lw	$3,0($7)
addiu	$3,$3,1
sw	$3,0($7)
$L3:
addiu	$12,$12,4
beq	$13,$11,$L1
addiu	$4,$4,16

lw	$5,0($12)
j	$L5
li	$13,1			# 0x1

$L1:
j	$31
addiu	$sp,$sp,8

$L6:
j	$L2
move	$2,$0

.set	macro
.set	reorder
.end	rv40_mc_2mv_hw
.size	rv40_mc_2mv_hw, .-rv40_mc_2mv_hw
.align	2
.set	nomips16
.set	nomicromips
.ent	rv40_decode_mv_aux.isra.0
.type	rv40_decode_mv_aux.isra.0, @function
rv40_decode_mv_aux.isra.0:
.frame	$sp,88,$31		# vars= 8, regs= 9/0, args= 40, gp= 0
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-88
sw	$31,84($sp)
sw	$23,80($sp)
sw	$22,76($sp)
sw	$21,72($sp)
sw	$20,68($sp)
sw	$19,64($sp)
sw	$18,60($sp)
sw	$17,56($sp)
sw	$16,52($sp)
move	$17,$5
move	$18,$6
lb	$5,16($5)
sw	$0,40($sp)
andi	$2,$5,0x00ff
sltu	$3,$2,12
beq	$3,$0,$L26
addiu	$16,$6,4

sll	$3,$2,2
lui	$2,%hi($L15)
addiu	$2,$2,%lo($L15)
addu	$2,$2,$3
lw	$2,0($2)
j	$2
nop

.rdata
.align	2
.align	2
$L15:
.word	$L26
.word	$L26
.word	$L14
.word	$L27
.word	$L17
.word	$L17
.word	$L18
.word	$L19
.word	$L20
.word	$L20
.word	$L21
.word	$L14
.text
$L18:
lbu	$3,0($4)
li	$2,2			# 0x2
beq	$3,$2,$L42
li	$2,3			# 0x3

$L19:
lw	$2,4($17)
andi	$2,$2,0x70
beq	$2,$0,$L39
move	$19,$0

addiu	$23,$sp,40
li	$21,2			# 0x2
li	$20,1			# 0x1
li	$22,4			# 0x4
$L22:
sw	$21,16($sp)
sw	$21,20($sp)
andi	$2,$19,0x2
sw	$2,24($sp)
andi	$2,$19,0x1
sll	$2,$2,1
sw	$2,28($sp)
sw	$20,32($sp)
sw	$20,36($sp)
move	$4,$17
move	$5,$19
move	$6,$16
jal	rv40_mc_2mv_hw
move	$7,$23

addiu	$19,$19,1
bne	$19,$22,$L22
li	$3,1			# 0x1

lui	$2,%hi(mc_flag)
$L35:
sw	$3,%lo(mc_flag)($2)
$L34:
lw	$2,40($sp)
sll	$5,$2,3
addiu	$3,$5,-4
andi	$2,$2,0x7f
sll	$4,$2,16
addiu	$7,$5,4
$L13:
addu	$2,$16,$3
lw	$3,0($2)
li	$6,33554432			# 0x2000000
or	$3,$3,$6
sw	$3,0($2)
lbu	$3,21($17)
li	$2,-855638016			# 0xffffffffcd000000
or	$3,$3,$2
lbu	$2,22($17)
sll	$2,$2,8
or	$2,$3,$2
or	$2,$2,$4
sw	$2,0($18)
addu	$5,$16,$5
li	$2,-1056964608			# 0xffffffffc1000000
sw	$2,0($5)
addu	$16,$16,$7
li	$2,-1610612736			# 0xffffffffa0000000
ori	$2,$2,0xffff
sw	$2,0($16)
lw	$31,84($sp)
lw	$23,80($sp)
lw	$22,76($sp)
lw	$21,72($sp)
lw	$20,68($sp)
lw	$19,64($sp)
lw	$18,60($sp)
lw	$17,56($sp)
lw	$16,52($sp)
j	$31
addiu	$sp,$sp,88

$L14:
li	$2,3			# 0x3
$L42:
sw	$2,16($sp)
sw	$2,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
li	$19,1			# 0x1
sw	$19,32($sp)
sw	$0,36($sp)
$L37:
move	$4,$17
move	$5,$0
move	$6,$16
jal	rv40_mc_2mv_hw
addiu	$7,$sp,40

lui	$2,%hi(mc_flag)
j	$L34
sw	$19,%lo(mc_flag)($2)

$L27:
move	$19,$0
addiu	$23,$sp,40
li	$22,2			# 0x2
li	$21,1			# 0x1
li	$20,4			# 0x4
$L16:
sw	$22,16($sp)
sw	$22,20($sp)
andi	$2,$19,0x2
sw	$2,24($sp)
andi	$2,$19,0x1
sll	$2,$2,1
sw	$2,28($sp)
sw	$21,32($sp)
sw	$0,36($sp)
move	$4,$17
move	$5,$19
move	$6,$16
jal	rv40_mc_2mv_hw
move	$7,$23

addiu	$19,$19,1
bne	$19,$20,$L16
lui	$2,%hi(mc_flag)

j	$L35
li	$3,1			# 0x1

$L17:
li	$2,3			# 0x3
sw	$2,16($sp)
sw	$2,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
xori	$2,$5,0x4
sltu	$2,$2,1
sw	$2,32($sp)
xori	$5,$5,0x5
sltu	$5,$5,1
sw	$5,36($sp)
$L38:
move	$4,$17
move	$5,$0
move	$6,$16
jal	rv40_mc_2mv_hw
addiu	$7,$sp,40

li	$3,1			# 0x1
j	$L35
lui	$2,%hi(mc_flag)

$L20:
li	$2,8			# 0x8
beq	$5,$2,$L40
li	$2,9			# 0x9

beq	$5,$2,$L41
li	$21,3			# 0x3

li	$7,4			# 0x4
move	$4,$0
li	$3,-4			# 0xfffffffffffffffc
move	$5,$0
$L25:
li	$6,1			# 0x1
lui	$2,%hi(mc_flag)
j	$L13
sw	$6,%lo(mc_flag)($2)

$L21:
li	$2,3			# 0x3
sw	$2,16($sp)
sw	$2,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
li	$19,1			# 0x1
sw	$19,32($sp)
j	$L37
sw	$19,36($sp)

$L39:
li	$2,3			# 0x3
sw	$2,16($sp)
sw	$2,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
li	$2,1			# 0x1
sw	$2,32($sp)
j	$L38
sw	$2,36($sp)

$L26:
li	$7,4			# 0x4
move	$4,$0
li	$3,-4			# 0xfffffffffffffffc
j	$L13
move	$5,$0

$L41:
sw	$21,16($sp)
li	$19,2			# 0x2
sw	$19,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
li	$20,1			# 0x1
sw	$20,32($sp)
sw	$0,36($sp)
move	$4,$17
move	$5,$0
move	$6,$16
jal	rv40_mc_2mv_hw
addiu	$7,$sp,40

sw	$21,16($sp)
sw	$19,20($sp)
sw	$0,24($sp)
sw	$19,28($sp)
sw	$20,32($sp)
sw	$0,36($sp)
move	$4,$17
li	$5,1			# 0x1
$L33:
move	$6,$16
jal	rv40_mc_2mv_hw
addiu	$7,$sp,40

lw	$2,40($sp)
sll	$5,$2,3
addiu	$3,$5,-4
andi	$2,$2,0x7f
sll	$4,$2,16
j	$L25
addiu	$7,$5,4

$L40:
li	$19,2			# 0x2
sw	$19,16($sp)
li	$21,3			# 0x3
sw	$21,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
li	$20,1			# 0x1
sw	$20,32($sp)
sw	$0,36($sp)
move	$4,$17
move	$5,$0
move	$6,$16
jal	rv40_mc_2mv_hw
addiu	$7,$sp,40

sw	$19,16($sp)
sw	$21,20($sp)
sw	$19,24($sp)
sw	$0,28($sp)
sw	$20,32($sp)
sw	$0,36($sp)
move	$4,$17
j	$L33
li	$5,2			# 0x2

.set	macro
.set	reorder
.end	rv40_decode_mv_aux.isra.0
.size	rv40_decode_mv_aux.isra.0, .-rv40_decode_mv_aux.isra.0
.section	.p1_main,"ax",@progbits
.align	2
.globl	main
.set	nomips16
.set	nomicromips
.ent	main
.type	main, @function
main:
.frame	$sp,176,$31		# vars= 120, regs= 10/0, args= 16, gp= 0
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
la	$sp, 0xF400BFFC
li	$2,3			# 0x3
#APP
# 38 "rv9_p1.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
li	$2,-201326592			# 0xfffffffff4000000
li	$3,42405			# 0xa5a5
sw	$3,22012($2)
li	$3,-2147483648			# 0xffffffff80000000
ori	$3,$3,0xffff
sw	$3,21896($2)
sw	$0,26204($2)
li	$3,321585152			# 0x132b0000
sw	$0,12($3)
sw	$0,26200($2)
sw	$0,8($3)
lui	$3,%hi(last_mc_flag)
sw	$0,%lo(last_mc_flag)($3)
lui	$4,%hi(mc_flag)
sw	$0,%lo(mc_flag)($4)
li	$4,8388608			# 0x800000
addiu	$4,$4,128
sw	$4,23504($2)
sw	$4,23508($2)
li	$3,4194304			# 0x400000
addiu	$3,$3,64
sw	$3,23520($2)
sw	$3,23524($2)
sw	$4,23712($2)
sw	$4,23716($2)
sw	$3,23728($2)
sw	$3,23732($2)
sw	$3,23456($2)
li	$2,321650688			# 0x132c0000
addiu	$2,$2,23448
li	$3,320929792			# 0x13210000
sw	$2,0($3)
li	$3,-201326592			# 0xfffffffff4000000
$L137:
lw	$2,16512($3)
slt	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L137
li	$4,321585152			# 0x132b0000
.set	macro
.set	reorder

addiu	$2,$4,4128
sw	$2,4($4)
sw	$2,23448($3)
li	$2,321650688			# 0x132c0000
addiu	$2,$2,17488
sw	$2,23452($3)
li	$2,-2089811968			# 0xffffffff83700000
addiu	$2,$2,880
sw	$2,23460($3)
li	$4,1			# 0x1
li	$2,320929792			# 0x13210000
sw	$4,4($2)
li	$2,321388544			# 0x13280000
addiu	$6,$2,64
li	$5,4			# 0x4
#APP
# 162 "rv9_p1.c" 1
sw	 $5,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$6,$2,80
li	$5,2			# 0x2
#APP
# 163 "rv9_p1.c" 1
sw	 $5,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$6,$2,116
li	$5,786432			# 0xc0000
addiu	$5,$5,20
#APP
# 165 "rv9_p1.c" 1
sw	 $5,0($6)	#i_sw
# 0 "" 2
#NO_APP
lbu	$5,16386($3)
sll	$5,$5,4
addiu	$6,$2,84
#APP
# 166 "rv9_p1.c" 1
sw	 $5,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,68
#APP
# 167 "rv9_p1.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
sw	$4,22008($3)
li	$3,320929792			# 0x13210000
addiu	$4,$3,4
$L138:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L138
li	$2,321585152			# 0x132b0000
.set	macro
.set	reorder

addiu	$sp,$sp,-176
sw	$31,172($sp)
sw	$fp,168($sp)
sw	$23,164($sp)
sw	$22,160($sp)
sw	$21,156($sp)
sw	$20,152($sp)
sw	$19,148($sp)
sw	$18,144($sp)
sw	$17,140($sp)
sw	$16,136($sp)
li	$16,-201326592			# 0xfffffffff4000000
addiu	$2,$2,5008
sw	$2,23448($16)
li	$2,321650688			# 0x132c0000
addiu	$3,$2,18368
sw	$3,23452($16)
li	$3,-2089811968			# 0xffffffff83700000
addiu	$3,$3,880
sw	$3,23460($16)
li	$3,1			# 0x1
sw	$3,0($4)
addiu	$2,$2,21896
li	$3,321191936			# 0x13250000
addiu	$3,$3,88
#APP
# 181 "rv9_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$16,16384
lbu	$2,1($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L139
lui	$6,%hi(mc_flag)
.set	macro
.set	reorder

addiu	$5,$16,17488
lb	$2,16($5)
slt	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L140
lw	$2,%lo(mc_flag)($6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
jal	rv40_decode_mv_aux.isra.0
addiu	$6,$16,21668
.set	macro
.set	reorder

sb	$0,21896($16)
lui	$6,%hi(mc_flag)
$L139:
lw	$2,%lo(mc_flag)($6)
$L140:
.set	noreorder
.set	nomacro
beq	$2,$0,$L48
li	$3,320929792			# 0x13210000
.set	macro
.set	reorder

li	$3,321191936			# 0x13250000
addiu	$3,$3,84
li	$2,321650688			# 0x132c0000
addiu	$2,$2,21669
#APP
# 191 "rv9_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%lo(mc_flag)($6)
lui	$7,%hi(last_mc_flag)
sw	$2,%lo(last_mc_flag)($7)
li	$3,320929792			# 0x13210000
$L48:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L48
li	$18,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

$L141:
lw	$2,16512($18)
slt	$2,$2,3
.set	noreorder
.set	nomacro
bne	$2,$0,$L141
li	$2,321585152			# 0x132b0000
.set	macro
.set	reorder

addiu	$3,$2,5888
sw	$3,4($2)
sw	$3,23448($18)
li	$4,321650688			# 0x132c0000
addiu	$5,$4,19248
sw	$5,23452($18)
li	$5,57671680			# 0x3700000
addiu	$5,$5,880
sw	$5,23460($18)
addiu	$2,$2,12
sw	$2,23464($18)
addiu	$4,$4,26204
sw	$4,23468($18)
li	$2,262144			# 0x40000
addiu	$2,$2,4
sw	$2,23472($18)
li	$2,-2147221504			# 0xffffffff80040000
addiu	$2,$2,4
sw	$2,23476($18)
li	$2,1			# 0x1
sw	$2,26204($18)
li	$4,320929792			# 0x13210000
sw	$2,4($4)
addiu	$21,$18,16384
lhu	$9,30($21)
sw	$9,100($sp)
lhu	$10,32($21)
sw	$10,108($sp)
addiu	$16,$18,17488
lb	$2,24($16)
bltz	$2,$L52
lb	$4,23($16)
.set	noreorder
.set	nomacro
bltz	$4,$L52
move	$7,$0
.set	macro
.set	reorder

sw	$0,36($sp)
sw	$0,80($sp)
sw	$0,96($sp)
sw	$0,40($sp)
sw	$0,92($sp)
addiu	$15,$18,24920
sw	$15,32($sp)
addiu	$24,$18,24536
sw	$24,68($sp)
addiu	$9,$18,24152
addiu	$13,$18,24664
addiu	$25,$18,24280
sw	$25,64($sp)
addiu	$8,$18,23896
addiu	$10,$18,21156
sw	$10,16($sp)
addiu	$15,$18,20644
sw	$15,60($sp)
addiu	$fp,$18,20132
move	$12,$3
addiu	$24,$18,21744
sw	$24,24($sp)
addiu	$25,$18,21668
sw	$25,88($sp)
addiu	$23,$18,22232
addiu	$3,$18,22168
sw	$3,84($sp)
addiu	$10,$18,16592
sw	$10,28($sp)
addiu	$19,$18,16576
addiu	$15,$18,16448
sw	$15,104($sp)
addiu	$24,$18,19248
sw	$24,56($sp)
addiu	$11,$18,18368
addiu	$25,$18,16608
sw	$25,20($sp)
move	$22,$0
addiu	$3,$18,23688
sw	$3,52($sp)
addiu	$18,$18,23480
li	$17,321650688			# 0x132c0000
move	$20,$8
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L133
move	$8,$7
.set	macro
.set	reorder

$L53:
lui	$2,%hi(mc_flag)
sw	$0,%lo(mc_flag)($2)
lb	$2,16($16)
andi	$3,$2,0x00ff
sltu	$3,$3,2
sw	$3,44($sp)
xori	$2,$2,0xb
sltu	$2,$2,1
sw	$2,48($sp)
lbu	$3,21($16)
sw	$3,72($sp)
lbu	$7,22($16)
sw	$7,76($sp)
lbu	$2,1($21)
.set	noreorder
.set	nomacro
beq	$2,$0,$L142
lui	$25,%hi(last_mc_flag)
.set	macro
.set	reorder

$L134:
lb	$2,16($11)
slt	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L54
move	$4,$21
.set	macro
.set	reorder

move	$5,$11
lw	$6,24($sp)
sw	$8,128($sp)
sw	$9,124($sp)
sw	$11,116($sp)
sw	$12,112($sp)
.set	noreorder
.set	nomacro
jal	rv40_decode_mv_aux.isra.0
sw	$13,120($sp)
.set	macro
.set	reorder

lui	$10,%hi(last_mc_flag)
lw	$2,%lo(last_mc_flag)($10)
lw	$8,128($sp)
lw	$9,124($sp)
lw	$11,116($sp)
lw	$12,112($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L55
lw	$13,120($sp)
.set	macro
.set	reorder

li	$4,-201326592			# 0xfffffffff4000000
$L146:
li	$3,-2147483648			# 0xffffffff80000000
ori	$3,$3,0xffff
$L56:
lw	$2,21896($4)
.set	noreorder
.set	nomacro
bne	$2,$3,$L56
lui	$15,%hi(last_mc_flag)
.set	macro
.set	reorder

sw	$0,%lo(last_mc_flag)($15)
$L55:
lui	$24,%hi(mc_flag)
lw	$2,%lo(mc_flag)($24)
.set	noreorder
.set	nomacro
beq	$2,$0,$L135
li	$3,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
sb	$0,21896($2)
lhu	$2,24($sp)
li	$25,321650688			# 0x132c0000
ori	$25,$25,0x1
or	$2,$2,$25
li	$3,321191936			# 0x13250000
ori	$3,$3,0x54
#APP
# 252 "rv9_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%lo(mc_flag)($24)
lui	$6,%hi(last_mc_flag)
sw	$2,%lo(last_mc_flag)($6)
$L59:
li	$3,-201326592			# 0xfffffffff4000000
$L135:
addiu	$2,$3,22008
lw	$4,22008($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L59
addiu	$5,$3,21900
.set	macro
.set	reorder

lhu	$4,32($16)
sll	$4,$4,24
lw	$6,0($16)
or	$4,$4,$6
sw	$4,21900($3)
lbu	$3,20($16)
andi	$3,$3,0x3f
sw	$3,4($5)
addiu	$3,$16,80
andi	$3,$3,0xffff
or	$3,$3,$17
sw	$3,8($5)
addiu	$4,$17,21900
sw	$4,12($5)
lhu	$3,34($16)
sll	$3,$3,1
sh	$3,16($5)
li	$3,1			# 0x1
sb	$3,18($5)
sb	$0,19($5)
sw	$0,20($5)
lw	$3,36($16)
sw	$3,28($5)
lw	$3,40($16)
sw	$3,32($5)
lw	$3,44($16)
sh	$3,24($5)
lbu	$3,25($16)
sh	$3,26($5)
andi	$15,$fp,0xffff
or	$15,$15,$17
li	$3,321388544			# 0x13280000
addiu	$5,$3,104
#APP
# 272 "rv9_p1.c" 1
sw	 $15,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$14,$fp,320
andi	$5,$14,0xffff
or	$5,$5,$17
addiu	$6,$3,108
#APP
# 273 "rv9_p1.c" 1
sw	 $5,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$5,$fp,416
andi	$6,$5,0xffff
or	$6,$6,$17
addiu	$7,$3,112
#APP
# 274 "rv9_p1.c" 1
sw	 $6,0($7)	#i_sw
# 0 "" 2
#NO_APP
addiu	$6,$3,12
#APP
# 275 "rv9_p1.c" 1
sw	 $4,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$4,108
addiu	$6,$3,88
#APP
# 276 "rv9_p1.c" 1
sw	 $4,0($6)	#i_sw
# 0 "" 2
#NO_APP
sw	$0,0($2)
addiu	$3,$3,64
li	$2,1			# 0x1
#APP
# 278 "rv9_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
slt	$2,$22,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L60
li	$4,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

li	$3,42405			# 0xa5a5
$L105:
lw	$2,22012($4)
.set	noreorder
.set	nomacro
bne	$2,$3,$L105
addiu	$2,$8,1
.set	macro
.set	reorder

lbu	$3,3($21)
slt	$10,$2,$3
xori	$10,$10,0x1
li	$2,320			# 0x140
li	$4,240			# 0xf0
movn	$4,$2,$10
li	$2,268			# 0x10c
li	$3,204			# 0xcc
movz	$2,$3,$10
li	$3,256			# 0x100
li	$24,192			# 0xc0
movn	$24,$3,$10
addu	$2,$13,$2
addiu	$10,$10,2
addiu	$25,$4,16
subu	$24,$24,$4
$L64:
lw	$3,4($19)
addu	$3,$3,$4
move	$6,$0
$L63:
#APP
# 387 "rv9_p1.c" 1
.word	0b01110000011011111110110001010100	#S32LDI XR1,$3,-20
# 0 "" 2
# 388 "rv9_p1.c" 1
.word	0b01110000011011111110110010010100	#S32LDI XR2,$3,-20
# 0 "" 2
# 389 "rv9_p1.c" 1
.word	0b01110000011011111110110011010100	#S32LDI XR3,$3,-20
# 0 "" 2
# 390 "rv9_p1.c" 1
.word	0b01110000011011111110110100010100	#S32LDI XR4,$3,-20
# 0 "" 2
# 392 "rv9_p1.c" 1
.word	0b01110010000110000100100101111101	#S32SFL XR5,XR2,XR1,XR6,PTN2
# 0 "" 2
# 393 "rv9_p1.c" 1
.word	0b01110010001000001101000111111101	#S32SFL XR7,XR4,XR3,XR8,PTN2
# 0 "" 2
# 394 "rv9_p1.c" 1
.word	0b01110011001010011010001001111101	#S32SFL XR9,XR8,XR6,XR10,PTN3
# 0 "" 2
# 395 "rv9_p1.c" 1
.word	0b01110011001100010101111011111101	#S32SFL XR11,XR7,XR5,XR12,PTN3
# 0 "" 2
# 397 "rv9_p1.c" 1
.word	0b01110000010011111111000001010101	#S32SDI XR1,$2,-16
# 0 "" 2
# 398 "rv9_p1.c" 1
.word	0b01110000010011111111000010010101	#S32SDI XR2,$2,-16
# 0 "" 2
# 399 "rv9_p1.c" 1
.word	0b01110000010011111111000011010101	#S32SDI XR3,$2,-16
# 0 "" 2
# 400 "rv9_p1.c" 1
.word	0b01110000010011111111000100010101	#S32SDI XR4,$2,-16
# 0 "" 2
#NO_APP
addiu	$6,$6,1
slt	$7,$10,$6
beq	$7,$0,$L63
addu	$2,$24,$4
addiu	$4,$4,4
.set	noreorder
.set	nomacro
bne	$4,$25,$L64
addu	$2,$20,$2
.set	macro
.set	reorder

li	$10,96			# 0x60
move	$24,$0
li	$25,1			# 0x1
$L68:
lw	$4,8($19)
addu	$4,$4,$10
.set	noreorder
.set	nomacro
beq	$24,$0,$L65
addiu	$6,$4,96
.set	macro
.set	reorder

addiu	$2,$9,128
addiu	$3,$9,136
$L66:
li	$7,2			# 0x2
$L67:
#APP
# 417 "rv9_p1.c" 1
.word	0b01110000100011111111010001010100	#S32LDI XR1,$4,-12
# 0 "" 2
# 418 "rv9_p1.c" 1
.word	0b01110000100011111111010010010100	#S32LDI XR2,$4,-12
# 0 "" 2
# 419 "rv9_p1.c" 1
.word	0b01110000100011111111010011010100	#S32LDI XR3,$4,-12
# 0 "" 2
# 420 "rv9_p1.c" 1
.word	0b01110000100011111111010100010100	#S32LDI XR4,$4,-12
# 0 "" 2
# 422 "rv9_p1.c" 1
.word	0b01110010000110000100100101111101	#S32SFL XR5,XR2,XR1,XR6,PTN2
# 0 "" 2
# 423 "rv9_p1.c" 1
.word	0b01110010001000001101000111111101	#S32SFL XR7,XR4,XR3,XR8,PTN2
# 0 "" 2
# 424 "rv9_p1.c" 1
.word	0b01110011001010011010001001111101	#S32SFL XR9,XR8,XR6,XR10,PTN3
# 0 "" 2
# 425 "rv9_p1.c" 1
.word	0b01110011001100010101111011111101	#S32SFL XR11,XR7,XR5,XR12,PTN3
# 0 "" 2
# 427 "rv9_p1.c" 1
.word	0b01110000010011111111000001010101	#S32SDI XR1,$2,-16
# 0 "" 2
# 428 "rv9_p1.c" 1
.word	0b01110000010011111111000010010101	#S32SDI XR2,$2,-16
# 0 "" 2
# 429 "rv9_p1.c" 1
.word	0b01110000010011111111000011010101	#S32SDI XR3,$2,-16
# 0 "" 2
# 430 "rv9_p1.c" 1
.word	0b01110000010011111111000100010101	#S32SDI XR4,$2,-16
# 0 "" 2
# 432 "rv9_p1.c" 1
.word	0b01110000110011111111010001010100	#S32LDI XR1,$6,-12
# 0 "" 2
# 433 "rv9_p1.c" 1
.word	0b01110000110011111111010010010100	#S32LDI XR2,$6,-12
# 0 "" 2
# 434 "rv9_p1.c" 1
.word	0b01110000110011111111010011010100	#S32LDI XR3,$6,-12
# 0 "" 2
# 435 "rv9_p1.c" 1
.word	0b01110000110011111111010100010100	#S32LDI XR4,$6,-12
# 0 "" 2
# 437 "rv9_p1.c" 1
.word	0b01110010000110000100100101111101	#S32SFL XR5,XR2,XR1,XR6,PTN2
# 0 "" 2
# 438 "rv9_p1.c" 1
.word	0b01110010001000001101000111111101	#S32SFL XR7,XR4,XR3,XR8,PTN2
# 0 "" 2
# 439 "rv9_p1.c" 1
.word	0b01110011001010011010001001111101	#S32SFL XR9,XR8,XR6,XR10,PTN3
# 0 "" 2
# 440 "rv9_p1.c" 1
.word	0b01110011001100010101111011111101	#S32SFL XR11,XR7,XR5,XR12,PTN3
# 0 "" 2
# 442 "rv9_p1.c" 1
.word	0b01110000011011111111000001010101	#S32SDI XR1,$3,-16
# 0 "" 2
# 443 "rv9_p1.c" 1
.word	0b01110000011011111111000010010101	#S32SDI XR2,$3,-16
# 0 "" 2
# 444 "rv9_p1.c" 1
.word	0b01110000011011111111000011010101	#S32SDI XR3,$3,-16
# 0 "" 2
# 445 "rv9_p1.c" 1
.word	0b01110000011011111111000100010101	#S32SDI XR4,$3,-16
# 0 "" 2
#NO_APP
bne	$7,$25,$L87
.set	noreorder
.set	nomacro
bne	$24,$7,$L88
addiu	$10,$10,4
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$22,$2,$L69
andi	$7,$23,0xffff
.set	macro
.set	reorder

lw	$10,36($sp)
addiu	$2,$10,1
lbu	$3,3($21)
xor	$2,$2,$3
sltu	$2,$2,1
li	$3,-201326592			# 0xfffffffff4000000
lw	$7,16432($3)
addiu	$3,$3,16384
lw	$24,96($sp)
sll	$4,$24,8
lw	$24,100($sp)
mul	$25,$24,$10
addu	$4,$25,$4
addu	$4,$7,$4
lw	$10,96($sp)
sll	$6,$10,7
lw	$10,108($sp)
lw	$24,36($sp)
mul	$25,$10,$24
addu	$6,$25,$6
lw	$3,52($3)
.set	noreorder
.set	nomacro
beq	$8,$0,$L70
addu	$6,$3,$6
.set	macro
.set	reorder

addiu	$3,$8,-1
lw	$24,40($sp)
sll	$10,$24,8
lw	$24,100($sp)
mul	$25,$3,$24
addu	$3,$25,$10
addu	$3,$7,$3
$L71:
andi	$7,$13,0xffff
or	$7,$7,$17
sw	$7,0($18)
sw	$4,4($18)
addiu	$2,$2,3
sll	$2,$2,6
andi	$4,$2,0x3fc0
sll	$4,$4,16
addiu	$7,$2,-192
andi	$7,$7,0x3fc0
or	$4,$4,$7
sw	$4,8($18)
andi	$4,$2,0x7fc0
sll	$4,$4,16
or	$2,$4,$2
sw	$2,12($18)
lhu	$2,32($sp)
or	$2,$2,$17
sw	$2,16($18)
sw	$6,20($18)
andi	$7,$23,0xffff
or	$7,$7,$17
sw	$7,32($18)
addiu	$3,$3,192
sw	$3,36($18)
li	$2,321585152			# 0x132b0000
addiu	$2,$2,8
sw	$2,48($18)
addiu	$2,$17,26200
sw	$2,52($18)
li	$2,262144			# 0x40000
addiu	$2,$2,4
sw	$2,56($18)
li	$2,-2147221504			# 0xffffffff80040000
addiu	$2,$2,4
sw	$2,60($18)
andi	$2,$18,0xffff
or	$2,$2,$17
li	$3,320995328			# 0x13220000
sw	$2,0($3)
li	$2,-201326592			# 0xfffffffff4000000
addiu	$4,$2,26200
$L143:
lw	$3,26200($2)
bne	$3,$0,$L143
li	$2,1			# 0x1
sw	$2,0($4)
li	$3,320995328			# 0x13220000
sw	$2,4($3)
$L83:
lw	$4,4($19)
$L136:
addiu	$4,$4,-4
lw	$3,16($sp)
addiu	$10,$3,-24
lw	$3,8($19)
addiu	$3,$3,-4
lw	$24,16($sp)
addiu	$2,$24,304
li	$6,4			# 0x4
$L76:
#APP
# 547 "rv9_p1.c" 1
.word	0b01110000100000000001010001010100	#S32LDI XR1,$4,20
# 0 "" 2
# 548 "rv9_p1.c" 1
.word	0b01110000011000000000110010010100	#S32LDI XR2,$3,12
# 0 "" 2
# 549 "rv9_p1.c" 1
.word	0b01110000100000000001010011010100	#S32LDI XR3,$4,20
# 0 "" 2
# 550 "rv9_p1.c" 1
.word	0b01110000011000000000110100010100	#S32LDI XR4,$3,12
# 0 "" 2
# 551 "rv9_p1.c" 1
.word	0b01110000100000000001010101010100	#S32LDI XR5,$4,20
# 0 "" 2
# 552 "rv9_p1.c" 1
.word	0b01110000011000000000110110010100	#S32LDI XR6,$3,12
# 0 "" 2
# 553 "rv9_p1.c" 1
.word	0b01110000100000000001010111010100	#S32LDI XR7,$4,20
# 0 "" 2
# 554 "rv9_p1.c" 1
.word	0b01110000011000000000111000010100	#S32LDI XR8,$3,12
# 0 "" 2
# 556 "rv9_p1.c" 1
.word	0b01110001010000000001010001010101	#S32SDI XR1,$10,20
# 0 "" 2
# 557 "rv9_p1.c" 1
.word	0b01110000010000000000110010010101	#S32SDI XR2,$2,12
# 0 "" 2
# 558 "rv9_p1.c" 1
.word	0b01110001010000000001010011010101	#S32SDI XR3,$10,20
# 0 "" 2
# 559 "rv9_p1.c" 1
.word	0b01110000010000000000110100010101	#S32SDI XR4,$2,12
# 0 "" 2
# 560 "rv9_p1.c" 1
.word	0b01110001010000000001010101010101	#S32SDI XR5,$10,20
# 0 "" 2
# 561 "rv9_p1.c" 1
.word	0b01110000010000000000110110010101	#S32SDI XR6,$2,12
# 0 "" 2
# 562 "rv9_p1.c" 1
.word	0b01110001010000000001010111010101	#S32SDI XR7,$10,20
# 0 "" 2
# 563 "rv9_p1.c" 1
.word	0b01110000010000000000111000010101	#S32SDI XR8,$2,12
# 0 "" 2
#NO_APP
addiu	$6,$6,-1
bne	$6,$0,$L76
lw	$25,28($sp)
lhu	$3,0($25)
or	$3,$3,$17
li	$2,321323008			# 0x13270000
addiu	$4,$2,136
#APP
# 565 "rv9_p1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,-201326592			# 0xfffffffff4000000
sw	$0,22012($3)
addiu	$2,$2,132
li	$3,1			# 0x1
#APP
# 567 "rv9_p1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
$L75:
lbu	$2,21($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L77
li	$10,4			# 0x4
.set	macro
.set	reorder

lbu	$10,20($16)
xori	$10,$10,0x64
sltu	$10,$10,1
sll	$10,$10,2
$L77:
lbu	$4,22($16)
sltu	$4,$4,1
lw	$2,0($19)
lw	$24,0($16)
ori	$6,$24,0xffff
move	$3,$24
lw	$25,48($sp)
#APP
# 577 "rv9_p1.c" 1
movn	$3,$6,$25	#i_movn
# 0 "" 2
#NO_APP
li	$6,16711680			# 0xff0000
ori	$6,$6,0xffff
or	$6,$24,$6
lw	$24,44($sp)
#APP
# 578 "rv9_p1.c" 1
movn	$3,$6,$24	#i_movn
# 0 "" 2
#NO_APP
addiu	$6,$23,136
andi	$6,$6,0xffff
or	$6,$6,$17
sw	$6,8($2)
addiu	$6,$23,128
andi	$6,$6,0xffff
or	$6,$6,$17
sw	$6,12($2)
sw	$7,16($2)
andi	$5,$5,0xffff
or	$5,$5,$17
sw	$5,20($2)
andi	$14,$14,0xffff
or	$14,$14,$17
sw	$14,24($2)
sw	$15,28($2)
lh	$5,28($16)
sll	$5,$5,16
lh	$6,30($16)
or	$5,$5,$6
sw	$5,32($2)
lh	$5,26($16)
sw	$5,36($2)
lb	$5,17($16)
sll	$5,$5,24
lw	$6,8($16)
or	$5,$5,$6
sw	$5,40($2)
lb	$5,18($16)
sll	$5,$5,24
lw	$6,12($16)
or	$5,$5,$6
sw	$5,44($2)
lb	$5,16($16)
sll	$5,$5,24
or	$3,$3,$5
sw	$3,48($2)
lbu	$3,20($16)
sll	$3,$3,16
lbu	$5,28($21)
sll	$5,$5,24
or	$3,$3,$5
ori	$3,$3,0x2
sll	$4,$4,3
or	$4,$3,$4
lbu	$3,0($21)
addiu	$3,$3,-1
sll	$3,$3,4
or	$3,$4,$3
or	$10,$3,$10
sw	$10,52($2)
li	$2,-201326592			# 0xfffffffff4000000
addiu	$4,$2,26204
$L144:
lw	$3,26204($2)
bne	$3,$0,$L144
li	$2,1			# 0x1
sw	$2,0($4)
addiu	$12,$12,880
li	$3,321585152			# 0x132b0000
addiu	$2,$3,15505
sltu	$2,$12,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L131
addiu	$4,$22,3
.set	macro
.set	reorder

addiu	$12,$3,4128
$L131:
move	$5,$12
li	$3,-201326592			# 0xfffffffff4000000
$L145:
lw	$2,16512($3)
slt	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L145
li	$2,321585152			# 0x132b0000
.set	macro
.set	reorder

sw	$5,4($2)
sw	$5,23448($3)
lhu	$4,20($sp)
or	$4,$4,$17
sw	$4,23452($3)
li	$4,57671680			# 0x3700000
addiu	$4,$4,880
sw	$4,23460($3)
addiu	$2,$2,12
sw	$2,23464($3)
addiu	$2,$17,26204
sw	$2,23468($3)
li	$2,262144			# 0x40000
addiu	$2,$2,4
sw	$2,23472($3)
li	$2,-2147221504			# 0xffffffff80040000
addiu	$2,$2,4
sw	$2,23476($3)
li	$3,1			# 0x1
li	$2,320929792			# 0x13210000
sw	$3,4($2)
lb	$2,24($11)
.set	noreorder
.set	nomacro
bltz	$2,$L52
addiu	$22,$22,1
.set	macro
.set	reorder

lb	$3,23($11)
lw	$5,52($sp)
sw	$18,52($sp)
move	$4,$11
lw	$11,56($sp)
lw	$25,20($sp)
sw	$25,56($sp)
lw	$6,28($sp)
sw	$19,28($sp)
lw	$7,60($sp)
lw	$10,16($sp)
sw	$10,60($sp)
sw	$fp,16($sp)
lw	$10,64($sp)
sw	$13,64($sp)
move	$13,$20
lw	$14,68($sp)
lw	$15,32($sp)
sw	$15,68($sp)
sw	$9,32($sp)
lw	$24,40($sp)
sw	$24,96($sp)
.set	noreorder
.set	nomacro
bltz	$3,$L52
sw	$8,36($sp)
.set	macro
.set	reorder

lw	$8,80($sp)
lw	$9,76($sp)
sw	$9,80($sp)
lw	$15,92($sp)
sw	$15,40($sp)
lw	$24,72($sp)
sw	$24,92($sp)
move	$9,$14
move	$20,$10
move	$fp,$7
lw	$3,88($sp)
lw	$25,24($sp)
sw	$25,88($sp)
sw	$3,24($sp)
lw	$3,84($sp)
sw	$23,84($sp)
move	$23,$3
move	$19,$6
sw	$16,20($sp)
move	$16,$4
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
bne	$2,$3,$L53
move	$18,$5
.set	macro
.set	reorder

$L133:
move	$2,$21
lw	$21,104($sp)
sw	$2,104($sp)
lui	$2,%hi(mc_flag)
sw	$0,%lo(mc_flag)($2)
lb	$2,16($16)
andi	$3,$2,0x00ff
sltu	$3,$3,2
sw	$3,44($sp)
xori	$2,$2,0xb
sltu	$2,$2,1
sw	$2,48($sp)
lbu	$3,21($16)
sw	$3,72($sp)
lbu	$7,22($16)
sw	$7,76($sp)
lbu	$2,1($21)
bne	$2,$0,$L134
$L54:
lui	$25,%hi(last_mc_flag)
$L142:
lw	$2,%lo(last_mc_flag)($25)
.set	noreorder
.set	nomacro
bne	$2,$0,$L146
li	$4,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L135
li	$3,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

$L87:
.set	noreorder
.set	nomacro
j	$L67
li	$7,1			# 0x1
.set	macro
.set	reorder

$L88:
.set	noreorder
.set	nomacro
j	$L68
li	$24,1			# 0x1
.set	macro
.set	reorder

$L69:
.set	noreorder
.set	nomacro
j	$L83
or	$7,$7,$17
.set	macro
.set	reorder

$L60:
andi	$7,$23,0xffff
.set	noreorder
.set	nomacro
beq	$22,$0,$L75
or	$7,$7,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L136
lw	$4,4($19)
.set	macro
.set	reorder

$L70:
.set	noreorder
.set	nomacro
j	$L71
addiu	$3,$7,-256
.set	macro
.set	reorder

$L52:
li	$3,320929792			# 0x13210000
$L51:
lw	$2,4($3)
andi	$2,$2,0x4
beq	$2,$0,$L51
li	$3,320995328			# 0x13220000
$L117:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L117
li	$2,321585152			# 0x132b0000
.set	macro
.set	reorder

li	$3,1			# 0x1
sw	$3,0($2)
#APP
# 643 "rv9_p1.c" 1
nop	#i_nop
# 0 "" 2
# 644 "rv9_p1.c" 1
nop	#i_nop
# 0 "" 2
# 645 "rv9_p1.c" 1
nop	#i_nop
# 0 "" 2
# 646 "rv9_p1.c" 1
nop	#i_nop
# 0 "" 2
# 647 "rv9_p1.c" 1
wait	#i_wait
# 0 "" 2
#NO_APP
move	$2,$0
lw	$31,172($sp)
lw	$fp,168($sp)
lw	$23,164($sp)
lw	$22,160($sp)
lw	$21,156($sp)
lw	$20,152($sp)
lw	$19,148($sp)
lw	$18,144($sp)
lw	$17,140($sp)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,176
.set	macro
.set	reorder

$L65:
lw	$7,32($sp)
addiu	$2,$7,132
.set	noreorder
.set	nomacro
j	$L66
addiu	$3,$7,140
.set	macro
.set	reorder

.end	main
.size	main, .-main
.globl	last_mc_flag
.section	.bss,"aw",@nobits
.align	2
.type	last_mc_flag, @object
.size	last_mc_flag, 4
last_mc_flag:
.space	4
.globl	mc_flag
.align	2
.type	mc_flag, @object
.size	mc_flag, 4
mc_flag:
.space	4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
