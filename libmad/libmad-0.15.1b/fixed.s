.file	1 "fixed.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.globl	mad_f_abs
.set	nomips16
.set	nomicromips
.ent	mad_f_abs
.type	mad_f_abs, @function
mad_f_abs:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
subu	$2,$0,$4
slt	$3,$4,0
j	$31
movz	$2,$4,$3

.set	macro
.set	reorder
.end	mad_f_abs
.size	mad_f_abs, .-mad_f_abs
.align	2
.globl	mad_f_div
.set	nomips16
.set	nomicromips
.ent	mad_f_div
.type	mad_f_div, @function
mad_f_div:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
teq	$5,$0,7
div	$0,$4,$5
mflo	$2
subu	$6,$0,$2
slt	$3,$2,0
movn	$2,$6,$3
bltz	$4,$L22
move	$6,$2

teq	$5,$0,7
div	$0,$4,$5
mfhi	$7
bltz	$5,$L23
move	$3,$7

$L6:
slt	$8,$2,8
beq	$8,$0,$L7
nop

beq	$7,$0,$L20
li	$2,28			# 0x1c

$L18:
sll	$3,$3,1
addiu	$2,$2,-1
slt	$7,$3,$5
bne	$7,$0,$L11
sll	$6,$6,1

subu	$3,$3,$5
addiu	$6,$6,1
$L11:
beq	$2,$0,$L24
addiu	$7,$6,1

bne	$3,$0,$L18
nop

$L9:
addiu	$7,$6,1
slt	$3,$3,$5
srl	$4,$4,31
movz	$6,$7,$3
subu	$3,$0,$4
xor	$6,$6,$3
addu	$6,$6,$4
j	$31
sll	$2,$6,$2

$L7:
li	$3,8			# 0x8
beq	$2,$3,$L25
nop

$L17:
j	$31
move	$2,$0

$L22:
subu	$4,$0,$4
subu	$5,$0,$5
teq	$5,$0,7
div	$0,$4,$5
mfhi	$7
bgez	$5,$L6
move	$3,$7

$L23:
subu	$4,$0,$4
.option	pic0
j	$L6
.option	pic2
subu	$5,$0,$5

$L24:
sll	$3,$3,1
slt	$3,$3,$5
srl	$4,$4,31
movz	$6,$7,$3
subu	$3,$0,$4
xor	$6,$6,$3
addu	$6,$6,$4
j	$31
sll	$2,$6,$2

$L25:
bne	$7,$0,$L17
nop

bgez	$4,$L17
nop

li	$2,28			# 0x1c
$L20:
.option	pic0
j	$L9
.option	pic2
move	$3,$0

.set	macro
.set	reorder
.end	mad_f_div
.size	mad_f_div, .-mad_f_div
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
