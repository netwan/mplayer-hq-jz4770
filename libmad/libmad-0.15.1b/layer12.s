.file	1 "layer12.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	I_sample
.type	I_sample, @function
I_sample:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,28($sp)
sw	$16,24($sp)
.cprestore	16
lw	$25,%call16(mad_bit_read)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$16,$5

addiu	$8,$16,-1
li	$3,1			# 0x1
lw	$31,28($sp)
li	$7,29			# 0x1d
sll	$3,$3,$8
subu	$5,$7,$16
xor	$2,$2,$3
and	$4,$2,$3
addiu	$6,$16,-2
lw	$16,24($sp)
lui	$7,%hi(linear_table)
subu	$4,$0,$4
addiu	$7,$7,%lo(linear_table)
sll	$6,$6,2
or	$2,$4,$2
sll	$3,$2,$5
addu	$6,$6,$7
li	$2,268435456			# 0x10000000
addiu	$sp,$sp,32
sra	$2,$2,$8
lw	$4,0($6)
addu	$2,$2,$3
mult	$2,$4
mflo	$4
mfhi	$5
srl	$3,$4,28
sll	$2,$5,4
j	$31
or	$2,$2,$3

.set	macro
.set	reorder
.end	I_sample
.size	I_sample, .-I_sample
.align	2
.set	nomips16
.set	nomicromips
.ent	II_samples
.type	II_samples, @function
II_samples:
.frame	$sp,72,$31		# vars= 16, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-72
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$18,48($sp)
move	$18,$5
sw	$17,44($sp)
sw	$31,68($sp)
sw	$22,64($sp)
sw	$21,60($sp)
sw	$20,56($sp)
sw	$19,52($sp)
sw	$16,40($sp)
.cprestore	16
lbu	$20,2($5)
bne	$20,$0,$L12
move	$17,$6

addiu	$16,$sp,24
lbu	$20,3($5)
addiu	$22,$sp,36
move	$21,$4
move	$19,$16
$L6:
lw	$25,%call16(mad_bit_read)($28)
move	$4,$21
move	$5,$20
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
addiu	$19,$19,4

lw	$28,16($sp)
bne	$19,$22,$L6
sw	$2,-4($19)

$L5:
addiu	$7,$20,-1
li	$2,1			# 0x1
move	$4,$0
sll	$7,$2,$7
li	$2,29			# 0x1d
li	$9,12			# 0xc
subu	$20,$2,$20
$L7:
addu	$3,$16,$4
lw	$2,8($18)
lw	$8,4($18)
addu	$5,$17,$4
addiu	$4,$4,4
lw	$3,0($3)
xor	$6,$7,$3
and	$3,$6,$7
subu	$3,$0,$3
or	$3,$3,$6
sll	$3,$3,$20
addu	$2,$3,$2
mult	$2,$8
mflo	$2
mfhi	$3
srl	$2,$2,28
sll	$3,$3,4
or	$2,$3,$2
bne	$4,$9,$L7
sw	$2,0($5)

lw	$31,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,72

$L12:
lw	$25,%call16(mad_bit_read)($28)
addiu	$16,$sp,24
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
lbu	$5,3($5)

lhu	$3,0($18)
teq	$3,$0,7
divu	$0,$2,$3
mflo	$4
mfhi	$2
teq	$3,$0,7
divu	$0,$4,$3
sw	$2,24($sp)
mflo	$2
mfhi	$5
teq	$3,$0,7
divu	$0,$2,$3
sw	$5,28($sp)
mfhi	$4
.option	pic0
j	$L5
.option	pic2
sw	$4,32($sp)

.set	macro
.set	reorder
.end	II_samples
.size	II_samples, .-II_samples
.align	2
.globl	mad_layer_I
.set	nomips16
.set	nomicromips
.ent	mad_layer_I
.type	mad_layer_I, @function
mad_layer_I:
.frame	$sp,224,$31		# vars= 160, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$2,4($5)
addiu	$sp,$sp,-224
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$22,208($sp)
move	$22,$4
sw	$31,220($sp)
sw	$fp,216($sp)
sw	$23,212($sp)
sw	$21,204($sp)
sw	$20,200($sp)
sw	$19,196($sp)
sw	$18,192($sp)
sw	$17,188($sp)
sw	$16,184($sp)
.cprestore	16
bne	$2,$0,$L14
sw	$5,228($sp)

li	$3,32			# 0x20
lw	$2,28($5)
li	$19,1			# 0x1
sw	$3,152($sp)
$L15:
andi	$2,$2,0x10
bne	$2,$0,$L77
lw	$3,228($sp)

$L20:
lw	$3,152($sp)
$L79:
beq	$3,$0,$L70
addiu	$18,$22,28

addiu	$17,$sp,88
sll	$16,$19,5
addu	$3,$17,$3
li	$21,15			# 0xf
move	$20,$17
sw	$3,156($sp)
$L27:
move	$23,$0
$L26:
lw	$25,%call16(mad_bit_read)($28)
li	$5,4			# 0x4
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$18

move	$4,$0
lw	$28,16($sp)
beq	$2,$21,$L29
addiu	$5,$2,1

beq	$2,$0,$L78
addu	$2,$20,$23

andi	$4,$5,0x00ff
$L78:
addiu	$23,$23,32
bne	$23,$16,$L26
sb	$4,0($2)

lw	$3,156($sp)
addiu	$20,$20,1
bne	$20,$3,$L27
lw	$3,152($sp)

sltu	$2,$3,32
beq	$2,$0,$L28
move	$21,$3

$L22:
addiu	$16,$21,32
li	$20,15			# 0xf
.option	pic0
j	$L31
.option	pic2
addu	$16,$17,$16

$L72:
beq	$2,$0,$L30
nop

andi	$3,$4,0x00ff
$L30:
addiu	$21,$21,1
sb	$3,0($16)
sb	$3,-32($16)
sltu	$2,$21,32
beq	$2,$0,$L71
addiu	$16,$16,1

$L31:
lw	$25,%call16(mad_bit_read)($28)
li	$5,4			# 0x4
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$18

move	$3,$0
lw	$28,16($sp)
bne	$2,$20,$L72
addiu	$4,$2,1

$L29:
li	$3,529			# 0x211
li	$2,-1			# 0xffffffffffffffff
sw	$3,60($22)
$L66:
lw	$31,220($sp)
lw	$fp,216($sp)
lw	$23,212($sp)
lw	$22,208($sp)
lw	$21,204($sp)
lw	$20,200($sp)
lw	$19,196($sp)
lw	$18,192($sp)
lw	$17,188($sp)
lw	$16,184($sp)
j	$31
addiu	$sp,$sp,224

$L14:
li	$3,2			# 0x2
bne	$2,$3,$L73
lw	$3,228($sp)

li	$19,2			# 0x2
lw	$23,8($3)
lw	$2,28($3)
addiu	$23,$23,1
ori	$2,$2,0x100
sll	$23,$23,2
sw	$2,28($3)
andi	$2,$2,0x10
beq	$2,$0,$L20
sw	$23,152($sp)

lw	$3,228($sp)
$L77:
li	$6,1073676288			# 0x3fff0000
lw	$25,%call16(mad_bit_crc)($28)
ori	$6,$6,0xffff
lw	$4,28($22)
addu	$6,$19,$6
lw	$5,32($22)
lhu	$7,24($3)
lw	$3,152($sp)
mul	$6,$6,$3
addiu	$6,$6,32
.reloc	1f,R_MIPS_JALR,mad_bit_crc
1:	jalr	$25
sll	$6,$6,2

lw	$4,228($sp)
lw	$28,16($sp)
sh	$2,24($4)
lhu	$3,26($4)
beq	$3,$2,$L79
lw	$3,152($sp)

lw	$2,44($4)
andi	$2,$2,0x1
bne	$2,$0,$L20
li	$3,513			# 0x201

li	$2,-1			# 0xffffffffffffffff
.option	pic0
j	$L66
.option	pic2
sw	$3,60($22)

$L73:
li	$19,2			# 0x2
lw	$2,28($3)
li	$3,32			# 0x20
.option	pic0
j	$L15
.option	pic2
sw	$3,152($sp)

$L71:
sll	$16,$19,5
$L28:
move	$22,$0
addiu	$fp,$sp,24
li	$21,32			# 0x20
move	$20,$0
$L33:
addu	$23,$20,$22
addu	$2,$17,$23
lbu	$2,0($2)
bne	$2,$0,$L74
addiu	$20,$20,32

$L32:
bne	$20,$16,$L33
nop

addiu	$22,$22,1
bne	$22,$21,$L33
move	$20,$0

lw	$3,228($sp)
lui	$20,%hi(sf_table)
addiu	$21,$sp,24
sw	$0,160($sp)
addiu	$20,$20,%lo(sf_table)
addiu	$3,$3,48
li	$22,2			# 0x2
sw	$3,156($sp)
lw	$3,228($sp)
addiu	$3,$3,1584
sw	$3,164($sp)
lw	$3,152($sp)
sltu	$3,$3,32
sw	$3,168($sp)
lw	$3,152($sp)
$L81:
beq	$3,$0,$L39
move	$fp,$0

lw	$fp,156($sp)
move	$8,$0
$L38:
move	$23,$0
move	$6,$fp
$L37:
addu	$7,$23,$8
addu	$4,$17,$7
lbu	$5,0($4)
bne	$5,$0,$L75
move	$2,$0

$L36:
addiu	$23,$23,32
sw	$2,0($6)
bne	$23,$16,$L37
addiu	$6,$6,4608

lw	$3,152($sp)
addiu	$8,$8,1
bne	$8,$3,$L38
addiu	$fp,$fp,4

move	$fp,$3
lw	$3,168($sp)
beq	$3,$0,$L80
lw	$3,156($sp)

$L39:
lw	$3,160($sp)
addiu	$6,$3,12
lw	$3,228($sp)
addu	$6,$6,$fp
sll	$6,$6,2
.option	pic0
j	$L45
.option	pic2
addu	$23,$3,$6

$L76:
bne	$19,$22,$L42
sw	$0,0($23)

sw	$0,4608($23)
$L42:
addiu	$fp,$fp,1
sltu	$2,$fp,32
beq	$2,$0,$L46
addiu	$23,$23,4

$L45:
addu	$2,$17,$fp
lbu	$5,0($2)
beq	$5,$0,$L76
nop

.option	pic0
jal	I_sample
.option	pic2
move	$4,$18

move	$7,$0
move	$8,$23
$L43:
addu	$4,$7,$fp
addiu	$8,$8,4608
addu	$4,$21,$4
addiu	$7,$7,32
lbu	$4,0($4)
sll	$4,$4,2
addu	$4,$20,$4
lw	$4,0($4)
mult	$4,$2
mflo	$4
mfhi	$5
srl	$4,$4,28
sll	$5,$5,4
or	$4,$5,$4
bne	$16,$7,$L43
sw	$4,-4608($8)

addiu	$fp,$fp,1
sltu	$2,$fp,32
bne	$2,$0,$L45
addiu	$23,$23,4

$L46:
lw	$3,156($sp)
$L80:
lw	$4,164($sp)
addiu	$3,$3,128
sw	$3,156($sp)
lw	$3,160($sp)
addiu	$3,$3,32
sw	$3,160($sp)
lw	$3,156($sp)
bne	$3,$4,$L81
lw	$3,152($sp)

.option	pic0
j	$L66
.option	pic2
move	$2,$0

$L74:
lw	$25,%call16(mad_bit_read)($28)
li	$5,6			# 0x6
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$18

addu	$3,$fp,$23
lw	$28,16($sp)
.option	pic0
j	$L32
.option	pic2
sb	$2,0($3)

$L75:
move	$4,$18
sw	$6,176($sp)
sw	$7,180($sp)
.option	pic0
jal	I_sample
.option	pic2
sw	$8,172($sp)

lw	$7,180($sp)
lw	$6,176($sp)
lw	$8,172($sp)
addu	$7,$21,$7
lbu	$4,0($7)
sll	$4,$4,2
addu	$4,$20,$4
lw	$10,0($4)
mult	$2,$10
mflo	$2
mfhi	$3
srl	$2,$2,28
sll	$4,$3,4
.option	pic0
j	$L36
.option	pic2
or	$2,$4,$2

$L70:
move	$21,$0
.option	pic0
j	$L22
.option	pic2
addiu	$17,$sp,88

.set	macro
.set	reorder
.end	mad_layer_I
.size	mad_layer_I, .-mad_layer_I
.align	2
.globl	mad_layer_II
.set	nomips16
.set	nomicromips
.ent	mad_layer_II
.type	mad_layer_II, @function
mad_layer_II:
.frame	$sp,480,$31		# vars= 416, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-480
lui	$28,%hi(__gnu_local_gp)
sw	$23,468($sp)
move	$23,$5
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$4,480($sp)
sw	$31,476($sp)
sw	$fp,472($sp)
sw	$22,464($sp)
sw	$21,460($sp)
sw	$20,456($sp)
sw	$19,452($sp)
sw	$18,448($sp)
sw	$17,444($sp)
sw	$16,440($sp)
.cprestore	16
lw	$4,28($23)
lw	$5,4($5)
.set	noreorder
.set	nomacro
beq	$5,$0,$L83
andi	$2,$4,0x1000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L195
andi	$2,$4,0x400
.set	macro
.set	reorder

li	$22,2			# 0x2
li	$2,4			# 0x4
$L84:
sll	$3,$2,2
sll	$2,$2,5
addu	$2,$3,$2
lui	$3,%hi(sbquant_table)
addiu	$3,$3,%lo(sbquant_table)
addu	$2,$2,$3
addiu	$3,$2,4
lw	$fp,0($2)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$5,$2,$L89
sw	$3,368($sp)
.set	macro
.set	reorder

$L201:
li	$2,32			# 0x20
sltu	$3,$fp,33
movn	$2,$fp,$3
sw	$2,372($sp)
$L90:
lw	$4,480($sp)
lw	$3,28($4)
lw	$2,32($4)
sw	$3,356($sp)
sw	$2,360($sp)
lw	$3,372($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L196
move	$20,$0
.set	macro
.set	reorder

$L91:
sltu	$2,$20,$fp
.set	noreorder
.set	nomacro
beq	$2,$0,$L98
lw	$3,480($sp)
.set	macro
.set	reorder

lui	$16,%hi(bitalloc_table)
addu	$18,$sp,$20
lw	$19,368($sp)
addiu	$16,$16,%lo(bitalloc_table)
addiu	$17,$3,28
addiu	$18,$18,312
$L97:
addu	$2,$19,$20
lw	$25,%call16(mad_bit_read)($28)
move	$4,$17
addiu	$20,$20,1
lbu	$2,0($2)
addiu	$18,$18,1
sll	$2,$2,2
addu	$2,$16,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
lhu	$5,0($2)
.set	macro
.set	reorder

sltu	$3,$20,$fp
andi	$2,$2,0x00ff
lw	$28,16($sp)
sb	$2,-1($18)
.set	noreorder
.set	nomacro
bne	$3,$0,$L97
sb	$2,-33($18)
.set	macro
.set	reorder

$L98:
.set	noreorder
.set	nomacro
beq	$fp,$0,$L197
addiu	$3,$sp,280
.set	macro
.set	reorder

sll	$17,$22,5
move	$16,$0
sw	$3,380($sp)
addiu	$20,$sp,216
lw	$3,480($sp)
addiu	$21,$3,28
$L101:
move	$18,$0
lw	$3,380($sp)
$L207:
addu	$19,$18,$16
addu	$2,$3,$19
lbu	$2,0($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L198
addiu	$18,$18,32
.set	macro
.set	reorder

$L99:
.set	noreorder
.set	nomacro
bne	$18,$17,$L207
lw	$3,380($sp)
.set	macro
.set	reorder

addiu	$16,$16,1
bne	$16,$fp,$L101
lw	$2,28($23)
andi	$2,$2,0x10
.set	noreorder
.set	nomacro
bne	$2,$0,$L199
lw	$3,480($sp)
.set	macro
.set	reorder

$L104:
lw	$3,480($sp)
addiu	$6,$sp,24
addiu	$7,$sp,216
sw	$17,400($sp)
move	$18,$0
addiu	$3,$3,28
sw	$6,392($sp)
sw	$7,388($sp)
move	$17,$18
sw	$6,376($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L114
.option	pic2
sw	$3,384($sp)
.set	macro
.set	reorder

$L107:
addiu	$21,$21,1
addiu	$20,$20,32
addiu	$18,$18,96
.set	noreorder
.set	nomacro
bne	$21,$22,$L113
addiu	$19,$19,32
.set	macro
.set	reorder

lw	$6,376($sp)
addiu	$17,$17,1
sltu	$2,$17,$fp
addiu	$6,$6,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L200
sw	$6,376($sp)
.set	macro
.set	reorder

$L114:
sll	$16,$17,1
lw	$3,380($sp)
addiu	$4,$sp,216
lw	$18,376($sp)
addu	$16,$16,$17
addu	$20,$3,$17
addu	$19,$4,$17
move	$21,$0
sw	$16,396($sp)
$L113:
lbu	$2,0($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L107
lw	$25,%call16(mad_bit_read)($28)
.set	macro
.set	reorder

li	$5,6			# 0x6
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
lw	$4,384($sp)
.set	macro
.set	reorder

li	$5,1			# 0x1
lbu	$16,0($19)
andi	$2,$2,0x00ff
lw	$28,16($sp)
.set	noreorder
.set	nomacro
beq	$16,$5,$L109
sb	$2,0($18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L110
li	$6,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$6,$L111
li	$7,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$7,$L208
lw	$25,%call16(mad_bit_read)($28)
.set	macro
.set	reorder

$L108:
andi	$2,$16,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L107
sll	$3,$21,7
.set	macro
.set	reorder

sll	$2,$21,5
addiu	$4,$sp,24
subu	$2,$3,$2
lw	$3,396($sp)
addu	$2,$3,$2
addu	$2,$4,$2
addu	$2,$2,$16
lbu	$2,-1($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L107
.option	pic2
sb	$2,1($18)
.set	macro
.set	reorder

$L198:
lw	$25,%call16(mad_bit_read)($28)
li	$5,2			# 0x2
move	$4,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
addu	$19,$20,$19
.set	macro
.set	reorder

lw	$28,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L99
.option	pic2
sb	$2,0($19)
.set	macro
.set	reorder

$L110:
lw	$25,%call16(mad_bit_read)($28)
li	$5,6			# 0x6
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
lw	$4,384($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
sb	$2,1($18)
$L109:
lw	$25,%call16(mad_bit_read)($28)
$L208:
li	$5,6			# 0x6
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
lw	$4,384($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L108
.option	pic2
sb	$2,2($18)
.set	macro
.set	reorder

$L195:
.set	noreorder
.set	nomacro
bne	$2,$0,$L150
li	$22,2			# 0x2
.set	macro
.set	reorder

lw	$2,16($23)
srl	$2,$2,1
li	$3,48001			# 0xbb81
$L209:
sltu	$6,$2,$3
.set	noreorder
.set	nomacro
beq	$6,$0,$L87
addiu	$3,$3,32000
.set	macro
.set	reorder

lw	$2,20($23)
li	$6,3			# 0x3
xori	$3,$2,0x7d00
li	$2,2			# 0x2
movz	$2,$6,$3
sll	$3,$2,2
sll	$2,$2,5
addu	$2,$3,$2
lui	$3,%hi(sbquant_table)
addiu	$3,$3,%lo(sbquant_table)
addu	$2,$2,$3
addiu	$3,$2,4
lw	$fp,0($2)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$5,$2,$L201
sw	$3,368($sp)
.set	macro
.set	reorder

$L89:
lw	$2,8($23)
ori	$4,$4,0x100
sw	$4,28($23)
addiu	$2,$2,1
sll	$2,$2,2
sltu	$3,$fp,$2
movn	$2,$fp,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L90
.option	pic2
sw	$2,372($sp)
.set	macro
.set	reorder

$L83:
.set	noreorder
.set	nomacro
beq	$2,$0,$L202
andi	$2,$4,0x400
.set	macro
.set	reorder

lw	$4,480($sp)
li	$fp,30			# 0x1e
li	$22,1			# 0x1
li	$20,30			# 0x1e
lw	$3,28($4)
lw	$2,32($4)
lui	$4,%hi(sbquant_table+148)
addiu	$4,$4,%lo(sbquant_table+148)
sw	$3,356($sp)
sw	$2,360($sp)
sw	$4,368($sp)
$L142:
lw	$3,480($sp)
lui	$16,%hi(bitalloc_table)
addiu	$21,$sp,280
lw	$18,368($sp)
addiu	$16,$16,%lo(bitalloc_table)
sw	$20,372($sp)
addiu	$17,$3,28
addu	$19,$21,$20
move	$20,$17
$L93:
lbu	$2,0($18)
move	$4,$20
lw	$25,%call16(mad_bit_read)($28)
addiu	$18,$18,1
sll	$2,$2,2
addu	$2,$16,$2
lhu	$17,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

li	$6,2			# 0x2
lw	$28,16($sp)
move	$4,$20
move	$5,$17
sb	$2,0($21)
.set	noreorder
.set	nomacro
bne	$22,$6,$L92
lw	$25,%call16(mad_bit_read)($28)
.set	macro
.set	reorder

.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
lw	$28,16($sp)
sb	$2,32($21)
$L92:
addiu	$21,$21,1
bne	$21,$19,$L93
.option	pic0
.set	noreorder
.set	nomacro
j	$L91
.option	pic2
lw	$20,372($sp)
.set	macro
.set	reorder

$L202:
.set	noreorder
.set	nomacro
bne	$2,$0,$L88
li	$22,1			# 0x1
.set	macro
.set	reorder

lw	$2,16($23)
li	$3,131072			# 0x20000
ori	$3,$3,0xee01
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L138
li	$3,546			# 0x222
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L209
.option	pic2
li	$3,48001			# 0xbb81
.set	macro
.set	reorder

$L200:
lw	$17,400($sp)
$L105:
lw	$3,372($sp)
sll	$2,$22,12
lw	$6,372($sp)
addiu	$7,$23,176
sll	$5,$22,7
sw	$0,380($sp)
sll	$4,$3,1
sw	$0,384($sp)
sll	$3,$22,9
sw	$7,392($sp)
addu	$4,$4,$6
sw	$23,416($sp)
addu	$2,$3,$2
sll	$7,$fp,2
sw	$4,408($sp)
addiu	$3,$sp,24
subu	$4,$5,$17
sw	$2,404($sp)
lui	$16,%hi(sf_table)
sw	$7,400($sp)
sltu	$2,$6,$fp
sw	$3,396($sp)
sltu	$18,$fp,32
sw	$4,376($sp)
addiu	$16,$16,%lo(sf_table)
sw	$2,412($sp)
sw	$18,420($sp)
$L133:
lw	$3,372($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L131
lw	$7,384($sp)
.set	macro
.set	reorder

addiu	$3,$sp,24
lw	$2,392($sp)
addiu	$21,$sp,280
move	$23,$0
sw	$fp,424($sp)
srl	$13,$7,2
lw	$7,380($sp)
addiu	$14,$2,-128
sw	$22,428($sp)
addu	$13,$3,$13
sll	$7,$7,27
move	$20,$21
move	$22,$23
sw	$7,388($sp)
move	$fp,$14
.option	pic0
.set	noreorder
.set	nomacro
j	$L118
.option	pic2
move	$21,$13
.set	macro
.set	reorder

$L115:
sw	$0,0($23)
sw	$0,128($23)
sw	$0,256($23)
$L116:
addiu	$19,$19,32
addiu	$9,$9,96
.set	noreorder
.set	nomacro
bne	$19,$17,$L117
addiu	$23,$23,4608
.set	macro
.set	reorder

lw	$3,372($sp)
addiu	$22,$22,1
addiu	$21,$21,3
.set	noreorder
.set	nomacro
beq	$22,$3,$L203
addiu	$fp,$fp,4
.set	macro
.set	reorder

$L118:
lw	$3,368($sp)
move	$19,$0
move	$23,$fp
move	$9,$21
addu	$18,$3,$22
$L117:
addu	$2,$19,$22
addu	$2,$20,$2
lbu	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L115
lw	$7,480($sp)
.set	macro
.set	reorder

addiu	$6,$sp,344
lbu	$3,0($18)
lbu	$5,0($9)
addiu	$4,$7,28
sw	$9,436($sp)
lui	$7,%hi(bitalloc_table)
sll	$3,$3,2
addiu	$7,$7,%lo(bitalloc_table)
sll	$5,$5,2
addu	$3,$3,$7
addu	$5,$16,$5
lhu	$8,2($3)
lw	$7,0($5)
lui	$5,%hi(offset_table)
sll	$3,$8,4
addiu	$5,$5,%lo(offset_table)
subu	$3,$3,$8
sw	$7,432($sp)
addu	$3,$3,$5
addu	$2,$3,$2
lbu	$2,-1($2)
sll	$5,$2,2
sll	$2,$2,4
subu	$5,$2,$5
lui	$2,%hi(qc_table)
addiu	$2,$2,%lo(qc_table)
.option	pic0
.set	noreorder
.set	nomacro
jal	II_samples
.option	pic2
addu	$5,$2,$5
.set	macro
.set	reorder

lw	$2,344($sp)
lw	$7,432($sp)
#APP
# 498 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010001110000100001100110	#S32MUL XR1,XR2,$2,$7
# 0 "" 2
#NO_APP
lw	$2,348($sp)
#APP
# 499 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010001110001000011100110	#S32MUL XR3,XR4,$2,$7
# 0 "" 2
#NO_APP
lw	$2,352($sp)
#APP
# 500 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010001110001100101100110	#S32MUL XR5,XR6,$2,$7
# 0 "" 2
#NO_APP
li	$2,4			# 0x4
#APP
# 501 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010111111000100001100110	#S32EXTR XR1,XR2,$2,31
# 0 "" 2
# 502 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010111111001000011100110	#S32EXTR XR3,XR4,$2,31
# 0 "" 2
# 503 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010111111001100101100110	#S32EXTR XR5,XR6,$2,31
# 0 "" 2
# 504 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010011001100010001110000	#D32SLL XR1,XR1,XR3,XR3,1
# 0 "" 2
# 505 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010000000001010101110000	#D32SLL XR5,XR5,XR0,XR0,1
# 0 "" 2
#NO_APP
lw	$3,388($sp)
addiu	$2,$3,-128
li	$3,32			# 0x20
addu	$2,$2,$23
#APP
# 506 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010000111000000001010111	#S32SDIV XR1,$2,$3,2
# 0 "" 2
# 507 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010000111000000011010111	#S32SDIV XR3,$2,$3,2
# 0 "" 2
# 508 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010000111000000101010111	#S32SDIV XR5,$2,$3,2
# 0 "" 2
#NO_APP
.option	pic0
.set	noreorder
.set	nomacro
j	$L116
.option	pic2
lw	$9,436($sp)
.set	macro
.set	reorder

$L203:
lw	$fp,424($sp)
lw	$22,428($sp)
$L131:
lw	$7,412($sp)
.set	noreorder
.set	nomacro
beq	$7,$0,$L119
lw	$3,372($sp)
.set	macro
.set	reorder

addiu	$21,$sp,280
lw	$7,384($sp)
lw	$18,372($sp)
addiu	$2,$3,1164
lw	$3,380($sp)
srl	$10,$7,2
lw	$7,408($sp)
sw	$17,388($sp)
addu	$2,$3,$2
addu	$10,$10,$7
lw	$7,416($sp)
addiu	$3,$sp,24
sll	$2,$2,2
addu	$19,$3,$10
lw	$3,380($sp)
addu	$23,$7,$2
move	$17,$19
sll	$2,$3,27
lw	$3,480($sp)
move	$19,$2
move	$2,$21
addiu	$20,$3,28
move	$21,$fp
move	$fp,$18
.option	pic0
.set	noreorder
.set	nomacro
j	$L125
.option	pic2
move	$18,$2
.set	macro
.set	reorder

$L205:
li	$2,2			# 0x2
sw	$0,-4608($23)
sw	$0,-4480($23)
.set	noreorder
.set	nomacro
bne	$22,$2,$L124
sw	$0,-4352($23)
.set	macro
.set	reorder

sw	$0,0($23)
sw	$0,128($23)
sw	$0,256($23)
$L124:
addiu	$fp,$fp,1
addiu	$23,$23,4
sltu	$2,$fp,$21
.set	noreorder
.set	nomacro
beq	$2,$0,$L204
addiu	$17,$17,3
.set	macro
.set	reorder

$L125:
addu	$2,$18,$fp
lbu	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L205
lw	$4,368($sp)
.set	macro
.set	reorder

lui	$5,%hi(bitalloc_table)
lui	$7,%hi(offset_table)
addiu	$5,$5,%lo(bitalloc_table)
addu	$3,$4,$fp
addiu	$7,$7,%lo(offset_table)
addiu	$6,$sp,344
lbu	$3,0($3)
move	$4,$20
sll	$3,$3,2
addu	$3,$3,$5
lhu	$5,2($3)
sll	$3,$5,4
subu	$3,$3,$5
addu	$3,$3,$7
addu	$2,$3,$2
lbu	$2,-1($2)
sll	$5,$2,2
sll	$2,$2,4
subu	$5,$2,$5
lui	$2,%hi(qc_table)
addiu	$2,$2,%lo(qc_table)
.option	pic0
.set	noreorder
.set	nomacro
jal	II_samples
.option	pic2
addu	$5,$2,$5
.set	macro
.set	reorder

addiu	$3,$19,-4736
lw	$7,376($sp)
li	$6,4			# 0x4
addu	$3,$3,$23
li	$5,32			# 0x20
addu	$11,$7,$17
move	$4,$17
$L123:
lbu	$2,0($4)
lw	$7,344($sp)
sll	$2,$2,2
addu	$2,$16,$2
lw	$2,0($2)
#APP
# 539 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000111000100000100001100110	#S32MUL XR1,XR2,$7,$2
# 0 "" 2
#NO_APP
lw	$7,348($sp)
#APP
# 540 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000111000100001000011100110	#S32MUL XR3,XR4,$7,$2
# 0 "" 2
#NO_APP
lw	$7,352($sp)
#APP
# 541 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000111000100001100101100110	#S32MUL XR5,XR6,$7,$2
# 0 "" 2
# 542 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000110111111000100001100110	#S32EXTR XR1,XR2,$6,31
# 0 "" 2
# 543 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000110111111001000011100110	#S32EXTR XR3,XR4,$6,31
# 0 "" 2
# 544 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000110111111001100101100110	#S32EXTR XR5,XR6,$6,31
# 0 "" 2
# 545 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010011001100010001110000	#D32SLL XR1,XR1,XR3,XR3,1
# 0 "" 2
# 546 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010000000001010101110000	#D32SLL XR5,XR5,XR0,XR0,1
# 0 "" 2
#NO_APP
move	$2,$3
#APP
# 547 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010001011000000001010111	#S32SDIV XR1,$2,$5,2
# 0 "" 2
# 548 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010001011000000011010111	#S32SDIV XR3,$2,$5,2
# 0 "" 2
# 549 "libmad-0.15.1b/layer12.c" 1
.word	0b01110000010001011000000101010111	#S32SDIV XR5,$2,$5,2
# 0 "" 2
#NO_APP
addiu	$4,$4,96
.set	noreorder
.set	nomacro
bne	$4,$11,$L123
addiu	$3,$3,4608
.set	macro
.set	reorder

addiu	$fp,$fp,1
addiu	$23,$23,4
sltu	$2,$fp,$21
.set	noreorder
.set	nomacro
bne	$2,$0,$L125
addiu	$17,$17,3
.set	macro
.set	reorder

$L204:
lw	$17,388($sp)
move	$fp,$21
$L119:
lw	$2,392($sp)
lw	$3,404($sp)
lw	$20,420($sp)
move	$5,$2
addu	$7,$2,$3
lw	$2,400($sp)
addiu	$6,$2,-128
$L132:
li	$4,3			# 0x3
move	$3,$5
$L130:
beq	$20,$0,$L128
addu	$2,$6,$3
$L126:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L126
$L128:
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$4,$0,$L130
addiu	$3,$3,128
.set	macro
.set	reorder

addiu	$5,$5,4608
.set	noreorder
.set	nomacro
bne	$5,$7,$L132
lw	$2,384($sp)
.set	macro
.set	reorder

lw	$3,392($sp)
lw	$7,380($sp)
addiu	$2,$2,1
addiu	$3,$3,384
addiu	$7,$7,96
sw	$2,384($sp)
li	$2,12			# 0xc
sw	$3,392($sp)
sw	$7,380($sp)
lw	$3,384($sp)
.set	noreorder
.set	nomacro
bne	$3,$2,$L133
lw	$31,476($sp)
.set	macro
.set	reorder

move	$2,$0
lw	$fp,472($sp)
lw	$23,468($sp)
lw	$22,464($sp)
lw	$21,460($sp)
lw	$20,456($sp)
lw	$19,452($sp)
lw	$18,448($sp)
lw	$17,444($sp)
lw	$16,440($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,480
.set	macro
.set	reorder

$L150:
$L88:
lw	$2,20($23)
xori	$2,$2,0xbb80
.option	pic0
.set	noreorder
.set	nomacro
j	$L84
.option	pic2
sltu	$2,$0,$2
.set	macro
.set	reorder

$L111:
sb	$2,1($18)
.option	pic0
.set	noreorder
.set	nomacro
j	$L107
.option	pic2
sb	$2,2($18)
.set	macro
.set	reorder

$L87:
sltu	$2,$2,$3
beq	$2,$0,$L88
.option	pic0
.set	noreorder
.set	nomacro
j	$L84
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L138:
lw	$4,480($sp)
lw	$31,476($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$fp,472($sp)
lw	$23,468($sp)
lw	$22,464($sp)
lw	$21,460($sp)
lw	$20,456($sp)
lw	$19,452($sp)
lw	$18,448($sp)
lw	$17,444($sp)
lw	$16,440($sp)
sw	$3,60($4)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,480
.set	macro
.set	reorder

$L199:
addiu	$4,$sp,356
lw	$25,%call16(mad_bit_length)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_length
1:	jalr	$25
addiu	$5,$3,28
.set	macro
.set	reorder

lw	$28,16($sp)
move	$6,$2
lw	$4,356($sp)
lw	$5,360($sp)
lw	$25,%call16(mad_bit_crc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_crc
1:	jalr	$25
lhu	$7,24($23)
.set	macro
.set	reorder

lhu	$3,26($23)
lw	$28,16($sp)
.set	noreorder
.set	nomacro
beq	$3,$2,$L104
sh	$2,24($23)
.set	macro
.set	reorder

$L143:
lw	$2,44($23)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L103
lw	$4,480($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$fp,$0,$L186
addiu	$3,$sp,280
.set	macro
.set	reorder

sll	$17,$22,5
.option	pic0
.set	noreorder
.set	nomacro
j	$L104
.option	pic2
sw	$3,380($sp)
.set	macro
.set	reorder

$L197:
lw	$2,28($23)
andi	$2,$2,0x10
.set	noreorder
.set	nomacro
bne	$2,$0,$L206
lw	$3,480($sp)
.set	macro
.set	reorder

$L186:
.option	pic0
.set	noreorder
.set	nomacro
j	$L105
.option	pic2
sll	$17,$22,5
.set	macro
.set	reorder

$L196:
.option	pic0
.set	noreorder
.set	nomacro
j	$L142
.option	pic2
move	$20,$3
.set	macro
.set	reorder

$L103:
li	$3,513			# 0x201
lw	$31,476($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$fp,472($sp)
lw	$23,468($sp)
lw	$22,464($sp)
lw	$21,460($sp)
lw	$20,456($sp)
lw	$19,452($sp)
lw	$18,448($sp)
lw	$17,444($sp)
lw	$16,440($sp)
sw	$3,60($4)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,480
.set	macro
.set	reorder

$L206:
addiu	$4,$sp,356
lw	$25,%call16(mad_bit_length)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_length
1:	jalr	$25
addiu	$5,$3,28
.set	macro
.set	reorder

lw	$28,16($sp)
move	$6,$2
lw	$4,356($sp)
lw	$5,360($sp)
lw	$25,%call16(mad_bit_crc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mad_bit_crc
1:	jalr	$25
lhu	$7,24($23)
.set	macro
.set	reorder

lhu	$3,26($23)
lw	$28,16($sp)
.set	noreorder
.set	nomacro
bne	$3,$2,$L143
sh	$2,24($23)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L105
.option	pic2
sll	$17,$22,5
.set	macro
.set	reorder

.end	mad_layer_II
.size	mad_layer_II, .-mad_layer_II
.rdata
.align	2
.type	qc_table, @object
.size	qc_table, 204
qc_table:
.half	3
.byte	2
.byte	5
.word	357913941
.word	134217728
.half	5
.byte	3
.byte	7
.word	429496730
.word	134217728
.half	7
.byte	0
.byte	3
.word	306783378
.word	67108864
.half	9
.byte	4
.byte	10
.word	477218588
.word	134217728
.half	15
.byte	0
.byte	4
.word	286331153
.word	33554432
.half	31
.byte	0
.byte	5
.word	277094664
.word	16777216
.half	63
.byte	0
.byte	6
.word	272696336
.word	8388608
.half	127
.byte	0
.byte	7
.word	270549121
.word	4194304
.half	255
.byte	0
.byte	8
.word	269488144
.word	2097152
.half	511
.byte	0
.byte	9
.word	268960770
.word	1048576
.half	1023
.byte	0
.byte	10
.word	268697856
.word	524288
.half	2047
.byte	0
.byte	11
.word	268566592
.word	262144
.half	4095
.byte	0
.byte	12
.word	268501008
.word	131072
.half	8191
.byte	0
.byte	13
.word	268468228
.word	65536
.half	16383
.byte	0
.byte	14
.word	268451841
.word	32768
.half	32767
.byte	0
.byte	15
.word	268443648
.word	16384
.half	-1
.byte	0
.byte	16
.word	268439552
.word	8192
.align	2
.type	offset_table, @object
.size	offset_table, 90
offset_table:
.byte	0
.byte	1
.byte	16
.space	12
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	16
.space	8
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	0
.byte	1
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	16
.byte	0
.byte	2
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.align	2
.type	bitalloc_table, @object
.size	bitalloc_table, 32
bitalloc_table:
.half	2
.half	0
.half	2
.half	3
.half	3
.half	3
.half	3
.half	1
.half	4
.half	2
.half	4
.half	3
.half	4
.half	4
.half	4
.half	5
.align	2
.type	sbquant_table, @object
.size	sbquant_table, 180
sbquant_table:
.word	27
.byte	7
.byte	7
.byte	7
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.space	3
.space	2
.word	30
.byte	7
.byte	7
.byte	7
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	2
.word	8
.byte	5
.byte	5
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.space	22
.space	2
.word	12
.byte	5
.byte	5
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.space	18
.space	2
.word	30
.byte	4
.byte	4
.byte	4
.byte	4
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	2
.align	2
.type	linear_table, @object
.size	linear_table, 56
linear_table:
.word	357913941
.word	306783378
.word	286331153
.word	277094664
.word	272696336
.word	270549121
.word	269488144
.word	268960770
.word	268697856
.word	268566592
.word	268501008
.word	268468228
.word	268451841
.word	268443648
.align	2
.type	sf_table, @object
.size	sf_table, 256
sf_table:
.word	536870912
.word	426114725
.word	338207482
.word	268435456
.word	213057363
.word	169103741
.word	134217728
.word	106528681
.word	84551870
.word	67108864
.word	53264341
.word	42275935
.word	33554432
.word	26632170
.word	21137968
.word	16777216
.word	13316085
.word	10568984
.word	8388608
.word	6658043
.word	5284492
.word	4194304
.word	3329021
.word	2642246
.word	2097152
.word	1664511
.word	1321123
.word	1048576
.word	832255
.word	660561
.word	524288
.word	416128
.word	330281
.word	262144
.word	208064
.word	165140
.word	131072
.word	104032
.word	82570
.word	65536
.word	52016
.word	41285
.word	32768
.word	26008
.word	20643
.word	16384
.word	13004
.word	10321
.word	8192
.word	6502
.word	5161
.word	4096
.word	3251
.word	2580
.word	2048
.word	1625
.word	1290
.word	1024
.word	813
.word	645
.word	512
.word	406
.word	323
.word	0
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
