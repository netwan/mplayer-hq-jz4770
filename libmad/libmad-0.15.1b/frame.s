.file	1 "frame.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_header
.type	decode_header, @function
decode_header:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,28($sp)
addiu	$17,$5,28
sw	$18,32($sp)
move	$18,$5
sw	$16,24($sp)
li	$5,11			# 0xb
sw	$31,36($sp)
move	$16,$4
.cprestore	16
sw	$0,28($4)
move	$4,$17
lw	$25,%call16(mad_bit_skip)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_skip
1:	jalr	$25
sw	$0,32($16)

li	$5,1			# 0x1
lw	$28,16($sp)
lw	$25,%call16(mad_bit_read)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

bne	$2,$0,$L2
lw	$28,16($sp)

lw	$2,28($16)
ori	$2,$2,0x4000
sw	$2,28($16)
$L2:
lw	$25,%call16(mad_bit_read)($28)
li	$5,1			# 0x1
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

bne	$2,$0,$L3
lw	$28,16($sp)

lw	$2,28($16)
ori	$2,$2,0x1000
sw	$2,28($16)
lw	$25,%call16(mad_bit_read)($28)
$L46:
li	$5,2			# 0x2
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

li	$3,4			# 0x4
lw	$28,16($sp)
subu	$2,$3,$2
beq	$2,$3,$L41
sw	$2,0($16)

lw	$25,%call16(mad_bit_read)($28)
li	$5,1			# 0x1
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

beq	$2,$0,$L42
lw	$28,16($sp)

$L7:
lw	$25,%call16(mad_bit_read)($28)
li	$5,4			# 0x4
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

li	$3,15			# 0xf
beq	$2,$3,$L43
lw	$28,16($sp)

lw	$4,28($16)
andi	$4,$4,0x1000
bne	$4,$0,$L44
nop

lw	$3,0($16)
addiu	$3,$3,-1
sll	$4,$3,4
subu	$3,$4,$3
addu	$2,$3,$2
$L39:
sll	$3,$2,2
lw	$25,%call16(mad_bit_read)($28)
lui	$2,%hi(bitrate_table)
li	$5,2			# 0x2
addiu	$2,$2,%lo(bitrate_table)
move	$4,$17
addu	$2,$3,$2
lw	$2,0($2)
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
sw	$2,16($16)

li	$3,3			# 0x3
beq	$2,$3,$L45
lw	$28,16($sp)

lui	$3,%hi(samplerate_table)
lw	$4,28($16)
sll	$2,$2,2
addiu	$3,$3,%lo(samplerate_table)
andi	$5,$4,0x1000
addu	$2,$2,$3
lw	$2,0($2)
beq	$5,$0,$L13
sw	$2,20($16)

andi	$4,$4,0x4000
bne	$4,$0,$L14
srl	$3,$2,1

sw	$3,20($16)
$L13:
lw	$25,%call16(mad_bit_read)($28)
li	$5,1			# 0x1
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

beq	$2,$0,$L16
lw	$28,16($sp)

lw	$2,28($16)
ori	$2,$2,0x80
sw	$2,28($16)
$L16:
lw	$25,%call16(mad_bit_read)($28)
li	$5,1			# 0x1
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

beq	$2,$0,$L17
lw	$28,16($sp)

lw	$2,32($16)
ori	$2,$2,0x100
sw	$2,32($16)
$L17:
lw	$25,%call16(mad_bit_read)($28)
li	$5,2			# 0x2
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

li	$3,3			# 0x3
lw	$28,16($sp)
li	$5,2			# 0x2
subu	$2,$3,$2
move	$4,$17
lw	$25,%call16(mad_bit_read)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
sw	$2,4($16)

li	$5,1			# 0x1
lw	$28,16($sp)
move	$4,$17
lw	$25,%call16(mad_bit_read)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
sw	$2,8($16)

beq	$2,$0,$L18
lw	$28,16($sp)

lw	$2,28($16)
ori	$2,$2,0x20
sw	$2,28($16)
$L18:
lw	$25,%call16(mad_bit_read)($28)
li	$5,1			# 0x1
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

beq	$2,$0,$L19
lw	$28,16($sp)

lw	$2,28($16)
ori	$2,$2,0x40
sw	$2,28($16)
$L19:
lw	$25,%call16(mad_bit_read)($28)
li	$5,2			# 0x2
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

lw	$3,28($16)
lw	$28,16($sp)
andi	$3,$3,0x10
beq	$3,$0,$L5
sw	$2,12($16)

lw	$25,%call16(mad_bit_read)($28)
li	$5,16			# 0x10
.reloc	1f,R_MIPS_JALR,mad_bit_read
1:	jalr	$25
move	$4,$17

move	$3,$0
sh	$2,26($16)
$L5:
lw	$31,36($sp)
move	$2,$3
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

$L3:
lw	$2,28($16)
andi	$2,$2,0x4000
beq	$2,$0,$L46
lw	$25,%call16(mad_bit_read)($28)

li	$2,257			# 0x101
li	$3,-1			# 0xffffffffffffffff
.option	pic0
j	$L5
.option	pic2
sw	$2,60($18)

$L44:
lw	$4,0($16)
srl	$4,$4,1
addiu	$5,$4,3
sll	$4,$5,4
subu	$4,$4,$5
.option	pic0
j	$L39
.option	pic2
addu	$2,$4,$2

$L42:
lw	$2,28($16)
li	$6,16			# 0x10
lw	$25,%call16(mad_bit_crc)($28)
li	$7,65535			# 0xffff
lw	$4,28($18)
ori	$2,$2,0x10
lw	$5,32($18)
.reloc	1f,R_MIPS_JALR,mad_bit_crc
1:	jalr	$25
sw	$2,28($16)

lw	$28,16($sp)
.option	pic0
j	$L7
.option	pic2
sh	$2,24($16)

$L14:
srl	$2,$2,2
.option	pic0
j	$L13
.option	pic2
sw	$2,20($16)

$L41:
li	$2,258			# 0x102
li	$3,-1			# 0xffffffffffffffff
.option	pic0
j	$L5
.option	pic2
sw	$2,60($18)

$L43:
li	$2,259			# 0x103
li	$3,-1			# 0xffffffffffffffff
.option	pic0
j	$L5
.option	pic2
sw	$2,60($18)

$L45:
li	$2,260			# 0x104
li	$3,-1			# 0xffffffffffffffff
.option	pic0
j	$L5
.option	pic2
sw	$2,60($18)

.set	macro
.set	reorder
.end	decode_header
.size	decode_header, .-decode_header
.align	2
.globl	mad_header_init
.set	nomips16
.set	nomicromips
.ent	mad_header_init
.type	mad_header_init, @function
mad_header_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
sw	$0,0($4)
sw	$0,4($4)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$0,8($4)
sw	$0,12($4)
sw	$0,16($4)
lw	$2,%got(mad_timer_zero)($28)
sw	$0,20($4)
sh	$0,24($4)
sh	$0,26($4)
sw	$0,28($4)
sw	$0,32($4)
lw	$3,0($2)
lw	$2,4($2)
sw	$3,36($4)
j	$31
sw	$2,40($4)

.set	macro
.set	reorder
.end	mad_header_init
.size	mad_header_init, .-mad_header_init
.align	2
.globl	mad_frame_finish
.set	nomips16
.set	nomicromips
.ent	mad_frame_finish
.type	mad_frame_finish, @function
mad_frame_finish:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,9264($4)
beq	$2,$0,$L56
nop

lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,28($sp)
sw	$16,24($sp)
move	$16,$4
.cprestore	16
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
move	$4,$2

lw	$31,28($sp)
sw	$0,9264($16)
lw	$16,24($sp)
addiu	$sp,$sp,32
$L56:
j	$31
nop

.set	macro
.set	reorder
.end	mad_frame_finish
.size	mad_frame_finish, .-mad_frame_finish
.align	2
.globl	mad_header_decode
.set	nomips16
.set	nomicromips
.ent	mad_header_decode
.type	mad_header_decode, @function
mad_header_decode:
.frame	$sp,192,$31		# vars= 128, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-192
lui	$28,%hi(__gnu_local_gp)
sw	$16,152($sp)
move	$16,$5
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$20,168($sp)
sw	$31,188($sp)
sw	$fp,184($sp)
sw	$23,180($sp)
sw	$22,176($sp)
sw	$21,172($sp)
sw	$19,164($sp)
sw	$18,160($sp)
sw	$17,156($sp)
.cprestore	16
lw	$5,24($5)
beq	$5,$0,$L119
lw	$20,4($16)

lw	$2,8($16)
bne	$2,$0,$L60
move	$17,$4

lw	$2,12($16)
$L61:
beq	$2,$0,$L64
addiu	$18,$16,28

subu	$2,$20,$5
slt	$2,$2,8
bne	$2,$0,$L117
li	$2,255			# 0xff

lbu	$3,0($5)
bne	$3,$2,$L131
addiu	$2,$5,1

lbu	$2,1($5)
li	$3,224			# 0xe0
andi	$2,$2,0xe0
bne	$2,$3,$L67
addiu	$2,$5,1

$L68:
addiu	$2,$5,1
lw	$25,%call16(mad_bit_init)($28)
move	$4,$18
sw	$5,20($16)
.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jalr	$25
sw	$2,24($16)

move	$4,$17
.option	pic0
jal	decode_header
.option	pic2
move	$5,$16

li	$3,-1			# 0xffffffffffffffff
beq	$2,$3,$L59
lw	$28,16($sp)

lw	$2,0($17)
li	$3,1			# 0x1
beq	$2,$3,$L101
addiu	$4,$17,36

li	$3,3			# 0x3
beq	$2,$3,$L120
li	$3,576			# 0x240

li	$6,1152			# 0x480
$L71:
lw	$25,%call16(mad_timer_set)($28)
move	$5,$0
.reloc	1f,R_MIPS_JALR,mad_timer_set
1:	jalr	$25
lw	$7,20($17)

lw	$19,16($17)
beq	$19,$0,$L72
lw	$28,16($sp)

lw	$6,28($17)
li	$2,1			# 0x1
lw	$4,0($17)
srl	$3,$6,7
beq	$4,$2,$L121
andi	$5,$3,0x1

$L91:
li	$2,3			# 0x3
beq	$4,$2,$L122
li	$2,72			# 0x48

li	$2,144			# 0x90
$L93:
mul	$19,$2,$19
lw	$3,20($17)
teq	$3,$0,7
divu	$0,$19,$3
mflo	$3
addu	$3,$3,$5
lw	$5,20($16)
addiu	$4,$3,8
subu	$2,$20,$5
sltu	$4,$2,$4
bne	$4,$0,$L117
nop

$L94:
addu	$3,$5,$3
lw	$2,12($16)
bne	$2,$0,$L95
sw	$3,24($16)

lbu	$4,0($3)
li	$2,255			# 0xff
beq	$4,$2,$L123
nop

addiu	$5,$5,1
$L132:
sw	$5,24($16)
$L64:
lw	$25,%call16(mad_bit_init)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jalr	$25
move	$4,$18

lw	$28,16($sp)
lw	$25,%call16(mad_stream_sync)($28)
.reloc	1f,R_MIPS_JALR,mad_stream_sync
1:	jalr	$25
move	$4,$16

li	$3,-1			# 0xffffffffffffffff
beq	$2,$3,$L124
lw	$28,16($sp)

lw	$25,%call16(mad_bit_nextbyte)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_nextbyte
1:	jalr	$25
move	$4,$18

lw	$28,16($sp)
.option	pic0
j	$L68
.option	pic2
move	$5,$2

$L123:
lbu	$2,1($3)
li	$3,224			# 0xe0
andi	$2,$2,0xe0
bne	$2,$3,$L132
addiu	$5,$5,1

addiu	$5,$5,-1
li	$2,1			# 0x1
sw	$2,12($16)
$L95:
lw	$31,188($sp)
ori	$6,$6,0x8
lw	$fp,184($sp)
move	$2,$0
lw	$23,180($sp)
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$16,152($sp)
sw	$6,28($17)
lw	$17,156($sp)
j	$31
addiu	$sp,$sp,192

$L72:
lw	$19,16($16)
beq	$19,$0,$L74
nop

lw	$2,12($16)
beq	$2,$0,$L74
nop

lw	$4,0($17)
li	$2,3			# 0x3
beq	$4,$2,$L125
li	$2,589824			# 0x90000

$L75:
lw	$6,28($17)
li	$2,1			# 0x1
sw	$19,16($17)
ori	$6,$6,0x400
srl	$3,$6,7
sw	$6,28($17)
bne	$4,$2,$L91
andi	$5,$3,0x1

$L121:
sll	$2,$19,2
lw	$3,20($17)
sll	$19,$19,4
subu	$19,$19,$2
teq	$3,$0,7
divu	$0,$19,$3
mflo	$19
addu	$3,$5,$19
lw	$5,20($16)
sll	$3,$3,2
subu	$2,$20,$5
addiu	$4,$3,8
sltu	$4,$2,$4
beq	$4,$0,$L94
nop

$L117:
sw	$5,24($16)
li	$2,1			# 0x1
$L136:
sw	$2,60($16)
$L59:
lw	$31,188($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$fp,184($sp)
lw	$23,180($sp)
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$17,156($sp)
sw	$0,12($16)
lw	$16,152($sp)
j	$31
addiu	$sp,$sp,192

$L74:
lw	$3,28($17)
li	$4,3			# 0x3
lhu	$6,34($16)
lw	$5,0($17)
srl	$2,$3,7
lw	$fp,28($16)
lhu	$23,32($16)
sw	$6,136($sp)
beq	$5,$4,$L100
andi	$2,$2,0x1

li	$3,144			# 0x90
$L133:
sw	$3,140($sp)
$L78:
sll	$3,$2,2
lw	$25,%call16(mad_stream_sync)($28)
li	$21,1			# 0x1
li	$4,4			# 0x4
subu	$2,$21,$2
subu	$4,$4,$3
move	$19,$0
sw	$2,144($sp)
addiu	$22,$sp,24
sw	$4,148($sp)
.reloc	1f,R_MIPS_JALR,mad_stream_sync
1:	jalr	$25
move	$4,$16

bne	$2,$0,$L126
lw	$28,16($sp)

$L87:
addiu	$4,$16,64
move	$2,$16
move	$3,$22
$L80:
lw	$9,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$8,-12($2)
lw	$7,-8($2)
lw	$6,-4($2)
sw	$9,-16($3)
sw	$8,-12($3)
sw	$7,-8($3)
bne	$2,$4,$L80
sw	$6,-4($3)

addiu	$4,$sp,88
addiu	$5,$17,32
move	$2,$17
move	$3,$4
$L81:
lw	$9,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$8,-12($2)
lw	$7,-8($2)
lw	$6,-4($2)
sw	$9,-16($3)
sw	$8,-12($3)
sw	$7,-8($3)
bne	$2,$5,$L81
sw	$6,-4($3)

lw	$7,0($2)
move	$5,$22
lw	$6,4($2)
lw	$2,8($2)
sw	$7,0($3)
sw	$6,4($3)
.option	pic0
jal	decode_header
.option	pic2
sw	$2,8($3)

bne	$2,$0,$L82
lw	$28,16($sp)

lw	$2,0($17)
lw	$3,88($sp)
beq	$3,$2,$L127
lw	$3,108($sp)

$L82:
lw	$25,%call16(mad_bit_skip)($28)
$L134:
li	$5,8			# 0x8
.reloc	1f,R_MIPS_JALR,mad_bit_skip
1:	jalr	$25
move	$4,$18

lw	$28,16($sp)
lw	$25,%call16(mad_stream_sync)($28)
.reloc	1f,R_MIPS_JALR,mad_stream_sync
1:	jalr	$25
move	$4,$16

beq	$2,$0,$L87
lw	$28,16($sp)

$L126:
lw	$6,136($sp)
sltu	$2,$19,8
sw	$fp,28($16)
sh	$23,32($16)
bne	$2,$0,$L90
sh	$6,34($16)

lw	$4,0($17)
$L86:
li	$2,3			# 0x3
beq	$4,$2,$L128
sltu	$2,$19,641

$L89:
sll	$3,$19,2
sll	$2,$19,7
subu	$2,$2,$3
addu	$19,$2,$19
sll	$19,$19,3
.option	pic0
j	$L75
.option	pic2
sw	$19,16($16)

$L125:
ori	$2,$2,0xc401
sltu	$2,$19,$2
bne	$2,$0,$L75
nop

lw	$3,28($17)
lhu	$6,34($16)
lw	$fp,28($16)
srl	$2,$3,7
lhu	$23,32($16)
sw	$6,136($sp)
andi	$2,$2,0x1
$L100:
andi	$3,$3,0x1000
beq	$3,$0,$L133
li	$3,144			# 0x90

li	$6,72			# 0x48
.option	pic0
j	$L78
.option	pic2
sw	$6,140($sp)

$L127:
lw	$2,20($17)
bne	$3,$2,$L134
lw	$25,%call16(mad_bit_skip)($28)

lw	$25,%call16(mad_bit_nextbyte)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_nextbyte
1:	jalr	$25
move	$4,$18

lw	$3,20($16)
lw	$4,0($17)
lw	$28,16($sp)
beq	$4,$21,$L129
subu	$2,$2,$3

lw	$6,144($sp)
lw	$5,20($17)
addu	$3,$6,$2
mul	$2,$3,$5
lw	$3,140($sp)
teq	$3,$0,7
divu	$0,$2,$3
li	$2,274857984			# 0x10620000
ori	$2,$2,0x4dd3
mflo	$19
multu	$19,$2
mfhi	$19
srl	$19,$19,6
$L84:
sltu	$2,$19,8
bne	$2,$0,$L134
lw	$25,%call16(mad_bit_skip)($28)

lw	$3,136($sp)
sw	$fp,28($16)
sh	$23,32($16)
.option	pic0
j	$L86
.option	pic2
sh	$3,34($16)

$L101:
.option	pic0
j	$L71
.option	pic2
li	$6,384			# 0x180

$L120:
lw	$6,28($17)
andi	$2,$6,0x1000
li	$6,1152			# 0x480
.option	pic0
j	$L71
.option	pic2
movn	$6,$3,$2

$L122:
li	$3,144			# 0x90
andi	$4,$6,0x1000
.option	pic0
j	$L93
.option	pic2
movz	$2,$3,$4

$L128:
bne	$2,$0,$L89
nop

$L90:
li	$2,257			# 0x101
.option	pic0
j	$L59
.option	pic2
sw	$2,60($16)

$L60:
lw	$3,12($16)
bne	$3,$0,$L135
subu	$3,$20,$5

lw	$5,20($16)
subu	$3,$20,$5
$L135:
sltu	$3,$3,$2
bne	$3,$0,$L130
li	$3,1			# 0x1

sw	$0,8($16)
addu	$5,$5,$2
li	$2,1			# 0x1
.option	pic0
j	$L61
.option	pic2
sw	$3,12($16)

$L129:
lw	$6,148($sp)
lw	$19,20($17)
addu	$3,$2,$6
li	$2,91619328			# 0x5760000
mul	$19,$3,$19
ori	$2,$2,0x19f1
multu	$19,$2
mfhi	$19
.option	pic0
j	$L84
.option	pic2
srl	$19,$19,10

$L67:
$L131:
sw	$5,20($16)
sw	$2,24($16)
li	$2,257			# 0x101
.option	pic0
j	$L59
.option	pic2
sw	$2,60($16)

$L124:
lw	$2,24($16)
subu	$2,$20,$2
slt	$2,$2,8
bne	$2,$0,$L136
li	$2,1			# 0x1

addiu	$20,$20,-8
sw	$20,24($16)
.option	pic0
j	$L59
.option	pic2
sw	$2,60($16)

$L130:
subu	$5,$5,$20
sw	$20,24($16)
addu	$2,$5,$2
sw	$3,60($16)
.option	pic0
j	$L59
.option	pic2
sw	$2,8($16)

$L119:
li	$2,2			# 0x2
.option	pic0
j	$L59
.option	pic2
sw	$2,60($16)

.set	macro
.set	reorder
.end	mad_header_decode
.size	mad_header_decode, .-mad_header_decode
.align	2
.globl	mad_frame_decode
.set	nomips16
.set	nomicromips
.ent	mad_frame_decode
.type	mad_frame_decode, @function
mad_frame_decode:
.frame	$sp,48,$31		# vars= 8, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$3,28($4)
lw	$6,56($5)
addiu	$sp,$sp,-48
addiu	$28,$28,%lo(__gnu_local_gp)
andi	$2,$3,0x8
sw	$17,40($sp)
sw	$16,36($sp)
move	$17,$5
sw	$31,44($sp)
move	$16,$4
.cprestore	16
bne	$2,$0,$L138
sw	$6,44($4)

.option	pic0
jal	mad_header_decode
nop

.option	pic2
li	$3,-1			# 0xffffffffffffffff
beq	$2,$3,$L141
nop

lw	$3,28($16)
$L138:
lw	$2,0($16)
li	$4,-9			# 0xfffffffffffffff7
move	$5,$16
and	$4,$3,$4
addiu	$2,$2,-1
sw	$4,28($16)
sll	$3,$2,2
lui	$2,%hi(decoder_table)
addiu	$2,$2,%lo(decoder_table)
addu	$2,$3,$2
lw	$25,0($2)
jalr	$25
move	$4,$17

li	$3,-1			# 0xffffffffffffffff
beq	$2,$3,$L145
lw	$28,16($sp)

lw	$3,0($16)
li	$2,3			# 0x3
beq	$3,$2,$L143
lw	$25,%call16(mad_bit_init)($28)

addiu	$16,$sp,24
lw	$5,24($17)
.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jalr	$25
move	$4,$16

addiu	$4,$17,28
lw	$28,16($sp)
move	$5,$16
lw	$3,28($17)
lw	$2,32($17)
lw	$25,%call16(mad_bit_length)($28)
sw	$3,36($17)
.reloc	1f,R_MIPS_JALR,mad_bit_length
1:	jalr	$25
sw	$2,40($17)

move	$3,$0
sw	$2,44($17)
$L142:
lw	$31,44($sp)
move	$2,$3
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,48

$L143:
lw	$31,44($sp)
move	$3,$0
lw	$17,40($sp)
lw	$16,36($sp)
move	$2,$3
j	$31
addiu	$sp,$sp,48

$L145:
lw	$2,60($17)
andi	$2,$2,0xff00
bne	$2,$0,$L141
nop

lw	$2,20($17)
sw	$2,24($17)
$L141:
sw	$0,44($17)
.option	pic0
j	$L142
.option	pic2
li	$3,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	mad_frame_decode
.size	mad_frame_decode, .-mad_frame_decode
.align	2
.globl	mad_frame_mute
.set	nomips16
.set	nomicromips
.ent	mad_frame_mute
.type	mad_frame_mute, @function
mad_frame_mute:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$2,$4,4656
addiu	$5,$4,9264
$L147:
addiu	$3,$2,128
$L148:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L148
sw	$0,-4612($2)

bne	$5,$2,$L147
nop

lw	$5,9264($4)
beq	$5,$0,$L159
addiu	$4,$5,2304

addiu	$5,$5,2376
addiu	$3,$4,2304
$L158:
move	$2,$4
$L152:
sw	$0,0($2)
addiu	$2,$2,72
bne	$2,$3,$L152
sw	$0,-2376($2)

addiu	$4,$4,4
bne	$4,$5,$L158
addiu	$3,$4,2304

$L159:
j	$31
nop

.set	macro
.set	reorder
.end	mad_frame_mute
.size	mad_frame_mute, .-mad_frame_mute
.align	2
.globl	mad_frame_init
.set	nomips16
.set	nomicromips
.ent	mad_frame_init
.type	mad_frame_init, @function
mad_frame_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
sw	$0,0($4)
sw	$0,4($4)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$0,8($4)
sw	$0,12($4)
sw	$0,16($4)
lw	$3,%got(mad_timer_zero)($28)
sw	$0,20($4)
sh	$0,24($4)
sh	$0,26($4)
sw	$0,28($4)
sw	$0,32($4)
sw	$0,44($4)
sw	$0,9264($4)
lw	$5,0($3)
lw	$3,4($3)
sw	$5,36($4)
.option	pic0
j	mad_frame_mute
.option	pic2
sw	$3,40($4)

.set	macro
.set	reorder
.end	mad_frame_init
.size	mad_frame_init, .-mad_frame_init
.section	.data.rel.ro,"aw",@progbits
.align	2
.type	decoder_table, @object
.size	decoder_table, 12
decoder_table:
.word	mad_layer_I
.word	mad_layer_II
.word	mad_layer_III
.rdata
.align	2
.type	samplerate_table, @object
.size	samplerate_table, 12
samplerate_table:
.word	44100
.word	48000
.word	32000
.align	2
.type	bitrate_table, @object
.size	bitrate_table, 300
bitrate_table:
.word	0
.word	32000
.word	64000
.word	96000
.word	128000
.word	160000
.word	192000
.word	224000
.word	256000
.word	288000
.word	320000
.word	352000
.word	384000
.word	416000
.word	448000
.word	0
.word	32000
.word	48000
.word	56000
.word	64000
.word	80000
.word	96000
.word	112000
.word	128000
.word	160000
.word	192000
.word	224000
.word	256000
.word	320000
.word	384000
.word	0
.word	32000
.word	40000
.word	48000
.word	56000
.word	64000
.word	80000
.word	96000
.word	112000
.word	128000
.word	160000
.word	192000
.word	224000
.word	256000
.word	320000
.word	0
.word	32000
.word	48000
.word	56000
.word	64000
.word	80000
.word	96000
.word	112000
.word	128000
.word	144000
.word	160000
.word	176000
.word	192000
.word	224000
.word	256000
.word	0
.word	8000
.word	16000
.word	24000
.word	32000
.word	40000
.word	48000
.word	56000
.word	64000
.word	80000
.word	96000
.word	112000
.word	128000
.word	144000
.word	160000
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
