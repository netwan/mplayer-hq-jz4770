.file	1 "decoder.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	run_sync
.type	run_sync, @function
run_sync:
.frame	$sp,80,$31		# vars= 16, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$2,28($4)
addiu	$sp,$sp,-80
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,76($sp)
sw	$fp,72($sp)
sw	$23,68($sp)
sw	$22,64($sp)
sw	$21,60($sp)
sw	$20,56($sp)
sw	$19,52($sp)
sw	$18,48($sp)
sw	$17,44($sp)
sw	$16,40($sp)
.cprestore	16
beq	$2,$0,$L21
sw	$0,24($sp)

lw	$19,44($4)
beq	$19,$0,$L22
addiu	$23,$sp,24

lw	$23,24($4)
$L3:
lw	$16,20($4)
move	$fp,$4
lw	$25,%call16(mad_stream_init)($28)
li	$18,17			# 0x11
li	$20,32			# 0x20
.reloc	1f,R_MIPS_JALR,mad_stream_init
1:	jalr	$25
move	$4,$16

addiu	$17,$16,64
lw	$28,16($sp)
addiu	$22,$16,9332
move	$4,$17
lw	$25,%call16(mad_frame_init)($28)
.reloc	1f,R_MIPS_JALR,mad_frame_init
1:	jalr	$25
li	$21,16			# 0x10

addiu	$3,$16,13432
lw	$28,16($sp)
move	$4,$22
lw	$25,%call16(mad_synth_init)($28)
.reloc	1f,R_MIPS_JALR,mad_synth_init
1:	jalr	$25
sw	$3,32($sp)

lw	$2,4($fp)
sw	$2,56($16)
$L20:
lw	$25,28($fp)
move	$5,$16
jalr	$25
lw	$4,24($fp)

beq	$2,$18,$L46
lw	$28,16($sp)

beq	$2,$20,$L52
nop

beq	$2,$21,$L28
nop

$L4:
lw	$2,32($fp)
beq	$2,$0,$L9
lw	$25,%call16(mad_header_decode)($28)

move	$4,$17
.reloc	1f,R_MIPS_JALR,mad_header_decode
1:	jalr	$25
move	$5,$16

li	$25,-1			# 0xffffffffffffffff
beq	$2,$25,$L53
lw	$28,16($sp)

lw	$25,32($fp)
move	$5,$17
jalr	$25
lw	$4,24($fp)

beq	$2,$18,$L46
lw	$28,16($sp)

beq	$2,$20,$L4
nop

beq	$2,$21,$L28
nop

$L9:
lw	$25,%call16(mad_frame_decode)($28)
move	$4,$17
.reloc	1f,R_MIPS_JALR,mad_frame_decode
1:	jalr	$25
move	$5,$16

li	$3,-1			# 0xffffffffffffffff
beq	$2,$3,$L54
lw	$28,16($sp)

sw	$0,24($sp)
$L16:
lw	$25,36($fp)
beq	$25,$0,$L17
move	$5,$16

lw	$4,24($fp)
jalr	$25
move	$6,$17

beq	$2,$18,$L46
lw	$28,16($sp)

beq	$2,$20,$L4
nop

beq	$2,$21,$L28
nop

$L17:
lw	$25,%call16(mad_synth_frame)($28)
move	$4,$22
.reloc	1f,R_MIPS_JALR,mad_synth_frame
1:	jalr	$25
move	$5,$17

lw	$25,40($fp)
beq	$25,$0,$L4
lw	$28,16($sp)

lw	$4,24($fp)
move	$5,$17
jalr	$25
lw	$6,32($sp)

beq	$2,$21,$L28
lw	$28,16($sp)

$L48:
bne	$2,$18,$L4
nop

$L46:
.option	pic0
j	$L7
.option	pic2
li	$18,-1			# 0xffffffffffffffff

$L52:
lw	$2,60($16)
$L11:
li	$4,1			# 0x1
beq	$2,$4,$L20
nop

.option	pic0
j	$L7
.option	pic2
li	$18,-1			# 0xffffffffffffffff

$L53:
lw	$2,60($16)
andi	$4,$2,0xff00
beq	$4,$0,$L11
move	$4,$23

move	$5,$16
move	$25,$19
jalr	$25
move	$6,$17

bne	$2,$21,$L48
lw	$28,16($sp)

$L28:
move	$18,$0
$L7:
lw	$25,%call16(mad_frame_finish)($28)
.reloc	1f,R_MIPS_JALR,mad_frame_finish
1:	jalr	$25
move	$4,$17

lw	$28,16($sp)
lw	$25,%call16(mad_stream_finish)($28)
.reloc	1f,R_MIPS_JALR,mad_stream_finish
1:	jalr	$25
move	$4,$16

move	$2,$18
lw	$31,76($sp)
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,80

$L22:
lui	$19,%hi(error_default)
.option	pic0
j	$L3
.option	pic2
addiu	$19,$19,%lo(error_default)

$L21:
lw	$31,76($sp)
move	$2,$0
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,80

$L54:
lw	$2,60($16)
andi	$4,$2,0xff00
beq	$4,$0,$L11
move	$4,$23

move	$5,$16
move	$25,$19
jalr	$25
move	$6,$17

beq	$2,$18,$L46
lw	$28,16($sp)

beq	$2,$20,$L16
nop

bne	$2,$21,$L4
nop

.option	pic0
j	$L7
.option	pic2
move	$18,$0

.set	macro
.set	reorder
.end	run_sync
.size	run_sync, .-run_sync
.align	2
.set	nomips16
.set	nomicromips
.ent	error_default
.type	error_default, @function
error_default:
.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,60($5)
li	$2,513			# 0x201
bne	$3,$2,$L60
nop

lw	$2,0($4)
bne	$2,$0,$L63
li	$3,1			# 0x1

li	$2,32			# 0x20
j	$31
sw	$3,0($4)

$L60:
j	$31
move	$2,$0

$L63:
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,28($sp)
.cprestore	16
lw	$25,%call16(mad_frame_mute)($28)
.reloc	1f,R_MIPS_JALR,mad_frame_mute
1:	jalr	$25
move	$4,$6

li	$2,32			# 0x20
lw	$31,28($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	error_default
.size	error_default, .-error_default
.align	2
.globl	mad_decoder_init
.set	nomips16
.set	nomicromips
.ent	mad_decoder_init
.type	mad_decoder_init, @function
mad_decoder_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,-1			# 0xffffffffffffffff
sw	$0,4($4)
sw	$0,8($4)
sw	$0,20($4)
sw	$2,0($4)
sw	$2,12($4)
sw	$2,16($4)
lw	$2,16($sp)
sw	$5,24($4)
sw	$6,28($4)
sw	$7,32($4)
sw	$2,36($4)
lw	$2,20($sp)
sw	$2,40($4)
lw	$2,24($sp)
sw	$2,44($4)
lw	$2,28($sp)
j	$31
sw	$2,48($4)

.set	macro
.set	reorder
.end	mad_decoder_init
.size	mad_decoder_init, .-mad_decoder_init
.align	2
.globl	mad_decoder_finish
.set	nomips16
.set	nomicromips
.ent	mad_decoder_finish
.type	mad_decoder_finish, @function
mad_decoder_finish:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	mad_decoder_finish
.size	mad_decoder_finish, .-mad_decoder_finish
.align	2
.globl	mad_decoder_run
.set	nomips16
.set	nomicromips
.ent	mad_decoder_run
.type	mad_decoder_run, @function
mad_decoder_run:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
bne	$5,$0,$L78
sw	$5,0($4)

lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,28($sp)
move	$16,$4
sw	$31,36($sp)
sw	$17,32($sp)
.cprestore	16
lw	$25,%call16(malloc)($28)
.reloc	1f,R_MIPS_JALR,malloc
1:	jalr	$25
li	$4,22656			# 0x5880

beq	$2,$0,$L79
sw	$2,20($16)

.option	pic0
jal	run_sync
.option	pic2
move	$4,$16

lw	$28,16($sp)
move	$17,$2
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
lw	$4,20($16)

move	$2,$17
sw	$0,20($16)
$L73:
lw	$31,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,40

$L78:
j	$31
li	$2,-1			# 0xffffffffffffffff

$L79:
.option	pic0
j	$L73
.option	pic2
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	mad_decoder_run
.size	mad_decoder_run, .-mad_decoder_run
.align	2
.globl	mad_decoder_message
.set	nomips16
.set	nomicromips
.ent	mad_decoder_message
.type	mad_decoder_message, @function
mad_decoder_message:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
j	$31
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	mad_decoder_message
.size	mad_decoder_message, .-mad_decoder_message
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
