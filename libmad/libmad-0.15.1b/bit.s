.file	1 "bit.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.globl	mad_bit_init
.set	nomips16
.set	nomicromips
.ent	mad_bit_init
.type	mad_bit_init, @function
mad_bit_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,8			# 0x8
sw	$5,0($4)
sh	$0,4($4)
j	$31
sh	$2,6($4)

.set	macro
.set	reorder
.end	mad_bit_init
.size	mad_bit_init, .-mad_bit_init
.align	2
.globl	mad_bit_length
.set	nomips16
.set	nomicromips
.ent	mad_bit_length
.type	mad_bit_length, @function
mad_bit_length:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,0($4)
lw	$6,0($5)
lhu	$3,6($4)
addiu	$2,$2,1
lhu	$4,6($5)
subu	$2,$6,$2
sll	$2,$2,3
addu	$2,$3,$2
subu	$2,$2,$4
j	$31
addiu	$2,$2,8

.set	macro
.set	reorder
.end	mad_bit_length
.size	mad_bit_length, .-mad_bit_length
.align	2
.globl	mad_bit_nextbyte
.set	nomips16
.set	nomicromips
.ent	mad_bit_nextbyte
.type	mad_bit_nextbyte, @function
mad_bit_nextbyte:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lhu	$3,6($4)
li	$2,8			# 0x8
beq	$3,$2,$L7
nop

lw	$2,0($4)
j	$31
addiu	$2,$2,1

$L7:
j	$31
lw	$2,0($4)

.set	macro
.set	reorder
.end	mad_bit_nextbyte
.size	mad_bit_nextbyte, .-mad_bit_nextbyte
.align	2
.globl	mad_bit_skip
.set	nomips16
.set	nomicromips
.ent	mad_bit_skip
.type	mad_bit_skip, @function
mad_bit_skip:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lhu	$2,6($4)
srl	$6,$5,3
lw	$3,0($4)
andi	$5,$5,0x7
subu	$5,$2,$5
addu	$2,$3,$6
andi	$5,$5,0xffff
sltu	$3,$5,9
sw	$2,0($4)
bne	$3,$0,$L9
sh	$5,6($4)

addiu	$5,$5,8
addiu	$2,$2,1
andi	$5,$5,0xffff
sw	$2,0($4)
sh	$5,6($4)
$L9:
sltu	$5,$5,8
beq	$5,$0,$L11
nop

lw	$2,0($4)
lbu	$2,0($2)
sh	$2,4($4)
$L11:
j	$31
nop

.set	macro
.set	reorder
.end	mad_bit_skip
.size	mad_bit_skip, .-mad_bit_skip
.align	2
.globl	mad_bit_read
.set	nomips16
.set	nomicromips
.ent	mad_bit_read
.type	mad_bit_read, @function
mad_bit_read:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lhu	$2,6($4)
li	$3,8			# 0x8
beq	$2,$3,$L13
nop

lhu	$3,4($4)
$L14:
sltu	$6,$5,$2
bne	$6,$0,$L21
li	$7,1			# 0x1

lw	$6,0($4)
subu	$5,$5,$2
sll	$2,$7,$2
addiu	$8,$6,1
addiu	$2,$2,-1
sltu	$7,$5,8
and	$2,$3,$2
sw	$8,0($4)
li	$3,8			# 0x8
bne	$7,$0,$L17
sh	$3,6($4)

addiu	$7,$5,-8
addiu	$3,$6,2
srl	$7,$7,3
addiu	$7,$7,3
addu	$7,$6,$7
$L18:
sw	$3,0($4)
sll	$2,$2,8
lbu	$6,-1($3)
addiu	$3,$3,1
bne	$3,$7,$L18
or	$2,$6,$2

andi	$5,$5,0x7
$L17:
beq	$5,$0,$L22
li	$3,8			# 0x8

lw	$6,0($4)
sll	$2,$2,$5
subu	$5,$3,$5
lbu	$3,0($6)
sh	$5,6($4)
sra	$5,$3,$5
sh	$3,4($4)
or	$2,$5,$2
$L22:
j	$31
nop

$L21:
li	$6,1			# 0x1
subu	$5,$2,$5
sll	$2,$6,$2
addiu	$2,$2,-1
sh	$5,6($4)
and	$2,$3,$2
j	$31
sra	$2,$2,$5

$L13:
lw	$3,0($4)
lbu	$6,0($3)
andi	$3,$6,0xffff
.option	pic0
j	$L14
.option	pic2
sh	$6,4($4)

.set	macro
.set	reorder
.end	mad_bit_read
.size	mad_bit_read, .-mad_bit_read
.align	2
.globl	mad_bit_crc
.set	nomips16
.set	nomicromips
.ent	mad_bit_crc
.type	mad_bit_crc, @function
mad_bit_crc:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sltu	$2,$6,32
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
andi	$16,$7,0xffff
sw	$31,44($sp)
sw	$19,40($sp)
sw	$18,36($sp)
sw	$4,48($sp)
bne	$2,$0,$L24
sw	$5,52($sp)

lui	$18,%hi(crc_table)
move	$19,$6
addiu	$18,$18,%lo(crc_table)
$L25:
li	$5,32			# 0x20
addiu	$4,$sp,48
.option	pic0
jal	mad_bit_read
.option	pic2
addiu	$19,$19,-32

srl	$4,$16,8
srl	$3,$2,24
sll	$16,$16,8
xor	$4,$3,$4
andi	$4,$4,0xff
sll	$4,$4,1
srl	$6,$2,16
addu	$4,$18,$4
lhu	$5,0($4)
sltu	$4,$19,32
xor	$16,$5,$16
srl	$3,$16,8
sll	$16,$16,8
xor	$3,$3,$6
andi	$3,$3,0xff
sll	$3,$3,1
addu	$3,$18,$3
lhu	$3,0($3)
xor	$16,$3,$16
xor	$3,$2,$16
srl	$3,$3,7
sll	$16,$16,8
andi	$3,$3,0x1fe
addu	$3,$18,$3
lhu	$3,0($3)
xor	$16,$3,$16
srl	$3,$16,8
sll	$16,$16,8
xor	$2,$3,$2
andi	$2,$2,0xff
sll	$2,$2,1
addu	$2,$18,$2
lhu	$7,0($2)
beq	$4,$0,$L25
xor	$16,$7,$16

andi	$17,$17,0x1f
$L24:
srl	$2,$17,3
li	$3,2			# 0x2
beq	$2,$3,$L40
li	$3,3			# 0x3

beq	$2,$3,$L28
li	$3,1			# 0x1

beq	$2,$3,$L41
nop

$L26:
.option	pic0
j	$L30
.option	pic2
li	$18,-1			# 0xffffffffffffffff

$L32:
.option	pic0
jal	mad_bit_read
nop

.option	pic2
srl	$3,$16,15
sll	$16,$16,1
xor	$2,$2,$3
andi	$2,$2,0x1
xori	$3,$16,0x8005
movn	$16,$3,$2
$L30:
addiu	$17,$17,-1
li	$5,1			# 0x1
bne	$17,$18,$L32
addiu	$4,$sp,48

lw	$31,44($sp)
andi	$2,$16,0xffff
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,48

$L28:
li	$5,8			# 0x8
addiu	$4,$sp,48
.option	pic0
jal	mad_bit_read
.option	pic2
lui	$18,%hi(crc_table)

srl	$3,$16,8
addiu	$18,$18,%lo(crc_table)
xor	$2,$2,$3
andi	$2,$2,0xff
sll	$2,$2,1
sll	$16,$16,8
addu	$2,$18,$2
lhu	$7,0($2)
xor	$16,$7,$16
$L27:
li	$5,8			# 0x8
.option	pic0
jal	mad_bit_read
.option	pic2
addiu	$4,$sp,48

srl	$3,$16,8
sll	$16,$16,8
xor	$2,$3,$2
andi	$2,$2,0xff
sll	$2,$2,1
addu	$2,$18,$2
lhu	$7,0($2)
xor	$16,$7,$16
$L29:
li	$5,8			# 0x8
addiu	$4,$sp,48
.option	pic0
jal	mad_bit_read
.option	pic2
andi	$17,$17,0x7

srl	$3,$16,8
sll	$16,$16,8
xor	$2,$3,$2
andi	$2,$2,0xff
sll	$2,$2,1
addu	$18,$18,$2
lhu	$7,0($18)
.option	pic0
j	$L26
.option	pic2
xor	$16,$7,$16

$L40:
lui	$18,%hi(crc_table)
.option	pic0
j	$L27
.option	pic2
addiu	$18,$18,%lo(crc_table)

$L41:
lui	$18,%hi(crc_table)
.option	pic0
j	$L29
.option	pic2
addiu	$18,$18,%lo(crc_table)

.set	macro
.set	reorder
.end	mad_bit_crc
.size	mad_bit_crc, .-mad_bit_crc
.rdata
.align	2
.type	crc_table, @object
.size	crc_table, 512
crc_table:
.half	0
.half	-32763
.half	-32753
.half	10
.half	-32741
.half	30
.half	20
.half	-32751
.half	-32717
.half	54
.half	60
.half	-32711
.half	40
.half	-32723
.half	-32729
.half	34
.half	-32669
.half	102
.half	108
.half	-32663
.half	120
.half	-32643
.half	-32649
.half	114
.half	80
.half	-32683
.half	-32673
.half	90
.half	-32693
.half	78
.half	68
.half	-32703
.half	-32573
.half	198
.half	204
.half	-32567
.half	216
.half	-32547
.half	-32553
.half	210
.half	240
.half	-32523
.half	-32513
.half	250
.half	-32533
.half	238
.half	228
.half	-32543
.half	160
.half	-32603
.half	-32593
.half	170
.half	-32581
.half	190
.half	180
.half	-32591
.half	-32621
.half	150
.half	156
.half	-32615
.half	136
.half	-32627
.half	-32633
.half	130
.half	-32381
.half	390
.half	396
.half	-32375
.half	408
.half	-32355
.half	-32361
.half	402
.half	432
.half	-32331
.half	-32321
.half	442
.half	-32341
.half	430
.half	420
.half	-32351
.half	480
.half	-32283
.half	-32273
.half	490
.half	-32261
.half	510
.half	500
.half	-32271
.half	-32301
.half	470
.half	476
.half	-32295
.half	456
.half	-32307
.half	-32313
.half	450
.half	320
.half	-32443
.half	-32433
.half	330
.half	-32421
.half	350
.half	340
.half	-32431
.half	-32397
.half	374
.half	380
.half	-32391
.half	360
.half	-32403
.half	-32409
.half	354
.half	-32477
.half	294
.half	300
.half	-32471
.half	312
.half	-32451
.half	-32457
.half	306
.half	272
.half	-32491
.half	-32481
.half	282
.half	-32501
.half	270
.half	260
.half	-32511
.half	-31997
.half	774
.half	780
.half	-31991
.half	792
.half	-31971
.half	-31977
.half	786
.half	816
.half	-31947
.half	-31937
.half	826
.half	-31957
.half	814
.half	804
.half	-31967
.half	864
.half	-31899
.half	-31889
.half	874
.half	-31877
.half	894
.half	884
.half	-31887
.half	-31917
.half	854
.half	860
.half	-31911
.half	840
.half	-31923
.half	-31929
.half	834
.half	960
.half	-31803
.half	-31793
.half	970
.half	-31781
.half	990
.half	980
.half	-31791
.half	-31757
.half	1014
.half	1020
.half	-31751
.half	1000
.half	-31763
.half	-31769
.half	994
.half	-31837
.half	934
.half	940
.half	-31831
.half	952
.half	-31811
.half	-31817
.half	946
.half	912
.half	-31851
.half	-31841
.half	922
.half	-31861
.half	910
.half	900
.half	-31871
.half	640
.half	-32123
.half	-32113
.half	650
.half	-32101
.half	670
.half	660
.half	-32111
.half	-32077
.half	694
.half	700
.half	-32071
.half	680
.half	-32083
.half	-32089
.half	674
.half	-32029
.half	742
.half	748
.half	-32023
.half	760
.half	-32003
.half	-32009
.half	754
.half	720
.half	-32043
.half	-32033
.half	730
.half	-32053
.half	718
.half	708
.half	-32063
.half	-32189
.half	582
.half	588
.half	-32183
.half	600
.half	-32163
.half	-32169
.half	594
.half	624
.half	-32139
.half	-32129
.half	634
.half	-32149
.half	622
.half	612
.half	-32159
.half	544
.half	-32219
.half	-32209
.half	554
.half	-32197
.half	574
.half	564
.half	-32207
.half	-32237
.half	534
.half	540
.half	-32231
.half	520
.half	-32243
.half	-32249
.half	514
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
