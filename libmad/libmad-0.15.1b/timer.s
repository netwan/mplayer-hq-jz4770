.file	1 "timer.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	scale_rational
.type	scale_rational, @function
scale_rational:
.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,28($sp)
beq	$5,$0,$L2
.cprestore	16

move	$3,$5
.option	pic0
j	$L3
.option	pic2
move	$10,$4

$L14:
move	$3,$11
$L3:
teq	$3,$0,7
divu	$0,$10,$3
mfhi	$11
bne	$11,$0,$L14
move	$10,$3

$L5:
teq	$3,$0,7
divu	$0,$5,$3
mflo	$5
teq	$3,$0,7
divu	$0,$4,$3
beq	$5,$0,$L6
mflo	$3

move	$8,$5
.option	pic0
j	$L7
.option	pic2
move	$10,$6

$L15:
move	$8,$11
$L7:
teq	$8,$0,7
divu	$0,$10,$8
mfhi	$11
bne	$11,$0,$L15
move	$10,$8

$L13:
teq	$8,$0,7
divu	$0,$5,$8
mflo	$5
teq	$8,$0,7
divu	$0,$6,$8
beq	$5,$0,$L9
mflo	$8

sltu	$2,$5,$8
bne	$2,$0,$L24
lw	$31,28($sp)

sltu	$2,$5,$3
bne	$2,$0,$L25
nop

mul	$8,$8,$3
addiu	$sp,$sp,32
teq	$5,$0,7
divu	$0,$8,$5
j	$31
mflo	$2

$L25:
teq	$5,$0,7
divu	$0,$3,$5
addiu	$sp,$sp,32
mfhi	$4
mflo	$3
mul	$4,$4,$8
teq	$5,$0,7
divu	$0,$4,$5
madd	$3,$8
j	$31
mflo	$2

$L24:
teq	$5,$0,7
divu	$0,$8,$5
addiu	$sp,$sp,32
mfhi	$8
mflo	$9
mul	$10,$8,$3
teq	$5,$0,7
divu	$0,$10,$5
madd	$9,$3
j	$31
mflo	$2

$L6:
bne	$6,$0,$L13
move	$8,$6

$L9:
lw	$25,%call16(abort)($28)
$L26:
.reloc	1f,R_MIPS_JALR,abort
1:	jalr	$25
nop

$L2:
bne	$4,$0,$L5
move	$3,$4

.option	pic0
j	$L26
.option	pic2
lw	$25,%call16(abort)($28)

.set	macro
.set	reorder
.end	scale_rational
.size	scale_rational, .-scale_rational
.align	2
.globl	mad_timer_compare
.set	nomips16
.set	nomicromips
.ent	mad_timer_compare
.type	mad_timer_compare, @function
mad_timer_compare:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
subu	$2,$4,$6
sw	$4,0($sp)
sw	$5,4($sp)
sw	$6,8($sp)
bltz	$2,$L31
sw	$7,12($sp)

beq	$2,$0,$L32
li	$2,1			# 0x1

j	$31
nop

$L32:
subu	$5,$5,$7
bltz	$5,$L31
nop

j	$31
sltu	$2,$0,$5

$L31:
j	$31
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	mad_timer_compare
.size	mad_timer_compare, .-mad_timer_compare
.align	2
.globl	mad_timer_negate
.set	nomips16
.set	nomicromips
.ent	mad_timer_negate
.type	mad_timer_negate, @function
mad_timer_negate:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,0($4)
lw	$3,4($4)
subu	$5,$0,$2
beq	$3,$0,$L40
sw	$5,0($4)

nor	$2,$0,$2
sw	$2,0($4)
li	$2,352780288			# 0x15070000
addiu	$2,$2,19712
subu	$2,$2,$3
sw	$2,4($4)
$L40:
j	$31
nop

.set	macro
.set	reorder
.end	mad_timer_negate
.size	mad_timer_negate, .-mad_timer_negate
.align	2
.globl	mad_timer_abs
.set	nomips16
.set	nomicromips
.ent	mad_timer_abs
.type	mad_timer_abs, @function
mad_timer_abs:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
move	$2,$5
sw	$5,4($sp)
bltz	$5,$L44
sw	$6,8($sp)

sw	$2,0($4)
move	$2,$4
j	$31
sw	$6,4($4)

$L44:
beq	$6,$0,$L45
li	$3,352780288			# 0x15070000

nor	$2,$0,$5
addiu	$3,$3,19712
sw	$2,0($4)
move	$2,$4
subu	$6,$3,$6
j	$31
sw	$6,4($4)

$L45:
subu	$2,$0,$5
sw	$6,4($4)
sw	$2,0($4)
j	$31
move	$2,$4

.set	macro
.set	reorder
.end	mad_timer_abs
.size	mad_timer_abs, .-mad_timer_abs
.align	2
.globl	mad_timer_set
.set	nomips16
.set	nomicromips
.ent	mad_timer_set
.type	mad_timer_set, @function
mad_timer_set:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-32
sltu	$2,$6,$7
sw	$16,24($sp)
move	$16,$4
sw	$31,28($sp)
bne	$2,$0,$L47
sw	$5,0($4)

bne	$7,$0,$L72
nop

sw	$0,4($16)
$L46:
lw	$31,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

$L72:
teq	$7,$0,7
divu	$0,$6,$7
mflo	$2
mfhi	$6
addu	$5,$5,$2
sw	$5,0($4)
$L47:
li	$2,16000			# 0x3e80
beq	$7,$2,$L50
li	$2,22050			# 0x5622

sltu	$2,$7,16001
bne	$2,$0,$L73
li	$2,32000			# 0x7d00

beq	$7,$2,$L57
li	$2,11025			# 0x2b11

sltu	$2,$7,32001
bne	$2,$0,$L74
li	$2,48000			# 0xbb80

beq	$7,$2,$L61
li	$2,7350			# 0x1cb6

li	$2,352780288			# 0x15070000
addiu	$2,$2,19712
beq	$7,$2,$L62
nop

li	$2,44100			# 0xac44
beq	$7,$2,$L75
sll	$3,$6,2

$L49:
move	$4,$6
$L77:
li	$6,352780288			# 0x15070000
move	$5,$7
.option	pic0
jal	scale_rational
.option	pic2
addiu	$6,$6,19712

.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L73:
li	$2,1000			# 0x3e8
beq	$7,$2,$L52
li	$2,327680			# 0x50000

sltu	$2,$7,1001
bne	$2,$0,$L76
li	$2,11025			# 0x2b11

beq	$7,$2,$L54
li	$2,12000			# 0x2ee0

beq	$7,$2,$L55
li	$2,29400			# 0x72d8

li	$2,8000			# 0x1f40
bne	$7,$2,$L77
move	$4,$6

li	$2,44100			# 0xac44
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L54:
sll	$3,$6,2
sll	$2,$6,7
subu	$2,$2,$3
addu	$6,$2,$6
sll	$2,$6,8
sw	$2,4($16)
$L65:
li	$6,352780288			# 0x15070000
addiu	$6,$6,19712
sltu	$3,$2,$6
bne	$3,$0,$L46
li	$3,-1027080192			# 0xffffffffc2c80000

lw	$4,0($16)
lw	$31,28($sp)
addiu	$3,$3,30993
multu	$2,$3
mfhi	$3
srl	$3,$3,28
mul	$5,$3,$6
addu	$3,$4,$3
sw	$3,0($16)
subu	$6,$2,$5
sw	$6,4($16)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

$L61:
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L75:
sll	$2,$6,7
subu	$2,$2,$3
addu	$6,$2,$6
sll	$2,$6,6
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L74:
li	$2,22050			# 0x5622
beq	$7,$2,$L59
sll	$3,$6,2

li	$2,24000			# 0x5dc0
bne	$7,$2,$L77
move	$4,$6

li	$2,14700			# 0x396c
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L52:
addiu	$2,$2,25120
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L55:
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L59:
sll	$2,$6,7
subu	$2,$2,$3
addu	$6,$2,$6
sll	$2,$6,7
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L62:
sw	$6,4($16)
.option	pic0
j	$L65
.option	pic2
move	$2,$6

$L57:
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L50:
mul	$2,$6,$2
.option	pic0
j	$L65
.option	pic2
sw	$2,4($16)

$L76:
sltu	$2,$7,2
beq	$2,$0,$L49
nop

.option	pic0
j	$L46
.option	pic2
sw	$0,4($16)

.set	macro
.set	reorder
.end	mad_timer_set
.size	mad_timer_set, .-mad_timer_set
.align	2
.globl	mad_timer_add
.set	nomips16
.set	nomicromips
.ent	mad_timer_add
.type	mad_timer_add, @function
mad_timer_add:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,4($4)
li	$7,352780288			# 0x15070000
lw	$3,0($4)
sw	$5,4($sp)
addiu	$7,$7,19712
sw	$6,8($sp)
addu	$6,$6,$2
addu	$5,$3,$5
sltu	$2,$6,$7
sw	$6,4($4)
bne	$2,$0,$L80
sw	$5,0($4)

li	$3,-1027080192			# 0xffffffffc2c80000
addiu	$3,$3,30993
multu	$6,$3
mfhi	$3
srl	$2,$3,28
mul	$3,$2,$7
addu	$2,$2,$5
sw	$2,0($4)
subu	$6,$6,$3
sw	$6,4($4)
$L80:
j	$31
nop

.set	macro
.set	reorder
.end	mad_timer_add
.size	mad_timer_add, .-mad_timer_add
.align	2
.globl	mad_timer_multiply
.set	nomips16
.set	nomicromips
.ent	mad_timer_multiply
.type	mad_timer_multiply, @function
mad_timer_multiply:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
bltz	$5,$L82
nop

lw	$6,0($4)
lw	$2,4($4)
$L83:
sw	$0,0($4)
beq	$5,$0,$L95
sw	$0,4($4)

li	$7,352780288			# 0x15070000
li	$10,-1027080192			# 0xffffffffc2c80000
addiu	$7,$7,19712
addiu	$10,$10,30993
andi	$3,$5,0x1
$L94:
beq	$3,$0,$L86
srl	$5,$5,1

lw	$3,4($4)
lw	$8,0($4)
addu	$3,$2,$3
addu	$8,$6,$8
multu	$3,$10
sltu	$11,$3,$7
sw	$3,4($4)
sw	$8,0($4)
bne	$11,$0,$L86
mfhi	$9

srl	$9,$9,28
mul	$11,$9,$7
addu	$9,$9,$8
sw	$9,0($4)
subu	$3,$3,$11
sw	$3,4($4)
$L86:
sll	$2,$2,1
sll	$6,$6,1
multu	$2,$10
sltu	$8,$2,$7
bne	$8,$0,$L88
mfhi	$3

srl	$3,$3,28
mul	$8,$3,$7
addu	$6,$3,$6
subu	$2,$2,$8
$L88:
bne	$5,$0,$L94
andi	$3,$5,0x1

$L95:
j	$31
nop

$L82:
lw	$2,0($4)
subu	$5,$0,$5
lw	$3,4($4)
subu	$6,$0,$2
beq	$3,$0,$L91
sw	$6,0($4)

nor	$6,$0,$2
li	$2,352780288			# 0x15070000
addiu	$2,$2,19712
.option	pic0
j	$L83
.option	pic2
subu	$2,$2,$3

$L91:
.option	pic0
j	$L83
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	mad_timer_multiply
.size	mad_timer_multiply, .-mad_timer_multiply
.align	2
.globl	mad_timer_count
.set	nomips16
.set	nomicromips
.ent	mad_timer_count
.type	mad_timer_count, @function
mad_timer_count:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-32
li	$2,48			# 0x30
sw	$31,28($sp)
sw	$16,24($sp)
sw	$4,32($sp)
beq	$6,$2,$L98
sw	$5,36($sp)

slt	$2,$6,49
beq	$2,$0,$L99
li	$2,-2			# 0xfffffffffffffffe

beq	$6,$2,$L100
li	$3,-1851654144			# 0xffffffff91a20000

slt	$2,$6,-1
bne	$2,$0,$L116
li	$2,10			# 0xa

beq	$6,$2,$L98
slt	$2,$6,11

beq	$2,$0,$L104
li	$2,-1			# 0xffffffffffffffff

beq	$6,$2,$L105
li	$3,-2004353024			# 0xffffffff88880000

bne	$6,$0,$L110
move	$2,$4

lw	$31,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

$L116:
li	$2,-48			# 0xffffffffffffffd0
beq	$6,$2,$L121
lw	$4,32($sp)

slt	$2,$6,-47
beq	$2,$0,$L103
li	$2,-30			# 0xffffffffffffffe2

li	$2,-60			# 0xffffffffffffffc4
beq	$6,$2,$L121
nop

li	$2,-50			# 0xffffffffffffffce
bne	$6,$2,$L110
nop

$L121:
subu	$6,$0,$6
.option	pic0
jal	mad_timer_count
.option	pic2
lw	$5,36($sp)

addiu	$2,$2,1
lw	$31,28($sp)
lw	$16,24($sp)
addiu	$sp,$sp,32
sll	$4,$2,2
sll	$3,$2,7
subu	$3,$3,$4
addu	$4,$3,$2
li	$2,1098383360			# 0x41780000
sll	$4,$4,3
addiu	$2,$2,29855
mult	$4,$2
sra	$4,$4,31
mfhi	$2
sra	$2,$2,8
j	$31
subu	$2,$2,$4

$L99:
li	$2,11025			# 0x2b11
beq	$6,$2,$L98
slt	$2,$6,11026

bne	$2,$0,$L117
li	$2,24000			# 0x5dc0

beq	$6,$2,$L98
slt	$2,$6,24001

bne	$2,$0,$L118
li	$2,16000			# 0x3e80

li	$2,44100			# 0xac44
beq	$6,$2,$L98
li	$2,48000			# 0xbb80

beq	$6,$2,$L98
li	$2,32000			# 0x7d00

beq	$6,$2,$L122
mul	$16,$6,$4

$L110:
move	$2,$0
$L120:
lw	$31,28($sp)
$L123:
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

$L105:
lw	$31,28($sp)
sra	$2,$4,31
lw	$16,24($sp)
ori	$3,$3,0x8889
mult	$4,$3
addiu	$sp,$sp,32
mfhi	$3
addu	$4,$3,$4
sra	$4,$4,5
j	$31
subu	$2,$4,$2

$L117:
li	$2,75			# 0x4b
beq	$6,$2,$L98
slt	$2,$6,76

bne	$2,$0,$L119
li	$2,1000			# 0x3e8

beq	$6,$2,$L98
li	$2,8000			# 0x1f40

beq	$6,$2,$L98
li	$2,100			# 0x64

bne	$6,$2,$L120
move	$2,$0

$L98:
mul	$16,$6,$4
$L122:
lw	$4,36($sp)
li	$5,352780288			# 0x15070000
.option	pic0
jal	scale_rational
.option	pic2
addiu	$5,$5,19712

lw	$31,28($sp)
addu	$2,$16,$2
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

$L100:
lw	$31,28($sp)
sra	$2,$4,31
lw	$16,24($sp)
ori	$3,$3,0xb3c5
mult	$4,$3
addiu	$sp,$sp,32
mfhi	$3
addu	$4,$3,$4
sra	$4,$4,11
j	$31
subu	$2,$4,$2

$L103:
beq	$6,$2,$L121
lw	$4,32($sp)

slt	$2,$6,-30
bne	$2,$0,$L110
addiu	$2,$6,25

sltu	$2,$2,2
bne	$2,$0,$L121
nop

.option	pic0
j	$L120
.option	pic2
move	$2,$0

$L118:
beq	$6,$2,$L98
li	$2,22050			# 0x5622

beq	$6,$2,$L98
li	$2,12000			# 0x2ee0

beq	$6,$2,$L98
move	$2,$0

.option	pic0
j	$L123
.option	pic2
lw	$31,28($sp)

$L119:
li	$2,50			# 0x32
beq	$6,$2,$L98
li	$2,60			# 0x3c

beq	$6,$2,$L98
move	$2,$0

.option	pic0
j	$L123
.option	pic2
lw	$31,28($sp)

$L104:
slt	$2,$6,24
bne	$2,$0,$L110
slt	$2,$6,26

bne	$2,$0,$L98
li	$2,30			# 0x1e

beq	$6,$2,$L98
move	$2,$0

.option	pic0
j	$L123
.option	pic2
lw	$31,28($sp)

.set	macro
.set	reorder
.end	mad_timer_count
.size	mad_timer_count, .-mad_timer_count
.align	2
.globl	mad_timer_fraction
.set	nomips16
.set	nomicromips
.ent	mad_timer_fraction
.type	mad_timer_fraction, @function
mad_timer_fraction:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sw	$4,0($sp)
bltz	$4,$L135
sw	$5,4($sp)

$L125:
beq	$6,$0,$L128
li	$2,352780288			# 0x15070000

addiu	$2,$2,19712
bne	$6,$2,$L137
move	$4,$5

j	$31
move	$2,$5

$L135:
bne	$5,$0,$L136
li	$2,352780288			# 0x15070000

beq	$6,$0,$L130
nop

addiu	$2,$2,19712
beq	$6,$2,$L133
move	$4,$5

$L137:
li	$5,352780288			# 0x15070000
.option	pic0
j	scale_rational
.option	pic2
addiu	$5,$5,19712

$L128:
beq	$5,$0,$L130
nop

li	$2,352780288			# 0x15070000
addiu	$2,$2,19712
teq	$5,$0,7
divu	$0,$2,$5
j	$31
mflo	$2

$L136:
addiu	$2,$2,19712
.option	pic0
j	$L125
.option	pic2
subu	$5,$2,$5

$L130:
li	$5,352780288			# 0x15070000
j	$31
addiu	$2,$5,19713

$L133:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	mad_timer_fraction
.size	mad_timer_fraction, .-mad_timer_fraction
.align	2
.globl	mad_timer_string
.set	nomips16
.set	nomicromips
.ent	mad_timer_string
.type	mad_timer_string, @function
mad_timer_string:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-64
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$23,52($sp)
move	$23,$5
sw	$21,44($sp)
move	$21,$7
sw	$20,40($sp)
move	$20,$6
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
sw	$16,24($sp)
sw	$31,60($sp)
sw	$fp,56($sp)
sw	$22,48($sp)
sw	$19,36($sp)
.cprestore	16
sw	$4,64($sp)
sw	$5,68($sp)
lw	$17,80($sp)
lw	$16,84($sp)
bltz	$4,$L172
lw	$6,88($sp)

$L139:
li	$2,60			# 0x3c
beq	$16,$2,$L142
move	$22,$18

slt	$2,$16,61
beq	$2,$0,$L143
slt	$2,$16,-23

beq	$2,$0,$L144
slt	$2,$16,-25

beq	$2,$0,$L145
li	$2,-50			# 0xffffffffffffffce

beq	$16,$2,$L145
slt	$2,$16,-49

bne	$2,$0,$L173
li	$2,-48			# 0xffffffffffffffd0

beq	$16,$2,$L145
li	$2,-30			# 0xffffffffffffffe2

bne	$16,$2,$L183
move	$19,$0

$L145:
move	$4,$18
sw	$18,64($sp)
move	$5,$23
sw	$23,68($sp)
.option	pic0
jal	mad_timer_count
.option	pic2
move	$6,$16

li	$4,-600			# 0xfffffffffffffda8
lw	$28,16($sp)
mul	$4,$16,$4
addiu	$4,$4,-18
teq	$4,$0,7
divu	$0,$2,$4
mflo	$3
mfhi	$5
sll	$8,$3,1
sll	$3,$3,4
sltu	$6,$5,3
addu	$3,$8,$3
beq	$6,$0,$L174
addu	$3,$3,$2

$L151:
subu	$19,$0,$16
move	$fp,$0
teq	$19,$0,7
divu	$0,$3,$19
mfhi	$19
mflo	$22
$L141:
li	$2,48			# 0x30
beq	$17,$2,$L153
slt	$2,$17,49

beq	$2,$0,$L154
li	$2,-2			# 0xfffffffffffffffe

beq	$17,$2,$L155
slt	$2,$17,-1

bne	$2,$0,$L175
li	$2,10			# 0xa

beq	$17,$2,$L153
slt	$2,$17,11

beq	$2,$0,$L159
li	$2,-1			# 0xffffffffffffffff

beq	$17,$2,$L160
li	$6,-2004353024			# 0xffffffff88880000

bne	$17,$0,$L138
lw	$31,60($sp)

move	$4,$20
lw	$23,52($sp)
move	$5,$21
lw	$20,40($sp)
move	$6,$22
lw	$21,44($sp)
move	$7,$19
lw	$22,48($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
lw	$25,%call16(sprintf)($28)
sw	$fp,80($sp)
lw	$fp,56($sp)
.reloc	1f,R_MIPS_JALR,sprintf
1:	jr	$25
addiu	$sp,$sp,64

$L143:
li	$2,12000			# 0x2ee0
beq	$16,$2,$L142
slt	$2,$16,12001

bne	$2,$0,$L176
li	$2,24000			# 0x5dc0

beq	$16,$2,$L142
slt	$2,$16,24001

bne	$2,$0,$L177
li	$2,44100			# 0xac44

beq	$16,$2,$L142
li	$2,48000			# 0xbb80

beq	$16,$2,$L142
li	$2,32000			# 0x7d00

beq	$16,$2,$L142
nop

$L165:
move	$19,$0
$L183:
.option	pic0
j	$L141
.option	pic2
move	$fp,$0

$L176:
li	$2,1000			# 0x3e8
beq	$16,$2,$L142
slt	$2,$16,1001

beq	$2,$0,$L149
li	$2,75			# 0x4b

beq	$16,$2,$L142
li	$2,100			# 0x64

bne	$16,$2,$L165
nop

$L142:
li	$5,352780288			# 0x15070000
addiu	$5,$5,19712
teq	$16,$0,7
divu	$0,$5,$16
mflo	$5
teq	$5,$0,7
divu	$0,$23,$5
mfhi	$4
.option	pic0
jal	scale_rational
.option	pic2
mflo	$19

lw	$28,16($sp)
.option	pic0
j	$L141
.option	pic2
move	$fp,$2

$L158:
beq	$17,$2,$L157
slt	$2,$17,-30

bne	$2,$0,$L138
addiu	$2,$17,25

sltu	$2,$2,2
beq	$2,$0,$L185
lw	$31,60($sp)

$L157:
slt	$16,$16,0
movn	$fp,$0,$16
$L153:
move	$4,$18
$L184:
sw	$18,64($sp)
move	$5,$23
sw	$23,68($sp)
.option	pic0
jal	mad_timer_count
.option	pic2
move	$6,$17

move	$4,$20
lw	$28,16($sp)
move	$5,$21
lw	$31,60($sp)
move	$7,$fp
lw	$23,52($sp)
move	$6,$2
lw	$fp,56($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
lw	$25,%call16(sprintf)($28)
.reloc	1f,R_MIPS_JALR,sprintf
1:	jr	$25
addiu	$sp,$sp,64

$L172:
beq	$5,$0,$L178
li	$2,352780288			# 0x15070000

nor	$18,$0,$4
addiu	$2,$2,19712
.option	pic0
j	$L139
.option	pic2
subu	$23,$2,$5

$L154:
li	$2,11025			# 0x2b11
beq	$17,$2,$L153
slt	$2,$17,11026

bne	$2,$0,$L179
li	$2,24000			# 0x5dc0

beq	$17,$2,$L153
slt	$2,$17,24001

bne	$2,$0,$L180
li	$2,16000			# 0x3e80

li	$2,44100			# 0xac44
beq	$17,$2,$L153
li	$2,48000			# 0xbb80

beq	$17,$2,$L153
li	$2,32000			# 0x7d00

beq	$17,$2,$L184
move	$4,$18

$L138:
lw	$31,60($sp)
$L185:
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,64

$L175:
li	$2,-48			# 0xffffffffffffffd0
beq	$17,$2,$L157
slt	$2,$17,-47

beq	$2,$0,$L158
li	$2,-30			# 0xffffffffffffffe2

li	$2,-60			# 0xffffffffffffffc4
beq	$17,$2,$L157
li	$2,-50			# 0xffffffffffffffce

bne	$17,$2,$L138
slt	$16,$16,0

.option	pic0
j	$L153
.option	pic2
movn	$fp,$0,$16

$L144:
slt	$2,$16,26
bne	$2,$0,$L181
li	$2,48			# 0x30

beq	$16,$2,$L142
li	$2,50			# 0x32

beq	$16,$2,$L142
li	$2,30			# 0x1e

beq	$16,$2,$L142
nop

move	$19,$0
.option	pic0
j	$L141
.option	pic2
move	$fp,$0

$L160:
lw	$31,60($sp)
lw	$23,52($sp)
move	$4,$20
ori	$6,$6,0x8889
lw	$20,40($sp)
multu	$22,$6
lw	$18,32($sp)
lw	$17,28($sp)
move	$5,$21
lw	$16,24($sp)
lw	$21,44($sp)
mfhi	$6
lw	$25,%call16(sprintf)($28)
sw	$19,80($sp)
sw	$fp,84($sp)
lw	$19,36($sp)
lw	$fp,56($sp)
srl	$6,$6,5
sll	$7,$6,2
sll	$2,$6,6
subu	$7,$2,$7
subu	$7,$22,$7
lw	$22,48($sp)
.reloc	1f,R_MIPS_JALR,sprintf
1:	jr	$25
addiu	$sp,$sp,64

$L174:
addiu	$2,$5,-2
li	$5,-859045888			# 0xffffffffcccc0000
ori	$5,$5,0xcccd
multu	$4,$5
mfhi	$4
srl	$4,$4,3
teq	$4,$0,7
divu	$0,$2,$4
mflo	$4
sll	$4,$4,1
.option	pic0
j	$L151
.option	pic2
addu	$3,$3,$4

$L178:
.option	pic0
j	$L139
.option	pic2
subu	$18,$0,$4

$L173:
li	$2,-60			# 0xffffffffffffffc4
beq	$16,$2,$L145
move	$19,$0

.option	pic0
j	$L141
.option	pic2
move	$fp,$0

$L179:
li	$2,75			# 0x4b
beq	$17,$2,$L153
slt	$2,$17,76

bne	$2,$0,$L182
li	$2,1000			# 0x3e8

beq	$17,$2,$L153
li	$2,8000			# 0x1f40

beq	$17,$2,$L153
li	$2,100			# 0x64

bne	$17,$2,$L185
lw	$31,60($sp)

.option	pic0
j	$L184
.option	pic2
move	$4,$18

$L155:
li	$6,-2004353024			# 0xffffffff88880000
lw	$31,60($sp)
lw	$23,52($sp)
move	$4,$20
ori	$6,$6,0x8889
lw	$20,40($sp)
multu	$22,$6
lw	$18,32($sp)
lw	$17,28($sp)
move	$5,$21
lw	$16,24($sp)
lw	$21,44($sp)
mfhi	$8
lw	$25,%call16(sprintf)($28)
sw	$19,84($sp)
sw	$fp,88($sp)
lw	$19,36($sp)
lw	$fp,56($sp)
srl	$8,$8,5
multu	$8,$6
sll	$2,$8,6
sll	$3,$8,2
mfhi	$6
subu	$3,$2,$3
subu	$3,$22,$3
lw	$22,48($sp)
sw	$3,80($sp)
addiu	$sp,$sp,64
srl	$6,$6,5
sll	$7,$6,2
sll	$2,$6,6
subu	$7,$2,$7
.reloc	1f,R_MIPS_JALR,sprintf
1:	jr	$25
subu	$7,$8,$7

$L181:
slt	$2,$16,24
beq	$2,$0,$L142
li	$2,10			# 0xa

beq	$16,$2,$L142
nop

move	$19,$0
.option	pic0
j	$L141
.option	pic2
move	$fp,$0

$L177:
li	$2,16000			# 0x3e80
beq	$16,$2,$L142
li	$2,22050			# 0x5622

beq	$16,$2,$L142
nop

move	$19,$0
.option	pic0
j	$L141
.option	pic2
move	$fp,$0

$L149:
li	$2,8000			# 0x1f40
beq	$16,$2,$L142
li	$2,11025			# 0x2b11

beq	$16,$2,$L142
nop

move	$19,$0
.option	pic0
j	$L141
.option	pic2
move	$fp,$0

$L180:
beq	$17,$2,$L153
li	$2,22050			# 0x5622

beq	$17,$2,$L153
li	$2,12000			# 0x2ee0

bne	$17,$2,$L185
lw	$31,60($sp)

.option	pic0
j	$L184
.option	pic2
move	$4,$18

$L182:
li	$2,50			# 0x32
beq	$17,$2,$L153
li	$2,60			# 0x3c

bne	$17,$2,$L185
lw	$31,60($sp)

.option	pic0
j	$L184
.option	pic2
move	$4,$18

$L159:
slt	$2,$17,24
bne	$2,$0,$L138
slt	$2,$17,26

bne	$2,$0,$L153
li	$2,30			# 0x1e

bne	$17,$2,$L185
lw	$31,60($sp)

.option	pic0
j	$L184
.option	pic2
move	$4,$18

.set	macro
.set	reorder
.end	mad_timer_string
.size	mad_timer_string, .-mad_timer_string
.globl	mad_timer_zero
.rdata
.align	2
.type	mad_timer_zero, @object
.size	mad_timer_zero, 8
mad_timer_zero:
.space	8
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
