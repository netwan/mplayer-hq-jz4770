.file	1 "stream.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.globl	mad_stream_init
.set	nomips16
.set	nomicromips
.ent	mad_stream_init
.type	mad_stream_init, @function
mad_stream_init:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,24($sp)
move	$5,$0
move	$16,$4
sw	$31,28($sp)
.cprestore	16
addiu	$4,$4,28
sw	$0,0($16)
sw	$0,4($16)
sw	$0,8($16)
sw	$0,12($16)
sw	$0,16($16)
sw	$0,20($16)
lw	$25,%call16(mad_bit_init)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jalr	$25
sw	$0,24($16)

addiu	$4,$16,36
lw	$28,16($sp)
lw	$25,%call16(mad_bit_init)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jalr	$25
move	$5,$0

lw	$31,28($sp)
sw	$0,44($16)
sw	$0,48($16)
sw	$0,52($16)
sw	$0,56($16)
sw	$0,60($16)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	mad_stream_init
.size	mad_stream_init, .-mad_stream_init
.align	2
.globl	mad_stream_finish
.set	nomips16
.set	nomicromips
.ent	mad_stream_finish
.type	mad_stream_finish, @function
mad_stream_finish:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,48($4)
beq	$2,$0,$L10
nop

lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,28($sp)
sw	$16,24($sp)
move	$16,$4
.cprestore	16
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
move	$4,$2

lw	$31,28($sp)
sw	$0,48($16)
lw	$16,24($sp)
addiu	$sp,$sp,32
$L10:
j	$31
nop

.set	macro
.set	reorder
.end	mad_stream_finish
.size	mad_stream_finish, .-mad_stream_finish
.align	2
.globl	mad_stream_buffer
.set	nomips16
.set	nomicromips
.ent	mad_stream_buffer
.type	mad_stream_buffer, @function
mad_stream_buffer:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addu	$6,$5,$6
addiu	$28,$28,%lo(__gnu_local_gp)
li	$3,1			# 0x1
move	$2,$4
lw	$25,%call16(mad_bit_init)($28)
addiu	$4,$4,28
sw	$5,0($2)
sw	$6,4($2)
sw	$5,20($2)
sw	$5,24($2)
.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jr	$25
sw	$3,12($2)

.set	macro
.set	reorder
.end	mad_stream_buffer
.size	mad_stream_buffer, .-mad_stream_buffer
.align	2
.globl	mad_stream_skip
.set	nomips16
.set	nomicromips
.ent	mad_stream_skip
.type	mad_stream_skip, @function
mad_stream_skip:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,8($4)
addu	$5,$2,$5
j	$31
sw	$5,8($4)

.set	macro
.set	reorder
.end	mad_stream_skip
.size	mad_stream_skip, .-mad_stream_skip
.align	2
.globl	mad_stream_sync
.set	nomips16
.set	nomicromips
.ent	mad_stream_sync
.type	mad_stream_sync, @function
mad_stream_sync:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,32($sp)
addiu	$17,$4,28
sw	$16,28($sp)
move	$16,$4
.cprestore	16
sw	$31,36($sp)
lw	$25,%call16(mad_bit_nextbyte)($28)
.reloc	1f,R_MIPS_JALR,mad_bit_nextbyte
1:	jalr	$25
move	$4,$17

lw	$7,4($16)
addiu	$5,$7,-1
sltu	$3,$2,$5
beq	$3,$0,$L23
lw	$28,16($sp)

li	$6,255			# 0xff
li	$9,-32			# 0xffffffffffffffe0
.option	pic0
j	$L21
.option	pic2
li	$8,224			# 0xe0

$L18:
addiu	$2,$2,1
$L24:
beq	$2,$5,$L17
nop

$L21:
lbu	$3,0($2)
bne	$3,$6,$L18
nop

lbu	$3,1($2)
and	$3,$9,$3
bne	$3,$8,$L24
addiu	$2,$2,1

addiu	$2,$2,-1
$L23:
move	$5,$2
$L17:
subu	$7,$7,$5
slt	$7,$7,8
bne	$7,$0,$L20
lw	$25,%call16(mad_bit_init)($28)

.reloc	1f,R_MIPS_JALR,mad_bit_init
1:	jalr	$25
move	$4,$17

move	$2,$0
$L19:
lw	$31,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,40

$L20:
.option	pic0
j	$L19
.option	pic2
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	mad_stream_sync
.size	mad_stream_sync, .-mad_stream_sync
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"no error\000"
.align	2
$LC1:
.ascii	"input buffer too small (or EOF)\000"
.align	2
$LC2:
.ascii	"invalid (null) buffer pointer\000"
.align	2
$LC3:
.ascii	"not enough memory\000"
.align	2
$LC4:
.ascii	"lost synchronization\000"
.align	2
$LC5:
.ascii	"reserved header layer value\000"
.align	2
$LC6:
.ascii	"forbidden bitrate value\000"
.align	2
$LC7:
.ascii	"reserved sample frequency value\000"
.align	2
$LC8:
.ascii	"reserved emphasis value\000"
.align	2
$LC9:
.ascii	"CRC check failed\000"
.align	2
$LC10:
.ascii	"forbidden bit allocation value\000"
.align	2
$LC11:
.ascii	"bad scalefactor index\000"
.align	2
$LC12:
.ascii	"bad bitrate/mode combination\000"
.align	2
$LC13:
.ascii	"bad frame length\000"
.align	2
$LC14:
.ascii	"bad big_values count\000"
.align	2
$LC15:
.ascii	"reserved block_type\000"
.align	2
$LC16:
.ascii	"bad scalefactor selection info\000"
.align	2
$LC17:
.ascii	"bad main_data_begin pointer\000"
.align	2
$LC18:
.ascii	"bad audio data length\000"
.align	2
$LC19:
.ascii	"bad Huffman table select\000"
.align	2
$LC20:
.ascii	"Huffman data overrun\000"
.align	2
$LC21:
.ascii	"incompatible block_type for JS\000"
.text
.align	2
.globl	mad_stream_errorstr
.set	nomips16
.set	nomicromips
.ent	mad_stream_errorstr
.type	mad_stream_errorstr, @function
mad_stream_errorstr:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,60($4)
li	$3,529			# 0x211
beq	$2,$3,$L27
sltu	$3,$2,530

bne	$3,$0,$L56
li	$3,564			# 0x234

beq	$2,$3,$L41
sltu	$3,$2,565

beq	$3,$0,$L42
li	$3,561			# 0x231

beq	$2,$3,$L43
sltu	$3,$2,562

beq	$3,$0,$L44
li	$3,562			# 0x232

li	$3,545			# 0x221
beq	$2,$3,$L45
li	$3,546			# 0x222

bne	$2,$3,$L26
nop

lui	$2,%hi($LC12)
j	$31
addiu	$2,$2,%lo($LC12)

$L56:
li	$3,257			# 0x101
beq	$2,$3,$L29
sltu	$3,$2,258

bne	$3,$0,$L57
li	$3,260			# 0x104

beq	$2,$3,$L35
sltu	$3,$2,261

beq	$3,$0,$L36
li	$3,261			# 0x105

li	$3,258			# 0x102
beq	$2,$3,$L37
li	$3,259			# 0x103

bne	$2,$3,$L26
nop

lui	$2,%hi($LC6)
j	$31
addiu	$2,$2,%lo($LC6)

$L42:
li	$3,567			# 0x237
beq	$2,$3,$L49
sltu	$3,$2,568

beq	$3,$0,$L50
li	$3,568			# 0x238

li	$3,565			# 0x235
beq	$2,$3,$L51
li	$3,566			# 0x236

bne	$2,$3,$L26
nop

lui	$2,%hi($LC18)
j	$31
addiu	$2,$2,%lo($LC18)

$L57:
li	$3,1			# 0x1
beq	$2,$3,$L31
nop

beq	$2,$0,$L55
li	$3,2			# 0x2

beq	$2,$3,$L33
li	$3,49			# 0x31

bne	$2,$3,$L26
nop

lui	$2,%hi($LC3)
j	$31
addiu	$2,$2,%lo($LC3)

$L36:
beq	$2,$3,$L39
li	$3,513			# 0x201

bne	$2,$3,$L26
nop

lui	$2,%hi($LC9)
j	$31
addiu	$2,$2,%lo($LC9)

$L44:
beq	$2,$3,$L47
li	$3,563			# 0x233

bne	$2,$3,$L26
nop

lui	$2,%hi($LC15)
j	$31
addiu	$2,$2,%lo($LC15)

$L50:
beq	$2,$3,$L53
li	$3,569			# 0x239

bne	$2,$3,$L26
nop

lui	$2,%hi($LC21)
j	$31
addiu	$2,$2,%lo($LC21)

$L51:
lui	$2,%hi($LC17)
j	$31
addiu	$2,$2,%lo($LC17)

$L45:
lui	$2,%hi($LC11)
j	$31
addiu	$2,$2,%lo($LC11)

$L37:
lui	$2,%hi($LC5)
j	$31
addiu	$2,$2,%lo($LC5)

$L53:
lui	$2,%hi($LC20)
j	$31
addiu	$2,$2,%lo($LC20)

$L47:
lui	$2,%hi($LC14)
j	$31
addiu	$2,$2,%lo($LC14)

$L39:
lui	$2,%hi($LC8)
j	$31
addiu	$2,$2,%lo($LC8)

$L41:
lui	$2,%hi($LC16)
j	$31
addiu	$2,$2,%lo($LC16)

$L43:
lui	$2,%hi($LC13)
j	$31
addiu	$2,$2,%lo($LC13)

$L35:
lui	$2,%hi($LC7)
j	$31
addiu	$2,$2,%lo($LC7)

$L49:
lui	$2,%hi($LC19)
j	$31
addiu	$2,$2,%lo($LC19)

$L26:
j	$31
move	$2,$0

$L33:
lui	$2,%hi($LC2)
j	$31
addiu	$2,$2,%lo($LC2)

$L27:
lui	$2,%hi($LC10)
j	$31
addiu	$2,$2,%lo($LC10)

$L29:
lui	$2,%hi($LC4)
j	$31
addiu	$2,$2,%lo($LC4)

$L55:
lui	$2,%hi($LC0)
j	$31
addiu	$2,$2,%lo($LC0)

$L31:
lui	$2,%hi($LC1)
j	$31
addiu	$2,$2,%lo($LC1)

.set	macro
.set	reorder
.end	mad_stream_errorstr
.size	mad_stream_errorstr, .-mad_stream_errorstr
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
