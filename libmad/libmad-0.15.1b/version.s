.file	1 "version.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.globl	mad_build
.rdata
.align	2
.type	mad_build, @object
.size	mad_build, 10
mad_build:
.ascii	"FPM_MIPS \000"
.globl	mad_author
.align	2
.type	mad_author, @object
.size	mad_author, 48
mad_author:
.ascii	"Underbit Technologies, Inc. <info@underbit.com>\000"
.globl	mad_copyright
.align	2
.type	mad_copyright, @object
.size	mad_copyright, 52
mad_copyright:
.ascii	"Copyright (C) 2000-2004 Underbit Technologies, Inc.\000"
.globl	mad_version
.align	2
.type	mad_version, @object
.size	mad_version, 33
mad_version:
.ascii	"MPEG Audio Decoder 0.15.1 (beta)\000"
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
