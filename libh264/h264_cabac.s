.file	1 "h264_cabac.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.get_cabac_bypass,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_cabac_bypass
.type	get_cabac_bypass, @function
get_cabac_bypass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,0($4)
sll	$2,$2,1
andi	$3,$2,0xffff
bne	$3,$0,$L2
sw	$2,0($4)

lw	$3,16($4)
addiu	$6,$3,2
lbu	$5,0($3)
lbu	$3,1($3)
sw	$6,16($4)
sll	$5,$5,9
sll	$3,$3,1
addu	$3,$5,$3
addu	$2,$2,$3
li	$3,-65536			# 0xffffffffffff0000
addiu	$3,$3,1
addu	$2,$2,$3
sw	$2,0($4)
$L2:
lw	$3,4($4)
sll	$3,$3,17
slt	$5,$2,$3
bne	$5,$0,$L4
subu	$3,$2,$3

li	$2,1			# 0x1
j	$31
sw	$3,0($4)

$L4:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	get_cabac_bypass
.size	get_cabac_bypass, .-get_cabac_bypass
.section	.text.fill_decode_neighbors,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	fill_decode_neighbors
.type	fill_decode_neighbors, @function
fill_decode_neighbors:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$6,65536			# 0x10000
lw	$3,168($4)
li	$7,131072			# 0x20000
lw	$13,2192($4)
addu	$6,$4,$6
addu	$7,$4,$7
lw	$2,-5248($6)
lw	$8,-5252($6)
lw	$6,9448($7)
li	$7,-1			# 0xffffffffffffffff
sll	$2,$3,$2
sw	$7,10812($4)
subu	$2,$6,$2
lw	$7,%got(left_block_options.7269)($28)
addiu	$12,$6,-1
addiu	$9,$2,-1
addiu	$11,$2,1
addiu	$7,$7,%lo(left_block_options.7269)
beq	$8,$0,$L19
sw	$7,10808($4)

li	$7,1073676288			# 0x3fff0000
lw	$10,7996($4)
andi	$5,$5,0x80
ori	$7,$7,0xffff
addu	$8,$6,$7
andi	$10,$10,0x1
sll	$8,$8,2
addu	$8,$13,$8
lw	$8,0($8)
beq	$10,$0,$L9
andi	$8,$8,0x80

beq	$8,$5,$L19
subu	$6,$6,$3

bne	$5,$0,$L20
addiu	$12,$6,-1

lw	$5,%got(left_block_options.7269+16)($28)
addu	$9,$9,$3
sw	$0,10812($4)
move	$3,$12
addiu	$5,$5,%lo(left_block_options.7269+16)
b	$L8
sw	$5,10808($4)

$L19:
move	$3,$12
$L8:
sll	$5,$9,2
sw	$9,10768($4)
sw	$3,10784($4)
sll	$7,$2,2
addu	$5,$13,$5
sw	$2,10772($4)
sw	$11,10776($4)
addu	$7,$13,$7
sw	$12,10780($4)
sll	$6,$11,2
lw	$8,0($5)
sll	$9,$9,1
addu	$6,$13,$6
sll	$5,$12,2
sll	$3,$3,2
sw	$8,10788($4)
li	$8,65536			# 0x10000
lw	$14,0($7)
addu	$5,$13,$5
addu	$8,$4,$8
addu	$3,$13,$3
lw	$10,-5268($8)
lw	$7,-5272($8)
sw	$14,10792($4)
lw	$8,0($6)
addu	$9,$10,$9
lhu	$6,0($9)
sw	$8,10796($4)
lw	$5,0($5)
sw	$5,10800($4)
lw	$3,0($3)
beq	$6,$7,$L13
sw	$3,10804($4)

sll	$2,$2,1
sw	$0,10788($4)
addu	$2,$10,$2
lhu	$2,0($2)
beq	$7,$2,$L21
sll	$6,$12,1

sw	$0,10792($4)
$L21:
addu	$6,$10,$6
lhu	$2,0($6)
beq	$7,$2,$L22
sll	$2,$11,1

sw	$0,10804($4)
sw	$0,10800($4)
$L13:
sll	$2,$11,1
$L22:
addu	$2,$10,$2
lhu	$2,0($2)
beq	$7,$2,$L23
nop

j	$31
sw	$0,10796($4)

$L23:
j	$31
nop

$L9:
beq	$5,$0,$L11
addu	$7,$2,$7

sll	$7,$7,2
addu	$7,$13,$7
lw	$14,0($7)
lw	$10,8($7)
lw	$6,4($7)
srl	$14,$14,7
srl	$7,$10,7
andi	$10,$14,0x1
move	$14,$3
movn	$14,$0,$10
andi	$7,$7,0x1
srl	$6,$6,7
andi	$6,$6,0x1
move	$10,$14
move	$14,$3
movn	$14,$0,$7
addu	$9,$10,$9
move	$7,$14
move	$14,$3
movn	$14,$0,$6
addu	$11,$7,$11
beq	$8,$5,$L19
addu	$2,$14,$2

$L20:
lw	$5,%got(left_block_options.7269+48)($28)
addu	$3,$3,$12
addiu	$5,$5,%lo(left_block_options.7269+48)
b	$L8
sw	$5,10808($4)

$L11:
beq	$8,$0,$L19
lw	$5,%got(left_block_options.7269+32)($28)

move	$3,$12
addiu	$5,$5,%lo(left_block_options.7269+32)
b	$L8
sw	$5,10808($4)

.set	macro
.set	reorder
.end	fill_decode_neighbors
.size	fill_decode_neighbors, .-fill_decode_neighbors
.section	.text.fill_decode_caches,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	fill_decode_caches
.type	fill_decode_caches, @function
fill_decode_caches:
.frame	$sp,184,$31		# vars= 136, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,10780($4)
addiu	$sp,$sp,-184
lw	$11,10784($4)
andi	$10,$5,0x800
lw	$2,10800($4)
lw	$9,10804($4)
lw	$6,10768($4)
lw	$8,10772($4)
lw	$7,10776($4)
lw	$14,10792($4)
sw	$23,176($sp)
sw	$21,168($sp)
sw	$19,160($sp)
sw	$fp,180($sp)
sw	$22,172($sp)
sw	$20,164($sp)
sw	$18,156($sp)
sw	$17,152($sp)
sw	$16,148($sp)
sw	$3,16($sp)
sw	$11,20($sp)
sw	$2,8($sp)
sw	$9,12($sp)
lw	$19,10808($4)
lw	$23,10788($4)
bne	$10,$0,$L25
lw	$21,10796($4)

andi	$10,$5,0x7
beq	$10,$0,$L26
li	$13,-1			# 0xffffffffffffffff

lw	$15,12880($4)
li	$11,7			# 0x7
li	$12,65535			# 0xffff
movz	$11,$13,$15
sw	$12,11096($4)
sw	$12,11088($4)
sw	$12,11084($4)
li	$12,61162			# 0xeeea
and	$13,$11,$14
bne	$13,$0,$L28
sw	$12,11092($4)

li	$12,46079			# 0xb3ff
sw	$12,11084($4)
li	$12,13311			# 0x33ff
sw	$12,11088($4)
li	$12,9962			# 0x26ea
sw	$12,11092($4)
$L28:
xor	$12,$5,$2
andi	$12,$12,0x80
bne	$12,$0,$L198
andi	$12,$5,0x80

and	$3,$11,$2
$L213:
bne	$3,$0,$L214
and	$3,$11,$23

$L34:
lw	$3,11084($4)
li	$9,24415			# 0x5f5f
andi	$3,$3,0xdf5f
sw	$9,11096($4)
sw	$3,11084($4)
$L33:
and	$3,$11,$23
$L214:
bne	$3,$0,$L215
and	$3,$11,$21

lw	$3,11084($4)
andi	$3,$3,0x7fff
sw	$3,11084($4)
and	$3,$11,$21
$L215:
bne	$3,$0,$L216
andi	$3,$5,0x1

lw	$3,11092($4)
andi	$3,$3,0xfbff
sw	$3,11092($4)
andi	$3,$5,0x1
$L216:
beq	$3,$0,$L38
andi	$3,$14,0x1

beq	$3,$0,$L39
li	$3,-1			# 0xffffffffffffffff

lw	$12,11856($4)
sll	$9,$8,2
lw	$3,10856($4)
addu	$9,$12,$9
lw	$9,0($9)
addu	$3,$3,$9
lw	$3,0($3)
sw	$3,10820($4)
$L46:
andi	$3,$2,0x1
bne	$3,$0,$L40
li	$9,6			# 0x6

li	$3,-1			# 0xffffffffffffffff
li	$9,2			# 0x2
and	$2,$11,$2
movn	$3,$9,$2
sb	$3,10835($4)
sb	$3,10827($4)
$L42:
lw	$3,12($sp)
andi	$2,$3,0x1
bne	$2,$0,$L43
lw	$2,8($sp)

and	$11,$11,$3
li	$3,2			# 0x2
li	$9,-1			# 0xffffffffffffffff
movz	$3,$9,$11
sb	$3,10851($4)
sb	$3,10843($4)
$L38:
bne	$14,$0,$L141
li	$9,1077936128			# 0x40400000

li	$3,64			# 0x40
addiu	$9,$9,16448
sb	$3,11210($4)
sw	$9,11188($4)
sb	$3,11209($4)
sb	$3,11186($4)
sb	$3,11185($4)
$L55:
bne	$2,$0,$L217
lw	$2,16($sp)

$L207:
li	$2,64			# 0x40
movz	$2,$0,$10
sb	$2,11216($4)
sb	$2,11192($4)
sb	$2,11203($4)
sb	$2,11195($4)
lw	$2,12($sp)
bne	$2,$0,$L218
lw	$2,20($sp)

li	$2,64			# 0x40
$L233:
movz	$2,$0,$10
sb	$2,11224($4)
sb	$2,11200($4)
sb	$2,11219($4)
beq	$14,$0,$L199
sb	$2,11211($4)

$L53:
li	$2,131072			# 0x20000
sll	$3,$8,1
addu	$2,$4,$2
lw	$9,8732($2)
addu	$3,$9,$3
lhu	$3,0($3)
sw	$3,8740($2)
lw	$2,8($sp)
beq	$2,$0,$L219
li	$2,463			# 0x1cf

li	$11,131072			# 0x20000
$L234:
lw	$12,16($sp)
lw	$10,20($sp)
li	$3,-2			# 0xfffffffffffffffe
addu	$11,$4,$11
lbu	$9,2($19)
lbu	$2,0($19)
sll	$15,$12,1
sll	$10,$10,1
lw	$12,8732($11)
and	$13,$9,$3
and	$3,$2,$3
addu	$2,$12,$10
addu	$12,$12,$15
lhu	$9,0($2)
lhu	$10,0($12)
sra	$9,$9,$13
sra	$2,$10,$3
andi	$9,$9,0x2
andi	$3,$2,0x2
sll	$9,$9,2
andi	$2,$10,0x1f0
or	$2,$3,$2
or	$2,$2,$9
sw	$2,8744($11)
$L25:
andi	$2,$5,0x78
bne	$2,$0,$L220
li	$15,65536			# 0x10000

andi	$2,$5,0x100
beq	$2,$0,$L62
li	$2,65536			# 0x10000

addu	$2,$4,$2
lw	$2,5140($2)
beq	$2,$0,$L211
lbu	$3,11($sp)

$L220:
addu	$15,$4,$15
lw	$2,6632($15)
beq	$2,$0,$L62
lw	$2,12($sp)

sll	$8,$8,2
li	$18,-2			# 0xfffffffffffffffe
lw	$fp,8($sp)
li	$22,-1			# 0xffffffffffffffff
sw	$8,28($sp)
move	$8,$18
sw	$2,40($sp)
move	$3,$18
lw	$2,16($sp)
move	$9,$22
movn	$8,$22,$21
sll	$7,$7,2
sll	$6,$6,2
sll	$2,$2,2
movz	$9,$18,$fp
li	$17,131072			# 0x20000
movn	$3,$22,$23
move	$12,$0
move	$25,$23
sw	$2,64($sp)
lw	$2,20($sp)
lw	$13,64($sp)
lw	$10,28($sp)
sll	$2,$2,2
lw	$11,28($sp)
sw	$8,88($sp)
addiu	$13,$13,1
addiu	$10,$10,2
sw	$3,84($sp)
sw	$2,100($sp)
addiu	$2,$7,2
sw	$13,104($sp)
addiu	$11,$11,3
lw	$8,100($sp)
addiu	$3,$6,1
lw	$13,40($sp)
sw	$9,108($sp)
addu	$9,$4,$17
addiu	$8,$8,1
sw	$2,76($sp)
sw	$10,68($sp)
andi	$13,$13,0x100
sw	$11,72($sp)
andi	$10,$fp,0x100
sw	$8,136($sp)
andi	$11,$fp,0x40
sw	$3,80($sp)
andi	$3,$fp,0x80
sw	$9,52($sp)
andi	$9,$5,0x50
sw	$10,112($sp)
andi	$10,$5,0x900
sw	$11,124($sp)
andi	$11,$14,0x100
sw	$13,116($sp)
andi	$13,$14,0x40
lw	$2,40($sp)
lw	$8,40($sp)
sw	$7,56($sp)
move	$7,$4
andi	$2,$2,0x40
sw	$6,60($sp)
andi	$8,$8,0x80
sw	$3,92($sp)
sw	$2,128($sp)
move	$3,$4
sw	$8,96($sp)
move	$6,$4
sw	$9,32($sp)
sw	$fp,44($sp)
move	$fp,$21
sw	$10,36($sp)
sw	$11,120($sp)
sw	$13,132($sp)
$L123:
li	$8,12288			# 0x3000
sll	$2,$12,1
sll	$2,$8,$2
and	$8,$2,$5
beq	$8,$0,$L64
nop

and	$23,$2,$14
beq	$23,$0,$L65
sw	$0,11648($7)

lw	$9,11852($4)
lw	$8,28($sp)
lw	$10,11864($4)
lw	$11,2184($7)
addu	$9,$9,$8
sll	$8,$10,1
lw	$9,0($9)
addu	$8,$8,$10
addu	$9,$8,$9
sll	$9,$9,2
addu	$8,$11,$9
lw	$10,0($8)
lw	$11,4($8)
sw	$10,11264($3)
sw	$11,11268($3)
lw	$8,2184($7)
lw	$10,68($sp)
lw	$11,72($sp)
addu	$9,$8,$9
lw	$8,8($9)
lw	$9,12($9)
sw	$8,11272($3)
sw	$9,11276($3)
lw	$8,2276($7)
addu	$9,$8,$10
addu	$8,$8,$11
lw	$11,32($sp)
lb	$9,0($9)
sb	$9,11573($6)
sb	$9,11572($6)
lb	$8,0($8)
sb	$8,11575($6)
beq	$11,$0,$L68
sb	$8,11574($6)

$L200:
addiu	$8,$sp,8
sw	$19,48($sp)
move	$13,$0
sw	$3,140($sp)
addiu	$11,$sp,16
li	$24,8			# 0x8
sw	$8,24($sp)
move	$21,$19
move	$20,$6
move	$16,$3
$L72:
addiu	$9,$sp,8
addu	$8,$9,$13
move	$9,$18
lw	$8,0($8)
movn	$9,$22,$8
and	$8,$8,$2
beq	$8,$0,$L69
addu	$10,$11,$13

lw	$8,0($10)
lw	$9,11852($4)
lbu	$19,0($21)
sll	$8,$8,2
lw	$3,11864($4)
addu	$9,$9,$8
addiu	$8,$8,1
lw	$10,0($9)
mul	$9,$19,$3
lw	$3,2184($7)
addiu	$10,$10,3
addu	$19,$9,$10
sll	$19,$19,2
addu	$19,$3,$19
lw	$9,0($19)
sw	$9,11292($16)
lbu	$9,1($21)
lw	$19,11864($4)
mul	$3,$9,$19
addu	$9,$3,$10
lw	$10,2184($7)
sll	$9,$9,2
addu	$9,$10,$9
lw	$9,0($9)
sw	$9,11324($16)
lw	$9,2276($7)
lbu	$10,0($21)
addu	$8,$9,$8
and	$10,$10,$18
addu	$10,$8,$10
lb	$9,0($10)
sb	$9,11579($20)
lbu	$9,1($21)
and	$9,$9,$18
addu	$8,$8,$9
lb	$8,0($8)
sb	$8,11587($20)
$L70:
addiu	$13,$13,4
addiu	$21,$21,2
addiu	$16,$16,64
bne	$13,$24,$L72
addiu	$20,$20,16

lw	$19,48($sp)
lw	$3,140($sp)
$L73:
and	$8,$2,$fp
beq	$8,$0,$L221
lw	$10,88($sp)

$L201:
lw	$9,11852($4)
lw	$13,56($sp)
lw	$10,11864($4)
lw	$11,2184($7)
addu	$9,$9,$13
sll	$8,$10,1
lw	$9,0($9)
addu	$8,$8,$10
addu	$8,$8,$9
lw	$9,76($sp)
sll	$8,$8,2
addu	$8,$11,$8
lw	$8,0($8)
sw	$8,11280($3)
lw	$8,2276($7)
addu	$8,$8,$9
lb	$8,0($8)
sb	$8,11576($6)
and	$8,$2,$25
beq	$8,$0,$L222
lw	$10,84($sp)

$L202:
lw	$10,11852($4)
lw	$13,60($sp)
lw	$8,11864($4)
lw	$11,10812($4)
addu	$10,$10,$13
lw	$16,2184($7)
sll	$9,$8,1
andi	$13,$11,0x2
lw	$10,0($10)
and	$9,$9,$11
lw	$11,36($sp)
addu	$8,$8,$10
addu	$8,$8,$9
lw	$9,80($sp)
addiu	$8,$8,3
sll	$8,$8,2
addu	$8,$16,$8
lw	$8,0($8)
sw	$8,11260($3)
lw	$8,2276($7)
addu	$8,$8,$9
addu	$13,$8,$13
lb	$8,0($13)
beq	$11,$0,$L82
sb	$8,11571($6)

$L203:
lw	$2,-5252($15)
beq	$2,$0,$L64
li	$2,-2			# 0xfffffffffffffffe

sb	$2,11600($6)
sb	$2,11592($6)
sb	$2,11584($6)
$L124:
lw	$2,-5248($15)
$L212:
beq	$2,$0,$L100
andi	$2,$25,0x80

bne	$2,$0,$L223
andi	$2,$14,0x80

lb	$2,11571($6)
bltz	$2,$L101
sll	$2,$2,1

addiu	$8,$17,8782
sb	$2,11571($6)
addu	$8,$3,$8
lh	$9,11262($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11262($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L101:
andi	$2,$14,0x80
$L223:
bne	$2,$0,$L224
andi	$2,$fp,0x80

lb	$2,11572($6)
bltz	$2,$L104
sll	$2,$2,1

addiu	$8,$17,8786
sb	$2,11572($6)
addu	$8,$3,$8
lh	$9,11266($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11266($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L104:
lb	$2,11573($6)
bltz	$2,$L105
sll	$2,$2,1

addiu	$8,$17,8790
sb	$2,11573($6)
addu	$8,$3,$8
lh	$9,11270($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11270($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L105:
lb	$2,11574($6)
bltz	$2,$L106
sll	$2,$2,1

addiu	$8,$17,8794
sb	$2,11574($6)
addu	$8,$3,$8
lh	$9,11274($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11274($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L106:
lb	$2,11575($6)
bltz	$2,$L103
sll	$2,$2,1

addiu	$8,$17,8798
sb	$2,11575($6)
addu	$8,$3,$8
lh	$9,11278($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11278($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L103:
andi	$2,$fp,0x80
$L224:
bne	$2,$0,$L225
lw	$13,92($sp)

lb	$2,11576($6)
bltz	$2,$L225
sll	$2,$2,1

addiu	$8,$17,8802
sb	$2,11576($6)
addu	$8,$3,$8
lh	$9,11282($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11282($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
lw	$13,92($sp)
$L225:
bne	$13,$0,$L226
lw	$2,96($sp)

lb	$2,11579($6)
bltz	$2,$L110
sll	$2,$2,1

addiu	$8,$17,8814
sb	$2,11579($6)
addu	$8,$3,$8
lh	$9,11294($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11294($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L110:
lb	$2,11587($6)
bltz	$2,$L109
sll	$2,$2,1

addiu	$8,$17,8846
sb	$2,11587($6)
addu	$8,$3,$8
lh	$9,11326($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11326($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L109:
lw	$2,96($sp)
$L226:
bne	$2,$0,$L64
nop

lb	$2,11595($6)
bltz	$2,$L111
sll	$2,$2,1

addiu	$8,$17,8878
sb	$2,11595($6)
addu	$8,$3,$8
lh	$9,11358($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11358($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L111:
lb	$2,11603($6)
bltz	$2,$L64
sll	$2,$2,1

addiu	$8,$17,8910
sb	$2,11603($6)
addu	$8,$3,$8
lh	$9,11390($3)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11390($3)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L64:
lw	$2,6632($15)
addiu	$12,$12,1
addiu	$6,$6,40
addiu	$3,$3,160
sltu	$2,$12,$2
bne	$2,$0,$L123
addiu	$7,$7,4

$L62:
lbu	$3,11($sp)
$L211:
srl	$14,$14,24
lw	$fp,180($sp)
andi	$2,$14,0x1
lw	$23,176($sp)
andi	$14,$3,0x1
lw	$22,172($sp)
addu	$14,$2,$14
lw	$21,168($sp)
lw	$20,164($sp)
lw	$19,160($sp)
lw	$18,156($sp)
lw	$17,152($sp)
lw	$16,148($sp)
addiu	$sp,$sp,184
j	$31
sw	$14,11656($4)

$L65:
move	$9,$0
move	$8,$0
move	$11,$0
sw	$9,11268($3)
li	$9,-16908288			# 0xfffffffffefe0000
sw	$8,11264($3)
li	$8,-1			# 0xffffffffffffffff
ori	$9,$9,0xfefe
sw	$11,11276($3)
movz	$8,$9,$14
move	$10,$0
lw	$11,32($sp)
sw	$10,11272($3)
bne	$11,$0,$L200
sw	$8,11572($6)

$L68:
lw	$11,44($sp)
and	$8,$2,$11
beq	$8,$0,$L74
lw	$11,108($sp)

lbu	$8,0($19)
lw	$10,11864($4)
lw	$9,11852($4)
lw	$13,64($sp)
lw	$11,2184($7)
addu	$9,$9,$13
mul	$13,$8,$10
lw	$10,104($sp)
lw	$9,0($9)
addu	$8,$13,$9
addiu	$8,$8,3
sll	$8,$8,2
addu	$8,$11,$8
lw	$8,0($8)
sw	$8,11292($3)
lw	$8,2276($7)
lbu	$9,0($19)
addu	$8,$8,$10
and	$9,$9,$18
addu	$8,$8,$9
lb	$8,0($8)
sb	$8,11579($6)
and	$8,$2,$fp
bne	$8,$0,$L201
lw	$10,88($sp)

$L221:
and	$8,$2,$25
sw	$0,11280($3)
bne	$8,$0,$L202
sb	$10,11576($6)

lw	$10,84($sp)
$L222:
lw	$11,36($sp)
sw	$0,11260($3)
bne	$11,$0,$L203
sb	$10,11571($6)

$L82:
li	$8,-2			# 0xfffffffffffffffe
sb	$8,11600($6)
sb	$8,11592($6)
sb	$8,11584($6)
sb	$8,11598($6)
sb	$8,11582($6)
sw	$0,11304($3)
beq	$23,$0,$L204
sw	$0,11368($3)

lw	$10,11856($4)
addiu	$8,$17,8756
lw	$13,28($sp)
addu	$9,$7,$8
addu	$16,$3,$8
addu	$10,$10,$13
lw	$9,0($9)
lw	$10,0($10)
sll	$10,$10,2
addu	$10,$9,$10
lw	$9,0($10)
sw	$9,28($16)
lw	$9,4($10)
sw	$9,32($16)
lw	$9,8($10)
sw	$9,36($16)
lw	$9,12($10)
sw	$9,40($16)
$L85:
lw	$9,44($sp)
and	$8,$2,$9
beq	$8,$0,$L86
addiu	$8,$17,8844

lw	$8,11856($4)
addiu	$10,$17,8756
lw	$13,64($sp)
lbu	$9,0($19)
addu	$11,$7,$10
addu	$8,$8,$13
lw	$11,0($11)
addu	$13,$3,$10
lw	$8,0($8)
addiu	$8,$8,6
subu	$9,$8,$9
sll	$9,$9,2
addu	$9,$11,$9
lw	$9,0($9)
sw	$9,56($13)
lbu	$9,1($19)
subu	$8,$8,$9
sll	$8,$8,2
addu	$8,$11,$8
lw	$8,0($8)
sw	$8,88($13)
$L87:
lw	$8,40($sp)
and	$2,$2,$8
beq	$2,$0,$L88
addiu	$2,$17,8908

lw	$2,11856($4)
addiu	$9,$17,8756
lw	$11,100($sp)
li	$13,131072			# 0x20000
lbu	$8,2($19)
addu	$10,$7,$9
ori	$13,$13,0x22b8
addu	$2,$2,$11
lw	$10,0($10)
addu	$11,$3,$9
li	$9,131072			# 0x20000
lw	$2,0($2)
ori	$9,$9,0x2278
addiu	$2,$2,6
subu	$8,$2,$8
sll	$8,$8,2
addu	$8,$10,$8
lw	$8,0($8)
sw	$8,120($11)
lbu	$8,3($19)
subu	$2,$2,$8
addu	$8,$3,$13
sll	$2,$2,2
addu	$2,$10,$2
lw	$2,0($2)
sw	$2,152($11)
addu	$2,$3,$9
sw	$0,0($8)
sw	$0,0($2)
li	$2,3			# 0x3
lw	$8,-5260($15)
beq	$8,$2,$L227
li	$11,67371008			# 0x4040000

$L205:
lw	$2,-5252($15)
bne	$2,$0,$L124
nop

$L197:
lw	$2,6632($15)
addiu	$12,$12,1
addiu	$6,$6,40
addiu	$3,$3,160
sltu	$2,$12,$2
bne	$2,$0,$L123
addiu	$7,$7,4

b	$L211
lbu	$3,11($sp)

$L100:
beq	$2,$0,$L228
andi	$2,$14,0x80

lb	$2,11571($6)
bltz	$2,$L112
sra	$2,$2,1

addiu	$8,$17,8782
sb	$2,11571($6)
addu	$2,$3,$8
lh	$8,11262($3)
sll	$8,$8,1
sh	$8,11262($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L112:
andi	$2,$14,0x80
$L228:
beq	$2,$0,$L229
andi	$2,$fp,0x80

lb	$2,11572($6)
bltz	$2,$L115
sra	$2,$2,1

addiu	$8,$17,8786
sb	$2,11572($6)
addu	$2,$3,$8
lh	$8,11266($3)
sll	$8,$8,1
sh	$8,11266($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L115:
lb	$2,11573($6)
bltz	$2,$L116
sra	$2,$2,1

addiu	$8,$17,8790
sb	$2,11573($6)
addu	$2,$3,$8
lh	$8,11270($3)
sll	$8,$8,1
sh	$8,11270($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L116:
lb	$2,11574($6)
bltz	$2,$L117
sra	$2,$2,1

addiu	$8,$17,8794
sb	$2,11574($6)
addu	$2,$3,$8
lh	$8,11274($3)
sll	$8,$8,1
sh	$8,11274($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L117:
lb	$2,11575($6)
bltz	$2,$L114
sra	$2,$2,1

addiu	$8,$17,8798
sb	$2,11575($6)
addu	$2,$3,$8
lh	$8,11278($3)
sll	$8,$8,1
sh	$8,11278($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L114:
andi	$2,$fp,0x80
$L229:
beq	$2,$0,$L230
lw	$8,92($sp)

lb	$2,11576($6)
bltz	$2,$L230
sra	$2,$2,1

addiu	$8,$17,8802
sb	$2,11576($6)
addu	$2,$3,$8
lh	$8,11282($3)
sll	$8,$8,1
sh	$8,11282($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
lw	$8,92($sp)
$L230:
beq	$8,$0,$L231
lw	$9,96($sp)

lb	$2,11579($6)
bltz	$2,$L121
sra	$2,$2,1

addiu	$8,$17,8814
sb	$2,11579($6)
addu	$2,$3,$8
lh	$8,11294($3)
sll	$8,$8,1
sh	$8,11294($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L121:
lb	$2,11587($6)
bltz	$2,$L120
sra	$2,$2,1

addiu	$8,$17,8846
sb	$2,11587($6)
addu	$2,$3,$8
lh	$8,11326($3)
sll	$8,$8,1
sh	$8,11326($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L120:
lw	$9,96($sp)
$L231:
beq	$9,$0,$L64
nop

lb	$2,11595($6)
bltz	$2,$L122
sra	$2,$2,1

addiu	$8,$17,8878
sb	$2,11595($6)
addu	$2,$3,$8
lh	$8,11358($3)
sll	$8,$8,1
sh	$8,11358($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L122:
lb	$2,11603($6)
bltz	$2,$L64
sra	$2,$2,1

addiu	$8,$17,8910
addiu	$12,$12,1
sb	$2,11603($6)
addu	$2,$3,$8
lh	$8,11390($3)
addiu	$6,$6,40
addiu	$3,$3,160
sll	$8,$8,1
sh	$8,11230($3)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
lw	$2,6632($15)
sltu	$2,$12,$2
bne	$2,$0,$L123
addiu	$7,$7,4

b	$L211
lbu	$3,11($sp)

$L69:
sw	$0,11292($16)
sw	$0,11324($16)
sb	$9,11587($20)
b	$L70
sb	$9,11579($20)

$L74:
sw	$0,11292($3)
b	$L73
sb	$11,11579($6)

$L88:
li	$13,131072			# 0x20000
li	$9,131072			# 0x20000
addu	$8,$3,$2
ori	$13,$13,0x22b8
ori	$9,$9,0x2278
sw	$0,0($8)
sw	$0,-32($8)
addu	$2,$3,$9
addu	$8,$3,$13
sw	$0,0($8)
sw	$0,0($2)
li	$2,3			# 0x3
lw	$8,-5260($15)
bne	$8,$2,$L205
li	$11,67371008			# 0x4040000

$L227:
lw	$10,52($sp)
lw	$13,120($sp)
ori	$11,$11,0x404
sw	$11,9104($10)
sw	$11,9112($10)
sw	$11,9120($10)
bne	$13,$0,$L206
sw	$11,9128($10)

lw	$8,132($sp)
beq	$8,$0,$L94
li	$2,67371008			# 0x4040000

lw	$9,52($sp)
lw	$10,68($sp)
lw	$11,72($sp)
lw	$2,9088($9)
addu	$8,$2,$10
addu	$2,$2,$11
lbu	$8,0($8)
sb	$8,9096($9)
lbu	$2,0($2)
sb	$2,9098($9)
$L93:
lw	$8,112($sp)
beq	$8,$0,$L232
lw	$10,124($sp)

$L210:
li	$2,-128			# 0xffffffffffffff80
lw	$9,52($sp)
sb	$2,9103($9)
$L96:
lw	$9,116($sp)
beq	$9,$0,$L98
lw	$11,128($sp)

lw	$2,-5252($15)
li	$8,-128			# 0xffffffffffffff80
lw	$10,52($sp)
beq	$2,$0,$L197
sb	$8,9119($10)

b	$L212
lw	$2,-5248($15)

$L86:
addu	$9,$3,$8
sw	$0,0($9)
b	$L87
sw	$0,-32($9)

$L204:
addiu	$8,$17,8796
addu	$11,$3,$8
sw	$0,0($11)
sw	$0,-4($11)
sw	$0,-8($11)
b	$L85
sw	$0,-12($11)

$L26:
bne	$14,$0,$L141
move	$3,$0

move	$9,$0
sb	$3,11210($4)
sw	$9,11188($4)
sb	$3,11209($4)
sb	$3,11186($4)
beq	$2,$0,$L207
sb	$3,11185($4)

lw	$2,16($sp)
$L217:
lw	$3,11232($4)
lbu	$9,8($19)
sll	$2,$2,5
addu	$2,$3,$2
addu	$3,$2,$9
lbu	$3,0($3)
sb	$3,11195($4)
lbu	$3,9($19)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11203($4)
lbu	$3,12($19)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11192($4)
lbu	$3,13($19)
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,11216($4)
lw	$2,12($sp)
beq	$2,$0,$L233
li	$2,64			# 0x40

lw	$2,20($sp)
$L218:
lw	$3,11232($4)
lbu	$9,10($19)
sll	$2,$2,5
addu	$2,$3,$2
addu	$3,$2,$9
lbu	$3,0($3)
sb	$3,11211($4)
lbu	$3,11($19)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11219($4)
lbu	$3,14($19)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11200($4)
lbu	$3,15($19)
addu	$2,$2,$3
lbu	$2,0($2)
bne	$14,$0,$L53
sb	$2,11224($4)

$L199:
li	$2,463			# 0x1cf
li	$3,15			# 0xf
movn	$3,$2,$10
li	$2,131072			# 0x20000
addu	$2,$4,$2
sw	$3,8740($2)
lw	$2,8($sp)
bne	$2,$0,$L234
li	$11,131072			# 0x20000

li	$2,463			# 0x1cf
$L219:
li	$3,15			# 0xf
movn	$3,$2,$10
li	$2,131072			# 0x20000
addu	$2,$4,$2
b	$L25
sw	$3,8744($2)

$L141:
lw	$3,11232($4)
sll	$9,$8,5
addu	$3,$3,$9
lw	$9,28($3)
sw	$9,11188($4)
lbu	$9,9($3)
sb	$9,11185($4)
lbu	$9,10($3)
sb	$9,11186($4)
lbu	$9,17($3)
sb	$9,11209($4)
lbu	$3,18($3)
b	$L55
sb	$3,11210($4)

$L206:
li	$2,-2139095040			# 0xffffffff80800000
lw	$8,112($sp)
ori	$2,$2,0x8080
bne	$8,$0,$L210
sw	$2,9096($10)

lw	$10,124($sp)
$L232:
beq	$10,$0,$L97
li	$2,4			# 0x4

lw	$11,52($sp)
lw	$13,104($sp)
lbu	$8,0($19)
lw	$2,9088($11)
and	$8,$8,$18
addu	$2,$2,$13
addu	$2,$2,$8
lbu	$2,0($2)
b	$L96
sb	$2,9103($11)

$L198:
beq	$12,$0,$L30
nop

and	$3,$11,$2
bne	$3,$0,$L31
li	$12,24575			# 0x5fff

lw	$3,11084($4)
andi	$3,$3,0xdfff
sw	$12,11096($4)
sw	$3,11084($4)
$L31:
and	$9,$11,$9
bne	$9,$0,$L214
and	$3,$11,$23

lw	$9,11084($4)
lw	$3,11096($4)
andi	$9,$9,0xff5f
andi	$3,$3,0xff5f
sw	$9,11084($4)
b	$L33
sw	$3,11096($4)

$L98:
beq	$11,$0,$L99
li	$8,4			# 0x4

lw	$13,52($sp)
lw	$10,136($sp)
lbu	$9,2($19)
lw	$2,-5252($15)
lw	$8,9088($13)
and	$9,$9,$18
addu	$8,$8,$10
addu	$8,$8,$9
lbu	$8,0($8)
beq	$2,$0,$L197
sb	$8,9119($13)

b	$L212
lw	$2,-5248($15)

$L94:
lw	$13,52($sp)
ori	$2,$2,0x404
b	$L93
sw	$2,9096($13)

$L99:
lw	$2,-5252($15)
lw	$11,52($sp)
beq	$2,$0,$L197
sb	$8,9119($11)

b	$L212
lw	$2,-5248($15)

$L97:
lw	$8,52($sp)
b	$L96
sb	$2,9103($8)

$L30:
lw	$12,168($4)
lw	$9,2192($4)
addu	$3,$3,$12
sll	$3,$3,2
addu	$3,$9,$3
lw	$3,0($3)
and	$3,$11,$3
beq	$3,$0,$L34
and	$3,$11,$2

b	$L213
nop

$L39:
li	$9,2			# 0x2
movn	$3,$9,$13
sb	$3,10823($4)
sb	$3,10822($4)
sb	$3,10821($4)
b	$L46
sb	$3,10820($4)

$L43:
lw	$13,20($sp)
li	$11,6			# 0x6
lw	$12,11856($4)
lbu	$9,2($19)
sll	$13,$13,2
lw	$3,10856($4)
addu	$12,$12,$13
subu	$9,$11,$9
lw	$12,0($12)
addu	$3,$3,$12
addu	$9,$3,$9
lb	$9,0($9)
sb	$9,10843($4)
lbu	$9,3($19)
subu	$11,$11,$9
addu	$3,$3,$11
lb	$3,0($3)
b	$L38
sb	$3,10851($4)

$L40:
lw	$13,16($sp)
lw	$12,11856($4)
lbu	$3,0($19)
sll	$13,$13,2
lw	$2,10856($4)
addu	$12,$12,$13
subu	$3,$9,$3
lw	$12,0($12)
addu	$2,$2,$12
addu	$3,$2,$3
lb	$3,0($3)
sb	$3,10827($4)
lbu	$3,1($19)
subu	$9,$9,$3
addu	$2,$2,$9
lb	$2,0($2)
b	$L42
sb	$2,10835($4)

.set	macro
.set	reorder
.end	fill_decode_caches
.size	fill_decode_caches, .-fill_decode_caches
.section	.text.pred_motion,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	pred_motion
.type	pred_motion, @function
pred_motion:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,%got(scan8)($28)
sll	$11,$7,3
sll	$12,$7,5
li	$24,65536			# 0x10000
addiu	$3,$3,%lo(scan8)
addu	$2,$11,$12
addu	$5,$3,$5
addu	$15,$4,$2
addu	$24,$4,$24
lbu	$14,0($5)
addiu	$sp,$sp,-8
lw	$25,-5252($24)
addiu	$5,$14,-8
lw	$9,24($sp)
addiu	$10,$14,-1
sw	$16,0($sp)
addu	$3,$2,$5
sw	$17,4($sp)
addu	$2,$2,$10
addu	$6,$5,$6
addiu	$2,$2,2812
addiu	$8,$3,2812
addu	$5,$15,$5
addu	$10,$15,$10
addu	$16,$15,$6
sll	$3,$2,2
lb	$13,11568($5)
sll	$8,$8,2
lb	$10,11568($10)
addu	$3,$4,$3
lb	$16,11568($16)
addu	$8,$4,$8
beq	$25,$0,$L236
li	$2,-2			# 0xfffffffffffffffe

beq	$16,$2,$L275
slt	$2,$14,20

addu	$5,$11,$12
$L286:
addu	$2,$5,$6
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$4,$2
$L241:
xor	$6,$13,$9
xor	$4,$9,$16
sltu	$6,$6,1
sltu	$4,$4,1
xor	$5,$10,$9
addu	$4,$4,$6
sltu	$5,$5,1
addu	$4,$4,$5
slt	$7,$4,2
bne	$7,$0,$L283
li	$7,1			# 0x1

$L251:
lh	$5,0($3)
lh	$4,0($8)
slt	$7,$4,$5
bne	$7,$0,$L277
lh	$6,0($2)

slt	$7,$6,$4
beq	$7,$0,$L253
nop

slt	$4,$6,$5
movz	$5,$6,$4
move	$4,$5
$L253:
lh	$5,2($3)
lw	$6,28($sp)
lh	$3,2($8)
lh	$2,2($2)
sw	$4,0($6)
slt	$4,$3,$5
beq	$4,$0,$L284
slt	$4,$2,$3

slt	$4,$3,$2
$L287:
beq	$4,$0,$L285
lw	$17,32($sp)

slt	$3,$5,$2
movz	$5,$2,$3
move	$3,$5
$L285:
sw	$3,0($17)
$L235:
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L236:
bne	$16,$2,$L286
addu	$5,$11,$12

$L256:
addiu	$14,$14,-9
addu	$5,$11,$12
xor	$6,$13,$9
addu	$2,$5,$14
addu	$5,$4,$5
addiu	$2,$2,2812
addu	$5,$5,$14
sll	$2,$2,2
sltu	$6,$6,1
lb	$16,11568($5)
addu	$2,$4,$2
xor	$5,$10,$9
sltu	$5,$5,1
xor	$4,$9,$16
sltu	$4,$4,1
addu	$4,$4,$6
addu	$4,$4,$5
slt	$7,$4,2
beq	$7,$0,$L251
li	$7,1			# 0x1

$L283:
beq	$4,$7,$L278
li	$4,-2			# 0xfffffffffffffffe

bne	$13,$4,$L251
nop

bne	$16,$13,$L251
nop

beq	$10,$16,$L251
lw	$17,28($sp)

lh	$4,0($3)
lh	$2,2($3)
lw	$3,32($sp)
sw	$4,0($17)
b	$L235
sw	$2,0($3)

$L277:
slt	$7,$4,$6
beq	$7,$0,$L253
nop

slt	$4,$5,$6
lh	$2,2($2)
movz	$5,$6,$4
lw	$6,28($sp)
move	$4,$5
lh	$5,2($3)
lh	$3,2($8)
sw	$4,0($6)
slt	$4,$3,$5
bne	$4,$0,$L287
slt	$4,$3,$2

slt	$4,$2,$3
$L284:
beq	$4,$0,$L285
lw	$17,32($sp)

slt	$3,$2,$5
movz	$5,$2,$3
b	$L285
move	$3,$5

$L275:
bne	$2,$0,$L256
li	$2,4			# 0x4

andi	$5,$14,0x7
bne	$5,$2,$L256
nop

lb	$2,11579($15)
beq	$2,$16,$L256
sll	$5,$7,7

lw	$6,2696($4)
addu	$5,$12,$5
addiu	$2,$5,11288
lw	$15,104($6)
addu	$2,$4,$2
sw	$0,0($2)
lw	$6,-5248($24)
beq	$6,$0,$L280
nop

lw	$6,10800($4)
andi	$6,$6,0x80
bne	$6,$0,$L256
slt	$6,$14,36

xori	$6,$6,0x1
addiu	$6,$6,2694
li	$11,12288			# 0x3000
sll	$6,$6,2
sll	$16,$7,1
addu	$6,$4,$6
sll	$16,$11,$16
sra	$14,$14,2
lw	$12,4($6)
sll	$12,$12,2
addu	$11,$15,$12
lw	$11,0($11)
and	$11,$16,$11
beq	$11,$0,$L258
andi	$6,$14,0x3

lw	$24,11864($4)
addu	$5,$4,$5
lw	$16,11852($4)
sll	$7,$7,2
lw	$15,2696($4)
andi	$11,$14,0x2
mul	$4,$6,$24
addu	$6,$16,$12
addu	$7,$15,$7
lw	$14,0($6)
lw	$6,96($7)
addiu	$4,$4,3
addu	$4,$4,$14
sll	$4,$4,2
addu	$4,$6,$4
lh	$6,0($4)
sh	$6,11288($5)
lh	$4,2($4)
srl	$6,$4,31
addu	$6,$6,$4
sra	$6,$6,1
sh	$6,11290($5)
lw	$5,188($7)
addu	$4,$5,$12
addu	$4,$4,$11
lb	$16,1($4)
b	$L241
sll	$16,$16,1

$L278:
bne	$5,$0,$L281
nop

bne	$6,$0,$L282
lw	$4,28($sp)

lh	$3,0($2)
lh	$2,2($2)
lw	$6,32($sp)
sw	$3,0($4)
b	$L235
sw	$2,0($6)

$L282:
lh	$3,0($8)
lh	$2,2($8)
lw	$6,28($sp)
lw	$17,32($sp)
sw	$3,0($6)
b	$L235
sw	$2,0($17)

$L281:
lh	$4,0($3)
lh	$2,2($3)
lw	$3,28($sp)
sw	$4,0($3)
lw	$4,32($sp)
b	$L235
sw	$2,0($4)

$L280:
lw	$6,10800($4)
andi	$6,$6,0x80
beq	$6,$0,$L256
sll	$25,$7,1

lw	$6,7996($4)
sra	$14,$14,5
lw	$11,168($4)
lw	$24,10780($4)
li	$12,12288			# 0x3000
andi	$6,$6,0x1
sll	$6,$6,1
addu	$24,$11,$24
addu	$6,$6,$14
sll	$12,$12,$25
sra	$14,$6,2
mul	$17,$11,$14
addu	$11,$17,$24
sll	$11,$11,2
addu	$11,$15,$11
lw	$11,0($11)
and	$12,$12,$11
beq	$12,$0,$L258
sll	$11,$24,2

lw	$14,11852($4)
lw	$15,11864($4)
addu	$5,$4,$5
lw	$12,2696($4)
sll	$7,$7,2
addu	$14,$14,$11
mul	$4,$6,$15
addu	$7,$12,$7
lw	$14,0($14)
and	$16,$6,$16
lw	$12,96($7)
addiu	$14,$14,3
addu	$6,$4,$14
sll	$6,$6,2
addu	$6,$12,$6
lh	$4,0($6)
sh	$4,11288($5)
lhu	$6,2($6)
sll	$6,$6,1
sh	$6,11290($5)
lw	$4,188($7)
addu	$11,$4,$11
addu	$4,$11,$16
lb	$16,1($4)
b	$L241
sra	$16,$16,1

$L258:
b	$L241
li	$16,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	pred_motion
.size	pred_motion, .-pred_motion
.section	.text.refill.isra.0,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	refill.isra.0
.type	refill.isra.0, @function
refill.isra.0:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$7,0($5)
lw	$3,0($4)
addiu	$8,$7,2
lbu	$6,0($7)
lbu	$2,1($7)
sll	$6,$6,9
sll	$2,$2,1
addu	$2,$6,$2
addu	$3,$2,$3
li	$2,-65536			# 0xffffffffffff0000
addiu	$2,$2,1
addu	$2,$3,$2
sw	$2,0($4)
j	$31
sw	$8,0($5)

.set	macro
.set	reorder
.end	refill.isra.0
.size	refill.isra.0, .-refill.isra.0
.section	.text.refill2.isra.1,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	refill2.isra.1
.type	refill2.isra.1, @function
refill2.isra.1:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$8,0($4)
lw	$2,0($5)
addiu	$9,$8,-1
addiu	$10,$2,2
lbu	$7,0($2)
xor	$9,$9,$8
lbu	$3,1($2)
sra	$9,$9,15
lw	$2,%got(ff_h264_norm_shift)($28)
sll	$7,$7,9
sll	$3,$3,1
addu	$9,$2,$9
li	$2,7			# 0x7
addu	$6,$7,$3
lbu	$9,0($9)
subu	$7,$2,$9
li	$2,-65536			# 0xffffffffffff0000
addiu	$2,$2,1
addu	$3,$6,$2
sll	$2,$3,$7
addu	$2,$8,$2
sw	$2,0($4)
j	$31
sw	$10,0($5)

.set	macro
.set	reorder
.end	refill2.isra.1
.size	refill2.isra.1, .-refill2.isra.1
.section	.text.get_cabac_noinline,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_cabac_noinline
.type	get_cabac_noinline, @function
get_cabac_noinline:
.frame	$sp,40,$31		# vars= 8, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$7,4($4)
addiu	$sp,$sp,-40
lw	$8,%got(ff_h264_lps_range)($28)
.cprestore	16
sw	$31,36($sp)
andi	$3,$7,0xc0
lbu	$11,0($5)
sll	$3,$3,1
lw	$9,0($4)
addu	$3,$3,$11
addu	$3,$8,$3
lbu	$8,0($3)
subu	$7,$7,$8
sll	$10,$7,17
subu	$3,$10,$9
sra	$2,$3,31
lw	$3,%got(ff_h264_mlps_state)($28)
movn	$7,$8,$2
and	$10,$2,$10
subu	$9,$9,$10
xor	$2,$2,$11
addu	$3,$3,$2
sw	$9,0($4)
andi	$2,$2,0x1
sw	$7,4($4)
lbu	$3,128($3)
lw	$7,%got(ff_h264_norm_shift)($28)
sb	$3,0($5)
lw	$3,4($4)
lw	$5,0($4)
addu	$7,$7,$3
lbu	$7,0($7)
sll	$5,$5,$7
sll	$3,$3,$7
andi	$7,$5,0xffff
sw	$5,0($4)
bne	$7,$0,$L291
sw	$3,4($4)

lw	$25,%got(refill2.isra.1)($28)
addiu	$5,$4,16
addiu	$25,$25,%lo(refill2.isra.1)
.reloc	1f,R_MIPS_JALR,refill2.isra.1
1:	jalr	$25
sw	$2,24($sp)

lw	$2,24($sp)
$L291:
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	get_cabac_noinline
.size	get_cabac_noinline, .-get_cabac_noinline
.section	.text.decode_cabac_field_decoding_flag,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_field_decoding_flag
.type	decode_cabac_field_decoding_flag, @function
decode_cabac_field_decoding_flag:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$8,131072			# 0x20000
lw	$7,168($4)
li	$9,65536			# 0x10000
lw	$10,2192($4)
addu	$2,$4,$8
lw	$3,7992($4)
addu	$9,$4,$9
lw	$25,%got(get_cabac_noinline)($28)
sll	$7,$7,1
lw	$6,9448($2)
move	$5,$4
addiu	$11,$8,8272
lw	$2,-5248($9)
addiu	$4,$8,8224
lw	$8,-5268($9)
subu	$7,$6,$7
lw	$9,-5272($9)
sltu	$3,$0,$3
sll	$6,$7,1
sll	$7,$7,2
addu	$8,$8,$6
addu	$7,$10,$7
and	$3,$3,$2
lhu	$6,0($8)
addiu	$3,$3,70
lw	$2,0($7)
addu	$4,$5,$4
addiu	$25,$25,%lo(get_cabac_noinline)
xor	$6,$6,$9
srl	$2,$2,7
sltu	$6,$6,1
and	$2,$6,$2
addu	$2,$3,$2
addu	$2,$2,$11
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jr	$25
addu	$5,$5,$2

.set	macro
.set	reorder
.end	decode_cabac_field_decoding_flag
.size	decode_cabac_field_decoding_flag, .-decode_cabac_field_decoding_flag
.section	.text.decode_cabac_intra_mb_type,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_intra_mb_type
.type	decode_cabac_intra_mb_type, @function
decode_cabac_intra_mb_type:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
li	$3,131072			# 0x20000
sw	$17,28($sp)
addiu	$17,$3,8272
.cprestore	16
sw	$19,36($sp)
move	$19,$6
addu	$5,$5,$17
sw	$16,24($sp)
sw	$31,44($sp)
move	$16,$4
sw	$20,40($sp)
addu	$17,$4,$5
beq	$6,$0,$L295
sw	$18,32($sp)

lw	$2,10800($4)
addiu	$3,$3,8224
lw	$4,10792($4)
addu	$18,$16,$3
lw	$20,%got(get_cabac_noinline)($28)
andi	$2,$2,0x6
sltu	$2,$0,$2
addiu	$5,$2,1
andi	$3,$4,0x6
movn	$2,$5,$3
addiu	$20,$20,%lo(get_cabac_noinline)
move	$4,$18
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$17,$2

beq	$2,$0,$L300
addiu	$17,$17,2

li	$2,131072			# 0x20000
$L317:
addu	$16,$16,$2
lw	$5,8228($16)
lw	$4,8224($16)
addiu	$3,$5,-2
sll	$2,$3,17
slt	$2,$4,$2
bne	$2,$0,$L315
sw	$3,8228($16)

lw	$4,8240($16)
lw	$3,8236($16)
beq	$4,$3,$L304
li	$2,25			# 0x19

lw	$31,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L295:
lw	$20,%got(get_cabac_noinline)($28)
addiu	$3,$3,8224
move	$5,$17
addu	$18,$4,$3
addiu	$20,$20,%lo(get_cabac_noinline)
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$18

bne	$2,$0,$L317
li	$2,131072			# 0x20000

$L300:
lw	$31,44($sp)
move	$2,$0
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L315:
addiu	$5,$5,-258
srl	$5,$5,31
sll	$4,$4,$5
sll	$3,$3,$5
andi	$2,$4,0xffff
sw	$4,8224($16)
bne	$2,$0,$L304
sw	$3,8228($16)

lw	$3,8240($16)
addiu	$6,$3,2
lbu	$2,0($3)
lbu	$5,1($3)
sw	$6,8240($16)
sll	$3,$2,9
sll	$2,$5,1
addu	$2,$3,$2
addu	$4,$4,$2
li	$2,-65536			# 0xffffffffffff0000
addiu	$2,$2,1
addu	$4,$4,$2
sw	$4,8224($16)
$L304:
addiu	$5,$17,1
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$18

addiu	$5,$17,2
sll	$16,$2,2
sll	$2,$2,4
move	$4,$18
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
subu	$16,$2,$16

bne	$2,$0,$L316
addiu	$16,$16,1

$L303:
addiu	$5,$19,3
sll	$19,$19,1
addu	$5,$17,$5
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$18

addiu	$5,$19,3
sll	$2,$2,1
addu	$5,$17,$5
move	$4,$18
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$16,$2,$16

lw	$31,44($sp)
addu	$2,$2,$16
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L316:
addiu	$5,$19,2
move	$4,$18
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$17,$5

addiu	$2,$2,1
sll	$2,$2,2
b	$L303
addu	$16,$16,$2

.set	macro
.set	reorder
.end	decode_cabac_intra_mb_type
.size	decode_cabac_intra_mb_type, .-decode_cabac_intra_mb_type
.section	.text.decode_cabac_mb_skip,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_skip
.type	decode_cabac_mb_skip, @function
decode_cabac_mb_skip:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,65536			# 0x10000
addu	$10,$4,$2
lw	$2,-5252($10)
beq	$2,$0,$L319
move	$3,$4

lw	$12,168($4)
li	$2,-2			# 0xfffffffffffffffe
andi	$4,$6,0x1
and	$2,$6,$2
mul	$7,$2,$12
addu	$2,$7,$5
beq	$4,$0,$L320
addiu	$7,$2,-1

lw	$13,-5268($10)
move	$11,$7
sll	$7,$7,1
lw	$8,-5272($10)
addu	$7,$13,$7
lhu	$7,0($7)
beq	$7,$8,$L337
sll	$4,$11,2

lw	$9,-5248($10)
$L322:
beq	$9,$0,$L331
nop

subu	$5,$2,$12
sll	$4,$5,1
addu	$4,$13,$4
lhu	$4,0($4)
$L326:
beq	$8,$7,$L324
nop

$L335:
beq	$8,$4,$L325
move	$2,$0

$L328:
li	$4,65536			# 0x10000
lw	$25,%got(get_cabac_noinline)($28)
addiu	$7,$2,13
addu	$4,$3,$4
li	$5,131072			# 0x20000
addiu	$25,$25,%lo(get_cabac_noinline)
lw	$6,-5260($4)
addiu	$4,$5,8224
addu	$4,$3,$4
xori	$6,$6,0x3
movz	$2,$7,$6
addu	$2,$2,$5
addiu	$5,$2,8283
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jr	$25
addu	$5,$3,$5

$L319:
li	$6,131072			# 0x20000
lw	$2,10384($4)
lw	$5,168($4)
addu	$6,$4,$6
lw	$4,-5268($10)
xori	$2,$2,0x3
lw	$8,-5272($10)
sltu	$2,$0,$2
lw	$6,9448($6)
sll	$2,$5,$2
addiu	$11,$6,-1
subu	$5,$6,$2
sll	$2,$11,1
sll	$6,$5,1
addu	$2,$4,$2
addu	$4,$4,$6
lhu	$7,0($2)
bne	$8,$7,$L335
lhu	$4,0($4)

$L324:
lw	$6,2192($3)
sll	$2,$11,2
addu	$2,$6,$2
lw	$2,0($2)
srl	$2,$2,11
xori	$2,$2,0x1
bne	$8,$4,$L328
andi	$2,$2,0x1

$L325:
lw	$4,2192($3)
sll	$5,$5,2
addiu	$7,$2,1
lw	$25,%got(get_cabac_noinline)($28)
addu	$5,$4,$5
addiu	$25,$25,%lo(get_cabac_noinline)
lw	$4,0($5)
li	$5,131072			# 0x20000
andi	$4,$4,0x800
movz	$2,$7,$4
li	$4,65536			# 0x10000
addu	$4,$3,$4
addiu	$7,$2,13
lw	$6,-5260($4)
addiu	$4,$5,8224
addu	$4,$3,$4
xori	$6,$6,0x3
movz	$2,$7,$6
addu	$2,$2,$5
addiu	$5,$2,8283
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jr	$25
addu	$5,$3,$5

$L320:
lw	$4,-5248($10)
beq	$4,$0,$L338
sll	$9,$7,1

subu	$5,$2,$12
lw	$4,-5268($10)
lw	$6,-5272($10)
sll	$2,$5,1
addu	$2,$4,$2
lhu	$8,0($2)
beq	$8,$6,$L332
sll	$2,$7,1

addu	$4,$4,$2
lhu	$2,0($4)
beq	$6,$2,$L336
move	$11,$7

b	$L328
move	$2,$0

$L338:
lw	$13,-5268($10)
move	$11,$7
lw	$8,-5272($10)
addu	$9,$13,$9
lhu	$7,0($9)
$L331:
addiu	$6,$6,-1
mul	$2,$12,$6
addu	$5,$2,$5
sll	$4,$5,1
addu	$4,$13,$4
b	$L326
lhu	$4,0($4)

$L337:
lw	$14,2192($3)
lw	$9,-5248($10)
addu	$4,$14,$4
lw	$4,0($4)
srl	$4,$4,7
andi	$4,$4,0x1
bne	$9,$4,$L322
nop

addu	$11,$12,$11
sll	$4,$11,1
addu	$4,$13,$4
b	$L322
lhu	$7,0($4)

$L332:
lw	$2,2192($3)
sll	$6,$5,2
addu	$2,$2,$6
lw	$2,0($2)
andi	$2,$2,0x80
bne	$2,$0,$L323
sll	$2,$7,1

addu	$4,$4,$2
lhu	$2,0($4)
beq	$8,$2,$L334
move	$11,$7

b	$L325
move	$2,$0

$L323:
subu	$5,$5,$12
sll	$6,$5,1
addu	$2,$4,$2
addu	$6,$4,$6
move	$11,$7
lhu	$7,0($2)
b	$L326
lhu	$4,0($6)

$L334:
b	$L324
move	$4,$8

$L336:
move	$4,$8
b	$L324
move	$8,$6

.set	macro
.set	reorder
.end	decode_cabac_mb_skip
.size	decode_cabac_mb_skip, .-decode_cabac_mb_skip
.section	.text.get_cabac,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_cabac
.type	get_cabac, @function
get_cabac:
.frame	$sp,40,$31		# vars= 8, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$7,4($4)
addiu	$sp,$sp,-40
lw	$8,%got(ff_h264_lps_range)($28)
.cprestore	16
sw	$31,36($sp)
andi	$3,$7,0xc0
lbu	$11,0($5)
sll	$3,$3,1
lw	$9,0($4)
addu	$3,$3,$11
addu	$3,$8,$3
lbu	$8,0($3)
subu	$7,$7,$8
sll	$10,$7,17
subu	$3,$10,$9
sra	$2,$3,31
lw	$3,%got(ff_h264_mlps_state)($28)
movn	$7,$8,$2
and	$10,$2,$10
subu	$9,$9,$10
xor	$2,$2,$11
addu	$3,$3,$2
sw	$9,0($4)
andi	$2,$2,0x1
sw	$7,4($4)
lbu	$3,128($3)
lw	$7,%got(ff_h264_norm_shift)($28)
sb	$3,0($5)
lw	$3,4($4)
lw	$5,0($4)
addu	$7,$7,$3
lbu	$7,0($7)
sll	$5,$5,$7
sll	$3,$3,$7
andi	$7,$5,0xffff
sw	$5,0($4)
bne	$7,$0,$L340
sw	$3,4($4)

lw	$25,%got(refill2.isra.1)($28)
addiu	$5,$4,16
addiu	$25,$25,%lo(refill2.isra.1)
.reloc	1f,R_MIPS_JALR,refill2.isra.1
1:	jalr	$25
sw	$2,24($sp)

lw	$2,24($sp)
$L340:
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	get_cabac
.size	get_cabac, .-get_cabac
.section	.text.decode_cabac_mb_intra4x4_pred_mode,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_intra4x4_pred_mode
.type	decode_cabac_mb_intra4x4_pred_mode, @function
decode_cabac_mb_intra4x4_pred_mode:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,36($sp)
li	$19,131072			# 0x20000
sw	$17,28($sp)
sw	$20,40($sp)
addiu	$20,$19,8224
sw	$16,24($sp)
addiu	$2,$19,8340
lw	$17,%got(get_cabac)($28)
addu	$20,$4,$20
move	$16,$5
sw	$18,32($sp)
addu	$5,$4,$2
.cprestore	16
addiu	$17,$17,%lo(get_cabac)
sw	$31,44($sp)
move	$18,$4
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$20

beq	$2,$0,$L346
lw	$31,44($sp)

move	$2,$16
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L346:
addiu	$19,$19,8341
move	$4,$20
addu	$18,$18,$19
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$18

move	$4,$20
move	$5,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$19,$2

sll	$2,$2,1
move	$4,$20
move	$5,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
addu	$18,$19,$2

sll	$2,$2,2
lw	$31,44($sp)
lw	$20,40($sp)
addu	$2,$2,$18
lw	$19,36($sp)
lw	$18,32($sp)
slt	$16,$2,$16
lw	$17,28($sp)
xori	$16,$16,0x1
addu	$2,$16,$2
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	decode_cabac_mb_intra4x4_pred_mode
.size	decode_cabac_mb_intra4x4_pred_mode, .-decode_cabac_mb_intra4x4_pred_mode
.section	.text.decode_cabac_mb_ref,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_ref
.type	decode_cabac_mb_ref, @function
decode_cabac_mb_ref:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$7,%got(scan8)($28)
addiu	$sp,$sp,-56
sll	$3,$5,3
sll	$2,$5,5
sw	$17,28($sp)
addiu	$7,$7,%lo(scan8)
.cprestore	16
move	$17,$4
sw	$31,52($sp)
addu	$6,$7,$6
sw	$22,48($sp)
addu	$2,$3,$2
sw	$21,44($sp)
li	$4,65536			# 0x10000
sw	$20,40($sp)
lbu	$3,0($6)
addu	$2,$17,$2
addu	$4,$17,$4
sw	$19,36($sp)
sw	$18,32($sp)
addiu	$5,$3,-1
sw	$16,24($sp)
lw	$6,-5260($4)
addiu	$3,$3,-8
addu	$4,$2,$5
addu	$2,$2,$3
lb	$16,11568($4)
li	$4,3			# 0x3
beq	$6,$4,$L358
lb	$2,11568($2)

slt	$16,$0,$16
addiu	$3,$16,2
slt	$2,$0,$2
movn	$16,$3,$2
$L350:
li	$18,131072			# 0x20000
lw	$20,%got(get_cabac)($28)
move	$22,$0
addiu	$19,$18,8224
addiu	$20,$20,%lo(get_cabac)
addu	$19,$17,$19
addiu	$18,$18,8272
b	$L352
li	$21,32			# 0x20

$L353:
beq	$22,$21,$L359
addiu	$16,$16,4

$L352:
addiu	$5,$16,54
move	$4,$19
addu	$5,$5,$18
move	$25,$20
addu	$5,$17,$5
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sra	$16,$16,2

bne	$2,$0,$L353
addiu	$22,$22,1

addiu	$22,$22,-1
lw	$31,52($sp)
move	$2,$22
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L359:
lw	$31,52($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L358:
blez	$16,$L354
addu	$5,$17,$5

li	$4,131072			# 0x20000
addu	$5,$4,$5
lb	$16,9092($5)
nor	$16,$0,$16
srl	$16,$16,31
$L349:
blez	$2,$L350
li	$2,131072			# 0x20000

addu	$3,$17,$3
addiu	$4,$16,2
addu	$3,$2,$3
lb	$2,9092($3)
slt	$2,$2,0
b	$L350
movz	$16,$4,$2

$L354:
b	$L349
move	$16,$0

.set	macro
.set	reorder
.end	decode_cabac_mb_ref
.size	decode_cabac_mb_ref, .-decode_cabac_mb_ref
.section	.text.decode_cabac_residual_dc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_residual_dc
.type	decode_cabac_residual_dc, @function
decode_cabac_residual_dc:
.frame	$sp,376,$31		# vars= 312, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$3,131072			# 0x20000
addiu	$sp,$sp,-376
addu	$3,$4,$3
sw	$4,376($sp)
sw	$5,380($sp)
.cprestore	16
sw	$20,352($sp)
move	$20,$6
sw	$31,372($sp)
sw	$fp,368($sp)
sw	$23,364($sp)
sw	$22,360($sp)
sw	$21,356($sp)
sw	$19,348($sp)
sw	$18,344($sp)
sw	$17,340($sp)
sw	$16,336($sp)
sw	$7,388($sp)
lw	$2,8240($3)
lw	$5,8228($3)
lw	$4,8224($3)
sw	$2,296($sp)
sw	$5,284($sp)
sw	$4,280($sp)
bne	$6,$0,$L361
lw	$2,8744($3)

lw	$3,8740($3)
andi	$2,$2,0x100
andi	$3,$3,0x100
$L362:
sltu	$2,$0,$2
lw	$19,%got(get_cabac)($28)
addiu	$4,$2,2
sll	$22,$20,2
movn	$2,$4,$3
li	$17,131072			# 0x20000
addiu	$16,$sp,280
addiu	$21,$17,8272
lw	$3,376($sp)
addiu	$19,$19,%lo(get_cabac)
addu	$5,$22,$2
move	$4,$16
addiu	$5,$5,85
move	$25,$19
addu	$5,$5,$21
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
addu	$5,$3,$5

beq	$2,$0,$L397
lw	$28,16($sp)

lw	$25,376($sp)
li	$2,65536			# 0x10000
lw	$3,%got(coeff_abs_level_m1_offset.7766)($28)
lw	$4,396($sp)
addu	$2,$25,$2
addiu	$3,$3,%lo(coeff_abs_level_m1_offset.7766)
addiu	$18,$4,-1
lw	$4,%got(last_coeff_flag_offset.7765)($28)
lw	$2,-5248($2)
addu	$22,$22,$3
addiu	$4,$4,%lo(last_coeff_flag_offset.7765)
sll	$3,$2,1
lw	$fp,0($22)
sll	$2,$2,3
move	$22,$0
subu	$2,$2,$3
lw	$3,%got(significant_coeff_flag_offset.7764)($28)
addu	$fp,$fp,$21
addu	$2,$2,$20
addiu	$3,$3,%lo(significant_coeff_flag_offset.7764)
sll	$2,$2,2
addu	$fp,$25,$fp
addu	$3,$2,$3
addu	$2,$2,$4
sw	$fp,332($sp)
lw	$17,0($3)
lw	$23,0($2)
addu	$17,$17,$21
addu	$21,$23,$21
addu	$17,$25,$17
blez	$18,$L384
addu	$23,$25,$21

move	$fp,$0
sw	$18,328($sp)
b	$L368
addiu	$21,$sp,24

$L367:
addiu	$fp,$fp,1
beq	$fp,$18,$L406
lw	$3,328($sp)

$L368:
addu	$5,$17,$fp
move	$25,$19
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$16

beq	$2,$0,$L367
lw	$28,16($sp)

sll	$2,$22,2
addu	$5,$23,$fp
addu	$2,$21,$2
move	$4,$16
move	$25,$19
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sw	$fp,0($2)

addiu	$22,$22,1
beq	$2,$0,$L367
lw	$28,16($sp)

lw	$2,396($sp)
sw	$2,328($sp)
$L366:
lw	$3,328($sp)
$L406:
beq	$3,$18,$L398
sll	$2,$22,2

beq	$20,$0,$L399
lw	$25,376($sp)

$L370:
lw	$25,376($sp)
li	$3,131072			# 0x20000
li	$2,64			# 0x40
addu	$3,$25,$3
lw	$25,388($sp)
lw	$4,9448($3)
sll	$2,$2,$25
lw	$3,8732($3)
sll	$4,$4,1
addu	$3,$3,$4
lhu	$4,0($3)
or	$2,$2,$4
sh	$2,0($3)
$L371:
sll	$23,$22,2
move	$20,$0
addiu	$23,$23,-4
addiu	$fp,$sp,296
b	$L383
addu	$23,$21,$23

$L402:
lw	$2,%got(coeff_abs_level_transition.7770)($28)
lw	$3,328($sp)
lw	$25,380($sp)
addiu	$2,$2,%lo(coeff_abs_level_transition.7770)
sll	$17,$3,1
addu	$20,$2,$20
lw	$2,280($sp)
addu	$17,$25,$17
sll	$2,$2,1
lbu	$20,0($20)
andi	$3,$2,0xffff
beq	$3,$0,$L400
sw	$2,280($sp)

$L373:
lw	$3,284($sp)
addiu	$23,$23,-4
sll	$3,$3,17
subu	$2,$2,$3
sra	$5,$2,31
and	$3,$5,$3
nor	$4,$0,$5
addu	$2,$2,$3
subu	$3,$4,$5
sw	$2,280($sp)
beq	$22,$0,$L401
sh	$3,0($17)

$L383:
lw	$2,%got(coeff_abs_level1_ctx.7768)($28)
move	$4,$16
lw	$25,332($sp)
addiu	$22,$22,-1
addiu	$2,$2,%lo(coeff_abs_level1_ctx.7768)
addu	$3,$2,$20
lw	$2,0($23)
lbu	$5,0($3)
lw	$3,392($sp)
addu	$5,$25,$5
addu	$2,$3,$2
move	$25,$19
lbu	$2,0($2)
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sw	$2,328($sp)

beq	$2,$0,$L402
lw	$28,16($sp)

lw	$3,%got(coeff_abs_levelgt1_ctx.7769)($28)
li	$17,2			# 0x2
lw	$4,%got(coeff_abs_level_transition.7770)($28)
li	$21,15			# 0xf
lw	$25,332($sp)
addiu	$3,$3,%lo(coeff_abs_levelgt1_ctx.7769)
addiu	$4,$4,%lo(coeff_abs_level_transition.7770)
addu	$2,$20,$3
addu	$20,$4,$20
lbu	$18,0($2)
lbu	$20,8($20)
addu	$18,$25,$18
move	$4,$16
$L407:
move	$25,$19
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$18

beq	$2,$0,$L403
lw	$28,16($sp)

addiu	$17,$17,1
bne	$17,$21,$L407
move	$4,$16

lw	$2,%got(get_cabac_bypass)($28)
move	$17,$0
b	$L376
addiu	$18,$2,%lo(get_cabac_bypass)

$L379:
addiu	$17,$17,1
$L376:
move	$25,$18
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$16

bne	$2,$0,$L379
lw	$28,16($sp)

beq	$17,$0,$L387
nop

li	$21,1			# 0x1
$L381:
move	$25,$18
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$16

addiu	$17,$17,-1
addu	$2,$2,$21
lw	$28,16($sp)
bne	$17,$0,$L381
addu	$21,$21,$2

addiu	$3,$21,14
subu	$21,$0,$3
$L378:
lw	$2,280($sp)
lw	$3,328($sp)
lw	$25,380($sp)
sll	$2,$2,1
sll	$17,$3,1
andi	$3,$2,0xffff
addu	$17,$25,$17
beq	$3,$0,$L404
sw	$2,280($sp)

$L382:
lw	$4,284($sp)
addiu	$23,$23,-4
sll	$4,$4,17
subu	$2,$2,$4
sra	$5,$2,31
and	$4,$5,$4
xor	$3,$5,$21
addu	$2,$2,$4
subu	$3,$3,$5
sw	$2,280($sp)
bne	$22,$0,$L383
sh	$3,0($17)

$L401:
lw	$3,376($sp)
li	$2,131072			# 0x20000
lw	$31,372($sp)
lw	$fp,368($sp)
addu	$2,$3,$2
lw	$3,284($sp)
lw	$23,364($sp)
lw	$22,360($sp)
lw	$21,356($sp)
sw	$3,8228($2)
lw	$3,280($sp)
lw	$20,352($sp)
lw	$19,348($sp)
lw	$18,344($sp)
lw	$17,340($sp)
lw	$16,336($sp)
sw	$3,8224($2)
lw	$3,296($sp)
sw	$3,8240($2)
j	$31
addiu	$sp,$sp,376

$L361:
lw	$25,388($sp)
lw	$4,8740($3)
addiu	$5,$25,6
sra	$2,$2,$5
sra	$3,$4,$5
andi	$2,$2,0x1
b	$L362
andi	$3,$3,0x1

$L399:
$L405:
li	$2,131072			# 0x20000
addu	$2,$25,$2
lw	$3,9448($2)
lw	$2,8732($2)
sll	$3,$3,1
addu	$2,$2,$3
lhu	$3,0($2)
ori	$3,$3,0x100
b	$L371
sh	$3,0($2)

$L403:
lw	$2,280($sp)
subu	$21,$0,$17
lw	$3,328($sp)
lw	$25,380($sp)
sll	$2,$2,1
sll	$17,$3,1
andi	$3,$2,0xffff
addu	$17,$25,$17
bne	$3,$0,$L382
sw	$2,280($sp)

$L404:
lw	$2,%got(refill.isra.0)($28)
move	$4,$16
addiu	$25,$2,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
move	$5,$fp

lw	$28,16($sp)
b	$L382
lw	$2,280($sp)

$L400:
lw	$2,%got(refill.isra.0)($28)
move	$4,$16
addiu	$25,$2,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
move	$5,$fp

lw	$28,16($sp)
b	$L373
lw	$2,280($sp)

$L387:
b	$L378
li	$21,-15			# 0xfffffffffffffff1

$L397:
lw	$3,376($sp)
lw	$2,284($sp)
lw	$31,372($sp)
addu	$17,$3,$17
lw	$fp,368($sp)
lw	$23,364($sp)
sw	$2,8228($17)
lw	$2,280($sp)
lw	$22,360($sp)
lw	$21,356($sp)
lw	$20,352($sp)
lw	$19,348($sp)
lw	$18,344($sp)
lw	$16,336($sp)
sw	$2,8224($17)
lw	$2,296($sp)
sw	$2,8240($17)
lw	$17,340($sp)
j	$31
addiu	$sp,$sp,376

$L398:
addiu	$22,$22,1
addu	$2,$21,$2
bne	$20,$0,$L370
sw	$3,0($2)

b	$L405
lw	$25,376($sp)

$L384:
sw	$0,328($sp)
b	$L366
addiu	$21,$sp,24

.set	macro
.set	reorder
.end	decode_cabac_residual_dc
.size	decode_cabac_residual_dc, .-decode_cabac_residual_dc
.section	.text.decode_cabac_residual_nondc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_residual_nondc
.type	decode_cabac_residual_nondc, @function
decode_cabac_residual_nondc:
.frame	$sp,384,$31		# vars= 320, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-384
sw	$20,360($sp)
li	$20,131072			# 0x20000
sw	$17,348($sp)
addu	$17,$4,$20
sw	$4,384($sp)
.cprestore	16
sw	$21,364($sp)
move	$21,$6
lw	$2,8240($17)
lw	$4,8228($17)
lw	$3,8224($17)
sw	$31,380($sp)
sw	$2,296($sp)
li	$2,5			# 0x5
sw	$fp,376($sp)
sw	$23,372($sp)
sw	$22,368($sp)
sw	$19,356($sp)
sw	$18,352($sp)
sw	$16,344($sp)
sw	$5,388($sp)
sw	$7,396($sp)
sw	$4,284($sp)
beq	$6,$2,$L409
sw	$3,280($sp)

lw	$3,%got(scan8)($28)
sll	$22,$6,2
lw	$19,%got(get_cabac)($28)
addiu	$20,$20,8272
lw	$25,384($sp)
addiu	$16,$sp,280
addiu	$3,$3,%lo(scan8)
addiu	$19,$19,%lo(get_cabac)
addu	$3,$7,$3
sw	$19,328($sp)
lbu	$23,0($3)
lw	$3,384($sp)
addu	$19,$3,$23
lbu	$2,11183($19)
lbu	$5,11176($19)
sltu	$2,$0,$2
addiu	$3,$2,2
movn	$2,$3,$5
addu	$5,$22,$2
addiu	$5,$5,85
addu	$5,$5,$20
addu	$5,$25,$5
lw	$25,328($sp)
jalr	$25
move	$4,$16

beq	$2,$0,$L452
lw	$28,16($sp)

lw	$2,408($sp)
lw	$3,384($sp)
lw	$25,384($sp)
addiu	$18,$2,-1
li	$2,65536			# 0x10000
addu	$2,$3,$2
lw	$3,%got(coeff_abs_level_m1_offset.7766)($28)
lw	$2,-5248($2)
addiu	$3,$3,%lo(coeff_abs_level_m1_offset.7766)
addu	$22,$22,$3
sll	$4,$2,1
sll	$3,$2,3
lw	$fp,0($22)
subu	$2,$3,$4
lw	$3,%got(significant_coeff_flag_offset.7764)($28)
lw	$4,%got(last_coeff_flag_offset.7765)($28)
addu	$2,$2,$21
addiu	$3,$3,%lo(significant_coeff_flag_offset.7764)
sll	$2,$2,2
addiu	$4,$4,%lo(last_coeff_flag_offset.7765)
addu	$3,$2,$3
addu	$2,$2,$4
addu	$fp,$fp,$20
lw	$17,0($3)
lw	$2,0($2)
addu	$fp,$25,$fp
addu	$17,$17,$20
addu	$20,$2,$20
sw	$fp,332($sp)
addu	$17,$25,$17
blez	$18,$L438
addu	$20,$25,$20

sw	$18,336($sp)
move	$22,$0
move	$23,$0
b	$L419
addiu	$21,$sp,24

$L417:
addiu	$23,$23,1
beq	$23,$18,$L460
lw	$3,336($sp)

$L419:
lw	$25,328($sp)
addu	$5,$17,$23
jalr	$25
move	$4,$16

beq	$2,$0,$L417
lw	$28,16($sp)

sll	$2,$22,2
lw	$25,328($sp)
addu	$5,$20,$23
addu	$2,$21,$2
move	$4,$16
sw	$23,0($2)
jalr	$25
addiu	$22,$22,1

beq	$2,$0,$L417
lw	$28,16($sp)

lw	$2,408($sp)
sw	$2,336($sp)
$L418:
lw	$3,336($sp)
$L460:
beq	$3,$18,$L453
sll	$2,$22,2

$L420:
sb	$22,11184($19)
$L433:
sll	$23,$22,2
move	$20,$0
addiu	$23,$23,-4
addiu	$fp,$sp,296
b	$L432
addu	$23,$21,$23

$L456:
lw	$2,%got(coeff_abs_level_transition.7770)($28)
sll	$3,$17,2
lw	$25,404($sp)
sll	$17,$17,1
addiu	$2,$2,%lo(coeff_abs_level_transition.7770)
addu	$3,$25,$3
lw	$25,388($sp)
addu	$20,$2,$20
lw	$2,280($sp)
lw	$18,0($3)
addu	$17,$25,$17
sll	$2,$2,1
lbu	$20,0($20)
andi	$3,$2,0xffff
sw	$2,280($sp)
beq	$3,$0,$L454
subu	$18,$0,$18

$L422:
lw	$4,284($sp)
addiu	$23,$23,-4
sll	$5,$4,17
subu	$3,$2,$5
sra	$4,$3,31
xor	$2,$4,$18
subu	$2,$2,$4
and	$4,$4,$5
addiu	$2,$2,32
addu	$3,$3,$4
sra	$2,$2,6
sw	$3,280($sp)
beq	$22,$0,$L455
sh	$2,0($17)

$L432:
lw	$4,%got(coeff_abs_level1_ctx.7768)($28)
addiu	$22,$22,-1
lw	$25,400($sp)
lw	$2,0($23)
addiu	$4,$4,%lo(coeff_abs_level1_ctx.7768)
addu	$3,$4,$20
addu	$2,$25,$2
lw	$25,328($sp)
move	$4,$16
lbu	$5,0($3)
lw	$3,332($sp)
lbu	$17,0($2)
jalr	$25
addu	$5,$3,$5

beq	$2,$0,$L456
lw	$28,16($sp)

lw	$3,%got(coeff_abs_levelgt1_ctx.7769)($28)
li	$19,2			# 0x2
lw	$4,%got(coeff_abs_level_transition.7770)($28)
li	$21,15			# 0xf
lw	$25,332($sp)
addiu	$3,$3,%lo(coeff_abs_levelgt1_ctx.7769)
addiu	$4,$4,%lo(coeff_abs_level_transition.7770)
addu	$2,$20,$3
addu	$20,$4,$20
lbu	$18,0($2)
lbu	$20,8($20)
addu	$18,$25,$18
lw	$25,328($sp)
$L461:
move	$4,$16
jalr	$25
move	$5,$18

beq	$2,$0,$L457
lw	$28,16($sp)

addiu	$19,$19,1
bne	$19,$21,$L461
lw	$25,328($sp)

lw	$2,%got(get_cabac_bypass)($28)
move	$21,$0
b	$L425
addiu	$18,$2,%lo(get_cabac_bypass)

$L428:
addiu	$21,$21,1
$L425:
move	$25,$18
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$16

bne	$2,$0,$L428
lw	$28,16($sp)

beq	$21,$0,$L437
li	$19,1			# 0x1

$L430:
move	$25,$18
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$16

addiu	$21,$21,-1
addu	$2,$2,$19
lw	$28,16($sp)
bne	$21,$0,$L430
addu	$19,$19,$2

addiu	$3,$19,14
subu	$18,$0,$3
$L427:
lw	$2,280($sp)
sll	$21,$17,1
lw	$3,388($sp)
sll	$2,$2,1
addu	$21,$3,$21
andi	$3,$2,0xffff
beq	$3,$0,$L458
sw	$2,280($sp)

$L431:
lw	$5,284($sp)
sll	$17,$17,2
lw	$25,404($sp)
addiu	$23,$23,-4
sll	$7,$5,17
addu	$17,$25,$17
subu	$4,$2,$7
sra	$5,$4,31
lw	$2,0($17)
xor	$3,$5,$18
subu	$3,$3,$5
and	$5,$5,$7
mul	$2,$3,$2
addu	$4,$4,$5
sw	$4,280($sp)
addiu	$2,$2,32
srl	$2,$2,6
bne	$22,$0,$L432
sh	$2,0($21)

$L455:
lw	$3,384($sp)
li	$2,131072			# 0x20000
lw	$31,380($sp)
lw	$fp,376($sp)
addu	$2,$3,$2
lw	$3,284($sp)
lw	$23,372($sp)
lw	$22,368($sp)
lw	$21,364($sp)
sw	$3,8228($2)
lw	$3,280($sp)
lw	$20,360($sp)
lw	$19,356($sp)
lw	$18,352($sp)
lw	$17,348($sp)
lw	$16,344($sp)
sw	$3,8224($2)
lw	$3,296($sp)
sw	$3,8240($2)
j	$31
addiu	$sp,$sp,384

$L452:
lw	$2,284($sp)
sb	$0,11184($19)
lw	$31,380($sp)
lw	$fp,376($sp)
sw	$2,8228($17)
lw	$2,280($sp)
lw	$23,372($sp)
lw	$22,368($sp)
lw	$21,364($sp)
lw	$20,360($sp)
lw	$19,356($sp)
lw	$18,352($sp)
lw	$16,344($sp)
sw	$2,8224($17)
lw	$2,296($sp)
sw	$2,8240($17)
lw	$17,348($sp)
j	$31
addiu	$sp,$sp,384

$L457:
lw	$2,280($sp)
sll	$21,$17,1
lw	$3,388($sp)
subu	$18,$0,$19
sll	$2,$2,1
addu	$21,$3,$21
andi	$3,$2,0xffff
bne	$3,$0,$L431
sw	$2,280($sp)

$L458:
lw	$4,%got(refill.isra.0)($28)
move	$5,$fp
addiu	$25,$4,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
move	$4,$16

lw	$28,16($sp)
b	$L431
lw	$2,280($sp)

$L454:
lw	$2,%got(refill.isra.0)($28)
move	$4,$16
addiu	$25,$2,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
move	$5,$fp

lw	$28,16($sp)
b	$L422
lw	$2,280($sp)

$L437:
b	$L427
li	$18,-15			# 0xfffffffffffffff1

$L409:
lw	$25,384($sp)
li	$2,65536			# 0x10000
addiu	$20,$20,8272
lw	$19,%got(get_cabac)($28)
lw	$18,%got(significant_coeff_flag_offset_8x8.7767)($28)
move	$22,$0
addu	$2,$25,$2
lw	$fp,%got(last_coeff_flag_offset_8x8)($28)
addiu	$19,$19,%lo(get_cabac)
addiu	$18,$18,%lo(significant_coeff_flag_offset_8x8.7767)
lw	$4,-5248($2)
move	$23,$0
sw	$19,328($sp)
addiu	$16,$sp,280
addiu	$21,$sp,24
sll	$6,$4,3
sll	$2,$4,5
sll	$5,$4,6
subu	$2,$2,$6
lw	$6,%got(last_coeff_flag_offset.7765)($28)
subu	$5,$5,$4
lw	$4,%got(significant_coeff_flag_offset.7764)($28)
addiu	$19,$fp,%lo(last_coeff_flag_offset_8x8)
addiu	$6,$6,%lo(last_coeff_flag_offset.7765)
addiu	$4,$4,%lo(significant_coeff_flag_offset.7764)
addu	$18,$18,$5
addu	$4,$2,$4
addu	$2,$2,$6
lw	$17,20($4)
lw	$2,20($2)
addu	$17,$17,$20
addu	$20,$2,$20
addu	$17,$25,$17
addu	$20,$25,$20
sw	$20,332($sp)
b	$L415
li	$20,63			# 0x3f

$L413:
addiu	$23,$23,1
beq	$23,$20,$L462
lw	$3,408($sp)

$L415:
addu	$2,$18,$23
lw	$25,328($sp)
move	$4,$16
lbu	$5,0($2)
jalr	$25
addu	$5,$17,$5

beq	$2,$0,$L413
lw	$28,16($sp)

addu	$5,$19,$23
lw	$3,332($sp)
sll	$2,$22,2
lw	$25,328($sp)
move	$4,$16
lbu	$5,0($5)
addu	$2,$21,$2
addiu	$fp,$22,1
sw	$23,0($2)
addu	$5,$3,$5
jalr	$25
move	$22,$fp

beq	$2,$0,$L413
lw	$28,16($sp)

lw	$23,408($sp)
move	$22,$fp
lw	$3,408($sp)
$L462:
addiu	$2,$3,-1
beq	$2,$23,$L459
sll	$2,$22,2

$L416:
lw	$2,%got(scan8)($28)
li	$fp,131072			# 0x20000
lw	$25,396($sp)
sll	$3,$22,8
addiu	$fp,$fp,8698
addiu	$2,$2,%lo(scan8)
addu	$3,$22,$3
addu	$2,$25,$2
lw	$25,384($sp)
andi	$3,$3,0xffff
lbu	$2,0($2)
addu	$fp,$25,$fp
addiu	$2,$2,11184
sw	$fp,332($sp)
addu	$2,$25,$2
sh	$3,0($2)
b	$L433
sh	$3,8($2)

$L453:
addiu	$22,$22,1
addu	$2,$21,$2
b	$L420
sw	$3,0($2)

$L459:
addiu	$22,$22,1
addu	$2,$21,$2
b	$L416
sw	$23,0($2)

$L438:
move	$22,$0
sw	$0,336($sp)
b	$L418
addiu	$21,$sp,24

.set	macro
.set	reorder
.end	decode_cabac_residual_nondc
.size	decode_cabac_residual_nondc, .-decode_cabac_residual_nondc
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"overflow in decode_cabac_mb_mvd\012\000"
.section	.text.decode_cabac_mb_mvd_bak,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_mvd_bak
.type	decode_cabac_mb_mvd_bak, @function
decode_cabac_mb_mvd_bak:
.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$2,$5,3
lw	$25,%call16(abs)($28)
sll	$5,$5,5
addiu	$sp,$sp,-64
addu	$5,$2,$5
lw	$2,%got(scan8)($28)
sw	$16,28($sp)
li	$3,65536			# 0x10000
sw	$19,40($sp)
addiu	$2,$2,%lo(scan8)
sw	$21,48($sp)
addiu	$19,$3,4384
sw	$18,36($sp)
addu	$6,$2,$6
.cprestore	16
move	$21,$7
sw	$31,60($sp)
move	$18,$4
sw	$20,44($sp)
lbu	$16,0($6)
li	$20,40			# 0x28
sw	$17,32($sp)
sw	$23,56($sp)
addu	$16,$5,$16
sw	$22,52($sp)
addiu	$2,$16,-1
addiu	$16,$16,-8
sll	$2,$2,1
sll	$16,$16,1
addu	$2,$2,$7
addu	$16,$16,$21
addu	$2,$2,$19
sll	$2,$2,1
addu	$2,$4,$2
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($2)

addu	$3,$16,$19
lw	$28,16($sp)
move	$17,$2
sll	$3,$3,1
addu	$3,$18,$3
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($3)

li	$4,47			# 0x2f
addu	$2,$17,$2
lw	$28,16($sp)
slt	$3,$2,3
bne	$3,$0,$L478
movn	$20,$4,$21

li	$3,1			# 0x1
li	$5,2			# 0x2
slt	$2,$2,33
movn	$5,$3,$2
$L465:
li	$19,131072			# 0x20000
lw	$21,%got(get_cabac)($28)
addu	$5,$5,$20
addiu	$22,$19,8272
addiu	$19,$19,8224
addu	$5,$5,$22
addu	$19,$18,$19
addiu	$21,$21,%lo(get_cabac)
addu	$5,$18,$5
move	$25,$21
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$19

beq	$2,$0,$L466
li	$16,1			# 0x1

li	$17,3			# 0x3
li	$23,9			# 0x9
$L467:
addu	$5,$17,$20
move	$4,$19
addu	$5,$5,$22
move	$25,$21
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
addu	$5,$18,$5

slt	$3,$17,6
beq	$2,$0,$L471
lw	$28,16($sp)

addiu	$16,$16,1
bne	$16,$23,$L467
addu	$17,$17,$3

lw	$20,%got(get_cabac_bypass)($28)
li	$17,3			# 0x3
li	$21,1			# 0x1
li	$22,25			# 0x19
b	$L469
addiu	$20,$20,%lo(get_cabac_bypass)

$L472:
addiu	$17,$17,1
beq	$17,$22,$L489
addu	$16,$16,$3

$L469:
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$19

sll	$3,$21,$17
bne	$2,$0,$L472
lw	$28,16($sp)

li	$21,-1			# 0xffffffffffffffff
li	$22,1			# 0x1
$L473:
addiu	$17,$17,-1
beq	$17,$21,$L471
move	$4,$19

$L475:
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
nop

sll	$3,$22,$17
beq	$2,$0,$L473
lw	$28,16($sp)

addiu	$17,$17,-1
addu	$16,$16,$3
bne	$17,$21,$L475
move	$4,$19

$L471:
li	$5,131072			# 0x20000
subu	$16,$0,$16
addu	$17,$18,$5
lw	$2,8224($17)
sll	$2,$2,1
andi	$3,$2,0xffff
beq	$3,$0,$L490
sw	$2,8224($17)

$L476:
li	$3,131072			# 0x20000
addu	$18,$18,$3
lw	$4,8228($18)
sll	$4,$4,17
subu	$3,$2,$4
sra	$2,$3,31
and	$4,$2,$4
addu	$3,$3,$4
xor	$16,$2,$16
subu	$2,$16,$2
sw	$3,8224($18)
$L466:
lw	$31,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L478:
b	$L465
move	$5,$0

$L489:
lw	$6,%got($LC0)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)

li	$2,-2147483648			# 0xffffffff80000000
lw	$31,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L490:
lw	$25,%got(refill.isra.0)($28)
addiu	$5,$5,8240
move	$4,$19
addiu	$25,$25,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
addu	$5,$18,$5

b	$L476
lw	$2,8224($17)

.set	macro
.set	reorder
.end	decode_cabac_mb_mvd_bak
.size	decode_cabac_mb_mvd_bak, .-decode_cabac_mb_mvd_bak
.section	.text.ff_h264_init_cabac_states,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_init_cabac_states
.set	nomips16
.set	nomicromips
.ent	ff_h264_init_cabac_states
.type	ff_h264_init_cabac_states, @function
ff_h264_init_cabac_states:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,65536			# 0x10000
addu	$2,$4,$2
lw	$3,-5260($2)
li	$2,1			# 0x1
beq	$3,$2,$L495
li	$2,196608			# 0x30000

lw	$3,%got(cabac_context_init_PB)($28)
li	$5,920			# 0x398
addu	$2,$4,$2
addiu	$3,$3,%lo(cabac_context_init_PB)
lw	$2,-15280($2)
mul	$6,$2,$5
addu	$5,$6,$3
$L492:
li	$2,131072			# 0x20000
lw	$8,2872($4)
addiu	$7,$5,920
addiu	$2,$2,8272
addu	$4,$4,$2
$L494:
lb	$2,0($5)
addiu	$5,$5,2
lb	$3,-1($5)
mul	$2,$2,$8
sra	$2,$2,4
addu	$2,$2,$3
sll	$2,$2,1
addiu	$2,$2,-127
sra	$3,$2,31
xor	$2,$3,$2
slt	$3,$2,125
bne	$3,$0,$L493
andi	$6,$2,0x1

addiu	$2,$6,124
$L493:
addiu	$4,$4,1
bne	$5,$7,$L494
sb	$2,-1($4)

j	$31
nop

$L495:
lw	$5,%got(cabac_context_init_I)($28)
b	$L492
addiu	$5,$5,%lo(cabac_context_init_I)

.set	macro
.set	reorder
.end	ff_h264_init_cabac_states
.size	ff_h264_init_cabac_states, .-ff_h264_init_cabac_states
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"Reference %d >= %d\012\000"
.align	2
$LC2:
.ascii	"cabac decode of qscale diff failed at %d %d\012\000"
.section	.text.ff_h264_decode_mb_cabac,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_mb_cabac
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_mb_cabac
.type	ff_h264_decode_mb_cabac, @function
ff_h264_decode_mb_cabac:
.frame	$sp,216,$31		# vars= 136, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$6,7996($4)
li	$2,65536			# 0x10000
lw	$7,168($4)
addiu	$sp,$sp,-216
lw	$8,12888($4)
addu	$2,$4,$2
lw	$5,7992($4)
li	$3,131072			# 0x20000
mul	$9,$6,$7
sw	$fp,208($sp)
addu	$3,$4,$3
.cprestore	32
sw	$8,96($sp)
move	$fp,$4
sw	$31,212($sp)
sw	$23,204($sp)
sw	$22,200($sp)
sw	$21,196($sp)
addu	$8,$9,$5
sw	$20,192($sp)
sw	$19,188($sp)
sw	$18,184($sp)
sw	$17,180($sp)
sw	$16,176($sp)
sw	$8,68($sp)
lw	$4,-5260($2)
lw	$2,-5252($2)
sw	$8,9448($3)
li	$3,1			# 0x1
beq	$4,$3,$L503
nop

beq	$2,$0,$L499
andi	$2,$6,0x1

beq	$2,$0,$L1056
lw	$25,%got(decode_cabac_mb_skip)($28)

lw	$2,10752($fp)
beq	$2,$0,$L1056
nop

lw	$2,10756($fp)
beq	$2,$0,$L501
nop

$L505:
li	$2,131072			# 0x20000
$L1057:
lw	$4,11232($fp)
lw	$25,%call16(memset)($28)
move	$5,$0
addu	$2,$fp,$2
sw	$0,60($sp)
li	$6,32			# 0x20
lw	$16,9448($2)
sll	$2,$16,5
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2

move	$5,$0
lw	$28,32($sp)
addiu	$4,$fp,11192
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,40			# 0x28

li	$2,65536			# 0x10000
lw	$28,32($sp)
addu	$2,$fp,$2
lw	$2,-5248($2)
beq	$2,$0,$L508
lw	$5,60($sp)

ori	$5,$5,0x80
$L508:
li	$2,65536			# 0x10000
li	$3,3			# 0x3
addu	$2,$fp,$2
lw	$4,-5260($2)
beq	$4,$3,$L984
lw	$25,%got(fill_decode_neighbors)($28)

ori	$2,$5,0x3808
move	$5,$2
sw	$2,60($sp)
addiu	$25,$25,%lo(fill_decode_neighbors)
.reloc	1f,R_MIPS_JALR,fill_decode_neighbors
1:	jalr	$25
move	$4,$fp

move	$4,$fp
lw	$28,32($sp)
lw	$25,%got(fill_decode_caches)($28)
addiu	$25,$25,%lo(fill_decode_caches)
.reloc	1f,R_MIPS_JALR,fill_decode_caches
1:	jalr	$25
lw	$5,60($sp)

li	$4,-2			# 0xfffffffffffffffe
lb	$2,11572($fp)
lw	$28,32($sp)
beq	$2,$4,$L776
lb	$3,11579($fp)

beq	$3,$4,$L776
nop

lw	$4,11264($fp)
or	$2,$2,$4
beq	$2,$0,$L512
move	$2,$0

lw	$2,11292($fp)
or	$3,$3,$2
bne	$3,$0,$L985
addiu	$2,$sp,56

$L776:
move	$2,$0
$L512:
lw	$9,60($sp)
sw	$0,11580($fp)
sw	$0,11588($fp)
sw	$0,11596($fp)
sw	$0,11604($fp)
sw	$2,11296($fp)
sw	$2,11300($fp)
sw	$2,11304($fp)
sw	$2,11308($fp)
sw	$2,11328($fp)
sw	$2,11332($fp)
sw	$2,11336($fp)
sw	$2,11340($fp)
sw	$2,11360($fp)
sw	$2,11364($fp)
sw	$2,11368($fp)
sw	$2,11372($fp)
sw	$2,11392($fp)
sw	$2,11396($fp)
sw	$2,11400($fp)
b	$L511
sw	$2,11404($fp)

$L499:
lw	$25,%got(decode_cabac_mb_skip)($28)
$L1056:
addiu	$17,$25,%lo(decode_cabac_mb_skip)
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_skip
1:	jalr	$25
move	$4,$fp

beq	$2,$0,$L986
lw	$28,32($sp)

li	$16,65536			# 0x10000
addu	$16,$fp,$16
lw	$2,-5252($16)
beq	$2,$0,$L1057
li	$2,131072			# 0x20000

lw	$2,7996($fp)
andi	$2,$2,0x1
bne	$2,$0,$L505
lw	$8,68($sp)

move	$4,$fp
lw	$3,2192($fp)
move	$25,$17
sll	$2,$8,2
addu	$2,$3,$2
li	$3,2048			# 0x800
sw	$3,0($2)
lw	$6,7996($fp)
lw	$5,7992($fp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_skip
1:	jalr	$25
addiu	$6,$6,1

lw	$28,32($sp)
bne	$2,$0,$L505
sw	$2,10756($fp)

lw	$25,%got(decode_cabac_field_decoding_flag)($28)
addiu	$25,$25,%lo(decode_cabac_field_decoding_flag)
.reloc	1f,R_MIPS_JALR,decode_cabac_field_decoding_flag
1:	jalr	$25
move	$4,$fp

lw	$28,32($sp)
sw	$2,-5248($16)
b	$L505
sw	$2,-5244($16)

$L986:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$2,-5252($2)
$L503:
bne	$2,$0,$L501
nop

$L525:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$2,-5248($2)
$L524:
lw	$25,%got(fill_decode_neighbors)($28)
li	$16,65536			# 0x10000
subu	$5,$0,$2
sw	$0,10752($fp)
addu	$16,$fp,$16
addiu	$25,$25,%lo(fill_decode_neighbors)
.reloc	1f,R_MIPS_JALR,fill_decode_neighbors
1:	jalr	$25
move	$4,$fp

li	$2,3			# 0x3
lw	$18,-5260($16)
beq	$18,$2,$L987
lw	$28,32($sp)

li	$2,2			# 0x2
beq	$18,$2,$L988
lw	$25,%got(decode_cabac_intra_mb_type)($28)

li	$5,3			# 0x3
li	$6,1			# 0x1
addiu	$25,$25,%lo(decode_cabac_intra_mb_type)
.reloc	1f,R_MIPS_JALR,decode_cabac_intra_mb_type
1:	jalr	$25
move	$4,$fp

li	$3,5			# 0x5
lw	$4,-5264($16)
beq	$4,$3,$L989
lw	$28,32($sp)

$L532:
lw	$3,%got(i_mb_type_info)($28)
sll	$2,$2,2
move	$16,$0
addiu	$3,$3,%lo(i_mb_type_info)
addu	$2,$2,$3
lbu	$3,2($2)
lhu	$5,0($2)
lbu	$2,3($2)
sw	$3,10764($fp)
sw	$5,56($sp)
sw	$2,72($sp)
$L533:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$2,-5248($2)
beq	$2,$0,$L1058
lw	$3,68($sp)

ori	$5,$5,0x80
sw	$5,56($sp)
lw	$3,68($sp)
$L1058:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
sll	$3,$3,1
lw	$4,-5268($2)
lw	$6,-5272($2)
sw	$3,76($sp)
andi	$3,$5,0x4
lw	$8,76($sp)
addu	$4,$4,$8
beq	$3,$0,$L539
sh	$6,0($4)

li	$17,131072			# 0x20000
lw	$25,%call16(memcpy)($28)
li	$6,256			# 0x100
addu	$3,$fp,$17
addiu	$4,$17,6944
lw	$2,8224($3)
addu	$4,$fp,$4
lw	$16,8240($3)
andi	$3,$2,0x1
subu	$16,$16,$3
andi	$2,$2,0x1ff
addiu	$3,$16,-1
movn	$16,$3,$2
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$16

addiu	$5,$16,256
lw	$2,11896($fp)
bne	$2,$0,$L990
lw	$28,32($sp)

$L542:
li	$4,131072			# 0x20000
lw	$25,%call16(ff_init_cabac_decoder)($28)
addu	$16,$fp,$4
addiu	$4,$4,8224
lw	$6,8244($16)
addu	$4,$fp,$4
.reloc	1f,R_MIPS_JALR,ff_init_cabac_decoder
1:	jalr	$25
subu	$6,$6,$5

li	$7,495			# 0x1ef
lw	$3,8732($16)
li	$5,16			# 0x10
lw	$2,8748($16)
li	$6,32			# 0x20
lw	$9,68($sp)
lw	$11,76($sp)
lw	$28,32($sp)
addu	$2,$2,$9
addu	$3,$3,$11
sll	$4,$9,5
sh	$7,0($3)
sb	$0,0($2)
lw	$2,2172($fp)
lw	$25,%call16(memset)($28)
addu	$2,$2,$9
sb	$0,0($2)
lw	$2,11232($fp)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$2,$4

move	$2,$0
lw	$12,68($sp)
lw	$3,2192($fp)
sll	$4,$12,2
addu	$3,$3,$4
lw	$4,56($sp)
sw	$4,0($3)
sw	$0,8752($16)
$L843:
lw	$31,212($sp)
lw	$fp,208($sp)
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
j	$31
addiu	$sp,$sp,216

$L501:
lw	$2,7996($fp)
andi	$2,$2,0x1
bne	$2,$0,$L525
lw	$25,%got(decode_cabac_field_decoding_flag)($28)

addiu	$25,$25,%lo(decode_cabac_field_decoding_flag)
.reloc	1f,R_MIPS_JALR,decode_cabac_field_decoding_flag
1:	jalr	$25
move	$4,$fp

li	$3,65536			# 0x10000
lw	$28,32($sp)
addu	$3,$fp,$3
sw	$2,-5248($3)
b	$L524
sw	$2,-5244($3)

$L539:
lw	$3,-5244($2)
beq	$3,$0,$L1059
lw	$25,%got(fill_decode_caches)($28)

lw	$4,6624($2)
lw	$3,6628($2)
sll	$4,$4,1
sll	$3,$3,1
sw	$4,6624($2)
sw	$3,6628($2)
$L1059:
addiu	$25,$25,%lo(fill_decode_caches)
.reloc	1f,R_MIPS_JALR,fill_decode_caches
1:	jalr	$25
move	$4,$fp

lw	$3,56($sp)
andi	$2,$3,0x7
beq	$2,$0,$L544
lw	$28,32($sp)

andi	$3,$3,0x1
beq	$3,$0,$L545
lw	$13,96($sp)

li	$18,2			# 0x2
bne	$13,$0,$L546
lw	$16,%got(decode_cabac_mb_intra4x4_pred_mode)($28)

$L548:
lw	$20,%got(scan8)($28)
addiu	$16,$16,%lo(decode_cabac_mb_intra4x4_pred_mode)
lw	$17,%got(scan8+16)($28)
addiu	$20,$20,%lo(scan8)
addiu	$17,$17,%lo(scan8+16)
$L547:
lbu	$19,0($20)
move	$4,$fp
move	$25,$16
addiu	$20,$20,1
addu	$19,$fp,$19
lb	$5,10808($19)
lb	$2,10815($19)
slt	$3,$2,$5
movz	$2,$5,$3
slt	$5,$2,0
movn	$2,$18,$5
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_intra4x4_pred_mode
1:	jalr	$25
move	$5,$2

lw	$28,32($sp)
bne	$20,$17,$L547
sb	$2,10816($19)

lw	$25,%call16(ff_h264_write_back_intra_pred_mode)($28)
$L1054:
.reloc	1f,R_MIPS_JALR,ff_h264_write_back_intra_pred_mode
1:	jalr	$25
move	$4,$fp

lw	$28,32($sp)
lw	$25,%call16(ff_h264_check_intra4x4_pred_mode)($28)
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra4x4_pred_mode
1:	jalr	$25
move	$4,$fp

bltz	$2,$L949
lw	$28,32($sp)

$L552:
lw	$2,11896($fp)
bne	$2,$0,$L991
li	$2,131072			# 0x20000

$L585:
lw	$3,56($sp)
$L557:
andi	$2,$3,0x78
$L1073:
bne	$2,$0,$L992
lw	$8,68($sp)

$L705:
andi	$22,$3,0x2
beq	$22,$0,$L993
li	$2,131072			# 0x20000

$L716:
li	$4,131072			# 0x20000
lw	$9,72($sp)
lw	$11,76($sp)
addu	$2,$fp,$4
lw	$12,96($sp)
sw	$9,8736($2)
lw	$2,8732($2)
addu	$2,$2,$11
beq	$12,$0,$L726
sh	$9,0($2)

andi	$2,$9,0xf
beq	$2,$0,$L1060
lw	$8,68($sp)

andi	$2,$3,0x7
beq	$2,$0,$L994
addiu	$19,$4,8224

lw	$17,%got(get_cabac_noinline)($28)
addiu	$4,$4,8224
addu	$19,$fp,$4
addiu	$17,$17,%lo(get_cabac_noinline)
$L728:
lw	$9,68($sp)
lw	$2,2192($fp)
sll	$4,$9,2
addu	$2,$2,$4
sw	$3,0($2)
lw	$2,56($sp)
$L730:
andi	$2,$2,0x80
beq	$2,$0,$L732
nop

lw	$2,2872($fp)
beq	$2,$0,$L733
li	$2,131072			# 0x20000

lw	$21,%got(luma_dc_field_scan)($28)
addiu	$3,$2,9292
addiu	$2,$2,9276
addu	$3,$fp,$3
addu	$16,$fp,$2
addiu	$21,$21,%lo(luma_dc_field_scan)
sw	$3,76($sp)
$L734:
li	$18,131072			# 0x20000
li	$3,60			# 0x3c
addu	$20,$fp,$18
li	$5,61			# 0x3d
addiu	$18,$18,8272
lw	$2,8752($20)
move	$4,$19
move	$25,$17
movz	$5,$3,$2
addu	$5,$5,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

beq	$2,$0,$L995
lw	$28,32($sp)

li	$5,2			# 0x2
li	$22,1			# 0x1
b	$L737
li	$20,103			# 0x67

$L739:
addiu	$22,$22,1
beq	$22,$20,$L996
lw	$6,%got($LC2)($28)

$L737:
addiu	$5,$5,60
move	$4,$19
addu	$5,$5,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

li	$5,3			# 0x3
bne	$2,$0,$L739
lw	$28,32($sp)

andi	$2,$22,0x1
bne	$2,$0,$L997
addiu	$2,$22,1

sra	$2,$2,1
subu	$2,$0,$2
$L741:
lw	$3,2872($fp)
li	$4,131072			# 0x20000
addu	$4,$fp,$4
addu	$3,$2,$3
sw	$2,8752($4)
sltu	$2,$3,52
bne	$2,$0,$L948
nop

bltz	$3,$L998
nop

addiu	$3,$3,-52
$L948:
sw	$3,2872($fp)
$L743:
andi	$2,$3,0xff
addu	$2,$fp,$2
lbu	$3,13116($2)
lbu	$2,13372($2)
sw	$3,10740($fp)
b	$L745
sw	$2,10744($fp)

$L987:
lw	$2,10800($fp)
li	$18,131072			# 0x20000
lw	$3,10792($fp)
lw	$17,%got(get_cabac_noinline)($28)
addiu	$16,$18,8224
addiu	$2,$2,-1
addiu	$3,$3,-1
andi	$2,$2,0x100
sltu	$2,$2,1
addiu	$4,$2,1
andi	$3,$3,0x100
movz	$2,$4,$3
addu	$16,$fp,$16
addiu	$17,$17,%lo(get_cabac_noinline)
move	$4,$16
move	$25,$17
addu	$2,$2,$18
addiu	$5,$2,8299
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

bne	$2,$0,$L999
lw	$28,32($sp)

$L528:
lw	$3,%got(b_mb_type_info)($28)
sll	$2,$2,2
sw	$0,72($sp)
addiu	$3,$3,%lo(b_mb_type_info)
addu	$2,$2,$3
lhu	$5,0($2)
lbu	$16,2($2)
b	$L533
sw	$5,56($sp)

$L984:
lw	$2,5140($2)
ori	$5,$5,0x5908
beq	$2,$0,$L510
sw	$5,60($sp)

lw	$25,%got(fill_decode_neighbors)($28)
addiu	$25,$25,%lo(fill_decode_neighbors)
.reloc	1f,R_MIPS_JALR,fill_decode_neighbors
1:	jalr	$25
move	$4,$fp

move	$4,$fp
lw	$28,32($sp)
lw	$25,%got(fill_decode_caches)($28)
addiu	$25,$25,%lo(fill_decode_caches)
.reloc	1f,R_MIPS_JALR,fill_decode_caches
1:	jalr	$25
lw	$5,60($sp)

lw	$28,32($sp)
$L510:
lw	$25,%call16(pred_direct_motion_hw)($28)
addiu	$5,$sp,60
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$fp

lw	$9,60($sp)
ori	$9,$9,0x800
sw	$9,60($sp)
$L511:
lw	$2,7996($fp)
li	$5,131072			# 0x20000
lw	$10,11864($fp)
andi	$3,$9,0x3000
addu	$5,$fp,$5
lw	$4,7992($fp)
sll	$2,$2,2
lw	$14,9448($5)
mul	$5,$2,$10
sll	$4,$4,2
sll	$14,$14,2
bne	$3,$0,$L513
addu	$10,$5,$4

lw	$2,2276($fp)
li	$3,-1			# 0xffffffffffffffff
addu	$2,$2,$14
sh	$3,0($2)
sh	$3,2($2)
$L513:
li	$8,65536			# 0x10000
addu	$8,$fp,$8
lw	$2,6632($8)
beq	$2,$0,$L522
addiu	$4,$fp,11580

li	$12,131072			# 0x20000
li	$13,-131072			# 0xfffffffffffe0000
addiu	$2,$12,8912
addiu	$18,$13,2384
addiu	$17,$12,6480
sll	$10,$10,2
addiu	$5,$fp,2276
addu	$2,$fp,$2
move	$3,$0
li	$11,12288			# 0x3000
addiu	$13,$13,2512
addu	$12,$fp,$12
andi	$15,$9,0x800
$L521:
sll	$6,$3,1
sll	$6,$11,$6
and	$6,$6,$9
beq	$6,$0,$L517
addu	$6,$2,$18

lw	$19,11864($fp)
lw	$7,-92($5)
addu	$22,$2,$13
sll	$19,$19,2
addu	$7,$7,$10
$L518:
lw	$20,0($6)
addiu	$6,$6,32
lw	$21,-28($6)
sw	$20,0($7)
sw	$21,4($7)
lw	$20,-24($6)
lw	$21,-20($6)
sw	$20,8($7)
sw	$21,12($7)
bne	$22,$6,$L518
addu	$7,$7,$19

lw	$7,9448($12)
addu	$6,$5,$17
lw	$19,11856($fp)
sll	$7,$7,2
lw	$6,0($6)
addu	$7,$19,$7
lw	$19,0($7)
sll	$19,$19,2
beq	$15,$0,$L519
addu	$19,$6,$19

move	$6,$0
move	$7,$0
sw	$6,0($19)
sw	$7,4($19)
sw	$6,8($19)
sw	$7,12($19)
sw	$6,16($19)
sw	$7,20($19)
sw	$6,24($19)
sw	$7,28($19)
$L520:
lw	$6,0($5)
lb	$7,0($4)
addu	$6,$6,$14
sb	$7,0($6)
lb	$7,2($4)
sb	$7,1($6)
lb	$7,16($4)
sb	$7,2($6)
lb	$7,18($4)
sb	$7,3($6)
$L517:
lw	$6,6632($8)
addiu	$3,$3,1
addiu	$5,$5,4
addiu	$4,$4,40
sltu	$6,$3,$6
bne	$6,$0,$L521
addiu	$2,$2,160

$L522:
li	$2,65536			# 0x10000
li	$3,3			# 0x3
addu	$2,$fp,$2
lw	$4,-5260($2)
beq	$4,$3,$L1000
andi	$9,$9,0x40

$L516:
lw	$3,2192($fp)
sll	$4,$16,2
lw	$5,60($sp)
lw	$2,2172($fp)
addu	$4,$3,$4
lw	$8,68($sp)
li	$3,131072			# 0x20000
sw	$5,0($4)
addu	$2,$2,$16
lw	$5,2872($fp)
addu	$3,$fp,$3
li	$4,65536			# 0x10000
sll	$9,$8,1
addu	$4,$fp,$4
sb	$5,0($2)
sll	$16,$16,1
lw	$6,8732($3)
move	$2,$0
lw	$5,8748($3)
lw	$7,-5268($4)
addu	$6,$6,$9
lw	$9,68($sp)
lw	$8,-5272($4)
addu	$16,$7,$16
addu	$4,$5,$9
li	$5,1			# 0x1
sh	$8,0($16)
sw	$5,10752($fp)
sh	$0,0($6)
sb	$0,0($4)
sw	$0,8752($3)
lw	$31,212($sp)
lw	$fp,208($sp)
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
j	$31
addiu	$sp,$sp,216

$L544:
li	$2,4			# 0x4
beq	$16,$2,$L1001
andi	$2,$3,0x100

bne	$2,$0,$L1002
andi	$2,$3,0x8

bne	$2,$0,$L1003
li	$2,65536			# 0x10000

andi	$2,$3,0x10
bne	$2,$0,$L640
li	$2,65536			# 0x10000

addu	$8,$fp,$2
lw	$4,6632($8)
beq	$4,$0,$L557
addiu	$2,$2,6624

lw	$9,%got(decode_cabac_mb_ref)($28)
move	$18,$0
sw	$fp,64($sp)
addu	$20,$fp,$2
li	$11,4096			# 0x1000
li	$19,-1			# 0xffffffffffffffff
b	$L677
move	$21,$fp

$L1005:
lw	$7,0($20)
addiu	$25,$9,%lo(decode_cabac_mb_ref)
move	$4,$fp
sltu	$7,$7,2
move	$5,$18
bne	$7,$0,$L673
move	$2,$0

sw	$8,156($sp)
sw	$9,164($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref
1:	jalr	$25
sw	$11,152($sp)

lw	$10,0($20)
sll	$3,$2,8
lw	$28,32($sp)
addu	$3,$2,$3
lw	$8,156($sp)
sltu	$4,$2,$10
lw	$9,164($sp)
beq	$4,$0,$L674
lw	$11,152($sp)

andi	$2,$3,0xffff
lw	$3,56($sp)
$L673:
sh	$2,11580($16)
addiu	$16,$16,2
sh	$2,11586($16)
sh	$2,11594($16)
bne	$17,$23,$L798
sh	$2,11602($16)

$L1006:
lw	$2,6632($8)
addiu	$18,$18,1
addiu	$20,$20,4
sltu	$4,$18,$2
beq	$4,$0,$L1004
addiu	$21,$21,40

$L677:
sll	$22,$18,1
move	$17,$0
li	$23,1			# 0x1
move	$16,$21
$L676:
addu	$2,$22,$17
sll	$2,$11,$2
and	$2,$2,$3
bne	$2,$0,$L1005
sll	$6,$17,2

sh	$19,11580($16)
addiu	$16,$16,2
sh	$19,11586($16)
sh	$19,11594($16)
beq	$17,$23,$L1006
sh	$19,11602($16)

$L798:
b	$L676
li	$17,1			# 0x1

$L990:
addiu	$4,$17,7200
lw	$25,%call16(memcpy)($28)
li	$6,128			# 0x80
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$4,$fp,$4

addiu	$5,$16,384
b	$L542
lw	$28,32($sp)

$L519:
lw	$6,0($2)
lw	$7,4($2)
sw	$6,0($19)
sw	$7,4($19)
lw	$6,8($2)
lw	$7,12($2)
sw	$6,8($19)
sw	$7,12($19)
lw	$6,-84($2)
sw	$6,24($19)
lw	$6,-52($2)
sw	$6,20($19)
lw	$6,-20($2)
b	$L520
sw	$6,16($19)

$L726:
lw	$8,68($sp)
$L1060:
lw	$2,2192($fp)
lw	$9,72($sp)
sll	$4,$8,2
addu	$2,$2,$4
bne	$9,$0,$L1007
sw	$3,0($2)

lw	$2,56($sp)
andi	$3,$2,0x2
beq	$3,$0,$L731
li	$19,131072			# 0x20000

lw	$17,%got(get_cabac_noinline)($28)
addiu	$19,$19,8224
addiu	$17,$17,%lo(get_cabac_noinline)
b	$L730
addu	$19,$fp,$19

$L1000:
beq	$9,$0,$L516
li	$3,131072			# 0x20000

lhu	$4,-5238($2)
addu	$3,$fp,$3
srl	$4,$4,1
lw	$5,9448($3)
lw	$3,9088($3)
sll	$5,$5,2
addu	$3,$3,$5
sb	$4,1($3)
lhu	$4,-5236($2)
srl	$4,$4,1
sb	$4,2($3)
lhu	$2,-5234($2)
srl	$2,$2,1
b	$L516
sb	$2,3($3)

$L988:
li	$16,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
addiu	$19,$16,8224
addiu	$5,$16,8286
addu	$19,$fp,$19
addiu	$17,$17,%lo(get_cabac_noinline)
addu	$5,$fp,$5
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$19

bne	$2,$0,$L535
lw	$28,32($sp)

addiu	$5,$16,8287
move	$4,$19
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

beq	$2,$0,$L1008
move	$4,$19

addiu	$5,$16,8289
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

lw	$28,32($sp)
subu	$2,$18,$2
$L537:
lw	$3,%got(p_mb_type_info)($28)
sll	$2,$2,2
sw	$0,72($sp)
addiu	$3,$3,%lo(p_mb_type_info)
addu	$2,$2,$3
lhu	$5,0($2)
lbu	$16,2($2)
b	$L533
sw	$5,56($sp)

$L732:
lw	$2,2872($fp)
beq	$2,$0,$L735
li	$2,131072			# 0x20000

lw	$21,%got(luma_dc_zigzag_scan)($28)
addiu	$3,$2,9148
addiu	$2,$2,9132
addu	$3,$fp,$3
addu	$16,$fp,$2
addiu	$21,$21,%lo(luma_dc_zigzag_scan)
b	$L734
sw	$3,76($sp)

$L995:
sw	$0,8752($20)
$L745:
lw	$2,56($sp)
andi	$2,$2,0x2
bne	$2,$0,$L1009
li	$18,131072			# 0x20000

move	$7,$0
addiu	$18,$18,6944
move	$23,$0
addu	$18,$fp,$18
move	$20,$fp
$L755:
lw	$3,72($sp)
sra	$2,$3,$23
andi	$2,$2,0x1
beq	$2,$0,$L750
lw	$3,%got(scan8)($28)

lw	$4,56($sp)
li	$5,16777216			# 0x1000000
and	$2,$4,$5
bne	$2,$0,$L1010
li	$31,3			# 0x3

lw	$fp,2872($20)
andi	$2,$4,0x7
lw	$25,%got(decode_cabac_residual_nondc)($28)
movn	$31,$0,$2
addiu	$21,$7,4
sll	$4,$fp,6
addiu	$25,$25,%lo(decode_cabac_residual_nondc)
li	$22,16			# 0x10
addiu	$2,$31,15056
move	$17,$7
sw	$25,64($sp)
sll	$2,$2,2
move	$19,$18
addu	$2,$20,$2
lw	$fp,0($2)
addu	$fp,$fp,$4
$L754:
lw	$25,64($sp)
move	$7,$17
li	$6,2			# 0x2
sw	$16,16($sp)
addiu	$17,$17,1
sw	$fp,20($sp)
move	$5,$19
sw	$22,24($sp)
jalr	$25
move	$4,$20

addiu	$19,$19,32
bne	$17,$21,$L754
lw	$28,32($sp)

$L752:
addiu	$23,$23,1
li	$2,4			# 0x4
move	$7,$21
bne	$23,$2,$L755
addiu	$18,$18,128

move	$fp,$20
$L749:
lw	$8,72($sp)
$L1053:
andi	$2,$8,0x30
beq	$2,$0,$L1061
lw	$3,72($sp)

lw	$19,%got(chroma_dc_scan)($28)
li	$18,131072			# 0x20000
lw	$17,%got(decode_cabac_residual_dc)($28)
li	$20,4			# 0x4
addiu	$5,$18,7456
addiu	$19,$19,%lo(chroma_dc_scan)
addiu	$17,$17,%lo(decode_cabac_residual_dc)
sw	$20,20($sp)
addu	$5,$fp,$5
li	$6,3			# 0x3
sw	$19,16($sp)
move	$7,$0
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_dc
1:	jalr	$25
move	$4,$fp

addiu	$5,$18,7584
li	$6,3			# 0x3
sw	$19,16($sp)
addu	$5,$fp,$5
sw	$20,20($sp)
li	$7,1			# 0x1
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_dc
1:	jalr	$25
move	$4,$fp

lw	$28,32($sp)
lw	$3,72($sp)
$L1061:
andi	$2,$3,0x20
beq	$2,$0,$L758
lw	$18,%got(decode_cabac_residual_nondc)($28)

li	$19,131072			# 0x20000
addiu	$16,$16,1
addiu	$19,$19,7456
addiu	$18,$18,%lo(decode_cabac_residual_nondc)
li	$23,16			# 0x10
addu	$19,$fp,$19
move	$2,$0
sw	$18,64($sp)
li	$21,15			# 0xf
$L761:
addiu	$20,$2,1
lw	$2,56($sp)
li	$5,3			# 0x3
addu	$4,$fp,$23
andi	$2,$2,0x7
movn	$5,$0,$2
addiu	$17,$23,4
move	$18,$19
lw	$22,10724($4)
addu	$2,$5,$20
sll	$4,$22,6
addiu	$2,$2,15056
sll	$2,$2,2
addu	$2,$fp,$2
lw	$22,0($2)
addu	$22,$22,$4
$L760:
lw	$25,64($sp)
move	$7,$23
li	$6,4			# 0x4
sw	$16,16($sp)
addiu	$23,$23,1
sw	$22,20($sp)
move	$5,$18
sw	$21,24($sp)
jalr	$25
move	$4,$fp

bne	$23,$17,$L760
addiu	$18,$18,32

li	$3,2			# 0x2
addiu	$19,$19,128
bne	$20,$3,$L761
li	$2,1			# 0x1

$L762:
lw	$3,2172($fp)
li	$2,131072			# 0x20000
lw	$8,68($sp)
li	$6,65536			# 0x10000
lw	$4,2872($fp)
addu	$2,$fp,$2
addu	$6,$fp,$6
addu	$3,$3,$8
sb	$4,0($3)
lw	$2,9448($2)
lw	$3,11232($fp)
lw	$4,11192($fp)
lw	$5,11196($fp)
sll	$2,$2,5
addu	$3,$3,$2
sw	$4,0($3)
sw	$5,4($3)
lw	$3,11232($fp)
lw	$4,11200($fp)
lw	$5,11204($fp)
addu	$3,$3,$2
sw	$4,8($3)
sw	$5,12($3)
lw	$3,11232($fp)
lw	$4,11224($fp)
addu	$3,$3,$2
sw	$4,16($3)
lw	$3,11232($fp)
lw	$4,11212($fp)
addu	$3,$3,$2
sw	$4,20($3)
lw	$3,11232($fp)
lw	$4,11216($fp)
lw	$5,11220($fp)
addu	$2,$3,$2
sw	$4,24($2)
sw	$5,28($2)
lw	$2,-5244($6)
beq	$2,$0,$L843
lw	$31,212($sp)

lw	$4,6624($6)
move	$2,$0
lw	$3,6628($6)
srl	$4,$4,1
lw	$fp,208($sp)
srl	$3,$3,1
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
sw	$4,6624($6)
sw	$3,6628($6)
j	$31
addiu	$sp,$sp,216

$L750:
addiu	$21,$7,4
addiu	$3,$3,%lo(scan8)
addu	$2,$3,$7
lbu	$4,0($2)
addiu	$2,$4,11184
addu	$4,$20,$4
addu	$2,$20,$2
sb	$0,9($2)
sb	$0,8($2)
sb	$0,1($2)
b	$L752
sb	$0,11184($4)

$L758:
sb	$0,11226($fp)
sb	$0,11225($fp)
sb	$0,11218($fp)
sb	$0,11217($fp)
sb	$0,11202($fp)
sb	$0,11201($fp)
sb	$0,11194($fp)
b	$L762
sb	$0,11193($fp)

$L989:
addiu	$3,$2,-1
b	$L532
movn	$2,$3,$2

$L993:
lw	$17,%got(get_cabac_noinline)($28)
li	$19,2			# 0x2
addu	$21,$fp,$2
move	$25,$0
addiu	$18,$2,8272
lw	$20,8740($21)
addiu	$2,$2,8224
lw	$23,8744($21)
addiu	$17,$17,%lo(get_cabac_noinline)
addu	$16,$fp,$2
andi	$5,$20,0x4
movz	$25,$19,$5
andi	$2,$23,0x2
sltu	$2,$2,1
move	$4,$16
addu	$5,$2,$25
move	$25,$17
addiu	$5,$5,73
addu	$5,$5,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

move	$31,$0
andi	$5,$20,0x8
movz	$31,$19,$5
move	$20,$2
andi	$2,$2,0x1
xori	$2,$2,0x1
move	$4,$16
addu	$5,$2,$31
move	$25,$17
addiu	$5,$5,73
addu	$5,$5,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

move	$31,$0
sll	$2,$2,1
andi	$3,$23,0x8
addu	$20,$20,$2
sltu	$3,$3,1
andi	$5,$20,0x1
movz	$31,$19,$5
move	$4,$16
move	$25,$17
addu	$5,$3,$31
addiu	$5,$5,73
addu	$5,$5,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

move	$4,$16
sll	$2,$2,2
move	$25,$17
addu	$20,$20,$2
andi	$2,$20,0x2
movz	$22,$19,$2
andi	$2,$20,0x4
sltu	$2,$2,1
addu	$5,$2,$22
addiu	$5,$5,73
addu	$5,$5,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

sll	$2,$2,3
lw	$3,11896($fp)
lw	$28,32($sp)
addu	$2,$20,$2
bne	$3,$0,$L721
sw	$2,72($sp)

b	$L716
lw	$3,56($sp)

$L992:
li	$3,131072			# 0x20000
addu	$3,$fp,$3
lw	$2,8748($3)
addu	$2,$2,$8
sb	$0,0($2)
lw	$2,7996($fp)
lw	$14,11864($fp)
lw	$4,7992($fp)
sll	$2,$2,2
lw	$13,56($sp)
lw	$18,9448($3)
mul	$5,$2,$14
sll	$4,$4,2
andi	$3,$13,0x3000
sll	$18,$18,2
beq	$3,$0,$L1011
addu	$2,$5,$4

$L706:
li	$12,65536			# 0x10000
addu	$12,$fp,$12
lw	$3,6632($12)
beq	$3,$0,$L715
nop

li	$16,131072			# 0x20000
li	$17,-131072			# 0xfffffffffffe0000
addiu	$6,$16,8912
addiu	$21,$17,2384
addiu	$20,$16,6480
sll	$14,$2,2
addiu	$9,$fp,2276
addiu	$8,$fp,11580
addu	$6,$fp,$6
move	$7,$0
li	$15,12288			# 0x3000
addiu	$17,$17,2512
addu	$16,$fp,$16
andi	$19,$13,0x800
$L714:
sll	$2,$7,1
sll	$2,$15,$2
and	$2,$2,$13
beq	$2,$0,$L710
addu	$2,$6,$21

lw	$10,11864($fp)
lw	$3,-92($9)
addu	$11,$6,$17
sll	$10,$10,2
addu	$3,$3,$14
$L711:
lw	$4,0($2)
addiu	$2,$2,32
lw	$5,-28($2)
sw	$4,0($3)
sw	$5,4($3)
lw	$4,-24($2)
lw	$5,-20($2)
sw	$4,8($3)
sw	$5,12($3)
bne	$2,$11,$L711
addu	$3,$3,$10

lw	$3,9448($16)
addu	$2,$9,$20
lw	$4,11856($fp)
sll	$3,$3,2
lw	$2,0($2)
addu	$3,$4,$3
lw	$4,0($3)
sll	$4,$4,2
beq	$19,$0,$L712
addu	$4,$2,$4

move	$24,$0
move	$25,$0
sw	$24,0($4)
sw	$25,4($4)
sw	$24,8($4)
sw	$25,12($4)
sw	$24,16($4)
sw	$25,20($4)
sw	$24,24($4)
sw	$25,28($4)
$L713:
lw	$2,0($9)
lb	$3,0($8)
addu	$2,$2,$18
sb	$3,0($2)
lb	$3,2($8)
sb	$3,1($2)
lb	$3,16($8)
sb	$3,2($2)
lb	$3,18($8)
sb	$3,3($2)
$L710:
lw	$2,6632($12)
addiu	$7,$7,1
addiu	$9,$9,4
addiu	$8,$8,40
sltu	$2,$7,$2
bne	$2,$0,$L714
addiu	$6,$6,160

$L715:
li	$2,65536			# 0x10000
li	$3,3			# 0x3
addu	$2,$fp,$2
lw	$4,-5260($2)
beq	$4,$3,$L1012
nop

$L947:
b	$L705
lw	$3,56($sp)

$L712:
lw	$2,0($6)
lw	$3,4($6)
sw	$2,0($4)
sw	$3,4($4)
lw	$2,8($6)
lw	$3,12($6)
sw	$2,8($4)
sw	$3,12($4)
lw	$2,-84($6)
sw	$2,24($4)
lw	$2,-52($6)
sw	$2,20($4)
lw	$2,-20($6)
b	$L713
sw	$2,16($4)

$L1010:
andi	$2,$4,0x7
lw	$9,76($sp)
sltu	$2,$2,1
lw	$8,2872($20)
addiu	$2,$2,15062
lw	$25,%got(decode_cabac_residual_nondc)($28)
li	$4,64			# 0x40
sll	$2,$2,2
sw	$9,16($sp)
sll	$8,$8,8
addu	$2,$20,$2
sw	$4,24($sp)
li	$6,5			# 0x5
addiu	$25,$25,%lo(decode_cabac_residual_nondc)
lw	$2,0($2)
move	$4,$20
move	$5,$18
addiu	$21,$7,4
addu	$2,$2,$8
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_nondc
1:	jalr	$25
sw	$2,20($sp)

b	$L752
lw	$28,32($sp)

$L731:
li	$2,131072			# 0x20000
sw	$0,11196($fp)
sw	$0,11204($fp)
addu	$2,$fp,$2
sw	$0,11212($fp)
sw	$0,11220($fp)
sb	$0,11226($fp)
sb	$0,11225($fp)
sb	$0,11218($fp)
sb	$0,11217($fp)
sb	$0,11202($fp)
sb	$0,11201($fp)
sb	$0,11194($fp)
sb	$0,11193($fp)
b	$L762
sw	$0,8752($2)

$L1007:
li	$19,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
lw	$2,56($sp)
addiu	$19,$19,8224
addiu	$17,$17,%lo(get_cabac_noinline)
b	$L730
addu	$19,$fp,$19

$L535:
li	$5,17			# 0x11
$L934:
lw	$25,%got(decode_cabac_intra_mb_type)($28)
move	$6,$0
addiu	$25,$25,%lo(decode_cabac_intra_mb_type)
.reloc	1f,R_MIPS_JALR,decode_cabac_intra_mb_type
1:	jalr	$25
move	$4,$fp

b	$L532
lw	$28,32($sp)

$L1009:
li	$23,131072			# 0x20000
lw	$25,%got(decode_cabac_residual_dc)($28)
li	$2,16			# 0x10
sw	$21,16($sp)
addiu	$23,$23,6944
move	$6,$0
addu	$23,$fp,$23
sw	$2,20($sp)
move	$7,$0
addiu	$25,$25,%lo(decode_cabac_residual_dc)
move	$4,$fp
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_dc
1:	jalr	$25
move	$5,$23

lw	$3,72($sp)
andi	$2,$3,0xf
beq	$2,$0,$L747
lw	$28,32($sp)

li	$3,65536			# 0x10000
lw	$2,2872($fp)
lw	$18,%got(decode_cabac_residual_nondc)($28)
addiu	$21,$16,1
addu	$3,$fp,$3
sll	$2,$2,6
move	$22,$0
lw	$17,-5312($3)
addiu	$18,$18,%lo(decode_cabac_residual_nondc)
li	$20,15			# 0xf
li	$19,16			# 0x10
addu	$17,$17,$2
$L748:
move	$7,$22
sw	$21,16($sp)
li	$6,1			# 0x1
sw	$17,20($sp)
addiu	$22,$22,1
sw	$20,24($sp)
move	$5,$23
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_nondc
1:	jalr	$25
move	$4,$fp

addiu	$23,$23,32
bne	$22,$19,$L748
lw	$28,32($sp)

b	$L1053
lw	$8,72($sp)

$L733:
lw	$21,%got(luma_dc_field_scan)($28)
addu	$2,$fp,$2
addiu	$21,$21,%lo(luma_dc_field_scan)
lw	$11,9436($2)
lw	$16,9432($2)
b	$L734
sw	$11,76($sp)

$L735:
lw	$21,%got(luma_dc_zigzag_scan)($28)
addu	$2,$fp,$2
addiu	$21,$21,%lo(luma_dc_zigzag_scan)
lw	$12,9424($2)
lw	$16,9420($2)
b	$L734
sw	$12,76($sp)

$L999:
addiu	$5,$18,8302
move	$4,$16
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

beq	$2,$0,$L1013
move	$4,$16

addiu	$5,$18,8303
addiu	$18,$18,8304
addu	$5,$fp,$5
addu	$19,$fp,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$16

move	$4,$16
move	$5,$19
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
sll	$18,$2,3

sll	$2,$2,2
move	$4,$16
move	$5,$19
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$18,$2,$18

sll	$2,$2,1
move	$4,$16
addu	$18,$2,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$19

addu	$2,$2,$18
slt	$3,$2,8
beq	$3,$0,$L530
lw	$28,32($sp)

b	$L528
addiu	$2,$2,3

$L991:
lw	$8,68($sp)
lw	$5,10800($fp)
addu	$2,$fp,$2
lw	$4,10780($fp)
lw	$6,10772($fp)
lw	$3,8748($2)
beq	$5,$0,$L781
addu	$16,$3,$8

addu	$4,$3,$4
lbu	$2,0($4)
sltu	$2,$0,$2
$L558:
lw	$4,10792($fp)
beq	$4,$0,$L1062
li	$19,131072			# 0x20000

addu	$3,$3,$6
addiu	$4,$2,1
lbu	$3,0($3)
movn	$2,$4,$3
$L1062:
lw	$17,%got(get_cabac_noinline)($28)
addu	$2,$2,$19
addiu	$18,$19,8224
addiu	$5,$2,8336
addu	$18,$fp,$18
addiu	$17,$17,%lo(get_cabac_noinline)
addu	$5,$fp,$5
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$18

lw	$28,32($sp)
bne	$2,$0,$L1014
move	$5,$2

move	$2,$0
$L560:
lw	$25,%call16(ff_h264_check_intra_pred_mode)($28)
move	$4,$fp
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra_pred_mode
1:	jalr	$25
sb	$2,0($16)

bltz	$2,$L949
lw	$28,32($sp)

lw	$3,56($sp)
b	$L557
sw	$2,10760($fp)

$L997:
b	$L741
sra	$2,$2,1

$L1011:
lw	$3,2276($fp)
li	$4,-1			# 0xffffffffffffffff
addu	$3,$3,$18
sh	$4,0($3)
b	$L706
sh	$4,2($3)

$L1003:
addu	$21,$fp,$2
lw	$4,6632($21)
beq	$4,$0,$L557
addiu	$2,$2,6624

lw	$16,%got(decode_cabac_mb_ref)($28)
addiu	$17,$fp,11580
addu	$19,$fp,$2
move	$18,$0
li	$22,4096			# 0x1000
b	$L622
li	$20,-1			# 0xffffffffffffffff

$L1016:
lw	$7,0($19)
move	$4,$fp
move	$5,$18
sltu	$7,$7,2
bne	$7,$0,$L620
move	$2,$0

addiu	$25,$16,%lo(decode_cabac_mb_ref)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref
1:	jalr	$25
nop

sll	$5,$2,8
lw	$28,32($sp)
lw	$3,56($sp)
addu	$2,$5,$2
sll	$4,$2,16
addu	$2,$2,$4
$L620:
sw	$2,0($17)
sw	$2,8($17)
sw	$2,16($17)
sw	$2,24($17)
$L621:
lw	$2,6632($21)
addiu	$18,$18,1
addiu	$17,$17,40
sltu	$5,$18,$2
beq	$5,$0,$L1015
addiu	$19,$19,4

$L622:
sll	$2,$18,1
sll	$2,$22,$2
and	$2,$2,$3
bne	$2,$0,$L1016
move	$6,$0

sw	$20,0($17)
sw	$20,8($17)
sw	$20,16($17)
b	$L621
sw	$20,24($17)

$L1008:
addiu	$5,$16,8288
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

sll	$18,$2,1
lw	$28,32($sp)
b	$L537
addu	$2,$18,$2

$L1012:
andi	$13,$13,0x40
beq	$13,$0,$L947
nop

li	$3,131072			# 0x20000
lhu	$4,-5238($2)
addu	$3,$fp,$3
srl	$4,$4,1
lw	$5,9448($3)
lw	$3,9088($3)
sll	$5,$5,2
addu	$3,$3,$5
sb	$4,1($3)
lhu	$4,-5236($2)
srl	$4,$4,1
sb	$4,2($3)
lhu	$2,-5234($2)
srl	$2,$2,1
sb	$2,3($3)
b	$L705
lw	$3,56($sp)

$L747:
sw	$0,11196($fp)
sw	$0,11204($fp)
sw	$0,11212($fp)
b	$L749
sw	$0,11220($fp)

$L985:
lw	$25,%got(pred_motion)($28)
move	$5,$0
sw	$0,16($sp)
li	$6,4			# 0x4
sw	$2,20($sp)
addiu	$2,$sp,40
move	$7,$0
addiu	$25,$25,%lo(pred_motion)
sw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,pred_motion
1:	jalr	$25
move	$4,$fp

lw	$3,40($sp)
lhu	$2,56($sp)
sll	$3,$3,16
b	$L512
addu	$2,$2,$3

$L1013:
addiu	$5,$18,8304
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

lw	$28,32($sp)
b	$L528
addiu	$2,$2,1

$L545:
lw	$25,%call16(ff_h264_check_intra_pred_mode)($28)
move	$4,$fp
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra_pred_mode
1:	jalr	$25
lw	$5,10764($fp)

lw	$28,32($sp)
bgez	$2,$L552
sw	$2,10764($fp)

$L949:
b	$L843
li	$2,-1			# 0xffffffffffffffff

$L1001:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$3,-5260($2)
li	$2,3			# 0x3
beq	$3,$2,$L1017
li	$2,131072			# 0x20000

lw	$16,%got(get_cabac)($28)
lw	$19,%got(p_sub_mb_type_info)($28)
addiu	$3,$sp,40
addiu	$17,$2,8224
addiu	$21,$2,8293
addiu	$22,$2,8294
sw	$3,140($sp)
addiu	$2,$2,8295
li	$23,60296			# 0xeb88
li	$20,60304			# 0xeb90
addu	$2,$fp,$2
addu	$17,$fp,$17
addu	$21,$fp,$21
addu	$23,$fp,$23
sw	$2,64($sp)
addu	$20,$fp,$20
addiu	$16,$16,%lo(get_cabac)
addiu	$19,$19,%lo(p_sub_mb_type_info)
addu	$22,$fp,$22
move	$18,$3
$L574:
move	$4,$17
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$21

move	$3,$0
beq	$2,$0,$L1018
lw	$28,32($sp)

$L573:
sll	$2,$3,2
addiu	$23,$23,2
addu	$2,$19,$2
addiu	$18,$18,4
lhu	$3,0($2)
lbu	$2,2($2)
sh	$3,-2($23)
bne	$23,$20,$L574
sw	$2,-4($18)

li	$3,65536			# 0x10000
$L1055:
addu	$2,$fp,$3
lw	$2,6632($2)
beq	$2,$0,$L1019
lw	$23,%got(scan8)($28)

li	$2,60296			# 0xeb88
addiu	$3,$3,6624
lw	$10,%got(decode_cabac_mb_ref)($28)
addu	$9,$fp,$2
addu	$22,$fp,$3
move	$19,$0
li	$8,4096			# 0x1000
addiu	$23,$23,%lo(scan8)
move	$21,$9
$L581:
sll	$18,$19,3
sll	$3,$19,5
move	$2,$18
sll	$20,$19,1
addu	$18,$2,$3
sll	$20,$8,$20
move	$16,$0
addu	$18,$fp,$18
li	$7,16			# 0x10
move	$17,$21
$L580:
lhu	$2,0($17)
andi	$3,$2,0x100
bne	$3,$0,$L575
and	$2,$2,$20

beq	$2,$0,$L579
li	$6,-1			# 0xffffffffffffffff

lw	$2,0($22)
addiu	$25,$10,%lo(decode_cabac_mb_ref)
move	$4,$fp
move	$5,$19
sltu	$2,$2,2
bne	$2,$0,$L577
move	$6,$16

sw	$7,144($sp)
sw	$8,156($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref
1:	jalr	$25
sw	$10,168($sp)

lw	$3,0($22)
lw	$28,32($sp)
lw	$7,144($sp)
sltu	$4,$2,$3
lw	$8,156($sp)
beq	$4,$0,$L644
lw	$10,168($sp)

sll	$6,$2,24
sra	$6,$6,24
$L579:
addu	$2,$23,$16
lbu	$3,0($2)
addu	$3,$18,$3
sb	$6,11577($3)
sb	$6,11576($3)
sb	$6,11569($3)
$L575:
addiu	$16,$16,4
bne	$16,$7,$L580
addiu	$17,$17,2

li	$2,65536			# 0x10000
addiu	$19,$19,1
addu	$2,$fp,$2
lw	$2,6632($2)
sltu	$3,$19,$2
bne	$3,$0,$L581
addiu	$22,$22,4

lw	$3,96($sp)
beq	$3,$0,$L582
nop

$L772:
li	$4,65536			# 0x10000
lw	$3,11956($fp)
addu	$4,$fp,$4
bne	$3,$0,$L1020
lw	$5,-5240($4)

lw	$3,-5236($4)
li	$4,24117248			# 0x1700000
addiu	$4,$4,368
or	$3,$3,$5
and	$3,$3,$4
sltu	$3,$3,1
sw	$3,96($sp)
$L582:
beq	$2,$0,$L585
li	$2,60296			# 0xeb88

addiu	$11,$fp,11288
addu	$2,$fp,$2
addiu	$12,$fp,11579
move	$21,$0
sw	$11,112($sp)
sw	$2,124($sp)
li	$2,60304			# 0xeb90
sw	$12,116($sp)
move	$18,$21
addu	$2,$fp,$2
sw	$2,100($sp)
li	$2,65536			# 0x10000
addu	$9,$fp,$2
$L616:
sll	$2,$18,1
lw	$20,124($sp)
li	$3,4096			# 0x1000
sw	$18,64($sp)
sll	$17,$18,5
sll	$3,$3,$2
sll	$19,$18,3
move	$10,$0
sw	$17,84($sp)
sw	$3,104($sp)
li	$3,12288			# 0x3000
move	$8,$20
sw	$19,80($sp)
sll	$3,$3,$2
move	$22,$10
move	$20,$17
sw	$3,128($sp)
$L615:
lw	$4,%got(scan8)($28)
addu	$2,$19,$20
addiu	$4,$4,%lo(scan8)
addu	$3,$4,$22
lbu	$4,0($3)
addu	$3,$fp,$2
addu	$3,$3,$4
lb	$5,11569($3)
sb	$5,11568($3)
lhu	$3,0($8)
andi	$5,$3,0x100
beq	$5,$0,$L586
lw	$11,104($sp)

addu	$2,$2,$4
li	$3,34960			# 0x8890
addu	$2,$2,$3
sll	$2,$2,2
addu	$2,$fp,$2
sw	$0,0($2)
sw	$0,4($2)
sw	$0,32($2)
sw	$0,36($2)
$L587:
lw	$15,100($sp)
addiu	$8,$8,2
bne	$8,$15,$L615
addiu	$22,$22,4

lw	$24,112($sp)
lw	$25,116($sp)
lw	$18,64($sp)
lw	$2,6632($9)
addiu	$24,$24,160
addiu	$25,$25,40
addiu	$18,$18,1
sw	$24,112($sp)
sltu	$2,$18,$2
bne	$2,$0,$L616
sw	$25,116($sp)

b	$L557
lw	$3,56($sp)

$L586:
and	$5,$3,$11
beq	$5,$0,$L588
addu	$2,$2,$4

lw	$12,140($sp)
li	$4,2			# 0x2
addu	$2,$12,$22
andi	$12,$3,0x18
lw	$2,0($2)
sw	$2,88($sp)
li	$2,1			# 0x1
lw	$13,88($sp)
movn	$2,$4,$12
blez	$13,$L587
move	$12,$2

andi	$4,$3,0x8
sw	$22,132($sp)
andi	$2,$3,0x10
sw	$8,136($sp)
andi	$3,$3,0x20
move	$11,$0
andi	$4,$4,0xffff
andi	$2,$2,0xffff
andi	$3,$3,0xffff
sw	$4,92($sp)
move	$17,$22
sw	$2,108($sp)
move	$23,$19
sw	$3,120($sp)
move	$19,$17
move	$21,$20
move	$22,$12
move	$17,$fp
b	$L613
move	$20,$11

$L1024:
beq	$7,$3,$L1021
slt	$3,$16,20

$L592:
addu	$3,$23,$21
addu	$3,$3,$5
addiu	$3,$3,2812
sll	$3,$3,2
addu	$3,$17,$3
$L596:
xor	$18,$6,$24
xor	$5,$6,$7
sltu	$18,$18,1
sltu	$5,$5,1
xor	$6,$6,$25
addu	$5,$5,$18
sltu	$6,$6,1
addu	$5,$5,$6
slt	$31,$5,2
bne	$31,$0,$L1063
li	$31,1			# 0x1

$L605:
lh	$5,0($4)
lh	$18,0($2)
slt	$7,$18,$5
beq	$7,$0,$L606
lh	$6,0($3)

slt	$7,$18,$6
beq	$7,$0,$L607
nop

slt	$18,$5,$6
movz	$5,$6,$18
move	$18,$5
$L607:
lh	$24,2($2)
lh	$4,2($4)
lh	$2,2($3)
slt	$3,$24,$4
beq	$3,$0,$L608
slt	$3,$2,$24

slt	$3,$24,$2
beq	$3,$0,$L1064
lw	$25,%got(decode_cabac_mb_mvd_bak)($28)

slt	$24,$4,$2
movz	$4,$2,$24
move	$24,$4
$L601:
lw	$25,%got(decode_cabac_mb_mvd_bak)($28)
$L1064:
move	$7,$0
lw	$5,64($sp)
move	$4,$17
sw	$9,164($sp)
move	$6,$19
addiu	$25,$25,%lo(decode_cabac_mb_mvd_bak)
sw	$10,168($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_bak
1:	jalr	$25
sw	$24,144($sp)

li	$7,1			# 0x1
lw	$28,32($sp)
addu	$3,$2,$18
lw	$5,64($sp)
move	$4,$17
move	$6,$19
lw	$25,%got(decode_cabac_mb_mvd_bak)($28)
addiu	$25,$25,%lo(decode_cabac_mb_mvd_bak)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_bak
1:	jalr	$25
sw	$3,160($sp)

lw	$24,144($sp)
lw	$8,92($sp)
lw	$28,32($sp)
addu	$2,$2,$24
lw	$3,160($sp)
lw	$9,164($sp)
beq	$8,$0,$L609
lw	$10,168($sp)

subu	$18,$3,$18
subu	$24,$2,$24
sll	$3,$3,16
sll	$2,$2,16
sll	$18,$18,16
sll	$24,$24,16
sra	$3,$3,16
sra	$2,$2,16
sra	$18,$18,16
sra	$24,$24,16
sh	$3,36($10)
sh	$3,32($10)
sh	$3,4($10)
sh	$2,38($10)
sh	$2,34($10)
sh	$2,6($10)
sh	$18,36($fp)
sh	$18,32($fp)
sh	$18,4($fp)
sh	$24,38($fp)
sh	$24,34($fp)
sh	$24,6($fp)
$L610:
addu	$13,$23,$21
lw	$23,80($sp)
li	$4,131072			# 0x20000
lw	$21,84($sp)
addu	$16,$16,$13
lw	$13,88($sp)
addiu	$20,$20,1
sll	$16,$16,2
addu	$19,$19,$22
addu	$16,$17,$16
addu	$4,$16,$4
sh	$3,11248($16)
sh	$2,11250($16)
sh	$18,8768($4)
beq	$20,$13,$L1023
sh	$24,8770($4)

$L613:
lw	$15,%got(scan8)($28)
addu	$2,$23,$21
li	$fp,34960			# 0x8890
lw	$31,-5252($9)
addiu	$15,$15,%lo(scan8)
addu	$3,$15,$19
lbu	$16,0($3)
addu	$3,$17,$2
addiu	$18,$16,-8
addiu	$7,$16,-1
addu	$6,$2,$16
addu	$4,$2,$7
addu	$2,$2,$18
addu	$5,$22,$18
addiu	$10,$6,2812
addu	$fp,$6,$fp
addiu	$4,$4,2812
addiu	$2,$2,2812
addu	$7,$3,$7
addu	$6,$3,$16
addu	$18,$3,$18
addu	$3,$3,$5
lb	$25,11568($7)
sll	$10,$10,2
lb	$6,11568($6)
sll	$fp,$fp,2
lb	$24,11568($18)
sll	$4,$4,2
lb	$7,11568($3)
sll	$2,$2,2
addu	$10,$17,$10
addu	$fp,$17,$fp
addu	$4,$17,$4
addu	$2,$17,$2
bne	$31,$0,$L1024
li	$3,-2			# 0xfffffffffffffffe

bne	$7,$3,$L592
nop

addiu	$5,$16,-9
$L1065:
addu	$7,$23,$21
xor	$18,$6,$24
addu	$3,$7,$5
addu	$7,$17,$7
sltu	$18,$18,1
addu	$5,$7,$5
addiu	$3,$3,2812
lb	$7,11568($5)
sll	$3,$3,2
xor	$5,$6,$7
sltu	$5,$5,1
xor	$6,$6,$25
addu	$5,$5,$18
sltu	$6,$6,1
addu	$5,$5,$6
slt	$31,$5,2
beq	$31,$0,$L605
addu	$3,$17,$3

li	$31,1			# 0x1
$L1063:
beq	$5,$31,$L1025
li	$5,-2			# 0xfffffffffffffffe

bne	$24,$5,$L605
nop

bne	$7,$24,$L605
nop

beq	$25,$7,$L605
nop

$L937:
lh	$18,0($4)
b	$L601
lh	$24,2($4)

$L609:
lw	$11,108($sp)
beq	$11,$0,$L611
lw	$12,120($sp)

subu	$18,$3,$18
subu	$24,$2,$24
sll	$3,$3,16
sll	$2,$2,16
sll	$18,$18,16
sll	$24,$24,16
sra	$3,$3,16
sra	$2,$2,16
sra	$18,$18,16
sra	$24,$24,16
sh	$3,4($10)
sh	$2,6($10)
sh	$18,4($fp)
b	$L610
sh	$24,6($fp)

$L608:
beq	$3,$0,$L1064
lw	$25,%got(decode_cabac_mb_mvd_bak)($28)

slt	$24,$2,$4
movz	$4,$2,$24
b	$L1064
move	$24,$4

$L606:
slt	$7,$6,$18
beq	$7,$0,$L607
nop

slt	$18,$6,$5
movz	$5,$6,$18
b	$L607
move	$18,$5

$L588:
li	$4,34960			# 0x8890
addiu	$3,$2,2812
addu	$2,$2,$4
sll	$3,$3,2
sll	$2,$2,2
addu	$3,$fp,$3
addu	$2,$fp,$2
sw	$0,0($3)
sw	$0,4($3)
sw	$0,32($3)
sw	$0,36($3)
sw	$0,36($2)
sw	$0,32($2)
sw	$0,4($2)
b	$L587
sw	$0,0($2)

$L611:
subu	$18,$3,$18
subu	$24,$2,$24
sll	$3,$3,16
sll	$2,$2,16
sll	$18,$18,16
sll	$24,$24,16
sra	$3,$3,16
sra	$2,$2,16
sra	$18,$18,16
beq	$12,$0,$L610
sra	$24,$24,16

sh	$3,32($10)
sh	$2,34($10)
sh	$18,32($fp)
b	$L610
sh	$24,34($fp)

$L577:
b	$L579
move	$6,$0

$L1021:
bne	$3,$0,$L1065
addiu	$5,$16,-9

li	$3,4			# 0x4
andi	$5,$16,0x7
bne	$5,$3,$L1065
addiu	$5,$16,-9

lw	$8,116($sp)
lb	$3,0($8)
beq	$3,$7,$L1065
nop

lw	$5,2696($17)
lw	$3,112($sp)
lw	$31,104($5)
sw	$0,0($3)
lw	$5,-5248($9)
beq	$5,$0,$L1026
nop

lw	$5,10800($17)
andi	$5,$5,0x80
bne	$5,$0,$L1065
addiu	$5,$16,-9

slt	$5,$16,36
lw	$8,128($sp)
xori	$5,$5,0x1
addiu	$5,$5,2694
sra	$7,$16,2
sll	$5,$5,2
addu	$5,$17,$5
lw	$5,4($5)
sll	$11,$5,2
addu	$5,$31,$11
lw	$5,0($5)
and	$5,$8,$5
beq	$5,$0,$L791
andi	$18,$7,0x3

lw	$5,11864($17)
andi	$8,$7,0x2
lw	$12,64($sp)
lw	$14,11852($17)
lw	$13,2696($17)
sll	$31,$12,2
mul	$12,$18,$5
addu	$5,$14,$11
addu	$18,$13,$31
lw	$7,0($5)
lw	$31,96($18)
addiu	$5,$12,3
lw	$12,112($sp)
addu	$5,$5,$7
sll	$5,$5,2
addu	$5,$31,$5
lh	$7,0($5)
sh	$7,0($12)
lh	$7,2($5)
srl	$5,$7,31
addu	$5,$5,$7
sra	$5,$5,1
sh	$5,2($12)
lw	$5,188($18)
addu	$5,$5,$11
addu	$5,$5,$8
lb	$7,1($5)
b	$L596
sll	$7,$7,1

$L1023:
lw	$22,132($sp)
move	$fp,$17
lw	$8,136($sp)
move	$19,$23
b	$L587
move	$20,$21

$L1025:
bne	$6,$0,$L937
nop

bne	$18,$0,$L1027
nop

lh	$18,0($3)
b	$L601
lh	$24,2($3)

$L1018:
move	$4,$17
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$22

li	$3,1			# 0x1
beq	$2,$0,$L573
lw	$28,32($sp)

lw	$5,64($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$17

li	$4,3			# 0x3
li	$3,2			# 0x2
lw	$28,32($sp)
b	$L573
movz	$3,$4,$2

$L1015:
beq	$2,$0,$L557
li	$21,131072			# 0x20000

li	$18,131072			# 0x20000
addiu	$18,$18,8816
li	$20,65536			# 0x10000
addu	$18,$fp,$18
move	$17,$0
addiu	$23,$21,8820
addiu	$22,$21,8824
addu	$20,$fp,$20
move	$19,$fp
b	$L639
move	$16,$fp

$L623:
sw	$0,11296($16)
addu	$5,$16,$23
sw	$0,11300($16)
addu	$25,$16,$2
sw	$0,11304($16)
addu	$4,$16,$22
sw	$0,11308($16)
sw	$0,11328($16)
sw	$0,11332($16)
sw	$0,11336($16)
sw	$0,11340($16)
sw	$0,11360($16)
sw	$0,11364($16)
sw	$0,11368($16)
sw	$0,11372($16)
sw	$0,11392($16)
sw	$0,11396($16)
sw	$0,11400($16)
sw	$0,11404($16)
sw	$0,0($18)
sw	$0,0($5)
sw	$0,0($4)
sw	$0,0($25)
sw	$0,20($25)
sw	$0,24($25)
sw	$0,28($25)
sw	$0,32($25)
sw	$0,52($25)
sw	$0,56($25)
sw	$0,60($25)
sw	$0,64($25)
sw	$0,84($25)
sw	$0,88($25)
sw	$0,92($25)
sw	$0,96($25)
$L638:
lw	$2,6632($20)
addiu	$17,$17,1
addiu	$18,$18,160
addiu	$16,$16,160
sltu	$2,$17,$2
beq	$2,$0,$L557
addiu	$19,$19,40

$L639:
sll	$4,$17,1
li	$2,4096			# 0x1000
sll	$2,$2,$4
and	$2,$2,$3
beq	$2,$0,$L623
addiu	$2,$21,8828

lb	$2,11576($19)
li	$4,-2			# 0xfffffffffffffffe
lb	$3,11580($19)
lb	$7,11572($19)
beq	$2,$4,$L624
lb	$8,11579($19)

addiu	$6,$16,11280
move	$4,$2
$L625:
xor	$5,$3,$7
xor	$2,$3,$4
sltu	$5,$5,1
sltu	$2,$2,1
xor	$3,$3,$8
addu	$2,$2,$5
sltu	$3,$3,1
addu	$2,$2,$3
slt	$9,$2,2
bne	$9,$0,$L1028
li	$9,1			# 0x1

$L634:
lh	$2,11292($16)
lh	$8,11264($16)
slt	$4,$8,$2
beq	$4,$0,$L635
lh	$3,0($6)

slt	$4,$8,$3
beq	$4,$0,$L636
nop

slt	$8,$2,$3
movz	$2,$3,$8
move	$8,$2
$L636:
lh	$2,11294($16)
lh	$3,11266($16)
slt	$5,$3,$2
beq	$5,$0,$L637
lh	$4,2($6)

slt	$5,$3,$4
beq	$5,$0,$L1066
lw	$9,%got(decode_cabac_mb_mvd_bak)($28)

slt	$3,$2,$4
movz	$2,$4,$3
move	$3,$2
$L630:
lw	$9,%got(decode_cabac_mb_mvd_bak)($28)
$L1066:
move	$6,$0
move	$7,$0
sw	$3,160($sp)
sw	$8,156($sp)
move	$4,$fp
addiu	$25,$9,%lo(decode_cabac_mb_mvd_bak)
move	$5,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_bak
1:	jalr	$25
sw	$25,144($sp)

move	$6,$0
lw	$25,144($sp)
li	$7,1			# 0x1
move	$4,$fp
sw	$2,164($sp)
jalr	$25
move	$5,$17

addu	$6,$16,$23
lw	$3,160($sp)
sll	$5,$2,16
lw	$9,164($sp)
lw	$8,156($sp)
addu	$3,$2,$3
lw	$28,32($sp)
addu	$8,$9,$8
sll	$4,$3,16
andi	$9,$9,0xffff
addiu	$3,$21,8828
andi	$2,$8,0xffff
addu	$2,$2,$4
addu	$5,$9,$5
addu	$7,$16,$3
addu	$4,$16,$22
sw	$5,0($18)
sw	$5,0($6)
sw	$5,0($4)
sw	$5,0($7)
sw	$5,20($7)
sw	$5,24($7)
sw	$5,28($7)
sw	$5,32($7)
sw	$5,52($7)
sw	$5,56($7)
sw	$5,60($7)
sw	$5,64($7)
sw	$5,84($7)
sw	$5,88($7)
sw	$5,92($7)
sw	$5,96($7)
sw	$2,11296($16)
sw	$2,11300($16)
sw	$2,11304($16)
sw	$2,11308($16)
sw	$2,11328($16)
sw	$2,11332($16)
sw	$2,11336($16)
sw	$2,11340($16)
sw	$2,11360($16)
sw	$2,11364($16)
sw	$2,11368($16)
sw	$2,11372($16)
sw	$2,11392($16)
sw	$2,11396($16)
sw	$2,11400($16)
sw	$2,11404($16)
b	$L638
lw	$3,56($sp)

$L637:
slt	$5,$4,$3
beq	$5,$0,$L1066
lw	$9,%got(decode_cabac_mb_mvd_bak)($28)

slt	$3,$4,$2
movz	$2,$4,$3
b	$L1066
move	$3,$2

$L635:
slt	$4,$3,$8
beq	$4,$0,$L636
nop

slt	$8,$3,$2
movz	$2,$3,$8
b	$L636
move	$8,$2

$L624:
lb	$4,11571($19)
b	$L625
addiu	$6,$16,11260

$L1002:
lw	$25,%call16(pred_direct_motion_hw)($28)
addiu	$5,$sp,56
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$fp

li	$2,131072			# 0x20000
lw	$28,32($sp)
addu	$2,$fp,$2
sw	$0,8816($2)
sw	$0,8820($2)
sw	$0,8824($2)
sw	$0,8828($2)
sw	$0,8848($2)
sw	$0,8852($2)
sw	$0,8856($2)
sw	$0,8860($2)
sw	$0,8880($2)
sw	$0,8884($2)
sw	$0,8888($2)
sw	$0,8892($2)
sw	$0,8912($2)
sw	$0,8916($2)
sw	$0,8920($2)
sw	$0,8924($2)
sw	$0,8976($2)
sw	$0,8980($2)
sw	$0,8984($2)
sw	$0,8988($2)
sw	$0,9008($2)
sw	$0,9012($2)
sw	$0,9016($2)
sw	$0,9020($2)
sw	$0,9040($2)
sw	$0,9044($2)
sw	$0,9048($2)
sw	$0,9052($2)
sw	$0,9072($2)
sw	$0,9076($2)
sw	$0,9080($2)
sw	$0,9084($2)
lw	$2,11956($fp)
lw	$8,96($sp)
lw	$3,56($sp)
and	$8,$8,$2
b	$L557
sw	$8,96($sp)

$L1028:
beq	$2,$9,$L1029
li	$2,-2			# 0xfffffffffffffffe

bne	$7,$2,$L634
nop

bne	$4,$7,$L634
nop

beq	$8,$4,$L634
nop

$L940:
lh	$8,11292($16)
b	$L630
lh	$3,11294($16)

$L546:
lw	$5,11656($fp)
li	$2,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
addiu	$4,$2,8224
addu	$2,$2,$5
addiu	$17,$17,%lo(get_cabac_noinline)
addiu	$5,$2,8671
addu	$4,$fp,$4
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

beq	$2,$0,$L548
lw	$28,32($sp)

lw	$2,56($sp)
li	$3,16777216			# 0x1000000
lw	$20,%got(scan8)($28)
addiu	$16,$16,%lo(decode_cabac_mb_intra4x4_pred_mode)
lw	$17,%got(scan8+16)($28)
or	$2,$2,$3
addiu	$20,$20,%lo(scan8)
addiu	$17,$17,%lo(scan8+16)
sw	$2,56($sp)
$L550:
lbu	$19,0($20)
move	$4,$fp
move	$25,$16
addiu	$20,$20,4
addu	$2,$fp,$19
lb	$5,10808($2)
lb	$2,10815($2)
slt	$3,$2,$5
movz	$2,$5,$3
slt	$5,$2,0
movn	$2,$18,$5
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_intra4x4_pred_mode
1:	jalr	$25
move	$5,$2

addiu	$3,$19,10816
sll	$5,$2,8
lw	$28,32($sp)
addu	$3,$fp,$3
addu	$2,$5,$2
andi	$2,$2,0xffff
sh	$2,0($3)
bne	$20,$17,$L550
sh	$2,8($3)

b	$L1054
lw	$25,%call16(ff_h264_write_back_intra_pred_mode)($28)

$L1027:
lh	$18,0($2)
b	$L601
lh	$24,2($2)

$L721:
lw	$19,8744($21)
move	$4,$16
lw	$20,8740($21)
move	$25,$17
sra	$19,$19,4
sra	$20,$20,4
andi	$19,$19,0x3
sltu	$2,$0,$19
addiu	$3,$2,2
andi	$20,$20,0x3
movn	$2,$3,$20
addiu	$2,$2,77
addu	$5,$2,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

beq	$2,$0,$L723
lw	$28,32($sp)

li	$3,4			# 0x4
li	$2,5			# 0x5
xori	$19,$19,0x2
movn	$2,$3,$19
xori	$20,$20,0x2
move	$4,$16
move	$25,$17
addiu	$3,$2,2
movz	$2,$3,$20
addiu	$2,$2,77
addu	$5,$2,$18
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$fp,$5

addiu	$2,$2,1
lw	$28,32($sp)
sll	$2,$2,4
$L723:
lw	$8,72($sp)
lw	$3,56($sp)
or	$8,$8,$2
b	$L716
sw	$8,72($sp)

$L781:
b	$L558
move	$2,$0

$L994:
lw	$5,11656($fp)
lw	$17,%got(get_cabac_noinline)($28)
addu	$19,$fp,$19
addu	$4,$4,$5
addiu	$17,$17,%lo(get_cabac_noinline)
addiu	$5,$4,8671
move	$25,$17
addu	$5,$fp,$5
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$19

lw	$3,56($sp)
sll	$2,$2,24
lw	$28,32($sp)
or	$3,$2,$3
b	$L728
sw	$3,56($sp)

$L644:
lw	$6,%got($LC1)($28)
li	$5,16			# 0x10
lw	$4,0($fp)
move	$7,$2
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC1)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)

b	$L843
li	$2,-1			# 0xffffffffffffffff

$L640:
addu	$23,$fp,$2
lw	$4,6632($23)
beq	$4,$0,$L557
addiu	$2,$2,6624

lw	$16,%got(decode_cabac_mb_ref)($28)
addiu	$17,$fp,11596
addu	$19,$fp,$2
li	$20,1			# 0x1
move	$18,$0
li	$21,4096			# 0x1000
li	$22,-1			# 0xffffffffffffffff
$L646:
sll	$2,$18,1
sll	$2,$21,$2
and	$2,$2,$3
bne	$2,$0,$L766
sll	$2,$21,$20

sw	$22,-16($17)
and	$2,$3,$2
beq	$2,$0,$L1030
sw	$22,-8($17)

$L770:
lw	$2,0($19)
sltu	$2,$2,2
bne	$2,$0,$L793
li	$6,8			# 0x8

addiu	$25,$16,%lo(decode_cabac_mb_ref)
move	$4,$fp
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref
1:	jalr	$25
move	$5,$18

lw	$3,0($19)
sltu	$4,$2,$3
beq	$4,$0,$L644
lw	$28,32($sp)

sll	$4,$2,8
lw	$3,56($sp)
addu	$2,$4,$2
sll	$4,$2,16
addu	$2,$2,$4
$L643:
sw	$2,0($17)
sw	$2,8($17)
$L645:
lw	$2,6632($23)
addiu	$18,$18,1
addiu	$17,$17,40
addiu	$19,$19,4
sltu	$4,$18,$2
bne	$4,$0,$L646
addiu	$20,$20,2

beq	$2,$0,$L557
addiu	$15,$fp,11288

li	$13,131072			# 0x20000
move	$21,$0
move	$18,$0
addiu	$14,$13,8816
move	$23,$fp
b	$L671
move	$22,$15

$L1032:
li	$2,65536			# 0x10000
move	$18,$22
addu	$2,$fp,$2
addiu	$18,$18,1
move	$22,$23
lw	$2,6632($2)
addiu	$21,$21,2
addiu	$23,$11,40
sltu	$2,$18,$2
beq	$2,$0,$L557
addiu	$22,$22,160

$L671:
sll	$17,$18,3
sll	$4,$18,5
sll	$15,$18,2
addu	$17,$17,$4
li	$8,12288			# 0x3000
move	$19,$0
sw	$15,64($sp)
sll	$8,$8,$21
addu	$10,$fp,$17
addiu	$16,$22,-11288
move	$20,$23
move	$11,$23
move	$23,$22
move	$22,$18
move	$18,$17
$L670:
addu	$4,$21,$19
li	$2,4096			# 0x1000
sll	$2,$2,$4
and	$2,$2,$3
bne	$2,$0,$L1031
li	$4,131072			# 0x20000

addiu	$2,$4,8820
addu	$4,$16,$14
addu	$6,$16,$2
sw	$0,0($4)
sw	$0,0($6)
sw	$0,4($6)
sw	$0,8($6)
sw	$0,28($6)
sw	$0,32($6)
sw	$0,36($6)
sw	$0,40($6)
sw	$0,11296($16)
sw	$0,11300($16)
sw	$0,11304($16)
sw	$0,11308($16)
sw	$0,11328($16)
sw	$0,11332($16)
sw	$0,11336($16)
sw	$0,11340($16)
$L669:
addiu	$16,$16,64
bne	$19,$0,$L1032
addiu	$20,$20,16

b	$L670
li	$19,1			# 0x1

$L766:
lw	$2,0($19)
sltu	$2,$2,2
beq	$2,$0,$L768
move	$6,$0

move	$2,$0
sw	$2,-16($17)
sw	$2,-8($17)
$L1035:
sll	$2,$21,$20
and	$2,$3,$2
bne	$2,$0,$L770
nop

$L1030:
sw	$22,0($17)
b	$L645
sw	$22,8($17)

$L1031:
sll	$17,$19,3
bne	$17,$0,$L649
lb	$3,11580($20)

lb	$2,11572($11)
beq	$3,$2,$L1033
lw	$12,%got(scan8)($28)

$L1068:
li	$13,65536			# 0x10000
addu	$13,$fp,$13
addiu	$12,$12,%lo(scan8)
addu	$2,$12,$17
lw	$15,-5252($13)
lbu	$12,0($2)
addiu	$5,$12,-8
addiu	$4,$12,-1
addu	$9,$18,$5
addu	$7,$18,$4
addiu	$2,$12,-4
addiu	$7,$7,2812
addiu	$9,$9,2812
addu	$4,$10,$4
addu	$5,$10,$5
addu	$24,$10,$2
sll	$7,$7,2
lb	$6,11568($4)
sll	$9,$9,2
lb	$5,11568($5)
addu	$7,$fp,$7
lb	$4,11568($24)
beq	$15,$0,$L652
addu	$9,$fp,$9

li	$15,-2			# 0xfffffffffffffffe
beq	$4,$15,$L1034
nop

$L653:
addu	$2,$18,$2
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$fp,$2
$L657:
xor	$13,$3,$5
xor	$12,$3,$4
sltu	$13,$13,1
sltu	$12,$12,1
xor	$3,$3,$6
addu	$12,$12,$13
sltu	$3,$3,1
addu	$12,$12,$3
slt	$15,$12,2
bne	$15,$0,$L658
nop

lh	$4,0($7)
lh	$3,0($9)
slt	$6,$3,$4
beq	$6,$0,$L659
lh	$5,0($2)

slt	$6,$3,$5
beq	$6,$0,$L667
nop

slt	$3,$4,$5
movz	$4,$5,$3
move	$3,$4
$L667:
lh	$4,2($7)
lh	$9,2($9)
slt	$5,$9,$4
beq	$5,$0,$L668
lh	$2,2($2)

slt	$5,$9,$2
beq	$5,$0,$L1067
lw	$15,%got(decode_cabac_mb_mvd_bak)($28)

slt	$9,$4,$2
movz	$4,$2,$9
move	$9,$4
$L651:
lw	$15,%got(decode_cabac_mb_mvd_bak)($28)
$L1067:
move	$7,$0
move	$6,$17
sw	$3,160($sp)
sw	$8,156($sp)
move	$4,$fp
addiu	$12,$15,%lo(decode_cabac_mb_mvd_bak)
sw	$9,164($sp)
sw	$10,168($sp)
move	$5,$22
sw	$11,152($sp)
move	$25,$12
sw	$14,148($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_bak
1:	jalr	$25
sw	$12,144($sp)

li	$7,1			# 0x1
lw	$12,144($sp)
move	$6,$17
move	$4,$fp
move	$5,$22
move	$25,$12
jalr	$25
move	$17,$2

lw	$9,164($sp)
sll	$6,$2,16
lw	$3,160($sp)
lw	$14,148($sp)
addu	$5,$2,$9
lw	$28,32($sp)
addu	$3,$17,$3
lw	$8,156($sp)
li	$2,131072			# 0x20000
lw	$10,168($sp)
sll	$5,$5,16
lw	$11,152($sp)
addiu	$4,$2,8820
andi	$17,$17,0xffff
andi	$2,$3,0xffff
addu	$2,$2,$5
addu	$3,$17,$6
addu	$7,$16,$4
addu	$5,$16,$14
sw	$3,0($5)
sw	$3,0($7)
sw	$3,4($7)
sw	$3,8($7)
sw	$3,28($7)
sw	$3,32($7)
sw	$3,36($7)
sw	$3,40($7)
sw	$2,11296($16)
sw	$2,11300($16)
sw	$2,11304($16)
sw	$2,11308($16)
sw	$2,11328($16)
sw	$2,11332($16)
sw	$2,11336($16)
sw	$2,11340($16)
b	$L669
lw	$3,56($sp)

$L768:
addiu	$25,$16,%lo(decode_cabac_mb_ref)
move	$4,$fp
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref
1:	jalr	$25
move	$5,$18

lw	$3,0($19)
sltu	$4,$2,$3
beq	$4,$0,$L644
lw	$28,32($sp)

sll	$4,$2,8
lw	$3,56($sp)
addu	$2,$4,$2
sll	$4,$2,16
addu	$2,$2,$4
sw	$2,-16($17)
b	$L1035
sw	$2,-8($17)

$L793:
b	$L643
move	$2,$0

$L649:
lb	$2,11595($11)
bne	$3,$2,$L1068
lw	$12,%got(scan8)($28)

lh	$3,68($23)
b	$L651
lh	$9,70($23)

$L652:
li	$13,-2			# 0xfffffffffffffffe
bne	$4,$13,$L653
nop

$L764:
addiu	$2,$12,-9
$L1071:
addu	$12,$18,$2
addu	$2,$10,$2
addiu	$12,$12,2812
sll	$12,$12,2
lb	$4,11568($2)
b	$L657
addu	$2,$fp,$12

$L668:
slt	$5,$2,$9
beq	$5,$0,$L1067
lw	$15,%got(decode_cabac_mb_mvd_bak)($28)

slt	$9,$2,$4
movz	$4,$2,$9
b	$L1067
move	$9,$4

$L659:
slt	$6,$5,$3
beq	$6,$0,$L667
nop

slt	$3,$5,$4
movz	$4,$5,$3
b	$L667
move	$3,$4

$L996:
lw	$2,7996($fp)
li	$5,16			# 0x10
lw	$4,0($fp)
lw	$7,7992($fp)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC2)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L843
li	$2,-1			# 0xffffffffffffffff

$L1014:
addiu	$19,$19,8339
move	$4,$18
addu	$19,$fp,$19
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$19

bne	$2,$0,$L1036
lw	$28,32($sp)

li	$2,1			# 0x1
b	$L560
li	$5,1			# 0x1

$L658:
li	$15,1			# 0x1
beq	$12,$15,$L1037
nop

li	$3,-2			# 0xfffffffffffffffe
beq	$5,$3,$L1038
nop

$L665:
lh	$5,0($7)
lh	$3,0($9)
slt	$6,$3,$5
beq	$6,$0,$L666
lh	$4,0($2)

slt	$6,$3,$4
beq	$6,$0,$L667
nop

slt	$3,$5,$4
movz	$5,$4,$3
b	$L667
move	$3,$5

$L1004:
beq	$2,$0,$L557
addiu	$24,$fp,11288

lw	$23,64($sp)
move	$15,$0
li	$25,131072			# 0x20000
move	$10,$17
move	$19,$0
addiu	$14,$25,8816
move	$22,$15
b	$L704
move	$17,$24

$L1040:
li	$2,65536			# 0x10000
addiu	$19,$19,1
addu	$2,$fp,$2
addiu	$22,$22,2
addiu	$23,$23,40
lw	$2,6632($2)
sltu	$2,$19,$2
beq	$2,$0,$L557
addiu	$17,$11,160

$L704:
sll	$18,$19,3
sll	$4,$19,5
li	$2,12288			# 0x3000
addu	$18,$18,$4
sll	$2,$2,$22
sll	$8,$19,2
move	$20,$0
addu	$9,$fp,$18
sw	$2,64($sp)
addiu	$16,$17,-11288
sw	$8,80($sp)
move	$11,$17
move	$21,$23
move	$17,$18
$L703:
addu	$4,$22,$20
li	$2,4096			# 0x1000
sll	$2,$2,$4
and	$2,$2,$3
bne	$2,$0,$L1039
li	$4,131072			# 0x20000

addiu	$2,$4,8820
addu	$4,$16,$14
addu	$6,$16,$2
sw	$0,0($4)
sw	$0,0($6)
sw	$0,28($6)
sw	$0,32($6)
sw	$0,60($6)
sw	$0,64($6)
sw	$0,92($6)
sw	$0,96($6)
sw	$0,11296($16)
sw	$0,11300($16)
sw	$0,11328($16)
sw	$0,11332($16)
sw	$0,11360($16)
sw	$0,11364($16)
sw	$0,11392($16)
sw	$0,11396($16)
$L702:
li	$2,1			# 0x1
addiu	$16,$16,8
beq	$20,$2,$L1040
addiu	$21,$21,2

b	$L703
move	$20,$10

$L1039:
sll	$18,$20,2
bne	$18,$0,$L680
lb	$3,11580($21)

lb	$2,11579($23)
beq	$3,$2,$L1041
lw	$5,%got(scan8)($28)

$L1070:
li	$13,65536			# 0x10000
addu	$13,$fp,$13
addiu	$5,$5,%lo(scan8)
addu	$2,$5,$18
lw	$15,-5252($13)
lbu	$12,0($2)
addiu	$5,$12,-8
addiu	$4,$12,-1
addu	$8,$17,$5
addu	$7,$17,$4
addiu	$2,$12,-6
addiu	$7,$7,2812
addiu	$8,$8,2812
addu	$4,$9,$4
addu	$5,$9,$5
addu	$24,$9,$2
sll	$7,$7,2
lb	$6,11568($4)
sll	$8,$8,2
lb	$5,11568($5)
addu	$7,$fp,$7
lb	$4,11568($24)
beq	$15,$0,$L685
addu	$8,$fp,$8

li	$15,-2			# 0xfffffffffffffffe
beq	$4,$15,$L1042
nop

$L686:
addu	$2,$17,$2
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$fp,$2
$L690:
xor	$13,$3,$5
xor	$12,$3,$4
sltu	$13,$13,1
sltu	$12,$12,1
xor	$3,$3,$6
addu	$12,$12,$13
sltu	$3,$3,1
addu	$12,$12,$3
slt	$15,$12,2
bne	$15,$0,$L691
nop

lh	$4,0($7)
lh	$3,0($8)
slt	$6,$3,$4
beq	$6,$0,$L692
lh	$5,0($2)

slt	$6,$3,$5
beq	$6,$0,$L700
nop

slt	$3,$4,$5
movz	$4,$5,$3
move	$3,$4
$L700:
lh	$4,2($7)
lh	$8,2($8)
slt	$5,$8,$4
beq	$5,$0,$L701
lh	$2,2($2)

slt	$5,$8,$2
beq	$5,$0,$L1069
lw	$15,%got(decode_cabac_mb_mvd_bak)($28)

slt	$8,$4,$2
movz	$4,$2,$8
move	$8,$4
$L682:
lw	$15,%got(decode_cabac_mb_mvd_bak)($28)
$L1069:
move	$7,$0
move	$6,$18
sw	$3,160($sp)
sw	$8,156($sp)
move	$4,$fp
addiu	$12,$15,%lo(decode_cabac_mb_mvd_bak)
sw	$9,164($sp)
sw	$10,168($sp)
move	$5,$19
sw	$11,152($sp)
move	$25,$12
sw	$14,148($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_bak
1:	jalr	$25
sw	$12,144($sp)

li	$7,1			# 0x1
lw	$12,144($sp)
move	$6,$18
move	$4,$fp
move	$5,$19
move	$25,$12
jalr	$25
move	$18,$2

lw	$8,156($sp)
sll	$6,$2,16
lw	$3,160($sp)
lw	$14,148($sp)
addu	$5,$2,$8
lw	$28,32($sp)
addu	$3,$18,$3
lw	$9,164($sp)
li	$2,131072			# 0x20000
lw	$10,168($sp)
sll	$5,$5,16
lw	$11,152($sp)
addiu	$4,$2,8820
andi	$18,$18,0xffff
andi	$2,$3,0xffff
addu	$2,$2,$5
addu	$3,$18,$6
addu	$7,$16,$4
addu	$5,$16,$14
sw	$3,0($5)
sw	$3,0($7)
sw	$3,28($7)
sw	$3,32($7)
sw	$3,60($7)
sw	$3,64($7)
sw	$3,92($7)
sw	$3,96($7)
sw	$2,11296($16)
sw	$2,11300($16)
sw	$2,11328($16)
sw	$2,11332($16)
sw	$2,11360($16)
sw	$2,11364($16)
sw	$2,11392($16)
sw	$2,11396($16)
b	$L702
lw	$3,56($sp)

$L685:
li	$13,-2			# 0xfffffffffffffffe
bne	$4,$13,$L686
nop

$L765:
addiu	$2,$12,-9
$L1072:
addu	$12,$17,$2
addu	$2,$9,$2
addiu	$12,$12,2812
sll	$12,$12,2
lb	$4,11568($2)
b	$L690
addu	$2,$fp,$12

$L701:
slt	$5,$2,$8
beq	$5,$0,$L1069
lw	$15,%got(decode_cabac_mb_mvd_bak)($28)

slt	$8,$2,$4
movz	$4,$2,$8
b	$L1069
move	$8,$4

$L692:
slt	$6,$5,$3
beq	$6,$0,$L700
nop

slt	$3,$5,$4
movz	$4,$5,$3
b	$L700
move	$3,$4

$L680:
lb	$2,11576($23)
li	$4,-2			# 0xfffffffffffffffe
beq	$2,$4,$L683
nop

addiu	$4,$11,-8
$L684:
bne	$3,$2,$L1070
lw	$5,%got(scan8)($28)

lh	$3,0($4)
b	$L682
lh	$8,2($4)

$L691:
li	$15,1			# 0x1
beq	$12,$15,$L1043
nop

li	$3,-2			# 0xfffffffffffffffe
beq	$5,$3,$L1044
nop

$L698:
lh	$5,0($7)
lh	$3,0($8)
slt	$6,$3,$5
beq	$6,$0,$L699
lh	$4,0($2)

slt	$6,$3,$4
beq	$6,$0,$L700
nop

slt	$3,$5,$4
movz	$5,$4,$3
b	$L700
move	$3,$5

$L530:
li	$3,13			# 0xd
beq	$2,$3,$L1045
li	$3,14			# 0xe

beq	$2,$3,$L777
li	$3,15			# 0xf

bne	$2,$3,$L1046
move	$4,$16

b	$L528
li	$2,22			# 0x16

$L1017:
lw	$16,%got(get_cabac)($28)
li	$5,60296			# 0xeb88
lw	$22,%got(b_sub_mb_type_info)($28)
addiu	$4,$2,8309
addiu	$3,$2,8310
addiu	$18,$2,8224
addiu	$23,$2,8308
addiu	$8,$sp,40
addu	$5,$fp,$5
addiu	$2,$2,8311
li	$21,60304			# 0xeb90
sw	$8,140($sp)
addu	$4,$fp,$4
sw	$5,64($sp)
addu	$3,$fp,$3
addu	$18,$fp,$18
addu	$23,$fp,$23
sw	$4,80($sp)
addu	$21,$fp,$21
sw	$3,84($sp)
addiu	$16,$16,%lo(get_cabac)
addiu	$22,$22,%lo(b_sub_mb_type_info)
addu	$20,$fp,$2
move	$19,$8
move	$17,$5
$L567:
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$23

bne	$2,$0,$L1047
lw	$28,32($sp)

$L563:
sll	$2,$2,2
addiu	$17,$17,2
addu	$2,$22,$2
addiu	$19,$19,4
lhu	$3,0($2)
lbu	$2,2($2)
sh	$3,-2($17)
bne	$17,$21,$L567
sw	$2,-4($19)

li	$4,65536			# 0x10000
addu	$4,$fp,$4
lhu	$3,-5238($4)
lhu	$5,-5240($4)
lhu	$2,-5236($4)
lhu	$4,-5234($4)
or	$3,$3,$5
or	$2,$3,$2
or	$2,$2,$4
andi	$2,$2,0x100
beq	$2,$0,$L1055
li	$3,65536			# 0x10000

lw	$25,%call16(pred_direct_motion_hw)($28)
addiu	$5,$sp,56
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$fp

li	$3,-2			# 0xfffffffffffffffe
lw	$28,32($sp)
li	$6,131072			# 0x20000
lw	$5,64($sp)
addiu	$6,$6,9088
sb	$3,11638($fp)
sb	$3,11598($fp)
sb	$3,11622($fp)
sb	$3,11582($fp)
lw	$2,%got(scan8)($28)
addiu	$4,$2,%lo(scan8)
$L571:
lhu	$2,0($5)
addiu	$5,$5,2
lbu	$3,0($4)
addiu	$4,$4,4
srl	$2,$2,1
addu	$3,$3,$6
andi	$2,$2,0xff
sll	$7,$2,8
addu	$3,$fp,$3
addu	$2,$2,$7
andi	$2,$2,0xffff
sh	$2,4($3)
bne	$21,$5,$L571
sh	$2,12($3)

b	$L1055
li	$3,65536			# 0x10000

$L1047:
lw	$5,80($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$18

move	$4,$18
beq	$2,$0,$L1048
lw	$5,84($sp)

move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
nop

move	$4,$18
beq	$2,$0,$L565
move	$5,$20

move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
nop

bne	$2,$0,$L1049
li	$3,7			# 0x7

$L566:
move	$4,$18
sw	$3,160($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$20

move	$4,$18
lw	$3,160($sp)
sll	$2,$2,1
move	$5,$20
move	$25,$16
addu	$3,$2,$3
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sw	$3,160($sp)

lw	$3,160($sp)
lw	$28,32($sp)
addu	$2,$3,$2
b	$L563
andi	$2,$2,0xffff

$L1041:
lh	$3,4($11)
b	$L682
lh	$8,6($11)

$L998:
addiu	$3,$3,52
b	$L743
sw	$3,2872($fp)

$L1026:
lw	$5,10800($17)
andi	$5,$5,0x80
beq	$5,$0,$L1065
addiu	$5,$16,-9

lw	$5,7996($17)
sra	$18,$16,5
lw	$11,168($17)
lw	$8,10780($17)
andi	$5,$5,0x1
lw	$13,128($sp)
sll	$5,$5,1
addu	$8,$11,$8
addu	$18,$5,$18
sra	$5,$18,2
mul	$12,$11,$5
addu	$5,$12,$8
sll	$5,$5,2
addu	$5,$31,$5
lw	$5,0($5)
and	$5,$13,$5
beq	$5,$0,$L791
lw	$15,64($sp)

sll	$11,$8,2
lw	$5,11852($17)
and	$8,$18,$7
lw	$13,2696($17)
lw	$12,11864($17)
sll	$31,$15,2
addu	$5,$5,$11
addu	$31,$13,$31
mul	$13,$18,$12
lw	$5,0($5)
lw	$7,96($31)
addiu	$5,$5,3
addu	$5,$13,$5
sll	$5,$5,2
addu	$5,$7,$5
lh	$7,0($5)
sh	$7,0($3)
lhu	$5,2($5)
sll	$5,$5,1
sh	$5,2($3)
lw	$5,188($31)
addu	$5,$5,$11
addu	$5,$5,$8
lb	$7,1($5)
b	$L596
sra	$7,$7,1

$L1034:
slt	$2,$12,20
bne	$2,$0,$L1071
addiu	$2,$12,-9

li	$2,4			# 0x4
andi	$15,$12,0x7
bne	$15,$2,$L1071
addiu	$2,$12,-9

lb	$2,11579($11)
beq	$2,$4,$L764
nop

lw	$15,2696($fp)
lw	$24,104($15)
sw	$0,0($23)
lw	$13,-5248($13)
bne	$13,$0,$L655
move	$2,$23

lw	$13,10800($fp)
andi	$13,$13,0x80
beq	$13,$0,$L764
nop

lw	$31,7996($fp)
sra	$12,$12,5
lw	$25,168($fp)
lw	$13,10780($fp)
andi	$31,$31,0x1
sll	$31,$31,1
addu	$13,$25,$13
addu	$12,$31,$12
sra	$31,$12,2
mul	$15,$25,$31
addu	$25,$15,$13
sll	$15,$25,2
addu	$24,$24,$15
lw	$15,0($24)
and	$15,$8,$15
beq	$15,$0,$L795
sll	$13,$13,2

lw	$24,11852($fp)
lw	$15,2696($fp)
and	$4,$12,$4
lw	$25,11864($fp)
lw	$31,64($sp)
addu	$24,$24,$13
addu	$15,$15,$31
lw	$24,0($24)
mul	$31,$12,$25
addiu	$24,$24,3
addu	$12,$31,$24
lw	$24,96($15)
sll	$12,$12,2
addu	$12,$24,$12
lh	$24,0($12)
sh	$24,0($23)
lhu	$12,2($12)
sll	$12,$12,1
sh	$12,2($23)
lw	$12,188($15)
addu	$13,$12,$13
addu	$4,$13,$4
lb	$4,1($4)
b	$L657
sra	$4,$4,1

$L1029:
bne	$3,$0,$L940
nop

bne	$5,$0,$L1050
nop

lh	$8,0($6)
b	$L630
lh	$3,2($6)

$L683:
lb	$2,11573($23)
b	$L684
addiu	$4,$11,-20

$L1042:
slt	$2,$12,20
bne	$2,$0,$L1072
addiu	$2,$12,-9

li	$2,4			# 0x4
andi	$15,$12,0x7
bne	$15,$2,$L1072
addiu	$2,$12,-9

lb	$2,11579($23)
beq	$2,$4,$L765
nop

lw	$15,2696($fp)
lw	$24,104($15)
sw	$0,0($11)
lw	$13,-5248($13)
bne	$13,$0,$L688
move	$2,$11

lw	$13,10800($fp)
andi	$13,$13,0x80
beq	$13,$0,$L765
nop

lw	$31,7996($fp)
sra	$12,$12,5
lw	$25,168($fp)
lw	$13,10780($fp)
andi	$31,$31,0x1
sll	$31,$31,1
addu	$13,$25,$13
addu	$12,$31,$12
sra	$31,$12,2
mul	$15,$25,$31
addu	$25,$15,$13
sll	$15,$25,2
addu	$24,$24,$15
lw	$15,0($24)
lw	$24,64($sp)
and	$15,$24,$15
beq	$15,$0,$L800
sll	$13,$13,2

lw	$24,11852($fp)
lw	$15,2696($fp)
and	$4,$12,$4
lw	$25,11864($fp)
lw	$31,80($sp)
addu	$24,$24,$13
addu	$15,$15,$31
lw	$24,0($24)
mul	$31,$12,$25
addiu	$24,$24,3
addu	$12,$31,$24
lw	$24,96($15)
sll	$12,$12,2
addu	$12,$24,$12
lh	$24,0($12)
sh	$24,0($11)
lhu	$12,2($12)
sll	$12,$12,1
sh	$12,2($11)
lw	$12,188($15)
addu	$13,$12,$13
addu	$4,$13,$4
lb	$4,1($4)
b	$L690
sra	$4,$4,1

$L1033:
lh	$3,-24($23)
b	$L651
lh	$9,-22($23)

$L1036:
move	$4,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$19

li	$4,3			# 0x3
li	$3,2			# 0x2
lw	$28,32($sp)
movz	$4,$3,$2
move	$2,$4
b	$L560
move	$5,$4

$L1048:
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$20

addiu	$2,$2,1
lw	$28,32($sp)
b	$L563
andi	$2,$2,0xffff

$L565:
b	$L566
li	$3,3			# 0x3

$L666:
slt	$6,$4,$3
beq	$6,$0,$L667
nop

slt	$3,$4,$5
movz	$5,$4,$3
b	$L667
move	$3,$5

$L699:
slt	$6,$4,$3
beq	$6,$0,$L700
nop

slt	$3,$4,$5
movz	$5,$4,$3
b	$L700
move	$3,$5

$L674:
lw	$6,%got($LC1)($28)
li	$5,16			# 0x10
lw	$4,0($fp)
move	$7,$2
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC1)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$10,16($sp)

b	$L843
li	$2,-1			# 0xffffffffffffffff

$L1020:
lw	$3,-5236($4)
li	$4,7340032			# 0x700000
addiu	$4,$4,112
or	$3,$3,$5
and	$3,$3,$4
sltu	$3,$3,1
b	$L582
sw	$3,96($sp)

$L1037:
bne	$3,$0,$L943
nop

bne	$13,$0,$L1051
nop

lh	$3,0($2)
b	$L651
lh	$9,2($2)

$L1043:
bne	$3,$0,$L946
nop

bne	$13,$0,$L1052
nop

lh	$3,0($2)
b	$L682
lh	$8,2($2)

$L1046:
sll	$18,$2,1
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$19

addu	$2,$18,$2
lw	$28,32($sp)
b	$L528
addiu	$2,$2,-4

$L1038:
bne	$4,$5,$L665
nop

beq	$6,$4,$L665
nop

$L943:
lh	$3,0($7)
b	$L651
lh	$9,2($7)

$L1044:
bne	$4,$5,$L698
nop

beq	$6,$4,$L698
nop

$L946:
lh	$3,0($7)
b	$L682
lh	$8,2($7)

$L777:
b	$L528
li	$2,11			# 0xb

$L1045:
b	$L934
li	$5,32			# 0x20

$L1050:
lh	$8,11264($16)
b	$L630
lh	$3,11266($16)

$L1019:
lw	$9,96($sp)
bne	$9,$0,$L772
lw	$3,56($sp)

b	$L1073
andi	$2,$3,0x78

$L1052:
lh	$3,0($8)
b	$L682
lh	$8,2($8)

$L1049:
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$20

addiu	$2,$2,11
lw	$28,32($sp)
b	$L563
andi	$2,$2,0xffff

$L1051:
lh	$3,0($9)
b	$L651
lh	$9,2($9)

$L791:
b	$L596
li	$7,-1			# 0xffffffffffffffff

$L795:
b	$L657
li	$4,-1			# 0xffffffffffffffff

$L655:
lw	$4,10800($fp)
andi	$4,$4,0x80
bne	$4,$0,$L764
nop

slt	$4,$12,36
xori	$4,$4,0x1
addiu	$4,$4,2694
sra	$12,$12,2
sll	$4,$4,2
addu	$4,$fp,$4
lw	$15,4($4)
sll	$15,$15,2
addu	$24,$24,$15
lw	$4,0($24)
and	$4,$8,$4
beq	$4,$0,$L795
andi	$13,$12,0x3

lw	$4,11864($fp)
andi	$12,$12,0x2
lw	$31,11852($fp)
lw	$24,2696($fp)
mul	$4,$13,$4
lw	$13,64($sp)
addu	$31,$31,$15
addu	$24,$24,$13
lw	$13,0($31)
lw	$31,96($24)
addiu	$4,$4,3
addu	$4,$4,$13
sll	$4,$4,2
addu	$4,$31,$4
lh	$13,0($4)
sh	$13,0($23)
lh	$13,2($4)
srl	$4,$13,31
addu	$4,$4,$13
sra	$4,$4,1
sh	$4,2($23)
lw	$4,188($24)
addu	$15,$4,$15
addu	$12,$15,$12
lb	$4,1($12)
b	$L657
sll	$4,$4,1

$L800:
b	$L690
li	$4,-1			# 0xffffffffffffffff

$L688:
lw	$4,10800($fp)
andi	$4,$4,0x80
bne	$4,$0,$L765
lw	$25,64($sp)

slt	$4,$12,36
xori	$4,$4,0x1
addiu	$4,$4,2694
sra	$12,$12,2
sll	$4,$4,2
addu	$4,$fp,$4
lw	$15,4($4)
sll	$15,$15,2
addu	$24,$24,$15
lw	$4,0($24)
and	$4,$25,$4
beq	$4,$0,$L800
andi	$13,$12,0x3

lw	$4,11864($fp)
andi	$12,$12,0x2
lw	$31,11852($fp)
lw	$24,2696($fp)
mul	$4,$13,$4
lw	$13,80($sp)
addu	$31,$31,$15
addu	$24,$24,$13
lw	$13,0($31)
lw	$31,96($24)
addiu	$4,$4,3
addu	$4,$4,$13
sll	$4,$4,2
addu	$4,$31,$4
lh	$13,0($4)
sh	$13,0($11)
lh	$13,2($4)
srl	$4,$13,31
addu	$4,$4,$13
sra	$4,$4,1
sh	$4,2($11)
lw	$4,188($24)
addu	$15,$4,$15
addu	$12,$15,$12
lb	$4,1($12)
b	$L690
sll	$4,$4,1

.set	macro
.set	reorder
.end	ff_h264_decode_mb_cabac
.size	ff_h264_decode_mb_cabac, .-ff_h264_decode_mb_cabac
.rdata
.align	2
.type	coeff_abs_levelgt1_ctx.7769, @object
.size	coeff_abs_levelgt1_ctx.7769, 8
coeff_abs_levelgt1_ctx.7769:
.byte	5
.byte	5
.byte	5
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.align	2
.type	coeff_abs_level_transition.7770, @object
.size	coeff_abs_level_transition.7770, 16
coeff_abs_level_transition.7770:
.byte	1
.byte	2
.byte	3
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	6
.byte	7
.byte	7
.align	2
.type	coeff_abs_level1_ctx.7768, @object
.size	coeff_abs_level1_ctx.7768, 8
coeff_abs_level1_ctx.7768:
.byte	1
.byte	2
.byte	3
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	significant_coeff_flag_offset_8x8.7767, @object
.size	significant_coeff_flag_offset_8x8.7767, 126
significant_coeff_flag_offset_8x8.7767:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	5
.byte	4
.byte	4
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	4
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	6
.byte	7
.byte	7
.byte	7
.byte	8
.byte	9
.byte	10
.byte	9
.byte	8
.byte	7
.byte	7
.byte	6
.byte	11
.byte	12
.byte	13
.byte	11
.byte	6
.byte	7
.byte	8
.byte	9
.byte	14
.byte	10
.byte	9
.byte	8
.byte	6
.byte	11
.byte	12
.byte	13
.byte	11
.byte	6
.byte	9
.byte	14
.byte	10
.byte	9
.byte	11
.byte	12
.byte	13
.byte	11
.byte	14
.byte	10
.byte	12
.byte	0
.byte	1
.byte	1
.byte	2
.byte	2
.byte	3
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	7
.byte	7
.byte	8
.byte	4
.byte	5
.byte	6
.byte	9
.byte	10
.byte	10
.byte	8
.byte	11
.byte	12
.byte	11
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	11
.byte	12
.byte	11
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	11
.byte	12
.byte	11
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	13
.byte	13
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	13
.byte	13
.byte	9
.byte	9
.byte	10
.byte	10
.byte	14
.byte	14
.byte	14
.byte	14
.byte	14
.align	2
.type	coeff_abs_level_m1_offset.7766, @object
.size	coeff_abs_level_m1_offset.7766, 24
coeff_abs_level_m1_offset.7766:
.word	227
.word	237
.word	247
.word	257
.word	266
.word	426
.align	2
.type	last_coeff_flag_offset.7765, @object
.size	last_coeff_flag_offset.7765, 48
last_coeff_flag_offset.7765:
.word	166
.word	181
.word	195
.word	210
.word	213
.word	417
.word	338
.word	353
.word	367
.word	382
.word	385
.word	451
.align	2
.type	significant_coeff_flag_offset.7764, @object
.size	significant_coeff_flag_offset.7764, 48
significant_coeff_flag_offset.7764:
.word	105
.word	120
.word	134
.word	149
.word	152
.word	402
.word	277
.word	292
.word	306
.word	321
.word	324
.word	436
.align	2
.type	left_block_options.7269, @object
.size	left_block_options.7269, 64
left_block_options.7269:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	7
.byte	10
.byte	8
.byte	11
.byte	7
.byte	15
.byte	23
.byte	31
.byte	2
.byte	26
.byte	10
.byte	18
.byte	2
.byte	2
.byte	3
.byte	3
.byte	8
.byte	11
.byte	8
.byte	11
.byte	23
.byte	23
.byte	31
.byte	31
.byte	10
.byte	18
.byte	10
.byte	18
.byte	0
.byte	0
.byte	1
.byte	1
.byte	7
.byte	10
.byte	7
.byte	10
.byte	7
.byte	7
.byte	15
.byte	15
.byte	2
.byte	26
.byte	2
.byte	26
.byte	0
.byte	2
.byte	0
.byte	2
.byte	7
.byte	10
.byte	7
.byte	10
.byte	7
.byte	23
.byte	7
.byte	23
.byte	2
.byte	26
.byte	2
.byte	26
.type	last_coeff_flag_offset_8x8, @object
.size	last_coeff_flag_offset_8x8, 63
last_coeff_flag_offset_8x8:
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	5
.byte	5
.byte	6
.byte	6
.byte	6
.byte	6
.byte	7
.byte	7
.byte	7
.byte	7
.byte	8
.byte	8
.byte	8
.align	2
.type	cabac_context_init_PB, @object
.size	cabac_context_init_PB, 2760
cabac_context_init_PB:
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	-28
.byte	127
.byte	-23
.byte	104
.byte	-6
.byte	53
.byte	-1
.byte	54
.byte	7
.byte	51
.byte	23
.byte	33
.byte	23
.byte	2
.byte	21
.byte	0
.byte	1
.byte	9
.byte	0
.byte	49
.byte	-37
.byte	118
.byte	5
.byte	57
.byte	-13
.byte	78
.byte	-11
.byte	65
.byte	1
.byte	62
.byte	12
.byte	49
.byte	-4
.byte	73
.byte	17
.byte	50
.byte	18
.byte	64
.byte	9
.byte	43
.byte	29
.byte	0
.byte	26
.byte	67
.byte	16
.byte	90
.byte	9
.byte	104
.byte	-46
.byte	127
.byte	-20
.byte	104
.byte	1
.byte	67
.byte	-13
.byte	78
.byte	-11
.byte	65
.byte	1
.byte	62
.byte	-6
.byte	86
.byte	-17
.byte	95
.byte	-6
.byte	61
.byte	9
.byte	45
.byte	-3
.byte	69
.byte	-6
.byte	81
.byte	-11
.byte	96
.byte	6
.byte	55
.byte	7
.byte	67
.byte	-5
.byte	86
.byte	2
.byte	88
.byte	0
.byte	58
.byte	-3
.byte	76
.byte	-10
.byte	94
.byte	5
.byte	54
.byte	4
.byte	69
.byte	-3
.byte	81
.byte	0
.byte	88
.byte	-7
.byte	67
.byte	-5
.byte	74
.byte	-4
.byte	74
.byte	-5
.byte	80
.byte	-7
.byte	72
.byte	1
.byte	58
.byte	0
.byte	41
.byte	0
.byte	63
.byte	0
.byte	63
.byte	0
.byte	63
.byte	-9
.byte	83
.byte	4
.byte	86
.byte	0
.byte	97
.byte	-7
.byte	72
.byte	13
.byte	41
.byte	3
.byte	62
.byte	0
.byte	45
.byte	-4
.byte	78
.byte	-3
.byte	96
.byte	-27
.byte	126
.byte	-28
.byte	98
.byte	-25
.byte	101
.byte	-23
.byte	67
.byte	-28
.byte	82
.byte	-20
.byte	94
.byte	-16
.byte	83
.byte	-22
.byte	110
.byte	-21
.byte	91
.byte	-18
.byte	102
.byte	-13
.byte	93
.byte	-29
.byte	127
.byte	-7
.byte	92
.byte	-5
.byte	89
.byte	-7
.byte	96
.byte	-13
.byte	108
.byte	-3
.byte	46
.byte	-1
.byte	65
.byte	-1
.byte	57
.byte	-9
.byte	93
.byte	-3
.byte	74
.byte	-9
.byte	92
.byte	-8
.byte	87
.byte	-23
.byte	126
.byte	5
.byte	54
.byte	6
.byte	60
.byte	6
.byte	59
.byte	6
.byte	69
.byte	-1
.byte	48
.byte	0
.byte	68
.byte	-4
.byte	69
.byte	-8
.byte	88
.byte	-2
.byte	85
.byte	-6
.byte	78
.byte	-1
.byte	75
.byte	-7
.byte	77
.byte	2
.byte	54
.byte	5
.byte	50
.byte	-3
.byte	68
.byte	1
.byte	50
.byte	6
.byte	42
.byte	-4
.byte	81
.byte	1
.byte	63
.byte	-4
.byte	70
.byte	0
.byte	67
.byte	2
.byte	57
.byte	-2
.byte	76
.byte	11
.byte	35
.byte	4
.byte	64
.byte	1
.byte	61
.byte	11
.byte	35
.byte	18
.byte	25
.byte	12
.byte	24
.byte	13
.byte	29
.byte	13
.byte	36
.byte	-10
.byte	93
.byte	-7
.byte	73
.byte	-2
.byte	73
.byte	13
.byte	46
.byte	9
.byte	49
.byte	-7
.byte	100
.byte	9
.byte	53
.byte	2
.byte	53
.byte	5
.byte	53
.byte	-2
.byte	61
.byte	0
.byte	56
.byte	0
.byte	56
.byte	-13
.byte	63
.byte	-5
.byte	60
.byte	-1
.byte	62
.byte	4
.byte	57
.byte	-6
.byte	69
.byte	4
.byte	57
.byte	14
.byte	39
.byte	4
.byte	51
.byte	13
.byte	68
.byte	3
.byte	64
.byte	1
.byte	61
.byte	9
.byte	63
.byte	7
.byte	50
.byte	16
.byte	39
.byte	5
.byte	44
.byte	4
.byte	52
.byte	11
.byte	48
.byte	-5
.byte	60
.byte	-1
.byte	59
.byte	0
.byte	59
.byte	22
.byte	33
.byte	5
.byte	44
.byte	14
.byte	43
.byte	-1
.byte	78
.byte	0
.byte	60
.byte	9
.byte	69
.byte	11
.byte	28
.byte	2
.byte	40
.byte	3
.byte	44
.byte	0
.byte	49
.byte	0
.byte	46
.byte	2
.byte	44
.byte	2
.byte	51
.byte	0
.byte	47
.byte	4
.byte	39
.byte	2
.byte	62
.byte	6
.byte	46
.byte	0
.byte	54
.byte	3
.byte	54
.byte	2
.byte	58
.byte	4
.byte	63
.byte	6
.byte	51
.byte	6
.byte	57
.byte	7
.byte	53
.byte	6
.byte	52
.byte	6
.byte	55
.byte	11
.byte	45
.byte	14
.byte	36
.byte	8
.byte	53
.byte	-1
.byte	82
.byte	7
.byte	55
.byte	-3
.byte	78
.byte	15
.byte	46
.byte	22
.byte	31
.byte	-1
.byte	84
.byte	25
.byte	7
.byte	30
.byte	-7
.byte	28
.byte	3
.byte	28
.byte	4
.byte	32
.byte	0
.byte	34
.byte	-1
.byte	30
.byte	6
.byte	30
.byte	6
.byte	32
.byte	9
.byte	31
.byte	19
.byte	26
.byte	27
.byte	26
.byte	30
.byte	37
.byte	20
.byte	28
.byte	34
.byte	17
.byte	70
.byte	1
.byte	67
.byte	5
.byte	59
.byte	9
.byte	67
.byte	16
.byte	30
.byte	18
.byte	32
.byte	18
.byte	35
.byte	22
.byte	29
.byte	24
.byte	31
.byte	23
.byte	38
.byte	18
.byte	43
.byte	20
.byte	41
.byte	11
.byte	63
.byte	9
.byte	59
.byte	9
.byte	64
.byte	-1
.byte	94
.byte	-2
.byte	89
.byte	-9
.byte	108
.byte	-6
.byte	76
.byte	-2
.byte	44
.byte	0
.byte	45
.byte	0
.byte	52
.byte	-3
.byte	64
.byte	-2
.byte	59
.byte	-4
.byte	70
.byte	-4
.byte	75
.byte	-8
.byte	82
.byte	-17
.byte	102
.byte	-9
.byte	77
.byte	3
.byte	24
.byte	0
.byte	42
.byte	0
.byte	48
.byte	0
.byte	55
.byte	-6
.byte	59
.byte	-7
.byte	71
.byte	-12
.byte	83
.byte	-11
.byte	87
.byte	-30
.byte	119
.byte	1
.byte	58
.byte	-3
.byte	29
.byte	-1
.byte	36
.byte	1
.byte	38
.byte	2
.byte	43
.byte	-6
.byte	55
.byte	0
.byte	58
.byte	0
.byte	64
.byte	-3
.byte	74
.byte	-10
.byte	90
.byte	0
.byte	70
.byte	-4
.byte	29
.byte	5
.byte	31
.byte	7
.byte	42
.byte	1
.byte	59
.byte	-2
.byte	58
.byte	-3
.byte	72
.byte	-3
.byte	81
.byte	-11
.byte	97
.byte	0
.byte	58
.byte	8
.byte	5
.byte	10
.byte	14
.byte	14
.byte	18
.byte	13
.byte	27
.byte	2
.byte	40
.byte	0
.byte	58
.byte	-3
.byte	70
.byte	-6
.byte	79
.byte	-8
.byte	85
.byte	0
.byte	0
.byte	-13
.byte	106
.byte	-16
.byte	106
.byte	-10
.byte	87
.byte	-21
.byte	114
.byte	-18
.byte	110
.byte	-14
.byte	98
.byte	-22
.byte	110
.byte	-21
.byte	106
.byte	-18
.byte	103
.byte	-21
.byte	107
.byte	-23
.byte	108
.byte	-26
.byte	112
.byte	-10
.byte	96
.byte	-12
.byte	95
.byte	-5
.byte	91
.byte	-9
.byte	93
.byte	-22
.byte	94
.byte	-5
.byte	86
.byte	9
.byte	67
.byte	-4
.byte	80
.byte	-10
.byte	85
.byte	-1
.byte	70
.byte	7
.byte	60
.byte	9
.byte	58
.byte	5
.byte	61
.byte	12
.byte	50
.byte	15
.byte	50
.byte	18
.byte	49
.byte	17
.byte	54
.byte	10
.byte	41
.byte	7
.byte	46
.byte	-1
.byte	51
.byte	7
.byte	49
.byte	8
.byte	52
.byte	9
.byte	41
.byte	6
.byte	47
.byte	2
.byte	55
.byte	13
.byte	41
.byte	10
.byte	44
.byte	6
.byte	50
.byte	5
.byte	53
.byte	13
.byte	49
.byte	4
.byte	63
.byte	6
.byte	64
.byte	-2
.byte	69
.byte	-2
.byte	59
.byte	6
.byte	70
.byte	10
.byte	44
.byte	9
.byte	31
.byte	12
.byte	43
.byte	3
.byte	53
.byte	14
.byte	34
.byte	10
.byte	38
.byte	-3
.byte	52
.byte	13
.byte	40
.byte	17
.byte	32
.byte	7
.byte	44
.byte	7
.byte	38
.byte	13
.byte	50
.byte	10
.byte	57
.byte	26
.byte	43
.byte	14
.byte	11
.byte	11
.byte	14
.byte	9
.byte	11
.byte	18
.byte	11
.byte	21
.byte	9
.byte	23
.byte	-2
.byte	32
.byte	-15
.byte	32
.byte	-15
.byte	34
.byte	-21
.byte	39
.byte	-23
.byte	42
.byte	-33
.byte	41
.byte	-31
.byte	46
.byte	-28
.byte	38
.byte	-12
.byte	21
.byte	29
.byte	45
.byte	-24
.byte	53
.byte	-45
.byte	48
.byte	-26
.byte	65
.byte	-43
.byte	43
.byte	-19
.byte	39
.byte	-10
.byte	30
.byte	9
.byte	18
.byte	26
.byte	20
.byte	27
.byte	0
.byte	57
.byte	-14
.byte	82
.byte	-5
.byte	75
.byte	-19
.byte	97
.byte	-35
.byte	125
.byte	27
.byte	0
.byte	28
.byte	0
.byte	31
.byte	-4
.byte	27
.byte	6
.byte	34
.byte	8
.byte	30
.byte	10
.byte	24
.byte	22
.byte	33
.byte	19
.byte	22
.byte	32
.byte	26
.byte	31
.byte	21
.byte	41
.byte	26
.byte	44
.byte	23
.byte	47
.byte	16
.byte	65
.byte	14
.byte	71
.byte	8
.byte	60
.byte	6
.byte	63
.byte	17
.byte	65
.byte	21
.byte	24
.byte	23
.byte	20
.byte	26
.byte	23
.byte	27
.byte	32
.byte	28
.byte	23
.byte	28
.byte	24
.byte	23
.byte	40
.byte	24
.byte	32
.byte	28
.byte	29
.byte	23
.byte	42
.byte	19
.byte	57
.byte	22
.byte	53
.byte	22
.byte	61
.byte	11
.byte	86
.byte	12
.byte	40
.byte	11
.byte	51
.byte	14
.byte	59
.byte	-4
.byte	79
.byte	-7
.byte	71
.byte	-5
.byte	69
.byte	-9
.byte	70
.byte	-8
.byte	66
.byte	-10
.byte	68
.byte	-19
.byte	73
.byte	-12
.byte	69
.byte	-16
.byte	70
.byte	-15
.byte	67
.byte	-20
.byte	62
.byte	-19
.byte	70
.byte	-16
.byte	66
.byte	-22
.byte	65
.byte	-20
.byte	63
.byte	9
.byte	-2
.byte	26
.byte	-9
.byte	33
.byte	-9
.byte	39
.byte	-7
.byte	41
.byte	-2
.byte	45
.byte	3
.byte	49
.byte	9
.byte	45
.byte	27
.byte	36
.byte	59
.byte	-6
.byte	66
.byte	-7
.byte	35
.byte	-7
.byte	42
.byte	-8
.byte	45
.byte	-5
.byte	48
.byte	-12
.byte	56
.byte	-6
.byte	60
.byte	-5
.byte	62
.byte	-8
.byte	66
.byte	-8
.byte	76
.byte	-5
.byte	85
.byte	-6
.byte	81
.byte	-10
.byte	77
.byte	-7
.byte	81
.byte	-17
.byte	80
.byte	-18
.byte	73
.byte	-4
.byte	74
.byte	-10
.byte	83
.byte	-9
.byte	71
.byte	-9
.byte	67
.byte	-1
.byte	61
.byte	-8
.byte	66
.byte	-14
.byte	66
.byte	0
.byte	59
.byte	2
.byte	59
.byte	21
.byte	-13
.byte	33
.byte	-14
.byte	39
.byte	-7
.byte	46
.byte	-2
.byte	51
.byte	2
.byte	60
.byte	6
.byte	61
.byte	17
.byte	55
.byte	34
.byte	42
.byte	62
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	-28
.byte	127
.byte	-23
.byte	104
.byte	-6
.byte	53
.byte	-1
.byte	54
.byte	7
.byte	51
.byte	22
.byte	25
.byte	34
.byte	0
.byte	16
.byte	0
.byte	-2
.byte	9
.byte	4
.byte	41
.byte	-29
.byte	118
.byte	2
.byte	65
.byte	-6
.byte	71
.byte	-13
.byte	79
.byte	5
.byte	52
.byte	9
.byte	50
.byte	-3
.byte	70
.byte	10
.byte	54
.byte	26
.byte	34
.byte	19
.byte	22
.byte	40
.byte	0
.byte	57
.byte	2
.byte	41
.byte	36
.byte	26
.byte	69
.byte	-45
.byte	127
.byte	-15
.byte	101
.byte	-4
.byte	76
.byte	-6
.byte	71
.byte	-13
.byte	79
.byte	5
.byte	52
.byte	6
.byte	69
.byte	-13
.byte	90
.byte	0
.byte	52
.byte	8
.byte	43
.byte	-2
.byte	69
.byte	-5
.byte	82
.byte	-10
.byte	96
.byte	2
.byte	59
.byte	2
.byte	75
.byte	-3
.byte	87
.byte	-3
.byte	100
.byte	1
.byte	56
.byte	-3
.byte	74
.byte	-6
.byte	85
.byte	0
.byte	59
.byte	-3
.byte	81
.byte	-7
.byte	86
.byte	-5
.byte	95
.byte	-1
.byte	66
.byte	-1
.byte	77
.byte	1
.byte	70
.byte	-2
.byte	86
.byte	-5
.byte	72
.byte	0
.byte	61
.byte	0
.byte	41
.byte	0
.byte	63
.byte	0
.byte	63
.byte	0
.byte	63
.byte	-9
.byte	83
.byte	4
.byte	86
.byte	0
.byte	97
.byte	-7
.byte	72
.byte	13
.byte	41
.byte	3
.byte	62
.byte	13
.byte	15
.byte	7
.byte	51
.byte	2
.byte	80
.byte	-39
.byte	127
.byte	-18
.byte	91
.byte	-17
.byte	96
.byte	-26
.byte	81
.byte	-35
.byte	98
.byte	-24
.byte	102
.byte	-23
.byte	97
.byte	-27
.byte	119
.byte	-24
.byte	99
.byte	-21
.byte	110
.byte	-18
.byte	102
.byte	-36
.byte	127
.byte	0
.byte	80
.byte	-5
.byte	89
.byte	-7
.byte	94
.byte	-4
.byte	92
.byte	0
.byte	39
.byte	0
.byte	65
.byte	-15
.byte	84
.byte	-35
.byte	127
.byte	-2
.byte	73
.byte	-12
.byte	104
.byte	-9
.byte	91
.byte	-31
.byte	127
.byte	3
.byte	55
.byte	7
.byte	56
.byte	7
.byte	55
.byte	8
.byte	61
.byte	-3
.byte	53
.byte	0
.byte	68
.byte	-7
.byte	74
.byte	-9
.byte	88
.byte	-13
.byte	103
.byte	-13
.byte	91
.byte	-9
.byte	89
.byte	-14
.byte	92
.byte	-8
.byte	76
.byte	-12
.byte	87
.byte	-23
.byte	110
.byte	-24
.byte	105
.byte	-10
.byte	78
.byte	-20
.byte	112
.byte	-17
.byte	99
.byte	-78
.byte	127
.byte	-70
.byte	127
.byte	-50
.byte	127
.byte	-46
.byte	127
.byte	-4
.byte	66
.byte	-5
.byte	78
.byte	-4
.byte	71
.byte	-8
.byte	72
.byte	2
.byte	59
.byte	-1
.byte	55
.byte	-7
.byte	70
.byte	-6
.byte	75
.byte	-8
.byte	89
.byte	-34
.byte	119
.byte	-3
.byte	75
.byte	32
.byte	20
.byte	30
.byte	22
.byte	-44
.byte	127
.byte	0
.byte	54
.byte	-5
.byte	61
.byte	0
.byte	58
.byte	-1
.byte	60
.byte	-3
.byte	61
.byte	-8
.byte	67
.byte	-25
.byte	84
.byte	-14
.byte	74
.byte	-5
.byte	65
.byte	5
.byte	52
.byte	2
.byte	57
.byte	0
.byte	61
.byte	-9
.byte	69
.byte	-11
.byte	70
.byte	18
.byte	55
.byte	-4
.byte	71
.byte	0
.byte	58
.byte	7
.byte	61
.byte	9
.byte	41
.byte	18
.byte	25
.byte	9
.byte	32
.byte	5
.byte	43
.byte	9
.byte	47
.byte	0
.byte	44
.byte	0
.byte	51
.byte	2
.byte	46
.byte	19
.byte	38
.byte	-4
.byte	66
.byte	15
.byte	38
.byte	12
.byte	42
.byte	9
.byte	34
.byte	0
.byte	89
.byte	4
.byte	45
.byte	10
.byte	28
.byte	10
.byte	31
.byte	33
.byte	-11
.byte	52
.byte	-43
.byte	18
.byte	15
.byte	28
.byte	0
.byte	35
.byte	-22
.byte	38
.byte	-25
.byte	34
.byte	0
.byte	39
.byte	-18
.byte	32
.byte	-12
.byte	102
.byte	-94
.byte	0
.byte	0
.byte	56
.byte	-15
.byte	33
.byte	-4
.byte	29
.byte	10
.byte	37
.byte	-5
.byte	51
.byte	-29
.byte	39
.byte	-9
.byte	52
.byte	-34
.byte	69
.byte	-58
.byte	67
.byte	-63
.byte	44
.byte	-5
.byte	32
.byte	7
.byte	55
.byte	-29
.byte	32
.byte	1
.byte	0
.byte	0
.byte	27
.byte	36
.byte	33
.byte	-25
.byte	34
.byte	-30
.byte	36
.byte	-28
.byte	38
.byte	-28
.byte	38
.byte	-27
.byte	34
.byte	-18
.byte	35
.byte	-16
.byte	34
.byte	-14
.byte	32
.byte	-8
.byte	37
.byte	-6
.byte	35
.byte	0
.byte	30
.byte	10
.byte	28
.byte	18
.byte	26
.byte	25
.byte	29
.byte	41
.byte	0
.byte	75
.byte	2
.byte	72
.byte	8
.byte	77
.byte	14
.byte	35
.byte	18
.byte	31
.byte	17
.byte	35
.byte	21
.byte	30
.byte	17
.byte	45
.byte	20
.byte	42
.byte	18
.byte	45
.byte	27
.byte	26
.byte	16
.byte	54
.byte	7
.byte	66
.byte	16
.byte	56
.byte	11
.byte	73
.byte	10
.byte	67
.byte	-10
.byte	116
.byte	-23
.byte	112
.byte	-15
.byte	71
.byte	-7
.byte	61
.byte	0
.byte	53
.byte	-5
.byte	66
.byte	-11
.byte	77
.byte	-9
.byte	80
.byte	-9
.byte	84
.byte	-10
.byte	87
.byte	-34
.byte	127
.byte	-21
.byte	101
.byte	-3
.byte	39
.byte	-5
.byte	53
.byte	-7
.byte	61
.byte	-11
.byte	75
.byte	-15
.byte	77
.byte	-17
.byte	91
.byte	-25
.byte	107
.byte	-25
.byte	111
.byte	-28
.byte	122
.byte	-11
.byte	76
.byte	-10
.byte	44
.byte	-10
.byte	52
.byte	-10
.byte	57
.byte	-9
.byte	58
.byte	-16
.byte	72
.byte	-7
.byte	69
.byte	-4
.byte	69
.byte	-5
.byte	74
.byte	-9
.byte	86
.byte	2
.byte	66
.byte	-9
.byte	34
.byte	1
.byte	32
.byte	11
.byte	31
.byte	5
.byte	52
.byte	-2
.byte	55
.byte	-2
.byte	67
.byte	0
.byte	73
.byte	-8
.byte	89
.byte	3
.byte	52
.byte	7
.byte	4
.byte	10
.byte	8
.byte	17
.byte	8
.byte	16
.byte	19
.byte	3
.byte	37
.byte	-1
.byte	61
.byte	-5
.byte	73
.byte	-1
.byte	70
.byte	-4
.byte	78
.byte	0
.byte	0
.byte	-21
.byte	126
.byte	-23
.byte	124
.byte	-20
.byte	110
.byte	-26
.byte	126
.byte	-25
.byte	124
.byte	-17
.byte	105
.byte	-27
.byte	121
.byte	-27
.byte	117
.byte	-17
.byte	102
.byte	-26
.byte	117
.byte	-27
.byte	116
.byte	-33
.byte	122
.byte	-10
.byte	95
.byte	-14
.byte	100
.byte	-8
.byte	95
.byte	-17
.byte	111
.byte	-28
.byte	114
.byte	-6
.byte	89
.byte	-2
.byte	80
.byte	-4
.byte	82
.byte	-9
.byte	85
.byte	-8
.byte	81
.byte	-1
.byte	72
.byte	5
.byte	64
.byte	1
.byte	67
.byte	9
.byte	56
.byte	0
.byte	69
.byte	1
.byte	69
.byte	7
.byte	69
.byte	-7
.byte	69
.byte	-6
.byte	67
.byte	-16
.byte	77
.byte	-2
.byte	64
.byte	2
.byte	61
.byte	-6
.byte	67
.byte	-3
.byte	64
.byte	2
.byte	57
.byte	-3
.byte	65
.byte	-3
.byte	66
.byte	0
.byte	62
.byte	9
.byte	51
.byte	-1
.byte	66
.byte	-2
.byte	71
.byte	-2
.byte	75
.byte	-1
.byte	70
.byte	-9
.byte	72
.byte	14
.byte	60
.byte	16
.byte	37
.byte	0
.byte	47
.byte	18
.byte	35
.byte	11
.byte	37
.byte	12
.byte	41
.byte	10
.byte	41
.byte	2
.byte	48
.byte	12
.byte	41
.byte	13
.byte	41
.byte	0
.byte	59
.byte	3
.byte	50
.byte	19
.byte	40
.byte	3
.byte	66
.byte	18
.byte	50
.byte	19
.byte	-6
.byte	18
.byte	-6
.byte	14
.byte	0
.byte	26
.byte	-12
.byte	31
.byte	-16
.byte	33
.byte	-25
.byte	33
.byte	-22
.byte	37
.byte	-28
.byte	39
.byte	-30
.byte	42
.byte	-30
.byte	47
.byte	-42
.byte	45
.byte	-36
.byte	49
.byte	-34
.byte	41
.byte	-17
.byte	32
.byte	9
.byte	69
.byte	-71
.byte	63
.byte	-63
.byte	66
.byte	-64
.byte	77
.byte	-74
.byte	54
.byte	-39
.byte	52
.byte	-35
.byte	41
.byte	-10
.byte	36
.byte	0
.byte	40
.byte	-1
.byte	30
.byte	14
.byte	28
.byte	26
.byte	23
.byte	37
.byte	12
.byte	55
.byte	11
.byte	65
.byte	37
.byte	-33
.byte	39
.byte	-36
.byte	40
.byte	-37
.byte	38
.byte	-30
.byte	46
.byte	-33
.byte	42
.byte	-30
.byte	40
.byte	-24
.byte	49
.byte	-29
.byte	38
.byte	-12
.byte	40
.byte	-10
.byte	38
.byte	-3
.byte	46
.byte	-5
.byte	31
.byte	20
.byte	29
.byte	30
.byte	25
.byte	44
.byte	12
.byte	48
.byte	11
.byte	49
.byte	26
.byte	45
.byte	22
.byte	22
.byte	23
.byte	22
.byte	27
.byte	21
.byte	33
.byte	20
.byte	26
.byte	28
.byte	30
.byte	24
.byte	27
.byte	34
.byte	18
.byte	42
.byte	25
.byte	39
.byte	18
.byte	50
.byte	12
.byte	70
.byte	21
.byte	54
.byte	14
.byte	71
.byte	11
.byte	83
.byte	25
.byte	32
.byte	21
.byte	49
.byte	21
.byte	54
.byte	-5
.byte	85
.byte	-6
.byte	81
.byte	-10
.byte	77
.byte	-7
.byte	81
.byte	-17
.byte	80
.byte	-18
.byte	73
.byte	-4
.byte	74
.byte	-10
.byte	83
.byte	-9
.byte	71
.byte	-9
.byte	67
.byte	-1
.byte	61
.byte	-8
.byte	66
.byte	-14
.byte	66
.byte	0
.byte	59
.byte	2
.byte	59
.byte	17
.byte	-10
.byte	32
.byte	-13
.byte	42
.byte	-9
.byte	49
.byte	-5
.byte	53
.byte	0
.byte	64
.byte	3
.byte	68
.byte	10
.byte	66
.byte	27
.byte	47
.byte	57
.byte	-5
.byte	71
.byte	0
.byte	24
.byte	-1
.byte	36
.byte	-2
.byte	42
.byte	-2
.byte	52
.byte	-9
.byte	57
.byte	-6
.byte	63
.byte	-4
.byte	65
.byte	-4
.byte	67
.byte	-7
.byte	82
.byte	-3
.byte	81
.byte	-3
.byte	76
.byte	-7
.byte	72
.byte	-6
.byte	78
.byte	-12
.byte	72
.byte	-14
.byte	68
.byte	-3
.byte	70
.byte	-6
.byte	76
.byte	-5
.byte	66
.byte	-5
.byte	62
.byte	0
.byte	57
.byte	-4
.byte	61
.byte	-9
.byte	60
.byte	1
.byte	54
.byte	2
.byte	58
.byte	17
.byte	-10
.byte	32
.byte	-13
.byte	42
.byte	-9
.byte	49
.byte	-5
.byte	53
.byte	0
.byte	64
.byte	3
.byte	68
.byte	10
.byte	66
.byte	27
.byte	47
.byte	57
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	-28
.byte	127
.byte	-23
.byte	104
.byte	-6
.byte	53
.byte	-1
.byte	54
.byte	7
.byte	51
.byte	29
.byte	16
.byte	25
.byte	0
.byte	14
.byte	0
.byte	-10
.byte	51
.byte	-3
.byte	62
.byte	-27
.byte	99
.byte	26
.byte	16
.byte	-4
.byte	85
.byte	-24
.byte	102
.byte	5
.byte	57
.byte	6
.byte	57
.byte	-17
.byte	73
.byte	14
.byte	57
.byte	20
.byte	40
.byte	20
.byte	10
.byte	29
.byte	0
.byte	54
.byte	0
.byte	37
.byte	42
.byte	12
.byte	97
.byte	-32
.byte	127
.byte	-22
.byte	117
.byte	-2
.byte	74
.byte	-4
.byte	85
.byte	-24
.byte	102
.byte	5
.byte	57
.byte	-6
.byte	93
.byte	-14
.byte	88
.byte	-6
.byte	44
.byte	4
.byte	55
.byte	-11
.byte	89
.byte	-15
.byte	103
.byte	-21
.byte	116
.byte	19
.byte	57
.byte	20
.byte	58
.byte	4
.byte	84
.byte	6
.byte	96
.byte	1
.byte	63
.byte	-5
.byte	85
.byte	-13
.byte	106
.byte	5
.byte	63
.byte	6
.byte	75
.byte	-3
.byte	90
.byte	-1
.byte	101
.byte	3
.byte	55
.byte	-4
.byte	79
.byte	-2
.byte	75
.byte	-12
.byte	97
.byte	-7
.byte	50
.byte	1
.byte	60
.byte	0
.byte	41
.byte	0
.byte	63
.byte	0
.byte	63
.byte	0
.byte	63
.byte	-9
.byte	83
.byte	4
.byte	86
.byte	0
.byte	97
.byte	-7
.byte	72
.byte	13
.byte	41
.byte	3
.byte	62
.byte	7
.byte	34
.byte	-9
.byte	88
.byte	-20
.byte	127
.byte	-36
.byte	127
.byte	-17
.byte	91
.byte	-14
.byte	95
.byte	-25
.byte	84
.byte	-25
.byte	86
.byte	-12
.byte	89
.byte	-17
.byte	91
.byte	-31
.byte	127
.byte	-14
.byte	76
.byte	-18
.byte	103
.byte	-13
.byte	90
.byte	-37
.byte	127
.byte	11
.byte	80
.byte	5
.byte	76
.byte	2
.byte	84
.byte	5
.byte	78
.byte	-6
.byte	55
.byte	4
.byte	61
.byte	-14
.byte	83
.byte	-37
.byte	127
.byte	-5
.byte	79
.byte	-11
.byte	104
.byte	-11
.byte	91
.byte	-30
.byte	127
.byte	0
.byte	65
.byte	-2
.byte	79
.byte	0
.byte	72
.byte	-4
.byte	92
.byte	-6
.byte	56
.byte	3
.byte	68
.byte	-8
.byte	71
.byte	-13
.byte	98
.byte	-4
.byte	86
.byte	-12
.byte	88
.byte	-5
.byte	82
.byte	-3
.byte	72
.byte	-4
.byte	67
.byte	-8
.byte	72
.byte	-16
.byte	89
.byte	-9
.byte	69
.byte	-1
.byte	59
.byte	5
.byte	66
.byte	4
.byte	57
.byte	-4
.byte	71
.byte	-2
.byte	71
.byte	2
.byte	58
.byte	-1
.byte	74
.byte	-4
.byte	44
.byte	-1
.byte	69
.byte	0
.byte	62
.byte	-7
.byte	51
.byte	-4
.byte	47
.byte	-6
.byte	42
.byte	-3
.byte	41
.byte	-6
.byte	53
.byte	8
.byte	76
.byte	-9
.byte	78
.byte	-11
.byte	83
.byte	9
.byte	52
.byte	0
.byte	67
.byte	-5
.byte	90
.byte	1
.byte	67
.byte	-15
.byte	72
.byte	-5
.byte	75
.byte	-8
.byte	80
.byte	-21
.byte	83
.byte	-21
.byte	64
.byte	-13
.byte	31
.byte	-25
.byte	64
.byte	-29
.byte	94
.byte	9
.byte	75
.byte	17
.byte	63
.byte	-8
.byte	74
.byte	-5
.byte	35
.byte	-2
.byte	27
.byte	13
.byte	91
.byte	3
.byte	65
.byte	-7
.byte	69
.byte	8
.byte	77
.byte	-10
.byte	66
.byte	3
.byte	62
.byte	-3
.byte	68
.byte	-20
.byte	81
.byte	0
.byte	30
.byte	1
.byte	7
.byte	-3
.byte	23
.byte	-21
.byte	74
.byte	16
.byte	66
.byte	-23
.byte	124
.byte	17
.byte	37
.byte	44
.byte	-18
.byte	50
.byte	-34
.byte	-22
.byte	127
.byte	4
.byte	39
.byte	0
.byte	42
.byte	7
.byte	34
.byte	11
.byte	29
.byte	8
.byte	31
.byte	6
.byte	37
.byte	7
.byte	42
.byte	3
.byte	40
.byte	8
.byte	33
.byte	13
.byte	43
.byte	13
.byte	36
.byte	4
.byte	47
.byte	3
.byte	55
.byte	2
.byte	58
.byte	6
.byte	60
.byte	8
.byte	44
.byte	11
.byte	44
.byte	14
.byte	42
.byte	7
.byte	48
.byte	4
.byte	56
.byte	4
.byte	52
.byte	13
.byte	37
.byte	9
.byte	49
.byte	19
.byte	58
.byte	10
.byte	48
.byte	12
.byte	45
.byte	0
.byte	69
.byte	20
.byte	33
.byte	8
.byte	63
.byte	35
.byte	-18
.byte	33
.byte	-25
.byte	28
.byte	-3
.byte	24
.byte	10
.byte	27
.byte	0
.byte	34
.byte	-14
.byte	52
.byte	-44
.byte	39
.byte	-24
.byte	19
.byte	17
.byte	31
.byte	25
.byte	36
.byte	29
.byte	24
.byte	33
.byte	34
.byte	15
.byte	30
.byte	20
.byte	22
.byte	73
.byte	20
.byte	34
.byte	19
.byte	31
.byte	27
.byte	44
.byte	19
.byte	16
.byte	15
.byte	36
.byte	15
.byte	36
.byte	21
.byte	28
.byte	25
.byte	21
.byte	30
.byte	20
.byte	31
.byte	12
.byte	27
.byte	16
.byte	24
.byte	42
.byte	0
.byte	93
.byte	14
.byte	56
.byte	15
.byte	57
.byte	26
.byte	38
.byte	-24
.byte	127
.byte	-24
.byte	115
.byte	-22
.byte	82
.byte	-9
.byte	62
.byte	0
.byte	53
.byte	0
.byte	59
.byte	-14
.byte	85
.byte	-13
.byte	89
.byte	-13
.byte	94
.byte	-11
.byte	92
.byte	-29
.byte	127
.byte	-21
.byte	100
.byte	-14
.byte	57
.byte	-12
.byte	67
.byte	-11
.byte	71
.byte	-10
.byte	77
.byte	-21
.byte	85
.byte	-16
.byte	88
.byte	-23
.byte	104
.byte	-15
.byte	98
.byte	-37
.byte	127
.byte	-10
.byte	82
.byte	-8
.byte	48
.byte	-8
.byte	61
.byte	-8
.byte	66
.byte	-7
.byte	70
.byte	-14
.byte	75
.byte	-10
.byte	79
.byte	-9
.byte	83
.byte	-12
.byte	92
.byte	-18
.byte	108
.byte	-4
.byte	79
.byte	-22
.byte	69
.byte	-16
.byte	75
.byte	-2
.byte	58
.byte	1
.byte	58
.byte	-13
.byte	78
.byte	-9
.byte	83
.byte	-4
.byte	81
.byte	-13
.byte	99
.byte	-13
.byte	81
.byte	-6
.byte	38
.byte	-13
.byte	62
.byte	-6
.byte	58
.byte	-2
.byte	59
.byte	-16
.byte	73
.byte	-10
.byte	76
.byte	-13
.byte	86
.byte	-9
.byte	83
.byte	-10
.byte	87
.byte	0
.byte	0
.byte	-22
.byte	127
.byte	-25
.byte	127
.byte	-25
.byte	120
.byte	-27
.byte	127
.byte	-19
.byte	114
.byte	-23
.byte	117
.byte	-25
.byte	118
.byte	-26
.byte	117
.byte	-24
.byte	113
.byte	-28
.byte	118
.byte	-31
.byte	120
.byte	-37
.byte	124
.byte	-10
.byte	94
.byte	-15
.byte	102
.byte	-10
.byte	99
.byte	-13
.byte	106
.byte	-50
.byte	127
.byte	-5
.byte	92
.byte	17
.byte	57
.byte	-5
.byte	86
.byte	-13
.byte	94
.byte	-12
.byte	91
.byte	-2
.byte	77
.byte	0
.byte	71
.byte	-1
.byte	73
.byte	4
.byte	64
.byte	-7
.byte	81
.byte	5
.byte	64
.byte	15
.byte	57
.byte	1
.byte	67
.byte	0
.byte	68
.byte	-10
.byte	67
.byte	1
.byte	68
.byte	0
.byte	77
.byte	2
.byte	64
.byte	0
.byte	68
.byte	-5
.byte	78
.byte	7
.byte	55
.byte	5
.byte	59
.byte	2
.byte	65
.byte	14
.byte	54
.byte	15
.byte	44
.byte	5
.byte	60
.byte	2
.byte	70
.byte	-2
.byte	76
.byte	-18
.byte	86
.byte	12
.byte	70
.byte	5
.byte	64
.byte	-12
.byte	70
.byte	11
.byte	55
.byte	5
.byte	56
.byte	0
.byte	69
.byte	2
.byte	65
.byte	-6
.byte	74
.byte	5
.byte	54
.byte	7
.byte	54
.byte	-6
.byte	76
.byte	-11
.byte	82
.byte	-2
.byte	77
.byte	-2
.byte	77
.byte	25
.byte	42
.byte	17
.byte	-13
.byte	16
.byte	-9
.byte	17
.byte	-12
.byte	27
.byte	-21
.byte	37
.byte	-30
.byte	41
.byte	-40
.byte	42
.byte	-41
.byte	48
.byte	-47
.byte	39
.byte	-32
.byte	46
.byte	-40
.byte	52
.byte	-51
.byte	46
.byte	-41
.byte	52
.byte	-39
.byte	43
.byte	-19
.byte	32
.byte	11
.byte	61
.byte	-55
.byte	56
.byte	-46
.byte	62
.byte	-50
.byte	81
.byte	-67
.byte	45
.byte	-20
.byte	35
.byte	-2
.byte	28
.byte	15
.byte	34
.byte	1
.byte	39
.byte	1
.byte	30
.byte	17
.byte	20
.byte	38
.byte	18
.byte	45
.byte	15
.byte	54
.byte	0
.byte	79
.byte	36
.byte	-16
.byte	37
.byte	-14
.byte	37
.byte	-17
.byte	32
.byte	1
.byte	34
.byte	15
.byte	29
.byte	15
.byte	24
.byte	25
.byte	34
.byte	22
.byte	31
.byte	16
.byte	35
.byte	18
.byte	31
.byte	28
.byte	33
.byte	41
.byte	36
.byte	28
.byte	27
.byte	47
.byte	21
.byte	62
.byte	18
.byte	31
.byte	19
.byte	26
.byte	36
.byte	24
.byte	24
.byte	23
.byte	27
.byte	16
.byte	24
.byte	30
.byte	31
.byte	29
.byte	22
.byte	41
.byte	22
.byte	42
.byte	16
.byte	60
.byte	15
.byte	52
.byte	14
.byte	60
.byte	3
.byte	78
.byte	-16
.byte	123
.byte	21
.byte	53
.byte	22
.byte	56
.byte	25
.byte	61
.byte	21
.byte	33
.byte	19
.byte	50
.byte	17
.byte	61
.byte	-3
.byte	78
.byte	-8
.byte	74
.byte	-9
.byte	72
.byte	-10
.byte	72
.byte	-18
.byte	75
.byte	-12
.byte	71
.byte	-11
.byte	63
.byte	-5
.byte	70
.byte	-17
.byte	75
.byte	-14
.byte	72
.byte	-16
.byte	67
.byte	-8
.byte	53
.byte	-14
.byte	59
.byte	-9
.byte	52
.byte	-11
.byte	68
.byte	9
.byte	-2
.byte	30
.byte	-10
.byte	31
.byte	-4
.byte	33
.byte	-1
.byte	33
.byte	7
.byte	31
.byte	12
.byte	37
.byte	23
.byte	31
.byte	38
.byte	20
.byte	64
.byte	-9
.byte	71
.byte	-7
.byte	37
.byte	-8
.byte	44
.byte	-11
.byte	49
.byte	-10
.byte	56
.byte	-12
.byte	59
.byte	-8
.byte	63
.byte	-9
.byte	67
.byte	-6
.byte	68
.byte	-10
.byte	79
.byte	-3
.byte	78
.byte	-8
.byte	74
.byte	-9
.byte	72
.byte	-10
.byte	72
.byte	-18
.byte	75
.byte	-12
.byte	71
.byte	-11
.byte	63
.byte	-5
.byte	70
.byte	-17
.byte	75
.byte	-14
.byte	72
.byte	-16
.byte	67
.byte	-8
.byte	53
.byte	-14
.byte	59
.byte	-9
.byte	52
.byte	-11
.byte	68
.byte	9
.byte	-2
.byte	30
.byte	-10
.byte	31
.byte	-4
.byte	33
.byte	-1
.byte	33
.byte	7
.byte	31
.byte	12
.byte	37
.byte	23
.byte	31
.byte	38
.byte	20
.byte	64
.align	2
.type	cabac_context_init_I, @object
.size	cabac_context_init_I, 920
cabac_context_init_I:
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	20
.byte	-15
.byte	2
.byte	54
.byte	3
.byte	74
.byte	-28
.byte	127
.byte	-23
.byte	104
.byte	-6
.byte	53
.byte	-1
.byte	54
.byte	7
.byte	51
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	41
.byte	0
.byte	63
.byte	0
.byte	63
.byte	0
.byte	63
.byte	-9
.byte	83
.byte	4
.byte	86
.byte	0
.byte	97
.byte	-7
.byte	72
.byte	13
.byte	41
.byte	3
.byte	62
.byte	0
.byte	11
.byte	1
.byte	55
.byte	0
.byte	69
.byte	-17
.byte	127
.byte	-13
.byte	102
.byte	0
.byte	82
.byte	-7
.byte	74
.byte	-21
.byte	107
.byte	-27
.byte	127
.byte	-31
.byte	127
.byte	-24
.byte	127
.byte	-18
.byte	95
.byte	-27
.byte	127
.byte	-21
.byte	114
.byte	-30
.byte	127
.byte	-17
.byte	123
.byte	-12
.byte	115
.byte	-16
.byte	122
.byte	-11
.byte	115
.byte	-12
.byte	63
.byte	-2
.byte	68
.byte	-15
.byte	84
.byte	-13
.byte	104
.byte	-3
.byte	70
.byte	-8
.byte	93
.byte	-10
.byte	90
.byte	-30
.byte	127
.byte	-1
.byte	74
.byte	-6
.byte	97
.byte	-7
.byte	91
.byte	-20
.byte	127
.byte	-4
.byte	56
.byte	-5
.byte	82
.byte	-7
.byte	76
.byte	-22
.byte	125
.byte	-7
.byte	93
.byte	-11
.byte	87
.byte	-3
.byte	77
.byte	-5
.byte	71
.byte	-4
.byte	63
.byte	-4
.byte	68
.byte	-12
.byte	84
.byte	-7
.byte	62
.byte	-7
.byte	65
.byte	8
.byte	61
.byte	5
.byte	56
.byte	-2
.byte	66
.byte	1
.byte	64
.byte	0
.byte	61
.byte	-2
.byte	78
.byte	1
.byte	50
.byte	7
.byte	52
.byte	10
.byte	35
.byte	0
.byte	44
.byte	11
.byte	38
.byte	1
.byte	45
.byte	0
.byte	46
.byte	5
.byte	44
.byte	31
.byte	17
.byte	1
.byte	51
.byte	7
.byte	50
.byte	28
.byte	19
.byte	16
.byte	33
.byte	14
.byte	62
.byte	-13
.byte	108
.byte	-15
.byte	100
.byte	-13
.byte	101
.byte	-13
.byte	91
.byte	-12
.byte	94
.byte	-10
.byte	88
.byte	-16
.byte	84
.byte	-10
.byte	86
.byte	-7
.byte	83
.byte	-13
.byte	87
.byte	-19
.byte	94
.byte	1
.byte	70
.byte	0
.byte	72
.byte	-5
.byte	74
.byte	18
.byte	59
.byte	-8
.byte	102
.byte	-15
.byte	100
.byte	0
.byte	95
.byte	-4
.byte	75
.byte	2
.byte	72
.byte	-11
.byte	75
.byte	-3
.byte	71
.byte	15
.byte	46
.byte	-13
.byte	69
.byte	0
.byte	62
.byte	0
.byte	65
.byte	21
.byte	37
.byte	-15
.byte	72
.byte	9
.byte	57
.byte	16
.byte	54
.byte	0
.byte	62
.byte	12
.byte	72
.byte	24
.byte	0
.byte	15
.byte	9
.byte	8
.byte	25
.byte	13
.byte	18
.byte	15
.byte	9
.byte	13
.byte	19
.byte	10
.byte	37
.byte	12
.byte	18
.byte	6
.byte	29
.byte	20
.byte	33
.byte	15
.byte	30
.byte	4
.byte	45
.byte	1
.byte	58
.byte	0
.byte	62
.byte	7
.byte	61
.byte	12
.byte	38
.byte	11
.byte	45
.byte	15
.byte	39
.byte	11
.byte	42
.byte	13
.byte	44
.byte	16
.byte	45
.byte	12
.byte	41
.byte	10
.byte	49
.byte	30
.byte	34
.byte	18
.byte	42
.byte	10
.byte	55
.byte	17
.byte	51
.byte	17
.byte	46
.byte	0
.byte	89
.byte	26
.byte	-19
.byte	22
.byte	-17
.byte	26
.byte	-17
.byte	30
.byte	-25
.byte	28
.byte	-20
.byte	33
.byte	-23
.byte	37
.byte	-27
.byte	33
.byte	-23
.byte	40
.byte	-28
.byte	38
.byte	-17
.byte	33
.byte	-11
.byte	40
.byte	-15
.byte	41
.byte	-6
.byte	38
.byte	1
.byte	41
.byte	17
.byte	30
.byte	-6
.byte	27
.byte	3
.byte	26
.byte	22
.byte	37
.byte	-16
.byte	35
.byte	-4
.byte	38
.byte	-8
.byte	38
.byte	-3
.byte	37
.byte	3
.byte	38
.byte	5
.byte	42
.byte	0
.byte	35
.byte	16
.byte	39
.byte	22
.byte	14
.byte	48
.byte	27
.byte	37
.byte	21
.byte	60
.byte	12
.byte	68
.byte	2
.byte	97
.byte	-3
.byte	71
.byte	-6
.byte	42
.byte	-5
.byte	50
.byte	-3
.byte	54
.byte	-2
.byte	62
.byte	0
.byte	58
.byte	1
.byte	63
.byte	-2
.byte	72
.byte	-1
.byte	74
.byte	-9
.byte	91
.byte	-5
.byte	67
.byte	-5
.byte	27
.byte	-3
.byte	39
.byte	-2
.byte	44
.byte	0
.byte	46
.byte	-16
.byte	64
.byte	-8
.byte	68
.byte	-10
.byte	78
.byte	-6
.byte	77
.byte	-10
.byte	86
.byte	-12
.byte	92
.byte	-15
.byte	55
.byte	-10
.byte	60
.byte	-6
.byte	62
.byte	-4
.byte	65
.byte	-12
.byte	73
.byte	-8
.byte	76
.byte	-7
.byte	80
.byte	-9
.byte	88
.byte	-17
.byte	110
.byte	-11
.byte	97
.byte	-20
.byte	84
.byte	-11
.byte	79
.byte	-6
.byte	73
.byte	-4
.byte	74
.byte	-13
.byte	86
.byte	-13
.byte	96
.byte	-11
.byte	97
.byte	-19
.byte	117
.byte	-8
.byte	78
.byte	-5
.byte	33
.byte	-4
.byte	48
.byte	-2
.byte	53
.byte	-3
.byte	62
.byte	-13
.byte	71
.byte	-10
.byte	79
.byte	-12
.byte	86
.byte	-13
.byte	90
.byte	-14
.byte	97
.byte	0
.byte	0
.byte	-6
.byte	93
.byte	-6
.byte	84
.byte	-8
.byte	79
.byte	0
.byte	66
.byte	-1
.byte	71
.byte	0
.byte	62
.byte	-2
.byte	60
.byte	-2
.byte	59
.byte	-5
.byte	75
.byte	-3
.byte	62
.byte	-4
.byte	58
.byte	-9
.byte	66
.byte	-1
.byte	79
.byte	0
.byte	71
.byte	3
.byte	68
.byte	10
.byte	44
.byte	-7
.byte	62
.byte	15
.byte	36
.byte	14
.byte	40
.byte	16
.byte	27
.byte	12
.byte	29
.byte	1
.byte	44
.byte	20
.byte	36
.byte	18
.byte	32
.byte	5
.byte	42
.byte	1
.byte	48
.byte	10
.byte	62
.byte	17
.byte	46
.byte	9
.byte	64
.byte	-12
.byte	104
.byte	-11
.byte	97
.byte	-16
.byte	96
.byte	-7
.byte	88
.byte	-8
.byte	85
.byte	-7
.byte	85
.byte	-9
.byte	85
.byte	-13
.byte	88
.byte	4
.byte	66
.byte	-3
.byte	77
.byte	-3
.byte	76
.byte	-6
.byte	76
.byte	10
.byte	58
.byte	-1
.byte	76
.byte	-1
.byte	83
.byte	-7
.byte	99
.byte	-14
.byte	95
.byte	2
.byte	95
.byte	0
.byte	76
.byte	-5
.byte	74
.byte	0
.byte	70
.byte	-11
.byte	75
.byte	1
.byte	68
.byte	0
.byte	65
.byte	-14
.byte	73
.byte	3
.byte	62
.byte	4
.byte	62
.byte	-1
.byte	68
.byte	-13
.byte	75
.byte	11
.byte	55
.byte	5
.byte	64
.byte	12
.byte	70
.byte	15
.byte	6
.byte	6
.byte	19
.byte	7
.byte	16
.byte	12
.byte	14
.byte	18
.byte	13
.byte	13
.byte	11
.byte	13
.byte	15
.byte	15
.byte	16
.byte	12
.byte	23
.byte	13
.byte	23
.byte	15
.byte	20
.byte	14
.byte	26
.byte	14
.byte	44
.byte	17
.byte	40
.byte	17
.byte	47
.byte	24
.byte	17
.byte	21
.byte	21
.byte	25
.byte	22
.byte	31
.byte	27
.byte	22
.byte	29
.byte	19
.byte	35
.byte	14
.byte	50
.byte	10
.byte	57
.byte	7
.byte	63
.byte	-2
.byte	77
.byte	-4
.byte	82
.byte	-3
.byte	94
.byte	9
.byte	69
.byte	-12
.byte	109
.byte	36
.byte	-35
.byte	36
.byte	-34
.byte	32
.byte	-26
.byte	37
.byte	-30
.byte	44
.byte	-32
.byte	34
.byte	-18
.byte	34
.byte	-15
.byte	40
.byte	-15
.byte	33
.byte	-7
.byte	35
.byte	-5
.byte	33
.byte	0
.byte	38
.byte	2
.byte	33
.byte	13
.byte	23
.byte	35
.byte	13
.byte	58
.byte	29
.byte	-3
.byte	26
.byte	0
.byte	22
.byte	30
.byte	31
.byte	-7
.byte	35
.byte	-15
.byte	34
.byte	-3
.byte	34
.byte	3
.byte	36
.byte	-1
.byte	34
.byte	5
.byte	32
.byte	11
.byte	35
.byte	5
.byte	34
.byte	12
.byte	39
.byte	11
.byte	30
.byte	29
.byte	34
.byte	26
.byte	29
.byte	39
.byte	19
.byte	66
.byte	31
.byte	21
.byte	31
.byte	31
.byte	25
.byte	50
.byte	-17
.byte	120
.byte	-20
.byte	112
.byte	-18
.byte	114
.byte	-11
.byte	85
.byte	-15
.byte	92
.byte	-14
.byte	89
.byte	-26
.byte	71
.byte	-15
.byte	81
.byte	-14
.byte	80
.byte	0
.byte	68
.byte	-14
.byte	70
.byte	-24
.byte	56
.byte	-23
.byte	68
.byte	-24
.byte	50
.byte	-11
.byte	74
.byte	23
.byte	-13
.byte	26
.byte	-13
.byte	40
.byte	-15
.byte	49
.byte	-14
.byte	44
.byte	3
.byte	45
.byte	6
.byte	44
.byte	34
.byte	33
.byte	54
.byte	19
.byte	82
.byte	-3
.byte	75
.byte	-1
.byte	23
.byte	1
.byte	34
.byte	1
.byte	43
.byte	0
.byte	54
.byte	-2
.byte	55
.byte	0
.byte	61
.byte	1
.byte	64
.byte	0
.byte	68
.byte	-9
.byte	92
.byte	-14
.byte	106
.byte	-13
.byte	97
.byte	-15
.byte	90
.byte	-12
.byte	90
.byte	-18
.byte	88
.byte	-10
.byte	73
.byte	-9
.byte	79
.byte	-14
.byte	86
.byte	-10
.byte	73
.byte	-10
.byte	70
.byte	-10
.byte	69
.byte	-5
.byte	66
.byte	-9
.byte	64
.byte	-5
.byte	58
.byte	2
.byte	59
.byte	21
.byte	-10
.byte	24
.byte	-11
.byte	28
.byte	-8
.byte	28
.byte	-1
.byte	29
.byte	3
.byte	29
.byte	9
.byte	35
.byte	20
.byte	29
.byte	36
.byte	14
.byte	67
.align	2
.type	b_sub_mb_type_info, @object
.size	b_sub_mb_type_info, 52
b_sub_mb_type_info:
.half	256
.byte	1
.space	1
.half	4104
.byte	1
.space	1
.half	16392
.byte	1
.space	1
.half	20488
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	-16368
.byte	2
.space	1
.half	-16352
.byte	2
.space	1
.half	-4080
.byte	2
.space	1
.half	-4064
.byte	2
.space	1
.half	12352
.byte	4
.space	1
.half	-16320
.byte	4
.space	1
.half	-4032
.byte	4
.space	1
.align	2
.type	b_mb_type_info, @object
.size	b_mb_type_info, 92
b_mb_type_info:
.half	256
.byte	1
.space	1
.half	4104
.byte	1
.space	1
.half	16392
.byte	1
.space	1
.half	20488
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	-16368
.byte	2
.space	1
.half	-16352
.byte	2
.space	1
.half	-28656
.byte	2
.space	1
.half	-28640
.byte	2
.space	1
.half	24592
.byte	2
.space	1
.half	24608
.byte	2
.space	1
.half	-20464
.byte	2
.space	1
.half	-20448
.byte	2
.space	1
.half	-8176
.byte	2
.space	1
.half	-8160
.byte	2
.space	1
.half	28688
.byte	2
.space	1
.half	28704
.byte	2
.space	1
.half	-12272
.byte	2
.space	1
.half	-12256
.byte	2
.space	1
.half	-4080
.byte	2
.space	1
.half	-4064
.byte	2
.space	1
.half	-4032
.byte	4
.space	1
.align	2
.type	p_sub_mb_type_info, @object
.size	p_sub_mb_type_info, 16
p_sub_mb_type_info:
.half	4104
.byte	1
.space	1
.half	4112
.byte	2
.space	1
.half	4128
.byte	2
.space	1
.half	4160
.byte	4
.space	1
.align	2
.type	p_mb_type_info, @object
.size	p_mb_type_info, 20
p_mb_type_info:
.half	4104
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	12352
.byte	4
.space	1
.half	12864
.byte	4
.space	1
.align	2
.type	i_mb_type_info, @object
.size	i_mb_type_info, 104
i_mb_type_info:
.half	1
.byte	-1
.byte	-1
.half	2
.byte	2
.byte	0
.half	2
.byte	1
.byte	0
.half	2
.byte	0
.byte	0
.half	2
.byte	3
.byte	0
.half	2
.byte	2
.byte	16
.half	2
.byte	1
.byte	16
.half	2
.byte	0
.byte	16
.half	2
.byte	3
.byte	16
.half	2
.byte	2
.byte	32
.half	2
.byte	1
.byte	32
.half	2
.byte	0
.byte	32
.half	2
.byte	3
.byte	32
.half	2
.byte	2
.byte	15
.half	2
.byte	1
.byte	15
.half	2
.byte	0
.byte	15
.half	2
.byte	3
.byte	15
.half	2
.byte	2
.byte	31
.half	2
.byte	1
.byte	31
.half	2
.byte	0
.byte	31
.half	2
.byte	3
.byte	31
.half	2
.byte	2
.byte	47
.half	2
.byte	1
.byte	47
.half	2
.byte	0
.byte	47
.half	2
.byte	3
.byte	47
.half	4
.byte	-1
.byte	-1
.align	2
.type	chroma_dc_scan, @object
.size	chroma_dc_scan, 4
chroma_dc_scan:
.byte	0
.byte	16
.byte	32
.byte	48
.align	2
.type	luma_dc_field_scan, @object
.size	luma_dc_field_scan, 16
luma_dc_field_scan:
.byte	0
.byte	32
.byte	16
.byte	-128
.byte	-96
.byte	48
.byte	-112
.byte	-80
.byte	64
.byte	96
.byte	-64
.byte	-32
.byte	80
.byte	112
.byte	-48
.byte	-16
.align	2
.type	luma_dc_zigzag_scan, @object
.size	luma_dc_zigzag_scan, 16
luma_dc_zigzag_scan:
.byte	0
.byte	16
.byte	32
.byte	-128
.byte	48
.byte	64
.byte	80
.byte	96
.byte	-112
.byte	-96
.byte	-80
.byte	-64
.byte	112
.byte	-48
.byte	-32
.byte	-16
.align	2
.type	scan8, @object
.size	scan8, 24
scan8:
.byte	12
.byte	13
.byte	20
.byte	21
.byte	14
.byte	15
.byte	22
.byte	23
.byte	28
.byte	29
.byte	36
.byte	37
.byte	30
.byte	31
.byte	38
.byte	39
.byte	9
.byte	10
.byte	17
.byte	18
.byte	33
.byte	34
.byte	41
.byte	42

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
