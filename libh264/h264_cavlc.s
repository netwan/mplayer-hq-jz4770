.file	1 "h264_cavlc.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.fill_decode_neighbors,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	fill_decode_neighbors
.type	fill_decode_neighbors, @function
fill_decode_neighbors:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$6,65536			# 0x10000
lw	$3,168($4)
li	$7,131072			# 0x20000
lw	$13,2192($4)
addu	$6,$4,$6
addu	$7,$4,$7
lw	$2,-5248($6)
lw	$8,-5252($6)
lw	$6,9448($7)
li	$7,-1			# 0xffffffffffffffff
sll	$2,$3,$2
sw	$7,10812($4)
subu	$2,$6,$2
lw	$7,%got(left_block_options.7269)($28)
addiu	$12,$6,-1
addiu	$9,$2,-1
addiu	$11,$2,1
addiu	$7,$7,%lo(left_block_options.7269)
beq	$8,$0,$L14
sw	$7,10808($4)

li	$7,1073676288			# 0x3fff0000
lw	$10,7996($4)
andi	$5,$5,0x80
ori	$7,$7,0xffff
addu	$8,$6,$7
andi	$10,$10,0x1
sll	$8,$8,2
addu	$8,$13,$8
lw	$8,0($8)
beq	$10,$0,$L4
andi	$8,$8,0x80

beq	$8,$5,$L14
subu	$6,$6,$3

bne	$5,$0,$L15
addiu	$12,$6,-1

lw	$5,%got(left_block_options.7269+16)($28)
addu	$9,$9,$3
sw	$0,10812($4)
move	$3,$12
addiu	$5,$5,%lo(left_block_options.7269+16)
b	$L3
sw	$5,10808($4)

$L14:
move	$3,$12
$L3:
sll	$5,$9,2
sw	$9,10768($4)
sw	$3,10784($4)
sll	$7,$2,2
addu	$5,$13,$5
sw	$2,10772($4)
sw	$11,10776($4)
addu	$7,$13,$7
sw	$12,10780($4)
sll	$6,$11,2
lw	$8,0($5)
sll	$9,$9,1
addu	$6,$13,$6
sll	$5,$12,2
sll	$3,$3,2
sw	$8,10788($4)
li	$8,65536			# 0x10000
lw	$14,0($7)
addu	$5,$13,$5
addu	$8,$4,$8
addu	$3,$13,$3
lw	$10,-5268($8)
lw	$7,-5272($8)
sw	$14,10792($4)
lw	$8,0($6)
addu	$9,$10,$9
lhu	$6,0($9)
sw	$8,10796($4)
lw	$5,0($5)
sw	$5,10800($4)
lw	$3,0($3)
beq	$6,$7,$L8
sw	$3,10804($4)

sll	$2,$2,1
sw	$0,10788($4)
addu	$2,$10,$2
lhu	$2,0($2)
beq	$7,$2,$L17
sll	$6,$12,1

sw	$0,10792($4)
$L17:
addu	$6,$10,$6
lhu	$2,0($6)
beq	$7,$2,$L18
sll	$2,$11,1

sw	$0,10804($4)
sw	$0,10800($4)
$L8:
sll	$2,$11,1
$L18:
addu	$2,$10,$2
lhu	$2,0($2)
beq	$7,$2,$L19
nop

j	$31
sw	$0,10796($4)

$L19:
j	$31
nop

$L4:
beq	$5,$0,$L6
addu	$7,$2,$7

sll	$7,$7,2
addu	$7,$13,$7
lw	$14,0($7)
lw	$10,8($7)
lw	$6,4($7)
srl	$14,$14,7
srl	$7,$10,7
andi	$10,$14,0x1
move	$14,$3
movn	$14,$0,$10
andi	$7,$7,0x1
srl	$6,$6,7
andi	$6,$6,0x1
move	$10,$14
move	$14,$3
movn	$14,$0,$7
addu	$9,$10,$9
move	$7,$14
move	$14,$3
movn	$14,$0,$6
addu	$11,$7,$11
beq	$8,$5,$L14
addu	$2,$14,$2

$L15:
lw	$5,%got(left_block_options.7269+48)($28)
addu	$3,$3,$12
addiu	$5,$5,%lo(left_block_options.7269+48)
b	$L3
sw	$5,10808($4)

$L6:
beq	$8,$0,$L14
lw	$5,%got(left_block_options.7269+32)($28)

move	$3,$12
addiu	$5,$5,%lo(left_block_options.7269+32)
b	$L3
sw	$5,10808($4)

.set	macro
.set	reorder
.end	fill_decode_neighbors
.size	fill_decode_neighbors, .-fill_decode_neighbors
.section	.text.fill_decode_caches,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	fill_decode_caches
.type	fill_decode_caches, @function
fill_decode_caches:
.frame	$sp,160,$31		# vars= 112, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,10780($4)
addiu	$sp,$sp,-160
lw	$11,10784($4)
andi	$10,$5,0x800
lw	$2,10800($4)
lw	$9,10804($4)
lw	$6,10768($4)
lw	$8,10772($4)
lw	$7,10776($4)
sw	$23,152($sp)
sw	$21,144($sp)
sw	$20,140($sp)
sw	$18,132($sp)
sw	$fp,156($sp)
sw	$22,148($sp)
sw	$19,136($sp)
sw	$17,128($sp)
sw	$16,124($sp)
sw	$3,16($sp)
sw	$11,20($sp)
sw	$2,8($sp)
sw	$9,12($sp)
lw	$20,10808($4)
lw	$21,10788($4)
lw	$18,10792($4)
bne	$10,$0,$L21
lw	$23,10796($4)

andi	$10,$5,0x7
beq	$10,$0,$L40
li	$10,7			# 0x7

lw	$13,12880($4)
li	$12,-1			# 0xffffffffffffffff
li	$11,65535			# 0xffff
movz	$10,$12,$13
sw	$11,11096($4)
sw	$11,11088($4)
sw	$11,11084($4)
li	$11,61162			# 0xeeea
and	$12,$10,$18
bne	$12,$0,$L24
sw	$11,11092($4)

li	$11,46079			# 0xb3ff
sw	$11,11084($4)
li	$11,13311			# 0x33ff
sw	$11,11088($4)
li	$11,9962			# 0x26ea
sw	$11,11092($4)
$L24:
xor	$11,$5,$2
andi	$11,$11,0x80
bne	$11,$0,$L159
andi	$11,$5,0x80

and	$3,$10,$2
$L169:
bne	$3,$0,$L170
and	$3,$10,$21

$L30:
lw	$3,11084($4)
li	$9,24415			# 0x5f5f
andi	$3,$3,0xdf5f
sw	$9,11096($4)
sw	$3,11084($4)
$L29:
and	$3,$10,$21
$L170:
bne	$3,$0,$L171
and	$3,$10,$23

lw	$3,11084($4)
andi	$3,$3,0x7fff
sw	$3,11084($4)
and	$3,$10,$23
$L171:
bne	$3,$0,$L172
andi	$3,$5,0x1

lw	$3,11092($4)
andi	$3,$3,0xfbff
sw	$3,11092($4)
andi	$3,$5,0x1
$L172:
beq	$3,$0,$L40
andi	$3,$18,0x1

beq	$3,$0,$L34
li	$3,-1			# 0xffffffffffffffff

lw	$11,11856($4)
sll	$9,$8,2
lw	$3,10856($4)
addu	$9,$11,$9
lw	$9,0($9)
addu	$3,$3,$9
lw	$3,0($3)
sw	$3,10820($4)
$L42:
andi	$3,$2,0x1
bne	$3,$0,$L35
li	$9,6			# 0x6

li	$3,-1			# 0xffffffffffffffff
li	$9,2			# 0x2
and	$2,$10,$2
movn	$3,$9,$2
sb	$3,10835($4)
sb	$3,10827($4)
$L37:
lw	$2,12($sp)
andi	$3,$2,0x1
bne	$3,$0,$L38
lw	$11,20($sp)

and	$10,$10,$2
li	$3,-1			# 0xffffffffffffffff
li	$2,2			# 0x2
movz	$2,$3,$10
sb	$2,10851($4)
sb	$2,10843($4)
$L40:
bne	$18,$0,$L160
nop

$L43:
li	$2,64			# 0x40
li	$3,1077936128			# 0x40400000
sb	$2,11210($4)
addiu	$3,$3,16448
sb	$2,11209($4)
sb	$2,11186($4)
sb	$2,11185($4)
sw	$3,11188($4)
lw	$2,8($sp)
bne	$2,$0,$L173
lw	$2,16($sp)

li	$2,64			# 0x40
$L185:
sb	$2,11216($4)
sb	$2,11192($4)
sb	$2,11203($4)
sb	$2,11195($4)
lw	$2,12($sp)
bne	$2,$0,$L174
lw	$2,20($sp)

li	$2,64			# 0x40
$L186:
sb	$2,11224($4)
sb	$2,11200($4)
sb	$2,11219($4)
sb	$2,11211($4)
$L21:
andi	$2,$5,0x78
bne	$2,$0,$L175
li	$24,65536			# 0x10000

andi	$2,$5,0x100
beq	$2,$0,$L50
li	$2,65536			# 0x10000

addu	$2,$4,$2
lw	$2,5140($2)
beq	$2,$0,$L167
lbu	$3,11($sp)

$L175:
addu	$24,$4,$24
lw	$2,6632($24)
beq	$2,$0,$L50
lw	$2,8($sp)

sll	$8,$8,2
li	$19,-2			# 0xfffffffffffffffe
sw	$20,36($sp)
li	$22,-1			# 0xffffffffffffffff
sw	$8,40($sp)
move	$8,$19
sw	$2,44($sp)
move	$3,$19
lw	$2,16($sp)
move	$9,$19
lw	$10,44($sp)
sll	$6,$6,2
movn	$8,$22,$23
sll	$7,$7,2
sll	$2,$2,2
movn	$3,$22,$21
li	$14,131072			# 0x20000
li	$fp,131072			# 0x20000
lw	$11,40($sp)
addiu	$15,$14,8910
lw	$13,40($sp)
addiu	$14,$14,8878
sw	$2,84($sp)
addiu	$2,$2,1
movn	$9,$22,$10
addiu	$11,$11,2
addiu	$13,$13,3
sw	$2,108($sp)
addu	$15,$4,$15
sw	$8,80($sp)
addiu	$8,$6,1
sw	$3,76($sp)
addiu	$3,$7,2
lw	$2,12($sp)
addu	$14,$4,$14
sw	$9,112($sp)
andi	$9,$10,0x80
sw	$11,60($sp)
andi	$10,$5,0x50
andi	$2,$2,0x80
sw	$13,64($sp)
sw	$8,72($sp)
andi	$11,$5,0x900
sw	$2,92($sp)
andi	$13,$21,0x80
andi	$2,$18,0x80
sw	$7,52($sp)
andi	$8,$23,0x80
sw	$6,56($sp)
move	$12,$0
sw	$3,68($sp)
sw	$9,88($sp)
move	$3,$4
move	$7,$4
sw	$10,28($sp)
move	$6,$4
sw	$11,32($sp)
sw	$13,96($sp)
move	$25,$21
sw	$2,100($sp)
sw	$8,104($sp)
$L96:
li	$8,12288			# 0x3000
sll	$2,$12,1
sll	$2,$8,$2
and	$8,$2,$5
beq	$8,$0,$L52
nop

and	$8,$2,$18
beq	$8,$0,$L53
sw	$0,11648($7)

lw	$9,11852($4)
lw	$13,40($sp)
lw	$10,11864($4)
lw	$11,2184($7)
addu	$9,$9,$13
sll	$8,$10,1
lw	$9,0($9)
addu	$8,$8,$10
addu	$9,$8,$9
sll	$9,$9,2
addu	$8,$11,$9
lw	$10,0($8)
lw	$11,4($8)
sw	$10,11264($6)
sw	$11,11268($6)
lw	$8,2184($7)
lw	$10,60($sp)
lw	$11,64($sp)
addu	$9,$8,$9
lw	$8,8($9)
lw	$9,12($9)
sw	$8,11272($6)
sw	$9,11276($6)
lw	$8,2276($7)
addu	$9,$8,$10
addu	$8,$8,$11
lw	$11,28($sp)
lb	$9,0($9)
sb	$9,11573($3)
sb	$9,11572($3)
lb	$8,0($8)
sb	$8,11575($3)
beq	$11,$0,$L56
sb	$8,11574($3)

$L161:
addiu	$8,$sp,8
lw	$20,36($sp)
move	$13,$0
sw	$25,48($sp)
addiu	$11,$sp,16
sw	$3,116($sp)
li	$21,8			# 0x8
sw	$8,24($sp)
move	$17,$3
move	$16,$6
$L60:
addiu	$9,$sp,8
addu	$8,$9,$13
move	$9,$19
lw	$8,0($8)
movn	$9,$22,$8
and	$8,$8,$2
beq	$8,$0,$L57
addu	$10,$11,$13

lw	$8,0($10)
lw	$9,11852($4)
lbu	$25,0($20)
sll	$8,$8,2
lw	$3,11864($4)
addu	$9,$9,$8
addiu	$8,$8,1
lw	$10,0($9)
mul	$9,$25,$3
lw	$3,2184($7)
addiu	$10,$10,3
addu	$25,$9,$10
sll	$25,$25,2
addu	$25,$3,$25
lw	$9,0($25)
sw	$9,11292($16)
lbu	$9,1($20)
lw	$25,11864($4)
mul	$3,$9,$25
addu	$9,$3,$10
lw	$10,2184($7)
sll	$9,$9,2
addu	$9,$10,$9
lw	$9,0($9)
sw	$9,11324($16)
lw	$9,2276($7)
lbu	$10,0($20)
addu	$8,$9,$8
and	$10,$10,$19
addu	$10,$8,$10
lb	$9,0($10)
sb	$9,11579($17)
lbu	$9,1($20)
and	$9,$9,$19
addu	$8,$8,$9
lb	$8,0($8)
sb	$8,11587($17)
$L58:
addiu	$13,$13,4
addiu	$20,$20,2
addiu	$16,$16,64
bne	$13,$21,$L60
addiu	$17,$17,16

lw	$25,48($sp)
lw	$3,116($sp)
$L61:
and	$8,$2,$23
beq	$8,$0,$L176
lw	$10,80($sp)

$L162:
lw	$9,11852($4)
and	$2,$2,$25
lw	$8,52($sp)
lw	$10,11864($4)
lw	$11,2184($7)
addu	$9,$9,$8
sll	$8,$10,1
lw	$9,0($9)
addu	$8,$8,$10
addu	$8,$8,$9
lw	$9,68($sp)
sll	$8,$8,2
addu	$8,$11,$8
lw	$8,0($8)
sw	$8,11280($6)
lw	$8,2276($7)
addu	$8,$8,$9
lb	$8,0($8)
beq	$2,$0,$L67
sb	$8,11576($3)

$L163:
lw	$9,11852($4)
lw	$11,56($sp)
lw	$2,11864($4)
lw	$10,10812($4)
addu	$9,$9,$11
lw	$13,2184($7)
sll	$8,$2,1
andi	$11,$10,0x2
lw	$9,0($9)
and	$8,$8,$10
addu	$2,$2,$9
addu	$2,$2,$8
addiu	$2,$2,3
sll	$2,$2,2
addu	$2,$13,$2
lw	$13,72($sp)
lw	$2,0($2)
sw	$2,11260($6)
lw	$2,2276($7)
addu	$2,$2,$13
addu	$11,$2,$11
lb	$2,0($11)
sb	$2,11571($3)
lw	$8,32($sp)
beq	$8,$0,$L177
li	$2,-2			# 0xfffffffffffffffe

$L164:
lw	$2,-5252($24)
beq	$2,$0,$L52
li	$2,-2			# 0xfffffffffffffffe

sb	$2,11600($3)
sb	$2,11592($3)
sb	$2,11584($3)
$L97:
lw	$2,-5248($24)
beq	$2,$0,$L73
lw	$9,96($sp)

bne	$9,$0,$L178
lw	$10,100($sp)

lb	$2,11571($3)
bltz	$2,$L178
nop

sll	$2,$2,1
addiu	$8,$fp,8782
sb	$2,11571($3)
addu	$8,$6,$8
lh	$9,11262($6)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11262($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
lw	$10,100($sp)
$L178:
bne	$10,$0,$L76
nop

lb	$2,11572($3)
bltz	$2,$L77
sll	$2,$2,1

addiu	$8,$fp,8786
sb	$2,11572($3)
addu	$8,$6,$8
lh	$9,11266($6)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11266($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L77:
lb	$2,11573($3)
bltz	$2,$L78
sll	$2,$2,1

li	$11,131072			# 0x20000
sb	$2,11573($3)
ori	$11,$11,0x2256
lh	$9,11270($6)
addu	$8,$6,$11
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11270($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L78:
lb	$2,11574($3)
bltz	$2,$L79
sll	$2,$2,1

li	$13,131072			# 0x20000
sb	$2,11574($3)
ori	$13,$13,0x225a
lh	$9,11274($6)
addu	$8,$6,$13
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11274($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L79:
lb	$2,11575($3)
bltz	$2,$L179
lw	$10,104($sp)

sll	$2,$2,1
li	$9,131072			# 0x20000
sb	$2,11575($3)
ori	$9,$9,0x225e
addu	$8,$6,$9
lh	$9,11278($6)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11278($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L76:
lw	$10,104($sp)
$L179:
bne	$10,$0,$L180
lw	$13,88($sp)

lb	$2,11576($3)
bltz	$2,$L180
nop

sll	$2,$2,1
li	$11,131072			# 0x20000
sb	$2,11576($3)
ori	$11,$11,0x2262
lh	$9,11282($6)
addu	$8,$6,$11
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11282($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
lw	$13,88($sp)
$L180:
bne	$13,$0,$L82
nop

lb	$2,11579($3)
bltz	$2,$L83
sll	$2,$2,1

li	$9,131072			# 0x20000
sb	$2,11579($3)
ori	$9,$9,0x226e
addu	$8,$6,$9
lh	$9,11294($6)
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11294($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L83:
lb	$2,11587($3)
bltz	$2,$L82
sll	$2,$2,1

li	$10,131072			# 0x20000
sb	$2,11587($3)
ori	$10,$10,0x228e
lh	$9,11326($6)
addu	$8,$6,$10
srl	$2,$9,31
addu	$2,$2,$9
sra	$2,$2,1
sh	$2,11326($6)
lh	$2,0($8)
sra	$2,$2,1
sh	$2,0($8)
$L82:
lw	$11,92($sp)
bne	$11,$0,$L52
nop

lb	$2,11595($3)
bltz	$2,$L84
sll	$2,$2,1

sb	$2,11595($3)
lh	$8,11358($6)
srl	$2,$8,31
addu	$2,$2,$8
sra	$2,$2,1
sh	$2,11358($6)
lh	$2,0($14)
sra	$2,$2,1
sh	$2,0($14)
$L84:
lb	$2,11603($3)
bltz	$2,$L52
sll	$2,$2,1

sb	$2,11603($3)
lh	$8,11390($6)
srl	$2,$8,31
addu	$2,$2,$8
sra	$2,$2,1
sh	$2,11390($6)
lh	$2,0($15)
sra	$2,$2,1
sh	$2,0($15)
$L52:
lw	$2,6632($24)
addiu	$12,$12,1
addiu	$3,$3,40
addiu	$6,$6,160
sltu	$2,$12,$2
addiu	$15,$15,160
addiu	$14,$14,160
bne	$2,$0,$L96
addiu	$7,$7,4

$L50:
lbu	$3,11($sp)
$L167:
srl	$18,$18,24
lw	$fp,156($sp)
andi	$2,$18,0x1
lw	$23,152($sp)
andi	$18,$3,0x1
lw	$22,148($sp)
addu	$18,$2,$18
lw	$21,144($sp)
lw	$20,140($sp)
lw	$19,136($sp)
lw	$17,128($sp)
lw	$16,124($sp)
sw	$18,11656($4)
lw	$18,132($sp)
j	$31
addiu	$sp,$sp,160

$L53:
move	$9,$0
move	$8,$0
move	$11,$0
sw	$9,11268($6)
li	$9,-16908288			# 0xfffffffffefe0000
sw	$8,11264($6)
li	$8,-1			# 0xffffffffffffffff
ori	$9,$9,0xfefe
sw	$11,11276($6)
movz	$8,$9,$18
move	$10,$0
lw	$11,28($sp)
sw	$10,11272($6)
bne	$11,$0,$L161
sw	$8,11572($3)

$L56:
lw	$11,44($sp)
and	$8,$2,$11
beq	$8,$0,$L62
lw	$13,112($sp)

lw	$13,36($sp)
lw	$10,11864($4)
lw	$9,11852($4)
lw	$11,84($sp)
lbu	$8,0($13)
addu	$9,$9,$11
lw	$11,2184($7)
mul	$13,$8,$10
lw	$10,36($sp)
lw	$9,0($9)
addu	$8,$13,$9
addiu	$8,$8,3
sll	$8,$8,2
addu	$8,$11,$8
lw	$11,108($sp)
lw	$8,0($8)
sw	$8,11292($6)
lw	$8,2276($7)
lbu	$9,0($10)
addu	$8,$8,$11
and	$9,$9,$19
addu	$8,$8,$9
lb	$8,0($8)
sb	$8,11579($3)
and	$8,$2,$23
bne	$8,$0,$L162
lw	$10,80($sp)

$L176:
and	$2,$2,$25
sw	$0,11280($6)
bne	$2,$0,$L163
sb	$10,11576($3)

$L67:
lw	$2,76($sp)
sw	$0,11260($6)
sb	$2,11571($3)
lw	$8,32($sp)
bne	$8,$0,$L164
li	$2,-2			# 0xfffffffffffffffe

$L177:
sb	$2,11600($3)
sb	$2,11592($3)
sb	$2,11584($3)
sb	$2,11598($3)
sb	$2,11582($3)
sw	$0,11304($6)
sw	$0,11368($6)
lw	$2,-5252($24)
bne	$2,$0,$L97
nop

lw	$2,6632($24)
addiu	$12,$12,1
addiu	$3,$3,40
addiu	$6,$6,160
sltu	$2,$12,$2
addiu	$15,$15,160
addiu	$14,$14,160
bne	$2,$0,$L96
addiu	$7,$7,4

b	$L167
lbu	$3,11($sp)

$L73:
lw	$13,96($sp)
beq	$13,$0,$L181
lw	$2,100($sp)

lb	$2,11571($3)
bltz	$2,$L85
sra	$2,$2,1

addiu	$8,$fp,8782
sb	$2,11571($3)
addu	$2,$6,$8
lh	$8,11262($6)
sll	$8,$8,1
sh	$8,11262($6)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L85:
lw	$2,100($sp)
$L181:
beq	$2,$0,$L182
lw	$13,104($sp)

lb	$2,11572($3)
bltz	$2,$L88
sra	$2,$2,1

addiu	$8,$fp,8786
sb	$2,11572($3)
addu	$2,$6,$8
lh	$8,11266($6)
sll	$8,$8,1
sh	$8,11266($6)
lh	$8,0($2)
sll	$8,$8,1
sh	$8,0($2)
$L88:
lb	$2,11573($3)
bltz	$2,$L89
sra	$2,$2,1

li	$9,131072			# 0x20000
sb	$2,11573($3)
ori	$9,$9,0x2256
lh	$2,11270($6)
addu	$8,$6,$9
sll	$2,$2,1
sh	$2,11270($6)
move	$2,$8
lh	$8,0($8)
sll	$8,$8,1
sh	$8,0($2)
$L89:
lb	$2,11574($3)
bltz	$2,$L90
sra	$2,$2,1

li	$10,131072			# 0x20000
sb	$2,11574($3)
ori	$10,$10,0x225a
lh	$2,11274($6)
addu	$8,$6,$10
sll	$2,$2,1
sh	$2,11274($6)
move	$2,$8
lh	$8,0($8)
sll	$8,$8,1
sh	$8,0($2)
$L90:
lb	$2,11575($3)
bltz	$2,$L87
sra	$2,$2,1

li	$11,131072			# 0x20000
sb	$2,11575($3)
ori	$11,$11,0x225e
lh	$2,11278($6)
addu	$8,$6,$11
sll	$2,$2,1
sh	$2,11278($6)
move	$2,$8
lh	$8,0($8)
sll	$8,$8,1
sh	$8,0($2)
$L87:
lw	$13,104($sp)
$L182:
beq	$13,$0,$L183
lw	$10,88($sp)

lb	$2,11576($3)
bltz	$2,$L183
sra	$2,$2,1

li	$9,131072			# 0x20000
sb	$2,11576($3)
ori	$9,$9,0x2262
lh	$2,11282($6)
addu	$8,$6,$9
sll	$2,$2,1
sh	$2,11282($6)
move	$2,$8
lh	$8,0($8)
sll	$8,$8,1
sh	$8,0($2)
lw	$10,88($sp)
$L183:
beq	$10,$0,$L184
lw	$2,92($sp)

lb	$2,11579($3)
bltz	$2,$L94
sra	$2,$2,1

li	$11,131072			# 0x20000
sb	$2,11579($3)
ori	$11,$11,0x226e
lh	$2,11294($6)
addu	$8,$6,$11
sll	$2,$2,1
sh	$2,11294($6)
move	$2,$8
lh	$8,0($8)
sll	$8,$8,1
sh	$8,0($2)
$L94:
lb	$2,11587($3)
bltz	$2,$L93
sra	$2,$2,1

li	$13,131072			# 0x20000
sb	$2,11587($3)
ori	$13,$13,0x228e
lh	$2,11326($6)
addu	$8,$6,$13
sll	$2,$2,1
sh	$2,11326($6)
move	$2,$8
lh	$8,0($8)
sll	$8,$8,1
sh	$8,0($2)
$L93:
lw	$2,92($sp)
$L184:
beq	$2,$0,$L52
nop

lb	$2,11595($3)
bltz	$2,$L95
sra	$2,$2,1

sb	$2,11595($3)
lh	$2,11358($6)
sll	$2,$2,1
sh	$2,11358($6)
lh	$2,0($14)
sll	$2,$2,1
sh	$2,0($14)
$L95:
lb	$2,11603($3)
bltz	$2,$L52
sra	$2,$2,1

addiu	$12,$12,1
addiu	$3,$3,40
sb	$2,11563($3)
addiu	$6,$6,160
lh	$2,11230($6)
addiu	$15,$15,160
addiu	$14,$14,160
sll	$2,$2,1
sh	$2,11230($6)
lh	$2,-160($15)
sll	$2,$2,1
sh	$2,-160($15)
lw	$2,6632($24)
sltu	$2,$12,$2
bne	$2,$0,$L96
addiu	$7,$7,4

b	$L167
lbu	$3,11($sp)

$L57:
sw	$0,11292($16)
sw	$0,11324($16)
sb	$9,11587($17)
b	$L58
sb	$9,11579($17)

$L62:
sw	$0,11292($6)
b	$L61
sb	$13,11579($3)

$L160:
lw	$2,11232($4)
$L168:
sll	$3,$8,5
addu	$2,$2,$3
lw	$3,28($2)
sw	$3,11188($4)
lbu	$3,9($2)
sb	$3,11185($4)
lbu	$3,10($2)
sb	$3,11186($4)
lbu	$3,17($2)
sb	$3,11209($4)
lbu	$2,18($2)
sb	$2,11210($4)
lw	$2,8($sp)
beq	$2,$0,$L185
li	$2,64			# 0x40

lw	$2,16($sp)
$L173:
lw	$3,11232($4)
lbu	$9,8($20)
sll	$2,$2,5
addu	$2,$3,$2
addu	$3,$2,$9
lbu	$3,0($3)
sb	$3,11195($4)
lbu	$3,9($20)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11203($4)
lbu	$3,12($20)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11192($4)
lbu	$3,13($20)
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,11216($4)
lw	$2,12($sp)
beq	$2,$0,$L186
li	$2,64			# 0x40

lw	$2,20($sp)
$L174:
lw	$3,11232($4)
lbu	$9,10($20)
sll	$2,$2,5
addu	$2,$3,$2
addu	$3,$2,$9
lbu	$3,0($3)
sb	$3,11211($4)
lbu	$3,11($20)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11219($4)
lbu	$3,14($20)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11200($4)
lbu	$3,15($20)
addu	$2,$2,$3
lbu	$2,0($2)
b	$L21
sb	$2,11224($4)

$L159:
beq	$11,$0,$L26
nop

and	$3,$10,$2
bne	$3,$0,$L27
li	$11,24575			# 0x5fff

lw	$3,11084($4)
andi	$3,$3,0xdfff
sw	$11,11096($4)
sw	$3,11084($4)
$L27:
and	$9,$10,$9
bne	$9,$0,$L170
and	$3,$10,$21

lw	$9,11084($4)
lw	$3,11096($4)
andi	$9,$9,0xff5f
andi	$3,$3,0xff5f
sw	$9,11084($4)
b	$L29
sw	$3,11096($4)

$L38:
li	$9,6			# 0x6
lw	$10,11856($4)
lbu	$3,2($20)
sll	$11,$11,2
lw	$2,10856($4)
addu	$10,$10,$11
subu	$3,$9,$3
lw	$10,0($10)
addu	$2,$2,$10
addu	$3,$2,$3
lb	$3,0($3)
sb	$3,10843($4)
lbu	$3,3($20)
subu	$9,$9,$3
addu	$2,$2,$9
lb	$2,0($2)
beq	$18,$0,$L43
sb	$2,10851($4)

b	$L168
lw	$2,11232($4)

$L26:
lw	$11,168($4)
lw	$9,2192($4)
addu	$3,$3,$11
sll	$3,$3,2
addu	$3,$9,$3
lw	$3,0($3)
and	$3,$10,$3
beq	$3,$0,$L30
and	$3,$10,$2

b	$L169
nop

$L35:
lw	$12,16($sp)
lw	$11,11856($4)
lbu	$3,0($20)
sll	$12,$12,2
lw	$2,10856($4)
addu	$11,$11,$12
subu	$3,$9,$3
lw	$11,0($11)
addu	$2,$2,$11
addu	$3,$2,$3
lb	$3,0($3)
sb	$3,10827($4)
lbu	$3,1($20)
subu	$9,$9,$3
addu	$2,$2,$9
lb	$2,0($2)
b	$L37
sb	$2,10835($4)

$L34:
li	$9,2			# 0x2
movn	$3,$9,$12
sb	$3,10823($4)
sb	$3,10822($4)
sb	$3,10821($4)
b	$L42
sb	$3,10820($4)

.set	macro
.set	reorder
.end	fill_decode_caches
.size	fill_decode_caches, .-fill_decode_caches
.section	.text.pred_motion,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	pred_motion
.type	pred_motion, @function
pred_motion:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,%got(scan8)($28)
sll	$11,$7,3
sll	$12,$7,5
li	$24,65536			# 0x10000
addiu	$3,$3,%lo(scan8)
addu	$2,$11,$12
addu	$5,$3,$5
addu	$15,$4,$2
addu	$24,$4,$24
lbu	$14,0($5)
addiu	$sp,$sp,-8
lw	$25,-5252($24)
addiu	$5,$14,-8
lw	$9,24($sp)
addiu	$10,$14,-1
sw	$16,0($sp)
addu	$3,$2,$5
sw	$17,4($sp)
addu	$2,$2,$10
addu	$6,$5,$6
addiu	$2,$2,2812
addiu	$8,$3,2812
addu	$5,$15,$5
addu	$10,$15,$10
addu	$16,$15,$6
sll	$3,$2,2
lb	$13,11568($5)
sll	$8,$8,2
lb	$10,11568($10)
addu	$3,$4,$3
lb	$16,11568($16)
addu	$8,$4,$8
beq	$25,$0,$L188
li	$2,-2			# 0xfffffffffffffffe

beq	$16,$2,$L227
slt	$2,$14,20

addu	$5,$11,$12
$L238:
addu	$2,$5,$6
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$4,$2
$L193:
xor	$6,$13,$9
xor	$4,$9,$16
sltu	$6,$6,1
sltu	$4,$4,1
xor	$5,$10,$9
addu	$4,$4,$6
sltu	$5,$5,1
addu	$4,$4,$5
slt	$7,$4,2
bne	$7,$0,$L235
li	$7,1			# 0x1

$L203:
lh	$5,0($3)
lh	$4,0($8)
slt	$7,$4,$5
bne	$7,$0,$L229
lh	$6,0($2)

slt	$7,$6,$4
beq	$7,$0,$L205
nop

slt	$4,$6,$5
movz	$5,$6,$4
move	$4,$5
$L205:
lh	$5,2($3)
lw	$6,28($sp)
lh	$3,2($8)
lh	$2,2($2)
sw	$4,0($6)
slt	$4,$3,$5
beq	$4,$0,$L236
slt	$4,$2,$3

slt	$4,$3,$2
$L239:
beq	$4,$0,$L237
lw	$17,32($sp)

slt	$3,$5,$2
movz	$5,$2,$3
move	$3,$5
$L237:
sw	$3,0($17)
$L187:
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L188:
bne	$16,$2,$L238
addu	$5,$11,$12

$L208:
addiu	$14,$14,-9
addu	$5,$11,$12
xor	$6,$13,$9
addu	$2,$5,$14
addu	$5,$4,$5
addiu	$2,$2,2812
addu	$5,$5,$14
sll	$2,$2,2
sltu	$6,$6,1
lb	$16,11568($5)
addu	$2,$4,$2
xor	$5,$10,$9
sltu	$5,$5,1
xor	$4,$9,$16
sltu	$4,$4,1
addu	$4,$4,$6
addu	$4,$4,$5
slt	$7,$4,2
beq	$7,$0,$L203
li	$7,1			# 0x1

$L235:
beq	$4,$7,$L230
li	$4,-2			# 0xfffffffffffffffe

bne	$13,$4,$L203
nop

bne	$16,$13,$L203
nop

beq	$10,$16,$L203
lw	$17,28($sp)

lh	$4,0($3)
lh	$2,2($3)
lw	$3,32($sp)
sw	$4,0($17)
b	$L187
sw	$2,0($3)

$L229:
slt	$7,$4,$6
beq	$7,$0,$L205
nop

slt	$4,$5,$6
lh	$2,2($2)
movz	$5,$6,$4
lw	$6,28($sp)
move	$4,$5
lh	$5,2($3)
lh	$3,2($8)
sw	$4,0($6)
slt	$4,$3,$5
bne	$4,$0,$L239
slt	$4,$3,$2

slt	$4,$2,$3
$L236:
beq	$4,$0,$L237
lw	$17,32($sp)

slt	$3,$2,$5
movz	$5,$2,$3
b	$L237
move	$3,$5

$L227:
bne	$2,$0,$L208
li	$2,4			# 0x4

andi	$5,$14,0x7
bne	$5,$2,$L208
nop

lb	$2,11579($15)
beq	$2,$16,$L208
sll	$5,$7,7

lw	$6,2696($4)
addu	$5,$12,$5
addiu	$2,$5,11288
lw	$15,104($6)
addu	$2,$4,$2
sw	$0,0($2)
lw	$6,-5248($24)
beq	$6,$0,$L232
nop

lw	$6,10800($4)
andi	$6,$6,0x80
bne	$6,$0,$L208
slt	$6,$14,36

xori	$6,$6,0x1
addiu	$6,$6,2694
li	$11,12288			# 0x3000
sll	$6,$6,2
sll	$16,$7,1
addu	$6,$4,$6
sll	$16,$11,$16
sra	$14,$14,2
lw	$12,4($6)
sll	$12,$12,2
addu	$11,$15,$12
lw	$11,0($11)
and	$11,$16,$11
beq	$11,$0,$L210
andi	$6,$14,0x3

lw	$24,11864($4)
addu	$5,$4,$5
lw	$16,11852($4)
sll	$7,$7,2
lw	$15,2696($4)
andi	$11,$14,0x2
mul	$4,$6,$24
addu	$6,$16,$12
addu	$7,$15,$7
lw	$14,0($6)
lw	$6,96($7)
addiu	$4,$4,3
addu	$4,$4,$14
sll	$4,$4,2
addu	$4,$6,$4
lh	$6,0($4)
sh	$6,11288($5)
lh	$4,2($4)
srl	$6,$4,31
addu	$6,$6,$4
sra	$6,$6,1
sh	$6,11290($5)
lw	$5,188($7)
addu	$4,$5,$12
addu	$4,$4,$11
lb	$16,1($4)
b	$L193
sll	$16,$16,1

$L230:
bne	$5,$0,$L233
nop

bne	$6,$0,$L234
lw	$4,28($sp)

lh	$3,0($2)
lh	$2,2($2)
lw	$6,32($sp)
sw	$3,0($4)
b	$L187
sw	$2,0($6)

$L234:
lh	$3,0($8)
lh	$2,2($8)
lw	$6,28($sp)
lw	$17,32($sp)
sw	$3,0($6)
b	$L187
sw	$2,0($17)

$L233:
lh	$4,0($3)
lh	$2,2($3)
lw	$3,28($sp)
sw	$4,0($3)
lw	$4,32($sp)
b	$L187
sw	$2,0($4)

$L232:
lw	$6,10800($4)
andi	$6,$6,0x80
beq	$6,$0,$L208
sll	$25,$7,1

lw	$6,7996($4)
sra	$14,$14,5
lw	$11,168($4)
lw	$24,10780($4)
li	$12,12288			# 0x3000
andi	$6,$6,0x1
sll	$6,$6,1
addu	$24,$11,$24
addu	$6,$6,$14
sll	$12,$12,$25
sra	$14,$6,2
mul	$17,$11,$14
addu	$11,$17,$24
sll	$11,$11,2
addu	$11,$15,$11
lw	$11,0($11)
and	$12,$12,$11
beq	$12,$0,$L210
sll	$11,$24,2

lw	$14,11852($4)
lw	$15,11864($4)
addu	$5,$4,$5
lw	$12,2696($4)
sll	$7,$7,2
addu	$14,$14,$11
mul	$4,$6,$15
addu	$7,$12,$7
lw	$14,0($14)
and	$16,$6,$16
lw	$12,96($7)
addiu	$14,$14,3
addu	$6,$4,$14
sll	$6,$6,2
addu	$6,$12,$6
lh	$4,0($6)
sh	$4,11288($5)
lhu	$6,2($6)
sll	$6,$6,1
sh	$6,11290($5)
lw	$4,188($7)
addu	$11,$4,$11
addu	$4,$11,$16
lb	$16,1($4)
b	$L193
sra	$16,$16,1

$L210:
b	$L193
li	$16,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	pred_motion
.size	pred_motion, .-pred_motion
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"corrupted macroblock %d %d (total_coeff=%d)\012\000"
.align	2
$LC1:
.ascii	"Invalid level prefix\012\000"
.align	2
$LC2:
.ascii	"negative number of zero coeffs at %d %d\012\000"
.section	.text.decode_residual,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_residual
.type	decode_residual, @function
decode_residual:
.frame	$sp,152,$31		# vars= 80, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-152
li	$2,26			# 0x1a
.cprestore	24
sw	$17,116($sp)
sw	$31,148($sp)
sw	$fp,144($sp)
sw	$23,140($sp)
sw	$22,136($sp)
sw	$21,132($sp)
sw	$20,128($sp)
sw	$19,124($sp)
sw	$18,120($sp)
sw	$16,112($sp)
.set	noreorder
.set	nomacro
beq	$7,$2,$L312
lw	$17,172($sp)
.set	macro
.set	reorder

li	$2,25			# 0x19
.set	noreorder
.set	nomacro
beq	$7,$2,$L313
lw	$2,%got(scan8)($28)
.set	macro
.set	reorder

addiu	$2,$2,%lo(scan8)
addu	$2,$7,$2
lbu	$9,0($2)
addu	$9,$4,$9
lbu	$3,11176($9)
lbu	$2,11183($9)
addu	$2,$2,$3
slt	$3,$2,64
beq	$3,$0,$L247
addiu	$2,$2,1
sra	$2,$2,1
$L247:
lw	$8,%got(coeff_token_table_index.7710)($28)
andi	$2,$2,0x1f
lw	$10,8($5)
sll	$2,$2,2
lw	$11,0($5)
li	$12,16711680			# 0xff0000
addiu	$8,$8,%lo(coeff_token_table_index.7710)
srl	$3,$10,3
addu	$2,$2,$8
li	$13,-16777216			# 0xffffffffff000000
addu	$3,$11,$3
lw	$14,0($2)
addiu	$12,$12,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$8,8
sll	$8,$8,8
ori	$13,$13,0xff00
and	$3,$2,$12
and	$2,$8,$13
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
sll	$8,$14,4
or	$2,$3,$2
lw	$3,%got(coeff_token_vlc)($28)
andi	$14,$10,0x7
sll	$2,$2,$14
addiu	$3,$3,%lo(coeff_token_vlc)
srl	$2,$2,24
addu	$3,$8,$3
sll	$2,$2,2
lw	$15,4($3)
addu	$2,$15,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L314
lh	$8,0($2)
.set	macro
.set	reorder

addu	$10,$3,$10
sra	$14,$8,2
sw	$10,8($5)
sb	$14,11184($9)
$L242:
.set	noreorder
.set	nomacro
beq	$14,$0,$L307
move	$2,$0
.set	macro
.set	reorder

lw	$9,176($sp)
sw	$4,100($sp)
sltu	$2,$9,$14
.set	noreorder
.set	nomacro
bne	$2,$0,$L315
sw	$7,96($sp)
.set	macro
.set	reorder

lw	$12,8($5)
li	$9,16711680			# 0xff0000
lw	$10,0($5)
li	$7,-16777216			# 0xffffffffff000000
addiu	$9,$9,255
srl	$2,$12,3
ori	$7,$7,0xff00
addu	$2,$10,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$4,$3,8
srl	$2,$3,8
li	$13,1			# 0x1
and	$3,$2,$9
and	$2,$4,$7
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$12,0x7
or	$2,$3,$2
sll	$2,$2,$4
andi	$4,$8,0x3
srl	$2,$2,29
addu	$12,$4,$12
andi	$3,$2,0x4
andi	$8,$2,0x1
sra	$3,$3,1
sw	$12,8($5)
sll	$8,$8,1
andi	$2,$2,0x2
subu	$3,$13,$3
subu	$2,$13,$2
subu	$8,$13,$8
slt	$11,$4,$14
sw	$3,32($sp)
sw	$2,36($sp)
.set	noreorder
.set	nomacro
beq	$11,$0,$L253
sw	$8,40($sp)
.set	macro
.set	reorder

srl	$2,$12,3
lw	$21,%got(cavlc_level_tab)($28)
slt	$15,$14,11
addu	$2,$10,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$8,$3,8
srl	$2,$3,8
slt	$11,$4,3
and	$3,$2,$9
and	$2,$8,$7
or	$2,$3,$2
sll	$8,$2,16
srl	$2,$2,16
andi	$3,$12,0x7
or	$2,$8,$2
sll	$2,$2,$3
xori	$8,$15,0x1
and	$8,$11,$8
srl	$3,$2,24
sll	$2,$8,8
addiu	$21,$21,%lo(cavlc_level_tab)
addu	$2,$2,$3
sll	$2,$2,1
addu	$2,$21,$2
lb	$3,1($2)
lb	$2,0($2)
addu	$12,$12,$3
slt	$3,$2,100
.set	noreorder
.set	nomacro
beq	$3,$0,$L316
sw	$12,8($5)
.set	macro
.set	reorder

sra	$3,$2,31
sll	$7,$4,2
ori	$3,$3,0x1
movz	$3,$0,$11
move	$11,$3
addu	$2,$11,$2
addiu	$3,$sp,32
addiu	$11,$2,3
addu	$7,$3,$7
sw	$2,0($7)
sltu	$2,$11,7
li	$11,2			# 0x2
movn	$11,$13,$2
$L263:
addiu	$4,$4,1
slt	$2,$4,$14
.set	noreorder
.set	nomacro
beq	$2,$0,$L311
lw	$22,%got(suffix_limit.7723)($28)
.set	macro
.set	reorder

sll	$4,$4,2
sll	$20,$14,2
lw	$15,0($5)
li	$19,16711680			# 0xff0000
sw	$14,104($sp)
li	$18,-16777216			# 0xffffffffff000000
addu	$13,$3,$4
addu	$20,$3,$20
addiu	$22,$22,%lo(suffix_limit.7723)
addiu	$19,$19,255
li	$fp,15			# 0xf
.set	noreorder
.set	nomacro
b	$L270
ori	$18,$18,0xff00
.set	macro
.set	reorder

$L319:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($8)  
lwr $10, 0($8)  

# 0 "" 2
#NO_APP
srl	$2,$10,8
sw	$25,8($5)
sll	$10,$10,8
and	$2,$2,$19
and	$10,$10,$18
or	$10,$2,$10
sll	$2,$10,16
srl	$10,$10,16
or	$10,$2,$10
sll	$10,$10,$24
srl	$10,$10,$23
addu	$10,$10,$16
$L269:
addiu	$16,$10,2
andi	$10,$10,0x1
sra	$2,$16,1
subu	$16,$0,$10
xor	$16,$2,$16
addu	$10,$10,$16
$L266:
sll	$2,$11,2
sw	$10,0($13)
addiu	$13,$13,4
addu	$2,$22,$2
lw	$2,0($2)
addu	$10,$10,$2
sll	$2,$2,1
sltu	$10,$2,$10
.set	noreorder
.set	nomacro
beq	$13,$20,$L317
addu	$11,$11,$10
.set	macro
.set	reorder

$L270:
lw	$2,8($5)
sll	$3,$11,8
srl	$4,$2,3
andi	$9,$2,0x7
addu	$4,$15,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($4)  
lwr $8, 0($4)  

# 0 "" 2
#NO_APP
srl	$7,$8,8
sll	$8,$8,8
and	$7,$7,$19
and	$8,$8,$18
or	$4,$7,$8
sll	$7,$4,16
srl	$4,$4,16
or	$4,$7,$4
sll	$4,$4,$9
srl	$4,$4,24
addu	$3,$3,$4
sll	$3,$3,1
addu	$3,$21,$3
lb	$4,1($3)
lb	$10,0($3)
addu	$2,$2,$4
slt	$4,$10,100
addiu	$3,$10,-100
.set	noreorder
.set	nomacro
bne	$4,$0,$L266
sw	$2,8($5)
.set	macro
.set	reorder

li	$8,32			# 0x20
li	$9,8			# 0x8
.set	noreorder
.set	nomacro
beq	$3,$9,$L318
subu	$23,$8,$11
.set	macro
.set	reorder

$L267:
srl	$8,$2,3
li	$9,35			# 0x23
addu	$8,$15,$8
addiu	$4,$3,-3
subu	$12,$9,$3
slt	$9,$3,15
andi	$24,$2,0x7
addu	$25,$2,$11
move	$7,$8
addu	$2,$4,$2
sll	$10,$fp,$11
.set	noreorder
.set	nomacro
bne	$9,$0,$L319
sll	$16,$3,$11
.set	macro
.set	reorder

#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
srl	$16,$8,8
sw	$2,8($5)
sll	$7,$8,8
and	$2,$16,$19
and	$16,$7,$18
or	$16,$2,$16
sll	$2,$16,16
srl	$16,$16,16
li	$9,1			# 0x1
or	$16,$2,$16
sll	$14,$16,$24
sll	$4,$9,$4
srl	$12,$14,$12
addiu	$4,$4,-4096
.set	noreorder
.set	nomacro
beq	$3,$fp,$L269
addu	$10,$12,$10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L269
addu	$10,$10,$4
.set	macro
.set	reorder

$L317:
lw	$14,104($sp)
$L311:
lw	$3,32($sp)
$L253:
lw	$2,176($sp)
.set	noreorder
.set	nomacro
beq	$14,$2,$L296
lw	$4,96($sp)
.set	macro
.set	reorder

li	$2,26			# 0x1a
beq	$4,$2,$L320
lw	$9,8($5)
lw	$2,0($5)
srl	$4,$9,3
andi	$10,$9,0x7
addu	$2,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$7,$4,8
addiu	$2,$2,255
sll	$8,$4,8
and	$4,$7,$2
lw	$7,%got(total_zeros_vlc)($28)
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$2,$8,$2
or	$2,$4,$2
sll	$4,$2,16
srl	$2,$2,16
sll	$8,$14,4
addiu	$7,$7,%lo(total_zeros_vlc)
or	$2,$4,$2
sll	$2,$2,$10
addu	$4,$7,$8
srl	$2,$2,23
lw	$4,-12($4)
sll	$2,$2,2
addu	$2,$4,$2
lh	$4,2($2)
lh	$10,0($2)
addu	$2,$9,$4
sw	$2,8($5)
$L271:
addu	$9,$10,$14
lw	$8,96($sp)
lw	$2,168($sp)
addiu	$9,$9,-1
slt	$7,$8,25
.set	noreorder
.set	nomacro
beq	$7,$0,$L273
addu	$9,$2,$9
.set	macro
.set	reorder

lbu	$2,0($9)
slt	$7,$14,2
sll	$8,$2,2
sll	$2,$2,1
addu	$8,$17,$8
addu	$4,$6,$2
lw	$2,0($8)
mul	$2,$3,$2
addiu	$2,$2,32
srl	$2,$2,6
.set	noreorder
.set	nomacro
bne	$7,$0,$L283
sh	$2,0($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$10,$L298
lw	$2,%got(run7_vlc+4)($28)
.set	macro
.set	reorder

li	$16,16711680			# 0xff0000
lw	$19,%got(run_vlc)($28)
li	$15,-16777216			# 0xffffffffff000000
li	$18,268369920			# 0xfff0000
addiu	$13,$sp,36
lw	$20,%lo(run7_vlc+4)($2)
li	$11,1			# 0x1
addiu	$16,$16,255
addiu	$19,$19,%lo(run_vlc)
ori	$15,$15,0xff00
.set	noreorder
.set	nomacro
b	$L290
ori	$18,$18,0xffff
.set	macro
.set	reorder

$L321:
lw	$3,0($5)
addu	$2,$19,$2
srl	$4,$7,3
andi	$12,$7,0x7
lw	$8,4($2)
addu	$2,$3,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$4,$3,8
sll	$3,$3,8
and	$4,$4,$16
and	$3,$3,$15
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$12
srl	$2,$2,29
sll	$2,$2,2
addu	$2,$8,$2
lh	$4,2($2)
lh	$3,0($2)
addu	$7,$7,$4
sw	$7,8($5)
$L287:
addiu	$2,$3,1
lw	$7,0($13)
subu	$10,$10,$3
subu	$9,$9,$2
addiu	$11,$11,1
addiu	$13,$13,4
lbu	$3,0($9)
sll	$4,$3,2
sll	$3,$3,1
addu	$4,$17,$4
addu	$3,$6,$3
lw	$2,0($4)
mul	$2,$2,$7
addiu	$2,$2,32
srl	$2,$2,6
.set	noreorder
.set	nomacro
beq	$11,$14,$L283
sh	$2,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$10,$L331
sll	$7,$11,2
.set	macro
.set	reorder

$L290:
addu	$2,$10,$18
lw	$7,8($5)
slt	$3,$10,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L321
sll	$2,$2,4
.set	macro
.set	reorder

lw	$8,0($5)
srl	$2,$7,3
andi	$12,$7,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$16
and	$4,$4,$15
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$12
srl	$2,$2,26
sll	$2,$2,2
addu	$2,$20,$2
lh	$4,2($2)
.set	noreorder
.set	nomacro
bltz	$4,$L322
lh	$3,0($2)
.set	macro
.set	reorder

addu	$7,$4,$7
.set	noreorder
.set	nomacro
b	$L287
sw	$7,8($5)
.set	macro
.set	reorder

$L298:
li	$11,1			# 0x1
sll	$7,$11,2
$L331:
addiu	$2,$sp,32
addu	$7,$2,$7
$L291:
addiu	$9,$9,-1
lbu	$3,0($9)
lw	$2,0($7)
addiu	$11,$11,1
addiu	$7,$7,4
sll	$4,$3,2
sll	$3,$3,1
addu	$4,$17,$4
addu	$3,$6,$3
slt	$5,$11,$14
lw	$4,0($4)
mul	$2,$2,$4
addiu	$2,$2,32
srl	$2,$2,6
.set	noreorder
.set	nomacro
bne	$5,$0,$L291
sh	$2,0($3)
.set	macro
.set	reorder

$L283:
bltz	$10,$L323
$L292:
move	$2,$0
$L307:
lw	$31,148($sp)
lw	$fp,144($sp)
lw	$23,140($sp)
lw	$22,136($sp)
lw	$21,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

$L322:
addiu	$7,$7,6
srl	$2,$7,3
andi	$21,$7,0x7
addu	$8,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($8)  
lwr $12, 0($8)  

# 0 "" 2
#NO_APP
srl	$8,$12,8
sll	$12,$12,8
and	$8,$8,$16
and	$12,$12,$15
or	$12,$8,$12
sll	$8,$12,16
srl	$12,$12,16
or	$2,$8,$12
sll	$2,$2,$21
srl	$2,$2,$4
addu	$3,$2,$3
sll	$2,$3,2
addu	$2,$20,$2
lh	$4,2($2)
lh	$3,0($2)
addu	$7,$4,$7
.set	noreorder
.set	nomacro
b	$L287
sw	$7,8($5)
.set	macro
.set	reorder

$L320:
lw	$11,8($5)
lw	$2,0($5)
srl	$4,$11,3
andi	$9,$11,0x7
addu	$2,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$7
sll	$8,$2,8
li	$2,16711680			# 0xff0000
srl	$7,$7,8
addiu	$2,$2,255
and	$4,$7,$2
lw	$7,%got(chroma_dc_total_zeros_vlc)($28)
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$2,$8,$2
or	$2,$4,$2
sll	$4,$2,16
srl	$2,$2,16
sll	$8,$14,4
addiu	$7,$7,%lo(chroma_dc_total_zeros_vlc)
or	$2,$4,$2
sll	$2,$2,$9
addu	$4,$7,$8
lw	$8,168($sp)
srl	$2,$2,29
lw	$4,-12($4)
sll	$2,$2,2
addu	$2,$4,$2
lh	$10,0($2)
lh	$2,2($2)
addu	$9,$14,$10
addu	$2,$11,$2
addiu	$9,$9,-1
addu	$9,$8,$9
sw	$2,8($5)
$L273:
lbu	$2,0($9)
slt	$4,$14,2
sll	$2,$2,1
addu	$2,$6,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L283
sh	$3,0($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$10,$L297
lw	$2,%got(run7_vlc+4)($28)
.set	macro
.set	reorder

li	$16,16711680			# 0xff0000
lw	$18,%got(run_vlc)($28)
li	$15,-16777216			# 0xffffffffff000000
li	$17,268369920			# 0xfff0000
addiu	$13,$sp,36
lw	$19,%lo(run7_vlc+4)($2)
li	$11,1			# 0x1
addiu	$16,$16,255
addiu	$18,$18,%lo(run_vlc)
ori	$15,$15,0xff00
.set	noreorder
.set	nomacro
b	$L281
ori	$17,$17,0xffff
.set	macro
.set	reorder

$L324:
lw	$3,0($5)
addu	$2,$18,$2
srl	$4,$7,3
andi	$12,$7,0x7
lw	$8,4($2)
addu	$2,$3,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$4,$3,8
sll	$3,$3,8
and	$4,$4,$16
and	$3,$3,$15
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$12
srl	$2,$2,29
sll	$2,$2,2
addu	$2,$8,$2
lh	$3,2($2)
lh	$2,0($2)
addu	$7,$7,$3
sw	$7,8($5)
$L278:
addiu	$4,$2,1
lw	$3,0($13)
subu	$10,$10,$2
subu	$9,$9,$4
addiu	$11,$11,1
addiu	$13,$13,4
lbu	$2,0($9)
sll	$2,$2,1
addu	$2,$6,$2
.set	noreorder
.set	nomacro
beq	$11,$14,$L283
sh	$3,0($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$10,$L332
sll	$3,$11,2
.set	macro
.set	reorder

$L281:
addu	$2,$10,$17
lw	$7,8($5)
slt	$3,$10,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L324
sll	$2,$2,4
.set	macro
.set	reorder

lw	$8,0($5)
srl	$3,$7,3
andi	$12,$7,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$16
and	$4,$4,$15
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$12
srl	$2,$2,26
sll	$2,$2,2
addu	$2,$19,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L325
lh	$2,0($2)
.set	macro
.set	reorder

addu	$7,$3,$7
.set	noreorder
.set	nomacro
b	$L278
sw	$7,8($5)
.set	macro
.set	reorder

$L325:
addiu	$7,$7,6
srl	$4,$7,3
andi	$20,$7,0x7
addu	$8,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($8)  
lwr $12, 0($8)  

# 0 "" 2
#NO_APP
srl	$8,$12,8
sll	$12,$12,8
and	$8,$8,$16
and	$12,$12,$15
or	$12,$8,$12
sll	$8,$12,16
srl	$4,$12,16
or	$4,$8,$4
sll	$4,$4,$20
srl	$3,$4,$3
addu	$2,$3,$2
sll	$3,$2,2
addu	$3,$19,$3
lh	$2,0($3)
lh	$3,2($3)
addu	$7,$3,$7
.set	noreorder
.set	nomacro
b	$L278
sw	$7,8($5)
.set	macro
.set	reorder

$L312:
lw	$11,8($5)
lw	$2,0($5)
srl	$3,$11,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$9,$3,8
lw	$2,%got(chroma_dc_coeff_token_vlc+4)($28)
sll	$10,$3,8
li	$3,16711680			# 0xff0000
addiu	$3,$3,255
lw	$12,%lo(chroma_dc_coeff_token_vlc+4)($2)
andi	$2,$11,0x7
and	$8,$9,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$10,$3
or	$3,$8,$3
sll	$8,$3,16
srl	$3,$3,16
or	$3,$8,$3
sll	$2,$3,$2
srl	$2,$2,24
sll	$2,$2,2
addu	$2,$12,$2
lh	$3,2($2)
lh	$8,0($2)
addu	$11,$11,$3
sra	$14,$8,2
.set	noreorder
.set	nomacro
b	$L242
sw	$11,8($5)
.set	macro
.set	reorder

$L313:
lbu	$3,11188($4)
lbu	$2,11195($4)
addu	$2,$2,$3
slt	$3,$2,64
beq	$3,$0,$L244
addiu	$2,$2,1
sra	$2,$2,1
$L244:
lw	$8,%got(coeff_token_table_index.7710)($28)
andi	$2,$2,0x1f
lw	$9,8($5)
sll	$2,$2,2
lw	$10,0($5)
li	$11,16711680			# 0xff0000
addiu	$8,$8,%lo(coeff_token_table_index.7710)
srl	$3,$9,3
addu	$2,$2,$8
addu	$3,$10,$3
li	$13,-16777216			# 0xffffffffff000000
lw	$12,0($2)
addiu	$11,$11,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sll	$8,$3,8
srl	$2,$2,8
ori	$13,$13,0xff00
and	$3,$2,$11
and	$2,$8,$13
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
sll	$8,$12,4
or	$2,$3,$2
lw	$3,%got(coeff_token_vlc)($28)
andi	$12,$9,0x7
sll	$2,$2,$12
addiu	$3,$3,%lo(coeff_token_vlc)
srl	$2,$2,24
addu	$3,$8,$3
sll	$2,$2,2
lw	$14,4($3)
addu	$2,$14,$2
lh	$12,2($2)
.set	noreorder
.set	nomacro
bltz	$12,$L326
lh	$8,0($2)
.set	macro
.set	reorder

$L246:
addu	$9,$12,$9
sra	$14,$8,2
.set	noreorder
.set	nomacro
b	$L242
sw	$9,8($5)
.set	macro
.set	reorder

$L296:
.set	noreorder
.set	nomacro
b	$L271
move	$10,$0
.set	macro
.set	reorder

$L316:
addiu	$2,$2,-100
li	$3,8			# 0x8
.set	noreorder
.set	nomacro
beq	$2,$3,$L327
srl	$3,$12,3
.set	macro
.set	reorder

$L255:
slt	$3,$2,14
.set	noreorder
.set	nomacro
beq	$3,$0,$L256
li	$3,14			# 0xe
.set	macro
.set	reorder

bne	$8,$0,$L328
$L257:
li	$3,3			# 0x3
bne	$4,$3,$L293
$L262:
addiu	$8,$2,2
andi	$2,$2,0x1
subu	$9,$0,$2
sra	$8,$8,1
sll	$7,$4,2
addiu	$3,$sp,32
xor	$8,$8,$9
addu	$2,$2,$8
addu	$7,$3,$7
li	$11,2			# 0x2
.set	noreorder
.set	nomacro
b	$L263
sw	$2,0($7)
.set	macro
.set	reorder

$L314:
addiu	$10,$10,8
srl	$14,$10,3
andi	$2,$10,0x7
addu	$11,$11,$14
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $16, 3($11)  
lwr $16, 0($11)  

# 0 "" 2
#NO_APP
srl	$14,$16,8
sll	$16,$16,8
and	$12,$14,$12
and	$13,$16,$13
or	$12,$12,$13
sll	$14,$12,16
srl	$11,$12,16
or	$11,$14,$11
sll	$11,$11,$2
srl	$3,$11,$3
addu	$2,$3,$8
sll	$2,$2,2
addu	$2,$15,$2
lh	$8,0($2)
lh	$3,2($2)
sra	$14,$8,2
addu	$10,$3,$10
sw	$10,8($5)
.set	noreorder
.set	nomacro
b	$L242
sb	$14,11184($9)
.set	macro
.set	reorder

$L326:
addiu	$9,$9,8
srl	$3,$9,3
andi	$2,$9,0x7
addu	$10,$10,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($10)  
lwr $15, 0($10)  

# 0 "" 2
#NO_APP
srl	$10,$15,8
sll	$15,$15,8
and	$11,$10,$11
and	$13,$15,$13
or	$11,$11,$13
sll	$10,$11,16
srl	$3,$11,16
or	$3,$10,$3
sll	$3,$3,$2
srl	$2,$3,$12
addu	$2,$2,$8
sll	$2,$2,2
addu	$2,$14,$2
lh	$8,0($2)
.set	noreorder
.set	nomacro
b	$L246
lh	$12,2($2)
.set	macro
.set	reorder

$L318:
srl	$3,$2,3
andi	$8,$2,0x7
addu	$3,$15,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$7,8
sll	$7,$7,8
and	$4,$4,$19
and	$7,$7,$18
or	$7,$4,$7
sll	$4,$7,16
srl	$7,$7,16
or	$3,$4,$7
sll	$3,$3,$8
ori	$3,$3,0x1
clz	$3,$3
addiu	$4,$3,1
addiu	$3,$3,8
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L267
sw	$2,8($5)
.set	macro
.set	reorder

$L297:
li	$11,1			# 0x1
sll	$3,$11,2
$L332:
addiu	$2,$sp,32
addu	$3,$2,$3
$L282:
addiu	$9,$9,-1
lbu	$2,0($9)
lw	$5,0($3)
addiu	$11,$11,1
addiu	$3,$3,4
sll	$2,$2,1
slt	$4,$11,$14
addu	$2,$6,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L282
sh	$5,0($2)
.set	macro
.set	reorder

bgez	$10,$L292
.set	noreorder
.set	nomacro
b	$L330
lw	$3,100($sp)
.set	macro
.set	reorder

$L256:
.set	noreorder
.set	nomacro
bne	$2,$3,$L259
li	$9,16711680			# 0xff0000
.set	macro
.set	reorder

beq	$8,$0,$L260
lw	$3,8($5)
srl	$7,$3,3
andi	$8,$3,0x7
addu	$10,$10,$7
addiu	$3,$3,1
lbu	$2,0($10)
sw	$3,8($5)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,28
$L293:
.set	noreorder
.set	nomacro
b	$L262
addiu	$2,$2,2
.set	macro
.set	reorder

$L259:
lw	$8,8($5)
addiu	$9,$9,255
srl	$3,$8,3
addu	$10,$10,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($10)  
lwr $7, 0($10)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
and	$3,$3,$9
li	$9,-16777216			# 0xffffffffff000000
ori	$9,$9,0xff00
and	$7,$7,$9
or	$7,$3,$7
sll	$3,$7,16
srl	$7,$7,16
addiu	$9,$2,-3
or	$3,$3,$7
andi	$7,$8,0x7
sll	$3,$3,$7
li	$7,35			# 0x23
addu	$8,$9,$8
subu	$7,$7,$2
srl	$3,$3,$7
sw	$8,8($5)
li	$7,15			# 0xf
.set	noreorder
.set	nomacro
beq	$2,$7,$L294
addiu	$3,$3,30
.set	macro
.set	reorder

slt	$2,$2,29
.set	noreorder
.set	nomacro
beq	$2,$0,$L329
lw	$7,100($sp)
.set	macro
.set	reorder

li	$2,1			# 0x1
sll	$2,$2,$9
addiu	$2,$2,-4096
.set	noreorder
.set	nomacro
b	$L257
addu	$2,$2,$3
.set	macro
.set	reorder

$L328:
lw	$7,8($5)
sll	$2,$2,1
srl	$8,$7,3
andi	$9,$7,0x7
addu	$10,$10,$8
addiu	$7,$7,1
lbu	$3,0($10)
sw	$7,8($5)
sll	$3,$3,$9
andi	$3,$3,0x00ff
srl	$3,$3,7
addu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L262
addiu	$2,$2,2
.set	macro
.set	reorder

$L323:
lw	$3,100($sp)
$L330:
li	$5,16			# 0x10
lw	$6,%got($LC2)($28)
lw	$25,%call16(av_log)($28)
lw	$2,7996($3)
addiu	$6,$6,%lo($LC2)
lw	$4,0($3)
lw	$7,7992($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L307
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L327:
andi	$13,$12,0x7
addu	$3,$10,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($3)  
lwr $11, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$11,8
sll	$11,$11,8
and	$9,$3,$9
and	$7,$11,$7
or	$7,$9,$7
sll	$3,$7,16
srl	$7,$7,16
or	$2,$3,$7
sll	$2,$2,$13
ori	$2,$2,0x1
clz	$2,$2
addiu	$3,$2,1
addiu	$2,$2,8
addu	$12,$3,$12
.set	noreorder
.set	nomacro
b	$L255
sw	$12,8($5)
.set	macro
.set	reorder

$L294:
.set	noreorder
.set	nomacro
b	$L257
move	$2,$3
.set	macro
.set	reorder

$L260:
lw	$7,8($5)
li	$8,16711680			# 0xff0000
addiu	$8,$8,255
srl	$2,$7,3
addu	$10,$10,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($10)  
lwr $3, 0($10)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$3,$3,$8
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$7,0x7
or	$2,$3,$2
sll	$2,$2,$8
addiu	$7,$7,4
srl	$2,$2,28
sw	$7,8($5)
.set	noreorder
.set	nomacro
b	$L257
addiu	$2,$2,14
.set	macro
.set	reorder

$L315:
lw	$3,100($sp)
li	$5,16			# 0x10
lw	$2,7996($4)
lw	$6,%got($LC0)($28)
lw	$4,0($4)
lw	$7,7992($3)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC0)
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$14,20($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L307
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L329:
li	$5,16			# 0x10
lw	$6,%got($LC1)($28)
lw	$25,%call16(av_log)($28)
lw	$4,0($7)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC1)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L307
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	decode_residual
.size	decode_residual, .-decode_residual
.section	.text.unlikely.ff_h264_decode_init_vlc,"ax",@progbits
.align	2
.globl	ff_h264_decode_init_vlc
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_init_vlc
.type	ff_h264_decode_init_vlc, @function
ff_h264_decode_init_vlc:
.frame	$sp,104,$31		# vars= 0, regs= 10/0, args= 56, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(done.7678)($28)
addiu	$sp,$sp,-104
.cprestore	56
sw	$31,100($sp)
sw	$fp,96($sp)
sw	$23,92($sp)
sw	$22,88($sp)
sw	$21,84($sp)
sw	$20,80($sp)
sw	$19,76($sp)
sw	$18,72($sp)
sw	$17,68($sp)
sw	$16,64($sp)
lw	$3,%lo(done.7678)($2)
bne	$3,$0,$L350
lw	$31,100($sp)

li	$8,1			# 0x1
lw	$4,%got(chroma_dc_coeff_token_bits)($28)
lw	$3,%got(chroma_dc_coeff_token_vlc)($28)
li	$9,4			# 0x4
lw	$7,%got(chroma_dc_coeff_token_len)($28)
li	$5,8			# 0x8
sw	$8,%lo(done.7678)($2)
addiu	$4,$4,%lo(chroma_dc_coeff_token_bits)
lw	$2,%got(chroma_dc_coeff_token_vlc_table)($28)
addiu	$3,$3,%lo(chroma_dc_coeff_token_vlc)
lw	$25,%call16(init_vlc_sparse)($28)
li	$6,20			# 0x14
sw	$4,24($sp)
addiu	$7,$7,%lo(chroma_dc_coeff_token_len)
addiu	$2,$2,%lo(chroma_dc_coeff_token_vlc_table)
sw	$8,16($sp)
sw	$8,20($sp)
move	$4,$3
sw	$8,28($sp)
move	$17,$0
sw	$8,32($sp)
move	$23,$0
sw	$0,36($sp)
li	$18,1			# 0x1
sw	$0,40($sp)
sw	$0,44($sp)
sw	$9,48($sp)
sw	$2,4($3)
li	$2,256			# 0x100
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,12($3)

lw	$28,56($sp)
lw	$16,%got(coeff_token_vlc)($28)
lw	$22,%got(coeff_token_vlc_tables)($28)
lw	$21,%got(coeff_token_vlc_tables_size)($28)
lw	$20,%got(coeff_token_len)($28)
addiu	$16,$16,%lo(coeff_token_vlc)
lw	$19,%got(coeff_token_bits)($28)
addiu	$22,$22,%lo(coeff_token_vlc_tables)
addiu	$21,$21,%lo(coeff_token_vlc_tables_size)
addiu	$20,$20,%lo(coeff_token_len)
addiu	$19,$19,%lo(coeff_token_bits)
$L335:
sll	$2,$23,2
sw	$18,16($sp)
sll	$7,$17,4
sw	$18,20($sp)
addu	$2,$22,$2
sw	$18,28($sp)
addu	$7,$7,$17
sw	$18,32($sp)
sw	$0,36($sp)
addu	$3,$21,$17
sw	$0,40($sp)
addu	$4,$19,$7
sw	$2,4($16)
li	$2,4			# 0x4
sw	$0,44($sp)
li	$5,8			# 0x8
sw	$4,24($sp)
move	$4,$16
sw	$2,48($sp)
addiu	$16,$16,16
lw	$2,0($3)
li	$6,68			# 0x44
lw	$25,%call16(init_vlc_sparse)($28)
addu	$7,$20,$7
addiu	$17,$17,4
addu	$23,$23,$2
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,-4($16)

li	$2,16			# 0x10
bne	$17,$2,$L335
lw	$28,56($sp)

lw	$16,%got(chroma_dc_total_zeros_vlc)($28)
move	$17,$0
lw	$21,%got(chroma_dc_total_zeros_vlc_tables)($28)
li	$22,8			# 0x8
lw	$20,%got(chroma_dc_total_zeros_len)($28)
li	$18,1			# 0x1
lw	$19,%got(chroma_dc_total_zeros_bits)($28)
addiu	$16,$16,%lo(chroma_dc_total_zeros_vlc)
addiu	$21,$21,%lo(chroma_dc_total_zeros_vlc_tables)
addiu	$20,$20,%lo(chroma_dc_total_zeros_len)
addiu	$19,$19,%lo(chroma_dc_total_zeros_bits)
$L336:
sll	$2,$17,3
lw	$25,%call16(init_vlc_sparse)($28)
addu	$3,$19,$17
sw	$22,12($16)
addu	$2,$21,$2
sw	$18,16($sp)
sw	$18,20($sp)
addu	$7,$20,$17
sw	$3,24($sp)
li	$5,3			# 0x3
sw	$2,4($16)
li	$2,4			# 0x4
li	$6,4			# 0x4
sw	$18,28($sp)
move	$4,$16
sw	$18,32($sp)
sw	$0,36($sp)
addiu	$17,$17,4
sw	$0,40($sp)
addiu	$16,$16,16
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

li	$2,12			# 0xc
bne	$17,$2,$L336
lw	$28,56($sp)

lw	$17,%got(total_zeros_vlc)($28)
move	$16,$0
lw	$22,%got(total_zeros_vlc_tables)($28)
li	$21,512			# 0x200
lw	$20,%got(total_zeros_len)($28)
li	$18,1			# 0x1
lw	$19,%got(total_zeros_bits)($28)
addiu	$17,$17,%lo(total_zeros_vlc)
addiu	$22,$22,%lo(total_zeros_vlc_tables)
addiu	$20,$20,%lo(total_zeros_len)
addiu	$19,$19,%lo(total_zeros_bits)
$L337:
sll	$2,$16,7
lw	$25,%call16(init_vlc_sparse)($28)
addu	$3,$19,$16
sw	$21,12($17)
addu	$2,$22,$2
sw	$18,16($sp)
sw	$18,20($sp)
addu	$7,$20,$16
sw	$3,24($sp)
li	$5,9			# 0x9
sw	$2,4($17)
li	$2,4			# 0x4
li	$6,16			# 0x10
sw	$18,28($sp)
move	$4,$17
sw	$18,32($sp)
sw	$0,36($sp)
addiu	$16,$16,16
sw	$0,40($sp)
addiu	$17,$17,16
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

li	$2,240			# 0xf0
bne	$16,$2,$L337
lw	$28,56($sp)

lw	$17,%got(run_vlc)($28)
move	$16,$0
lw	$22,%got(run_vlc_tables)($28)
li	$21,8			# 0x8
lw	$20,%got(run_len)($28)
li	$18,1			# 0x1
lw	$19,%got(run_bits)($28)
addiu	$17,$17,%lo(run_vlc)
addiu	$22,$22,%lo(run_vlc_tables)
addiu	$20,$20,%lo(run_len)
addiu	$19,$19,%lo(run_bits)
$L338:
sll	$2,$16,1
lw	$25,%call16(init_vlc_sparse)($28)
addu	$3,$19,$16
sw	$21,12($17)
addu	$2,$22,$2
sw	$18,16($sp)
li	$fp,4			# 0x4
sw	$18,20($sp)
addu	$7,$20,$16
sw	$3,24($sp)
li	$5,3			# 0x3
sw	$2,4($17)
li	$6,7			# 0x7
sw	$18,28($sp)
move	$4,$17
sw	$18,32($sp)
sw	$0,36($sp)
addiu	$16,$16,16
sw	$0,40($sp)
li	$23,1			# 0x1
sw	$0,44($sp)
addiu	$17,$17,16
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$fp,48($sp)

li	$2,96			# 0x60
bne	$16,$2,$L338
lw	$28,56($sp)

lw	$3,%got(run_bits+96)($28)
li	$5,6			# 0x6
lw	$2,%got(run7_vlc)($28)
li	$6,16			# 0x10
lw	$7,%got(run_len+96)($28)
addiu	$3,$3,%lo(run_bits+96)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(run7_vlc)
sw	$23,16($sp)
addiu	$7,$7,%lo(run_len+96)
sw	$23,20($sp)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(run7_vlc_table)($28)
sw	$23,28($sp)
sw	$23,32($sp)
addiu	$3,$3,%lo(run7_vlc_table)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$fp,48($sp)
sw	$3,4($2)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,12($2)

move	$7,$0
lw	$28,56($sp)
li	$24,1			# 0x1
li	$10,8			# 0x8
li	$13,108			# 0x6c
li	$9,30			# 0x1e
lw	$14,%got(cavlc_level_tab)($28)
li	$8,256			# 0x100
li	$15,7			# 0x7
addiu	$14,$14,%lo(cavlc_level_tab)
$L339:
sll	$6,$7,9
sll	$12,$24,$7
addu	$6,$14,$6
move	$5,$0
addiu	$11,$7,1
$L343:
sll	$2,$5,1
ori	$2,$2,0x1
clz	$2,$2
addiu	$4,$2,-22
subu	$3,$9,$2
addu	$4,$4,$7
subu	$3,$3,$7
slt	$4,$4,9
srl	$3,$5,$3
beq	$4,$0,$L340
addiu	$2,$2,-23

sll	$4,$2,$7
addu	$2,$2,$11
addu	$3,$3,$4
subu	$3,$3,$12
sb	$2,1($6)
addiu	$2,$3,2
andi	$3,$3,0x1
subu	$4,$0,$3
sra	$2,$2,1
xor	$2,$4,$2
addu	$3,$3,$2
b	$L341
sb	$3,0($6)

$L340:
beq	$2,$10,$L342
andi	$3,$2,0x00ff

addiu	$2,$3,100
addiu	$3,$3,1
sb	$2,0($6)
b	$L341
sb	$3,1($6)

$L342:
sb	$13,0($6)
sb	$10,1($6)
$L341:
addiu	$5,$5,1
bne	$5,$8,$L343
addiu	$6,$6,2

addiu	$7,$7,1
bne	$7,$15,$L339
lw	$31,100($sp)

$L350:
lw	$fp,96($sp)
lw	$23,92($sp)
lw	$22,88($sp)
lw	$21,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
j	$31
addiu	$sp,$sp,104

.set	macro
.set	reorder
.end	ff_h264_decode_init_vlc
.size	ff_h264_decode_init_vlc, .-ff_h264_decode_init_vlc
.section	.rodata.str1.4
.align	2
$LC3:
.ascii	"mb_type %d in %c slice too large at %d %d\012\000"
.align	2
$LC4:
.ascii	"B sub_mb_type %u out of range at %d %d\012\000"
.align	2
$LC5:
.ascii	"P sub_mb_type %u out of range at %d %d\012\000"
.align	2
$LC6:
.ascii	"ref %u overflow\012\000"
.align	2
$LC7:
.ascii	"cbp too large (%u) at %d %d\012\000"
.align	2
$LC8:
.ascii	"dquant out of range (%d) at %d %d\012\000"
.section	.text.ff_h264_decode_mb_cavlc,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_mb_cavlc
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_mb_cavlc
.type	ff_h264_decode_mb_cavlc, @function
ff_h264_decode_mb_cavlc:
.frame	$sp,264,$31		# vars= 184, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$8,7996($4)
addiu	$sp,$sp,-264
lw	$5,168($4)
li	$3,65536			# 0x10000
sw	$fp,256($sp)
move	$fp,$4
addu	$3,$4,$3
lw	$4,7992($4)
mul	$6,$8,$5
lw	$9,12888($fp)
li	$2,131072			# 0x20000
.cprestore	32
sw	$31,260($sp)
sw	$23,252($sp)
addu	$2,$fp,$2
sw	$22,248($sp)
sw	$21,244($sp)
sw	$20,240($sp)
sw	$19,236($sp)
sw	$18,232($sp)
sw	$17,228($sp)
sw	$16,224($sp)
lw	$7,-5260($3)
addu	$3,$6,$4
sw	$9,124($sp)
sw	$3,96($sp)
sw	$3,9448($2)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$7,$3,$L352
li	$3,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,8000($fp)
.set	noreorder
.set	nomacro
beq	$2,$3,$L786
li	$5,16711680			# 0xff0000
.set	macro
.set	reorder

$L353:
addiu	$4,$2,-1
.set	noreorder
.set	nomacro
beq	$2,$0,$L352
sw	$4,8000($fp)
.set	macro
.set	reorder

li	$3,65536			# 0x10000
addu	$3,$fp,$3
lw	$2,-5252($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L356
andi	$2,$8,0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L854
lw	$10,96($sp)
.set	macro
.set	reorder

bne	$4,$0,$L854
lw	$4,10340($fp)
lw	$5,10332($fp)
srl	$2,$4,3
andi	$6,$4,0x7
addu	$5,$5,$2
addiu	$4,$4,1
lbu	$2,0($5)
sw	$4,10340($fp)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,-5248($3)
sw	$2,-5244($3)
$L356:
lw	$10,96($sp)
$L854:
move	$5,$0
lw	$2,11232($fp)
li	$6,32			# 0x20
lw	$25,%call16(memset)($28)
sll	$4,$10,5
sw	$0,88($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$2,$4
.set	macro
.set	reorder

move	$5,$0
lw	$28,32($sp)
addiu	$4,$fp,11192
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,40			# 0x28
.set	macro
.set	reorder

li	$2,65536			# 0x10000
lw	$28,32($sp)
addu	$2,$fp,$2
lw	$2,-5248($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L358
lw	$5,88($sp)
.set	macro
.set	reorder

ori	$5,$5,0x80
$L358:
li	$2,65536			# 0x10000
li	$3,3			# 0x3
addu	$2,$fp,$2
lw	$4,-5260($2)
.set	noreorder
.set	nomacro
beq	$4,$3,$L787
lw	$25,%got(fill_decode_neighbors)($28)
.set	macro
.set	reorder

ori	$2,$5,0x3808
move	$5,$2
sw	$2,88($sp)
addiu	$25,$25,%lo(fill_decode_neighbors)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fill_decode_neighbors
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

move	$4,$fp
lw	$28,32($sp)
lw	$25,%got(fill_decode_caches)($28)
addiu	$25,$25,%lo(fill_decode_caches)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fill_decode_caches
1:	jalr	$25
lw	$5,88($sp)
.set	macro
.set	reorder

li	$4,-2			# 0xfffffffffffffffe
lb	$2,11572($fp)
lw	$28,32($sp)
.set	noreorder
.set	nomacro
beq	$2,$4,$L625
lb	$3,11579($fp)
.set	macro
.set	reorder

beq	$3,$4,$L625
lw	$4,11264($fp)
or	$2,$2,$4
.set	noreorder
.set	nomacro
beq	$2,$0,$L362
move	$2,$0
.set	macro
.set	reorder

lw	$2,11292($fp)
or	$3,$3,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L788
addiu	$2,$sp,72
.set	macro
.set	reorder

$L625:
move	$2,$0
$L362:
lw	$11,88($sp)
sw	$0,11580($fp)
sw	$0,11588($fp)
sw	$0,11596($fp)
sw	$0,11604($fp)
sw	$2,11296($fp)
sw	$2,11300($fp)
sw	$2,11304($fp)
sw	$2,11308($fp)
sw	$2,11328($fp)
sw	$2,11332($fp)
sw	$2,11336($fp)
sw	$2,11340($fp)
sw	$2,11360($fp)
sw	$2,11364($fp)
sw	$2,11368($fp)
sw	$2,11372($fp)
sw	$2,11392($fp)
sw	$2,11396($fp)
sw	$2,11400($fp)
.set	noreorder
.set	nomacro
b	$L361
sw	$2,11404($fp)
.set	macro
.set	reorder

$L352:
li	$6,65536			# 0x10000
addu	$6,$fp,$6
lw	$2,-5252($6)
.set	noreorder
.set	nomacro
bne	$2,$0,$L369
andi	$2,$8,0x1
.set	macro
.set	reorder

$L371:
lw	$5,10332($fp)
lw	$8,10340($fp)
$L370:
srl	$2,$8,3
sw	$0,10752($fp)
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$5,$9,8
addiu	$2,$2,255
sll	$6,$9,8
and	$4,$5,$2
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$2,$6,$2
or	$2,$4,$2
sll	$4,$2,16
srl	$3,$2,16
andi	$2,$8,0x7
or	$3,$4,$3
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L789
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$5,$3
li	$3,31			# 0x1f
addiu	$4,$8,32
subu	$3,$3,$5
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$4,$3
addiu	$2,$2,-1
sw	$3,10340($fp)
li	$3,3			# 0x3
sw	$2,88($sp)
.set	noreorder
.set	nomacro
beq	$7,$3,$L790
move	$16,$2
.set	macro
.set	reorder

$L374:
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$7,$3,$L791
sltu	$3,$2,5
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$3,-5264($2)
li	$2,5			# 0x5
beq	$3,$2,$L792
$L377:
sltu	$2,$16,26
.set	noreorder
.set	nomacro
beq	$2,$0,$L793
li	$2,65536			# 0x10000
.set	macro
.set	reorder

lw	$2,%got(i_mb_type_info)($28)
$L862:
sll	$16,$16,2
move	$17,$0
addiu	$2,$2,%lo(i_mb_type_info)
addu	$16,$16,$2
lbu	$2,2($16)
lhu	$5,0($16)
lbu	$16,3($16)
sw	$2,10764($fp)
sw	$5,88($sp)
sw	$16,108($sp)
$L376:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$2,-5248($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L855
li	$2,65536			# 0x10000
.set	macro
.set	reorder

ori	$5,$5,0x80
sw	$5,88($sp)
$L855:
lw	$12,96($sp)
andi	$3,$5,0x4
addu	$2,$fp,$2
sll	$12,$12,1
lw	$4,-5268($2)
lw	$6,-5272($2)
sw	$12,120($sp)
addu	$4,$4,$12
.set	noreorder
.set	nomacro
beq	$3,$0,$L382
sh	$6,0($4)
.set	macro
.set	reorder

lw	$3,10340($fp)
subu	$2,$0,$3
andi	$2,$2,0x7
.set	noreorder
.set	nomacro
bne	$2,$0,$L794
addu	$2,$2,$3
.set	macro
.set	reorder

$L383:
li	$6,131072			# 0x20000
li	$9,16711680			# 0xff0000
addiu	$6,$6,6944
li	$8,-16777216			# 0xffffffffff000000
addu	$6,$fp,$6
move	$7,$0
li	$11,384			# 0x180
li	$10,256			# 0x100
addiu	$9,$9,255
.set	noreorder
.set	nomacro
b	$L384
ori	$8,$8,0xff00
.set	macro
.set	reorder

$L386:
lw	$5,10340($fp)
addiu	$6,$6,1
lw	$3,10332($fp)
srl	$2,$5,3
andi	$12,$5,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$9
and	$4,$4,$8
or	$2,$3,$4
sll	$3,$2,16
srl	$2,$2,16
addiu	$5,$5,8
or	$2,$3,$2
sll	$2,$2,$12
sw	$5,10340($fp)
srl	$2,$2,24
sb	$2,-1($6)
$L384:
lw	$2,11896($fp)
move	$13,$10
movn	$13,$11,$2
sltu	$2,$7,$13
.set	noreorder
.set	nomacro
bne	$2,$0,$L386
addiu	$7,$7,1
.set	macro
.set	reorder

lw	$2,2172($fp)
li	$5,16			# 0x10
lw	$14,96($sp)
li	$6,32			# 0x20
lw	$25,%call16(memset)($28)
addu	$2,$2,$14
sll	$3,$14,5
sb	$0,0($2)
lw	$4,11232($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$3
.set	macro
.set	reorder

move	$2,$0
lw	$17,96($sp)
lw	$3,2192($fp)
sll	$4,$17,2
addu	$3,$3,$4
lw	$4,88($sp)
sw	$4,0($3)
$L672:
lw	$31,260($sp)
lw	$fp,256($sp)
lw	$23,252($sp)
lw	$22,248($sp)
lw	$21,244($sp)
lw	$20,240($sp)
lw	$19,236($sp)
lw	$18,232($sp)
lw	$17,228($sp)
lw	$16,224($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

$L369:
bne	$2,$0,$L371
lw	$4,10340($fp)
lw	$5,10332($fp)
srl	$9,$4,3
andi	$3,$4,0x7
addu	$9,$5,$9
addiu	$8,$4,1
lbu	$2,0($9)
sw	$8,10340($fp)
sll	$2,$2,$3
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,-5248($6)
.set	noreorder
.set	nomacro
b	$L370
sw	$2,-5244($6)
.set	macro
.set	reorder

$L382:
lw	$3,-5244($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L856
lw	$25,%got(fill_decode_neighbors)($28)
.set	macro
.set	reorder

lw	$4,6624($2)
lw	$3,6628($2)
sll	$4,$4,1
sll	$3,$3,1
sw	$4,6624($2)
sw	$3,6628($2)
$L856:
addiu	$25,$25,%lo(fill_decode_neighbors)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fill_decode_neighbors
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

move	$4,$fp
lw	$28,32($sp)
lw	$25,%got(fill_decode_caches)($28)
addiu	$25,$25,%lo(fill_decode_caches)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fill_decode_caches
1:	jalr	$25
lw	$5,88($sp)
.set	macro
.set	reorder

lw	$23,88($sp)
lw	$28,32($sp)
andi	$2,$23,0x7
.set	noreorder
.set	nomacro
beq	$2,$0,$L388
move	$6,$23
.set	macro
.set	reorder

andi	$2,$23,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L389
lw	$18,124($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$18,$0,$L390
li	$9,1			# 0x1
.set	macro
.set	reorder

lw	$6,10332($fp)
lw	$15,10340($fp)
$L391:
lw	$10,%got(scan8)($28)
li	$14,16711680			# 0xff0000
li	$13,-16777216			# 0xffffffffff000000
li	$12,2			# 0x2
addiu	$10,$10,%lo(scan8)
addiu	$14,$14,255
li	$11,4			# 0x4
move	$7,$10
ori	$13,$13,0xff00
$L397:
lbu	$5,0($7)
srl	$2,$15,3
andi	$3,$15,0x7
addu	$2,$6,$2
addu	$16,$fp,$5
addiu	$8,$15,1
lbu	$4,0($2)
lb	$2,10808($16)
lb	$16,10815($16)
sll	$3,$4,$3
sw	$8,10340($fp)
slt	$4,$16,$2
movn	$2,$16,$4
andi	$3,$3,0x00ff
srl	$3,$3,7
slt	$4,$2,0
.set	noreorder
.set	nomacro
beq	$3,$0,$L795
movn	$2,$12,$4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$9,$11,$L857
sll	$3,$2,8
.set	macro
.set	reorder

$L394:
lbu	$3,0($7)
addu	$7,$7,$9
addu	$3,$fp,$3
sb	$2,10816($3)
subu	$2,$7,$10
slt	$2,$2,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L858
lw	$25,%call16(ff_h264_write_back_intra_pred_mode)($28)
.set	macro
.set	reorder

$L802:
lw	$6,10332($fp)
.set	noreorder
.set	nomacro
b	$L397
lw	$15,10340($fp)
.set	macro
.set	reorder

$L789:
lw	$10,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$16,%got(ff_ue_golomb_vlc_code)($28)
addu	$3,$10,$2
addu	$2,$16,$2
lbu	$4,0($3)
li	$3,3			# 0x3
lbu	$2,0($2)
addu	$4,$4,$8
move	$16,$2
sw	$2,88($sp)
.set	noreorder
.set	nomacro
bne	$7,$3,$L374
sw	$4,10340($fp)
.set	macro
.set	reorder

$L790:
sltu	$3,$2,23
.set	noreorder
.set	nomacro
beq	$3,$0,$L375
lw	$3,%got(b_mb_type_info)($28)
.set	macro
.set	reorder

sll	$2,$2,2
sw	$0,108($sp)
addiu	$3,$3,%lo(b_mb_type_info)
addu	$2,$2,$3
lhu	$5,0($2)
lbu	$17,2($2)
.set	noreorder
.set	nomacro
b	$L376
sw	$5,88($sp)
.set	macro
.set	reorder

$L787:
lw	$2,5140($2)
ori	$5,$5,0x5908
.set	noreorder
.set	nomacro
beq	$2,$0,$L360
sw	$5,88($sp)
.set	macro
.set	reorder

lw	$25,%got(fill_decode_neighbors)($28)
addiu	$25,$25,%lo(fill_decode_neighbors)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fill_decode_neighbors
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

move	$4,$fp
lw	$28,32($sp)
lw	$25,%got(fill_decode_caches)($28)
addiu	$25,$25,%lo(fill_decode_caches)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fill_decode_caches
1:	jalr	$25
lw	$5,88($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
$L360:
lw	$25,%call16(pred_direct_motion_hw)($28)
addiu	$5,$sp,88
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$11,88($sp)
ori	$11,$11,0x800
sw	$11,88($sp)
$L361:
lw	$2,7996($fp)
li	$5,131072			# 0x20000
lw	$12,11864($fp)
andi	$3,$11,0x3000
addu	$5,$fp,$5
lw	$4,7992($fp)
sll	$2,$2,2
lw	$14,9448($5)
mul	$5,$2,$12
sll	$4,$4,2
sll	$14,$14,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L363
addu	$12,$5,$4
.set	macro
.set	reorder

lw	$2,2276($fp)
li	$3,-1			# 0xffffffffffffffff
addu	$2,$2,$14
sh	$3,0($2)
sh	$3,2($2)
$L363:
li	$10,65536			# 0x10000
addu	$10,$fp,$10
lw	$2,6632($10)
.set	noreorder
.set	nomacro
beq	$2,$0,$L364
addiu	$5,$fp,11580
.set	macro
.set	reorder

sll	$12,$12,2
addiu	$9,$fp,2276
addiu	$8,$fp,11296
move	$4,$0
li	$13,12288			# 0x3000
$L367:
sll	$2,$4,1
sll	$2,$13,$2
and	$2,$2,$11
.set	noreorder
.set	nomacro
beq	$2,$0,$L365
move	$2,$8
.set	macro
.set	reorder

lw	$15,11864($fp)
addiu	$16,$8,128
lw	$3,-92($9)
sll	$15,$15,2
addu	$3,$3,$12
$L366:
lw	$6,0($2)
addiu	$2,$2,32
lw	$7,-28($2)
sw	$6,0($3)
sw	$7,4($3)
lw	$6,-24($2)
lw	$7,-20($2)
sw	$6,8($3)
sw	$7,12($3)
.set	noreorder
.set	nomacro
bne	$16,$2,$L366
addu	$3,$3,$15
.set	macro
.set	reorder

lw	$2,0($9)
lb	$3,0($5)
addu	$2,$2,$14
sb	$3,0($2)
lb	$3,2($5)
sb	$3,1($2)
lb	$3,16($5)
sb	$3,2($2)
lb	$3,18($5)
sb	$3,3($2)
$L365:
lw	$2,6632($10)
addiu	$4,$4,1
addiu	$9,$9,4
addiu	$5,$5,40
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L367
addiu	$8,$8,160
.set	macro
.set	reorder

lw	$11,88($sp)
$L364:
lw	$8,96($sp)
lw	$2,2192($fp)
lw	$4,2172($fp)
sll	$3,$8,2
sll	$6,$8,1
addu	$2,$2,$3
li	$3,65536			# 0x10000
sw	$11,0($2)
addu	$4,$4,$8
lw	$5,2872($fp)
addu	$3,$fp,$3
move	$2,$0
sb	$5,0($4)
lw	$4,-5268($3)
lw	$5,-5272($3)
lw	$31,260($sp)
addu	$3,$4,$6
lw	$23,252($sp)
lw	$22,248($sp)
lw	$21,244($sp)
lw	$20,240($sp)
lw	$19,236($sp)
lw	$18,232($sp)
lw	$17,228($sp)
lw	$16,224($sp)
sh	$5,0($3)
li	$3,1			# 0x1
sw	$3,10752($fp)
lw	$fp,256($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

$L794:
.set	noreorder
.set	nomacro
b	$L383
sw	$2,10340($fp)
.set	macro
.set	reorder

$L388:
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$17,$2,$L797
andi	$2,$23,0x100
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L798
andi	$2,$23,0x8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L799
li	$2,65536			# 0x10000
.set	macro
.set	reorder

andi	$2,$23,0x10
.set	noreorder
.set	nomacro
bne	$2,$0,$L501
li	$2,65536			# 0x10000
.set	macro
.set	reorder

addu	$3,$fp,$2
lw	$18,6632($3)
.set	noreorder
.set	nomacro
beq	$18,$0,$L402
addiu	$2,$2,6624
.set	macro
.set	reorder

lw	$24,%got(ff_golomb_vlc_len)($28)
li	$21,16711680			# 0xff0000
lw	$22,%got(ff_ue_golomb_vlc_code)($28)
li	$20,-16777216			# 0xffffffffff000000
addu	$10,$fp,$2
move	$8,$0
li	$15,4096			# 0x1000
li	$12,1			# 0x1
li	$19,2			# 0x2
addiu	$21,$21,255
move	$17,$fp
move	$11,$fp
.set	noreorder
.set	nomacro
b	$L547
ori	$20,$20,0xff00
.set	macro
.set	reorder

$L801:
addiu	$8,$8,1
addiu	$11,$11,40
.set	noreorder
.set	nomacro
beq	$8,$18,$L800
addiu	$10,$10,4
.set	macro
.set	reorder

$L547:
sll	$5,$8,1
move	$16,$0
move	$3,$11
$L546:
addu	$4,$16,$5
sll	$4,$15,$4
and	$4,$4,$23
.set	noreorder
.set	nomacro
beq	$4,$0,$L543
li	$2,65535			# 0xffff
.set	macro
.set	reorder

lw	$9,0($10)
.set	noreorder
.set	nomacro
beq	$9,$12,$L543
move	$2,$0
.set	macro
.set	reorder

bne	$9,$19,$L544
lw	$4,10340($fp)
lw	$7,10332($fp)
srl	$2,$4,3
andi	$9,$4,0x7
addu	$7,$7,$2
addiu	$4,$4,1
lbu	$2,0($7)
sw	$4,10340($fp)
sll	$2,$2,$9
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$2,$2,0x1
andi	$4,$2,0x00ff
sll	$2,$4,8
addu	$2,$2,$4
andi	$2,$2,0xffff
$L543:
addiu	$3,$3,2
sh	$2,11578($3)
sh	$2,11586($3)
sh	$2,11594($3)
.set	noreorder
.set	nomacro
beq	$16,$12,$L801
sh	$2,11602($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L546
li	$16,1			# 0x1
.set	macro
.set	reorder

$L795:
srl	$3,$8,3
andi	$8,$8,0x7
addu	$6,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($6)  
lwr $4, 0($6)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$6,$4,8
and	$4,$3,$14
and	$3,$6,$13
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
addiu	$15,$15,4
or	$3,$4,$3
sll	$8,$3,$8
sw	$15,10340($fp)
srl	$8,$8,29
slt	$2,$8,$2
xori	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$9,$11,$L394
addu	$2,$2,$8
.set	macro
.set	reorder

sll	$3,$2,8
$L857:
addiu	$5,$5,10816
addu	$2,$2,$3
addu	$5,$fp,$5
andi	$2,$2,0xffff
addu	$7,$7,$9
sh	$2,0($5)
sh	$2,8($5)
subu	$2,$7,$10
slt	$2,$2,16
.set	noreorder
.set	nomacro
bne	$2,$0,$L802
lw	$25,%call16(ff_h264_write_back_intra_pred_mode)($28)
.set	macro
.set	reorder

$L858:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_write_back_intra_pred_mode
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$25,%call16(ff_h264_check_intra4x4_pred_mode)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra4x4_pred_mode
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
lw	$28,32($sp)
.set	macro
.set	reorder

$L401:
lw	$2,11896($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L803
lw	$6,88($sp)
.set	macro
.set	reorder

$L402:
andi	$2,$6,0x78
.set	noreorder
.set	nomacro
bne	$2,$0,$L804
li	$5,131072			# 0x20000
.set	macro
.set	reorder

$L564:
andi	$2,$6,0x2
beq	$2,$0,$L805
$L569:
lw	$8,124($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L859
lw	$10,96($sp)
.set	macro
.set	reorder

lw	$9,108($sp)
andi	$2,$9,0xf
.set	noreorder
.set	nomacro
beq	$2,$0,$L860
li	$2,131072			# 0x20000
.set	macro
.set	reorder

andi	$2,$6,0x7
beq	$2,$0,$L806
lw	$12,10332($fp)
$L578:
lw	$9,96($sp)
li	$2,131072			# 0x20000
lw	$4,2192($fp)
addu	$2,$fp,$2
lw	$10,120($sp)
sll	$3,$9,2
lw	$13,108($sp)
addu	$3,$4,$3
lw	$4,8732($2)
addu	$4,$4,$10
sh	$13,0($4)
sw	$13,8736($2)
sw	$6,0($3)
.set	noreorder
.set	nomacro
b	$L580
lw	$9,88($sp)
.set	macro
.set	reorder

$L791:
.set	noreorder
.set	nomacro
beq	$3,$0,$L379
addiu	$16,$2,-5
.set	macro
.set	reorder

lw	$3,%got(p_mb_type_info)($28)
sll	$2,$2,2
sw	$0,108($sp)
addiu	$3,$3,%lo(p_mb_type_info)
addu	$2,$2,$3
lhu	$5,0($2)
lbu	$17,2($2)
.set	noreorder
.set	nomacro
b	$L376
sw	$5,88($sp)
.set	macro
.set	reorder

$L859:
li	$2,131072			# 0x20000
$L860:
lw	$4,2192($fp)
addu	$2,$fp,$2
lw	$12,120($sp)
sll	$3,$10,2
lw	$13,108($sp)
addu	$3,$4,$3
lw	$4,8732($2)
addu	$4,$4,$12
sh	$13,0($4)
sw	$13,8736($2)
sw	$6,0($3)
.set	noreorder
.set	nomacro
bne	$13,$0,$L581
lw	$23,88($sp)
.set	macro
.set	reorder

andi	$2,$23,0x2
bne	$2,$0,$L581
sw	$0,11196($fp)
sw	$0,11204($fp)
sw	$0,11212($fp)
sw	$0,11220($fp)
$L752:
sb	$0,11226($fp)
sb	$0,11225($fp)
sb	$0,11218($fp)
sb	$0,11217($fp)
sb	$0,11202($fp)
sb	$0,11201($fp)
sb	$0,11194($fp)
sb	$0,11193($fp)
$L618:
lw	$4,2172($fp)
li	$2,131072			# 0x20000
lw	$8,96($sp)
li	$3,65536			# 0x10000
lw	$5,2872($fp)
addu	$2,$fp,$2
addu	$3,$fp,$3
addu	$4,$4,$8
sb	$5,0($4)
lw	$2,9448($2)
lw	$4,11232($fp)
lw	$6,11192($fp)
lw	$7,11196($fp)
sll	$2,$2,5
addu	$4,$4,$2
sw	$6,0($4)
sw	$7,4($4)
lw	$4,11232($fp)
lw	$6,11200($fp)
lw	$7,11204($fp)
addu	$4,$4,$2
sw	$6,8($4)
sw	$7,12($4)
lw	$4,11232($fp)
lw	$5,11224($fp)
addu	$4,$4,$2
sw	$5,16($4)
lw	$4,11232($fp)
lw	$5,11212($fp)
addu	$4,$4,$2
sw	$5,20($4)
lw	$4,11232($fp)
lw	$6,11216($fp)
lw	$7,11220($fp)
addu	$2,$4,$2
sw	$6,24($2)
sw	$7,28($2)
lw	$2,-5244($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L672
lw	$31,260($sp)
.set	macro
.set	reorder

lw	$5,6624($3)
move	$2,$0
lw	$4,6628($3)
srl	$5,$5,1
lw	$fp,256($sp)
srl	$4,$4,1
lw	$23,252($sp)
lw	$22,248($sp)
lw	$21,244($sp)
lw	$20,240($sp)
lw	$19,236($sp)
lw	$18,232($sp)
lw	$17,228($sp)
lw	$16,224($sp)
sw	$5,6624($3)
sw	$4,6628($3)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

$L581:
lw	$12,10332($fp)
move	$9,$23
$L580:
li	$7,131072			# 0x20000
lw	$8,2872($fp)
andi	$4,$9,0x7
addu	$6,$fp,$7
andi	$3,$9,0x80
lw	$5,6928($6)
lw	$17,6932($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L584
movn	$17,$5,$4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$8,$0,$L585
lw	$10,%got(luma_dc_field_scan)($28)
.set	macro
.set	reorder

addiu	$2,$7,9356
addiu	$7,$7,9276
addu	$2,$fp,$2
addu	$18,$fp,$7
addiu	$10,$10,%lo(luma_dc_field_scan)
sw	$2,104($sp)
$L586:
lw	$11,10340($fp)
li	$4,16711680			# 0xff0000
addiu	$4,$4,255
srl	$2,$11,3
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($2)  
lwr $19, 0($2)  

# 0 "" 2
#NO_APP
srl	$6,$19,8
sll	$2,$19,8
and	$3,$6,$4
li	$4,-16777216			# 0xffffffffff000000
andi	$6,$11,0x7
ori	$4,$4,0xff00
and	$4,$2,$4
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$6
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L807
ori	$4,$2,0x1
.set	macro
.set	reorder

clz	$6,$4
li	$4,31			# 0x1f
addiu	$3,$11,32
subu	$4,$4,$6
sll	$4,$4,1
addiu	$4,$4,-31
subu	$3,$3,$4
srl	$2,$2,$4
andi	$4,$2,0x1
.set	noreorder
.set	nomacro
beq	$4,$0,$L590
sw	$3,10340($fp)
.set	macro
.set	reorder

srl	$2,$2,1
subu	$7,$0,$2
$L589:
addu	$2,$7,$8
sltu	$3,$2,52
bne	$3,$0,$L592
bltz	$2,$L808
addiu	$2,$2,-52
sw	$2,2872($fp)
$L594:
sltu	$3,$2,52
.set	noreorder
.set	nomacro
beq	$3,$0,$L809
lw	$6,%got($LC8)($28)
.set	macro
.set	reorder

$L595:
addu	$3,$fp,$2
andi	$9,$9,0x2
lbu	$4,13116($3)
lbu	$3,13372($3)
sw	$4,10740($fp)
.set	noreorder
.set	nomacro
bne	$9,$0,$L810
sw	$3,10744($fp)
.set	macro
.set	reorder

lw	$9,104($sp)
li	$16,131072			# 0x20000
move	$21,$0
addiu	$16,$16,6944
addiu	$9,$9,64
move	$22,$0
addu	$16,$fp,$16
sw	$9,100($sp)
$L609:
li	$10,1			# 0x1
lw	$12,108($sp)
sll	$2,$10,$21
and	$2,$2,$12
.set	noreorder
.set	nomacro
beq	$2,$0,$L602
lw	$3,%got(scan8)($28)
.set	macro
.set	reorder

lw	$2,88($sp)
li	$4,16777216			# 0x1000000
and	$4,$2,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L603
lw	$5,%got(decode_residual)($28)
.set	macro
.set	reorder

lw	$13,%got(decode_residual)($28)
move	$20,$22
lw	$23,104($sp)
addiu	$19,$13,%lo(decode_residual)
andi	$2,$2,0x7
$L869:
lw	$10,2872($fp)
sltu	$2,$2,1
sw	$23,16($sp)
addiu	$2,$2,15062
li	$14,16			# 0x10
sll	$2,$2,2
sll	$10,$10,8
addu	$2,$fp,$2
sw	$14,24($sp)
move	$7,$20
move	$4,$fp
lw	$2,0($2)
move	$5,$17
move	$6,$16
move	$25,$19
addu	$2,$2,$10
addiu	$23,$23,16
addiu	$20,$20,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_residual
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$3,100($sp)
.set	noreorder
.set	nomacro
beq	$23,$3,$L604
lw	$2,88($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L869
andi	$2,$2,0x7
.set	macro
.set	reorder

$L602:
addiu	$20,$22,4
addiu	$3,$3,%lo(scan8)
addu	$2,$3,$22
lbu	$4,0($2)
addiu	$2,$4,11184
addu	$4,$fp,$4
addu	$2,$fp,$2
sb	$0,9($2)
sb	$0,8($2)
sb	$0,1($2)
sb	$0,11184($4)
$L606:
addiu	$21,$21,1
li	$2,4			# 0x4
move	$22,$20
.set	noreorder
.set	nomacro
bne	$21,$2,$L609
addiu	$16,$16,128
.set	macro
.set	reorder

$L600:
lw	$8,108($sp)
andi	$2,$8,0x30
.set	noreorder
.set	nomacro
beq	$2,$0,$L861
lw	$9,108($sp)
.set	macro
.set	reorder

lw	$20,%got(chroma_dc_scan)($28)
li	$19,131072			# 0x20000
lw	$16,%got(decode_residual)($28)
li	$21,4			# 0x4
addiu	$6,$19,7456
sw	$0,20($sp)
addiu	$20,$20,%lo(chroma_dc_scan)
addiu	$16,$16,%lo(decode_residual)
sw	$21,24($sp)
addu	$6,$fp,$6
li	$7,26			# 0x1a
sw	$20,16($sp)
move	$4,$fp
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_residual
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
li	$7,26			# 0x1a
.set	macro
.set	reorder

addiu	$6,$19,7584
sw	$20,16($sp)
sw	$0,20($sp)
addu	$6,$fp,$6
sw	$21,24($sp)
move	$4,$fp
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_residual
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$9,108($sp)
$L861:
andi	$2,$9,0x20
.set	noreorder
.set	nomacro
beq	$2,$0,$L752
li	$20,131072			# 0x20000
.set	macro
.set	reorder

lw	$16,%got(decode_residual)($28)
li	$23,20			# 0x14
addiu	$20,$20,7456
addiu	$16,$16,%lo(decode_residual)
addu	$20,$fp,$20
addiu	$18,$18,1
move	$2,$0
sw	$16,100($sp)
li	$21,15			# 0xf
sw	$20,104($sp)
move	$22,$23
addiu	$20,$2,1
$L853:
lw	$2,88($sp)
li	$3,3			# 0x3
lw	$16,104($sp)
addu	$4,$fp,$22
andi	$2,$2,0x7
movn	$3,$0,$2
addiu	$19,$22,-4
lw	$4,10720($4)
addu	$2,$3,$20
sll	$4,$4,6
addiu	$2,$2,15056
sll	$2,$2,2
addu	$2,$fp,$2
lw	$23,0($2)
addu	$23,$23,$4
$L616:
lw	$25,100($sp)
move	$7,$19
move	$6,$16
sw	$18,16($sp)
sw	$23,20($sp)
move	$4,$fp
sw	$21,24($sp)
.set	noreorder
.set	nomacro
jalr	$25
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
addiu	$19,$19,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$19,$22,$L616
addiu	$16,$16,32
.set	macro
.set	reorder

lw	$3,104($sp)
li	$4,2			# 0x2
addiu	$22,$22,4
li	$2,1			# 0x1
addiu	$3,$3,128
.set	noreorder
.set	nomacro
beq	$20,$4,$L618
sw	$3,104($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L853
addiu	$20,$2,1
.set	macro
.set	reorder

$L603:
addiu	$20,$22,4
move	$23,$22
move	$22,$16
addiu	$19,$5,%lo(decode_residual)
li	$6,3			# 0x3
$L870:
lw	$11,2872($fp)
andi	$2,$2,0x7
sw	$18,16($sp)
movn	$6,$0,$2
li	$8,16			# 0x10
sll	$11,$11,6
move	$7,$23
sw	$8,24($sp)
move	$4,$fp
move	$2,$6
addiu	$2,$2,15056
move	$6,$22
sll	$2,$2,2
move	$5,$17
addu	$2,$fp,$2
move	$25,$19
addiu	$23,$23,1
lw	$2,0($2)
addiu	$22,$22,32
addu	$2,$2,$11
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_residual
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
lw	$28,32($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$23,$20,$L606
lw	$2,88($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L870
li	$6,3			# 0x3
.set	macro
.set	reorder

$L419:
srl	$7,$5,3
andi	$15,$5,0x7
addu	$3,$3,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$7
sll	$3,$3,8
srl	$7,$7,8
and	$3,$3,$24
and	$7,$7,$25
or	$3,$7,$3
sll	$7,$3,16
srl	$3,$3,16
or	$3,$7,$3
sll	$3,$3,$15
srl	$3,$3,23
addu	$7,$13,$3
addu	$3,$16,$3
lbu	$7,0($7)
lbu	$3,0($3)
addu	$5,$7,$5
sltu	$7,$3,$8
.set	noreorder
.set	nomacro
bne	$7,$0,$L418
sw	$5,10340($fp)
.set	macro
.set	reorder

move	$7,$3
$L545:
lw	$6,%got($LC6)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

$L753:
li	$2,-1			# 0xffffffffffffffff
$L817:
lw	$31,260($sp)
lw	$fp,256($sp)
lw	$23,252($sp)
lw	$22,248($sp)
lw	$21,244($sp)
lw	$20,240($sp)
lw	$19,236($sp)
lw	$18,232($sp)
lw	$17,228($sp)
lw	$16,224($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

$L584:
.set	noreorder
.set	nomacro
beq	$8,$0,$L587
addiu	$2,$7,9212
.set	macro
.set	reorder

lw	$10,%got(luma_dc_zigzag_scan)($28)
addiu	$7,$7,9132
addu	$2,$fp,$2
addu	$18,$fp,$7
addiu	$10,$10,%lo(luma_dc_zigzag_scan)
.set	noreorder
.set	nomacro
b	$L586
sw	$2,104($sp)
.set	macro
.set	reorder

$L592:
.set	noreorder
.set	nomacro
b	$L595
sw	$2,2872($fp)
.set	macro
.set	reorder

$L786:
lw	$4,10340($fp)
lw	$2,10332($fp)
addiu	$5,$5,255
srl	$3,$4,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$3,$3,$5
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$5,$4,0x7
or	$2,$3,$2
sll	$2,$2,$5
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L811
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$5,$3
li	$3,31			# 0x1f
addiu	$4,$4,32
subu	$3,$3,$5
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$4,$3
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
b	$L353
sw	$3,10340($fp)
.set	macro
.set	reorder

$L604:
lw	$4,%got(scan8)($28)
addiu	$20,$22,4
addiu	$4,$4,%lo(scan8)
addu	$2,$4,$22
lbu	$5,0($2)
addiu	$6,$5,11184
addu	$5,$fp,$5
addu	$6,$fp,$6
lbu	$7,11184($5)
lbu	$4,8($6)
lbu	$8,1($6)
lbu	$2,9($6)
addu	$4,$4,$8
addu	$2,$4,$2
addu	$2,$2,$7
.set	noreorder
.set	nomacro
b	$L606
sb	$2,11184($5)
.set	macro
.set	reorder

$L792:
.set	noreorder
.set	nomacro
beq	$16,$0,$L862
lw	$2,%got(i_mb_type_info)($28)
.set	macro
.set	reorder

addiu	$16,$16,-1
.set	noreorder
.set	nomacro
b	$L377
sw	$16,88($sp)
.set	macro
.set	reorder

$L375:
addiu	$16,$2,-23
.set	noreorder
.set	nomacro
b	$L377
sw	$16,88($sp)
.set	macro
.set	reorder

$L807:
lw	$4,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$3,0($4)
lb	$7,0($2)
addu	$3,$3,$11
.set	noreorder
.set	nomacro
b	$L589
sw	$3,10340($fp)
.set	macro
.set	reorder

$L804:
lw	$2,7996($fp)
lw	$12,11864($fp)
andi	$3,$6,0x3000
addu	$5,$fp,$5
lw	$4,7992($fp)
sll	$2,$2,2
lw	$14,9448($5)
mul	$5,$2,$12
sll	$4,$4,2
sll	$14,$14,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L812
addu	$12,$5,$4
.set	macro
.set	reorder

$L565:
li	$11,65536			# 0x10000
addu	$11,$fp,$11
lw	$2,6632($11)
.set	noreorder
.set	nomacro
beq	$2,$0,$L564
sll	$12,$12,2
.set	macro
.set	reorder

addiu	$10,$fp,2276
addiu	$8,$fp,11580
addiu	$9,$fp,11296
move	$7,$0
li	$13,12288			# 0x3000
$L568:
sll	$2,$7,1
sll	$2,$13,$2
and	$2,$2,$6
.set	noreorder
.set	nomacro
beq	$2,$0,$L566
move	$2,$9
.set	macro
.set	reorder

lw	$15,11864($fp)
addiu	$16,$9,128
lw	$3,-92($10)
sll	$15,$15,2
addu	$3,$3,$12
$L567:
lw	$4,0($2)
addiu	$2,$2,32
lw	$5,-28($2)
sw	$4,0($3)
sw	$5,4($3)
lw	$4,-24($2)
lw	$5,-20($2)
sw	$4,8($3)
sw	$5,12($3)
.set	noreorder
.set	nomacro
bne	$16,$2,$L567
addu	$3,$3,$15
.set	macro
.set	reorder

lw	$2,0($10)
lb	$3,0($8)
addu	$2,$2,$14
sb	$3,0($2)
lb	$3,2($8)
sb	$3,1($2)
lb	$3,16($8)
sb	$3,2($2)
lb	$3,18($8)
sb	$3,3($2)
$L566:
lw	$2,6632($11)
addiu	$7,$7,1
addiu	$10,$10,4
addiu	$8,$8,40
sltu	$2,$7,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L568
addiu	$9,$9,160
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L564
lw	$6,88($sp)
.set	macro
.set	reorder

$L805:
lw	$5,10340($fp)
li	$4,16711680			# 0xff0000
lw	$2,10332($fp)
addiu	$4,$4,255
srl	$3,$5,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$3,$3,$4
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$5,0x7
or	$2,$3,$2
sll	$2,$2,$4
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L813
ori	$4,$2,0x1
.set	macro
.set	reorder

clz	$7,$4
li	$4,31			# 0x1f
addiu	$3,$5,32
subu	$4,$4,$7
sll	$4,$4,1
addiu	$4,$4,-31
srl	$2,$2,$4
subu	$3,$3,$4
addiu	$7,$2,-1
sw	$3,10340($fp)
$L571:
sltu	$2,$7,48
.set	noreorder
.set	nomacro
beq	$2,$0,$L814
li	$5,16			# 0x10
.set	macro
.set	reorder

lw	$2,11896($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L573
andi	$2,$6,0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L574
lw	$2,%got(golomb_to_inter_cbp)($28)
.set	macro
.set	reorder

lw	$2,%got(golomb_to_intra4x4_cbp)($28)
addiu	$2,$2,%lo(golomb_to_intra4x4_cbp)
addu	$7,$7,$2
lbu	$7,0($7)
.set	noreorder
.set	nomacro
b	$L569
sw	$7,108($sp)
.set	macro
.set	reorder

$L379:
.set	noreorder
.set	nomacro
b	$L377
sw	$16,88($sp)
.set	macro
.set	reorder

$L590:
.set	noreorder
.set	nomacro
b	$L589
srl	$7,$2,1
.set	macro
.set	reorder

$L810:
li	$19,65536			# 0x10000
lw	$16,%got(decode_residual)($28)
sw	$10,16($sp)
li	$21,131072			# 0x20000
addu	$19,$fp,$19
sll	$2,$2,6
addiu	$20,$21,6944
lw	$3,-5312($19)
addiu	$16,$16,%lo(decode_residual)
addu	$20,$fp,$20
li	$7,25			# 0x19
addu	$2,$3,$2
li	$3,16			# 0x10
move	$4,$fp
sw	$2,20($sp)
move	$6,$20
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_residual
1:	jalr	$25
sw	$3,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$3,108($sp)
andi	$2,$3,0xf
.set	noreorder
.set	nomacro
beq	$2,$0,$L597
addiu	$2,$21,6976
.set	macro
.set	reorder

sw	$20,100($sp)
addiu	$3,$18,1
sw	$17,116($sp)
addu	$2,$fp,$2
sw	$18,112($sp)
move	$20,$0
li	$23,15			# 0xf
addu	$21,$fp,$21
sw	$2,104($sp)
move	$22,$19
move	$17,$fp
move	$19,$3
move	$fp,$20
$L598:
lw	$6,100($sp)
addiu	$18,$fp,4
lw	$20,104($sp)
$L601:
lw	$2,2872($17)
move	$7,$fp
lw	$8,-5312($22)
move	$4,$17
lw	$5,6928($21)
move	$25,$16
sll	$2,$2,6
sw	$19,16($sp)
sw	$23,24($sp)
addiu	$fp,$fp,1
addu	$2,$8,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_residual
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

move	$6,$20
lw	$28,32($sp)
.set	noreorder
.set	nomacro
bltz	$2,$L753
addiu	$20,$20,32
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$fp,$18,$L601
lw	$3,100($sp)
.set	macro
.set	reorder

li	$2,16			# 0x10
lw	$8,104($sp)
addiu	$3,$3,128
addiu	$8,$8,128
sw	$3,100($sp)
.set	noreorder
.set	nomacro
bne	$fp,$2,$L598
sw	$8,104($sp)
.set	macro
.set	reorder

move	$fp,$17
lw	$18,112($sp)
.set	noreorder
.set	nomacro
b	$L600
lw	$17,116($sp)
.set	macro
.set	reorder

$L390:
lw	$15,10340($fp)
lw	$6,10332($fp)
srl	$3,$15,3
andi	$4,$15,0x7
addu	$3,$6,$3
addiu	$15,$15,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L627
sw	$15,10340($fp)
.set	macro
.set	reorder

li	$2,16777216			# 0x1000000
li	$9,4			# 0x4
or	$2,$23,$2
.set	noreorder
.set	nomacro
b	$L391
sw	$2,88($sp)
.set	macro
.set	reorder

$L587:
lw	$18,9428($6)
lw	$10,%got(luma_dc_zigzag_scan)($28)
sw	$18,104($sp)
addiu	$10,$10,%lo(luma_dc_zigzag_scan)
.set	noreorder
.set	nomacro
b	$L586
lw	$18,9420($6)
.set	macro
.set	reorder

$L585:
lw	$14,9440($6)
lw	$18,9432($6)
sw	$14,104($sp)
.set	noreorder
.set	nomacro
b	$L586
addiu	$10,$10,%lo(luma_dc_field_scan)
.set	macro
.set	reorder

$L812:
lw	$2,2276($fp)
li	$3,-1			# 0xffffffffffffffff
addu	$2,$2,$14
sh	$3,0($2)
.set	noreorder
.set	nomacro
b	$L565
sh	$3,2($2)
.set	macro
.set	reorder

$L799:
addu	$9,$fp,$2
lw	$3,6632($9)
.set	noreorder
.set	nomacro
beq	$3,$0,$L402
addiu	$2,$2,6624
.set	macro
.set	reorder

lw	$16,%got(ff_golomb_vlc_len)($28)
li	$14,16711680			# 0xff0000
lw	$15,%got(ff_ue_golomb_vlc_code)($28)
li	$13,-16777216			# 0xffffffffff000000
addiu	$5,$fp,11580
addu	$8,$fp,$2
move	$4,$0
li	$10,4096			# 0x1000
li	$11,1			# 0x1
li	$12,2			# 0x2
addiu	$14,$14,255
.set	noreorder
.set	nomacro
b	$L474
ori	$13,$13,0xff00
.set	macro
.set	reorder

$L816:
lw	$3,10340($fp)
lw	$7,10332($fp)
srl	$2,$3,3
andi	$17,$3,0x7
addu	$7,$7,$2
addiu	$3,$3,1
lbu	$2,0($7)
sw	$3,10340($fp)
sll	$2,$2,$17
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$3,$2,0x1
sll	$2,$3,8
addu	$2,$2,$3
sll	$3,$2,16
addu	$2,$2,$3
$L470:
sw	$2,0($5)
sw	$2,8($5)
sw	$2,16($5)
sw	$2,24($5)
$L469:
lw	$2,6632($9)
addiu	$4,$4,1
addiu	$5,$5,40
sltu	$3,$4,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L815
addiu	$8,$8,4
.set	macro
.set	reorder

$L474:
sll	$2,$4,1
sll	$2,$10,$2
and	$2,$2,$23
beq	$2,$0,$L469
lw	$17,0($8)
.set	noreorder
.set	nomacro
beq	$17,$11,$L470
move	$2,$0
.set	macro
.set	reorder

beq	$17,$12,$L816
lw	$18,10340($fp)
lw	$2,10332($fp)
srl	$3,$18,3
andi	$19,$18,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
and	$3,$3,$14
and	$7,$7,$13
or	$7,$3,$7
sll	$3,$7,16
srl	$7,$7,16
or	$2,$3,$7
sll	$2,$2,$19
srl	$2,$2,23
addu	$3,$16,$2
addu	$2,$15,$2
lbu	$3,0($3)
lbu	$7,0($2)
addu	$18,$3,$18
sll	$2,$7,8
sltu	$17,$7,$17
addu	$2,$2,$7
.set	noreorder
.set	nomacro
beq	$17,$0,$L545
sw	$18,10340($fp)
.set	macro
.set	reorder

sll	$3,$2,16
.set	noreorder
.set	nomacro
b	$L470
addu	$2,$2,$3
.set	macro
.set	reorder

$L573:
.set	noreorder
.set	nomacro
beq	$2,$0,$L575
lw	$2,%got(golomb_to_inter_cbp_gray)($28)
.set	macro
.set	reorder

lw	$2,%got(golomb_to_intra4x4_cbp_gray)($28)
addiu	$2,$2,%lo(golomb_to_intra4x4_cbp_gray)
addu	$7,$7,$2
lbu	$7,0($7)
.set	noreorder
.set	nomacro
b	$L569
sw	$7,108($sp)
.set	macro
.set	reorder

$L806:
lw	$3,10340($fp)
lw	$12,10332($fp)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$12,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,10340($fp)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sll	$2,$2,24
or	$6,$6,$2
.set	noreorder
.set	nomacro
b	$L578
sw	$6,88($sp)
.set	macro
.set	reorder

$L597:
sw	$0,11196($fp)
sw	$0,11204($fp)
sw	$0,11212($fp)
.set	noreorder
.set	nomacro
b	$L600
sw	$0,11220($fp)
.set	macro
.set	reorder

$L788:
lw	$25,%got(pred_motion)($28)
move	$5,$0
sw	$0,16($sp)
li	$6,4			# 0x4
sw	$2,20($sp)
addiu	$2,$sp,40
move	$7,$0
addiu	$25,$25,%lo(pred_motion)
sw	$2,24($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,pred_motion
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$3,40($sp)
lhu	$2,72($sp)
sll	$3,$3,16
.set	noreorder
.set	nomacro
b	$L362
addu	$2,$2,$3
.set	macro
.set	reorder

$L627:
.set	noreorder
.set	nomacro
b	$L391
li	$9,1			# 0x1
.set	macro
.set	reorder

$L544:
lw	$13,10340($fp)
lw	$2,10332($fp)
srl	$4,$13,3
andi	$14,$13,0x7
addu	$2,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$7,$4,8
and	$2,$2,$21
and	$7,$7,$20
or	$7,$2,$7
sll	$4,$7,16
srl	$7,$7,16
or	$2,$4,$7
sll	$2,$2,$14
srl	$2,$2,23
addu	$4,$24,$2
addu	$2,$22,$2
lbu	$4,0($4)
lbu	$7,0($2)
addu	$13,$4,$13
sll	$2,$7,8
sltu	$9,$7,$9
addu	$2,$7,$2
.set	noreorder
.set	nomacro
beq	$9,$0,$L545
sw	$13,10340($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L543
andi	$2,$2,0xffff
.set	macro
.set	reorder

$L803:
lw	$6,10340($fp)
li	$4,16711680			# 0xff0000
lw	$2,10332($fp)
addiu	$4,$4,255
lw	$5,%got(ff_golomb_vlc_len)($28)
srl	$3,$6,3
lw	$25,%call16(ff_h264_check_intra_pred_mode)($28)
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$3,$3,$4
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$6,0x7
or	$2,$3,$2
lw	$3,%got(ff_ue_golomb_vlc_code)($28)
sll	$2,$2,$4
move	$4,$fp
srl	$2,$2,23
addu	$5,$5,$2
addu	$2,$3,$2
lbu	$3,0($5)
lbu	$5,0($2)
addu	$2,$3,$6
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra_pred_mode
1:	jalr	$25
sw	$2,10340($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L753
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$6,88($sp)
.set	noreorder
.set	nomacro
b	$L402
sw	$2,10760($fp)
.set	macro
.set	reorder

$L389:
lw	$25,%call16(ff_h264_check_intra_pred_mode)($28)
move	$4,$fp
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra_pred_mode
1:	jalr	$25
lw	$5,10764($fp)
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
bgez	$2,$L401
sw	$2,10764($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L817
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L797:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$3,-5260($2)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L818
lw	$2,%got(ff_golomb_vlc_len)($28)
.set	macro
.set	reorder

addiu	$25,$sp,72
lw	$9,%got(p_sub_mb_type_info)($28)
li	$4,60296			# 0xeb88
li	$11,60304			# 0xeb90
lw	$12,10332($fp)
li	$10,16711680			# 0xff0000
lw	$5,10340($fp)
li	$8,-16777216			# 0xffffffffff000000
lw	$16,%got(ff_ue_golomb_vlc_code)($28)
addu	$4,$fp,$4
sw	$25,168($sp)
addu	$11,$fp,$11
sw	$2,140($sp)
addiu	$10,$10,255
addiu	$9,$9,%lo(p_sub_mb_type_info)
move	$6,$25
ori	$8,$8,0xff00
move	$13,$2
$L413:
srl	$3,$5,3
andi	$14,$5,0x7
addu	$3,$12,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
and	$3,$3,$10
and	$7,$7,$8
or	$7,$3,$7
sll	$3,$7,16
srl	$7,$7,16
or	$2,$3,$7
sll	$2,$2,$14
srl	$2,$2,23
addu	$3,$13,$2
addu	$2,$16,$2
lbu	$3,0($3)
lbu	$7,0($2)
addu	$5,$3,$5
sll	$2,$7,2
andi	$7,$7,0xffff
sltu	$3,$7,4
sw	$5,10340($fp)
addu	$2,$2,$9
.set	noreorder
.set	nomacro
beq	$3,$0,$L819
sh	$7,0($4)
.set	macro
.set	reorder

lhu	$3,0($2)
addiu	$4,$4,2
lbu	$2,2($2)
addiu	$6,$6,4
sh	$3,-2($4)
.set	noreorder
.set	nomacro
bne	$4,$11,$L413
sw	$2,-4($6)
.set	macro
.set	reorder

$L409:
li	$2,65536			# 0x10000
$L866:
addu	$3,$fp,$2
lw	$18,6632($3)
.set	noreorder
.set	nomacro
beq	$18,$0,$L820
addiu	$2,$2,6624
.set	macro
.set	reorder

lw	$13,140($sp)
addiu	$3,$sp,40
addu	$14,$fp,$2
li	$17,60296			# 0xeb88
li	$9,60304			# 0xeb90
sw	$3,136($sp)
li	$2,16711680			# 0xff0000
li	$24,-16777216			# 0xffffffffff000000
move	$10,$0
li	$20,4096			# 0x1000
addu	$17,$fp,$17
addu	$9,$fp,$9
li	$21,-1			# 0xffffffffffffffff
li	$22,2			# 0x2
addiu	$25,$2,255
andi	$19,$23,0x200
move	$12,$3
ori	$24,$24,0xff00
$L422:
.set	noreorder
.set	nomacro
bne	$19,$0,$L415
li	$8,1			# 0x1
.set	macro
.set	reorder

lw	$8,0($14)
$L415:
sll	$6,$10,1
li	$11,1			# 0x1
sll	$6,$20,$6
move	$2,$17
move	$4,$12
$L421:
lhu	$3,0($2)
andi	$5,$3,0x100
.set	noreorder
.set	nomacro
bne	$5,$0,$L416
and	$3,$3,$6
.set	macro
.set	reorder

beq	$3,$0,$L417
beq	$8,$11,$L630
lw	$5,10340($fp)
.set	noreorder
.set	nomacro
bne	$8,$22,$L419
lw	$3,10332($fp)
.set	macro
.set	reorder

srl	$15,$5,3
andi	$7,$5,0x7
addu	$3,$3,$15
addiu	$5,$5,1
lbu	$3,0($3)
sw	$5,10340($fp)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
xori	$3,$3,0x1
$L418:
sw	$3,0($4)
$L416:
addiu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$9,$L421
addiu	$4,$4,4
.set	macro
.set	reorder

addiu	$10,$10,1
addiu	$12,$12,16
.set	noreorder
.set	nomacro
bne	$10,$18,$L422
addiu	$14,$14,4
.set	macro
.set	reorder

lw	$8,124($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L863
lw	$2,%got(scan8)($28)
.set	macro
.set	reorder

li	$3,65536			# 0x10000
$L868:
lw	$2,11956($fp)
addu	$3,$fp,$3
.set	noreorder
.set	nomacro
beq	$2,$0,$L424
lw	$4,-5240($3)
.set	macro
.set	reorder

lw	$2,-5236($3)
li	$3,7340032			# 0x700000
addiu	$3,$3,112
or	$2,$2,$4
and	$2,$2,$3
sltu	$2,$2,1
sw	$2,124($sp)
$L425:
.set	noreorder
.set	nomacro
beq	$18,$0,$L426
addiu	$9,$sp,40
.set	macro
.set	reorder

sw	$9,136($sp)
lw	$2,%got(scan8)($28)
$L863:
addiu	$17,$fp,11579
addiu	$14,$fp,11288
sw	$0,112($sp)
li	$21,134217728			# 0x8000000
sw	$23,188($sp)
addiu	$2,$2,%lo(scan8)
sw	$17,152($sp)
sw	$14,148($sp)
sw	$2,164($sp)
li	$2,60296			# 0xeb88
addu	$2,$fp,$2
sw	$2,180($sp)
li	$2,60304			# 0xeb90
addu	$2,$fp,$2
sw	$2,128($sp)
li	$2,65536			# 0x10000
addu	$19,$fp,$2
li	$2,16711680			# 0xff0000
move	$17,$19
addiu	$20,$2,255
li	$2,-16777216			# 0xffffffffff000000
ori	$22,$2,0xff00
$L465:
lw	$18,112($sp)
li	$25,12288			# 0x3000
li	$19,4096			# 0x1000
lw	$15,180($sp)
lw	$16,164($sp)
sll	$3,$18,1
sll	$2,$18,5
sll	$19,$19,$3
sll	$3,$25,$3
sll	$8,$18,3
sll	$4,$18,2
sw	$19,144($sp)
addu	$8,$8,$2
sw	$3,172($sp)
lw	$2,168($sp)
lw	$3,136($sp)
addu	$25,$fp,$8
sw	$4,176($sp)
sw	$2,104($sp)
sw	$3,100($sp)
$L464:
lhu	$3,0($15)
andi	$2,$3,0x100
beq	$2,$0,$L427
lbu	$2,0($16)
addu	$2,$25,$2
lb	$3,11569($2)
sb	$3,11568($2)
$L428:
lw	$14,100($sp)
addiu	$15,$15,2
lw	$18,104($sp)
addiu	$16,$16,4
lw	$19,128($sp)
addiu	$14,$14,4
addiu	$18,$18,4
sw	$14,100($sp)
.set	noreorder
.set	nomacro
bne	$15,$19,$L464
sw	$18,104($sp)
.set	macro
.set	reorder

lw	$25,112($sp)
lw	$3,136($sp)
lw	$4,148($sp)
lw	$5,152($sp)
addiu	$25,$25,1
lw	$2,6632($17)
addiu	$3,$3,16
addiu	$4,$4,160
addiu	$5,$5,40
sw	$25,112($sp)
sltu	$2,$25,$2
sw	$3,136($sp)
sw	$4,148($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L465
sw	$5,152($sp)
.set	macro
.set	reorder

lw	$23,188($sp)
$L426:
.set	noreorder
.set	nomacro
b	$L402
move	$6,$23
.set	macro
.set	reorder

$L417:
.set	noreorder
.set	nomacro
b	$L416
sw	$21,0($4)
.set	macro
.set	reorder

$L427:
lw	$12,100($sp)
lbu	$2,0($16)
lw	$10,144($sp)
lb	$5,0($12)
addu	$4,$25,$2
and	$6,$3,$10
sb	$5,11577($4)
sb	$5,11576($4)
sb	$5,11569($4)
.set	noreorder
.set	nomacro
beq	$6,$0,$L429
sb	$5,11568($4)
.set	macro
.set	reorder

lw	$13,104($sp)
li	$14,1			# 0x1
li	$18,2			# 0x2
andi	$11,$3,0x18
movn	$14,$18,$11
lw	$24,0($13)
.set	noreorder
.set	nomacro
blez	$24,$L428
move	$11,$14
.set	macro
.set	reorder

andi	$4,$3,0x8
lw	$23,140($sp)
andi	$2,$3,0x10
sw	$15,156($sp)
andi	$3,$3,0x20
sw	$16,160($sp)
andi	$2,$2,0xffff
andi	$3,$3,0xffff
move	$10,$0
sw	$2,116($sp)
andi	$19,$4,0xffff
sw	$3,132($sp)
.set	noreorder
.set	nomacro
b	$L462
move	$9,$16
.set	macro
.set	reorder

$L826:
li	$7,-2			# 0xfffffffffffffffe
beq	$13,$7,$L821
$L433:
addu	$3,$8,$3
addiu	$3,$3,2812
sll	$3,$3,2
addu	$3,$fp,$3
$L437:
xor	$14,$12,$15
xor	$7,$12,$13
sltu	$14,$14,1
sltu	$7,$7,1
xor	$12,$12,$16
addu	$7,$7,$14
sltu	$12,$12,1
addu	$7,$7,$12
slt	$18,$7,2
.set	noreorder
.set	nomacro
bne	$18,$0,$L864
li	$18,1			# 0x1
.set	macro
.set	reorder

$L446:
lh	$12,0($5)
lh	$7,0($6)
slt	$14,$7,$12
.set	noreorder
.set	nomacro
beq	$14,$0,$L447
lh	$13,0($3)
.set	macro
.set	reorder

slt	$14,$7,$13
beq	$14,$0,$L448
slt	$7,$12,$13
movz	$12,$13,$7
move	$7,$12
$L448:
lh	$5,2($5)
lh	$6,2($6)
slt	$12,$6,$5
.set	noreorder
.set	nomacro
beq	$12,$0,$L449
lh	$3,2($3)
.set	macro
.set	reorder

slt	$12,$6,$3
beq	$12,$0,$L442
slt	$6,$5,$3
movz	$5,$3,$6
move	$6,$5
$L442:
lw	$12,10340($fp)
lw	$13,10332($fp)
srl	$3,$12,3
andi	$15,$12,0x7
addu	$3,$13,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($3)  
lwr $14, 0($3)  

# 0 "" 2
#NO_APP
srl	$5,$14,8
sll	$3,$14,8
and	$5,$5,$20
and	$3,$3,$22
or	$14,$5,$3
sll	$5,$14,16
srl	$14,$14,16
or	$14,$5,$14
sll	$3,$14,$15
sltu	$5,$3,$21
.set	noreorder
.set	nomacro
beq	$5,$0,$L823
li	$14,31			# 0x1f
.set	macro
.set	reorder

ori	$5,$3,0x1
clz	$5,$5
addiu	$12,$12,32
subu	$5,$14,$5
sll	$5,$5,1
addiu	$5,$5,-31
subu	$12,$12,$5
srl	$3,$3,$5
andi	$5,$3,0x1
.set	noreorder
.set	nomacro
beq	$5,$0,$L452
sw	$12,10340($fp)
.set	macro
.set	reorder

srl	$3,$3,1
subu	$5,$0,$3
$L451:
srl	$3,$12,3
andi	$14,$12,0x7
addu	$13,$13,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($13)  
lwr $3, 0($13)  

# 0 "" 2
#NO_APP
srl	$15,$3,8
sll	$3,$3,8
and	$15,$15,$20
and	$3,$3,$22
or	$13,$15,$3
sll	$3,$13,16
srl	$13,$13,16
or	$13,$3,$13
sll	$13,$13,$14
sltu	$3,$13,$21
.set	noreorder
.set	nomacro
beq	$3,$0,$L824
addu	$5,$5,$7
.set	macro
.set	reorder

ori	$3,$13,0x1
clz	$3,$3
li	$7,31			# 0x1f
addiu	$12,$12,32
subu	$3,$7,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$13,$13,$3
subu	$12,$12,$3
andi	$3,$13,0x1
srl	$13,$13,1
.set	noreorder
.set	nomacro
beq	$3,$0,$L455
sw	$12,10340($fp)
.set	macro
.set	reorder

subu	$13,$0,$13
$L455:
.set	noreorder
.set	nomacro
beq	$19,$0,$L458
addu	$6,$13,$6
.set	macro
.set	reorder

sll	$5,$5,16
sll	$6,$6,16
sra	$5,$5,16
sra	$6,$6,16
sh	$5,36($4)
sh	$5,32($4)
sh	$5,4($4)
sh	$6,38($4)
sh	$6,34($4)
sh	$6,6($4)
$L459:
addu	$2,$8,$2
addiu	$10,$10,1
addiu	$3,$2,2812
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$fp,$2
addu	$3,$fp,$3
addu	$9,$9,$11
sh	$5,0($3)
.set	noreorder
.set	nomacro
beq	$10,$24,$L825
sh	$6,11250($2)
.set	macro
.set	reorder

$L462:
lbu	$2,0($9)
lw	$18,-5252($17)
addiu	$14,$2,-8
addiu	$13,$2,-1
addu	$6,$8,$14
addu	$5,$8,$13
addu	$4,$8,$2
addu	$3,$11,$14
addiu	$4,$4,2812
addiu	$5,$5,2812
addiu	$6,$6,2812
addu	$13,$25,$13
addu	$12,$25,$2
addu	$14,$25,$14
addu	$7,$25,$3
lb	$16,11568($13)
sll	$4,$4,2
lb	$12,11568($12)
sll	$5,$5,2
lb	$15,11568($14)
sll	$6,$6,2
lb	$13,11568($7)
addu	$4,$fp,$4
addu	$5,$fp,$5
.set	noreorder
.set	nomacro
bne	$18,$0,$L826
addu	$6,$fp,$6
.set	macro
.set	reorder

li	$18,-2			# 0xfffffffffffffffe
bne	$13,$18,$L433
$L619:
addiu	$7,$2,-9
$L865:
xor	$14,$12,$15
addu	$3,$8,$7
addu	$7,$25,$7
sltu	$14,$14,1
addiu	$3,$3,2812
lb	$13,11568($7)
sll	$3,$3,2
xor	$7,$12,$13
sltu	$7,$7,1
xor	$12,$12,$16
addu	$7,$7,$14
sltu	$12,$12,1
addu	$7,$7,$12
slt	$18,$7,2
.set	noreorder
.set	nomacro
beq	$18,$0,$L446
addu	$3,$fp,$3
.set	macro
.set	reorder

li	$18,1			# 0x1
$L864:
.set	noreorder
.set	nomacro
beq	$7,$18,$L827
li	$7,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

bne	$15,$7,$L446
bne	$13,$7,$L446
beq	$16,$7,$L446
$L749:
lh	$7,0($5)
.set	noreorder
.set	nomacro
b	$L442
lh	$6,2($5)
.set	macro
.set	reorder

$L458:
lw	$12,116($sp)
.set	noreorder
.set	nomacro
beq	$12,$0,$L460
lw	$13,132($sp)
.set	macro
.set	reorder

addu	$2,$8,$2
sll	$5,$5,16
addiu	$3,$2,2812
sll	$6,$6,16
sll	$2,$2,2
sll	$3,$3,2
sra	$5,$5,16
sra	$6,$6,16
addu	$2,$fp,$2
addu	$3,$fp,$3
sh	$5,4($4)
addiu	$10,$10,1
sh	$6,6($4)
addu	$9,$9,$11
sh	$5,0($3)
.set	noreorder
.set	nomacro
bne	$10,$24,$L462
sh	$6,11250($2)
.set	macro
.set	reorder

$L825:
lw	$15,156($sp)
.set	noreorder
.set	nomacro
b	$L428
lw	$16,160($sp)
.set	macro
.set	reorder

$L449:
slt	$12,$3,$6
beq	$12,$0,$L442
slt	$6,$3,$5
movz	$5,$3,$6
.set	noreorder
.set	nomacro
b	$L442
move	$6,$5
.set	macro
.set	reorder

$L447:
slt	$14,$13,$7
beq	$14,$0,$L448
slt	$7,$13,$12
movz	$12,$13,$7
.set	noreorder
.set	nomacro
b	$L448
move	$7,$12
.set	macro
.set	reorder

$L823:
srl	$3,$3,23
lw	$18,%got(ff_se_golomb_vlc_code)($28)
addu	$5,$23,$3
addu	$3,$18,$3
lbu	$14,0($5)
lb	$5,0($3)
addu	$12,$14,$12
.set	noreorder
.set	nomacro
b	$L451
sw	$12,10340($fp)
.set	macro
.set	reorder

$L824:
srl	$13,$13,23
lw	$18,%got(ff_se_golomb_vlc_code)($28)
addu	$3,$23,$13
addu	$13,$18,$13
lbu	$3,0($3)
lb	$13,0($13)
addu	$12,$3,$12
.set	noreorder
.set	nomacro
b	$L455
sw	$12,10340($fp)
.set	macro
.set	reorder

$L452:
.set	noreorder
.set	nomacro
b	$L451
srl	$5,$3,1
.set	macro
.set	reorder

$L460:
sll	$5,$5,16
sll	$6,$6,16
sra	$5,$5,16
.set	noreorder
.set	nomacro
beq	$13,$0,$L459
sra	$6,$6,16
.set	macro
.set	reorder

sh	$5,32($4)
.set	noreorder
.set	nomacro
b	$L459
sh	$6,34($4)
.set	macro
.set	reorder

$L821:
slt	$3,$2,20
.set	noreorder
.set	nomacro
bne	$3,$0,$L619
li	$13,4			# 0x4
.set	macro
.set	reorder

andi	$3,$2,0x7
.set	noreorder
.set	nomacro
bne	$3,$13,$L619
lw	$14,152($sp)
.set	macro
.set	reorder

lb	$3,0($14)
.set	noreorder
.set	nomacro
beq	$3,$7,$L865
addiu	$7,$2,-9
.set	macro
.set	reorder

lw	$7,2696($fp)
lw	$3,148($sp)
lw	$7,104($7)
sw	$0,0($3)
sw	$7,204($sp)
lw	$7,-5248($17)
beq	$7,$0,$L828
lw	$7,10800($fp)
andi	$7,$7,0x80
.set	noreorder
.set	nomacro
bne	$7,$0,$L865
addiu	$7,$2,-9
.set	macro
.set	reorder

slt	$7,$2,36
lw	$14,204($sp)
xori	$7,$7,0x1
addiu	$7,$7,2694
sra	$13,$2,2
sll	$7,$7,2
andi	$18,$13,0x3
addu	$7,$fp,$7
sw	$18,184($sp)
lw	$7,4($7)
sll	$18,$7,2
addu	$7,$14,$18
lw	$14,172($sp)
lw	$7,0($7)
and	$7,$14,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L633
lw	$7,184($sp)
.set	macro
.set	reorder

andi	$13,$13,0x2
lw	$14,2696($fp)
sw	$13,192($sp)
lw	$13,11864($fp)
mult	$7,$13
lw	$13,11852($fp)
mflo	$7
sw	$7,184($sp)
addu	$7,$13,$18
lw	$13,176($sp)
lw	$7,0($7)
addu	$14,$14,$13
mflo	$13
sw	$7,204($sp)
addiu	$7,$13,3
lw	$13,204($sp)
addu	$7,$7,$13
lw	$13,96($14)
sll	$7,$7,2
addu	$7,$13,$7
lw	$13,148($sp)
sw	$7,204($sp)
lh	$7,0($7)
sh	$7,0($13)
lw	$7,204($sp)
lh	$13,2($7)
srl	$7,$13,31
addu	$7,$7,$13
lw	$13,148($sp)
sra	$7,$7,1
sh	$7,2($13)
lw	$7,188($14)
lw	$14,192($sp)
addu	$7,$7,$18
addu	$7,$7,$14
lb	$13,1($7)
.set	noreorder
.set	nomacro
b	$L437
sll	$13,$13,1
.set	macro
.set	reorder

$L429:
addu	$2,$8,$2
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$fp,$2
sw	$0,36($2)
sw	$0,32($2)
sw	$0,4($2)
.set	noreorder
.set	nomacro
b	$L428
sw	$0,0($2)
.set	macro
.set	reorder

$L827:
bne	$12,$0,$L749
bne	$14,$0,$L829
lh	$7,0($3)
.set	noreorder
.set	nomacro
b	$L442
lh	$6,2($3)
.set	macro
.set	reorder

$L630:
.set	noreorder
.set	nomacro
b	$L418
move	$3,$0
.set	macro
.set	reorder

$L829:
lh	$7,0($6)
.set	noreorder
.set	nomacro
b	$L442
lh	$6,2($6)
.set	macro
.set	reorder

$L811:
lw	$9,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$16,%got(ff_ue_golomb_vlc_code)($28)
addu	$3,$9,$2
addu	$2,$16,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$4,$3,$4
.set	noreorder
.set	nomacro
b	$L353
sw	$4,10340($fp)
.set	macro
.set	reorder

$L815:
.set	noreorder
.set	nomacro
beq	$2,$0,$L402
li	$13,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$20,%got(ff_golomb_vlc_len)($28)
li	$8,65536			# 0x10000
lw	$19,%got(ff_se_golomb_vlc_code)($28)
li	$12,-16777216			# 0xffffffffff000000
move	$3,$0
li	$10,4096			# 0x1000
li	$17,-2			# 0xfffffffffffffffe
addiu	$13,$13,255
li	$14,134217728			# 0x8000000
li	$18,31			# 0x1f
li	$21,1			# 0x1
addu	$8,$fp,$8
move	$4,$fp
move	$2,$fp
.set	noreorder
.set	nomacro
b	$L500
ori	$12,$12,0xff00
.set	macro
.set	reorder

$L475:
lw	$5,6632($8)
addiu	$3,$3,1
addiu	$2,$2,160
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L402
addiu	$4,$4,40
.set	macro
.set	reorder

$L500:
sll	$5,$3,1
sll	$5,$10,$5
and	$5,$5,$23
beq	$5,$0,$L475
lb	$7,11576($4)
lb	$6,11580($4)
lb	$15,11572($4)
.set	noreorder
.set	nomacro
beq	$7,$17,$L476
lb	$16,11579($4)
.set	macro
.set	reorder

addiu	$11,$2,11280
$L477:
xor	$9,$6,$15
xor	$5,$6,$7
sltu	$9,$9,1
sltu	$5,$5,1
xor	$6,$6,$16
addu	$5,$5,$9
sltu	$6,$6,1
addu	$5,$5,$6
slt	$22,$5,2
bne	$22,$0,$L830
$L487:
lh	$6,11292($2)
lh	$5,11264($2)
slt	$9,$5,$6
.set	noreorder
.set	nomacro
beq	$9,$0,$L488
lh	$7,0($11)
.set	macro
.set	reorder

slt	$9,$5,$7
beq	$9,$0,$L489
slt	$5,$6,$7
movz	$6,$7,$5
move	$5,$6
$L489:
lh	$7,11294($2)
lh	$6,11266($2)
lh	$9,2($11)
slt	$11,$6,$7
.set	noreorder
.set	nomacro
beq	$11,$0,$L490
sw	$5,72($sp)
.set	macro
.set	reorder

slt	$11,$6,$9
beq	$11,$0,$L491
slt	$6,$7,$9
movz	$7,$9,$6
move	$6,$7
$L491:
sw	$6,40($sp)
move	$9,$6
move	$15,$5
$L483:
lw	$11,10340($fp)
lw	$7,10332($fp)
srl	$6,$11,3
andi	$5,$11,0x7
addu	$6,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $16, 3($6)  
lwr $16, 0($6)  

# 0 "" 2
#NO_APP
srl	$6,$16,8
sll	$16,$16,8
and	$6,$6,$13
and	$16,$16,$12
or	$6,$6,$16
sll	$16,$6,16
srl	$6,$6,16
or	$6,$16,$6
sll	$5,$6,$5
sltu	$6,$5,$14
.set	noreorder
.set	nomacro
beq	$6,$0,$L831
ori	$6,$5,0x1
.set	macro
.set	reorder

clz	$6,$6
addiu	$11,$11,32
subu	$6,$18,$6
sll	$6,$6,1
addiu	$6,$6,-31
srl	$5,$5,$6
subu	$11,$11,$6
andi	$6,$5,0x1
srl	$5,$5,1
.set	noreorder
.set	nomacro
beq	$6,$0,$L493
sw	$11,10340($fp)
.set	macro
.set	reorder

subu	$5,$0,$5
$L493:
addu	$5,$5,$15
srl	$15,$11,3
andi	$6,$11,0x7
addu	$7,$7,$15
sw	$5,72($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($7)  
lwr $15, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$15,8
sll	$15,$15,8
and	$7,$7,$13
and	$15,$15,$12
or	$7,$7,$15
sll	$15,$7,16
srl	$7,$7,16
or	$7,$15,$7
sll	$6,$7,$6
sltu	$7,$6,$14
.set	noreorder
.set	nomacro
beq	$7,$0,$L832
ori	$7,$6,0x1
.set	macro
.set	reorder

clz	$7,$7
addiu	$11,$11,32
subu	$7,$18,$7
sll	$7,$7,1
addiu	$7,$7,-31
srl	$6,$6,$7
subu	$7,$11,$7
andi	$11,$6,0x1
srl	$6,$6,1
.set	noreorder
.set	nomacro
beq	$11,$0,$L497
sw	$7,10340($fp)
.set	macro
.set	reorder

subu	$6,$0,$6
$L497:
addu	$6,$6,$9
andi	$5,$5,0xffff
sll	$7,$6,16
sw	$6,40($sp)
move	$6,$23
addu	$5,$5,$7
sw	$5,11296($2)
sw	$5,11300($2)
sw	$5,11304($2)
sw	$5,11308($2)
sw	$5,11328($2)
sw	$5,11332($2)
sw	$5,11336($2)
sw	$5,11340($2)
sw	$5,11360($2)
sw	$5,11364($2)
sw	$5,11368($2)
sw	$5,11372($2)
sw	$5,11392($2)
sw	$5,11396($2)
sw	$5,11400($2)
.set	noreorder
.set	nomacro
b	$L475
sw	$5,11404($2)
.set	macro
.set	reorder

$L490:
slt	$11,$9,$6
beq	$11,$0,$L491
slt	$6,$9,$7
movz	$7,$9,$6
.set	noreorder
.set	nomacro
b	$L491
move	$6,$7
.set	macro
.set	reorder

$L488:
slt	$9,$7,$5
beq	$9,$0,$L489
slt	$5,$7,$6
movz	$6,$7,$5
.set	noreorder
.set	nomacro
b	$L489
move	$5,$6
.set	macro
.set	reorder

$L832:
srl	$6,$6,23
addu	$7,$20,$6
addu	$6,$19,$6
lbu	$7,0($7)
lb	$6,0($6)
addu	$11,$7,$11
.set	noreorder
.set	nomacro
b	$L497
sw	$11,10340($fp)
.set	macro
.set	reorder

$L831:
srl	$5,$5,23
addu	$6,$20,$5
addu	$5,$19,$5
lbu	$6,0($6)
lb	$5,0($5)
addu	$11,$6,$11
.set	noreorder
.set	nomacro
b	$L493
sw	$11,10340($fp)
.set	macro
.set	reorder

$L476:
lb	$7,11571($4)
.set	noreorder
.set	nomacro
b	$L477
addiu	$11,$2,11260
.set	macro
.set	reorder

$L798:
lw	$25,%call16(ff_h264_pred_direct_motion)($28)
addiu	$5,$sp,88
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_pred_direct_motion
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$8,124($sp)
lw	$2,11956($fp)
lw	$28,32($sp)
lw	$6,88($sp)
and	$8,$8,$2
.set	noreorder
.set	nomacro
b	$L402
sw	$8,124($sp)
.set	macro
.set	reorder

$L813:
lw	$4,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$3,%got(ff_ue_golomb_vlc_code)($28)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$3,0($4)
lbu	$7,0($2)
addu	$3,$3,$5
.set	noreorder
.set	nomacro
b	$L571
sw	$3,10340($fp)
.set	macro
.set	reorder

$L808:
addiu	$2,$2,52
.set	noreorder
.set	nomacro
b	$L594
sw	$2,2872($fp)
.set	macro
.set	reorder

$L830:
beq	$5,$21,$L833
bne	$15,$17,$L487
bne	$7,$17,$L487
beq	$16,$17,$L487
$L750:
lh	$15,11292($2)
lh	$9,11294($2)
sw	$15,72($sp)
.set	noreorder
.set	nomacro
b	$L483
sw	$9,40($sp)
.set	macro
.set	reorder

$L828:
lw	$7,10800($fp)
andi	$7,$7,0x80
.set	noreorder
.set	nomacro
beq	$7,$0,$L865
addiu	$7,$2,-9
.set	macro
.set	reorder

lw	$7,7996($fp)
sra	$18,$2,5
lw	$13,168($fp)
lw	$14,10780($fp)
andi	$7,$7,0x1
sll	$7,$7,1
addu	$14,$13,$14
addu	$13,$7,$18
lw	$7,168($fp)
sw	$14,184($sp)
sra	$14,$13,2
sw	$13,216($sp)
mul	$18,$7,$14
lw	$13,184($sp)
lw	$14,172($sp)
addu	$7,$18,$13
lw	$13,204($sp)
sw	$7,192($sp)
sll	$7,$7,2
addu	$7,$13,$7
lw	$7,0($7)
and	$7,$14,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L633
lw	$18,184($sp)
.set	macro
.set	reorder

li	$13,-2			# 0xfffffffffffffffe
lw	$7,216($sp)
lw	$14,11852($fp)
sll	$18,$18,2
and	$7,$7,$13
sw	$18,220($sp)
sw	$7,192($sp)
lw	$13,220($sp)
lw	$18,2696($fp)
addu	$7,$14,$13
lw	$14,176($sp)
lw	$13,216($sp)
addu	$18,$18,$14
lw	$14,11864($fp)
lw	$7,0($7)
sw	$18,184($sp)
lw	$18,96($18)
addiu	$7,$7,3
sw	$18,208($sp)
mul	$18,$13,$14
lw	$13,208($sp)
lw	$14,192($sp)
addu	$7,$18,$7
lw	$18,184($sp)
sw	$7,204($sp)
sll	$7,$7,2
addu	$7,$13,$7
lh	$13,0($7)
sh	$13,0($3)
lhu	$7,2($7)
lw	$13,220($sp)
sll	$7,$7,1
sh	$7,2($3)
lw	$7,188($18)
addu	$7,$7,$13
addu	$7,$7,$14
lb	$13,1($7)
.set	noreorder
.set	nomacro
b	$L437
sra	$13,$13,1
.set	macro
.set	reorder

$L575:
addiu	$2,$2,%lo(golomb_to_inter_cbp_gray)
addu	$7,$7,$2
lbu	$7,0($7)
.set	noreorder
.set	nomacro
b	$L569
sw	$7,108($sp)
.set	macro
.set	reorder

$L501:
addu	$17,$fp,$2
lw	$3,6632($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L402
addiu	$2,$2,6624
.set	macro
.set	reorder

lw	$22,%got(ff_golomb_vlc_len)($28)
li	$20,16711680			# 0xff0000
lw	$21,%got(ff_ue_golomb_vlc_code)($28)
li	$19,-16777216			# 0xffffffffff000000
addu	$11,$fp,$2
move	$9,$0
li	$16,4096			# 0x1000
li	$13,1			# 0x1
li	$18,2			# 0x2
addiu	$20,$20,255
move	$25,$fp
move	$12,$fp
.set	noreorder
.set	nomacro
b	$L508
ori	$19,$19,0xff00
.set	macro
.set	reorder

$L835:
lw	$2,6632($17)
addiu	$9,$9,1
addiu	$12,$12,40
sltu	$3,$9,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L834
addiu	$11,$11,4
.set	macro
.set	reorder

$L508:
sll	$5,$9,1
move	$8,$0
move	$4,$12
$L507:
addu	$2,$8,$5
sll	$2,$16,$2
and	$2,$2,$23
.set	noreorder
.set	nomacro
beq	$2,$0,$L504
li	$3,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$10,0($11)
.set	noreorder
.set	nomacro
beq	$10,$13,$L504
move	$3,$0
.set	macro
.set	reorder

bne	$10,$18,$L505
lw	$3,10340($fp)
lw	$7,10332($fp)
srl	$2,$3,3
andi	$10,$3,0x7
addu	$7,$7,$2
addiu	$3,$3,1
lbu	$2,0($7)
sw	$3,10340($fp)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$3,$2,0x1
sll	$2,$3,8
addu	$2,$2,$3
sll	$3,$2,16
addu	$3,$2,$3
$L504:
addiu	$4,$4,16
sw	$3,11564($4)
.set	noreorder
.set	nomacro
beq	$8,$13,$L835
sw	$3,11572($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L507
li	$8,1			# 0x1
.set	macro
.set	reorder

$L505:
lw	$14,10340($fp)
lw	$2,10332($fp)
srl	$3,$14,3
andi	$15,$14,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$7,$3,8
and	$2,$2,$20
and	$7,$7,$19
or	$7,$2,$7
sll	$3,$7,16
srl	$7,$7,16
or	$2,$3,$7
sll	$2,$2,$15
srl	$2,$2,23
addu	$3,$22,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$7,0($2)
addu	$14,$3,$14
sll	$2,$7,8
sltu	$10,$7,$10
addu	$2,$2,$7
.set	noreorder
.set	nomacro
beq	$10,$0,$L545
sw	$14,10340($fp)
.set	macro
.set	reorder

sll	$3,$2,16
.set	noreorder
.set	nomacro
b	$L504
addu	$3,$2,$3
.set	macro
.set	reorder

$L574:
addiu	$2,$2,%lo(golomb_to_inter_cbp)
addu	$7,$7,$2
lbu	$7,0($7)
.set	noreorder
.set	nomacro
b	$L569
sw	$7,108($sp)
.set	macro
.set	reorder

$L800:
li	$8,-16777216			# 0xffffffffff000000
li	$9,16711680			# 0xff0000
ori	$18,$8,0xff00
move	$22,$0
move	$8,$16
addiu	$9,$9,255
move	$16,$fp
.set	noreorder
.set	nomacro
b	$L548
move	$4,$23
.set	macro
.set	reorder

$L837:
li	$2,65536			# 0x10000
addiu	$22,$22,1
addu	$2,$fp,$2
addiu	$16,$16,160
lw	$2,6632($2)
sltu	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L402
addiu	$17,$17,40
.set	macro
.set	reorder

$L548:
sll	$23,$22,1
li	$21,12			# 0xc
move	$20,$0
move	$19,$16
$L563:
addu	$3,$23,$20
li	$2,4096			# 0x1000
sll	$2,$2,$3
and	$2,$2,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L836
addu	$2,$17,$21
.set	macro
.set	reorder

move	$2,$0
$L549:
sw	$2,11296($19)
addiu	$19,$19,8
sw	$2,11292($19)
sw	$2,11320($19)
sw	$2,11324($19)
sw	$2,11352($19)
sw	$2,11356($19)
sw	$2,11384($19)
sw	$2,11388($19)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$20,$2,$L837
addiu	$21,$21,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L563
move	$20,$8
.set	macro
.set	reorder

$L836:
sll	$5,$20,2
.set	noreorder
.set	nomacro
bne	$5,$0,$L550
lb	$2,11568($2)
.set	macro
.set	reorder

lb	$3,11579($17)
beq	$2,$3,$L838
$L551:
sw	$2,16($sp)
addiu	$2,$sp,72
li	$6,2			# 0x2
sw	$8,200($sp)
move	$4,$fp
sw	$9,196($sp)
sw	$2,20($sp)
addiu	$2,$sp,40
sw	$2,24($sp)
lw	$2,%got(pred_motion)($28)
addiu	$25,$2,%lo(pred_motion)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,pred_motion
1:	jalr	$25
move	$7,$22
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$6,72($sp)
lw	$7,40($sp)
lw	$4,88($sp)
lw	$9,196($sp)
lw	$8,200($sp)
$L552:
lw	$12,10340($fp)
lw	$5,10332($fp)
srl	$3,$12,3
andi	$2,$12,0x7
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($3)  
lwr $11, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$11,8
sll	$11,$11,8
and	$3,$3,$9
and	$11,$11,$18
or	$3,$3,$11
sll	$10,$3,16
srl	$3,$3,16
or	$3,$10,$3
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L839
li	$3,31			# 0x1f
.set	macro
.set	reorder

ori	$10,$2,0x1
clz	$10,$10
addiu	$12,$12,32
subu	$10,$3,$10
sll	$10,$10,1
addiu	$10,$10,-31
srl	$3,$2,$10
subu	$10,$12,$10
andi	$2,$3,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L557
sw	$10,10340($fp)
.set	macro
.set	reorder

srl	$3,$3,1
subu	$2,$0,$3
$L556:
addu	$2,$2,$6
srl	$6,$10,3
andi	$3,$10,0x7
addu	$5,$5,$6
sw	$2,72($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$6,8
sll	$6,$6,8
and	$5,$5,$9
and	$6,$6,$18
or	$6,$5,$6
sll	$5,$6,16
srl	$6,$6,16
or	$5,$5,$6
sll	$3,$5,$3
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L840
ori	$5,$3,0x1
.set	macro
.set	reorder

clz	$6,$5
li	$5,31			# 0x1f
addiu	$10,$10,32
subu	$5,$5,$6
sll	$5,$5,1
addiu	$5,$5,-31
srl	$3,$3,$5
subu	$5,$10,$5
andi	$6,$3,0x1
srl	$3,$3,1
.set	noreorder
.set	nomacro
beq	$6,$0,$L560
sw	$5,10340($fp)
.set	macro
.set	reorder

subu	$3,$0,$3
$L560:
addu	$3,$3,$7
andi	$2,$2,0xffff
sll	$5,$3,16
sw	$3,40($sp)
move	$6,$4
.set	noreorder
.set	nomacro
b	$L549
addu	$2,$2,$5
.set	macro
.set	reorder

$L840:
lw	$6,%got(ff_golomb_vlc_len)($28)
srl	$3,$3,23
lw	$5,%got(ff_se_golomb_vlc_code)($28)
addu	$6,$6,$3
addu	$3,$5,$3
lbu	$5,0($6)
lb	$3,0($3)
addu	$10,$5,$10
.set	noreorder
.set	nomacro
b	$L560
sw	$10,10340($fp)
.set	macro
.set	reorder

$L839:
lw	$10,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$10,0($10)
lb	$2,0($2)
addu	$10,$10,$12
.set	noreorder
.set	nomacro
b	$L556
sw	$10,10340($fp)
.set	macro
.set	reorder

$L557:
.set	noreorder
.set	nomacro
b	$L556
srl	$2,$3,1
.set	macro
.set	reorder

$L550:
lb	$3,11576($17)
li	$6,-2			# 0xfffffffffffffffe
beq	$3,$6,$L553
addiu	$7,$16,11280
$L554:
bne	$2,$3,$L551
lh	$6,0($7)
lh	$7,2($7)
sw	$6,72($sp)
.set	noreorder
.set	nomacro
b	$L552
sw	$7,40($sp)
.set	macro
.set	reorder

$L834:
.set	noreorder
.set	nomacro
beq	$2,$0,$L402
lw	$2,%got(scan8)($28)
.set	macro
.set	reorder

addiu	$24,$fp,11288
move	$21,$0
sw	$8,112($sp)
move	$22,$0
addiu	$2,$2,%lo(scan8)
li	$16,1			# 0x1
sw	$2,164($sp)
li	$2,65536			# 0x10000
.set	noreorder
.set	nomacro
b	$L542
addu	$15,$fp,$2
.set	macro
.set	reorder

$L842:
lw	$2,6632($15)
addiu	$22,$22,1
addiu	$21,$21,2
addiu	$24,$24,160
sltu	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L402
addiu	$25,$25,40
.set	macro
.set	reorder

$L542:
li	$5,12288			# 0x3000
lw	$7,164($sp)
sll	$2,$22,5
sll	$3,$22,3
sll	$5,$5,$21
addu	$3,$3,$2
sll	$8,$22,2
sw	$5,100($sp)
addiu	$2,$24,-11288
li	$5,12			# 0xc
move	$4,$0
sw	$8,104($sp)
addu	$13,$fp,$3
$L541:
addu	$8,$21,$4
li	$9,4096			# 0x1000
sll	$8,$9,$8
and	$8,$8,$23
.set	noreorder
.set	nomacro
bne	$8,$0,$L841
addu	$8,$25,$5
.set	macro
.set	reorder

move	$8,$0
$L510:
sw	$8,11296($2)
addiu	$2,$2,64
sw	$8,11236($2)
addiu	$5,$5,16
sw	$8,11240($2)
addiu	$7,$7,8
sw	$8,11244($2)
sw	$8,11264($2)
sw	$8,11268($2)
sw	$8,11272($2)
.set	noreorder
.set	nomacro
beq	$4,$16,$L842
sw	$8,11276($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L541
lw	$4,112($sp)
.set	macro
.set	reorder

$L841:
sll	$6,$4,3
.set	noreorder
.set	nomacro
bne	$6,$0,$L511
lb	$8,11568($8)
.set	macro
.set	reorder

lb	$6,11572($25)
beq	$8,$6,$L843
$L512:
lbu	$14,0($7)
lw	$18,-5252($15)
addiu	$12,$14,-1
addiu	$11,$14,-8
addu	$9,$3,$12
addu	$10,$3,$11
addiu	$6,$14,-4
addiu	$9,$9,2812
addiu	$10,$10,2812
addu	$11,$13,$11
addu	$19,$13,$12
addu	$17,$13,$6
sll	$9,$9,2
lb	$12,11568($11)
sll	$10,$10,2
lb	$11,11568($19)
addu	$9,$fp,$9
lb	$17,11568($17)
.set	noreorder
.set	nomacro
beq	$18,$0,$L514
addu	$10,$fp,$10
.set	macro
.set	reorder

li	$18,-2			# 0xfffffffffffffffe
beq	$17,$18,$L844
$L515:
addu	$6,$3,$6
addiu	$6,$6,2812
sll	$6,$6,2
addu	$6,$fp,$6
$L519:
xor	$18,$8,$12
xor	$14,$8,$17
sltu	$18,$18,1
sltu	$14,$14,1
xor	$8,$8,$11
addu	$14,$14,$18
sltu	$8,$8,1
addu	$14,$14,$8
slt	$19,$14,2
bne	$19,$0,$L520
lh	$8,0($9)
lh	$12,0($10)
slt	$14,$12,$8
.set	noreorder
.set	nomacro
beq	$14,$0,$L521
lh	$11,0($6)
.set	macro
.set	reorder

slt	$14,$12,$11
beq	$14,$0,$L530
slt	$12,$8,$11
movz	$8,$11,$12
move	$12,$8
$L530:
lh	$8,2($9)
lh	$9,2($10)
lh	$6,2($6)
slt	$10,$9,$8
.set	noreorder
.set	nomacro
beq	$10,$0,$L531
sw	$12,72($sp)
.set	macro
.set	reorder

slt	$10,$9,$6
beq	$10,$0,$L532
slt	$9,$8,$6
movz	$8,$6,$9
move	$9,$8
$L532:
sw	$9,40($sp)
$L513:
lw	$11,10340($fp)
li	$17,16711680			# 0xff0000
lw	$14,10332($fp)
li	$18,-16777216			# 0xffffffffff000000
ori	$17,$17,0xff
srl	$6,$11,3
ori	$18,$18,0xff00
addu	$6,$14,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($6)  
lwr $20, 0($6)  

# 0 "" 2
#NO_APP
srl	$10,$20,8
sll	$6,$20,8
and	$10,$10,$17
and	$6,$6,$18
or	$10,$10,$6
sll	$6,$10,16
srl	$10,$10,16
andi	$8,$11,0x7
or	$10,$6,$10
sll	$8,$10,$8
li	$19,134217728			# 0x8000000
sltu	$6,$8,$19
.set	noreorder
.set	nomacro
beq	$6,$0,$L845
li	$17,31			# 0x1f
.set	macro
.set	reorder

ori	$6,$8,0x1
clz	$6,$6
addiu	$11,$11,32
subu	$6,$17,$6
sll	$6,$6,1
addiu	$6,$6,-31
subu	$11,$11,$6
srl	$8,$8,$6
andi	$6,$8,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L535
sw	$11,10340($fp)
.set	macro
.set	reorder

srl	$8,$8,1
subu	$6,$0,$8
$L534:
addu	$6,$6,$12
srl	$10,$11,3
li	$18,16711680			# 0xff0000
li	$19,-16777216			# 0xffffffffff000000
sw	$6,72($sp)
addu	$14,$14,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($14)  
lwr $12, 0($14)  

# 0 "" 2
#NO_APP
srl	$10,$12,8
sll	$12,$12,8
ori	$18,$18,0xff
ori	$19,$19,0xff00
and	$10,$10,$18
and	$12,$12,$19
or	$12,$10,$12
sll	$10,$12,16
srl	$12,$12,16
andi	$8,$11,0x7
or	$10,$10,$12
sll	$8,$10,$8
li	$20,134217728			# 0x8000000
sltu	$10,$8,$20
.set	noreorder
.set	nomacro
beq	$10,$0,$L846
ori	$10,$8,0x1
.set	macro
.set	reorder

clz	$10,$10
li	$17,31			# 0x1f
addiu	$11,$11,32
subu	$10,$17,$10
sll	$10,$10,1
addiu	$10,$10,-31
srl	$8,$8,$10
subu	$10,$11,$10
andi	$11,$8,0x1
srl	$8,$8,1
.set	noreorder
.set	nomacro
beq	$11,$0,$L538
sw	$10,10340($fp)
.set	macro
.set	reorder

subu	$8,$0,$8
$L538:
addu	$9,$8,$9
andi	$8,$6,0xffff
sll	$10,$9,16
sw	$9,40($sp)
move	$6,$23
.set	noreorder
.set	nomacro
b	$L510
addu	$8,$8,$10
.set	macro
.set	reorder

$L511:
lb	$6,11595($25)
bne	$8,$6,$L512
lh	$12,68($24)
lh	$9,70($24)
sw	$12,72($sp)
.set	noreorder
.set	nomacro
b	$L513
sw	$9,40($sp)
.set	macro
.set	reorder

$L845:
lw	$20,%got(ff_golomb_vlc_len)($28)
srl	$8,$8,23
lw	$10,%got(ff_se_golomb_vlc_code)($28)
addu	$6,$20,$8
addu	$8,$10,$8
lbu	$10,0($6)
lb	$6,0($8)
addu	$11,$10,$11
.set	noreorder
.set	nomacro
b	$L534
sw	$11,10340($fp)
.set	macro
.set	reorder

$L846:
lw	$12,%got(ff_golomb_vlc_len)($28)
srl	$8,$8,23
lw	$14,%got(ff_se_golomb_vlc_code)($28)
addu	$10,$12,$8
addu	$8,$14,$8
lbu	$10,0($10)
lb	$8,0($8)
addu	$11,$10,$11
.set	noreorder
.set	nomacro
b	$L538
sw	$11,10340($fp)
.set	macro
.set	reorder

$L514:
li	$18,-2			# 0xfffffffffffffffe
bne	$17,$18,$L515
$L620:
addiu	$6,$14,-9
$L867:
addu	$14,$3,$6
addu	$6,$13,$6
addiu	$14,$14,2812
sll	$14,$14,2
lb	$17,11568($6)
.set	noreorder
.set	nomacro
b	$L519
addu	$6,$fp,$14
.set	macro
.set	reorder

$L531:
slt	$10,$6,$9
beq	$10,$0,$L532
slt	$9,$6,$8
movz	$8,$6,$9
.set	noreorder
.set	nomacro
b	$L532
move	$9,$8
.set	macro
.set	reorder

$L535:
.set	noreorder
.set	nomacro
b	$L534
srl	$6,$8,1
.set	macro
.set	reorder

$L521:
slt	$14,$11,$12
beq	$14,$0,$L530
slt	$12,$11,$8
movz	$8,$11,$12
.set	noreorder
.set	nomacro
b	$L530
move	$12,$8
.set	macro
.set	reorder

$L520:
.set	noreorder
.set	nomacro
beq	$14,$16,$L847
li	$19,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

beq	$12,$19,$L848
$L528:
lh	$11,0($9)
lh	$12,0($10)
slt	$14,$12,$11
.set	noreorder
.set	nomacro
beq	$14,$0,$L529
lh	$8,0($6)
.set	macro
.set	reorder

slt	$14,$12,$8
beq	$14,$0,$L530
slt	$12,$11,$8
movz	$11,$8,$12
.set	noreorder
.set	nomacro
b	$L530
move	$12,$11
.set	macro
.set	reorder

$L818:
lw	$20,%got(ff_golomb_vlc_len)($28)
addiu	$19,$sp,72
lw	$9,%got(b_sub_mb_type_info)($28)
li	$4,60296			# 0xeb88
li	$11,60304			# 0xeb90
lw	$5,10340($fp)
li	$10,16711680			# 0xff0000
lw	$12,10332($fp)
li	$8,-16777216			# 0xffffffffff000000
lw	$16,%got(ff_ue_golomb_vlc_code)($28)
addu	$4,$fp,$4
sw	$19,168($sp)
addu	$11,$fp,$11
sw	$20,140($sp)
addiu	$10,$10,255
addiu	$9,$9,%lo(b_sub_mb_type_info)
move	$6,$19
ori	$8,$8,0xff00
move	$13,$20
$L407:
srl	$3,$5,3
andi	$14,$5,0x7
addu	$3,$12,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
and	$3,$3,$10
and	$7,$7,$8
or	$7,$3,$7
sll	$3,$7,16
srl	$7,$7,16
or	$2,$3,$7
sll	$2,$2,$14
srl	$2,$2,23
addu	$3,$13,$2
addu	$2,$16,$2
lbu	$3,0($3)
lbu	$7,0($2)
addu	$5,$3,$5
sll	$2,$7,2
andi	$7,$7,0xffff
sltu	$3,$7,13
sw	$5,10340($fp)
addu	$2,$2,$9
.set	noreorder
.set	nomacro
beq	$3,$0,$L849
sh	$7,0($4)
.set	macro
.set	reorder

lhu	$3,0($2)
addiu	$4,$4,2
lbu	$2,2($2)
addiu	$6,$6,4
sh	$3,-2($4)
.set	noreorder
.set	nomacro
bne	$4,$11,$L407
sw	$2,-4($6)
.set	macro
.set	reorder

li	$4,65536			# 0x10000
addu	$4,$fp,$4
lhu	$3,-5238($4)
lhu	$5,-5240($4)
lhu	$2,-5236($4)
lhu	$4,-5234($4)
or	$3,$3,$5
or	$2,$3,$2
or	$2,$2,$4
andi	$2,$2,0x100
.set	noreorder
.set	nomacro
beq	$2,$0,$L866
li	$2,65536			# 0x10000
.set	macro
.set	reorder

lw	$25,%call16(ff_h264_pred_direct_motion)($28)
addiu	$5,$sp,88
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_pred_direct_motion
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

li	$2,-2			# 0xfffffffffffffffe
lw	$28,32($sp)
lw	$23,88($sp)
sb	$2,11638($fp)
sb	$2,11598($fp)
sb	$2,11622($fp)
.set	noreorder
.set	nomacro
b	$L409
sb	$2,11582($fp)
.set	macro
.set	reorder

$L838:
lh	$6,11292($16)
lh	$7,11294($16)
sw	$6,72($sp)
.set	noreorder
.set	nomacro
b	$L552
sw	$7,40($sp)
.set	macro
.set	reorder

$L553:
lb	$3,11573($17)
.set	noreorder
.set	nomacro
b	$L554
addiu	$7,$16,11268
.set	macro
.set	reorder

$L844:
slt	$6,$14,20
.set	noreorder
.set	nomacro
bne	$6,$0,$L867
addiu	$6,$14,-9
.set	macro
.set	reorder

li	$19,4			# 0x4
andi	$6,$14,0x7
.set	noreorder
.set	nomacro
bne	$6,$19,$L867
addiu	$6,$14,-9
.set	macro
.set	reorder

lb	$6,11579($25)
beq	$6,$18,$L620
lw	$17,2696($fp)
lw	$17,104($17)
sw	$0,0($24)
sw	$17,128($sp)
lw	$17,-5248($15)
.set	noreorder
.set	nomacro
beq	$17,$0,$L850
move	$6,$24
.set	macro
.set	reorder

lw	$17,10800($fp)
andi	$17,$17,0x80
bne	$17,$0,$L620
slt	$17,$14,36
xori	$17,$17,0x1
addiu	$17,$17,2694
sra	$14,$14,2
sll	$17,$17,2
addu	$17,$fp,$17
lw	$18,4($17)
lw	$17,128($sp)
sll	$18,$18,2
addu	$19,$17,$18
lw	$17,0($19)
lw	$19,100($sp)
and	$17,$19,$17
.set	noreorder
.set	nomacro
beq	$17,$0,$L640
andi	$20,$14,0x3
.set	macro
.set	reorder

andi	$14,$14,0x2
lw	$17,2696($fp)
lw	$19,11852($fp)
sw	$14,128($sp)
lw	$14,11864($fp)
addu	$19,$19,$18
mult	$20,$14
lw	$19,0($19)
mflo	$14
sw	$14,116($sp)
lw	$14,104($sp)
addu	$20,$17,$14
mflo	$14
addiu	$17,$14,3
addu	$17,$17,$19
lw	$19,96($20)
sll	$17,$17,2
addu	$17,$19,$17
lh	$19,0($17)
sh	$19,0($24)
lh	$19,2($17)
srl	$17,$19,31
addu	$17,$17,$19
sra	$17,$17,1
sh	$17,2($24)
lw	$17,188($20)
lw	$20,128($sp)
addu	$18,$17,$18
addu	$14,$18,$20
lb	$17,1($14)
.set	noreorder
.set	nomacro
b	$L519
sll	$17,$17,1
.set	macro
.set	reorder

$L843:
lh	$12,-24($24)
lh	$9,-22($24)
sw	$12,72($sp)
.set	noreorder
.set	nomacro
b	$L513
sw	$9,40($sp)
.set	macro
.set	reorder

$L793:
lw	$25,%call16(av_get_pict_type_char)($28)
lw	$17,0($fp)
addu	$2,$fp,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_get_pict_type_char
1:	jalr	$25
lw	$4,-5264($2)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$28,32($sp)
move	$4,$17
lw	$8,7992($fp)
move	$7,$16
lw	$3,7996($fp)
sw	$2,16($sp)
lw	$6,%got($LC3)($28)
lw	$25,%call16(av_log)($28)
sw	$8,20($sp)
addiu	$6,$6,%lo($LC3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L672
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L424:
lw	$2,-5236($3)
li	$3,24117248			# 0x1700000
addiu	$3,$3,368
or	$2,$2,$4
and	$2,$2,$3
sltu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L425
sw	$2,124($sp)
.set	macro
.set	reorder

$L833:
bne	$6,$0,$L750
bne	$9,$0,$L851
lh	$15,0($11)
lh	$9,2($11)
sw	$15,72($sp)
.set	noreorder
.set	nomacro
b	$L483
sw	$9,40($sp)
.set	macro
.set	reorder

$L529:
slt	$14,$8,$12
beq	$14,$0,$L530
slt	$12,$8,$11
movz	$11,$8,$12
.set	noreorder
.set	nomacro
b	$L530
move	$12,$11
.set	macro
.set	reorder

$L847:
bne	$8,$0,$L751
bne	$18,$0,$L852
lh	$12,0($6)
lh	$9,2($6)
sw	$12,72($sp)
.set	noreorder
.set	nomacro
b	$L513
sw	$9,40($sp)
.set	macro
.set	reorder

$L819:
lw	$2,7996($fp)
li	$5,16			# 0x10
lw	$3,7992($fp)
lw	$6,%got($LC5)($28)
lw	$4,0($fp)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC5)
sw	$2,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L817
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L848:
bne	$17,$19,$L528
beq	$11,$19,$L528
$L751:
lh	$12,0($9)
lh	$9,2($9)
sw	$12,72($sp)
.set	noreorder
.set	nomacro
b	$L513
sw	$9,40($sp)
.set	macro
.set	reorder

$L809:
li	$5,16			# 0x10
lw	$3,7992($fp)
lw	$2,7996($fp)
lw	$4,0($fp)
addiu	$6,$6,%lo($LC8)
lw	$25,%call16(av_log)($28)
$L754:
sw	$2,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L817
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L820:
lw	$9,124($sp)
.set	noreorder
.set	nomacro
bne	$9,$0,$L868
li	$3,65536			# 0x10000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L402
move	$6,$23
.set	macro
.set	reorder

$L849:
lw	$2,7996($fp)
li	$5,16			# 0x10
lw	$3,7992($fp)
lw	$6,%got($LC4)($28)
lw	$4,0($fp)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC4)
sw	$2,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L817
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L851:
lh	$15,11264($2)
lh	$9,11266($2)
sw	$15,72($sp)
.set	noreorder
.set	nomacro
b	$L483
sw	$9,40($sp)
.set	macro
.set	reorder

$L852:
lh	$12,0($10)
lh	$9,2($10)
sw	$12,72($sp)
.set	noreorder
.set	nomacro
b	$L513
sw	$9,40($sp)
.set	macro
.set	reorder

$L814:
lw	$6,%got($LC7)($28)
lw	$3,7992($fp)
lw	$2,7996($fp)
lw	$4,0($fp)
addiu	$6,$6,%lo($LC7)
.set	noreorder
.set	nomacro
b	$L754
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

$L850:
lw	$17,10800($fp)
andi	$17,$17,0x80
beq	$17,$0,$L620
lw	$20,7996($fp)
sra	$14,$14,5
lw	$18,168($fp)
lw	$17,10780($fp)
andi	$20,$20,0x1
sll	$20,$20,1
addu	$17,$18,$17
addu	$14,$20,$14
sra	$19,$14,2
mul	$20,$18,$19
addu	$18,$20,$17
lw	$20,128($sp)
sw	$18,116($sp)
sll	$18,$18,2
addu	$19,$20,$18
lw	$18,0($19)
lw	$19,100($sp)
and	$18,$19,$18
.set	noreorder
.set	nomacro
beq	$18,$0,$L640
li	$19,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

lw	$18,11852($fp)
sll	$17,$17,2
lw	$20,2696($fp)
and	$19,$14,$19
sw	$19,128($sp)
addu	$19,$18,$17
lw	$18,104($sp)
lw	$19,0($19)
addu	$20,$20,$18
lw	$18,11864($fp)
addiu	$19,$19,3
sw	$20,116($sp)
lw	$20,96($20)
sw	$20,212($sp)
mul	$20,$14,$18
lw	$18,212($sp)
addu	$14,$20,$19
sll	$14,$14,2
addu	$14,$18,$14
lh	$19,0($14)
sh	$19,0($24)
lhu	$14,2($14)
lw	$19,116($sp)
sll	$14,$14,1
sh	$14,2($24)
lw	$14,188($19)
addu	$17,$14,$17
lw	$14,128($sp)
addu	$20,$17,$14
lb	$17,1($20)
.set	noreorder
.set	nomacro
b	$L519
sra	$17,$17,1
.set	macro
.set	reorder

$L633:
.set	noreorder
.set	nomacro
b	$L437
li	$13,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L640:
.set	noreorder
.set	nomacro
b	$L519
li	$17,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	ff_h264_decode_mb_cavlc
.size	ff_h264_decode_mb_cavlc, .-ff_h264_decode_mb_cavlc
.rdata
.align	2
.type	suffix_limit.7723, @object
.size	suffix_limit.7723, 28
suffix_limit.7723:
.word	0
.word	3
.word	6
.word	12
.word	24
.word	48
.word	2147483647
.align	2
.type	coeff_token_table_index.7710, @object
.size	coeff_token_table_index.7710, 68
coeff_token_table_index.7710:
.word	0
.word	0
.word	1
.word	1
.word	2
.word	2
.word	2
.word	2
.word	3
.word	3
.word	3
.word	3
.word	3
.word	3
.word	3
.word	3
.word	3
.align	2
.type	left_block_options.7269, @object
.size	left_block_options.7269, 64
left_block_options.7269:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	7
.byte	10
.byte	8
.byte	11
.byte	7
.byte	15
.byte	23
.byte	31
.byte	2
.byte	26
.byte	10
.byte	18
.byte	2
.byte	2
.byte	3
.byte	3
.byte	8
.byte	11
.byte	8
.byte	11
.byte	23
.byte	23
.byte	31
.byte	31
.byte	10
.byte	18
.byte	10
.byte	18
.byte	0
.byte	0
.byte	1
.byte	1
.byte	7
.byte	10
.byte	7
.byte	10
.byte	7
.byte	7
.byte	15
.byte	15
.byte	2
.byte	26
.byte	2
.byte	26
.byte	0
.byte	2
.byte	0
.byte	2
.byte	7
.byte	10
.byte	7
.byte	10
.byte	7
.byte	23
.byte	7
.byte	23
.byte	2
.byte	26
.byte	2
.byte	26
.local	done.7678
.comm	done.7678,4,4
.local	cavlc_level_tab
.comm	cavlc_level_tab,3584,4
.local	run7_vlc_table
.comm	run7_vlc_table,384,4
.local	run7_vlc
.comm	run7_vlc,16,4
.local	run_vlc_tables
.comm	run_vlc_tables,192,4
.local	run_vlc
.comm	run_vlc,96,4
.local	chroma_dc_total_zeros_vlc_tables
.comm	chroma_dc_total_zeros_vlc_tables,96,4
.local	chroma_dc_total_zeros_vlc
.comm	chroma_dc_total_zeros_vlc,48,4
.local	total_zeros_vlc_tables
.comm	total_zeros_vlc_tables,30720,4
.local	total_zeros_vlc
.comm	total_zeros_vlc,240,4
.local	chroma_dc_coeff_token_vlc_table
.comm	chroma_dc_coeff_token_vlc_table,1024,4
.local	chroma_dc_coeff_token_vlc
.comm	chroma_dc_coeff_token_vlc,16,4
.align	2
.type	coeff_token_vlc_tables_size, @object
.size	coeff_token_vlc_tables_size, 16
coeff_token_vlc_tables_size:
.word	520
.word	332
.word	280
.word	256
.local	coeff_token_vlc_tables
.comm	coeff_token_vlc_tables,5552,4
.local	coeff_token_vlc
.comm	coeff_token_vlc,64,4
.align	2
.type	run_bits, @object
.size	run_bits, 112
run_bits:
.byte	1
.byte	0
.space	14
.byte	1
.byte	1
.byte	0
.space	13
.byte	3
.byte	2
.byte	1
.byte	0
.space	12
.byte	3
.byte	2
.byte	1
.byte	1
.byte	0
.space	11
.byte	3
.byte	2
.byte	3
.byte	2
.byte	1
.byte	0
.space	10
.byte	3
.byte	0
.byte	1
.byte	3
.byte	2
.byte	5
.byte	4
.space	9
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.space	1
.align	2
.type	run_len, @object
.size	run_len, 112
run_len:
.byte	1
.byte	1
.space	14
.byte	1
.byte	2
.byte	2
.space	13
.byte	2
.byte	2
.byte	2
.byte	2
.space	12
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.space	11
.byte	2
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.space	10
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.space	9
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.space	1
.align	2
.type	chroma_dc_total_zeros_bits, @object
.size	chroma_dc_total_zeros_bits, 12
chroma_dc_total_zeros_bits:
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.align	2
.type	chroma_dc_total_zeros_len, @object
.size	chroma_dc_total_zeros_len, 12
chroma_dc_total_zeros_len:
.byte	1
.byte	2
.byte	3
.byte	3
.byte	1
.byte	2
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.align	2
.type	total_zeros_bits, @object
.size	total_zeros_bits, 256
total_zeros_bits:
.byte	1
.byte	3
.byte	2
.byte	3
.byte	2
.byte	3
.byte	2
.byte	3
.byte	2
.byte	3
.byte	2
.byte	3
.byte	2
.byte	3
.byte	2
.byte	1
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	5
.byte	4
.byte	3
.byte	2
.byte	3
.byte	2
.byte	3
.byte	2
.byte	1
.byte	0
.space	1
.byte	5
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	4
.byte	3
.byte	2
.byte	3
.byte	2
.byte	1
.byte	1
.byte	0
.space	2
.byte	3
.byte	7
.byte	5
.byte	4
.byte	6
.byte	5
.byte	4
.byte	3
.byte	3
.byte	2
.byte	2
.byte	1
.byte	0
.space	3
.byte	5
.byte	4
.byte	3
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	2
.byte	1
.byte	1
.byte	0
.space	4
.byte	1
.byte	1
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	2
.byte	1
.byte	1
.byte	0
.space	5
.byte	1
.byte	1
.byte	5
.byte	4
.byte	3
.byte	3
.byte	2
.byte	1
.byte	1
.byte	0
.space	6
.byte	1
.byte	1
.byte	1
.byte	3
.byte	3
.byte	2
.byte	2
.byte	1
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	3
.byte	2
.byte	1
.byte	1
.byte	1
.space	8
.byte	1
.byte	0
.byte	1
.byte	3
.byte	2
.byte	1
.byte	1
.space	9
.byte	0
.byte	1
.byte	1
.byte	2
.byte	1
.byte	3
.space	10
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.space	11
.byte	0
.byte	1
.byte	1
.byte	1
.space	12
.byte	0
.byte	1
.byte	1
.space	13
.byte	0
.byte	1
.space	14
.space	16
.align	2
.type	total_zeros_len, @object
.size	total_zeros_len, 256
total_zeros_len:
.byte	1
.byte	3
.byte	3
.byte	4
.byte	4
.byte	5
.byte	5
.byte	6
.byte	6
.byte	7
.byte	7
.byte	8
.byte	8
.byte	9
.byte	9
.byte	9
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	6
.byte	6
.byte	6
.byte	6
.space	1
.byte	4
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	3
.byte	3
.byte	4
.byte	5
.byte	5
.byte	6
.byte	5
.byte	6
.space	2
.byte	5
.byte	3
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	4
.byte	3
.byte	4
.byte	5
.byte	5
.byte	5
.space	3
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	5
.byte	4
.byte	5
.space	4
.byte	6
.byte	5
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	3
.byte	6
.space	5
.byte	6
.byte	5
.byte	3
.byte	3
.byte	3
.byte	2
.byte	3
.byte	4
.byte	3
.byte	6
.space	6
.byte	6
.byte	4
.byte	5
.byte	3
.byte	2
.byte	2
.byte	3
.byte	3
.byte	6
.space	7
.byte	6
.byte	6
.byte	4
.byte	2
.byte	2
.byte	3
.byte	2
.byte	5
.space	8
.byte	5
.byte	5
.byte	3
.byte	2
.byte	2
.byte	2
.byte	4
.space	9
.byte	4
.byte	4
.byte	3
.byte	3
.byte	1
.byte	3
.space	10
.byte	4
.byte	4
.byte	2
.byte	1
.byte	3
.space	11
.byte	3
.byte	3
.byte	1
.byte	2
.space	12
.byte	2
.byte	2
.byte	1
.space	13
.byte	1
.byte	1
.space	14
.space	16
.align	2
.type	coeff_token_bits, @object
.size	coeff_token_bits, 272
coeff_token_bits:
.byte	1
.byte	0
.byte	0
.byte	0
.byte	5
.byte	1
.byte	0
.byte	0
.byte	7
.byte	4
.byte	1
.byte	0
.byte	7
.byte	6
.byte	5
.byte	3
.byte	7
.byte	6
.byte	5
.byte	3
.byte	7
.byte	6
.byte	5
.byte	4
.byte	15
.byte	6
.byte	5
.byte	4
.byte	11
.byte	14
.byte	5
.byte	4
.byte	8
.byte	10
.byte	13
.byte	4
.byte	15
.byte	14
.byte	9
.byte	4
.byte	11
.byte	10
.byte	13
.byte	12
.byte	15
.byte	14
.byte	9
.byte	12
.byte	11
.byte	10
.byte	13
.byte	8
.byte	15
.byte	1
.byte	9
.byte	12
.byte	11
.byte	14
.byte	13
.byte	8
.byte	7
.byte	10
.byte	9
.byte	12
.byte	4
.byte	6
.byte	5
.byte	8
.byte	3
.byte	0
.byte	0
.byte	0
.byte	11
.byte	2
.byte	0
.byte	0
.byte	7
.byte	7
.byte	3
.byte	0
.byte	7
.byte	10
.byte	9
.byte	5
.byte	7
.byte	6
.byte	5
.byte	4
.byte	4
.byte	6
.byte	5
.byte	6
.byte	7
.byte	6
.byte	5
.byte	8
.byte	15
.byte	6
.byte	5
.byte	4
.byte	11
.byte	14
.byte	13
.byte	4
.byte	15
.byte	10
.byte	9
.byte	4
.byte	11
.byte	14
.byte	13
.byte	12
.byte	8
.byte	10
.byte	9
.byte	8
.byte	15
.byte	14
.byte	13
.byte	12
.byte	11
.byte	10
.byte	9
.byte	12
.byte	7
.byte	11
.byte	6
.byte	8
.byte	9
.byte	8
.byte	10
.byte	1
.byte	7
.byte	6
.byte	5
.byte	4
.byte	15
.byte	0
.byte	0
.byte	0
.byte	15
.byte	14
.byte	0
.byte	0
.byte	11
.byte	15
.byte	13
.byte	0
.byte	8
.byte	12
.byte	14
.byte	12
.byte	15
.byte	10
.byte	11
.byte	11
.byte	11
.byte	8
.byte	9
.byte	10
.byte	9
.byte	14
.byte	13
.byte	9
.byte	8
.byte	10
.byte	9
.byte	8
.byte	15
.byte	14
.byte	13
.byte	13
.byte	11
.byte	14
.byte	10
.byte	12
.byte	15
.byte	10
.byte	13
.byte	12
.byte	11
.byte	14
.byte	9
.byte	12
.byte	8
.byte	10
.byte	13
.byte	8
.byte	13
.byte	7
.byte	9
.byte	12
.byte	9
.byte	12
.byte	11
.byte	10
.byte	5
.byte	8
.byte	7
.byte	6
.byte	1
.byte	4
.byte	3
.byte	2
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	4
.byte	5
.byte	6
.byte	0
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.byte	17
.byte	18
.byte	19
.byte	20
.byte	21
.byte	22
.byte	23
.byte	24
.byte	25
.byte	26
.byte	27
.byte	28
.byte	29
.byte	30
.byte	31
.byte	32
.byte	33
.byte	34
.byte	35
.byte	36
.byte	37
.byte	38
.byte	39
.byte	40
.byte	41
.byte	42
.byte	43
.byte	44
.byte	45
.byte	46
.byte	47
.byte	48
.byte	49
.byte	50
.byte	51
.byte	52
.byte	53
.byte	54
.byte	55
.byte	56
.byte	57
.byte	58
.byte	59
.byte	60
.byte	61
.byte	62
.byte	63
.align	2
.type	coeff_token_len, @object
.size	coeff_token_len, 272
coeff_token_len:
.byte	1
.byte	0
.byte	0
.byte	0
.byte	6
.byte	2
.byte	0
.byte	0
.byte	8
.byte	6
.byte	3
.byte	0
.byte	9
.byte	8
.byte	7
.byte	5
.byte	10
.byte	9
.byte	8
.byte	6
.byte	11
.byte	10
.byte	9
.byte	7
.byte	13
.byte	11
.byte	10
.byte	8
.byte	13
.byte	13
.byte	11
.byte	9
.byte	13
.byte	13
.byte	13
.byte	10
.byte	14
.byte	14
.byte	13
.byte	11
.byte	14
.byte	14
.byte	14
.byte	13
.byte	15
.byte	15
.byte	14
.byte	14
.byte	15
.byte	15
.byte	15
.byte	14
.byte	16
.byte	15
.byte	15
.byte	15
.byte	16
.byte	16
.byte	16
.byte	15
.byte	16
.byte	16
.byte	16
.byte	16
.byte	16
.byte	16
.byte	16
.byte	16
.byte	2
.byte	0
.byte	0
.byte	0
.byte	6
.byte	2
.byte	0
.byte	0
.byte	6
.byte	5
.byte	3
.byte	0
.byte	7
.byte	6
.byte	6
.byte	4
.byte	8
.byte	6
.byte	6
.byte	4
.byte	8
.byte	7
.byte	7
.byte	5
.byte	9
.byte	8
.byte	8
.byte	6
.byte	11
.byte	9
.byte	9
.byte	6
.byte	11
.byte	11
.byte	11
.byte	7
.byte	12
.byte	11
.byte	11
.byte	9
.byte	12
.byte	12
.byte	12
.byte	11
.byte	12
.byte	12
.byte	12
.byte	11
.byte	13
.byte	13
.byte	13
.byte	12
.byte	13
.byte	13
.byte	13
.byte	13
.byte	13
.byte	14
.byte	13
.byte	13
.byte	14
.byte	14
.byte	14
.byte	13
.byte	14
.byte	14
.byte	14
.byte	14
.byte	4
.byte	0
.byte	0
.byte	0
.byte	6
.byte	4
.byte	0
.byte	0
.byte	6
.byte	5
.byte	4
.byte	0
.byte	6
.byte	5
.byte	5
.byte	4
.byte	7
.byte	5
.byte	5
.byte	4
.byte	7
.byte	5
.byte	5
.byte	4
.byte	7
.byte	6
.byte	6
.byte	4
.byte	7
.byte	6
.byte	6
.byte	4
.byte	8
.byte	7
.byte	7
.byte	5
.byte	8
.byte	8
.byte	7
.byte	6
.byte	9
.byte	8
.byte	8
.byte	7
.byte	9
.byte	9
.byte	8
.byte	8
.byte	9
.byte	9
.byte	9
.byte	8
.byte	10
.byte	9
.byte	9
.byte	9
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	10
.byte	6
.byte	0
.byte	0
.byte	0
.byte	6
.byte	6
.byte	0
.byte	0
.byte	6
.byte	6
.byte	6
.byte	0
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.align	2
.type	chroma_dc_coeff_token_bits, @object
.size	chroma_dc_coeff_token_bits, 20
chroma_dc_coeff_token_bits:
.byte	1
.byte	0
.byte	0
.byte	0
.byte	7
.byte	1
.byte	0
.byte	0
.byte	4
.byte	6
.byte	1
.byte	0
.byte	3
.byte	3
.byte	2
.byte	5
.byte	2
.byte	3
.byte	2
.byte	0
.align	2
.type	chroma_dc_coeff_token_len, @object
.size	chroma_dc_coeff_token_len, 20
chroma_dc_coeff_token_len:
.byte	2
.byte	0
.byte	0
.byte	0
.byte	6
.byte	1
.byte	0
.byte	0
.byte	6
.byte	6
.byte	3
.byte	0
.byte	6
.byte	7
.byte	7
.byte	6
.byte	6
.byte	8
.byte	8
.byte	7
.align	2
.type	golomb_to_intra4x4_cbp_gray, @object
.size	golomb_to_intra4x4_cbp_gray, 16
golomb_to_intra4x4_cbp_gray:
.byte	15
.byte	0
.byte	7
.byte	11
.byte	13
.byte	14
.byte	3
.byte	5
.byte	10
.byte	12
.byte	1
.byte	2
.byte	4
.byte	8
.byte	6
.byte	9
.align	2
.type	golomb_to_inter_cbp_gray, @object
.size	golomb_to_inter_cbp_gray, 16
golomb_to_inter_cbp_gray:
.byte	0
.byte	1
.byte	2
.byte	4
.byte	8
.byte	3
.byte	5
.byte	10
.byte	12
.byte	15
.byte	7
.byte	11
.byte	13
.byte	14
.byte	6
.byte	9
.align	2
.type	b_sub_mb_type_info, @object
.size	b_sub_mb_type_info, 52
b_sub_mb_type_info:
.half	256
.byte	1
.space	1
.half	4104
.byte	1
.space	1
.half	16392
.byte	1
.space	1
.half	20488
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	-16368
.byte	2
.space	1
.half	-16352
.byte	2
.space	1
.half	-4080
.byte	2
.space	1
.half	-4064
.byte	2
.space	1
.half	12352
.byte	4
.space	1
.half	-16320
.byte	4
.space	1
.half	-4032
.byte	4
.space	1
.align	2
.type	b_mb_type_info, @object
.size	b_mb_type_info, 92
b_mb_type_info:
.half	256
.byte	1
.space	1
.half	4104
.byte	1
.space	1
.half	16392
.byte	1
.space	1
.half	20488
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	-16368
.byte	2
.space	1
.half	-16352
.byte	2
.space	1
.half	-28656
.byte	2
.space	1
.half	-28640
.byte	2
.space	1
.half	24592
.byte	2
.space	1
.half	24608
.byte	2
.space	1
.half	-20464
.byte	2
.space	1
.half	-20448
.byte	2
.space	1
.half	-8176
.byte	2
.space	1
.half	-8160
.byte	2
.space	1
.half	28688
.byte	2
.space	1
.half	28704
.byte	2
.space	1
.half	-12272
.byte	2
.space	1
.half	-12256
.byte	2
.space	1
.half	-4080
.byte	2
.space	1
.half	-4064
.byte	2
.space	1
.half	-4032
.byte	4
.space	1
.align	2
.type	p_sub_mb_type_info, @object
.size	p_sub_mb_type_info, 16
p_sub_mb_type_info:
.half	4104
.byte	1
.space	1
.half	4112
.byte	2
.space	1
.half	4128
.byte	2
.space	1
.half	4160
.byte	4
.space	1
.align	2
.type	p_mb_type_info, @object
.size	p_mb_type_info, 20
p_mb_type_info:
.half	4104
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	12352
.byte	4
.space	1
.half	12864
.byte	4
.space	1
.align	2
.type	i_mb_type_info, @object
.size	i_mb_type_info, 104
i_mb_type_info:
.half	1
.byte	-1
.byte	-1
.half	2
.byte	2
.byte	0
.half	2
.byte	1
.byte	0
.half	2
.byte	0
.byte	0
.half	2
.byte	3
.byte	0
.half	2
.byte	2
.byte	16
.half	2
.byte	1
.byte	16
.half	2
.byte	0
.byte	16
.half	2
.byte	3
.byte	16
.half	2
.byte	2
.byte	32
.half	2
.byte	1
.byte	32
.half	2
.byte	0
.byte	32
.half	2
.byte	3
.byte	32
.half	2
.byte	2
.byte	15
.half	2
.byte	1
.byte	15
.half	2
.byte	0
.byte	15
.half	2
.byte	3
.byte	15
.half	2
.byte	2
.byte	31
.half	2
.byte	1
.byte	31
.half	2
.byte	0
.byte	31
.half	2
.byte	3
.byte	31
.half	2
.byte	2
.byte	47
.half	2
.byte	1
.byte	47
.half	2
.byte	0
.byte	47
.half	2
.byte	3
.byte	47
.half	4
.byte	-1
.byte	-1
.align	2
.type	chroma_dc_scan, @object
.size	chroma_dc_scan, 4
chroma_dc_scan:
.byte	0
.byte	16
.byte	32
.byte	48
.align	2
.type	luma_dc_field_scan, @object
.size	luma_dc_field_scan, 16
luma_dc_field_scan:
.byte	0
.byte	32
.byte	16
.byte	-128
.byte	-96
.byte	48
.byte	-112
.byte	-80
.byte	64
.byte	96
.byte	-64
.byte	-32
.byte	80
.byte	112
.byte	-48
.byte	-16
.align	2
.type	luma_dc_zigzag_scan, @object
.size	luma_dc_zigzag_scan, 16
luma_dc_zigzag_scan:
.byte	0
.byte	16
.byte	32
.byte	-128
.byte	48
.byte	64
.byte	80
.byte	96
.byte	-112
.byte	-96
.byte	-80
.byte	-64
.byte	112
.byte	-48
.byte	-32
.byte	-16
.align	2
.type	golomb_to_inter_cbp, @object
.size	golomb_to_inter_cbp, 48
golomb_to_inter_cbp:
.byte	0
.byte	16
.byte	1
.byte	2
.byte	4
.byte	8
.byte	32
.byte	3
.byte	5
.byte	10
.byte	12
.byte	15
.byte	47
.byte	7
.byte	11
.byte	13
.byte	14
.byte	6
.byte	9
.byte	31
.byte	35
.byte	37
.byte	42
.byte	44
.byte	33
.byte	34
.byte	36
.byte	40
.byte	39
.byte	43
.byte	45
.byte	46
.byte	17
.byte	18
.byte	20
.byte	24
.byte	19
.byte	21
.byte	26
.byte	28
.byte	23
.byte	27
.byte	29
.byte	30
.byte	22
.byte	25
.byte	38
.byte	41
.align	2
.type	golomb_to_intra4x4_cbp, @object
.size	golomb_to_intra4x4_cbp, 48
golomb_to_intra4x4_cbp:
.byte	47
.byte	31
.byte	15
.byte	0
.byte	23
.byte	27
.byte	29
.byte	30
.byte	7
.byte	11
.byte	13
.byte	14
.byte	39
.byte	43
.byte	45
.byte	46
.byte	16
.byte	3
.byte	5
.byte	10
.byte	12
.byte	19
.byte	21
.byte	26
.byte	28
.byte	35
.byte	37
.byte	42
.byte	44
.byte	1
.byte	2
.byte	4
.byte	8
.byte	17
.byte	18
.byte	20
.byte	24
.byte	6
.byte	9
.byte	22
.byte	25
.byte	32
.byte	33
.byte	34
.byte	36
.byte	40
.byte	38
.byte	41
.align	2
.type	scan8, @object
.size	scan8, 24
scan8:
.byte	12
.byte	13
.byte	20
.byte	21
.byte	14
.byte	15
.byte	22
.byte	23
.byte	28
.byte	29
.byte	36
.byte	37
.byte	30
.byte	31
.byte	38
.byte	39
.byte	9
.byte	10
.byte	17
.byte	18
.byte	33
.byte	34
.byte	41
.byte	42

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
