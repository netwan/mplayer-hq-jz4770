.file	1 "h264_direct.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.get_scale_factor,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_scale_factor
.type	get_scale_factor, @function
get_scale_factor:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$8,$7,3
sll	$7,$7,5
addu	$3,$8,$7
sll	$2,$3,4
subu	$2,$2,$3
li	$3,65536			# 0x10000
addu	$2,$4,$2
addu	$2,$3,$2
lw	$9,6916($2)
subu	$6,$6,$9
slt	$10,$6,-128
bne	$10,$0,$L6
slt	$2,$6,128

beq	$2,$0,$L7
nop

bne	$6,$0,$L5
li	$2,256			# 0x100

j	$31
nop

$L6:
li	$6,-128			# 0xffffffffffffff80
$L5:
addu	$7,$8,$7
sll	$8,$7,4
subu	$7,$8,$7
addu	$4,$4,$7
li	$7,65536			# 0x10000
addu	$4,$7,$4
lw	$2,6932($4)
beq	$2,$0,$L13
nop

$L9:
j	$31
li	$2,256			# 0x100

$L7:
addu	$7,$8,$7
sll	$8,$7,4
subu	$7,$8,$7
addu	$4,$4,$7
li	$7,65536			# 0x10000
addu	$4,$7,$4
lw	$2,6932($4)
bne	$2,$0,$L9
li	$6,127			# 0x7f

$L13:
subu	$2,$0,$6
slt	$3,$6,0
movz	$2,$6,$3
li	$4,127			# 0x7f
move	$3,$2
sra	$3,$3,1
subu	$2,$5,$9
addiu	$3,$3,16384
slt	$5,$2,128
teq	$6,$0,7
div	$0,$3,$6
movz	$2,$4,$5
li	$3,-128			# 0xffffffffffffff80
slt	$4,$2,-128
movn	$2,$3,$4
li	$3,1023			# 0x3ff
mflo	$6
mul	$2,$6,$2
addiu	$2,$2,32
sra	$2,$2,6
slt	$4,$2,1024
movz	$2,$3,$4
li	$3,-1024			# 0xfffffffffffffc00
slt	$4,$2,-1024
j	$31
movn	$2,$3,$4

.set	macro
.set	reorder
.end	get_scale_factor
.size	get_scale_factor, .-get_scale_factor
.section	.text.fill_colmap,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	fill_colmap
.type	fill_colmap, @function
fill_colmap:
.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-64
move	$2,$5
sw	$21,48($sp)
.cprestore	16
sw	$23,56($sp)
move	$23,$6
sw	$22,52($sp)
move	$22,$4
sw	$19,40($sp)
sw	$31,60($sp)
sw	$20,44($sp)
sw	$18,36($sp)
sw	$17,32($sp)
sw	$16,28($sp)
lw	$21,84($sp)
beq	$21,$0,$L42
move	$19,$7

li	$3,65536			# 0x10000
li	$18,16			# 0x10
addu	$3,$4,$3
li	$17,1			# 0x1
lw	$16,6624($3)
addiu	$16,$16,8
sll	$16,$16,1
$L32:
sll	$3,$23,8
lw	$25,%call16(memset)($28)
sll	$20,$23,6
move	$5,$0
subu	$20,$3,$20
li	$6,192			# 0xc0
addu	$20,$2,$20
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$20

sll	$3,$18,5
lw	$2,80($sp)
sll	$5,$18,3
li	$12,65536			# 0x10000
addu	$5,$5,$3
sll	$2,$2,1
sll	$3,$5,4
addu	$2,$23,$2
subu	$3,$3,$5
sll	$4,$2,6
addiu	$14,$12,7520
sll	$2,$2,2
ori	$25,$12,0x8b98
addu	$14,$3,$14
addu	$2,$22,$2
addu	$3,$22,$3
addu	$25,$4,$25
li	$13,131072			# 0x20000
addu	$25,$22,$25
addu	$14,$22,$14
move	$7,$0
addu	$13,$2,$13
li	$24,3			# 0x3
li	$22,-4			# 0xfffffffffffffffc
addu	$12,$12,$3
li	$23,1			# 0x1
slt	$15,$18,$16
lw	$2,-29544($13)
$L46:
blez	$2,$L31
nop

move	$9,$0
move	$11,$20
move	$10,$25
xor	$4,$7,$19
$L29:
beq	$17,$0,$L43
lw	$8,0($10)

andi	$2,$8,0x3
beq	$2,$24,$L44
nop

$L19:
beq	$15,$0,$L21
nop

lw	$2,6920($12)
lw	$5,6720($12)
sll	$3,$2,2
andi	$2,$5,0x3
addu	$2,$3,$2
beq	$2,$8,$L33
move	$6,$14

b	$L24
move	$5,$18

$L28:
lw	$3,0($2)
lw	$2,-200($2)
sll	$3,$3,2
andi	$2,$2,0x3
addu	$2,$3,$2
beq	$2,$8,$L22
nop

$L24:
addiu	$5,$5,1
move	$2,$6
bne	$5,$16,$L28
addiu	$6,$6,600

$L21:
lw	$2,-29544($13)
addiu	$9,$9,1
addiu	$10,$10,4
slt	$2,$9,$2
bne	$2,$0,$L29
addiu	$11,$11,4

$L31:
beq	$7,$23,$L14
li	$7,1			# 0x1

b	$L46
lw	$2,-29544($13)

$L44:
and	$8,$8,$22
addu	$8,$8,$7
b	$L19
addiu	$8,$8,1

$L43:
b	$L19
ori	$8,$8,0x3

$L33:
move	$5,$18
$L22:
beq	$21,$0,$L45
sll	$2,$9,1

addiu	$5,$5,-16
xor	$5,$5,$19
$L45:
addu	$2,$2,$4
addiu	$2,$2,16
sll	$2,$2,2
addu	$2,$20,$2
beq	$7,$19,$L26
sw	$5,0($2)

bne	$17,$0,$L21
nop

$L26:
sw	$5,0($11)
addiu	$9,$9,1
lw	$2,-29544($13)
addiu	$10,$10,4
slt	$2,$9,$2
bne	$2,$0,$L29
addiu	$11,$11,4

b	$L31
nop

$L14:
lw	$31,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L42:
li	$3,65536			# 0x10000
lw	$17,10384($4)
move	$18,$0
addu	$3,$4,$3
xori	$17,$17,0x3
sltu	$17,$0,$17
b	$L32
lw	$16,6624($3)

.set	macro
.set	reorder
.end	fill_colmap
.size	fill_colmap, .-fill_colmap
.section	.text.ff_h264_direct_dist_scale_factor,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_direct_dist_scale_factor
.set	nomips16
.set	nomicromips
.ent	ff_h264_direct_dist_scale_factor
.type	ff_h264_direct_dist_scale_factor, @function
ff_h264_direct_dist_scale_factor:
.frame	$sp,88,$31		# vars= 24, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,10384($4)
addiu	$sp,$sp,-88
lw	$5,2696($4)
li	$3,131072			# 0x20000
sw	$16,48($sp)
move	$16,$4
xori	$2,$2,0x2
sw	$19,60($sp)
sltu	$2,$2,1
sw	$18,56($sp)
addiu	$2,$2,66
.cprestore	16
li	$4,65536			# 0x10000
sw	$31,84($sp)
sll	$2,$2,2
sw	$fp,80($sp)
addu	$6,$16,$3
sw	$23,76($sp)
addu	$2,$5,$2
sw	$22,72($sp)
addu	$4,$16,$4
sw	$21,68($sp)
addiu	$3,$3,-29828
sw	$20,64($sp)
sw	$17,52($sp)
addiu	$5,$5,268
lw	$4,6624($4)
addu	$3,$16,$3
lw	$6,-29820($6)
move	$18,$0
lw	$2,4($2)
sll	$19,$4,1
sw	$5,36($sp)
sw	$4,24($sp)
sw	$6,32($sp)
sw	$3,40($sp)
sw	$2,28($sp)
$L49:
lw	$25,36($sp)
lw	$20,0($25)
lw	$25,40($sp)
beq	$19,$0,$L52
lw	$21,0($25)

lw	$4,%got(get_scale_factor)($28)
move	$fp,$0
sll	$22,$18,5
addiu	$17,$4,%lo(get_scale_factor)
$L51:
addiu	$7,$fp,16
xor	$23,$fp,$18
move	$4,$16
move	$5,$20
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_scale_factor
1:	jalr	$25
move	$6,$21

addu	$3,$22,$23
addiu	$fp,$fp,1
lw	$28,16($sp)
addiu	$3,$3,17688
sll	$3,$3,2
addu	$3,$16,$3
bne	$fp,$19,$L51
sw	$2,0($3)

$L52:
lw	$25,36($sp)
li	$2,1			# 0x1
addiu	$25,$25,4
sw	$25,36($sp)
lw	$25,40($sp)
addiu	$25,$25,4
beq	$18,$2,$L62
sw	$25,40($sp)

b	$L49
li	$18,1			# 0x1

$L62:
lw	$25,24($sp)
beq	$25,$0,$L47
li	$19,65536			# 0x10000

lw	$17,%got(get_scale_factor)($28)
move	$18,$0
addiu	$19,$19,5152
addiu	$17,$17,%lo(get_scale_factor)
addu	$19,$16,$19
$L54:
lw	$5,28($sp)
move	$7,$18
lw	$6,32($sp)
move	$25,$17
move	$4,$16
.reloc	1f,R_MIPS_JALR,get_scale_factor
1:	jalr	$25
addiu	$18,$18,1

addiu	$19,$19,4
lw	$25,24($sp)
bne	$18,$25,$L54
sw	$2,-4($19)

$L47:
lw	$31,84($sp)
lw	$fp,80($sp)
lw	$23,76($sp)
lw	$22,72($sp)
lw	$21,68($sp)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,88

.set	macro
.set	reorder
.end	ff_h264_direct_dist_scale_factor
.size	ff_h264_direct_dist_scale_factor, .-ff_h264_direct_dist_scale_factor
.section	.text.ff_h264_direct_ref_list_init,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_direct_ref_list_init
.set	nomips16
.set	nomicromips
.ent	ff_h264_direct_ref_list_init
.type	ff_h264_direct_ref_list_init, @function
ff_h264_direct_ref_list_init:
.frame	$sp,80,$31		# vars= 8, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$12,10384($4)
addiu	$sp,$sp,-80
li	$2,131072			# 0x20000
sw	$17,44($sp)
li	$11,65536			# 0x10000
addu	$2,$4,$2
sw	$19,52($sp)
andi	$17,$12,0x1
sw	$16,40($sp)
xori	$17,$17,0x1
sw	$18,48($sp)
lw	$19,2696($4)
move	$16,$4
sll	$4,$17,7
lw	$18,-30016($2)
addiu	$9,$11,6624
.cprestore	24
sll	$7,$17,3
sw	$31,76($sp)
addiu	$4,$4,296
sw	$fp,72($sp)
andi	$18,$18,0x1
sw	$23,68($sp)
addu	$9,$16,$9
sw	$22,64($sp)
addu	$7,$19,$7
sw	$21,60($sp)
addu	$4,$19,$4
sw	$20,56($sp)
move	$10,$0
addiu	$11,$11,6920
li	$13,57600			# 0xe100
xori	$18,$18,0x1
$L65:
lw	$3,0($9)
beq	$3,$0,$L68
sw	$3,552($7)

sll	$2,$3,3
sll	$3,$3,5
addu	$8,$10,$11
addu	$3,$2,$3
addu	$5,$16,$8
sll	$2,$3,4
move	$6,$4
subu	$2,$2,$3
addu	$8,$2,$8
addu	$8,$16,$8
$L67:
lw	$3,0($5)
addiu	$5,$5,600
addiu	$6,$6,4
sll	$3,$3,2
lw	$2,-800($5)
andi	$2,$2,0x3
addu	$2,$3,$2
bne	$5,$8,$L67
sw	$2,-4($6)

$L68:
addiu	$10,$10,28800
addiu	$9,$9,4
addiu	$7,$7,4
bne	$10,$13,$L65
addiu	$4,$4,64

li	$2,3			# 0x3
beq	$12,$2,$L87
lw	$25,%call16(memcpy)($28)

$L69:
li	$2,65536			# 0x10000
lw	$3,10384($16)
addu	$2,$16,$2
lw	$4,-5252($2)
sw	$4,568($19)
li	$4,3			# 0x3
beq	$3,$4,$L88
sw	$0,5148($2)

li	$4,131072			# 0x20000
addu	$4,$16,$4
lw	$5,-30016($4)
and	$3,$5,$3
beq	$3,$0,$L89
nop

$L73:
lw	$3,52($19)
li	$2,3			# 0x3
beq	$3,$2,$L91
li	$19,65536			# 0x10000

$L63:
lw	$31,76($sp)
$L92:
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,80

$L89:
lw	$3,-29528($4)
bne	$3,$0,$L73
sll	$3,$5,1

lw	$4,168($16)
addiu	$3,$3,-3
mul	$3,$4,$3
sw	$3,5148($2)
li	$2,3			# 0x3
lw	$3,52($19)
bne	$3,$2,$L92
lw	$31,76($sp)

li	$19,65536			# 0x10000
$L91:
addu	$20,$16,$19
lw	$2,5140($20)
bne	$2,$0,$L63
addiu	$22,$19,5472

lw	$21,%got(fill_colmap)($28)
addiu	$fp,$19,6240
addu	$22,$16,$22
addiu	$21,$21,%lo(fill_colmap)
addiu	$19,$19,5856
sw	$22,32($sp)
addu	$fp,$16,$fp
move	$22,$0
lw	$5,32($sp)
$L93:
move	$6,$22
move	$4,$16
sw	$18,16($sp)
move	$7,$17
move	$25,$21
.reloc	1f,R_MIPS_JALR,fill_colmap
1:	jalr	$25
sw	$0,20($sp)

li	$23,1			# 0x1
lw	$3,-5252($20)
addu	$5,$16,$19
move	$7,$0
move	$6,$22
beq	$3,$0,$L76
move	$4,$16

move	$25,$21
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,fill_colmap
1:	jalr	$25
sw	$23,20($sp)

li	$7,1			# 0x1
move	$6,$22
sw	$23,16($sp)
sw	$23,20($sp)
move	$4,$16
move	$25,$21
.reloc	1f,R_MIPS_JALR,fill_colmap
1:	jalr	$25
move	$5,$fp

$L76:
bne	$22,$0,$L63
li	$22,1			# 0x1

b	$L93
lw	$5,32($sp)

$L88:
li	$4,131072			# 0x20000
lw	$3,2696($16)
addu	$4,$16,$4
lw	$17,276($3)
lw	$3,-29828($4)
lw	$4,-29824($4)
subu	$3,$3,$17
subu	$17,$4,$17
subu	$6,$0,$3
subu	$4,$0,$17
slt	$7,$3,0
slt	$5,$17,0
movn	$3,$6,$7
movn	$17,$4,$5
slt	$3,$3,$17
xori	$17,$3,0x1
move	$18,$17
b	$L73
sw	$17,5144($2)

$L87:
addiu	$4,$19,560
addiu	$5,$19,552
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
li	$6,8			# 0x8

addiu	$4,$19,424
lw	$28,24($sp)
addiu	$5,$19,296
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
li	$6,128			# 0x80

b	$L69
lw	$28,24($sp)

.set	macro
.set	reorder
.end	ff_h264_direct_ref_list_init
.size	ff_h264_direct_ref_list_init, .-ff_h264_direct_ref_list_init
.section	.text.ff_h264_pred_direct_motion,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_pred_direct_motion
.set	nomips16
.set	nomicromips
.ent	ff_h264_pred_direct_motion
.type	ff_h264_pred_direct_motion, @function
ff_h264_pred_direct_motion:
.frame	$sp,104,$31		# vars= 56, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,65536			# 0x10000
addiu	$sp,$sp,-104
addu	$2,$4,$2
.cprestore	0
sw	$fp,100($sp)
sw	$23,96($sp)
sw	$22,92($sp)
sw	$21,88($sp)
sw	$20,84($sp)
sw	$19,80($sp)
sw	$18,76($sp)
sw	$17,72($sp)
sw	$16,68($sp)
lw	$3,5140($2)
bne	$3,$0,$L263
li	$3,131072			# 0x20000

lw	$6,0($5)
lw	$19,11864($4)
addu	$3,$4,$3
lw	$8,9448($3)
lw	$9,-29992($3)
sll	$3,$8,2
addu	$7,$9,$3
lw	$7,0($7)
andi	$10,$7,0x80
beq	$10,$0,$L149
andi	$16,$6,0x40

andi	$3,$6,0x80
beq	$3,$0,$L264
move	$20,$0

lw	$2,5148($2)
li	$20,2			# 0x2
addu	$8,$8,$2
sll	$3,$8,2
addu	$9,$9,$3
lw	$7,0($9)
$L151:
sw	$7,12($sp)
bne	$16,$0,$L154
sw	$7,8($sp)

$L270:
andi	$2,$7,0xf
beq	$2,$0,$L155
andi	$2,$7,0x30

ori	$6,$6,0x5108
li	$18,20744			# 0x5108
sw	$6,0($5)
$L153:
lw	$5,11852($4)
li	$2,131072			# 0x20000
addu	$2,$4,$2
addu	$5,$5,$3
lw	$9,-29996($2)
lw	$5,0($5)
lw	$25,-29908($2)
lw	$8,-29904($2)
lw	$17,-30000($2)
sll	$2,$5,2
addu	$25,$25,$3
addu	$9,$9,$2
addu	$3,$8,$3
addu	$17,$17,$2
sw	$9,36($sp)
bne	$20,$0,$L157
sw	$3,40($sp)

lw	$2,7996($4)
andi	$2,$2,0x1
beq	$2,$0,$L277
li	$2,65536			# 0x10000

sll	$2,$19,3
addiu	$3,$3,2
addu	$9,$9,$2
addiu	$25,$25,2
addu	$17,$17,$2
sw	$3,40($sp)
sw	$9,36($sp)
$L157:
li	$2,65536			# 0x10000
$L277:
addu	$3,$4,$2
addiu	$5,$2,5664
addiu	$8,$2,5472
addu	$5,$4,$5
lw	$3,-5252($3)
addiu	$24,$2,5152
addu	$fp,$4,$8
sw	$5,44($sp)
beq	$3,$0,$L158
addu	$24,$4,$24

andi	$3,$6,0x80
beq	$3,$0,$L159
li	$3,131072			# 0x20000

lw	$3,7996($4)
li	$5,131072			# 0x20000
addiu	$9,$2,5856
addu	$5,$4,$5
andi	$3,$3,0x1
sll	$24,$3,7
sll	$3,$3,9
lw	$8,-29528($5)
addiu	$5,$2,6048
subu	$3,$3,$24
addiu	$2,$2,5216
addu	$9,$3,$9
addu	$3,$3,$5
addu	$2,$24,$2
addu	$3,$4,$3
sra	$10,$7,3
sll	$8,$8,4
xor	$5,$7,$6
sw	$3,44($sp)
andi	$5,$5,0x80
addu	$fp,$4,$9
addu	$24,$4,$2
beq	$5,$0,$L161
and	$10,$10,$8

move	$6,$0
$L160:
lw	$13,%got(scan8)($28)
li	$11,60296			# 0xeb88
addiu	$12,$sp,8
move	$9,$0
addu	$11,$4,$11
addiu	$13,$13,%lo(scan8)
sw	$12,32($sp)
li	$22,4			# 0x4
$L167:
beq	$16,$0,$L162
sra	$7,$9,1

lhu	$2,0($11)
andi	$2,$2,0x100
beq	$2,$0,$L163
nop

$L162:
mul	$2,$20,$7
sh	$18,0($11)
addiu	$3,$sp,8
sll	$8,$7,2
andi	$15,$9,0x1
addu	$8,$3,$8
addu	$21,$2,$15
lbu	$2,0($13)
lw	$14,0($8)
addu	$23,$25,$21
addiu	$5,$2,2812
addiu	$3,$2,2852
addiu	$8,$2,11608
addiu	$12,$2,11568
addu	$8,$4,$8
sll	$5,$5,2
sll	$3,$3,2
sh	$0,0($8)
andi	$14,$14,0x7
sh	$0,8($8)
addu	$5,$4,$5
addu	$3,$4,$3
beq	$14,$0,$L164
addu	$12,$4,$12

sh	$0,0($12)
sh	$0,8($12)
sw	$0,0($5)
sw	$0,4($5)
sw	$0,32($5)
sw	$0,36($5)
sw	$0,0($3)
sw	$0,4($3)
sw	$0,32($3)
sw	$0,36($3)
$L163:
addiu	$9,$9,1
addiu	$11,$11,2
bne	$9,$22,$L167
addiu	$13,$13,4

$L94:
lw	$fp,100($sp)
$L281:
lw	$23,96($sp)
$L284:
lw	$22,92($sp)
lw	$21,88($sp)
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,104

$L149:
andi	$2,$6,0x80
beq	$2,$0,$L195
li	$20,2			# 0x2

lw	$2,7996($4)
li	$8,-2			# 0xfffffffffffffffe
lw	$3,168($4)
sll	$7,$19,1
lw	$10,7992($4)
sll	$19,$19,3
and	$2,$2,$8
mul	$11,$2,$3
sll	$20,$3,2
subu	$19,$19,$7
addiu	$20,$20,2
addu	$8,$11,$10
addu	$2,$3,$8
sll	$3,$8,2
sll	$2,$2,2
addu	$7,$9,$3
addu	$9,$9,$2
lw	$7,0($7)
lw	$2,0($9)
andi	$8,$7,0xf
sw	$7,8($sp)
beq	$8,$0,$L152
sw	$2,12($sp)

andi	$2,$2,0xf
beq	$2,$0,$L152
nop

bne	$16,$0,$L152
nop

ori	$6,$6,0xf110
li	$18,20744			# 0x5108
b	$L153
sw	$6,0($5)

$L263:
li	$2,131072			# 0x20000
lw	$3,0($5)
addiu	$16,$sp,8
lw	$18,11864($4)
addu	$2,$4,$2
addiu	$11,$4,11579
addiu	$14,$sp,16
lw	$17,9448($2)
addiu	$12,$4,11280
move	$13,$0
li	$6,61440			# 0xf000
li	$21,-2			# 0xfffffffffffffffe
li	$23,12288			# 0x3000
li	$22,-1			# 0xffffffffffffffff
li	$20,4			# 0x4
andi	$3,$3,0x40
move	$15,$16
$L108:
lb	$2,-3($11)
lb	$9,0($11)
beq	$2,$21,$L96
lb	$8,-7($11)

sltu	$7,$9,$8
move	$19,$9
movz	$19,$8,$7
move	$10,$12
move	$7,$19
sltu	$19,$2,$19
movn	$7,$2,$19
bltz	$7,$L98
sw	$7,0($14)

$L269:
xor	$9,$9,$7
xor	$8,$8,$7
sltu	$9,$9,1
sltu	$8,$8,1
addu	$19,$9,$8
xor	$2,$2,$7
sltu	$2,$2,1
addu	$2,$19,$2
slt	$2,$2,2
bne	$2,$0,$L99
nop

lh	$7,12($12)
lh	$2,-16($12)
slt	$9,$2,$7
beq	$9,$0,$L100
lh	$8,0($10)

slt	$9,$2,$8
bne	$9,$0,$L265
nop

$L101:
lh	$8,14($12)
lh	$7,-14($12)
lh	$9,2($10)
slt	$10,$7,$8
beq	$10,$0,$L102
slt	$10,$9,$7

slt	$10,$7,$9
bne	$10,$0,$L266
nop

$L103:
sll	$7,$7,16
andi	$2,$2,0xffff
addu	$2,$2,$7
sw	$2,0($15)
$L104:
addiu	$13,$13,2
addiu	$11,$11,40
addiu	$15,$15,4
addiu	$14,$14,4
bne	$13,$20,$L108
addiu	$12,$12,160

lw	$2,16($sp)
bltz	$2,$L267
lw	$2,20($sp)

$L109:
lw	$7,8($sp)
$L282:
lw	$2,12($sp)
or	$2,$2,$7
or	$2,$2,$3
beq	$2,$0,$L268
li	$2,131072			# 0x20000

sll	$8,$17,2
addu	$2,$4,$2
lw	$2,-29992($2)
addu	$7,$2,$8
lw	$25,0($7)
andi	$7,$25,0x80
beq	$7,$0,$L112
nop

lw	$7,0($5)
andi	$8,$7,0x80
bne	$8,$0,$L113
li	$8,65536			# 0x10000

li	$9,65536			# 0x10000
lw	$8,7996($4)
li	$17,-2			# 0xfffffffffffffffe
lw	$11,168($4)
addu	$9,$4,$9
lw	$10,7992($4)
and	$8,$8,$17
move	$21,$0
lw	$17,5144($9)
addu	$8,$8,$17
mul	$9,$8,$11
addu	$17,$9,$10
sll	$8,$17,2
addu	$2,$2,$8
lw	$25,0($2)
$L114:
ori	$6,$6,0x108
sw	$25,28($sp)
bne	$3,$0,$L117
sw	$25,24($sp)

andi	$2,$25,0xf
beq	$2,$0,$L118
ori	$9,$7,0x108

b	$L116
sw	$9,0($5)

$L96:
sltu	$7,$9,$8
lb	$2,-8($11)
move	$19,$9
movz	$19,$8,$7
addiu	$10,$12,-20
move	$7,$19
sltu	$19,$2,$19
movn	$7,$2,$19
bgez	$7,$L269
sw	$7,0($14)

$L98:
sll	$2,$23,$13
sw	$0,0($15)
sw	$22,0($14)
bne	$3,$0,$L107
nor	$2,$0,$2

lw	$7,0($5)
and	$7,$7,$2
sw	$7,0($5)
$L107:
b	$L104
and	$6,$6,$2

$L102:
beq	$10,$0,$L103
nop

slt	$7,$9,$8
movz	$8,$9,$7
b	$L103
move	$7,$8

$L100:
slt	$9,$8,$2
beq	$9,$0,$L101
nop

slt	$2,$8,$7
movz	$7,$8,$2
b	$L101
move	$2,$7

$L195:
sw	$7,12($sp)
beq	$16,$0,$L270
sw	$7,8($sp)

$L154:
lw	$18,11956($4)
li	$2,20800			# 0x5140
li	$8,20744			# 0x5108
ori	$6,$6,0xf040
movn	$2,$8,$18
sw	$6,0($5)
b	$L153
move	$18,$2

$L264:
lw	$3,7996($4)
lw	$8,5144($2)
li	$2,-2			# 0xfffffffffffffffe
lw	$7,168($4)
and	$2,$3,$2
lw	$3,7992($4)
addu	$2,$2,$8
mul	$10,$2,$7
addu	$8,$10,$3
sll	$3,$8,2
addu	$9,$9,$3
b	$L151
lw	$7,0($9)

$L112:
lw	$7,0($5)
andi	$9,$7,0x80
beq	$9,$0,$L114
li	$21,2			# 0x2

lw	$9,7996($4)
li	$17,-2			# 0xfffffffffffffffe
lw	$8,168($4)
sll	$10,$18,1
lw	$11,7992($4)
sll	$18,$18,3
and	$9,$9,$17
mul	$12,$9,$8
sll	$21,$8,2
subu	$18,$18,$10
addiu	$21,$21,2
ori	$6,$6,0x108
addu	$17,$12,$11
addu	$9,$8,$17
sll	$8,$17,2
sll	$9,$9,2
addu	$10,$2,$8
addu	$2,$2,$9
lw	$25,0($10)
lw	$2,0($2)
andi	$9,$25,0xf
sw	$25,24($sp)
beq	$9,$0,$L115
sw	$2,28($sp)

andi	$2,$2,0xf
beq	$2,$0,$L278
ori	$9,$7,0x40

bne	$3,$0,$L278
nop

ori	$9,$7,0x110
b	$L116
sw	$9,0($5)

$L266:
slt	$7,$8,$9
movz	$8,$9,$7
b	$L103
move	$7,$8

$L265:
slt	$2,$7,$8
movz	$7,$8,$2
b	$L101
move	$2,$7

$L118:
andi	$9,$25,0x30
bne	$9,$0,$L271
nop

$L117:
lw	$10,11956($4)
addiu	$2,$6,56
ori	$9,$7,0x40
movz	$6,$2,$10
sw	$9,0($5)
$L116:
lw	$7,11852($4)
li	$2,131072			# 0x20000
addu	$2,$4,$2
addu	$7,$7,$8
lw	$11,-29996($2)
lw	$7,0($7)
lw	$17,-29908($2)
lw	$10,-29904($2)
lw	$12,-30000($2)
sll	$2,$7,2
addu	$17,$17,$8
addu	$11,$11,$2
addu	$8,$10,$8
addu	$fp,$12,$2
sw	$11,36($sp)
bne	$21,$0,$L120
sw	$8,32($sp)

lw	$2,7996($4)
andi	$2,$2,0x1
beq	$2,$0,$L279
xor	$7,$25,$9

sll	$2,$18,3
addiu	$8,$8,2
addu	$11,$11,$2
addiu	$17,$17,2
addu	$fp,$fp,$2
sw	$8,32($sp)
sw	$11,36($sp)
$L120:
xor	$7,$25,$9
$L279:
andi	$7,$7,0x80
beq	$7,$0,$L121
andi	$2,$9,0x8

lw	$20,16($sp)
move	$13,$0
lw	$24,8($sp)
li	$10,60296			# 0xeb88
lw	$19,20($sp)
li	$25,131072			# 0x20000
andi	$7,$20,0xff
lw	$23,12($sp)
sll	$15,$7,8
slt	$20,$0,$20
movn	$13,$24,$20
addu	$15,$7,$15
move	$7,$0
andi	$2,$19,0xff
slt	$19,$0,$19
movn	$7,$23,$19
move	$20,$13
sll	$14,$2,8
lw	$13,%got(scan8)($28)
addu	$10,$4,$10
addu	$14,$2,$14
sw	$20,44($sp)
move	$19,$0
addiu	$13,$13,%lo(scan8)
sw	$7,40($sp)
move	$8,$0
addu	$25,$4,$25
li	$22,4			# 0x4
andi	$15,$15,0xffff
andi	$14,$14,0xffff
$L127:
beq	$3,$0,$L122
sra	$9,$8,1

lhu	$2,0($10)
andi	$2,$2,0x100
beq	$2,$0,$L123
nop

$L122:
sll	$7,$9,2
sh	$6,0($10)
lbu	$2,0($13)
addu	$7,$16,$7
addiu	$12,$2,11568
addiu	$11,$2,11608
lw	$7,16($7)
addu	$12,$4,$12
addu	$11,$4,$11
andi	$7,$7,0x7
sh	$15,0($12)
sh	$15,8($12)
sh	$14,0($11)
bne	$7,$0,$L191
sh	$14,8($11)

lw	$7,-29804($25)
bne	$7,$0,$L280
move	$7,$23

mul	$7,$21,$9
mul	$20,$18,$9
andi	$12,$8,0x1
addu	$11,$7,$12
sll	$7,$12,1
addu	$7,$7,$12
addu	$12,$17,$11
addu	$9,$20,$7
lb	$7,0($12)
beq	$7,$0,$L272
nop

bgez	$7,$L280
move	$7,$23

lw	$7,32($sp)
addu	$11,$7,$11
lb	$7,0($11)
bne	$7,$0,$L280
move	$7,$23

sll	$7,$9,2
lw	$9,36($sp)
addu	$7,$9,$7
$L262:
lhu	$9,0($7)
addiu	$9,$9,1
andi	$9,$9,0xffff
sltu	$9,$9,3
beq	$9,$0,$L191
nop

lhu	$7,2($7)
addiu	$7,$7,1
andi	$7,$7,0xffff
sltu	$7,$7,3
beq	$7,$0,$L191
lw	$7,40($sp)

addiu	$19,$19,1
b	$L124
lw	$9,44($sp)

$L152:
ori	$6,$6,0xf040
li	$18,20744			# 0x5108
b	$L153
sw	$6,0($5)

$L115:
ori	$9,$7,0x40
$L278:
b	$L116
sw	$9,0($5)

$L158:
li	$3,131072			# 0x20000
sra	$10,$7,3
addu	$3,$4,$3
xor	$2,$7,$6
andi	$2,$2,0x80
lw	$3,-29528($3)
sll	$3,$3,4
beq	$2,$0,$L161
and	$10,$10,$3

li	$2,2			# 0x2
andi	$6,$6,0x80
movn	$2,$0,$6
b	$L160
move	$6,$2

$L191:
move	$7,$23
$L280:
move	$9,$24
$L124:
addiu	$11,$2,2812
addiu	$2,$2,2852
sll	$11,$11,2
sll	$2,$2,2
addu	$11,$4,$11
addu	$2,$4,$2
sw	$9,0($11)
sw	$9,4($11)
sw	$9,32($11)
sw	$9,36($11)
sw	$7,0($2)
sw	$7,4($2)
sw	$7,32($2)
sw	$7,36($2)
$L123:
addiu	$8,$8,1
addiu	$10,$10,2
bne	$8,$22,$L127
addiu	$13,$13,4

bne	$3,$0,$L281
lw	$fp,100($sp)

andi	$fp,$19,0x3
bne	$fp,$0,$L281
lw	$fp,100($sp)

li	$2,-65536			# 0xffffffffffff0000
lw	$3,0($5)
addiu	$2,$2,24199
and	$2,$3,$2
ori	$2,$2,0x108
sw	$2,0($5)
$L275:
lw	$fp,100($sp)
lw	$23,96($sp)
lw	$22,92($sp)
lw	$21,88($sp)
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,104

$L164:
lb	$8,0($23)
move	$5,$17
bltz	$8,$L165
addu	$3,$10,$8

sll	$3,$3,2
addu	$3,$fp,$3
lw	$3,0($3)
$L166:
sll	$8,$15,1
sll	$14,$3,8
addu	$15,$8,$15
mul	$8,$19,$7
addiu	$12,$2,11568
addu	$12,$4,$12
addu	$7,$8,$15
sll	$8,$3,2
sll	$7,$7,2
addu	$3,$3,$14
addu	$8,$24,$8
addu	$7,$5,$7
andi	$3,$3,0xffff
lw	$8,0($8)
addiu	$5,$2,2812
sh	$3,0($12)
addiu	$2,$2,2852
sh	$3,8($12)
sll	$5,$5,2
lh	$3,2($7)
sll	$2,$2,2
lh	$14,0($7)
addu	$5,$4,$5
addu	$2,$4,$2
sll	$3,$3,$6
mul	$12,$8,$14
srl	$7,$3,31
addu	$3,$7,$3
sra	$3,$3,1
mul	$7,$8,$3
addiu	$12,$12,128
sra	$12,$12,8
subu	$8,$12,$14
andi	$12,$12,0xffff
addiu	$7,$7,128
andi	$8,$8,0xffff
sra	$7,$7,8
subu	$3,$7,$3
sll	$7,$7,16
sll	$3,$3,16
addu	$7,$12,$7
addu	$3,$8,$3
sw	$7,0($5)
sw	$7,4($5)
sw	$7,32($5)
sw	$7,36($5)
sw	$3,0($2)
sw	$3,4($2)
sw	$3,32($2)
b	$L163
sw	$3,36($2)

$L161:
andi	$6,$6,0x8
beq	$6,$0,$L169
lw	$20,%got(scan8)($28)

andi	$7,$7,0x7
sw	$0,11620($4)
sw	$0,11628($4)
sw	$0,11636($4)
bne	$7,$0,$L199
sw	$0,11644($4)

lb	$2,0($25)
bltz	$2,$L171
lw	$3,40($sp)

addu	$2,$10,$2
sll	$2,$2,2
addu	$2,$fp,$2
lw	$6,0($2)
sll	$2,$6,2
addu	$24,$24,$2
lw	$2,0($24)
$L172:
lh	$8,2($17)
sll	$3,$6,8
lh	$5,0($17)
addu	$6,$3,$6
mul	$7,$2,$8
mul	$3,$2,$5
sll	$2,$6,16
addu	$6,$6,$2
addiu	$2,$7,128
addiu	$3,$3,128
sra	$2,$2,8
sra	$3,$3,8
subu	$8,$2,$8
subu	$5,$3,$5
sll	$2,$2,16
sll	$8,$8,16
andi	$3,$3,0xffff
andi	$5,$5,0xffff
addu	$2,$3,$2
addu	$3,$5,$8
$L170:
sw	$6,11580($4)
sw	$6,11588($4)
sw	$6,11596($4)
sw	$6,11604($4)
sw	$2,11296($4)
sw	$2,11300($4)
sw	$2,11304($4)
sw	$2,11308($4)
sw	$2,11328($4)
sw	$2,11332($4)
sw	$2,11336($4)
sw	$2,11340($4)
sw	$2,11360($4)
sw	$2,11364($4)
sw	$2,11368($4)
sw	$2,11372($4)
sw	$2,11392($4)
sw	$2,11396($4)
sw	$2,11400($4)
sw	$2,11404($4)
sw	$3,11456($4)
sw	$3,11460($4)
sw	$3,11464($4)
sw	$3,11468($4)
sw	$3,11488($4)
sw	$3,11492($4)
sw	$3,11496($4)
sw	$3,11500($4)
sw	$3,11520($4)
sw	$3,11524($4)
sw	$3,11528($4)
sw	$3,11532($4)
sw	$3,11552($4)
sw	$3,11556($4)
sw	$3,11560($4)
sw	$3,11564($4)
lw	$fp,100($sp)
lw	$23,96($sp)
lw	$22,92($sp)
lw	$21,88($sp)
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,104

$L155:
beq	$2,$0,$L154
nop

ori	$6,$6,0xf100
or	$6,$6,$2
li	$18,20744			# 0x5108
b	$L153
sw	$6,0($5)

$L99:
bne	$9,$0,$L273
nop

bne	$8,$0,$L274
nop

lw	$2,0($10)
b	$L104
sw	$2,0($15)

$L159:
sra	$10,$7,3
addu	$3,$4,$3
xor	$2,$7,$6
andi	$2,$2,0x80
lw	$3,-29528($3)
sll	$3,$3,4
beq	$2,$0,$L161
and	$10,$10,$3

b	$L160
li	$6,2			# 0x2

$L169:
li	$21,60296			# 0xeb88
move	$15,$0
addu	$21,$4,$21
addiu	$20,$20,%lo(scan8)
li	$22,4			# 0x4
andi	$7,$7,0x7
andi	$23,$18,0x8
$L182:
beq	$16,$0,$L174
nop

lhu	$2,0($21)
andi	$2,$2,0x100
beq	$2,$0,$L175
nop

$L174:
sh	$18,0($21)
lbu	$2,0($20)
addiu	$3,$2,11608
addu	$3,$4,$3
sh	$0,0($3)
beq	$7,$0,$L176
sh	$0,8($3)

addiu	$5,$2,2812
addiu	$3,$2,2852
sll	$5,$5,2
addiu	$2,$2,11568
sll	$3,$3,2
addu	$5,$4,$5
addu	$3,$4,$3
addu	$2,$4,$2
sh	$0,0($2)
sh	$0,8($2)
sw	$0,0($5)
sw	$0,4($5)
sw	$0,32($5)
sw	$0,36($5)
sw	$0,0($3)
sw	$0,4($3)
sw	$0,32($3)
sw	$0,36($3)
$L175:
addiu	$15,$15,1
$L276:
addiu	$21,$21,2
bne	$15,$22,$L182
addiu	$20,$20,4

lw	$fp,100($sp)
lw	$23,96($sp)
lw	$22,92($sp)
lw	$21,88($sp)
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,104

$L176:
addu	$3,$25,$15
lb	$3,0($3)
bltz	$3,$L177
lw	$5,40($sp)

addu	$3,$10,$3
move	$12,$17
sll	$3,$3,2
addu	$3,$fp,$3
lw	$3,0($3)
$L178:
sll	$8,$3,8
sll	$6,$3,2
addiu	$5,$2,11568
addu	$3,$3,$8
addu	$6,$24,$6
addu	$5,$4,$5
andi	$3,$3,0xffff
lw	$6,0($6)
andi	$11,$15,0x1
sra	$9,$15,1
sh	$3,0($5)
beq	$23,$0,$L179
sh	$3,8($5)

sll	$5,$9,1
sll	$3,$11,1
addu	$9,$5,$9
addu	$3,$3,$11
mul	$8,$19,$9
addiu	$5,$2,2812
addiu	$2,$2,2852
sll	$5,$5,2
sll	$2,$2,2
addu	$5,$4,$5
addu	$2,$4,$2
addu	$3,$8,$3
sll	$3,$3,2
addu	$12,$12,$3
lh	$9,2($12)
lh	$8,0($12)
mul	$3,$6,$9
mul	$6,$6,$8
addiu	$3,$3,128
addiu	$6,$6,128
sra	$3,$3,8
sra	$6,$6,8
subu	$9,$3,$9
subu	$8,$6,$8
sll	$3,$3,16
sll	$9,$9,16
andi	$6,$6,0xffff
andi	$8,$8,0xffff
addu	$3,$6,$3
addu	$6,$8,$9
sw	$3,0($5)
sw	$3,4($5)
sw	$3,32($5)
sw	$3,36($5)
sw	$6,0($2)
sw	$6,4($2)
sw	$6,32($2)
b	$L175
sw	$6,36($2)

$L268:
lbu	$7,16($sp)
lbu	$6,20($sp)
sw	$0,11296($4)
sll	$3,$7,8
sw	$0,11300($4)
sll	$2,$6,8
sw	$0,11304($4)
addu	$3,$3,$7
sw	$0,11308($4)
addu	$2,$2,$6
sw	$0,11328($4)
sll	$7,$3,16
sw	$0,11332($4)
sll	$6,$2,16
sw	$0,11336($4)
addu	$3,$3,$7
sw	$0,11340($4)
addu	$2,$2,$6
sw	$0,11360($4)
sw	$0,11364($4)
sw	$3,11580($4)
sw	$3,11588($4)
sw	$3,11596($4)
sw	$3,11604($4)
sw	$2,11620($4)
sw	$2,11628($4)
sw	$2,11636($4)
sw	$2,11644($4)
li	$2,-65536			# 0xffffffffffff0000
sw	$0,11368($4)
sw	$0,11372($4)
addiu	$2,$2,24199
sw	$0,11392($4)
sw	$0,11396($4)
sw	$0,11400($4)
sw	$0,11404($4)
sw	$0,11456($4)
sw	$0,11460($4)
sw	$0,11464($4)
sw	$0,11468($4)
sw	$0,11488($4)
sw	$0,11492($4)
sw	$0,11496($4)
sw	$0,11500($4)
sw	$0,11520($4)
sw	$0,11524($4)
sw	$0,11528($4)
sw	$0,11532($4)
sw	$0,11552($4)
sw	$0,11556($4)
sw	$0,11560($4)
sw	$0,11564($4)
lw	$3,0($5)
and	$2,$3,$2
ori	$2,$2,0x108
b	$L275
sw	$2,0($5)

$L165:
lw	$5,40($sp)
lw	$8,44($sp)
addu	$21,$5,$21
lw	$5,36($sp)
lb	$3,0($21)
addu	$3,$10,$3
sll	$3,$3,2
addu	$3,$8,$3
b	$L166
lw	$3,0($3)

$L121:
beq	$2,$0,$L131
lw	$12,16($sp)

lw	$8,16($sp)
andi	$25,$25,0x7
lw	$7,20($sp)
andi	$6,$8,0xff
andi	$5,$7,0xff
sll	$3,$6,8
sll	$2,$5,8
addu	$3,$3,$6
addu	$2,$2,$5
sll	$6,$3,16
sll	$5,$2,16
addu	$3,$3,$6
addu	$2,$2,$5
sw	$3,11580($4)
sw	$3,11588($4)
sw	$3,11596($4)
sw	$3,11604($4)
sw	$2,11620($4)
sw	$2,11628($4)
sw	$2,11636($4)
bne	$25,$0,$L132
sw	$2,11644($4)

li	$2,131072			# 0x20000
addu	$2,$4,$2
lw	$3,-29804($2)
bne	$3,$0,$L257
lw	$3,8($sp)

lb	$3,0($17)
bne	$3,$0,$L133
nop

lhu	$2,0($fp)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L132
nop

lhu	$2,2($fp)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L257
lw	$3,8($sp)

$L134:
blez	$8,$L135
move	$3,$0

lw	$3,8($sp)
$L135:
bgtz	$7,$L257
nop

b	$L136
move	$2,$0

$L133:
bgez	$3,$L257
lw	$3,8($sp)

lw	$10,32($sp)
lb	$3,0($10)
bne	$3,$0,$L257
lw	$3,8($sp)

lw	$11,36($sp)
lhu	$3,0($11)
addiu	$3,$3,1
andi	$3,$3,0xffff
sltu	$3,$3,3
beq	$3,$0,$L257
lw	$3,8($sp)

lhu	$3,2($11)
addiu	$3,$3,1
andi	$3,$3,0xffff
sltu	$3,$3,3
beq	$3,$0,$L257
lw	$3,8($sp)

lw	$2,9444($2)
sltu	$2,$2,34
beq	$2,$0,$L134
nop

$L132:
lw	$3,8($sp)
$L257:
lw	$2,12($sp)
$L136:
sw	$3,11296($4)
sw	$3,11300($4)
sw	$3,11304($4)
sw	$3,11308($4)
sw	$3,11328($4)
sw	$3,11332($4)
sw	$3,11336($4)
sw	$3,11340($4)
sw	$3,11360($4)
sw	$3,11364($4)
sw	$3,11368($4)
sw	$3,11372($4)
sw	$3,11392($4)
sw	$3,11396($4)
sw	$3,11400($4)
sw	$3,11404($4)
sw	$2,11456($4)
sw	$2,11460($4)
sw	$2,11464($4)
sw	$2,11468($4)
sw	$2,11488($4)
sw	$2,11492($4)
sw	$2,11496($4)
sw	$2,11500($4)
sw	$2,11520($4)
sw	$2,11524($4)
sw	$2,11528($4)
sw	$2,11532($4)
sw	$2,11552($4)
sw	$2,11556($4)
sw	$2,11560($4)
b	$L94
sw	$2,11564($4)

$L179:
sll	$11,$11,1
sll	$9,$9,1
move	$8,$0
$L180:
sra	$2,$8,1
andi	$13,$8,0x1
addu	$2,$2,$9
addu	$13,$13,$11
mul	$5,$19,$2
addu	$3,$20,$8
addiu	$8,$8,1
addu	$13,$5,$13
lbu	$5,0($3)
sll	$13,$13,2
addiu	$14,$5,2812
addu	$13,$12,$13
sll	$14,$14,2
addiu	$5,$5,2852
lh	$2,0($13)
addu	$14,$4,$14
sll	$5,$5,2
mul	$2,$6,$2
addu	$5,$4,$5
addiu	$2,$2,128
sra	$2,$2,8
sll	$2,$2,16
sra	$2,$2,16
sh	$2,0($14)
lh	$3,2($13)
mul	$3,$6,$3
addiu	$3,$3,128
sra	$3,$3,8
sll	$3,$3,16
sra	$3,$3,16
sh	$3,2($14)
lh	$14,2($13)
lh	$13,0($13)
subu	$3,$3,$14
subu	$2,$2,$13
sll	$3,$3,16
andi	$2,$2,0xffff
addu	$2,$2,$3
bne	$8,$22,$L180
sw	$2,0($5)

b	$L276
addiu	$15,$15,1

$L267:
bgez	$2,$L282
lw	$7,8($sp)

sw	$0,20($sp)
bne	$3,$0,$L183
sw	$0,16($sp)

lw	$2,0($5)
li	$6,61440			# 0xf000
ori	$2,$2,0xf000
b	$L109
sw	$2,0($5)

$L199:
move	$3,$0
move	$2,$0
b	$L170
move	$6,$0

$L177:
lw	$8,44($sp)
lw	$12,36($sp)
addu	$3,$5,$15
lb	$3,0($3)
addu	$3,$10,$3
sll	$3,$3,2
addu	$3,$8,$3
b	$L178
lw	$3,0($3)

$L131:
li	$2,60296			# 0xeb88
lw	$22,20($sp)
li	$24,131072			# 0x20000
andi	$13,$6,0x8
lw	$10,8($sp)
andi	$8,$12,0xff
lw	$9,12($sp)
sw	$12,40($sp)
sll	$12,$8,8
andi	$7,$22,0xff
sw	$0,44($sp)
sll	$11,$7,8
sw	$13,48($sp)
addu	$12,$8,$12
lw	$8,%got(scan8)($28)
addu	$11,$7,$11
addu	$2,$4,$2
addiu	$8,$8,%lo(scan8)
move	$7,$0
addu	$24,$4,$24
li	$15,4			# 0x4
andi	$12,$12,0xffff
andi	$11,$11,0xffff
andi	$25,$25,0x7
$L148:
beq	$3,$0,$L137
nop

lhu	$13,0($2)
andi	$13,$13,0x100
beq	$13,$0,$L138
nop

$L137:
sh	$6,0($2)
lbu	$13,0($8)
addiu	$16,$13,2812
addiu	$14,$13,2852
addiu	$19,$13,11568
sll	$16,$16,2
sll	$14,$14,2
addiu	$13,$13,11608
addu	$16,$4,$16
addu	$14,$4,$14
addu	$19,$4,$19
sw	$10,0($16)
addu	$13,$4,$13
sw	$10,4($16)
sw	$10,32($16)
sw	$10,36($16)
sw	$9,0($14)
sw	$9,4($14)
sw	$9,32($14)
sw	$9,36($14)
sh	$12,0($19)
sh	$12,8($19)
sh	$11,0($13)
bne	$25,$0,$L138
sh	$11,8($13)

lw	$13,-29804($24)
bne	$13,$0,$L138
addu	$13,$17,$7

lb	$13,0($13)
beq	$13,$0,$L139
move	$21,$fp

bgez	$13,$L138
lw	$19,32($sp)

addu	$13,$19,$7
lb	$13,0($13)
bne	$13,$0,$L138
nop

lw	$13,9444($24)
sltu	$13,$13,34
bne	$13,$0,$L138
lw	$21,36($sp)

$L139:
lw	$13,48($sp)
andi	$20,$7,0x1
beq	$13,$0,$L140
sra	$19,$7,1

sll	$13,$20,1
sll	$23,$19,1
addu	$20,$13,$20
addu	$13,$23,$19
mul	$19,$18,$13
addu	$13,$19,$20
sll	$13,$13,2
addu	$21,$21,$13
lhu	$13,0($21)
addiu	$13,$13,1
andi	$13,$13,0xffff
sltu	$13,$13,3
beq	$13,$0,$L138
nop

lhu	$13,2($21)
addiu	$13,$13,1
andi	$13,$13,0xffff
sltu	$13,$13,3
beq	$13,$0,$L138
lw	$20,40($sp)

bne	$20,$0,$L141
nop

sw	$0,0($16)
sw	$0,4($16)
sw	$0,32($16)
sw	$0,36($16)
$L141:
bne	$22,$0,$L283
lw	$13,44($sp)

sw	$0,0($14)
sw	$0,4($14)
sw	$0,32($14)
sw	$0,36($14)
lw	$13,44($sp)
$L283:
addiu	$13,$13,4
sw	$13,44($sp)
$L138:
addiu	$7,$7,1
addiu	$2,$2,2
bne	$7,$15,$L148
addiu	$8,$8,4

bne	$3,$0,$L281
lw	$fp,100($sp)

lw	$3,44($sp)
andi	$2,$3,0xf
bne	$2,$0,$L284
lw	$23,96($sp)

li	$3,-65536			# 0xffffffffffff0000
lw	$2,0($5)
addiu	$3,$3,24199
and	$2,$2,$3
ori	$2,$2,0x108
b	$L94
sw	$2,0($5)

$L113:
li	$21,2			# 0x2
addu	$8,$4,$8
lw	$8,5148($8)
addu	$17,$17,$8
sll	$8,$17,2
addu	$2,$2,$8
b	$L114
lw	$25,0($2)

$L272:
sll	$7,$9,2
b	$L262
addu	$7,$fp,$7

$L271:
ori	$7,$7,0x100
or	$9,$7,$9
b	$L116
sw	$9,0($5)

$L273:
lw	$2,12($12)
b	$L104
sw	$2,0($15)

$L274:
lw	$2,-16($12)
b	$L104
sw	$2,0($15)

$L171:
lw	$5,44($sp)
lw	$17,36($sp)
lb	$2,0($3)
addu	$2,$10,$2
sll	$2,$2,2
addu	$2,$5,$2
lw	$6,0($2)
sll	$2,$6,2
addu	$24,$24,$2
b	$L172
lw	$2,0($24)

$L183:
b	$L109
li	$6,61440			# 0xf000

$L140:
sll	$20,$20,1
sw	$0,52($sp)
sll	$19,$19,1
sw	$2,56($sp)
move	$14,$0
move	$16,$8
$L146:
sra	$13,$14,1
andi	$23,$14,0x1
addu	$13,$13,$19
addu	$23,$23,$20
mul	$2,$18,$13
addu	$13,$2,$23
sll	$13,$13,2
addu	$13,$21,$13
lhu	$23,0($13)
addiu	$23,$23,1
andi	$23,$23,0xffff
sltu	$23,$23,3
beq	$23,$0,$L143
addiu	$14,$14,1

lhu	$13,2($13)
addiu	$13,$13,1
andi	$13,$13,0xffff
sltu	$13,$13,3
beq	$13,$0,$L143
lw	$13,52($sp)

lw	$2,40($sp)
addiu	$13,$13,1
bne	$2,$0,$L144
sw	$13,52($sp)

lbu	$13,0($16)
addiu	$13,$13,2812
sll	$13,$13,2
addu	$13,$4,$13
sw	$0,0($13)
$L144:
bne	$22,$0,$L143
nop

lbu	$13,0($16)
addiu	$13,$13,2852
sll	$13,$13,2
addu	$13,$4,$13
sw	$0,0($13)
$L143:
bne	$14,$15,$L146
addiu	$16,$16,1

lw	$14,52($sp)
andi	$13,$14,0x3
bne	$13,$0,$L147
lw	$2,56($sp)

lhu	$13,0($2)
addiu	$13,$13,-56
sh	$13,0($2)
$L147:
lw	$19,44($sp)
lw	$20,52($sp)
addu	$19,$19,$20
b	$L138
sw	$19,44($sp)

.set	macro
.set	reorder
.end	ff_h264_pred_direct_motion
.size	ff_h264_pred_direct_motion, .-ff_h264_pred_direct_motion
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"sub4x4-------->\012\000"
.section	.text.pred_direct_motion_hw,"ax",@progbits
.align	2
.align	5
.globl	pred_direct_motion_hw
.set	nomips16
.set	nomicromips
.ent	pred_direct_motion_hw
.type	pred_direct_motion_hw, @function
pred_direct_motion_hw:
.frame	$sp,112,$31		# vars= 48, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-112
lw	$2,7996($4)
lw	$9,7992($4)
sw	$16,72($sp)
move	$16,$4
sll	$7,$2,2
sw	$20,88($sp)
lw	$8,168($16)
li	$4,131072			# 0x20000
lw	$6,11864($16)
sll	$3,$9,2
addu	$4,$16,$4
lw	$20,11868($16)
mul	$11,$2,$8
sw	$fp,104($sp)
sw	$19,84($sp)
lw	$10,-29992($4)
lw	$19,-30000($4)
sw	$18,80($sp)
sw	$17,76($sp)
.cprestore	16
addu	$8,$11,$9
sw	$31,108($sp)
mul	$11,$7,$6
sw	$23,100($sp)
sll	$7,$8,2
sw	$22,96($sp)
sll	$9,$9,1
sw	$21,92($sp)
addu	$7,$10,$7
lw	$18,-29908($4)
lw	$17,0($5)
lw	$fp,0($7)
addu	$6,$11,$3
lw	$11,-29996($4)
sll	$3,$2,1
lw	$4,-29904($4)
andi	$17,$17,0x40
mul	$8,$3,$20
sll	$3,$6,2
addu	$11,$11,$3
addu	$19,$19,$3
andi	$3,$fp,0x40
sw	$11,48($sp)
addu	$2,$8,$9
addu	$18,$18,$2
beq	$3,$0,$L286
addu	$20,$4,$2

lw	$2,11956($16)
beq	$2,$0,$L443
lw	$25,%call16(printf)($28)

$L286:
bne	$17,$0,$L464
li	$2,61504			# 0xf040

andi	$2,$fp,0xf
beq	$2,$0,$L288
li	$2,61504			# 0xf040

li	$2,20744			# 0x5108
li	$13,20744			# 0x5108
b	$L290
sw	$2,0($5)

$L288:
$L464:
li	$13,20744			# 0x5108
sw	$2,0($5)
$L287:
beq	$17,$0,$L444
nop

$L290:
li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$2,-5248($2)
beq	$2,$0,$L465
li	$2,65536			# 0x10000

lw	$2,0($5)
ori	$2,$2,0x80
sw	$2,0($5)
li	$2,65536			# 0x10000
$L465:
addu	$3,$16,$2
lw	$4,5140($3)
beq	$4,$0,$L292
addiu	$25,$2,5472

addiu	$11,$sp,40
addiu	$4,$16,11579
addiu	$9,$16,11659
li	$8,-2			# 0xfffffffffffffffe
li	$12,-1			# 0xffffffffffffffff
move	$6,$11
$L303:
lb	$3,-3($4)
lb	$2,0($4)
beq	$3,$8,$L445
lb	$7,-7($4)

bltz	$2,$L294
nop

$L455:
slt	$10,$7,$2
bne	$10,$0,$L446
nop

$L295:
sw	$2,0($6)
$L297:
slt	$2,$3,$2
beq	$2,$0,$L301
nop

bltz	$3,$L301
nop

$L299:
sw	$3,0($6)
$L301:
addiu	$4,$4,40
bne	$4,$9,$L303
addiu	$6,$6,4

lw	$2,40($sp)
bltz	$2,$L447
lw	$3,44($sp)

$L304:
move	$3,$16
addiu	$4,$sp,24
move	$7,$16
move	$8,$0
li	$15,-2			# 0xfffffffffffffffe
li	$23,1			# 0x1
bltz	$2,$L308
li	$14,8			# 0x8

$L450:
lb	$9,11576($7)
lb	$21,11572($7)
beq	$9,$15,$L309
lb	$22,11579($7)

xor	$10,$21,$2
xor	$6,$9,$2
sltu	$10,$10,1
sltu	$6,$6,1
xor	$2,$22,$2
addu	$6,$6,$10
sltu	$2,$2,1
addu	$6,$6,$2
slt	$24,$6,2
bne	$24,$0,$L448
addiu	$12,$3,11280

$L320:
lh	$2,11292($3)
lh	$6,11264($3)
slt	$10,$6,$2
beq	$10,$0,$L321
lh	$9,0($12)

slt	$10,$6,$9
bne	$10,$0,$L449
nop

$L322:
lh	$9,11294($3)
lh	$2,11266($3)
lh	$10,2($12)
sw	$6,0($4)
slt	$6,$2,$9
beq	$6,$0,$L466
slt	$6,$10,$2

slt	$6,$2,$10
$L468:
beq	$6,$0,$L429
nop

slt	$2,$9,$10
movz	$9,$10,$2
move	$2,$9
$L429:
sw	$2,4($4)
$L316:
addiu	$8,$8,4
addiu	$3,$3,160
addiu	$4,$4,8
beq	$8,$14,$L325
addiu	$7,$7,40

$L451:
addu	$2,$11,$8
lw	$2,0($2)
bgez	$2,$L450
nop

$L308:
addiu	$8,$8,4
sw	$0,4($4)
sw	$0,0($4)
addiu	$3,$3,160
addiu	$4,$4,8
bne	$8,$14,$L451
addiu	$7,$7,40

$L325:
lw	$22,44($sp)
bltz	$22,$L452
nop

$L307:
lw	$2,40($sp)
bltz	$2,$L327
li	$3,-4097			# 0xffffffffffffefff

lw	$2,0($5)
$L326:
andi	$2,$2,0x8
beq	$2,$0,$L328
lw	$2,36($sp)

lw	$6,40($sp)
andi	$5,$22,0xff
sll	$2,$5,8
andi	$7,$fp,0x7
andi	$4,$6,0xff
sll	$3,$4,8
addu	$2,$2,$5
addu	$3,$3,$4
sll	$5,$2,16
sll	$4,$3,16
addu	$2,$2,$5
addu	$3,$3,$4
sw	$2,11620($16)
sw	$3,11580($16)
sw	$3,11588($16)
sw	$3,11596($16)
sw	$3,11604($16)
sw	$2,11628($16)
sw	$2,11636($16)
beq	$7,$0,$L453
sw	$2,11644($16)

lw	$2,36($sp)
$L469:
lw	$5,28($sp)
lhu	$3,32($sp)
lhu	$4,24($sp)
sll	$2,$2,16
sll	$5,$5,16
addu	$2,$3,$2
addu	$3,$4,$5
$L333:
lw	$31,108($sp)
sw	$3,11296($16)
sw	$3,11300($16)
sw	$3,11304($16)
sw	$3,11308($16)
sw	$3,11328($16)
sw	$3,11332($16)
sw	$3,11336($16)
sw	$3,11340($16)
sw	$3,11360($16)
sw	$3,11364($16)
sw	$3,11368($16)
sw	$3,11372($16)
sw	$3,11392($16)
sw	$3,11396($16)
sw	$3,11400($16)
sw	$3,11404($16)
sw	$2,11456($16)
sw	$2,11460($16)
sw	$2,11464($16)
sw	$2,11468($16)
sw	$2,11488($16)
sw	$2,11492($16)
sw	$2,11496($16)
sw	$2,11500($16)
sw	$2,11520($16)
sw	$2,11524($16)
sw	$2,11528($16)
sw	$2,11532($16)
sw	$2,11552($16)
sw	$2,11556($16)
sw	$2,11560($16)
sw	$2,11564($16)
lw	$fp,104($sp)
lw	$23,100($sp)
lw	$22,96($sp)
lw	$21,92($sp)
lw	$20,88($sp)
lw	$19,84($sp)
lw	$18,80($sp)
lw	$17,76($sp)
lw	$16,72($sp)
j	$31
addiu	$sp,$sp,112

$L292:
lw	$3,-5252($3)
addiu	$22,$2,5664
addiu	$21,$2,5152
addu	$25,$16,$25
addu	$22,$16,$22
bne	$3,$0,$L454
addu	$21,$16,$21

lw	$3,0($5)
$L350:
andi	$3,$3,0x8
beq	$3,$0,$L364
lw	$14,%got(scan8)($28)

andi	$7,$fp,0x7
sw	$0,11620($16)
sw	$0,11628($16)
sw	$0,11636($16)
bne	$7,$0,$L385
sw	$0,11644($16)

lb	$2,0($18)
bltz	$2,$L366
nop

sll	$2,$2,2
addu	$25,$25,$2
lw	$5,0($25)
sll	$2,$5,2
addu	$21,$21,$2
lw	$2,0($21)
$L367:
lh	$7,2($19)
sll	$3,$5,8
lh	$4,0($19)
addu	$5,$3,$5
mul	$6,$7,$2
mul	$3,$4,$2
sll	$2,$5,16
addu	$5,$5,$2
addiu	$2,$6,128
addiu	$3,$3,128
sra	$2,$2,8
sra	$3,$3,8
subu	$7,$2,$7
subu	$4,$3,$4
sll	$2,$2,16
sll	$7,$7,16
andi	$3,$3,0xffff
andi	$4,$4,0xffff
addu	$2,$3,$2
addu	$3,$4,$7
$L365:
sw	$5,11580($16)
sw	$5,11588($16)
sw	$5,11596($16)
sw	$5,11604($16)
sw	$2,11296($16)
sw	$2,11300($16)
sw	$2,11304($16)
sw	$2,11308($16)
sw	$2,11328($16)
sw	$2,11332($16)
sw	$2,11336($16)
sw	$2,11340($16)
sw	$2,11360($16)
sw	$2,11364($16)
sw	$2,11368($16)
sw	$2,11372($16)
sw	$2,11392($16)
sw	$2,11396($16)
sw	$2,11400($16)
sw	$2,11404($16)
sw	$3,11456($16)
sw	$3,11460($16)
sw	$3,11464($16)
sw	$3,11468($16)
sw	$3,11488($16)
sw	$3,11492($16)
sw	$3,11496($16)
sw	$3,11500($16)
sw	$3,11520($16)
sw	$3,11524($16)
sw	$3,11528($16)
sw	$3,11532($16)
sw	$3,11552($16)
sw	$3,11556($16)
sw	$3,11560($16)
sw	$3,11564($16)
lw	$31,108($sp)
lw	$fp,104($sp)
lw	$23,100($sp)
lw	$22,96($sp)
lw	$21,92($sp)
lw	$20,88($sp)
lw	$19,84($sp)
lw	$18,80($sp)
lw	$17,76($sp)
lw	$16,72($sp)
j	$31
addiu	$sp,$sp,112

$L446:
bltz	$7,$L295
nop

sw	$7,0($6)
b	$L297
move	$2,$7

$L445:
bgez	$2,$L455
lb	$3,-8($4)

$L294:
bgez	$7,$L378
sw	$7,0($6)

bgez	$3,$L299
nop

b	$L301
sw	$12,0($6)

$L454:
lw	$3,0($5)
andi	$7,$3,0x80
beq	$7,$0,$L467
xor	$4,$fp,$3

addiu	$25,$2,5856
addiu	$22,$2,6240
addiu	$21,$2,5216
addu	$25,$16,$25
addu	$22,$16,$22
addu	$21,$16,$21
$L467:
andi	$4,$4,0x80
beq	$4,$0,$L350
li	$4,61504			# 0xf040

lw	$6,7996($16)
lw	$9,168($16)
li	$3,-2			# 0xfffffffffffffffe
li	$2,61760			# 0xf140
lw	$8,7992($16)
movn	$2,$4,$17
and	$4,$6,$3
mul	$6,$4,$9
or	$2,$2,$7
andi	$4,$2,0x80
sw	$2,0($5)
beq	$4,$0,$L352
addu	$3,$6,$8

li	$8,131072			# 0x20000
lw	$4,168($16)
sll	$7,$3,2
lw	$6,7996($16)
addu	$8,$16,$8
addu	$3,$3,$4
andi	$6,$6,0x1
lw	$4,-29992($8)
sll	$3,$3,2
addu	$7,$4,$7
addu	$3,$4,$3
lw	$4,0($7)
lw	$3,0($3)
sw	$4,24($sp)
beq	$6,$0,$L353
sw	$3,28($sp)

lw	$7,11868($16)
lw	$6,11864($16)
sll	$7,$7,1
sll	$6,$6,4
subu	$7,$0,$7
subu	$6,$0,$6
addu	$18,$18,$7
addu	$20,$20,$7
lw	$7,48($sp)
addu	$19,$19,$6
addu	$7,$7,$6
sw	$7,48($sp)
$L353:
andi	$4,$4,0xf
bne	$4,$0,$L456
nop

$L354:
ori	$2,$2,0x40
move	$11,$0
sw	$2,0($5)
$L355:
lw	$10,%got(scan8)($28)
li	$9,60296			# 0xeb88
move	$8,$0
addu	$9,$16,$9
addiu	$10,$10,%lo(scan8)
li	$14,20744			# 0x5108
addiu	$13,$sp,24
li	$12,4			# 0x4
$L363:
beq	$17,$0,$L358
sra	$4,$8,1

lhu	$2,0($9)
andi	$2,$2,0x100
beq	$2,$0,$L359
nop

$L358:
sll	$3,$4,2
sh	$14,0($9)
lbu	$2,0($10)
addu	$3,$13,$3
addiu	$5,$2,11608
lw	$6,0($3)
addu	$3,$16,$5
andi	$5,$6,0x7
sh	$0,0($3)
beq	$5,$0,$L360
sh	$0,8($3)

addiu	$4,$2,2812
addiu	$3,$2,2852
sll	$4,$4,2
addiu	$2,$2,11568
sll	$3,$3,2
addu	$4,$16,$4
addu	$3,$16,$3
addu	$2,$16,$2
sh	$0,0($2)
sh	$0,8($2)
sw	$0,0($4)
sw	$0,4($4)
sw	$0,32($4)
sw	$0,36($4)
sw	$0,0($3)
sw	$0,4($3)
sw	$0,32($3)
sw	$0,36($3)
$L359:
addiu	$8,$8,1
addiu	$9,$9,2
bne	$8,$12,$L363
addiu	$10,$10,4

lw	$31,108($sp)
lw	$fp,104($sp)
lw	$23,100($sp)
lw	$22,96($sp)
lw	$21,92($sp)
lw	$20,88($sp)
lw	$19,84($sp)
lw	$18,80($sp)
lw	$17,76($sp)
lw	$16,72($sp)
j	$31
addiu	$sp,$sp,112

$L444:
lw	$2,0($5)
ori	$2,$2,0x100
b	$L290
sw	$2,0($5)

$L328:
andi	$4,$22,0xff
lw	$23,40($sp)
sll	$14,$4,8
lhu	$8,32($sp)
li	$11,60296			# 0xeb88
sll	$2,$2,16
lw	$3,28($sp)
andi	$5,$23,0xff
lhu	$9,24($sp)
addu	$8,$2,$8
lw	$12,%got(scan8)($28)
li	$2,131072			# 0x20000
sll	$15,$5,8
sll	$3,$3,16
addu	$2,$16,$2
addu	$15,$5,$15
addu	$14,$4,$14
addu	$9,$3,$9
sw	$2,52($sp)
addu	$11,$16,$11
addiu	$12,$12,%lo(scan8)
move	$10,$0
li	$21,4			# 0x4
andi	$7,$fp,0x7
andi	$15,$15,0xffff
andi	$14,$14,0xffff
andi	$fp,$13,0x8
$L346:
beq	$17,$0,$L335
nop

lhu	$2,0($11)
andi	$2,$2,0x100
beq	$2,$0,$L336
nop

$L335:
sh	$13,0($11)
lbu	$4,0($12)
addiu	$3,$4,2812
addiu	$2,$4,2852
addiu	$5,$4,11568
sll	$3,$3,2
sll	$2,$2,2
addiu	$4,$4,11608
addu	$3,$16,$3
addu	$2,$16,$2
addu	$5,$16,$5
sw	$9,0($3)
addu	$4,$16,$4
sw	$9,4($3)
sw	$9,32($3)
sw	$9,36($3)
sw	$8,0($2)
sw	$8,4($2)
sw	$8,32($2)
sw	$8,36($2)
sh	$15,0($5)
sh	$15,8($5)
sh	$14,0($4)
bne	$7,$0,$L336
sh	$14,8($4)

lw	$4,11868($16)
sra	$6,$10,1
andi	$5,$10,0x1
mul	$24,$6,$4
addu	$4,$24,$5
addu	$24,$18,$4
lb	$24,0($24)
beq	$24,$0,$L381
nop

bgez	$24,$L336
addu	$4,$20,$4

lb	$4,0($4)
bne	$4,$0,$L336
lw	$24,52($sp)

lw	$4,9444($24)
slt	$24,$4,34
beq	$24,$0,$L382
nop

bne	$4,$0,$L336
nop

$L382:
beq	$fp,$0,$L339
lw	$25,48($sp)

$L458:
sll	$4,$6,1
sll	$24,$5,1
addu	$6,$4,$6
lw	$4,11864($16)
addu	$5,$24,$5
mul	$24,$6,$4
addu	$5,$24,$5
sll	$5,$5,2
addu	$25,$25,$5
lhu	$4,0($25)
addiu	$4,$4,1
andi	$4,$4,0xffff
sltu	$4,$4,3
beq	$4,$0,$L336
nop

lhu	$4,2($25)
addiu	$4,$4,1
andi	$4,$4,0xffff
sltu	$4,$4,3
beq	$4,$0,$L336
nop

bne	$23,$0,$L340
nop

sw	$0,0($3)
sw	$0,4($3)
sw	$0,32($3)
sw	$0,36($3)
$L340:
bne	$22,$0,$L336
nop

sw	$0,0($2)
sw	$0,4($2)
sw	$0,32($2)
sw	$0,36($2)
$L336:
addiu	$10,$10,1
addiu	$11,$11,2
bne	$10,$21,$L346
addiu	$12,$12,4

lw	$31,108($sp)
$L462:
lw	$fp,104($sp)
lw	$23,100($sp)
lw	$22,96($sp)
lw	$21,92($sp)
lw	$20,88($sp)
lw	$19,84($sp)
lw	$18,80($sp)
lw	$17,76($sp)
lw	$16,72($sp)
j	$31
addiu	$sp,$sp,112

$L321:
slt	$10,$9,$6
beq	$10,$0,$L322
nop

slt	$6,$9,$2
lh	$10,2($12)
movz	$2,$9,$6
lh	$9,11294($3)
move	$6,$2
lh	$2,11266($3)
sw	$6,0($4)
slt	$6,$2,$9
bne	$6,$0,$L468
slt	$6,$2,$10

slt	$6,$10,$2
$L466:
beq	$6,$0,$L429
nop

slt	$2,$10,$9
movz	$9,$10,$2
b	$L429
move	$2,$9

$L381:
bne	$fp,$0,$L458
move	$25,$19

$L339:
sll	$5,$5,1
sw	$8,60($sp)
sll	$6,$6,1
sw	$7,64($sp)
move	$4,$0
move	$24,$12
$L345:
sra	$2,$4,1
lw	$7,11864($16)
andi	$3,$4,0x1
addu	$2,$2,$6
addu	$3,$3,$5
mul	$8,$2,$7
addu	$2,$8,$3
sll	$2,$2,2
addu	$2,$25,$2
lhu	$3,0($2)
addiu	$3,$3,1
andi	$3,$3,0xffff
sltu	$3,$3,3
beq	$3,$0,$L342
addiu	$4,$4,1

lhu	$2,2($2)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L342
nop

bne	$23,$0,$L344
nop

lbu	$2,0($24)
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$16,$2
sw	$0,0($2)
$L344:
bne	$22,$0,$L342
nop

lbu	$2,0($24)
addiu	$2,$2,2852
sll	$2,$2,2
addu	$2,$16,$2
sw	$0,0($2)
$L342:
bne	$4,$21,$L345
addiu	$24,$24,1

addiu	$10,$10,1
lw	$8,60($sp)
lw	$7,64($sp)
addiu	$11,$11,2
bne	$10,$21,$L346
addiu	$12,$12,4

b	$L462
lw	$31,108($sp)

$L364:
li	$15,60296			# 0xeb88
move	$24,$0
addu	$15,$16,$15
addiu	$14,$14,%lo(scan8)
li	$23,4			# 0x4
andi	$7,$fp,0x7
andi	$fp,$13,0x8
$L376:
beq	$17,$0,$L368
nop

lhu	$2,0($15)
andi	$2,$2,0x100
beq	$2,$0,$L369
nop

$L368:
sh	$13,0($15)
lbu	$2,0($14)
addiu	$3,$2,11608
addu	$3,$16,$3
sh	$0,0($3)
beq	$7,$0,$L370
sh	$0,8($3)

addiu	$4,$2,2812
addiu	$3,$2,2852
sll	$4,$4,2
addiu	$2,$2,11568
sll	$3,$3,2
addu	$4,$16,$4
addu	$3,$16,$3
addu	$2,$16,$2
sh	$0,0($2)
sh	$0,8($2)
sw	$0,0($4)
sw	$0,4($4)
sw	$0,32($4)
sw	$0,36($4)
sw	$0,0($3)
sw	$0,4($3)
sw	$0,32($3)
sw	$0,36($3)
$L369:
addiu	$24,$24,1
$L463:
addiu	$15,$15,2
bne	$24,$23,$L376
addiu	$14,$14,4

lw	$31,108($sp)
lw	$fp,104($sp)
lw	$23,100($sp)
lw	$22,96($sp)
lw	$21,92($sp)
lw	$20,88($sp)
lw	$19,84($sp)
lw	$18,80($sp)
lw	$17,76($sp)
lw	$16,72($sp)
j	$31
addiu	$sp,$sp,112

$L443:
lw	$4,%got($LC0)($28)
sw	$5,56($sp)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC0)

li	$2,61504			# 0xf040
lw	$5,56($sp)
li	$13,20800			# 0x5140
lw	$28,16($sp)
b	$L287
sw	$2,0($5)

$L453:
lb	$2,0($18)
bne	$2,$0,$L330
nop

lhu	$2,0($19)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L469
lw	$2,36($sp)

lhu	$2,2($19)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L469
lw	$2,36($sp)

$L331:
blez	$6,$L379
lhu	$2,24($sp)

lw	$3,28($sp)
sll	$3,$3,16
addu	$3,$2,$3
$L332:
blez	$22,$L380
lw	$4,36($sp)

lhu	$2,32($sp)
sll	$4,$4,16
b	$L333
addu	$2,$2,$4

$L449:
slt	$6,$2,$9
movz	$2,$9,$6
b	$L322
move	$6,$2

$L370:
lw	$4,11868($16)
sra	$8,$24,1
andi	$10,$24,0x1
mul	$3,$8,$4
addu	$4,$3,$10
addu	$3,$18,$4
lb	$3,0($3)
bltz	$3,$L371
addu	$4,$20,$4

sll	$3,$3,2
move	$9,$19
addu	$3,$25,$3
lw	$3,0($3)
$L372:
sll	$6,$3,8
sll	$5,$3,2
addiu	$4,$2,11568
addu	$3,$3,$6
addu	$5,$21,$5
addu	$4,$16,$4
andi	$3,$3,0xffff
lw	$5,0($5)
sh	$3,0($4)
beq	$fp,$0,$L373
sh	$3,8($4)

sll	$3,$8,1
lw	$6,11864($16)
sll	$4,$10,1
addu	$3,$3,$8
addu	$10,$4,$10
mul	$11,$3,$6
addiu	$4,$2,2812
addiu	$2,$2,2852
sll	$4,$4,2
sll	$2,$2,2
addu	$4,$16,$4
addu	$2,$16,$2
addu	$8,$11,$10
sll	$8,$8,2
addu	$9,$9,$8
lh	$8,2($9)
lh	$6,0($9)
mul	$3,$8,$5
mul	$5,$6,$5
addiu	$3,$3,128
addiu	$5,$5,128
sra	$3,$3,8
sra	$5,$5,8
subu	$8,$3,$8
subu	$6,$5,$6
sll	$3,$3,16
sll	$8,$8,16
andi	$5,$5,0xffff
andi	$6,$6,0xffff
addu	$3,$5,$3
addu	$5,$6,$8
sw	$3,0($4)
sw	$3,4($4)
sw	$3,32($4)
sw	$3,36($4)
sw	$5,0($2)
sw	$5,4($2)
sw	$5,32($2)
b	$L369
sw	$5,36($2)

$L360:
sll	$15,$4,1
lw	$5,11868($16)
andi	$7,$8,0x1
sra	$3,$15,$11
mul	$6,$3,$5
addu	$3,$6,$7
addu	$5,$18,$3
lb	$5,0($5)
bltz	$5,$L361
addu	$3,$20,$3

sll	$5,$5,1
move	$6,$19
sra	$5,$5,$11
sll	$5,$5,2
addu	$5,$25,$5
lw	$5,0($5)
$L362:
sll	$4,$4,3
lw	$3,11864($16)
sll	$23,$7,1
subu	$4,$4,$15
addu	$7,$23,$7
sra	$4,$4,$11
sll	$15,$5,8
mul	$23,$4,$3
sll	$4,$5,2
addu	$5,$5,$15
addu	$4,$21,$4
andi	$5,$5,0xffff
lw	$4,0($4)
addu	$3,$23,$7
addiu	$7,$2,11568
sll	$3,$3,2
addu	$7,$16,$7
addu	$3,$6,$3
sh	$5,0($7)
addiu	$6,$2,2812
sh	$5,8($7)
addiu	$2,$2,2852
lh	$5,2($3)
sll	$6,$6,2
lh	$15,0($3)
sll	$2,$2,2
addu	$6,$16,$6
sll	$3,$5,$11
mul	$7,$15,$4
srl	$5,$3,31
addu	$2,$16,$2
addu	$5,$5,$3
sra	$5,$5,1
mul	$3,$4,$5
addiu	$7,$7,128
sra	$7,$7,8
subu	$4,$7,$15
andi	$7,$7,0xffff
addiu	$3,$3,128
andi	$4,$4,0xffff
sra	$3,$3,8
subu	$5,$3,$5
sll	$3,$3,16
sll	$5,$5,16
addu	$3,$7,$3
addu	$5,$4,$5
sw	$3,0($6)
sw	$3,4($6)
sw	$3,32($6)
sw	$3,36($6)
sw	$5,0($2)
sw	$5,4($2)
sw	$5,32($2)
b	$L359
sw	$5,36($2)

$L309:
lb	$9,11571($7)
xor	$10,$21,$2
sltu	$10,$10,1
xor	$6,$9,$2
sltu	$6,$6,1
xor	$2,$22,$2
addu	$6,$6,$10
sltu	$2,$2,1
addu	$6,$6,$2
slt	$24,$6,2
beq	$24,$0,$L320
addiu	$12,$3,11260

$L448:
beq	$6,$23,$L460
nop

bne	$21,$15,$L320
nop

bne	$9,$15,$L320
nop

beq	$22,$15,$L320
nop

$L430:
lh	$6,11292($3)
lh	$2,11294($3)
b	$L429
sw	$6,0($4)

$L373:
sll	$10,$10,1
sll	$8,$8,1
move	$6,$0
$L374:
sra	$2,$6,1
lw	$11,11864($16)
andi	$3,$6,0x1
addu	$2,$2,$8
addu	$3,$3,$10
mul	$12,$2,$11
addu	$4,$14,$6
addiu	$6,$6,1
lbu	$4,0($4)
addu	$11,$12,$3
addiu	$12,$4,2812
sll	$11,$11,2
sll	$12,$12,2
addu	$11,$9,$11
addu	$12,$16,$12
addiu	$4,$4,2852
lh	$2,0($11)
sll	$4,$4,2
mul	$2,$2,$5
addu	$4,$16,$4
addiu	$2,$2,128
sra	$2,$2,8
sll	$2,$2,16
sra	$2,$2,16
sh	$2,0($12)
lh	$3,2($11)
mul	$3,$3,$5
addiu	$3,$3,128
sra	$3,$3,8
sll	$3,$3,16
sra	$3,$3,16
sh	$3,2($12)
lh	$12,2($11)
lh	$11,0($11)
subu	$3,$3,$12
subu	$2,$2,$11
sll	$3,$3,16
andi	$2,$2,0xffff
addu	$2,$2,$3
bne	$6,$23,$L374
sw	$2,0($4)

b	$L463
addiu	$24,$24,1

$L447:
bgez	$3,$L304
move	$22,$0

sw	$0,40($sp)
sw	$0,36($sp)
sw	$0,32($sp)
sw	$0,28($sp)
b	$L307
sw	$0,24($sp)

$L452:
lw	$2,0($5)
li	$3,-16385			# 0xffffffffffffbfff
and	$13,$13,$3
and	$2,$2,$3
b	$L326
sw	$2,0($5)

$L327:
lw	$2,0($5)
and	$13,$13,$3
and	$2,$2,$3
b	$L326
sw	$2,0($5)

$L385:
move	$5,$0
move	$3,$0
b	$L365
move	$2,$0

$L371:
lw	$9,48($sp)
lb	$3,0($4)
sll	$3,$3,2
addu	$3,$22,$3
b	$L372
lw	$3,0($3)

$L352:
lw	$7,7996($16)
li	$8,131072			# 0x20000
lw	$12,168($16)
li	$9,2			# 0x2
addu	$8,$16,$8
lw	$11,11864($16)
andi	$6,$7,0x1
lw	$10,11868($16)
li	$7,4			# 0x4
addu	$3,$3,$12
lw	$8,-29992($8)
movn	$7,$9,$6
li	$4,1			# 0x1
sll	$3,$3,2
movn	$9,$4,$6
addu	$3,$8,$3
mul	$7,$7,$11
lw	$6,0($3)
mul	$4,$9,$10
sw	$6,28($sp)
sll	$3,$7,2
sw	$6,24($sp)
andi	$7,$6,0x1f
lw	$6,48($sp)
addu	$18,$18,$4
addu	$20,$20,$4
addu	$6,$6,$3
addu	$19,$19,$3
beq	$7,$0,$L357
sw	$6,48($sp)

bne	$17,$0,$L357
nop

ori	$2,$2,0x8
li	$11,2			# 0x2
b	$L355
sw	$2,0($5)

$L330:
bgez	$2,$L469
lw	$2,36($sp)

lb	$2,0($20)
bne	$2,$0,$L469
lw	$2,36($sp)

lw	$11,48($sp)
lhu	$2,0($11)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L469
lw	$2,36($sp)

lhu	$2,2($11)
addiu	$2,$2,1
andi	$2,$2,0xffff
sltu	$2,$2,3
beq	$2,$0,$L469
lw	$2,36($sp)

li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$2,9444($2)
slt	$3,$2,34
beq	$3,$0,$L331
nop

bne	$2,$0,$L469
lw	$2,36($sp)

b	$L331
nop

$L361:
lw	$6,48($sp)
lb	$3,0($3)
sll	$3,$3,1
sra	$3,$3,$11
sll	$3,$3,2
addu	$3,$22,$3
b	$L362
lw	$5,0($3)

$L357:
ori	$2,$2,0x40
li	$11,2			# 0x2
b	$L355
sw	$2,0($5)

$L456:
andi	$3,$3,0xf
beq	$3,$0,$L354
nop

bne	$17,$0,$L354
move	$11,$0

ori	$2,$2,0x10
b	$L355
sw	$2,0($5)

$L460:
bne	$2,$0,$L430
nop

bne	$10,$0,$L461
nop

lh	$6,0($12)
lh	$2,2($12)
sw	$6,0($4)
b	$L316
sw	$2,4($4)

$L366:
lb	$2,0($20)
lw	$19,48($sp)
sll	$2,$2,2
addu	$22,$22,$2
lw	$5,0($22)
sll	$2,$5,2
addu	$21,$21,$2
b	$L367
lw	$2,0($21)

$L461:
lh	$6,11264($3)
lh	$2,11266($3)
sw	$6,0($4)
b	$L316
sw	$2,4($4)

$L379:
b	$L332
move	$3,$0

$L380:
b	$L333
move	$2,$0

$L378:
b	$L297
move	$2,$7

.set	macro
.set	reorder
.end	pred_direct_motion_hw
.size	pred_direct_motion_hw, .-pred_direct_motion_hw
.rdata
.align	2
.type	scan8, @object
.size	scan8, 24
scan8:
.byte	12
.byte	13
.byte	20
.byte	21
.byte	14
.byte	15
.byte	22
.byte	23
.byte	28
.byte	29
.byte	36
.byte	37
.byte	30
.byte	31
.byte	38
.byte	39
.byte	9
.byte	10
.byte	17
.byte	18
.byte	33
.byte	34
.byte	41
.byte	42

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
