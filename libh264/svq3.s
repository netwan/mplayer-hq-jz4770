.file	1 "svq3.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.get_bits.isra.2,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_bits.isra.2
.type	get_bits.isra.2, @function
get_bits.isra.2:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$9,0($5)
srl	$2,$9,3
addu	$4,$4,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
srl	$7,$3,8
sll	$8,$3,8
li	$3,16711680			# 0xff0000
addu	$2,$6,$9
addiu	$3,$3,255
andi	$9,$9,0x7
and	$4,$7,$3
sw	$2,0($5)
li	$3,-16777216			# 0xffffffffff000000
subu	$2,$0,$6
ori	$3,$3,0xff00
and	$3,$8,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$3,$3,$9
.set	noreorder
.set	nomacro
j	$31
srl	$2,$3,$2
.set	macro
.set	reorder

.end	get_bits.isra.2
.size	get_bits.isra.2, .-get_bits.isra.2
.section	.text.svq3_get_ue_golomb.isra.6,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	svq3_get_ue_golomb.isra.6
.type	svq3_get_ue_golomb.isra.6, @function
svq3_get_ue_golomb.isra.6:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$8,0($5)
li	$11,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
addiu	$11,$11,255
srl	$2,$8,3
ori	$10,$10,0xff00
addu	$2,$4,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$2,$2,8
srl	$3,$3,8
and	$2,$2,$10
and	$3,$3,$11
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$7,$8,0x7
or	$2,$3,$2
sll	$2,$2,$7
li	$3,-1434451968			# 0xffffffffaa800000
and	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L3
lw	$13,%got(ff_interleaved_golomb_vlc_len)($28)
.set	macro
.set	reorder

srl	$7,$2,24
addu	$2,$13,$7
lbu	$2,0($2)
sltu	$3,$2,9
.set	noreorder
.set	nomacro
bne	$3,$0,$L11
lw	$12,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$3,9			# 0x9
.set	noreorder
.set	nomacro
bne	$2,$3,$L24
addiu	$8,$8,8
.set	macro
.set	reorder

lw	$12,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$9,1			# 0x1
li	$14,9			# 0x9
$L9:
srl	$3,$8,3
addu	$7,$12,$7
addu	$3,$4,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$6,8
sll	$2,$6,8
and	$3,$3,$11
and	$2,$2,$10
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$15,$8,0x7
or	$3,$3,$2
sll	$2,$3,$15
lbu	$3,0($7)
sll	$9,$9,4
srl	$7,$2,24
or	$9,$3,$9
addu	$2,$13,$7
lbu	$2,0($2)
sltu	$3,$2,9
beq	$3,$0,$L22
$L4:
addu	$8,$8,$2
$L7:
addiu	$2,$2,-1
$L23:
sw	$8,0($5)
addu	$7,$12,$7
sra	$3,$2,1
lbu	$2,0($7)
sll	$9,$9,$3
or	$2,$9,$2
.set	noreorder
.set	nomacro
j	$31
addiu	$2,$2,-1
.set	macro
.set	reorder

$L22:
.set	noreorder
.set	nomacro
beq	$2,$14,$L9
addiu	$8,$8,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L23
addiu	$2,$2,-1
.set	macro
.set	reorder

$L11:
.set	noreorder
.set	nomacro
b	$L4
li	$9,1			# 0x1
.set	macro
.set	reorder

$L3:
lw	$4,%got(ff_interleaved_golomb_vlc_len)($28)
srl	$2,$2,24
lw	$3,%got(ff_interleaved_ue_golomb_vlc_code)($28)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$3,0($4)
lbu	$2,0($2)
addu	$8,$3,$8
.set	noreorder
.set	nomacro
j	$31
sw	$8,0($5)
.set	macro
.set	reorder

$L24:
.set	noreorder
.set	nomacro
b	$L7
li	$9,1			# 0x1
.set	macro
.set	reorder

.end	svq3_get_ue_golomb.isra.6
.size	svq3_get_ue_golomb.isra.6, .-svq3_get_ue_golomb.isra.6
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"SVQ3 does not support multithreaded decoding, patch welc"
.ascii	"ome! (check latest SVN too)\012\000"
.align	2
$LC1:
.ascii	"SEQH\000"
.align	2
$LC2:
.ascii	"watermark size: %dx%d\012\000"
.align	2
$LC3:
.ascii	"u1: %x u2: %x u3: %x compressed data size: %d offset: %d"
.ascii	"\012\000"
.align	2
$LC4:
.ascii	"could not uncompress watermark logo\012\000"
.align	2
$LC5:
.ascii	"watermark key %#x\012\000"
.section	.text.unlikely.svq3_decode_init,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	svq3_decode_init
.type	svq3_decode_init, @function
svq3_decode_init:
.frame	$sp,120,$31		# vars= 40, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,612($4)
addiu	$sp,$sp,-120
.cprestore	32
slt	$2,$2,2
sw	$23,108($sp)
sw	$17,84($sp)
move	$23,$4
sw	$31,116($sp)
sw	$fp,112($sp)
sw	$22,104($sp)
sw	$21,100($sp)
sw	$20,96($sp)
sw	$19,92($sp)
sw	$18,88($sp)
sw	$16,80($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L26
lw	$17,136($4)
.set	macro
.set	reorder

lw	$6,%got($LC0)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$16,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L62
lw	$31,116($sp)
.set	macro
.set	reorder

$L26:
lw	$25,%call16(ff_h264_decode_init)($28)
.reloc	1f,R_MIPS_JALR,ff_h264_decode_init
1:	jalr	$25
.set	noreorder
.set	nomacro
bltz	$2,$L51
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$6,132($23)
li	$2,131072			# 0x20000
lw	$8,12($23)
li	$5,1			# 0x1
lw	$7,596($23)
addu	$2,$17,$2
lw	$4,124($17)
lw	$6,48($6)
sw	$8,64($17)
sw	$7,68($17)
sw	$5,2948($17)
sw	$5,9452($2)
lw	$2,0($6)
.set	noreorder
.set	nomacro
beq	$4,$0,$L29
sw	$2,52($23)
.set	macro
.set	reorder

$L34:
.set	noreorder
.set	nomacro
b	$L27
move	$16,$0
.set	macro
.set	reorder

$L29:
lw	$8,40($23)
li	$2,196608			# 0x30000
lw	$7,44($23)
li	$6,4			# 0x4
addu	$2,$17,$2
lw	$25,%call16(MPV_common_init)($28)
move	$4,$17
sw	$8,8($17)
sw	$7,12($17)
sw	$5,-15024($2)
sw	$5,-15020($2)
sw	$0,-15016($2)
sw	$6,10744($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_common_init
1:	jalr	$25
sw	$6,10740($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L51
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$2,160($17)
move	$4,$17
lw	$25,%call16(ff_h264_alloc_tables)($28)
sll	$2,$2,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_alloc_tables
1:	jalr	$25
sw	$2,11864($17)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$18,24($23)
lw	$20,28($23)
lw	$16,%got($LC1)($28)
move	$19,$18
addiu	$16,$16,%lo($LC1)
$L31:
subu	$2,$19,$18
slt	$2,$2,$20
.set	noreorder
.set	nomacro
beq	$2,$0,$L60
lw	$25,%call16(memcmp)($28)
.set	macro
.set	reorder

li	$6,4			# 0x4
move	$4,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcmp
1:	jalr	$25
move	$5,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L32
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$5,%got($LC1)($28)
$L63:
li	$6,4			# 0x4
lw	$25,%call16(memcmp)($28)
move	$4,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcmp
1:	jalr	$25
addiu	$5,$5,%lo($LC1)
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L33
move	$16,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L27
move	$16,$0
.set	macro
.set	reorder

$L32:
.set	noreorder
.set	nomacro
b	$L31
addiu	$19,$19,1
.set	macro
.set	reorder

$L60:
beq	$19,$0,$L34
.set	noreorder
.set	nomacro
b	$L63
lw	$5,%got($LC1)($28)
.set	macro
.set	reorder

$L33:
li	$18,16711680			# 0xff0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 7($19)  
lwr $4, 4($19)  

# 0 "" 2
#NO_APP
addiu	$18,$18,255
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$18
li	$18,-16777216			# 0xffffffffff000000
ori	$18,$18,0xff00
and	$18,$4,$18
or	$18,$2,$18
sll	$2,$18,16
srl	$18,$18,16
or	$18,$18,$2
sll	$5,$18,3
sra	$2,$5,3
.set	noreorder
.set	nomacro
bltz	$2,$L54
addiu	$4,$19,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$5,$L64
lw	$21,%got(get_bits.isra.2)($28)
.set	macro
.set	reorder

$L54:
move	$2,$0
move	$5,$0
move	$4,$0
lw	$21,%got(get_bits.isra.2)($28)
$L64:
addu	$2,$4,$2
addiu	$20,$sp,48
sw	$4,40($sp)
li	$6,3			# 0x3
sw	$5,52($sp)
addiu	$21,$21,%lo(get_bits.isra.2)
sw	$2,44($sp)
sw	$0,48($sp)
move	$25,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_bits.isra.2
1:	jalr	$25
move	$5,$20
.set	macro
.set	reorder

sltu	$4,$2,8
.set	noreorder
.set	nomacro
beq	$4,$0,$L38
lw	$28,32($sp)
.set	macro
.set	reorder

sll	$4,$2,2
lw	$2,%got($L40)($28)
addiu	$2,$2,%lo($L40)
addu	$2,$2,$4
lw	$2,0($2)
addu	$2,$2,$28
j	$2
.rdata
.align	2
.align	2
$L40:
.gpword	$L39
.gpword	$L41
.gpword	$L42
.gpword	$L43
.gpword	$L44
.gpword	$L45
.gpword	$L46
.gpword	$L47
.section	.text.unlikely.svq3_decode_init
$L39:
li	$2,160			# 0xa0
sw	$2,40($23)
li	$2,120			# 0x78
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L41:
li	$2,128			# 0x80
sw	$2,40($23)
li	$2,96			# 0x60
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L42:
li	$2,176			# 0xb0
sw	$2,40($23)
li	$2,144			# 0x90
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L43:
li	$2,352			# 0x160
sw	$2,40($23)
li	$2,288			# 0x120
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L44:
li	$2,704			# 0x2c0
sw	$2,40($23)
li	$2,576			# 0x240
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L45:
li	$2,240			# 0xf0
sw	$2,40($23)
li	$2,180			# 0xb4
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L46:
li	$2,320			# 0x140
sw	$2,40($23)
li	$2,240			# 0xf0
.set	noreorder
.set	nomacro
b	$L38
sw	$2,44($23)
.set	macro
.set	reorder

$L47:
lw	$4,40($sp)
li	$6,12			# 0xc
move	$25,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_bits.isra.2
1:	jalr	$25
move	$5,$20
.set	macro
.set	reorder

li	$6,12			# 0xc
lw	$4,40($sp)
move	$5,$20
move	$25,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_bits.isra.2
1:	jalr	$25
sw	$2,40($23)
.set	macro
.set	reorder

lw	$28,32($sp)
sw	$2,44($23)
$L38:
lw	$8,48($sp)
li	$9,196608			# 0x30000
lw	$4,40($sp)
addu	$9,$17,$9
srl	$2,$8,3
andi	$10,$8,0x7
addu	$2,$4,$2
addiu	$5,$8,1
addiu	$11,$8,6
lbu	$6,0($2)
srl	$2,$5,3
sw	$5,48($sp)
andi	$7,$5,0x7
addu	$5,$4,$2
sll	$6,$6,$10
srl	$10,$11,3
andi	$6,$6,0x00ff
srl	$6,$6,7
addu	$10,$4,$10
addiu	$2,$8,8
sw	$6,-15024($9)
andi	$12,$11,0x7
lbu	$5,0($5)
sw	$11,48($sp)
sll	$5,$5,$7
andi	$5,$5,0x00ff
srl	$5,$5,7
sw	$5,-15020($9)
lbu	$5,0($10)
sw	$2,48($sp)
sll	$2,$5,$12
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10096($17)
$L48:
lw	$5,48($sp)
srl	$8,$5,3
andi	$9,$5,0x7
addu	$8,$4,$8
addiu	$7,$5,1
lbu	$6,0($8)
sll	$6,$6,$9
andi	$6,$6,0x00ff
srl	$6,$6,7
.set	noreorder
.set	nomacro
beq	$6,$0,$L61
sw	$7,48($sp)
.set	macro
.set	reorder

addiu	$5,$5,9
.set	noreorder
.set	nomacro
b	$L48
sw	$5,48($sp)
.set	macro
.set	reorder

$L61:
srl	$6,$7,3
addiu	$5,$5,2
addu	$6,$4,$6
andi	$7,$7,0x7
li	$8,196608			# 0x30000
lbu	$6,0($6)
xori	$2,$2,0x1
sw	$5,48($sp)
addu	$17,$17,$8
sll	$5,$6,$7
andi	$5,$5,0x00ff
srl	$5,$5,7
sw	$5,-15016($17)
.set	noreorder
.set	nomacro
beq	$5,$0,$L27
sw	$2,264($23)
.set	macro
.set	reorder

lw	$22,%got(svq3_get_ue_golomb.isra.6)($28)
addiu	$22,$22,%lo(svq3_get_ue_golomb.isra.6)
move	$25,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_get_ue_golomb.isra.6
1:	jalr	$25
move	$5,$20
.set	macro
.set	reorder

move	$5,$20
lw	$4,40($sp)
move	$25,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_get_ue_golomb.isra.6
1:	jalr	$25
sw	$2,76($sp)
.set	macro
.set	reorder

move	$5,$20
lw	$4,40($sp)
move	$25,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_get_ue_golomb.isra.6
1:	jalr	$25
move	$fp,$2
.set	macro
.set	reorder

li	$6,8			# 0x8
lw	$4,40($sp)
move	$5,$20
move	$25,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_bits.isra.2
1:	jalr	$25
sw	$2,64($sp)
.set	macro
.set	reorder

li	$6,2			# 0x2
lw	$4,40($sp)
move	$5,$20
move	$25,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_bits.isra.2
1:	jalr	$25
sw	$2,68($sp)
.set	macro
.set	reorder

move	$5,$20
lw	$4,40($sp)
move	$25,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_get_ue_golomb.isra.6
1:	jalr	$25
move	$20,$2
.set	macro
.set	reorder

li	$5,-1			# 0xffffffffffffffff
lw	$7,76($sp)
lw	$22,48($sp)
lw	$28,32($sp)
mul	$4,$7,$fp
teq	$fp,$0,7
divu	$0,$5,$fp
addiu	$22,$22,7
srl	$10,$7,30
sll	$6,$7,2
sra	$22,$22,3
sll	$4,$4,2
sw	$4,56($sp)
.set	noreorder
.set	nomacro
bne	$10,$0,$L51
mflo	$5
.set	macro
.set	reorder

sltu	$5,$5,$6
.set	noreorder
.set	nomacro
bne	$5,$0,$L51
lw	$25,%call16(av_malloc)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
sw	$2,72($sp)
.set	macro
.set	reorder

li	$5,48			# 0x30
lw	$28,32($sp)
move	$4,$23
lw	$7,76($sp)
move	$21,$2
sw	$fp,16($sp)
lw	$6,%got($LC2)($28)
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC2)
.set	macro
.set	reorder

li	$5,48			# 0x30
lw	$28,32($sp)
move	$4,$23
lw	$2,68($sp)
lw	$9,72($sp)
lw	$7,64($sp)
lw	$6,%got($LC3)($28)
lw	$25,%call16(av_log)($28)
sw	$2,16($sp)
addiu	$6,$6,%lo($LC3)
sw	$9,24($sp)
sw	$20,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$22,28($sp)
.set	macro
.set	reorder

addiu	$6,$22,8
lw	$28,32($sp)
addiu	$5,$sp,56
addu	$6,$19,$6
subu	$7,$18,$22
lw	$25,%call16(uncompress)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,uncompress
1:	jalr	$25
move	$4,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L53
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$6,%got($LC4)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$23
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC4)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L27
li	$16,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L53:
lw	$25,%call16(ff_svq1_packet_checksum)($28)
move	$6,$0
lw	$5,56($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_svq1_packet_checksum
1:	jalr	$25
move	$4,$21
.set	macro
.set	reorder

li	$5,48			# 0x30
lw	$28,32($sp)
sll	$6,$2,16
move	$4,$23
or	$2,$6,$2
move	$7,$2
lw	$6,%got($LC5)($28)
lw	$25,%call16(av_log)($28)
sw	$2,-15008($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC5)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L62
lw	$31,116($sp)
.set	macro
.set	reorder

$L51:
li	$16,-1			# 0xffffffffffffffff
$L27:
lw	$31,116($sp)
$L62:
move	$2,$16
lw	$fp,112($sp)
lw	$23,108($sp)
lw	$22,104($sp)
lw	$21,100($sp)
lw	$20,96($sp)
lw	$19,92($sp)
lw	$18,88($sp)
lw	$17,84($sp)
lw	$16,80($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	svq3_decode_init
.size	svq3_decode_init, .-svq3_decode_init
.section	.rodata.str1.4
.align	2
$LC6:
.ascii	"unsupported slice header (%02X)\012\000"
.align	2
$LC7:
.ascii	"slice after bitstream end\012\000"
.align	2
$LC8:
.ascii	"illegal slice type %d \012\000"
.section	.text.svq3_decode_slice_header,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	svq3_decode_slice_header
.type	svq3_decode_slice_header, @function
svq3_decode_slice_header:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,10340($4)
addiu	$sp,$sp,-40
lw	$8,10332($4)
li	$7,16711680			# 0xff0000
li	$5,-16777216			# 0xffffffffff000000
sw	$16,24($sp)
srl	$3,$2,3
sw	$18,32($sp)
move	$16,$4
sw	$17,28($sp)
addiu	$11,$7,255
.cprestore	16
addu	$3,$8,$3
sw	$31,36($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$9,$4,8
ori	$5,$5,0xff00
and	$4,$3,$11
and	$3,$9,$5
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$7,$2,0x7
or	$3,$4,$3
sll	$7,$3,$7
li	$3,131072			# 0x20000
srl	$7,$7,24
addu	$3,$16,$3
andi	$18,$7,0x9f
addiu	$12,$2,8
lw	$17,9448($3)
addiu	$3,$18,-1
sltu	$3,$3,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L66
sw	$12,10340($16)
.set	macro
.set	reorder

andi	$3,$7,0x60
.set	noreorder
.set	nomacro
beq	$3,$0,$L66
srl	$3,$12,3
.set	macro
.set	reorder

lw	$10,10344($16)
sra	$6,$7,5
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($3)  
lwr $9, 0($3)  

# 0 "" 2
#NO_APP
srl	$7,$9,8
sll	$9,$9,8
and	$7,$7,$11
and	$5,$9,$5
or	$5,$7,$5
sll	$7,$5,16
andi	$6,$6,0x3
srl	$5,$5,16
sll	$4,$6,3
andi	$3,$12,0x7
or	$5,$7,$5
sll	$5,$5,$3
subu	$3,$0,$4
addu	$4,$4,$12
srl	$3,$5,$3
li	$5,196608			# 0x30000
sll	$3,$3,3
addu	$5,$16,$5
addu	$3,$4,$3
slt	$10,$10,$3
.set	noreorder
.set	nomacro
bne	$10,$0,$L113
sw	$3,-15012($5)
.set	macro
.set	reorder

li	$4,1			# 0x1
addiu	$2,$2,16
subu	$4,$4,$6
sll	$4,$4,3
sw	$2,10340($16)
lw	$5,-15008($5)
addu	$3,$3,$4
.set	noreorder
.set	nomacro
beq	$5,$0,$L70
sw	$3,10344($16)
.set	macro
.set	reorder

sra	$2,$2,3
addiu	$2,$2,1
addu	$2,$8,$2
lw	$8,10332($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
xor	$3,$3,$5
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 3($2)  
swr $3, 0($2)  

# 0 "" 2
#NO_APP
lw	$2,10340($16)
$L70:
.set	noreorder
.set	nomacro
beq	$6,$0,$L71
sra	$4,$2,3
.set	macro
.set	reorder

lw	$5,10344($16)
lw	$25,%call16(memcpy)($28)
addiu	$6,$6,-1
addu	$4,$8,$4
sra	$5,$5,3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$8,$5
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$2,10340($16)
lw	$8,10332($16)
$L71:
srl	$3,$2,3
li	$10,16711680			# 0xff0000
addu	$3,$8,$3
li	$9,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
addiu	$10,$10,255
srl	$4,$4,8
sll	$3,$3,8
ori	$9,$9,0xff00
and	$4,$4,$10
and	$3,$3,$9
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$6,$2,0x7
or	$3,$4,$3
sll	$6,$3,$6
li	$3,-1434451968			# 0xffffffffaa800000
and	$3,$6,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L72
move	$5,$2
.set	macro
.set	reorder

lw	$12,%got(ff_interleaved_golomb_vlc_len)($28)
srl	$6,$6,24
addu	$2,$12,$6
lbu	$2,0($2)
sltu	$3,$2,9
.set	noreorder
.set	nomacro
bne	$3,$0,$L95
lw	$11,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$3,9			# 0x9
.set	noreorder
.set	nomacro
bne	$2,$3,$L119
addiu	$5,$5,8
.set	macro
.set	reorder

lw	$11,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$7,1			# 0x1
li	$13,9			# 0x9
$L78:
srl	$3,$5,3
addu	$6,$11,$6
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$2,$4,8
and	$3,$3,$10
and	$2,$2,$9
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$14,$5,0x7
or	$3,$3,$2
sll	$2,$3,$14
lbu	$3,0($6)
sll	$7,$7,4
srl	$6,$2,24
or	$7,$3,$7
addu	$2,$12,$6
lbu	$2,0($2)
sltu	$3,$2,9
beq	$3,$0,$L115
$L73:
addu	$5,$5,$2
$L76:
addiu	$2,$2,-1
$L118:
addu	$6,$11,$6
sra	$2,$2,1
lbu	$3,0($6)
sll	$7,$7,$2
li	$2,-2147483648			# 0xffffffff80000000
or	$7,$7,$3
addiu	$7,$7,-1
.set	noreorder
.set	nomacro
beq	$7,$2,$L79
sw	$5,10340($16)
.set	macro
.set	reorder

$L75:
slt	$2,$7,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L79
lw	$3,%got(golomb_to_pict_type)($28)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addu	$2,$16,$2
addiu	$3,$3,%lo(golomb_to_pict_type)
addu	$7,$7,$3
lbu	$3,0($7)
sw	$3,-5264($2)
li	$2,2			# 0x2
beq	$18,$2,$L116
lw	$2,10340($16)
lw	$7,7992($16)
sw	$0,8000($16)
addiu	$2,$2,1
sw	$2,10340($16)
$L84:
li	$4,16711680			# 0xff0000
srl	$5,$2,3
li	$3,-16777216			# 0xffffffffff000000
addiu	$10,$4,255
addu	$5,$8,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($5)  
lwr $4, 0($5)  

# 0 "" 2
#NO_APP
srl	$6,$4,8
sll	$5,$4,8
ori	$3,$3,0xff00
and	$6,$6,$10
and	$5,$5,$3
or	$5,$6,$5
sll	$6,$5,16
srl	$5,$5,16
andi	$4,$2,0x7
or	$6,$6,$5
sll	$5,$6,$4
li	$9,65536			# 0x10000
addiu	$6,$2,8
srl	$5,$5,24
addu	$9,$16,$9
srl	$4,$6,3
sw	$6,10340($16)
sw	$5,-5272($9)
andi	$6,$6,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($4)  
lwr $9, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$9,8
sll	$9,$9,8
and	$4,$4,$10
and	$3,$9,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
addiu	$5,$2,13
or	$3,$4,$3
sll	$3,$3,$6
srl	$6,$5,3
sw	$5,10340($16)
srl	$3,$3,27
addu	$6,$8,$6
andi	$4,$5,0x7
sw	$3,2872($16)
addiu	$5,$2,15
lbu	$3,0($6)
li	$6,196608			# 0x30000
addu	$6,$16,$6
sw	$5,10340($16)
sll	$3,$3,$4
andi	$3,$3,0x00ff
lw	$4,-15016($6)
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L85
sw	$3,2892($16)
.set	macro
.set	reorder

addiu	$5,$2,16
sw	$5,10340($16)
$L85:
addiu	$3,$5,3
.set	noreorder
.set	nomacro
b	$L109
addiu	$5,$5,4
.set	macro
.set	reorder

$L100:
addiu	$3,$3,9
addiu	$5,$5,9
$L109:
srl	$4,$3,3
sw	$3,10340($16)
andi	$6,$3,0x7
addu	$4,$8,$4
lbu	$2,0($4)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L100
sw	$5,10340($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$7,$L88
sll	$2,$17,2
.set	macro
.set	reorder

lw	$3,11856($16)
lw	$4,10856($16)
li	$5,-1			# 0xffffffffffffffff
lw	$25,%call16(memset)($28)
li	$6,4			# 0x4
addu	$2,$3,$2
lw	$2,-4($2)
addiu	$2,$2,3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

li	$5,-1			# 0xffffffffffffffff
lw	$6,7992($16)
lw	$3,11856($16)
lw	$28,16($sp)
subu	$2,$17,$6
lw	$4,10856($16)
sll	$6,$6,3
sll	$2,$2,2
lw	$25,%call16(memset)($28)
addu	$2,$3,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$28,16($sp)
$L88:
lw	$2,7996($16)
.set	noreorder
.set	nomacro
blez	$2,$L92
li	$5,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,168($16)
lw	$3,11856($16)
lw	$6,160($16)
subu	$2,$17,$2
lw	$7,7992($16)
lw	$4,10856($16)
sll	$2,$2,2
lw	$25,%call16(memset)($28)
subu	$6,$6,$7
addu	$2,$3,$2
sll	$6,$6,3
lw	$2,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$2,7992($16)
.set	noreorder
.set	nomacro
blez	$2,$L92
move	$2,$0
.set	macro
.set	reorder

lw	$5,168($16)
lw	$4,11856($16)
lw	$3,10856($16)
subu	$17,$17,$5
sll	$17,$17,2
addu	$17,$4,$17
lw	$4,-4($17)
addu	$3,$3,$4
li	$4,-1			# 0xffffffffffffffff
sb	$4,3($3)
$L68:
lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L115:
.set	noreorder
.set	nomacro
beq	$2,$13,$L78
addiu	$5,$5,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L118
addiu	$2,$2,-1
.set	macro
.set	reorder

$L95:
.set	noreorder
.set	nomacro
b	$L73
li	$7,1			# 0x1
.set	macro
.set	reorder

$L92:
lw	$31,36($sp)
move	$2,$0
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L72:
lw	$4,%got(ff_interleaved_golomb_vlc_len)($28)
srl	$6,$6,24
lw	$3,%got(ff_interleaved_ue_golomb_vlc_code)($28)
addu	$4,$4,$6
addu	$6,$3,$6
lbu	$3,0($4)
lbu	$7,0($6)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L75
sw	$2,10340($16)
.set	macro
.set	reorder

$L116:
lw	$2,188($16)
slt	$3,$2,64
beq	$3,$0,$L117
li	$12,6			# 0x6
li	$10,26			# 0x1a
$L83:
lw	$2,10340($16)
lw	$11,7996($16)
lw	$13,160($16)
srl	$4,$2,3
lw	$7,7992($16)
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
li	$4,16711680			# 0xff0000
srl	$6,$3,8
addiu	$4,$4,255
sll	$9,$3,8
and	$5,$6,$4
li	$4,-16777216			# 0xffffffffff000000
andi	$3,$2,0x7
ori	$4,$4,0xff00
and	$4,$9,$4
or	$4,$5,$4
sll	$5,$4,16
srl	$4,$4,16
addu	$2,$12,$2
or	$4,$5,$4
sll	$4,$4,$3
mul	$3,$11,$13
sw	$2,10340($16)
addu	$5,$3,$7
srl	$3,$4,$10
subu	$3,$3,$5
.set	noreorder
.set	nomacro
b	$L84
sw	$3,8000($16)
.set	macro
.set	reorder

$L117:
addiu	$2,$2,-1
ori	$3,$2,0x1
clz	$10,$3
li	$2,32			# 0x20
.set	noreorder
.set	nomacro
b	$L83
subu	$12,$2,$10
.set	macro
.set	reorder

$L66:
lw	$6,%got($LC6)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L68
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L79:
lw	$6,%got($LC8)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC8)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L68
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L113:
lw	$6,%got($LC7)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L68
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L119:
.set	noreorder
.set	nomacro
b	$L76
li	$7,1			# 0x1
.set	macro
.set	reorder

.end	svq3_decode_slice_header
.size	svq3_decode_slice_header, .-svq3_decode_slice_header
.section	.rodata.str1.4
.align	2
$LC9:
.ascii	"invalid MV vlc\012\000"
.section	.text.svq3_mc_dir,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	svq3_mc_dir
.type	svq3_mc_dir, @function
svq3_mc_dir:
.frame	$sp,216,$31		# vars= 128, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-216
li	$2,4			# 0x4
sw	$fp,208($sp)
andi	$fp,$5,0x5
.cprestore	40
sw	$23,204($sp)
move	$23,$4
sw	$31,212($sp)
sw	$22,200($sp)
sw	$21,196($sp)
sw	$20,192($sp)
sw	$19,188($sp)
sw	$18,184($sp)
sw	$17,180($sp)
sw	$16,176($sp)
sw	$6,224($sp)
sw	$7,228($sp)
.set	noreorder
.set	nomacro
beq	$fp,$2,$L121
lw	$9,232($sp)
.set	macro
.set	reorder

li	$3,16			# 0x10
andi	$2,$5,0x1
sra	$fp,$3,$2
$L121:
li	$2,-1431699456			# 0xffffffffaaaa0000
lw	$11,228($sp)
addiu	$5,$5,1
lw	$3,184($23)
ori	$2,$2,0xaaab
lw	$13,224($sp)
multu	$5,$2
lw	$12,228($sp)
li	$5,16			# 0x10
lw	$4,180($23)
addiu	$6,$23,3504
lw	$17,228($sp)
sll	$11,$11,5
sw	$0,88($sp)
mfhi	$2
sll	$7,$12,7
sw	$11,80($sp)
sra	$10,$fp,3
lw	$15,80($sp)
li	$11,2			# 0x2
subu	$4,$4,$fp
subu	$11,$11,$10
sra	$16,$fp,2
srl	$2,$2,1
sll	$11,$11,4
sra	$5,$5,$2
sw	$16,136($sp)
sw	$5,48($sp)
addiu	$5,$23,3568
lw	$8,48($sp)
lw	$16,48($sp)
subu	$2,$3,$8
move	$3,$5
move	$5,$6
movn	$5,$3,$9
li	$3,-96			# 0xffffffffffffffa0
xori	$6,$13,0x4
movn	$3,$0,$6
li	$6,3			# 0x3
sll	$12,$2,1
subu	$10,$6,$10
addu	$6,$15,$7
addiu	$7,$23,288
sw	$3,68($sp)
sll	$3,$4,1
sw	$6,156($sp)
addiu	$6,$23,888
sll	$4,$4,3
lw	$13,68($sp)
movz	$6,$7,$17
sll	$2,$2,3
subu	$3,$4,$3
subu	$2,$2,$12
lw	$8,156($sp)
subu	$3,$3,$13
subu	$2,$2,$13
sw	$6,64($sp)
addiu	$6,$23,3768
addiu	$7,$23,3812
sw	$3,144($sp)
move	$3,$6
sw	$2,148($sp)
sll	$12,$17,1
movn	$3,$7,$9
li	$2,12288			# 0x3000
lw	$17,48($sp)
sll	$4,$10,4
sll	$2,$2,$12
addiu	$10,$8,11288
sw	$3,152($sp)
addu	$11,$5,$11
sw	$2,164($sp)
sra	$15,$fp,1
lw	$2,136($sp)
sra	$16,$16,1
lw	$3,228($sp)
addu	$4,$5,$4
sra	$17,$17,2
sw	$11,124($sp)
sll	$2,$2,2
sw	$15,132($sp)
sll	$3,$3,3
sw	$16,76($sp)
addu	$10,$23,$10
sw	$4,140($sp)
sw	$17,116($sp)
sw	$2,60($sp)
sw	$3,84($sp)
sw	$10,160($sp)
sw	$fp,52($sp)
$L123:
lw	$15,88($sp)
move	$10,$0
move	$8,$23
sra	$2,$15,1
sra	$16,$15,2
andi	$2,$2,0x2
andi	$17,$15,0x8
sw	$16,104($sp)
sw	$2,108($sp)
sw	$17,112($sp)
$L208:
lw	$18,7992($8)
sra	$4,$10,2
lw	$19,7996($8)
sra	$2,$10,1
lw	$11,104($sp)
andi	$3,$4,0x1
sll	$5,$18,2
lw	$6,11864($8)
sll	$7,$19,2
lw	$9,108($sp)
addu	$4,$5,$4
lw	$13,112($sp)
addu	$5,$7,$11
lw	$12,88($sp)
addu	$3,$3,$9
lw	$15,224($sp)
mul	$11,$5,$6
andi	$2,$2,0x4
addu	$2,$3,$2
sll	$18,$18,4
addu	$2,$2,$13
sll	$19,$19,4
li	$3,4			# 0x4
addu	$9,$11,$4
sw	$2,72($sp)
addu	$18,$18,$10
addu	$19,$19,$12
.set	noreorder
.set	nomacro
beq	$15,$3,$L124
sw	$9,56($sp)
.set	macro
.set	reorder

lw	$16,%got(scan8)($28)
li	$12,65536			# 0x10000
lw	$9,80($sp)
lw	$17,84($sp)
addu	$12,$8,$12
addiu	$16,$16,%lo(scan8)
lw	$11,136($sp)
addu	$4,$16,$2
lw	$14,-5252($12)
addu	$2,$17,$9
lbu	$6,0($4)
addu	$4,$8,$2
addiu	$7,$6,-8
addiu	$9,$6,-1
addu	$5,$2,$7
addu	$2,$2,$9
addu	$13,$11,$7
addiu	$2,$2,2812
addiu	$5,$5,2812
addu	$7,$4,$7
addu	$9,$4,$9
addu	$11,$4,$13
sll	$2,$2,2
lb	$7,11568($7)
sll	$5,$5,2
lb	$9,11568($9)
addu	$2,$8,$2
lb	$11,11568($11)
.set	noreorder
.set	nomacro
beq	$14,$0,$L125
addu	$5,$8,$5
.set	macro
.set	reorder

li	$14,-2			# 0xfffffffffffffffe
beq	$11,$14,$L272
lw	$12,84($sp)
$L307:
xori	$4,$11,0x1
lw	$15,80($sp)
sltu	$4,$4,1
addu	$3,$12,$15
addu	$3,$3,$13
addiu	$3,$3,2812
sll	$3,$3,2
addu	$3,$8,$3
$L130:
xori	$12,$7,0x1
sltu	$12,$12,1
xori	$6,$9,0x1
addu	$4,$12,$4
sltu	$6,$6,1
addu	$4,$4,$6
slt	$13,$4,2
.set	noreorder
.set	nomacro
bne	$13,$0,$L300
li	$13,1			# 0x1
.set	macro
.set	reorder

$L139:
lh	$6,0($2)
lh	$4,0($5)
slt	$9,$4,$6
.set	noreorder
.set	nomacro
beq	$9,$0,$L140
lh	$7,0($3)
.set	macro
.set	reorder

slt	$9,$4,$7
beq	$9,$0,$L141
slt	$4,$6,$7
movz	$6,$7,$4
move	$4,$6
$L141:
lh	$6,2($2)
lh	$2,2($5)
slt	$5,$2,$6
.set	noreorder
.set	nomacro
beq	$5,$0,$L142
lh	$3,2($3)
.set	macro
.set	reorder

slt	$5,$2,$3
.set	noreorder
.set	nomacro
beq	$5,$0,$L301
sll	$5,$18,3
.set	macro
.set	reorder

slt	$2,$6,$3
movz	$6,$3,$2
move	$2,$6
$L135:
sll	$5,$18,3
$L301:
lw	$9,68($sp)
sll	$3,$18,1
subu	$3,$3,$5
addu	$5,$9,$3
slt	$6,$4,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L302
sll	$6,$19,3
.set	macro
.set	reorder

lw	$11,144($sp)
addu	$3,$11,$3
slt	$5,$3,$4
movz	$3,$4,$5
move	$5,$3
$L302:
lw	$12,68($sp)
sll	$3,$19,1
subu	$3,$3,$6
addu	$6,$12,$3
slt	$4,$2,$6
.set	noreorder
.set	nomacro
bne	$4,$0,$L303
lw	$15,224($sp)
.set	macro
.set	reorder

lw	$13,148($sp)
addu	$3,$13,$3
slt	$6,$3,$2
movz	$3,$2,$6
move	$6,$3
$L303:
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$15,$2,$L215
li	$13,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$7,10340($8)
lw	$11,10332($8)
li	$9,-16777216			# 0xffffffffff000000
addiu	$13,$13,255
srl	$2,$7,3
ori	$9,$9,0xff00
addu	$4,$11,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($4)  
lwr $2, 0($4)  

# 0 "" 2
#NO_APP
srl	$3,$2,8
sll	$2,$2,8
and	$3,$3,$13
and	$2,$2,$9
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$12,$7,0x7
or	$2,$3,$2
sll	$2,$2,$12
li	$3,-1434451968			# 0xffffffffaa800000
and	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L274
addiu	$14,$4,3
.set	macro
.set	reorder

addiu	$3,$7,8
srl	$15,$3,3
andi	$3,$3,0x7
addu	$15,$11,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $16, 3($15)  
lwr $16, 0($15)  

# 0 "" 2
#NO_APP
move	$15,$16
sll	$15,$15,8
srl	$16,$16,8
and	$9,$15,$9
and	$13,$16,$13
or	$9,$13,$9
sll	$15,$9,16
srl	$9,$9,16
or	$9,$15,$9
sll	$3,$9,$3
srl	$3,$3,8
or	$3,$3,$2
li	$2,-1431699456			# 0xffffffffaaaa0000
ori	$2,$2,0xaaaa
and	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L216
ori	$3,$3,0x1
.set	macro
.set	reorder

li	$2,31			# 0x1f
sll	$4,$3,$2
$L304:
addiu	$2,$2,-1
sll	$9,$3,2
srl	$3,$3,30
srl	$4,$4,$2
or	$3,$3,$9
subu	$3,$3,$4
.set	noreorder
.set	nomacro
bgez	$3,$L304
sll	$4,$3,$2
.set	macro
.set	reorder

sll	$17,$3,$2
sll	$4,$2,1
addiu	$7,$7,63
srl	$17,$17,$2
subu	$7,$7,$4
andi	$3,$3,0x1
addiu	$17,$17,-1
subu	$3,$0,$3
sw	$7,10340($8)
srl	$2,$7,3
xor	$3,$17,$3
addiu	$17,$3,1
addu	$2,$11,$2
sra	$9,$17,1
addiu	$14,$2,3
andi	$12,$7,0x7
$L148:
li	$13,16711680			# 0xff0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($14)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$14,-16777216			# 0xffffffffff000000
srl	$4,$3,8
addiu	$13,$13,255
sll	$3,$3,8
ori	$14,$14,0xff00
and	$4,$4,$13
and	$3,$3,$14
or	$3,$4,$3
sll	$2,$3,16
srl	$3,$3,16
or	$3,$2,$3
sll	$12,$3,$12
li	$2,-1434451968			# 0xffffffffaa800000
and	$2,$12,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L275
addiu	$2,$7,8
.set	macro
.set	reorder

srl	$3,$2,3
andi	$2,$2,0x7
addu	$11,$11,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($11)  
lwr $4, 0($11)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$13,$3,$13
and	$3,$4,$14
or	$3,$13,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
li	$3,-1431699456			# 0xffffffffaaaa0000
srl	$2,$2,8
ori	$3,$3,0xaaaa
or	$2,$2,$12
and	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L155
ori	$2,$2,0x1
.set	macro
.set	reorder

li	$11,31			# 0x1f
sll	$3,$2,$11
$L305:
addiu	$11,$11,-1
sll	$4,$2,2
srl	$2,$2,30
srl	$3,$3,$11
or	$2,$2,$4
subu	$2,$2,$3
.set	noreorder
.set	nomacro
bgez	$2,$L305
sll	$3,$2,$11
.set	macro
.set	reorder

sll	$16,$2,$11
andi	$2,$2,0x1
srl	$16,$16,$11
subu	$2,$0,$2
addiu	$16,$16,-1
addiu	$7,$7,63
sll	$3,$11,1
xor	$16,$16,$2
subu	$3,$7,$3
addiu	$16,$16,1
li	$2,-2147483648			# 0xffffffff80000000
sra	$16,$16,1
.set	noreorder
.set	nomacro
beq	$9,$2,$L155
sw	$3,10340($8)
.set	macro
.set	reorder

$L290:
lw	$17,224($sp)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$17,$2,$L276
lw	$3,224($sp)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L277
addiu	$3,$5,24579
.set	macro
.set	reorder

li	$17,-1431699456			# 0xffffffffaaaa0000
ori	$17,$17,0xaaab
multu	$3,$17
addiu	$6,$6,24579
mfhi	$3
multu	$6,$17
mfhi	$17
srl	$3,$3,2
addiu	$3,$3,-4096
addu	$16,$3,$16
srl	$17,$17,2
addu	$22,$18,$16
addiu	$17,$17,-4096
addu	$17,$17,$9
.set	noreorder
.set	nomacro
bltz	$22,$L278
addu	$21,$19,$17
.set	macro
.set	reorder

lw	$2,180($8)
lw	$12,52($sp)
subu	$2,$2,$12
addiu	$3,$2,-1
slt	$3,$22,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L187
lw	$13,48($sp)
.set	macro
.set	reorder

lw	$3,184($8)
subu	$3,$3,$13
$L188:
lw	$20,64($8)
srl	$20,$20,14
andi	$20,$20,0x1
$L186:
addiu	$2,$2,15
slt	$4,$2,$22
movn	$22,$2,$4
move	$5,$22
$L185:
slt	$2,$21,-16
.set	noreorder
.set	nomacro
bne	$2,$0,$L222
lw	$11,64($sp)
.set	macro
.set	reorder

addiu	$2,$3,15
slt	$3,$2,$21
movn	$21,$2,$3
$L190:
lw	$6,192($8)
lw	$11,64($sp)
lw	$4,2088($8)
mul	$7,$19,$6
lw	$2,0($11)
addu	$3,$7,$18
mul	$7,$6,$21
addu	$23,$4,$3
addu	$5,$7,$5
.set	noreorder
.set	nomacro
bne	$20,$0,$L279
addu	$5,$2,$5
.set	macro
.set	reorder

$L191:
lw	$13,124($sp)
move	$4,$23
lw	$7,48($sp)
lw	$25,0($13)
sw	$8,172($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$10,168($sp)
.set	macro
.set	reorder

lw	$8,172($sp)
lw	$28,40($sp)
lw	$2,64($8)
andi	$2,$2,0x2000
.set	noreorder
.set	nomacro
bne	$2,$0,$L195
lw	$10,168($sp)
.set	macro
.set	reorder

slt	$2,$21,$19
lw	$15,132($sp)
addu	$21,$21,$2
lw	$2,76($sp)
slt	$3,$22,$18
sw	$16,92($sp)
addu	$22,$22,$3
sw	$17,96($sp)
addiu	$15,$15,1
sw	$10,100($sp)
addiu	$2,$2,1
lw	$16,140($sp)
move	$23,$0
sra	$22,$22,1
sw	$15,120($sp)
sra	$21,$21,1
sw	$2,128($sp)
sra	$18,$18,1
sra	$19,$19,1
li	$fp,8			# 0x8
move	$17,$23
move	$23,$8
$L194:
lw	$6,196($23)
addu	$4,$23,$17
lw	$8,64($sp)
mul	$2,$6,$19
lw	$9,2092($4)
addu	$3,$8,$17
addiu	$17,$17,4
lw	$3,4($3)
addu	$7,$2,$18
mul	$2,$21,$6
addu	$9,$9,$7
addu	$5,$2,$22
.set	noreorder
.set	nomacro
bne	$20,$0,$L280
addu	$5,$3,$5
.set	macro
.set	reorder

$L193:
lw	$25,0($16)
move	$4,$9
.set	noreorder
.set	nomacro
jalr	$25
lw	$7,76($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$17,$fp,$L194
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$16,92($sp)
move	$8,$23
lw	$17,96($sp)
lw	$10,100($sp)
$L195:
sll	$fp,$16,1
sll	$22,$17,1
sll	$16,$16,3
sll	$17,$17,3
subu	$fp,$16,$fp
subu	$22,$17,$22
$L166:
lw	$9,48($sp)
sll	$22,$22,16
li	$2,8			# 0x8
andi	$fp,$fp,0xffff
.set	noreorder
.set	nomacro
beq	$9,$2,$L281
addu	$22,$fp,$22
.set	macro
.set	reorder

lw	$3,52($sp)
$L308:
li	$2,8			# 0x8
.set	noreorder
.set	nomacro
beq	$3,$2,$L282
lw	$13,52($sp)
.set	macro
.set	reorder

li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$13,$2,$L306
lw	$2,%got(scan8)($28)
.set	macro
.set	reorder

$L201:
lw	$3,48($sp)
$L310:
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$3,$2,$L261
lw	$2,%got(scan8)($28)
.set	macro
.set	reorder

$L183:
lw	$9,228($sp)
$L309:
lw	$11,56($sp)
lw	$3,11864($8)
addiu	$2,$9,546
lw	$12,60($sp)
sll	$4,$11,2
sll	$2,$2,2
addu	$2,$8,$2
lw	$2,0($2)
addu	$2,$2,$4
li	$4,4			# 0x4
.set	noreorder
.set	nomacro
beq	$12,$4,$L283
sll	$5,$3,2
.set	macro
.set	reorder

$L203:
lw	$15,60($sp)
li	$4,8			# 0x8
.set	noreorder
.set	nomacro
beq	$15,$4,$L284
lw	$17,60($sp)
.set	macro
.set	reorder

li	$4,16			# 0x10
beq	$17,$4,$L285
$L205:
lw	$11,52($sp)
addu	$10,$10,$11
slt	$2,$10,16
.set	noreorder
.set	nomacro
bne	$2,$0,$L208
lw	$12,88($sp)
.set	macro
.set	reorder

move	$23,$8
lw	$13,48($sp)
addu	$12,$12,$13
slt	$2,$12,16
.set	noreorder
.set	nomacro
bne	$2,$0,$L123
sw	$12,88($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L254
move	$2,$0
.set	macro
.set	reorder

$L215:
move	$17,$0
move	$16,$0
$L146:
li	$2,-1431699456			# 0xffffffffaaaa0000
addiu	$3,$5,12289
ori	$2,$2,0xaaab
multu	$3,$2
addiu	$6,$6,12289
mfhi	$3
multu	$6,$2
mfhi	$2
srl	$3,$3,1
addiu	$3,$3,-4096
addu	$16,$3,$16
srl	$2,$2,1
sra	$23,$16,1
addiu	$2,$2,-4096
addu	$23,$18,$23
addu	$17,$2,$17
andi	$20,$16,0x1
andi	$2,$17,0x1
sll	$2,$2,1
sra	$22,$17,1
addu	$20,$20,$2
.set	noreorder
.set	nomacro
bltz	$23,$L286
addu	$22,$19,$22
.set	macro
.set	reorder

lw	$2,180($8)
lw	$12,52($sp)
subu	$2,$2,$12
addiu	$3,$2,-1
slt	$3,$23,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L173
lw	$4,184($8)
.set	macro
.set	reorder

lw	$13,48($sp)
subu	$4,$4,$13
$L174:
lw	$21,64($8)
srl	$21,$21,14
andi	$21,$21,0x1
$L172:
addiu	$2,$2,15
slt	$3,$2,$23
movn	$23,$2,$3
move	$3,$23
$L171:
slt	$2,$22,-16
.set	noreorder
.set	nomacro
bne	$2,$0,$L220
lw	$9,64($sp)
.set	macro
.set	reorder

addiu	$2,$4,15
slt	$4,$2,$22
movn	$22,$2,$4
$L176:
lw	$6,192($8)
lw	$9,64($sp)
lw	$fp,2088($8)
mul	$4,$19,$6
lw	$5,0($9)
addu	$2,$4,$18
mul	$4,$6,$22
addu	$fp,$fp,$2
addu	$3,$4,$3
.set	noreorder
.set	nomacro
bne	$21,$0,$L287
addu	$5,$5,$3
.set	macro
.set	reorder

$L177:
lw	$13,124($sp)
sll	$20,$20,2
lw	$7,48($sp)
move	$4,$fp
addu	$2,$13,$20
lw	$25,0($2)
sw	$8,172($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$10,168($sp)
.set	macro
.set	reorder

lw	$8,172($sp)
lw	$28,40($sp)
lw	$2,64($8)
andi	$2,$2,0x2000
.set	noreorder
.set	nomacro
bne	$2,$0,$L182
lw	$10,168($sp)
.set	macro
.set	reorder

slt	$2,$22,$19
lw	$3,76($sp)
addu	$22,$22,$2
lw	$2,132($sp)
slt	$fp,$23,$18
lw	$9,140($sp)
sra	$18,$18,1
sw	$16,96($sp)
addu	$fp,$23,$fp
sw	$17,100($sp)
addiu	$2,$2,1
addiu	$3,$3,1
sw	$18,92($sp)
move	$18,$0
sra	$fp,$fp,1
sw	$2,120($sp)
sra	$22,$22,1
sw	$3,128($sp)
sra	$19,$19,1
addu	$20,$9,$20
li	$23,8			# 0x8
move	$16,$18
move	$17,$8
move	$18,$10
$L181:
lw	$6,196($17)
addu	$4,$17,$16
lw	$2,92($sp)
lw	$11,64($sp)
mul	$5,$6,$19
lw	$9,2092($4)
addu	$3,$11,$16
addiu	$16,$16,4
lw	$3,4($3)
addu	$7,$5,$2
mul	$2,$22,$6
addu	$9,$9,$7
addu	$5,$2,$fp
.set	noreorder
.set	nomacro
bne	$21,$0,$L288
addu	$5,$3,$5
.set	macro
.set	reorder

$L180:
lw	$25,0($20)
move	$4,$9
.set	noreorder
.set	nomacro
jalr	$25
lw	$7,76($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$23,$L181
lw	$28,40($sp)
.set	macro
.set	reorder

move	$8,$17
lw	$16,96($sp)
lw	$17,100($sp)
move	$10,$18
$L182:
sll	$fp,$16,1
lw	$15,224($sp)
sll	$22,$17,1
addu	$16,$fp,$16
addu	$17,$22,$17
li	$2,4			# 0x4
move	$fp,$16
.set	noreorder
.set	nomacro
bne	$15,$2,$L166
move	$22,$17
.set	macro
.set	reorder

lw	$9,228($sp)
sll	$17,$17,16
lw	$11,56($sp)
andi	$22,$16,0xffff
lw	$3,11864($8)
addu	$22,$22,$17
addiu	$2,$9,546
lw	$12,60($sp)
sll	$4,$11,2
sll	$2,$2,2
addu	$2,$8,$2
lw	$2,0($2)
addu	$2,$2,$4
li	$4,4			# 0x4
.set	noreorder
.set	nomacro
bne	$12,$4,$L203
sll	$5,$3,2
.set	macro
.set	reorder

$L283:
lw	$13,116($sp)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$13,$3,$L205
sw	$22,0($2)
.set	macro
.set	reorder

addu	$2,$2,$5
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$13,$3,$L205
sw	$22,0($2)
.set	macro
.set	reorder

addu	$2,$2,$5
addu	$4,$2,$5
sw	$22,0($2)
.set	noreorder
.set	nomacro
b	$L205
sw	$22,0($4)
.set	macro
.set	reorder

$L124:
lw	$9,56($sp)
li	$5,131072			# 0x20000
lw	$2,984($8)
lw	$11,228($sp)
addu	$5,$8,$5
sll	$3,$9,2
addu	$2,$2,$3
lh	$4,0($2)
lh	$3,2($2)
sll	$4,$4,1
.set	noreorder
.set	nomacro
bne	$11,$0,$L143
sll	$3,$3,1
.set	macro
.set	reorder

lw	$6,10692($5)
lw	$2,10696($5)
mul	$4,$4,$6
mul	$3,$3,$6
$L258:
teq	$2,$0,7
div	$0,$4,$2
mflo	$4
teq	$2,$0,7
div	$0,$3,$2
addiu	$4,$4,1
sra	$4,$4,1
mflo	$2
addiu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L135
sra	$2,$2,1
.set	macro
.set	reorder

$L261:
$L306:
addiu	$2,$2,%lo(scan8)
lw	$15,72($sp)
$L299:
lw	$16,84($sp)
lw	$17,80($sp)
addu	$3,$2,$15
addu	$2,$16,$17
lbu	$3,0($3)
addu	$2,$2,$3
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$8,$2
.set	noreorder
.set	nomacro
b	$L183
sw	$22,0($2)
.set	macro
.set	reorder

$L125:
li	$3,-2			# 0xfffffffffffffffe
.set	noreorder
.set	nomacro
bne	$11,$3,$L307
lw	$12,84($sp)
.set	macro
.set	reorder

$L209:
lw	$16,84($sp)
$L311:
addiu	$6,$6,-9
lw	$17,80($sp)
xori	$12,$7,0x1
sltu	$12,$12,1
addu	$3,$16,$17
addu	$4,$8,$3
addu	$3,$3,$6
addu	$6,$4,$6
addiu	$3,$3,2812
lb	$11,11568($6)
xori	$6,$9,0x1
sltu	$6,$6,1
sll	$3,$3,2
xori	$4,$11,0x1
sltu	$4,$4,1
addu	$4,$12,$4
addu	$4,$4,$6
slt	$13,$4,2
.set	noreorder
.set	nomacro
beq	$13,$0,$L139
addu	$3,$8,$3
.set	macro
.set	reorder

li	$13,1			# 0x1
$L300:
.set	noreorder
.set	nomacro
beq	$4,$13,$L289
li	$4,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

bne	$7,$4,$L139
bne	$11,$7,$L139
beq	$9,$11,$L139
$L260:
lh	$4,0($2)
.set	noreorder
.set	nomacro
b	$L135
lh	$2,2($2)
.set	macro
.set	reorder

$L143:
lw	$2,10696($5)
lw	$5,10692($5)
subu	$5,$5,$2
mul	$4,$4,$5
.set	noreorder
.set	nomacro
b	$L258
mul	$3,$3,$5
.set	macro
.set	reorder

$L142:
slt	$5,$3,$2
.set	noreorder
.set	nomacro
beq	$5,$0,$L301
sll	$5,$18,3
.set	macro
.set	reorder

slt	$2,$3,$6
movz	$6,$3,$2
.set	noreorder
.set	nomacro
b	$L301
move	$2,$6
.set	macro
.set	reorder

$L140:
slt	$9,$7,$4
beq	$9,$0,$L141
slt	$4,$7,$6
movz	$6,$7,$4
.set	noreorder
.set	nomacro
b	$L141
move	$4,$6
.set	macro
.set	reorder

$L220:
lw	$6,192($8)
li	$22,-16			# 0xfffffffffffffff0
lw	$fp,2088($8)
mul	$4,$19,$6
lw	$5,0($9)
addu	$2,$4,$18
mul	$4,$6,$22
addu	$fp,$fp,$2
addu	$3,$4,$3
.set	noreorder
.set	nomacro
beq	$21,$0,$L177
addu	$5,$5,$3
.set	macro
.set	reorder

$L287:
lw	$9,48($sp)
lw	$11,180($8)
lw	$12,52($sp)
addiu	$2,$9,1
lw	$9,184($8)
lw	$4,2856($8)
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$7,$12,1
sw	$8,172($sp)
sw	$10,168($sp)
sw	$2,16($sp)
sw	$23,20($sp)
sw	$22,24($sp)
sw	$11,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$9,32($sp)
.set	macro
.set	reorder

lw	$8,172($sp)
lw	$10,168($sp)
lw	$5,2856($8)
.set	noreorder
.set	nomacro
b	$L177
lw	$6,192($8)
.set	macro
.set	reorder

$L274:
lw	$9,%got(ff_interleaved_golomb_vlc_len)($28)
srl	$2,$2,24
lw	$4,%got(ff_interleaved_se_golomb_vlc_code)($28)
addu	$9,$9,$2
addu	$2,$4,$2
lbu	$4,0($9)
lb	$9,0($2)
addu	$7,$4,$7
srl	$2,$7,3
sw	$7,10340($8)
andi	$12,$7,0x7
addu	$2,$11,$2
.set	noreorder
.set	nomacro
b	$L148
addiu	$14,$2,3
.set	macro
.set	reorder

$L275:
lw	$11,%got(ff_interleaved_golomb_vlc_len)($28)
srl	$12,$12,24
lw	$4,%got(ff_interleaved_se_golomb_vlc_code)($28)
addu	$11,$11,$12
addu	$12,$4,$12
lbu	$2,0($11)
lb	$16,0($12)
addu	$7,$2,$7
li	$2,-2147483648			# 0xffffffff80000000
.set	noreorder
.set	nomacro
bne	$9,$2,$L290
sw	$7,10340($8)
.set	macro
.set	reorder

$L155:
lw	$6,%got($LC9)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($8)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC9)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
$L254:
lw	$31,212($sp)
lw	$fp,208($sp)
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L281:
lw	$11,88($sp)
slt	$2,$11,8
.set	noreorder
.set	nomacro
beq	$2,$0,$L308
lw	$3,52($sp)
.set	macro
.set	reorder

lw	$12,%got(scan8)($28)
lw	$13,72($sp)
lw	$15,84($sp)
addiu	$12,$12,%lo(scan8)
lw	$16,80($sp)
lw	$17,52($sp)
addu	$3,$12,$13
addu	$5,$15,$16
move	$2,$12
lbu	$4,0($3)
addu	$3,$4,$5
addiu	$3,$3,2820
sll	$3,$3,2
addu	$3,$8,$3
.set	noreorder
.set	nomacro
beq	$17,$9,$L291
sw	$22,0($3)
.set	macro
.set	reorder

lw	$9,52($sp)
li	$3,4			# 0x4
.set	noreorder
.set	nomacro
bne	$9,$3,$L309
lw	$9,228($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L299
lw	$15,72($sp)
.set	macro
.set	reorder

$L284:
lw	$16,116($sp)
li	$4,1			# 0x1
sw	$22,0($2)
.set	noreorder
.set	nomacro
beq	$16,$4,$L205
sw	$22,4($2)
.set	macro
.set	reorder

addu	$4,$2,$5
li	$6,2			# 0x2
sw	$22,0($4)
.set	noreorder
.set	nomacro
beq	$16,$6,$L205
sw	$22,4($4)
.set	macro
.set	reorder

sll	$3,$3,3
addu	$4,$4,$5
addu	$6,$3,$5
sw	$22,0($4)
addu	$3,$2,$3
addu	$4,$4,$5
addu	$2,$2,$6
sw	$22,4($3)
sw	$22,0($4)
.set	noreorder
.set	nomacro
b	$L205
sw	$22,4($2)
.set	macro
.set	reorder

$L173:
.set	noreorder
.set	nomacro
bltz	$22,$L292
lw	$3,48($sp)
.set	macro
.set	reorder

subu	$4,$4,$3
addiu	$3,$4,-1
slt	$3,$22,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L174
move	$3,$23
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L176
move	$21,$0
.set	macro
.set	reorder

$L282:
slt	$2,$10,8
$L210:
.set	noreorder
.set	nomacro
beq	$2,$0,$L310
lw	$3,48($sp)
.set	macro
.set	reorder

lw	$4,%got(scan8)($28)
lw	$9,72($sp)
lw	$11,84($sp)
addiu	$4,$4,%lo(scan8)
lw	$12,80($sp)
addu	$3,$4,$9
addu	$2,$11,$12
lbu	$3,0($3)
addu	$2,$2,$3
addiu	$2,$2,2813
sll	$2,$2,2
addu	$2,$8,$2
.set	noreorder
.set	nomacro
b	$L201
sw	$22,0($2)
.set	macro
.set	reorder

$L288:
lw	$10,180($17)
lw	$3,184($17)
lw	$8,128($sp)
sra	$10,$10,1
lw	$4,2856($17)
sra	$3,$3,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,120($sp)
sw	$9,168($sp)
sw	$8,16($sp)
sw	$fp,20($sp)
sw	$22,24($sp)
sw	$10,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$5,2856($17)
lw	$6,196($17)
.set	noreorder
.set	nomacro
b	$L180
lw	$9,168($sp)
.set	macro
.set	reorder

$L222:
lw	$6,192($8)
li	$21,-16			# 0xfffffffffffffff0
lw	$4,2088($8)
mul	$7,$19,$6
lw	$2,0($11)
addu	$3,$7,$18
mul	$7,$6,$21
addu	$23,$4,$3
addu	$5,$7,$5
.set	noreorder
.set	nomacro
beq	$20,$0,$L191
addu	$5,$2,$5
.set	macro
.set	reorder

$L279:
lw	$11,48($sp)
lw	$9,180($8)
lw	$3,184($8)
lw	$12,52($sp)
addiu	$2,$11,1
lw	$4,2856($8)
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$7,$12,1
sw	$8,172($sp)
sw	$10,168($sp)
sw	$2,16($sp)
sw	$22,20($sp)
sw	$21,24($sp)
sw	$9,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$8,172($sp)
lw	$10,168($sp)
lw	$5,2856($8)
.set	noreorder
.set	nomacro
b	$L191
lw	$6,192($8)
.set	macro
.set	reorder

$L285:
lw	$9,116($sp)
addu	$4,$2,$5
li	$6,2			# 0x2
sw	$22,0($2)
sw	$22,4($2)
sw	$22,8($2)
sw	$22,12($2)
sw	$22,0($4)
sw	$22,4($4)
sw	$22,8($4)
.set	noreorder
.set	nomacro
beq	$9,$6,$L205
sw	$22,12($4)
.set	macro
.set	reorder

sll	$3,$3,3
addu	$4,$4,$5
addu	$6,$3,$5
sw	$22,0($4)
addu	$3,$2,$3
addu	$4,$4,$5
addu	$2,$2,$6
sw	$22,4($3)
sw	$22,8($3)
sw	$22,12($3)
sw	$22,0($4)
sw	$22,4($2)
sw	$22,8($2)
.set	noreorder
.set	nomacro
b	$L205
sw	$22,12($2)
.set	macro
.set	reorder

$L187:
.set	noreorder
.set	nomacro
bltz	$21,$L293
lw	$3,184($8)
.set	macro
.set	reorder

lw	$9,48($sp)
subu	$3,$3,$9
addiu	$4,$3,-1
slt	$4,$21,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L188
move	$5,$22
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L190
move	$20,$0
.set	macro
.set	reorder

$L276:
addiu	$22,$6,1
addiu	$fp,$5,1
sra	$22,$22,1
li	$3,-1431699456			# 0xffffffffaaaa0000
addu	$22,$22,$9
sra	$fp,$fp,1
addiu	$2,$22,12288
ori	$4,$3,0xaaab
multu	$2,$4
addu	$fp,$fp,$16
addiu	$3,$fp,12288
mfhi	$2
multu	$3,$4
mfhi	$3
srl	$2,$2,1
addiu	$2,$2,-4096
sll	$4,$2,1
srl	$3,$3,1
addu	$4,$4,$2
addiu	$3,$3,-4096
subu	$4,$22,$4
sll	$5,$3,1
sll	$4,$4,2
addu	$5,$5,$3
addu	$20,$18,$3
subu	$5,$fp,$5
addu	$17,$19,$2
.set	noreorder
.set	nomacro
bltz	$20,$L294
addu	$21,$5,$4
.set	macro
.set	reorder

lw	$6,180($8)
lw	$12,52($sp)
subu	$6,$6,$12
addiu	$2,$6,-1
slt	$2,$20,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L160
lw	$3,184($8)
.set	macro
.set	reorder

lw	$13,48($sp)
subu	$4,$3,$13
$L161:
lw	$16,64($8)
srl	$16,$16,14
andi	$16,$16,0x1
$L159:
addiu	$6,$6,15
slt	$2,$6,$20
movn	$20,$6,$2
move	$2,$20
$L158:
slt	$3,$17,-16
.set	noreorder
.set	nomacro
bne	$3,$0,$L218
lw	$3,64($sp)
.set	macro
.set	reorder

addiu	$3,$4,15
slt	$4,$3,$17
movn	$17,$3,$4
$L163:
lw	$6,192($8)
lw	$3,64($sp)
lw	$23,2088($8)
mul	$4,$19,$6
lw	$5,0($3)
addu	$3,$4,$18
mul	$4,$6,$17
addu	$23,$23,$3
addu	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$16,$0,$L295
addu	$5,$5,$2
.set	macro
.set	reorder

$L164:
lw	$15,152($sp)
sll	$9,$21,2
lw	$13,48($sp)
move	$4,$23
lw	$7,52($sp)
addu	$21,$15,$9
sw	$13,16($sp)
lw	$25,0($21)
sw	$8,172($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$10,168($sp)
.set	macro
.set	reorder

lw	$8,172($sp)
lw	$28,40($sp)
lw	$2,64($8)
andi	$2,$2,0x2000
.set	noreorder
.set	nomacro
bne	$2,$0,$L169
lw	$10,168($sp)
.set	macro
.set	reorder

slt	$3,$20,$18
sw	$22,92($sp)
slt	$2,$17,$19
sw	$10,96($sp)
addu	$20,$20,$3
lw	$3,76($sp)
addu	$17,$17,$2
lw	$2,132($sp)
move	$23,$0
addiu	$3,$3,1
addiu	$2,$2,1
sra	$20,$20,1
sra	$17,$17,1
sw	$3,120($sp)
sra	$18,$18,1
sw	$2,100($sp)
sra	$19,$19,1
move	$22,$23
move	$23,$8
$L168:
lw	$6,196($23)
addu	$4,$23,$22
lw	$8,64($sp)
mul	$2,$6,$19
lw	$9,2092($4)
addu	$3,$8,$22
addiu	$22,$22,4
lw	$3,4($3)
addu	$7,$2,$18
mul	$2,$17,$6
addu	$9,$9,$7
addu	$5,$2,$20
.set	noreorder
.set	nomacro
bne	$16,$0,$L296
addu	$5,$3,$5
.set	macro
.set	reorder

$L167:
lw	$11,76($sp)
lw	$7,132($sp)
sw	$11,16($sp)
lw	$25,0($21)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$9
.set	macro
.set	reorder

li	$12,8			# 0x8
.set	noreorder
.set	nomacro
bne	$22,$12,$L168
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$22,92($sp)
move	$8,$23
lw	$10,96($sp)
$L169:
sll	$fp,$fp,1
.set	noreorder
.set	nomacro
b	$L166
sll	$22,$22,1
.set	macro
.set	reorder

$L280:
lw	$10,180($23)
lw	$3,184($23)
lw	$8,128($sp)
sra	$10,$10,1
lw	$4,2856($23)
sra	$3,$3,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,120($sp)
sw	$9,168($sp)
sw	$8,16($sp)
sw	$22,20($sp)
sw	$21,24($sp)
sw	$10,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$5,2856($23)
lw	$6,196($23)
.set	noreorder
.set	nomacro
b	$L193
lw	$9,168($sp)
.set	macro
.set	reorder

$L286:
lw	$21,64($8)
slt	$3,$23,-16
lw	$2,180($8)
lw	$4,184($8)
lw	$9,52($sp)
srl	$21,$21,14
lw	$11,48($sp)
andi	$21,$21,0x1
subu	$2,$2,$9
.set	noreorder
.set	nomacro
beq	$3,$0,$L172
subu	$4,$4,$11
.set	macro
.set	reorder

li	$3,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L171
li	$23,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L278:
lw	$20,64($8)
slt	$4,$22,-16
lw	$2,180($8)
lw	$3,184($8)
lw	$9,52($sp)
srl	$20,$20,14
lw	$11,48($sp)
andi	$20,$20,0x1
subu	$2,$2,$9
.set	noreorder
.set	nomacro
beq	$4,$0,$L186
subu	$3,$3,$11
.set	macro
.set	reorder

li	$5,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L185
li	$22,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L272:
slt	$13,$6,20
.set	noreorder
.set	nomacro
bne	$13,$0,$L311
lw	$16,84($sp)
.set	macro
.set	reorder

andi	$13,$6,0x7
bne	$13,$3,$L311
lb	$3,11579($4)
beq	$3,$11,$L311
lw	$4,2696($8)
lw	$3,160($sp)
lw	$4,104($4)
sw	$0,0($3)
lw	$12,-5248($12)
bne	$12,$0,$L128
lw	$12,10800($8)
andi	$12,$12,0x80
.set	noreorder
.set	nomacro
beq	$12,$0,$L311
lw	$16,84($sp)
.set	macro
.set	reorder

lw	$14,7996($8)
sra	$6,$6,5
lw	$12,168($8)
lw	$13,10780($8)
andi	$14,$14,0x1
lw	$16,164($sp)
sll	$14,$14,1
addu	$13,$12,$13
addu	$6,$14,$6
sra	$14,$6,2
mul	$15,$12,$14
addu	$12,$15,$13
sll	$12,$12,2
addu	$4,$4,$12
lw	$4,0($4)
and	$4,$16,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L214
lw	$17,228($sp)
.set	macro
.set	reorder

sll	$13,$13,2
lw	$14,11852($8)
and	$11,$6,$11
lw	$15,11864($8)
sll	$4,$17,2
lw	$17,156($sp)
lw	$16,2696($8)
addu	$14,$14,$13
addu	$12,$8,$17
mul	$17,$6,$15
addu	$16,$16,$4
lw	$4,0($14)
addiu	$4,$4,3
lw	$14,96($16)
addu	$4,$17,$4
sll	$4,$4,2
addu	$4,$14,$4
lh	$6,0($4)
sh	$6,11288($12)
lhu	$4,2($4)
sll	$4,$4,1
sh	$4,11290($12)
lw	$4,188($16)
addu	$13,$4,$13
addu	$11,$13,$11
lb	$11,1($11)
sra	$11,$11,1
xori	$4,$11,0x1
.set	noreorder
.set	nomacro
b	$L130
sltu	$4,$4,1
.set	macro
.set	reorder

$L218:
lw	$6,192($8)
li	$17,-16			# 0xfffffffffffffff0
lw	$23,2088($8)
mul	$4,$19,$6
lw	$5,0($3)
addu	$3,$4,$18
mul	$4,$6,$17
addu	$23,$23,$3
addu	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$16,$0,$L164
addu	$5,$5,$2
.set	macro
.set	reorder

$L295:
lw	$9,48($sp)
lw	$11,180($8)
lw	$12,52($sp)
addiu	$3,$9,1
lw	$9,184($8)
lw	$4,2856($8)
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$7,$12,1
sw	$8,172($sp)
sw	$10,168($sp)
sw	$3,16($sp)
sw	$20,20($sp)
sw	$17,24($sp)
sw	$11,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$9,32($sp)
.set	macro
.set	reorder

lw	$8,172($sp)
lw	$10,168($sp)
lw	$5,2856($8)
.set	noreorder
.set	nomacro
b	$L164
lw	$6,192($8)
.set	macro
.set	reorder

$L160:
.set	noreorder
.set	nomacro
bltz	$17,$L297
lw	$16,48($sp)
.set	macro
.set	reorder

subu	$4,$3,$16
addiu	$2,$4,-1
slt	$2,$17,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L161
move	$2,$20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L163
move	$16,$0
.set	macro
.set	reorder

$L277:
.set	noreorder
.set	nomacro
b	$L146
move	$17,$9
.set	macro
.set	reorder

$L296:
lw	$10,180($23)
lw	$3,184($23)
lw	$8,120($sp)
sra	$10,$10,1
lw	$4,2856($23)
sra	$3,$3,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,100($sp)
sw	$9,168($sp)
sw	$8,16($sp)
sw	$20,20($sp)
sw	$17,24($sp)
sw	$10,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$5,2856($23)
lw	$6,196($23)
.set	noreorder
.set	nomacro
b	$L167
lw	$9,168($sp)
.set	macro
.set	reorder

$L292:
lw	$15,48($sp)
.set	noreorder
.set	nomacro
b	$L174
subu	$4,$4,$15
.set	macro
.set	reorder

$L289:
bne	$6,$0,$L260
bne	$12,$0,$L298
lh	$4,0($3)
.set	noreorder
.set	nomacro
b	$L135
lh	$2,2($3)
.set	macro
.set	reorder

$L293:
lw	$15,48($sp)
.set	noreorder
.set	nomacro
b	$L188
subu	$3,$3,$15
.set	macro
.set	reorder

$L294:
lw	$16,64($8)
slt	$2,$20,-16
lw	$6,180($8)
lw	$3,184($8)
lw	$9,52($sp)
srl	$16,$16,14
lw	$11,48($sp)
andi	$16,$16,0x1
subu	$6,$6,$9
.set	noreorder
.set	nomacro
beq	$2,$0,$L159
subu	$4,$3,$11
.set	macro
.set	reorder

li	$2,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L158
li	$20,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L291:
slt	$2,$10,8
.set	noreorder
.set	nomacro
beq	$2,$0,$L310
lw	$3,48($sp)
.set	macro
.set	reorder

addu	$3,$4,$5
addiu	$3,$3,2821
sll	$3,$3,2
addu	$3,$8,$3
.set	noreorder
.set	nomacro
b	$L210
sw	$22,0($3)
.set	macro
.set	reorder

$L297:
lw	$15,48($sp)
.set	noreorder
.set	nomacro
b	$L161
subu	$4,$3,$15
.set	macro
.set	reorder

$L216:
li	$9,-2147483648			# 0xffffffff80000000
.set	noreorder
.set	nomacro
b	$L148
move	$2,$4
.set	macro
.set	reorder

$L298:
lh	$4,0($5)
.set	noreorder
.set	nomacro
b	$L135
lh	$2,2($5)
.set	macro
.set	reorder

$L128:
lw	$11,10800($8)
andi	$11,$11,0x80
.set	noreorder
.set	nomacro
bne	$11,$0,$L209
slt	$11,$6,36
.set	macro
.set	reorder

xori	$11,$11,0x1
addiu	$11,$11,2694
sra	$6,$6,2
sll	$11,$11,2
addu	$11,$8,$11
lw	$12,4($11)
lw	$11,164($sp)
sll	$12,$12,2
addu	$4,$4,$12
lw	$4,0($4)
and	$4,$11,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L214
andi	$16,$6,0x3
.set	macro
.set	reorder

lw	$4,11864($8)
andi	$6,$6,0x2
lw	$14,11852($8)
lw	$13,228($sp)
lw	$15,156($sp)
mul	$16,$16,$4
addu	$14,$14,$12
sll	$11,$13,2
addu	$13,$8,$15
lw	$15,2696($8)
move	$4,$0
addu	$15,$15,$11
lw	$11,0($14)
addiu	$16,$16,3
addu	$11,$16,$11
lw	$14,96($15)
sll	$11,$11,2
addu	$11,$14,$11
lh	$14,0($11)
sh	$14,11288($13)
lh	$14,2($11)
srl	$11,$14,31
addu	$11,$11,$14
sra	$11,$11,1
sh	$11,11290($13)
lw	$11,188($15)
addu	$12,$11,$12
addu	$6,$12,$6
lb	$11,1($6)
.set	noreorder
.set	nomacro
b	$L130
sll	$11,$11,1
.set	macro
.set	reorder

$L214:
.set	noreorder
.set	nomacro
b	$L130
li	$11,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	svq3_mc_dir
.size	svq3_mc_dir, .-svq3_mc_dir
.section	.rodata.str1.4
.align	2
$LC10:
.ascii	"%c hpel:%d, tpel:%d aqp:%d qp:%d, slice_num:%02X\012\000"
.align	2
$LC11:
.ascii	"error in B-frame picture id\012\000"
.align	2
$LC12:
.ascii	"luma prediction:%d\012\000"
.align	2
$LC13:
.ascii	"weird prediction\012\000"
.align	2
$LC14:
.ascii	"check_intra_pred_mode = -1\012\000"
.align	2
$LC15:
.ascii	"cbp_vlc=%d\012\000"
.align	2
$LC16:
.ascii	"qscale:%d\012\000"
.align	2
$LC17:
.ascii	"error while decoding MB %d %d\012\000"
.align	2
$LC18:
.ascii	"error while decoding intra luma dc\012\000"
.align	2
$LC19:
.ascii	"error while decoding block\012\000"
.align	2
$LC20:
.ascii	"error while decoding chroma dc block\012\000"
.align	2
$LC21:
.ascii	"error while decoding chroma ac block\012\000"
.section	.text.svq3_decode_frame,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	svq3_decode_frame
.type	svq3_decode_frame, @function
svq3_decode_frame:
.frame	$sp,216,$31		# vars= 128, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$3,20($7)
addiu	$sp,$sp,-216
.cprestore	40
sw	$23,204($sp)
sw	$3,152($sp)
sw	$31,212($sp)
sw	$fp,208($sp)
sw	$22,200($sp)
sw	$21,196($sp)
sw	$20,192($sp)
sw	$19,188($sp)
sw	$18,184($sp)
sw	$17,180($sp)
sw	$16,176($sp)
lw	$9,152($sp)
lw	$3,16($7)
.set	noreorder
.set	nomacro
bne	$9,$0,$L313
lw	$23,136($4)
.set	macro
.set	reorder

lw	$2,2692($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L770
move	$2,$0
.set	macro
.set	reorder

lw	$2,10096($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L316
addiu	$4,$23,888
.set	macro
.set	reorder

addiu	$7,$23,1096
move	$3,$5
$L317:
lw	$10,0($4)
addiu	$4,$4,16
addiu	$3,$3,16
lw	$9,-12($4)
lw	$8,-8($4)
lw	$5,-4($4)
sw	$10,-16($3)
sw	$9,-12($3)
sw	$8,-8($3)
.set	noreorder
.set	nomacro
bne	$4,$7,$L317
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$5,0($4)
lw	$4,4($4)
sw	$5,0($3)
sw	$4,4($3)
li	$3,216			# 0xd8
sw	$0,2692($23)
lw	$31,212($sp)
lw	$fp,208($sp)
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
sw	$3,0($6)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L323:
lw	$2,708($16)
slt	$3,$2,8
.set	noreorder
.set	nomacro
bne	$3,$0,$L325
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$3,$L316
slt	$3,$2,32
.set	macro
.set	reorder

$L939:
.set	noreorder
.set	nomacro
bne	$3,$0,$L326
li	$3,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$4,$3,$L316
slt	$2,$2,48
.set	macro
.set	reorder

bne	$2,$0,$L326
$L316:
move	$2,$0
$L770:
lw	$31,212($sp)
lw	$fp,208($sp)
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L313:
lw	$25,152($sp)
sll	$25,$25,3
sra	$2,$25,3
.set	noreorder
.set	nomacro
bltz	$2,$L623
sw	$25,156($sp)
.set	macro
.set	reorder

bltz	$25,$L623
addu	$2,$3,$2
move	$7,$25
$L318:
lw	$25,%got(svq3_decode_slice_header)($28)
move	$16,$4
sw	$3,10332($23)
li	$3,131072			# 0x20000
sw	$7,10344($23)
move	$4,$23
addu	$3,$23,$3
sw	$2,10336($23)
addiu	$25,$25,%lo(svq3_decode_slice_header)
sw	$0,10340($23)
sw	$6,168($sp)
sw	$0,9448($3)
sw	$5,164($sp)
sw	$0,7996($23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_decode_slice_header
1:	jalr	$25
sw	$0,7992($23)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L823
lw	$28,40($sp)
.set	macro
.set	reorder

li	$17,65536			# 0x10000
lw	$2,404($16)
addu	$17,$23,$17
andi	$2,$2,0x1
lw	$4,-5264($17)
lw	$3,-5272($17)
sw	$4,2904($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L856
sw	$3,136($23)
.set	macro
.set	reorder

xori	$2,$4,0x1
lw	$3,2688($23)
sltu	$2,$2,1
sw	$4,2140($23)
.set	noreorder
.set	nomacro
beq	$3,$0,$L857
sw	$2,2136($23)
.set	macro
.set	reorder

$L321:
lw	$2,128($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L323
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$3,$L316
slt	$2,$2,5
.set	macro
.set	reorder

$L958:
.set	noreorder
.set	nomacro
beq	$2,$0,$L770
move	$2,$0
.set	macro
.set	reorder

lw	$2,708($16)
slt	$3,$2,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L939
slt	$3,$2,32
.set	macro
.set	reorder

$L325:
lw	$2,9812($23)
$L935:
.set	noreorder
.set	nomacro
beq	$2,$0,$L327
li	$2,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$2,$L770
move	$2,$0
.set	macro
.set	reorder

$L616:
sw	$0,9812($23)
$L327:
lw	$25,%call16(ff_h264_frame_start)($28)
$L937:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_frame_start
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L823
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$3,2904($23)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L858
li	$2,131072			# 0x20000
.set	macro
.set	reorder

li	$3,65536			# 0x10000
addu	$2,$23,$2
addu	$3,$23,$3
lw	$5,10680($2)
lw	$4,-5272($3)
subu	$3,$4,$5
sw	$5,10700($2)
sw	$4,10680($2)
.set	noreorder
.set	nomacro
bltz	$3,$L859
sw	$3,10696($2)
.set	macro
.set	reorder

$L332:
addiu	$11,$23,80
$L961:
li	$6,1			# 0x1
li	$5,4			# 0x4
li	$10,3			# 0x3
li	$9,-2			# 0xfffffffffffffffe
move	$7,$23
move	$8,$0
$L940:
move	$4,$7
$L338:
li	$2,-1			# 0xffffffffffffffff
$L335:
addu	$3,$4,$2
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$5,$L335
sb	$6,11580($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$8,$10,$L336
addiu	$8,$8,1
.set	macro
.set	reorder

sb	$9,11584($4)
.set	noreorder
.set	nomacro
b	$L338
addiu	$4,$4,8
.set	macro
.set	reorder

$L623:
move	$2,$0
move	$7,$0
.set	noreorder
.set	nomacro
b	$L318
move	$3,$0
.set	macro
.set	reorder

$L336:
addiu	$7,$7,40
.set	noreorder
.set	nomacro
bne	$7,$11,$L940
move	$8,$0
.set	macro
.set	reorder

lw	$2,164($23)
.set	noreorder
.set	nomacro
blez	$2,$L598
sw	$0,7996($23)
.set	macro
.set	reorder

$L719:
lw	$2,160($23)
.set	noreorder
.set	nomacro
blez	$2,$L342
sw	$0,7992($23)
.set	macro
.set	reorder

move	$fp,$23
move	$2,$0
lw	$4,168($fp)
li	$3,131072			# 0x20000
lw	$5,10344($fp)
addu	$3,$fp,$3
mul	$6,$8,$4
addu	$2,$6,$2
sw	$2,9448($3)
lw	$10,10340($fp)
addiu	$2,$10,7
slt	$2,$2,$5
bne	$2,$0,$L860
$L343:
andi	$5,$10,0x7
.set	noreorder
.set	nomacro
beq	$5,$0,$L346
srl	$3,$10,3
.set	macro
.set	reorder

lw	$6,10332($fp)
li	$7,16711680			# 0xff0000
addu	$3,$6,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$2,8
sll	$2,$2,8
and	$4,$4,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$2,$2,$7
or	$4,$4,$2
sll	$2,$4,16
subu	$7,$0,$10
srl	$4,$4,16
or	$4,$2,$4
andi	$2,$7,0x7
sll	$4,$4,$5
subu	$2,$0,$2
srl	$4,$4,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L346
addiu	$7,$3,3
.set	macro
.set	reorder

move	$2,$7
$L344:
li	$13,16711680			# 0xff0000
li	$12,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 0($2)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
addiu	$13,$13,255
srl	$2,$4,8
sll	$3,$4,8
ori	$12,$12,0xff00
and	$3,$3,$12
and	$2,$2,$13
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$5
li	$3,-1434451968			# 0xffffffffaa800000
and	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L347
lw	$9,%got(ff_interleaved_golomb_vlc_len)($28)
.set	macro
.set	reorder

srl	$2,$2,24
addu	$3,$9,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L861
sw	$9,140($sp)
.set	macro
.set	reorder

lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$11,1			# 0x1
$L348:
addu	$10,$10,$3
$L351:
addiu	$3,$3,-1
$L930:
lw	$13,2904($fp)
addu	$2,$7,$2
sw	$10,10340($fp)
sra	$3,$3,1
lbu	$2,0($2)
sll	$11,$11,$3
or	$2,$11,$2
addiu	$2,$2,-1
sw	$2,136($sp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$13,$2,$L941
lw	$3,136($sp)
.set	macro
.set	reorder

$L355:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$13,$2,$L863
lw	$9,136($sp)
.set	macro
.set	reorder

$L356:
lw	$25,136($sp)
slt	$2,$25,34
.set	noreorder
.set	nomacro
beq	$2,$0,$L358
move	$23,$fp
.set	macro
.set	reorder

$L357:
lw	$2,11864($fp)
li	$12,131072			# 0x20000
sll	$5,$8,2
lw	$3,7992($fp)
li	$4,65535			# 0xffff
lw	$16,136($sp)
li	$11,13311			# 0x33ff
li	$10,24415			# 0x5f5f
addu	$12,$fp,$12
movn	$11,$4,$8
mul	$9,$5,$2
movn	$10,$4,$3
sll	$7,$3,2
lw	$12,9448($12)
sw	$11,11088($fp)
sw	$4,11092($fp)
sw	$10,11096($fp)
sw	$12,144($sp)
.set	noreorder
.set	nomacro
bne	$16,$0,$L361
addu	$17,$9,$7
.set	macro
.set	reorder

li	$12,2			# 0x2
.set	noreorder
.set	nomacro
beq	$13,$12,$L362
lw	$9,144($sp)
.set	macro
.set	reorder

li	$6,-1			# 0xffffffffffffffff
lw	$4,992($fp)
sll	$9,$9,2
addu	$4,$4,$9
sw	$9,148($sp)
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$6,$L362
li	$6,6			# 0x6
.set	macro
.set	reorder

sltu	$10,$4,7
movn	$6,$4,$10
li	$4,4			# 0x4
andi	$9,$6,0x5
sw	$6,160($sp)
.set	noreorder
.set	nomacro
beq	$9,$4,$L632
sw	$9,92($sp)
.set	macro
.set	reorder

andi	$4,$6,0x1
li	$6,16			# 0x10
sra	$6,$6,$4
sra	$13,$6,3
sra	$4,$6,2
sw	$6,64($sp)
subu	$12,$12,$13
sll	$4,$4,2
sll	$12,$12,4
sw	$4,56($sp)
$L391:
lw	$25,160($sp)
li	$14,-1431699456			# 0xffffffffaaaa0000
li	$15,16			# 0x10
lw	$9,64($sp)
ori	$14,$14,0xaaab
lw	$11,180($fp)
addiu	$6,$25,1
lw	$10,184($fp)
addiu	$19,$fp,3504
sw	$0,84($sp)
multu	$6,$14
subu	$4,$11,$9
sra	$9,$9,1
addu	$12,$19,$12
mfhi	$6
sll	$18,$4,1
sw	$9,112($sp)
sll	$16,$4,3
sw	$12,96($sp)
lw	$9,64($sp)
li	$12,131072			# 0x20000
subu	$16,$16,$18
addu	$12,$fp,$12
addiu	$16,$16,96
srl	$6,$6,1
addiu	$9,$9,1
sw	$12,68($sp)
sra	$15,$15,$6
sw	$16,120($sp)
move	$22,$8
subu	$6,$10,$15
sw	$9,132($sp)
sw	$15,60($sp)
li	$15,3			# 0x3
lw	$25,60($sp)
sll	$17,$6,1
subu	$13,$15,$13
sll	$15,$6,3
sra	$25,$25,2
subu	$15,$15,$17
sll	$13,$13,4
sw	$25,100($sp)
addiu	$15,$15,96
lw	$25,60($sp)
addu	$13,$19,$13
move	$21,$3
sw	$15,124($sp)
move	$18,$5
sra	$25,$25,1
sw	$13,116($sp)
move	$13,$7
sw	$25,72($sp)
$L392:
lw	$25,84($sp)
move	$3,$0
lw	$9,60($sp)
move	$7,$18
move	$19,$22
sra	$25,$25,2
addiu	$9,$9,1
move	$18,$21
sw	$25,88($sp)
addiu	$25,$fp,2100
sw	$9,128($sp)
move	$8,$2
move	$21,$3
.set	noreorder
.set	nomacro
b	$L418
sw	$25,80($sp)
.set	macro
.set	reorder

$L871:
.set	noreorder
.set	nomacro
bltz	$22,$L399
addiu	$2,$6,-1
.set	macro
.set	reorder

slt	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L399
move	$5,$23
.set	macro
.set	reorder

sw	$0,48($sp)
$L401:
lw	$6,192($fp)
lw	$2,2088($fp)
lw	$3,288($fp)
mul	$7,$19,$6
lw	$9,48($sp)
addu	$4,$7,$18
mul	$7,$6,$22
addu	$2,$2,$4
addu	$5,$7,$5
.set	noreorder
.set	nomacro
bne	$9,$0,$L864
addu	$5,$3,$5
.set	macro
.set	reorder

$L402:
lw	$9,96($sp)
sll	$20,$20,2
move	$4,$2
addu	$2,$9,$20
lw	$25,0($2)
.set	noreorder
.set	nomacro
jalr	$25
lw	$7,60($sp)
.set	macro
.set	reorder

lw	$2,64($fp)
andi	$2,$2,0x2000
.set	noreorder
.set	nomacro
bne	$2,$0,$L408
lw	$28,40($sp)
.set	macro
.set	reorder

slt	$2,$22,$19
lw	$9,112($sp)
addu	$22,$22,$2
lw	$25,72($sp)
lw	$2,116($sp)
slt	$4,$23,$18
sra	$18,$18,1
addu	$23,$23,$4
addu	$20,$2,$20
addiu	$9,$9,1
sw	$18,76($sp)
addiu	$25,$25,1
addiu	$18,$fp,2092
sra	$19,$19,1
sw	$9,104($sp)
move	$2,$20
sw	$25,108($sp)
sra	$23,$23,1
sra	$22,$22,1
move	$20,$19
move	$19,$18
move	$18,$fp
move	$fp,$21
move	$21,$17
move	$17,$16
move	$16,$2
$L407:
lw	$6,196($18)
addiu	$19,$19,4
lw	$2,76($sp)
lw	$3,-1804($19)
mul	$5,$6,$20
lw	$8,-4($19)
addu	$4,$5,$2
mul	$2,$22,$6
addu	$5,$2,$23
addu	$5,$3,$5
lw	$3,48($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L865
addu	$8,$8,$4
.set	macro
.set	reorder

$L406:
lw	$25,0($16)
move	$4,$8
.set	noreorder
.set	nomacro
jalr	$25
lw	$7,72($sp)
.set	macro
.set	reorder

lw	$25,80($sp)
.set	noreorder
.set	nomacro
bne	$19,$25,$L407
lw	$28,40($sp)
.set	macro
.set	reorder

move	$16,$17
move	$17,$21
move	$21,$fp
move	$fp,$18
$L408:
sll	$5,$17,1
lw	$4,2184($fp)
sll	$6,$16,1
lw	$8,11864($fp)
addu	$17,$5,$17
lw	$25,52($sp)
addu	$16,$6,$16
lw	$3,56($sp)
sll	$17,$17,16
li	$5,4			# 0x4
andi	$16,$16,0xffff
addu	$4,$4,$25
addu	$16,$16,$17
.set	noreorder
.set	nomacro
beq	$3,$5,$L866
sll	$2,$8,2
.set	macro
.set	reorder

lw	$25,56($sp)
li	$5,8			# 0x8
.set	noreorder
.set	nomacro
beq	$25,$5,$L867
li	$5,16			# 0x10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$25,$5,$L868
lw	$3,100($sp)
.set	macro
.set	reorder

$L410:
lw	$9,64($sp)
addu	$21,$21,$9
slt	$2,$21,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L942
lw	$25,84($sp)
.set	macro
.set	reorder

$L416:
lw	$18,7992($fp)
lw	$19,7996($fp)
lw	$11,180($fp)
lw	$10,184($fp)
sll	$13,$18,2
lw	$25,64($sp)
sll	$7,$19,2
lw	$2,60($sp)
subu	$4,$11,$25
subu	$6,$10,$2
$L418:
lw	$3,88($sp)
sra	$5,$21,2
lw	$12,984($fp)
sll	$18,$18,4
addu	$5,$13,$5
lw	$9,68($sp)
addu	$7,$7,$3
addu	$18,$21,$18
mul	$3,$7,$8
sll	$7,$18,1
lw	$2,10692($9)
lw	$9,10696($9)
sll	$19,$19,4
addu	$5,$3,$5
lw	$3,84($sp)
sll	$5,$5,2
addu	$19,$3,$19
sw	$5,52($sp)
sll	$5,$18,3
lw	$25,52($sp)
subu	$7,$7,$5
addu	$8,$12,$25
addiu	$16,$7,-96
lh	$5,0($8)
lh	$8,2($8)
sll	$5,$5,1
sll	$8,$8,1
mul	$5,$5,$2
mul	$2,$8,$2
teq	$9,$0,7
div	$0,$5,$9
mflo	$5
teq	$9,$0,7
div	$0,$2,$9
addiu	$5,$5,1
sra	$5,$5,1
slt	$8,$5,$16
mflo	$2
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$8,$0,$L393
sra	$2,$2,1
.set	macro
.set	reorder

lw	$3,120($sp)
addu	$7,$3,$7
slt	$16,$5,$7
movz	$5,$7,$16
move	$16,$5
$L393:
sll	$17,$19,3
sll	$5,$19,1
subu	$5,$5,$17
addiu	$17,$5,-96
slt	$7,$2,$17
.set	noreorder
.set	nomacro
bne	$7,$0,$L394
lw	$9,124($sp)
.set	macro
.set	reorder

addu	$17,$9,$5
slt	$5,$2,$17
movn	$17,$2,$5
$L394:
li	$2,-1431699456			# 0xffffffffaaaa0000
addiu	$16,$16,12289
ori	$2,$2,0xaaab
multu	$16,$2
addiu	$17,$17,12289
mfhi	$16
multu	$17,$2
mfhi	$17
srl	$16,$16,1
addiu	$16,$16,-4096
sra	$23,$16,1
srl	$17,$17,1
addu	$23,$18,$23
addiu	$17,$17,-4096
andi	$20,$17,0x1
sll	$2,$20,1
sra	$22,$17,1
andi	$20,$16,0x1
addu	$20,$20,$2
.set	noreorder
.set	nomacro
bltz	$23,$L870
addu	$22,$19,$22
.set	macro
.set	reorder

addiu	$2,$4,-1
slt	$2,$23,$2
bne	$2,$0,$L871
$L399:
lw	$2,64($fp)
srl	$2,$2,14
andi	$2,$2,0x1
sw	$2,48($sp)
$L397:
addiu	$4,$4,15
slt	$2,$4,$23
movn	$23,$4,$2
move	$5,$23
$L396:
slt	$2,$22,-16
.set	noreorder
.set	nomacro
bne	$2,$0,$L634
addiu	$6,$6,15
.set	macro
.set	reorder

lw	$3,288($fp)
lw	$9,48($sp)
slt	$2,$6,$22
movn	$22,$6,$2
lw	$6,192($fp)
lw	$2,2088($fp)
mul	$7,$19,$6
addu	$4,$7,$18
mul	$7,$6,$22
addu	$2,$2,$4
addu	$5,$7,$5
.set	noreorder
.set	nomacro
beq	$9,$0,$L402
addu	$5,$3,$5
.set	macro
.set	reorder

$L864:
lw	$3,128($sp)
lw	$4,2856($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,132($sp)
sw	$2,172($sp)
sw	$3,16($sp)
sw	$23,20($sp)
sw	$22,24($sp)
sw	$11,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$10,32($sp)
.set	macro
.set	reorder

lw	$5,2856($fp)
lw	$6,192($fp)
.set	noreorder
.set	nomacro
b	$L402
lw	$2,172($sp)
.set	macro
.set	reorder

$L634:
.set	noreorder
.set	nomacro
b	$L401
li	$22,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L865:
lw	$9,108($sp)
lw	$4,2856($18)
sw	$23,20($sp)
sw	$22,24($sp)
sw	$9,16($sp)
lw	$3,180($18)
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,104($sp)
sra	$3,$3,1
sw	$3,28($sp)
lw	$3,184($18)
sw	$8,172($sp)
sra	$3,$3,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$5,2856($18)
lw	$6,196($18)
.set	noreorder
.set	nomacro
b	$L406
lw	$8,172($sp)
.set	macro
.set	reorder

$L866:
lw	$3,100($sp)
li	$5,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$5,$L818
sw	$16,0($4)
.set	macro
.set	reorder

lw	$9,100($sp)
addu	$4,$4,$2
li	$5,2			# 0x2
.set	noreorder
.set	nomacro
beq	$9,$5,$L818
sw	$16,0($4)
.set	macro
.set	reorder

lw	$9,64($sp)
addu	$4,$4,$2
addu	$2,$4,$2
sw	$16,0($4)
addu	$21,$21,$9
sw	$16,0($2)
slt	$2,$21,16
.set	noreorder
.set	nomacro
bne	$2,$0,$L416
lw	$8,11864($fp)
.set	macro
.set	reorder

lw	$25,84($sp)
$L942:
move	$2,$8
lw	$3,60($sp)
addu	$25,$25,$3
slt	$3,$25,16
.set	noreorder
.set	nomacro
beq	$3,$0,$L417
sw	$25,84($sp)
.set	macro
.set	reorder

lw	$11,180($fp)
lw	$21,7992($fp)
lw	$22,7996($fp)
lw	$10,184($fp)
subu	$4,$11,$9
lw	$9,60($sp)
sll	$13,$21,2
sll	$18,$22,2
.set	noreorder
.set	nomacro
b	$L392
subu	$6,$10,$9
.set	macro
.set	reorder

$L868:
addu	$6,$4,$2
li	$5,2			# 0x2
sw	$16,0($4)
sw	$16,4($4)
sw	$16,8($4)
sw	$16,12($4)
sw	$16,0($6)
sw	$16,4($6)
sw	$16,8($6)
.set	noreorder
.set	nomacro
beq	$3,$5,$L818
sw	$16,12($6)
.set	macro
.set	reorder

sll	$5,$8,3
addu	$6,$6,$2
addu	$7,$5,$2
sw	$16,0($6)
addu	$5,$4,$5
addu	$6,$6,$2
addu	$4,$4,$7
sw	$16,4($5)
sw	$16,8($5)
sw	$16,12($5)
sw	$16,0($6)
sw	$16,4($4)
sw	$16,8($4)
sw	$16,12($4)
$L818:
.set	noreorder
.set	nomacro
b	$L410
lw	$8,11864($fp)
.set	macro
.set	reorder

$L870:
lw	$3,64($fp)
slt	$2,$23,-16
srl	$3,$3,14
andi	$3,$3,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L397
sw	$3,48($sp)
.set	macro
.set	reorder

li	$5,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L396
li	$23,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L867:
lw	$3,100($sp)
li	$5,1			# 0x1
sw	$16,0($4)
.set	noreorder
.set	nomacro
beq	$3,$5,$L818
sw	$16,4($4)
.set	macro
.set	reorder

lw	$9,100($sp)
addu	$6,$4,$2
li	$5,2			# 0x2
sw	$16,0($6)
.set	noreorder
.set	nomacro
beq	$9,$5,$L818
sw	$16,4($6)
.set	macro
.set	reorder

sll	$5,$8,3
addu	$6,$6,$2
addu	$7,$5,$2
sw	$16,0($6)
addu	$5,$4,$5
addu	$6,$6,$2
addu	$4,$4,$7
sw	$16,4($5)
sw	$16,0($6)
sw	$16,4($4)
.set	noreorder
.set	nomacro
b	$L410
lw	$8,11864($fp)
.set	macro
.set	reorder

$L361:
lw	$9,136($sp)
sltu	$2,$9,8
.set	noreorder
.set	nomacro
beq	$2,$0,$L444
li	$2,8			# 0x8
.set	macro
.set	reorder

li	$2,196608			# 0x30000
addu	$2,$fp,$2
lw	$5,-15020($2)
bne	$5,$0,$L445
lw	$7,-15024($2)
$L446:
.set	noreorder
.set	nomacro
beq	$7,$0,$L447
li	$18,1			# 0x1
.set	macro
.set	reorder

lw	$4,10340($fp)
li	$18,2			# 0x2
srl	$2,$4,3
andi	$7,$4,0x7
addu	$6,$6,$2
addiu	$4,$4,1
lbu	$2,0($6)
sw	$4,10340($fp)
li	$4,1			# 0x1
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$2,$2,0x1
xor	$2,$5,$2
movn	$18,$4,$2
$L447:
li	$21,1073676288			# 0x3fff0000
lw	$25,144($sp)
addiu	$23,$17,-1
sw	$0,60($sp)
ori	$21,$21,0xffff
sw	$18,72($sp)
addu	$19,$25,$21
addiu	$22,$fp,2184
sll	$19,$19,2
move	$21,$23
move	$20,$fp
move	$16,$fp
move	$23,$17
$L462:
blez	$3,$L448
lw	$3,11856($fp)
lw	$2,10856($fp)
addu	$3,$3,$19
lw	$3,0($3)
addu	$2,$2,$3
li	$3,-1			# 0xffffffffffffffff
lb	$2,6($2)
.set	noreorder
.set	nomacro
beq	$2,$3,$L448
move	$3,$0
.set	macro
.set	reorder

lw	$5,0($22)
move	$4,$20
$L451:
lw	$2,11864($fp)
addiu	$4,$4,32
li	$7,4			# 0x4
mul	$6,$3,$2
addiu	$3,$3,1
addu	$2,$6,$21
sll	$2,$2,2
addu	$2,$5,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
bne	$3,$7,$L451
sw	$2,11260($4)
.set	macro
.set	reorder

lw	$2,7996($fp)
.set	noreorder
.set	nomacro
blez	$2,$L943
lw	$25,%call16(memset)($28)
.set	macro
.set	reorder

$L911:
lw	$2,11864($fp)
addiu	$4,$20,11264
lw	$5,0($22)
li	$6,16			# 0x10
lw	$25,%call16(memcpy)($28)
li	$17,1			# 0x1
subu	$2,$23,$2
li	$18,-2			# 0xfffffffffffffffe
sll	$2,$2,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$5,$2
.set	macro
.set	reorder

addiu	$4,$16,11572
lw	$2,168($fp)
li	$6,4			# 0x4
lw	$9,144($sp)
lw	$8,11856($fp)
lw	$5,10856($fp)
subu	$2,$9,$2
lw	$28,40($sp)
sll	$2,$2,2
addu	$2,$8,$2
lw	$25,%call16(memset)($28)
lw	$2,0($2)
addu	$2,$5,$2
lb	$5,0($2)
move	$2,$17
nor	$5,$0,$5
movz	$2,$18,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$5,$2
.set	macro
.set	reorder

lw	$2,160($fp)
lw	$4,7992($fp)
addiu	$2,$2,-1
slt	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L454
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$2,11864($fp)
lw	$6,0($22)
lw	$5,11856($fp)
subu	$2,$23,$2
lw	$4,10856($fp)
lw	$3,144($sp)
addiu	$2,$2,4
sll	$2,$2,2
addu	$2,$6,$2
li	$6,-1			# 0xffffffffffffffff
lw	$2,0($2)
sw	$2,11280($20)
lw	$2,168($fp)
subu	$2,$3,$2
addiu	$2,$2,1
sll	$2,$2,2
addu	$2,$5,$2
lw	$5,0($2)
addu	$5,$4,$5
lb	$5,6($5)
beq	$5,$6,$L642
lw	$2,-4($2)
addu	$4,$4,$2
lb	$2,0($4)
nor	$2,$0,$2
movz	$17,$18,$2
sb	$17,11576($16)
lw	$4,7992($fp)
$L456:
.set	noreorder
.set	nomacro
blez	$4,$L944
li	$2,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

$L912:
lw	$2,11864($fp)
li	$7,1073676288			# 0x3fff0000
lw	$5,0($22)
ori	$7,$7,0xffff
lw	$4,11856($fp)
subu	$2,$23,$2
lw	$3,10856($fp)
lw	$9,144($sp)
addu	$2,$2,$7
sll	$2,$2,2
addu	$2,$5,$2
lw	$2,0($2)
sw	$2,11260($20)
lw	$2,168($fp)
subu	$2,$9,$2
addu	$2,$2,$7
sll	$2,$2,2
addu	$2,$4,$2
li	$4,-2			# 0xfffffffffffffffe
lw	$2,0($2)
addu	$2,$3,$2
lb	$2,3($2)
nor	$3,$0,$2
li	$2,1			# 0x1
movz	$2,$4,$3
sb	$2,11571($16)
$L459:
lw	$2,2904($fp)
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
bne	$2,$3,$L460
lw	$25,60($sp)
.set	macro
.set	reorder

addiu	$16,$16,40
addiu	$20,$20,160
.set	noreorder
.set	nomacro
beq	$25,$0,$L872
addiu	$22,$22,4
.set	macro
.set	reorder

lw	$18,72($sp)
move	$17,$23
$L461:
lw	$3,136($sp)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$3,$2,$L873
lw	$16,%got(svq3_mc_dir)($28)
.set	macro
.set	reorder

move	$16,$0
li	$20,4			# 0x4
$L465:
lw	$2,11864($fp)
move	$5,$0
lw	$4,2184($fp)
li	$6,16			# 0x10
lw	$25,%call16(memset)($28)
mul	$3,$16,$2
addiu	$16,$16,1
addu	$2,$3,$17
sll	$2,$2,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$20,$L465
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$16,%got(svq3_mc_dir)($28)
addiu	$16,$16,%lo(svq3_mc_dir)
lw	$9,136($sp)
$L957:
move	$5,$0
li	$7,1			# 0x1
move	$4,$fp
xori	$2,$9,0x3
sltu	$2,$2,1
sw	$2,16($sp)
$L825:
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_mc_dir
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L810
lw	$28,40($sp)
.set	macro
.set	reorder

addiu	$19,$19,4
$L933:
sw	$19,148($sp)
li	$7,8			# 0x8
$L956:
sw	$0,60($sp)
move	$16,$0
move	$18,$0
sw	$7,72($sp)
$L607:
lw	$2,11856($fp)
li	$5,2			# 0x2
lw	$3,148($sp)
li	$6,8			# 0x8
lw	$4,10856($fp)
move	$20,$0
lw	$25,%call16(memset)($28)
addu	$2,$2,$3
lw	$2,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$28,40($sp)
$L501:
.set	noreorder
.set	nomacro
beq	$18,$0,$L503
li	$17,3			# 0x3
.set	macro
.set	reorder

lw	$2,2904($fp)
.set	noreorder
.set	nomacro
beq	$2,$17,$L874
lw	$25,%call16(memset)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L932
li	$3,1			# 0x1
.set	macro
.set	reorder

$L608:
lw	$5,10340($fp)
li	$3,16711680			# 0xff0000
lw	$12,10332($fp)
li	$6,-16777216			# 0xffffffffff000000
addiu	$8,$3,255
srl	$2,$5,3
ori	$6,$6,0xff00
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$8
and	$4,$4,$6
or	$2,$2,$4
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$5,0x7
or	$2,$3,$2
sll	$4,$2,$4
li	$2,-1434451968			# 0xffffffffaa800000
and	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L875
lw	$9,140($sp)
.set	macro
.set	reorder

addiu	$2,$5,8
srl	$3,$2,3
andi	$2,$2,0x7
addu	$3,$12,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
and	$3,$3,$8
and	$6,$7,$6
or	$6,$3,$6
sll	$3,$6,16
srl	$6,$6,16
or	$3,$3,$6
sll	$2,$3,$2
li	$3,-1431699456			# 0xffffffffaaaa0000
srl	$2,$2,8
ori	$3,$3,0xaaaa
or	$2,$2,$4
and	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L653
ori	$2,$2,0x1
.set	macro
.set	reorder

li	$4,31			# 0x1f
sll	$3,$2,$4
$L945:
addiu	$4,$4,-1
sll	$6,$2,2
srl	$2,$2,30
srl	$3,$3,$4
or	$2,$2,$6
subu	$2,$2,$3
.set	noreorder
.set	nomacro
bgez	$2,$L945
sll	$3,$2,$4
.set	macro
.set	reorder

andi	$2,$2,0x1
srl	$3,$3,$4
subu	$2,$0,$2
addiu	$3,$3,-1
addiu	$5,$5,63
sll	$4,$4,1
xor	$2,$3,$2
subu	$4,$5,$4
addiu	$2,$2,1
sra	$2,$2,1
sw	$4,10340($fp)
$L522:
lw	$7,2872($fp)
addu	$7,$2,$7
slt	$2,$7,32
.set	noreorder
.set	nomacro
beq	$2,$0,$L876
sw	$7,2872($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L519
lw	$25,%got(svq3_dct_tables)($28)
.set	macro
.set	reorder

li	$17,131072			# 0x20000
lw	$18,%got(luma_dc_zigzag_scan)($28)
li	$11,16711680			# 0xff0000
addiu	$17,$17,6944
lw	$24,%got(ff_interleaved_ue_golomb_vlc_code)($28)
li	$10,-16777216			# 0xffffffffff000000
lw	$13,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
lw	$9,140($sp)
addu	$17,$fp,$17
move	$8,$0
addiu	$11,$11,255
li	$21,-1434451968			# 0xffffffffaa800000
li	$19,-2147483648			# 0xffffffff80000000
li	$15,9			# 0x9
addiu	$22,$25,%lo(svq3_dct_tables)
addiu	$18,$18,%lo(luma_dc_zigzag_scan)
ori	$10,$10,0xff00
$L525:
lw	$6,10340($fp)
srl	$2,$6,3
andi	$4,$6,0x7
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$11
and	$3,$3,$10
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$4
and	$3,$2,$21
.set	noreorder
.set	nomacro
bne	$3,$0,$L530
srl	$2,$2,24
.set	macro
.set	reorder

addu	$3,$9,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L877
lw	$14,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$7,1			# 0x1
$L531:
addu	$6,$6,$3
$L534:
addiu	$3,$3,-1
$L927:
addu	$2,$14,$2
sra	$3,$3,1
lbu	$2,0($2)
sll	$7,$7,$3
or	$2,$7,$2
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
beq	$2,$0,$L519
sw	$6,10340($fp)
.set	macro
.set	reorder

beq	$2,$19,$L526
$L533:
addiu	$4,$2,1
andi	$2,$2,0x1
sra	$3,$4,1
slt	$5,$3,16
.set	noreorder
.set	nomacro
bne	$5,$0,$L878
addiu	$2,$2,-1
.set	macro
.set	reorder

andi	$3,$3,0xf
.set	noreorder
.set	nomacro
beq	$3,$0,$L654
sra	$4,$4,5
.set	macro
.set	reorder

slt	$5,$3,3
.set	noreorder
.set	nomacro
bne	$5,$0,$L529
li	$5,2			# 0x2
.set	macro
.set	reorder

slt	$5,$3,10
$L529:
addu	$8,$8,$3
slt	$3,$8,16
.set	noreorder
.set	nomacro
beq	$3,$0,$L526
addu	$4,$4,$5
.set	macro
.set	reorder

$L879:
addu	$5,$18,$8
xor	$3,$4,$2
subu	$2,$3,$2
lbu	$3,0($5)
addiu	$8,$8,1
sll	$3,$3,1
addu	$3,$17,$3
.set	noreorder
.set	nomacro
b	$L525
sh	$2,0($3)
.set	macro
.set	reorder

$L878:
sll	$3,$3,1
addu	$4,$3,$22
lbu	$3,0($4)
addu	$8,$8,$3
slt	$3,$8,16
.set	noreorder
.set	nomacro
bne	$3,$0,$L879
lbu	$4,1($4)
.set	macro
.set	reorder

$L526:
lw	$6,%got($LC18)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC18)
.set	macro
.set	reorder

lw	$28,40($sp)
$L464:
lw	$8,7996($23)
$L358:
lw	$6,%got($LC17)($28)
$L964:
li	$5,16			# 0x10
lw	$4,0($23)
lw	$7,7992($23)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$8,16($sp)
.set	macro
.set	reorder

$L823:
lw	$31,212($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$fp,208($sp)
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L530:
addu	$3,$9,$2
addu	$2,$24,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
bne	$2,$0,$L533
sw	$6,10340($fp)
.set	macro
.set	reorder

$L519:
lw	$3,60($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L946
lw	$9,144($sp)
.set	macro
.set	reorder

lw	$2,2872($fp)
slt	$2,$2,24
.set	noreorder
.set	nomacro
beq	$2,$0,$L658
sltu	$23,$0,$16
.set	macro
.set	reorder

li	$2,1			# 0x1
li	$3,2			# 0x2
movn	$2,$3,$20
sw	$2,48($sp)
$L539:
lw	$5,48($sp)
li	$3,16			# 0x10
lw	$6,%got(scan_patterns.8591)($28)
li	$11,16711680			# 0xff0000
lw	$7,%got(svq3_dct_tables)($28)
li	$10,-16777216			# 0xffffffffff000000
sll	$21,$5,1
lw	$22,%got(ff_interleaved_ue_golomb_vlc_code)($28)
sll	$2,$5,2
lw	$13,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
addu	$21,$21,$5
lw	$14,140($sp)
addiu	$6,$6,%lo(scan_patterns.8591)
sra	$21,$21,2
addu	$2,$2,$6
sra	$3,$3,$21
move	$25,$0
addiu	$11,$11,255
sw	$2,92($sp)
li	$18,-1434451968			# 0xffffffffaa800000
sw	$3,52($sp)
li	$19,-2147483648			# 0xffffffff80000000
li	$15,9			# 0x9
sll	$24,$21,4
addiu	$9,$7,%lo(svq3_dct_tables)
ori	$10,$10,0xff00
lw	$3,60($sp)
$L949:
sra	$2,$3,$25
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L540
andi	$2,$25,0x2
.set	macro
.set	reorder

lw	$4,92($sp)
sll	$2,$2,2
lw	$12,10332($fp)
andi	$3,$25,0x1
sw	$25,100($sp)
sll	$5,$25,2
sll	$3,$3,1
sw	$2,64($sp)
lw	$31,0($4)
move	$2,$0
sw	$5,68($sp)
sw	$3,56($sp)
move	$25,$2
$L610:
.set	noreorder
.set	nomacro
beq	$23,$0,$L541
lw	$3,68($sp)
.set	macro
.set	reorder

lw	$6,56($sp)
andi	$2,$25,0x1
andi	$3,$25,0x2
lw	$7,64($sp)
sll	$3,$3,1
addu	$2,$2,$6
addu	$2,$2,$3
addu	$2,$2,$7
$L542:
lw	$4,%got(scan8)($28)
li	$5,65536			# 0x10000
sll	$17,$2,4
lw	$20,52($sp)
ori	$5,$5,0xd90
addiu	$4,$4,%lo(scan8)
addu	$17,$17,$5
addu	$3,$4,$2
sll	$17,$17,1
li	$6,1			# 0x1
lbu	$2,0($3)
addu	$17,$fp,$17
move	$8,$23
addu	$2,$fp,$2
sb	$6,11184($2)
$L561:
lw	$6,10340($fp)
srl	$3,$6,3
andi	$2,$6,0x7
addu	$3,$12,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$7,8
sll	$3,$7,8
and	$4,$4,$11
and	$3,$3,$10
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
and	$3,$2,$18
.set	noreorder
.set	nomacro
bne	$3,$0,$L550
srl	$2,$2,24
.set	macro
.set	reorder

addu	$3,$14,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L880
lw	$16,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$7,1			# 0x1
$L551:
addu	$6,$6,$3
$L555:
addiu	$3,$3,-1
$L926:
addu	$2,$16,$2
sra	$3,$3,1
lbu	$2,0($2)
sll	$7,$7,$3
or	$2,$7,$2
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
beq	$2,$0,$L554
sw	$6,10340($fp)
.set	macro
.set	reorder

beq	$2,$19,$L544
$L553:
addiu	$3,$2,1
andi	$2,$2,0x1
sra	$4,$3,1
slt	$5,$4,16
.set	noreorder
.set	nomacro
beq	$5,$0,$L545
addiu	$2,$2,-1
.set	macro
.set	reorder

addu	$4,$24,$4
sll	$3,$4,1
addu	$3,$3,$9
lbu	$4,0($3)
addu	$8,$8,$4
slt	$4,$8,$20
.set	noreorder
.set	nomacro
beq	$4,$0,$L544
lbu	$3,1($3)
.set	macro
.set	reorder

addu	$4,$31,$8
$L947:
xor	$3,$3,$2
subu	$2,$3,$2
lbu	$3,0($4)
addiu	$8,$8,1
sll	$3,$3,1
addu	$3,$17,$3
.set	noreorder
.set	nomacro
b	$L561
sh	$2,0($3)
.set	macro
.set	reorder

$L550:
addu	$3,$14,$2
addu	$2,$22,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
bne	$2,$0,$L553
sw	$6,10340($fp)
.set	macro
.set	reorder

$L554:
lw	$3,48($sp)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$3,$2,$L560
slt	$3,$20,16
.set	macro
.set	reorder

addiu	$2,$20,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L560
move	$8,$20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L561
move	$20,$2
.set	macro
.set	reorder

$L545:
beq	$21,$0,$L547
andi	$4,$4,0x7
.set	noreorder
.set	nomacro
beq	$4,$0,$L660
sra	$3,$3,4
.set	macro
.set	reorder

li	$5,1			# 0x1
.set	noreorder
.set	nomacro
beq	$4,$5,$L663
slt	$5,$4,5
.set	macro
.set	reorder

xori	$5,$5,0x1
subu	$5,$0,$5
addu	$3,$3,$5
$L883:
addu	$8,$8,$4
slt	$4,$8,$20
.set	noreorder
.set	nomacro
bne	$4,$0,$L947
addu	$4,$31,$8
.set	macro
.set	reorder

$L544:
lw	$6,%got($LC19)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC19)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L880:
.set	noreorder
.set	nomacro
bne	$3,$15,$L882
addiu	$6,$6,8
.set	macro
.set	reorder

lw	$16,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$7,1			# 0x1
$L557:
srl	$4,$6,3
addu	$2,$13,$2
addu	$4,$12,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$3,8
sll	$3,$3,8
and	$4,$4,$11
and	$3,$3,$10
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$6,0x7
or	$3,$4,$3
lbu	$4,0($2)
sll	$5,$3,$5
sll	$7,$7,4
srl	$2,$5,24
or	$7,$4,$7
addu	$3,$14,$2
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L551
.set	noreorder
.set	nomacro
beq	$3,$15,$L557
addiu	$6,$6,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L926
addiu	$3,$3,-1
.set	macro
.set	reorder

$L547:
andi	$4,$4,0xf
.set	noreorder
.set	nomacro
beq	$4,$0,$L662
sra	$3,$3,5
.set	macro
.set	reorder

slt	$5,$4,3
.set	noreorder
.set	nomacro
bne	$5,$0,$L948
li	$5,2			# 0x2
.set	macro
.set	reorder

slt	$5,$4,10
.set	noreorder
.set	nomacro
b	$L883
addu	$3,$3,$5
.set	macro
.set	reorder

$L660:
li	$5,8			# 0x8
.set	noreorder
.set	nomacro
b	$L883
addu	$3,$3,$5
.set	macro
.set	reorder

$L662:
li	$5,4			# 0x4
.set	noreorder
.set	nomacro
b	$L883
addu	$3,$3,$5
.set	macro
.set	reorder

$L663:
li	$5,2			# 0x2
$L948:
.set	noreorder
.set	nomacro
b	$L883
addu	$3,$3,$5
.set	macro
.set	reorder

$L541:
.set	noreorder
.set	nomacro
b	$L542
addu	$2,$25,$3
.set	macro
.set	reorder

$L877:
.set	noreorder
.set	nomacro
bne	$3,$15,$L884
addiu	$6,$6,8
.set	macro
.set	reorder

lw	$14,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$7,1			# 0x1
$L536:
srl	$3,$6,3
addu	$2,$13,$2
addu	$3,$12,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$23,$4,8
and	$4,$3,$11
and	$3,$23,$10
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$6,0x7
or	$3,$4,$3
lbu	$4,0($2)
sll	$3,$3,$5
sll	$7,$7,4
srl	$2,$3,24
or	$7,$4,$7
addu	$3,$9,$2
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L531
.set	noreorder
.set	nomacro
beq	$3,$15,$L536
addiu	$6,$6,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L927
addiu	$3,$3,-1
.set	macro
.set	reorder

$L560:
addiu	$25,$25,1
li	$2,4			# 0x4
bne	$25,$2,$L610
lw	$25,100($sp)
$L540:
addiu	$25,$25,1
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
bne	$25,$2,$L949
lw	$3,60($sp)
.set	macro
.set	reorder

lw	$7,60($sp)
andi	$2,$7,0x30
.set	noreorder
.set	nomacro
beq	$2,$0,$L520
li	$2,131072			# 0x20000
.set	macro
.set	reorder

lw	$8,10332($fp)
li	$11,16711680			# 0xff0000
lw	$20,%got(ff_interleaved_ue_golomb_vlc_code)($28)
addiu	$16,$2,7456
lw	$13,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
addiu	$2,$2,7712
lw	$21,140($sp)
addu	$16,$fp,$16
li	$10,-16777216			# 0xffffffffff000000
addu	$24,$fp,$2
addiu	$11,$11,255
li	$17,-1434451968			# 0xffffffffaa800000
li	$18,-2147483648			# 0xffffffff80000000
li	$14,9			# 0x9
li	$19,3			# 0x3
move	$15,$16
ori	$10,$10,0xff00
$L576:
move	$12,$0
$L563:
lw	$6,10340($fp)
srl	$2,$6,3
andi	$4,$6,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$11
and	$3,$3,$10
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$4
and	$3,$2,$17
.set	noreorder
.set	nomacro
bne	$3,$0,$L566
srl	$2,$2,24
.set	macro
.set	reorder

addu	$3,$21,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L885
lw	$9,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$7,1			# 0x1
$L567:
addu	$6,$6,$3
$L571:
addiu	$3,$3,-1
$L929:
addu	$2,$9,$2
sra	$3,$3,1
lbu	$2,0($2)
sll	$7,$7,$3
or	$2,$7,$2
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
beq	$2,$0,$L570
sw	$6,10340($fp)
.set	macro
.set	reorder

beq	$2,$18,$L564
$L569:
addiu	$3,$2,1
andi	$2,$2,0x1
sra	$3,$3,1
slt	$4,$3,3
.set	noreorder
.set	nomacro
bne	$4,$0,$L666
addiu	$2,$2,-1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$19,$L667
addiu	$5,$3,9
.set	macro
.set	reorder

andi	$4,$3,0x3
sra	$3,$5,2
subu	$3,$3,$4
$L565:
addu	$12,$12,$4
slt	$4,$12,4
.set	noreorder
.set	nomacro
beq	$4,$0,$L950
lw	$6,%got($LC20)($28)
.set	macro
.set	reorder

lw	$9,%got(chroma_dc_scan)($28)
$L951:
xor	$3,$3,$2
subu	$2,$3,$2
addiu	$9,$9,%lo(chroma_dc_scan)
addu	$4,$9,$12
addiu	$12,$12,1
lbu	$3,0($4)
sll	$3,$3,1
addu	$3,$15,$3
.set	noreorder
.set	nomacro
b	$L563
sh	$2,0($3)
.set	macro
.set	reorder

$L666:
move	$4,$0
addu	$12,$12,$4
slt	$4,$12,4
.set	noreorder
.set	nomacro
bne	$4,$0,$L951
lw	$9,%got(chroma_dc_scan)($28)
.set	macro
.set	reorder

$L564:
lw	$6,%got($LC20)($28)
$L950:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC20)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L566:
addu	$3,$21,$2
addu	$2,$20,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
bne	$2,$0,$L569
sw	$6,10340($fp)
.set	macro
.set	reorder

$L570:
addiu	$15,$15,128
bne	$15,$24,$L576
lw	$25,60($sp)
andi	$2,$25,0x20
.set	noreorder
.set	nomacro
beq	$2,$0,$L946
lw	$9,144($sp)
.set	macro
.set	reorder

lw	$2,%got(svq3_dct_tables)($28)
li	$11,16711680			# 0xff0000
lw	$3,%got(zigzag_scan)($28)
li	$10,-16777216			# 0xffffffffff000000
lw	$22,%got(scan8)($28)
li	$25,1			# 0x1
lw	$21,%got(ff_interleaved_ue_golomb_vlc_code)($28)
addiu	$11,$11,255
lw	$12,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$18,-1434451968			# 0xffffffffaa800000
lw	$13,140($sp)
li	$17,-2147483648			# 0xffffffff80000000
li	$15,9			# 0x9
addiu	$20,$2,%lo(svq3_dct_tables)
addiu	$19,$3,%lo(zigzag_scan)
addiu	$22,$22,%lo(scan8)
ori	$10,$10,0xff00
$L592:
lbu	$2,16($22)
li	$9,1			# 0x1
addu	$2,$fp,$2
sb	$25,11184($2)
$L577:
lw	$5,10340($fp)
srl	$2,$5,3
andi	$4,$5,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$11
and	$3,$3,$10
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$4
and	$3,$2,$18
.set	noreorder
.set	nomacro
bne	$3,$0,$L582
srl	$2,$2,24
.set	macro
.set	reorder

addu	$3,$13,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L887
lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$6,1			# 0x1
$L583:
addu	$5,$5,$3
$L587:
addiu	$3,$3,-1
$L928:
addu	$2,$7,$2
sra	$3,$3,1
lbu	$2,0($2)
sll	$6,$6,$3
or	$2,$6,$2
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
beq	$2,$0,$L586
sw	$5,10340($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$17,$L952
lw	$6,%got($LC21)($28)
.set	macro
.set	reorder

$L585:
addiu	$4,$2,1
andi	$2,$2,0x1
sra	$3,$4,1
slt	$5,$3,16
.set	noreorder
.set	nomacro
bne	$5,$0,$L888
addiu	$2,$2,-1
.set	macro
.set	reorder

andi	$3,$3,0xf
.set	noreorder
.set	nomacro
beq	$3,$0,$L670
sra	$4,$4,5
.set	macro
.set	reorder

slt	$5,$3,3
.set	noreorder
.set	nomacro
bne	$5,$0,$L581
li	$5,2			# 0x2
.set	macro
.set	reorder

slt	$5,$3,10
$L581:
addu	$9,$9,$3
slt	$3,$9,16
.set	noreorder
.set	nomacro
beq	$3,$0,$L578
addu	$4,$4,$5
.set	macro
.set	reorder

$L889:
addu	$5,$19,$9
xor	$3,$4,$2
subu	$2,$3,$2
lbu	$3,0($5)
addiu	$9,$9,1
sll	$3,$3,1
addu	$3,$16,$3
.set	noreorder
.set	nomacro
b	$L577
sh	$2,0($3)
.set	macro
.set	reorder

$L888:
sll	$3,$3,1
addu	$4,$3,$20
lbu	$3,0($4)
addu	$9,$9,$3
slt	$3,$9,16
.set	noreorder
.set	nomacro
bne	$3,$0,$L889
lbu	$4,1($4)
.set	macro
.set	reorder

$L578:
lw	$6,%got($LC21)($28)
$L952:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC21)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L582:
addu	$3,$13,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$5,$3,$5
.set	noreorder
.set	nomacro
bne	$2,$0,$L585
sw	$5,10340($fp)
.set	macro
.set	reorder

$L586:
addiu	$16,$16,32
.set	noreorder
.set	nomacro
bne	$16,$24,$L592
addiu	$22,$22,1
.set	macro
.set	reorder

$L520:
lw	$9,144($sp)
$L946:
li	$3,131072			# 0x20000
lw	$2,2192($fp)
lw	$25,72($sp)
addu	$3,$fp,$3
sll	$4,$9,2
lw	$7,60($sp)
addu	$2,$2,$4
andi	$4,$25,0x7
sw	$7,8736($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L593
sw	$25,0($2)
.set	macro
.set	reorder

lw	$25,%call16(ff_h264_check_intra_pred_mode)($28)
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra_pred_mode
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,40($sp)
sw	$2,10760($fp)
$L593:
lw	$9,136($sp)
.set	noreorder
.set	nomacro
beq	$9,$0,$L612
lw	$25,%call16(ff_h264_hl_decode_mb)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_hl_decode_mb
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,40($sp)
$L612:
lw	$4,2904($fp)
li	$2,3			# 0x3
beq	$4,$2,$L594
lw	$2,10096($fp)
bne	$2,$0,$L594
lw	$3,7996($fp)
lw	$6,168($fp)
lw	$2,7992($fp)
lw	$5,2192($fp)
mul	$7,$3,$6
li	$3,2			# 0x2
addu	$2,$7,$2
sll	$2,$2,2
.set	noreorder
.set	nomacro
beq	$4,$3,$L890
addu	$2,$5,$2
.set	macro
.set	reorder

$L675:
li	$3,-1			# 0xffffffffffffffff
sw	$3,0($2)
$L594:
lw	$2,7992($fp)
lw	$3,160($fp)
addiu	$2,$2,1
slt	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L596
sw	$2,7992($fp)
.set	macro
.set	reorder

lw	$8,7996($fp)
li	$3,131072			# 0x20000
lw	$4,168($fp)
addu	$3,$fp,$3
lw	$5,10344($fp)
mul	$6,$8,$4
addu	$2,$6,$2
sw	$2,9448($3)
lw	$10,10340($fp)
addiu	$2,$10,7
slt	$2,$2,$5
beq	$2,$0,$L343
$L860:
lw	$6,10332($fp)
srl	$3,$10,3
andi	$5,$10,0x7
addu	$3,$6,$3
.set	noreorder
.set	nomacro
b	$L344
addiu	$2,$3,3
.set	macro
.set	reorder

$L890:
lw	$9,136($sp)
slt	$3,$9,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L675
addiu	$3,$9,-1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L594
sw	$3,0($2)
.set	macro
.set	reorder

$L887:
.set	noreorder
.set	nomacro
bne	$3,$15,$L891
addiu	$5,$5,8
.set	macro
.set	reorder

lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$6,1			# 0x1
$L589:
srl	$3,$5,3
andi	$14,$5,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sll	$3,$3,8
srl	$4,$4,8
and	$3,$3,$10
and	$4,$4,$11
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
addu	$23,$12,$2
or	$3,$4,$3
sll	$3,$3,$14
lbu	$4,0($23)
sll	$6,$6,4
srl	$2,$3,24
addu	$3,$13,$2
or	$6,$4,$6
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L583
.set	noreorder
.set	nomacro
beq	$3,$15,$L589
addiu	$5,$5,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L928
addiu	$3,$3,-1
.set	macro
.set	reorder

$L885:
.set	noreorder
.set	nomacro
bne	$3,$14,$L892
addiu	$6,$6,8
.set	macro
.set	reorder

lw	$9,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$7,1			# 0x1
$L573:
srl	$3,$6,3
addu	$2,$13,$2
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$22,$4,8
and	$4,$3,$11
and	$3,$22,$10
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$6,0x7
or	$3,$4,$3
lbu	$4,0($2)
sll	$3,$3,$5
sll	$7,$7,4
srl	$2,$3,24
or	$7,$4,$7
addu	$3,$21,$2
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L567
.set	noreorder
.set	nomacro
beq	$3,$14,$L573
addiu	$6,$6,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L929
addiu	$3,$3,-1
.set	macro
.set	reorder

$L667:
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
b	$L565
li	$4,1			# 0x1
.set	macro
.set	reorder

$L670:
.set	noreorder
.set	nomacro
b	$L581
li	$5,4			# 0x4
.set	macro
.set	reorder

$L503:
lw	$25,%call16(memset)($28)
addiu	$4,$fp,11192
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,36			# 0x24
.set	macro
.set	reorder

li	$4,131072			# 0x20000
lw	$25,3008($fp)
addiu	$4,$4,6944
.set	noreorder
.set	nomacro
jalr	$25
addu	$4,$fp,$4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L608
lw	$28,40($sp)
.set	macro
.set	reorder

$L508:
lw	$6,10340($fp)
li	$11,16711680			# 0xff0000
lw	$12,10332($fp)
li	$10,-16777216			# 0xffffffffff000000
addiu	$11,$11,255
srl	$2,$6,3
ori	$10,$10,0xff00
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$11
and	$3,$3,$10
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$6,0x7
or	$2,$3,$2
sll	$2,$2,$4
li	$3,-1434451968			# 0xffffffffaa800000
and	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L893
lw	$9,140($sp)
.set	macro
.set	reorder

srl	$2,$2,24
addu	$3,$9,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L894
li	$4,9			# 0x9
.set	macro
.set	reorder

lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$8,1			# 0x1
$L510:
addu	$6,$6,$3
$L513:
addiu	$3,$3,-1
$L934:
sw	$6,10340($fp)
addu	$2,$7,$2
sra	$3,$3,1
lbu	$7,0($2)
sll	$8,$8,$3
or	$7,$8,$7
addiu	$7,$7,-1
$L512:
sltu	$2,$7,48
.set	noreorder
.set	nomacro
beq	$2,$0,$L895
lw	$3,72($sp)
.set	macro
.set	reorder

andi	$2,$3,0x7
.set	noreorder
.set	nomacro
beq	$2,$0,$L518
lw	$3,%got(golomb_to_inter_cbp)($28)
.set	macro
.set	reorder

lw	$3,%got(golomb_to_intra4x4_cbp)($28)
lw	$2,2904($fp)
addiu	$3,$3,%lo(golomb_to_intra4x4_cbp)
addu	$7,$7,$3
lbu	$7,0($7)
sw	$7,60($sp)
$L509:
li	$3,1			# 0x1
$L932:
beq	$2,$3,$L652
lw	$2,2892($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L652
lw	$7,60($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$0,$L946
lw	$9,144($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L608
move	$16,$0
.set	macro
.set	reorder

$L346:
li	$2,196608			# 0x30000
lw	$25,%got(svq3_decode_slice_header)($28)
lw	$9,156($sp)
move	$4,$fp
addu	$2,$fp,$2
addiu	$25,$25,%lo(svq3_decode_slice_header)
lw	$2,-15012($2)
sw	$9,10344($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_decode_slice_header
1:	jalr	$25
sw	$2,10340($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L823
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$10,10340($fp)
lw	$6,10332($fp)
lw	$8,7996($fp)
srl	$3,$10,3
andi	$5,$10,0x7
addu	$3,$6,$3
.set	noreorder
.set	nomacro
b	$L344
addiu	$2,$3,3
.set	macro
.set	reorder

$L347:
lw	$25,%got(ff_interleaved_golomb_vlc_len)($28)
srl	$2,$2,24
lw	$3,%got(ff_interleaved_ue_golomb_vlc_code)($28)
lw	$13,2904($fp)
addu	$4,$25,$2
addu	$2,$3,$2
sw	$25,140($sp)
lbu	$3,0($4)
lbu	$2,0($2)
addu	$10,$3,$10
sw	$2,136($sp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$13,$2,$L355
sw	$10,10340($fp)
.set	macro
.set	reorder

lw	$3,136($sp)
$L941:
addiu	$3,$3,8
sw	$3,136($sp)
lw	$25,136($sp)
slt	$2,$25,34
.set	noreorder
.set	nomacro
bne	$2,$0,$L357
move	$23,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L964
lw	$6,%got($LC17)($28)
.set	macro
.set	reorder

$L652:
.set	noreorder
.set	nomacro
b	$L519
move	$16,$0
.set	macro
.set	reorder

$L654:
.set	noreorder
.set	nomacro
b	$L529
li	$5,4			# 0x4
.set	macro
.set	reorder

$L861:
li	$4,9			# 0x9
.set	noreorder
.set	nomacro
bne	$3,$4,$L896
addiu	$10,$10,8
.set	macro
.set	reorder

lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$11,1			# 0x1
lw	$9,140($sp)
li	$14,9			# 0x9
$L353:
srl	$3,$10,3
andi	$5,$10,0x7
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$13
and	$4,$4,$12
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
addu	$15,$7,$2
or	$3,$4,$3
sll	$2,$3,$5
lbu	$4,0($15)
sll	$11,$11,4
srl	$2,$2,24
addu	$3,$9,$2
or	$11,$4,$11
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L348
.set	noreorder
.set	nomacro
beq	$3,$14,$L353
addiu	$10,$10,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L930
addiu	$3,$3,-1
.set	macro
.set	reorder

$L362:
sll	$21,$3,4
.set	noreorder
.set	nomacro
bltz	$21,$L897
sll	$22,$8,4
.set	macro
.set	reorder

lw	$3,180($fp)
addiu	$2,$3,-17
slt	$2,$21,$2
beq	$2,$0,$L898
.set	noreorder
.set	nomacro
bltz	$22,$L368
lw	$2,184($fp)
.set	macro
.set	reorder

addiu	$4,$2,-17
slt	$4,$22,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L368
move	$20,$0
.set	macro
.set	reorder

move	$4,$21
move	$18,$22
move	$19,$21
$L370:
lw	$6,192($fp)
lw	$23,2088($fp)
lw	$5,288($fp)
mul	$3,$22,$6
addu	$2,$3,$21
mul	$3,$6,$18
addu	$23,$23,$2
addu	$7,$3,$4
.set	noreorder
.set	nomacro
bne	$20,$0,$L899
addu	$5,$5,$7
.set	macro
.set	reorder

$L372:
lw	$25,3504($fp)
li	$7,16			# 0x10
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$4,64($fp)
andi	$4,$4,0x2000
.set	noreorder
.set	nomacro
bne	$4,$0,$L378
lw	$28,40($sp)
.set	macro
.set	reorder

slt	$2,$18,$22
sw	$16,72($sp)
slt	$3,$19,$21
addu	$18,$18,$2
sra	$21,$21,1
addu	$19,$19,$3
sra	$18,$18,1
sw	$21,60($sp)
sra	$19,$19,1
sra	$22,$22,1
addiu	$21,$fp,2092
addiu	$23,$fp,2100
move	$16,$18
$L377:
lw	$6,196($fp)
addiu	$21,$21,4
lw	$9,60($sp)
lw	$3,-4($21)
mul	$25,$6,$22
lw	$2,-1804($21)
mul	$4,$16,$6
addu	$18,$25,$9
addu	$5,$4,$19
addu	$18,$3,$18
.set	noreorder
.set	nomacro
bne	$20,$0,$L900
addu	$5,$2,$5
.set	macro
.set	reorder

$L376:
lw	$25,3520($fp)
li	$7,8			# 0x8
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$21,$23,$L377
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$16,72($sp)
$L378:
lw	$2,2904($fp)
li	$3,3			# 0x3
beq	$2,$3,$L901
$L821:
li	$7,2048			# 0x800
sw	$0,60($sp)
li	$18,2048			# 0x800
sw	$7,72($sp)
$L375:
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L500
lw	$25,72($sp)
.set	macro
.set	reorder

andi	$20,$25,0x1
$L619:
move	$21,$0
li	$19,4			# 0x4
$L498:
lw	$2,11864($fp)
move	$5,$0
lw	$4,2184($fp)
li	$6,16			# 0x10
lw	$25,%call16(memset)($28)
mul	$3,$21,$2
addiu	$21,$21,1
addu	$2,$3,$17
sll	$2,$2,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$21,$19,$L498
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$3,2904($fp)
li	$2,3			# 0x3
beq	$3,$2,$L649
bne	$20,$0,$L501
$L500:
lw	$9,144($sp)
sll	$9,$9,2
$L965:
.set	noreorder
.set	nomacro
b	$L607
sw	$9,148($sp)
.set	macro
.set	reorder

$L649:
move	$19,$0
li	$21,4			# 0x4
$L499:
lw	$2,11864($fp)
move	$5,$0
lw	$4,2188($fp)
li	$6,16			# 0x10
lw	$25,%call16(memset)($28)
mul	$3,$19,$2
addiu	$19,$19,1
addu	$2,$3,$17
sll	$2,$2,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$19,$21,$L499
lw	$28,40($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$20,$0,$L501
lw	$9,144($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L965
sll	$9,$9,2
.set	macro
.set	reorder

$L444:
.set	noreorder
.set	nomacro
beq	$9,$2,$L470
li	$2,33			# 0x21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$9,$2,$L781
lw	$3,136($sp)
.set	macro
.set	reorder

lw	$25,%call16(memset)($28)
addiu	$4,$fp,10816
li	$5,-1			# 0xffffffffffffffff
li	$6,40			# 0x28
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$18,$fp,10828
.set	macro
.set	reorder

addiu	$19,$fp,10860
lw	$28,40($sp)
move	$16,$18
$L492:
lw	$25,%call16(memset)($28)
move	$4,$16
li	$5,2			# 0x2
li	$6,4			# 0x4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$16,$16,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$19,$L492
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$25,%call16(ff_h264_write_back_intra_pred_mode)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_write_back_intra_pred_mode
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,40($sp)
$L496:
lw	$25,%call16(memset)($28)
move	$4,$18
li	$5,11			# 0xb
li	$6,4			# 0x4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$18,$18,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$18,$19,$L496
lw	$28,40($sp)
.set	macro
.set	reorder

li	$2,13311			# 0x33ff
lw	$3,2904($fp)
sw	$2,11088($fp)
li	$2,24415			# 0x5f5f
sw	$2,11096($fp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L953
li	$3,1			# 0x1
.set	macro
.set	reorder

$L959:
sw	$0,60($sp)
move	$16,$0
move	$18,$0
li	$20,1			# 0x1
.set	noreorder
.set	nomacro
b	$L619
sw	$3,72($sp)
.set	macro
.set	reorder

$L658:
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
b	$L539
sw	$4,48($sp)
.set	macro
.set	reorder

$L417:
lw	$9,92($sp)
li	$3,4			# 0x4
.set	noreorder
.set	nomacro
beq	$9,$3,$L963
lw	$25,160($sp)
.set	macro
.set	reorder

li	$4,16			# 0x10
andi	$3,$25,0x1
sra	$4,$4,$3
li	$3,2			# 0x2
sra	$5,$4,3
sw	$4,92($sp)
sra	$4,$4,2
subu	$3,$3,$5
sll	$4,$4,2
sll	$12,$3,4
sw	$4,56($sp)
$L419:
li	$3,3			# 0x3
lw	$9,92($sp)
lw	$25,60($sp)
addiu	$4,$fp,3568
subu	$3,$3,$5
lw	$10,180($fp)
lw	$8,184($fp)
addu	$12,$4,$12
sll	$3,$3,4
sw	$0,80($sp)
subu	$5,$10,$9
subu	$6,$8,$25
lw	$25,92($sp)
addu	$3,$4,$3
sw	$12,88($sp)
sll	$14,$5,1
sll	$11,$5,3
sll	$13,$6,1
sw	$3,112($sp)
sll	$7,$6,3
subu	$11,$11,$14
subu	$7,$7,$13
li	$3,131072			# 0x20000
addiu	$11,$11,96
addiu	$7,$7,96
sra	$9,$9,1
addu	$3,$fp,$3
sw	$11,116($sp)
addiu	$25,$25,1
sw	$7,120($sp)
sw	$9,108($sp)
move	$14,$2
sw	$3,64($sp)
sw	$25,128($sp)
$L420:
lw	$9,80($sp)
addiu	$2,$fp,2100
lw	$25,60($sp)
move	$21,$0
sra	$9,$9,2
sw	$2,76($sp)
addiu	$25,$25,1
sw	$9,84($sp)
.set	noreorder
.set	nomacro
b	$L443
sw	$25,124($sp)
.set	macro
.set	reorder

$L910:
.set	noreorder
.set	nomacro
bltz	$22,$L427
addiu	$3,$6,-1
.set	macro
.set	reorder

slt	$3,$22,$3
beq	$3,$0,$L427
move	$5,$23
sw	$0,48($sp)
$L429:
lw	$6,192($fp)
lw	$2,2088($fp)
lw	$3,888($fp)
mul	$7,$19,$6
lw	$9,48($sp)
addu	$4,$7,$18
mul	$7,$6,$22
addu	$2,$2,$4
addu	$5,$7,$5
.set	noreorder
.set	nomacro
bne	$9,$0,$L903
addu	$5,$3,$5
.set	macro
.set	reorder

$L430:
lw	$9,88($sp)
sll	$20,$20,2
lw	$7,60($sp)
addu	$3,$9,$20
lw	$25,0($3)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$2
.set	macro
.set	reorder

lw	$3,64($fp)
andi	$3,$3,0x2000
.set	noreorder
.set	nomacro
bne	$3,$0,$L436
lw	$28,40($sp)
.set	macro
.set	reorder

slt	$3,$22,$19
lw	$9,72($sp)
addu	$22,$22,$3
lw	$25,112($sp)
lw	$3,108($sp)
slt	$4,$23,$18
sra	$18,$18,1
addu	$20,$25,$20
addu	$23,$23,$4
addiu	$3,$3,1
sw	$18,68($sp)
addiu	$9,$9,1
addiu	$18,$fp,2092
sra	$19,$19,1
sw	$3,96($sp)
move	$2,$20
sw	$9,104($sp)
sra	$23,$23,1
sra	$22,$22,1
move	$20,$19
move	$19,$18
move	$18,$fp
move	$fp,$17
move	$17,$16
move	$16,$2
$L435:
lw	$6,196($18)
addiu	$19,$19,4
lw	$2,68($sp)
lw	$3,-1204($19)
mul	$5,$6,$20
lw	$8,-4($19)
addu	$4,$5,$2
mul	$2,$22,$6
addu	$5,$2,$23
addu	$5,$3,$5
lw	$3,48($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L904
addu	$8,$8,$4
.set	macro
.set	reorder

$L434:
lw	$25,0($16)
move	$4,$8
.set	noreorder
.set	nomacro
jalr	$25
lw	$7,72($sp)
.set	macro
.set	reorder

lw	$25,76($sp)
.set	noreorder
.set	nomacro
bne	$19,$25,$L435
lw	$28,40($sp)
.set	macro
.set	reorder

move	$16,$17
move	$17,$fp
move	$fp,$18
$L436:
sll	$4,$17,1
lw	$3,2188($fp)
sll	$5,$16,1
lw	$6,11864($fp)
addu	$17,$4,$17
lw	$25,52($sp)
addu	$16,$5,$16
lw	$2,56($sp)
sll	$17,$17,16
li	$5,4			# 0x4
andi	$16,$16,0xffff
addu	$3,$3,$25
addu	$16,$16,$17
.set	noreorder
.set	nomacro
beq	$2,$5,$L905
sll	$4,$6,2
.set	macro
.set	reorder

lw	$9,56($sp)
li	$5,8			# 0x8
.set	noreorder
.set	nomacro
beq	$9,$5,$L906
li	$5,16			# 0x10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$5,$L907
lw	$9,100($sp)
.set	macro
.set	reorder

$L438:
lw	$25,92($sp)
addu	$21,$21,$25
slt	$3,$21,16
.set	noreorder
.set	nomacro
beq	$3,$0,$L954
lw	$2,80($sp)
.set	macro
.set	reorder

$L441:
lw	$10,180($fp)
lw	$8,184($fp)
lw	$2,92($sp)
lw	$3,60($sp)
lw	$14,11864($fp)
subu	$5,$10,$2
subu	$6,$8,$3
$L443:
lw	$19,7996($fp)
sra	$4,$21,2
lw	$2,84($sp)
lw	$18,7992($fp)
sll	$7,$19,2
lw	$9,984($fp)
lw	$25,64($sp)
sll	$19,$19,4
addu	$7,$7,$2
sll	$12,$18,2
mul	$2,$7,$14
addu	$4,$12,$4
lw	$11,10696($25)
lw	$3,10692($25)
sll	$18,$18,4
lw	$25,80($sp)
addu	$18,$21,$18
subu	$3,$3,$11
sll	$16,$18,1
addu	$4,$2,$4
sll	$7,$18,3
sll	$4,$4,2
subu	$7,$16,$7
addu	$9,$9,$4
sw	$4,52($sp)
addiu	$16,$7,-96
addu	$19,$25,$19
lh	$4,0($9)
lh	$9,2($9)
sll	$4,$4,1
sll	$9,$9,1
mul	$4,$4,$3
mul	$3,$9,$3
teq	$11,$0,7
div	$0,$4,$11
mflo	$4
teq	$11,$0,7
div	$0,$3,$11
addiu	$4,$4,1
sra	$4,$4,1
slt	$9,$4,$16
mflo	$3
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$9,$0,$L421
sra	$3,$3,1
.set	macro
.set	reorder

lw	$2,116($sp)
addu	$7,$2,$7
slt	$16,$4,$7
movz	$4,$7,$16
move	$16,$4
$L421:
sll	$17,$19,3
sll	$4,$19,1
subu	$4,$4,$17
addiu	$17,$4,-96
slt	$7,$3,$17
.set	noreorder
.set	nomacro
bne	$7,$0,$L955
li	$2,-1431699456			# 0xffffffffaaaa0000
.set	macro
.set	reorder

lw	$9,120($sp)
addu	$17,$9,$4
slt	$4,$3,$17
movn	$17,$3,$4
$L955:
addiu	$16,$16,12289
ori	$2,$2,0xaaab
multu	$16,$2
addiu	$17,$17,12289
mfhi	$16
multu	$17,$2
mfhi	$17
srl	$16,$16,1
addiu	$16,$16,-4096
sra	$23,$16,1
srl	$17,$17,1
addu	$23,$18,$23
addiu	$17,$17,-4096
andi	$20,$17,0x1
sll	$3,$20,1
sra	$22,$17,1
andi	$20,$16,0x1
addu	$20,$20,$3
.set	noreorder
.set	nomacro
bltz	$23,$L909
addu	$22,$19,$22
.set	macro
.set	reorder

addiu	$3,$5,-1
slt	$3,$23,$3
bne	$3,$0,$L910
$L427:
lw	$2,64($fp)
srl	$2,$2,14
andi	$2,$2,0x1
sw	$2,48($sp)
$L425:
addiu	$5,$5,15
slt	$3,$5,$23
movn	$23,$5,$3
move	$5,$23
$L424:
slt	$3,$22,-16
.set	noreorder
.set	nomacro
bne	$3,$0,$L637
addiu	$6,$6,15
.set	macro
.set	reorder

lw	$2,2088($fp)
lw	$9,48($sp)
slt	$3,$6,$22
movn	$22,$6,$3
lw	$6,192($fp)
lw	$3,888($fp)
mul	$7,$19,$6
addu	$4,$7,$18
mul	$7,$6,$22
addu	$2,$2,$4
addu	$5,$7,$5
.set	noreorder
.set	nomacro
beq	$9,$0,$L430
addu	$5,$3,$5
.set	macro
.set	reorder

$L903:
lw	$3,124($sp)
lw	$4,2856($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,128($sp)
sw	$2,172($sp)
sw	$3,16($sp)
sw	$23,20($sp)
sw	$22,24($sp)
sw	$10,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$8,32($sp)
.set	macro
.set	reorder

lw	$5,2856($fp)
lw	$6,192($fp)
.set	noreorder
.set	nomacro
b	$L430
lw	$2,172($sp)
.set	macro
.set	reorder

$L637:
.set	noreorder
.set	nomacro
b	$L429
li	$22,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L904:
lw	$9,104($sp)
lw	$4,2856($18)
sw	$23,20($sp)
sw	$22,24($sp)
sw	$9,16($sp)
lw	$3,180($18)
lw	$25,%call16(ff_emulated_edge_mc)($28)
lw	$7,96($sp)
sra	$3,$3,1
sw	$3,28($sp)
lw	$3,184($18)
sw	$8,172($sp)
sra	$3,$3,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$5,2856($18)
lw	$6,196($18)
.set	noreorder
.set	nomacro
b	$L434
lw	$8,172($sp)
.set	macro
.set	reorder

$L905:
lw	$2,100($sp)
li	$5,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$5,$L438
sw	$16,0($3)
.set	macro
.set	reorder

addu	$3,$3,$4
li	$5,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$5,$L438
sw	$16,0($3)
.set	macro
.set	reorder

lw	$25,92($sp)
addu	$3,$3,$4
addu	$4,$3,$4
sw	$16,0($3)
addu	$21,$21,$25
slt	$3,$21,16
.set	noreorder
.set	nomacro
bne	$3,$0,$L441
sw	$16,0($4)
.set	macro
.set	reorder

lw	$2,80($sp)
$L954:
lw	$3,60($sp)
addu	$2,$2,$3
sw	$2,80($sp)
slt	$2,$2,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L956
li	$7,8			# 0x8
.set	macro
.set	reorder

lw	$10,180($fp)
lw	$8,184($fp)
lw	$14,11864($fp)
subu	$5,$10,$25
.set	noreorder
.set	nomacro
b	$L420
subu	$6,$8,$3
.set	macro
.set	reorder

$L909:
lw	$2,64($fp)
slt	$3,$23,-16
srl	$2,$2,14
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$3,$0,$L425
sw	$2,48($sp)
.set	macro
.set	reorder

li	$5,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L424
li	$23,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L906:
lw	$25,100($sp)
li	$5,1			# 0x1
sw	$16,0($3)
.set	noreorder
.set	nomacro
beq	$25,$5,$L438
sw	$16,4($3)
.set	macro
.set	reorder

addu	$5,$3,$4
li	$7,2			# 0x2
sw	$16,0($5)
.set	noreorder
.set	nomacro
beq	$25,$7,$L438
sw	$16,4($5)
.set	macro
.set	reorder

sll	$6,$6,3
addu	$5,$5,$4
addu	$7,$6,$4
sw	$16,0($5)
addu	$6,$3,$6
addu	$5,$5,$4
addu	$3,$3,$7
sw	$16,4($6)
sw	$16,0($5)
.set	noreorder
.set	nomacro
b	$L438
sw	$16,4($3)
.set	macro
.set	reorder

$L907:
addu	$5,$3,$4
li	$7,2			# 0x2
sw	$16,0($3)
sw	$16,4($3)
sw	$16,8($3)
sw	$16,12($3)
sw	$16,0($5)
sw	$16,4($5)
sw	$16,8($5)
.set	noreorder
.set	nomacro
beq	$9,$7,$L438
sw	$16,12($5)
.set	macro
.set	reorder

sll	$6,$6,3
addu	$5,$5,$4
addu	$7,$6,$4
sw	$16,0($5)
addu	$6,$3,$6
addu	$5,$5,$4
addu	$3,$3,$7
sw	$16,4($6)
sw	$16,8($6)
sw	$16,12($6)
sw	$16,0($5)
sw	$16,4($3)
sw	$16,8($3)
.set	noreorder
.set	nomacro
b	$L438
sw	$16,12($3)
.set	macro
.set	reorder

$L863:
slt	$2,$9,4
.set	noreorder
.set	nomacro
bne	$2,$0,$L357
addiu	$9,$9,4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L356
sw	$9,136($sp)
.set	macro
.set	reorder

$L898:
lw	$2,184($fp)
$L368:
lw	$20,64($fp)
srl	$20,$20,14
andi	$20,$20,0x1
$L366:
addiu	$3,$3,-1
slt	$19,$21,$3
movn	$3,$21,$19
move	$19,$3
move	$4,$3
$L365:
slt	$3,$22,-16
.set	noreorder
.set	nomacro
beq	$3,$0,$L371
addiu	$2,$2,-1
.set	macro
.set	reorder

lw	$6,192($fp)
li	$18,-16			# 0xfffffffffffffff0
lw	$23,2088($fp)
lw	$5,288($fp)
mul	$3,$22,$6
addu	$2,$3,$21
mul	$3,$6,$18
addu	$23,$23,$2
addu	$7,$3,$4
.set	noreorder
.set	nomacro
beq	$20,$0,$L372
addu	$5,$5,$7
.set	macro
.set	reorder

$L899:
li	$2,17			# 0x11
lw	$4,2856($fp)
sw	$19,20($sp)
li	$7,17			# 0x11
sw	$18,24($sp)
sw	$2,16($sp)
lw	$2,180($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$2,28($sp)
lw	$2,184($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

lw	$5,2856($fp)
.set	noreorder
.set	nomacro
b	$L372
lw	$6,192($fp)
.set	macro
.set	reorder

$L448:
sw	$0,11292($20)
sw	$0,11324($20)
sw	$0,11356($20)
sw	$0,11388($20)
lw	$2,7996($fp)
.set	noreorder
.set	nomacro
bgtz	$2,$L911
lw	$25,%call16(memset)($28)
.set	macro
.set	reorder

$L943:
addiu	$4,$16,11571
li	$5,-2			# 0xfffffffffffffffe
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,8			# 0x8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L459
lw	$28,40($sp)
.set	macro
.set	reorder

$L872:
li	$7,1			# 0x1
lw	$3,7992($fp)
.set	noreorder
.set	nomacro
b	$L462
sw	$7,60($sp)
.set	macro
.set	reorder

$L875:
srl	$2,$4,24
lw	$3,%got(ff_interleaved_se_golomb_vlc_code)($28)
addu	$4,$9,$2
addu	$2,$3,$2
lbu	$3,0($4)
lb	$2,0($2)
addu	$5,$3,$5
.set	noreorder
.set	nomacro
b	$L522
sw	$5,10340($fp)
.set	macro
.set	reorder

$L874:
addiu	$4,$fp,11192
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,36			# 0x24
.set	macro
.set	reorder

li	$4,131072			# 0x20000
lw	$25,3008($fp)
addiu	$4,$4,6944
.set	noreorder
.set	nomacro
jalr	$25
addu	$4,$fp,$4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L608
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$2,2904($fp)
.set	noreorder
.set	nomacro
beq	$2,$17,$L508
li	$3,1			# 0x1
.set	macro
.set	reorder

b	$L932
$L454:
.set	noreorder
.set	nomacro
bgtz	$4,$L912
sb	$18,11576($16)
.set	macro
.set	reorder

li	$2,-2			# 0xfffffffffffffffe
$L944:
.set	noreorder
.set	nomacro
b	$L459
sb	$2,11571($16)
.set	macro
.set	reorder

$L518:
lw	$2,2904($fp)
addiu	$3,$3,%lo(golomb_to_inter_cbp)
addu	$7,$7,$3
lbu	$7,0($7)
.set	noreorder
.set	nomacro
b	$L509
sw	$7,60($sp)
.set	macro
.set	reorder

$L873:
move	$5,$0
move	$7,$0
sw	$0,16($sp)
move	$4,$fp
addiu	$16,$16,%lo(svq3_mc_dir)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,svq3_mc_dir
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L810
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$3,136($sp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L957
lw	$9,136($sp)
.set	macro
.set	reorder

move	$16,$0
li	$18,4			# 0x4
$L469:
lw	$2,11864($fp)
move	$5,$0
lw	$4,2188($fp)
li	$6,16			# 0x10
lw	$25,%call16(memset)($28)
mul	$3,$16,$2
addiu	$16,$16,1
addu	$2,$3,$17
sll	$2,$2,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$18,$L469
lw	$28,40($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L933
addiu	$19,$19,4
.set	macro
.set	reorder

$L445:
lw	$4,10340($fp)
lw	$7,-15024($2)
srl	$8,$4,3
andi	$10,$4,0x7
addu	$8,$6,$8
addiu	$4,$4,1
lbu	$2,0($8)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$7,$2,$L446
sw	$4,10340($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L447
li	$18,3			# 0x3
.set	macro
.set	reorder

$L371:
slt	$18,$22,$2
movn	$2,$22,$18
.set	noreorder
.set	nomacro
b	$L370
move	$18,$2
.set	macro
.set	reorder

$L781:
move	$4,$fp
lw	$16,%got(i_mb_type_info)($28)
lw	$25,%call16(ff_h264_check_intra_pred_mode)($28)
addiu	$2,$3,-8
addiu	$16,$16,%lo(i_mb_type_info)
sll	$2,$2,2
addu	$16,$2,$16
lbu	$2,2($16)
andi	$5,$2,0x1
sll	$3,$5,1
sra	$2,$2,1
addu	$5,$3,$5
xori	$2,$2,0x1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra_pred_mode
1:	jalr	$25
xor	$5,$2,$5
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
lw	$28,40($sp)
.set	noreorder
.set	nomacro
beq	$2,$3,$L913
sw	$2,10764($fp)
.set	macro
.set	reorder

lbu	$16,3($16)
li	$7,2			# 0x2
lw	$2,2904($fp)
move	$18,$0
sw	$7,72($sp)
sw	$16,60($sp)
.set	noreorder
.set	nomacro
b	$L375
li	$16,2			# 0x2
.set	macro
.set	reorder

$L894:
.set	noreorder
.set	nomacro
bne	$3,$4,$L914
addiu	$6,$6,8
.set	macro
.set	reorder

lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$8,1			# 0x1
lw	$9,140($sp)
li	$13,9			# 0x9
$L515:
srl	$3,$6,3
andi	$5,$6,0x7
addu	$3,$12,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$11
and	$4,$4,$10
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
addu	$14,$7,$2
or	$3,$4,$3
sll	$2,$3,$5
lbu	$4,0($14)
sll	$8,$8,4
srl	$2,$2,24
addu	$3,$9,$2
or	$8,$4,$8
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L510
.set	noreorder
.set	nomacro
beq	$3,$13,$L515
addiu	$6,$6,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L934
addiu	$3,$3,-1
.set	macro
.set	reorder

$L900:
li	$7,9			# 0x9
lw	$4,2856($fp)
sw	$19,20($sp)
sw	$16,24($sp)
sw	$7,16($sp)
lw	$2,180($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sra	$2,$2,1
sw	$2,28($sp)
lw	$2,184($fp)
sra	$2,$2,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

lw	$5,2856($fp)
.set	noreorder
.set	nomacro
b	$L376
lw	$6,196($fp)
.set	macro
.set	reorder

$L893:
lw	$25,140($sp)
srl	$2,$2,24
lw	$3,%got(ff_interleaved_ue_golomb_vlc_code)($28)
addu	$4,$25,$2
addu	$2,$3,$2
lbu	$3,0($4)
lbu	$7,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
b	$L512
sw	$6,10340($fp)
.set	macro
.set	reorder

$L897:
lw	$20,64($fp)
slt	$4,$21,-16
lw	$3,180($fp)
lw	$2,184($fp)
srl	$20,$20,14
.set	noreorder
.set	nomacro
beq	$4,$0,$L366
andi	$20,$20,0x1
.set	macro
.set	reorder

li	$4,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L365
li	$19,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L856:
lw	$25,%call16(av_get_pict_type_char)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_get_pict_type_char
1:	jalr	$25
lw	$18,0($23)
.set	macro
.set	reorder

li	$3,196608			# 0x30000
move	$7,$2
lw	$28,40($sp)
addu	$3,$23,$3
li	$5,48			# 0x30
move	$4,$18
lw	$8,-15024($3)
lw	$6,%got($LC10)($28)
lw	$25,%call16(av_log)($28)
sw	$8,16($sp)
addiu	$6,$6,%lo($LC10)
lw	$2,-15020($3)
sw	$2,20($sp)
lw	$2,2892($23)
sw	$2,24($sp)
lw	$2,2872($23)
sw	$2,28($sp)
lw	$2,-5272($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

lw	$4,2904($23)
lw	$3,2688($23)
lw	$28,40($sp)
xori	$2,$4,0x1
sltu	$2,$2,1
sw	$4,2140($23)
.set	noreorder
.set	nomacro
bne	$3,$0,$L321
sw	$2,2136($23)
.set	macro
.set	reorder

$L857:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$4,$2,$L770
move	$2,$0
.set	macro
.set	reorder

lw	$2,128($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L958
slt	$2,$2,5
.set	macro
.set	reorder

lw	$2,708($16)
slt	$3,$2,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L939
slt	$3,$2,32
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L935
lw	$2,9812($23)
.set	macro
.set	reorder

$L460:
li	$3,2			# 0x2
lw	$18,72($sp)
.set	noreorder
.set	nomacro
bne	$2,$3,$L461
move	$17,$23
.set	macro
.set	reorder

lw	$16,%got(svq3_mc_dir)($28)
move	$7,$0
lw	$9,136($sp)
move	$4,$fp
sw	$0,16($sp)
addiu	$16,$16,%lo(svq3_mc_dir)
.set	noreorder
.set	nomacro
b	$L825
addiu	$5,$9,-1
.set	macro
.set	reorder

$L470:
lw	$25,%call16(memset)($28)
addiu	$4,$fp,10816
li	$5,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,40			# 0x28
.set	macro
.set	reorder

lw	$2,7992($fp)
.set	noreorder
.set	nomacro
blez	$2,$L475
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$3,144($sp)
addiu	$5,$fp,10827
lw	$4,11856($fp)
li	$6,4			# 0x4
lw	$7,10856($fp)
sll	$2,$3,2
move	$3,$0
addiu	$2,$2,-4
addu	$4,$4,$2
$L473:
lw	$2,0($4)
addiu	$5,$5,8
addiu	$2,$2,6
subu	$2,$2,$3
addiu	$3,$3,1
addu	$2,$7,$2
lb	$2,0($2)
.set	noreorder
.set	nomacro
bne	$3,$6,$L473
sb	$2,-8($5)
.set	macro
.set	reorder

lb	$3,10827($fp)
li	$2,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
beq	$3,$2,$L915
li	$2,24415			# 0x5f5f
.set	macro
.set	reorder

$L475:
lw	$2,7996($fp)
.set	noreorder
.set	nomacro
blez	$2,$L477
lw	$25,144($sp)
.set	macro
.set	reorder

lw	$4,168($fp)
lw	$3,11856($fp)
lw	$2,10856($fp)
subu	$4,$25,$4
sll	$4,$4,2
addu	$3,$3,$4
lw	$4,0($3)
addu	$4,$2,$4
lb	$5,0($4)
sb	$5,10820($fp)
lw	$4,0($3)
addu	$4,$2,$4
lb	$4,1($4)
sb	$4,10821($fp)
lw	$4,0($3)
addu	$4,$2,$4
lb	$4,2($4)
sb	$4,10822($fp)
lw	$3,0($3)
addu	$2,$2,$3
lb	$2,3($2)
sb	$2,10823($fp)
li	$2,-1			# 0xffffffffffffffff
beq	$5,$2,$L916
$L477:
lw	$16,%got(scan8+16)($28)
li	$11,16711680			# 0xff0000
lw	$14,%got(svq3_pred_0)($28)
li	$10,-16777216			# 0xffffffffff000000
lw	$13,%got(svq3_pred_1)($28)
addiu	$11,$11,255
lw	$12,%got(scan8)($28)
addiu	$16,$16,%lo(scan8+16)
lw	$21,%got(ff_interleaved_ue_golomb_vlc_code)($28)
li	$20,-1434451968			# 0xffffffffaa800000
lw	$15,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$19,9			# 0x9
lw	$8,140($sp)
addiu	$14,$14,%lo(svq3_pred_0)
addiu	$13,$13,%lo(svq3_pred_1)
li	$18,-1			# 0xffffffffffffffff
addiu	$12,$12,%lo(scan8)
ori	$10,$10,0xff00
$L490:
lw	$6,10340($fp)
lw	$9,10332($fp)
srl	$2,$6,3
andi	$5,$6,0x7
addu	$2,$9,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$11
and	$4,$4,$10
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$5
and	$3,$2,$20
.set	noreorder
.set	nomacro
bne	$3,$0,$L479
srl	$2,$2,24
.set	macro
.set	reorder

addu	$3,$8,$2
lbu	$3,0($3)
sltu	$4,$3,9
.set	noreorder
.set	nomacro
beq	$4,$0,$L917
lw	$22,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	macro
.set	reorder

li	$7,1			# 0x1
$L480:
addu	$6,$6,$3
$L483:
addiu	$3,$3,-1
$L936:
sw	$6,10340($fp)
addu	$2,$22,$2
sra	$3,$3,1
lbu	$2,0($2)
sll	$7,$7,$3
or	$7,$7,$2
addiu	$7,$7,-1
$L482:
sltu	$2,$7,25
.set	noreorder
.set	nomacro
beq	$2,$0,$L918
sll	$2,$7,1
.set	macro
.set	reorder

lbu	$9,0($12)
addu	$2,$2,$14
addu	$4,$fp,$9
addiu	$5,$9,10815
move	$3,$4
lbu	$7,0($2)
lb	$4,10815($4)
move	$9,$3
lb	$22,10808($3)
addu	$5,$fp,$5
lbu	$6,1($2)
addiu	$23,$4,1
addiu	$2,$22,1
sll	$4,$23,2
sll	$3,$2,1
sll	$2,$2,5
addu	$4,$4,$23
subu	$3,$2,$3
addu	$3,$4,$3
addu	$3,$3,$13
addu	$3,$3,$7
lb	$7,0($3)
addiu	$2,$7,1
sb	$7,1($5)
lb	$4,10809($9)
sll	$3,$2,2
addu	$3,$3,$2
addiu	$4,$4,1
sll	$2,$4,1
sll	$4,$4,5
subu	$2,$4,$2
addu	$2,$3,$2
addu	$2,$2,$13
addu	$2,$2,$6
lb	$2,0($2)
.set	noreorder
.set	nomacro
beq	$7,$18,$L488
sb	$2,2($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$18,$L488
addiu	$12,$12,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$12,$16,$L490
lw	$25,%call16(ff_h264_write_back_intra_pred_mode)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_write_back_intra_pred_mode
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,40($sp)
lw	$25,%call16(ff_h264_check_intra4x4_pred_mode)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_check_intra4x4_pred_mode
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

li	$2,65535			# 0xffff
lw	$4,7996($fp)
li	$5,13311			# 0x33ff
lw	$3,7992($fp)
lw	$28,40($sp)
movn	$5,$2,$4
move	$4,$5
li	$5,24415			# 0x5f5f
sw	$4,11088($fp)
movz	$2,$5,$3
lw	$3,2904($fp)
sw	$2,11096($fp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L959
li	$3,1			# 0x1
.set	macro
.set	reorder

$L953:
lw	$25,%call16(memset)($28)
addiu	$4,$fp,11192
move	$5,$0
li	$6,36			# 0x24
sw	$3,72($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$20,1			# 0x1
.set	macro
.set	reorder

li	$4,131072			# 0x20000
lw	$25,3008($fp)
addiu	$4,$4,6944
.set	noreorder
.set	nomacro
jalr	$25
addu	$4,$fp,$4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L508
lw	$28,40($sp)
.set	macro
.set	reorder

$L917:
.set	noreorder
.set	nomacro
bne	$3,$19,$L919
addiu	$6,$6,8
.set	macro
.set	reorder

lw	$22,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
li	$7,1			# 0x1
$L485:
srl	$3,$6,3
andi	$5,$6,0x7
addu	$3,$9,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$11
and	$4,$4,$10
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
addu	$23,$15,$2
or	$3,$4,$3
sll	$2,$3,$5
lbu	$4,0($23)
sll	$7,$7,4
srl	$2,$2,24
addu	$3,$8,$2
or	$7,$4,$7
lbu	$3,0($3)
sltu	$4,$3,9
bne	$4,$0,$L480
.set	noreorder
.set	nomacro
beq	$3,$19,$L485
addiu	$6,$6,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L936
addiu	$3,$3,-1
.set	macro
.set	reorder

$L479:
addu	$3,$8,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$7,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
b	$L482
sw	$6,10340($fp)
.set	macro
.set	reorder

$L632:
li	$25,4			# 0x4
li	$9,4			# 0x4
move	$13,$0
sw	$25,56($sp)
li	$12,32			# 0x20
.set	noreorder
.set	nomacro
b	$L391
sw	$9,64($sp)
.set	macro
.set	reorder

$L963:
move	$5,$0
li	$12,32			# 0x20
.set	noreorder
.set	nomacro
b	$L419
sw	$3,56($sp)
.set	macro
.set	reorder

$L642:
li	$17,-2			# 0xfffffffffffffffe
sb	$17,11576($16)
.set	noreorder
.set	nomacro
b	$L456
lw	$4,7992($fp)
.set	macro
.set	reorder

$L901:
lw	$21,7992($fp)
lw	$22,7996($fp)
sll	$21,$21,4
.set	noreorder
.set	nomacro
bltz	$21,$L920
sll	$22,$22,4
.set	macro
.set	reorder

lw	$3,180($fp)
addiu	$2,$3,-17
slt	$2,$21,$2
beq	$2,$0,$L921
.set	noreorder
.set	nomacro
bltz	$22,$L383
lw	$2,184($fp)
.set	macro
.set	reorder

addiu	$4,$2,-17
slt	$4,$22,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L383
move	$20,$0
.set	macro
.set	reorder

move	$4,$21
move	$18,$22
move	$19,$21
$L385:
lw	$6,192($fp)
lw	$23,2088($fp)
lw	$5,888($fp)
mul	$3,$22,$6
addu	$2,$3,$21
mul	$3,$6,$18
addu	$23,$23,$2
addu	$7,$3,$4
.set	noreorder
.set	nomacro
bne	$20,$0,$L922
addu	$5,$5,$7
.set	macro
.set	reorder

$L387:
lw	$25,3568($fp)
li	$7,16			# 0x10
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$4,64($fp)
andi	$4,$4,0x2000
.set	noreorder
.set	nomacro
bne	$4,$0,$L822
lw	$28,40($sp)
.set	macro
.set	reorder

slt	$2,$18,$22
sw	$16,72($sp)
slt	$3,$19,$21
addu	$18,$18,$2
sra	$21,$21,1
addu	$19,$19,$3
sra	$18,$18,1
sw	$21,60($sp)
sra	$19,$19,1
sra	$22,$22,1
addiu	$21,$fp,2092
addiu	$23,$fp,2100
move	$16,$18
$L390:
lw	$6,196($fp)
addiu	$21,$21,4
lw	$9,60($sp)
lw	$3,-4($21)
mul	$25,$6,$22
lw	$2,-1204($21)
mul	$4,$16,$6
addu	$18,$25,$9
addu	$5,$4,$19
addu	$18,$3,$18
.set	noreorder
.set	nomacro
bne	$20,$0,$L923
addu	$5,$2,$5
.set	macro
.set	reorder

$L389:
lw	$25,3584($fp)
li	$7,8			# 0x8
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$21,$23,$L390
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$16,72($sp)
$L822:
.set	noreorder
.set	nomacro
b	$L821
lw	$2,2904($fp)
.set	macro
.set	reorder

$L921:
lw	$2,184($fp)
$L383:
lw	$20,64($fp)
srl	$20,$20,14
andi	$20,$20,0x1
$L381:
addiu	$3,$3,-1
slt	$19,$21,$3
movn	$3,$21,$19
move	$19,$3
move	$4,$3
$L380:
slt	$3,$22,-16
.set	noreorder
.set	nomacro
bne	$3,$0,$L924
addiu	$2,$2,-1
.set	macro
.set	reorder

slt	$18,$22,$2
movn	$2,$22,$18
.set	noreorder
.set	nomacro
b	$L385
move	$18,$2
.set	macro
.set	reorder

$L924:
.set	noreorder
.set	nomacro
b	$L385
li	$18,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L596:
lw	$8,7996($fp)
move	$23,$fp
$L342:
lw	$25,%call16(ff_draw_horiz_band)($28)
sll	$5,$8,4
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_draw_horiz_band
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$8,7996($23)
lw	$2,164($23)
lw	$28,40($sp)
addiu	$8,$8,1
slt	$2,$8,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L719
sw	$8,7996($23)
.set	macro
.set	reorder

$L598:
lw	$25,%call16(MPV_frame_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_frame_end
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

li	$2,3			# 0x3
lw	$3,2904($23)
.set	noreorder
.set	nomacro
beq	$3,$2,$L960
lw	$3,164($sp)
.set	macro
.set	reorder

lw	$2,10096($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L599
addiu	$2,$23,288
.set	macro
.set	reorder

lw	$3,164($sp)
$L960:
addiu	$2,$23,2088
addiu	$4,$23,2296
$L600:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L600
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
$L938:
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
lw	$2,2688($23)
.set	noreorder
.set	nomacro
beq	$2,$0,$L925
li	$3,216			# 0xd8
.set	macro
.set	reorder

$L962:
lw	$25,168($sp)
lw	$2,152($sp)
.set	noreorder
.set	nomacro
b	$L770
sw	$3,0($25)
.set	macro
.set	reorder

$L876:
lw	$6,%got($LC16)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L922:
li	$2,17			# 0x11
lw	$4,2856($fp)
sw	$19,20($sp)
li	$7,17			# 0x11
sw	$18,24($sp)
sw	$2,16($sp)
lw	$2,180($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$2,28($sp)
lw	$2,184($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

lw	$5,2856($fp)
.set	noreorder
.set	nomacro
b	$L387
lw	$6,192($fp)
.set	macro
.set	reorder

$L884:
lw	$14,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L534
li	$7,1			# 0x1
.set	macro
.set	reorder

$L920:
lw	$20,64($fp)
slt	$4,$21,-16
lw	$3,180($fp)
lw	$2,184($fp)
srl	$20,$20,14
.set	noreorder
.set	nomacro
beq	$4,$0,$L381
andi	$20,$20,0x1
.set	macro
.set	reorder

li	$4,-16			# 0xfffffffffffffff0
.set	noreorder
.set	nomacro
b	$L380
li	$19,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L923:
li	$7,9			# 0x9
lw	$4,2856($fp)
sw	$19,20($sp)
sw	$16,24($sp)
sw	$7,16($sp)
lw	$2,180($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sra	$2,$2,1
sw	$2,28($sp)
lw	$2,184($fp)
sra	$2,$2,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

lw	$5,2856($fp)
.set	noreorder
.set	nomacro
b	$L389
lw	$6,196($fp)
.set	macro
.set	reorder

$L896:
lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L351
li	$11,1			# 0x1
.set	macro
.set	reorder

$L653:
.set	noreorder
.set	nomacro
b	$L522
li	$2,-2147483648			# 0xffffffff80000000
.set	macro
.set	reorder

$L915:
.set	noreorder
.set	nomacro
b	$L475
sw	$2,11096($fp)
.set	macro
.set	reorder

$L914:
lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L513
li	$8,1			# 0x1
.set	macro
.set	reorder

$L891:
lw	$7,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L587
li	$6,1			# 0x1
.set	macro
.set	reorder

$L892:
lw	$9,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L571
li	$7,1			# 0x1
.set	macro
.set	reorder

$L858:
li	$2,65536			# 0x10000
li	$3,131072			# 0x20000
addu	$2,$23,$2
addu	$3,$23,$3
lw	$2,-5272($2)
lw	$4,10700($3)
subu	$2,$2,$4
.set	noreorder
.set	nomacro
bgez	$2,$L330
sw	$2,10692($3)
.set	macro
.set	reorder

addiu	$2,$2,256
sw	$2,10692($3)
$L330:
.set	noreorder
.set	nomacro
beq	$2,$0,$L331
li	$3,131072			# 0x20000
.set	macro
.set	reorder

addu	$3,$23,$3
lw	$3,10696($3)
slt	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L961
addiu	$11,$23,80
.set	macro
.set	reorder

$L331:
lw	$6,%got($LC11)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L770
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L859:
addiu	$3,$3,256
.set	noreorder
.set	nomacro
b	$L332
sw	$3,10696($2)
.set	macro
.set	reorder

$L326:
lw	$2,9812($23)
.set	noreorder
.set	nomacro
bne	$2,$0,$L616
lw	$25,%call16(ff_h264_frame_start)($28)
.set	macro
.set	reorder

b	$L937
$L599:
addiu	$4,$23,496
$L602:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L602
sw	$5,-4($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L938
lw	$4,0($2)
.set	macro
.set	reorder

$L925:
lw	$2,10096($23)
bne	$2,$0,$L962
.set	noreorder
.set	nomacro
b	$L770
lw	$2,152($sp)
.set	macro
.set	reorder

$L895:
lw	$6,%got($LC15)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC15)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L810:
.set	noreorder
.set	nomacro
b	$L464
move	$23,$fp
.set	macro
.set	reorder

$L882:
lw	$16,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L555
li	$7,1			# 0x1
.set	macro
.set	reorder

$L913:
lw	$6,%got($LC14)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L488:
lw	$6,%got($LC13)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L918:
lw	$6,%got($LC12)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$23,$fp
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC12)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L464
lw	$28,40($sp)
.set	macro
.set	reorder

$L916:
li	$2,13311			# 0x33ff
.set	noreorder
.set	nomacro
b	$L477
sw	$2,11088($fp)
.set	macro
.set	reorder

$L919:
lw	$22,%got(ff_interleaved_dirac_golomb_vlc_code)($28)
.set	noreorder
.set	nomacro
b	$L483
li	$7,1			# 0x1
.set	macro
.set	reorder

.end	svq3_decode_frame
.size	svq3_decode_frame, .-svq3_decode_frame
.section	.text.ff_svq3_luma_dc_dequant_idct_c,"ax",@progbits
.align	2
.align	5
.globl	ff_svq3_luma_dc_dequant_idct_c
.set	nomips16
.set	nomicromips
.ent	ff_svq3_luma_dc_dequant_idct_c
.type	ff_svq3_luma_dc_dequant_idct_c, @function
ff_svq3_luma_dc_dequant_idct_c:
.frame	$sp,88,$31		# vars= 64, regs= 3/0, args= 0, gp= 8
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(svq3_dequant_coeff)($28)
sll	$5,$5,2
addiu	$sp,$sp,-88
lw	$13,%got(y_offset.8543)($28)
addiu	$2,$2,%lo(svq3_dequant_coeff)
.cprestore	0
addiu	$7,$sp,8
sw	$18,84($sp)
addu	$5,$5,$2
sw	$17,80($sp)
addiu	$14,$sp,72
sw	$16,76($sp)
addiu	$13,$13,%lo(y_offset.8543)
lw	$8,0($5)
move	$6,$7
$L967:
lw	$5,0($13)
addiu	$6,$6,16
addiu	$13,$13,4
sll	$5,$5,1
addu	$5,$4,$5
lh	$3,0($5)
lh	$12,128($5)
lh	$24,32($5)
lh	$15,160($5)
addu	$16,$3,$12
subu	$12,$3,$12
sll	$2,$16,2
sll	$10,$15,4
sll	$3,$16,4
sll	$17,$12,2
sll	$5,$12,4
sll	$11,$24,3
sll	$9,$24,4
sll	$25,$15,3
subu	$3,$3,$2
subu	$5,$5,$17
subu	$11,$11,$24
addu	$10,$10,$15
addu	$9,$9,$24
subu	$15,$25,$15
addu	$2,$3,$16
subu	$10,$11,$10
addu	$3,$5,$12
addu	$5,$9,$15
addu	$11,$3,$10
addu	$9,$2,$5
subu	$3,$3,$10
subu	$2,$2,$5
sw	$11,-12($6)
sw	$9,-16($6)
sw	$3,-8($6)
bne	$6,$14,$L967
sw	$2,-4($6)

lw	$12,%got(x_offset.8542)($28)
addiu	$13,$sp,24
li	$11,524288			# 0x80000
addiu	$12,$12,%lo(x_offset.8542)
$L968:
lw	$15,0($7)
addiu	$7,$7,4
addiu	$12,$12,4
lw	$6,-4($12)
lw	$3,28($7)
lw	$24,12($7)
lw	$16,44($7)
addu	$17,$15,$3
subu	$15,$15,$3
sll	$2,$17,2
sll	$14,$24,3
sll	$9,$24,4
sll	$3,$17,4
sll	$18,$15,2
sll	$5,$15,4
sll	$25,$16,3
subu	$14,$14,$24
addu	$9,$9,$24
subu	$3,$3,$2
subu	$5,$5,$18
sll	$10,$16,4
subu	$24,$25,$16
addu	$2,$3,$17
addu	$10,$10,$16
addu	$3,$5,$15
addu	$5,$9,$24
subu	$10,$14,$10
addu	$14,$2,$5
subu	$2,$2,$5
mul	$5,$14,$8
addu	$9,$3,$10
subu	$3,$3,$10
addu	$10,$5,$11
mul	$5,$9,$8
sra	$10,$10,20
addu	$9,$5,$11
mul	$5,$3,$8
addu	$3,$5,$11
mul	$5,$2,$8
sra	$3,$3,20
addu	$2,$5,$11
sll	$5,$6,1
sra	$2,$2,20
sra	$6,$9,20
addu	$5,$4,$5
sh	$10,0($5)
sh	$6,64($5)
sh	$3,256($5)
bne	$7,$13,$L968
sh	$2,320($5)

lw	$18,84($sp)
lw	$17,80($sp)
lw	$16,76($sp)
j	$31
addiu	$sp,$sp,88

.set	macro
.set	reorder
.end	ff_svq3_luma_dc_dequant_idct_c
.size	ff_svq3_luma_dc_dequant_idct_c, .-ff_svq3_luma_dc_dequant_idct_c
.section	.text.ff_svq3_add_idct_c,"ax",@progbits
.align	2
.align	5
.globl	ff_svq3_add_idct_c
.set	nomips16
.set	nomicromips
.ent	ff_svq3_add_idct_c
.type	ff_svq3_add_idct_c, @function
ff_svq3_add_idct_c:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(svq3_dequant_coeff)($28)
addiu	$sp,$sp,-16
sll	$7,$7,2
sw	$19,12($sp)
addiu	$2,$2,%lo(svq3_dequant_coeff)
sw	$18,8($sp)
sw	$17,4($sp)
addu	$7,$7,$2
sw	$16,0($sp)
lw	$3,32($sp)
beq	$3,$0,$L978
lw	$11,0($7)

li	$2,1			# 0x1
beq	$3,$2,$L982
nop

lh	$2,0($5)
sra	$2,$2,3
mul	$10,$2,$11
srl	$2,$10,31
addu	$2,$2,$10
li	$10,169			# 0xa9
sra	$2,$2,1
mul	$2,$2,$10
$L975:
li	$10,524288			# 0x80000
sh	$0,0($5)
b	$L973
addu	$10,$2,$10

$L978:
li	$10,524288			# 0x80000
$L973:
addiu	$15,$5,32
move	$9,$5
move	$8,$5
$L976:
lh	$24,0($8)
addiu	$8,$8,8
lh	$3,-4($8)
lh	$17,-6($8)
lh	$16,-2($8)
addu	$2,$24,$3
subu	$24,$24,$3
sll	$25,$2,2
sll	$3,$2,4
sll	$12,$16,3
sll	$13,$17,4
sll	$19,$24,2
sll	$7,$24,4
sll	$14,$17,3
sll	$18,$16,4
subu	$3,$3,$25
addu	$13,$13,$17
subu	$12,$12,$16
subu	$7,$7,$19
subu	$14,$14,$17
addu	$16,$18,$16
addu	$12,$13,$12
addu	$2,$3,$2
subu	$13,$14,$16
addu	$3,$7,$24
andi	$2,$2,0xffff
andi	$7,$12,0xffff
andi	$3,$3,0xffff
andi	$12,$13,0xffff
addu	$14,$7,$2
addu	$13,$12,$3
subu	$2,$2,$7
subu	$3,$3,$12
sh	$14,-8($8)
sh	$13,-6($8)
sh	$2,-2($8)
bne	$8,$15,$L976
sh	$3,-4($8)

addu	$24,$4,$6
lw	$14,%got(ff_cropTbl)($28)
addiu	$5,$5,8
addu	$15,$24,$6
addiu	$14,$14,1024
addu	$6,$15,$6
$L977:
lh	$13,0($9)
addiu	$9,$9,2
addiu	$4,$4,1
lbu	$16,-1($4)
addiu	$24,$24,1
addiu	$15,$15,1
lh	$3,14($9)
addiu	$6,$6,1
lh	$18,6($9)
lh	$17,22($9)
addu	$8,$13,$3
sll	$12,$18,4
sll	$19,$8,2
sll	$2,$8,4
sll	$7,$17,3
subu	$2,$2,$19
subu	$7,$7,$17
addu	$12,$12,$18
addu	$2,$2,$8
addu	$12,$12,$7
subu	$13,$13,$3
addu	$25,$2,$12
sll	$3,$13,2
mul	$7,$25,$11
sll	$8,$18,3
sll	$19,$17,4
subu	$8,$8,$18
addu	$17,$19,$17
subu	$2,$2,$12
addu	$25,$7,$10
sll	$7,$13,4
sra	$25,$25,20
subu	$7,$7,$3
addu	$25,$16,$25
addu	$3,$7,$13
subu	$7,$8,$17
addu	$25,$14,$25
addu	$8,$3,$7
subu	$3,$3,$7
mul	$12,$8,$11
lbu	$13,0($25)
mul	$8,$3,$11
sb	$13,-1($4)
addu	$7,$12,$10
addu	$3,$8,$10
lbu	$8,-1($24)
sra	$7,$7,20
sra	$3,$3,20
addu	$7,$8,$7
mul	$12,$2,$11
addu	$7,$14,$7
lbu	$7,0($7)
addu	$2,$12,$10
sb	$7,-1($24)
lbu	$7,-1($15)
sra	$2,$2,20
addu	$3,$7,$3
addu	$3,$14,$3
lbu	$3,0($3)
sb	$3,-1($15)
lbu	$3,-1($6)
addu	$2,$3,$2
addu	$2,$14,$2
lbu	$2,0($2)
bne	$9,$5,$L977
sb	$2,-1($6)

lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

$L982:
lh	$3,0($5)
li	$10,196608			# 0x30000
ori	$10,$10,0xf752
b	$L975
mul	$2,$3,$10

.set	macro
.set	reorder
.end	ff_svq3_add_idct_c
.size	ff_svq3_add_idct_c, .-ff_svq3_add_idct_c
.section	.data.rel.ro.local,"aw",@progbits
.align	2
.type	scan_patterns.8591, @object
.size	scan_patterns.8591, 16
scan_patterns.8591:
.word	luma_dc_zigzag_scan
.word	zigzag_scan
.word	svq3_scan
.word	chroma_dc_scan
.rdata
.align	2
.type	x_offset.8542, @object
.size	x_offset.8542, 16
x_offset.8542:
.word	0
.word	16
.word	64
.word	80
.align	2
.type	y_offset.8543, @object
.size	y_offset.8543, 16
y_offset.8543:
.word	0
.word	32
.word	128
.word	160
.globl	svq3_decoder
.section	.rodata.str1.4
.align	2
$LC22:
.ascii	"svq3\000"
.align	2
$LC23:
.ascii	"Sorenson Vector Quantizer 3 / Sorenson Video 3 / SVQ3\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	svq3_decoder, @object
.size	svq3_decoder, 72
svq3_decoder:
.word	$LC22
.word	0
.word	24
.word	181616
.word	svq3_decode_init
.word	0
.word	ff_h264_decode_end
.word	svq3_decode_frame
.word	35
.space	12
.word	__compound_literal.0
.word	$LC23
.space	16
.rdata
.align	2
.type	__compound_literal.0, @object
.size	__compound_literal.0, 8
__compound_literal.0:
.word	12
.word	-1
.align	2
.type	svq3_dequant_coeff, @object
.size	svq3_dequant_coeff, 128
svq3_dequant_coeff:
.word	3881
.word	4351
.word	4890
.word	5481
.word	6154
.word	6914
.word	7761
.word	8718
.word	9781
.word	10987
.word	12339
.word	13828
.word	15523
.word	17435
.word	19561
.word	21873
.word	24552
.word	27656
.word	30847
.word	34870
.word	38807
.word	43747
.word	49103
.word	54683
.word	61694
.word	68745
.word	77615
.word	89113
.word	100253
.word	109366
.word	126635
.word	141533
.align	2
.type	svq3_dct_tables, @object
.size	svq3_dct_tables, 64
svq3_dct_tables:
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	2
.byte	1
.byte	0
.byte	2
.byte	3
.byte	1
.byte	4
.byte	1
.byte	5
.byte	1
.byte	0
.byte	3
.byte	1
.byte	2
.byte	2
.byte	2
.byte	6
.byte	1
.byte	7
.byte	1
.byte	8
.byte	1
.byte	9
.byte	1
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	2
.byte	2
.byte	1
.byte	0
.byte	3
.byte	0
.byte	4
.byte	0
.byte	5
.byte	3
.byte	1
.byte	4
.byte	1
.byte	1
.byte	2
.byte	1
.byte	3
.byte	0
.byte	6
.byte	0
.byte	7
.byte	0
.byte	8
.byte	0
.byte	9
.align	2
.type	svq3_pred_1, @object
.size	svq3_pred_1, 180
svq3_pred_1:
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	2
.byte	1
.byte	-1
.byte	-1
.byte	-1
.byte	1
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	2
.byte	1
.byte	-1
.byte	-1
.byte	-1
.byte	1
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	1
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	0
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	0
.byte	2
.byte	1
.byte	4
.byte	3
.byte	0
.byte	1
.byte	2
.byte	4
.byte	3
.byte	0
.byte	2
.byte	1
.byte	4
.byte	3
.byte	2
.byte	0
.byte	1
.byte	3
.byte	4
.byte	0
.byte	4
.byte	2
.byte	1
.byte	3
.byte	2
.byte	0
.byte	-1
.byte	-1
.byte	-1
.byte	2
.byte	1
.byte	0
.byte	4
.byte	3
.byte	1
.byte	2
.byte	4
.byte	0
.byte	3
.byte	2
.byte	1
.byte	0
.byte	4
.byte	3
.byte	2
.byte	1
.byte	4
.byte	3
.byte	0
.byte	1
.byte	2
.byte	4
.byte	0
.byte	3
.byte	2
.byte	0
.byte	-1
.byte	-1
.byte	-1
.byte	2
.byte	0
.byte	1
.byte	4
.byte	3
.byte	1
.byte	2
.byte	0
.byte	4
.byte	3
.byte	2
.byte	1
.byte	0
.byte	4
.byte	3
.byte	2
.byte	1
.byte	3
.byte	4
.byte	0
.byte	2
.byte	4
.byte	1
.byte	0
.byte	3
.byte	0
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	0
.byte	2
.byte	1
.byte	3
.byte	4
.byte	1
.byte	2
.byte	3
.byte	0
.byte	4
.byte	2
.byte	0
.byte	1
.byte	3
.byte	4
.byte	2
.byte	1
.byte	3
.byte	0
.byte	4
.byte	2
.byte	0
.byte	4
.byte	3
.byte	1
.byte	0
.byte	2
.byte	-1
.byte	-1
.byte	-1
.byte	0
.byte	2
.byte	4
.byte	1
.byte	3
.byte	1
.byte	4
.byte	2
.byte	0
.byte	3
.byte	4
.byte	2
.byte	0
.byte	1
.byte	3
.byte	2
.byte	0
.byte	1
.byte	4
.byte	3
.byte	4
.byte	2
.byte	1
.byte	0
.byte	3
.align	2
.type	svq3_pred_0, @object
.size	svq3_pred_0, 50
svq3_pred_0:
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	1
.byte	1
.byte	2
.byte	0
.byte	3
.byte	0
.byte	2
.byte	1
.byte	1
.byte	2
.byte	0
.byte	3
.byte	0
.byte	4
.byte	1
.byte	3
.byte	2
.byte	2
.byte	3
.byte	1
.byte	4
.byte	0
.byte	4
.byte	1
.byte	3
.byte	2
.byte	2
.byte	3
.byte	1
.byte	4
.byte	2
.byte	4
.byte	3
.byte	3
.byte	4
.byte	2
.byte	4
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.align	2
.type	svq3_scan, @object
.size	svq3_scan, 16
svq3_scan:
.byte	0
.byte	1
.byte	2
.byte	6
.byte	10
.byte	3
.byte	7
.byte	11
.byte	4
.byte	8
.byte	5
.byte	9
.byte	12
.byte	13
.byte	14
.byte	15
.align	2
.type	i_mb_type_info, @object
.size	i_mb_type_info, 104
i_mb_type_info:
.half	1
.byte	-1
.byte	-1
.half	2
.byte	2
.byte	0
.half	2
.byte	1
.byte	0
.half	2
.byte	0
.byte	0
.half	2
.byte	3
.byte	0
.half	2
.byte	2
.byte	16
.half	2
.byte	1
.byte	16
.half	2
.byte	0
.byte	16
.half	2
.byte	3
.byte	16
.half	2
.byte	2
.byte	32
.half	2
.byte	1
.byte	32
.half	2
.byte	0
.byte	32
.half	2
.byte	3
.byte	32
.half	2
.byte	2
.byte	15
.half	2
.byte	1
.byte	15
.half	2
.byte	0
.byte	15
.half	2
.byte	3
.byte	15
.half	2
.byte	2
.byte	31
.half	2
.byte	1
.byte	31
.half	2
.byte	0
.byte	31
.half	2
.byte	3
.byte	31
.half	2
.byte	2
.byte	47
.half	2
.byte	1
.byte	47
.half	2
.byte	0
.byte	47
.half	2
.byte	3
.byte	47
.half	4
.byte	-1
.byte	-1
.align	2
.type	chroma_dc_scan, @object
.size	chroma_dc_scan, 4
chroma_dc_scan:
.byte	0
.byte	16
.byte	32
.byte	48
.align	2
.type	luma_dc_zigzag_scan, @object
.size	luma_dc_zigzag_scan, 16
luma_dc_zigzag_scan:
.byte	0
.byte	16
.byte	32
.byte	-128
.byte	48
.byte	64
.byte	80
.byte	96
.byte	-112
.byte	-96
.byte	-80
.byte	-64
.byte	112
.byte	-48
.byte	-32
.byte	-16
.align	2
.type	zigzag_scan, @object
.size	zigzag_scan, 16
zigzag_scan:
.byte	0
.byte	1
.byte	4
.byte	8
.byte	5
.byte	2
.byte	3
.byte	6
.byte	9
.byte	12
.byte	13
.byte	10
.byte	7
.byte	11
.byte	14
.byte	15
.align	2
.type	golomb_to_inter_cbp, @object
.size	golomb_to_inter_cbp, 48
golomb_to_inter_cbp:
.byte	0
.byte	16
.byte	1
.byte	2
.byte	4
.byte	8
.byte	32
.byte	3
.byte	5
.byte	10
.byte	12
.byte	15
.byte	47
.byte	7
.byte	11
.byte	13
.byte	14
.byte	6
.byte	9
.byte	31
.byte	35
.byte	37
.byte	42
.byte	44
.byte	33
.byte	34
.byte	36
.byte	40
.byte	39
.byte	43
.byte	45
.byte	46
.byte	17
.byte	18
.byte	20
.byte	24
.byte	19
.byte	21
.byte	26
.byte	28
.byte	23
.byte	27
.byte	29
.byte	30
.byte	22
.byte	25
.byte	38
.byte	41
.align	2
.type	golomb_to_intra4x4_cbp, @object
.size	golomb_to_intra4x4_cbp, 48
golomb_to_intra4x4_cbp:
.byte	47
.byte	31
.byte	15
.byte	0
.byte	23
.byte	27
.byte	29
.byte	30
.byte	7
.byte	11
.byte	13
.byte	14
.byte	39
.byte	43
.byte	45
.byte	46
.byte	16
.byte	3
.byte	5
.byte	10
.byte	12
.byte	19
.byte	21
.byte	26
.byte	28
.byte	35
.byte	37
.byte	42
.byte	44
.byte	1
.byte	2
.byte	4
.byte	8
.byte	17
.byte	18
.byte	20
.byte	24
.byte	6
.byte	9
.byte	22
.byte	25
.byte	32
.byte	33
.byte	34
.byte	36
.byte	40
.byte	38
.byte	41
.align	2
.type	golomb_to_pict_type, @object
.size	golomb_to_pict_type, 5
golomb_to_pict_type:
.byte	2
.byte	3
.byte	1
.byte	6
.byte	5
.align	2
.type	scan8, @object
.size	scan8, 24
scan8:
.byte	12
.byte	13
.byte	20
.byte	21
.byte	14
.byte	15
.byte	22
.byte	23
.byte	28
.byte	29
.byte	36
.byte	37
.byte	30
.byte	31
.byte	38
.byte	39
.byte	9
.byte	10
.byte	17
.byte	18
.byte	33
.byte	34
.byte	41
.byte	42

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
