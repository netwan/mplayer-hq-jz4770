.file	1 "h264_p1.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.text
.align	2
.globl	aux_do_get_phy_addr
.set	nomips16
.set	nomicromips
.ent	aux_do_get_phy_addr
.type	aux_do_get_phy_addr, @function
aux_do_get_phy_addr:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
move	$2,$4
li	$3,-16777216			# 0xffffffffff000000
and	$3,$4,$3
li	$4,-201326592			# 0xfffffffff4000000
beq	$3,$4,$L4
li	$4,321650688			# 0x132c0000

j	$31
nop

$L4:
andi	$2,$2,0xffff
j	$31
or	$2,$2,$4

.set	macro
.set	reorder
.end	aux_do_get_phy_addr
.size	aux_do_get_phy_addr, .-aux_do_get_phy_addr
.align	2
.globl	do_get_phy_addr
.set	nomips16
.set	nomicromips
.ent	do_get_phy_addr
.type	do_get_phy_addr, @function
do_get_phy_addr:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
j	$31
move	$2,$4

.set	macro
.set	reorder
.end	do_get_phy_addr
.size	do_get_phy_addr, .-do_get_phy_addr
.align	2
.globl	aux_end
.set	nomips16
.set	nomicromips
.ent	aux_end
.type	aux_end, @function
aux_end:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
sw	$5,0($4)
#APP
# 19 "eyer_driver_aux.c" 1
nop	#i_nop
# 0 "" 2
# 20 "eyer_driver_aux.c" 1
nop	#i_nop
# 0 "" 2
# 21 "eyer_driver_aux.c" 1
nop	#i_nop
# 0 "" 2
# 22 "eyer_driver_aux.c" 1
nop	#i_nop
# 0 "" 2
# 23 "eyer_driver_aux.c" 1
nop	#i_nop
# 0 "" 2
# 24 "eyer_driver_aux.c" 1
wait
# 0 "" 2
#NO_APP
j	$31
.end	aux_end
.size	aux_end, .-aux_end
.align	2
.globl	cfg_sde
.set	nomips16
.set	nomicromips
.ent	cfg_sde
.type	cfg_sde, @function
cfg_sde:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
lw	$12,20($sp)
lw	$11,24($sp)
blez	$5,$L10
andi	$6,$6,0x00ff

slt	$13,$0,$4
slt	$2,$6,$5
beq	$2,$0,$L11
move	$9,$13

andi	$14,$13,0x00ff
addiu	$10,$6,-1
slt	$3,$5,$10
bne	$3,$0,$L32
move	$2,$14

slt	$6,$5,$6
beq	$6,$0,$L25
slt	$10,$4,$10

$L23:
move	$3,$0
$L15:
addu	$9,$9,$3
li	$3,4			# 0x4
movz	$3,$0,$2
addu	$3,$9,$3
li	$2,8			# 0x8
movz	$2,$0,$10
move	$10,$2
$L17:
addu	$3,$3,$10
sll	$3,$3,24
addu	$3,$3,$5
sw	$3,0($7)
li	$2,301989888			# 0x12000000
addu	$5,$5,$2
li	$2,-16777216			# 0xffffffffff000000
and	$2,$8,$2
li	$3,-201326592			# 0xfffffffff4000000
beq	$2,$3,$L33
sw	$5,4($7)

$L18:
li	$2,-16777216			# 0xffffffffff000000
and	$2,$12,$2
li	$3,-201326592			# 0xfffffffff4000000
beq	$2,$3,$L34
sw	$8,8($7)

$L19:
sw	$12,12($7)
sll	$3,$4,2
sll	$8,$4,4
addu	$3,$3,$8
subu	$8,$3,$4
sll	$8,$8,2
addu	$6,$11,$8
move	$2,$0
li	$5,76			# 0x4c
$L20:
addu	$3,$7,$2
addu	$4,$6,$2
lw	$4,0($4)
addiu	$2,$2,4
bne	$2,$5,$L20
sw	$4,16($3)

addiu	$8,$8,76
addu	$2,$11,$8
lw	$3,0($2)
sw	$3,92($7)
lw	$3,8($2)
sw	$3,96($7)
lw	$3,12($2)
sw	$3,100($7)
lw	$2,28($2)
j	$31
sw	$2,104($7)

$L10:
move	$9,$0
$L11:
addiu	$10,$6,-1
slt	$2,$5,$10
bne	$2,$0,$L14
move	$2,$0

slt	$6,$5,$6
beq	$6,$0,$L25
slt	$10,$4,$10

j	$L15
move	$3,$0

$L14:
slt	$6,$5,$6
bne	$6,$0,$L23
move	$10,$0

addiu	$3,$9,2
$L21:
move	$10,$0
addu	$3,$3,$10
sll	$3,$3,24
addu	$3,$3,$5
sw	$3,0($7)
li	$2,301989888			# 0x12000000
addu	$5,$5,$2
li	$2,-16777216			# 0xffffffffff000000
and	$2,$8,$2
li	$3,-201326592			# 0xfffffffff4000000
bne	$2,$3,$L18
sw	$5,4($7)

$L33:
andi	$8,$8,0xffff
li	$2,321650688			# 0x132c0000
or	$8,$8,$2
li	$2,-16777216			# 0xffffffffff000000
and	$2,$12,$2
li	$3,-201326592			# 0xfffffffff4000000
bne	$2,$3,$L19
sw	$8,8($7)

$L34:
andi	$12,$12,0xffff
li	$2,321650688			# 0x132c0000
j	$L19
or	$12,$12,$2

$L25:
j	$L15
li	$3,2			# 0x2

$L32:
beq	$14,$0,$L21
addiu	$3,$13,2

addiu	$3,$13,6
j	$L17
move	$10,$0

.set	macro
.set	reorder
.end	cfg_sde
.size	cfg_sde, .-cfg_sde
.align	2
.globl	backup_sde_bac_aux
.set	nomips16
.set	nomicromips
.ent	backup_sde_bac_aux
.type	backup_sde_bac_aux, @function
backup_sde_bac_aux:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
sll	$2,$4,2
sll	$3,$4,4
addu	$2,$2,$3
subu	$4,$2,$4
sll	$4,$4,2
addu	$7,$7,$4
sll	$5,$5,24
li	$2,16711680			# 0xff0000
ori	$2,$2,0xffff
or	$5,$5,$2
#APP
# 39 "t_sde_aux.c" 1
.word	0b01110000000001010000001111101111	#S32I2M XR15,$5
# 0 "" 2
# 44 "t_sde_aux.c" 1
.word	0b01110000110000000000000001010000	#S32LDD XR1,$6,0
# 0 "" 2
#NO_APP
move	$3,$6
#APP
# 45 "t_sde_aux.c" 1
.word	0b01110000011000000000010010010100	#S32LDI XR2,$3,4
# 0 "" 2
# 46 "t_sde_aux.c" 1
.word	0b01110000011000000001010011010100	#S32LDI XR3,$3,20
# 0 "" 2
# 47 "t_sde_aux.c" 1
.word	0b01110000000101111100100010100111	#S32AND XR2,XR2,XR15
# 0 "" 2
# 48 "t_sde_aux.c" 1
.word	0b01110000111000000000000001010001	#S32STD XR1,$7,0
# 0 "" 2
# 49 "t_sde_aux.c" 1
.word	0b01110011000000001000110100111101	#S32SFL XR4,XR3,XR2,XR0,PTN3
# 0 "" 2
# 50 "t_sde_aux.c" 1
.word	0b01110010000000000001000100110001	#D32SLR XR4,XR4,XR0,XR0,8
# 0 "" 2
# 51 "t_sde_aux.c" 1
.word	0b01110011000100001001000000111101	#S32SFL XR0,XR4,XR2,XR4,PTN3
# 0 "" 2
#NO_APP
move	$5,$7
#APP
# 53 "t_sde_aux.c" 1
.word	0b01110000101000000000010100010101	#S32SDI XR4,$5,4
# 0 "" 2
# 54 "t_sde_aux.c" 1
.word	0b01110000000000100000000001101110	#S32M2I XR1, $2
# 0 "" 2
# 55 "t_sde_aux.c" 1
.word	0b01110000011000000000010111010100	#S32LDI XR7,$3,4
# 0 "" 2
# 56 "t_sde_aux.c" 1
.word	0b01110000011000000000011000010100	#S32LDI XR8,$3,4
# 0 "" 2
# 57 "t_sde_aux.c" 1
.word	0b01110000011000000000011001010100	#S32LDI XR9,$3,4
# 0 "" 2
#NO_APP
andi	$4,$2,0x4
bne	$4,$0,$L39
#APP
# 69 "t_sde_aux.c" 1
.word	0b01110011000000011110001000111101	#S32SFL XR8,XR8,XR7,XR0,PTN3
# 0 "" 2
#NO_APP
move	$4,$5
#APP
# 70 "t_sde_aux.c" 1
.word	0b01110000100000000000011000010101	#S32SDI XR8,$4,4
# 0 "" 2
# 71 "t_sde_aux.c" 1
.word	0b01110000011000000000110001010100	#S32LDI XR1,$3,12
# 0 "" 2
# 72 "t_sde_aux.c" 1
.word	0b01110000011000000000010010010100	#S32LDI XR2,$3,4
# 0 "" 2
# 73 "t_sde_aux.c" 1
.word	0b01110000011000000000010011010100	#S32LDI XR3,$3,4
# 0 "" 2
# 74 "t_sde_aux.c" 1
.word	0b01110000011000000000010100010100	#S32LDI XR4,$3,4
# 0 "" 2
# 75 "t_sde_aux.c" 1
.word	0b01110000011000000001010101010100	#S32LDI XR5,$3,20
# 0 "" 2
# 76 "t_sde_aux.c" 1
.word	0b01110000011000000000010110010100	#S32LDI XR6,$3,4
# 0 "" 2
# 77 "t_sde_aux.c" 1
.word	0b01110000011000000000010111010100	#S32LDI XR7,$3,4
# 0 "" 2
# 78 "t_sde_aux.c" 1
.word	0b01110000011000000000011000010100	#S32LDI XR8,$3,4
# 0 "" 2
# 79 "t_sde_aux.c" 1
.word	0b01110000011000000001011001010100	#S32LDI XR9,$3,20
# 0 "" 2
# 80 "t_sde_aux.c" 1
.word	0b01110000011000000000011010010100	#S32LDI XR10,$3,4
# 0 "" 2
# 81 "t_sde_aux.c" 1
.word	0b01110000011000000000011011010100	#S32LDI XR11,$3,4
# 0 "" 2
# 82 "t_sde_aux.c" 1
.word	0b01110000011000000000011100010100	#S32LDI XR12,$3,4
# 0 "" 2
# 83 "t_sde_aux.c" 1
.word	0b01110000100000000000010001010101	#S32SDI XR1,$4,4
# 0 "" 2
# 84 "t_sde_aux.c" 1
.word	0b01110000100000000000010010010101	#S32SDI XR2,$4,4
# 0 "" 2
# 85 "t_sde_aux.c" 1
.word	0b01110000100000000000010011010101	#S32SDI XR3,$4,4
# 0 "" 2
# 86 "t_sde_aux.c" 1
.word	0b01110000100000000000010100010101	#S32SDI XR4,$4,4
# 0 "" 2
# 87 "t_sde_aux.c" 1
.word	0b01110000100000000000011001010101	#S32SDI XR9,$4,4
# 0 "" 2
# 88 "t_sde_aux.c" 1
.word	0b01110000100000000000011010010101	#S32SDI XR10,$4,4
# 0 "" 2
# 89 "t_sde_aux.c" 1
.word	0b01110000100000000000011011010101	#S32SDI XR11,$4,4
# 0 "" 2
# 90 "t_sde_aux.c" 1
.word	0b01110000100000000000011100010101	#S32SDI XR12,$4,4
# 0 "" 2
# 91 "t_sde_aux.c" 1
.word	0b01110000011000000001010001010100	#S32LDI XR1,$3,20
# 0 "" 2
# 92 "t_sde_aux.c" 1
.word	0b01110000011000000000010010010100	#S32LDI XR2,$3,4
# 0 "" 2
# 93 "t_sde_aux.c" 1
.word	0b01110000011000000000010011010100	#S32LDI XR3,$3,4
# 0 "" 2
# 94 "t_sde_aux.c" 1
.word	0b01110000011000000000010100010100	#S32LDI XR4,$3,4
# 0 "" 2
#NO_APP
move	$3,$4
#APP
# 95 "t_sde_aux.c" 1
.word	0b01110000011000000000010101010101	#S32SDI XR5,$3,4
# 0 "" 2
# 96 "t_sde_aux.c" 1
.word	0b01110000011000000000010110010101	#S32SDI XR6,$3,4
# 0 "" 2
# 97 "t_sde_aux.c" 1
.word	0b01110000011000000000010111010101	#S32SDI XR7,$3,4
# 0 "" 2
# 98 "t_sde_aux.c" 1
.word	0b01110000011000000000011000010101	#S32SDI XR8,$3,4
# 0 "" 2
# 99 "t_sde_aux.c" 1
.word	0b01110000011000000000010001010101	#S32SDI XR1,$3,4
# 0 "" 2
# 100 "t_sde_aux.c" 1
.word	0b01110000011000000000010010010101	#S32SDI XR2,$3,4
# 0 "" 2
# 101 "t_sde_aux.c" 1
.word	0b01110000011000000000010011010101	#S32SDI XR3,$3,4
# 0 "" 2
# 102 "t_sde_aux.c" 1
.word	0b01110000011000000000010100010101	#S32SDI XR4,$3,4
# 0 "" 2
#NO_APP
srl	$2,$2,24
.set	noreorder
.set	nomacro
j	$31
andi	$2,$2,0x1
.set	macro
.set	reorder

$L39:
lw	$4,32($6)
andi	$5,$4,0xf000
srl	$8,$5,8
srl	$4,$4,16
andi	$4,$4,0xff00
addu	$5,$8,$4
lbu	$3,38($6)
srl	$3,$3,4
addu	$4,$5,$3
lbu	$3,39($6)
sll	$3,$3,16
addu	$3,$4,$3
sw	$3,8($7)
srl	$2,$2,24
.set	noreorder
.set	nomacro
j	$31
andi	$2,$2,0x1
.set	macro
.set	reorder

.end	backup_sde_bac_aux
.size	backup_sde_bac_aux, .-backup_sde_bac_aux
.align	2
.globl	hw_dblk_mb_aux_c
.set	nomips16
.set	nomicromips
.ent	hw_dblk_mb_aux_c
.type	hw_dblk_mb_aux_c, @function
hw_dblk_mb_aux_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-40
sw	$fp,36($sp)
sw	$23,32($sp)
sw	$22,28($sp)
sw	$21,24($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
lw	$13,56($sp)
lw	$22,60($sp)
lw	$fp,64($sp)
lw	$16,68($sp)
lw	$12,72($sp)
lw	$17,76($sp)
lw	$14,80($sp)
lw	$9,84($sp)
lw	$20,88($sp)
lhu	$10,0($4)
xori	$15,$5,0x4
sltu	$15,$15,1
xori	$5,$5,0x1
sltu	$5,$5,1
.set	noreorder
.set	nomacro
beq	$6,$0,$L41
andi	$25,$10,0x3
.set	macro
.set	reorder

lui	$3,%hi(nnz_left_dblk)
lw	$21,%lo(nnz_left_dblk)($3)
lui	$24,%hi(mb_qp_left)
lh	$19,%lo(mb_qp_left)($24)
$L42:
lui	$11,%hi(mb_qp_up)
.set	noreorder
.set	nomacro
bne	$7,$0,$L43
addiu	$11,$11,%lo(mb_qp_up)
.set	macro
.set	reorder

addu	$2,$11,$6
sb	$0,0($2)
$L43:
lw	$2,20($4)
lw	$23,4($14)
li	$8,251658240			# 0xf000000
and	$23,$23,$8
andi	$8,$2,0x80
sll	$18,$8,14
andi	$8,$2,0x20
sll	$8,$8,15
or	$8,$18,$8
andi	$18,$2,0x2000
sll	$18,$18,9
or	$8,$8,$18
andi	$18,$2,0x8000
sll	$18,$18,8
or	$8,$8,$18
sw	$8,%lo(nnz_left_dblk)($3)
andi	$8,$2,0xc0c
sll	$18,$8,2
andi	$3,$2,0x3030
sra	$3,$3,2
or	$8,$18,$3
andi	$2,$2,0xc3c3
or	$3,$8,$2
sra	$2,$23,8
or	$2,$3,$2
or	$2,$21,$2
andi	$3,$10,0x38
srl	$8,$3,2
lui	$3,%hi(mv_msk_table.1766)
addiu	$3,$3,%lo(mv_msk_table.1766)
addu	$3,$8,$3
lh	$21,0($3)
sll	$21,$21,24
or	$2,$2,$21
sw	$2,16($16)
sw	$2,16($12)
sw	$20,8($16)
.set	noreorder
.set	nomacro
bne	$5,$0,$L59
sw	$20,8($12)
.set	macro
.set	reorder

lhu	$3,44($4)
sll	$2,$15,1
sll	$20,$15,3
addu	$2,$2,$20
addu	$2,$3,$2
addiu	$2,$2,11
sll	$20,$2,24
$L44:
lw	$18,0($14)
addu	$2,$11,$6
lbu	$3,0($2)
li	$2,-201326592			# 0xfffffffff4000000
addiu	$2,$2,19664
addu	$8,$2,$3
lb	$21,0($8)
addu	$21,$21,$22
addiu	$21,$21,1
sra	$21,$21,1
sll	$23,$fp,18
or	$23,$23,$22
lb	$8,256($8)
addu	$8,$8,$fp
addiu	$8,$8,1
sra	$8,$8,1
sll	$8,$8,12
or	$23,$23,$8
addu	$2,$2,$19
lb	$8,256($2)
addu	$8,$8,$fp
addiu	$8,$8,1
sra	$8,$8,1
sll	$8,$8,24
or	$8,$23,$8
lb	$2,0($2)
addu	$2,$2,$22
addiu	$2,$2,1
sra	$2,$2,1
sll	$2,$2,6
or	$8,$8,$2
sw	$8,20($16)
sw	$8,20($12)
addu	$8,$3,$13
addiu	$8,$8,1
sra	$8,$8,1
sll	$3,$13,6
or	$8,$8,$3
sll	$2,$21,18
or	$3,$8,$2
addu	$2,$13,$19
addiu	$2,$2,1
sra	$2,$2,1
sll	$2,$2,12
or	$2,$3,$2
or	$2,$2,$20
sw	$2,24($16)
sw	$2,24($12)
lui	$19,%hi(left_mb_type)
lw	$20,%lo(left_mb_type)($19)
andi	$2,$20,0x28
and	$2,$2,$10
sltu	$2,$0,$2
andi	$21,$10,0x8
andi	$21,$21,0xffff
and	$3,$18,$21
li	$23,16			# 0x10
move	$8,$0
movn	$8,$23,$3
move	$3,$8
andi	$fp,$10,0x20
li	$8,4			# 0x4
movz	$8,$0,$fp
move	$fp,$8
andi	$8,$10,0x10
li	$22,64			# 0x40
movz	$22,$0,$8
or	$3,$3,$2
or	$2,$3,$fp
or	$8,$2,$22
lw	$2,92($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L64
movn	$23,$0,$7
.set	macro
.set	reorder

sltu	$2,$6,1
andi	$3,$10,0xc00
sra	$3,$3,9
sra	$21,$21,3
or	$21,$3,$21
sll	$3,$21,1
lui	$21,%hi(edg_msk_table.1767)
addiu	$21,$21,%lo(edg_msk_table.1767)
addu	$21,$3,$21
lh	$3,0($21)
or	$2,$2,$3
or	$7,$2,$23
sll	$7,$7,16
$L49:
sll	$8,$8,24
or	$8,$7,$8
sltu	$3,$0,$25
sll	$7,$3,4
andi	$2,$20,0x3
sltu	$2,$0,$2
sll	$2,$2,5
or	$3,$7,$2
andi	$2,$18,0x3
sltu	$2,$0,$2
sll	$2,$2,3
or	$2,$3,$2
ori	$3,$2,0x445
or	$3,$3,$8
sw	$3,28($16)
sw	$17,56($16)
sw	$17,56($12)
ori	$2,$2,0x345
or	$2,$2,$8
.set	noreorder
.set	nomacro
bne	$5,$0,$L51
sw	$2,28($12)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$25,$0,$L52
lui	$2,%hi(ref_left_dblk)
.set	macro
.set	reorder

lhu	$2,8($14)
lw	$5,28($4)
sw	$5,0($9)
lui	$7,%hi(ref_left_dblk)
lw	$3,%lo(ref_left_dblk)($7)
or	$2,$2,$3
sw	$2,4($9)
andi	$2,$5,0xff00
sll	$3,$2,8
li	$8,-16777216			# 0xffffffffff000000
and	$2,$5,$8
or	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$15,$0,$L53
sw	$2,%lo(ref_left_dblk)($7)
.set	macro
.set	reorder

addiu	$9,$9,8
$L54:
lhu	$12,46($4)
andi	$16,$12,0x3
addiu	$8,$9,4
andi	$5,$12,0x30
sll	$7,$5,4
andi	$3,$12,0xc
sll	$3,$3,2
or	$5,$7,$3
andi	$3,$12,0xc0
sll	$3,$3,10
or	$3,$5,$3
andi	$2,$12,0x300
sll	$2,$2,16
or	$2,$3,$2
or	$2,$2,$16
sw	$2,0($9)
addiu	$2,$14,12
#APP
# 212 "h264_p1_dblk.c" 1
.word	0b01110000010000000000000001010000	#S32LDD XR1,$2,0
# 0 "" 2
# 213 "h264_p1_dblk.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 214 "h264_p1_dblk.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 215 "h264_p1_dblk.c" 1
.word	0b01110000010000000000110100010000	#S32LDD XR4,$2,12
# 0 "" 2
#NO_APP
addiu	$2,$4,64
#APP
# 218 "h264_p1_dblk.c" 1
.word	0b01110000010000000000001001010000	#S32LDD XR9,$2,0
# 0 "" 2
# 219 "h264_p1_dblk.c" 1
.word	0b01110000010000000000011010010000	#S32LDD XR10,$2,4
# 0 "" 2
# 220 "h264_p1_dblk.c" 1
.word	0b01110000010000000000101011010000	#S32LDD XR11,$2,8
# 0 "" 2
# 221 "h264_p1_dblk.c" 1
.word	0b01110000010000000000111100010000	#S32LDD XR12,$2,12
# 0 "" 2
#NO_APP
lui	$2,%hi(mv_left_dblk)
addiu	$2,$2,%lo(mv_left_dblk)
#APP
# 224 "h264_p1_dblk.c" 1
.word	0b01110000010000000000000101010000	#S32LDD XR5,$2,0
# 0 "" 2
# 225 "h264_p1_dblk.c" 1
.word	0b01110000010000000000010110010000	#S32LDD XR6,$2,4
# 0 "" 2
# 226 "h264_p1_dblk.c" 1
.word	0b01110000010000000000100111010000	#S32LDD XR7,$2,8
# 0 "" 2
# 227 "h264_p1_dblk.c" 1
.word	0b01110000010000000000111000010000	#S32LDD XR8,$2,12
# 0 "" 2
# 230 "h264_p1_dblk.c" 1
.word	0b01110000010000000000001001010001	#S32STD XR9,$2,0
# 0 "" 2
# 231 "h264_p1_dblk.c" 1
.word	0b01110000010000000000011010010001	#S32STD XR10,$2,4
# 0 "" 2
# 232 "h264_p1_dblk.c" 1
.word	0b01110000010000000000101011010001	#S32STD XR11,$2,8
# 0 "" 2
# 233 "h264_p1_dblk.c" 1
.word	0b01110000010000000000111100010001	#S32STD XR12,$2,12
# 0 "" 2
# 235 "h264_p1_dblk.c" 1
.word	0b01110001000000000000000001010001	#S32STD XR1,$8,0
# 0 "" 2
# 236 "h264_p1_dblk.c" 1
.word	0b01110001000000000000010010010001	#S32STD XR2,$8,4
# 0 "" 2
# 237 "h264_p1_dblk.c" 1
.word	0b01110001000000000000100011010001	#S32STD XR3,$8,8
# 0 "" 2
# 238 "h264_p1_dblk.c" 1
.word	0b01110001000000000000110100010001	#S32STD XR4,$8,12
# 0 "" 2
# 241 "h264_p1_dblk.c" 1
.word	0b01110001000000000001000101010001	#S32STD XR5,$8,16
# 0 "" 2
# 242 "h264_p1_dblk.c" 1
.word	0b01110001000000000001010110010001	#S32STD XR6,$8,20
# 0 "" 2
# 243 "h264_p1_dblk.c" 1
.word	0b01110001000000000001100111010001	#S32STD XR7,$8,24
# 0 "" 2
# 244 "h264_p1_dblk.c" 1
.word	0b01110001000000000001111000010001	#S32STD XR8,$8,28
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$15,$0,$L55
lhu	$5,44($4)
.set	macro
.set	reorder

addiu	$3,$9,36
addiu	$14,$14,28
#APP
# 254 "h264_p1_dblk.c" 1
.word	0b01110001110000000000000001010000	#S32LDD XR1,$14,0
# 0 "" 2
# 255 "h264_p1_dblk.c" 1
.word	0b01110001110000000000010010010000	#S32LDD XR2,$14,4
# 0 "" 2
# 256 "h264_p1_dblk.c" 1
.word	0b01110001110000000000100011010000	#S32LDD XR3,$14,8
# 0 "" 2
# 257 "h264_p1_dblk.c" 1
.word	0b01110001110000000000110100010000	#S32LDD XR4,$14,12
# 0 "" 2
#NO_APP
addiu	$2,$4,128
#APP
# 260 "h264_p1_dblk.c" 1
.word	0b01110000010000000000001001010000	#S32LDD XR9,$2,0
# 0 "" 2
# 261 "h264_p1_dblk.c" 1
.word	0b01110000010000000000011010010000	#S32LDD XR10,$2,4
# 0 "" 2
# 262 "h264_p1_dblk.c" 1
.word	0b01110000010000000000101011010000	#S32LDD XR11,$2,8
# 0 "" 2
# 263 "h264_p1_dblk.c" 1
.word	0b01110000010000000000111100010000	#S32LDD XR12,$2,12
# 0 "" 2
#NO_APP
lui	$2,%hi(mv_left_dblk+16)
addiu	$2,$2,%lo(mv_left_dblk+16)
#APP
# 266 "h264_p1_dblk.c" 1
.word	0b01110000010000000000000101010000	#S32LDD XR5,$2,0
# 0 "" 2
# 267 "h264_p1_dblk.c" 1
.word	0b01110000010000000000010110010000	#S32LDD XR6,$2,4
# 0 "" 2
# 268 "h264_p1_dblk.c" 1
.word	0b01110000010000000000100111010000	#S32LDD XR7,$2,8
# 0 "" 2
# 269 "h264_p1_dblk.c" 1
.word	0b01110000010000000000111000010000	#S32LDD XR8,$2,12
# 0 "" 2
# 272 "h264_p1_dblk.c" 1
.word	0b01110000010000000000001001010001	#S32STD XR9,$2,0
# 0 "" 2
# 273 "h264_p1_dblk.c" 1
.word	0b01110000010000000000011010010001	#S32STD XR10,$2,4
# 0 "" 2
# 274 "h264_p1_dblk.c" 1
.word	0b01110000010000000000101011010001	#S32STD XR11,$2,8
# 0 "" 2
# 275 "h264_p1_dblk.c" 1
.word	0b01110000010000000000111100010001	#S32STD XR12,$2,12
# 0 "" 2
# 277 "h264_p1_dblk.c" 1
.word	0b01110000011000000000000001010001	#S32STD XR1,$3,0
# 0 "" 2
# 278 "h264_p1_dblk.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 279 "h264_p1_dblk.c" 1
.word	0b01110000011000000000100011010001	#S32STD XR3,$3,8
# 0 "" 2
# 280 "h264_p1_dblk.c" 1
.word	0b01110000011000000000110100010001	#S32STD XR4,$3,12
# 0 "" 2
# 282 "h264_p1_dblk.c" 1
.word	0b01110000011000000001000101010001	#S32STD XR5,$3,16
# 0 "" 2
# 283 "h264_p1_dblk.c" 1
.word	0b01110000011000000001010110010001	#S32STD XR6,$3,20
# 0 "" 2
# 284 "h264_p1_dblk.c" 1
.word	0b01110000011000000001100111010001	#S32STD XR7,$3,24
# 0 "" 2
# 285 "h264_p1_dblk.c" 1
.word	0b01110000011000000001111000010001	#S32STD XR8,$3,28
# 0 "" 2
#NO_APP
addiu	$4,$4,172
addiu	$9,$9,64
.set	noreorder
.set	nomacro
beq	$5,$0,$L51
addiu	$2,$5,-1
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
$L57:
#APP
# 294 "h264_p1_dblk.c" 1
.word	0b01110000100000000000010001010100	#S32LDI XR1,$4,4
# 0 "" 2
# 295 "h264_p1_dblk.c" 1
.word	0b01110001001000000000010001010101	#S32SDI XR1,$9,4
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$3,$L57
$L51:
addu	$6,$11,$6
sb	$13,0($6)
sh	$13,%lo(mb_qp_left)($24)
sw	$10,%lo(left_mb_type)($19)
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L64:
.set	noreorder
.set	nomacro
j	$L49
li	$7,-65536			# 0xffffffffffff0000
.set	macro
.set	reorder

$L59:
.set	noreorder
.set	nomacro
j	$L44
move	$20,$0
.set	macro
.set	reorder

$L41:
lui	$24,%hi(mb_qp_left)
sh	$0,%lo(mb_qp_left)($24)
lui	$3,%hi(nnz_left_dblk)
sw	$0,%lo(nnz_left_dblk)($3)
lui	$2,%hi(ref_left_dblk)
sw	$0,%lo(ref_left_dblk)($2)
addiu	$2,$2,%lo(ref_left_dblk)
sw	$0,4($2)
lui	$2,%hi(mv_left_dblk)
sw	$0,%lo(mv_left_dblk)($2)
addiu	$2,$2,%lo(mv_left_dblk)
sw	$0,4($2)
sw	$0,8($2)
sw	$0,12($2)
sw	$0,16($2)
sw	$0,20($2)
sw	$0,24($2)
sw	$0,28($2)
move	$19,$0
.set	noreorder
.set	nomacro
j	$L42
move	$21,$0
.set	macro
.set	reorder

$L52:
sw	$0,%lo(ref_left_dblk)($2)
addiu	$2,$2,%lo(ref_left_dblk)
sw	$0,4($2)
addu	$6,$11,$6
sb	$13,0($6)
sh	$13,%lo(mb_qp_left)($24)
sw	$10,%lo(left_mb_type)($19)
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L53:
lhu	$3,10($14)
lw	$2,32($4)
sw	$2,8($9)
addiu	$7,$7,%lo(ref_left_dblk)
lw	$5,4($7)
or	$3,$3,$5
sw	$3,12($9)
addiu	$9,$9,16
andi	$3,$2,0xff00
sll	$3,$3,8
and	$2,$2,$8
or	$2,$3,$2
.set	noreorder
.set	nomacro
j	$L54
sw	$2,4($7)
.set	macro
.set	reorder

$L55:
addiu	$4,$4,108
addiu	$9,$9,32
.set	noreorder
.set	nomacro
beq	$5,$0,$L51
addiu	$2,$5,-1
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
$L58:
#APP
# 304 "h264_p1_dblk.c" 1
.word	0b01110000100000000000010001010100	#S32LDI XR1,$4,4
# 0 "" 2
# 305 "h264_p1_dblk.c" 1
.word	0b01110001001000000000010001010101	#S32SDI XR1,$9,4
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$3,$L58
addu	$6,$11,$6
sb	$13,0($6)
sh	$13,%lo(mb_qp_left)($24)
sw	$10,%lo(left_mb_type)($19)
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

.end	hw_dblk_mb_aux_c
.size	hw_dblk_mb_aux_c, .-hw_dblk_mb_aux_c
.section	.p1_main,"ax",@progbits
.align	2
.globl	main
.set	nomips16
.set	nomicromips
.ent	main
.type	main, @function
main:
.frame	$sp,280,$31		# vars= 208, regs= 10/0, args= 32, gp= 0
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
la	$sp, 0xF400BFFC
addiu	$sp,$sp,-280
sw	$31,276($sp)
sw	$fp,272($sp)
sw	$23,268($sp)
sw	$22,264($sp)
sw	$21,260($sp)
sw	$20,256($sp)
sw	$19,252($sp)
sw	$18,248($sp)
sw	$17,244($sp)
sw	$16,240($sp)
li	$2,3			# 0x3
#APP
# 27 "h264_p1.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
li	$2,321585152			# 0x132b0000
sw	$0,36($2)
move	$3,$0
li	$2,320864256			# 0x13200000
addiu	$4,$2,108
#APP
# 35 "h264_p1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,112
#APP
# 36 "h264_p1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,116
#APP
# 37 "h264_p1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,120
#APP
# 38 "h264_p1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,124
li	$3,1			# 0x1
#APP
# 39 "h264_p1.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,96
li	$3,1980039168			# 0x76050000
addiu	$3,$3,1284
#APP
# 40 "h264_p1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,-201326592			# 0xfffffffff4000000
addiu	$2,$3,22860
lw	$16,22860($3)
lw	$21,22864($3)
lw	$6,22868($3)
lw	$3,22904($3)
sw	$3,220($sp)
lw	$3,40($2)
slt	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L243
move	$23,$16
.set	macro
.set	reorder

li	$9,15			# 0xf
sw	$9,224($sp)
$L77:
li	$2,-201326592			# 0xfffffffff4000000
addiu	$3,$2,22860
lw	$10,24($3)
sw	$10,228($sp)
lw	$4,16($3)
sw	$4,19660($2)
addiu	$4,$2,20332
sw	$4,140($sp)
addiu	$4,$2,20812
sw	$4,136($sp)
addiu	$4,$2,20392
sw	$4,132($sp)
addiu	$4,$2,20872
sw	$4,128($sp)
li	$4,1			# 0x1
sw	$4,124($sp)
lw	$4,124($sp)
sw	$4,22840($2)
li	$4,321650688			# 0x132c0000
addiu	$7,$4,21292
li	$5,321060864			# 0x13230000
sw	$7,0($5)
addiu	$5,$2,10500
sw	$5,120($sp)
addiu	$5,$2,12744
sw	$5,112($sp)
sw	$5,116($sp)
sw	$0,10500($2)
addiu	$4,$4,10501
li	$2,321191936			# 0x13250000
addiu	$2,$2,84
#APP
# 106 "h264_p1.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
andi	$6,$6,0xff
sw	$6,184($sp)
addiu	$2,$6,4
sll	$2,$2,8
sw	$2,188($sp)
sra	$11,$2,1
sw	$11,192($sp)
xori	$2,$10,0x4
sltu	$2,$2,1
sw	$2,216($sp)
lbu	$2,32($3)
sll	$2,$2,16
lw	$9,216($sp)
or	$2,$9,$2
lw	$3,28($3)
sll	$3,$3,24
or	$3,$2,$3
sw	$3,196($sp)
move	$6,$0
li	$7,715784192			# 0x2aaa0000
ori	$7,$7,0xaaab
li	$8,64			# 0x40
addiu	$10,$sp,32
$L266:
addu	$5,$10,$6
mult	$6,$7
mfhi	$2
sra	$4,$6,31
subu	$4,$2,$4
sll	$2,$4,1
sll	$3,$4,3
subu	$2,$3,$2
subu	$2,$6,$2
sll	$2,$2,4
or	$2,$2,$4
addiu	$6,$6,1
.set	noreorder
.set	nomacro
bne	$6,$8,$L266
sb	$2,0($5)
.set	macro
.set	reorder

li	$7,-201326592			# 0xfffffffff4000000
addiu	$2,$7,14796
sw	$2,16($sp)
addiu	$2,$2,19656
sw	$2,20($sp)
addiu	$2,$7,22912
sw	$2,24($sp)
move	$4,$16
move	$5,$0
lw	$6,184($sp)
.set	noreorder
.set	nomacro
jal	cfg_sde
addiu	$7,$7,13068
.set	macro
.set	reorder

li	$2,321650688			# 0x132c0000
addiu	$2,$2,13069
li	$3,321454080			# 0x13290000
sw	$2,4($3)
addiu	$16,$16,1
sw	$16,148($sp)
lw	$3,184($sp)
.set	noreorder
.set	nomacro
beq	$3,$16,$L244
addiu	$9,$21,1
.set	macro
.set	reorder

sw	$21,180($sp)
$L79:
lw	$10,216($sp)
sll	$2,$10,1
sll	$3,$10,3
addu	$3,$2,$3
sw	$3,232($sp)
move	$fp,$0
li	$2,-201326592			# 0xfffffffff4000000
addiu	$11,$2,13176
sw	$11,160($sp)
addiu	$3,$2,13068
sw	$3,204($sp)
addiu	$9,$2,15100
sw	$9,172($sp)
addiu	$22,$2,14796
addiu	$10,$11,22092
sw	$10,156($sp)
addiu	$11,$11,21276
sw	$11,164($sp)
li	$3,1			# 0x1
sw	$3,168($sp)
lw	$9,228($sp)
xori	$2,$9,0x1
sltu	$2,$2,1
sw	$2,200($sp)
$L158:
li	$2,-201326592			# 0xfffffffff4000000
lw	$4,22832($2)
sll	$3,$23,8
lw	$9,188($sp)
mul	$10,$21,$9
addu	$3,$10,$3
addu	$3,$3,$4
sw	$3,152($sp)
lw	$4,22836($2)
sll	$3,$23,7
lw	$9,192($sp)
mul	$10,$21,$9
addu	$3,$10,$3
addu	$3,$3,$4
sw	$3,144($sp)
lw	$3,112($sp)
sw	$3,116($sp)
li	$3,7			# 0x7
beq	$fp,$3,$L80
lw	$3,116($sp)
addiu	$3,$3,40
sw	$3,112($sp)
lw	$17,120($sp)
addiu	$3,$17,280
sw	$3,120($sp)
sll	$3,$fp,6
sll	$16,$fp,8
subu	$4,$16,$3
addiu	$16,$2,21292
addu	$16,$4,$16
li	$4,80412672			# 0x4cb0000
ori	$4,$4,0xc389
addu	$4,$fp,$4
sll	$4,$4,2
sw	$4,208($sp)
sll	$4,$fp,2
subu	$3,$3,$4
addiu	$4,$2,20332
addu	$4,$3,$4
sw	$4,140($sp)
lw	$4,140($sp)
addiu	$4,$4,60
sw	$4,132($sp)
addiu	$2,$2,20812
addu	$3,$3,$2
sw	$3,136($sp)
lw	$2,136($sp)
addiu	$2,$2,60
$L162:
sw	$2,128($sp)
lw	$11,172($sp)
sw	$11,16($sp)
lw	$3,156($sp)
sw	$3,20($sp)
li	$2,-201326592			# 0xfffffffff4000000
addiu	$2,$2,22912
sw	$2,24($sp)
lw	$4,148($sp)
lw	$5,168($sp)
lw	$6,184($sp)
.set	noreorder
.set	nomacro
jal	cfg_sde
lw	$7,160($sp)
.set	macro
.set	reorder

li	$4,321454080			# 0x13290000
.set	noreorder
.set	nomacro
j	$L82
li	$3,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

$L246:
lw	$2,13064($3)
bne	$2,$0,$L245
$L82:
lw	$2,0($4)
andi	$2,$2,0x8
beq	$2,$0,$L246
$L159:
li	$2,320864256			# 0x13200000
$L264:
addiu	$2,$2,96
move	$3,$0
#APP
# 461 "h264_p1.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
li	$2,321519616			# 0x132a0000
sw	$3,20($2)
li	$3,23130			# 0x5a5a
li	$2,321585152			# 0x132b0000
sw	$3,36($2)
#APP
# 480 "h264_p1.c" 1
nop	#i_nop
# 0 "" 2
# 481 "h264_p1.c" 1
nop	#i_nop
# 0 "" 2
# 482 "h264_p1.c" 1
nop	#i_nop
# 0 "" 2
# 483 "h264_p1.c" 1
nop	#i_nop
# 0 "" 2
# 484 "h264_p1.c" 1
wait	#i_wait
# 0 "" 2
#NO_APP
move	$2,$0
lw	$31,276($sp)
lw	$fp,272($sp)
lw	$23,268($sp)
lw	$22,264($sp)
lw	$21,260($sp)
lw	$20,256($sp)
lw	$19,252($sp)
lw	$18,248($sp)
lw	$17,244($sp)
lw	$16,240($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,280
.set	macro
.set	reorder

$L80:
addiu	$3,$2,12744
sw	$3,112($sp)
lw	$17,120($sp)
addiu	$3,$2,10500
sw	$3,120($sp)
addiu	$3,$2,20752
sw	$3,140($sp)
addiu	$3,$2,20332
sw	$3,132($sp)
addiu	$3,$2,21232
sw	$3,136($sp)
li	$3,321847296			# 0x132f0000
addiu	$3,$3,3648
sw	$3,208($sp)
addiu	$16,$2,22636
.set	noreorder
.set	nomacro
j	$L162
addiu	$2,$2,20812
.set	macro
.set	reorder

$L245:
sw	$0,13064($3)
li	$4,-201326592			# 0xfffffffff4000000
$L83:
lw	$2,22840($4)
lw	$3,124($sp)
addiu	$2,$2,8
sltu	$2,$3,$2
beq	$2,$0,$L83
lbu	$2,3($22)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L165
lw	$9,160($sp)
.set	macro
.set	reorder

andi	$2,$9,0xfffc
li	$3,321650688			# 0x132c0000
or	$2,$2,$3
addiu	$2,$2,1
li	$3,321454080			# 0x13290000
addiu	$3,$3,4
#APP
# 166 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
sw	$0,176($sp)
$L84:
lhu	$2,0($22)
lw	$3,120($sp)
sw	$0,0($3)
lw	$3,0($17)
andi	$3,$2,0x7
.set	noreorder
.set	nomacro
beq	$3,$0,$L85
li	$3,2			# 0x2
.set	macro
.set	reorder

li	$3,-805306368			# 0xffffffffd0000000
addiu	$3,$3,84
sw	$3,4($17)
lw	$3,120($sp)
li	$4,321650688			# 0x132c0000
or	$3,$3,$4
sw	$3,8($17)
li	$3,-402653184			# 0xffffffffe8000000
ori	$3,$3,0xffff
sw	$3,12($17)
li	$3,-838860800			# 0xffffffffce000000
sw	$3,0($17)
$L86:
li	$3,-201326592			# 0xfffffffff4000000
lb	$17,6($22)
lw	$4,19660($3)
addu	$17,$17,$4
sltu	$4,$17,52
bne	$4,$0,$L241
bltz	$17,$L247
addiu	$17,$17,-52
$L241:
sw	$17,19660($3)
$L122:
li	$3,-201326592			# 0xfffffffff4000000
addu	$4,$17,$3
lbu	$20,19664($4)
lbu	$19,19920($4)
lw	$3,176($sp)
lw	$18,124($sp)
bne	$3,$0,$L248
$L125:
lw	$8,140($sp)
lw	$4,136($sp)
lw	$24,124($sp)
sll	$5,$23,2
sll	$6,$23,4
addu	$5,$5,$6
subu	$5,$5,$23
sll	$3,$5,2
li	$5,-201326592			# 0xfffffffff4000000
addiu	$5,$5,22912
addu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$23,$0,$L126
andi	$25,$2,0x3
.set	macro
.set	reorder

lui	$11,%hi(nnz_left_dblk)
lw	$12,%lo(nnz_left_dblk)($11)
lui	$6,%hi(mb_qp_left)
sw	$6,212($sp)
lh	$9,%lo(mb_qp_left)($6)
$L127:
lui	$13,%hi(mb_qp_up)
.set	noreorder
.set	nomacro
bne	$21,$0,$L128
addiu	$13,$13,%lo(mb_qp_up)
.set	macro
.set	reorder

addu	$3,$13,$23
sb	$0,0($3)
$L128:
lw	$3,20($22)
lw	$6,4($5)
li	$10,251658240			# 0xf000000
and	$10,$6,$10
andi	$7,$3,0x80
sll	$14,$7,14
andi	$6,$3,0x20
sll	$6,$6,15
or	$7,$14,$6
andi	$6,$3,0x2000
sll	$6,$6,9
or	$6,$7,$6
andi	$7,$3,0x8000
sll	$7,$7,8
or	$6,$6,$7
sw	$6,%lo(nnz_left_dblk)($11)
andi	$7,$3,0xc0c
sll	$7,$7,2
andi	$6,$3,0x3030
sra	$6,$6,2
or	$6,$7,$6
andi	$3,$3,0xc3c3
or	$6,$6,$3
sra	$10,$10,8
or	$6,$6,$10
or	$12,$12,$6
andi	$3,$2,0x38
srl	$3,$3,2
lui	$11,%hi(mv_msk_table.1766)
addiu	$11,$11,%lo(mv_msk_table.1766)
addu	$3,$3,$11
lh	$10,0($3)
sll	$10,$10,24
or	$12,$12,$10
sw	$12,16($8)
sw	$12,16($4)
lw	$3,196($sp)
sw	$3,8($8)
sw	$3,8($4)
lw	$10,200($sp)
.set	noreorder
.set	nomacro
bne	$10,$0,$L173
lw	$11,232($sp)
.set	macro
.set	reorder

lhu	$14,44($22)
addu	$14,$14,$11
addiu	$14,$14,11
sll	$14,$14,24
$L129:
lw	$3,0($5)
addu	$6,$13,$23
lbu	$10,0($6)
li	$11,-201326592			# 0xfffffffff4000000
addiu	$11,$11,19664
addu	$31,$11,$10
lb	$6,0($31)
addu	$6,$20,$6
addiu	$6,$6,1
sra	$6,$6,1
sll	$12,$19,18
or	$15,$12,$20
lb	$12,256($31)
addu	$12,$19,$12
addiu	$12,$12,1
sra	$12,$12,1
sll	$7,$12,12
or	$12,$15,$7
addu	$11,$11,$9
lb	$7,256($11)
addu	$7,$19,$7
addiu	$7,$7,1
sra	$7,$7,1
sll	$7,$7,24
or	$7,$12,$7
lb	$12,0($11)
addu	$12,$20,$12
addiu	$12,$12,1
sra	$12,$12,1
sll	$12,$12,6
or	$7,$7,$12
sw	$7,20($8)
sw	$7,20($4)
addu	$10,$10,$17
addiu	$10,$10,1
sra	$10,$10,1
sll	$7,$17,6
or	$10,$10,$7
sll	$6,$6,18
or	$10,$10,$6
addu	$6,$17,$9
addiu	$6,$6,1
sra	$6,$6,1
sll	$6,$6,12
or	$10,$10,$6
or	$10,$10,$14
sw	$10,24($8)
sw	$10,24($4)
lui	$15,%hi(left_mb_type)
lw	$6,%lo(left_mb_type)($15)
andi	$11,$6,0x28
and	$11,$11,$2
sltu	$11,$0,$11
andi	$12,$2,0x8
andi	$12,$12,0xffff
and	$7,$3,$12
li	$14,16			# 0x10
move	$9,$0
movn	$9,$14,$7
move	$7,$9
andi	$10,$2,0x20
li	$9,4			# 0x4
movz	$9,$0,$10
move	$10,$9
andi	$9,$2,0x10
li	$31,64			# 0x40
movz	$31,$0,$9
or	$7,$7,$11
or	$7,$7,$10
or	$9,$7,$31
lw	$10,220($sp)
.set	noreorder
.set	nomacro
beq	$10,$0,$L178
movn	$14,$0,$21
.set	macro
.set	reorder

andi	$7,$2,0xc00
sra	$7,$7,9
sra	$12,$12,3
or	$7,$7,$12
sll	$7,$7,1
lui	$10,%hi(edg_msk_table.1767)
addiu	$10,$10,%lo(edg_msk_table.1767)
addu	$7,$7,$10
lh	$12,0($7)
sltu	$7,$23,1
or	$12,$12,$7
or	$14,$12,$14
sll	$14,$14,16
$L134:
sll	$9,$9,24
or	$9,$14,$9
sltu	$7,$0,$25
sll	$7,$7,4
andi	$6,$6,0x3
sltu	$6,$0,$6
sll	$6,$6,5
or	$6,$7,$6
andi	$3,$3,0x3
sltu	$3,$0,$3
sll	$3,$3,3
ori	$7,$3,0x445
or	$7,$7,$6
or	$7,$7,$9
sw	$7,28($8)
sw	$24,56($8)
sw	$24,56($4)
ori	$3,$3,0x345
or	$3,$3,$6
or	$3,$3,$9
sw	$3,28($4)
lw	$11,200($sp)
bne	$11,$0,$L136
.set	noreorder
.set	nomacro
bne	$25,$0,$L137
lui	$3,%hi(ref_left_dblk)
.set	macro
.set	reorder

sll	$3,$fp,5
sll	$6,$fp,8
subu	$4,$6,$3
li	$3,-201326592			# 0xfffffffff4000000
addiu	$3,$3,32640
addu	$3,$4,$3
lhu	$6,8($5)
lw	$8,28($22)
sw	$8,0($3)
lui	$4,%hi(ref_left_dblk)
lw	$7,%lo(ref_left_dblk)($4)
or	$6,$6,$7
sw	$6,4($3)
andi	$6,$8,0xff00
sll	$6,$6,8
li	$7,-16777216			# 0xffffffffff000000
and	$8,$8,$7
or	$8,$6,$8
sw	$8,%lo(ref_left_dblk)($4)
lw	$9,216($sp)
.set	noreorder
.set	nomacro
bne	$9,$0,$L138
addiu	$4,$4,%lo(ref_left_dblk)
.set	macro
.set	reorder

addiu	$3,$3,8
$L139:
lhu	$7,46($22)
andi	$9,$7,0x3
addiu	$4,$3,4
andi	$8,$7,0x30
sll	$10,$8,4
andi	$8,$7,0xc
sll	$6,$8,2
or	$8,$10,$6
andi	$6,$7,0xc0
sll	$6,$6,10
or	$6,$8,$6
andi	$7,$7,0x300
sll	$7,$7,16
or	$6,$6,$7
or	$6,$6,$9
sw	$6,0($3)
addiu	$6,$5,12
#APP
# 212 "h264_p1_dblk.c" 1
.word	0b01110000110000000000000001010000	#S32LDD XR1,$6,0
# 0 "" 2
# 213 "h264_p1_dblk.c" 1
.word	0b01110000110000000000010010010000	#S32LDD XR2,$6,4
# 0 "" 2
# 214 "h264_p1_dblk.c" 1
.word	0b01110000110000000000100011010000	#S32LDD XR3,$6,8
# 0 "" 2
# 215 "h264_p1_dblk.c" 1
.word	0b01110000110000000000110100010000	#S32LDD XR4,$6,12
# 0 "" 2
#NO_APP
addiu	$6,$22,64
#APP
# 218 "h264_p1_dblk.c" 1
.word	0b01110000110000000000001001010000	#S32LDD XR9,$6,0
# 0 "" 2
# 219 "h264_p1_dblk.c" 1
.word	0b01110000110000000000011010010000	#S32LDD XR10,$6,4
# 0 "" 2
# 220 "h264_p1_dblk.c" 1
.word	0b01110000110000000000101011010000	#S32LDD XR11,$6,8
# 0 "" 2
# 221 "h264_p1_dblk.c" 1
.word	0b01110000110000000000111100010000	#S32LDD XR12,$6,12
# 0 "" 2
#NO_APP
lui	$6,%hi(mv_left_dblk)
addiu	$6,$6,%lo(mv_left_dblk)
#APP
# 224 "h264_p1_dblk.c" 1
.word	0b01110000110000000000000101010000	#S32LDD XR5,$6,0
# 0 "" 2
# 225 "h264_p1_dblk.c" 1
.word	0b01110000110000000000010110010000	#S32LDD XR6,$6,4
# 0 "" 2
# 226 "h264_p1_dblk.c" 1
.word	0b01110000110000000000100111010000	#S32LDD XR7,$6,8
# 0 "" 2
# 227 "h264_p1_dblk.c" 1
.word	0b01110000110000000000111000010000	#S32LDD XR8,$6,12
# 0 "" 2
# 230 "h264_p1_dblk.c" 1
.word	0b01110000110000000000001001010001	#S32STD XR9,$6,0
# 0 "" 2
# 231 "h264_p1_dblk.c" 1
.word	0b01110000110000000000011010010001	#S32STD XR10,$6,4
# 0 "" 2
# 232 "h264_p1_dblk.c" 1
.word	0b01110000110000000000101011010001	#S32STD XR11,$6,8
# 0 "" 2
# 233 "h264_p1_dblk.c" 1
.word	0b01110000110000000000111100010001	#S32STD XR12,$6,12
# 0 "" 2
# 235 "h264_p1_dblk.c" 1
.word	0b01110000100000000000000001010001	#S32STD XR1,$4,0
# 0 "" 2
# 236 "h264_p1_dblk.c" 1
.word	0b01110000100000000000010010010001	#S32STD XR2,$4,4
# 0 "" 2
# 237 "h264_p1_dblk.c" 1
.word	0b01110000100000000000100011010001	#S32STD XR3,$4,8
# 0 "" 2
# 238 "h264_p1_dblk.c" 1
.word	0b01110000100000000000110100010001	#S32STD XR4,$4,12
# 0 "" 2
# 241 "h264_p1_dblk.c" 1
.word	0b01110000100000000001000101010001	#S32STD XR5,$4,16
# 0 "" 2
# 242 "h264_p1_dblk.c" 1
.word	0b01110000100000000001010110010001	#S32STD XR6,$4,20
# 0 "" 2
# 243 "h264_p1_dblk.c" 1
.word	0b01110000100000000001100111010001	#S32STD XR7,$4,24
# 0 "" 2
# 244 "h264_p1_dblk.c" 1
.word	0b01110000100000000001111000010001	#S32STD XR8,$4,28
# 0 "" 2
#NO_APP
lw	$10,216($sp)
.set	noreorder
.set	nomacro
beq	$10,$0,$L140
lhu	$6,44($22)
.set	macro
.set	reorder

addiu	$4,$3,36
addiu	$5,$5,28
#APP
# 254 "h264_p1_dblk.c" 1
.word	0b01110000101000000000000001010000	#S32LDD XR1,$5,0
# 0 "" 2
# 255 "h264_p1_dblk.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 256 "h264_p1_dblk.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 257 "h264_p1_dblk.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
#NO_APP
addiu	$5,$22,128
#APP
# 260 "h264_p1_dblk.c" 1
.word	0b01110000101000000000001001010000	#S32LDD XR9,$5,0
# 0 "" 2
# 261 "h264_p1_dblk.c" 1
.word	0b01110000101000000000011010010000	#S32LDD XR10,$5,4
# 0 "" 2
# 262 "h264_p1_dblk.c" 1
.word	0b01110000101000000000101011010000	#S32LDD XR11,$5,8
# 0 "" 2
# 263 "h264_p1_dblk.c" 1
.word	0b01110000101000000000111100010000	#S32LDD XR12,$5,12
# 0 "" 2
#NO_APP
lui	$5,%hi(mv_left_dblk+16)
addiu	$5,$5,%lo(mv_left_dblk+16)
#APP
# 266 "h264_p1_dblk.c" 1
.word	0b01110000101000000000000101010000	#S32LDD XR5,$5,0
# 0 "" 2
# 267 "h264_p1_dblk.c" 1
.word	0b01110000101000000000010110010000	#S32LDD XR6,$5,4
# 0 "" 2
# 268 "h264_p1_dblk.c" 1
.word	0b01110000101000000000100111010000	#S32LDD XR7,$5,8
# 0 "" 2
# 269 "h264_p1_dblk.c" 1
.word	0b01110000101000000000111000010000	#S32LDD XR8,$5,12
# 0 "" 2
# 272 "h264_p1_dblk.c" 1
.word	0b01110000101000000000001001010001	#S32STD XR9,$5,0
# 0 "" 2
# 273 "h264_p1_dblk.c" 1
.word	0b01110000101000000000011010010001	#S32STD XR10,$5,4
# 0 "" 2
# 274 "h264_p1_dblk.c" 1
.word	0b01110000101000000000101011010001	#S32STD XR11,$5,8
# 0 "" 2
# 275 "h264_p1_dblk.c" 1
.word	0b01110000101000000000111100010001	#S32STD XR12,$5,12
# 0 "" 2
# 277 "h264_p1_dblk.c" 1
.word	0b01110000100000000000000001010001	#S32STD XR1,$4,0
# 0 "" 2
# 278 "h264_p1_dblk.c" 1
.word	0b01110000100000000000010010010001	#S32STD XR2,$4,4
# 0 "" 2
# 279 "h264_p1_dblk.c" 1
.word	0b01110000100000000000100011010001	#S32STD XR3,$4,8
# 0 "" 2
# 280 "h264_p1_dblk.c" 1
.word	0b01110000100000000000110100010001	#S32STD XR4,$4,12
# 0 "" 2
# 282 "h264_p1_dblk.c" 1
.word	0b01110000100000000001000101010001	#S32STD XR5,$4,16
# 0 "" 2
# 283 "h264_p1_dblk.c" 1
.word	0b01110000100000000001010110010001	#S32STD XR6,$4,20
# 0 "" 2
# 284 "h264_p1_dblk.c" 1
.word	0b01110000100000000001100111010001	#S32STD XR7,$4,24
# 0 "" 2
# 285 "h264_p1_dblk.c" 1
.word	0b01110000100000000001111000010001	#S32STD XR8,$4,28
# 0 "" 2
#NO_APP
addiu	$5,$22,172
addiu	$3,$3,64
.set	noreorder
.set	nomacro
beq	$6,$0,$L136
addiu	$4,$6,-1
.set	macro
.set	reorder

li	$6,-1			# 0xffffffffffffffff
$L142:
#APP
# 294 "h264_p1_dblk.c" 1
.word	0b01110000101000000000010001010100	#S32LDI XR1,$5,4
# 0 "" 2
# 295 "h264_p1_dblk.c" 1
.word	0b01110000011000000000010001010101	#S32SDI XR1,$3,4
# 0 "" 2
#NO_APP
addiu	$4,$4,-1
bne	$4,$6,$L142
$L136:
addu	$13,$13,$23
$L262:
sb	$17,0($13)
lw	$11,212($sp)
sh	$17,%lo(mb_qp_left)($11)
sw	$2,%lo(left_mb_type)($15)
move	$4,$23
lw	$5,224($sp)
move	$6,$22
li	$7,-201326592			# 0xfffffffff4000000
.set	noreorder
.set	nomacro
jal	backup_sde_bac_aux
addiu	$7,$7,22912
.set	macro
.set	reorder

lw	$3,176($sp)
bne	$3,$0,$L249
$L144:
lw	$9,144($sp)
$L265:
lw	$10,192($sp)
subu	$2,$9,$10
addiu	$2,$2,64
lw	$11,152($sp)
addiu	$5,$11,-244
addiu	$4,$9,-124
addiu	$3,$16,4
#APP
# 349 "h264_p1.c" 1
sw	 $11,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$16,20
#APP
# 350 "h264_p1.c" 1
sw	 $9,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$9,8
addiu	$6,$16,36
#APP
# 351 "h264_p1.c" 1
sw	 $3,0($6)	#i_sw
# 0 "" 2
#NO_APP
lw	$9,188($sp)
subu	$3,$11,$9
addiu	$3,$3,192
addiu	$6,$16,52
#APP
# 352 "h264_p1.c" 1
sw	 $3,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$16,68
#APP
# 353 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,8
addiu	$3,$16,84
#APP
# 354 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$2,7			# 0x7
beq	$fp,$2,$L250
.set	noreorder
.set	nomacro
bne	$fp,$0,$L260
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

li	$2,1			# 0x1
lw	$11,168($sp)
beq	$11,$2,$L251
li	$2,321781760			# 0x132e0000
sw	$2,100($16)
sw	$2,116($16)
sw	$2,132($16)
li	$2,-201326592			# 0xfffffffff4000000
$L260:
lw	$2,22872($2)
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
beq	$2,$21,$L252
addiu	$3,$16,12
.set	macro
.set	reorder

li	$2,1048576			# 0x100000
addiu	$2,$2,192
#APP
# 377 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$2,524288			# 0x80000
addiu	$2,$2,32
addiu	$3,$16,28
#APP
# 378 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$16,$16,44
#APP
# 379 "h264_p1.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
$L149:
lw	$2,124($sp)
addiu	$2,$2,1
lw	$9,208($sp)
sw	$2,0($9)
lw	$4,116($sp)
lw	$2,112($sp)
li	$3,-16777216			# 0xffffffffff000000
and	$3,$2,$3
li	$5,-201326592			# 0xfffffffff4000000
.set	noreorder
.set	nomacro
beq	$3,$5,$L253
li	$3,321650688			# 0x132c0000
.set	macro
.set	reorder

$L150:
sw	$2,12($4)
lw	$2,116($sp)
lhu	$3,24($22)
sh	$3,16($2)
lw	$2,116($sp)
lw	$3,20($22)
sw	$3,0($2)
lw	$3,116($sp)
li	$2,-16777216			# 0xffffffffff000000
lw	$10,164($sp)
and	$2,$10,$2
li	$4,-201326592			# 0xfffffffff4000000
.set	noreorder
.set	nomacro
beq	$2,$4,$L254
andi	$4,$10,0xffff
.set	macro
.set	reorder

$L151:
lw	$11,164($sp)
sw	$11,8($3)
lw	$2,116($sp)
lbu	$3,36($22)
sh	$3,24($2)
lw	$2,116($sp)
lw	$3,28($22)
sw	$3,28($2)
lw	$2,116($sp)
lw	$3,32($22)
sw	$3,32($2)
lw	$2,116($sp)
lb	$3,2($22)
sh	$3,26($2)
lw	$2,116($sp)
sb	$18,18($2)
lw	$3,116($sp)
xori	$2,$fp,0x7
li	$4,128			# 0x80
movz	$4,$0,$2
sb	$4,19($3)
lw	$5,116($sp)
addiu	$2,$sp,32
addu	$20,$2,$20
lbu	$3,0($20)
andi	$3,$3,0x7f
sll	$4,$3,7
addu	$19,$2,$19
lbu	$2,0($19)
andi	$2,$2,0x7f
sll	$2,$2,14
or	$3,$4,$2
addiu	$4,$sp,32
addu	$17,$4,$17
lbu	$2,0($17)
andi	$2,$2,0x7f
or	$2,$3,$2
sw	$2,4($5)
addiu	$3,$18,15
slt	$2,$18,0
movz	$3,$18,$2
andi	$2,$3,0x10
andi	$18,$18,0xf
or	$18,$2,$18
li	$2,320864256			# 0x13200000
addiu	$2,$2,108
#APP
# 398 "h264_p1.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$9,176($sp)
.set	noreorder
.set	nomacro
bne	$9,$0,$L255
li	$2,7			# 0x7
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$fp,$2,$L180
lw	$10,156($sp)
.set	macro
.set	reorder

addiu	$3,$10,816
addiu	$fp,$fp,1
$L156:
lw	$2,124($sp)
addiu	$2,$2,1
sw	$2,124($sp)
lw	$11,148($sp)
addiu	$2,$11,1
lw	$9,184($sp)
.set	noreorder
.set	nomacro
beq	$9,$2,$L256
lw	$10,180($sp)
.set	macro
.set	reorder

lw	$5,180($sp)
$L157:
lw	$11,168($sp)
addiu	$11,$11,1
sw	$11,168($sp)
lw	$23,148($sp)
lw	$21,180($sp)
lw	$9,156($sp)
sw	$9,164($sp)
lw	$4,204($sp)
lw	$10,160($sp)
sw	$10,204($sp)
sw	$4,160($sp)
move	$4,$22
lw	$22,172($sp)
sw	$4,172($sp)
sw	$3,156($sp)
sw	$5,180($sp)
.set	noreorder
.set	nomacro
j	$L158
sw	$2,148($sp)
.set	macro
.set	reorder

$L178:
.set	noreorder
.set	nomacro
j	$L134
li	$14,-65536			# 0xffffffffffff0000
.set	macro
.set	reorder

$L173:
.set	noreorder
.set	nomacro
j	$L129
move	$14,$0
.set	macro
.set	reorder

$L126:
lui	$7,%hi(mb_qp_left)
sw	$7,212($sp)
sh	$0,%lo(mb_qp_left)($7)
lui	$11,%hi(nnz_left_dblk)
sw	$0,%lo(nnz_left_dblk)($11)
lui	$3,%hi(ref_left_dblk)
sw	$0,%lo(ref_left_dblk)($3)
addiu	$3,$3,%lo(ref_left_dblk)
sw	$0,4($3)
lui	$3,%hi(mv_left_dblk)
sw	$0,%lo(mv_left_dblk)($3)
addiu	$3,$3,%lo(mv_left_dblk)
sw	$0,4($3)
sw	$0,8($3)
sw	$0,12($3)
sw	$0,16($3)
sw	$0,20($3)
sw	$0,24($3)
sw	$0,28($3)
move	$9,$0
.set	noreorder
.set	nomacro
j	$L127
move	$12,$0
.set	macro
.set	reorder

$L85:
lw	$11,228($sp)
beq	$11,$3,$L257
addiu	$3,$22,176
sw	$3,96($sp)
lhu	$3,44($22)
srl	$3,$3,1
addiu	$3,$3,44
sll	$3,$3,2
addu	$3,$22,$3
sw	$3,100($sp)
$L88:
andi	$4,$2,0x8
.set	noreorder
.set	nomacro
beq	$4,$0,$L89
addiu	$3,$17,4
.set	macro
.set	reorder

andi	$4,$2,0x1000
andi	$4,$4,0xffff
sw	$4,104($sp)
andi	$5,$2,0x4000
sw	$5,108($sp)
andi	$2,$2,0x5000
xori	$2,$2,0x5000
sltu	$2,$2,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L166
sll	$2,$2,31
.set	macro
.set	reorder

lw	$6,96($sp)
lw	$3,0($6)
sw	$3,4($17)
addiu	$3,$17,12
lbu	$4,28($22)
sll	$4,$4,12
andi	$4,$4,0xffff
li	$5,65536			# 0x10000
addiu	$5,$5,240
or	$4,$4,$5
or	$4,$4,$2
lhu	$5,2($6)
andi	$5,$5,0x7
sll	$5,$5,20
or	$4,$4,$5
sw	$4,8($17)
li	$4,1			# 0x1
$L90:
lw	$5,108($sp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L92
lw	$7,100($sp)
.set	macro
.set	reorder

lw	$5,0($7)
sw	$5,0($3)
lbu	$5,32($22)
sll	$5,$5,12
andi	$5,$5,0xffff
li	$6,1073807360			# 0x40010000
addiu	$6,$6,240
or	$5,$5,$6
lhu	$6,2($7)
andi	$6,$6,0x7
sll	$6,$6,20
or	$5,$5,$6
or	$2,$5,$2
sw	$2,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L92:
lw	$2,-4($3)
li	$5,33554432			# 0x2000000
or	$2,$2,$5
sw	$2,-4($3)
li	$2,-805306368			# 0xffffffffd0000000
addiu	$2,$2,84
sw	$2,0($3)
lw	$2,120($sp)
li	$5,321650688			# 0x132c0000
or	$2,$2,$5
sw	$2,4($3)
li	$2,-402653184			# 0xffffffffe8000000
ori	$2,$2,0xffff
sw	$2,8($3)
andi	$2,$23,0xff
li	$3,-838860800			# 0xffffffffce000000
or	$3,$2,$3
sll	$2,$21,8
andi	$2,$2,0xffff
or	$2,$3,$2
andi	$4,$4,0x7f
sll	$4,$4,16
or	$4,$2,$4
sw	$4,0($17)
.set	noreorder
.set	nomacro
j	$L86
lhu	$2,0($22)
.set	macro
.set	reorder

$L137:
sw	$0,%lo(ref_left_dblk)($3)
addiu	$3,$3,%lo(ref_left_dblk)
sw	$0,4($3)
addu	$13,$13,$23
sb	$17,0($13)
lw	$11,212($sp)
sh	$17,%lo(mb_qp_left)($11)
sw	$2,%lo(left_mb_type)($15)
move	$4,$23
lw	$5,224($sp)
move	$6,$22
li	$7,-201326592			# 0xfffffffff4000000
.set	noreorder
.set	nomacro
jal	backup_sde_bac_aux
addiu	$7,$7,22912
.set	macro
.set	reorder

lw	$3,176($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L265
lw	$9,144($sp)
.set	macro
.set	reorder

$L249:
lw	$3,140($sp)
lw	$2,124($sp)
addiu	$2,$2,1
sw	$2,56($3)
lw	$3,136($sp)
lw	$2,124($sp)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
j	$L144
sw	$2,56($3)
.set	macro
.set	reorder

$L254:
li	$2,321650688			# 0x132c0000
or	$2,$4,$2
.set	noreorder
.set	nomacro
j	$L151
sw	$2,164($sp)
.set	macro
.set	reorder

$L253:
andi	$2,$2,0xffff
.set	noreorder
.set	nomacro
j	$L150
or	$2,$2,$3
.set	macro
.set	reorder

$L252:
li	$2,1048576			# 0x100000
addiu	$2,$2,256
#APP
# 373 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$2,524288			# 0x80000
addiu	$2,$2,64
addiu	$3,$16,28
#APP
# 374 "h264_p1.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$16,$16,44
#APP
# 375 "h264_p1.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
j	$L149
$L250:
sw	$5,100($16)
sw	$4,116($16)
lw	$10,144($sp)
addiu	$2,$10,-116
sw	$2,132($16)
.set	noreorder
.set	nomacro
j	$L260
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

$L248:
.set	noreorder
.set	nomacro
j	$L125
addiu	$18,$18,1
.set	macro
.set	reorder

$L256:
addiu	$5,$10,1
.set	noreorder
.set	nomacro
j	$L157
move	$2,$0
.set	macro
.set	reorder

$L180:
li	$3,-201326592			# 0xfffffffff4000000
ori	$3,$3,0x8694
.set	noreorder
.set	nomacro
j	$L156
move	$fp,$0
.set	macro
.set	reorder

$L89:
andi	$4,$2,0x10
.set	noreorder
.set	nomacro
beq	$4,$0,$L93
andi	$4,$2,0x20
.set	macro
.set	reorder

andi	$4,$2,0x1000
sw	$4,104($sp)
andi	$4,$2,0x4000
sw	$4,108($sp)
andi	$5,$2,0x5000
xori	$5,$5,0x5000
sltu	$5,$5,1
sll	$5,$5,31
move	$7,$0
move	$9,$0
move	$4,$0
addiu	$15,$sp,104
li	$8,65536			# 0x10000
addiu	$8,$8,176
or	$8,$5,$8
li	$10,8			# 0x8
$L95:
addu	$5,$15,$7
lw	$5,0($5)
.set	noreorder
.set	nomacro
beq	$5,$0,$L94
addiu	$6,$sp,96
.set	macro
.set	reorder

addu	$5,$6,$7
lw	$11,0($5)
lw	$5,0($11)
sw	$5,0($3)
sll	$5,$9,30
or	$5,$8,$5
addu	$6,$22,$7
lbu	$6,28($6)
sll	$6,$6,12
andi	$6,$6,0xffff
or	$5,$5,$6
lhu	$6,2($11)
andi	$6,$6,0x7
sll	$6,$6,20
or	$5,$5,$6
sw	$5,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L94:
addiu	$7,$7,4
.set	noreorder
.set	nomacro
bne	$7,$10,$L95
addiu	$9,$9,1
.set	macro
.set	reorder

lw	$5,96($sp)
addiu	$5,$5,4
sw	$5,96($sp)
lw	$5,100($sp)
addiu	$5,$5,4
sw	$5,100($sp)
andi	$5,$2,0x2000
sw	$5,104($sp)
andi	$5,$2,0x8000
sw	$5,108($sp)
andi	$2,$2,0xa000
xori	$2,$2,0xa000
sltu	$2,$2,1
sll	$2,$2,31
move	$6,$0
move	$8,$0
li	$7,65536			# 0x10000
addiu	$7,$7,2224
or	$7,$2,$7
li	$9,8			# 0x8
addu	$2,$15,$6
$L261:
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L96
addiu	$10,$sp,96
.set	macro
.set	reorder

addu	$2,$10,$6
lw	$10,0($2)
lw	$2,0($10)
sw	$2,0($3)
sll	$2,$8,30
or	$2,$7,$2
addu	$5,$22,$6
lbu	$5,30($5)
sll	$5,$5,12
andi	$5,$5,0xffff
or	$2,$2,$5
lhu	$5,2($10)
andi	$5,$5,0x7
sll	$5,$5,20
or	$2,$2,$5
sw	$2,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L96:
addiu	$6,$6,4
.set	noreorder
.set	nomacro
beq	$6,$9,$L92
addiu	$8,$8,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L261
addu	$2,$15,$6
.set	macro
.set	reorder

$L140:
addiu	$5,$22,108
addiu	$3,$3,32
.set	noreorder
.set	nomacro
beq	$6,$0,$L136
addiu	$4,$6,-1
.set	macro
.set	reorder

li	$6,-1			# 0xffffffffffffffff
$L143:
#APP
# 304 "h264_p1_dblk.c" 1
.word	0b01110000101000000000010001010100	#S32LDI XR1,$5,4
# 0 "" 2
# 305 "h264_p1_dblk.c" 1
.word	0b01110000011000000000010001010101	#S32SDI XR1,$3,4
# 0 "" 2
#NO_APP
addiu	$4,$4,-1
bne	$4,$6,$L143
.set	noreorder
.set	nomacro
j	$L262
addu	$13,$13,$23
.set	macro
.set	reorder

$L138:
lhu	$8,10($5)
lw	$6,32($22)
sw	$6,8($3)
lw	$9,4($4)
or	$8,$8,$9
sw	$8,12($3)
addiu	$3,$3,16
andi	$8,$6,0xff00
sll	$8,$8,8
and	$6,$6,$7
or	$6,$8,$6
.set	noreorder
.set	nomacro
j	$L139
sw	$6,4($4)
.set	macro
.set	reorder

$L257:
addiu	$3,$22,112
.set	noreorder
.set	nomacro
j	$L88
sw	$3,96($sp)
.set	macro
.set	reorder

$L247:
addiu	$17,$17,52
.set	noreorder
.set	nomacro
j	$L122
sw	$17,19660($3)
.set	macro
.set	reorder

$L93:
.set	noreorder
.set	nomacro
beq	$4,$0,$L98
addiu	$20,$22,36
.set	macro
.set	reorder

andi	$4,$2,0x1000
sw	$4,104($sp)
andi	$4,$2,0x4000
sw	$4,108($sp)
andi	$5,$2,0x5000
xori	$5,$5,0x5000
sltu	$5,$5,1
sll	$5,$5,31
move	$7,$0
move	$8,$0
move	$4,$0
addiu	$15,$sp,104
li	$11,65536			# 0x10000
addiu	$11,$11,224
or	$11,$5,$11
li	$10,1			# 0x1
$L100:
addu	$5,$15,$7
lw	$5,0($5)
.set	noreorder
.set	nomacro
beq	$5,$0,$L99
addiu	$6,$sp,96
.set	macro
.set	reorder

addu	$5,$6,$7
lw	$9,0($5)
lw	$5,0($9)
sw	$5,0($3)
sll	$5,$8,30
or	$5,$11,$5
addu	$6,$22,$7
lbu	$6,28($6)
sll	$6,$6,12
andi	$6,$6,0xffff
or	$5,$5,$6
lhu	$6,2($9)
andi	$6,$6,0x7
sll	$6,$6,20
or	$5,$5,$6
sw	$5,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L99:
.set	noreorder
.set	nomacro
bne	$8,$10,$L167
addiu	$7,$7,4
.set	macro
.set	reorder

lw	$5,96($sp)
addiu	$5,$5,4
sw	$5,96($sp)
lw	$5,100($sp)
addiu	$5,$5,4
sw	$5,100($sp)
andi	$5,$2,0x2000
sw	$5,104($sp)
andi	$5,$2,0x8000
sw	$5,108($sp)
andi	$2,$2,0xa000
xori	$2,$2,0xa000
sltu	$2,$2,1
sll	$2,$2,31
move	$6,$0
move	$7,$0
li	$11,65536			# 0x10000
addiu	$11,$11,736
or	$11,$2,$11
li	$10,1			# 0x1
$L102:
addu	$2,$15,$6
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L101
addiu	$9,$sp,96
.set	macro
.set	reorder

addu	$2,$9,$6
lw	$9,0($2)
lw	$2,0($9)
sw	$2,0($3)
sll	$2,$7,30
or	$2,$11,$2
addu	$5,$22,$6
lbu	$5,29($5)
sll	$5,$5,12
andi	$5,$5,0xffff
or	$2,$2,$5
lhu	$5,2($9)
andi	$5,$5,0x7
sll	$5,$5,20
or	$2,$2,$5
sw	$2,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L101:
.set	noreorder
.set	nomacro
beq	$7,$10,$L92
addiu	$6,$6,4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L102
move	$7,$8
.set	macro
.set	reorder

$L166:
.set	noreorder
.set	nomacro
j	$L90
move	$4,$0
.set	macro
.set	reorder

$L98:
move	$19,$22
move	$18,$0
move	$4,$0
addiu	$15,$sp,104
li	$2,65536			# 0x10000
li	$25,1			# 0x1
li	$24,8			# 0x8
li	$31,4			# 0x4
addiu	$14,$2,160
lh	$5,0($20)
andi	$2,$5,0x1000
sw	$2,104($sp)
andi	$2,$5,0x4000
sw	$2,108($sp)
andi	$2,$5,0x5000
xori	$2,$2,0x5000
andi	$6,$5,0x8
.set	noreorder
.set	nomacro
beq	$6,$0,$L103
sltu	$2,$2,1
.set	macro
.set	reorder

$L258:
andi	$5,$18,0x2
sll	$5,$5,10
or	$5,$5,$14
sll	$2,$2,31
or	$2,$5,$2
andi	$7,$18,0x1
sll	$7,$7,1
sll	$7,$7,8
or	$7,$2,$7
move	$6,$0
move	$8,$0
$L105:
addu	$2,$15,$6
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L104
addiu	$10,$sp,96
.set	macro
.set	reorder

addu	$2,$10,$6
lw	$9,0($2)
lw	$2,0($9)
sw	$2,0($3)
sll	$2,$8,30
or	$2,$2,$7
addu	$5,$19,$6
lbu	$5,28($5)
sll	$5,$5,12
andi	$5,$5,0xffff
or	$2,$2,$5
lhu	$5,2($9)
andi	$5,$5,0x7
sll	$5,$5,20
or	$2,$2,$5
sw	$2,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L104:
addiu	$6,$6,4
.set	noreorder
.set	nomacro
bne	$6,$24,$L105
addiu	$8,$8,1
.set	macro
.set	reorder

lw	$2,96($sp)
addiu	$2,$2,4
sw	$2,96($sp)
lw	$2,100($sp)
addiu	$2,$2,4
sw	$2,100($sp)
$L106:
addiu	$18,$18,1
$L263:
addiu	$20,$20,2
.set	noreorder
.set	nomacro
beq	$18,$31,$L92
addiu	$19,$19,1
.set	macro
.set	reorder

lh	$5,0($20)
andi	$2,$5,0x1000
sw	$2,104($sp)
andi	$2,$5,0x4000
sw	$2,108($sp)
andi	$2,$5,0x5000
xori	$2,$2,0x5000
andi	$6,$5,0x8
.set	noreorder
.set	nomacro
bne	$6,$0,$L258
sltu	$2,$2,1
.set	macro
.set	reorder

$L103:
andi	$6,$5,0x10
.set	noreorder
.set	nomacro
beq	$6,$0,$L107
andi	$5,$5,0x20
.set	macro
.set	reorder

sll	$2,$2,31
andi	$10,$18,0x2
andi	$7,$18,0x1
sll	$7,$7,1
sll	$7,$7,8
sll	$9,$10,10
li	$11,65536			# 0x10000
ori	$11,$11,0x60
or	$9,$9,$11
or	$9,$9,$2
or	$9,$9,$7
move	$8,$0
move	$6,$0
$L109:
addu	$5,$15,$8
lw	$5,0($5)
.set	noreorder
.set	nomacro
beq	$5,$0,$L108
addiu	$11,$sp,96
.set	macro
.set	reorder

addu	$5,$11,$8
lw	$11,0($5)
lw	$5,0($11)
sw	$5,0($3)
sll	$5,$6,30
or	$12,$5,$9
addu	$5,$19,$8
lbu	$5,28($5)
sll	$5,$5,12
andi	$5,$5,0xffff
or	$5,$12,$5
lhu	$11,2($11)
andi	$11,$11,0x7
sll	$11,$11,20
or	$5,$5,$11
sw	$5,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L108:
.set	noreorder
.set	nomacro
bne	$6,$25,$L169
addiu	$8,$8,4
.set	macro
.set	reorder

lw	$9,96($sp)
addiu	$5,$9,4
sw	$5,96($sp)
lw	$8,100($sp)
addiu	$5,$8,4
sw	$5,100($sp)
addiu	$5,$10,1
sll	$5,$5,10
li	$10,65536			# 0x10000
ori	$10,$10,0x60
or	$5,$5,$10
or	$2,$2,$5
or	$7,$7,$2
move	$5,$0
move	$10,$0
$L111:
addu	$2,$15,$5
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L110
addiu	$11,$sp,96
.set	macro
.set	reorder

addu	$2,$11,$5
lw	$11,0($2)
lw	$2,0($11)
sw	$2,0($3)
sll	$2,$10,30
or	$12,$2,$7
addu	$2,$19,$5
lbu	$2,28($2)
sll	$2,$2,12
andi	$2,$2,0xffff
or	$2,$12,$2
lhu	$11,2($11)
andi	$11,$11,0x7
sll	$11,$11,20
or	$2,$2,$11
sw	$2,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L110:
.set	noreorder
.set	nomacro
bne	$10,$25,$L170
addiu	$5,$5,4
.set	macro
.set	reorder

addiu	$9,$9,8
sw	$9,96($sp)
addiu	$8,$8,8
.set	noreorder
.set	nomacro
j	$L106
sw	$8,100($sp)
.set	macro
.set	reorder

$L251:
sw	$5,100($16)
sw	$4,116($16)
lw	$3,144($sp)
addiu	$2,$3,-116
sw	$2,132($16)
.set	noreorder
.set	nomacro
j	$L260
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

$L244:
sw	$9,180($sp)
.set	noreorder
.set	nomacro
j	$L79
sw	$0,148($sp)
.set	macro
.set	reorder

$L243:
lw	$2,48($2)
slt	$2,$2,2
xori	$2,$2,0x1
sll	$3,$2,4
subu	$3,$3,$2
.set	noreorder
.set	nomacro
j	$L77
sw	$3,224($sp)
.set	macro
.set	reorder

$L170:
.set	noreorder
.set	nomacro
j	$L111
move	$10,$6
.set	macro
.set	reorder

$L169:
.set	noreorder
.set	nomacro
j	$L109
li	$6,1			# 0x1
.set	macro
.set	reorder

$L167:
.set	noreorder
.set	nomacro
j	$L100
li	$8,1			# 0x1
.set	macro
.set	reorder

$L107:
.set	noreorder
.set	nomacro
beq	$5,$0,$L259
andi	$12,$18,0x1
.set	macro
.set	reorder

andi	$7,$18,0x1
sll	$7,$7,1
andi	$5,$18,0x2
sll	$5,$5,10
li	$6,65536			# 0x10000
ori	$6,$6,0x90
or	$5,$5,$6
sll	$2,$2,31
or	$2,$5,$2
sll	$10,$7,8
or	$10,$2,$10
move	$8,$0
move	$9,$0
$L115:
addu	$5,$15,$8
lw	$5,0($5)
.set	noreorder
.set	nomacro
beq	$5,$0,$L114
addiu	$11,$sp,96
.set	macro
.set	reorder

addu	$5,$11,$8
lw	$11,0($5)
lw	$5,0($11)
sw	$5,0($3)
sll	$5,$9,30
or	$5,$5,$10
addu	$6,$19,$8
lbu	$6,28($6)
sll	$6,$6,12
andi	$6,$6,0xffff
or	$5,$5,$6
lhu	$6,2($11)
andi	$6,$6,0x7
sll	$6,$6,20
or	$5,$5,$6
sw	$5,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L114:
.set	noreorder
.set	nomacro
bne	$9,$25,$L171
addiu	$8,$8,4
.set	macro
.set	reorder

lw	$10,96($sp)
addiu	$5,$10,4
sw	$5,96($sp)
lw	$8,100($sp)
addiu	$5,$8,4
sw	$5,100($sp)
addiu	$5,$7,1
sll	$5,$5,8
or	$2,$2,$5
move	$7,$0
move	$11,$0
$L117:
addu	$5,$15,$7
lw	$5,0($5)
.set	noreorder
.set	nomacro
beq	$5,$0,$L116
addiu	$6,$sp,96
.set	macro
.set	reorder

addu	$5,$6,$7
lw	$6,0($5)
lw	$5,0($6)
sw	$5,0($3)
sll	$5,$11,30
or	$12,$5,$2
addu	$5,$19,$7
lbu	$5,28($5)
sll	$5,$5,12
andi	$5,$5,0xffff
or	$5,$12,$5
lhu	$6,2($6)
andi	$6,$6,0x7
sll	$6,$6,20
or	$5,$5,$6
sw	$5,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L116:
.set	noreorder
.set	nomacro
bne	$11,$25,$L172
addiu	$7,$7,4
.set	macro
.set	reorder

addiu	$10,$10,8
sw	$10,96($sp)
addiu	$8,$8,8
.set	noreorder
.set	nomacro
j	$L106
sw	$8,100($sp)
.set	macro
.set	reorder

$L259:
sll	$2,$2,31
sll	$12,$12,1
move	$10,$0
andi	$13,$18,0x2
li	$5,65536			# 0x10000
ori	$5,$5,0x50
or	$2,$2,$5
sw	$2,212($sp)
$L113:
andi	$2,$10,0x2
sra	$2,$2,1
addu	$2,$2,$13
sll	$2,$2,10
andi	$6,$10,0x1
addu	$6,$6,$12
sll	$6,$6,8
addiu	$9,$sp,96
move	$7,$0
move	$8,$0
lw	$11,212($sp)
or	$6,$11,$6
or	$6,$6,$2
$L119:
addu	$2,$15,$7
lw	$2,0($2)
beq	$2,$0,$L118
lw	$5,0($9)
lw	$2,0($5)
sw	$2,0($3)
sll	$2,$8,30
or	$11,$6,$2
addu	$2,$19,$7
lbu	$2,28($2)
sll	$2,$2,12
andi	$2,$2,0xffff
or	$2,$11,$2
lhu	$5,2($5)
andi	$5,$5,0x7
sll	$5,$5,20
or	$2,$2,$5
sw	$2,4($3)
addiu	$4,$4,1
addiu	$3,$3,8
$L118:
addiu	$8,$8,1
addiu	$7,$7,4
.set	noreorder
.set	nomacro
bne	$7,$24,$L119
addiu	$9,$9,4
.set	macro
.set	reorder

lw	$2,96($sp)
addiu	$2,$2,4
sw	$2,96($sp)
lw	$2,100($sp)
addiu	$2,$2,4
addiu	$10,$10,1
.set	noreorder
.set	nomacro
bne	$10,$31,$L113
sw	$2,100($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L263
addiu	$18,$18,1
.set	macro
.set	reorder

$L171:
.set	noreorder
.set	nomacro
j	$L115
li	$9,1			# 0x1
.set	macro
.set	reorder

$L172:
.set	noreorder
.set	nomacro
j	$L117
move	$11,$9
.set	macro
.set	reorder

$L255:
lw	$2,120($sp)
li	$3,-1912602624			# 0xffffffff8e000000
sw	$3,0($2)
li	$4,320864256			# 0x13200000
$L155:
#APP
# 442 "h264_p1.c" 1
lw	 $2,124($4)	#i_lw
# 0 "" 2
#NO_APP
lw	$3,124($sp)
addiu	$3,$3,1
xor	$2,$2,$3
andi	$2,$2,0x1f
.set	noreorder
.set	nomacro
bne	$2,$0,$L155
li	$2,7			# 0x7
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$fp,$2,$L159
sll	$7,$fp,4
.set	macro
.set	reorder

li	$4,321847296			# 0x132f0000
addiu	$3,$4,36
li	$2,40173568			# 0x2650000
ori	$2,$2,0xe12a
addu	$2,$fp,$2
sll	$2,$2,3
addiu	$2,$2,4
addiu	$4,$4,2404
addu	$6,$7,$4
addu	$3,$7,$3
$L160:
lw	$5,0($3)
subu	$4,$3,$7
addiu	$3,$3,148
.set	noreorder
.set	nomacro
bne	$3,$6,$L160
sw	$5,-16($4)
.set	macro
.set	reorder

li	$5,321847296			# 0x132f0000
addiu	$3,$5,2380
addiu	$5,$5,2988
$L161:
lw	$4,0($2)
sw	$4,0($3)
lw	$4,608($2)
sw	$4,608($3)
addiu	$3,$3,76
.set	noreorder
.set	nomacro
bne	$3,$5,$L161
addiu	$2,$2,76
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L264
li	$2,320864256			# 0x13200000
.set	macro
.set	reorder

$L165:
li	$10,1			# 0x1
.set	noreorder
.set	nomacro
j	$L84
sw	$10,176($sp)
.set	macro
.set	reorder

.end	main
.size	main, .-main
.rdata
.align	2
.type	edg_msk_table.1767, @object
.size	edg_msk_table.1767, 16
edg_msk_table.1767:
.half	0
.half	0
.half	170
.half	170
.half	0
.half	238
.half	170
.half	238
.align	2
.type	mv_msk_table.1766, @object
.size	mv_msk_table.1766, 16
mv_msk_table.1766:
.half	0
.half	238
.half	174
.half	238
.half	234
.half	238
.half	174
.half	234

.comm	mb_qp_up,120,4

.comm	mb_qp_left,2,2

.comm	nnz_left_dblk,4,4

.comm	mv_left_dblk,32,4

.comm	ref_left_dblk,8,4

.comm	left_mb_type,4,4

.comm	DBLK_BASE,4,4

.comm	tcsm1_word_bank_backup,24,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
