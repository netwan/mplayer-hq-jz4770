	.file	1 "h264_cavlc_p1.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.text
	.align	2
	.globl	aux_do_get_phy_addr
	.set	nomips16
	.set	nomicromips
	.ent	aux_do_get_phy_addr
	.type	aux_do_get_phy_addr, @function
aux_do_get_phy_addr:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	move	$2,$4
	li	$3,-16777216			# 0xffffffffff000000
	and	$3,$4,$3
	li	$4,-201326592			# 0xfffffffff4000000
	beq	$3,$4,$L4
	li	$4,321650688			# 0x132c0000

	j	$31
	nop

$L4:
	andi	$2,$2,0xffff
	j	$31
	or	$2,$2,$4

	.set	macro
	.set	reorder
	.end	aux_do_get_phy_addr
	.size	aux_do_get_phy_addr, .-aux_do_get_phy_addr
	.align	2
	.globl	do_get_phy_addr
	.set	nomips16
	.set	nomicromips
	.ent	do_get_phy_addr
	.type	do_get_phy_addr, @function
do_get_phy_addr:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	j	$31
	move	$2,$4

	.set	macro
	.set	reorder
	.end	do_get_phy_addr
	.size	do_get_phy_addr, .-do_get_phy_addr
	.align	2
	.globl	aux_end
	.set	nomips16
	.set	nomicromips
	.ent	aux_end
	.type	aux_end, @function
aux_end:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	sw	$5,0($4)
#APP
 # 19 "eyer_driver_aux.c" 1
	nop	#i_nop
 # 0 "" 2
 # 20 "eyer_driver_aux.c" 1
	nop	#i_nop
 # 0 "" 2
 # 21 "eyer_driver_aux.c" 1
	nop	#i_nop
 # 0 "" 2
 # 22 "eyer_driver_aux.c" 1
	nop	#i_nop
 # 0 "" 2
 # 23 "eyer_driver_aux.c" 1
	nop	#i_nop
 # 0 "" 2
 # 24 "eyer_driver_aux.c" 1
	wait
 # 0 "" 2
#NO_APP
	j	$31
	.end	aux_end
	.size	aux_end, .-aux_end
	.align	2
	.globl	vlc_cfg_sde
	.set	nomips16
	.set	nomicromips
	.ent	vlc_cfg_sde
	.type	vlc_cfg_sde, @function
vlc_cfg_sde:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lw	$8,16($sp)
	lw	$12,20($sp)
	lw	$11,24($sp)
	blez	$5,$L10
	andi	$6,$6,0x00ff

	slt	$13,$0,$4
	slt	$2,$6,$5
	beq	$2,$0,$L11
	move	$9,$13

	andi	$14,$13,0x00ff
	addiu	$10,$6,-1
	slt	$3,$5,$10
	bne	$3,$0,$L32
	move	$2,$14

	slt	$6,$5,$6
	beq	$6,$0,$L25
	slt	$10,$4,$10

$L23:
	move	$3,$0
$L15:
	addu	$9,$9,$3
	li	$3,4			# 0x4
	movz	$3,$0,$2
	addu	$3,$9,$3
	li	$2,8			# 0x8
	movz	$2,$0,$10
	move	$10,$2
$L17:
	addu	$3,$3,$10
	sll	$3,$3,24
	addu	$3,$3,$5
	sw	$3,0($7)
	li	$2,301989888			# 0x12000000
	addu	$5,$5,$2
	li	$2,-16777216			# 0xffffffffff000000
	and	$2,$8,$2
	li	$3,-201326592			# 0xfffffffff4000000
	beq	$2,$3,$L33
	sw	$5,4($7)

$L18:
	li	$2,-16777216			# 0xffffffffff000000
	and	$2,$12,$2
	li	$3,-201326592			# 0xfffffffff4000000
	beq	$2,$3,$L34
	sw	$8,8($7)

$L19:
	sw	$12,12($7)
	sll	$3,$4,2
	sll	$8,$4,4
	addu	$3,$3,$8
	subu	$8,$3,$4
	sll	$8,$8,2
	addu	$6,$11,$8
	move	$2,$0
	li	$5,76			# 0x4c
$L20:
	addu	$3,$7,$2
	addu	$4,$6,$2
	lw	$4,0($4)
	addiu	$2,$2,4
	bne	$2,$5,$L20
	sw	$4,16($3)

	addiu	$8,$8,76
	addu	$2,$11,$8
	lw	$3,0($2)
	sw	$3,92($7)
	lw	$3,8($2)
	sw	$3,96($7)
	lw	$3,12($2)
	sw	$3,100($7)
	lw	$2,28($2)
	j	$31
	sw	$2,104($7)

$L10:
	move	$9,$0
$L11:
	addiu	$10,$6,-1
	slt	$2,$5,$10
	bne	$2,$0,$L14
	move	$2,$0

	slt	$6,$5,$6
	beq	$6,$0,$L25
	slt	$10,$4,$10

	j	$L15
	move	$3,$0

$L14:
	slt	$6,$5,$6
	bne	$6,$0,$L23
	move	$10,$0

	addiu	$3,$9,2
$L21:
	move	$10,$0
	addu	$3,$3,$10
	sll	$3,$3,24
	addu	$3,$3,$5
	sw	$3,0($7)
	li	$2,301989888			# 0x12000000
	addu	$5,$5,$2
	li	$2,-16777216			# 0xffffffffff000000
	and	$2,$8,$2
	li	$3,-201326592			# 0xfffffffff4000000
	bne	$2,$3,$L18
	sw	$5,4($7)

$L33:
	andi	$8,$8,0xffff
	li	$2,321650688			# 0x132c0000
	or	$8,$8,$2
	li	$2,-16777216			# 0xffffffffff000000
	and	$2,$12,$2
	li	$3,-201326592			# 0xfffffffff4000000
	bne	$2,$3,$L19
	sw	$8,8($7)

$L34:
	andi	$12,$12,0xffff
	li	$2,321650688			# 0x132c0000
	j	$L19
	or	$12,$12,$2

$L25:
	j	$L15
	li	$3,2			# 0x2

$L32:
	beq	$14,$0,$L21
	addiu	$3,$13,2

	addiu	$3,$13,6
	j	$L17
	move	$10,$0

	.set	macro
	.set	reorder
	.end	vlc_cfg_sde
	.size	vlc_cfg_sde, .-vlc_cfg_sde
	.align	2
	.globl	backup_sde_vlc_aux
	.set	nomips16
	.set	nomicromips
	.ent	backup_sde_vlc_aux
	.type	backup_sde_vlc_aux, @function
backup_sde_vlc_aux:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	sll	$2,$4,2
	sll	$3,$4,4
	addu	$2,$2,$3
	subu	$4,$2,$4
	sll	$4,$4,2
	addu	$6,$6,$4
	lhu	$2,0($5)
	sw	$2,0($6)
	lw	$3,8($5)
	andi	$2,$2,0x4
	bne	$2,$0,$L41
	sw	$3,4($6)

	lw	$2,32($5)
	li	$3,-65536			# 0xffffffffffff0000
	and	$2,$2,$3
	lh	$3,30($5)
	or	$2,$2,$3
	sw	$2,8($6)
	addiu	$2,$5,48
	addiu	$6,$6,12
	addiu	$5,$5,64
$L39:
	lw	$3,0($2)
	sw	$3,0($6)
	lw	$3,32($2)
	sw	$3,16($6)
	addiu	$2,$2,4
	bne	$2,$5,$L39
	addiu	$6,$6,4

	j	$31
	nop

$L41:
	lw	$3,32($5)
	andi	$4,$3,0xf000
	srl	$7,$4,8
	srl	$3,$3,16
	andi	$3,$3,0xff00
	addu	$4,$7,$3
	lbu	$2,38($5)
	srl	$2,$2,4
	addu	$3,$4,$2
	lbu	$2,39($5)
	sll	$2,$2,16
	addu	$2,$3,$2
	j	$31
	sw	$2,8($6)

	.set	macro
	.set	reorder
	.end	backup_sde_vlc_aux
	.size	backup_sde_vlc_aux, .-backup_sde_vlc_aux
	.align	2
	.globl	vlc_hw_dblk_mb_aux_c
	.set	nomips16
	.set	nomicromips
	.ent	vlc_hw_dblk_mb_aux_c
	.type	vlc_hw_dblk_mb_aux_c, @function
vlc_hw_dblk_mb_aux_c:
	.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
	.mask	0x40ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-40
	sw	$fp,36($sp)
	sw	$23,32($sp)
	sw	$22,28($sp)
	sw	$21,24($sp)
	sw	$20,20($sp)
	sw	$19,16($sp)
	sw	$18,12($sp)
	sw	$17,8($sp)
	sw	$16,4($sp)
	lw	$21,56($sp)
	lw	$14,60($sp)
	lw	$18,64($sp)
	lw	$19,68($sp)
	lw	$13,72($sp)
	lw	$12,76($sp)
	lw	$16,84($sp)
	lw	$22,92($sp)
	lhu	$10,0($5)
	xori	$17,$6,0x4
	sltu	$17,$17,1
	xori	$6,$6,0x1
	sltu	$6,$6,1
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L43
	andi	$fp,$10,0x3
	.set	macro
	.set	reorder

	lui	$23,%hi(nnz_left_dblk)
	lw	$4,%lo(nnz_left_dblk)($23)
	sll	$4,$4,20
	lui	$20,%hi(mb_qp_left)
	lh	$8,%lo(mb_qp_left)($20)
$L44:
	lui	$11,%hi(mb_qp_up)
	.set	noreorder
	.set	nomacro
	bne	$21,$0,$L45
	addiu	$11,$11,%lo(mb_qp_up)
	.set	macro
	.set	reorder

	addu	$2,$11,$7
	sb	$0,0($2)
$L45:
	lw	$25,4($16)
	move	$9,$0
	move	$3,$0
	li	$24,15			# 0xf
	li	$15,4			# 0x4
$L46:
	sll	$2,$3,2
	sll	$2,$24,$2
	and	$2,$2,$25
	sltu	$2,$0,$2
	sll	$2,$2,$3
	addiu	$3,$3,1
	.set	noreorder
	.set	nomacro
	bne	$3,$15,$L46
	or	$9,$9,$2
	.set	macro
	.set	reorder

	lw	$2,20($5)
	andi	$3,$2,0xc0c
	sll	$15,$3,2
	andi	$3,$2,0x3030
	sra	$3,$3,2
	or	$3,$15,$3
	andi	$2,$2,0xc3c3
	or	$2,$3,$2
	sll	$9,$9,16
	or	$9,$2,$9
	or	$4,$9,$4
	lw	$25,12($5)
	move	$9,$0
	move	$3,$0
	li	$24,15			# 0xf
	li	$15,4			# 0x4
$L47:
	sll	$2,$3,2
	sll	$2,$24,$2
	and	$2,$2,$25
	sltu	$2,$0,$2
	sll	$2,$2,$3
	addiu	$3,$3,1
	.set	noreorder
	.set	nomacro
	bne	$3,$15,$L47
	or	$9,$9,$2
	.set	macro
	.set	reorder

	sw	$9,%lo(nnz_left_dblk)($23)
	andi	$2,$10,0x38
	srl	$3,$2,2
	lui	$2,%hi(mv_msk_table.1773)
	addiu	$2,$2,%lo(mv_msk_table.1773)
	addu	$2,$3,$2
	lh	$2,0($2)
	sll	$2,$2,24
	or	$4,$4,$2
	sw	$4,16($13)
	sw	$4,16($12)
	sw	$22,8($13)
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L63
	sw	$22,8($12)
	.set	macro
	.set	reorder

	lhu	$3,44($5)
	sll	$2,$17,1
	sll	$15,$17,3
	addu	$2,$2,$15
	addu	$2,$3,$2
	addiu	$2,$2,11
	sll	$15,$2,24
$L48:
	lw	$9,0($16)
	addu	$2,$11,$7
	lbu	$3,0($2)
	li	$4,-201326592			# 0xfffffffff4000000
	addiu	$4,$4,19664
	addu	$25,$4,$3
	lb	$22,0($25)
	addu	$22,$22,$18
	addiu	$22,$22,1
	sra	$22,$22,1
	sll	$23,$19,18
	or	$24,$23,$18
	lb	$2,256($25)
	addu	$2,$2,$19
	addiu	$2,$2,1
	sra	$2,$2,1
	sll	$2,$2,12
	or	$23,$24,$2
	addu	$4,$4,$8
	lb	$2,256($4)
	addu	$2,$2,$19
	addiu	$2,$2,1
	sra	$2,$2,1
	sll	$2,$2,24
	or	$2,$23,$2
	lb	$4,0($4)
	addu	$4,$4,$18
	addiu	$4,$4,1
	sra	$4,$4,1
	sll	$4,$4,6
	or	$2,$2,$4
	sw	$2,20($13)
	sw	$2,20($12)
	addu	$4,$3,$14
	addiu	$4,$4,1
	sra	$4,$4,1
	sll	$3,$14,6
	or	$4,$4,$3
	sll	$2,$22,18
	or	$3,$4,$2
	addu	$2,$14,$8
	addiu	$2,$2,1
	sra	$2,$2,1
	sll	$2,$2,12
	or	$2,$3,$2
	or	$2,$2,$15
	sw	$2,24($13)
	sw	$2,24($12)
	lui	$8,%hi(left_mb_type)
	lw	$18,%lo(left_mb_type)($8)
	andi	$4,$18,0x28
	and	$4,$4,$10
	sltu	$24,$0,$4
	andi	$19,$10,0x8
	andi	$19,$19,0xffff
	and	$4,$9,$19
	li	$15,16			# 0x10
	move	$2,$0
	movn	$2,$15,$4
	move	$4,$2
	andi	$2,$10,0x20
	li	$23,4			# 0x4
	movz	$23,$0,$2
	andi	$3,$10,0x10
	li	$2,64			# 0x40
	move	$22,$0
	movn	$22,$2,$3
	or	$4,$4,$24
	or	$2,$4,$23
	or	$3,$2,$22
	movn	$15,$0,$21
	lw	$2,96($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L68
	move	$21,$15
	.set	macro
	.set	reorder

	sltu	$2,$7,1
	andi	$4,$10,0xc00
	sra	$4,$4,9
	sra	$19,$19,3
	or	$19,$4,$19
	sll	$19,$19,1
	lui	$4,%hi(edg_msk_table.1774)
	addiu	$4,$4,%lo(edg_msk_table.1774)
	addu	$4,$19,$4
	lh	$15,0($4)
	or	$2,$2,$15
	or	$21,$2,$21
	sll	$21,$21,16
$L53:
	sll	$2,$3,24
	or	$15,$21,$2
	sltu	$3,$0,$fp
	sll	$4,$3,4
	andi	$2,$18,0x3
	sltu	$2,$0,$2
	sll	$2,$2,5
	or	$3,$4,$2
	andi	$2,$9,0x3
	sltu	$2,$0,$2
	sll	$2,$2,3
	or	$2,$3,$2
	ori	$3,$2,0x445
	or	$3,$3,$15
	sw	$3,28($13)
	lw	$3,80($sp)
	sw	$3,56($13)
	sw	$3,56($12)
	ori	$2,$2,0x345
	or	$2,$2,$15
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L55
	sw	$2,28($12)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$fp,$0,$L56
	lui	$2,%hi(ref_left_dblk)
	.set	macro
	.set	reorder

	lhu	$2,8($16)
	lw	$4,28($5)
	lw	$3,88($sp)
	sw	$4,0($3)
	lui	$6,%hi(ref_left_dblk)
	lw	$3,%lo(ref_left_dblk)($6)
	or	$2,$2,$3
	lw	$3,88($sp)
	sw	$2,4($3)
	andi	$2,$4,0xff00
	sll	$3,$2,8
	li	$9,-16777216			# 0xffffffffff000000
	and	$2,$4,$9
	or	$2,$3,$2
	.set	noreorder
	.set	nomacro
	bne	$17,$0,$L57
	sw	$2,%lo(ref_left_dblk)($6)
	.set	macro
	.set	reorder

	lw	$4,88($sp)
	addiu	$3,$4,8
$L58:
	lhu	$13,46($5)
	andi	$15,$13,0x3
	addiu	$12,$3,4
	andi	$6,$13,0x30
	sll	$9,$6,4
	andi	$4,$13,0xc
	sll	$4,$4,2
	or	$6,$9,$4
	andi	$4,$13,0xc0
	sll	$4,$4,10
	or	$4,$6,$4
	andi	$2,$13,0x300
	sll	$2,$2,16
	or	$2,$4,$2
	or	$2,$2,$15
	sw	$2,0($3)
	addiu	$2,$16,12
#APP
 # 215 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr1,$2,0
 # 0 "" 2
 # 216 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr2,$2,4
 # 0 "" 2
 # 217 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr3,$2,8
 # 0 "" 2
 # 218 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr4,$2,12
 # 0 "" 2
#NO_APP
	addiu	$2,$5,64
#APP
 # 221 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr9,$2,0
 # 0 "" 2
 # 222 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr10,$2,4
 # 0 "" 2
 # 223 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr11,$2,8
 # 0 "" 2
 # 224 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr12,$2,12
 # 0 "" 2
#NO_APP
	lui	$2,%hi(mv_left_dblk)
	addiu	$2,$2,%lo(mv_left_dblk)
#APP
 # 227 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr5,$2,0
 # 0 "" 2
 # 228 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr6,$2,4
 # 0 "" 2
 # 229 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr7,$2,8
 # 0 "" 2
 # 230 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr8,$2,12
 # 0 "" 2
 # 233 "h264_cavlc_p1_dblk.c" 1
	S32STD xr9,$2,0
 # 0 "" 2
 # 234 "h264_cavlc_p1_dblk.c" 1
	S32STD xr10,$2,4
 # 0 "" 2
 # 235 "h264_cavlc_p1_dblk.c" 1
	S32STD xr11,$2,8
 # 0 "" 2
 # 236 "h264_cavlc_p1_dblk.c" 1
	S32STD xr12,$2,12
 # 0 "" 2
 # 238 "h264_cavlc_p1_dblk.c" 1
	S32STD xr1,$12,0
 # 0 "" 2
 # 239 "h264_cavlc_p1_dblk.c" 1
	S32STD xr2,$12,4
 # 0 "" 2
 # 240 "h264_cavlc_p1_dblk.c" 1
	S32STD xr3,$12,8
 # 0 "" 2
 # 241 "h264_cavlc_p1_dblk.c" 1
	S32STD xr4,$12,12
 # 0 "" 2
 # 244 "h264_cavlc_p1_dblk.c" 1
	S32STD xr5,$12,16
 # 0 "" 2
 # 245 "h264_cavlc_p1_dblk.c" 1
	S32STD xr6,$12,20
 # 0 "" 2
 # 246 "h264_cavlc_p1_dblk.c" 1
	S32STD xr7,$12,24
 # 0 "" 2
 # 247 "h264_cavlc_p1_dblk.c" 1
	S32STD xr8,$12,28
 # 0 "" 2
#NO_APP
	.set	noreorder
	.set	nomacro
	beq	$17,$0,$L59
	lhu	$6,44($5)
	.set	macro
	.set	reorder

	addiu	$4,$3,36
	addiu	$16,$16,28
#APP
 # 257 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr1,$16,0
 # 0 "" 2
 # 258 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr2,$16,4
 # 0 "" 2
 # 259 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr3,$16,8
 # 0 "" 2
 # 260 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr4,$16,12
 # 0 "" 2
#NO_APP
	addiu	$2,$5,96
#APP
 # 263 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr9,$2,0
 # 0 "" 2
 # 264 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr10,$2,4
 # 0 "" 2
 # 265 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr11,$2,8
 # 0 "" 2
 # 266 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr12,$2,12
 # 0 "" 2
#NO_APP
	lui	$2,%hi(mv_left_dblk+16)
	addiu	$2,$2,%lo(mv_left_dblk+16)
#APP
 # 269 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr5,$2,0
 # 0 "" 2
 # 270 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr6,$2,4
 # 0 "" 2
 # 271 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr7,$2,8
 # 0 "" 2
 # 272 "h264_cavlc_p1_dblk.c" 1
	S32LDD xr8,$2,12
 # 0 "" 2
 # 275 "h264_cavlc_p1_dblk.c" 1
	S32STD xr9,$2,0
 # 0 "" 2
 # 276 "h264_cavlc_p1_dblk.c" 1
	S32STD xr10,$2,4
 # 0 "" 2
 # 277 "h264_cavlc_p1_dblk.c" 1
	S32STD xr11,$2,8
 # 0 "" 2
 # 278 "h264_cavlc_p1_dblk.c" 1
	S32STD xr12,$2,12
 # 0 "" 2
 # 280 "h264_cavlc_p1_dblk.c" 1
	S32STD xr1,$4,0
 # 0 "" 2
 # 281 "h264_cavlc_p1_dblk.c" 1
	S32STD xr2,$4,4
 # 0 "" 2
 # 282 "h264_cavlc_p1_dblk.c" 1
	S32STD xr3,$4,8
 # 0 "" 2
 # 283 "h264_cavlc_p1_dblk.c" 1
	S32STD xr4,$4,12
 # 0 "" 2
 # 285 "h264_cavlc_p1_dblk.c" 1
	S32STD xr5,$4,16
 # 0 "" 2
 # 286 "h264_cavlc_p1_dblk.c" 1
	S32STD xr6,$4,20
 # 0 "" 2
 # 287 "h264_cavlc_p1_dblk.c" 1
	S32STD xr7,$4,24
 # 0 "" 2
 # 288 "h264_cavlc_p1_dblk.c" 1
	S32STD xr8,$4,28
 # 0 "" 2
#NO_APP
	addiu	$5,$5,108
	addiu	$3,$3,64
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L55
	addiu	$2,$6,-1
	.set	macro
	.set	reorder

	li	$4,-1			# 0xffffffffffffffff
$L61:
#APP
 # 297 "h264_cavlc_p1_dblk.c" 1
	S32LDI xr1,$5,4
 # 0 "" 2
 # 298 "h264_cavlc_p1_dblk.c" 1
	S32SDI xr1,$3,4
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-1
	bne	$2,$4,$L61
$L55:
	addu	$7,$11,$7
	sb	$14,0($7)
	sh	$14,%lo(mb_qp_left)($20)
	sw	$10,%lo(left_mb_type)($8)
	lw	$fp,36($sp)
	lw	$23,32($sp)
	lw	$22,28($sp)
	lw	$21,24($sp)
	lw	$20,20($sp)
	lw	$19,16($sp)
	lw	$18,12($sp)
	lw	$17,8($sp)
	lw	$16,4($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,40
	.set	macro
	.set	reorder

$L68:
	.set	noreorder
	.set	nomacro
	j	$L53
	li	$21,-65536			# 0xffffffffffff0000
	.set	macro
	.set	reorder

$L63:
	.set	noreorder
	.set	nomacro
	j	$L48
	move	$15,$0
	.set	macro
	.set	reorder

$L43:
	lui	$20,%hi(mb_qp_left)
	sh	$0,%lo(mb_qp_left)($20)
	lui	$23,%hi(nnz_left_dblk)
	sw	$0,%lo(nnz_left_dblk)($23)
	lui	$2,%hi(ref_left_dblk)
	sw	$0,%lo(ref_left_dblk)($2)
	addiu	$2,$2,%lo(ref_left_dblk)
	sw	$0,4($2)
	lui	$2,%hi(mv_left_dblk)
	sw	$0,%lo(mv_left_dblk)($2)
	addiu	$2,$2,%lo(mv_left_dblk)
	sw	$0,4($2)
	sw	$0,8($2)
	sw	$0,12($2)
	sw	$0,16($2)
	sw	$0,20($2)
	sw	$0,24($2)
	sw	$0,28($2)
	move	$8,$0
	.set	noreorder
	.set	nomacro
	j	$L44
	move	$4,$0
	.set	macro
	.set	reorder

$L56:
	sw	$0,%lo(ref_left_dblk)($2)
	addiu	$2,$2,%lo(ref_left_dblk)
	sw	$0,4($2)
	addu	$7,$11,$7
	sb	$14,0($7)
	sh	$14,%lo(mb_qp_left)($20)
	sw	$10,%lo(left_mb_type)($8)
	lw	$fp,36($sp)
	lw	$23,32($sp)
	lw	$22,28($sp)
	lw	$21,24($sp)
	lw	$20,20($sp)
	lw	$19,16($sp)
	lw	$18,12($sp)
	lw	$17,8($sp)
	lw	$16,4($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,40
	.set	macro
	.set	reorder

$L57:
	lhu	$3,10($16)
	lw	$2,32($5)
	lw	$4,88($sp)
	sw	$2,8($4)
	addiu	$6,$6,%lo(ref_left_dblk)
	lw	$4,4($6)
	or	$3,$3,$4
	lw	$4,88($sp)
	sw	$3,12($4)
	addiu	$3,$4,16
	andi	$4,$2,0xff00
	sll	$4,$4,8
	and	$2,$2,$9
	or	$2,$4,$2
	.set	noreorder
	.set	nomacro
	j	$L58
	sw	$2,4($6)
	.set	macro
	.set	reorder

$L59:
	addiu	$5,$5,76
	addiu	$3,$3,32
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L55
	addiu	$2,$6,-1
	.set	macro
	.set	reorder

	li	$4,-1			# 0xffffffffffffffff
$L62:
#APP
 # 307 "h264_cavlc_p1_dblk.c" 1
	S32LDI xr1,$5,4
 # 0 "" 2
 # 308 "h264_cavlc_p1_dblk.c" 1
	S32SDI xr1,$3,4
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-1
	bne	$2,$4,$L62
	addu	$7,$11,$7
	sb	$14,0($7)
	sh	$14,%lo(mb_qp_left)($20)
	sw	$10,%lo(left_mb_type)($8)
	lw	$fp,36($sp)
	lw	$23,32($sp)
	lw	$22,28($sp)
	lw	$21,24($sp)
	lw	$20,20($sp)
	lw	$19,16($sp)
	lw	$18,12($sp)
	lw	$17,8($sp)
	lw	$16,4($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,40
	.set	macro
	.set	reorder

	.end	vlc_hw_dblk_mb_aux_c
	.size	vlc_hw_dblk_mb_aux_c, .-vlc_hw_dblk_mb_aux_c
	.section	.p1_main,"ax",@progbits
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,312,$31		# vars= 208, regs= 10/0, args= 64, gp= 0
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-312
	sw	$31,308($sp)
	sw	$fp,304($sp)
	sw	$23,300($sp)
	sw	$22,296($sp)
	sw	$21,292($sp)
	sw	$20,288($sp)
	sw	$19,284($sp)
	sw	$18,280($sp)
	sw	$17,276($sp)
	sw	$16,272($sp)
	li	$2,3			# 0x3
#APP
 # 25 "h264_cavlc_p1.c" 1
	S32I2M xr16,$2
 # 0 "" 2
#NO_APP
	li	$2,321585152			# 0x132b0000
	sw	$0,36($2)
	move	$3,$0
	li	$2,320864256			# 0x13200000
	addiu	$4,$2,108
#APP
 # 33 "h264_cavlc_p1.c" 1
	sw	 $3,0($4)	#i_sw
 # 0 "" 2
#NO_APP
	addiu	$4,$2,112
#APP
 # 34 "h264_cavlc_p1.c" 1
	sw	 $3,0($4)	#i_sw
 # 0 "" 2
#NO_APP
	addiu	$4,$2,116
#APP
 # 35 "h264_cavlc_p1.c" 1
	sw	 $3,0($4)	#i_sw
 # 0 "" 2
#NO_APP
	addiu	$4,$2,120
#APP
 # 36 "h264_cavlc_p1.c" 1
	sw	 $3,0($4)	#i_sw
 # 0 "" 2
#NO_APP
	li	$4,1			# 0x1
	addiu	$3,$2,124
#APP
 # 37 "h264_cavlc_p1.c" 1
	sw	 $4,0($3)	#i_sw
 # 0 "" 2
#NO_APP
	addiu	$2,$2,96
	li	$3,1980039168			# 0x76050000
	addiu	$3,$3,1284
#APP
 # 38 "h264_cavlc_p1.c" 1
	sw	 $3,0($2)	#i_sw
 # 0 "" 2
#NO_APP
	li	$2,-201326592			# 0xfffffffff4000000
	addiu	$3,$2,22860
	lw	$22,22860($2)
	move	$18,$22
	lw	$21,4($3)
	sw	$21,184($sp)
	lw	$6,8($3)
	lw	$8,44($3)
	sw	$8,236($sp)
	lw	$10,24($3)
	sw	$10,216($sp)
	lw	$5,16($3)
	sw	$5,19660($2)
	addiu	$5,$2,20332
	sw	$5,172($sp)
	addiu	$5,$2,20812
	sw	$5,168($sp)
	addiu	$5,$2,20392
	sw	$5,164($sp)
	addiu	$5,$2,20872
	sw	$5,160($sp)
	sw	$4,156($sp)
	lw	$4,156($sp)
	sw	$4,22840($2)
	li	$4,321650688			# 0x132c0000
	addiu	$7,$4,21292
	li	$5,321060864			# 0x13230000
	sw	$7,0($5)
	addiu	$5,$2,10500
	sw	$5,152($sp)
	addiu	$5,$2,12744
	sw	$5,144($sp)
	sw	$5,148($sp)
	sw	$0,10500($2)
	addiu	$4,$4,10501
	li	$2,321191936			# 0x13250000
	addiu	$2,$2,84
#APP
 # 107 "h264_cavlc_p1.c" 1
	sw	 $4,0($2)	#i_sw
 # 0 "" 2
#NO_APP
	andi	$6,$6,0xff
	sw	$6,176($sp)
	addiu	$2,$6,4
	sll	$2,$2,8
	sw	$2,212($sp)
	sra	$2,$2,1
	sw	$2,224($sp)
	xori	$2,$10,0x4
	sltu	$2,$2,1
	lbu	$4,32($3)
	sll	$4,$4,16
	or	$2,$2,$4
	lw	$3,28($3)
	sll	$3,$3,24
	or	$3,$2,$3
	sw	$3,240($sp)
	move	$5,$0
	li	$7,715784192			# 0x2aaa0000
	ori	$7,$7,0xaaab
	li	$8,64			# 0x40
$L83:
	addiu	$3,$sp,64
	addu	$6,$3,$5
	mult	$5,$7
	mfhi	$2
	sra	$4,$5,31
	subu	$4,$2,$4
	sll	$2,$4,1
	sll	$3,$4,3
	subu	$2,$3,$2
	subu	$2,$5,$2
	sll	$2,$2,4
	or	$2,$2,$4
	addiu	$5,$5,1
	.set	noreorder
	.set	nomacro
	bne	$5,$8,$L83
	sb	$2,0($6)
	.set	macro
	.set	reorder

	li	$7,-201326592			# 0xfffffffff4000000
	addiu	$2,$7,14796
	sw	$2,16($sp)
	addiu	$2,$2,19656
	sw	$2,20($sp)
	addiu	$2,$7,22912
	sw	$2,24($sp)
	move	$4,$22
	move	$5,$0
	lw	$6,176($sp)
	.set	noreorder
	.set	nomacro
	jal	vlc_cfg_sde
	addiu	$7,$7,13068
	.set	macro
	.set	reorder

	li	$2,321650688			# 0x132c0000
	addiu	$2,$2,13069
	li	$3,321454080			# 0x13290000
	sw	$2,4($3)
	addiu	$25,$22,1
	lw	$2,176($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$25,$L222
	addiu	$3,$21,1
	.set	macro
	.set	reorder

	sw	$21,204($sp)
$L84:
	move	$23,$0
	li	$2,-201326592			# 0xfffffffff4000000
	addiu	$15,$2,13176
	addiu	$8,$2,13068
	sw	$8,220($sp)
	addiu	$10,$2,15100
	sw	$10,200($sp)
	addiu	$19,$2,14796
	addiu	$2,$15,22092
	sw	$2,196($sp)
	addiu	$3,$15,21276
	sw	$3,208($sp)
	li	$8,1			# 0x1
	sw	$8,180($sp)
	li	$fp,-201326592			# 0xfffffffff4000000
	addiu	$10,$fp,10500
	sw	$10,244($sp)
	addiu	$2,$fp,20332
	sw	$2,232($sp)
	move	$20,$19
	sw	$15,192($sp)
	sw	$25,188($sp)
	move	$2,$23
$L146:
	addiu	$4,$fp,22832
	lw	$3,22832($fp)
	sw	$3,228($sp)
	lw	$23,4($4)
	lw	$5,144($sp)
	sw	$5,148($sp)
	li	$5,7			# 0x7
	.set	noreorder
	.set	nomacro
	beq	$2,$5,$L85
	li	$10,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

	lw	$5,148($sp)
	addiu	$5,$5,40
	sw	$5,144($sp)
	lw	$19,152($sp)
	addiu	$5,$19,280
	sw	$5,152($sp)
	sll	$5,$2,6
	sll	$16,$2,8
	subu	$16,$16,$5
	li	$6,-201326592			# 0xfffffffff4000000
	ori	$6,$6,0x532c
	addu	$16,$16,$6
	li	$17,80412672			# 0x4cb0000
	ori	$17,$17,0xc389
	addu	$17,$2,$17
	sll	$17,$17,2
	sll	$6,$2,2
	subu	$5,$5,$6
	lw	$8,232($sp)
	addu	$6,$5,$8
	sw	$6,172($sp)
	lw	$6,172($sp)
	addiu	$6,$6,60
	sw	$6,164($sp)
	addiu	$4,$4,-2020
	addu	$5,$5,$4
	sw	$5,168($sp)
	lw	$4,168($sp)
	addiu	$4,$4,60
$L150:
	sw	$4,160($sp)
	lw	$10,200($sp)
	sw	$10,16($sp)
	lw	$3,196($sp)
	sw	$3,20($sp)
	addiu	$4,$fp,22912
	sw	$4,24($sp)
	lw	$4,188($sp)
	lw	$5,180($sp)
	lw	$6,176($sp)
	lw	$7,192($sp)
	.set	noreorder
	.set	nomacro
	jal	vlc_cfg_sde
	sw	$2,268($sp)
	.set	macro
	.set	reorder

	lw	$2,268($sp)
$L86:
	lw	$3,22840($fp)
	lw	$4,156($sp)
	addiu	$3,$3,8
	sltu	$3,$4,$3
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L86
	li	$6,321454080			# 0x13290000
	.set	macro
	.set	reorder

	j	$L198
$L224:
	li	$4,-201326592			# 0xfffffffff4000000
	lw	$3,13064($4)
	bne	$3,$0,$L223
$L198:
	lw	$3,0($6)
	andi	$3,$3,0x8
	beq	$3,$0,$L224
$L147:
	li	$3,320864256			# 0x13200000
$L243:
	addiu	$3,$3,96
	li	$2,847314944			# 0x32810000
	addiu	$2,$2,256
#APP
 # 465 "h264_cavlc_p1.c" 1
	sw	 $2,0($3)	#i_sw
 # 0 "" 2
#NO_APP
	li	$3,1			# 0x1
	li	$2,321519616			# 0x132a0000
	sw	$3,20($2)
	li	$3,23130			# 0x5a5a
	li	$2,321585152			# 0x132b0000
	sw	$3,36($2)
#APP
 # 484 "h264_cavlc_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 485 "h264_cavlc_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 486 "h264_cavlc_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 487 "h264_cavlc_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 488 "h264_cavlc_p1.c" 1
	wait	#i_wait
 # 0 "" 2
#NO_APP
	move	$2,$0
	lw	$31,308($sp)
	lw	$fp,304($sp)
	lw	$23,300($sp)
	lw	$22,296($sp)
	lw	$21,292($sp)
	lw	$20,288($sp)
	lw	$19,284($sp)
	lw	$18,280($sp)
	lw	$17,276($sp)
	lw	$16,272($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,312
	.set	macro
	.set	reorder

$L223:
	sw	$0,13064($4)
	addiu	$5,$22,1
	lw	$8,176($sp)
	slt	$5,$5,$8
	beq	$5,$0,$L225
$L89:
	lw	$4,22908($fp)
	lw	$6,16($20)
	addu	$4,$4,$6
	lw	$5,22900($fp)
	sltu	$4,$4,$5
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L226
	lw	$8,192($sp)
	.set	macro
	.set	reorder

$L244:
	andi	$4,$8,0xfffc
	li	$5,321650688			# 0x132c0000
	or	$4,$4,$5
	addiu	$4,$4,1
	li	$5,321454080			# 0x13290000
	addiu	$5,$5,4
#APP
 # 178 "h264_cavlc_p1.c" 1
	sw	 $4,0($5)	#i_sw
 # 0 "" 2
#NO_APP
	move	$21,$0
$L90:
	lhu	$6,0($20)
	lw	$4,152($sp)
	sw	$0,0($4)
	lw	$4,0($19)
	andi	$4,$6,0x7
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L227
	li	$4,-805306368			# 0xffffffffd0000000
	.set	macro
	.set	reorder

	li	$4,2			# 0x2
	lw	$10,216($sp)
	.set	noreorder
	.set	nomacro
	beq	$10,$4,$L228
	addiu	$4,$20,112
	.set	macro
	.set	reorder

	sw	$4,128($sp)
	lhu	$4,44($20)
	srl	$4,$4,1
	addiu	$4,$4,28
	sll	$4,$4,2
	addu	$4,$20,$4
	sw	$4,132($sp)
$L95:
	andi	$4,$6,0x8
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L96
	addiu	$5,$19,4
	.set	macro
	.set	reorder

	andi	$4,$6,0x1000
	sw	$4,136($sp)
	andi	$4,$6,0x4000
	sw	$4,140($sp)
	andi	$6,$6,0x5000
	xori	$6,$6,0x5000
	sltu	$6,$6,1
	sll	$6,$6,31
	move	$10,$0
	move	$11,$0
	move	$4,$0
	addiu	$3,$sp,136
	li	$14,65536			# 0x10000
	addiu	$14,$14,240
	or	$14,$6,$14
	li	$12,1			# 0x1
$L98:
	addu	$6,$3,$10
	lw	$6,0($6)
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L97
	addiu	$7,$sp,128
	.set	macro
	.set	reorder

	addu	$6,$7,$10
	lw	$8,0($6)
	lw	$6,0($8)
	sw	$6,0($5)
	sll	$6,$11,30
	or	$6,$14,$6
	addu	$7,$20,$10
	lbu	$7,28($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$6,$6,$7
	lhu	$7,2($8)
	andi	$7,$7,0x7
	sll	$7,$7,20
	or	$6,$6,$7
	sw	$6,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L97:
	.set	noreorder
	.set	nomacro
	beq	$11,$12,$L99
	addiu	$10,$10,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L98
	li	$11,1			# 0x1
	.set	macro
	.set	reorder

$L85:
	ori	$10,$10,0x31c8
	sw	$10,144($sp)
	lw	$19,152($sp)
	lw	$3,244($sp)
	sw	$3,152($sp)
	addiu	$5,$4,-2080
	sw	$5,172($sp)
	addiu	$5,$4,-2500
	sw	$5,164($sp)
	addiu	$5,$4,-1600
	sw	$5,168($sp)
	li	$17,321847296			# 0x132f0000
	addiu	$17,$17,3648
	addiu	$16,$4,-196
	.set	noreorder
	.set	nomacro
	j	$L150
	addiu	$4,$4,-2020
	.set	macro
	.set	reorder

$L227:
	addiu	$4,$4,84
	sw	$4,4($19)
	lw	$4,152($sp)
	li	$5,321650688			# 0x132c0000
	or	$4,$4,$5
	sw	$4,8($19)
	li	$4,-402653184			# 0xffffffffe8000000
	ori	$4,$4,0xffff
	sw	$4,12($19)
	li	$4,-838860800			# 0xffffffffce000000
	sw	$4,0($19)
	lb	$4,5($20)
$L240:
	lw	$5,19660($fp)
	addu	$4,$4,$5
	sltu	$5,$4,52
	bne	$5,$0,$L217
	bltz	$4,$L229
	addiu	$4,$4,-52
$L217:
	sw	$4,19660($fp)
$L128:
	addiu	$4,$fp,19660
	lw	$9,19660($fp)
	addu	$5,$4,$9
	lbu	$11,4($5)
	lbu	$10,260($5)
	lw	$22,156($sp)
	bne	$21,$0,$L230
$L131:
	sll	$4,$18,8
	lw	$3,184($sp)
	lw	$8,212($sp)
	mul	$5,$3,$8
	addu	$4,$5,$4
	lw	$8,228($sp)
	addu	$19,$4,$8
	sll	$4,$18,7
	lw	$8,224($sp)
	mul	$5,$3,$8
	addu	$4,$5,$4
	addu	$23,$4,$23
	lw	$5,172($sp)
	lw	$4,168($sp)
	sw	$3,16($sp)
	sw	$9,20($sp)
	sw	$11,24($sp)
	sw	$10,28($sp)
	sw	$5,32($sp)
	sw	$4,36($sp)
	sw	$22,40($sp)
	sll	$4,$18,2
	sll	$5,$18,4
	addu	$4,$4,$5
	subu	$4,$4,$18
	sll	$4,$4,2
	addiu	$12,$fp,22912
	addu	$4,$4,$12
	sw	$4,44($sp)
	sll	$4,$2,5
	sll	$5,$2,8
	subu	$4,$5,$4
	addiu	$5,$fp,32640
	addu	$4,$4,$5
	sw	$4,48($sp)
	lw	$8,240($sp)
	sw	$8,52($sp)
	lw	$3,236($sp)
	sw	$3,56($sp)
	lw	$4,180($sp)
	move	$5,$20
	lw	$6,216($sp)
	move	$7,$18
	sw	$2,268($sp)
	sw	$9,256($sp)
	sw	$10,264($sp)
	sw	$11,260($sp)
	.set	noreorder
	.set	nomacro
	jal	vlc_hw_dblk_mb_aux_c
	sw	$12,252($sp)
	.set	macro
	.set	reorder

	move	$4,$18
	move	$5,$20
	lw	$12,252($sp)
	.set	noreorder
	.set	nomacro
	jal	backup_sde_vlc_aux
	move	$6,$12
	.set	macro
	.set	reorder

	lw	$8,212($sp)
	subu	$7,$19,$8
	addiu	$7,$7,192
	lw	$3,224($sp)
	subu	$4,$23,$3
	addiu	$4,$4,64
	addiu	$6,$19,-244
	addiu	$5,$23,-124
	sw	$19,4($16)
	sw	$23,20($16)
	addiu	$8,$23,8
	sw	$8,36($16)
	sw	$7,52($16)
	sw	$4,68($16)
	addiu	$4,$4,8
	sw	$4,84($16)
	li	$4,7			# 0x7
	lw	$2,268($sp)
	lw	$9,256($sp)
	lw	$10,264($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$4,$L218
	lw	$11,260($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L133
	li	$4,1			# 0x1
	.set	macro
	.set	reorder

	lw	$8,180($sp)
	beq	$8,$4,$L218
	li	$4,321781760			# 0x132e0000
	sw	$4,100($16)
	sw	$4,116($16)
	sw	$4,132($16)
$L133:
	lw	$4,22872($fp)
$L241:
	addiu	$4,$4,-1
	lw	$3,184($sp)
	.set	noreorder
	.set	nomacro
	beq	$4,$3,$L231
	li	$4,1048576			# 0x100000
	.set	macro
	.set	reorder

	addiu	$4,$4,192
	sw	$4,12($16)
	li	$4,524288			# 0x80000
	addiu	$4,$4,32
	sw	$4,28($16)
	sw	$4,44($16)
$L136:
	lw	$4,156($sp)
	addiu	$4,$4,1
	sw	$4,0($17)
	lw	$6,148($sp)
	lw	$5,144($sp)
	li	$4,-16777216			# 0xffffffffff000000
	and	$4,$5,$4
	.set	noreorder
	.set	nomacro
	beq	$4,$fp,$L232
	li	$4,321650688			# 0x132c0000
	.set	macro
	.set	reorder

$L137:
	sw	$5,12($6)
	lw	$4,148($sp)
	lhu	$5,24($20)
	sh	$5,16($4)
	lw	$4,148($sp)
	lw	$5,20($20)
	sw	$5,0($4)
	lw	$5,148($sp)
	li	$4,-16777216			# 0xffffffffff000000
	lw	$8,208($sp)
	and	$4,$8,$4
	.set	noreorder
	.set	nomacro
	beq	$4,$fp,$L233
	andi	$6,$8,0xffff
	.set	macro
	.set	reorder

$L138:
	lw	$3,208($sp)
	sw	$3,8($5)
	lw	$4,148($sp)
	lbu	$5,36($20)
	sh	$5,24($4)
	lw	$4,148($sp)
	lw	$5,28($20)
	sw	$5,28($4)
	lw	$4,148($sp)
	lw	$5,32($20)
	sw	$5,32($4)
	lw	$4,148($sp)
	lb	$5,2($20)
	sh	$5,26($4)
	lw	$4,148($sp)
	sb	$22,18($4)
	lw	$5,148($sp)
	xori	$4,$2,0x7
	li	$6,128			# 0x80
	movz	$6,$0,$4
	sb	$6,19($5)
	lw	$5,148($sp)
	addiu	$4,$sp,64
	addu	$11,$4,$11
	lbu	$4,0($11)
	andi	$4,$4,0x7f
	sll	$4,$4,7
	addiu	$6,$sp,64
	addu	$10,$6,$10
	lbu	$6,0($10)
	andi	$6,$6,0x7f
	sll	$6,$6,14
	or	$4,$4,$6
	addiu	$7,$sp,64
	addu	$9,$7,$9
	lbu	$6,0($9)
	andi	$6,$6,0x7f
	or	$4,$4,$6
	sw	$4,4($5)
	addiu	$5,$22,15
	slt	$4,$22,0
	movz	$5,$22,$4
	andi	$4,$5,0x10
	andi	$22,$22,0xf
	or	$22,$4,$22
	li	$4,320864256			# 0x13200000
	addiu	$4,$4,108
#APP
 # 401 "h264_cavlc_p1.c" 1
	sw	 $22,0($4)	#i_sw
 # 0 "" 2
#NO_APP
	.set	noreorder
	.set	nomacro
	bne	$21,$0,$L234
	li	$4,8			# 0x8
	.set	macro
	.set	reorder

	addiu	$2,$2,1
	.set	noreorder
	.set	nomacro
	beq	$2,$4,$L166
	li	$4,7			# 0x7
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$4,$L144
	ori	$7,$fp,0x8694
	.set	macro
	.set	reorder

$L143:
	lw	$8,196($sp)
	addiu	$7,$8,816
$L144:
	lw	$4,156($sp)
	addiu	$4,$4,1
	sw	$4,156($sp)
	lw	$10,188($sp)
	addiu	$4,$10,1
	lw	$3,176($sp)
	.set	noreorder
	.set	nomacro
	beq	$3,$4,$L236
	lw	$21,204($sp)
	.set	macro
	.set	reorder

	lw	$6,204($sp)
$L145:
	lw	$8,180($sp)
	addiu	$8,$8,1
	sw	$8,180($sp)
	lw	$18,188($sp)
	lw	$10,204($sp)
	sw	$10,184($sp)
	lw	$3,196($sp)
	sw	$3,208($sp)
	lw	$5,220($sp)
	lw	$8,192($sp)
	sw	$8,220($sp)
	sw	$5,192($sp)
	move	$5,$20
	lw	$20,200($sp)
	sw	$5,200($sp)
	sw	$7,196($sp)
	move	$22,$18
	sw	$6,204($sp)
	.set	noreorder
	.set	nomacro
	j	$L146
	sw	$4,188($sp)
	.set	macro
	.set	reorder

$L225:
	addiu	$21,$21,1
	lw	$4,22872($4)
	slt	$4,$21,$4
	bne	$4,$0,$L89
	.set	noreorder
	.set	nomacro
	j	$L90
	li	$21,1			# 0x1
	.set	macro
	.set	reorder

$L156:
	move	$15,$0
	move	$4,$0
	addiu	$3,$sp,136
	li	$6,65536			# 0x10000
	li	$25,1			# 0x1
	li	$9,4			# 0x4
	addiu	$8,$6,96
$L105:
	sll	$6,$15,1
	addu	$6,$20,$6
	lh	$7,36($6)
	andi	$6,$7,0x1000
	sw	$6,136($sp)
	andi	$6,$7,0x4000
	sw	$6,140($sp)
	andi	$6,$7,0x5000
	xori	$6,$6,0x5000
	andi	$10,$7,0x8
	.set	noreorder
	.set	nomacro
	beq	$10,$0,$L110
	sltu	$6,$6,1
	.set	macro
	.set	reorder

	andi	$7,$15,0x2
	sll	$7,$7,10
	li	$10,65536			# 0x10000
	ori	$10,$10,0xa0
	or	$7,$7,$10
	andi	$11,$15,0x1
	sll	$11,$11,1
	sll	$11,$11,8
	or	$7,$7,$11
	sll	$6,$6,31
	or	$11,$7,$6
	move	$10,$0
	move	$12,$0
$L112:
	addu	$6,$3,$10
	lw	$6,0($6)
	beq	$6,$0,$L111
	addiu	$7,$sp,128
	addu	$6,$7,$10
	lw	$13,0($6)
	lw	$6,0($13)
	sw	$6,0($5)
	sll	$6,$12,30
	or	$6,$6,$11
	addu	$7,$31,$10
	lbu	$7,28($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$6,$6,$7
	lhu	$7,2($13)
	andi	$7,$7,0x7
	sll	$7,$7,20
	or	$6,$6,$7
	sw	$6,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L111:
	.set	noreorder
	.set	nomacro
	bne	$12,$25,$L159
	addiu	$10,$10,4
	.set	macro
	.set	reorder

	lw	$6,128($sp)
	addiu	$6,$6,4
	sw	$6,128($sp)
	lw	$6,132($sp)
	addiu	$6,$6,4
	sw	$6,132($sp)
$L113:
	addiu	$15,$15,1
$L242:
	.set	noreorder
	.set	nomacro
	bne	$15,$9,$L105
	addiu	$31,$31,1
	.set	macro
	.set	reorder

$L99:
	lw	$6,-4($5)
	li	$7,33554432			# 0x2000000
	or	$6,$6,$7
	sw	$6,-4($5)
	li	$6,-805306368			# 0xffffffffd0000000
	addiu	$6,$6,84
	sw	$6,0($5)
	lw	$6,152($sp)
	li	$7,321650688			# 0x132c0000
	or	$6,$6,$7
	sw	$6,4($5)
	li	$6,-402653184			# 0xffffffffe8000000
	ori	$6,$6,0xffff
	sw	$6,8($5)
	andi	$5,$18,0xff
	li	$6,-838860800			# 0xffffffffce000000
	or	$5,$5,$6
	lw	$8,184($sp)
	sll	$6,$8,8
	andi	$6,$6,0xffff
	or	$5,$5,$6
	andi	$4,$4,0x7f
	sll	$4,$4,16
	or	$4,$5,$4
	sw	$4,0($19)
	.set	noreorder
	.set	nomacro
	j	$L240
	lb	$4,5($20)
	.set	macro
	.set	reorder

$L218:
	sw	$6,100($16)
	sw	$5,116($16)
	addiu	$23,$23,-116
	sw	$23,132($16)
	.set	noreorder
	.set	nomacro
	j	$L241
	lw	$4,22872($fp)
	.set	macro
	.set	reorder

$L222:
	sw	$3,204($sp)
	.set	noreorder
	.set	nomacro
	j	$L84
	move	$25,$0
	.set	macro
	.set	reorder

$L230:
	.set	noreorder
	.set	nomacro
	j	$L131
	addiu	$22,$22,1
	.set	macro
	.set	reorder

$L233:
	li	$4,321650688			# 0x132c0000
	or	$4,$6,$4
	.set	noreorder
	.set	nomacro
	j	$L138
	sw	$4,208($sp)
	.set	macro
	.set	reorder

$L232:
	andi	$5,$5,0xffff
	.set	noreorder
	.set	nomacro
	j	$L137
	or	$5,$5,$4
	.set	macro
	.set	reorder

$L231:
	addiu	$4,$4,256
	sw	$4,12($16)
	li	$4,524288			# 0x80000
	addiu	$4,$4,64
	sw	$4,28($16)
	sw	$4,44($16)
	j	$L136
$L236:
	addiu	$6,$21,1
	.set	noreorder
	.set	nomacro
	j	$L145
	move	$4,$0
	.set	macro
	.set	reorder

$L166:
	.set	noreorder
	.set	nomacro
	j	$L143
	move	$2,$0
	.set	macro
	.set	reorder

$L96:
	andi	$4,$6,0x10
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L100
	andi	$4,$6,0x20
	.set	macro
	.set	reorder

	andi	$4,$6,0x1000
	sw	$4,136($sp)
	andi	$4,$6,0x4000
	sw	$4,140($sp)
	andi	$7,$6,0x5000
	xori	$7,$7,0x5000
	sltu	$7,$7,1
	sll	$7,$7,31
	move	$11,$0
	move	$12,$0
	move	$4,$0
	addiu	$3,$sp,136
	li	$22,65536			# 0x10000
	addiu	$22,$22,176
	or	$22,$7,$22
	li	$14,1			# 0x1
$L102:
	addu	$7,$3,$11
	lw	$7,0($7)
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L101
	addiu	$8,$sp,128
	.set	macro
	.set	reorder

	addu	$7,$8,$11
	lw	$9,0($7)
	lw	$7,0($9)
	sw	$7,0($5)
	sll	$7,$12,30
	or	$7,$22,$7
	addu	$8,$20,$11
	lbu	$8,28($8)
	sll	$8,$8,12
	andi	$8,$8,0xffff
	or	$7,$7,$8
	lhu	$8,2($9)
	andi	$8,$8,0x7
	sll	$8,$8,20
	or	$7,$7,$8
	sw	$7,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L101:
	.set	noreorder
	.set	nomacro
	bne	$12,$14,$L154
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	lw	$7,128($sp)
	addiu	$7,$7,4
	sw	$7,128($sp)
	lw	$7,132($sp)
	addiu	$7,$7,4
	sw	$7,132($sp)
	andi	$7,$6,0x2000
	sw	$7,136($sp)
	andi	$7,$6,0x8000
	sw	$7,140($sp)
	andi	$6,$6,0xa000
	xori	$6,$6,0xa000
	sltu	$6,$6,1
	sll	$6,$6,31
	move	$10,$0
	move	$11,$0
	li	$22,65536			# 0x10000
	addiu	$22,$22,2224
	or	$22,$6,$22
	li	$14,1			# 0x1
$L104:
	addu	$6,$3,$10
	lw	$6,0($6)
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L103
	addiu	$7,$sp,128
	.set	macro
	.set	reorder

	addu	$6,$7,$10
	lw	$8,0($6)
	lw	$6,0($8)
	sw	$6,0($5)
	sll	$6,$11,30
	or	$6,$22,$6
	addu	$7,$20,$10
	lbu	$7,30($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$6,$6,$7
	lhu	$7,2($8)
	andi	$7,$7,0x7
	sll	$7,$7,20
	or	$6,$6,$7
	sw	$6,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L103:
	.set	noreorder
	.set	nomacro
	beq	$11,$14,$L99
	addiu	$10,$10,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L104
	move	$11,$12
	.set	macro
	.set	reorder

$L100:
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L156
	move	$31,$20
	.set	macro
	.set	reorder

	andi	$4,$6,0x1000
	sw	$4,136($sp)
	andi	$4,$6,0x4000
	sw	$4,140($sp)
	andi	$7,$6,0x5000
	xori	$7,$7,0x5000
	sltu	$7,$7,1
	sll	$7,$7,31
	move	$11,$0
	move	$12,$0
	move	$4,$0
	addiu	$3,$sp,136
	li	$14,65536			# 0x10000
	addiu	$14,$14,224
	or	$14,$7,$14
	li	$22,1			# 0x1
$L107:
	addu	$7,$3,$11
	lw	$7,0($7)
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L106
	addiu	$8,$sp,128
	.set	macro
	.set	reorder

	addu	$7,$8,$11
	lw	$9,0($7)
	lw	$7,0($9)
	sw	$7,0($5)
	sll	$7,$12,30
	or	$7,$14,$7
	addu	$8,$20,$11
	lbu	$8,28($8)
	sll	$8,$8,12
	andi	$8,$8,0xffff
	or	$7,$7,$8
	lhu	$8,2($9)
	andi	$8,$8,0x7
	sll	$8,$8,20
	or	$7,$7,$8
	sw	$7,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L106:
	.set	noreorder
	.set	nomacro
	beq	$12,$22,$L237
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L107
	li	$12,1			# 0x1
	.set	macro
	.set	reorder

$L229:
	addiu	$4,$4,52
	.set	noreorder
	.set	nomacro
	j	$L128
	sw	$4,19660($fp)
	.set	macro
	.set	reorder

$L228:
	addiu	$4,$20,80
	.set	noreorder
	.set	nomacro
	j	$L95
	sw	$4,128($sp)
	.set	macro
	.set	reorder

$L154:
	.set	noreorder
	.set	nomacro
	j	$L102
	li	$12,1			# 0x1
	.set	macro
	.set	reorder

$L226:
	lhu	$4,6($20)
	bne	$4,$0,$L244
	.set	noreorder
	.set	nomacro
	j	$L90
	li	$21,1			# 0x1
	.set	macro
	.set	reorder

$L159:
	.set	noreorder
	.set	nomacro
	j	$L112
	li	$12,1			# 0x1
	.set	macro
	.set	reorder

$L110:
	andi	$10,$7,0x10
	.set	noreorder
	.set	nomacro
	beq	$10,$0,$L114
	andi	$7,$7,0x20
	.set	macro
	.set	reorder

	sll	$6,$6,31
	andi	$22,$15,0x2
	andi	$11,$15,0x1
	sll	$11,$11,1
	sll	$11,$11,8
	sll	$12,$22,10
	or	$12,$12,$8
	or	$12,$12,$11
	or	$12,$12,$6
	move	$13,$0
	move	$14,$0
$L116:
	addu	$7,$3,$13
	lw	$7,0($7)
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L115
	addiu	$10,$sp,128
	.set	macro
	.set	reorder

	addu	$7,$10,$13
	lw	$10,0($7)
	lw	$7,0($10)
	sw	$7,0($5)
	sll	$7,$14,30
	or	$24,$7,$12
	addu	$7,$31,$13
	lbu	$7,28($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$7,$24,$7
	lhu	$10,2($10)
	andi	$10,$10,0x7
	sll	$10,$10,20
	or	$7,$7,$10
	sw	$7,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L115:
	.set	noreorder
	.set	nomacro
	beq	$14,$25,$L238
	addiu	$13,$13,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L116
	li	$14,1			# 0x1
	.set	macro
	.set	reorder

$L114:
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L239
	sll	$6,$6,31
	.set	macro
	.set	reorder

	andi	$22,$15,0x2
	sll	$22,$22,10
	li	$10,65536			# 0x10000
	ori	$10,$10,0x90
	or	$22,$22,$10
	andi	$11,$15,0x1
	sll	$11,$11,1
	sll	$13,$11,8
	or	$13,$22,$13
	or	$13,$13,$6
	move	$12,$0
	move	$14,$0
$L122:
	addu	$7,$3,$12
	lw	$7,0($7)
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L121
	addiu	$10,$sp,128
	.set	macro
	.set	reorder

	addu	$7,$10,$12
	lw	$10,0($7)
	lw	$7,0($10)
	sw	$7,0($5)
	sll	$7,$14,30
	or	$24,$7,$13
	addu	$7,$31,$12
	lbu	$7,28($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$7,$24,$7
	lhu	$10,2($10)
	andi	$10,$10,0x7
	sll	$10,$10,20
	or	$7,$7,$10
	sw	$7,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L121:
	.set	noreorder
	.set	nomacro
	bne	$14,$25,$L162
	addiu	$12,$12,4
	.set	macro
	.set	reorder

	lw	$13,128($sp)
	addiu	$7,$13,4
	sw	$7,128($sp)
	lw	$12,132($sp)
	addiu	$7,$12,4
	sw	$7,132($sp)
	or	$6,$6,$22
	addiu	$7,$11,1
	sll	$7,$7,8
	or	$6,$6,$7
	move	$11,$0
	move	$22,$0
$L124:
	addu	$7,$3,$11
	lw	$7,0($7)
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L123
	addiu	$10,$sp,128
	.set	macro
	.set	reorder

	addu	$7,$10,$11
	lw	$10,0($7)
	lw	$7,0($10)
	sw	$7,0($5)
	sll	$7,$22,30
	or	$24,$7,$6
	addu	$7,$31,$11
	lbu	$7,28($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$7,$24,$7
	lhu	$10,2($10)
	andi	$10,$10,0x7
	sll	$10,$10,20
	or	$7,$7,$10
	sw	$7,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L123:
	.set	noreorder
	.set	nomacro
	beq	$22,$25,$L216
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L124
	move	$22,$14
	.set	macro
	.set	reorder

$L238:
	lw	$13,128($sp)
	addiu	$7,$13,4
	sw	$7,128($sp)
	lw	$12,132($sp)
	addiu	$7,$12,4
	sw	$7,132($sp)
	addiu	$7,$22,1
	sll	$7,$7,10
	or	$7,$7,$8
	or	$7,$11,$7
	or	$6,$6,$7
	move	$11,$0
	move	$22,$0
$L118:
	addu	$7,$3,$11
	lw	$7,0($7)
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L117
	addiu	$10,$sp,128
	.set	macro
	.set	reorder

	addu	$7,$10,$11
	lw	$10,0($7)
	lw	$7,0($10)
	sw	$7,0($5)
	sll	$7,$22,30
	or	$24,$7,$6
	addu	$7,$31,$11
	lbu	$7,28($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$7,$24,$7
	lhu	$10,2($10)
	andi	$10,$10,0x7
	sll	$10,$10,20
	or	$7,$7,$10
	sw	$7,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L117:
	.set	noreorder
	.set	nomacro
	beq	$22,$25,$L216
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L118
	move	$22,$14
	.set	macro
	.set	reorder

$L216:
	addiu	$13,$13,8
	sw	$13,128($sp)
	addiu	$12,$12,8
	.set	noreorder
	.set	nomacro
	j	$L113
	sw	$12,132($sp)
	.set	macro
	.set	reorder

$L237:
	lw	$7,128($sp)
	addiu	$7,$7,4
	sw	$7,128($sp)
	lw	$7,132($sp)
	addiu	$7,$7,4
	sw	$7,132($sp)
	andi	$7,$6,0x2000
	sw	$7,136($sp)
	andi	$7,$6,0x8000
	sw	$7,140($sp)
	andi	$6,$6,0xa000
	xori	$6,$6,0xa000
	sltu	$6,$6,1
	sll	$6,$6,31
	move	$10,$0
	move	$11,$0
	li	$14,65536			# 0x10000
	addiu	$14,$14,736
	or	$14,$6,$14
	li	$22,1			# 0x1
$L109:
	addu	$6,$3,$10
	lw	$6,0($6)
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L108
	addiu	$7,$sp,128
	.set	macro
	.set	reorder

	addu	$6,$7,$10
	lw	$8,0($6)
	lw	$6,0($8)
	sw	$6,0($5)
	sll	$6,$11,30
	or	$6,$14,$6
	addu	$7,$20,$10
	lbu	$7,29($7)
	sll	$7,$7,12
	andi	$7,$7,0xffff
	or	$6,$6,$7
	lhu	$7,2($8)
	andi	$7,$7,0x7
	sll	$7,$7,20
	or	$6,$6,$7
	sw	$6,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L108:
	.set	noreorder
	.set	nomacro
	beq	$11,$22,$L99
	addiu	$10,$10,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L109
	move	$11,$12
	.set	macro
	.set	reorder

$L239:
	andi	$14,$15,0x1
	sll	$14,$14,1
	move	$12,$0
	andi	$24,$15,0x2
	li	$7,65536			# 0x10000
	ori	$7,$7,0x50
	or	$6,$6,$7
	sw	$6,248($sp)
$L120:
	andi	$6,$12,0x2
	sra	$6,$6,1
	addu	$6,$6,$24
	sll	$6,$6,10
	andi	$11,$12,0x1
	addu	$11,$11,$14
	sll	$11,$11,8
	move	$10,$0
	move	$13,$0
	lw	$7,248($sp)
	or	$11,$7,$11
	or	$11,$11,$6
$L126:
	addu	$6,$3,$10
	lw	$6,0($6)
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L125
	addiu	$7,$sp,128
	.set	macro
	.set	reorder

	addu	$6,$7,$10
	lw	$7,0($6)
	lw	$6,0($7)
	sw	$6,0($5)
	sll	$6,$13,30
	or	$22,$11,$6
	addu	$6,$31,$10
	lbu	$6,28($6)
	sll	$6,$6,12
	andi	$6,$6,0xffff
	or	$6,$22,$6
	lhu	$7,2($7)
	andi	$7,$7,0x7
	sll	$7,$7,20
	or	$6,$6,$7
	sw	$6,4($5)
	addiu	$4,$4,1
	addiu	$5,$5,8
$L125:
	.set	noreorder
	.set	nomacro
	bne	$13,$25,$L164
	addiu	$10,$10,4
	.set	macro
	.set	reorder

	lw	$6,128($sp)
	addiu	$6,$6,4
	sw	$6,128($sp)
	lw	$6,132($sp)
	addiu	$6,$6,4
	addiu	$12,$12,1
	.set	noreorder
	.set	nomacro
	bne	$12,$9,$L120
	sw	$6,132($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L242
	addiu	$15,$15,1
	.set	macro
	.set	reorder

$L164:
	.set	noreorder
	.set	nomacro
	j	$L126
	li	$13,1			# 0x1
	.set	macro
	.set	reorder

$L162:
	.set	noreorder
	.set	nomacro
	j	$L122
	li	$14,1			# 0x1
	.set	macro
	.set	reorder

$L234:
	move	$23,$2
	lw	$2,152($sp)
	li	$3,-1912602624			# 0xffffffff8e000000
	sw	$3,0($2)
	li	$4,320864256			# 0x13200000
$L142:
#APP
 # 446 "h264_cavlc_p1.c" 1
	lw	 $2,124($4)	#i_lw
 # 0 "" 2
#NO_APP
	lw	$3,156($sp)
	addiu	$3,$3,1
	xor	$2,$2,$3
	andi	$2,$2,0x1f
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L142
	li	$2,7			# 0x7
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$23,$2,$L147
	sll	$7,$23,4
	.set	macro
	.set	reorder

	li	$4,321847296			# 0x132f0000
	addiu	$3,$4,36
	li	$2,40173568			# 0x2650000
	ori	$2,$2,0xe12a
	addu	$2,$23,$2
	sll	$2,$2,3
	addiu	$2,$2,4
	addiu	$4,$4,2404
	addu	$6,$7,$4
	addu	$3,$7,$3
$L148:
	lw	$5,0($3)
	subu	$4,$3,$7
	addiu	$3,$3,148
	.set	noreorder
	.set	nomacro
	bne	$3,$6,$L148
	sw	$5,-16($4)
	.set	macro
	.set	reorder

	li	$5,321847296			# 0x132f0000
	addiu	$3,$5,2380
	addiu	$5,$5,2988
$L149:
	lw	$4,0($2)
	sw	$4,0($3)
	lw	$4,608($2)
	sw	$4,608($3)
	addiu	$3,$3,76
	.set	noreorder
	.set	nomacro
	bne	$3,$5,$L149
	addiu	$2,$2,76
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L243
	li	$3,320864256			# 0x13200000
	.set	macro
	.set	reorder

	.end	main
	.size	main, .-main
	.rdata
	.align	2
	.type	edg_msk_table.1774, @object
	.size	edg_msk_table.1774, 16
edg_msk_table.1774:
	.half	0
	.half	0
	.half	170
	.half	170
	.half	0
	.half	238
	.half	170
	.half	238
	.align	2
	.type	mv_msk_table.1773, @object
	.size	mv_msk_table.1773, 16
mv_msk_table.1773:
	.half	0
	.half	238
	.half	174
	.half	238
	.half	234
	.half	238
	.half	174
	.half	234

	.comm	mb_qp_up,120,4

	.comm	mb_qp_left,2,2

	.comm	nnz_left_dblk,4,4

	.comm	mv_left_dblk,32,4

	.comm	ref_left_dblk,8,4

	.comm	left_mb_type,4,4

	.comm	DBLK_BASE,4,4

	.comm	tcsm1_word_bank_backup,24,4
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
