.file	1 "h264_refs.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.add_sorted,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	add_sorted
.type	add_sorted, @function
add_sorted:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,16($sp)
li	$24,2147418112			# 0x7fff0000
move	$2,$0
beq	$11,$0,$L18
ori	$24,$24,0xffff

$L2:
blez	$6,$L20
li	$15,-2147483648			# 0xffffffff80000000

$L9:
move	$9,$0
move	$8,$5
$L5:
lw	$10,0($8)
addiu	$9,$9,1
addiu	$8,$8,4
slt	$12,$9,$6
lw	$3,276($10)
slt	$13,$7,$3
beq	$13,$11,$L4
slt	$14,$3,$15

beq	$14,$11,$L4
nop

sw	$10,0($4)
move	$15,$3
$L4:
bne	$12,$0,$L5
li	$3,-2147483648			# 0xffffffff80000000

movz	$3,$24,$11
beq	$3,$15,$L20
nop

lw	$3,0($4)
addiu	$2,$2,1
addiu	$4,$4,4
lw	$7,276($3)
bne	$11,$0,$L2
subu	$7,$7,$11

$L18:
bgtz	$6,$L9
move	$15,$24

$L20:
j	$31
nop

.set	macro
.set	reorder
.end	add_sorted
.size	add_sorted, .-add_sorted
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"%d %d %p\012\000"
.section	.text.find_short,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	find_short
.type	find_short, @function
find_short:
.frame	$sp,72,$31		# vars= 0, regs= 9/0, args= 24, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-72
sw	$21,56($sp)
li	$21,196608			# 0x30000
.cprestore	24
addu	$21,$4,$21
sw	$31,68($sp)
sw	$23,64($sp)
sw	$22,60($sp)
sw	$20,52($sp)
sw	$19,48($sp)
sw	$18,44($sp)
sw	$17,40($sp)
sw	$16,36($sp)
lw	$2,-15284($21)
blez	$2,$L27
li	$16,131072			# 0x20000

lw	$17,%got($LC0)($28)
move	$18,$6
addiu	$16,$16,10716
move	$23,$5
move	$22,$4
move	$19,$0
addiu	$17,$17,%lo($LC0)
b	$L26
addu	$16,$4,$16

$L24:
lw	$2,280($20)
beq	$2,$23,$L34
lw	$31,68($sp)

$L25:
lw	$2,-15284($21)
addiu	$19,$19,1
slt	$2,$19,$2
beq	$2,$0,$L35
lw	$31,68($sp)

$L26:
lw	$4,0($22)
addiu	$16,$16,4
lw	$3,404($4)
andi	$3,$3,0x800
beq	$3,$0,$L24
lw	$20,-4($16)

lw	$2,280($20)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
move	$7,$19
sw	$20,20($sp)
move	$6,$17
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

lw	$2,280($20)
bne	$2,$23,$L25
lw	$28,24($sp)

lw	$31,68($sp)
$L34:
move	$2,$20
lw	$23,64($sp)
lw	$22,60($sp)
lw	$21,56($sp)
lw	$20,52($sp)
lw	$17,40($sp)
lw	$16,36($sp)
sw	$19,0($18)
lw	$19,48($sp)
lw	$18,44($sp)
j	$31
addiu	$sp,$sp,72

$L27:
lw	$31,68($sp)
$L35:
move	$2,$0
lw	$23,64($sp)
lw	$22,60($sp)
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,72

.set	macro
.set	reorder
.end	find_short
.size	find_short, .-find_short
.section	.text.remove_short_at_index,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	remove_short_at_index
.type	remove_short_at_index, @function
remove_short_at_index:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$7,196608			# 0x30000
move	$8,$4
addu	$7,$4,$7
li	$4,35446			# 0x8a76
lw	$3,-15284($7)
addu	$2,$5,$4
sll	$2,$2,2
addiu	$3,$3,-1
addu	$2,$8,$2
sw	$0,4($2)
bne	$3,$0,$L38
sw	$3,-15284($7)

j	$31
nop

$L38:
addiu	$7,$5,1
lw	$25,%call16(memmove)($28)
subu	$6,$3,$5
addu	$5,$7,$4
sll	$6,$6,2
sll	$5,$5,2
addiu	$4,$2,4
addu	$5,$8,$5
.reloc	1f,R_MIPS_JALR,memmove
1:	jr	$25
addiu	$5,$5,4

.set	macro
.set	reorder
.end	remove_short_at_index
.size	remove_short_at_index, .-remove_short_at_index
.section	.text.split_field_copy,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	split_field_copy
.type	split_field_copy, @function
split_field_copy:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,80($5)
and	$2,$6,$2
sltu	$2,$0,$2
beq	$2,$0,$L52
addiu	$8,$5,592

move	$3,$4
$L44:
lw	$12,0($5)
addiu	$5,$5,16
addiu	$3,$3,16
lw	$11,-12($5)
lw	$10,-8($5)
lw	$9,-4($5)
sw	$12,-16($3)
sw	$11,-12($3)
sw	$10,-8($3)
bne	$5,$8,$L44
sw	$9,-4($3)

lw	$8,0($5)
lw	$5,4($5)
sw	$8,0($3)
sw	$5,4($3)
li	$3,3			# 0x3
beq	$6,$3,$L52
addiu	$3,$4,16

addiu	$9,$4,32
li	$8,2			# 0x2
$L48:
beq	$6,$8,$L51
lw	$5,0($3)

$L47:
sll	$5,$5,1
addiu	$3,$3,4
bne	$3,$9,$L48
sw	$5,-4($3)

xori	$3,$6,0x2
lw	$5,288($4)
sltu	$3,$3,1
sw	$6,80($4)
addiu	$3,$3,66
sll	$5,$5,1
sll	$3,$3,2
addu	$7,$7,$5
addu	$3,$4,$3
lw	$3,4($3)
sw	$7,288($4)
sw	$3,276($4)
$L52:
j	$31
nop

$L51:
lw	$10,-16($3)
addu	$10,$10,$5
b	$L47
sw	$10,-16($3)

.set	macro
.set	reorder
.end	split_field_copy
.size	split_field_copy, .-split_field_copy
.section	.text.build_def_list,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	build_def_list
.type	build_def_list, @function
build_def_list:
.frame	$sp,72,$31		# vars= 8, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-72
move	$2,$0
sw	$23,60($sp)
slt	$10,$0,$6
sw	$18,40($sp)
sw	$20,48($sp)
move	$20,$0
sw	$16,32($sp)
move	$16,$0
sw	$fp,64($sp)
sw	$22,56($sp)
move	$22,$6
sw	$19,44($sp)
move	$19,$4
sw	$17,36($sp)
move	$17,$5
.cprestore	16
sw	$31,68($sp)
sw	$21,52($sp)
sw	$7,84($sp)
lw	$23,88($sp)
lw	$18,%got(split_field_copy)($28)
xori	$fp,$23,0x3
$L54:
beq	$10,$0,$L103
slt	$21,$16,$22

$L101:
sll	$4,$20,2
addu	$4,$17,$4
$L55:
lw	$3,0($4)
beq	$3,$0,$L59
addiu	$4,$4,4

lw	$3,80($3)
and	$3,$23,$3
bne	$3,$0,$L99
slt	$21,$16,$22

$L59:
addiu	$20,$20,1
bne	$20,$22,$L55
slt	$3,$16,$20

beq	$3,$0,$L54
move	$10,$0

$L57:
sll	$8,$16,2
$L104:
addu	$8,$17,$8
$L61:
lw	$3,0($8)
beq	$3,$0,$L62
addiu	$8,$8,4

lw	$3,80($3)
and	$3,$fp,$3
bne	$3,$0,$L100
nop

$L62:
addiu	$16,$16,1
slt	$21,$16,$22
bne	$21,$0,$L61
nop

$L58:
beq	$10,$0,$L67
sll	$3,$20,2

addu	$3,$17,$3
lw	$5,0($3)
lw	$3,84($sp)
bne	$3,$0,$L64
move	$4,$20

$L102:
lw	$4,280($5)
$L64:
sw	$4,288($5)
$L105:
sll	$3,$2,3
sll	$4,$2,5
addiu	$8,$2,1
addu	$3,$3,$4
li	$7,1			# 0x1
sll	$4,$3,4
sw	$8,24($sp)
addiu	$25,$18,%lo(split_field_copy)
subu	$4,$4,$3
move	$6,$23
addu	$4,$19,$4
.reloc	1f,R_MIPS_JALR,split_field_copy
1:	jalr	$25
addiu	$20,$20,1

lw	$8,24($sp)
slt	$10,$20,$22
move	$2,$8
$L63:
beq	$21,$0,$L54
sll	$3,$16,2

addu	$3,$17,$3
lw	$5,0($3)
lw	$3,84($sp)
bne	$3,$0,$L66
move	$4,$16

lw	$4,280($5)
$L66:
sw	$4,288($5)
sll	$3,$2,3
sll	$4,$2,5
sw	$10,24($sp)
move	$7,$0
addu	$3,$3,$4
addiu	$25,$18,%lo(split_field_copy)
sll	$4,$3,4
move	$6,$fp
subu	$4,$4,$3
addiu	$21,$2,1
addu	$4,$19,$4
.reloc	1f,R_MIPS_JALR,split_field_copy
1:	jalr	$25
addiu	$16,$16,1

lw	$10,24($sp)
bne	$10,$0,$L101
move	$2,$21

$L67:
slt	$21,$16,$22
$L103:
bne	$21,$0,$L104
sll	$8,$16,2

lw	$31,68($sp)
lw	$fp,64($sp)
lw	$23,60($sp)
lw	$22,56($sp)
lw	$21,52($sp)
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
j	$31
addiu	$sp,$sp,72

$L99:
bne	$21,$0,$L57
slt	$10,$20,$22

b	$L58
nop

$L100:
beq	$10,$0,$L63
slt	$21,$16,$22

sll	$3,$20,2
addu	$3,$17,$3
lw	$5,0($3)
lw	$3,84($sp)
beq	$3,$0,$L102
move	$4,$20

b	$L105
sw	$4,288($5)

.set	macro
.set	reorder
.end	build_def_list
.size	build_def_list, .-build_def_list
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"short term list:\012\000"
.align	2
$LC2:
.ascii	"%d fn:%d poc:%d %p\012\000"
.section	.text.print_short_term,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	print_short_term
.type	print_short_term, @function
print_short_term:
.frame	$sp,64,$31		# vars= 0, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,0($4)
lw	$2,404($3)
andi	$2,$2,0x800
bne	$2,$0,$L117
lw	$6,%got($LC1)($28)

j	$31
nop

$L117:
addiu	$sp,$sp,-64
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
sw	$20,56($sp)
li	$20,196608			# 0x30000
sw	$19,52($sp)
move	$19,$4
addu	$20,$19,$20
.cprestore	32
addiu	$6,$6,%lo($LC1)
sw	$31,60($sp)
sw	$18,48($sp)
move	$4,$3
sw	$17,44($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$16,40($sp)

lw	$2,-15284($20)
beq	$2,$0,$L106
lw	$28,32($sp)

li	$17,131072			# 0x20000
lw	$18,%got($LC2)($28)
move	$16,$0
addiu	$17,$17,10716
addiu	$18,$18,%lo($LC2)
addu	$17,$19,$17
$L109:
lw	$2,0($17)
li	$5,48			# 0x30
lw	$4,0($19)
move	$7,$16
lw	$25,%call16(av_log)($28)
move	$6,$18
addiu	$16,$16,1
lw	$3,280($2)
addiu	$17,$17,4
sw	$3,16($sp)
lw	$3,276($2)
sw	$3,20($sp)
lw	$2,0($2)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,24($sp)

lw	$2,-15284($20)
sltu	$2,$16,$2
bne	$2,$0,$L109
lw	$28,32($sp)

$L106:
lw	$31,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	print_short_term
.size	print_short_term, .-print_short_term
.section	.rodata.str1.4
.align	2
$LC3:
.ascii	"long term list:\012\000"
.section	.text.print_long_term,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	print_long_term
.type	print_long_term, @function
print_long_term:
.frame	$sp,64,$31		# vars= 0, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,0($4)
lw	$2,404($3)
andi	$2,$2,0x800
bne	$2,$0,$L130
lw	$6,%got($LC3)($28)

j	$31
nop

$L130:
addiu	$sp,$sp,-64
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
.cprestore	32
addiu	$6,$6,%lo($LC3)
sw	$20,56($sp)
sw	$19,52($sp)
li	$19,16			# 0x10
sw	$18,48($sp)
move	$18,$4
sw	$17,44($sp)
move	$4,$3
sw	$16,40($sp)
li	$17,131072			# 0x20000
sw	$31,60($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$16,$0

addiu	$17,$17,10844
lw	$28,32($sp)
addu	$17,$18,$17
lw	$20,%got($LC2)($28)
addiu	$20,$20,%lo($LC2)
$L121:
lw	$2,0($17)
move	$7,$16
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
addiu	$17,$17,4
beq	$2,$0,$L120
addiu	$16,$16,1

lw	$3,280($2)
move	$6,$20
lw	$4,0($18)
sw	$3,16($sp)
lw	$3,276($2)
sw	$3,20($sp)
lw	$2,0($2)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,24($sp)

lw	$28,32($sp)
$L120:
bne	$16,$19,$L121
lw	$31,60($sp)

lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	print_long_term
.size	print_long_term, .-print_long_term
.section	.rodata.str1.4
.align	2
$LC4:
.ascii	"remove short %d count %d\012\000"
.section	.text.remove_short,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	remove_short
.type	remove_short, @function
remove_short:
.frame	$sp,64,$31		# vars= 16, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,52($sp)
move	$17,$4
lw	$4,0($4)
.cprestore	24
sw	$18,56($sp)
move	$18,$5
sw	$16,48($sp)
sw	$31,60($sp)
lw	$2,404($4)
andi	$2,$2,0x800
bne	$2,$0,$L153
move	$16,$6

$L132:
lw	$25,%got(find_short)($28)
addiu	$6,$sp,32
move	$4,$17
addiu	$25,$25,%lo(find_short)
.reloc	1f,R_MIPS_JALR,find_short
1:	jalr	$25
move	$5,$18

beq	$2,$0,$L143
lw	$28,24($sp)

lw	$6,80($2)
and	$6,$16,$6
beq	$6,$0,$L154
sw	$6,80($2)

$L143:
lw	$31,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

$L153:
li	$2,196608			# 0x30000
lw	$6,%got($LC4)($28)
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
addu	$2,$17,$2
addiu	$6,$6,%lo($LC4)
move	$7,$18
lw	$2,-15284($2)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L132
lw	$28,24($sp)

$L154:
li	$3,196608			# 0x30000
addu	$4,$17,$3
lw	$4,-16160($4)
beq	$4,$0,$L156
lw	$25,%got(remove_short_at_index)($28)

beq	$2,$4,$L157
addiu	$3,$3,-16156

b	$L137
addu	$3,$17,$3

$L138:
beq	$2,$5,$L157
lw	$25,%got(remove_short_at_index)($28)

$L137:
lw	$5,0($3)
bne	$5,$0,$L138
addiu	$3,$3,4

lw	$25,%got(remove_short_at_index)($28)
$L156:
move	$4,$17
lw	$5,32($sp)
addiu	$25,$25,%lo(remove_short_at_index)
.reloc	1f,R_MIPS_JALR,remove_short_at_index
1:	jalr	$25
sw	$2,40($sp)

lw	$2,40($sp)
$L155:
lw	$31,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

$L157:
li	$3,4			# 0x4
lw	$5,32($sp)
move	$4,$17
addiu	$25,$25,%lo(remove_short_at_index)
sw	$3,80($2)
.reloc	1f,R_MIPS_JALR,remove_short_at_index
1:	jalr	$25
sw	$2,40($sp)

b	$L155
lw	$2,40($sp)

.set	macro
.set	reorder
.end	remove_short
.size	remove_short, .-remove_short
.section	.text.remove_long,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	remove_long
.type	remove_long, @function
remove_long:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$5,$5,2
li	$2,131072			# 0x20000
addu	$5,$4,$5
addu	$2,$5,$2
lw	$2,10844($2)
beq	$2,$0,$L177
nop

lw	$3,80($2)
and	$6,$6,$3
bne	$6,$0,$L177
sw	$6,80($2)

li	$3,196608			# 0x30000
addu	$6,$4,$3
lw	$6,-16160($6)
beq	$6,$0,$L160
nop

beq	$2,$6,$L161
addiu	$3,$3,-16156

b	$L163
addu	$3,$4,$3

$L164:
beq	$2,$6,$L161
nop

$L163:
lw	$6,0($3)
bne	$6,$0,$L164
addiu	$3,$3,4

$L160:
li	$3,131072			# 0x20000
sw	$0,292($2)
addu	$5,$5,$3
li	$3,196608			# 0x30000
sw	$0,10844($5)
addu	$4,$4,$3
lw	$3,-15288($4)
addiu	$3,$3,-1
sw	$3,-15288($4)
$L177:
j	$31
nop

$L161:
li	$3,4			# 0x4
sw	$0,292($2)
sw	$3,80($2)
li	$3,131072			# 0x20000
addu	$5,$5,$3
li	$3,196608			# 0x30000
sw	$0,10844($5)
addu	$4,$4,$3
lw	$3,-15288($4)
addiu	$3,$3,-1
j	$31
sw	$3,-15288($4)

.set	macro
.set	reorder
.end	remove_long
.size	remove_long, .-remove_long
.section	.text.ff_h264_fill_default_ref_list,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_fill_default_ref_list
.set	nomips16
.set	nomicromips
.ent	ff_h264_fill_default_ref_list
.type	ff_h264_fill_default_ref_list, @function
ff_h264_fill_default_ref_list:
.frame	$sp,824,$31		# vars= 752, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-824
li	$2,3			# 0x3
sw	$16,784($sp)
li	$16,65536			# 0x10000
sw	$fp,816($sp)
move	$fp,$4
addu	$16,$4,$16
.cprestore	24
sw	$31,820($sp)
sw	$23,812($sp)
sw	$22,808($sp)
sw	$21,804($sp)
sw	$20,800($sp)
sw	$19,796($sp)
sw	$18,792($sp)
sw	$17,788($sp)
lw	$4,-5260($16)
beq	$4,$2,$L198
li	$2,196608			# 0x30000

lw	$4,10384($fp)
li	$17,131072			# 0x20000
lw	$18,%got(build_def_list)($28)
addu	$2,$fp,$2
addiu	$20,$17,10976
sw	$4,16($sp)
addiu	$5,$17,10716
lw	$6,-15284($2)
addu	$19,$fp,$20
addiu	$18,$18,%lo(build_def_list)
addu	$5,$fp,$5
move	$7,$0
move	$25,$18
.reloc	1f,R_MIPS_JALR,build_def_list
1:	jalr	$25
move	$4,$19

addiu	$5,$17,10844
move	$17,$2
lw	$6,10384($fp)
sll	$4,$17,5
sll	$2,$2,3
addu	$5,$fp,$5
addu	$2,$2,$4
sw	$6,16($sp)
li	$7,1			# 0x1
sll	$8,$2,4
li	$6,16			# 0x10
subu	$4,$8,$2
move	$25,$18
.reloc	1f,R_MIPS_JALR,build_def_list
1:	jalr	$25
addu	$4,$19,$4

lw	$5,6624($16)
addu	$2,$2,$17
sltu	$4,$2,$5
bne	$4,$0,$L199
lw	$28,24($sp)

$L191:
lw	$31,820($sp)
$L202:
$L204:
move	$2,$0
lw	$fp,816($sp)
lw	$23,812($sp)
lw	$22,808($sp)
lw	$21,804($sp)
lw	$20,800($sp)
lw	$19,796($sp)
lw	$18,792($sp)
lw	$17,788($sp)
lw	$16,784($sp)
j	$31
addiu	$sp,$sp,824

$L198:
lw	$2,10384($fp)
beq	$2,$4,$L180
xori	$2,$2,0x2

lw	$4,2696($fp)
sltu	$2,$2,1
addiu	$2,$2,66
sll	$2,$2,2
addu	$2,$4,$2
lw	$22,4($2)
$L181:
li	$17,131072			# 0x20000
li	$9,65536			# 0x10000
addiu	$3,$17,10976
addiu	$21,$17,10716
addiu	$17,$17,10844
addiu	$9,$9,6624
addu	$17,$fp,$17
addiu	$25,$sp,760
li	$18,196608			# 0x30000
sw	$17,768($sp)
addu	$21,$fp,$21
addu	$16,$fp,$3
sw	$25,772($sp)
addu	$19,$fp,$9
move	$23,$0
addiu	$17,$sp,632
addu	$18,$fp,$18
$L183:
lw	$25,%got(add_sorted)($28)
xori	$2,$23,0x1
lw	$6,-15284($18)
move	$4,$17
move	$5,$21
sw	$2,16($sp)
addiu	$25,$25,%lo(add_sorted)
.reloc	1f,R_MIPS_JALR,add_sorted
1:	jalr	$25
move	$7,$22

move	$5,$21
lw	$28,24($sp)
sll	$4,$2,2
lw	$6,-15284($18)
move	$7,$22
addu	$4,$17,$4
sw	$23,16($sp)
move	$20,$2
lw	$25,%got(add_sorted)($28)
addiu	$25,$25,%lo(add_sorted)
.reloc	1f,R_MIPS_JALR,add_sorted
1:	jalr	$25
addiu	$19,$19,4

move	$7,$0
lw	$28,24($sp)
addu	$6,$2,$20
lw	$11,10384($fp)
move	$4,$16
move	$5,$17
lw	$25,%got(build_def_list)($28)
addiu	$25,$25,%lo(build_def_list)
.reloc	1f,R_MIPS_JALR,build_def_list
1:	jalr	$25
sw	$11,16($sp)

li	$6,16			# 0x10
move	$20,$2
lw	$28,24($sp)
sll	$4,$20,5
lw	$2,10384($fp)
sll	$10,$20,3
lw	$5,768($sp)
li	$7,1			# 0x1
addu	$10,$10,$4
lw	$25,%got(build_def_list)($28)
sw	$2,16($sp)
sll	$11,$10,4
addiu	$25,$25,%lo(build_def_list)
subu	$4,$11,$10
addu	$4,$16,$4
.reloc	1f,R_MIPS_JALR,build_def_list
1:	jalr	$25
addiu	$16,$16,19200

lw	$6,-4($19)
addu	$7,$2,$20
sltu	$2,$7,$6
bne	$2,$0,$L200
lw	$28,24($sp)

$L182:
sll	$2,$23,2
addiu	$25,$sp,760
addu	$2,$25,$2
beq	$23,$0,$L192
sw	$7,0($2)

lw	$7,760($sp)
lw	$2,764($sp)
bne	$7,$2,$L202
lw	$31,820($sp)

slt	$2,$7,2
bne	$2,$0,$L202
li	$2,131072			# 0x20000

addu	$4,$fp,$2
lw	$5,10976($4)
lw	$4,30176($4)
bne	$5,$4,$L204
addiu	$2,$2,11576

move	$4,$0
b	$L186
addu	$2,$fp,$2

$L201:
beq	$4,$7,$L203
li	$5,131072			# 0x20000

$L186:
lw	$6,0($2)
addiu	$4,$4,1
lw	$5,19200($2)
beq	$6,$5,$L201
addiu	$2,$2,600

bne	$4,$7,$L202
lw	$31,820($sp)

li	$5,131072			# 0x20000
$L203:
addiu	$6,$sp,32
addu	$5,$fp,$5
move	$4,$6
addiu	$2,$5,30776
addiu	$5,$5,31368
$L188:
lw	$10,4($2)
addiu	$2,$2,16
addiu	$4,$4,16
lw	$9,-8($2)
lw	$8,-4($2)
lw	$7,-16($2)
sw	$10,-12($4)
sw	$9,-8($4)
sw	$8,-4($4)
bne	$2,$5,$L188
sw	$7,-16($4)

lw	$7,4($2)
li	$5,131072			# 0x20000
lw	$8,0($2)
addu	$5,$fp,$5
sw	$7,4($4)
addiu	$2,$5,30176
addiu	$7,$5,30776
sw	$8,0($4)
addiu	$5,$5,30768
$L189:
lw	$10,0($2)
addiu	$2,$2,16
addiu	$7,$7,16
lw	$9,-12($2)
lw	$8,-8($2)
lw	$4,-4($2)
sw	$10,-16($7)
sw	$9,-12($7)
sw	$8,-8($7)
bne	$2,$5,$L189
sw	$4,-4($7)

lw	$5,0($2)
addiu	$8,$sp,624
lw	$4,4($2)
li	$2,131072			# 0x20000
addu	$2,$fp,$2
sw	$5,0($7)
addiu	$2,$2,30176
sw	$4,4($7)
$L190:
lw	$7,0($6)
addiu	$6,$6,16
addiu	$2,$2,16
lw	$5,-12($6)
lw	$4,-8($6)
lw	$3,-4($6)
sw	$7,-16($2)
sw	$5,-12($2)
sw	$4,-8($2)
bne	$6,$8,$L190
sw	$3,-4($2)

lw	$4,0($6)
lw	$3,4($6)
sw	$4,0($2)
b	$L191
sw	$3,4($2)

$L192:
b	$L183
li	$23,1			# 0x1

$L200:
sll	$4,$7,3
lw	$25,%call16(memset)($28)
sll	$2,$23,8
sw	$7,776($sp)
sll	$10,$23,10
sll	$5,$7,5
addu	$10,$2,$10
move	$2,$4
addu	$4,$2,$5
subu	$2,$6,$7
sll	$5,$4,4
sll	$6,$10,4
subu	$5,$5,$4
subu	$4,$6,$10
sll	$6,$2,3
sll	$2,$2,5
li	$3,131072			# 0x20000
addu	$2,$6,$2
addu	$4,$5,$4
ori	$3,$3,0x2ae0
addu	$4,$4,$3
sll	$6,$2,4
addu	$4,$fp,$4
move	$5,$0
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
subu	$6,$6,$2

lw	$28,24($sp)
b	$L182
lw	$7,776($sp)

$L199:
sll	$4,$2,3
lw	$25,%call16(memset)($28)
sll	$6,$2,5
subu	$2,$5,$2
addu	$5,$4,$6
sll	$6,$2,3
sll	$4,$5,4
sll	$2,$2,5
subu	$4,$4,$5
addu	$2,$6,$2
addu	$4,$4,$20
sll	$6,$2,4
addu	$4,$fp,$4
move	$5,$0
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
subu	$6,$6,$2

move	$2,$0
lw	$31,820($sp)
lw	$fp,816($sp)
lw	$23,812($sp)
lw	$22,808($sp)
lw	$21,804($sp)
lw	$20,800($sp)
lw	$19,796($sp)
lw	$18,792($sp)
lw	$17,788($sp)
lw	$16,784($sp)
j	$31
addiu	$sp,$sp,824

$L180:
lw	$2,2696($fp)
b	$L181
lw	$22,276($2)

.set	macro
.set	reorder
.end	ff_h264_fill_default_ref_list
.size	ff_h264_fill_default_ref_list, .-ff_h264_fill_default_ref_list
.section	.rodata.str1.4
.align	2
$LC5:
.ascii	"reference count overflow\012\000"
.align	2
$LC6:
.ascii	"abs_diff_pic_num overflow\012\000"
.align	2
$LC7:
.ascii	"long_term_pic_idx overflow\012\000"
.align	2
$LC8:
.ascii	"illegal reordering_of_pic_nums_idc\012\000"
.align	2
$LC9:
.ascii	"Missing reference picture\012\000"
.align	2
$LC10:
.ascii	"reference picture missing during reorder\012\000"
.section	.text.ff_h264_decode_ref_pic_list_reordering,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_ref_pic_list_reordering
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_ref_pic_list_reordering
.type	ff_h264_decode_ref_pic_list_reordering, @function
ff_h264_decode_ref_pic_list_reordering:
.frame	$sp,96,$31		# vars= 32, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$25,%got(print_short_term)($28)
addiu	$sp,$sp,-96
.cprestore	16
addiu	$25,$25,%lo(print_short_term)
sw	$31,92($sp)
sw	$23,84($sp)
move	$23,$4
sw	$fp,88($sp)
sw	$22,80($sp)
sw	$21,76($sp)
sw	$20,72($sp)
sw	$19,68($sp)
sw	$18,64($sp)
sw	$17,60($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,print_short_term
1:	jalr	$25
sw	$16,56($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%got(print_long_term)($28)
addiu	$25,$25,%lo(print_long_term)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,print_long_term
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addu	$3,$23,$2
lw	$3,6632($3)
.set	noreorder
.set	nomacro
beq	$3,$0,$L206
lw	$28,16($sp)
.set	macro
.set	reorder

li	$19,131072			# 0x20000
lw	$20,%got(ff_golomb_vlc_len)($28)
addiu	$14,$2,6624
sw	$0,24($sp)
addiu	$3,$19,10976
addiu	$2,$2,6640
addu	$3,$23,$3
addu	$2,$23,$2
addu	$19,$23,$19
addu	$21,$23,$14
sw	$3,44($sp)
move	$17,$0
sw	$2,40($sp)
sw	$19,36($sp)
move	$19,$23
sw	$3,32($sp)
sw	$2,28($sp)
sw	$21,48($sp)
$L242:
lw	$3,0($21)
lw	$25,%call16(memcpy)($28)
lw	$4,28($sp)
sll	$2,$3,3
lw	$5,32($sp)
sll	$3,$3,5
addu	$2,$2,$3
sll	$6,$2,4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
subu	$6,$6,$2
.set	macro
.set	reorder

lw	$6,10340($19)
lw	$7,10332($19)
lw	$28,16($sp)
srl	$3,$6,3
andi	$4,$6,0x7
addu	$3,$7,$3
addiu	$6,$6,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L207
sw	$6,10340($19)
.set	macro
.set	reorder

lw	$3,24($sp)
li	$12,16711680			# 0xff0000
lw	$14,%got(ff_ue_golomb_vlc_code)($28)
move	$22,$0
lw	$fp,28($sp)
addiu	$18,$12,255
sll	$16,$3,5
lw	$3,36($sp)
lw	$23,10704($3)
lw	$3,24($sp)
sll	$2,$3,9
subu	$2,$2,$16
sll	$16,$2,4
subu	$16,$16,$2
$L241:
srl	$2,$6,3
lw	$5,%got(ff_ue_golomb_vlc_code)($28)
li	$4,-16777216			# 0xffffffffff000000
addu	$2,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$8,$3,8
srl	$2,$3,8
ori	$4,$4,0xff00
and	$3,$2,$18
and	$2,$8,$4
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$6,0x7
or	$2,$3,$2
sll	$2,$2,$8
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$14,$2
lbu	$8,0($3)
lbu	$3,0($2)
addu	$2,$8,$6
li	$6,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$6,$L207
sw	$2,10340($19)
.set	macro
.set	reorder

lw	$6,0($21)
sltu	$6,$22,$6
.set	noreorder
.set	nomacro
beq	$6,$0,$L279
lw	$6,%got($LC5)($28)
.set	macro
.set	reorder

sltu	$6,$3,3
.set	noreorder
.set	nomacro
beq	$6,$0,$L210
lw	$6,%got($LC8)($28)
.set	macro
.set	reorder

li	$6,2			# 0x2
.set	noreorder
.set	nomacro
bne	$3,$6,$L280
srl	$6,$2,3
.set	macro
.set	reorder

lw	$7,10332($19)
andi	$3,$2,0x7
addu	$6,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($6)  
lwr $7, 0($6)  

# 0 "" 2
#NO_APP
srl	$6,$7,8
sll	$7,$7,8
and	$6,$6,$18
and	$4,$7,$4
or	$4,$6,$4
sll	$6,$4,16
srl	$4,$4,16
or	$4,$6,$4
sll	$3,$4,$3
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L281
ori	$4,$3,0x1
.set	macro
.set	reorder

lw	$12,10384($19)
clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
subu	$2,$2,$4
srl	$3,$3,$4
sw	$2,10340($19)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$12,$2,$L250
addiu	$3,$3,-1
.set	macro
.set	reorder

$L288:
andi	$2,$3,0x1
xori	$4,$12,0x3
movz	$12,$4,$2
sra	$2,$3,1
slt	$4,$2,32
.set	noreorder
.set	nomacro
beq	$4,$0,$L290
lw	$6,%got($LC7)($28)
.set	macro
.set	reorder

li	$4,35478			# 0x8a96
$L294:
addu	$2,$2,$4
sll	$2,$2,2
addu	$2,$19,$2
lw	$11,4($2)
.set	noreorder
.set	nomacro
beq	$11,$0,$L291
lw	$6,%got($LC10)($28)
.set	macro
.set	reorder

lw	$2,80($11)
and	$2,$12,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L292
li	$5,16			# 0x10
.set	macro
.set	reorder

sw	$3,288($11)
$L228:
lw	$7,0($21)
move	$4,$22
addiu	$3,$4,1
sltu	$5,$3,$7
.set	noreorder
.set	nomacro
beq	$5,$0,$L235
addiu	$2,$fp,288
.set	macro
.set	reorder

$L234:
lw	$5,4($2)
lw	$6,292($11)
beq	$6,$5,$L283
addiu	$2,$2,600
move	$4,$3
$L285:
addiu	$3,$4,1
sltu	$5,$3,$7
bne	$5,$0,$L234
$L235:
slt	$2,$22,$4
.set	noreorder
.set	nomacro
beq	$2,$0,$L238
sll	$5,$4,3
.set	macro
.set	reorder

sll	$4,$4,5
li	$2,65536			# 0x10000
addu	$4,$5,$4
addiu	$2,$2,6040
sll	$5,$4,4
addu	$2,$17,$2
subu	$4,$5,$4
addiu	$13,$fp,-600
addu	$5,$2,$4
addu	$5,$19,$5
$L237:
addiu	$3,$5,600
addiu	$4,$5,592
move	$2,$5
$L236:
lw	$9,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$8,-12($2)
lw	$7,-8($2)
lw	$6,-4($2)
sw	$9,-16($3)
sw	$8,-12($3)
sw	$7,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L236
sw	$6,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
addiu	$5,$5,-600
lw	$2,4($2)
sw	$4,0($3)
.set	noreorder
.set	nomacro
bne	$5,$13,$L237
sw	$2,4($3)
.set	macro
.set	reorder

$L238:
addiu	$3,$11,592
move	$2,$fp
$L231:
lw	$7,0($11)
addiu	$11,$11,16
addiu	$2,$2,16
lw	$6,-12($11)
lw	$5,-8($11)
lw	$4,-4($11)
sw	$7,-16($2)
sw	$6,-12($2)
sw	$5,-8($2)
.set	noreorder
.set	nomacro
bne	$11,$3,$L231
sw	$4,-4($2)
.set	macro
.set	reorder

lw	$3,4($11)
lw	$4,0($11)
sw	$3,4($2)
sw	$4,0($2)
li	$2,3			# 0x3
lw	$3,10384($19)
.set	noreorder
.set	nomacro
beq	$3,$2,$L233
addiu	$2,$fp,16
.set	macro
.set	reorder

addiu	$6,$fp,32
li	$5,2			# 0x2
$L240:
beq	$12,$5,$L284
$L239:
addiu	$2,$2,4
sw	$12,80($fp)
lw	$3,-4($2)
sll	$3,$3,1
.set	noreorder
.set	nomacro
bne	$2,$6,$L240
sw	$3,-4($2)
.set	macro
.set	reorder

sll	$2,$22,1
sll	$5,$22,3
move	$3,$2
addu	$2,$3,$5
xori	$4,$12,0x2
sll	$3,$2,4
sltu	$4,$4,1
subu	$2,$3,$2
addu	$2,$2,$16
addu	$2,$2,$4
addiu	$2,$2,18110
sll	$2,$2,2
addu	$2,$19,$2
lw	$2,4($2)
sw	$2,276($fp)
$L233:
addiu	$22,$22,1
lw	$7,10332($19)
lw	$6,10340($19)
.set	noreorder
.set	nomacro
b	$L241
addiu	$fp,$fp,600
.set	macro
.set	reorder

$L283:
lw	$5,0($2)
lw	$6,288($11)
.set	noreorder
.set	nomacro
beq	$6,$5,$L235
addiu	$2,$2,600
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L285
move	$4,$3
.set	macro
.set	reorder

$L284:
lw	$3,-16($2)
lw	$4,0($2)
addu	$3,$3,$4
.set	noreorder
.set	nomacro
b	$L239
sw	$3,-16($2)
.set	macro
.set	reorder

$L280:
andi	$8,$2,0x7
addu	$7,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($7)  
lwr $6, 0($7)  

# 0 "" 2
#NO_APP
move	$7,$6
sll	$7,$7,8
srl	$6,$6,8
and	$7,$7,$4
and	$6,$6,$18
or	$7,$6,$7
sll	$6,$7,16
srl	$7,$7,16
or	$6,$6,$7
sll	$4,$6,$8
li	$6,134217728			# 0x8000000
sltu	$6,$4,$6
beq	$6,$0,$L286
ori	$5,$4,0x1
clz	$6,$5
li	$5,31			# 0x1f
addiu	$2,$2,32
subu	$5,$5,$6
sll	$5,$5,1
addiu	$5,$5,-31
srl	$4,$4,$5
subu	$5,$2,$5
lw	$2,36($sp)
addiu	$4,$4,-1
sw	$5,10340($19)
lw	$5,10708($2)
addiu	$2,$4,1
sltu	$4,$5,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L293
lw	$6,%got($LC6)($28)
.set	macro
.set	reorder

$L214:
bne	$3,$0,$L215
lw	$12,10384($19)
subu	$23,$23,$2
addiu	$5,$5,-1
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$12,$2,$L249
and	$23,$23,$5
.set	macro
.set	reorder

$L289:
andi	$2,$23,0x1
xori	$3,$12,0x3
movz	$12,$3,$2
sra	$5,$23,1
$L217:
li	$2,196608			# 0x30000
addu	$2,$19,$2
lw	$2,-15284($2)
addiu	$3,$2,-1
.set	noreorder
.set	nomacro
bltz	$3,$L291
lw	$6,%got($LC10)($28)
.set	macro
.set	reorder

li	$4,35446			# 0x8a76
li	$6,-1			# 0xffffffffffffffff
addu	$2,$2,$4
sll	$2,$2,2
addu	$2,$19,$2
$L222:
lw	$11,0($2)
addiu	$3,$3,-1
lw	$4,280($11)
.set	noreorder
.set	nomacro
bne	$4,$5,$L220
addiu	$2,$2,-4
.set	macro
.set	reorder

lw	$4,80($11)
and	$4,$12,$4
bne	$4,$0,$L221
$L220:
bne	$3,$6,$L222
lw	$6,%got($LC10)($28)
$L291:
li	$5,16			# 0x10
$L292:
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
addiu	$6,$6,%lo($LC10)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$14,52($sp)
.set	macro
.set	reorder

move	$5,$0
lw	$28,16($sp)
li	$6,600			# 0x258
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,16($sp)
.set	noreorder
.set	nomacro
b	$L233
lw	$14,52($sp)
.set	macro
.set	reorder

$L281:
srl	$3,$3,23
lw	$12,10384($19)
addu	$4,$20,$3
addu	$3,$5,$3
lbu	$4,0($4)
addu	$2,$4,$2
sw	$2,10340($19)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$12,$2,$L288
lbu	$3,0($3)
.set	macro
.set	reorder

$L250:
move	$2,$3
slt	$4,$2,32
.set	noreorder
.set	nomacro
bne	$4,$0,$L294
li	$4,35478			# 0x8a96
.set	macro
.set	reorder

lw	$6,%got($LC7)($28)
$L290:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L221:
.set	noreorder
.set	nomacro
b	$L228
sw	$23,288($11)
.set	macro
.set	reorder

$L215:
lw	$12,10384($19)
addu	$23,$2,$23
addiu	$5,$5,-1
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$12,$2,$L289
and	$23,$23,$5
.set	macro
.set	reorder

$L249:
.set	noreorder
.set	nomacro
b	$L217
move	$5,$23
.set	macro
.set	reorder

$L286:
srl	$4,$4,23
addu	$6,$20,$4
addu	$4,$5,$4
lbu	$5,0($6)
lbu	$4,0($4)
addu	$2,$5,$2
sw	$2,10340($19)
lw	$2,36($sp)
lw	$5,10708($2)
addiu	$2,$4,1
sltu	$4,$5,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L214
lw	$6,%got($LC6)($28)
.set	macro
.set	reorder

$L293:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L207:
lw	$3,24($sp)
li	$2,65536			# 0x10000
lw	$4,32($sp)
addiu	$21,$21,4
addu	$2,$19,$2
addiu	$3,$3,1
addiu	$4,$4,19200
sw	$3,24($sp)
lw	$3,28($sp)
lw	$5,24($sp)
sw	$4,32($sp)
addiu	$3,$3,28800
sw	$3,28($sp)
lw	$3,6632($2)
sltu	$4,$5,$3
.set	noreorder
.set	nomacro
bne	$4,$0,$L242
addiu	$17,$17,28800
.set	macro
.set	reorder

lw	$21,48($sp)
move	$23,$19
.set	noreorder
.set	nomacro
beq	$3,$0,$L206
move	$22,$0
.set	macro
.set	reorder

lw	$19,%got($LC9)($28)
move	$20,$2
lw	$fp,44($sp)
addiu	$19,$19,%lo($LC9)
$L275:
lw	$2,0($21)
.set	noreorder
.set	nomacro
beq	$2,$0,$L295
lw	$3,40($sp)
.set	macro
.set	reorder

move	$17,$0
lw	$16,40($sp)
.set	noreorder
.set	nomacro
b	$L246
addiu	$18,$fp,592
.set	macro
.set	reorder

$L244:
lw	$2,0($21)
addiu	$17,$17,1
sltu	$2,$17,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L247
addiu	$16,$16,600
.set	macro
.set	reorder

$L246:
lw	$2,0($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L244
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$4,0($23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$6,$19
.set	macro
.set	reorder

lw	$2,0($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L252
lw	$28,16($sp)
.set	macro
.set	reorder

move	$3,$fp
move	$2,$16
$L245:
lw	$8,0($3)
addiu	$3,$3,16
addiu	$2,$2,16
lw	$6,-12($3)
lw	$5,-8($3)
lw	$4,-4($3)
sw	$8,-16($2)
sw	$6,-12($2)
sw	$5,-8($2)
.set	noreorder
.set	nomacro
bne	$3,$18,$L245
sw	$4,-4($2)
.set	macro
.set	reorder

lw	$4,0($18)
addiu	$17,$17,1
lw	$3,4($18)
sw	$4,0($2)
sw	$3,4($2)
lw	$2,0($21)
sltu	$2,$17,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L246
addiu	$16,$16,600
.set	macro
.set	reorder

$L247:
lw	$3,40($sp)
$L295:
addiu	$22,$22,1
lw	$2,6632($20)
addiu	$21,$21,4
addiu	$fp,$fp,19200
addiu	$3,$3,28800
sltu	$2,$22,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L275
sw	$3,40($sp)
.set	macro
.set	reorder

$L206:
.set	noreorder
.set	nomacro
b	$L274
move	$2,$0
.set	macro
.set	reorder

$L210:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC8)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
$L274:
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L279:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC5)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L252:
.set	noreorder
.set	nomacro
b	$L274
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	ff_h264_decode_ref_pic_list_reordering
.size	ff_h264_decode_ref_pic_list_reordering, .-ff_h264_decode_ref_pic_list_reordering
.section	.text.ff_h264_fill_mbaff_ref_list,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_fill_mbaff_ref_list
.set	nomips16
.set	nomicromips
.ent	ff_h264_fill_mbaff_ref_list
.type	ff_h264_fill_mbaff_ref_list, @function
ff_h264_fill_mbaff_ref_list:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,65536			# 0x10000
addiu	$sp,$sp,-64
addiu	$3,$2,6632
addiu	$5,$2,6624
sw	$20,44($sp)
addu	$3,$4,$3
sw	$fp,60($sp)
addu	$20,$4,$5
sw	$23,56($sp)
sw	$22,52($sp)
sw	$21,48($sp)
sw	$19,40($sp)
sw	$18,36($sp)
sw	$17,32($sp)
sw	$16,28($sp)
sw	$4,12($sp)
sw	$3,20($sp)
sw	$4,8($sp)
sw	$4,16($sp)
$L297:
lw	$2,0($20)
beq	$2,$0,$L303
li	$7,65536			# 0x10000

lw	$2,8($sp)
li	$3,65536			# 0x10000
li	$4,65536			# 0x10000
lw	$5,8($sp)
li	$8,65536			# 0x10000
li	$9,65536			# 0x10000
ori	$3,$3,0x3f70
ori	$4,$4,0x3f80
ori	$7,$7,0x3f84
ori	$8,$8,0x3f88
ori	$9,$9,0x41c8
addu	$14,$2,$3
lw	$3,16($sp)
addu	$21,$2,$4
addu	$19,$2,$7
addu	$18,$2,$8
addu	$22,$2,$9
lw	$2,12($sp)
move	$16,$0
li	$fp,60446			# 0xec1e
li	$25,60438			# 0xec16
li	$24,60692			# 0xed14
li	$23,60964			# 0xee24
move	$4,$2
move	$6,$3
move	$15,$5
$L301:
li	$8,65536			# 0x10000
ori	$8,$8,0x19f0
addu	$7,$15,$8
move	$8,$14
addiu	$9,$7,592
$L298:
lw	$13,0($7)
addiu	$7,$7,16
addiu	$8,$8,16
lw	$12,-12($7)
lw	$11,-8($7)
lw	$10,-4($7)
sw	$13,-16($8)
sw	$12,-12($8)
sw	$11,-8($8)
bne	$7,$9,$L298
sw	$10,-4($8)

lw	$13,4($7)
li	$9,65536			# 0x10000
lw	$17,0($7)
li	$7,65536			# 0x10000
ori	$9,$9,0x3fc0
ori	$7,$7,0x407c
sw	$13,4($8)
addu	$10,$5,$9
sw	$17,0($8)
addu	$9,$5,$7
lw	$13,0($21)
li	$7,65536			# 0x10000
addiu	$12,$14,592
ori	$7,$7,0x4084
sll	$13,$13,1
addu	$11,$5,$7
move	$8,$22
sw	$13,0($21)
move	$7,$14
lw	$13,0($19)
sll	$13,$13,1
sw	$13,0($19)
lw	$13,0($18)
sll	$13,$13,1
sw	$13,0($18)
li	$13,1			# 0x1
sw	$13,0($10)
lw	$9,0($9)
sw	$9,0($11)
$L299:
lw	$13,0($7)
addiu	$7,$7,16
addiu	$8,$8,16
lw	$11,-12($7)
lw	$10,-8($7)
lw	$9,-4($7)
sw	$13,-16($8)
sw	$11,-12($8)
sw	$10,-8($8)
bne	$7,$12,$L299
sw	$9,-4($8)

lw	$12,0($7)
li	$13,65536			# 0x10000
lw	$7,4($7)
li	$9,65536			# 0x10000
ori	$13,$13,0x41d4
ori	$9,$9,0x1a00
addu	$11,$5,$13
sw	$12,0($8)
addu	$10,$15,$9
sw	$7,4($8)
move	$9,$22
$L300:
lw	$7,0($9)
addiu	$9,$9,4
lw	$8,0($10)
addiu	$10,$10,4
addu	$7,$7,$8
bne	$9,$11,$L300
sw	$7,-4($9)

li	$7,65536			# 0x10000
li	$10,65536			# 0x10000
li	$13,60308			# 0xeb94
ori	$7,$7,0x4218
ori	$10,$10,0x42d8
addu	$9,$5,$7
addu	$8,$5,$10
addu	$7,$6,$13
li	$10,2			# 0x2
li	$13,65536			# 0x10000
lh	$7,0($7)
addu	$17,$2,$23
ori	$13,$13,0x42dc
sw	$10,0($9)
addu	$10,$5,$13
lw	$12,0($8)
li	$13,60436			# 0xec14
li	$8,60444			# 0xec1c
addu	$11,$3,$13
li	$13,60310			# 0xeb96
sw	$12,0($10)
addu	$9,$3,$8
addu	$8,$6,$13
sh	$7,0($9)
addu	$13,$3,$fp
sh	$7,0($11)
li	$7,60948			# 0xee14
lh	$8,0($8)
addu	$10,$3,$25
addu	$12,$2,$7
addu	$9,$4,$24
li	$7,60694			# 0xed16
addiu	$16,$16,1
addu	$11,$4,$7
lw	$7,0($20)
sh	$8,0($13)
li	$13,60966			# 0xee26
sh	$8,0($10)
li	$8,60696			# 0xed18
lh	$10,0($9)
li	$9,60950			# 0xee16
addu	$13,$2,$13
addu	$9,$2,$9
addu	$8,$4,$8
sh	$10,0($17)
sltu	$7,$16,$7
sh	$10,0($12)
li	$12,60968			# 0xee28
lh	$17,0($11)
li	$10,60698			# 0xed1a
li	$11,60952			# 0xee18
addu	$12,$2,$12
addu	$11,$2,$11
sh	$17,0($13)
addu	$10,$4,$10
sh	$17,0($9)
li	$9,60970			# 0xee2a
lh	$13,0($8)
li	$8,60954			# 0xee1a
addu	$9,$2,$9
addu	$8,$2,$8
addiu	$15,$15,600
sh	$13,0($12)
addiu	$14,$14,1200
sh	$13,0($11)
addiu	$5,$5,1200
lh	$10,0($10)
addiu	$21,$21,1200
addiu	$19,$19,1200
addiu	$18,$18,1200
addiu	$22,$22,1200
sh	$10,0($9)
addiu	$6,$6,8
addiu	$3,$3,16
sh	$10,0($8)
addiu	$4,$4,16
bne	$7,$0,$L301
addiu	$2,$2,32

$L303:
lw	$8,16($sp)
addiu	$20,$20,4
lw	$9,8($sp)
lw	$10,12($sp)
addiu	$8,$8,4
lw	$13,20($sp)
addiu	$9,$9,28800
addiu	$10,$10,8
sw	$8,16($sp)
sw	$9,8($sp)
bne	$20,$13,$L297
sw	$10,12($sp)

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	ff_h264_fill_mbaff_ref_list
.size	ff_h264_fill_mbaff_ref_list, .-ff_h264_fill_mbaff_ref_list
.section	.text.ff_h264_remove_all_refs,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_remove_all_refs
.set	nomips16
.set	nomicromips
.ent	ff_h264_remove_all_refs
.type	ff_h264_remove_all_refs, @function
ff_h264_remove_all_refs:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$18,36($sp)
sw	$19,40($sp)
li	$19,16			# 0x10
sw	$16,28($sp)
move	$16,$0
sw	$17,32($sp)
move	$17,$4
.cprestore	16
sw	$31,44($sp)
lw	$18,%got(remove_long)($28)
addiu	$18,$18,%lo(remove_long)
move	$5,$16
$L333:
move	$6,$0
addiu	$16,$16,1
move	$25,$18
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
move	$4,$17

bne	$16,$19,$L333
move	$5,$16

li	$3,196608			# 0x30000
addu	$4,$17,$3
lw	$2,-15284($4)
blez	$2,$L321
nop

lw	$7,-16160($4)
li	$4,35447			# 0x8a77
li	$6,131072			# 0x20000
addu	$4,$2,$4
addiu	$3,$3,-16156
sll	$4,$4,2
addiu	$6,$6,10716
addu	$4,$17,$4
addu	$6,$17,$6
addu	$8,$17,$3
li	$9,4			# 0x4
$L320:
lw	$5,0($6)
beq	$7,$0,$L315
sw	$0,80($5)

bne	$5,$7,$L318
move	$2,$8

b	$L331
sw	$9,80($5)

$L319:
beq	$5,$3,$L316
nop

$L318:
lw	$3,0($2)
bne	$3,$0,$L319
addiu	$2,$2,4

$L315:
sw	$0,0($6)
addiu	$6,$6,4
bne	$6,$4,$L320
nop

$L321:
li	$2,196608			# 0x30000
$L332:
lw	$31,44($sp)
lw	$19,40($sp)
addu	$17,$17,$2
lw	$18,36($sp)
lw	$16,28($sp)
sw	$0,-15284($17)
lw	$17,32($sp)
j	$31
addiu	$sp,$sp,48

$L316:
sw	$9,80($5)
$L331:
addiu	$6,$6,4
bne	$6,$4,$L320
sw	$0,-4($6)

b	$L332
li	$2,196608			# 0x30000

.set	macro
.set	reorder
.end	ff_h264_remove_all_refs
.size	ff_h264_remove_all_refs, .-ff_h264_remove_all_refs
.section	.text.ff_generate_sliding_window_mmcos,"ax",@progbits
.align	2
.align	5
.globl	ff_generate_sliding_window_mmcos
.set	nomips16
.set	nomicromips
.ent	ff_generate_sliding_window_mmcos
.type	ff_generate_sliding_window_mmcos, @function
ff_generate_sliding_window_mmcos:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,196608			# 0x30000
addu	$2,$4,$2
sw	$0,-15292($2)
lw	$3,-15284($2)
beq	$3,$0,$L346
nop

lw	$5,-15288($2)
lw	$6,11932($4)
addu	$5,$3,$5
beq	$5,$6,$L344
li	$5,3			# 0x3

$L346:
j	$31
nop

$L344:
lw	$6,10384($4)
beq	$6,$5,$L336
nop

lw	$2,10456($4)
bne	$2,$0,$L345
li	$2,35445			# 0x8a75

lw	$2,2696($4)
lw	$2,80($2)
bne	$2,$0,$L346
li	$2,35445			# 0x8a75

$L345:
li	$5,1			# 0x1
addu	$3,$3,$2
li	$2,196608			# 0x30000
sll	$3,$3,2
addu	$3,$4,$3
addu	$4,$4,$2
lw	$2,4($3)
li	$3,2			# 0x2
sw	$5,-16084($4)
lw	$2,280($2)
sw	$3,-15292($4)
sw	$5,-16072($4)
sll	$2,$2,1
addiu	$3,$2,1
sw	$2,-16080($4)
j	$31
sw	$3,-16068($4)

$L336:
li	$6,35445			# 0x8a75
li	$5,1			# 0x1
addu	$3,$3,$6
sll	$3,$3,2
sw	$5,-16084($2)
addu	$4,$4,$3
lw	$3,4($4)
lw	$3,280($3)
sw	$5,-15292($2)
j	$31
sw	$3,-16080($2)

.set	macro
.set	reorder
.end	ff_generate_sliding_window_mmcos
.size	ff_generate_sliding_window_mmcos, .-ff_generate_sliding_window_mmcos
.section	.rodata.str1.4
.align	2
$LC11:
.ascii	"no mmco here\012\000"
.align	2
$LC12:
.ascii	"mmco:%d %d %d\012\000"
.align	2
$LC13:
.ascii	"mmco: unref short failure\012\000"
.align	2
$LC14:
.ascii	"mmco: unref short %d count %d\012\000"
.align	2
$LC15:
.ascii	"mmco: unref long failure\012\000"
.align	2
$LC16:
.ascii	"illegal short term reference assignment for second field"
.ascii	" in complementary field pair (first field is long term)\012"
.ascii	"\000"
.align	2
$LC17:
.ascii	"illegal short term buffer state detected\012\000"
.align	2
$LC18:
.ascii	"number of reference frames exceeds max (probably corrupt"
.ascii	" input), discarding one\012\000"
.section	.text.ff_h264_execute_ref_pic_marking,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_execute_ref_pic_marking
.set	nomips16
.set	nomicromips
.ent	ff_h264_execute_ref_pic_marking
.type	ff_h264_execute_ref_pic_marking, @function
ff_h264_execute_ref_pic_marking:
.frame	$sp,96,$31		# vars= 24, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-96
sw	$16,56($sp)
move	$16,$4
lw	$4,0($4)
.cprestore	24
sw	$31,92($sp)
sw	$fp,88($sp)
sw	$23,84($sp)
sw	$22,80($sp)
sw	$21,76($sp)
sw	$20,72($sp)
sw	$19,68($sp)
sw	$18,64($sp)
sw	$17,60($sp)
sw	$6,104($sp)
lw	$2,404($4)
andi	$2,$2,0x800
beq	$2,$0,$L432
lw	$3,104($sp)

beq	$6,$0,$L424
lw	$6,%got($LC11)($28)

$L432:
blez	$3,$L349
li	$19,131072			# 0x20000

sw	$0,44($sp)
addiu	$17,$5,8
ori	$19,$19,0xc130
addu	$19,$16,$19
bne	$2,$0,$L425
move	$20,$0

$L353:
lw	$2,-8($17)
li	$3,-3			# 0xfffffffffffffffd
li	$4,1			# 0x1
and	$3,$2,$3
beq	$3,$4,$L426
nop

$L354:
sltu	$3,$2,7
beq	$3,$0,$L359
lw	$4,%got($L361)($28)

sll	$2,$2,2
addiu	$4,$4,%lo($L361)
addu	$2,$4,$2
lw	$2,0($2)
addu	$2,$2,$28
j	$2
nop

.rdata
.align	2
.align	2
$L361:
.gpword	$L359
.gpword	$L360
.gpword	$L362
.gpword	$L363
.gpword	$L364
.gpword	$L365
.gpword	$L366
.section	.text.ff_h264_execute_ref_pic_marking
$L366:
lw	$5,0($17)
li	$7,35478			# 0x8a96
lw	$2,2696($16)
addu	$3,$5,$7
sll	$3,$3,2
addu	$3,$16,$3
lw	$3,4($3)
beq	$3,$2,$L374
move	$6,$0

lw	$2,%got(remove_long)($28)
move	$4,$16
addiu	$18,$2,%lo(remove_long)
move	$25,$18
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
sw	$7,48($sp)

li	$4,196608			# 0x30000
lw	$3,0($17)
lw	$7,48($sp)
addu	$4,$16,$4
lw	$2,2696($16)
lw	$28,24($sp)
addu	$3,$3,$7
lw	$5,-15288($4)
sll	$3,$3,2
addiu	$5,$5,1
addu	$3,$16,$3
sw	$2,4($3)
li	$3,1			# 0x1
sw	$3,292($2)
sw	$5,-15288($4)
$L374:
lw	$3,80($2)
li	$25,1			# 0x1
lw	$4,10384($16)
sw	$25,44($sp)
or	$3,$3,$4
sw	$3,80($2)
$L359:
lw	$3,104($sp)
$L438:
addiu	$20,$20,1
addiu	$17,$17,12
beq	$20,$3,$L377
addiu	$19,$19,12

$L427:
lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x800
beq	$2,$0,$L353
nop

$L425:
lw	$2,0($19)
li	$5,48			# 0x30
lw	$7,-4($19)
lw	$6,%got($LC12)($28)
lw	$25,%call16(av_log)($28)
sw	$2,16($sp)
lw	$2,4($19)
addiu	$6,$6,%lo($LC12)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,20($sp)

li	$3,-3			# 0xfffffffffffffffd
lw	$2,-8($17)
li	$4,1			# 0x1
and	$3,$2,$3
bne	$3,$4,$L354
lw	$28,24($sp)

$L426:
lw	$21,10384($16)
li	$2,3			# 0x3
beq	$21,$2,$L355
lw	$22,-4($17)

andi	$3,$22,0x1
xori	$2,$21,0x3
movz	$21,$2,$3
sra	$22,$22,1
$L355:
lw	$25,%got(find_short)($28)
addiu	$6,$sp,32
move	$4,$16
addiu	$25,$25,%lo(find_short)
.reloc	1f,R_MIPS_JALR,find_short
1:	jalr	$25
move	$5,$22

lw	$28,24($sp)
beq	$2,$0,$L357
move	$23,$2

b	$L354
lw	$2,-8($17)

$L364:
lw	$5,0($17)
slt	$2,$5,16
beq	$2,$0,$L359
sw	$5,32($sp)

lw	$2,%got(remove_long)($28)
addiu	$18,$2,%lo(remove_long)
$L375:
move	$6,$0
move	$25,$18
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
move	$4,$16

lw	$5,32($sp)
lw	$28,24($sp)
addiu	$5,$5,1
slt	$2,$5,16
bne	$2,$0,$L375
sw	$5,32($sp)

lw	$3,104($sp)
addiu	$20,$20,1
addiu	$17,$17,12
bne	$20,$3,$L427
addiu	$19,$19,12

$L377:
lw	$25,44($sp)
beq	$25,$0,$L433
li	$2,196608			# 0x30000

$L379:
li	$17,196608			# 0x30000
lw	$4,11932($16)
addu	$17,$16,$17
lw	$3,-15288($17)
lw	$2,-15284($17)
addu	$2,$3,$2
slt	$2,$4,$2
bne	$2,$0,$L434
lw	$6,%got($LC18)($28)

$L384:
lw	$25,%got(print_short_term)($28)
addiu	$25,$25,%lo(print_short_term)
.reloc	1f,R_MIPS_JALR,print_short_term
1:	jalr	$25
move	$4,$16

lw	$28,24($sp)
lw	$25,%got(print_long_term)($28)
addiu	$25,$25,%lo(print_long_term)
.reloc	1f,R_MIPS_JALR,print_long_term
1:	jalr	$25
move	$4,$16

move	$2,$0
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
j	$31
addiu	$sp,$sp,96

$L365:
li	$3,196608			# 0x30000
addu	$3,$16,$3
lw	$2,-15284($3)
beq	$2,$0,$L368
lw	$2,%got(remove_short)($28)

li	$7,131072			# 0x20000
move	$18,$3
addu	$fp,$16,$7
addiu	$2,$2,%lo(remove_short)
sw	$2,40($sp)
$L367:
lw	$2,10716($fp)
move	$6,$0
lw	$25,40($sp)
move	$4,$16
jalr	$25
lw	$5,280($2)

lw	$2,-15284($18)
bne	$2,$0,$L367
lw	$28,24($sp)

$L368:
lw	$2,%got(remove_long)($28)
move	$5,$0
sw	$0,32($sp)
addiu	$18,$2,%lo(remove_long)
$L376:
move	$6,$0
move	$25,$18
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
move	$4,$16

lw	$5,32($sp)
lw	$28,24($sp)
addiu	$5,$5,1
slt	$2,$5,16
bne	$2,$0,$L376
sw	$5,32($sp)

li	$3,131072			# 0x20000
lw	$2,2696($16)
addu	$3,$16,$3
sw	$0,280($2)
sw	$0,10680($3)
sw	$0,10664($3)
sw	$0,10660($3)
li	$3,1			# 0x1
sw	$0,272($2)
sw	$0,268($2)
sw	$0,276($2)
b	$L359
sw	$3,284($2)

$L362:
lw	$21,10384($16)
li	$2,3			# 0x3
beq	$21,$2,$L371
lw	$5,0($17)

andi	$3,$5,0x1
xori	$2,$21,0x3
movz	$21,$2,$3
sra	$5,$5,1
$L371:
li	$2,35478			# 0x8a96
sw	$5,32($sp)
addu	$2,$5,$2
sll	$2,$2,2
addu	$2,$16,$2
lw	$23,4($2)
beq	$23,$0,$L373
lw	$3,%got(remove_long)($28)

move	$4,$16
addiu	$18,$3,%lo(remove_long)
move	$25,$18
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
xori	$6,$21,0x3

b	$L359
lw	$28,24($sp)

$L363:
lw	$5,0($17)
li	$2,35478			# 0x8a96
addu	$2,$5,$2
sll	$2,$2,2
addu	$2,$16,$2
lw	$2,4($2)
beq	$2,$23,$L370
lw	$2,%got(remove_long)($28)

move	$6,$0
addiu	$18,$2,%lo(remove_long)
move	$25,$18
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
move	$4,$16

lw	$28,24($sp)
$L370:
lw	$2,%got(remove_short_at_index)($28)
move	$4,$16
addiu	$25,$2,%lo(remove_short_at_index)
.reloc	1f,R_MIPS_JALR,remove_short_at_index
1:	jalr	$25
lw	$5,32($sp)

li	$3,35478			# 0x8a96
lw	$2,0($17)
lw	$28,24($sp)
addu	$2,$2,$3
sll	$2,$2,2
addu	$2,$16,$2
beq	$23,$0,$L359
sw	$23,4($2)

li	$2,196608			# 0x30000
li	$3,1			# 0x1
addu	$2,$16,$2
sw	$3,292($23)
lw	$3,-15288($2)
addiu	$3,$3,1
b	$L359
sw	$3,-15288($2)

$L360:
lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x800
bne	$2,$0,$L429
li	$2,196608			# 0x30000

$L369:
lw	$2,%got(remove_short)($28)
move	$4,$16
move	$5,$22
addiu	$2,$2,%lo(remove_short)
move	$25,$2
.reloc	1f,R_MIPS_JALR,remove_short
1:	jalr	$25
xori	$6,$21,0x3

b	$L359
lw	$28,24($sp)

$L357:
lw	$3,-8($17)
li	$2,3			# 0x3
beq	$3,$2,$L430
li	$3,35478			# 0x8a96

lw	$6,%got($LC13)($28)
$L437:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)

b	$L359
lw	$28,24($sp)

$L424:
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)

lw	$28,24($sp)
$L349:
li	$2,196608			# 0x30000
$L433:
addu	$2,$16,$2
lw	$2,-15284($2)
bne	$2,$0,$L351
li	$3,131072			# 0x20000

lw	$2,2696($16)
$L380:
lw	$3,292($2)
beq	$3,$0,$L381
lw	$25,%got(remove_short)($28)

lw	$6,%got($LC16)($28)
li	$17,196608			# 0x30000
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
lw	$4,0($16)
addu	$17,$16,$17
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC16)

lw	$3,-15288($17)
lw	$2,-15284($17)
lw	$4,11932($16)
addu	$2,$3,$2
slt	$2,$4,$2
beq	$2,$0,$L384
lw	$28,24($sp)

lw	$6,%got($LC18)($28)
$L434:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC18)

lw	$2,-15288($17)
beq	$2,$0,$L385
lw	$28,24($sp)

lw	$2,-15284($17)
bne	$2,$0,$L435
li	$3,35445			# 0x8a75

li	$2,131072			# 0x20000
move	$5,$0
addiu	$2,$2,10844
li	$4,16			# 0x10
addu	$2,$16,$2
$L388:
lw	$3,0($2)
bne	$3,$0,$L436
lw	$25,%got(remove_long)($28)

addiu	$5,$5,1
bne	$5,$4,$L388
addiu	$2,$2,4

lw	$25,%got(remove_long)($28)
$L436:
move	$6,$0
addiu	$25,$25,%lo(remove_long)
.reloc	1f,R_MIPS_JALR,remove_long
1:	jalr	$25
move	$4,$16

b	$L384
lw	$28,24($sp)

$L430:
lw	$2,0($17)
addu	$2,$2,$3
sll	$2,$2,2
addu	$2,$16,$2
lw	$2,4($2)
beq	$2,$0,$L437
lw	$6,%got($LC13)($28)

lw	$2,280($2)
beq	$2,$22,$L359
li	$5,16			# 0x10

lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)

b	$L359
lw	$28,24($sp)

$L351:
lw	$2,2696($16)
addu	$3,$16,$3
lw	$3,10716($3)
bne	$3,$2,$L380
nop

li	$2,3			# 0x3
b	$L379
sw	$2,80($3)

$L373:
lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x800
beq	$2,$0,$L438
lw	$3,104($sp)

lw	$6,%got($LC15)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC15)

b	$L359
lw	$28,24($sp)

$L385:
lw	$2,-15284($17)
li	$3,35445			# 0x8a75
$L435:
lw	$25,%got(remove_short)($28)
move	$6,$0
addu	$2,$2,$3
addiu	$25,$25,%lo(remove_short)
sll	$2,$2,2
move	$4,$16
addu	$2,$16,$2
lw	$2,4($2)
.reloc	1f,R_MIPS_JALR,remove_short
1:	jalr	$25
lw	$5,280($2)

b	$L384
lw	$28,24($sp)

$L429:
lw	$6,%got($LC14)($28)
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
addu	$2,$16,$2
lw	$7,0($19)
addiu	$6,$6,%lo($LC14)
lw	$2,-15284($2)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L369
lw	$28,24($sp)

$L381:
move	$6,$0
lw	$5,280($2)
addiu	$25,$25,%lo(remove_short)
.reloc	1f,R_MIPS_JALR,remove_short
1:	jalr	$25
move	$4,$16

beq	$2,$0,$L382
lw	$28,24($sp)

lw	$6,%got($LC17)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC17)

lw	$28,24($sp)
$L382:
li	$17,196608			# 0x30000
addu	$17,$16,$17
lw	$6,-15284($17)
bne	$6,$0,$L431
li	$2,131072			# 0x20000

$L383:
lw	$2,2696($16)
li	$5,131072			# 0x20000
lw	$7,10384($16)
li	$4,196608			# 0x30000
addiu	$6,$6,1
addu	$5,$16,$5
lw	$3,80($2)
addu	$4,$16,$4
sw	$2,10716($5)
sw	$6,-15284($4)
or	$3,$3,$7
b	$L379
sw	$3,80($2)

$L431:
lw	$25,%call16(memmove)($28)
sll	$6,$6,2
addiu	$4,$2,10720
addiu	$5,$2,10716
addu	$4,$16,$4
.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
addu	$5,$16,$5

lw	$28,24($sp)
b	$L383
lw	$6,-15284($17)

.set	macro
.set	reorder
.end	ff_h264_execute_ref_pic_marking
.size	ff_h264_execute_ref_pic_marking, .-ff_h264_execute_ref_pic_marking
.section	.rodata.str1.4
.align	2
$LC19:
.ascii	"illegal long ref in memory management control operation "
.ascii	"%d\012\000"
.align	2
$LC20:
.ascii	"illegal memory management control operation %d\012\000"
.section	.text.ff_h264_decode_ref_pic_marking,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_ref_pic_marking
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_ref_pic_marking
.type	ff_h264_decode_ref_pic_marking, @function
ff_h264_decode_ref_pic_marking:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
li	$24,131072			# 0x20000
li	$6,196608			# 0x30000
addu	$24,$4,$24
addu	$3,$4,$6
li	$2,5			# 0x5
lw	$7,9472($24)
move	$25,$4
.set	noreorder
.set	nomacro
beq	$7,$2,$L459
sw	$0,-15292($3)
.set	macro
.set	reorder

lw	$3,8($5)
addiu	$sp,$sp,-64
lw	$12,0($5)
.cprestore	16
srl	$7,$3,3
sw	$31,60($sp)
sw	$fp,56($sp)
andi	$8,$3,0x7
addu	$7,$12,$7
sw	$23,52($sp)
sw	$22,48($sp)
addiu	$3,$3,1
sw	$21,44($sp)
sw	$20,40($sp)
sw	$19,36($sp)
sw	$18,32($sp)
sw	$17,28($sp)
sw	$16,24($sp)
lbu	$2,0($7)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L442
sw	$3,8($5)
.set	macro
.set	reorder

lw	$14,%got(ff_golomb_vlc_len)($28)
addiu	$6,$6,-16084
li	$11,16711680			# 0xff0000
lw	$13,%got(ff_ue_golomb_vlc_code)($28)
li	$10,-16777216			# 0xffffffffff000000
addu	$6,$4,$6
move	$9,$0
addiu	$11,$11,255
li	$16,-3			# 0xfffffffffffffffd
li	$4,1			# 0x1
li	$20,134217728			# 0x8000000
li	$23,31			# 0x1f
li	$21,2			# 0x2
li	$22,3			# 0x3
li	$18,6			# 0x6
li	$17,66			# 0x42
ori	$10,$10,0xff00
.set	noreorder
.set	nomacro
b	$L452
move	$fp,$14
.set	macro
.set	reorder

$L447:
beq	$2,$0,$L460
$L450:
.set	noreorder
.set	nomacro
beq	$7,$0,$L463
li	$3,196608			# 0x30000
.set	macro
.set	reorder

addiu	$9,$9,1
.set	noreorder
.set	nomacro
beq	$9,$17,$L463
addiu	$6,$6,12
.set	macro
.set	reorder

lw	$3,8($5)
$L452:
srl	$2,$3,3
andi	$15,$3,0x7
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
srl	$7,$8,8
sll	$8,$8,8
and	$7,$7,$11
and	$8,$8,$10
or	$8,$7,$8
sll	$7,$8,16
srl	$2,$8,16
or	$2,$7,$2
sll	$2,$2,$15
srl	$2,$2,23
addu	$7,$14,$2
addu	$2,$13,$2
lbu	$8,0($7)
lbu	$7,0($2)
addu	$3,$8,$3
and	$2,$7,$16
sw	$3,8($5)
.set	noreorder
.set	nomacro
beq	$2,$4,$L461
sw	$7,0($6)
.set	macro
.set	reorder

$L443:
addiu	$2,$7,-2
sltu	$2,$2,3
bne	$2,$0,$L446
.set	noreorder
.set	nomacro
bne	$7,$18,$L447
sltu	$2,$7,7
.set	macro
.set	reorder

$L446:
lw	$3,8($5)
srl	$2,$3,3
andi	$15,$3,0x7
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$8
sll	$2,$2,8
srl	$8,$8,8
and	$2,$2,$10
and	$8,$8,$11
or	$2,$8,$2
sll	$8,$2,16
srl	$2,$2,16
or	$2,$8,$2
sll	$2,$2,$15
srl	$2,$2,23
addu	$8,$14,$2
addu	$2,$13,$2
lbu	$8,0($8)
lbu	$2,0($2)
addu	$3,$8,$3
sltu	$8,$2,32
.set	noreorder
.set	nomacro
beq	$8,$0,$L448
sw	$3,8($5)
.set	macro
.set	reorder

sltu	$3,$2,16
bne	$3,$0,$L449
bne	$7,$21,$L448
lw	$3,10384($25)
beq	$3,$22,$L448
$L449:
sw	$2,8($6)
sltu	$2,$7,7
bne	$2,$0,$L450
$L460:
lw	$6,%got($LC20)($28)
li	$5,16			# 0x10
lw	$2,%call16(av_log)($28)
lw	$4,0($25)
move	$25,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC20)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L461:
srl	$2,$3,3
lw	$8,10704($24)
andi	$15,$3,0x7
addu	$2,$12,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($2)  
lwr $19, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$19
sll	$2,$2,8
srl	$19,$19,8
and	$2,$2,$10
and	$19,$19,$11
or	$2,$19,$2
sll	$19,$2,16
srl	$2,$2,16
or	$2,$19,$2
sll	$15,$2,$15
sltu	$2,$15,$20
.set	noreorder
.set	nomacro
beq	$2,$0,$L462
ori	$2,$15,0x1
.set	macro
.set	reorder

clz	$2,$2
addiu	$3,$3,32
subu	$2,$23,$2
sll	$2,$2,1
addiu	$2,$2,-31
srl	$15,$15,$2
subu	$3,$3,$2
addiu	$15,$15,-1
sw	$3,8($5)
$L445:
lw	$3,10708($24)
subu	$8,$8,$15
addiu	$15,$8,-1
addiu	$8,$3,-1
and	$8,$15,$8
.set	noreorder
.set	nomacro
b	$L443
sw	$8,4($6)
.set	macro
.set	reorder

$L462:
srl	$15,$15,23
lw	$2,%got(ff_ue_golomb_vlc_code)($28)
addu	$19,$fp,$15
addu	$15,$2,$15
lbu	$19,0($19)
lbu	$15,0($15)
addu	$3,$19,$3
.set	noreorder
.set	nomacro
b	$L445
sw	$3,8($5)
.set	macro
.set	reorder

$L459:
lw	$4,8($5)
lw	$6,0($5)
srl	$7,$4,3
andi	$8,$4,0x7
addu	$7,$6,$7
addiu	$4,$4,1
lbu	$2,0($7)
sw	$4,8($5)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,-1
sw	$2,10356($25)
lw	$4,8($5)
srl	$2,$4,3
andi	$7,$4,0x7
addu	$6,$6,$2
addiu	$4,$4,1
lbu	$2,0($6)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L453
sw	$4,8($5)
.set	macro
.set	reorder

li	$4,6			# 0x6
sw	$0,-16076($3)
move	$2,$0
sw	$4,-16084($3)
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
j	$31
sw	$4,-15292($3)
.set	macro
.set	reorder

$L463:
lw	$31,60($sp)
lw	$fp,56($sp)
move	$2,$0
addu	$25,$25,$3
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
sw	$9,-15292($25)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L442:
lw	$25,%call16(ff_generate_sliding_window_mmcos)($28)
.reloc	1f,R_MIPS_JALR,ff_generate_sliding_window_mmcos
1:	jalr	$25
move	$2,$0
lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L453:
.set	noreorder
.set	nomacro
j	$31
move	$2,$0
.set	macro
.set	reorder

$L448:
lw	$6,%got($LC19)($28)
li	$5,16			# 0x10
lw	$2,%call16(av_log)($28)
lw	$4,0($25)
move	$25,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC19)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

.end	ff_h264_decode_ref_pic_marking
.size	ff_h264_decode_ref_pic_marking, .-ff_h264_decode_ref_pic_marking

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
