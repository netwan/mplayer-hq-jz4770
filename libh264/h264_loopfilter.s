.file	1 "h264_loopfilter.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.check_mv,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	check_mv
.type	check_mv, @function
check_mv:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addu	$10,$4,$5
addu	$11,$4,$6
lb	$9,11568($10)
lb	$13,11568($11)
bne	$9,$13,$L2
li	$2,65536			# 0x10000

li	$2,-1			# 0xffffffffffffffff
beq	$9,$2,$L3
sll	$15,$5,2

sll	$14,$6,2
addiu	$3,$5,2812
addiu	$12,$6,2812
addu	$2,$4,$15
addu	$8,$4,$14
sll	$3,$3,2
sll	$12,$12,2
lh	$2,11250($2)
lh	$8,11250($8)
addu	$3,$4,$3
addu	$12,$4,$12
subu	$8,$2,$8
lh	$3,0($3)
lh	$2,0($12)
li	$12,65536			# 0x10000
subu	$24,$0,$8
slt	$25,$8,0
subu	$2,$3,$2
movn	$8,$24,$25
addu	$12,$4,$12
addiu	$2,$2,3
slt	$8,$8,$7
lw	$12,6632($12)
sltu	$2,$2,7
xori	$3,$2,0x1
xori	$2,$8,0x1
or	$2,$3,$2
li	$3,2			# 0x2
beq	$12,$3,$L22
nop

j	$31
nop

$L2:
addu	$2,$4,$2
lw	$3,6632($2)
li	$2,2			# 0x2
beq	$3,$2,$L6
nop

$L19:
j	$31
li	$2,1			# 0x1

$L22:
beq	$2,$0,$L11
nop

$L6:
lb	$2,11608($10)
bne	$2,$13,$L19
nop

lb	$2,11608($11)
bne	$2,$9,$L19
sll	$14,$6,2

sll	$15,$5,2
addu	$15,$4,$15
addu	$4,$4,$14
lh	$2,11410($15)
lh	$8,11410($4)
lh	$3,11250($4)
lh	$6,11250($15)
lh	$11,11408($4)
subu	$3,$2,$3
lh	$5,11248($15)
subu	$6,$6,$8
lh	$2,11408($15)
subu	$12,$0,$3
lh	$8,11248($4)
subu	$9,$0,$6
slt	$4,$3,0
slt	$10,$6,0
movn	$3,$12,$4
subu	$5,$5,$11
movn	$6,$9,$10
subu	$2,$2,$8
addiu	$5,$5,3
slt	$3,$3,$7
addiu	$2,$2,3
slt	$7,$6,$7
xori	$4,$3,0x1
sltu	$5,$5,7
xori	$3,$7,0x1
or	$3,$4,$3
sltu	$2,$2,7
xori	$4,$5,0x1
or	$3,$4,$3
xori	$2,$2,0x1
j	$31
or	$2,$2,$3

$L23:
sll	$15,$5,2
sll	$14,$6,2
$L11:
addu	$14,$4,$14
addiu	$3,$5,2852
addiu	$12,$6,2852
addu	$15,$4,$15
lh	$8,11410($14)
sll	$3,$3,2
sll	$12,$12,2
lh	$2,11410($15)
addu	$3,$4,$3
addu	$12,$4,$12
subu	$8,$2,$8
lh	$3,0($3)
lh	$2,0($12)
subu	$12,$0,$8
slt	$14,$8,0
subu	$2,$3,$2
movn	$8,$12,$14
addiu	$2,$2,3
slt	$8,$8,$7
sltu	$2,$2,7
xori	$3,$2,0x1
xori	$2,$8,0x1
or	$2,$3,$2
bne	$2,$0,$L6
nop

lb	$8,11608($10)
lb	$3,11608($11)
bne	$8,$3,$L6
nop

j	$31
nop

$L3:
li	$2,65536			# 0x10000
addu	$2,$4,$2
lw	$3,6632($2)
li	$2,2			# 0x2
beq	$3,$2,$L23
move	$2,$0

j	$31
nop

.set	macro
.set	reorder
.end	check_mv
.size	check_mv, .-check_mv
.section	.text.filter_mb_mbaff_edgev.isra.0,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	filter_mb_mbaff_edgev.isra.0
.type	filter_mb_mbaff_edgev.isra.0, @function
filter_mb_mbaff_edgev.isra.0:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-24
lw	$13,%got(tc0_table)($28)
move	$3,$0
li	$12,-256			# 0xffffffffffffff00
sw	$21,20($sp)
lw	$2,48($sp)
li	$10,8			# 0x8
lw	$9,40($sp)
addiu	$13,$13,%lo(tc0_table)
lw	$8,44($sp)
addu	$4,$2,$4
sw	$20,16($sp)
addu	$5,$2,$5
lw	$2,%got(alpha_table)($28)
sw	$19,12($sp)
sw	$18,8($sp)
addiu	$2,$2,%lo(alpha_table)
sw	$17,4($sp)
sw	$16,0($sp)
addu	$2,$4,$2
sll	$4,$4,2
lbu	$11,0($2)
lw	$2,%got(beta_table)($28)
sra	$15,$11,2
addiu	$2,$2,%lo(beta_table)
addiu	$15,$15,1
addu	$5,$5,$2
lbu	$14,0($5)
$L52:
sra	$2,$3,1
mul	$2,$2,$8
sll	$2,$2,1
addu	$2,$9,$2
lh	$2,0($2)
beq	$2,$0,$L25
slt	$5,$2,4

beq	$5,$0,$L26
lbu	$24,-1($6)

addu	$2,$4,$2
lbu	$25,0($6)
lbu	$17,-2($6)
addu	$2,$2,$13
lbu	$20,-3($6)
subu	$5,$24,$25
lbu	$18,1($6)
lbu	$19,0($2)
subu	$2,$0,$5
slt	$16,$5,0
movn	$5,$2,$16
slt	$5,$5,$11
beq	$5,$0,$L25
lbu	$21,2($6)

subu	$2,$17,$24
subu	$5,$0,$2
slt	$16,$2,0
movn	$2,$5,$16
slt	$2,$2,$14
beq	$2,$0,$L25
subu	$2,$18,$25

subu	$5,$0,$2
slt	$16,$2,0
movn	$2,$5,$16
slt	$2,$2,$14
beq	$2,$0,$L25
subu	$2,$20,$24

subu	$5,$0,$2
slt	$16,$2,0
movn	$2,$5,$16
slt	$2,$2,$14
bne	$2,$0,$L65
move	$5,$19

$L30:
subu	$2,$21,$25
subu	$16,$0,$2
slt	$20,$2,0
movn	$2,$16,$20
slt	$2,$2,$14
beq	$2,$0,$L70
subu	$2,$25,$24

beq	$19,$0,$L36
addu	$16,$24,$25

sll	$2,$18,1
addiu	$16,$16,1
subu	$20,$0,$19
sra	$16,$16,1
addu	$21,$21,$16
subu	$2,$21,$2
sra	$2,$2,1
slt	$16,$2,$20
beq	$16,$0,$L66
addu	$20,$18,$20

sb	$20,1($6)
$L36:
addiu	$5,$5,1
subu	$2,$25,$24
$L70:
subu	$17,$17,$18
sll	$2,$2,2
subu	$16,$0,$5
addu	$17,$2,$17
addiu	$2,$17,4
sra	$2,$2,3
slt	$17,$2,$16
bne	$17,$0,$L38
nop

slt	$16,$2,$5
movz	$2,$5,$16
move	$16,$2
$L38:
addu	$24,$24,$16
and	$2,$24,$12
beq	$2,$0,$L39
nop

subu	$24,$0,$24
sra	$24,$24,31
andi	$24,$24,0x00ff
$L40:
subu	$25,$25,$16
and	$2,$25,$12
beq	$2,$0,$L41
sb	$24,-1($6)

subu	$25,$0,$25
sra	$25,$25,31
andi	$25,$25,0x00ff
sb	$25,0($6)
$L25:
addiu	$3,$3,1
bne	$3,$10,$L52
addu	$6,$6,$7

lw	$21,20($sp)
$L69:
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,24

$L26:
lbu	$25,0($6)
lbu	$17,-2($6)
lbu	$19,-3($6)
subu	$2,$24,$25
lbu	$16,1($6)
subu	$5,$0,$2
slt	$18,$2,0
movn	$2,$5,$18
slt	$5,$2,$11
beq	$5,$0,$L25
lbu	$20,2($6)

subu	$5,$17,$24
subu	$18,$0,$5
slt	$21,$5,0
movn	$5,$18,$21
slt	$5,$5,$14
beq	$5,$0,$L25
subu	$5,$16,$25

subu	$18,$0,$5
slt	$21,$5,0
movn	$5,$18,$21
slt	$5,$5,$14
beq	$5,$0,$L25
slt	$2,$15,$2

bne	$2,$0,$L46
sll	$5,$17,1

subu	$2,$19,$24
subu	$5,$0,$2
slt	$18,$2,0
movn	$2,$5,$18
slt	$2,$2,$14
beq	$2,$0,$L47
sll	$2,$17,1

lbu	$2,-4($6)
sll	$5,$19,1
sll	$18,$17,1
addu	$5,$5,$19
sll	$2,$2,1
addu	$19,$19,$18
addu	$5,$2,$5
sll	$18,$24,1
subu	$2,$19,$17
addu	$21,$19,$18
addu	$5,$17,$5
sll	$18,$25,1
addu	$2,$24,$2
addu	$18,$21,$18
addu	$5,$24,$5
addu	$18,$16,$18
addu	$2,$25,$2
addu	$5,$25,$5
addiu	$18,$18,4
addiu	$2,$2,2
addiu	$5,$5,4
sra	$18,$18,3
sra	$2,$2,2
sra	$5,$5,3
sb	$18,-1($6)
sb	$2,-2($6)
sb	$5,-3($6)
$L49:
subu	$2,$20,$25
subu	$5,$0,$2
slt	$18,$2,0
movn	$2,$5,$18
slt	$2,$2,$14
beq	$2,$0,$L50
sll	$2,$16,1

lbu	$2,3($6)
sll	$5,$20,1
sll	$18,$24,1
addu	$5,$5,$20
sll	$2,$2,1
addu	$17,$17,$18
addu	$5,$2,$5
sll	$18,$25,1
addu	$5,$16,$5
addu	$2,$17,$18
sll	$17,$16,1
addu	$18,$24,$25
addu	$17,$2,$17
addu	$5,$25,$5
addu	$2,$16,$18
addu	$5,$24,$5
addu	$16,$20,$17
addu	$2,$20,$2
addiu	$16,$16,4
addiu	$2,$2,2
addiu	$5,$5,4
sra	$16,$16,3
sra	$2,$2,2
sra	$5,$5,3
sb	$16,0($6)
sb	$2,1($6)
b	$L25
sb	$5,2($6)

$L65:
beq	$19,$0,$L30
addiu	$5,$19,1

addu	$5,$24,$25
sll	$2,$17,1
addiu	$5,$5,1
subu	$16,$0,$19
sra	$5,$5,1
addu	$20,$20,$5
subu	$2,$20,$2
sra	$2,$2,1
slt	$5,$2,$16
beq	$5,$0,$L67
addu	$16,$17,$16

sb	$16,-2($6)
$L32:
b	$L30
addiu	$5,$19,1

$L46:
sll	$2,$16,1
addu	$24,$24,$5
addu	$25,$25,$2
addu	$5,$16,$24
addu	$2,$17,$25
addiu	$5,$5,2
addiu	$2,$2,2
sra	$5,$5,2
sra	$2,$2,2
addiu	$3,$3,1
sb	$5,-1($6)
sb	$2,0($6)
bne	$3,$10,$L52
addu	$6,$6,$7

b	$L69
lw	$21,20($sp)

$L41:
andi	$25,$25,0x00ff
b	$L25
sb	$25,0($6)

$L39:
b	$L40
andi	$24,$24,0x00ff

$L50:
addu	$2,$25,$2
addu	$2,$17,$2
addiu	$2,$2,2
sra	$2,$2,2
b	$L25
sb	$2,0($6)

$L47:
addu	$2,$24,$2
addu	$2,$16,$2
addiu	$2,$2,2
sra	$2,$2,2
b	$L49
sb	$2,-1($6)

$L67:
slt	$16,$2,$19
movz	$2,$19,$16
move	$16,$2
addu	$16,$17,$16
b	$L32
sb	$16,-2($6)

$L66:
slt	$20,$2,$19
movz	$2,$19,$20
move	$20,$2
addu	$20,$18,$20
b	$L36
sb	$20,1($6)

.set	macro
.set	reorder
.end	filter_mb_mbaff_edgev.isra.0
.size	filter_mb_mbaff_edgev.isra.0, .-filter_mb_mbaff_edgev.isra.0
.section	.text.filter_mb_mbaff_edgecv.isra.1,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	filter_mb_mbaff_edgecv.isra.1
.type	filter_mb_mbaff_edgecv.isra.1, @function
filter_mb_mbaff_edgecv.isra.1:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
lw	$10,%got(alpha_table)($28)
li	$8,4			# 0x4
li	$11,-256			# 0xffffffffffffff00
sw	$17,4($sp)
lw	$2,32($sp)
addiu	$10,$10,%lo(alpha_table)
lw	$9,28($sp)
lw	$3,24($sp)
addu	$4,$2,$4
sw	$16,0($sp)
addu	$5,$2,$5
lw	$2,%got(beta_table)($28)
addu	$10,$4,$10
sll	$9,$9,1
addiu	$2,$2,%lo(beta_table)
lbu	$13,0($10)
sll	$4,$4,2
addu	$5,$5,$2
lw	$10,%got(tc0_table)($28)
lbu	$14,0($5)
addiu	$10,$10,%lo(tc0_table)
$L85:
lh	$2,0($3)
beq	$2,$0,$L72
slt	$5,$2,4

beq	$5,$0,$L73
addu	$2,$4,$2

lbu	$12,-1($6)
lbu	$15,0($6)
addu	$2,$2,$10
lbu	$24,-2($6)
subu	$5,$12,$15
lbu	$25,0($2)
subu	$2,$0,$5
slt	$16,$5,0
movn	$5,$2,$16
slt	$5,$5,$13
beq	$5,$0,$L72
lbu	$17,1($6)

subu	$2,$24,$12
subu	$5,$0,$2
slt	$16,$2,0
movn	$2,$5,$16
slt	$2,$2,$14
beq	$2,$0,$L72
subu	$2,$17,$15

subu	$5,$0,$2
slt	$16,$2,0
movn	$2,$5,$16
slt	$2,$2,$14
beq	$2,$0,$L72
subu	$2,$15,$12

subu	$24,$24,$17
sll	$2,$2,2
addiu	$25,$25,1
addu	$24,$2,$24
subu	$5,$0,$25
addiu	$2,$24,4
sra	$2,$2,3
slt	$16,$2,$5
bne	$16,$0,$L77
nop

slt	$5,$25,$2
movz	$25,$2,$5
move	$5,$25
$L77:
addu	$12,$12,$5
and	$2,$12,$11
beq	$2,$0,$L78
nop

subu	$12,$0,$12
sra	$12,$12,31
andi	$12,$12,0x00ff
$L79:
subu	$15,$15,$5
and	$2,$15,$11
beq	$2,$0,$L80
sb	$12,-1($6)

subu	$15,$0,$15
sra	$15,$15,31
andi	$15,$15,0x00ff
sb	$15,0($6)
$L72:
addiu	$8,$8,-1
addu	$6,$6,$7
bne	$8,$0,$L85
addu	$3,$3,$9

lw	$17,4($sp)
$L92:
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L73:
lbu	$5,-1($6)
lbu	$12,0($6)
lbu	$15,-2($6)
subu	$2,$5,$12
subu	$25,$0,$2
slt	$16,$2,0
movn	$2,$25,$16
slt	$2,$2,$13
beq	$2,$0,$L72
lbu	$24,1($6)

subu	$2,$15,$5
subu	$16,$0,$2
slt	$17,$2,0
movn	$2,$16,$17
slt	$2,$2,$14
beq	$2,$0,$L72
subu	$2,$24,$12

subu	$16,$0,$2
slt	$17,$2,0
movn	$2,$16,$17
slt	$2,$2,$14
beq	$2,$0,$L72
sll	$2,$24,1

sll	$16,$15,1
addu	$12,$12,$2
addu	$5,$5,$16
addu	$12,$15,$12
addu	$5,$24,$5
addiu	$2,$12,2
addiu	$5,$5,2
sra	$2,$2,2
sra	$5,$5,2
addiu	$8,$8,-1
sb	$2,0($6)
addu	$3,$3,$9
sb	$5,-1($6)
bne	$8,$0,$L85
addu	$6,$6,$7

b	$L92
lw	$17,4($sp)

$L80:
andi	$15,$15,0x00ff
b	$L72
sb	$15,0($6)

$L78:
b	$L79
andi	$12,$12,0x00ff

.set	macro
.set	reorder
.end	filter_mb_mbaff_edgecv.isra.1
.size	filter_mb_mbaff_edgecv.isra.1, .-filter_mb_mbaff_edgecv.isra.1
.section	.text.ff_h264_filter_mb,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_filter_mb
.set	nomips16
.set	nomicromips
.ent	ff_h264_filter_mb
.type	ff_h264_filter_mb, @function
ff_h264_filter_mb:
.frame	$sp,168,$31		# vars= 88, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-168
li	$2,65536			# 0x10000
sw	$18,136($sp)
move	$18,$4
lw	$4,168($4)
addu	$2,$18,$2
lw	$3,2192($18)
sw	$21,148($sp)
mul	$9,$6,$4
sw	$6,176($sp)
.cprestore	32
sw	$31,164($sp)
sw	$fp,160($sp)
sw	$23,156($sp)
sw	$22,152($sp)
sw	$20,144($sp)
addu	$8,$9,$5
sw	$19,140($sp)
sw	$17,132($sp)
sll	$5,$8,2
sw	$16,128($sp)
sw	$8,68($sp)
addu	$3,$3,$5
sw	$7,180($sp)
li	$5,2			# 0x2
lw	$4,-5252($2)
lw	$21,0($3)
li	$3,4			# 0x4
andi	$6,$21,0x80
movn	$3,$5,$6
bne	$4,$0,$L95
sw	$3,76($sp)

lw	$4,10800($18)
move	$5,$0
$L96:
sra	$3,$21,3
lw	$2,%got(mask_edge_tab.7593)($28)
andi	$3,$3,0x7
addiu	$2,$2,%lo(mask_edge_tab.7593)
sw	$3,72($sp)
lw	$8,72($sp)
lw	$3,68($sp)
sw	$2,84($sp)
addu	$2,$2,$8
addiu	$16,$3,-1
li	$3,3			# 0x3
lbu	$9,0($2)
beq	$9,$3,$L385
sw	$9,80($sp)

$L113:
andi	$3,$21,0x28
beq	$4,$0,$L374
sw	$3,88($sp)

bne	$5,$0,$L209
li	$23,4			# 0x4

$L214:
li	$2,65536			# 0x10000
or	$3,$4,$21
addu	$2,$18,$2
andi	$5,$3,0x7
beq	$5,$0,$L115
lw	$2,-5252($2)

li	$4,196608			# 0x30000
li	$5,196608			# 0x30000
ori	$4,$4,0x3
ori	$5,$5,0x3
andi	$3,$3,0x80
sw	$4,40($sp)
beq	$3,$0,$L116
sw	$5,44($sp)

beq	$2,$0,$L386
li	$2,3			# 0x3

$L116:
li	$2,262144			# 0x40000
$L416:
li	$3,262144			# 0x40000
ori	$2,$2,0x4
ori	$3,$3,0x4
sw	$2,40($sp)
sw	$3,44($sp)
$L117:
lh	$20,40($sp)
$L404:
lh	$fp,42($sp)
lh	$11,44($sp)
lh	$2,46($sp)
addu	$3,$20,$fp
addu	$3,$3,$11
addu	$3,$3,$2
beq	$3,$0,$L124
li	$22,131072			# 0x20000

lw	$3,2172($18)
lw	$15,68($sp)
addu	$22,$18,$22
lw	$17,%got(alpha_table)($28)
addu	$5,$3,$16
lw	$19,%got(beta_table)($28)
addu	$3,$3,$15
lw	$4,9460($22)
addiu	$17,$17,%lo(alpha_table)
lb	$8,0($5)
addiu	$19,$19,%lo(beta_table)
lb	$6,0($3)
lw	$5,9464($22)
addu	$6,$6,$8
addiu	$6,$6,1
sra	$6,$6,1
addu	$3,$6,$4
addu	$6,$6,$5
addu	$9,$17,$3
addu	$7,$19,$6
lbu	$6,0($9)
beq	$6,$0,$L125
lbu	$7,0($7)

bne	$7,$0,$L387
nop

$L125:
andi	$3,$8,0xff
lw	$22,10740($18)
addu	$3,$18,$3
lbu	$3,13116($3)
addu	$22,$3,$22
addiu	$22,$22,1
sra	$22,$22,1
addu	$3,$22,$4
addu	$7,$22,$5
addu	$6,$17,$3
addu	$7,$19,$7
lbu	$6,0($6)
beq	$6,$0,$L127
lbu	$7,0($7)

bne	$7,$0,$L388
nop

$L127:
lw	$3,13628($18)
beq	$3,$0,$L129
nop

lw	$3,2172($18)
lw	$22,10744($18)
addu	$16,$3,$16
lbu	$3,0($16)
addu	$3,$18,$3
lbu	$3,13372($3)
addu	$22,$3,$22
addiu	$22,$22,1
sra	$22,$22,1
$L129:
addu	$4,$22,$4
addu	$5,$22,$5
addu	$17,$17,$4
addu	$19,$19,$5
lbu	$6,0($17)
beq	$6,$0,$L124
lbu	$7,0($19)

beq	$7,$0,$L124
slt	$3,$20,4

beq	$3,$0,$L131
lw	$9,%got(tc0_table)($28)

addiu	$5,$sp,56
sll	$8,$4,2
lw	$25,10684($18)
addiu	$9,$9,%lo(tc0_table)
sw	$5,16($sp)
addu	$3,$8,$20
addu	$5,$8,$11
addu	$4,$8,$fp
addu	$2,$8,$2
addu	$4,$4,$9
addu	$5,$5,$9
addu	$3,$3,$9
addu	$2,$2,$9
lbu	$8,0($4)
lw	$4,188($sp)
lbu	$9,0($3)
lbu	$2,0($2)
addiu	$8,$8,1
lbu	$3,0($5)
addiu	$9,$9,1
lw	$5,196($sp)
addiu	$2,$2,1
sb	$8,57($sp)
addiu	$3,$3,1
sb	$9,56($sp)
sb	$2,59($sp)
jalr	$25
sb	$3,58($sp)

lw	$28,32($sp)
$L124:
li	$2,1			# 0x1
bne	$23,$2,$L405
lw	$25,188($sp)

b	$L402
lw	$3,84($sp)

$L95:
lw	$4,10800($18)
xor	$3,$21,$4
andi	$3,$3,0x80
beq	$3,$0,$L96
move	$5,$0

beq	$4,$0,$L97
sra	$3,$21,3

andi	$3,$21,0x7
beq	$3,$0,$L98
addiu	$20,$sp,40

li	$6,262144			# 0x40000
lw	$5,-5248($2)
li	$7,262144			# 0x40000
ori	$6,$6,0x4
ori	$7,$7,0x4
sw	$6,40($sp)
sw	$7,44($sp)
sw	$6,48($sp)
sw	$7,52($sp)
$L99:
lw	$2,2172($18)
lw	$8,68($sp)
lw	$6,10780($18)
lw	$3,10784($18)
addu	$4,$2,$8
addu	$6,$2,$6
addu	$3,$2,$3
lb	$4,0($4)
lb	$2,0($6)
lb	$10,0($3)
andi	$8,$4,0xff
andi	$9,$2,0xff
andi	$7,$10,0xff
addu	$8,$18,$8
addu	$9,$18,$9
addu	$7,$18,$7
lbu	$3,13116($8)
addu	$6,$4,$2
lbu	$fp,13372($9)
addu	$4,$4,$10
lbu	$2,13372($8)
addiu	$6,$6,1
lbu	$8,13116($9)
addiu	$4,$4,1
lbu	$9,13116($7)
sra	$6,$6,1
lbu	$7,13372($7)
addu	$fp,$2,$fp
addu	$8,$3,$8
addu	$3,$3,$9
addu	$2,$2,$7
addiu	$8,$8,1
addiu	$fp,$fp,1
addiu	$3,$3,1
addiu	$2,$2,1
sra	$8,$8,1
sra	$fp,$fp,1
sra	$16,$4,1
sra	$23,$3,1
beq	$5,$0,$L111
sra	$9,$2,1

li	$2,131072			# 0x20000
lw	$25,%got(filter_mb_mbaff_edgev.isra.0)($28)
li	$17,1			# 0x1
lw	$14,196($sp)
addu	$19,$18,$2
lw	$7,192($sp)
sw	$6,24($sp)
addiu	$25,$25,%lo(filter_mb_mbaff_edgev.isra.0)
sll	$14,$14,2
sw	$20,16($sp)
sw	$17,20($sp)
addiu	$22,$sp,48
lw	$4,9460($19)
lw	$5,9464($19)
lw	$6,180($sp)
sw	$14,64($sp)
sw	$9,104($sp)
sw	$8,112($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgev.isra.0
1:	jalr	$25
sw	$25,92($sp)

lw	$15,192($sp)
lw	$3,180($sp)
lw	$4,9460($19)
sll	$6,$15,3
lw	$5,9464($19)
lw	$25,92($sp)
move	$7,$15
addu	$6,$3,$6
sw	$16,24($sp)
sw	$22,16($sp)
jalr	$25
sw	$17,20($sp)

lw	$28,32($sp)
lw	$8,112($sp)
lw	$4,9460($19)
lw	$5,9464($19)
lw	$16,%got(filter_mb_mbaff_edgecv.isra.1)($28)
lw	$6,184($sp)
lw	$7,196($sp)
addiu	$16,$16,%lo(filter_mb_mbaff_edgecv.isra.1)
sw	$8,24($sp)
sw	$20,16($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$17,20($sp)

move	$25,$16
lw	$3,184($sp)
lw	$8,64($sp)
lw	$4,9460($19)
lw	$5,9464($19)
lw	$7,196($sp)
addu	$6,$3,$8
sw	$22,16($sp)
sw	$17,20($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$23,24($sp)

move	$25,$16
lw	$4,9460($19)
lw	$5,9464($19)
lw	$6,188($sp)
lw	$7,196($sp)
sw	$20,16($sp)
sw	$17,20($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$fp,24($sp)

lw	$3,188($sp)
lw	$8,64($sp)
lw	$7,196($sp)
lw	$4,9460($19)
addu	$6,$3,$8
lw	$5,9464($19)
sw	$22,16($sp)
$L373:
lw	$9,104($sp)
move	$25,$16
sw	$17,20($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$9,24($sp)

sra	$3,$21,3
lw	$28,32($sp)
li	$5,1			# 0x1
andi	$3,$3,0x7
lw	$4,10800($18)
sw	$3,72($sp)
lw	$2,%got(mask_edge_tab.7593)($28)
lw	$8,72($sp)
lw	$3,68($sp)
addiu	$2,$2,%lo(mask_edge_tab.7593)
addiu	$16,$3,-1
sw	$2,84($sp)
addu	$2,$2,$8
li	$3,3			# 0x3
lbu	$9,0($2)
bne	$9,$3,$L113
sw	$9,80($sp)

$L385:
li	$3,131072			# 0x20000
addu	$3,$18,$3
lw	$3,8736($3)
andi	$3,$3,0xf
bne	$3,$0,$L113
nop

beq	$4,$0,$L213
lw	$25,80($sp)

andi	$8,$21,0x28
bne	$5,$0,$L132
sw	$8,88($sp)

b	$L214
li	$23,1			# 0x1

$L396:
li	$2,131072			# 0x20000
addu	$2,$18,$2
lw	$2,8736($2)
andi	$2,$2,0xf
beq	$2,$0,$L402
lw	$3,84($sp)

andi	$3,$21,0x28
sw	$3,88($sp)
$L374:
li	$23,4			# 0x4
$L209:
lw	$25,188($sp)
$L405:
li	$12,16777216			# 0x1000000
lw	$3,184($sp)
li	$11,131072			# 0x20000
lw	$14,180($sp)
and	$12,$21,$12
lw	$22,%got(alpha_table)($28)
addiu	$10,$25,2
lw	$20,%got(beta_table)($28)
addiu	$9,$3,2
addiu	$8,$14,4
sw	$21,64($sp)
addu	$11,$18,$11
move	$15,$23
move	$13,$18
li	$16,13			# 0xd
addiu	$22,$22,%lo(alpha_table)
addiu	$20,$20,%lo(beta_table)
move	$21,$8
move	$23,$9
b	$L150
move	$18,$10

$L390:
li	$3,196608			# 0x30000
ori	$2,$2,0x3
ori	$3,$3,0x3
sw	$2,40($sp)
sw	$3,44($sp)
$L135:
lw	$4,2172($13)
lw	$25,68($sp)
lw	$3,9460($11)
lw	$5,9464($11)
addu	$4,$4,$25
lb	$2,0($4)
addu	$3,$2,$3
addu	$2,$2,$5
addu	$4,$22,$3
addu	$2,$20,$2
lbu	$6,0($4)
beq	$6,$0,$L141
lbu	$7,0($2)

beq	$7,$0,$L141
lh	$2,40($sp)

slt	$4,$2,4
beq	$4,$0,$L142
move	$4,$21

lw	$8,%got(tc0_table)($28)
sll	$3,$3,2
lh	$17,46($sp)
lh	$14,42($sp)
addu	$2,$3,$2
lh	$4,44($sp)
addiu	$5,$8,%lo(tc0_table)
lw	$25,10668($13)
addu	$14,$3,$14
sw	$11,100($sp)
addu	$4,$3,$4
sw	$12,116($sp)
addu	$3,$3,$17
sw	$13,120($sp)
addu	$2,$2,$5
sw	$15,92($sp)
addu	$3,$3,$5
addu	$14,$14,$5
addu	$4,$4,$5
lbu	$19,0($2)
lbu	$2,0($3)
lbu	$17,0($14)
lbu	$14,0($4)
move	$4,$21
lw	$5,192($sp)
sb	$2,59($sp)
addiu	$2,$sp,56
sb	$19,56($sp)
sb	$17,57($sp)
sb	$14,58($sp)
jalr	$25
sw	$2,16($sp)

lw	$28,32($sp)
lw	$11,100($sp)
lw	$12,116($sp)
lw	$13,120($sp)
lw	$15,92($sp)
$L141:
andi	$fp,$fp,0x1
bne	$fp,$0,$L406
addiu	$2,$16,-11

lw	$3,10740($13)
lw	$4,9460($11)
lw	$17,9464($11)
addu	$14,$4,$3
addu	$3,$17,$3
addu	$5,$22,$14
addu	$3,$20,$3
lbu	$6,0($5)
beq	$6,$0,$L145
lbu	$7,0($3)

beq	$7,$0,$L145
lh	$3,40($sp)

slt	$2,$3,4
beq	$2,$0,$L146
move	$4,$23

lw	$9,%got(tc0_table)($28)
sll	$14,$14,2
lh	$5,42($sp)
lh	$2,46($sp)
addu	$3,$14,$3
lh	$4,44($sp)
addiu	$17,$9,%lo(tc0_table)
addu	$5,$14,$5
lw	$25,10684($13)
addu	$3,$3,$17
sw	$11,100($sp)
addu	$4,$14,$4
sw	$12,116($sp)
addu	$14,$14,$2
sw	$13,120($sp)
addu	$5,$5,$17
sw	$15,92($sp)
addu	$4,$4,$17
addu	$2,$14,$17
lbu	$14,0($3)
lbu	$5,0($5)
addiu	$17,$sp,56
lbu	$3,0($4)
move	$4,$23
lbu	$2,0($2)
addiu	$14,$14,1
addiu	$5,$5,1
sw	$17,16($sp)
addiu	$3,$3,1
addiu	$2,$2,1
sb	$14,56($sp)
sb	$5,57($sp)
sb	$3,58($sp)
sb	$2,59($sp)
jalr	$25
lw	$5,196($sp)

lw	$11,100($sp)
lw	$28,32($sp)
lw	$12,116($sp)
lw	$13,120($sp)
lw	$4,9460($11)
lw	$17,9464($11)
lw	$15,92($sp)
$L145:
lw	$2,10744($13)
addu	$4,$4,$2
addu	$2,$17,$2
addu	$3,$22,$4
addu	$2,$20,$2
lbu	$6,0($3)
beq	$6,$0,$L149
lbu	$7,0($2)

beq	$7,$0,$L406
addiu	$2,$16,-11

lh	$3,40($sp)
slt	$2,$3,4
beq	$2,$0,$L148
lw	$25,%got(tc0_table)($28)

sll	$4,$4,2
lh	$5,42($sp)
lh	$2,46($sp)
addu	$3,$4,$3
lh	$14,44($sp)
addiu	$17,$25,%lo(tc0_table)
addu	$5,$4,$5
lw	$25,10684($13)
addu	$3,$3,$17
sw	$11,100($sp)
addu	$14,$4,$14
sw	$12,116($sp)
addu	$4,$4,$2
sw	$13,120($sp)
addu	$5,$5,$17
sw	$15,92($sp)
addu	$14,$14,$17
addu	$2,$4,$17
lbu	$17,0($3)
lbu	$5,0($5)
move	$4,$18
lbu	$3,0($14)
lbu	$2,0($2)
addiu	$14,$17,1
addiu	$5,$5,1
addiu	$3,$3,1
addiu	$2,$2,1
sb	$14,56($sp)
addiu	$17,$sp,56
sb	$5,57($sp)
sb	$3,58($sp)
sb	$2,59($sp)
sw	$17,16($sp)
jalr	$25
lw	$5,196($sp)

lw	$28,32($sp)
lw	$11,100($sp)
lw	$12,116($sp)
lw	$13,120($sp)
lw	$15,92($sp)
$L149:
addiu	$2,$16,-11
$L406:
addiu	$18,$18,2
slt	$2,$2,$15
addiu	$16,$16,1
addiu	$23,$23,2
beq	$2,$0,$L389
addiu	$21,$21,4

$L150:
addiu	$fp,$16,-12
sll	$2,$fp,24
and	$2,$2,$12
bne	$2,$0,$L406
addiu	$2,$16,-11

lw	$25,64($sp)
andi	$2,$25,0x7
bne	$2,$0,$L390
li	$2,196608			# 0x30000

lw	$3,80($sp)
and	$2,$fp,$3
beq	$2,$0,$L136
lw	$8,88($sp)

move	$4,$0
move	$5,$0
li	$25,1			# 0x1
sw	$4,40($sp)
sw	$5,44($sp)
$L137:
addiu	$14,$16,32
addiu	$3,$sp,40
li	$24,2			# 0x2
addu	$19,$13,$16
move	$17,$16
move	$8,$21
move	$9,$fp
move	$10,$16
move	$fp,$23
move	$21,$25
move	$23,$18
move	$16,$3
move	$18,$14
$L140:
lbu	$2,11183($19)
lbu	$4,11184($19)
or	$2,$2,$4
andi	$2,$2,0x00ff
beq	$2,$0,$L138
addiu	$6,$17,-1

sh	$24,0($16)
$L139:
addiu	$17,$17,8
addiu	$19,$19,8
bne	$17,$18,$L140
addiu	$16,$16,2

lh	$3,40($sp)
move	$18,$23
lh	$5,42($sp)
move	$23,$fp
lh	$2,44($sp)
move	$21,$8
lh	$4,46($sp)
move	$fp,$9
addu	$3,$3,$5
addu	$2,$3,$2
addu	$2,$2,$4
bne	$2,$0,$L135
move	$16,$10

addiu	$2,$16,-11
addiu	$18,$18,2
slt	$2,$2,$15
addiu	$16,$16,1
addiu	$23,$23,2
bne	$2,$0,$L150
addiu	$21,$21,4

$L389:
lw	$21,64($sp)
move	$18,$13
$L132:
lw	$3,84($sp)
$L402:
lw	$8,72($sp)
lw	$20,10772($18)
addu	$2,$3,$8
lw	$3,10792($18)
lbu	$2,8($2)
sw	$2,72($sp)
li	$2,3			# 0x3
lw	$9,72($sp)
beq	$9,$2,$L391
li	$2,131072			# 0x20000

$L151:
andi	$9,$21,0x18
$L413:
li	$fp,4			# 0x4
beq	$3,$0,$L187
sw	$9,80($sp)

$L215:
li	$2,65536			# 0x10000
addu	$2,$18,$2
lw	$2,-5252($2)
beq	$2,$0,$L153
or	$2,$21,$3

lw	$14,176($sp)
andi	$2,$14,0x1
beq	$2,$0,$L392
andi	$2,$3,0x80

or	$2,$21,$3
$L410:
andi	$4,$2,0x7
bne	$4,$0,$L407
andi	$2,$2,0x80

xor	$2,$21,$3
andi	$2,$2,0x80
beq	$2,$0,$L408
lw	$8,80($sp)

li	$2,65536			# 0x10000
li	$3,65536			# 0x10000
ori	$2,$2,0x1
ori	$3,$3,0x1
li	$8,1			# 0x1
sw	$2,40($sp)
sw	$3,44($sp)
$L173:
lw	$3,%got(check_mv)($28)
addiu	$19,$sp,40
li	$16,12			# 0xc
li	$23,2			# 0x2
li	$22,16			# 0x10
addiu	$17,$18,11196
move	$9,$fp
move	$fp,$20
move	$20,$18
move	$18,$8
$L176:
lbu	$2,-8($17)
lbu	$5,0($17)
or	$2,$2,$5
andi	$2,$2,0x00ff
beq	$2,$0,$L174
addiu	$6,$16,-8

sh	$23,0($19)
$L175:
addiu	$16,$16,1
addiu	$17,$17,1
bne	$16,$22,$L176
addiu	$19,$19,2

move	$18,$20
move	$20,$fp
move	$fp,$9
$L171:
lh	$22,40($sp)
lh	$11,42($sp)
lh	$23,44($sp)
lh	$2,46($sp)
addu	$3,$22,$11
addu	$3,$3,$23
addu	$3,$3,$2
beq	$3,$0,$L169
lw	$9,68($sp)

lw	$3,2172($18)
li	$16,131072			# 0x20000
addu	$16,$18,$16
lw	$17,%got(alpha_table)($28)
addu	$5,$3,$20
lw	$19,%got(beta_table)($28)
addu	$3,$3,$9
lw	$4,9460($16)
addiu	$17,$17,%lo(alpha_table)
lb	$8,0($5)
addiu	$19,$19,%lo(beta_table)
lb	$6,0($3)
lw	$5,9464($16)
addu	$6,$6,$8
addiu	$6,$6,1
sra	$6,$6,1
addu	$3,$6,$4
addu	$6,$6,$5
addu	$9,$17,$3
addu	$7,$19,$6
lbu	$6,0($9)
beq	$6,$0,$L180
lbu	$7,0($7)

beq	$7,$0,$L409
andi	$8,$8,0xff

slt	$4,$22,4
beq	$4,$0,$L181
lw	$4,180($sp)

lw	$9,%got(tc0_table)($28)
addiu	$5,$sp,56
sll	$3,$3,2
lw	$25,10664($18)
sw	$2,92($sp)
addiu	$9,$9,%lo(tc0_table)
sw	$5,16($sp)
addu	$4,$3,$11
sw	$11,100($sp)
addu	$8,$3,$22
addu	$5,$3,$23
addu	$3,$3,$2
addu	$5,$5,$9
addu	$8,$8,$9
addu	$4,$4,$9
addu	$3,$3,$9
lbu	$10,0($5)
lbu	$8,0($8)
lbu	$9,0($4)
lbu	$3,0($3)
lw	$4,180($sp)
lw	$5,192($sp)
sb	$8,56($sp)
sb	$3,59($sp)
sb	$9,57($sp)
jalr	$25
sb	$10,58($sp)

lw	$3,2172($18)
lw	$28,32($sp)
lw	$4,9460($16)
addu	$3,$3,$20
lw	$5,9464($16)
lw	$2,92($sp)
lw	$11,100($sp)
lb	$8,0($3)
$L180:
andi	$8,$8,0xff
$L409:
lw	$16,10740($18)
addu	$8,$18,$8
lbu	$3,13116($8)
addu	$16,$3,$16
addiu	$16,$16,1
sra	$16,$16,1
addu	$3,$16,$4
addu	$7,$16,$5
addu	$6,$17,$3
addu	$7,$19,$7
lbu	$6,0($6)
beq	$6,$0,$L182
lbu	$7,0($7)

beq	$7,$0,$L182
nop

slt	$4,$22,4
beq	$4,$0,$L183
lw	$4,184($sp)

lw	$9,%got(tc0_table)($28)
addiu	$5,$sp,56
sll	$3,$3,2
lw	$25,10680($18)
sw	$2,92($sp)
addiu	$9,$9,%lo(tc0_table)
sw	$5,16($sp)
addu	$4,$3,$11
sw	$11,100($sp)
addu	$8,$3,$22
addu	$5,$3,$23
addu	$3,$3,$2
addu	$8,$8,$9
addu	$5,$5,$9
addu	$4,$4,$9
addu	$3,$3,$9
lbu	$10,0($8)
lbu	$8,0($5)
lbu	$9,0($4)
lbu	$3,0($3)
addiu	$10,$10,1
addiu	$8,$8,1
lw	$4,184($sp)
addiu	$9,$9,1
lw	$5,196($sp)
addiu	$3,$3,1
sb	$10,56($sp)
sb	$8,58($sp)
sb	$9,57($sp)
jalr	$25
sb	$3,59($sp)

li	$3,131072			# 0x20000
lw	$28,32($sp)
addu	$3,$18,$3
lw	$2,92($sp)
lw	$11,100($sp)
lw	$4,9460($3)
lw	$5,9464($3)
$L182:
lw	$3,13628($18)
beq	$3,$0,$L184
nop

lw	$3,2172($18)
lw	$16,10744($18)
addu	$20,$3,$20
lbu	$3,0($20)
addu	$3,$18,$3
lbu	$3,13372($3)
addu	$16,$3,$16
addiu	$16,$16,1
sra	$16,$16,1
$L184:
addu	$4,$16,$4
addu	$16,$16,$5
addu	$17,$17,$4
addu	$19,$19,$16
lbu	$6,0($17)
beq	$6,$0,$L169
lbu	$7,0($19)

beq	$7,$0,$L169
nop

slt	$3,$22,4
beq	$3,$0,$L186
lw	$9,%got(tc0_table)($28)

addiu	$5,$sp,56
sll	$8,$4,2
lw	$25,10680($18)
addiu	$9,$9,%lo(tc0_table)
sw	$5,16($sp)
addu	$4,$8,$11
addu	$3,$8,$22
addu	$5,$8,$23
addu	$2,$8,$2
addu	$4,$4,$9
addu	$5,$5,$9
addu	$3,$3,$9
addu	$2,$2,$9
lbu	$8,0($4)
lw	$4,188($sp)
lbu	$9,0($3)
lbu	$2,0($2)
addiu	$8,$8,1
lbu	$3,0($5)
addiu	$9,$9,1
lw	$5,196($sp)
addiu	$2,$2,1
sb	$8,57($sp)
addiu	$3,$3,1
sb	$9,56($sp)
sb	$2,59($sp)
jalr	$25
sb	$3,58($sp)

b	$L169
lw	$28,32($sp)

$L138:
lw	$14,%got(check_mv)($28)
bne	$21,$0,$L139
addiu	$25,$14,%lo(check_mv)

lw	$7,76($sp)
move	$4,$13
sw	$8,112($sp)
move	$5,$17
sw	$9,104($sp)
sw	$10,108($sp)
sw	$11,100($sp)
sw	$12,116($sp)
sw	$13,120($sp)
sw	$15,92($sp)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
sw	$24,96($sp)

lw	$28,32($sp)
lw	$24,96($sp)
lw	$15,92($sp)
lw	$13,120($sp)
lw	$12,116($sp)
lw	$11,100($sp)
lw	$10,108($sp)
lw	$9,104($sp)
lw	$8,112($sp)
b	$L139
sh	$2,0($16)

$L136:
beq	$8,$0,$L224
lw	$9,%got(check_mv)($28)

addiu	$6,$16,-1
lw	$7,76($sp)
move	$4,$13
sw	$11,100($sp)
move	$5,$16
addiu	$25,$9,%lo(check_mv)
sw	$12,116($sp)
sw	$13,120($sp)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
sw	$15,92($sp)

li	$25,1			# 0x1
sll	$2,$2,16
lw	$28,32($sp)
lw	$11,100($sp)
sra	$2,$2,16
lw	$12,116($sp)
lw	$13,120($sp)
lw	$15,92($sp)
sh	$2,46($sp)
sh	$2,44($sp)
sh	$2,42($sp)
b	$L137
sh	$2,40($sp)

$L392:
nor	$4,$0,$21
and	$2,$2,$4
beq	$2,$0,$L410
or	$2,$21,$3

lw	$16,168($18)
li	$20,131072			# 0x20000
lw	$15,192($sp)
li	$12,2			# 0x2
lw	$25,196($sp)
move	$22,$0
lw	$17,%got(alpha_table)($28)
sll	$16,$16,1
lw	$19,%got(beta_table)($28)
sll	$15,$15,1
lw	$3,68($sp)
sll	$25,$25,1
lw	$11,%got(tc0_table)($28)
addiu	$17,$17,%lo(alpha_table)
lw	$23,180($sp)
addiu	$19,$19,%lo(beta_table)
subu	$16,$3,$16
sw	$fp,84($sp)
addu	$20,$18,$20
sw	$15,88($sp)
addiu	$11,$11,%lo(tc0_table)
sw	$25,64($sp)
move	$fp,$21
move	$21,$12
$L168:
lw	$2,2192($18)
sll	$3,$16,2
addu	$2,$2,$3
lw	$3,0($2)
or	$2,$3,$fp
andi	$2,$2,0x7
beq	$2,$0,$L155
li	$4,196608			# 0x30000

li	$5,196608			# 0x30000
ori	$4,$4,0x3
ori	$5,$5,0x3
sw	$4,40($sp)
sw	$5,44($sp)
$L156:
lw	$2,2172($18)
$L403:
lw	$9,68($sp)
lw	$4,9460($20)
addu	$3,$2,$16
lw	$5,9464($20)
addu	$2,$2,$9
lb	$3,0($3)
lb	$2,0($2)
addu	$2,$2,$3
addiu	$2,$2,1
sra	$2,$2,1
addu	$9,$2,$4
addu	$2,$2,$5
addu	$6,$17,$9
addu	$2,$19,$2
lbu	$6,0($6)
beq	$6,$0,$L162
lbu	$7,0($2)

beq	$7,$0,$L162
nop

lh	$3,40($sp)
slt	$2,$3,4
beq	$2,$0,$L163
move	$4,$23

sll	$2,$9,2
lh	$4,42($sp)
lh	$9,46($sp)
addiu	$10,$sp,56
lh	$5,44($sp)
addu	$3,$2,$3
addu	$4,$2,$4
lw	$25,10664($18)
addu	$3,$3,$11
sw	$10,16($sp)
addu	$5,$2,$5
sw	$11,100($sp)
addu	$2,$2,$9
addu	$4,$4,$11
lbu	$9,0($3)
addu	$5,$5,$11
addu	$2,$2,$11
lbu	$3,0($4)
move	$4,$23
lbu	$10,0($5)
lbu	$2,0($2)
lw	$5,88($sp)
sb	$9,56($sp)
sb	$3,57($sp)
sb	$10,58($sp)
jalr	$25
sb	$2,59($sp)

lw	$28,32($sp)
$L375:
lw	$2,2172($18)
lw	$4,9460($20)
lw	$5,9464($20)
addu	$2,$2,$16
lw	$11,100($sp)
lb	$3,0($2)
$L162:
andi	$9,$3,0xff
lw	$3,10740($18)
addu	$9,$18,$9
lbu	$2,13116($9)
addu	$3,$2,$3
addiu	$3,$3,1
sra	$3,$3,1
addu	$2,$3,$4
addu	$3,$3,$5
addu	$6,$17,$2
addu	$3,$19,$3
lbu	$6,0($6)
beq	$6,$0,$L164
lbu	$7,0($3)

beq	$7,$0,$L164
lh	$3,40($sp)

lw	$14,184($sp)
slt	$5,$3,4
beq	$5,$0,$L165
addu	$4,$14,$22

lh	$12,44($sp)
sll	$2,$2,2
lh	$10,46($sp)
lh	$5,42($sp)
addu	$9,$2,$3
addu	$3,$2,$12
lw	$25,10680($18)
addu	$9,$9,$11
sw	$11,100($sp)
addu	$5,$2,$5
addu	$2,$2,$10
addu	$5,$5,$11
lbu	$10,0($9)
addu	$3,$3,$11
addu	$2,$2,$11
lbu	$9,0($5)
addiu	$12,$sp,56
lbu	$3,0($3)
addiu	$10,$10,1
lbu	$2,0($2)
addiu	$9,$9,1
lw	$5,64($sp)
addiu	$3,$3,1
sw	$12,16($sp)
addiu	$2,$2,1
sb	$10,56($sp)
sb	$9,57($sp)
sb	$3,58($sp)
jalr	$25
sb	$2,59($sp)

lw	$28,32($sp)
$L376:
lw	$2,2172($18)
lw	$4,9460($20)
lw	$5,9464($20)
addu	$2,$2,$16
lw	$11,100($sp)
lbu	$9,0($2)
addu	$9,$18,$9
$L164:
lw	$2,10744($18)
lbu	$3,13372($9)
addu	$3,$3,$2
addiu	$3,$3,1
sra	$2,$3,1
addu	$3,$2,$4
addu	$2,$2,$5
addu	$4,$17,$3
addu	$2,$19,$2
lbu	$6,0($4)
beq	$6,$0,$L166
lbu	$7,0($2)

beq	$7,$0,$L166
lh	$5,40($sp)

lw	$15,188($sp)
slt	$2,$5,4
beq	$2,$0,$L167
addu	$4,$15,$22

lh	$12,44($sp)
sll	$3,$3,2
lh	$2,46($sp)
lh	$9,42($sp)
addu	$10,$3,$5
addu	$5,$3,$12
lw	$25,10680($18)
addu	$10,$10,$11
sw	$11,100($sp)
addu	$9,$3,$9
addu	$3,$3,$2
addu	$5,$5,$11
lbu	$10,0($10)
addu	$2,$3,$11
addu	$9,$9,$11
lbu	$3,0($5)
addiu	$12,$sp,56
lbu	$2,0($2)
addiu	$10,$10,1
lbu	$9,0($9)
addiu	$3,$3,1
lw	$5,64($sp)
addiu	$2,$2,1
sw	$12,16($sp)
addiu	$9,$9,1
sb	$10,56($sp)
sb	$3,58($sp)
sb	$2,59($sp)
jalr	$25
sb	$9,57($sp)

lw	$28,32($sp)
lw	$11,100($sp)
$L166:
lw	$2,168($18)
lw	$25,196($sp)
lw	$3,192($sp)
addu	$16,$16,$2
li	$2,1			# 0x1
addu	$22,$22,$25
bne	$21,$2,$L227
addu	$23,$23,$3

move	$21,$fp
lw	$fp,84($sp)
$L169:
li	$2,1			# 0x1
beq	$fp,$2,$L411
lw	$31,164($sp)

$L187:
lw	$15,192($sp)
li	$11,16777216			# 0x1000000
lw	$3,180($sp)
li	$10,131072			# 0x20000
lw	$25,196($sp)
and	$22,$21,$11
sll	$14,$15,2
lw	$23,%got(alpha_table)($28)
sw	$21,64($sp)
move	$12,$fp
addu	$9,$3,$14
sll	$15,$25,1
move	$13,$22
move	$20,$0
li	$8,8			# 0x8
li	$17,1			# 0x1
addu	$10,$18,$10
addiu	$23,$23,%lo(alpha_table)
move	$21,$14
move	$fp,$9
move	$22,$15
b	$L205
move	$3,$18

$L393:
andi	$2,$9,0x7
beq	$2,$0,$L189
lw	$14,72($sp)

li	$4,196608			# 0x30000
li	$5,196608			# 0x30000
ori	$4,$4,0x3
ori	$5,$5,0x3
sw	$4,40($sp)
sw	$5,44($sp)
$L190:
lw	$5,2172($3)
lw	$25,68($sp)
lw	$6,9464($10)
lw	$4,9460($10)
addu	$5,$5,$25
lb	$2,0($5)
addu	$4,$2,$4
addu	$2,$2,$6
lw	$6,%got(beta_table)($28)
addu	$5,$23,$4
addiu	$6,$6,%lo(beta_table)
addu	$2,$6,$2
lbu	$6,0($5)
beq	$6,$0,$L196
lbu	$7,0($2)

beq	$7,$0,$L412
andi	$2,$17,0x1

lh	$5,40($sp)
slt	$2,$5,4
beq	$2,$0,$L197
lw	$9,%got(tc0_table)($28)

sll	$4,$4,2
lh	$2,46($sp)
lh	$16,42($sp)
addu	$5,$4,$5
lh	$11,44($sp)
addiu	$18,$9,%lo(tc0_table)
lw	$25,10664($3)
addu	$16,$4,$16
sw	$3,100($sp)
addu	$11,$4,$11
sw	$8,112($sp)
addu	$4,$4,$2
sw	$10,108($sp)
addu	$5,$5,$18
sw	$12,116($sp)
addu	$2,$4,$18
sw	$13,120($sp)
addu	$16,$16,$18
addu	$11,$11,$18
lbu	$18,0($5)
lbu	$2,0($2)
move	$4,$fp
lbu	$16,0($16)
lbu	$11,0($11)
lw	$5,192($sp)
sb	$2,59($sp)
addiu	$2,$sp,56
sb	$18,56($sp)
sb	$16,57($sp)
sb	$11,58($sp)
jalr	$25
sw	$2,16($sp)

lw	$28,32($sp)
lw	$3,100($sp)
lw	$8,112($sp)
lw	$10,108($sp)
lw	$12,116($sp)
lw	$13,120($sp)
$L196:
andi	$2,$17,0x1
$L412:
bne	$2,$0,$L378
lw	$14,%got(beta_table)($28)

lw	$5,10740($3)
lw	$2,9460($10)
lw	$4,9464($10)
addu	$16,$2,$5
addu	$5,$4,$5
addu	$6,$23,$16
addiu	$14,$14,%lo(beta_table)
lbu	$6,0($6)
addu	$5,$14,$5
beq	$6,$0,$L377
lbu	$7,0($5)

beq	$7,$0,$L377
lh	$11,40($sp)

addu	$20,$20,$22
lw	$15,184($sp)
slt	$2,$11,4
beq	$2,$0,$L201
addu	$4,$15,$20

lw	$25,%got(tc0_table)($28)
sll	$2,$16,2
lh	$5,42($sp)
lh	$16,46($sp)
addu	$11,$2,$11
lh	$18,44($sp)
addiu	$19,$25,%lo(tc0_table)
addu	$5,$2,$5
lw	$25,10680($3)
addu	$11,$11,$19
sw	$3,100($sp)
addu	$18,$2,$18
sw	$8,112($sp)
addu	$2,$2,$16
sw	$10,108($sp)
addu	$5,$5,$19
lbu	$24,0($11)
addu	$18,$18,$19
sw	$12,116($sp)
addu	$2,$2,$19
sw	$13,120($sp)
lbu	$5,0($5)
addiu	$24,$24,1
lbu	$11,0($18)
addiu	$16,$sp,56
lbu	$2,0($2)
addiu	$5,$5,1
sb	$24,56($sp)
addiu	$11,$11,1
sw	$16,16($sp)
addiu	$2,$2,1
sb	$5,57($sp)
sb	$11,58($sp)
sb	$2,59($sp)
jalr	$25
lw	$5,196($sp)

lw	$10,108($sp)
lw	$28,32($sp)
lw	$3,100($sp)
lw	$8,112($sp)
lw	$2,9460($10)
lw	$4,9464($10)
lw	$12,116($sp)
lw	$13,120($sp)
$L200:
lw	$5,10744($3)
lw	$6,%got(beta_table)($28)
addu	$2,$2,$5
addu	$4,$4,$5
addu	$5,$23,$2
addiu	$6,$6,%lo(beta_table)
addu	$4,$6,$4
lbu	$6,0($5)
beq	$6,$0,$L204
lbu	$7,0($4)

beq	$7,$0,$L204
lh	$5,40($sp)

lw	$9,188($sp)
slt	$11,$5,4
beq	$11,$0,$L203
addu	$4,$9,$20

lw	$14,%got(tc0_table)($28)
sll	$2,$2,2
lh	$16,42($sp)
lh	$11,44($sp)
addu	$5,$2,$5
lh	$19,46($sp)
addiu	$18,$14,%lo(tc0_table)
addu	$16,$2,$16
lw	$25,10680($3)
addu	$11,$2,$11
sw	$3,100($sp)
addu	$5,$5,$18
sw	$8,112($sp)
addu	$2,$2,$19
sw	$10,108($sp)
addu	$16,$16,$18
sw	$12,116($sp)
addu	$11,$11,$18
lbu	$5,0($5)
addu	$2,$2,$18
sw	$13,120($sp)
lbu	$16,0($16)
addiu	$18,$sp,56
lbu	$11,0($11)
addiu	$5,$5,1
lbu	$2,0($2)
addiu	$16,$16,1
sw	$18,16($sp)
addiu	$11,$11,1
sb	$5,56($sp)
addiu	$2,$2,1
lw	$5,196($sp)
sb	$16,57($sp)
sb	$11,58($sp)
jalr	$25
sb	$2,59($sp)

lw	$28,32($sp)
lw	$3,100($sp)
lw	$8,112($sp)
lw	$10,108($sp)
lw	$12,116($sp)
lw	$13,120($sp)
$L204:
addiu	$17,$17,1
addiu	$8,$8,8
slt	$2,$17,$12
beq	$2,$0,$L93
addu	$fp,$fp,$21

$L205:
sll	$2,$17,24
and	$2,$2,$13
beq	$2,$0,$L393
lw	$9,64($sp)

$L378:
addu	$20,$20,$22
$L394:
addiu	$17,$17,1
addiu	$8,$8,8
slt	$2,$17,$12
bne	$2,$0,$L205
addu	$fp,$fp,$21

$L93:
lw	$31,164($sp)
$L411:
lw	$fp,160($sp)
lw	$23,156($sp)
lw	$22,152($sp)
lw	$21,148($sp)
lw	$20,144($sp)
lw	$19,140($sp)
lw	$18,136($sp)
lw	$17,132($sp)
lw	$16,128($sp)
j	$31
addiu	$sp,$sp,168

$L189:
and	$2,$17,$14
beq	$2,$0,$L191
lw	$9,80($sp)

move	$4,$0
move	$5,$0
li	$25,1			# 0x1
addiu	$16,$8,12
sw	$4,40($sp)
sw	$5,44($sp)
$L192:
addiu	$11,$8,16
addu	$19,$3,$8
addiu	$18,$sp,40
li	$24,2			# 0x2
move	$9,$21
move	$14,$17
move	$21,$20
move	$17,$11
move	$20,$25
$L195:
lbu	$2,11188($19)
lbu	$4,11196($19)
or	$2,$2,$4
andi	$2,$2,0x00ff
beq	$2,$0,$L193
addiu	$6,$16,-8

sh	$24,0($18)
$L194:
addiu	$16,$16,1
addiu	$19,$19,1
bne	$16,$17,$L195
addiu	$18,$18,2

lh	$4,40($sp)
move	$20,$21
lh	$6,42($sp)
move	$21,$9
lh	$2,44($sp)
lh	$5,46($sp)
addu	$4,$4,$6
addu	$2,$4,$2
addu	$2,$2,$5
bne	$2,$0,$L190
move	$17,$14

b	$L394
addu	$20,$20,$22

$L193:
lw	$15,%got(check_mv)($28)
bne	$20,$0,$L194
addiu	$25,$15,%lo(check_mv)

lw	$7,76($sp)
move	$4,$3
sw	$3,100($sp)
move	$5,$16
sw	$8,112($sp)
sw	$9,104($sp)
sw	$10,108($sp)
sw	$12,116($sp)
sw	$13,120($sp)
sw	$14,92($sp)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
sw	$24,96($sp)

lw	$28,32($sp)
lw	$24,96($sp)
lw	$14,92($sp)
lw	$13,120($sp)
lw	$12,116($sp)
lw	$10,108($sp)
lw	$9,104($sp)
lw	$8,112($sp)
lw	$3,100($sp)
b	$L194
sh	$2,0($18)

$L377:
b	$L200
addu	$20,$20,$22

$L191:
beq	$9,$0,$L230
lw	$14,%got(check_mv)($28)

addiu	$16,$8,12
lw	$7,76($sp)
addiu	$6,$8,4
move	$4,$3
sw	$3,100($sp)
addiu	$25,$14,%lo(check_mv)
sw	$8,112($sp)
sw	$10,108($sp)
move	$5,$16
sw	$12,116($sp)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
sw	$13,120($sp)

li	$25,1			# 0x1
sll	$2,$2,16
lw	$28,32($sp)
lw	$3,100($sp)
sra	$2,$2,16
lw	$8,112($sp)
lw	$10,108($sp)
lw	$12,116($sp)
lw	$13,120($sp)
sh	$2,46($sp)
sh	$2,44($sp)
sh	$2,42($sp)
b	$L192
sh	$2,40($sp)

$L230:
move	$25,$0
b	$L192
addiu	$16,$8,12

$L224:
b	$L137
move	$25,$0

$L142:
lw	$25,10676($13)
lw	$5,192($sp)
sw	$11,100($sp)
sw	$12,116($sp)
sw	$13,120($sp)
jalr	$25
sw	$15,92($sp)

lw	$28,32($sp)
lw	$15,92($sp)
lw	$13,120($sp)
lw	$12,116($sp)
b	$L141
lw	$11,100($sp)

$L197:
lw	$25,10672($3)
move	$4,$fp
lw	$5,192($sp)
sw	$3,100($sp)
sw	$8,112($sp)
sw	$10,108($sp)
sw	$12,116($sp)
jalr	$25
sw	$13,120($sp)

lw	$28,32($sp)
lw	$13,120($sp)
lw	$12,116($sp)
lw	$10,108($sp)
lw	$8,112($sp)
b	$L196
lw	$3,100($sp)

$L391:
addu	$2,$18,$2
lw	$2,8736($2)
andi	$2,$2,0xf
bne	$2,$0,$L413
andi	$9,$21,0x18

$L152:
beq	$3,$0,$L411
lw	$31,164($sp)

li	$15,3			# 0x3
andi	$14,$21,0x18
li	$fp,1			# 0x1
sw	$14,80($sp)
b	$L215
sw	$15,72($sp)

$L153:
andi	$4,$2,0x7
bne	$4,$0,$L216
lw	$8,80($sp)

$L408:
beq	$8,$0,$L229
andi	$3,$3,0x18

beq	$3,$0,$L229
lw	$25,%got(check_mv)($28)

li	$5,12			# 0xc
lw	$7,76($sp)
li	$6,4			# 0x4
addiu	$25,$25,%lo(check_mv)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
move	$4,$18

li	$8,1			# 0x1
sll	$2,$2,16
lw	$28,32($sp)
sra	$2,$2,16
sh	$2,46($sp)
sh	$2,44($sp)
sh	$2,42($sp)
b	$L173
sh	$2,40($sp)

$L216:
andi	$2,$2,0x80
$L407:
bne	$2,$0,$L395
li	$2,196608			# 0x30000

li	$2,262144			# 0x40000
li	$3,262144			# 0x40000
ori	$2,$2,0x4
ori	$3,$3,0x4
sw	$2,40($sp)
b	$L171
sw	$3,44($sp)

$L146:
lw	$25,10692($13)
lw	$5,196($sp)
sw	$11,100($sp)
sw	$12,116($sp)
sw	$13,120($sp)
jalr	$25
sw	$15,92($sp)

lw	$11,100($sp)
lw	$28,32($sp)
lw	$15,92($sp)
lw	$13,120($sp)
lw	$4,9460($11)
lw	$17,9464($11)
b	$L145
lw	$12,116($sp)

$L148:
lw	$25,10692($13)
move	$4,$18
lw	$5,196($sp)
sw	$11,100($sp)
sw	$12,116($sp)
sw	$13,120($sp)
jalr	$25
sw	$15,92($sp)

lw	$28,32($sp)
lw	$15,92($sp)
lw	$13,120($sp)
lw	$12,116($sp)
b	$L149
lw	$11,100($sp)

$L203:
lw	$25,10688($3)
lw	$5,196($sp)
sw	$3,100($sp)
sw	$8,112($sp)
sw	$10,108($sp)
sw	$12,116($sp)
jalr	$25
sw	$13,120($sp)

lw	$28,32($sp)
lw	$13,120($sp)
lw	$12,116($sp)
lw	$10,108($sp)
lw	$8,112($sp)
b	$L204
lw	$3,100($sp)

$L201:
lw	$25,10688($3)
lw	$5,196($sp)
sw	$3,100($sp)
sw	$8,112($sp)
sw	$10,108($sp)
sw	$12,116($sp)
jalr	$25
sw	$13,120($sp)

lw	$10,108($sp)
lw	$28,32($sp)
lw	$13,120($sp)
lw	$12,116($sp)
lw	$2,9460($10)
lw	$4,9464($10)
lw	$8,112($sp)
b	$L200
lw	$3,100($sp)

$L97:
lw	$2,%got(mask_edge_tab.7593)($28)
andi	$3,$3,0x7
addiu	$2,$2,%lo(mask_edge_tab.7593)
sw	$3,72($sp)
sw	$2,84($sp)
addu	$2,$2,$3
lbu	$2,0($2)
sw	$2,80($sp)
li	$2,3			# 0x3
lw	$25,80($sp)
beq	$25,$2,$L396
andi	$15,$21,0x28

li	$23,4			# 0x4
b	$L209
sw	$15,88($sp)

$L115:
lw	$14,88($sp)
beq	$14,$0,$L118
move	$fp,$0

andi	$4,$4,0x28
bne	$4,$0,$L397
lw	$25,%got(check_mv)($28)

move	$fp,$0
$L118:
addiu	$19,$18,11196
lbu	$2,-1($19)
lbu	$5,0($19)
li	$17,12			# 0xc
lw	$8,%got(check_mv)($28)
addiu	$20,$sp,40
li	$3,2			# 0x2
or	$2,$2,$5
andi	$2,$2,0x00ff
li	$22,44			# 0x2c
beq	$2,$0,$L119
addiu	$6,$17,-1

$L398:
sh	$3,0($20)
$L120:
addiu	$17,$17,8
addiu	$19,$19,8
beq	$17,$22,$L117
addiu	$20,$20,2

lbu	$2,-1($19)
lbu	$5,0($19)
or	$2,$2,$5
andi	$2,$2,0x00ff
bne	$2,$0,$L398
addiu	$6,$17,-1

$L119:
bne	$fp,$0,$L120
lw	$7,76($sp)

addiu	$25,$8,%lo(check_mv)
sw	$3,100($sp)
move	$5,$17
sw	$8,112($sp)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
move	$4,$18

lw	$28,32($sp)
lw	$8,112($sp)
lw	$3,100($sp)
b	$L120
sh	$2,0($20)

$L395:
li	$3,196608			# 0x30000
ori	$2,$2,0x3
ori	$3,$3,0x3
sw	$2,40($sp)
b	$L171
sw	$3,44($sp)

$L229:
b	$L173
move	$8,$0

$L174:
bne	$18,$0,$L175
lw	$7,76($sp)

addiu	$25,$3,%lo(check_mv)
sw	$3,100($sp)
move	$5,$16
sw	$9,104($sp)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
move	$4,$20

lw	$28,32($sp)
lw	$9,104($sp)
lw	$3,100($sp)
b	$L175
sh	$2,0($19)

$L213:
lbu	$2,8($2)
lw	$20,10772($18)
lw	$3,10792($18)
bne	$2,$25,$L151
sw	$2,72($sp)

b	$L152
nop

$L227:
b	$L168
li	$21,1			# 0x1

$L155:
lw	$2,12828($18)
bne	$2,$0,$L157
li	$2,16777216			# 0x1000000

and	$3,$3,$2
beq	$3,$0,$L157
sll	$3,$16,1

lw	$2,8732($20)
addu	$2,$2,$3
lhu	$4,0($2)
andi	$2,$4,0x4
bne	$2,$0,$L158
li	$3,2			# 0x2

lbu	$2,11197($18)
lbu	$3,11196($18)
sltu	$2,$0,$2
addiu	$2,$2,1
sltu	$3,$0,$3
sll	$2,$2,16
addiu	$3,$3,1
sra	$2,$2,16
sh	$3,40($sp)
$L207:
andi	$4,$4,0x8
bne	$4,$0,$L159
sh	$2,42($sp)

lbu	$2,11199($18)
lbu	$3,11198($18)
sltu	$2,$0,$2
addiu	$2,$2,1
sltu	$3,$0,$3
sll	$2,$2,16
addiu	$3,$3,1
sra	$2,$2,16
sh	$3,44($sp)
b	$L156
sh	$2,46($sp)

$L111:
li	$2,131072			# 0x20000
lw	$25,%got(filter_mb_mbaff_edgev.isra.0)($28)
li	$17,2			# 0x2
lw	$3,192($sp)
addu	$22,$18,$2
sw	$6,24($sp)
sw	$20,16($sp)
addiu	$25,$25,%lo(filter_mb_mbaff_edgev.isra.0)
sw	$17,20($sp)
sll	$19,$3,1
lw	$4,9460($22)
lw	$5,9464($22)
move	$7,$19
lw	$6,180($sp)
sw	$9,104($sp)
sw	$8,112($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgev.isra.0
1:	jalr	$25
sw	$25,92($sp)

move	$7,$19
lw	$3,196($sp)
lw	$15,192($sp)
lw	$14,180($sp)
sll	$19,$3,1
lw	$4,9460($22)
addiu	$3,$sp,42
lw	$5,9464($22)
lw	$25,92($sp)
addu	$6,$14,$15
sw	$16,24($sp)
sw	$3,16($sp)
sw	$3,100($sp)
jalr	$25
sw	$17,20($sp)

move	$7,$19
lw	$28,32($sp)
lw	$8,112($sp)
lw	$4,9460($22)
lw	$5,9464($22)
lw	$16,%got(filter_mb_mbaff_edgecv.isra.1)($28)
lw	$6,184($sp)
sw	$8,24($sp)
addiu	$16,$16,%lo(filter_mb_mbaff_edgecv.isra.1)
sw	$20,16($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$17,20($sp)

move	$7,$19
lw	$8,184($sp)
move	$25,$16
lw	$14,196($sp)
lw	$3,100($sp)
lw	$4,9460($22)
lw	$5,9464($22)
addu	$6,$8,$14
sw	$17,20($sp)
sw	$3,16($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$23,24($sp)

move	$7,$19
lw	$4,9460($22)
move	$25,$16
lw	$5,9464($22)
lw	$6,188($sp)
sw	$20,16($sp)
sw	$17,20($sp)
.reloc	1f,R_MIPS_JALR,filter_mb_mbaff_edgecv.isra.1
1:	jalr	$25
sw	$fp,24($sp)

move	$7,$19
lw	$3,100($sp)
lw	$8,188($sp)
lw	$14,196($sp)
lw	$4,9460($22)
lw	$5,9464($22)
addu	$6,$8,$14
b	$L373
sw	$3,16($sp)

$L98:
lw	$5,-5248($2)
lw	$14,176($sp)
li	$12,131072			# 0x20000
lw	$7,%got(offset.7671)($28)
move	$3,$0
sll	$2,$5,1
andi	$11,$14,0x1
addu	$2,$2,$11
addiu	$7,$7,%lo(offset.7671)
sll	$2,$2,3
li	$8,2			# 0x2
addu	$7,$7,$2
li	$10,1			# 0x1
li	$13,16777216			# 0x1000000
addu	$12,$18,$12
li	$6,8			# 0x8
li	$9,4			# 0x4
move	$4,$20
beq	$5,$0,$L100
andi	$11,$11,0x00ff

sra	$2,$3,2
$L415:
sll	$2,$2,2
addu	$2,$18,$2
lw	$14,10800($2)
lw	$15,10780($2)
andi	$2,$14,0x7
beq	$2,$0,$L414
sra	$2,$3,1

$L400:
sh	$9,0($4)
$L103:
addiu	$3,$3,1
beq	$3,$6,$L99
addiu	$4,$4,2

bne	$5,$0,$L415
sra	$2,$3,2

$L100:
andi	$2,$3,0x1
sll	$2,$2,2
addu	$2,$18,$2
lw	$14,10800($2)
lw	$15,10780($2)
andi	$2,$14,0x7
bne	$2,$0,$L400
sra	$2,$3,1

$L414:
lw	$17,12828($18)
sll	$2,$2,3
addu	$2,$18,$2
bne	$17,$0,$L104
lbu	$16,11196($2)

and	$14,$14,$13
beq	$14,$0,$L104
nop

lw	$2,8732($12)
sll	$15,$15,1
addu	$2,$2,$15
bne	$5,$0,$L401
lhu	$2,0($2)

move	$14,$11
$L106:
move	$15,$8
movn	$15,$6,$14
b	$L108
and	$2,$15,$2

$L104:
addu	$2,$7,$3
lw	$14,11232($18)
sll	$15,$15,5
lbu	$2,0($2)
addu	$15,$14,$15
addu	$14,$15,$2
lbu	$2,0($14)
$L108:
or	$2,$2,$16
move	$25,$10
movn	$25,$8,$2
b	$L103
sh	$25,0($4)

$L157:
lw	$10,11232($18)
sll	$9,$16,5
addiu	$5,$sp,40
move	$3,$0
li	$7,2			# 0x2
li	$6,1			# 0x1
li	$4,4			# 0x4
$L161:
addu	$8,$3,$9
addu	$2,$18,$3
addu	$8,$10,$8
addiu	$5,$5,2
lbu	$12,11196($2)
addiu	$3,$3,1
lbu	$2,28($8)
move	$8,$6
or	$2,$2,$12
andi	$2,$2,0x00ff
movn	$8,$7,$2
bne	$3,$4,$L161
sh	$8,-2($5)

b	$L403
lw	$2,2172($18)

$L401:
srl	$14,$3,1
b	$L106
andi	$14,$14,0x1

$L388:
slt	$4,$20,4
beq	$4,$0,$L128
lw	$9,%got(tc0_table)($28)

addiu	$5,$sp,56
sll	$3,$3,2
lw	$25,10684($18)
sw	$2,92($sp)
addiu	$9,$9,%lo(tc0_table)
sw	$5,16($sp)
addu	$8,$3,$20
sw	$11,100($sp)
addu	$5,$3,$11
addu	$4,$3,$fp
addu	$3,$3,$2
addu	$4,$4,$9
addu	$5,$5,$9
addu	$8,$8,$9
addu	$3,$3,$9
lbu	$10,0($5)
lbu	$9,0($8)
lbu	$3,0($3)
lbu	$8,0($4)
addiu	$10,$10,1
addiu	$9,$9,1
lw	$4,184($sp)
addiu	$3,$3,1
lw	$5,196($sp)
addiu	$8,$8,1
sb	$10,58($sp)
sb	$9,56($sp)
sb	$3,59($sp)
jalr	$25
sb	$8,57($sp)

li	$3,131072			# 0x20000
lw	$28,32($sp)
addu	$3,$18,$3
lw	$2,92($sp)
lw	$11,100($sp)
lw	$4,9460($3)
b	$L127
lw	$5,9464($3)

$L397:
li	$5,12			# 0xc
lw	$7,76($sp)
li	$6,11			# 0xb
move	$4,$18
addiu	$25,$25,%lo(check_mv)
.reloc	1f,R_MIPS_JALR,check_mv
1:	jalr	$25
li	$fp,1			# 0x1

sll	$2,$2,16
lw	$28,32($sp)
sra	$2,$2,16
sh	$2,46($sp)
sh	$2,44($sp)
sh	$2,42($sp)
b	$L118
sh	$2,40($sp)

$L386:
lw	$3,10384($18)
bne	$3,$2,$L416
li	$2,262144			# 0x40000

b	$L404
lh	$20,40($sp)

$L387:
slt	$4,$20,4
beq	$4,$0,$L126
lw	$9,%got(tc0_table)($28)

addiu	$5,$sp,56
sll	$3,$3,2
lw	$25,10668($18)
sw	$2,92($sp)
addiu	$9,$9,%lo(tc0_table)
sw	$5,16($sp)
addu	$8,$3,$20
sw	$11,100($sp)
addu	$5,$3,$11
addu	$4,$3,$fp
addu	$3,$3,$2
addu	$5,$5,$9
addu	$8,$8,$9
addu	$4,$4,$9
addu	$3,$3,$9
lbu	$10,0($5)
lbu	$8,0($8)
lbu	$9,0($4)
lbu	$3,0($3)
lw	$4,180($sp)
lw	$5,192($sp)
sb	$8,56($sp)
sb	$3,59($sp)
sb	$9,57($sp)
jalr	$25
sb	$10,58($sp)

lw	$3,2172($18)
lw	$28,32($sp)
lw	$4,9460($22)
addu	$3,$3,$16
lw	$5,9464($22)
lw	$2,92($sp)
lw	$11,100($sp)
b	$L125
lb	$8,0($3)

$L186:
lw	$25,10688($18)
lw	$4,188($sp)
jalr	$25
lw	$5,196($sp)

b	$L169
lw	$28,32($sp)

$L181:
lw	$25,10672($18)
lw	$5,192($sp)
sw	$2,92($sp)
jalr	$25
sw	$11,100($sp)

lw	$3,2172($18)
lw	$28,32($sp)
lw	$4,9460($16)
addu	$3,$3,$20
lw	$5,9464($16)
lw	$11,100($sp)
lw	$2,92($sp)
b	$L180
lb	$8,0($3)

$L183:
lw	$25,10688($18)
lw	$5,196($sp)
sw	$2,92($sp)
jalr	$25
sw	$11,100($sp)

li	$3,131072			# 0x20000
lw	$28,32($sp)
addu	$3,$18,$3
lw	$11,100($sp)
lw	$2,92($sp)
lw	$4,9460($3)
b	$L182
lw	$5,9464($3)

$L131:
lw	$25,10692($18)
lw	$4,188($sp)
jalr	$25
lw	$5,196($sp)

b	$L124
lw	$28,32($sp)

$L128:
lw	$25,10692($18)
lw	$4,184($sp)
lw	$5,196($sp)
sw	$2,92($sp)
jalr	$25
sw	$11,100($sp)

li	$3,131072			# 0x20000
lw	$28,32($sp)
addu	$3,$18,$3
lw	$11,100($sp)
lw	$2,92($sp)
lw	$4,9460($3)
b	$L127
lw	$5,9464($3)

$L165:
lw	$25,10688($18)
lw	$5,64($sp)
jalr	$25
sw	$11,100($sp)

b	$L376
lw	$28,32($sp)

$L126:
lw	$25,10676($18)
lw	$4,180($sp)
lw	$5,192($sp)
sw	$2,92($sp)
jalr	$25
sw	$11,100($sp)

lw	$3,2172($18)
lw	$28,32($sp)
lw	$4,9460($22)
addu	$3,$3,$16
lw	$5,9464($22)
lw	$11,100($sp)
lw	$2,92($sp)
b	$L125
lb	$8,0($3)

$L167:
lw	$25,10688($18)
lw	$5,64($sp)
jalr	$25
sw	$11,100($sp)

lw	$28,32($sp)
b	$L166
lw	$11,100($sp)

$L163:
lw	$25,10672($18)
lw	$5,88($sp)
jalr	$25
sw	$11,100($sp)

b	$L375
lw	$28,32($sp)

$L159:
li	$2,2			# 0x2
li	$3,2			# 0x2
sh	$2,46($sp)
b	$L156
sh	$3,44($sp)

$L158:
li	$2,2			# 0x2
b	$L207
sh	$3,40($sp)

.set	macro
.set	reorder
.end	ff_h264_filter_mb
.size	ff_h264_filter_mb, .-ff_h264_filter_mb
.rdata
.align	2
$LC0:
.half	4
.half	4
.half	4
.half	4
.align	2
$LC1:
.half	3
.half	3
.half	3
.half	3
.section	.text.ff_h264_filter_mb_fast,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_filter_mb_fast
.set	nomips16
.set	nomicromips
.ent	ff_h264_filter_mb_fast
.type	ff_h264_filter_mb_fast, @function
ff_h264_filter_mb_fast:
.frame	$sp,200,$31		# vars= 112, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-200
li	$2,131072			# 0x20000
sw	$16,160($sp)
move	$16,$4
addu	$2,$16,$2
.cprestore	40
sw	$fp,192($sp)
move	$fp,$7
sw	$31,196($sp)
sw	$23,188($sp)
sw	$22,184($sp)
sw	$21,180($sp)
sw	$20,176($sp)
sw	$19,172($sp)
sw	$18,168($sp)
sw	$17,164($sp)
lw	$4,10792($4)
beq	$4,$0,$L418
lw	$3,9448($2)

lw	$25,10696($16)
beq	$25,$0,$L418
nop

lw	$4,13628($16)
bne	$4,$0,$L418
sll	$7,$3,2

lw	$4,2172($16)
lw	$8,10772($16)
lw	$6,2192($16)
addu	$5,$4,$3
lw	$3,9460($2)
addu	$4,$4,$8
lw	$9,10800($16)
addu	$7,$6,$7
lb	$23,0($5)
li	$2,67			# 0x43
lb	$6,-1($5)
lb	$5,0($4)
subu	$2,$2,$3
andi	$8,$23,0xff
lw	$10,0($7)
andi	$7,$6,0xff
andi	$4,$5,0xff
addu	$4,$16,$4
addu	$8,$16,$8
addu	$7,$16,$7
lbu	$20,13116($4)
addu	$11,$23,$6
lbu	$8,13116($8)
addu	$22,$23,$5
lbu	$21,13116($7)
addiu	$11,$11,1
addiu	$22,$22,1
addu	$20,$8,$20
addu	$21,$8,$21
addiu	$20,$20,1
addiu	$21,$21,1
slt	$4,$2,$23
sra	$11,$11,1
sra	$22,$22,1
sra	$21,$21,1
bne	$4,$0,$L421
sra	$20,$20,1

slt	$4,$2,$11
bne	$4,$0,$L421
slt	$4,$2,$22

bne	$4,$0,$L421
slt	$4,$2,$8

bne	$4,$0,$L421
slt	$4,$2,$21

bne	$4,$0,$L421
slt	$2,$2,$20

beq	$2,$0,$L877
lw	$31,196($sp)

$L421:
andi	$2,$10,0x7
beq	$2,$0,$L422
li	$2,131072			# 0x20000

lw	$6,%got($LC0)($28)
lw	$5,%got($LC1)($28)
lw	$15,10384($16)
addiu	$4,$6,%lo($LC0)
addiu	$2,$5,%lo($LC1)
lwl	$14,3($4)
lwl	$13,7($4)
lwl	$12,3($2)
lwl	$7,7($2)
lwr	$14,%lo($LC0)($6)
move	$6,$13
lwr	$6,4($4)
move	$4,$12
lwr	$7,4($2)
li	$2,3			# 0x3
lwr	$4,%lo($LC1)($5)
sw	$14,120($sp)
sw	$6,124($sp)
sw	$7,52($sp)
beq	$15,$2,$L865
sw	$4,48($sp)

bne	$9,$0,$L424
addiu	$19,$sp,48

$L875:
li	$2,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$2,$16,$2
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$2,9464($2)
$L425:
li	$4,16777216			# 0x1000000
addu	$5,$3,$23
and	$10,$10,$4
addu	$4,$2,$23
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$10,$0,$L427
lbu	$7,0($4)

beq	$6,$0,$L428
nop

bne	$7,$0,$L866
nop

$L428:
addu	$4,$3,$22
addu	$22,$2,$22
addu	$5,$17,$4
addu	$22,$18,$22
lbu	$6,0($5)
beq	$6,$0,$L430
lbu	$7,0($22)

bne	$7,$0,$L867
nop

$L430:
addu	$5,$3,$23
addu	$23,$2,$23
addu	$4,$17,$5
addu	$23,$18,$23
lbu	$6,0($4)
beq	$6,$0,$L432
lbu	$7,0($23)

beq	$7,$0,$L432
nop

lw	$2,224($sp)
lh	$3,48($sp)
sll	$4,$2,3
slt	$2,$3,4
beq	$2,$0,$L433
addu	$4,$fp,$4

lh	$2,54($sp)
sll	$5,$5,2
lh	$11,50($sp)
lh	$10,52($sp)
addu	$3,$5,$3
lw	$25,10664($16)
addu	$11,$5,$11
addu	$10,$5,$10
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$11,$11,$2
addu	$10,$10,$2
$L864:
addu	$2,$2,$5
lbu	$12,0($3)
lbu	$11,0($11)
lbu	$3,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$12,128($sp)
sb	$11,129($sp)
sb	$3,130($sp)
sb	$2,131($sp)
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

lw	$28,40($sp)
$L859:
li	$2,131072			# 0x20000
lw	$8,152($sp)
lw	$9,148($sp)
addu	$2,$16,$2
lw	$3,9460($2)
lw	$2,9464($2)
$L432:
beq	$9,$0,$L880
addu	$5,$3,$8

addu	$5,$3,$21
addu	$4,$2,$21
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L448
lbu	$7,0($4)

beq	$7,$0,$L448
nop

lh	$3,120($sp)
slt	$2,$3,4
beq	$2,$0,$L449
lw	$4,216($sp)

lh	$2,126($sp)
sll	$5,$5,2
lh	$10,122($sp)
lh	$9,124($sp)
addu	$3,$5,$3
lw	$25,10684($16)
addu	$10,$5,$10
addu	$9,$5,$9
sw	$8,152($sp)
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,128($sp)
sb	$3,130($sp)
sb	$9,129($sp)
jalr	$25
sb	$2,131($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L450:
li	$22,131072			# 0x20000
addu	$22,$16,$22
lw	$3,9460($22)
lw	$2,9464($22)
addu	$4,$21,$3
addu	$21,$21,$2
addu	$5,$17,$4
addu	$21,$18,$21
lbu	$6,0($5)
beq	$6,$0,$L448
lbu	$7,0($21)

beq	$7,$0,$L880
addu	$5,$3,$8

lh	$3,120($sp)
slt	$2,$3,4
beq	$2,$0,$L451
lw	$5,228($sp)

lw	$5,%got(tc0_table)($28)
sll	$4,$4,2
lh	$10,122($sp)
lh	$9,124($sp)
addu	$3,$4,$3
lh	$2,126($sp)
addiu	$5,$5,%lo(tc0_table)
addu	$10,$4,$10
lw	$25,10684($16)
addu	$9,$4,$9
sw	$8,152($sp)
addu	$2,$4,$2
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
addiu	$4,$sp,128
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
sw	$4,16($sp)
addiu	$2,$2,1
sb	$11,128($sp)
sb	$3,130($sp)
sb	$9,129($sp)
sb	$2,131($sp)
jalr	$25
lw	$4,220($sp)

lw	$28,40($sp)
lw	$3,9460($22)
lw	$2,9464($22)
lw	$8,152($sp)
$L448:
addu	$5,$3,$8
$L880:
addu	$4,$2,$8
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L452
lbu	$7,0($4)

beq	$7,$0,$L452
nop

lw	$2,216($sp)
lh	$3,48($sp)
addiu	$4,$2,4
slt	$2,$3,4
bne	$2,$0,$L868
lh	$2,54($sp)

lw	$25,10692($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L454:
li	$21,131072			# 0x20000
addu	$21,$16,$21
lw	$3,9460($21)
lw	$2,9464($21)
addu	$5,$8,$3
addu	$4,$8,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L452
lbu	$7,0($4)

beq	$7,$0,$L452
nop

lw	$2,220($sp)
lh	$3,48($sp)
addiu	$4,$2,4
slt	$2,$3,4
beq	$2,$0,$L455
sll	$5,$5,2

lh	$2,54($sp)
lh	$10,50($sp)
lh	$9,52($sp)
addu	$3,$5,$3
lw	$25,10684($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,128($sp)
sb	$3,130($sp)
sb	$9,129($sp)
jalr	$25
sb	$2,131($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
lw	$8,152($sp)
$L452:
addu	$5,$3,$20
addu	$4,$2,$20
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L456
lbu	$7,0($4)

beq	$7,$0,$L881
addu	$9,$3,$8

lh	$3,0($19)
slt	$2,$3,4
bne	$2,$0,$L869
sll	$5,$5,2

lw	$25,10688($16)
lw	$4,216($sp)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
$L860:
li	$2,131072			# 0x20000
lw	$8,152($sp)
addu	$2,$16,$2
lw	$3,9460($2)
lw	$2,9464($2)
$L456:
addu	$9,$3,$8
$L881:
lw	$6,228($sp)
addu	$5,$2,$8
addu	$4,$17,$9
addu	$5,$18,$5
sll	$21,$6,2
lbu	$6,0($4)
beq	$6,$0,$L458
lbu	$7,0($5)

beq	$7,$0,$L882
addu	$5,$3,$20

lw	$3,216($sp)
lh	$2,48($sp)
addu	$4,$3,$21
slt	$3,$2,4
bne	$3,$0,$L870
lh	$3,54($sp)

lw	$25,10688($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
$L861:
li	$2,131072			# 0x20000
lw	$8,152($sp)
addu	$2,$16,$2
lw	$3,9460($2)
lw	$2,9464($2)
$L458:
addu	$5,$3,$20
$L882:
addu	$20,$2,$20
addu	$4,$17,$5
addu	$20,$18,$20
lbu	$6,0($4)
beq	$6,$0,$L460
lbu	$7,0($20)

beq	$7,$0,$L883
addu	$3,$3,$8

lh	$3,0($19)
slt	$2,$3,4
bne	$2,$0,$L871
sll	$5,$5,2

lw	$25,10688($16)
lw	$4,220($sp)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
$L862:
li	$2,131072			# 0x20000
lw	$8,152($sp)
addu	$2,$16,$2
lw	$3,9460($2)
lw	$2,9464($2)
$L460:
addu	$3,$3,$8
$L883:
addu	$2,$2,$8
addu	$17,$17,$3
addu	$18,$18,$2
lbu	$6,0($17)
beq	$6,$0,$L417
lbu	$7,0($18)

beq	$7,$0,$L417
lw	$5,220($sp)

lh	$2,48($sp)
addu	$4,$5,$21
slt	$5,$2,4
beq	$5,$0,$L510
lh	$5,54($sp)

sll	$3,$3,2
lh	$9,50($sp)
lh	$8,52($sp)
addu	$2,$3,$2
lw	$25,10680($16)
addu	$9,$3,$9
addu	$8,$3,$8
addu	$3,$3,$5
addiu	$5,$sp,128
sw	$5,16($sp)
lw	$5,%got(tc0_table)($28)
addiu	$5,$5,%lo(tc0_table)
addu	$2,$2,$5
addu	$3,$3,$5
addu	$9,$9,$5
addu	$8,$8,$5
lbu	$10,0($2)
lbu	$2,0($3)
lbu	$9,0($9)
lbu	$8,0($8)
addiu	$10,$10,1
addiu	$2,$2,1
lw	$5,228($sp)
addiu	$9,$9,1
addiu	$3,$8,1
sb	$10,128($sp)
sb	$2,131($sp)
sb	$9,129($sp)
jalr	$25
sb	$3,130($sp)

b	$L877
lw	$31,196($sp)

$L418:
lw	$2,216($sp)
move	$4,$16
lw	$3,220($sp)
move	$7,$fp
lw	$8,224($sp)
lw	$25,%call16(ff_h264_filter_mb)($28)
sw	$2,16($sp)
sw	$3,20($sp)
sw	$8,24($sp)
lw	$2,228($sp)
.reloc	1f,R_MIPS_JALR,ff_h264_filter_mb
1:	jalr	$25
sw	$2,28($sp)

$L417:
lw	$31,196($sp)
$L877:
lw	$fp,192($sp)
lw	$23,188($sp)
lw	$22,184($sp)
lw	$21,180($sp)
lw	$20,176($sp)
lw	$19,172($sp)
lw	$18,168($sp)
lw	$17,164($sp)
lw	$16,160($sp)
j	$31
addiu	$sp,$sp,200

$L422:
li	$3,16777216			# 0x1000000
addu	$2,$16,$2
and	$3,$10,$3
addiu	$19,$sp,48
beq	$3,$0,$L465
lw	$4,8736($2)

li	$2,7			# 0x7
andi	$5,$4,0x7
beq	$5,$2,$L872
li	$5,131072			# 0x20000

$L465:
sll	$6,$10,2
lw	$5,10384($16)
sll	$2,$9,2
addu	$6,$6,$10
sra	$12,$10,3
sra	$6,$6,5
andi	$4,$4,0xf
andi	$7,$6,0x1
sll	$6,$7,1
addu	$2,$2,$9
addu	$6,$6,$7
sra	$7,$10,4
sltu	$4,$4,1
or	$6,$6,$7
li	$7,65536			# 0x10000
and	$4,$4,$12
sw	$6,32($sp)
addu	$7,$16,$7
sll	$12,$4,1
sra	$2,$2,5
sra	$6,$6,1
lw	$7,6632($7)
addu	$12,$12,$4
sw	$3,136($sp)
andi	$2,$2,0x1
sw	$8,152($sp)
and	$2,$2,$6
sw	$9,148($sp)
sll	$4,$2,1
sw	$11,140($sp)
subu	$12,$0,$12
sra	$10,$10,24
xori	$6,$7,0x2
addiu	$12,$12,4
addu	$2,$4,$2
addiu	$10,$10,1
xori	$5,$5,0x3
sw	$12,20($sp)
sltu	$6,$6,1
sw	$12,144($sp)
sltu	$4,$0,$5
sw	$10,24($sp)
sw	$6,16($sp)
addiu	$5,$16,11184
addiu	$6,$16,11568
sw	$4,36($sp)
addiu	$7,$16,11248
sw	$2,28($sp)
jalr	$25
move	$4,$19

lw	$28,40($sp)
lw	$12,144($sp)
lw	$11,140($sp)
lw	$9,148($sp)
lw	$8,152($sp)
lw	$3,136($sp)
$L466:
andi	$2,$9,0x7
beq	$2,$0,$L467
li	$4,262144			# 0x40000

li	$5,262144			# 0x40000
ori	$4,$4,0x4
ori	$5,$5,0x4
sw	$4,0($19)
sw	$5,4($19)
$L467:
lw	$2,10792($16)
andi	$2,$2,0x7
beq	$2,$0,$L468
li	$2,3			# 0x3

lw	$4,10384($16)
beq	$4,$2,$L873
li	$4,196608			# 0x30000

li	$5,196608			# 0x30000
ori	$4,$4,0x3
ori	$5,$5,0x3
sw	$4,32($19)
sw	$5,36($19)
$L468:
beq	$9,$0,$L884
li	$2,1			# 0x1

lw	$2,0($19)
lw	$4,4($19)
or	$2,$2,$4
beq	$2,$0,$L884
li	$2,1			# 0x1

li	$9,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$9,$16,$9
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$4,9460($9)
lw	$2,9464($9)
addu	$5,$11,$4
addu	$11,$11,$2
addu	$6,$17,$5
addu	$11,$18,$11
lbu	$6,0($6)
beq	$6,$0,$L474
lbu	$7,0($11)

beq	$7,$0,$L885
addu	$4,$4,$21

lh	$2,0($19)
slt	$4,$2,4
beq	$4,$0,$L475
move	$4,$fp

sll	$5,$5,2
lh	$4,6($19)
lh	$11,2($19)
addu	$13,$5,$2
lh	$10,4($19)
addiu	$2,$sp,120
lw	$25,10668($16)
addu	$11,$5,$11
sw	$3,136($sp)
addu	$10,$5,$10
sw	$8,152($sp)
sw	$2,16($sp)
addu	$5,$5,$4
lw	$2,%got(tc0_table)($28)
move	$4,$fp
sw	$9,148($sp)
sw	$12,144($sp)
addiu	$2,$2,%lo(tc0_table)
addu	$13,$13,$2
addu	$11,$11,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$13,0($13)
lbu	$11,0($11)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$13,120($sp)
sb	$11,121($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,122($sp)

lw	$9,148($sp)
lw	$28,40($sp)
lw	$3,136($sp)
lw	$8,152($sp)
lw	$4,9460($9)
lw	$2,9464($9)
lw	$12,144($sp)
$L474:
addu	$4,$4,$21
$L885:
addu	$2,$2,$21
addu	$5,$17,$4
addu	$2,$18,$2
lbu	$6,0($5)
beq	$6,$0,$L471
lbu	$7,0($2)

beq	$7,$0,$L884
li	$2,1			# 0x1

lh	$5,0($19)
slt	$2,$5,4
beq	$2,$0,$L476
sll	$4,$4,2

lh	$10,2($19)
lh	$9,4($19)
lh	$2,6($19)
addu	$11,$4,$5
lw	$5,%got(tc0_table)($28)
addu	$10,$4,$10
addu	$9,$4,$9
lw	$25,10684($16)
addu	$2,$4,$2
sw	$3,136($sp)
addiu	$5,$5,%lo(tc0_table)
sw	$8,152($sp)
addiu	$4,$sp,120
sw	$12,144($sp)
addu	$11,$11,$5
addu	$10,$10,$5
addu	$9,$9,$5
sw	$4,16($sp)
addu	$2,$2,$5
lbu	$11,0($11)
lbu	$10,0($10)
lbu	$9,0($9)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$10,$10,1
lw	$4,216($sp)
addiu	$9,$9,1
lw	$5,228($sp)
addiu	$2,$2,1
sb	$11,120($sp)
sb	$10,121($sp)
sb	$9,122($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$3,136($sp)
lw	$8,152($sp)
lw	$12,144($sp)
$L477:
li	$4,131072			# 0x20000
addu	$4,$16,$4
lw	$2,9460($4)
lw	$4,9464($4)
addu	$2,$21,$2
addu	$21,$21,$4
addu	$17,$17,$2
addu	$18,$18,$21
lbu	$6,0($17)
beq	$6,$0,$L471
lbu	$7,0($18)

beq	$7,$0,$L471
nop

lh	$4,0($19)
slt	$5,$4,4
beq	$5,$0,$L478
lw	$5,228($sp)

lh	$9,6($19)
sll	$2,$2,2
lh	$11,2($19)
lh	$10,4($19)
addu	$5,$2,$4
addiu	$4,$sp,120
lw	$25,10684($16)
addu	$11,$2,$11
sw	$3,136($sp)
addu	$10,$2,$10
sw	$8,152($sp)
addu	$2,$2,$9
lw	$9,%got(tc0_table)($28)
sw	$4,16($sp)
sw	$12,144($sp)
addiu	$9,$9,%lo(tc0_table)
lw	$4,220($sp)
addu	$5,$5,$9
addu	$11,$11,$9
addu	$10,$10,$9
addu	$2,$2,$9
lbu	$13,0($5)
lbu	$11,0($11)
lbu	$9,0($10)
lbu	$2,0($2)
addiu	$13,$13,1
addiu	$10,$11,1
lw	$5,228($sp)
addiu	$9,$9,1
addiu	$2,$2,1
sb	$13,120($sp)
sb	$10,121($sp)
sb	$9,122($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$3,136($sp)
lw	$8,152($sp)
lw	$12,144($sp)
$L471:
li	$2,1			# 0x1
$L884:
beq	$12,$2,$L874
nop

beq	$3,$0,$L488
nop

lw	$2,16($19)
lw	$3,20($19)
or	$2,$2,$3
beq	$2,$0,$L490
nop

li	$21,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$21,$16,$21
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($21)
lw	$2,9464($21)
addu	$5,$23,$3
addu	$4,$23,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L491
lbu	$7,0($4)

beq	$7,$0,$L886
addu	$3,$3,$8

lh	$3,16($19)
slt	$2,$3,4
beq	$2,$0,$L492
addiu	$4,$fp,8

lh	$2,22($19)
sll	$5,$5,2
lh	$10,18($19)
lh	$9,20($19)
addu	$3,$5,$3
lw	$25,10668($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$11,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,121($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
lw	$8,152($sp)
$L491:
addu	$3,$3,$8
$L886:
addu	$2,$2,$8
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L490
lbu	$7,0($2)

beq	$7,$0,$L490
lw	$2,216($sp)

lh	$5,16($19)
addiu	$4,$2,4
slt	$2,$5,4
beq	$2,$0,$L494
sll	$3,$3,2

lh	$2,22($19)
lh	$10,18($19)
lh	$9,20($19)
addu	$5,$3,$5
lw	$25,10684($16)
addu	$10,$3,$10
sw	$8,152($sp)
addu	$9,$3,$9
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$11,0($5)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L495:
li	$3,131072			# 0x20000
addu	$3,$16,$3
lw	$2,9460($3)
lw	$3,9464($3)
addu	$2,$8,$2
addu	$3,$8,$3
addu	$17,$17,$2
addu	$18,$18,$3
lbu	$6,0($17)
beq	$6,$0,$L490
lbu	$7,0($18)

beq	$7,$0,$L490
nop

lw	$5,220($sp)
lh	$3,16($19)
addiu	$4,$5,4
slt	$5,$3,4
beq	$5,$0,$L496
lw	$5,228($sp)

lh	$5,22($19)
sll	$2,$2,2
lh	$10,18($19)
lh	$9,20($19)
addu	$3,$2,$3
lw	$25,10684($16)
addu	$10,$2,$10
sw	$8,152($sp)
addu	$9,$2,$9
addu	$2,$2,$5
addiu	$5,$sp,120
sw	$5,16($sp)
lw	$5,%got(tc0_table)($28)
addiu	$5,$5,%lo(tc0_table)
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L490:
lw	$2,32($19)
lw	$3,36($19)
or	$2,$2,$3
beq	$2,$0,$L498
lw	$17,%got(alpha_table)($28)

li	$21,131072			# 0x20000
lw	$18,%got(beta_table)($28)
addu	$21,$16,$21
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($21)
lw	$2,9464($21)
addu	$4,$22,$3
addu	$22,$22,$2
addu	$5,$17,$4
addu	$22,$18,$22
lbu	$6,0($5)
beq	$6,$0,$L499
lbu	$7,0($22)

beq	$7,$0,$L887
addu	$3,$3,$20

lh	$3,32($19)
slt	$2,$3,4
beq	$2,$0,$L500
lw	$5,224($sp)

lh	$10,34($19)
sll	$4,$4,2
lh	$9,36($19)
lh	$2,38($19)
addu	$3,$4,$3
lw	$5,%got(tc0_table)($28)
addu	$10,$4,$10
addu	$9,$4,$9
lw	$25,10664($16)
addu	$2,$4,$2
sw	$8,152($sp)
addiu	$5,$5,%lo(tc0_table)
addiu	$4,$sp,120
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
sw	$4,16($sp)
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
move	$4,$fp
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$11,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,121($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
lw	$8,152($sp)
$L499:
addu	$3,$3,$20
$L887:
addu	$2,$2,$20
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L498
lbu	$7,0($2)

beq	$7,$0,$L498
nop

lh	$4,32($19)
slt	$2,$4,4
beq	$2,$0,$L502
lw	$5,228($sp)

lh	$2,38($19)
sll	$3,$3,2
lh	$10,34($19)
lh	$9,36($19)
addu	$5,$3,$4
lw	$25,10680($16)
addu	$10,$3,$10
lw	$4,216($sp)
addu	$9,$3,$9
sw	$8,152($sp)
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$11,0($5)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L503:
li	$3,131072			# 0x20000
addu	$3,$16,$3
lw	$2,9460($3)
lw	$3,9464($3)
addu	$2,$20,$2
addu	$20,$20,$3
addu	$17,$17,$2
addu	$18,$18,$20
lbu	$6,0($17)
beq	$6,$0,$L498
lbu	$7,0($18)

beq	$7,$0,$L498
nop

lh	$3,32($19)
slt	$4,$3,4
beq	$4,$0,$L504
lw	$5,228($sp)

lh	$4,38($19)
sll	$2,$2,2
lh	$10,34($19)
lh	$9,36($19)
addu	$3,$2,$3
lw	$5,%got(tc0_table)($28)
addu	$10,$2,$10
lw	$25,10680($16)
addu	$9,$2,$9
sw	$8,152($sp)
addiu	$5,$5,%lo(tc0_table)
addu	$2,$2,$4
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
addiu	$4,$sp,120
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
sw	$4,16($sp)
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
sb	$2,123($sp)
jalr	$25
lw	$4,220($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L498:
lw	$2,48($19)
lw	$3,52($19)
or	$2,$2,$3
beq	$2,$0,$L417
li	$2,131072			# 0x20000

lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$20,$16,$2
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($20)
lw	$2,9464($20)
addu	$5,$23,$3
addu	$23,$23,$2
addu	$4,$17,$5
addu	$23,$18,$23
lbu	$6,0($4)
beq	$6,$0,$L506
lbu	$7,0($23)

beq	$7,$0,$L888
addu	$3,$3,$8

lw	$4,224($sp)
lh	$3,48($19)
sll	$2,$4,3
addu	$4,$fp,$2
slt	$2,$3,4
beq	$2,$0,$L507
nop

lh	$2,54($19)
sll	$5,$5,2
lh	$10,50($19)
lh	$9,52($19)
addu	$3,$5,$3
lw	$25,10664($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$11,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,121($sp)

lw	$28,40($sp)
lw	$3,9460($20)
lw	$2,9464($20)
lw	$8,152($sp)
$L506:
addu	$3,$3,$8
$L888:
addu	$2,$2,$8
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L417
lbu	$7,0($2)

beq	$7,$0,$L877
lw	$31,196($sp)

lw	$2,228($sp)
lh	$5,48($19)
sll	$20,$2,2
lw	$2,216($sp)
addu	$4,$2,$20
slt	$2,$5,4
beq	$2,$0,$L508
sll	$3,$3,2

lh	$2,54($19)
lh	$10,50($19)
lh	$9,52($19)
addu	$5,$3,$5
lw	$25,10680($16)
addu	$10,$3,$10
sw	$8,152($sp)
addu	$9,$3,$9
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$11,0($5)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L509:
li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$3,9460($2)
lw	$2,9464($2)
addu	$3,$8,$3
addu	$2,$8,$2
addu	$17,$17,$3
addu	$18,$18,$2
lbu	$6,0($17)
beq	$6,$0,$L417
lbu	$7,0($18)

beq	$7,$0,$L877
lw	$31,196($sp)

lh	$5,48($19)
lw	$8,220($sp)
slt	$2,$5,4
beq	$2,$0,$L510
addu	$4,$8,$20

lh	$2,54($19)
sll	$3,$3,2
lh	$9,50($19)
lh	$8,52($19)
addu	$5,$3,$5
lw	$25,10680($16)
addu	$9,$3,$9
addu	$8,$3,$8
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$8,$8,$2
lbu	$10,0($5)
addu	$2,$3,$2
$L863:
lbu	$3,0($8)
addiu	$10,$10,1
lbu	$9,0($9)
lbu	$2,0($2)
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$8,$9,1
sb	$10,120($sp)
addiu	$2,$2,1
sb	$3,122($sp)
sb	$8,121($sp)
jalr	$25
sb	$2,123($sp)

b	$L877
lw	$31,196($sp)

$L865:
beq	$9,$0,$L875
addiu	$19,$sp,120

$L424:
li	$12,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
addu	$4,$11,$3
lw	$18,%got(beta_table)($28)
addu	$12,$16,$12
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$2,9464($12)
addu	$5,$17,$4
addu	$11,$11,$2
lbu	$6,0($5)
addu	$11,$18,$11
beq	$6,$0,$L425
lbu	$7,0($11)

beq	$7,$0,$L425
nop

lh	$2,120($sp)
slt	$3,$2,4
beq	$3,$0,$L426
lw	$5,224($sp)

sll	$4,$4,2
lw	$25,10668($16)
lh	$5,122($sp)
addu	$13,$4,$2
lh	$11,124($sp)
addiu	$2,$sp,128
lh	$3,126($sp)
addu	$5,$4,$5
sw	$8,152($sp)
addu	$11,$4,$11
sw	$9,148($sp)
sw	$2,16($sp)
addu	$3,$4,$3
lw	$2,%got(tc0_table)($28)
move	$4,$fp
sw	$10,136($sp)
sw	$12,144($sp)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$11,$11,$2
addu	$13,$13,$2
addu	$2,$3,$2
lbu	$14,0($5)
lbu	$3,0($11)
lbu	$13,0($13)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,130($sp)
sb	$13,128($sp)
sb	$2,131($sp)
jalr	$25
sb	$14,129($sp)

lw	$12,144($sp)
lw	$28,40($sp)
lw	$8,152($sp)
lw	$9,148($sp)
lw	$3,9460($12)
lw	$2,9464($12)
b	$L425
lw	$10,136($sp)

$L427:
beq	$6,$0,$L889
addu	$4,$3,$22

beq	$7,$0,$L889
nop

lh	$3,48($sp)
slt	$2,$3,4
beq	$2,$0,$L435
addiu	$4,$fp,4

lh	$2,54($sp)
sll	$5,$5,2
lh	$10,50($sp)
lh	$12,52($sp)
addu	$11,$5,$3
lw	$25,10668($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$3,$5,$12
sw	$9,148($sp)
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$11,$11,$2
addu	$10,$10,$2
addu	$3,$3,$2
addu	$2,$5,$2
lbu	$11,0($11)
lbu	$10,0($10)
lbu	$3,0($3)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$11,128($sp)
sb	$10,129($sp)
sb	$3,130($sp)
jalr	$25
sb	$2,131($sp)

lw	$28,40($sp)
lw	$8,152($sp)
lw	$9,148($sp)
$L436:
li	$10,131072			# 0x20000
addu	$10,$16,$10
lw	$3,9460($10)
lw	$2,9464($10)
addu	$5,$23,$3
addu	$4,$23,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L434
lbu	$7,0($4)

beq	$7,$0,$L434
nop

lh	$2,48($sp)
slt	$3,$2,4
beq	$3,$0,$L437
addiu	$4,$fp,8

sll	$5,$5,2
lw	$25,10668($16)
lh	$11,50($sp)
addu	$12,$5,$2
lh	$3,52($sp)
addiu	$2,$sp,128
lh	$13,54($sp)
addu	$11,$5,$11
sw	$8,152($sp)
addu	$3,$5,$3
sw	$9,148($sp)
sw	$2,16($sp)
addu	$5,$5,$13
lw	$2,%got(tc0_table)($28)
sw	$10,136($sp)
addiu	$2,$2,%lo(tc0_table)
addu	$12,$12,$2
addu	$11,$11,$2
addu	$3,$3,$2
addu	$2,$5,$2
lbu	$12,0($12)
lbu	$11,0($11)
lbu	$3,0($3)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$12,128($sp)
sb	$3,130($sp)
sb	$2,131($sp)
jalr	$25
sb	$11,129($sp)

lw	$10,136($sp)
lw	$28,40($sp)
lw	$8,152($sp)
lw	$9,148($sp)
lw	$2,9464($10)
lw	$3,9460($10)
addu	$4,$23,$2
addu	$10,$23,$3
addu	$4,$4,$18
addu	$5,$10,$17
lbu	$7,0($4)
lbu	$6,0($5)
$L438:
beq	$6,$0,$L889
addu	$4,$3,$22

beq	$7,$0,$L889
nop

lh	$3,48($sp)
slt	$5,$3,4
beq	$5,$0,$L439
addiu	$4,$fp,12

sll	$2,$10,2
lw	$25,10668($16)
lh	$12,54($sp)
addu	$11,$2,$3
lh	$5,50($sp)
addiu	$3,$sp,128
lh	$10,52($sp)
sw	$8,152($sp)
addu	$5,$2,$5
sw	$9,148($sp)
sw	$3,16($sp)
addu	$10,$2,$10
lw	$3,%got(tc0_table)($28)
addu	$2,$2,$12
addiu	$3,$3,%lo(tc0_table)
addu	$11,$11,$3
addu	$5,$5,$3
addu	$10,$10,$3
addu	$2,$2,$3
lbu	$12,0($11)
lbu	$11,0($5)
lbu	$3,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$12,128($sp)
sb	$3,130($sp)
sb	$2,131($sp)
jalr	$25
sb	$11,129($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$8,152($sp)
lw	$9,148($sp)
lw	$3,9460($2)
lw	$2,9464($2)
$L434:
addu	$4,$3,$22
$L889:
addu	$22,$2,$22
addu	$5,$17,$4
addu	$22,$18,$22
lbu	$6,0($5)
beq	$6,$0,$L440
lbu	$7,0($22)

bne	$7,$0,$L876
nop

$L440:
addu	$5,$3,$23
addu	$4,$2,$23
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L432
lbu	$7,0($4)

beq	$7,$0,$L432
lw	$4,224($sp)

lh	$3,48($sp)
sll	$11,$4,2
slt	$2,$3,4
beq	$2,$0,$L442
addu	$10,$fp,$11

lh	$4,52($sp)
sll	$5,$5,2
lh	$2,54($sp)
lh	$12,50($sp)
addu	$13,$5,$3
addu	$3,$5,$4
lw	$25,10664($16)
move	$4,$10
sw	$8,152($sp)
addu	$12,$5,$12
sw	$9,148($sp)
addu	$5,$5,$2
sw	$10,136($sp)
addiu	$2,$sp,128
sw	$11,140($sp)
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$13,$13,$2
addu	$12,$12,$2
addu	$3,$3,$2
addu	$2,$5,$2
lbu	$13,0($13)
lbu	$12,0($12)
lbu	$3,0($3)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$13,128($sp)
sb	$12,129($sp)
sb	$3,130($sp)
jalr	$25
sb	$2,131($sp)

lw	$28,40($sp)
lw	$8,152($sp)
lw	$9,148($sp)
lw	$10,136($sp)
lw	$11,140($sp)
$L443:
li	$22,131072			# 0x20000
addu	$22,$16,$22
lw	$3,9460($22)
lw	$2,9464($22)
addu	$12,$23,$3
addu	$5,$23,$2
addu	$4,$17,$12
addu	$5,$18,$5
lbu	$6,0($4)
beq	$6,$0,$L432
lbu	$7,0($5)

beq	$7,$0,$L432
nop

lh	$2,48($sp)
slt	$3,$2,4
beq	$3,$0,$L444
addu	$4,$10,$11

sll	$12,$12,2
lw	$25,10664($16)
lh	$10,54($sp)
addu	$13,$12,$2
lh	$5,50($sp)
addiu	$2,$sp,128
lh	$3,52($sp)
sw	$8,152($sp)
addu	$5,$12,$5
sw	$9,148($sp)
sw	$2,16($sp)
addu	$3,$12,$3
lw	$2,%got(tc0_table)($28)
addu	$12,$12,$10
sw	$11,140($sp)
addiu	$2,$2,%lo(tc0_table)
addu	$10,$13,$2
addu	$5,$5,$2
addu	$3,$3,$2
addu	$2,$12,$2
lbu	$12,0($10)
lbu	$10,0($5)
lbu	$3,0($3)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$10,129($sp)
sb	$3,130($sp)
sb	$2,131($sp)
jalr	$25
sb	$12,128($sp)

lw	$3,9460($22)
lw	$2,9464($22)
lw	$28,40($sp)
addu	$10,$23,$3
lw	$8,152($sp)
addu	$4,$23,$2
lw	$9,148($sp)
addu	$5,$10,$17
lw	$11,140($sp)
addu	$4,$4,$18
lbu	$6,0($5)
lbu	$7,0($4)
$L445:
beq	$6,$0,$L432
nop

beq	$7,$0,$L432
lw	$5,224($sp)

lh	$3,48($sp)
sll	$4,$5,4
slt	$5,$3,4
subu	$11,$4,$11
beq	$5,$0,$L446
addu	$4,$fp,$11

sll	$2,$10,2
lh	$5,54($sp)
lh	$12,50($sp)
lh	$10,52($sp)
addu	$3,$2,$3
lw	$25,10664($16)
addu	$12,$2,$12
addu	$10,$2,$10
addu	$2,$2,$5
addiu	$5,$sp,128
sw	$5,16($sp)
lw	$5,%got(tc0_table)($28)
addiu	$5,$5,%lo(tc0_table)
addu	$3,$3,$5
addu	$11,$12,$5
b	$L864
addu	$10,$10,$5

$L488:
lw	$2,8($19)
lw	$3,12($19)
or	$2,$2,$3
beq	$2,$0,$L511
li	$2,131072			# 0x20000

lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$2,$16,$2
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($2)
lw	$2,9464($2)
addu	$5,$23,$3
addu	$4,$23,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L514
lbu	$7,0($4)

beq	$7,$0,$L514
nop

lh	$3,8($19)
slt	$2,$3,4
beq	$2,$0,$L515
addiu	$4,$fp,4

lh	$2,14($19)
sll	$5,$5,2
lh	$10,10($19)
lh	$9,12($19)
addu	$3,$5,$3
lw	$25,10668($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$11,120($sp)
sb	$10,121($sp)
sb	$3,122($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L511:
lw	$2,16($19)
lw	$3,20($19)
or	$2,$2,$3
beq	$2,$0,$L517
li	$21,131072			# 0x20000

lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$21,$16,$21
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($21)
lw	$2,9464($21)
addu	$5,$23,$3
addu	$4,$23,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L518
lbu	$7,0($4)

beq	$7,$0,$L890
addu	$3,$3,$8

lh	$3,16($19)
slt	$2,$3,4
beq	$2,$0,$L519
addiu	$4,$fp,8

lh	$2,22($19)
sll	$5,$5,2
lh	$10,18($19)
lh	$9,20($19)
addu	$3,$5,$3
lw	$25,10668($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$11,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,121($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
lw	$8,152($sp)
$L518:
addu	$3,$3,$8
$L890:
addu	$2,$2,$8
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L517
lbu	$7,0($2)

beq	$7,$0,$L517
lw	$2,216($sp)

lh	$5,16($19)
addiu	$4,$2,4
slt	$2,$5,4
beq	$2,$0,$L521
sll	$3,$3,2

lh	$2,22($19)
lh	$10,18($19)
lh	$9,20($19)
addu	$5,$3,$5
lw	$25,10684($16)
addu	$10,$3,$10
sw	$8,152($sp)
addu	$9,$3,$9
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$11,0($5)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L522:
li	$3,131072			# 0x20000
addu	$3,$16,$3
lw	$2,9460($3)
lw	$3,9464($3)
addu	$2,$8,$2
addu	$3,$8,$3
addu	$17,$17,$2
addu	$18,$18,$3
lbu	$6,0($17)
beq	$6,$0,$L517
lbu	$7,0($18)

beq	$7,$0,$L517
lw	$5,220($sp)

lh	$3,16($19)
addiu	$4,$5,4
slt	$5,$3,4
beq	$5,$0,$L523
lw	$5,228($sp)

lh	$5,22($19)
sll	$2,$2,2
lh	$10,18($19)
lh	$9,20($19)
addu	$3,$2,$3
lw	$25,10684($16)
addu	$10,$2,$10
sw	$8,152($sp)
addu	$9,$2,$9
addu	$2,$2,$5
addiu	$5,$sp,120
sw	$5,16($sp)
lw	$5,%got(tc0_table)($28)
addiu	$5,$5,%lo(tc0_table)
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L517:
lw	$2,24($19)
$L878:
lw	$3,28($19)
or	$2,$2,$3
beq	$2,$0,$L524
li	$2,131072			# 0x20000

lw	$4,%got(alpha_table)($28)
addu	$2,$16,$2
addiu	$4,$4,%lo(alpha_table)
lw	$5,9460($2)
lw	$2,9464($2)
addu	$5,$23,$5
addu	$3,$23,$2
lw	$2,%got(beta_table)($28)
addu	$4,$5,$4
addiu	$2,$2,%lo(beta_table)
lbu	$6,0($4)
addu	$2,$3,$2
beq	$6,$0,$L524
lbu	$7,0($2)

beq	$7,$0,$L524
nop

lh	$3,24($19)
slt	$2,$3,4
beq	$2,$0,$L526
addiu	$4,$fp,12

lh	$2,30($19)
sll	$5,$5,2
lh	$10,26($19)
lh	$9,28($19)
addu	$3,$5,$3
lw	$25,10668($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$11,120($sp)
sb	$10,121($sp)
sb	$3,122($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L524:
lw	$2,32($19)
lw	$3,36($19)
or	$2,$2,$3
beq	$2,$0,$L528
nop

li	$21,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$21,$16,$21
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($21)
lw	$2,9464($21)
addu	$4,$22,$3
addu	$22,$22,$2
addu	$5,$17,$4
addu	$22,$18,$22
lbu	$6,0($5)
beq	$6,$0,$L529
lbu	$7,0($22)

beq	$7,$0,$L891
addu	$3,$3,$20

lh	$3,32($19)
slt	$2,$3,4
beq	$2,$0,$L530
lw	$5,224($sp)

lh	$10,34($19)
sll	$4,$4,2
lh	$9,36($19)
lh	$2,38($19)
addu	$3,$4,$3
lw	$5,%got(tc0_table)($28)
addu	$10,$4,$10
addu	$9,$4,$9
lw	$25,10664($16)
addu	$2,$4,$2
sw	$8,152($sp)
addiu	$5,$5,%lo(tc0_table)
addiu	$4,$sp,120
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
sw	$4,16($sp)
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
move	$4,$fp
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$11,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,121($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
lw	$8,152($sp)
$L529:
addu	$3,$3,$20
$L891:
addu	$2,$2,$20
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L528
lbu	$7,0($2)

beq	$7,$0,$L528
nop

lh	$4,32($19)
slt	$2,$4,4
beq	$2,$0,$L532
lw	$5,228($sp)

lh	$2,38($19)
sll	$3,$3,2
lh	$10,34($19)
lh	$9,36($19)
addu	$5,$3,$4
lw	$25,10680($16)
addu	$10,$3,$10
lw	$4,216($sp)
addu	$9,$3,$9
sw	$8,152($sp)
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$11,0($5)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L533:
li	$3,131072			# 0x20000
addu	$3,$16,$3
lw	$2,9460($3)
lw	$3,9464($3)
addu	$2,$20,$2
addu	$20,$20,$3
addu	$17,$17,$2
addu	$18,$18,$20
lbu	$6,0($17)
beq	$6,$0,$L528
lbu	$7,0($18)

beq	$7,$0,$L528
nop

lh	$3,32($19)
slt	$4,$3,4
beq	$4,$0,$L534
lw	$5,228($sp)

lh	$4,38($19)
sll	$2,$2,2
lh	$10,34($19)
lh	$9,36($19)
addu	$3,$2,$3
lw	$5,%got(tc0_table)($28)
addu	$10,$2,$10
lw	$25,10680($16)
addu	$9,$2,$9
sw	$8,152($sp)
addiu	$5,$5,%lo(tc0_table)
addu	$2,$2,$4
addu	$3,$3,$5
addu	$9,$9,$5
addu	$10,$10,$5
addu	$2,$2,$5
lbu	$11,0($3)
lbu	$3,0($9)
addiu	$4,$sp,120
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
sw	$4,16($sp)
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
sb	$2,123($sp)
jalr	$25
lw	$4,220($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L528:
lw	$2,40($19)
lw	$3,44($19)
or	$2,$2,$3
beq	$2,$0,$L535
li	$2,131072			# 0x20000

lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$2,$16,$2
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($2)
lw	$2,9464($2)
addu	$5,$23,$3
addu	$4,$23,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L538
lbu	$7,0($4)

beq	$7,$0,$L538
lw	$4,224($sp)

lh	$3,40($19)
sll	$2,$4,2
addu	$4,$fp,$2
slt	$2,$3,4
beq	$2,$0,$L539
nop

lh	$2,46($19)
sll	$5,$5,2
lh	$10,42($19)
lh	$9,44($19)
addu	$3,$5,$3
lw	$25,10664($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$11,120($sp)
sb	$10,121($sp)
sb	$3,122($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L535:
lw	$2,48($19)
lw	$3,52($19)
or	$2,$2,$3
beq	$2,$0,$L541
nop

li	$20,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$20,$16,$20
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($20)
lw	$2,9464($20)
addu	$5,$23,$3
addu	$4,$23,$2
addu	$6,$17,$5
addu	$4,$18,$4
lbu	$6,0($6)
beq	$6,$0,$L542
lbu	$7,0($4)

beq	$7,$0,$L892
addu	$3,$3,$8

lw	$4,224($sp)
lh	$3,48($19)
sll	$2,$4,3
addu	$4,$fp,$2
slt	$2,$3,4
beq	$2,$0,$L543
nop

lh	$2,54($19)
sll	$5,$5,2
lh	$10,50($19)
lh	$9,52($19)
addu	$3,$5,$3
lw	$25,10664($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$11,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$10,121($sp)

lw	$28,40($sp)
lw	$3,9460($20)
lw	$2,9464($20)
lw	$8,152($sp)
$L542:
addu	$3,$3,$8
$L892:
addu	$2,$2,$8
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L541
lbu	$7,0($2)

beq	$7,$0,$L541
lw	$2,228($sp)

lh	$5,48($19)
sll	$20,$2,2
lw	$2,216($sp)
addu	$4,$2,$20
slt	$2,$5,4
beq	$2,$0,$L545
sll	$3,$3,2

lh	$2,54($19)
lh	$10,50($19)
lh	$9,52($19)
addu	$5,$3,$5
lw	$25,10680($16)
addu	$10,$3,$10
sw	$8,152($sp)
addu	$9,$3,$9
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$3,$2
lbu	$11,0($5)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,120($sp)
sb	$3,122($sp)
sb	$9,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
lw	$8,152($sp)
$L546:
li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$3,9460($2)
lw	$2,9464($2)
addu	$3,$8,$3
addu	$2,$8,$2
addu	$17,$17,$3
addu	$18,$18,$2
lbu	$6,0($17)
beq	$6,$0,$L541
lbu	$7,0($18)

beq	$7,$0,$L541
nop

lh	$5,48($19)
lw	$8,220($sp)
slt	$2,$5,4
beq	$2,$0,$L547
addu	$4,$8,$20

lh	$2,54($19)
sll	$3,$3,2
lh	$9,50($19)
lh	$8,52($19)
addu	$5,$3,$5
lw	$25,10680($16)
addu	$9,$3,$9
addu	$8,$3,$8
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$8,$8,$2
addu	$9,$9,$2
addu	$2,$3,$2
lbu	$10,0($5)
lbu	$3,0($8)
lbu	$9,0($9)
lbu	$2,0($2)
addiu	$10,$10,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$8,$9,1
addiu	$2,$2,1
sb	$10,120($sp)
sb	$3,122($sp)
sb	$8,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
$L541:
lw	$2,56($19)
$L879:
lw	$3,60($19)
or	$2,$2,$3
beq	$2,$0,$L417
li	$2,131072			# 0x20000

lw	$4,%got(beta_table)($28)
addu	$2,$16,$2
addiu	$4,$4,%lo(beta_table)
lw	$3,9460($2)
lw	$2,9464($2)
addu	$3,$23,$3
addu	$23,$23,$2
lw	$2,%got(alpha_table)($28)
addu	$23,$23,$4
addiu	$2,$2,%lo(alpha_table)
addu	$2,$3,$2
lbu	$6,0($2)
beq	$6,$0,$L417
lbu	$7,0($23)

beq	$7,$0,$L877
lw	$31,196($sp)

lw	$4,224($sp)
lh	$5,56($19)
sll	$2,$4,4
sll	$4,$4,2
slt	$8,$5,4
subu	$2,$2,$4
beq	$8,$0,$L549
addu	$4,$fp,$2

lh	$2,62($19)
sll	$3,$3,2
lh	$9,58($19)
lh	$8,60($19)
addu	$5,$3,$5
lw	$25,10664($16)
addu	$9,$3,$9
addu	$8,$3,$8
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$8,$8,$2
addu	$9,$9,$2
addu	$2,$3,$2
lbu	$10,0($5)
lbu	$3,0($8)
lbu	$9,0($9)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$10,120($sp)
sb	$9,121($sp)
sb	$3,122($sp)
jalr	$25
sb	$2,123($sp)

b	$L877
lw	$31,196($sp)

$L874:
lw	$2,32($19)
lw	$3,36($19)
or	$2,$2,$3
beq	$2,$0,$L877
lw	$31,196($sp)

li	$2,131072			# 0x20000
lw	$17,%got(alpha_table)($28)
lw	$18,%got(beta_table)($28)
addu	$21,$16,$2
addiu	$17,$17,%lo(alpha_table)
addiu	$18,$18,%lo(beta_table)
lw	$3,9460($21)
lw	$2,9464($21)
addu	$4,$22,$3
addu	$22,$22,$2
addu	$5,$17,$4
addu	$22,$18,$22
lbu	$6,0($5)
beq	$6,$0,$L482
lbu	$7,0($22)

beq	$7,$0,$L893
addu	$3,$3,$20

lh	$3,32($19)
slt	$2,$3,4
beq	$2,$0,$L483
sll	$4,$4,2

lh	$9,34($19)
lh	$8,36($19)
lh	$2,38($19)
addu	$3,$4,$3
lw	$5,%got(tc0_table)($28)
addu	$9,$4,$9
addu	$8,$4,$8
lw	$25,10664($16)
addu	$2,$4,$2
addiu	$5,$5,%lo(tc0_table)
addiu	$4,$sp,120
addu	$3,$3,$5
addu	$8,$8,$5
addu	$9,$9,$5
sw	$4,16($sp)
addu	$2,$2,$5
lbu	$10,0($3)
lbu	$3,0($8)
move	$4,$fp
lbu	$9,0($9)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,122($sp)
sb	$10,120($sp)
sb	$2,123($sp)
jalr	$25
sb	$9,121($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
$L482:
addu	$3,$3,$20
$L893:
addu	$2,$2,$20
addu	$4,$17,$3
addu	$2,$18,$2
lbu	$6,0($4)
beq	$6,$0,$L417
lbu	$7,0($2)

beq	$7,$0,$L877
lw	$31,196($sp)

lh	$4,32($19)
slt	$2,$4,4
beq	$2,$0,$L485
sll	$3,$3,2

lh	$2,38($19)
lh	$9,34($19)
lh	$8,36($19)
addu	$5,$3,$4
lw	$25,10680($16)
addu	$9,$3,$9
lw	$4,216($sp)
addu	$8,$3,$8
addu	$3,$3,$2
addiu	$2,$sp,120
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$5,$5,$2
addu	$8,$8,$2
addu	$9,$9,$2
addu	$2,$3,$2
lbu	$10,0($5)
lbu	$3,0($8)
lbu	$9,0($9)
lbu	$2,0($2)
addiu	$10,$10,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$8,$9,1
addiu	$2,$2,1
sb	$10,120($sp)
sb	$3,122($sp)
sb	$8,121($sp)
jalr	$25
sb	$2,123($sp)

lw	$28,40($sp)
$L486:
li	$3,131072			# 0x20000
addu	$3,$16,$3
lw	$2,9460($3)
lw	$3,9464($3)
addu	$2,$20,$2
addu	$20,$20,$3
addu	$17,$17,$2
addu	$18,$18,$20
lbu	$6,0($17)
beq	$6,$0,$L417
lbu	$7,0($18)

beq	$7,$0,$L877
lw	$31,196($sp)

lh	$3,32($19)
slt	$4,$3,4
beq	$4,$0,$L487
lw	$5,%got(tc0_table)($28)

sll	$2,$2,2
lh	$4,38($19)
lh	$9,34($19)
addu	$3,$2,$3
lh	$8,36($19)
addiu	$5,$5,%lo(tc0_table)
lw	$25,10680($16)
addu	$9,$2,$9
addu	$8,$2,$8
addu	$3,$3,$5
addu	$2,$2,$4
addiu	$4,$sp,120
addu	$9,$9,$5
lbu	$10,0($3)
addu	$8,$8,$5
sw	$4,16($sp)
addu	$2,$2,$5
b	$L863
lw	$4,220($sp)

$L510:
lw	$25,10688($16)
jalr	$25
lw	$5,228($sp)

b	$L877
lw	$31,196($sp)

$L866:
lh	$3,48($sp)
slt	$2,$3,4
beq	$2,$0,$L429
addiu	$4,$fp,8

lh	$2,54($sp)
sll	$5,$5,2
lh	$10,50($sp)
lh	$12,52($sp)
addu	$11,$5,$3
lw	$25,10668($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$3,$5,$12
sw	$9,148($sp)
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$11,$11,$2
addu	$10,$10,$2
addu	$3,$3,$2
addu	$2,$5,$2
lbu	$11,0($11)
lbu	$10,0($10)
lbu	$3,0($3)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$11,128($sp)
sb	$3,130($sp)
sb	$2,131($sp)
jalr	$25
sb	$10,129($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$8,152($sp)
lw	$9,148($sp)
lw	$3,9460($2)
b	$L428
lw	$2,9464($2)

$L876:
lh	$3,0($19)
slt	$2,$3,4
beq	$2,$0,$L441
lw	$5,224($sp)

lh	$11,2($19)
sll	$4,$4,2
lh	$10,4($19)
lh	$2,6($19)
addu	$3,$4,$3
lw	$5,%got(tc0_table)($28)
addu	$11,$4,$11
addu	$10,$4,$10
lw	$25,10664($16)
addu	$2,$4,$2
sw	$8,152($sp)
addiu	$5,$5,%lo(tc0_table)
sw	$9,148($sp)
addiu	$4,$sp,128
addu	$3,$3,$5
addu	$10,$10,$5
addu	$11,$11,$5
sw	$4,16($sp)
addu	$2,$2,$5
lbu	$12,0($3)
lbu	$3,0($10)
move	$4,$fp
lbu	$11,0($11)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,130($sp)
sb	$12,128($sp)
sb	$2,131($sp)
jalr	$25
sb	$11,129($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$8,152($sp)
lw	$9,148($sp)
lw	$3,9460($2)
b	$L440
lw	$2,9464($2)

$L867:
lh	$3,0($19)
slt	$2,$3,4
beq	$2,$0,$L431
lw	$5,224($sp)

lh	$11,2($19)
sll	$4,$4,2
lh	$10,4($19)
lh	$2,6($19)
addu	$3,$4,$3
lw	$5,%got(tc0_table)($28)
addu	$11,$4,$11
addu	$10,$4,$10
lw	$25,10664($16)
addu	$2,$4,$2
sw	$8,152($sp)
addiu	$5,$5,%lo(tc0_table)
sw	$9,148($sp)
addiu	$4,$sp,128
addu	$3,$3,$5
addu	$10,$10,$5
addu	$11,$11,$5
sw	$4,16($sp)
addu	$2,$2,$5
lbu	$12,0($3)
lbu	$3,0($10)
move	$4,$fp
lbu	$11,0($11)
lbu	$2,0($2)
lw	$5,224($sp)
sb	$3,130($sp)
sb	$12,128($sp)
sb	$2,131($sp)
jalr	$25
sb	$11,129($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$8,152($sp)
lw	$9,148($sp)
lw	$3,9460($2)
b	$L430
lw	$2,9464($2)

$L868:
sll	$5,$5,2
lh	$10,50($sp)
lh	$9,52($sp)
addu	$3,$5,$3
lw	$25,10684($16)
addu	$10,$5,$10
sw	$8,152($sp)
addu	$9,$5,$9
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,128($sp)
sb	$3,130($sp)
sb	$9,129($sp)
jalr	$25
sb	$2,131($sp)

lw	$28,40($sp)
b	$L454
lw	$8,152($sp)

$L869:
lh	$2,6($19)
lh	$10,2($19)
lh	$9,4($19)
addu	$3,$5,$3
lw	$25,10680($16)
addu	$10,$5,$10
lw	$4,216($sp)
addu	$9,$5,$9
sw	$8,152($sp)
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,128($sp)
sb	$3,130($sp)
sb	$9,129($sp)
jalr	$25
sb	$2,131($sp)

b	$L860
lw	$28,40($sp)

$L871:
lh	$2,6($19)
lh	$10,2($19)
lh	$9,4($19)
addu	$3,$5,$3
lw	$25,10680($16)
addu	$10,$5,$10
lw	$4,220($sp)
addu	$9,$5,$9
sw	$8,152($sp)
addu	$5,$5,$2
addiu	$2,$sp,128
sw	$2,16($sp)
lw	$2,%got(tc0_table)($28)
addiu	$2,$2,%lo(tc0_table)
addu	$3,$3,$2
addu	$9,$9,$2
addu	$10,$10,$2
addu	$2,$5,$2
lbu	$11,0($3)
lbu	$3,0($9)
lbu	$10,0($10)
lbu	$2,0($2)
addiu	$11,$11,1
addiu	$3,$3,1
lw	$5,228($sp)
addiu	$9,$10,1
addiu	$2,$2,1
sb	$11,128($sp)
sb	$3,130($sp)
sb	$9,129($sp)
jalr	$25
sb	$2,131($sp)

b	$L862
lw	$28,40($sp)

$L870:
sll	$9,$9,2
lh	$5,50($sp)
lh	$10,52($sp)
addu	$2,$9,$2
lw	$25,10680($16)
addu	$5,$9,$5
sw	$8,152($sp)
addu	$10,$9,$10
addu	$9,$9,$3
addiu	$3,$sp,128
sw	$3,16($sp)
lw	$3,%got(tc0_table)($28)
addiu	$3,$3,%lo(tc0_table)
addu	$2,$2,$3
addu	$5,$5,$3
addu	$9,$9,$3
addu	$10,$10,$3
lbu	$12,0($2)
lbu	$11,0($5)
lbu	$2,0($9)
lbu	$3,0($10)
addiu	$10,$12,1
addiu	$9,$11,1
lw	$5,228($sp)
addiu	$2,$2,1
addiu	$3,$3,1
sb	$10,128($sp)
sb	$9,129($sp)
sb	$2,131($sp)
jalr	$25
sb	$3,130($sp)

b	$L861
lw	$28,40($sp)

$L872:
li	$4,131072			# 0x20000
ori	$4,$4,0x2
ori	$5,$5,0x2
li	$12,4			# 0x4
sw	$4,0($19)
sw	$5,4($19)
sw	$4,16($19)
sw	$5,20($19)
sw	$4,32($19)
sw	$5,36($19)
sw	$4,48($19)
b	$L466
sw	$5,52($19)

$L873:
li	$4,262144			# 0x40000
li	$5,262144			# 0x40000
ori	$4,$4,0x4
ori	$5,$5,0x4
sw	$4,32($19)
b	$L468
sw	$5,36($19)

$L514:
lw	$4,16($19)
lw	$5,20($19)
or	$4,$4,$5
bne	$4,$0,$L890
addu	$3,$3,$8

b	$L878
lw	$2,24($19)

$L538:
lw	$4,48($19)
lw	$5,52($19)
or	$4,$4,$5
bne	$4,$0,$L892
addu	$3,$3,$8

b	$L879
lw	$2,56($19)

$L455:
lw	$25,10692($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
b	$L452
lw	$8,152($sp)

$L431:
lw	$25,10672($16)
move	$4,$fp
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$9,148($sp)
lw	$8,152($sp)
lw	$3,9460($2)
b	$L430
lw	$2,9464($2)

$L426:
lw	$25,10676($16)
move	$4,$fp
sw	$8,152($sp)
sw	$9,148($sp)
sw	$10,136($sp)
jalr	$25
sw	$12,144($sp)

lw	$12,144($sp)
lw	$28,40($sp)
lw	$10,136($sp)
lw	$9,148($sp)
lw	$3,9460($12)
lw	$2,9464($12)
b	$L425
lw	$8,152($sp)

$L437:
lw	$25,10676($16)
lw	$5,224($sp)
sw	$8,152($sp)
sw	$9,148($sp)
jalr	$25
sw	$10,136($sp)

lw	$10,136($sp)
lw	$28,40($sp)
lw	$9,148($sp)
lw	$8,152($sp)
lw	$2,9464($10)
lw	$3,9460($10)
addu	$4,$23,$2
addu	$10,$23,$3
addu	$4,$4,$18
addu	$5,$10,$17
lbu	$7,0($4)
b	$L438
lbu	$6,0($5)

$L435:
lw	$25,10676($16)
lw	$5,224($sp)
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

lw	$28,40($sp)
lw	$9,148($sp)
b	$L436
lw	$8,152($sp)

$L444:
lw	$25,10672($16)
lw	$5,224($sp)
sw	$8,152($sp)
sw	$9,148($sp)
jalr	$25
sw	$11,140($sp)

lw	$3,9460($22)
lw	$2,9464($22)
lw	$28,40($sp)
addu	$10,$23,$3
lw	$11,140($sp)
addu	$4,$23,$2
lw	$9,148($sp)
addu	$5,$10,$17
lw	$8,152($sp)
addu	$4,$4,$18
lbu	$6,0($5)
b	$L445
lbu	$7,0($4)

$L442:
lw	$25,10672($16)
move	$4,$10
lw	$5,224($sp)
sw	$8,152($sp)
sw	$9,148($sp)
sw	$10,136($sp)
jalr	$25
sw	$11,140($sp)

lw	$28,40($sp)
lw	$11,140($sp)
lw	$10,136($sp)
lw	$9,148($sp)
b	$L443
lw	$8,152($sp)

$L439:
lw	$25,10676($16)
lw	$5,224($sp)
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$9,148($sp)
lw	$8,152($sp)
lw	$3,9460($2)
b	$L434
lw	$2,9464($2)

$L433:
lw	$25,10672($16)
lw	$5,224($sp)
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

b	$L859
lw	$28,40($sp)

$L446:
lw	$25,10672($16)
lw	$5,224($sp)
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$9,148($sp)
lw	$8,152($sp)
lw	$3,9460($2)
b	$L432
lw	$2,9464($2)

$L429:
lw	$25,10676($16)
lw	$5,224($sp)
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$9,148($sp)
lw	$8,152($sp)
lw	$3,9460($2)
b	$L428
lw	$2,9464($2)

$L441:
lw	$25,10672($16)
move	$4,$fp
sw	$8,152($sp)
jalr	$25
sw	$9,148($sp)

li	$2,131072			# 0x20000
lw	$28,40($sp)
addu	$2,$16,$2
lw	$9,148($sp)
lw	$8,152($sp)
lw	$3,9460($2)
b	$L440
lw	$2,9464($2)

$L451:
lw	$25,10692($16)
lw	$4,220($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($22)
lw	$2,9464($22)
b	$L448
lw	$8,152($sp)

$L449:
lw	$25,10692($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L450
lw	$8,152($sp)

$L475:
lw	$25,10676($16)
lw	$5,224($sp)
sw	$3,136($sp)
sw	$8,152($sp)
sw	$9,148($sp)
jalr	$25
sw	$12,144($sp)

lw	$9,148($sp)
lw	$28,40($sp)
lw	$12,144($sp)
lw	$8,152($sp)
lw	$4,9460($9)
lw	$2,9464($9)
b	$L474
lw	$3,136($sp)

$L478:
lw	$25,10692($16)
lw	$4,220($sp)
sw	$3,136($sp)
sw	$8,152($sp)
jalr	$25
sw	$12,144($sp)

lw	$28,40($sp)
lw	$12,144($sp)
lw	$8,152($sp)
b	$L471
lw	$3,136($sp)

$L476:
lw	$25,10692($16)
lw	$4,216($sp)
lw	$5,228($sp)
sw	$3,136($sp)
sw	$8,152($sp)
jalr	$25
sw	$12,144($sp)

lw	$28,40($sp)
lw	$12,144($sp)
lw	$8,152($sp)
b	$L477
lw	$3,136($sp)

$L547:
lw	$25,10688($16)
jalr	$25
lw	$5,228($sp)

b	$L541
lw	$28,40($sp)

$L545:
lw	$25,10688($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L546
lw	$8,152($sp)

$L519:
lw	$25,10676($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
b	$L518
lw	$8,152($sp)

$L539:
lw	$25,10672($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L535
lw	$8,152($sp)

$L515:
lw	$25,10676($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L511
lw	$8,152($sp)

$L504:
lw	$25,10688($16)
lw	$4,220($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L498
lw	$8,152($sp)

$L502:
lw	$25,10688($16)
lw	$4,216($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L503
lw	$8,152($sp)

$L496:
lw	$25,10692($16)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L490
lw	$8,152($sp)

$L494:
lw	$25,10692($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L495
lw	$8,152($sp)

$L534:
lw	$25,10688($16)
lw	$4,220($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L528
lw	$8,152($sp)

$L532:
lw	$25,10688($16)
lw	$4,216($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L533
lw	$8,152($sp)

$L500:
lw	$25,10672($16)
move	$4,$fp
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
b	$L499
lw	$8,152($sp)

$L549:
lw	$25,10672($16)
jalr	$25
lw	$5,224($sp)

b	$L877
lw	$31,196($sp)

$L530:
lw	$25,10672($16)
move	$4,$fp
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
b	$L529
lw	$8,152($sp)

$L508:
lw	$25,10688($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L509
lw	$8,152($sp)

$L523:
lw	$25,10692($16)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L517
lw	$8,152($sp)

$L521:
lw	$25,10692($16)
lw	$5,228($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L522
lw	$8,152($sp)

$L492:
lw	$25,10676($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($21)
lw	$2,9464($21)
b	$L491
lw	$8,152($sp)

$L526:
lw	$25,10676($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
b	$L524
lw	$8,152($sp)

$L543:
lw	$25,10672($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($20)
lw	$2,9464($20)
b	$L542
lw	$8,152($sp)

$L507:
lw	$25,10672($16)
lw	$5,224($sp)
jalr	$25
sw	$8,152($sp)

lw	$28,40($sp)
lw	$3,9460($20)
lw	$2,9464($20)
b	$L506
lw	$8,152($sp)

$L483:
lw	$25,10672($16)
move	$4,$fp
jalr	$25
lw	$5,224($sp)

lw	$28,40($sp)
lw	$3,9460($21)
b	$L482
lw	$2,9464($21)

$L487:
lw	$25,10688($16)
lw	$4,220($sp)
jalr	$25
lw	$5,228($sp)

b	$L877
lw	$31,196($sp)

$L485:
lw	$25,10688($16)
lw	$4,216($sp)
jalr	$25
lw	$5,228($sp)

b	$L486
lw	$28,40($sp)

.set	macro
.set	reorder
.end	ff_h264_filter_mb_fast
.size	ff_h264_filter_mb_fast, .-ff_h264_filter_mb_fast
.rdata
.align	2
.type	mask_edge_tab.7593, @object
.size	mask_edge_tab.7593, 16
mask_edge_tab.7593:
.byte	0
.byte	3
.byte	3
.byte	3
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	3
.byte	1
.byte	1
.byte	3
.byte	3
.byte	3
.byte	3
.align	2
.type	offset.7671, @object
.size	offset.7671, 32
offset.7671:
.byte	7
.byte	7
.byte	7
.byte	7
.byte	15
.byte	15
.byte	15
.byte	15
.byte	23
.byte	23
.byte	23
.byte	23
.byte	31
.byte	31
.byte	31
.byte	31
.byte	7
.byte	15
.byte	23
.byte	31
.byte	7
.byte	15
.byte	23
.byte	31
.byte	7
.byte	15
.byte	23
.byte	31
.byte	7
.byte	15
.byte	23
.byte	31
.align	2
.type	tc0_table, @object
.size	tc0_table, 624
tc0_table:
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	0
.byte	0
.byte	1
.byte	-1
.byte	0
.byte	0
.byte	1
.byte	-1
.byte	0
.byte	0
.byte	1
.byte	-1
.byte	0
.byte	0
.byte	1
.byte	-1
.byte	0
.byte	1
.byte	1
.byte	-1
.byte	0
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	1
.byte	1
.byte	-1
.byte	1
.byte	1
.byte	2
.byte	-1
.byte	1
.byte	1
.byte	2
.byte	-1
.byte	1
.byte	1
.byte	2
.byte	-1
.byte	1
.byte	1
.byte	2
.byte	-1
.byte	1
.byte	2
.byte	3
.byte	-1
.byte	1
.byte	2
.byte	3
.byte	-1
.byte	2
.byte	2
.byte	3
.byte	-1
.byte	2
.byte	2
.byte	4
.byte	-1
.byte	2
.byte	3
.byte	4
.byte	-1
.byte	2
.byte	3
.byte	4
.byte	-1
.byte	3
.byte	3
.byte	5
.byte	-1
.byte	3
.byte	4
.byte	6
.byte	-1
.byte	3
.byte	4
.byte	6
.byte	-1
.byte	4
.byte	5
.byte	7
.byte	-1
.byte	4
.byte	5
.byte	8
.byte	-1
.byte	4
.byte	6
.byte	9
.byte	-1
.byte	5
.byte	7
.byte	10
.byte	-1
.byte	6
.byte	8
.byte	11
.byte	-1
.byte	6
.byte	8
.byte	13
.byte	-1
.byte	7
.byte	10
.byte	14
.byte	-1
.byte	8
.byte	11
.byte	16
.byte	-1
.byte	9
.byte	12
.byte	18
.byte	-1
.byte	10
.byte	13
.byte	20
.byte	-1
.byte	11
.byte	15
.byte	23
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.byte	-1
.byte	13
.byte	17
.byte	25
.align	2
.type	beta_table, @object
.size	beta_table, 156
beta_table:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	6
.byte	6
.byte	7
.byte	7
.byte	8
.byte	8
.byte	9
.byte	9
.byte	10
.byte	10
.byte	11
.byte	11
.byte	12
.byte	12
.byte	13
.byte	13
.byte	14
.byte	14
.byte	15
.byte	15
.byte	16
.byte	16
.byte	17
.byte	17
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.byte	18
.align	2
.type	alpha_table, @object
.size	alpha_table, 156
alpha_table:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	12
.byte	13
.byte	15
.byte	17
.byte	20
.byte	22
.byte	25
.byte	28
.byte	32
.byte	36
.byte	40
.byte	45
.byte	50
.byte	56
.byte	63
.byte	71
.byte	80
.byte	90
.byte	101
.byte	113
.byte	127
.byte	-112
.byte	-94
.byte	-74
.byte	-53
.byte	-30
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
