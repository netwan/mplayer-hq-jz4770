.file	1 "h264_bs_cabac.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.get_cabac_bypass,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_cabac_bypass
.type	get_cabac_bypass, @function
get_cabac_bypass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,0($4)
sll	$2,$2,1
andi	$3,$2,0xffff
bne	$3,$0,$L2
sw	$2,0($4)

lw	$3,16($4)
addiu	$6,$3,2
lbu	$5,0($3)
lbu	$3,1($3)
sw	$6,16($4)
sll	$5,$5,9
sll	$3,$3,1
addu	$3,$5,$3
addu	$2,$2,$3
li	$3,-65536			# 0xffffffffffff0000
addiu	$3,$3,1
addu	$2,$2,$3
sw	$2,0($4)
$L2:
lw	$3,4($4)
sll	$3,$3,17
slt	$5,$2,$3
bne	$5,$0,$L4
subu	$3,$2,$3

li	$2,1			# 0x1
j	$31
sw	$3,0($4)

$L4:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	get_cabac_bypass
.size	get_cabac_bypass, .-get_cabac_bypass
.section	.text.predict_field_decoding_flag_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	predict_field_decoding_flag_hw
.type	predict_field_decoding_flag_hw, @function
predict_field_decoding_flag_hw:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,7996($4)
li	$5,65536			# 0x10000
lw	$7,168($4)
addu	$5,$4,$5
lw	$2,7992($4)
mul	$8,$7,$3
lw	$6,-5276($5)
lw	$3,-5272($5)
addu	$2,$8,$2
addu	$5,$6,$2
lbu	$5,-1($5)
beq	$5,$3,$L10
nop

subu	$2,$2,$7
addu	$6,$6,$2
lbu	$5,0($6)
beq	$3,$5,$L11
li	$3,65536			# 0x10000

move	$2,$0
addu	$4,$4,$3
sw	$2,-5248($4)
j	$31
sw	$2,-5244($4)

$L11:
lw	$3,2192($4)
sll	$2,$2,2
addu	$2,$3,$2
li	$3,65536			# 0x10000
lw	$2,0($2)
addu	$4,$4,$3
srl	$2,$2,7
andi	$2,$2,0x1
sw	$2,-5248($4)
j	$31
sw	$2,-5244($4)

$L10:
lw	$3,2192($4)
sll	$2,$2,2
addu	$2,$3,$2
li	$3,65536			# 0x10000
lw	$2,-4($2)
addu	$4,$4,$3
srl	$2,$2,7
andi	$2,$2,0x1
sw	$2,-5248($4)
j	$31
sw	$2,-5244($4)

.set	macro
.set	reorder
.end	predict_field_decoding_flag_hw
.size	predict_field_decoding_flag_hw, .-predict_field_decoding_flag_hw
.section	.text.refill.isra.0,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	refill.isra.0
.type	refill.isra.0, @function
refill.isra.0:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$7,0($5)
lw	$3,0($4)
addiu	$8,$7,2
lbu	$6,0($7)
lbu	$2,1($7)
sll	$6,$6,9
sll	$2,$2,1
addu	$2,$6,$2
addu	$3,$2,$3
li	$2,-65536			# 0xffffffffffff0000
addiu	$2,$2,1
addu	$2,$3,$2
sw	$2,0($4)
j	$31
sw	$8,0($5)

.set	macro
.set	reorder
.end	refill.isra.0
.size	refill.isra.0, .-refill.isra.0
.section	.text.refill2.isra.1,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	refill2.isra.1
.type	refill2.isra.1, @function
refill2.isra.1:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$8,0($4)
lw	$2,0($5)
addiu	$9,$8,-1
addiu	$10,$2,2
lbu	$7,0($2)
xor	$9,$9,$8
lbu	$3,1($2)
sra	$9,$9,15
lw	$2,%got(ff_h264_norm_shift)($28)
sll	$7,$7,9
sll	$3,$3,1
addu	$9,$2,$9
li	$2,7			# 0x7
addu	$6,$7,$3
lbu	$9,0($9)
subu	$7,$2,$9
li	$2,-65536			# 0xffffffffffff0000
addiu	$2,$2,1
addu	$3,$6,$2
sll	$2,$3,$7
addu	$2,$8,$2
sw	$2,0($4)
j	$31
sw	$10,0($5)

.set	macro
.set	reorder
.end	refill2.isra.1
.size	refill2.isra.1, .-refill2.isra.1
.section	.text.get_cabac_noinline,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_cabac_noinline
.type	get_cabac_noinline, @function
get_cabac_noinline:
.frame	$sp,40,$31		# vars= 8, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$7,4($4)
addiu	$sp,$sp,-40
lw	$8,%got(ff_h264_lps_range)($28)
.cprestore	16
sw	$31,36($sp)
andi	$3,$7,0xc0
lbu	$11,0($5)
sll	$3,$3,1
lw	$9,0($4)
addu	$3,$3,$11
addu	$3,$8,$3
lbu	$8,0($3)
subu	$7,$7,$8
sll	$10,$7,17
subu	$3,$10,$9
sra	$2,$3,31
lw	$3,%got(ff_h264_mlps_state)($28)
movn	$7,$8,$2
and	$10,$2,$10
subu	$9,$9,$10
xor	$2,$2,$11
addu	$3,$3,$2
sw	$9,0($4)
andi	$2,$2,0x1
sw	$7,4($4)
lbu	$3,128($3)
lw	$7,%got(ff_h264_norm_shift)($28)
sb	$3,0($5)
lw	$3,4($4)
lw	$5,0($4)
addu	$7,$7,$3
lbu	$7,0($7)
sll	$5,$5,$7
sll	$3,$3,$7
andi	$7,$5,0xffff
sw	$5,0($4)
bne	$7,$0,$L15
sw	$3,4($4)

lw	$25,%got(refill2.isra.1)($28)
addiu	$5,$4,16
addiu	$25,$25,%lo(refill2.isra.1)
.reloc	1f,R_MIPS_JALR,refill2.isra.1
1:	jalr	$25
sw	$2,24($sp)

lw	$2,24($sp)
$L15:
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	get_cabac_noinline
.size	get_cabac_noinline, .-get_cabac_noinline
.section	.text.decode_cabac_mb_skip_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_skip_hw
.type	decode_cabac_mb_skip_hw, @function
decode_cabac_mb_skip_hw:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,65536			# 0x10000
addu	$10,$4,$2
lw	$2,-5252($10)
beq	$2,$0,$L18
move	$3,$4

lw	$12,168($4)
li	$2,-2			# 0xfffffffffffffffe
andi	$4,$6,0x1
and	$2,$6,$2
mul	$7,$2,$12
addu	$2,$7,$5
beq	$4,$0,$L19
addiu	$7,$2,-1

lw	$4,-5276($10)
move	$11,$7
lw	$8,-5272($10)
addu	$7,$4,$7
lbu	$7,0($7)
beq	$7,$8,$L36
sll	$14,$11,2

lw	$9,-5248($10)
$L21:
beq	$9,$0,$L38
addiu	$9,$6,-1

subu	$6,$2,$12
addu	$4,$4,$6
lbu	$4,0($4)
$L25:
beq	$8,$7,$L23
nop

$L34:
beq	$8,$4,$L24
move	$2,$0

$L27:
li	$4,65536			# 0x10000
lw	$25,%got(get_cabac_noinline)($28)
addiu	$7,$2,13
addu	$4,$3,$4
li	$5,131072			# 0x20000
addiu	$25,$25,%lo(get_cabac_noinline)
lw	$6,-5264($4)
addiu	$4,$5,8224
addu	$4,$3,$4
xori	$6,$6,0x3
movz	$2,$7,$6
addu	$2,$2,$5
addiu	$5,$2,8283
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jr	$25
addu	$5,$3,$5

$L18:
lw	$7,168($4)
lw	$2,10384($4)
lw	$4,-5276($10)
mul	$8,$7,$6
xori	$2,$2,0x3
sltu	$2,$0,$2
sll	$2,$7,$2
addu	$6,$8,$5
lw	$8,-5272($10)
addiu	$11,$6,-1
subu	$6,$6,$2
addu	$5,$4,$11
addu	$4,$4,$6
lbu	$7,0($5)
bne	$8,$7,$L34
lbu	$4,0($4)

$L23:
lw	$5,2192($3)
sll	$2,$11,2
addu	$2,$5,$2
lw	$2,0($2)
srl	$2,$2,11
xori	$2,$2,0x1
bne	$8,$4,$L27
andi	$2,$2,0x1

$L24:
lw	$4,2192($3)
sll	$6,$6,2
addiu	$7,$2,1
lw	$25,%got(get_cabac_noinline)($28)
li	$5,131072			# 0x20000
addu	$6,$4,$6
addiu	$25,$25,%lo(get_cabac_noinline)
lw	$4,0($6)
andi	$4,$4,0x800
movz	$2,$7,$4
li	$4,65536			# 0x10000
addu	$4,$3,$4
addiu	$7,$2,13
lw	$6,-5264($4)
addiu	$4,$5,8224
addu	$4,$3,$4
xori	$6,$6,0x3
movz	$2,$7,$6
addu	$2,$2,$5
addiu	$5,$2,8283
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jr	$25
addu	$5,$3,$5

$L19:
lw	$4,-5248($10)
beq	$4,$0,$L37
move	$11,$7

lw	$4,-5276($10)
subu	$6,$2,$12
lw	$2,-5272($10)
addu	$5,$4,$6
lbu	$8,0($5)
beq	$8,$2,$L31
nop

addu	$4,$4,$7
lbu	$4,0($4)
beq	$2,$4,$L35
nop

b	$L27
move	$2,$0

$L37:
lw	$4,-5276($10)
lw	$8,-5272($10)
addu	$2,$4,$7
lbu	$7,0($2)
addiu	$9,$6,-1
$L38:
mul	$2,$12,$9
addu	$6,$2,$5
addu	$4,$4,$6
b	$L25
lbu	$4,0($4)

$L36:
lw	$13,2192($3)
lw	$9,-5248($10)
addu	$10,$13,$14
lw	$10,0($10)
srl	$10,$10,7
andi	$10,$10,0x1
bne	$9,$10,$L21
nop

addu	$11,$12,$11
addu	$7,$4,$11
b	$L21
lbu	$7,0($7)

$L31:
lw	$2,2192($3)
sll	$5,$6,2
addu	$2,$2,$5
lw	$2,0($2)
andi	$2,$2,0x80
bne	$2,$0,$L22
addu	$2,$4,$7

addu	$4,$4,$7
lbu	$2,0($4)
beq	$8,$2,$L33
move	$11,$7

b	$L24
move	$2,$0

$L22:
subu	$6,$6,$12
addu	$5,$4,$6
move	$11,$7
lbu	$7,0($2)
b	$L25
lbu	$4,0($5)

$L33:
b	$L23
move	$4,$8

$L35:
move	$4,$8
b	$L23
move	$8,$2

.set	macro
.set	reorder
.end	decode_cabac_mb_skip_hw
.size	decode_cabac_mb_skip_hw, .-decode_cabac_mb_skip_hw
.section	.text.decode_cabac_field_decoding_flag_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_field_decoding_flag_hw
.type	decode_cabac_field_decoding_flag_hw, @function
decode_cabac_field_decoding_flag_hw:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,7996($4)
li	$2,-2			# 0xfffffffffffffffe
lw	$6,168($4)
li	$7,65536			# 0x10000
lw	$8,7992($4)
move	$5,$4
and	$3,$3,$2
mul	$2,$6,$3
addu	$7,$4,$7
addiu	$3,$8,-1
sll	$6,$6,1
lw	$4,-5276($7)
lw	$7,-5272($7)
addu	$3,$3,$2
subu	$2,$2,$6
addu	$6,$4,$3
lbu	$6,0($6)
beq	$6,$7,$L45
addu	$2,$2,$8

move	$3,$0
$L40:
addu	$4,$4,$2
lbu	$4,0($4)
bne	$7,$4,$L41
addiu	$6,$3,1

lw	$4,2192($5)
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,0($2)
andi	$2,$2,0x80
movn	$3,$6,$2
$L41:
li	$4,131072			# 0x20000
lw	$25,%got(get_cabac_noinline)($28)
addu	$3,$3,$4
addiu	$4,$4,8224
addiu	$3,$3,8342
addu	$4,$5,$4
addiu	$25,$25,%lo(get_cabac_noinline)
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jr	$25
addu	$5,$5,$3

$L45:
lw	$6,2192($5)
sll	$3,$3,2
addu	$3,$6,$3
lw	$3,0($3)
srl	$3,$3,7
b	$L40
andi	$3,$3,0x1

.set	macro
.set	reorder
.end	decode_cabac_field_decoding_flag_hw
.size	decode_cabac_field_decoding_flag_hw, .-decode_cabac_field_decoding_flag_hw
.section	.text.decode_cabac_intra_mb_type_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_intra_mb_type_hw
.type	decode_cabac_intra_mb_type_hw, @function
decode_cabac_intra_mb_type_hw:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$16,24($sp)
li	$16,131072			# 0x20000
sw	$17,28($sp)
addiu	$17,$16,8272
.cprestore	16
sw	$19,36($sp)
move	$19,$6
addu	$5,$5,$17
sw	$18,32($sp)
sw	$31,44($sp)
move	$18,$4
sw	$20,40($sp)
beq	$6,$0,$L47
addu	$17,$4,$5

li	$3,65536			# 0x10000
lw	$5,10780($4)
addu	$3,$18,$3
lw	$2,-5276($3)
lw	$3,-5272($3)
addu	$6,$2,$5
lbu	$6,0($6)
beq	$6,$3,$L67
lw	$4,10772($4)

move	$5,$0
$L48:
addu	$2,$2,$4
lbu	$2,0($2)
beq	$3,$2,$L68
sll	$4,$4,2

$L49:
li	$16,131072			# 0x20000
lw	$20,%got(get_cabac_noinline)($28)
addu	$5,$17,$5
addiu	$16,$16,8224
addiu	$20,$20,%lo(get_cabac_noinline)
addu	$16,$18,$16
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$16

beq	$2,$0,$L53
addiu	$17,$17,2

li	$2,131072			# 0x20000
$L71:
addu	$18,$18,$2
lw	$5,8228($18)
lw	$4,8224($18)
addiu	$3,$5,-2
sll	$2,$3,17
slt	$2,$4,$2
bne	$2,$0,$L69
sw	$3,8228($18)

lw	$4,8240($18)
lw	$3,8236($18)
beq	$4,$3,$L57
li	$2,25			# 0x19

lw	$31,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L47:
lw	$20,%got(get_cabac_noinline)($28)
addiu	$16,$16,8224
move	$5,$17
addu	$16,$4,$16
addiu	$20,$20,%lo(get_cabac_noinline)
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$16

bne	$2,$0,$L71
li	$2,131072			# 0x20000

$L53:
lw	$31,44($sp)
move	$2,$0
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L68:
lw	$2,2192($18)
addiu	$3,$5,1
addu	$4,$2,$4
lw	$2,0($4)
andi	$2,$2,0x1
b	$L49
movz	$5,$3,$2

$L67:
lw	$6,2192($18)
sll	$5,$5,2
addu	$5,$6,$5
lw	$5,0($5)
andi	$5,$5,0x1
b	$L48
xori	$5,$5,0x1

$L69:
addiu	$5,$5,-258
srl	$5,$5,31
sll	$4,$4,$5
sll	$3,$3,$5
andi	$2,$4,0xffff
sw	$4,8224($18)
bne	$2,$0,$L57
sw	$3,8228($18)

lw	$3,8240($18)
addiu	$6,$3,2
lbu	$2,0($3)
lbu	$5,1($3)
sw	$6,8240($18)
sll	$3,$2,9
sll	$2,$5,1
addu	$2,$3,$2
addu	$4,$4,$2
li	$2,-65536			# 0xffffffffffff0000
addiu	$2,$2,1
addu	$4,$4,$2
sw	$4,8224($18)
$L57:
addiu	$5,$17,1
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$16

addiu	$5,$17,2
sll	$18,$2,2
sll	$2,$2,4
move	$4,$16
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
subu	$18,$2,$18

bne	$2,$0,$L70
addiu	$18,$18,1

$L56:
addiu	$5,$19,3
sll	$19,$19,1
addu	$5,$17,$5
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$16

addiu	$5,$19,3
sll	$2,$2,1
addu	$5,$17,$5
move	$4,$16
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$18,$2,$18

lw	$31,44($sp)
addu	$2,$2,$18
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L70:
addiu	$5,$19,2
move	$4,$16
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$17,$5

addiu	$2,$2,1
sll	$2,$2,2
b	$L56
addu	$18,$18,$2

.set	macro
.set	reorder
.end	decode_cabac_intra_mb_type_hw
.size	decode_cabac_intra_mb_type_hw, .-decode_cabac_intra_mb_type_hw
.section	.text.get_cabac,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_cabac
.type	get_cabac, @function
get_cabac:
.frame	$sp,40,$31		# vars= 8, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$7,4($4)
addiu	$sp,$sp,-40
lw	$8,%got(ff_h264_lps_range)($28)
.cprestore	16
sw	$31,36($sp)
andi	$3,$7,0xc0
lbu	$11,0($5)
sll	$3,$3,1
lw	$9,0($4)
addu	$3,$3,$11
addu	$3,$8,$3
lbu	$8,0($3)
subu	$7,$7,$8
sll	$10,$7,17
subu	$3,$10,$9
sra	$2,$3,31
lw	$3,%got(ff_h264_mlps_state)($28)
movn	$7,$8,$2
and	$10,$2,$10
subu	$9,$9,$10
xor	$2,$2,$11
addu	$3,$3,$2
sw	$9,0($4)
andi	$2,$2,0x1
sw	$7,4($4)
lbu	$3,128($3)
lw	$7,%got(ff_h264_norm_shift)($28)
sb	$3,0($5)
lw	$3,4($4)
lw	$5,0($4)
addu	$7,$7,$3
lbu	$7,0($7)
sll	$5,$5,$7
sll	$3,$3,$7
andi	$7,$5,0xffff
sw	$5,0($4)
bne	$7,$0,$L73
sw	$3,4($4)

lw	$25,%got(refill2.isra.1)($28)
addiu	$5,$4,16
addiu	$25,$25,%lo(refill2.isra.1)
.reloc	1f,R_MIPS_JALR,refill2.isra.1
1:	jalr	$25
sw	$2,24($sp)

lw	$2,24($sp)
$L73:
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	get_cabac
.size	get_cabac, .-get_cabac
.section	.text.decode_cabac_mb_intra4x4_pred_mode_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_intra4x4_pred_mode_hw
.type	decode_cabac_mb_intra4x4_pred_mode_hw, @function
decode_cabac_mb_intra4x4_pred_mode_hw:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$18,32($sp)
li	$18,131072			# 0x20000
sw	$16,24($sp)
sw	$19,36($sp)
addiu	$19,$18,8224
sw	$20,40($sp)
addiu	$3,$18,8340
lw	$16,%got(get_cabac)($28)
addu	$19,$4,$19
move	$20,$5
sw	$17,28($sp)
addu	$5,$4,$3
.cprestore	16
addiu	$16,$16,%lo(get_cabac)
sw	$31,44($sp)
move	$17,$4
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$19

beq	$2,$0,$L80
move	$2,$20

$L76:
lw	$31,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

$L80:
addiu	$18,$18,8341
move	$4,$19
addu	$17,$17,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$17

move	$5,$17
move	$4,$19
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$18,$2

sll	$2,$2,1
move	$5,$17
move	$4,$19
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
addu	$17,$18,$2

sll	$2,$2,2
addu	$2,$2,$17
slt	$20,$2,$20
bne	$20,$0,$L76
lw	$31,44($sp)

addiu	$2,$2,1
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	decode_cabac_mb_intra4x4_pred_mode_hw
.size	decode_cabac_mb_intra4x4_pred_mode_hw, .-decode_cabac_mb_intra4x4_pred_mode_hw
.section	.text.decode_cabac_residual_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_residual_hw
.type	decode_cabac_residual_hw, @function
decode_cabac_residual_hw:
.frame	$sp,344,$31		# vars= 280, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-344
lw	$3,7992($4)
li	$2,5			# 0x5
sw	$6,352($sp)
.cprestore	16
sw	$fp,336($sp)
sw	$31,340($sp)
sw	$23,332($sp)
sw	$22,328($sp)
sw	$21,324($sp)
sw	$20,320($sp)
sw	$19,316($sp)
sw	$18,312($sp)
sw	$17,308($sp)
sw	$16,304($sp)
sw	$4,344($sp)
sw	$5,348($sp)
sw	$3,284($sp)
sw	$7,356($sp)
lw	$6,168($4)
lw	$25,352($sp)
lw	$fp,7996($4)
beq	$25,$2,$L82
sw	$6,296($sp)

li	$2,131072			# 0x20000
addiu	$17,$2,8224
bne	$25,$0,$L83
addu	$17,$4,$17

addu	$3,$4,$2
lw	$2,8744($3)
lw	$3,8740($3)
andi	$2,$2,0x100
andi	$3,$3,0x100
$L84:
sltu	$2,$0,$2
lw	$6,352($sp)
addiu	$4,$2,2
lw	$20,%got(get_cabac)($28)
li	$16,131072			# 0x20000
lw	$25,344($sp)
movn	$2,$4,$3
sll	$19,$6,2
addiu	$16,$16,8272
addiu	$20,$20,%lo(get_cabac)
addu	$5,$19,$2
addiu	$5,$5,85
addu	$5,$5,$16
addu	$5,$25,$5
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$17

bne	$2,$0,$L88
lw	$28,16($sp)

lw	$3,352($sp)
addiu	$2,$3,-1
sltu	$2,$2,2
bne	$2,$0,$L172
li	$2,4			# 0x4

beq	$3,$2,$L173
lw	$2,%got(scan8)($28)

$L81:
lw	$31,340($sp)
lw	$fp,336($sp)
lw	$23,332($sp)
lw	$22,328($sp)
lw	$21,324($sp)
lw	$20,320($sp)
lw	$19,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
j	$31
addiu	$sp,$sp,344

$L83:
lw	$6,352($sp)
addiu	$3,$6,-1
sltu	$3,$3,2
bne	$3,$0,$L174
lw	$7,356($sp)

li	$3,3			# 0x3
beq	$6,$3,$L175
lw	$25,344($sp)

lw	$2,%got(scan8)($28)
addiu	$2,$2,%lo(scan8)
addu	$2,$7,$2
lbu	$3,16($2)
addu	$3,$25,$3
lbu	$2,11183($3)
b	$L84
lbu	$3,11176($3)

$L82:
lw	$25,344($sp)
li	$3,65536			# 0x10000
li	$2,131072			# 0x20000
lw	$18,%got(significant_coeff_flag_offset_8x8.7986)($28)
lw	$20,%got(get_cabac)($28)
move	$16,$0
addu	$3,$25,$3
lw	$23,%got(last_coeff_flag_offset_8x8_hw.7987)($28)
addiu	$22,$2,8272
addiu	$17,$2,8224
lw	$3,-5248($3)
addiu	$18,$18,%lo(significant_coeff_flag_offset_8x8.7986)
addu	$17,$25,$17
move	$fp,$0
sll	$5,$3,3
sll	$2,$3,5
sll	$4,$3,6
subu	$2,$2,$5
lw	$5,%got(last_coeff_flag_offset.7984)($28)
subu	$4,$4,$3
lw	$3,%got(significant_coeff_flag_offset.7983)($28)
addiu	$20,$20,%lo(get_cabac)
addiu	$5,$5,%lo(last_coeff_flag_offset.7984)
addiu	$3,$3,%lo(significant_coeff_flag_offset.7983)
addu	$18,$18,$4
addu	$3,$2,$3
addu	$2,$2,$5
addiu	$23,$23,%lo(last_coeff_flag_offset_8x8_hw.7987)
lw	$19,20($3)
addiu	$3,$sp,24
lw	$2,20($2)
li	$21,63			# 0x3f
addu	$19,$19,$22
sw	$3,284($sp)
addu	$22,$2,$22
addu	$19,$25,$19
addu	$22,$25,$22
b	$L93
sw	$22,280($sp)

$L91:
addiu	$fp,$fp,1
beq	$fp,$21,$L191
lw	$3,368($sp)

$L93:
addu	$2,$18,$fp
move	$4,$17
move	$25,$20
lbu	$5,0($2)
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
addu	$5,$19,$5

beq	$2,$0,$L91
lw	$28,16($sp)

addu	$5,$23,$fp
lw	$6,280($sp)
sll	$2,$16,2
addiu	$3,$sp,24
lbu	$5,0($5)
move	$4,$17
addu	$2,$3,$2
addiu	$22,$16,1
addu	$5,$6,$5
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sw	$fp,0($2)

move	$16,$22
beq	$2,$0,$L91
lw	$28,16($sp)

lw	$fp,368($sp)
move	$16,$22
lw	$3,368($sp)
$L191:
addiu	$2,$3,-1
beq	$2,$fp,$L176
sll	$2,$16,2

$L94:
li	$2,131072			# 0x20000
lw	$25,344($sp)
addiu	$2,$2,8698
addu	$2,$25,$2
sw	$2,288($sp)
lw	$2,%got(scan8)($28)
$L197:
sll	$3,$16,8
lw	$6,356($sp)
lw	$25,344($sp)
addu	$3,$16,$3
addiu	$2,$2,%lo(scan8)
andi	$3,$3,0xffff
addu	$2,$6,$2
lbu	$2,0($2)
addiu	$2,$2,11184
addu	$2,$25,$2
sh	$3,0($2)
sh	$3,8($2)
$L102:
lw	$2,%got(sde_tmp_blk)($28)
addiu	$3,$2,128
$L100:
sh	$0,0($2)
addiu	$2,$2,2
bne	$2,$3,$L100
nop

lw	$3,352($sp)
bne	$3,$0,$L105
lw	$6,352($sp)

lw	$2,%got(sde_nnz_dct)($28)
sw	$16,96($2)
$L106:
addiu	$22,$16,-1
bltz	$22,$L192
lw	$8,%got(sde_res_di)($28)

$L108:
li	$7,131072			# 0x20000
lw	$25,344($sp)
li	$17,131072			# 0x20000
addiu	$3,$7,8240
sll	$2,$22,2
addiu	$17,$17,8224
addiu	$23,$sp,24
li	$6,1			# 0x1
addu	$3,$25,$3
addu	$17,$25,$17
addu	$23,$23,$2
sw	$6,280($sp)
move	$fp,$0
sw	$3,292($sp)
$L134:
bne	$fp,$0,$L115
move	$5,$0

lw	$7,280($sp)
li	$5,4			# 0x4
slt	$2,$7,5
movn	$5,$7,$2
$L115:
lw	$2,0($23)
lw	$3,360($sp)
lw	$25,288($sp)
lw	$6,352($sp)
addu	$2,$3,$2
addu	$5,$25,$5
lbu	$18,0($2)
bne	$6,$0,$L116
sra	$2,$18,4

sll	$3,$2,2
lw	$2,%got(y_dc_table)($28)
addu	$2,$2,$3
lw	$2,0($2)
sw	$2,284($sp)
$L117:
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$17

bne	$2,$0,$L118
lw	$28,16($sp)

lw	$3,364($sp)
beq	$3,$0,$L177
lw	$3,344($sp)

li	$4,131072			# 0x20000
lw	$6,348($sp)
sll	$16,$18,1
addu	$19,$3,$4
addu	$16,$6,$16
lw	$2,8224($19)
sll	$2,$2,1
andi	$3,$2,0xffff
beq	$3,$0,$L178
sw	$2,8224($19)

$L122:
lw	$25,344($sp)
li	$3,131072			# 0x20000
lw	$6,284($sp)
sll	$18,$18,2
addu	$7,$25,$3
lw	$25,%got(sde_tmp_blk)($28)
sll	$5,$6,1
lw	$3,8228($7)
addu	$5,$25,$5
lw	$25,364($sp)
sll	$3,$3,17
addu	$18,$25,$18
subu	$2,$2,$3
sra	$6,$2,31
and	$3,$6,$3
nor	$4,$0,$6
addu	$2,$2,$3
subu	$4,$4,$6
sw	$2,8224($7)
sh	$4,0($16)
sh	$4,0($5)
lh	$2,0($16)
lw	$3,0($18)
mul	$2,$2,$3
addiu	$2,$2,32
srl	$2,$2,6
sh	$2,0($16)
$L121:
lw	$3,280($sp)
addiu	$3,$3,1
sw	$3,280($sp)
$L123:
addiu	$22,$22,-1
li	$2,-1			# 0xffffffffffffffff
bne	$22,$2,$L134
addiu	$23,$23,-4

lw	$25,352($sp)
$L190:
li	$2,3			# 0x3
beq	$25,$2,$L109
lw	$6,352($sp)

$L189:
li	$2,5			# 0x5
beq	$6,$2,$L179
lw	$8,%got(sde_res_di)($28)

$L192:
move	$2,$0
lw	$5,%got(sde_res_dct)($28)
li	$7,32			# 0x20
lw	$9,0($8)
sll	$6,$9,1
$L140:
lw	$3,%got(sde_tmp_blk)($28)
addu	$4,$3,$2
addu	$3,$2,$6
addiu	$2,$2,2
lhu	$4,0($4)
addu	$3,$5,$3
bne	$2,$7,$L140
sh	$4,0($3)

lw	$31,340($sp)
addiu	$9,$9,16
lw	$fp,336($sp)
lw	$23,332($sp)
lw	$22,328($sp)
lw	$21,324($sp)
lw	$20,320($sp)
lw	$19,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
sw	$9,0($8)
j	$31
addiu	$sp,$sp,344

$L88:
lw	$3,%got(coeff_abs_level_m1_offset.7985)($28)
li	$2,65536			# 0x10000
lw	$25,344($sp)
lw	$4,368($sp)
addiu	$3,$3,%lo(coeff_abs_level_m1_offset.7985)
lw	$6,352($sp)
addu	$2,$25,$2
addu	$19,$19,$3
addiu	$18,$4,-1
lw	$2,-5248($2)
lw	$3,0($19)
sll	$4,$2,1
addu	$3,$3,$16
sll	$2,$2,3
addu	$3,$25,$3
subu	$2,$2,$4
lw	$4,%got(last_coeff_flag_offset.7984)($28)
sw	$3,288($sp)
addu	$2,$2,$6
lw	$3,%got(significant_coeff_flag_offset.7983)($28)
addiu	$4,$4,%lo(last_coeff_flag_offset.7984)
sll	$2,$2,2
addiu	$3,$3,%lo(significant_coeff_flag_offset.7983)
addu	$3,$2,$3
addu	$2,$2,$4
lw	$19,0($3)
lw	$21,0($2)
addu	$19,$19,$16
addu	$16,$21,$16
addu	$19,$25,$19
blez	$18,$L146
addu	$21,$25,$16

sw	$18,280($sp)
move	$16,$0
move	$23,$0
b	$L97
addiu	$22,$sp,24

$L95:
addiu	$23,$23,1
beq	$23,$18,$L193
lw	$3,280($sp)

$L97:
addu	$5,$19,$23
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$17

beq	$2,$0,$L95
lw	$28,16($sp)

sll	$2,$16,2
addu	$5,$21,$23
addu	$2,$22,$2
move	$4,$17
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sw	$23,0($2)

addiu	$16,$16,1
beq	$2,$0,$L95
lw	$28,16($sp)

lw	$2,368($sp)
sw	$2,280($sp)
$L96:
lw	$3,280($sp)
$L193:
beq	$3,$18,$L180
sll	$2,$16,2

$L98:
lw	$3,296($sp)
lw	$6,284($sp)
lw	$25,352($sp)
mul	$7,$fp,$3
bne	$25,$0,$L99
addu	$23,$7,$6

lw	$3,344($sp)
li	$2,131072			# 0x20000
sll	$23,$23,1
addu	$2,$3,$2
lw	$2,8732($2)
addu	$23,$2,$23
lhu	$2,0($23)
ori	$2,$2,0x100
b	$L102
sh	$2,0($23)

$L118:
li	$19,4			# 0x4
lw	$6,288($sp)
slt	$2,$fp,5
movn	$19,$fp,$2
li	$16,2			# 0x2
li	$21,15			# 0xf
addiu	$19,$19,5
addu	$19,$6,$19
move	$4,$17
$L194:
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$19

beq	$2,$0,$L181
lw	$28,16($sp)

addiu	$16,$16,1
bne	$16,$21,$L194
move	$4,$17

lw	$3,%got(get_cabac_bypass)($28)
move	$21,$0
b	$L125
addiu	$19,$3,%lo(get_cabac_bypass)

$L125:
$L195:
move	$25,$19
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$17

bne	$2,$0,$L195
addiu	$21,$21,1

addiu	$21,$21,-1
beq	$21,$0,$L196
move	$25,$19

li	$16,1			# 0x1
$L130:
move	$25,$19
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$17

addiu	$21,$21,-1
addu	$2,$2,$16
bne	$21,$0,$L130
addu	$16,$16,$2

addiu	$16,$16,14
$L127:
move	$25,$19
$L196:
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$17

beq	$2,$0,$L131
lw	$28,16($sp)

subu	$16,$0,$16
lw	$6,348($sp)
sll	$3,$18,1
sll	$16,$16,16
addu	$3,$6,$3
sra	$16,$16,16
sh	$16,0($3)
$L132:
lw	$6,284($sp)
lw	$7,%got(sde_tmp_blk)($28)
lw	$25,364($sp)
sll	$2,$6,1
addu	$2,$7,$2
beq	$25,$0,$L133
sh	$16,0($2)

sll	$18,$18,2
lh	$2,0($3)
addu	$18,$25,$18
lw	$4,0($18)
mul	$2,$2,$4
addiu	$2,$2,32
srl	$2,$2,6
sh	$2,0($3)
$L133:
b	$L123
addiu	$fp,$fp,1

$L116:
lw	$25,352($sp)
xori	$3,$25,0x3
movn	$2,$18,$3
b	$L117
sw	$2,284($sp)

$L131:
lw	$25,348($sp)
sll	$16,$16,16
sll	$3,$18,1
sra	$16,$16,16
addu	$3,$25,$3
b	$L132
sh	$16,0($3)

$L181:
lw	$2,%got(get_cabac_bypass)($28)
b	$L127
addiu	$19,$2,%lo(get_cabac_bypass)

$L178:
lw	$7,%got(refill.isra.0)($28)
move	$4,$17
addiu	$25,$7,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
lw	$5,292($sp)

lw	$28,16($sp)
b	$L122
lw	$2,8224($19)

$L177:
lw	$25,344($sp)
li	$2,131072			# 0x20000
lw	$3,348($sp)
sll	$18,$18,1
addu	$16,$25,$2
addu	$18,$3,$18
lw	$2,8224($16)
sll	$2,$2,1
andi	$3,$2,0xffff
beq	$3,$0,$L182
sw	$2,8224($16)

$L120:
lw	$25,344($sp)
li	$3,131072			# 0x20000
lw	$6,284($sp)
addu	$7,$25,$3
lw	$25,%got(sde_tmp_blk)($28)
sll	$5,$6,1
lw	$3,8228($7)
addu	$5,$25,$5
sll	$3,$3,17
subu	$2,$2,$3
sra	$6,$2,31
and	$3,$6,$3
nor	$4,$0,$6
subu	$4,$4,$6
addu	$2,$2,$3
sw	$2,8224($7)
sh	$4,0($18)
b	$L121
sh	$4,0($5)

$L182:
lw	$4,%got(refill.isra.0)($28)
lw	$5,292($sp)
addiu	$25,$4,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
move	$4,$17

lw	$28,16($sp)
b	$L120
lw	$2,8224($16)

$L105:
addiu	$2,$6,-1
sltu	$2,$2,2
bne	$2,$0,$L183
lw	$3,352($sp)

li	$2,3			# 0x3
beq	$3,$2,$L184
lw	$25,352($sp)

li	$2,4			# 0x4
beq	$25,$2,$L185
lw	$6,356($sp)

addiu	$22,$16,-1
lw	$5,%got(sde_nnz_dct)($28)
addiu	$4,$6,3
addiu	$3,$6,2
addiu	$2,$6,1
sll	$4,$4,2
sll	$3,$3,2
sll	$2,$2,2
sll	$6,$6,2
addu	$4,$5,$4
addu	$3,$5,$3
addu	$2,$5,$2
sw	$16,0($4)
addu	$5,$5,$6
sw	$16,0($3)
sw	$16,0($2)
bgez	$22,$L108
sw	$16,0($5)

b	$L189
lw	$6,352($sp)

$L99:
lw	$6,352($sp)
addiu	$2,$6,-1
sltu	$2,$2,2
bne	$2,$0,$L186
lw	$3,352($sp)

li	$2,3			# 0x3
beq	$3,$2,$L187
lw	$25,352($sp)

li	$2,4			# 0x4
bne	$25,$2,$L197
lw	$2,%got(scan8)($28)

lw	$3,356($sp)
lw	$25,344($sp)
addiu	$2,$2,%lo(scan8)
addu	$2,$3,$2
lbu	$2,16($2)
addu	$2,$25,$2
b	$L102
sb	$16,11184($2)

$L184:
lw	$6,356($sp)
addiu	$22,$16,-1
lw	$3,%got(sde_nnz_dct)($28)
addiu	$2,$6,25
sll	$2,$2,2
addu	$2,$3,$2
bgez	$22,$L108
sw	$16,0($2)

$L109:
lw	$7,356($sp)
li	$2,1			# 0x1
beq	$7,$2,$L188
lw	$8,%got(sde_res_di)($28)

move	$2,$0
lw	$5,%got(sde_res_dct)($28)
li	$9,8			# 0x8
lw	$4,0($8)
sll	$7,$4,1
$L137:
lw	$3,%got(sde_tmp_blk)($28)
addu	$6,$3,$2
addu	$3,$2,$7
addiu	$2,$2,2
lhu	$6,0($6)
addu	$3,$5,$3
bne	$2,$9,$L137
sh	$6,0($3)

lw	$31,340($sp)
addiu	$4,$4,4
lw	$fp,336($sp)
lw	$23,332($sp)
lw	$22,328($sp)
lw	$21,324($sp)
lw	$20,320($sp)
lw	$19,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
sw	$4,0($8)
j	$31
addiu	$sp,$sp,344

$L185:
lw	$3,356($sp)
addiu	$2,$3,16
sll	$3,$2,2
lw	$2,%got(sde_nnz_dct)($28)
addu	$2,$2,$3
b	$L106
sw	$16,0($2)

$L176:
addiu	$16,$16,1
addu	$2,$sp,$2
b	$L94
sw	$fp,24($2)

$L188:
lw	$2,%got(sde_tmp_blk)($28)
lhu	$6,0($2)
lhu	$5,2($2)
lhu	$4,4($2)
lhu	$3,6($2)
lw	$2,%got(sde_v_dc)($28)
sh	$6,0($2)
sh	$5,2($2)
sh	$4,4($2)
b	$L81
sh	$3,6($2)

$L175:
lw	$6,356($sp)
addu	$3,$25,$2
addiu	$4,$6,6
lw	$2,8744($3)
lw	$3,8740($3)
sra	$2,$2,$4
sra	$3,$3,$4
andi	$2,$2,0x1
b	$L84
andi	$3,$3,0x1

$L173:
lw	$6,356($sp)
lw	$25,344($sp)
addiu	$2,$2,%lo(scan8)
addu	$2,$6,$2
lbu	$2,16($2)
addu	$2,$25,$2
b	$L81
sb	$0,11184($2)

$L180:
addiu	$16,$16,1
addu	$2,$sp,$2
b	$L98
sw	$3,24($2)

$L179:
move	$2,$0
lw	$5,%got(sde_res_dct)($28)
li	$7,128			# 0x80
lw	$9,0($8)
sll	$6,$9,1
lw	$25,%got(sde_tmp_blk)($28)
$L198:
addu	$3,$2,$6
addu	$3,$5,$3
addu	$4,$25,$2
addiu	$2,$2,2
lhu	$4,0($4)
bne	$2,$7,$L198
sh	$4,0($3)

lw	$31,340($sp)
addiu	$9,$9,64
lw	$fp,336($sp)
lw	$23,332($sp)
lw	$22,328($sp)
lw	$21,324($sp)
lw	$20,320($sp)
lw	$19,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
sw	$9,0($8)
j	$31
addiu	$sp,$sp,344

$L183:
lw	$7,356($sp)
addiu	$22,$16,-1
lw	$2,%got(sde_nnz_dct)($28)
sll	$3,$7,2
addu	$2,$2,$3
bgez	$22,$L108
sw	$16,0($2)

b	$L190
lw	$25,352($sp)

$L186:
lw	$2,%got(scan8)($28)
lw	$7,356($sp)
lw	$25,344($sp)
addiu	$2,$2,%lo(scan8)
addu	$2,$7,$2
lbu	$2,0($2)
addu	$2,$25,$2
b	$L102
sb	$16,11184($2)

$L187:
lw	$25,344($sp)
li	$2,131072			# 0x20000
sll	$23,$23,1
lw	$6,356($sp)
addu	$2,$25,$2
lw	$3,8732($2)
li	$2,64			# 0x40
sll	$2,$2,$6
addu	$23,$3,$23
lhu	$3,0($23)
or	$2,$2,$3
b	$L102
sh	$2,0($23)

$L172:
lw	$2,%got(scan8)($28)
lw	$6,356($sp)
lw	$25,344($sp)
addiu	$2,$2,%lo(scan8)
addu	$2,$6,$2
lbu	$2,0($2)
addu	$2,$25,$2
b	$L81
sb	$0,11184($2)

$L174:
lw	$2,%got(scan8)($28)
lw	$25,344($sp)
addiu	$2,$2,%lo(scan8)
addu	$2,$7,$2
lbu	$3,0($2)
addu	$3,$25,$3
lbu	$2,11183($3)
b	$L84
lbu	$3,11176($3)

$L146:
move	$16,$0
b	$L96
sw	$0,280($sp)

.set	macro
.set	reorder
.end	decode_cabac_residual_hw
.size	decode_cabac_residual_hw, .-decode_cabac_residual_hw
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"overflow in decode_cabac_mb_ref\012\000"
.section	.text.decode_cabac_mb_ref_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_ref_hw
.type	decode_cabac_mb_ref_hw, @function
decode_cabac_mb_ref_hw:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$7,%got(scan8)($28)
addiu	$sp,$sp,-64
sll	$3,$5,3
sll	$2,$5,5
sw	$17,28($sp)
addiu	$7,$7,%lo(scan8)
.cprestore	16
move	$17,$4
sw	$31,60($sp)
addu	$6,$7,$6
sw	$fp,56($sp)
addu	$2,$3,$2
sw	$23,52($sp)
li	$4,65536			# 0x10000
sw	$22,48($sp)
lbu	$3,0($6)
addu	$2,$17,$2
addu	$4,$17,$4
sw	$21,44($sp)
sw	$20,40($sp)
addiu	$5,$3,-1
sw	$19,36($sp)
sw	$18,32($sp)
addiu	$3,$3,-8
sw	$16,24($sp)
lw	$6,-5264($4)
addu	$4,$2,$5
addu	$2,$2,$3
lb	$16,11568($4)
li	$4,3			# 0x3
beq	$6,$4,$L211
lb	$2,11568($2)

slt	$16,$0,$16
addiu	$3,$16,2
slt	$2,$0,$2
movn	$16,$3,$2
$L202:
li	$18,131072			# 0x20000
lw	$20,%got(get_cabac)($28)
move	$fp,$0
addiu	$19,$18,8224
addiu	$20,$20,%lo(get_cabac)
addu	$19,$17,$19
addiu	$18,$18,8272
li	$23,4			# 0x4
li	$22,5			# 0x5
b	$L204
li	$21,32			# 0x20

$L206:
move	$2,$22
movn	$2,$23,$16
addiu	$fp,$fp,1
beq	$fp,$21,$L212
move	$16,$2

$L204:
addiu	$5,$16,54
move	$4,$19
addu	$5,$5,$18
move	$25,$20
addu	$5,$17,$5
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
slt	$16,$16,4

bne	$2,$0,$L206
lw	$28,16($sp)

lw	$31,60($sp)
move	$2,$fp
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,64

$L212:
lw	$6,%got($LC0)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)

move	$2,$0
lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,64

$L211:
blez	$16,$L207
addu	$5,$17,$5

li	$4,131072			# 0x20000
addu	$5,$4,$5
lbu	$16,9092($5)
sltu	$16,$16,1
$L201:
blez	$2,$L202
li	$2,131072			# 0x20000

addu	$3,$17,$3
addiu	$4,$16,2
addu	$3,$2,$3
lbu	$2,9092($3)
b	$L202
movz	$16,$4,$2

$L207:
b	$L201
move	$16,$0

.set	macro
.set	reorder
.end	decode_cabac_mb_ref_hw
.size	decode_cabac_mb_ref_hw, .-decode_cabac_mb_ref_hw
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"overflow in decode_cabac_mb_mvd\012\000"
.section	.text.decode_cabac_mb_mvd_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_cabac_mb_mvd_hw
.type	decode_cabac_mb_mvd_hw, @function
decode_cabac_mb_mvd_hw:
.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$2,$5,3
lw	$25,%call16(abs)($28)
sll	$5,$5,5
addiu	$sp,$sp,-64
addu	$5,$2,$5
lw	$2,%got(scan8)($28)
sw	$16,28($sp)
li	$3,65536			# 0x10000
sw	$19,40($sp)
addiu	$2,$2,%lo(scan8)
sw	$21,48($sp)
addiu	$19,$3,4384
sw	$18,36($sp)
addu	$6,$2,$6
.cprestore	16
move	$21,$7
sw	$31,60($sp)
move	$18,$4
sw	$20,44($sp)
lbu	$16,0($6)
li	$20,40			# 0x28
sw	$17,32($sp)
sw	$23,56($sp)
addu	$16,$5,$16
sw	$22,52($sp)
addiu	$2,$16,-1
addiu	$16,$16,-8
sll	$2,$2,1
sll	$16,$16,1
addu	$2,$2,$7
addu	$16,$16,$21
addu	$2,$2,$19
sll	$2,$2,1
addu	$2,$4,$2
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($2)

addu	$3,$16,$19
lw	$28,16($sp)
move	$17,$2
sll	$3,$3,1
addu	$3,$18,$3
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($3)

li	$4,47			# 0x2f
addu	$2,$17,$2
lw	$28,16($sp)
slt	$3,$2,3
bne	$3,$0,$L228
movn	$20,$4,$21

li	$3,1			# 0x1
li	$5,2			# 0x2
slt	$2,$2,33
movn	$5,$3,$2
$L215:
li	$19,131072			# 0x20000
lw	$21,%got(get_cabac)($28)
addu	$5,$5,$20
addiu	$22,$19,8272
addiu	$19,$19,8224
addu	$5,$5,$22
addu	$19,$18,$19
addiu	$21,$21,%lo(get_cabac)
addu	$5,$18,$5
move	$25,$21
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$19

beq	$2,$0,$L216
li	$16,1			# 0x1

li	$17,3			# 0x3
li	$23,9			# 0x9
$L217:
addu	$5,$17,$20
move	$4,$19
addu	$5,$5,$22
move	$25,$21
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
addu	$5,$18,$5

slt	$3,$17,6
beq	$2,$0,$L221
lw	$28,16($sp)

addiu	$16,$16,1
bne	$16,$23,$L217
addu	$17,$17,$3

lw	$20,%got(get_cabac_bypass)($28)
li	$17,3			# 0x3
li	$21,1			# 0x1
li	$22,25			# 0x19
b	$L219
addiu	$20,$20,%lo(get_cabac_bypass)

$L222:
addiu	$17,$17,1
beq	$17,$22,$L239
addu	$16,$16,$3

$L219:
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
move	$4,$19

sll	$3,$21,$17
bne	$2,$0,$L222
lw	$28,16($sp)

li	$21,-1			# 0xffffffffffffffff
li	$22,1			# 0x1
$L223:
addiu	$17,$17,-1
beq	$17,$21,$L221
move	$4,$19

$L225:
move	$25,$20
.reloc	1f,R_MIPS_JALR,get_cabac_bypass
1:	jalr	$25
nop

sll	$3,$22,$17
beq	$2,$0,$L223
lw	$28,16($sp)

addiu	$17,$17,-1
addu	$16,$16,$3
bne	$17,$21,$L225
move	$4,$19

$L221:
li	$5,131072			# 0x20000
subu	$16,$0,$16
addu	$17,$18,$5
lw	$2,8224($17)
sll	$2,$2,1
andi	$3,$2,0xffff
beq	$3,$0,$L240
sw	$2,8224($17)

$L226:
li	$3,131072			# 0x20000
addu	$18,$18,$3
lw	$4,8228($18)
sll	$4,$4,17
subu	$3,$2,$4
sra	$2,$3,31
and	$4,$2,$4
addu	$3,$3,$4
xor	$16,$2,$16
subu	$2,$16,$2
sw	$3,8224($18)
$L216:
lw	$31,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L228:
b	$L215
move	$5,$0

$L239:
lw	$6,%got($LC1)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC1)

li	$2,-2147483648			# 0xffffffff80000000
lw	$31,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L240:
lw	$25,%got(refill.isra.0)($28)
addiu	$5,$5,8240
move	$4,$19
addiu	$25,$25,%lo(refill.isra.0)
.reloc	1f,R_MIPS_JALR,refill.isra.0
1:	jalr	$25
addu	$5,$18,$5

b	$L226
lw	$2,8224($17)

.set	macro
.set	reorder
.end	decode_cabac_mb_mvd_hw
.size	decode_cabac_mb_mvd_hw, .-decode_cabac_mb_mvd_hw
.section	.text.pred_motion_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	pred_motion_hw
.type	pred_motion_hw, @function
pred_motion_hw:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,%got(scan8)($28)
sll	$11,$7,3
sll	$13,$7,5
li	$15,65536			# 0x10000
addiu	$3,$3,%lo(scan8)
addu	$2,$11,$13
addu	$5,$3,$5
addu	$15,$4,$15
addu	$9,$4,$2
lbu	$14,0($5)
addiu	$sp,$sp,-32
lw	$25,-5252($15)
sw	$22,28($sp)
addiu	$10,$14,-8
sw	$21,24($sp)
addiu	$12,$14,-1
sw	$20,20($sp)
addu	$5,$2,$10
sw	$19,16($sp)
addu	$3,$2,$12
sw	$18,12($sp)
addu	$24,$10,$6
sw	$17,8($sp)
addiu	$3,$3,2812
sw	$16,4($sp)
addiu	$5,$5,2812
lw	$8,48($sp)
addu	$10,$9,$10
addu	$12,$9,$12
addu	$9,$9,$24
sll	$3,$3,2
lb	$10,11568($10)
sll	$5,$5,2
lb	$12,11568($12)
addu	$3,$4,$3
lb	$9,11568($9)
beq	$25,$0,$L242
addu	$5,$4,$5

sll	$16,$7,7
lw	$17,2696($4)
addu	$25,$13,$16
addiu	$2,$25,11288
lw	$19,104($17)
addu	$2,$4,$2
sw	$0,0($2)
lw	$18,-5248($15)
bne	$18,$0,$L243
nop

lw	$15,7996($4)
andi	$20,$15,0x1
beq	$20,$0,$L244
slt	$20,$14,20

beq	$20,$0,$L245
li	$20,-2			# 0xfffffffffffffffe

beq	$9,$20,$L272
xori	$21,$14,0xf

lw	$20,168($4)
addiu	$16,$15,-1
lw	$18,7992($4)
sltu	$21,$21,1
mul	$22,$16,$20
addu	$16,$22,$18
addu	$16,$16,$21
sll	$16,$16,2
addu	$16,$19,$16
lw	$16,0($16)
andi	$16,$16,0x80
beq	$16,$0,$L247
sll	$15,$15,2

sll	$18,$18,2
addiu	$15,$15,-1
andi	$9,$14,0x7
sra	$13,$15,2
addu	$9,$18,$9
mul	$14,$20,$13
addiu	$9,$9,-4
addu	$6,$6,$9
sll	$9,$7,1
sra	$11,$6,2
addu	$20,$14,$11
li	$11,12288			# 0x3000
sll	$20,$20,2
sll	$11,$11,$9
addu	$19,$19,$20
ori	$11,$11,0x40
lw	$9,0($19)
and	$11,$11,$9
beq	$11,$0,$L271
addu	$25,$4,$25

lw	$11,11864($4)
lw	$13,11868($4)
sll	$7,$7,2
sra	$9,$15,1
mul	$4,$15,$11
addu	$17,$17,$7
lw	$7,96($17)
addu	$11,$4,$6
sra	$6,$6,1
sll	$11,$11,2
addu	$11,$7,$11
lh	$4,0($11)
sh	$4,11288($25)
lhu	$7,2($11)
sll	$7,$7,1
sh	$7,11290($25)
mul	$7,$9,$13
lw	$4,188($17)
addu	$6,$4,$6
addu	$4,$7,$6
lb	$9,0($4)
b	$L248
sra	$9,$9,1

$L242:
li	$2,-2			# 0xfffffffffffffffe
bne	$9,$2,$L294
addu	$2,$11,$13

$L250:
addiu	$14,$14,-9
addu	$11,$11,$13
xor	$6,$10,$8
addu	$2,$11,$14
addu	$11,$4,$11
addiu	$2,$2,2812
addu	$11,$11,$14
sll	$2,$2,2
sltu	$6,$6,1
lb	$9,11568($11)
addu	$2,$4,$2
xor	$4,$8,$9
sltu	$4,$4,1
xor	$8,$12,$8
addu	$4,$4,$6
sltu	$8,$8,1
addu	$4,$4,$8
slt	$7,$4,2
beq	$7,$0,$L283
nop

$L252:
li	$7,1			# 0x1
beq	$4,$7,$L284
li	$4,-2			# 0xfffffffffffffffe

beq	$10,$4,$L285
nop

$L261:
lh	$6,0($3)
lh	$4,0($5)
slt	$8,$4,$6
beq	$8,$0,$L262
lh	$7,0($2)

slt	$8,$4,$7
bne	$8,$0,$L286
nop

$L263:
lh	$6,2($3)
lw	$22,52($sp)
lh	$3,2($5)
lh	$2,2($2)
sw	$4,0($22)
slt	$4,$3,$6
beq	$4,$0,$L264
slt	$4,$2,$3

slt	$4,$3,$2
beq	$4,$0,$L265
nop

slt	$3,$6,$2
movz	$6,$2,$3
move	$3,$6
$L265:
lw	$2,56($sp)
sw	$3,0($2)
$L292:
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

$L243:
li	$6,-2			# 0xfffffffffffffffe
beq	$9,$6,$L249
nop

$L247:
addu	$2,$11,$13
$L294:
addu	$2,$2,$24
addiu	$2,$2,2812
sll	$2,$2,2
addu	$2,$4,$2
$L248:
xor	$6,$10,$8
xor	$4,$8,$9
sltu	$6,$6,1
sltu	$4,$4,1
xor	$8,$12,$8
addu	$4,$4,$6
sltu	$8,$8,1
addu	$4,$4,$8
slt	$7,$4,2
bne	$7,$0,$L252
nop

$L283:
lh	$6,0($3)
lh	$4,0($5)
slt	$8,$4,$6
beq	$8,$0,$L253
lh	$7,0($2)

slt	$8,$4,$7
bne	$8,$0,$L287
nop

$L254:
lh	$6,2($3)
lw	$14,52($sp)
lh	$3,2($5)
lh	$2,2($2)
sw	$4,0($14)
slt	$4,$3,$6
beq	$4,$0,$L295
slt	$4,$2,$3

slt	$4,$3,$2
$L297:
beq	$4,$0,$L296
lw	$15,56($sp)

slt	$3,$6,$2
movz	$6,$2,$3
move	$3,$6
$L296:
sw	$3,0($15)
$L241:
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

$L253:
slt	$8,$7,$4
beq	$8,$0,$L254
lw	$14,52($sp)

slt	$4,$7,$6
movz	$6,$7,$4
lh	$2,2($2)
move	$4,$6
lh	$6,2($3)
lh	$3,2($5)
sw	$4,0($14)
slt	$4,$3,$6
bne	$4,$0,$L297
slt	$4,$3,$2

slt	$4,$2,$3
$L295:
beq	$4,$0,$L296
lw	$15,56($sp)

slt	$3,$2,$6
movz	$6,$2,$3
b	$L296
move	$3,$6

$L287:
slt	$4,$6,$7
movz	$6,$7,$4
b	$L254
move	$4,$6

$L244:
li	$6,-2			# 0xfffffffffffffffe
bne	$9,$6,$L247
slt	$6,$14,20

b	$L293
nop

$L249:
lw	$15,7996($4)
andi	$6,$15,0x1
bne	$6,$0,$L272
slt	$6,$14,20

$L293:
bne	$6,$0,$L250
move	$25,$0

b	$L298
li	$6,4			# 0x4

$L284:
bne	$8,$0,$L289
lw	$20,52($sp)

bne	$6,$0,$L290
lw	$14,56($sp)

lh	$3,0($2)
lh	$2,2($2)
lw	$7,52($sp)
sw	$3,0($7)
b	$L241
sw	$2,0($14)

$L245:
li	$6,-2			# 0xfffffffffffffffe
bne	$9,$6,$L247
nop

$L272:
li	$25,1			# 0x1
li	$6,4			# 0x4
$L298:
andi	$20,$14,0x7
bne	$20,$6,$L250
addu	$6,$11,$13

li	$20,-2			# 0xfffffffffffffffe
addu	$6,$4,$6
lb	$6,11579($6)
beq	$6,$20,$L250
nop

lw	$6,10780($4)
sll	$6,$6,2
addu	$6,$19,$6
lw	$6,0($6)
beq	$18,$0,$L291
andi	$6,$6,0x80

bne	$6,$0,$L250
slt	$6,$14,20

bne	$6,$0,$L250
and	$6,$15,$20

addiu	$11,$14,-12
lw	$14,168($4)
lw	$9,7992($4)
sll	$6,$6,2
sra	$11,$11,3
addiu	$6,$6,-1
sll	$11,$11,1
sll	$9,$9,2
addu	$6,$6,$11
addiu	$9,$9,-1
sra	$15,$6,2
sra	$11,$9,2
mul	$20,$15,$14
sll	$18,$7,1
addu	$14,$20,$11
li	$11,12288			# 0x3000
sll	$14,$14,2
sll	$11,$11,$18
addu	$19,$19,$14
ori	$11,$11,0x40
lw	$14,0($19)
and	$11,$11,$14
beq	$11,$0,$L271
sll	$7,$7,2

lw	$11,11864($4)
addu	$13,$13,$16
lw	$14,11868($4)
addu	$17,$17,$7
mul	$15,$6,$11
addu	$4,$4,$13
lw	$11,96($17)
sra	$6,$6,1
addu	$7,$15,$9
sra	$9,$9,1
sll	$7,$7,2
addu	$7,$11,$7
lh	$11,0($7)
sh	$11,11288($4)
lh	$7,2($7)
sra	$7,$7,1
sh	$7,11290($4)
mul	$7,$6,$14
lw	$4,188($17)
addu	$9,$4,$9
addu	$4,$7,$9
lb	$9,0($4)
b	$L248
sll	$9,$9,1

$L264:
beq	$4,$0,$L265
nop

slt	$3,$2,$6
movz	$6,$2,$3
lw	$2,56($sp)
move	$3,$6
b	$L292
sw	$3,0($2)

$L262:
slt	$8,$7,$4
beq	$8,$0,$L263
nop

slt	$4,$7,$6
movz	$6,$7,$4
b	$L263
move	$4,$6

$L285:
bne	$9,$10,$L261
nop

beq	$12,$9,$L261
lw	$15,52($sp)

lh	$4,0($3)
lh	$2,2($3)
lw	$20,56($sp)
sw	$4,0($15)
b	$L241
sw	$2,0($20)

$L286:
slt	$4,$6,$7
movz	$6,$7,$4
b	$L263
move	$4,$6

$L290:
lh	$3,0($5)
lh	$2,2($5)
lw	$4,52($sp)
lw	$5,56($sp)
sw	$3,0($4)
b	$L241
sw	$2,0($5)

$L289:
lh	$4,0($3)
lh	$2,2($3)
lw	$22,56($sp)
sw	$4,0($20)
b	$L241
sw	$2,0($22)

$L291:
beq	$6,$0,$L242
sll	$18,$7,1

ori	$9,$15,0x1
lw	$11,168($4)
sll	$9,$9,1
lw	$15,7992($4)
sra	$14,$14,4
addu	$25,$25,$9
sll	$15,$15,2
sll	$6,$25,1
addiu	$15,$15,-1
addu	$6,$6,$14
sra	$9,$15,2
addiu	$6,$6,-1
sra	$14,$6,2
mul	$20,$14,$11
addu	$11,$20,$9
li	$9,12288			# 0x3000
sll	$11,$11,2
sll	$9,$9,$18
addu	$19,$19,$11
ori	$9,$9,0x40
lw	$11,0($19)
and	$9,$9,$11
beq	$9,$0,$L271
sll	$7,$7,2

lw	$9,11864($4)
addu	$13,$13,$16
lw	$11,11868($4)
addu	$17,$17,$7
mul	$14,$6,$9
addu	$4,$4,$13
lw	$9,96($17)
sra	$6,$6,1
addu	$7,$14,$15
sra	$15,$15,1
sll	$7,$7,2
addu	$7,$9,$7
lh	$9,0($7)
sh	$9,11288($4)
lhu	$7,2($7)
sll	$7,$7,1
sh	$7,11290($4)
mul	$7,$6,$11
lw	$4,188($17)
addu	$15,$4,$15
addu	$4,$7,$15
lb	$9,0($4)
b	$L248
sra	$9,$9,1

$L271:
b	$L248
li	$9,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	pred_motion_hw
.size	pred_motion_hw, .-pred_motion_hw
.section	.text.fill_caches_hw,"ax",@progbits
.align	2
.align	5
.globl	fill_caches_hw
.set	nomips16
.set	nomicromips
.ent	fill_caches_hw
.type	fill_caches_hw, @function
fill_caches_hw:
.frame	$sp,224,$31		# vars= 176, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$12,7996($4)
addiu	$sp,$sp,-224
lw	$10,168($4)
lw	$14,7992($4)
.cprestore	0
mul	$2,$12,$10
sw	$fp,220($sp)
sw	$23,216($sp)
sw	$22,212($sp)
sw	$21,208($sp)
sw	$20,204($sp)
sw	$19,200($sp)
sw	$18,196($sp)
sw	$17,192($sp)
addu	$11,$2,$14
sw	$16,188($sp)
beq	$6,$0,$L304
sw	$6,232($sp)

li	$2,65536			# 0x10000
li	$3,1			# 0x1
addu	$2,$4,$2
lw	$6,-5272($2)
beq	$6,$3,$L303
subu	$3,$11,$10

lw	$2,-5276($2)
addu	$6,$2,$11
addu	$2,$2,$3
lbu	$3,0($6)
lbu	$2,0($2)
beq	$3,$2,$L583
li	$2,65536			# 0x10000

$L304:
li	$2,1			# 0x1
lw	$6,10384($4)
li	$3,65536			# 0x10000
sw	$0,8($sp)
addiu	$9,$11,-1
sw	$2,12($sp)
li	$2,2			# 0x2
addu	$3,$4,$3
xori	$6,$6,0x3
sw	$9,48($sp)
sw	$2,16($sp)
li	$2,3			# 0x3
sltu	$6,$0,$6
sw	$9,52($sp)
sll	$6,$10,$6
lw	$16,-5252($3)
sw	$2,20($sp)
li	$2,7			# 0x7
subu	$6,$11,$6
lw	$3,-5276($3)
sw	$2,24($sp)
li	$2,10			# 0xa
addiu	$8,$6,-1
addiu	$7,$6,1
sw	$2,28($sp)
li	$2,8			# 0x8
sw	$2,32($sp)
li	$2,11			# 0xb
beq	$16,$0,$L458
sw	$2,36($sp)

li	$2,-2			# 0xfffffffffffffffe
lw	$15,2192($4)
srl	$13,$5,7
and	$2,$12,$2
mul	$17,$10,$2
xori	$13,$13,0x1
andi	$12,$12,0x1
andi	$13,$13,0x1
addu	$2,$17,$14
sll	$19,$2,2
subu	$14,$2,$10
addu	$19,$15,$19
addiu	$18,$14,-1
sll	$17,$14,2
lw	$14,-4($19)
sll	$18,$18,2
addu	$17,$15,$17
addu	$15,$15,$18
srl	$14,$14,7
lw	$18,0($17)
xori	$14,$14,0x1
lw	$17,0($15)
andi	$14,$14,0x1
beq	$12,$0,$L306
lw	$15,8($15)

bne	$13,$0,$L584
subu	$17,$7,$10

subu	$6,$6,$10
subu	$8,$8,$10
$L584:
xori	$15,$13,0x1
movn	$7,$17,$15
$L314:
beq	$14,$13,$L458
addiu	$2,$2,-1

sw	$2,48($sp)
beq	$13,$0,$L315
sw	$2,52($sp)

beq	$12,$0,$L316
li	$10,1			# 0x1

li	$10,3			# 0x3
li	$9,2			# 0x2
sw	$10,16($sp)
li	$10,8			# 0x8
sw	$9,8($sp)
sw	$9,12($sp)
move	$9,$2
sw	$10,24($sp)
li	$10,11			# 0xb
b	$L302
sw	$10,28($sp)

$L458:
move	$2,$9
$L302:
lw	$18,232($sp)
sw	$6,10772($4)
sw	$2,10780($4)
bne	$18,$0,$L562
sw	$9,10784($4)

li	$10,65536			# 0x10000
addu	$11,$3,$8
addu	$10,$4,$10
lbu	$11,0($11)
lw	$10,-5272($10)
beq	$11,$10,$L563
addu	$11,$3,$6

sw	$0,68($sp)
lbu	$11,0($11)
beq	$10,$11,$L564
addu	$11,$3,$7

$L602:
sw	$0,108($sp)
lbu	$11,0($11)
beq	$10,$11,$L565
move	$21,$0

addu	$11,$3,$2
$L603:
sw	$0,72($sp)
lbu	$11,0($11)
beq	$10,$11,$L566
nop

$L467:
move	$2,$0
addu	$3,$3,$9
sw	$2,40($sp)
lbu	$2,0($3)
beq	$10,$2,$L567
nop

$L468:
move	$2,$0
$L333:
sw	$2,44($sp)
$L321:
andi	$14,$5,0x7
beq	$14,$0,$L334
lw	$3,68($sp)

lw	$9,72($sp)
lw	$10,40($sp)
andi	$13,$3,0x7
andi	$12,$9,0x7
$L322:
li	$3,65535			# 0xffff
andi	$2,$21,0x7
sw	$3,11096($4)
sw	$3,11088($4)
sw	$3,11084($4)
li	$3,61162			# 0xeeea
bne	$2,$0,$L335
sw	$3,11092($4)

bne	$21,$0,$L568
li	$2,46079			# 0xb3ff

$L579:
sw	$2,11084($4)
li	$2,13311			# 0x33ff
sw	$2,11088($4)
li	$2,9962			# 0x26ea
sw	$2,11092($4)
$L335:
addiu	$2,$sp,40
addiu	$11,$sp,48
$L340:
andi	$3,$10,0x7
bne	$3,$0,$L337
addiu	$2,$2,4

beq	$10,$0,$L338
nop

lw	$3,12880($4)
bne	$3,$0,$L338
nop

$L337:
beq	$2,$11,$L339
nop

$L574:
b	$L340
lw	$10,0($2)

$L562:
addu	$10,$3,$6
lbu	$12,0($10)
li	$10,255			# 0xff
beq	$12,$10,$L459
sll	$12,$6,2

lw	$10,2192($4)
addu	$10,$10,$12
lw	$21,0($10)
srl	$10,$21,24
andi	$10,$10,0x1
sw	$10,108($sp)
$L318:
addu	$10,$3,$2
lbu	$12,0($10)
li	$10,255			# 0xff
beq	$12,$10,$L460
sll	$2,$2,2

lw	$10,2192($4)
addu	$2,$10,$2
lw	$10,0($2)
$L319:
addu	$3,$3,$9
sw	$10,40($sp)
li	$2,255			# 0xff
lbu	$3,0($3)
beq	$3,$2,$L461
sll	$9,$9,2

lw	$2,2192($4)
addu	$9,$2,$9
lw	$2,0($9)
$L320:
beq	$16,$0,$L462
sw	$2,44($sp)

andi	$14,$5,0x7
bne	$14,$0,$L463
move	$12,$0

lw	$9,11232($4)
sll	$3,$11,5
lw	$10,%got(scan8)($28)
move	$2,$0
li	$13,16			# 0x10
addu	$3,$9,$3
addiu	$10,$10,%lo(scan8)
lhu	$12,14($3)
$L323:
addu	$3,$10,$2
sra	$9,$12,$2
addiu	$2,$2,1
lbu	$3,0($3)
andi	$9,$9,0x1
addu	$3,$4,$3
bne	$2,$13,$L323
sb	$9,11184($3)

li	$16,65536			# 0x10000
addu	$16,$4,$16
lw	$2,6632($16)
beq	$2,$0,$L324
addiu	$9,$4,11296

sll	$11,$11,2
addiu	$10,$4,11588
addiu	$13,$4,2184
move	$12,$0
li	$17,12288			# 0x3000
li	$15,-1			# 0xffffffffffffffff
$L328:
sll	$2,$12,1
sll	$2,$17,$2
and	$2,$2,$5
beq	$2,$0,$L325
move	$2,$0

lw	$3,11852($4)
addiu	$20,$9,128
lw	$22,11860($4)
move	$2,$9
lw	$18,0($13)
addu	$3,$3,$11
lw	$23,92($13)
addu	$22,$22,$11
lw	$3,0($3)
lw	$19,0($22)
sll	$3,$3,2
addu	$19,$23,$19
addu	$3,$18,$3
$L326:
lw	$18,0($3)
addiu	$2,$2,32
sw	$18,-32($2)
lw	$18,4($3)
sw	$18,-28($2)
lw	$18,8($3)
sw	$18,-24($2)
lw	$18,12($3)
sw	$18,-20($2)
lw	$18,11864($4)
sll	$18,$18,2
bne	$2,$20,$L326
addu	$3,$3,$18

lb	$3,1($19)
lb	$2,0($19)
sll	$3,$3,16
andi	$2,$2,0xffff
addu	$2,$2,$3
sll	$3,$2,8
addu	$2,$3,$2
sw	$2,0($10)
sw	$2,-8($10)
lw	$2,11868($4)
addu	$19,$19,$2
lb	$3,1($19)
lb	$2,0($19)
sll	$3,$3,16
andi	$2,$2,0xffff
addu	$2,$2,$3
sll	$3,$2,8
addu	$2,$3,$2
sw	$2,16($10)
sw	$2,8($10)
$L327:
lw	$2,6632($16)
addiu	$12,$12,1
addiu	$10,$10,40
addiu	$9,$9,160
sltu	$2,$12,$2
bne	$2,$0,$L328
addiu	$13,$13,4

$L324:
sw	$0,72($sp)
sw	$0,68($sp)
$L334:
beq	$21,$0,$L355
nop

lw	$3,11232($4)
sll	$2,$6,5
lw	$9,12828($4)
addu	$2,$3,$2
lbu	$3,4($2)
sb	$3,11188($4)
lbu	$3,5($2)
sb	$3,11189($4)
lbu	$3,6($2)
sb	$3,11190($4)
lbu	$3,3($2)
sb	$3,11191($4)
lbu	$3,9($2)
sb	$3,11185($4)
lbu	$3,8($2)
sb	$3,11186($4)
lbu	$3,12($2)
sb	$3,11209($4)
lbu	$2,11($2)
sb	$2,11210($4)
$L367:
lw	$2,40($sp)
bne	$2,$0,$L356
nop

beq	$9,$0,$L359
li	$2,64			# 0x40

movz	$2,$0,$14
$L359:
sb	$2,11216($4)
sb	$2,11192($4)
sb	$2,11203($4)
sb	$2,11195($4)
$L358:
lw	$2,44($sp)
bne	$2,$0,$L360
nop

beq	$9,$0,$L363
li	$2,64			# 0x40

movz	$2,$0,$14
$L363:
sb	$2,11224($4)
sb	$2,11200($4)
sb	$2,11219($4)
sb	$2,11211($4)
$L362:
beq	$9,$0,$L585
andi	$2,$5,0x178

beq	$21,$0,$L368
li	$2,131072			# 0x20000

li	$9,131072			# 0x20000
sll	$2,$6,1
addu	$9,$4,$9
lw	$3,8732($9)
addu	$2,$3,$2
lhu	$2,0($2)
sw	$2,8740($9)
lw	$2,40($sp)
beq	$2,$0,$L569
nop

$L369:
lw	$10,48($sp)
li	$9,131072			# 0x20000
lw	$2,8($sp)
addu	$9,$4,$9
sll	$10,$10,1
ori	$2,$2,0x1
addu	$3,$3,$10
lhu	$3,0($3)
sra	$2,$3,$2
andi	$3,$3,0x1f0
andi	$2,$2,0x1
sll	$2,$2,1
or	$2,$2,$3
sw	$2,8744($9)
$L375:
lw	$2,44($sp)
beq	$2,$0,$L585
andi	$2,$5,0x178

li	$9,131072			# 0x20000
lw	$3,52($sp)
lw	$2,16($sp)
addu	$9,$4,$9
sll	$10,$3,1
ori	$2,$2,0x1
lw	$3,8732($9)
lw	$11,8744($9)
addu	$3,$3,$10
lhu	$3,0($3)
sra	$2,$3,$2
andi	$2,$2,0x1
sll	$2,$2,3
or	$2,$2,$11
sw	$2,8744($9)
andi	$2,$5,0x178
$L585:
beq	$2,$0,$L380
li	$17,65536			# 0x10000

addu	$17,$4,$17
lw	$2,6632($17)
beq	$2,$0,$L380
lw	$2,16($sp)

li	$22,-2			# 0xfffffffffffffffe
lw	$11,72($sp)
li	$23,-1			# 0xffffffffffffffff
lw	$18,8($sp)
move	$10,$22
lw	$3,40($sp)
sll	$7,$7,2
sw	$2,136($sp)
sll	$6,$6,2
lw	$2,48($sp)
move	$13,$22
lw	$9,44($sp)
sll	$8,$8,2
movn	$10,$23,$11
li	$20,131072			# 0x20000
sll	$2,$2,2
sw	$18,132($sp)
move	$16,$0
sw	$3,88($sp)
move	$24,$4
sw	$2,140($sp)
move	$12,$4
sw	$9,92($sp)
sw	$7,84($sp)
lw	$2,52($sp)
lw	$18,68($sp)
lw	$3,132($sp)
sll	$2,$2,2
lw	$7,136($sp)
movn	$13,$23,$18
sra	$3,$3,1
lw	$9,88($sp)
sra	$7,$7,1
lw	$11,92($sp)
sw	$10,100($sp)
sw	$6,60($sp)
andi	$9,$9,0x100
sw	$2,144($sp)
andi	$11,$11,0x100
sw	$8,80($sp)
andi	$8,$5,0x100
lw	$10,88($sp)
lw	$2,12($sp)
lw	$6,20($sp)
andi	$10,$10,0x40
sw	$13,104($sp)
sw	$3,172($sp)
andi	$3,$5,0x900
sw	$2,148($sp)
sw	$6,152($sp)
addiu	$6,$sp,48
sw	$7,176($sp)
addiu	$7,$sp,40
sw	$8,96($sp)
move	$fp,$6
sw	$9,156($sp)
andi	$9,$21,0x80
sw	$10,164($sp)
sw	$11,160($sp)
lw	$13,92($sp)
lw	$18,88($sp)
lw	$2,92($sp)
lw	$8,68($sp)
andi	$13,$13,0x40
lw	$10,72($sp)
andi	$18,$18,0x80
andi	$2,$2,0x80
sw	$21,56($sp)
andi	$8,$8,0x80
sw	$13,168($sp)
andi	$10,$10,0x80
sw	$2,116($sp)
sw	$18,112($sp)
move	$2,$4
sw	$3,76($sp)
move	$21,$7
sw	$8,120($sp)
sw	$9,124($sp)
sw	$10,128($sp)
b	$L440
sw	$5,64($sp)

$L571:
lw	$5,0($6)
lw	$2,11860($4)
lw	$8,11852($4)
sll	$5,$5,2
lw	$3,0($15)
lw	$6,4($15)
addu	$8,$8,$5
lw	$7,2184($24)
addu	$5,$2,$5
lw	$2,11864($4)
sra	$18,$6,1
lw	$8,0($8)
sra	$9,$3,1
mul	$6,$3,$2
lw	$5,0($5)
lw	$2,2276($24)
addiu	$8,$8,3
addiu	$5,$5,1
addu	$5,$2,$5
lw	$2,4($15)
addu	$3,$6,$8
sll	$3,$3,2
addu	$3,$7,$3
lw	$3,0($3)
sw	$3,11292($13)
lw	$6,11864($4)
mul	$3,$2,$6
addu	$6,$3,$8
sll	$6,$6,2
addu	$6,$7,$6
lw	$3,0($6)
sw	$3,11324($13)
lw	$3,11868($4)
mul	$2,$3,$9
addu	$9,$2,$5
mul	$2,$3,$18
lb	$3,0($9)
sb	$3,11579($14)
addu	$5,$2,$5
lb	$3,0($5)
sb	$3,11587($14)
$L387:
addiu	$10,$10,4
addiu	$15,$15,8
addiu	$13,$13,64
bne	$10,$19,$L389
addiu	$14,$14,16

lw	$3,232($sp)
bne	$3,$0,$L390
lw	$2,180($sp)

lw	$5,96($sp)
beq	$5,$0,$L586
lw	$6,68($sp)

lw	$3,5140($17)
bne	$3,$0,$L587
and	$3,$11,$6

$L390:
lw	$3,-5252($17)
beq	$3,$0,$L382
nop

lw	$6,68($sp)
$L586:
and	$3,$11,$6
$L587:
beq	$3,$0,$L393
lw	$10,104($sp)

lw	$7,11852($4)
lw	$8,80($sp)
lw	$3,11864($4)
lw	$5,11860($4)
addu	$7,$7,$8
lw	$9,80($sp)
sll	$6,$3,1
lw	$10,2184($24)
lw	$8,11868($4)
lw	$7,0($7)
addu	$3,$6,$3
addu	$5,$5,$9
lw	$9,2276($24)
addiu	$6,$8,1
lw	$13,72($sp)
addu	$3,$3,$7
lw	$5,0($5)
addiu	$3,$3,3
sll	$3,$3,2
addu	$5,$6,$5
addu	$3,$10,$3
addu	$5,$9,$5
lw	$3,0($3)
sw	$3,11260($2)
lb	$3,0($5)
sb	$3,11571($12)
and	$3,$11,$13
beq	$3,$0,$L588
lw	$3,100($sp)

$L572:
lw	$5,11852($4)
lw	$18,84($sp)
lw	$6,11864($4)
lw	$7,11860($4)
addu	$5,$5,$18
lw	$9,2184($24)
sll	$3,$6,1
lw	$8,11868($4)
addu	$7,$7,$18
lw	$10,2276($24)
lw	$5,0($5)
addu	$3,$3,$6
lw	$7,0($7)
addu	$3,$3,$5
sll	$3,$3,2
addu	$7,$7,$8
addu	$3,$9,$3
addu	$7,$10,$7
lw	$3,0($3)
sw	$3,11280($2)
lb	$3,0($7)
sb	$3,11576($12)
$L397:
lw	$5,76($sp)
beq	$5,$0,$L589
li	$3,-2			# 0xfffffffffffffffe

lw	$3,-5252($17)
beq	$3,$0,$L382
li	$3,-2			# 0xfffffffffffffffe

$L589:
sb	$3,11598($12)
sb	$3,11582($12)
sb	$3,11600($12)
sb	$3,11592($12)
sb	$3,11584($12)
sw	$0,11368($2)
sw	$0,11304($2)
sw	$0,11376($2)
sw	$0,11344($2)
sw	$0,11312($2)
lw	$3,12828($4)
beq	$3,$0,$L401
nop

beq	$25,$0,$L402
addiu	$3,$20,8796

lw	$8,11852($4)
addiu	$3,$20,8756
lw	$7,60($sp)
lw	$5,11864($4)
addu	$6,$24,$3
addu	$9,$2,$3
addu	$8,$8,$7
sll	$7,$5,1
lw	$6,0($6)
lw	$10,0($8)
addu	$5,$7,$5
addu	$5,$5,$10
sll	$5,$5,2
addu	$5,$6,$5
lw	$6,0($5)
sw	$6,28($9)
lw	$6,4($5)
sw	$6,32($9)
lw	$6,8($5)
sw	$6,36($9)
lw	$5,12($5)
sw	$5,40($9)
$L403:
lw	$8,88($sp)
and	$3,$11,$8
beq	$3,$0,$L404
addiu	$3,$20,8844

lw	$3,11852($4)
addiu	$5,$20,8756
lw	$10,140($sp)
lw	$9,11864($4)
addu	$6,$24,$5
addu	$8,$2,$5
addu	$3,$3,$10
lw	$10,132($sp)
lw	$6,0($6)
move	$5,$8
mul	$13,$10,$9
lw	$7,0($3)
addiu	$7,$7,3
addu	$3,$13,$7
sll	$3,$3,2
addu	$3,$6,$3
lw	$3,0($3)
sw	$3,56($8)
lw	$3,11864($4)
lw	$8,148($sp)
mul	$9,$8,$3
addu	$3,$9,$7
sll	$3,$3,2
addu	$3,$6,$3
lw	$3,0($3)
sw	$3,88($5)
$L405:
lw	$10,92($sp)
and	$11,$11,$10
beq	$11,$0,$L406
addiu	$3,$20,8908

lw	$3,11852($4)
addiu	$5,$20,8756
lw	$11,144($sp)
lw	$9,11864($4)
addu	$6,$24,$5
lw	$10,136($sp)
addu	$8,$2,$5
addu	$3,$3,$11
lw	$6,0($6)
move	$5,$8
mul	$11,$10,$9
lw	$7,0($3)
addiu	$7,$7,3
addu	$3,$11,$7
sll	$3,$3,2
addu	$3,$6,$3
lw	$3,0($3)
sw	$3,120($8)
lw	$3,11864($4)
lw	$8,152($sp)
mul	$9,$8,$3
addu	$3,$9,$7
sll	$3,$3,2
addu	$3,$6,$3
lw	$3,0($3)
sw	$3,152($5)
addiu	$3,$20,8888
addu	$5,$2,$3
li	$3,3			# 0x3
sw	$0,0($5)
sw	$0,-64($5)
sw	$0,8($5)
sw	$0,-24($5)
sw	$0,-56($5)
lw	$5,-5264($17)
beq	$5,$3,$L590
lw	$10,56($sp)

$L401:
lw	$3,-5252($17)
beq	$3,$0,$L382
nop

lw	$3,-5248($17)
beq	$3,$0,$L417
lw	$11,120($sp)

lw	$10,120($sp)
bne	$10,$0,$L591
lw	$11,124($sp)

lb	$3,11571($12)
bltz	$3,$L591
sll	$3,$3,1

addiu	$5,$20,8782
sb	$3,11571($12)
addu	$5,$2,$5
lh	$6,11262($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11262($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
lw	$11,124($sp)
$L591:
bne	$11,$0,$L592
lw	$13,128($sp)

lb	$3,11572($12)
bltz	$3,$L421
sll	$3,$3,1

addiu	$5,$20,8786
sb	$3,11572($12)
addu	$5,$2,$5
lh	$6,11266($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11266($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L421:
lb	$3,11573($12)
bltz	$3,$L422
sll	$3,$3,1

addiu	$5,$20,8790
sb	$3,11573($12)
addu	$5,$2,$5
lh	$6,11270($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11270($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L422:
lb	$3,11574($12)
bltz	$3,$L423
sll	$3,$3,1

addiu	$5,$20,8794
sb	$3,11574($12)
addu	$5,$2,$5
lh	$6,11274($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11274($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L423:
lb	$3,11575($12)
bltz	$3,$L420
sll	$3,$3,1

addiu	$5,$20,8798
sb	$3,11575($12)
addu	$5,$2,$5
lh	$6,11278($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11278($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L420:
lw	$13,128($sp)
$L592:
bne	$13,$0,$L593
lw	$18,112($sp)

lb	$3,11576($12)
bltz	$3,$L593
nop

sll	$3,$3,1
addiu	$5,$20,8802
sb	$3,11576($12)
addu	$5,$2,$5
lh	$6,11282($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11282($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
lw	$18,112($sp)
$L593:
bne	$18,$0,$L426
nop

lb	$3,11579($12)
bltz	$3,$L427
sll	$3,$3,1

li	$6,131072			# 0x20000
sb	$3,11579($12)
ori	$6,$6,0x226e
addu	$5,$2,$6
lh	$6,11294($2)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11294($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L427:
lb	$3,11587($12)
bltz	$3,$L426
sll	$3,$3,1

li	$7,131072			# 0x20000
sb	$3,11587($12)
ori	$7,$7,0x228e
lh	$6,11326($2)
addu	$5,$2,$7
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11326($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L426:
lw	$8,116($sp)
bne	$8,$0,$L382
nop

lb	$3,11595($12)
bltz	$3,$L428
sll	$3,$3,1

li	$9,131072			# 0x20000
sb	$3,11595($12)
ori	$9,$9,0x22ae
lh	$6,11358($2)
addu	$5,$2,$9
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11358($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L428:
lb	$3,11603($12)
bltz	$3,$L382
sll	$3,$3,1

li	$10,131072			# 0x20000
sb	$3,11603($12)
ori	$10,$10,0x22ce
lh	$6,11390($2)
addu	$5,$2,$10
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,11390($2)
lh	$6,0($5)
srl	$3,$6,31
addu	$3,$3,$6
sra	$3,$3,1
sh	$3,0($5)
$L382:
lw	$3,6632($17)
addiu	$16,$16,1
addiu	$12,$12,40
addiu	$2,$2,160
sltu	$3,$16,$3
beq	$3,$0,$L380
addiu	$24,$24,4

$L440:
sll	$3,$16,1
lw	$13,64($sp)
li	$11,12288			# 0x3000
sll	$11,$11,$3
ori	$3,$11,0x100
and	$3,$3,$13
bne	$3,$0,$L381
addu	$3,$4,$20

lw	$3,9456($3)
beq	$3,$0,$L382
nop

$L381:
lw	$18,56($sp)
and	$25,$11,$18
beq	$25,$0,$L383
sw	$0,11648($24)

lw	$6,11852($4)
lw	$5,60($sp)
lw	$3,11864($4)
lw	$8,2184($24)
addu	$6,$6,$5
lw	$7,11860($4)
sll	$5,$3,1
lw	$13,60($sp)
lw	$9,11868($4)
lw	$6,0($6)
addu	$3,$5,$3
addu	$5,$7,$13
lw	$10,2276($24)
addu	$3,$3,$6
lw	$5,0($5)
sll	$3,$3,2
addu	$3,$8,$3
addu	$5,$5,$9
lw	$6,0($3)
addu	$5,$10,$5
sw	$6,11264($2)
lw	$6,4($3)
sw	$6,11268($2)
lw	$6,8($3)
sw	$6,11272($2)
lw	$3,12($3)
sw	$3,11276($2)
lb	$3,0($5)
sb	$3,11573($12)
sb	$3,11572($12)
lb	$3,1($5)
sb	$3,11575($12)
sb	$3,11574($12)
$L384:
addiu	$15,$sp,8
sw	$2,180($sp)
move	$10,$0
li	$19,8			# 0x8
move	$14,$12
move	$13,$2
$L389:
addu	$3,$21,$10
move	$5,$22
lw	$3,0($3)
movn	$5,$23,$3
and	$3,$3,$11
bne	$3,$0,$L571
addu	$6,$fp,$10

sw	$0,11324($13)
sw	$0,11292($13)
sb	$5,11587($14)
b	$L387
sb	$5,11579($14)

$L303:
li	$2,65536			# 0x10000
$L583:
addu	$2,$4,$2
lw	$2,-5252($2)
bne	$2,$0,$L304
lw	$fp,220($sp)

lw	$23,216($sp)
lw	$22,212($sp)
lw	$21,208($sp)
lw	$20,204($sp)
lw	$19,200($sp)
lw	$18,196($sp)
lw	$17,192($sp)
lw	$16,188($sp)
j	$31
addiu	$sp,$sp,224

$L569:
beq	$14,$0,$L594
li	$2,131072			# 0x20000

li	$3,448			# 0x1c0
addu	$2,$4,$2
$L604:
b	$L375
sw	$3,8744($2)

$L383:
lw	$18,56($sp)
li	$5,-16908288			# 0xfffffffffefe0000
li	$3,-1			# 0xffffffffffffffff
sw	$0,11276($2)
ori	$5,$5,0xfefe
sw	$0,11272($2)
movz	$3,$5,$18
sw	$0,11268($2)
sw	$0,11264($2)
b	$L384
sw	$3,11572($12)

$L393:
lw	$13,72($sp)
sw	$0,11260($2)
and	$3,$11,$13
bne	$3,$0,$L572
sb	$10,11571($12)

lw	$3,100($sp)
$L588:
sw	$0,11280($2)
b	$L397
sb	$3,11576($12)

$L380:
lbu	$2,43($sp)
lw	$13,108($sp)
lw	$fp,220($sp)
andi	$2,$2,0x1
lw	$23,216($sp)
addu	$2,$13,$2
lw	$22,212($sp)
lw	$21,208($sp)
lw	$20,204($sp)
lw	$19,200($sp)
lw	$18,196($sp)
lw	$17,192($sp)
lw	$16,188($sp)
addiu	$sp,$sp,224
j	$31
sw	$2,11656($4)

$L406:
addu	$5,$2,$3
addiu	$3,$20,8888
sw	$0,0($5)
sw	$0,-32($5)
addu	$5,$2,$3
li	$3,3			# 0x3
sw	$0,0($5)
sw	$0,-64($5)
sw	$0,8($5)
sw	$0,-24($5)
sw	$0,-56($5)
lw	$5,-5264($17)
bne	$5,$3,$L401
lw	$10,56($sp)

$L590:
addu	$3,$4,$20
sw	$0,9104($3)
andi	$5,$10,0x100
sw	$0,9112($3)
sw	$0,9120($3)
bne	$5,$0,$L573
sw	$0,9128($3)

lw	$13,56($sp)
andi	$5,$13,0x40
beq	$5,$0,$L411
lw	$18,60($sp)

lw	$5,11860($4)
lw	$6,11868($4)
lw	$7,9088($3)
addu	$5,$5,$18
lw	$5,0($5)
addu	$5,$5,$6
addu	$5,$7,$5
lbu	$6,0($5)
sb	$6,9096($3)
lbu	$5,1($5)
sb	$5,9098($3)
$L410:
lw	$3,156($sp)
beq	$3,$0,$L595
lw	$5,164($sp)

li	$5,1			# 0x1
$L601:
addu	$3,$4,$20
sb	$5,9103($3)
$L413:
lw	$10,160($sp)
beq	$10,$0,$L415
lw	$11,168($sp)

li	$5,1			# 0x1
addu	$3,$4,$20
b	$L401
sb	$5,9119($3)

$L404:
addu	$5,$2,$3
sw	$0,0($5)
b	$L405
sw	$0,-32($5)

$L402:
addu	$7,$2,$3
sw	$0,0($7)
sw	$0,-4($7)
sw	$0,-8($7)
b	$L403
sw	$0,-12($7)

$L417:
beq	$11,$0,$L596
lw	$13,124($sp)

lb	$3,11571($12)
bltz	$3,$L596
sra	$3,$3,1

addiu	$5,$20,8782
sb	$3,11571($12)
addu	$3,$2,$5
lh	$5,11262($2)
sll	$5,$5,1
sh	$5,11262($2)
lh	$5,0($3)
sll	$5,$5,1
sh	$5,0($3)
lw	$13,124($sp)
$L596:
beq	$13,$0,$L597
lw	$18,128($sp)

lb	$3,11572($12)
bltz	$3,$L432
sra	$3,$3,1

addiu	$5,$20,8786
sb	$3,11572($12)
addu	$3,$2,$5
lh	$5,11266($2)
sll	$5,$5,1
sh	$5,11266($2)
lh	$5,0($3)
sll	$5,$5,1
sh	$5,0($3)
$L432:
lb	$3,11573($12)
bltz	$3,$L433
sra	$3,$3,1

addiu	$5,$20,8790
sb	$3,11573($12)
addu	$3,$2,$5
lh	$5,11270($2)
sll	$5,$5,1
sh	$5,11270($2)
lh	$5,0($3)
sll	$5,$5,1
sh	$5,0($3)
$L433:
lb	$3,11574($12)
bltz	$3,$L434
sra	$3,$3,1

addiu	$5,$20,8794
sb	$3,11574($12)
addu	$3,$2,$5
lh	$5,11274($2)
sll	$5,$5,1
sh	$5,11274($2)
lh	$5,0($3)
sll	$5,$5,1
sh	$5,0($3)
$L434:
lb	$3,11575($12)
bltz	$3,$L431
sra	$3,$3,1

addiu	$5,$20,8798
sb	$3,11575($12)
addu	$3,$2,$5
lh	$5,11278($2)
sll	$5,$5,1
sh	$5,11278($2)
lh	$5,0($3)
sll	$5,$5,1
sh	$5,0($3)
$L431:
lw	$18,128($sp)
$L597:
beq	$18,$0,$L598
lw	$3,112($sp)

lb	$3,11576($12)
bltz	$3,$L435
sra	$3,$3,1

addiu	$5,$20,8802
sb	$3,11576($12)
addu	$3,$2,$5
lh	$5,11282($2)
sll	$5,$5,1
sh	$5,11282($2)
lh	$5,0($3)
sll	$5,$5,1
sh	$5,0($3)
$L435:
lw	$3,112($sp)
$L598:
beq	$3,$0,$L599
lw	$8,116($sp)

lb	$3,11579($12)
bltz	$3,$L438
sra	$3,$3,1

li	$6,131072			# 0x20000
sb	$3,11579($12)
ori	$6,$6,0x226e
lh	$3,11294($2)
addu	$5,$2,$6
sll	$3,$3,1
sh	$3,11294($2)
move	$3,$5
lh	$5,0($5)
sll	$5,$5,1
sh	$5,0($3)
$L438:
lb	$3,11587($12)
bltz	$3,$L437
sra	$3,$3,1

li	$7,131072			# 0x20000
sb	$3,11587($12)
ori	$7,$7,0x228e
lh	$3,11326($2)
addu	$5,$2,$7
sll	$3,$3,1
sh	$3,11326($2)
move	$3,$5
lh	$5,0($5)
sll	$5,$5,1
sh	$5,0($3)
$L437:
lw	$8,116($sp)
$L599:
beq	$8,$0,$L382
nop

lb	$3,11595($12)
bltz	$3,$L439
sra	$3,$3,1

li	$9,131072			# 0x20000
sb	$3,11595($12)
ori	$9,$9,0x22ae
lh	$3,11358($2)
addu	$5,$2,$9
sll	$3,$3,1
sh	$3,11358($2)
move	$3,$5
lh	$5,0($5)
sll	$5,$5,1
sh	$5,0($3)
$L439:
lb	$3,11603($12)
bltz	$3,$L382
sra	$3,$3,1

li	$10,131072			# 0x20000
sb	$3,11603($12)
ori	$10,$10,0x22ce
lh	$3,11390($2)
addu	$5,$2,$10
sll	$3,$3,1
sh	$3,11390($2)
move	$3,$5
lh	$5,0($5)
sll	$5,$5,1
b	$L382
sh	$5,0($3)

$L360:
lw	$2,52($sp)
lw	$3,11232($4)
lw	$11,16($sp)
sll	$2,$2,5
lw	$10,20($sp)
lw	$12,36($sp)
addu	$2,$3,$2
lw	$3,32($sp)
addu	$11,$2,$11
addu	$10,$2,$10
addu	$3,$2,$3
lbu	$11,0($11)
addu	$2,$2,$12
sb	$11,11211($4)
lbu	$10,0($10)
sb	$10,11219($4)
lbu	$3,0($3)
sb	$3,11200($4)
lbu	$2,0($2)
b	$L362
sb	$2,11224($4)

$L356:
lw	$2,48($sp)
lw	$3,11232($4)
lw	$11,8($sp)
sll	$2,$2,5
lw	$10,12($sp)
lw	$12,28($sp)
addu	$2,$3,$2
lw	$3,24($sp)
addu	$11,$2,$11
addu	$10,$2,$10
addu	$3,$2,$3
lbu	$11,0($11)
addu	$2,$2,$12
sb	$11,11195($4)
lbu	$10,0($10)
sb	$10,11203($4)
lbu	$3,0($3)
sb	$3,11192($4)
lbu	$2,0($2)
b	$L358
sb	$2,11216($4)

$L355:
lw	$9,12828($4)
beq	$9,$0,$L366
li	$2,64			# 0x40

movz	$2,$0,$14
$L366:
sb	$2,11210($4)
sb	$2,11209($4)
sb	$2,11186($4)
sb	$2,11185($4)
sb	$2,11191($4)
sb	$2,11190($4)
sb	$2,11189($4)
b	$L367
sb	$2,11188($4)

$L338:
lw	$9,11084($4)
lw	$3,11096($4)
andi	$9,$9,0xdf5f
andi	$3,$3,0x5f5f
sw	$9,11084($4)
bne	$2,$11,$L574
sw	$3,11096($4)

$L339:
bne	$13,$0,$L341
lw	$10,68($sp)

bne	$10,$0,$L575
nop

lw	$2,11084($4)
$L582:
andi	$2,$2,0x7fff
sw	$2,11084($4)
$L341:
bne	$12,$0,$L600
andi	$2,$5,0x1

lw	$11,72($sp)
bne	$11,$0,$L576
nop

lw	$2,11092($4)
$L580:
andi	$2,$2,0xfbff
sw	$2,11092($4)
$L343:
andi	$2,$5,0x1
$L600:
beq	$2,$0,$L334
andi	$2,$21,0x1

beq	$2,$0,$L345
sll	$3,$6,3

lw	$2,10860($4)
addu	$2,$2,$3
lb	$3,4($2)
sb	$3,10820($4)
lb	$3,5($2)
sb	$3,10821($4)
lb	$3,6($2)
sb	$3,10822($4)
lb	$2,3($2)
sb	$2,10823($4)
$L354:
lw	$2,40($sp)
andi	$3,$2,0x1
bne	$3,$0,$L346
lw	$3,48($sp)

bne	$2,$0,$L347
andi	$2,$2,0x78

li	$2,-1			# 0xffffffffffffffff
$L349:
sb	$2,10835($4)
sb	$2,10827($4)
$L348:
lw	$2,44($sp)
andi	$3,$2,0x1
bne	$3,$0,$L350
lw	$9,20($sp)

bne	$2,$0,$L351
andi	$2,$2,0x78

li	$2,-1			# 0xffffffffffffffff
sb	$2,10851($4)
b	$L334
sb	$2,10843($4)

$L462:
sw	$0,72($sp)
b	$L321
sw	$0,68($sp)

$L368:
bne	$14,$0,$L577
addu	$2,$4,$2

sw	$0,8740($2)
lw	$2,40($sp)
beq	$2,$0,$L374
li	$2,131072			# 0x20000

$L372:
li	$2,131072			# 0x20000
addu	$2,$4,$2
b	$L369
lw	$3,8732($2)

$L306:
bne	$13,$0,$L314
subu	$19,$8,$10

andi	$17,$17,0x80
srl	$15,$15,7
movn	$8,$19,$17
subu	$20,$6,$10
subu	$17,$7,$10
andi	$18,$18,0x80
andi	$15,$15,0x1
movn	$6,$20,$18
b	$L314
movn	$7,$17,$15

$L573:
li	$11,16842752			# 0x1010000
ori	$11,$11,0x101
sw	$11,9096($3)
lw	$3,156($sp)
bne	$3,$0,$L601
li	$5,1			# 0x1

lw	$5,164($sp)
$L595:
beq	$5,$0,$L414
addu	$3,$4,$20

lw	$5,11860($4)
addu	$6,$4,$20
lw	$8,140($sp)
lw	$3,11868($4)
lw	$7,9088($6)
addu	$5,$5,$8
lw	$8,172($sp)
mul	$9,$8,$3
lw	$5,0($5)
addu	$3,$9,$7
addu	$3,$3,$5
lbu	$3,1($3)
b	$L413
sb	$3,9103($6)

$L315:
addu	$9,$10,$2
sw	$0,16($sp)
li	$10,7			# 0x7
li	$12,2			# 0x2
sw	$9,52($sp)
sw	$10,32($sp)
li	$10,10			# 0xa
sw	$12,12($sp)
sw	$12,20($sp)
b	$L302
sw	$10,36($sp)

$L374:
$L594:
addu	$2,$4,$2
b	$L375
sw	$0,8744($2)

$L568:
lw	$2,12880($4)
beq	$2,$0,$L335
nop

b	$L579
li	$2,46079			# 0xb3ff

$L463:
sw	$0,72($sp)
move	$13,$0
b	$L322
sw	$0,68($sp)

$L576:
lw	$2,12880($4)
beq	$2,$0,$L343
nop

b	$L580
lw	$2,11092($4)

$L577:
li	$3,448			# 0x1c0
sw	$3,8740($2)
lw	$2,40($sp)
bne	$2,$0,$L372
li	$2,131072			# 0x20000

b	$L604
addu	$2,$4,$2

$L345:
beq	$21,$0,$L473
li	$2,-1			# 0xffffffffffffffff

andi	$2,$21,0x78
beq	$2,$0,$L474
li	$9,2			# 0x2

lw	$3,12880($4)
li	$2,-1			# 0xffffffffffffffff
b	$L353
movz	$2,$9,$3

$L575:
lw	$2,12880($4)
beq	$2,$0,$L341
nop

b	$L582
lw	$2,11084($4)

$L325:
move	$3,$0
sw	$2,0($9)
sw	$3,4($9)
sw	$2,8($9)
sw	$3,12($9)
sw	$2,32($9)
sw	$3,36($9)
sw	$2,40($9)
sw	$3,44($9)
sw	$2,64($9)
sw	$3,68($9)
sw	$2,72($9)
sw	$3,76($9)
sw	$2,96($9)
sw	$3,100($9)
sw	$2,104($9)
sw	$3,108($9)
sw	$15,-8($10)
sw	$15,0($10)
sw	$15,8($10)
b	$L327
sw	$15,16($10)

$L415:
beq	$11,$0,$L416
addu	$3,$4,$20

lw	$3,11868($4)
addu	$6,$4,$20
lw	$8,176($sp)
lw	$5,11860($4)
lw	$13,144($sp)
mul	$9,$8,$3
lw	$7,9088($6)
addu	$5,$5,$13
lw	$5,0($5)
addu	$3,$9,$7
addu	$3,$3,$5
lbu	$3,1($3)
b	$L401
sb	$3,9119($6)

$L461:
b	$L320
move	$2,$0

$L460:
b	$L319
move	$10,$0

$L459:
sw	$0,108($sp)
b	$L318
move	$21,$0

$L563:
lw	$11,2192($4)
sll	$12,$8,2
addu	$11,$11,$12
lw	$11,0($11)
sw	$11,68($sp)
addu	$11,$3,$6
lbu	$11,0($11)
bne	$10,$11,$L602
addu	$11,$3,$7

$L564:
lw	$11,2192($4)
sll	$12,$6,2
addu	$11,$11,$12
lw	$21,0($11)
srl	$11,$21,24
andi	$11,$11,0x1
sw	$11,108($sp)
addu	$11,$3,$7
lbu	$11,0($11)
bne	$10,$11,$L603
addu	$11,$3,$2

$L565:
lw	$11,2192($4)
sll	$12,$7,2
addu	$11,$11,$12
lw	$11,0($11)
sw	$11,72($sp)
addu	$11,$3,$2
lbu	$11,0($11)
bne	$10,$11,$L467
nop

$L566:
lw	$11,2192($4)
sll	$2,$2,2
addu	$3,$3,$9
addu	$2,$11,$2
lw	$2,0($2)
sw	$2,40($sp)
lbu	$2,0($3)
bne	$10,$2,$L468
nop

$L567:
lw	$2,2192($4)
sll	$9,$9,2
addu	$2,$2,$9
b	$L333
lw	$2,0($2)

$L350:
lw	$3,52($sp)
lw	$2,10860($4)
sll	$3,$3,3
addu	$2,$2,$3
lw	$3,16($sp)
addu	$3,$2,$3
addu	$2,$2,$9
lb	$3,0($3)
sb	$3,10843($4)
lb	$2,0($2)
b	$L334
sb	$2,10851($4)

$L346:
lw	$2,10860($4)
lw	$9,12($sp)
sll	$3,$3,3
addu	$2,$2,$3
lw	$3,8($sp)
addu	$3,$2,$3
addu	$2,$2,$9
lb	$3,0($3)
sb	$3,10827($4)
lb	$2,0($2)
b	$L348
sb	$2,10835($4)

$L414:
b	$L413
sb	$0,9103($3)

$L411:
b	$L410
sw	$0,9096($3)

$L416:
b	$L401
sb	$0,9119($3)

$L316:
sw	$0,12($sp)
move	$9,$2
sw	$10,16($sp)
sw	$10,20($sp)
li	$10,7			# 0x7
sw	$10,32($sp)
li	$10,10			# 0xa
b	$L302
sw	$10,36($sp)

$L347:
beq	$2,$0,$L469
li	$9,2			# 0x2

lw	$3,12880($4)
li	$2,-1			# 0xffffffffffffffff
b	$L349
movz	$2,$9,$3

$L351:
beq	$2,$0,$L471
li	$2,2			# 0x2

lw	$3,12880($4)
li	$2,-1			# 0xffffffffffffffff
li	$9,2			# 0x2
movz	$2,$9,$3
sb	$2,10851($4)
b	$L334
sb	$2,10843($4)

$L473:
$L353:
sb	$2,10823($4)
sb	$2,10822($4)
sb	$2,10821($4)
b	$L354
sb	$2,10820($4)

$L471:
sb	$2,10851($4)
b	$L334
sb	$2,10843($4)

$L469:
b	$L349
li	$2,2			# 0x2

$L474:
b	$L353
li	$2,2			# 0x2

.set	macro
.set	reorder
.end	fill_caches_hw
.size	fill_caches_hw, .-fill_caches_hw
.section	.text.decode_mb_skip_hw,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_mb_skip_hw
.type	decode_mb_skip_hw, @function
decode_mb_skip_hw:
.frame	$sp,72,$31		# vars= 8, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-72
lw	$3,168($4)
lw	$2,7992($4)
move	$5,$0
sw	$20,48($sp)
li	$6,16			# 0x10
lw	$20,7996($4)
lw	$25,%call16(memset)($28)
sw	$16,32($sp)
move	$16,$4
mul	$7,$20,$3
.cprestore	16
sw	$31,68($sp)
sw	$fp,64($sp)
sw	$23,60($sp)
sw	$22,56($sp)
sw	$21,52($sp)
sw	$19,44($sp)
addu	$20,$7,$2
sw	$18,40($sp)
sw	$17,36($sp)
sw	$0,24($sp)
sll	$2,$20,5
lw	$4,11232($4)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$4,$2

move	$5,$0
lw	$28,16($sp)
addiu	$4,$16,11192
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,40			# 0x28

li	$2,65536			# 0x10000
lw	$28,16($sp)
addu	$2,$16,$2
lw	$2,-5248($2)
beq	$2,$0,$L607
lw	$5,24($sp)

ori	$5,$5,0x80
$L607:
li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$3,-5264($2)
li	$2,3			# 0x3
beq	$3,$2,$L674
lw	$25,%call16(fill_caches_hw)($28)

ori	$5,$5,0x3808
move	$6,$0
move	$4,$16
.reloc	1f,R_MIPS_JALR,fill_caches_hw
1:	jalr	$25
sw	$5,24($sp)

li	$2,-2			# 0xfffffffffffffffe
lb	$3,11572($16)
beq	$3,$2,$L645
lb	$4,11579($16)

beq	$4,$2,$L645
nop

beq	$3,$0,$L675
nop

$L611:
bne	$4,$0,$L612
nop

lw	$2,11292($16)
beq	$2,$0,$L681
move	$2,$0

$L612:
lb	$2,11576($16)
li	$5,-2			# 0xfffffffffffffffe
beq	$2,$5,$L613
nop

addiu	$5,$16,11280
$L614:
sltu	$3,$3,1
sltu	$2,$2,1
addu	$2,$2,$3
sltu	$4,$4,1
addu	$2,$2,$4
slt	$6,$2,2
bne	$6,$0,$L676
li	$6,1			# 0x1

$L621:
lh	$3,11292($16)
lh	$2,11264($16)
slt	$6,$2,$3
beq	$6,$0,$L624
lh	$4,0($5)

slt	$6,$2,$4
beq	$6,$0,$L625
nop

slt	$2,$3,$4
movz	$3,$4,$2
move	$2,$3
$L625:
lh	$4,11294($16)
lh	$3,11266($16)
slt	$6,$3,$4
beq	$6,$0,$L626
lh	$5,2($5)

slt	$6,$3,$5
beq	$6,$0,$L660
nop

slt	$3,$4,$5
movz	$4,$5,$3
andi	$2,$2,0xffff
sll	$4,$4,16
addu	$4,$2,$4
$L659:
li	$2,1			# 0x1
multu	$4,$2
mfhi	$3
mflo	$2
b	$L610
addu	$3,$4,$3

$L675:
lw	$2,11264($16)
bne	$2,$0,$L611
nop

$L645:
move	$2,$0
$L681:
move	$3,$0
$L610:
lw	$25,24($sp)
sw	$0,11580($16)
sw	$0,11588($16)
sw	$0,11596($16)
sw	$0,11604($16)
sw	$2,11296($16)
sw	$3,11300($16)
sw	$2,11304($16)
sw	$3,11308($16)
sw	$2,11328($16)
sw	$3,11332($16)
sw	$2,11336($16)
sw	$3,11340($16)
sw	$2,11360($16)
sw	$3,11364($16)
sw	$2,11368($16)
sw	$3,11372($16)
sw	$2,11392($16)
sw	$3,11396($16)
sw	$2,11400($16)
sw	$3,11404($16)
$L609:
lw	$18,7996($16)
andi	$4,$25,0x3000
lw	$17,11864($16)
lw	$5,11868($16)
sll	$3,$18,2
lw	$2,7992($16)
sll	$18,$18,1
mul	$7,$3,$17
mul	$3,$18,$5
sll	$6,$2,2
sll	$2,$2,1
addu	$17,$7,$6
bne	$4,$0,$L629
addu	$18,$3,$2

lw	$2,2276($16)
li	$3,-1			# 0xffffffffffffffff
addu	$2,$2,$18
addu	$5,$2,$5
sh	$3,0($2)
sh	$3,0($5)
$L629:
li	$24,65536			# 0x10000
addu	$24,$16,$24
lw	$2,6632($24)
beq	$2,$0,$L641
move	$4,$0

li	$21,131072			# 0x20000
addiu	$22,$17,2
addiu	$13,$21,8756
addiu	$fp,$21,8816
sll	$17,$17,2
addiu	$6,$16,11580
addu	$13,$16,$13
addiu	$12,$16,2184
sll	$22,$22,2
li	$19,12288			# 0x3000
addiu	$21,$21,8944
move	$11,$16
andi	$23,$25,0x800
$L640:
sll	$2,$4,1
sll	$2,$19,$2
and	$2,$2,$25
beq	$2,$0,$L633
addiu	$14,$11,128

lw	$10,0($12)
lw	$15,11864($16)
move	$3,$0
move	$2,$11
addu	$10,$10,$17
sll	$7,$15,2
$L634:
lw	$8,11296($2)
addu	$5,$10,$3
lw	$9,11300($2)
addiu	$2,$2,32
addu	$3,$3,$7
sw	$8,0($5)
sw	$9,4($5)
lw	$8,11272($2)
lw	$9,11276($2)
sw	$8,8($5)
bne	$14,$2,$L634
sw	$9,12($5)

lw	$2,12828($16)
beq	$2,$0,$L636
nop

beq	$23,$0,$L637
addu	$5,$11,$fp

lw	$2,0($13)
sll	$15,$15,3
move	$14,$0
addu	$3,$15,$7
addu	$2,$2,$17
addu	$5,$2,$7
sw	$14,0($2)
addu	$10,$2,$15
sw	$14,8($2)
addu	$8,$5,$7
move	$15,$0
addu	$3,$2,$3
addu	$7,$8,$7
sw	$15,4($2)
sw	$15,12($2)
sw	$14,0($5)
sw	$15,4($5)
sw	$14,8($5)
sw	$15,12($5)
sw	$14,0($8)
sw	$15,4($8)
sw	$14,8($10)
sw	$15,12($10)
sw	$14,0($7)
sw	$15,4($7)
sw	$14,8($3)
sw	$15,12($3)
$L636:
lw	$2,92($12)
$L680:
lb	$3,0($6)
addu	$2,$2,$18
sb	$3,0($2)
lb	$3,2($6)
sb	$3,1($2)
lw	$3,11868($16)
lb	$5,16($6)
addu	$3,$2,$3
sb	$5,0($3)
lw	$5,11868($16)
lb	$3,18($6)
addu	$2,$2,$5
sb	$3,1($2)
$L633:
lw	$2,6632($24)
addiu	$4,$4,1
addiu	$6,$6,40
addiu	$13,$13,4
sltu	$2,$4,$2
addiu	$11,$11,160
bne	$2,$0,$L640
addiu	$12,$12,4

$L641:
li	$2,65536			# 0x10000
li	$3,3			# 0x3
addu	$2,$16,$2
lw	$4,-5264($2)
beq	$4,$3,$L677
nop

$L632:
lw	$2,2192($16)
sll	$4,$20,2
lw	$5,24($sp)
lw	$3,2172($16)
addu	$4,$2,$4
li	$2,65536			# 0x10000
sw	$5,0($4)
addu	$3,$3,$20
lw	$4,2872($16)
addu	$2,$16,$2
sb	$4,0($3)
lw	$3,-5276($2)
lw	$2,-5272($2)
addu	$20,$3,$20
sb	$2,0($20)
li	$2,1			# 0x1
lw	$31,68($sp)
lw	$fp,64($sp)
lw	$23,60($sp)
lw	$22,56($sp)
lw	$21,52($sp)
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
sw	$2,10752($16)
lw	$16,32($sp)
j	$31
addiu	$sp,$sp,72

$L637:
lw	$8,0($13)
lw	$10,11864($16)
addu	$14,$11,$21
addu	$9,$8,$17
sll	$10,$10,2
addu	$8,$8,$22
$L638:
lw	$2,0($5)
addiu	$5,$5,32
lw	$3,-28($5)
sw	$2,0($9)
sw	$3,4($9)
addu	$9,$9,$10
lw	$2,-24($5)
lw	$3,-20($5)
sw	$2,0($8)
sw	$3,4($8)
bne	$5,$14,$L638
addu	$8,$8,$7

b	$L680
lw	$2,92($12)

$L677:
lw	$3,12828($16)
beq	$3,$0,$L632
nop

andi	$25,$25,0x40
beq	$25,$0,$L632
li	$3,131072			# 0x20000

lhu	$4,-5238($2)
addu	$3,$16,$3
srl	$4,$4,8
lw	$3,9088($3)
andi	$4,$4,0x1
addu	$18,$3,$18
sb	$4,1($18)
lhu	$3,-5236($2)
lw	$4,11868($16)
srl	$3,$3,8
addu	$4,$18,$4
andi	$3,$3,0x1
sb	$3,0($4)
lhu	$2,-5234($2)
lw	$3,11868($16)
srl	$2,$2,8
addu	$18,$18,$3
andi	$2,$2,0x1
b	$L632
sb	$2,1($18)

$L674:
ori	$5,$5,0x5908
move	$6,$0
move	$4,$16
.reloc	1f,R_MIPS_JALR,fill_caches_hw
1:	jalr	$25
sw	$5,24($sp)

addiu	$5,$sp,24
lw	$28,16($sp)
lw	$25,%call16(pred_direct_motion_hw)($28)
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$16

lw	$25,24($sp)
ori	$25,$25,0x800
b	$L609
sw	$25,24($sp)

$L613:
lb	$2,11571($16)
b	$L614
addiu	$5,$16,11260

$L624:
slt	$6,$4,$2
beq	$6,$0,$L625
nop

slt	$2,$4,$3
movz	$3,$4,$2
b	$L625
move	$2,$3

$L626:
slt	$6,$5,$3
beq	$6,$0,$L660
nop

slt	$3,$5,$4
movz	$4,$5,$3
andi	$2,$2,0xffff
sll	$4,$4,16
b	$L659
addu	$4,$4,$2

$L660:
sll	$3,$3,16
andi	$2,$2,0xffff
b	$L659
addu	$4,$3,$2

$L676:
bne	$2,$6,$L621
nop

bne	$4,$0,$L678
nop

bne	$3,$0,$L679
nop

lh	$3,2($5)
lhu	$4,0($5)
$L662:
sll	$3,$3,16
$L661:
addu	$4,$3,$4
multu	$4,$2
mfhi	$3
mflo	$2
b	$L610
addu	$3,$4,$3

$L679:
lh	$3,11266($16)
b	$L662
lhu	$4,11264($16)

$L678:
lh	$4,11294($16)
lhu	$3,11292($16)
b	$L661
sll	$4,$4,16

.set	macro
.set	reorder
.end	decode_mb_skip_hw
.size	decode_mb_skip_hw, .-decode_mb_skip_hw
.section	.rodata.str1.4
.align	2
$LC2:
.ascii	"decode_cabac_mb_type failed\012\000"
.align	2
$LC3:
.ascii	"top block unavailable for requested intra4x4 mode %d at "
.ascii	"%d %d\012\000"
.align	2
$LC4:
.ascii	"left block unavailable for requested intra4x4 mode %d at"
.ascii	" %d %d\012\000"
.align	2
$LC5:
.ascii	"out of range intra chroma pred mode at %d %d\012\000"
.align	2
$LC6:
.ascii	"top block unavailable for requested intra mode at %d %d\012"
.ascii	"\000"
.align	2
$LC7:
.ascii	"left block unavailable for requested intra mode at %d %d"
.ascii	"\012\000"
.align	2
$LC8:
.ascii	"cabac decode of qscale diff failed at %d %d\012\000"
.section	.text.ff_h264_decode_mb_cabac_hw,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_mb_cabac_hw
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_mb_cabac_hw
.type	ff_h264_decode_mb_cabac_hw, @function
ff_h264_decode_mb_cabac_hw:
.frame	$sp,208,$31		# vars= 128, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-208
lw	$6,7996($4)
lw	$5,168($4)
lw	$9,%got(sde_res_di)($28)
.cprestore	32
sw	$21,188($sp)
move	$21,$4
sw	$31,204($sp)
sw	$fp,200($sp)
sw	$23,196($sp)
sw	$22,192($sp)
sw	$20,184($sp)
sw	$19,180($sp)
sw	$18,176($sp)
sw	$17,172($sp)
sw	$16,168($sp)
sw	$0,0($9)
mul	$9,$6,$5
lw	$8,12888($21)
lw	$4,7992($4)
lw	$2,%got(sde_res_dct)($28)
sw	$8,108($sp)
addiu	$3,$2,816
addu	$8,$9,$4
sw	$8,80($sp)
$L683:
sh	$0,0($2)
addiu	$2,$2,2
bne	$2,$3,$L683
lw	$25,%got(sde_nnz_dct)($28)

addiu	$3,$25,108
move	$2,$25
$L684:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L684
li	$16,65536			# 0x10000

lw	$2,%got(sde_v_dc)($28)
lw	$3,%got(sde_v_dc)($28)
lw	$25,3008($21)
addu	$16,$21,$16
sh	$0,0($2)
li	$2,131072			# 0x20000
sh	$0,2($3)
addiu	$2,$2,6944
sh	$0,4($3)
sh	$0,6($3)
addu	$2,$21,$2
move	$4,$2
jalr	$25
sw	$2,104($sp)

li	$3,-5			# 0xfffffffffffffffb
lw	$2,-5264($16)
and	$2,$2,$3
li	$3,1			# 0x1
beq	$2,$3,$L685
lw	$28,32($sp)

lw	$2,-5252($16)
bne	$2,$0,$L1160
nop

lw	$6,7996($21)
lw	$16,%got(decode_cabac_mb_skip_hw)($28)
$L1233:
move	$4,$21
addiu	$16,$16,%lo(decode_cabac_mb_skip_hw)
move	$25,$16
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_skip_hw
1:	jalr	$25
lw	$5,7992($21)

beq	$2,$0,$L685
lw	$28,32($sp)

li	$17,65536			# 0x10000
addu	$17,$21,$17
lw	$2,-5252($17)
beq	$2,$0,$L1240
lw	$25,%got(decode_mb_skip_hw)($28)

lw	$2,7996($21)
andi	$2,$2,0x1
bne	$2,$0,$L1240
lw	$8,80($sp)

move	$4,$21
lw	$2,2192($21)
move	$25,$16
sll	$3,$8,2
addu	$2,$2,$3
li	$3,2048			# 0x800
sw	$3,0($2)
lw	$6,7996($21)
lw	$5,7992($21)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_skip_hw
1:	jalr	$25
addiu	$6,$6,1

move	$4,$21
lw	$28,32($sp)
beq	$2,$0,$L698
sw	$2,10756($21)

lw	$25,%got(predict_field_decoding_flag_hw)($28)
addiu	$25,$25,%lo(predict_field_decoding_flag_hw)
.reloc	1f,R_MIPS_JALR,predict_field_decoding_flag_hw
1:	jalr	$25
nop

lw	$28,32($sp)
$L696:
lw	$25,%got(decode_mb_skip_hw)($28)
$L1240:
addiu	$25,$25,%lo(decode_mb_skip_hw)
.reloc	1f,R_MIPS_JALR,decode_mb_skip_hw
1:	jalr	$25
move	$4,$21

move	$2,$0
lw	$3,80($sp)
lw	$8,80($sp)
sll	$6,$3,1
li	$3,131072			# 0x20000
addu	$3,$21,$3
lw	$5,8732($3)
lw	$4,8748($3)
addu	$5,$5,$6
addu	$4,$4,$8
sh	$0,0($5)
sb	$0,0($4)
sw	$0,8752($3)
lw	$31,204($sp)
lw	$fp,200($sp)
lw	$23,196($sp)
lw	$22,192($sp)
lw	$21,188($sp)
lw	$20,184($sp)
lw	$19,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
j	$31
addiu	$sp,$sp,208

$L685:
lw	$6,7996($21)
li	$16,65536			# 0x10000
$L1232:
addu	$16,$21,$16
lw	$3,-5252($16)
bne	$3,$0,$L1161
andi	$9,$6,0x1

lw	$7,168($21)
lw	$2,7992($21)
lw	$3,10384($21)
mul	$4,$7,$6
xori	$3,$3,0x3
sltu	$3,$0,$3
sw	$3,-5248($16)
sw	$0,10752($21)
addu	$2,$4,$2
subu	$8,$2,$7
addiu	$2,$2,-1
sw	$8,10772($21)
sw	$2,10780($21)
lw	$3,10384($21)
$L1234:
li	$2,3			# 0x3
beq	$3,$2,$L709
subu	$7,$8,$7

sw	$7,10772($21)
$L709:
li	$2,65536			# 0x10000
li	$3,1			# 0x1
addu	$2,$21,$2
lw	$18,-5264($2)
beq	$18,$3,$L1162
li	$3,2			# 0x2

beq	$18,$3,$L1163
li	$3,3			# 0x3

bne	$18,$3,$L716
nop

lw	$3,-5276($2)
lw	$6,10780($21)
lw	$4,-5272($2)
addu	$2,$3,$6
lbu	$2,0($2)
beq	$2,$4,$L1164
lw	$5,10772($21)

move	$2,$0
$L717:
addu	$3,$3,$5
lbu	$3,0($3)
beq	$4,$3,$L1165
sll	$5,$5,2

$L718:
li	$19,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
addu	$2,$2,$19
addiu	$18,$19,8224
addiu	$5,$2,8299
addu	$18,$21,$18
addiu	$16,$17,%lo(get_cabac_noinline)
addu	$5,$21,$5
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$18

bne	$2,$0,$L1166
lw	$28,32($sp)

$L719:
li	$3,65536			# 0x10000
sw	$2,60($sp)
li	$4,3			# 0x3
addu	$3,$21,$3
lw	$3,-5264($3)
beq	$3,$4,$L946
nop

$L724:
li	$4,2			# 0x2
beq	$3,$4,$L728
slt	$3,$2,5

lw	$2,60($sp)
$L727:
lw	$3,%got(i_mb_type_info)($28)
sll	$2,$2,2
move	$16,$0
addiu	$3,$3,%lo(i_mb_type_info)
addu	$2,$2,$3
lbu	$4,2($2)
lhu	$3,0($2)
lbu	$2,3($2)
sw	$4,10764($21)
sw	$3,60($sp)
sw	$2,96($sp)
$L726:
li	$2,65536			# 0x10000
addu	$2,$21,$2
lw	$2,-5248($2)
beq	$2,$0,$L1241
li	$8,65536			# 0x10000

lw	$2,60($sp)
ori	$2,$2,0x80
sw	$2,60($sp)
$L1241:
lw	$9,80($sp)
addu	$2,$21,$8
lw	$3,-5276($2)
lw	$4,-5272($2)
addu	$3,$3,$9
sb	$4,0($3)
lw	$5,60($sp)
andi	$3,$5,0x4
beq	$3,$0,$L731
li	$3,131072			# 0x20000

move	$10,$0
addu	$3,$21,$3
addiu	$8,$8,3472
li	$9,16			# 0x10
lw	$2,8224($3)
lw	$6,8240($3)
andi	$3,$2,0x1
subu	$6,$6,$3
andi	$2,$2,0x1ff
addiu	$3,$6,-1
movn	$6,$3,$2
move	$7,$6
$L735:
srl	$11,$10,3
srl	$2,$10,2
sll	$11,$11,2
andi	$2,$2,0x1
addu	$2,$2,$11
andi	$11,$10,0x3
sll	$2,$2,3
move	$4,$0
addu	$11,$2,$11
sll	$11,$11,2
$L734:
srl	$2,$4,3
srl	$5,$4,2
sll	$3,$2,2
andi	$2,$5,0x1
addu	$2,$3,$2
andi	$5,$4,0x3
sll	$2,$2,4
addu	$3,$7,$4
addu	$2,$2,$11
addiu	$4,$4,1
addu	$2,$2,$5
lbu	$3,0($3)
addu	$2,$2,$8
sll	$2,$2,1
addu	$2,$21,$2
bne	$4,$9,$L734
sh	$3,0($2)

addiu	$10,$10,1
bne	$10,$4,$L735
addiu	$7,$7,16

li	$9,65536			# 0x10000
addiu	$5,$6,256
addiu	$11,$6,320
move	$10,$0
addiu	$9,$9,3472
li	$12,8			# 0x8
$L737:
srl	$8,$10,2
andi	$2,$10,0x3
addiu	$2,$2,64
sll	$8,$8,3
move	$3,$0
addu	$8,$2,$8
sll	$8,$8,2
$L736:
srl	$2,$3,2
andi	$7,$3,0x3
sll	$2,$2,4
addu	$4,$5,$3
addu	$2,$2,$8
addiu	$3,$3,1
addu	$2,$2,$7
lbu	$4,0($4)
addu	$2,$2,$9
sll	$2,$2,1
addu	$2,$21,$2
bne	$3,$12,$L736
sh	$4,0($2)

addiu	$5,$5,8
bne	$5,$11,$L737
addiu	$10,$10,1

li	$9,65536			# 0x10000
addiu	$6,$6,384
move	$10,$0
addiu	$9,$9,3472
li	$11,8			# 0x8
$L739:
srl	$8,$10,2
andi	$2,$10,0x3
addiu	$2,$2,80
sll	$8,$8,3
move	$3,$0
addu	$8,$2,$8
sll	$8,$8,2
$L738:
srl	$2,$3,2
andi	$7,$3,0x3
sll	$2,$2,4
addu	$4,$5,$3
addu	$2,$2,$8
addiu	$3,$3,1
addu	$2,$2,$7
lbu	$4,0($4)
addu	$2,$2,$9
sll	$2,$2,1
addu	$2,$21,$2
bne	$3,$11,$L738
sh	$4,0($2)

addiu	$5,$5,8
bne	$5,$6,$L739
addiu	$10,$10,1

li	$2,131072			# 0x20000
lw	$25,%call16(ff_init_cabac_decoder)($28)
addu	$16,$21,$2
addiu	$4,$2,8224
lw	$6,8244($16)
addu	$4,$21,$4
.reloc	1f,R_MIPS_JALR,ff_init_cabac_decoder
1:	jalr	$25
subu	$6,$6,$5

li	$5,16			# 0x10
lw	$25,80($sp)
lw	$3,8732($16)
lw	$2,8748($16)
sll	$6,$25,1
lw	$8,80($sp)
lw	$28,32($sp)
sll	$4,$25,5
addu	$3,$3,$6
li	$6,495			# 0x1ef
addu	$2,$2,$8
lw	$25,%call16(memset)($28)
sh	$6,0($3)
li	$6,16			# 0x10
sb	$0,0($2)
lw	$2,2172($21)
addu	$2,$2,$8
sb	$0,0($2)
lw	$3,11232($21)
lbu	$7,13116($21)
lbu	$2,13372($21)
addu	$4,$3,$4
sw	$7,10740($21)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$2,10744($21)

move	$2,$0
lw	$9,80($sp)
lw	$3,2192($21)
sll	$4,$9,2
addu	$3,$3,$4
lw	$4,60($sp)
sw	$4,0($3)
$L1031:
lw	$31,204($sp)
lw	$fp,200($sp)
lw	$23,196($sp)
lw	$22,192($sp)
lw	$21,188($sp)
lw	$20,184($sp)
lw	$19,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
j	$31
addiu	$sp,$sp,208

$L1161:
beq	$9,$0,$L1167
lw	$25,%got(decode_cabac_field_decoding_flag_hw)($28)

lw	$7,168($21)
lw	$3,7992($21)
sw	$0,10752($21)
mul	$4,$7,$6
addu	$2,$4,$3
subu	$8,$2,$7
addiu	$2,$2,-1
sw	$8,10772($21)
sw	$2,10780($21)
$L702:
li	$2,-2			# 0xfffffffffffffffe
lw	$5,2192($21)
and	$2,$6,$2
mul	$4,$7,$2
addu	$2,$4,$3
li	$3,65536			# 0x10000
sll	$4,$2,2
addu	$3,$21,$3
addu	$4,$5,$4
lw	$6,-5248($3)
subu	$3,$2,$7
lw	$4,-4($4)
sll	$3,$3,2
sltu	$6,$6,1
addu	$5,$5,$3
srl	$4,$4,7
xori	$4,$4,0x1
lw	$3,0($5)
bne	$9,$0,$L1168
andi	$4,$4,0x1

bne	$6,$0,$L707
srl	$3,$3,7

andi	$3,$3,0x1
$L705:
beq	$3,$0,$L707
subu	$7,$8,$7

sw	$7,10772($21)
$L707:
beq	$4,$6,$L709
addiu	$2,$2,-1

b	$L709
sw	$2,10780($21)

$L1160:
lw	$2,7992($21)
beq	$2,$0,$L1169
lw	$6,7996($21)

$L690:
andi	$2,$6,0x1
beq	$2,$0,$L1233
lw	$16,%got(decode_cabac_mb_skip_hw)($28)

$L689:
lw	$2,10752($21)
beq	$2,$0,$L1233
lw	$16,%got(decode_cabac_mb_skip_hw)($28)

lw	$2,10756($21)
bne	$2,$0,$L1240
lw	$25,%got(decode_mb_skip_hw)($28)

b	$L1232
li	$16,65536			# 0x10000

$L731:
lw	$3,-5244($2)
beq	$3,$0,$L1242
lw	$25,%call16(fill_caches_hw)($28)

lw	$4,6624($2)
lw	$3,6628($2)
sll	$4,$4,1
sll	$3,$3,1
sw	$4,6624($2)
sw	$3,6628($2)
$L1242:
move	$6,$0
.reloc	1f,R_MIPS_JALR,fill_caches_hw
1:	jalr	$25
move	$4,$21

lw	$3,60($sp)
andi	$2,$3,0x7
bne	$2,$0,$L1170
lw	$28,32($sp)

li	$2,4			# 0x4
beq	$16,$2,$L1171
andi	$2,$3,0x100

bne	$2,$0,$L1172
andi	$2,$3,0x8

beq	$2,$0,$L833
andi	$2,$3,0x10

li	$2,65536			# 0x10000
addu	$4,$21,$2
lw	$5,6632($4)
beq	$5,$0,$L831
addiu	$2,$2,6624

lw	$23,%got(decode_cabac_mb_ref_hw)($28)
addiu	$20,$21,11580
addu	$18,$21,$2
move	$17,$0
li	$22,4096			# 0x1000
li	$19,-1			# 0xffffffffffffffff
move	$16,$20
b	$L838
move	$fp,$4

$L1174:
lw	$7,0($18)
move	$4,$21
move	$5,$17
sltu	$7,$7,2
bne	$7,$0,$L836
move	$2,$0

addiu	$25,$23,%lo(decode_cabac_mb_ref_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref_hw
1:	jalr	$25
nop

sll	$5,$2,8
lw	$28,32($sp)
lw	$3,60($sp)
addu	$2,$5,$2
sll	$4,$2,16
addu	$2,$2,$4
$L836:
sw	$2,0($16)
sw	$2,8($16)
sw	$2,16($16)
sw	$2,24($16)
$L837:
lw	$2,6632($fp)
addiu	$17,$17,1
addiu	$16,$16,40
sltu	$5,$17,$2
beq	$5,$0,$L1173
addiu	$18,$18,4

$L838:
sll	$2,$17,1
sll	$2,$22,$2
and	$2,$2,$3
bne	$2,$0,$L1174
move	$6,$0

sw	$19,0($16)
sw	$19,8($16)
sw	$19,16($16)
b	$L837
sw	$19,24($16)

$L1166:
addiu	$5,$19,8302
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

beq	$2,$0,$L1175
addiu	$5,$19,8303

addiu	$17,$19,8304
addu	$5,$21,$5
addu	$17,$21,$17
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$18

move	$4,$18
move	$5,$17
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
sll	$19,$2,3

sll	$2,$2,2
move	$4,$18
move	$5,$17
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
or	$19,$19,$2

move	$20,$2
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$17

sll	$20,$20,1
or	$2,$19,$2
or	$2,$2,$20
slt	$3,$2,8
bne	$3,$0,$L1176
lw	$28,32($sp)

li	$3,13			# 0xd
beq	$2,$3,$L1177
li	$3,14			# 0xe

beq	$2,$3,$L960
li	$3,15			# 0xf

bne	$2,$3,$L1178
move	$4,$18

b	$L719
li	$2,22			# 0x16

$L1170:
andi	$3,$3,0x1
beq	$3,$0,$L742
lw	$25,108($sp)

bne	$25,$0,$L743
li	$2,131072			# 0x20000

$L745:
lw	$2,%got(scan8)($28)
li	$18,2			# 0x2
lw	$16,%got(decode_cabac_mb_intra4x4_pred_mode_hw)($28)
lw	$17,%got(scan8+16)($28)
addiu	$20,$2,%lo(scan8)
addiu	$16,$16,%lo(decode_cabac_mb_intra4x4_pred_mode_hw)
addiu	$17,$17,%lo(scan8+16)
$L744:
lbu	$19,0($20)
move	$4,$21
move	$25,$16
addiu	$20,$20,1
addu	$19,$21,$19
lb	$5,10808($19)
lb	$2,10815($19)
slt	$3,$2,$5
movz	$2,$5,$3
slt	$5,$2,0
movn	$2,$18,$5
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_intra4x4_pred_mode_hw
1:	jalr	$25
move	$5,$2

lw	$28,32($sp)
bne	$20,$17,$L744
sb	$2,10816($19)

lw	$4,7996($21)
$L1238:
lw	$6,168($21)
lw	$2,7992($21)
lbu	$3,10852($21)
mul	$7,$4,$6
lb	$5,10831($21)
lw	$4,10860($21)
sll	$3,$3,4
lw	$6,%got(sde_luma_neighbor)($28)
addu	$3,$3,$5
addu	$2,$7,$2
sb	$3,0($6)
sll	$2,$2,3
addu	$4,$4,$2
sb	$5,0($4)
lw	$3,10860($21)
lb	$4,10839($21)
addu	$3,$3,$2
sb	$4,1($3)
lw	$3,10860($21)
lb	$4,10847($21)
addu	$3,$3,$2
sb	$4,2($3)
lw	$3,10860($21)
lb	$4,10855($21)
addu	$3,$3,$2
sb	$4,3($3)
lw	$3,10860($21)
lb	$4,10852($21)
addu	$3,$3,$2
sb	$4,4($3)
lw	$3,10860($21)
lb	$4,10853($21)
addu	$3,$3,$2
sb	$4,5($3)
lw	$3,10860($21)
lb	$4,10854($21)
addu	$2,$3,$2
sb	$4,6($2)
lw	$2,11088($21)
andi	$2,$2,0x8000
bne	$2,$0,$L749
lw	$4,%got(top.7811)($28)

addiu	$2,$21,10828
addiu	$5,$21,10832
addiu	$4,$4,%lo(top.7811)
$L757:
lb	$3,0($2)
addu	$3,$4,$3
lb	$7,0($3)
bltz	$7,$L1179
lw	$6,%got($LC3)($28)

beq	$7,$0,$L756
nop

sb	$7,0($2)
$L756:
addiu	$2,$2,1
bne	$2,$5,$L757
nop

$L749:
lw	$2,11096($21)
andi	$2,$2,0x8000
bne	$2,$0,$L948
lw	$4,%got(left.7812)($28)

addiu	$2,$21,10828
addiu	$5,$21,10860
addiu	$4,$4,%lo(left.7812)
$L760:
lb	$3,0($2)
addu	$3,$4,$3
lb	$7,0($3)
bltz	$7,$L1180
lw	$6,%got($LC4)($28)

beq	$7,$0,$L759
nop

sb	$7,0($2)
$L759:
addiu	$2,$2,8
bne	$2,$5,$L760
nop

$L948:
li	$5,65536			# 0x10000
lw	$2,10780($21)
li	$6,131072			# 0x20000
lw	$8,80($sp)
addu	$5,$21,$5
lw	$7,10772($21)
addu	$6,$21,$6
lw	$4,-5276($5)
lw	$3,8748($6)
lw	$5,-5272($5)
addu	$6,$4,$2
lbu	$6,0($6)
beq	$6,$5,$L1181
addu	$18,$3,$8

move	$2,$0
$L765:
addu	$4,$4,$7
lbu	$4,0($4)
beq	$5,$4,$L1182
addu	$3,$3,$7

$L766:
li	$19,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
addu	$2,$2,$19
addiu	$20,$19,8224
addiu	$5,$2,8336
addu	$20,$21,$20
addiu	$16,$17,%lo(get_cabac_noinline)
addu	$5,$21,$5
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$20

bne	$2,$0,$L1183
lw	$28,32($sp)

move	$2,$0
move	$3,$0
$L767:
lw	$5,%got(sde_chroma_neighbor)($28)
sb	$2,0($18)
lw	$4,11088($21)
sb	$2,0($5)
andi	$2,$4,0x8000
bne	$2,$0,$L768
lw	$2,%got(top.7827)($28)

addiu	$2,$2,%lo(top.7827)
addu	$3,$3,$2
lb	$3,0($3)
bltz	$3,$L1184
li	$5,16			# 0x10

$L768:
lw	$2,11096($21)
andi	$2,$2,0x8000
bne	$2,$0,$L769
move	$2,$3

lw	$2,%got(left.7828)($28)
addiu	$2,$2,%lo(left.7828)
addu	$3,$3,$2
lb	$2,0($3)
bltz	$2,$L1185
li	$5,16			# 0x10

$L769:
lw	$3,60($sp)
sw	$2,10760($21)
$L831:
andi	$2,$3,0x78
bne	$2,$0,$L1186
andi	$5,$3,0x2

beq	$5,$0,$L1243
li	$2,65536			# 0x10000

$L893:
li	$2,131072			# 0x20000
$L1247:
lw	$9,96($sp)
lw	$8,80($sp)
addu	$5,$21,$2
lw	$25,108($sp)
sw	$9,8736($5)
sll	$4,$8,1
lw	$5,8732($5)
addu	$4,$5,$4
beq	$25,$0,$L904
sh	$9,0($4)

andi	$4,$9,0xf
beq	$4,$0,$L1244
lw	$8,80($sp)

andi	$4,$3,0x7
beq	$4,$0,$L1188
lw	$17,%got(get_cabac_noinline)($28)

addiu	$2,$2,8224
addu	$18,$21,$2
addiu	$17,$17,%lo(get_cabac_noinline)
$L907:
lw	$8,80($sp)
lw	$2,2192($21)
sll	$4,$8,2
addu	$2,$2,$4
sw	$3,0($2)
lw	$2,60($sp)
$L909:
andi	$2,$2,0x80
beq	$2,$0,$L911
nop

lw	$2,2872($21)
beq	$2,$0,$L912
li	$2,131072			# 0x20000

lw	$fp,%got(luma_dc_field_scan)($28)
addiu	$3,$2,9292
addiu	$2,$2,9276
addu	$3,$21,$3
addu	$2,$21,$2
addiu	$fp,$fp,%lo(luma_dc_field_scan)
sw	$3,68($sp)
sw	$2,64($sp)
sw	$fp,72($sp)
$L913:
li	$19,131072			# 0x20000
move	$fp,$0
addu	$2,$21,$19
li	$23,2			# 0x2
addiu	$19,$19,8272
lw	$3,8752($2)
li	$22,3			# 0x3
li	$20,103			# 0x67
b	$L915
sltu	$16,$0,$3

$L918:
move	$4,$22
movn	$4,$23,$16
addiu	$fp,$fp,1
beq	$fp,$20,$L917
move	$16,$4

$L915:
addiu	$5,$16,60
move	$4,$18
addu	$5,$5,$19
move	$25,$17
addu	$5,$21,$5
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
slt	$16,$16,2

bne	$2,$0,$L918
lw	$28,32($sp)

andi	$2,$fp,0x1
bne	$2,$0,$L1189
addiu	$6,$fp,1

nor	$6,$0,$fp
srl	$2,$6,31
addu	$6,$2,$6
sra	$6,$6,1
$L920:
lw	$2,2872($21)
li	$3,131072			# 0x20000
addu	$3,$21,$3
addu	$2,$6,$2
sw	$6,8752($3)
sltu	$3,$2,52
bne	$3,$0,$L1144
nop

bltz	$2,$L1190
nop

addiu	$2,$2,-52
$L1144:
sw	$2,2872($21)
$L922:
andi	$2,$2,0xff
lw	$3,60($sp)
addu	$2,$21,$2
andi	$3,$3,0x2
lbu	$4,13116($2)
lbu	$2,13372($2)
sw	$4,10740($21)
bne	$3,$0,$L1191
sw	$2,10744($21)

li	$18,131072			# 0x20000
move	$7,$0
addiu	$18,$18,6944
move	$23,$0
addu	$18,$21,$18
move	$20,$21
$L932:
lw	$3,96($sp)
sra	$2,$3,$23
andi	$2,$2,0x1
beq	$2,$0,$L927
addiu	$19,$7,4

lw	$2,60($sp)
li	$5,16777216			# 0x1000000
and	$4,$2,$5
bne	$4,$0,$L1192
andi	$2,$2,0x7

li	$4,3			# 0x3
lw	$5,2872($20)
addiu	$19,$7,4
movn	$4,$0,$2
li	$21,16			# 0x10
sll	$5,$5,6
lw	$2,%got(decode_cabac_residual_hw)($28)
move	$16,$7
move	$17,$18
addiu	$4,$4,15056
addiu	$fp,$2,%lo(decode_cabac_residual_hw)
sll	$4,$4,2
addu	$4,$20,$4
lw	$22,0($4)
addu	$22,$22,$5
$L931:
lw	$3,64($sp)
move	$7,$16
li	$6,2			# 0x2
sw	$22,20($sp)
addiu	$16,$16,1
sw	$21,24($sp)
move	$5,$17
sw	$3,16($sp)
move	$25,$fp
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
move	$4,$20

addiu	$17,$17,32
bne	$19,$16,$L931
lw	$28,32($sp)

$L929:
addiu	$23,$23,1
li	$2,4			# 0x4
move	$7,$19
bne	$23,$2,$L932
addiu	$18,$18,128

move	$21,$20
$L926:
lw	$3,96($sp)
$L1236:
andi	$2,$3,0x30
beq	$2,$0,$L1245
lw	$3,96($sp)

lw	$19,%got(chroma_dc_scan)($28)
li	$18,131072			# 0x20000
lw	$17,%got(decode_cabac_residual_hw)($28)
li	$20,4			# 0x4
addiu	$5,$18,7456
sw	$0,20($sp)
addiu	$19,$19,%lo(chroma_dc_scan)
addiu	$17,$17,%lo(decode_cabac_residual_hw)
sw	$20,24($sp)
addu	$5,$21,$5
li	$6,3			# 0x3
sw	$19,16($sp)
move	$7,$0
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
move	$4,$21

addiu	$5,$18,7584
li	$6,3			# 0x3
sw	$19,16($sp)
addu	$5,$21,$5
sw	$0,20($sp)
li	$7,1			# 0x1
sw	$20,24($sp)
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
move	$4,$21

lw	$28,32($sp)
lw	$3,96($sp)
$L1245:
andi	$2,$3,0x20
beq	$2,$0,$L935
lw	$5,%got(sde_nnz_dct)($28)

li	$2,131072			# 0x20000
lw	$8,64($sp)
lw	$17,%got(decode_cabac_residual_hw)($28)
li	$20,15			# 0xf
addiu	$2,$2,7456
sw	$0,64($sp)
addiu	$16,$8,1
addu	$2,$21,$2
addiu	$17,$17,%lo(decode_cabac_residual_hw)
sw	$2,68($sp)
move	$2,$0
$L940:
lw	$4,60($sp)
li	$25,3			# 0x3
addiu	$22,$2,1
lw	$9,64($sp)
lw	$fp,68($sp)
andi	$4,$4,0x7
movn	$25,$0,$4
addu	$5,$21,$9
sll	$19,$22,2
move	$23,$9
lw	$5,10740($5)
addu	$4,$25,$22
addiu	$4,$4,15056
sll	$5,$5,6
sll	$4,$4,2
addu	$4,$21,$4
lw	$18,0($4)
addu	$18,$18,$5
$L937:
move	$7,$23
sw	$16,16($sp)
li	$6,4			# 0x4
sw	$18,20($sp)
addiu	$23,$23,1
sw	$20,24($sp)
move	$5,$fp
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
move	$4,$21

addiu	$fp,$fp,32
bne	$23,$19,$L937
lw	$28,32($sp)

li	$2,1			# 0x1
beq	$22,$2,$L1193
lw	$3,%got(sde_nnz_dct)($28)

$L938:
lw	$2,68($sp)
$L1257:
li	$4,2			# 0x2
lw	$3,64($sp)
addiu	$2,$2,128
addiu	$3,$3,4
sw	$2,68($sp)
li	$2,1			# 0x1
bne	$22,$4,$L940
sw	$3,64($sp)

$L941:
lw	$3,2172($21)
li	$2,65536			# 0x10000
lw	$8,80($sp)
lw	$4,2872($21)
addu	$2,$21,$2
addu	$3,$3,$8
sb	$4,0($3)
lw	$4,7996($21)
lw	$7,168($21)
lw	$5,7992($21)
lw	$3,11232($21)
mul	$8,$4,$7
lbu	$6,11199($21)
addu	$5,$8,$5
sll	$5,$5,5
addu	$3,$3,$5
sb	$6,0($3)
lw	$3,11232($21)
lbu	$4,11207($21)
addu	$3,$3,$5
sb	$4,1($3)
lw	$3,11232($21)
lbu	$4,11215($21)
addu	$3,$3,$5
sb	$4,2($3)
lw	$3,11232($21)
lbu	$4,11223($21)
addu	$3,$3,$5
sb	$4,3($3)
lw	$3,11232($21)
lbu	$4,11220($21)
addu	$3,$3,$5
sb	$4,4($3)
lw	$3,11232($21)
lbu	$4,11221($21)
addu	$3,$3,$5
sb	$4,5($3)
lw	$3,11232($21)
lbu	$4,11222($21)
addu	$3,$3,$5
sb	$4,6($3)
lw	$3,11232($21)
lbu	$4,11201($21)
addu	$3,$3,$5
sb	$4,9($3)
lw	$3,11232($21)
lbu	$4,11202($21)
addu	$3,$3,$5
sb	$4,8($3)
lw	$3,11232($21)
lbu	$4,11194($21)
addu	$3,$3,$5
sb	$4,7($3)
lw	$3,11232($21)
lbu	$4,11225($21)
addu	$3,$3,$5
sb	$4,12($3)
lw	$3,11232($21)
lbu	$4,11226($21)
addu	$3,$3,$5
sb	$4,11($3)
lw	$3,11232($21)
lbu	$4,11218($21)
addu	$3,$3,$5
sb	$4,10($3)
lw	$2,-5252($2)
beq	$2,$0,$L944
lw	$2,%got(scan8)($28)

move	$4,$0
move	$3,$0
li	$6,16			# 0x10
addiu	$2,$2,%lo(scan8)
move	$7,$2
sw	$2,76($sp)
$L945:
addu	$2,$7,$3
lbu	$2,0($2)
addu	$2,$21,$2
lbu	$2,11184($2)
sltu	$2,$0,$2
sll	$2,$2,$3
addiu	$3,$3,1
bne	$3,$6,$L945
addu	$4,$4,$2

lw	$2,11232($21)
addu	$5,$2,$5
sh	$4,14($5)
$L944:
li	$3,65536			# 0x10000
addu	$3,$21,$3
lw	$2,-5244($3)
beq	$2,$0,$L1031
lw	$31,204($sp)

lw	$5,6624($3)
move	$2,$0
lw	$4,6628($3)
srl	$5,$5,1
lw	$fp,200($sp)
srl	$4,$4,1
lw	$23,196($sp)
lw	$22,192($sp)
lw	$21,188($sp)
lw	$20,184($sp)
lw	$19,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
sw	$5,6624($3)
sw	$4,6628($3)
j	$31
addiu	$sp,$sp,208

$L1168:
b	$L705
xori	$3,$6,0x1

$L1169:
andi	$2,$6,0x1
bne	$2,$0,$L689
lw	$25,%got(predict_field_decoding_flag_hw)($28)

addiu	$25,$25,%lo(predict_field_decoding_flag_hw)
.reloc	1f,R_MIPS_JALR,predict_field_decoding_flag_hw
1:	jalr	$25
move	$4,$21

lw	$2,-5252($16)
lw	$28,32($sp)
bne	$2,$0,$L690
lw	$6,7996($21)

b	$L1233
lw	$16,%got(decode_cabac_mb_skip_hw)($28)

$L1175:
addiu	$5,$19,8304
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

lw	$28,32($sp)
addiu	$2,$2,1
$L712:
bltz	$2,$L947
sw	$2,60($sp)

li	$3,65536			# 0x10000
li	$4,3			# 0x3
addu	$3,$21,$3
lw	$3,-5264($3)
bne	$3,$4,$L724
nop

slt	$3,$2,23
beq	$3,$0,$L725
nop

$L946:
lw	$3,%got(b_mb_type_info)($28)
sll	$2,$2,2
sw	$0,96($sp)
addiu	$3,$3,%lo(b_mb_type_info)
addu	$2,$2,$3
lhu	$3,0($2)
lbu	$16,2($2)
b	$L726
sw	$3,60($sp)

$L927:
lw	$2,%got(scan8)($28)
addiu	$2,$2,%lo(scan8)
addu	$4,$2,$7
lbu	$5,0($4)
addiu	$4,$5,11184
addu	$5,$20,$5
addu	$4,$20,$4
sb	$0,9($4)
sb	$0,8($4)
sb	$0,1($4)
b	$L929
sb	$0,11184($5)

$L904:
lw	$8,80($sp)
$L1244:
lw	$2,2192($21)
lw	$9,96($sp)
sll	$4,$8,2
addu	$2,$2,$4
bne	$9,$0,$L1194
sw	$3,0($2)

lw	$2,60($sp)
andi	$3,$2,0x2
beq	$3,$0,$L910
nop

li	$18,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
addiu	$18,$18,8224
addiu	$17,$17,%lo(get_cabac_noinline)
b	$L909
addu	$18,$21,$18

$L728:
beq	$3,$0,$L729
lw	$3,%got(p_mb_type_info)($28)

sll	$2,$2,2
sw	$0,96($sp)
addiu	$3,$3,%lo(p_mb_type_info)
addu	$2,$2,$3
lhu	$3,0($2)
lbu	$16,2($2)
b	$L726
sw	$3,60($sp)

$L1165:
lw	$3,2192($21)
addiu	$4,$2,1
addu	$5,$3,$5
lw	$3,0($5)
andi	$3,$3,0x100
b	$L718
movz	$2,$4,$3

$L1164:
lw	$2,2192($21)
sll	$6,$6,2
addu	$6,$2,$6
lw	$2,0($6)
srl	$2,$2,8
xori	$2,$2,0x1
b	$L717
andi	$2,$2,0x1

$L911:
lw	$2,2872($21)
beq	$2,$0,$L914
li	$2,131072			# 0x20000

lw	$fp,%got(luma_dc_zigzag_scan)($28)
addiu	$3,$2,9148
addiu	$2,$2,9132
addu	$3,$21,$3
addu	$2,$21,$2
addiu	$fp,$fp,%lo(luma_dc_zigzag_scan)
sw	$3,68($sp)
sw	$2,64($sp)
b	$L913
sw	$fp,72($sp)

$L935:
lw	$2,104($5)
beq	$2,$0,$L942
lw	$6,%got(sde_res_di)($28)

move	$2,$0
li	$7,8			# 0x8
lw	$4,0($6)
sll	$6,$4,1
lw	$8,%got(sde_v_dc)($28)
$L1259:
addu	$3,$2,$6
lw	$9,%got(sde_res_dct)($28)
addu	$5,$8,$2
addu	$3,$9,$3
addiu	$2,$2,2
lhu	$5,0($5)
bne	$2,$7,$L1259
sh	$5,0($3)

addiu	$4,$4,4
lw	$25,%got(sde_res_di)($28)
sw	$4,0($25)
$L942:
sb	$0,11226($21)
sb	$0,11225($21)
sb	$0,11218($21)
sb	$0,11217($21)
sb	$0,11202($21)
sb	$0,11201($21)
sb	$0,11194($21)
b	$L941
sb	$0,11193($21)

$L1167:
addiu	$25,$25,%lo(decode_cabac_field_decoding_flag_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_field_decoding_flag_hw
1:	jalr	$25
move	$4,$21

lw	$6,7996($21)
lw	$7,168($21)
lw	$3,7992($21)
lw	$5,-5252($16)
andi	$9,$6,0x1
mul	$8,$7,$6
lw	$28,32($sp)
sw	$2,-5248($16)
sw	$2,-5244($16)
sw	$0,10752($21)
addu	$4,$8,$3
subu	$8,$4,$7
addiu	$4,$4,-1
sw	$8,10772($21)
bne	$5,$0,$L702
sw	$4,10780($21)

b	$L1234
lw	$3,10384($21)

$L1192:
sltu	$2,$2,1
lw	$9,68($sp)
addiu	$2,$2,15062
lw	$8,2872($20)
li	$4,64			# 0x40
lw	$21,%got(decode_cabac_residual_hw)($28)
sll	$2,$2,2
sw	$9,16($sp)
sll	$8,$8,8
addu	$2,$20,$2
sw	$4,24($sp)
addiu	$fp,$21,%lo(decode_cabac_residual_hw)
li	$6,5			# 0x5
lw	$2,0($2)
move	$4,$20
move	$5,$18
move	$25,$fp
addu	$8,$2,$8
addiu	$19,$7,4
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
sw	$8,20($sp)

b	$L929
lw	$28,32($sp)

$L1186:
li	$2,131072			# 0x20000
lw	$3,80($sp)
addu	$2,$21,$2
lw	$2,8748($2)
addu	$2,$2,$3
sb	$0,0($2)
lw	$4,7996($21)
lw	$7,11864($21)
lw	$3,7992($21)
sll	$19,$4,2
lw	$2,11868($21)
sll	$4,$4,1
lw	$15,60($sp)
mul	$8,$19,$7
sll	$6,$3,2
sll	$3,$3,1
andi	$5,$15,0x3000
addu	$19,$8,$6
mul	$6,$4,$2
bne	$5,$0,$L880
addu	$20,$6,$3

lw	$3,2276($21)
li	$4,-1			# 0xffffffffffffffff
addu	$3,$3,$20
addu	$2,$3,$2
sh	$4,0($3)
sh	$4,0($2)
$L880:
li	$16,65536			# 0x10000
addu	$16,$21,$16
lw	$2,6632($16)
beq	$2,$0,$L1246
li	$4,65536			# 0x10000

li	$24,131072			# 0x20000
addiu	$22,$19,2
addiu	$14,$24,8756
addiu	$23,$24,8816
sll	$19,$19,2
addiu	$8,$21,11580
addu	$14,$21,$14
addiu	$12,$21,2184
sll	$22,$22,2
move	$5,$0
li	$18,12288			# 0x3000
addiu	$24,$24,8944
move	$11,$21
andi	$25,$15,0x800
$L891:
sll	$2,$5,1
sll	$2,$18,$2
and	$2,$2,$15
beq	$2,$0,$L884
move	$3,$0

lw	$10,0($12)
addiu	$13,$11,128
lw	$17,11864($21)
move	$2,$11
addu	$10,$10,$19
sll	$9,$17,2
$L885:
lw	$6,11296($2)
addu	$4,$10,$3
lw	$7,11300($2)
addiu	$2,$2,32
addu	$3,$3,$9
sw	$6,0($4)
sw	$7,4($4)
lw	$6,11272($2)
lw	$7,11276($2)
sw	$6,8($4)
bne	$13,$2,$L885
sw	$7,12($4)

lw	$2,12828($21)
beq	$2,$0,$L887
nop

beq	$25,$0,$L888
addu	$4,$11,$23

lw	$2,0($14)
sll	$17,$17,3
move	$6,$0
addu	$3,$17,$9
addu	$2,$2,$19
move	$7,$0
addu	$4,$2,$9
sw	$6,0($2)
addu	$17,$2,$17
sw	$6,8($2)
addu	$10,$4,$9
sw	$7,4($2)
addu	$3,$2,$3
sw	$7,12($2)
addu	$9,$10,$9
sw	$6,0($4)
sw	$7,4($4)
sw	$6,8($4)
sw	$7,12($4)
sw	$6,0($10)
sw	$7,4($10)
sw	$6,8($17)
sw	$7,12($17)
sw	$6,0($9)
sw	$7,4($9)
sw	$6,8($3)
sw	$7,12($3)
$L887:
lw	$2,92($12)
$L1235:
lb	$3,0($8)
addu	$2,$2,$20
sb	$3,0($2)
lb	$3,2($8)
sb	$3,1($2)
lw	$3,11868($21)
lb	$4,16($8)
addu	$3,$2,$3
sb	$4,0($3)
lw	$4,11868($21)
lb	$3,18($8)
addu	$2,$2,$4
sb	$3,1($2)
$L884:
lw	$2,6632($16)
addiu	$5,$5,1
addiu	$8,$8,40
addiu	$14,$14,4
sltu	$2,$5,$2
addiu	$11,$11,160
bne	$2,$0,$L891
addiu	$12,$12,4

li	$4,65536			# 0x10000
$L1246:
li	$2,3			# 0x3
addu	$4,$21,$4
lw	$3,-5264($4)
beq	$3,$2,$L1195
lw	$3,60($sp)

$L1208:
andi	$5,$3,0x2
bne	$5,$0,$L1247
li	$2,131072			# 0x20000

li	$2,65536			# 0x10000
$L1243:
lw	$3,10780($21)
addu	$2,$21,$2
lw	$4,-5276($2)
lw	$6,-5272($2)
addu	$2,$4,$3
lbu	$2,0($2)
beq	$2,$6,$L1196
li	$2,131072			# 0x20000

move	$18,$0
move	$5,$0
$L894:
lw	$2,10772($21)
addu	$4,$4,$2
lbu	$2,0($4)
beq	$6,$2,$L1197
li	$4,131072			# 0x20000

lw	$17,%got(get_cabac_noinline)($28)
addu	$5,$5,$4
addiu	$4,$4,8224
addiu	$5,$5,8345
addu	$16,$21,$4
addiu	$17,$17,%lo(get_cabac_noinline)
addu	$5,$21,$5
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$16

move	$5,$0
andi	$3,$2,0x1
move	$22,$2
xori	$3,$3,0x1
$L897:
addu	$5,$3,$5
li	$23,131072			# 0x20000
addiu	$5,$5,73
addiu	$19,$23,8272
move	$4,$16
addu	$5,$5,$19
move	$25,$17
addu	$5,$21,$5
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
li	$20,2			# 0x2

move	$4,$16
sll	$2,$2,1
move	$25,$17
or	$22,$2,$22
move	$2,$0
andi	$5,$22,0x1
movz	$2,$20,$5
addu	$5,$2,$18
addiu	$5,$5,73
addu	$5,$5,$19
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

move	$4,$16
sll	$2,$2,2
move	$25,$17
or	$18,$2,$22
andi	$5,$18,0x2
movn	$20,$0,$5
andi	$2,$18,0x4
sltu	$2,$2,1
addu	$5,$2,$20
addiu	$5,$5,73
addu	$5,$5,$19
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

addu	$3,$21,$23
sll	$2,$2,3
move	$4,$16
lw	$20,8744($3)
or	$18,$2,$18
lw	$2,8740($3)
move	$25,$17
sra	$20,$20,4
sra	$5,$2,4
andi	$20,$20,0x3
sltu	$2,$0,$20
addiu	$3,$2,2
andi	$22,$5,0x3
movn	$2,$3,$22
addiu	$2,$2,77
addu	$5,$2,$19
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

beq	$2,$0,$L901
lw	$28,32($sp)

li	$3,4			# 0x4
li	$2,5			# 0x5
xori	$20,$20,0x2
movn	$2,$3,$20
xori	$5,$22,0x2
move	$4,$16
move	$25,$17
addiu	$3,$2,2
movz	$2,$3,$5
addiu	$2,$2,77
addu	$5,$2,$19
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

addiu	$2,$2,1
lw	$28,32($sp)
sll	$2,$2,4
$L901:
or	$2,$18,$2
lw	$3,60($sp)
b	$L893
sw	$2,96($sp)

$L888:
lw	$6,0($14)
lw	$10,11864($21)
addu	$13,$11,$24
addu	$7,$6,$19
sll	$10,$10,2
addu	$6,$6,$22
$L889:
lw	$2,0($4)
addiu	$4,$4,32
lw	$3,-28($4)
sw	$2,0($7)
sw	$3,4($7)
addu	$7,$7,$10
lw	$2,-24($4)
lw	$3,-20($4)
sw	$2,0($6)
sw	$3,4($6)
bne	$4,$13,$L889
addu	$6,$6,$9

b	$L1235
lw	$2,92($12)

$L725:
b	$L727
addiu	$2,$2,-23

$L1189:
b	$L920
sra	$6,$6,1

$L1163:
li	$19,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
addiu	$20,$19,8224
addiu	$5,$19,8286
addu	$20,$21,$20
addiu	$16,$17,%lo(get_cabac_noinline)
addu	$5,$21,$5
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$4,$20

bne	$2,$0,$L714
lw	$28,32($sp)

addiu	$5,$19,8287
move	$4,$20
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

bne	$2,$0,$L715
move	$4,$20

addiu	$5,$19,8288
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

sll	$3,$2,1
lw	$28,32($sp)
b	$L712
addu	$2,$3,$2

$L910:
li	$2,131072			# 0x20000
sw	$0,11196($21)
sw	$0,11204($21)
addu	$2,$21,$2
sw	$0,11212($21)
sw	$0,11220($21)
sb	$0,11226($21)
sb	$0,11225($21)
sb	$0,11218($21)
sb	$0,11217($21)
sb	$0,11202($21)
sb	$0,11201($21)
sb	$0,11194($21)
sb	$0,11193($21)
b	$L941
sw	$0,8752($2)

$L1194:
li	$18,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
lw	$2,60($sp)
addiu	$18,$18,8224
addiu	$17,$17,%lo(get_cabac_noinline)
b	$L909
addu	$18,$21,$18

$L1191:
lw	$17,%got(decode_cabac_residual_hw)($28)
li	$2,16			# 0x10
lw	$3,72($sp)
move	$6,$0
lw	$5,104($sp)
move	$7,$0
addiu	$17,$17,%lo(decode_cabac_residual_hw)
sw	$2,24($sp)
sw	$0,20($sp)
move	$4,$21
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
sw	$3,16($sp)

lw	$3,96($sp)
andi	$2,$3,0xf
beq	$2,$0,$L924
lw	$28,32($sp)

li	$3,65536			# 0x10000
lw	$2,2872($21)
lw	$8,64($sp)
move	$22,$0
addu	$3,$21,$3
lw	$23,104($sp)
sll	$2,$2,6
addiu	$16,$8,1
lw	$18,-5312($3)
li	$20,15			# 0xf
li	$19,16			# 0x10
addu	$18,$18,$2
$L925:
move	$7,$22
sw	$16,16($sp)
li	$6,1			# 0x1
sw	$18,20($sp)
addiu	$22,$22,1
sw	$20,24($sp)
move	$5,$23
move	$25,$17
.reloc	1f,R_MIPS_JALR,decode_cabac_residual_hw
1:	jalr	$25
move	$4,$21

addiu	$23,$23,32
bne	$22,$19,$L925
lw	$28,32($sp)

b	$L1236
lw	$3,96($sp)

$L914:
lw	$fp,%got(luma_dc_zigzag_scan)($28)
addu	$2,$21,$2
addiu	$fp,$fp,%lo(luma_dc_zigzag_scan)
lw	$3,9424($2)
lw	$2,9420($2)
sw	$fp,72($sp)
sw	$3,68($sp)
b	$L913
sw	$2,64($sp)

$L1162:
lw	$25,%got(decode_cabac_intra_mb_type_hw)($28)
li	$5,3			# 0x3
li	$6,1			# 0x1
addiu	$25,$25,%lo(decode_cabac_intra_mb_type_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_intra_mb_type_hw
1:	jalr	$25
move	$4,$21

b	$L712
lw	$28,32($sp)

$L729:
b	$L727
addiu	$2,$2,-5

$L912:
lw	$fp,%got(luma_dc_field_scan)($28)
addu	$2,$21,$2
addiu	$fp,$fp,%lo(luma_dc_field_scan)
lw	$25,9436($2)
lw	$2,9432($2)
sw	$fp,72($sp)
sw	$25,68($sp)
b	$L913
sw	$2,64($sp)

$L1183:
addiu	$17,$19,8339
move	$4,$20
addu	$17,$21,$17
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$17

bne	$2,$0,$L1198
lw	$28,32($sp)

li	$2,1			# 0x1
b	$L767
li	$3,1			# 0x1

$L716:
li	$2,-1			# 0xffffffffffffffff
sw	$2,60($sp)
$L947:
lw	$6,%got($LC2)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($21)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC2)

b	$L1031
li	$2,-1			# 0xffffffffffffffff

$L1182:
addiu	$4,$2,1
lbu	$3,0($3)
b	$L766
movn	$2,$4,$3

$L1181:
addu	$2,$3,$2
lbu	$2,0($2)
b	$L765
sltu	$2,$0,$2

$L917:
lw	$3,7996($21)
li	$2,131072			# 0x20000
lw	$6,%got($LC8)($28)
li	$8,-2147483648			# 0xffffffff80000000
lw	$4,0($21)
addu	$2,$21,$2
lw	$7,7992($21)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC8)
sw	$8,8752($2)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)

b	$L1031
li	$2,-1			# 0xffffffffffffffff

$L833:
bne	$2,$0,$L842
li	$2,65536			# 0x10000

addu	$16,$21,$2
lw	$4,6632($16)
beq	$4,$0,$L831
addiu	$2,$2,6624

lw	$17,%got(decode_cabac_mb_ref_hw)($28)
addiu	$18,$21,11582
addu	$19,$21,$2
li	$22,1			# 0x1
move	$20,$0
li	$23,4096			# 0x1000
b	$L865
li	$fp,-1			# 0xffffffffffffffff

$L1201:
sh	$fp,-2($18)
sh	$fp,6($18)
and	$2,$3,$2
sh	$fp,14($18)
beq	$2,$0,$L1199
sh	$fp,22($18)

$L954:
lw	$2,0($19)
sltu	$2,$2,2
bne	$2,$0,$L984
li	$6,4			# 0x4

addiu	$25,$17,%lo(decode_cabac_mb_ref_hw)
move	$5,$20
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref_hw
1:	jalr	$25
move	$4,$21

sll	$5,$2,8
lw	$28,32($sp)
lw	$3,60($sp)
addu	$2,$5,$2
andi	$2,$2,0xffff
$L863:
sh	$2,0($18)
sh	$2,8($18)
sh	$2,16($18)
sh	$2,24($18)
$L864:
lw	$2,6632($16)
addiu	$20,$20,1
addiu	$18,$18,40
addiu	$19,$19,4
sltu	$4,$20,$2
beq	$4,$0,$L1200
addiu	$22,$22,2

$L865:
sll	$2,$20,1
sll	$2,$23,$2
and	$2,$2,$3
beq	$2,$0,$L1201
sll	$2,$23,$22

lw	$2,0($19)
sltu	$2,$2,2
beq	$2,$0,$L952
move	$6,$0

move	$2,$0
$L953:
sh	$2,-2($18)
sh	$2,6($18)
sh	$2,14($18)
sh	$2,22($18)
sll	$2,$23,$22
and	$2,$3,$2
bne	$2,$0,$L954
nop

$L1199:
sh	$fp,0($18)
sh	$fp,8($18)
sh	$fp,16($18)
b	$L864
sh	$fp,24($18)

$L742:
lw	$2,10764($21)
sltu	$3,$2,7
beq	$3,$0,$L1202
li	$5,16			# 0x10

lw	$3,11088($21)
andi	$3,$3,0x8000
bne	$3,$0,$L763
lw	$3,%got(top.7827)($28)

addiu	$3,$3,%lo(top.7827)
addu	$2,$2,$3
lb	$2,0($2)
bgez	$2,$L763
lw	$6,%got($LC6)($28)

lw	$2,7996($21)
lw	$4,0($21)
lw	$7,7992($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC6)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L1237
li	$3,-1			# 0xffffffffffffffff

$L763:
lw	$3,11096($21)
andi	$3,$3,0x8000
bne	$3,$0,$L764
lw	$3,%got(left.7828)($28)

addiu	$3,$3,%lo(left.7828)
addu	$2,$2,$3
lb	$2,0($2)
bgez	$2,$L764
li	$5,16			# 0x10

lw	$2,7996($21)
lw	$6,%got($LC7)($28)
lw	$4,0($21)
lw	$7,7992($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC7)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

li	$3,-1			# 0xffffffffffffffff
$L1237:
li	$2,-1			# 0xffffffffffffffff
b	$L1031
sw	$3,10764($21)

$L764:
b	$L948
sw	$2,10764($21)

$L1176:
b	$L712
addiu	$2,$2,3

$L1173:
beq	$2,$0,$L831
lw	$fp,%got(pred_motion_hw)($28)

li	$16,131072			# 0x20000
addiu	$22,$sp,40
lw	$17,%got(decode_cabac_mb_mvd_hw)($28)
addiu	$16,$16,8816
li	$2,65536			# 0x10000
move	$8,$22
addiu	$18,$21,11296
addu	$16,$21,$16
move	$19,$0
addiu	$23,$sp,56
addu	$9,$21,$2
move	$5,$3
b	$L841
move	$22,$20

$L839:
move	$3,$0
sw	$2,0($18)
sw	$3,4($18)
sw	$2,8($18)
sw	$3,12($18)
sw	$2,32($18)
sw	$3,36($18)
sw	$2,40($18)
sw	$3,44($18)
sw	$2,64($18)
sw	$3,68($18)
sw	$2,72($18)
sw	$3,76($18)
sw	$2,96($18)
sw	$3,100($18)
sw	$2,104($18)
sw	$3,108($18)
sw	$2,0($16)
sw	$3,4($16)
sw	$2,8($16)
sw	$3,12($16)
sw	$2,32($16)
sw	$3,36($16)
sw	$2,40($16)
sw	$3,44($16)
sw	$2,64($16)
sw	$3,68($16)
sw	$2,72($16)
sw	$3,76($16)
sw	$2,96($16)
sw	$3,100($16)
sw	$2,104($16)
sw	$3,108($16)
$L840:
lw	$2,6632($9)
addiu	$19,$19,1
addiu	$22,$22,40
addiu	$16,$16,160
sltu	$2,$19,$2
beq	$2,$0,$L1203
addiu	$18,$18,160

$L841:
sll	$4,$19,1
li	$2,4096			# 0x1000
sll	$2,$2,$4
and	$2,$2,$5
beq	$2,$0,$L839
move	$2,$0

lb	$2,0($22)
move	$5,$0
li	$6,4			# 0x4
sw	$8,24($sp)
addiu	$25,$fp,%lo(pred_motion_hw)
sw	$8,148($sp)
sw	$9,156($sp)
move	$4,$21
move	$7,$19
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,pred_motion_hw
1:	jalr	$25
sw	$23,20($sp)

addiu	$25,$17,%lo(decode_cabac_mb_mvd_hw)
move	$6,$0
move	$7,$0
move	$4,$21
sw	$25,152($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_hw
1:	jalr	$25
move	$5,$19

move	$6,$0
lw	$3,56($sp)
li	$7,1			# 0x1
lw	$25,152($sp)
move	$4,$21
move	$5,$19
jalr	$25
addu	$20,$2,$3

lw	$3,40($sp)
sll	$4,$2,16
andi	$10,$20,0xffff
lw	$28,32($sp)
lw	$5,60($sp)
addu	$3,$2,$3
lw	$2,56($sp)
sll	$3,$3,16
subu	$2,$20,$2
addu	$10,$10,$3
andi	$2,$2,0xffff
addu	$4,$2,$4
li	$2,1			# 0x1
multu	$4,$2
mflo	$6
mfhi	$7
multu	$10,$2
mfhi	$3
mflo	$2
sw	$6,0($16)
addu	$7,$4,$7
sw	$6,8($16)
sw	$6,32($16)
sw	$6,40($16)
sw	$7,4($16)
sw	$7,12($16)
addu	$3,$10,$3
sw	$7,36($16)
sw	$7,44($16)
sw	$6,64($16)
sw	$7,68($16)
sw	$6,72($16)
sw	$7,76($16)
sw	$6,96($16)
sw	$7,100($16)
sw	$6,104($16)
sw	$7,108($16)
sw	$2,0($18)
sw	$3,4($18)
sw	$2,8($18)
sw	$3,12($18)
sw	$2,32($18)
sw	$3,36($18)
sw	$2,40($18)
sw	$3,44($18)
sw	$2,64($18)
sw	$3,68($18)
sw	$2,72($18)
sw	$3,76($18)
sw	$2,96($18)
sw	$3,100($18)
sw	$2,104($18)
sw	$3,108($18)
lw	$8,148($sp)
b	$L840
lw	$9,156($sp)

$L1171:
li	$2,65536			# 0x10000
addu	$2,$21,$2
lw	$3,-5264($2)
li	$2,3			# 0x3
beq	$3,$2,$L1204
li	$2,131072			# 0x20000

lw	$16,%got(get_cabac)($28)
addiu	$3,$sp,40
addiu	$18,$2,8224
addiu	$23,$2,8293
addiu	$20,$2,8294
sw	$3,136($sp)
addiu	$2,$2,8295
li	$17,60296			# 0xeb88
li	$22,60304			# 0xeb90
addu	$18,$21,$18
addu	$23,$21,$23
addu	$17,$21,$17
addu	$22,$21,$22
addiu	$16,$16,%lo(get_cabac)
addu	$20,$21,$20
addu	$fp,$21,$2
move	$19,$3
$L785:
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$23

move	$3,$0
beq	$2,$0,$L1205
lw	$28,32($sp)

$L784:
sll	$2,$3,2
lw	$3,%got(p_sub_mb_type_info)($28)
addiu	$17,$17,2
addiu	$19,$19,4
addiu	$3,$3,%lo(p_sub_mb_type_info)
addu	$2,$3,$2
lhu	$3,0($2)
lbu	$2,2($2)
sh	$3,-2($17)
bne	$17,$22,$L785
sw	$2,-4($19)

li	$3,65536			# 0x10000
$L1239:
addu	$fp,$21,$3
lw	$2,6632($fp)
beq	$2,$0,$L1206
addiu	$3,$3,6624

lw	$23,%got(scan8)($28)
lw	$7,%got(decode_cabac_mb_ref_hw)($28)
move	$20,$0
addu	$22,$21,$3
li	$3,60296			# 0xeb88
addiu	$23,$23,%lo(scan8)
addu	$3,$21,$3
move	$9,$fp
move	$fp,$21
$L791:
sll	$2,$20,3
sll	$18,$20,5
sll	$4,$20,1
addu	$18,$2,$18
li	$19,4096			# 0x1000
move	$16,$0
sll	$19,$19,$4
addu	$18,$fp,$18
li	$21,16			# 0x10
move	$17,$3
$L790:
lhu	$2,0($17)
andi	$6,$2,0x100
bne	$6,$0,$L786
and	$2,$2,$19

beq	$2,$0,$L789
li	$8,-1			# 0xffffffffffffffff

lw	$2,0($22)
addiu	$25,$7,%lo(decode_cabac_mb_ref_hw)
move	$4,$fp
move	$5,$20
sltu	$2,$2,2
bne	$2,$0,$L788
move	$6,$16

sw	$3,160($sp)
sw	$7,148($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref_hw
1:	jalr	$25
sw	$9,156($sp)

sll	$8,$2,24
lw	$28,32($sp)
lw	$3,160($sp)
lw	$7,148($sp)
sra	$8,$8,24
lw	$9,156($sp)
$L789:
addu	$2,$23,$16
lbu	$6,0($2)
addu	$6,$18,$6
sb	$8,11577($6)
sb	$8,11576($6)
sb	$8,11569($6)
$L786:
addiu	$16,$16,4
bne	$16,$21,$L790
addiu	$17,$17,2

lw	$2,6632($9)
addiu	$20,$20,1
sltu	$4,$20,$2
bne	$4,$0,$L791
addiu	$22,$22,4

lw	$8,108($sp)
beq	$8,$0,$L792
move	$21,$fp

li	$4,65536			# 0x10000
$L1258:
lw	$3,11956($21)
addu	$4,$21,$4
bne	$3,$0,$L1207
lw	$5,-5240($4)

lw	$3,-5236($4)
li	$4,24117248			# 0x1700000
addiu	$4,$4,368
or	$3,$3,$5
and	$3,$3,$4
sltu	$3,$3,1
sw	$3,108($sp)
$L792:
beq	$2,$0,$L795
lw	$2,%got(scan8)($28)

addiu	$9,$21,11288
addiu	$25,$21,11579
move	$23,$0
addiu	$2,$2,%lo(scan8)
sw	$9,84($sp)
sw	$25,120($sp)
move	$18,$23
move	$19,$21
sw	$2,76($sp)
li	$2,60296			# 0xeb88
addu	$2,$21,$2
sw	$2,140($sp)
li	$2,65536			# 0x10000
addu	$2,$21,$2
sw	$2,132($sp)
$L830:
sll	$3,$18,1
lw	$8,140($sp)
li	$2,12288			# 0x3000
sw	$18,68($sp)
li	$4,4096			# 0x1000
sll	$2,$2,$3
sll	$12,$18,3
sw	$8,64($sp)
sll	$20,$18,5
sll	$3,$4,$3
ori	$2,$2,0x40
sw	$12,88($sp)
move	$9,$0
sw	$20,92($sp)
sw	$3,112($sp)
move	$17,$12
sw	$2,124($sp)
move	$fp,$9
$L829:
lw	$9,76($sp)
addu	$2,$17,$20
lw	$21,64($sp)
addu	$3,$9,$fp
lbu	$4,0($3)
addu	$3,$19,$2
addu	$3,$3,$4
lb	$5,11569($3)
sb	$5,11568($3)
lhu	$3,0($21)
andi	$5,$3,0x100
beq	$5,$0,$L796
lw	$8,112($sp)

addu	$2,$2,$4
li	$3,34960			# 0x8890
move	$4,$0
addu	$2,$2,$3
move	$5,$0
sll	$2,$2,2
addu	$2,$19,$2
sw	$4,0($2)
sw	$5,4($2)
sw	$4,32($2)
sw	$5,36($2)
$L797:
lw	$8,64($sp)
addiu	$fp,$fp,4
li	$2,16			# 0x10
addiu	$8,$8,2
bne	$fp,$2,$L829
sw	$8,64($sp)

lw	$9,132($sp)
lw	$21,84($sp)
lw	$25,120($sp)
lw	$18,68($sp)
lw	$2,6632($9)
addiu	$21,$21,160
addiu	$25,$25,40
addiu	$18,$18,1
sw	$21,84($sp)
sltu	$2,$18,$2
bne	$2,$0,$L830
sw	$25,120($sp)

move	$21,$19
$L795:
b	$L831
lw	$3,60($sp)

$L1195:
lw	$2,12828($21)
beq	$2,$0,$L1208
nop

andi	$15,$15,0x40
beq	$15,$0,$L1208
li	$2,131072			# 0x20000

lhu	$3,-5238($4)
addu	$2,$21,$2
srl	$3,$3,8
lw	$2,9088($2)
andi	$3,$3,0x1
addu	$20,$2,$20
sb	$3,1($20)
lhu	$2,-5236($4)
lw	$3,11868($21)
srl	$2,$2,8
addu	$3,$20,$3
andi	$2,$2,0x1
sb	$2,0($3)
lhu	$2,-5234($4)
lw	$3,11868($21)
srl	$2,$2,8
addu	$20,$20,$3
andi	$2,$2,0x1
sb	$2,1($20)
b	$L1208
lw	$3,60($sp)

$L924:
sw	$0,11196($21)
sw	$0,11204($21)
sw	$0,11212($21)
b	$L926
sw	$0,11220($21)

$L1198:
move	$4,$20
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$17

li	$3,2			# 0x2
li	$4,3			# 0x3
lw	$28,32($sp)
movz	$4,$3,$2
move	$2,$4
b	$L767
move	$3,$4

$L715:
addiu	$5,$19,8289
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

lw	$28,32($sp)
b	$L712
subu	$2,$18,$2

$L960:
b	$L719
li	$2,11			# 0xb

$L698:
lw	$25,%got(decode_cabac_field_decoding_flag_hw)($28)
addiu	$25,$25,%lo(decode_cabac_field_decoding_flag_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_field_decoding_flag_hw
1:	jalr	$25
nop

lw	$28,32($sp)
sw	$2,-5248($17)
b	$L696
sw	$2,-5244($17)

$L842:
addu	$16,$21,$2
lw	$4,6632($16)
beq	$4,$0,$L831
addiu	$2,$2,6624

lw	$17,%got(decode_cabac_mb_ref_hw)($28)
addiu	$18,$21,11596
addu	$19,$21,$2
li	$fp,1			# 0x1
move	$20,$0
li	$23,4096			# 0x1000
b	$L854
li	$22,-1			# 0xffffffffffffffff

$L1210:
sw	$22,-16($18)
and	$2,$3,$2
bne	$2,$0,$L850
sw	$22,-8($18)

$L1211:
sw	$22,0($18)
sw	$22,8($18)
$L851:
lw	$2,6632($16)
addiu	$20,$20,1
addiu	$18,$18,40
addiu	$19,$19,4
sltu	$4,$20,$2
beq	$4,$0,$L1209
addiu	$fp,$fp,2

$L854:
sll	$2,$20,1
sll	$2,$23,$2
and	$2,$2,$3
beq	$2,$0,$L1210
sll	$2,$23,$fp

lw	$2,0($19)
sltu	$2,$2,2
beq	$2,$0,$L848
move	$6,$0

move	$2,$0
sw	$2,-16($18)
sw	$2,-8($18)
$L1221:
sll	$2,$23,$fp
and	$2,$3,$2
beq	$2,$0,$L1211
nop

$L850:
lw	$2,0($19)
sltu	$2,$2,2
beq	$2,$0,$L852
li	$6,8			# 0x8

move	$2,$0
sw	$2,0($18)
b	$L851
sw	$2,8($18)

$L1204:
lw	$16,%got(get_cabac)($28)
li	$19,60296			# 0xeb88
lw	$fp,%got(b_sub_mb_type_info)($28)
addiu	$23,$2,8308
addiu	$4,$2,8309
addiu	$3,$2,8310
addiu	$8,$sp,40
addu	$23,$21,$23
addu	$4,$21,$4
addu	$3,$21,$3
sw	$8,136($sp)
addiu	$18,$2,8224
sw	$23,64($sp)
addiu	$2,$2,8311
sw	$4,68($sp)
addu	$19,$21,$19
sw	$3,72($sp)
li	$20,60304			# 0xeb90
addu	$18,$21,$18
addu	$20,$21,$20
addiu	$16,$16,%lo(get_cabac)
addiu	$fp,$fp,%lo(b_sub_mb_type_info)
addu	$17,$21,$2
move	$22,$8
b	$L776
move	$23,$19

$L772:
sll	$2,$2,2
addiu	$23,$23,2
addu	$2,$fp,$2
addiu	$22,$22,4
lhu	$3,0($2)
lbu	$2,2($2)
sh	$3,-2($23)
beq	$23,$20,$L1212
sw	$2,-4($22)

$L776:
lw	$5,64($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$18

beq	$2,$0,$L772
lw	$28,32($sp)

lw	$5,68($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$4,$18

move	$4,$18
beq	$2,$0,$L1213
lw	$5,72($sp)

move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
nop

move	$4,$18
beq	$2,$0,$L774
move	$5,$17

move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
nop

bne	$2,$0,$L1214
li	$3,7			# 0x7

$L775:
move	$4,$18
sw	$3,160($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$17

move	$4,$18
lw	$3,160($sp)
sll	$2,$2,1
move	$5,$17
move	$25,$16
addu	$3,$2,$3
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
sw	$3,160($sp)

lw	$3,160($sp)
lw	$28,32($sp)
addu	$2,$3,$2
b	$L772
andi	$2,$2,0xffff

$L796:
and	$5,$3,$8
beq	$5,$0,$L798
addu	$2,$2,$4

lw	$9,136($sp)
li	$4,1			# 0x1
li	$5,2			# 0x2
andi	$6,$3,0x18
addu	$12,$9,$fp
movn	$4,$5,$6
lw	$2,0($12)
blez	$2,$L797
sw	$4,72($sp)

andi	$4,$3,0x8
lw	$11,132($sp)
andi	$2,$3,0x10
sw	$fp,144($sp)
andi	$3,$3,0x20
move	$9,$0
andi	$4,$4,0xffff
andi	$2,$2,0xffff
andi	$3,$3,0xffff
sw	$4,100($sp)
move	$18,$17
sw	$2,116($sp)
sw	$3,128($sp)
move	$22,$9
b	$L827
move	$17,$fp

$L1217:
lw	$14,2696($19)
lw	$3,84($sp)
lw	$8,104($14)
sw	$0,0($3)
lw	$24,-5248($11)
bne	$24,$0,$L802
li	$15,-2			# 0xfffffffffffffffe

lw	$15,7996($19)
andi	$25,$15,0x1
beq	$25,$0,$L803
li	$25,-2			# 0xfffffffffffffffe

slt	$25,$23,20
beq	$25,$0,$L804
li	$25,-2			# 0xfffffffffffffffe

beq	$7,$25,$L995
xori	$25,$23,0xf

lw	$9,168($19)
addiu	$24,$15,-1
lw	$31,7992($19)
sltu	$25,$25,1
mul	$21,$24,$9
addu	$24,$21,$31
addu	$24,$24,$25
sll	$24,$24,2
addu	$24,$8,$24
lw	$24,0($24)
andi	$24,$24,0x80
beq	$24,$0,$L806
sll	$15,$15,2

lw	$25,72($sp)
sll	$7,$31,2
addiu	$24,$15,-1
andi	$6,$23,0x7
addu	$6,$7,$6
sra	$7,$24,2
addiu	$6,$6,-4
mul	$21,$9,$7
addu	$15,$25,$6
lw	$25,124($sp)
sra	$6,$15,2
addu	$6,$21,$6
sll	$6,$6,2
addu	$6,$8,$6
lw	$6,0($6)
and	$6,$25,$6
beq	$6,$0,$L981
sra	$25,$15,1

lw	$7,11864($19)
lw	$8,68($sp)
lw	$31,11868($19)
sll	$6,$8,2
mul	$8,$24,$7
addu	$14,$14,$6
lw	$7,96($14)
addu	$6,$8,$15
sra	$15,$24,1
sll	$6,$6,2
addu	$6,$7,$6
lh	$7,0($6)
sh	$7,0($3)
mul	$7,$15,$31
lhu	$6,2($6)
sll	$6,$6,1
sh	$6,2($3)
lw	$6,188($14)
addu	$6,$6,$25
addu	$6,$7,$6
lb	$7,0($6)
sra	$7,$7,1
$L807:
xor	$14,$5,$10
xor	$6,$5,$7
sltu	$14,$14,1
sltu	$6,$6,1
xor	$5,$5,$13
addu	$6,$6,$14
sltu	$5,$5,1
addu	$6,$6,$5
slt	$15,$6,2
bne	$15,$0,$L1248
li	$15,1			# 0x1

$L819:
lh	$5,0($4)
lh	$21,0($2)
slt	$7,$21,$5
beq	$7,$0,$L820
lh	$6,0($3)

slt	$7,$21,$6
beq	$7,$0,$L821
slt	$10,$5,$6

move	$21,$5
movz	$21,$6,$10
$L821:
lh	$13,2($2)
lh	$4,2($4)
lh	$2,2($3)
slt	$3,$13,$4
beq	$3,$0,$L822
slt	$3,$2,$13

slt	$3,$13,$2
beq	$3,$0,$L1249
lw	$25,%got(decode_cabac_mb_mvd_hw)($28)

slt	$13,$4,$2
movz	$4,$2,$13
move	$13,$4
$L815:
lw	$25,%got(decode_cabac_mb_mvd_hw)($28)
$L1249:
move	$7,$0
lw	$5,68($sp)
move	$4,$19
sw	$11,164($sp)
move	$6,$17
addiu	$25,$25,%lo(decode_cabac_mb_mvd_hw)
sw	$12,156($sp)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_hw
1:	jalr	$25
sw	$13,148($sp)

li	$7,1			# 0x1
lw	$28,32($sp)
addu	$3,$2,$21
lw	$5,68($sp)
move	$4,$19
move	$6,$17
lw	$25,%got(decode_cabac_mb_mvd_hw)($28)
addiu	$25,$25,%lo(decode_cabac_mb_mvd_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_hw
1:	jalr	$25
sw	$3,160($sp)

lw	$13,148($sp)
lw	$8,100($sp)
lw	$28,32($sp)
addu	$2,$2,$13
lw	$3,160($sp)
lw	$11,164($sp)
beq	$8,$0,$L823
lw	$12,156($sp)

subu	$4,$3,$21
subu	$6,$2,$13
sll	$3,$3,16
sll	$2,$2,16
sll	$4,$4,16
sll	$6,$6,16
sra	$3,$3,16
sra	$2,$2,16
sra	$4,$4,16
sra	$6,$6,16
sh	$3,36($fp)
sh	$3,32($fp)
sh	$3,4($fp)
sh	$2,38($fp)
sh	$2,34($fp)
sh	$2,6($fp)
sh	$4,36($16)
sh	$4,32($16)
sh	$4,4($16)
sh	$6,38($16)
sh	$6,34($16)
sh	$6,6($16)
$L824:
addu	$5,$18,$20
lw	$10,0($12)
li	$7,131072			# 0x20000
lw	$18,88($sp)
addu	$5,$23,$5
lw	$20,92($sp)
addiu	$22,$22,1
sll	$5,$5,2
slt	$10,$22,$10
addu	$5,$19,$5
sh	$3,11248($5)
addu	$7,$5,$7
sh	$2,11250($5)
sh	$4,8768($7)
sh	$6,8770($7)
lw	$3,72($sp)
beq	$10,$0,$L1216
addu	$17,$17,$3

$L827:
lw	$21,76($sp)
addu	$2,$18,$20
lw	$25,72($sp)
li	$16,34960			# 0x8890
lw	$14,-5252($11)
addu	$3,$21,$17
lbu	$23,0($3)
addu	$3,$19,$2
addiu	$10,$23,-8
addiu	$7,$23,-1
addu	$5,$2,$23
addu	$4,$2,$7
addu	$2,$2,$10
addu	$6,$25,$10
addiu	$fp,$5,2812
addu	$16,$5,$16
addiu	$4,$4,2812
addiu	$2,$2,2812
addu	$7,$3,$7
addu	$5,$3,$23
addu	$10,$3,$10
addu	$3,$3,$6
lb	$13,11568($7)
sll	$fp,$fp,2
lb	$5,11568($5)
sll	$16,$16,2
lb	$10,11568($10)
sll	$4,$4,2
lb	$7,11568($3)
sll	$2,$2,2
addu	$fp,$19,$fp
addu	$16,$19,$16
addu	$4,$19,$4
bne	$14,$0,$L1217
addu	$2,$19,$2

$L801:
li	$3,-2			# 0xfffffffffffffffe
bne	$7,$3,$L806
nop

$L809:
addiu	$6,$23,-9
$L1251:
addu	$14,$18,$20
move	$7,$6
addu	$3,$14,$6
addu	$6,$19,$14
xor	$14,$5,$10
addu	$6,$6,$7
sltu	$14,$14,1
addiu	$3,$3,2812
lb	$7,11568($6)
sll	$3,$3,2
xor	$6,$5,$7
sltu	$6,$6,1
xor	$5,$5,$13
addu	$6,$6,$14
sltu	$5,$5,1
addu	$6,$6,$5
slt	$15,$6,2
beq	$15,$0,$L819
addu	$3,$19,$3

li	$15,1			# 0x1
$L1248:
beq	$6,$15,$L1218
nop

li	$5,-2			# 0xfffffffffffffffe
bne	$10,$5,$L819
nop

bne	$7,$10,$L819
nop

beq	$13,$7,$L819
nop

$L1142:
lh	$21,0($4)
b	$L815
lh	$13,2($4)

$L803:
beq	$7,$25,$L1250
slt	$25,$23,20

$L806:
addu	$3,$18,$20
addu	$3,$3,$6
addiu	$3,$3,2812
sll	$3,$3,2
b	$L807
addu	$3,$19,$3

$L823:
lw	$9,116($sp)
beq	$9,$0,$L825
lw	$25,128($sp)

subu	$4,$3,$21
subu	$6,$2,$13
sll	$3,$3,16
sll	$2,$2,16
sll	$4,$4,16
sll	$6,$6,16
sra	$3,$3,16
sra	$2,$2,16
sra	$4,$4,16
sra	$6,$6,16
sh	$3,4($fp)
sh	$2,6($fp)
sh	$4,4($16)
b	$L824
sh	$6,6($16)

$L822:
beq	$3,$0,$L1249
lw	$25,%got(decode_cabac_mb_mvd_hw)($28)

slt	$13,$2,$4
movz	$4,$2,$13
b	$L1249
move	$13,$4

$L820:
slt	$7,$6,$21
beq	$7,$0,$L821
slt	$10,$6,$5

move	$21,$5
b	$L821
movz	$21,$6,$10

$L802:
bne	$7,$15,$L806
nop

lw	$15,7996($19)
andi	$25,$15,0x1
bne	$25,$0,$L995
slt	$25,$23,20

$L1250:
bne	$25,$0,$L809
move	$9,$0

$L805:
li	$25,4			# 0x4
andi	$31,$23,0x7
bne	$31,$25,$L809
lw	$21,120($sp)

li	$31,-2			# 0xfffffffffffffffe
lb	$25,0($21)
beq	$25,$31,$L809
nop

beq	$24,$0,$L1219
nop

lw	$6,10780($19)
sll	$6,$6,2
addu	$6,$8,$6
lw	$6,0($6)
andi	$6,$6,0x80
bne	$6,$0,$L1251
addiu	$6,$23,-9

slt	$6,$23,20
bne	$6,$0,$L1251
addiu	$6,$23,-9

addiu	$6,$23,-12
lw	$24,7992($19)
and	$15,$15,$31
lw	$25,168($19)
sll	$7,$15,2
lw	$21,124($sp)
sra	$6,$6,3
addiu	$7,$7,-1
sll	$6,$6,1
sll	$15,$24,2
addu	$24,$7,$6
addiu	$15,$15,-1
sra	$6,$24,2
sra	$7,$15,2
mul	$9,$6,$25
addu	$6,$9,$7
sll	$6,$6,2
addu	$6,$8,$6
lw	$6,0($6)
and	$6,$21,$6
beq	$6,$0,$L981
lw	$25,68($sp)

lw	$7,11864($19)
lw	$8,84($sp)
sll	$6,$25,2
lw	$31,11868($19)
sra	$25,$15,1
addu	$14,$14,$6
mul	$6,$24,$7
sra	$24,$24,1
addu	$15,$6,$15
lw	$6,96($14)
sll	$15,$15,2
addu	$6,$6,$15
lh	$7,0($6)
sh	$7,0($8)
mul	$7,$24,$31
lh	$6,2($6)
sra	$6,$6,1
sh	$6,2($8)
lw	$6,188($14)
addu	$6,$6,$25
addu	$6,$7,$6
lb	$7,0($6)
b	$L807
sll	$7,$7,1

$L798:
li	$4,34960			# 0x8890
addiu	$3,$2,2812
addu	$2,$2,$4
sll	$3,$3,2
sll	$2,$2,2
addu	$3,$19,$3
addu	$2,$19,$2
sw	$0,36($3)
sw	$0,32($3)
sw	$0,4($3)
sw	$0,0($3)
sw	$0,36($2)
sw	$0,32($2)
sw	$0,4($2)
b	$L797
sw	$0,0($2)

$L825:
subu	$4,$3,$21
subu	$6,$2,$13
sll	$3,$3,16
sll	$2,$2,16
sll	$4,$4,16
sll	$6,$6,16
sra	$3,$3,16
sra	$2,$2,16
sra	$4,$4,16
beq	$25,$0,$L824
sra	$6,$6,16

sh	$3,32($fp)
sh	$2,34($fp)
sh	$4,32($16)
b	$L824
sh	$6,34($16)

$L788:
b	$L789
move	$8,$0

$L1216:
lw	$fp,144($sp)
b	$L797
move	$17,$18

$L804:
bne	$7,$25,$L806
nop

$L995:
b	$L805
li	$9,1			# 0x1

$L1218:
bne	$5,$0,$L1142
nop

bne	$14,$0,$L1220
nop

lh	$21,0($3)
b	$L815
lh	$13,2($3)

$L852:
addiu	$25,$17,%lo(decode_cabac_mb_ref_hw)
move	$4,$21
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref_hw
1:	jalr	$25
move	$5,$20

sll	$5,$2,8
lw	$28,32($sp)
lw	$3,60($sp)
addu	$2,$5,$2
sll	$4,$2,16
addu	$2,$2,$4
sw	$2,0($18)
b	$L851
sw	$2,8($18)

$L952:
addiu	$25,$17,%lo(decode_cabac_mb_ref_hw)
move	$5,$20
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref_hw
1:	jalr	$25
move	$4,$21

sll	$5,$2,8
lw	$28,32($sp)
lw	$3,60($sp)
addu	$2,$5,$2
b	$L953
andi	$2,$2,0xffff

$L984:
b	$L863
move	$2,$0

$L848:
addiu	$25,$17,%lo(decode_cabac_mb_ref_hw)
move	$4,$21
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_ref_hw
1:	jalr	$25
move	$5,$20

sll	$5,$2,8
lw	$28,32($sp)
lw	$3,60($sp)
addu	$2,$5,$2
sll	$4,$2,16
addu	$2,$2,$4
sw	$2,-16($18)
b	$L1221
sw	$2,-8($18)

$L1205:
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$20

li	$3,1			# 0x1
beq	$2,$0,$L784
lw	$28,32($sp)

move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$fp

li	$4,3			# 0x3
li	$3,2			# 0x2
lw	$28,32($sp)
b	$L784
movz	$3,$4,$2

$L1220:
lh	$21,0($2)
b	$L815
lh	$13,2($2)

$L1203:
b	$L831
move	$3,$5

$L743:
lw	$5,11656($21)
lw	$25,%got(get_cabac_noinline)($28)
addiu	$4,$2,8224
addu	$2,$2,$5
addu	$4,$21,$4
addiu	$5,$2,8671
addiu	$25,$25,%lo(get_cabac_noinline)
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

beq	$2,$0,$L745
lw	$28,32($sp)

lw	$3,60($sp)
li	$4,16777216			# 0x1000000
lw	$2,%got(scan8)($28)
li	$18,2			# 0x2
lw	$16,%got(decode_cabac_mb_intra4x4_pred_mode_hw)($28)
lw	$19,%got(scan8+16)($28)
or	$3,$3,$4
addiu	$20,$2,%lo(scan8)
addiu	$16,$16,%lo(decode_cabac_mb_intra4x4_pred_mode_hw)
sw	$3,60($sp)
addiu	$19,$19,%lo(scan8+16)
$L747:
lbu	$17,0($20)
move	$4,$21
move	$25,$16
addiu	$20,$20,4
addu	$2,$21,$17
lb	$5,10808($2)
lb	$2,10815($2)
slt	$3,$2,$5
movz	$2,$5,$3
slt	$5,$2,0
movn	$2,$18,$5
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_intra4x4_pred_mode_hw
1:	jalr	$25
move	$5,$2

addiu	$3,$17,10816
sll	$5,$2,8
lw	$28,32($sp)
addu	$3,$21,$3
addu	$2,$5,$2
andi	$2,$2,0xffff
sh	$2,0($3)
bne	$20,$19,$L747
sh	$2,8($3)

b	$L1238
lw	$4,7996($21)

$L1172:
lw	$25,%call16(pred_direct_motion_hw)($28)
addiu	$5,$sp,60
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$21

li	$2,131072			# 0x20000
move	$6,$0
lw	$4,11956($21)
move	$7,$0
lw	$28,32($sp)
addu	$2,$21,$2
lw	$3,60($sp)
sw	$6,8816($2)
sw	$7,8820($2)
sw	$6,8824($2)
sw	$7,8828($2)
sw	$6,8848($2)
sw	$7,8852($2)
sw	$6,8856($2)
sw	$7,8860($2)
sw	$6,8880($2)
sw	$7,8884($2)
sw	$6,8888($2)
sw	$7,8892($2)
sw	$6,8912($2)
sw	$7,8916($2)
sw	$6,8920($2)
sw	$7,8924($2)
sw	$6,8976($2)
sw	$7,8980($2)
sw	$6,8984($2)
sw	$7,8988($2)
sw	$6,9008($2)
sw	$7,9012($2)
sw	$6,9016($2)
sw	$7,9020($2)
sw	$6,9040($2)
sw	$7,9044($2)
sw	$6,9048($2)
sw	$7,9052($2)
sw	$6,9072($2)
sw	$7,9076($2)
sw	$6,9080($2)
lw	$8,108($sp)
sw	$7,9084($2)
and	$8,$8,$4
b	$L831
sw	$8,108($sp)

$L714:
lw	$25,%got(decode_cabac_intra_mb_type_hw)($28)
li	$5,17			# 0x11
move	$6,$0
addiu	$25,$25,%lo(decode_cabac_intra_mb_type_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_intra_mb_type_hw
1:	jalr	$25
move	$4,$21

lw	$28,32($sp)
b	$L712
addiu	$2,$2,5

$L1190:
addiu	$2,$2,52
b	$L922
sw	$2,2872($21)

$L1197:
li	$2,131072			# 0x20000
lw	$17,%got(get_cabac_noinline)($28)
li	$20,2			# 0x2
addu	$3,$21,$2
move	$7,$0
addiu	$16,$2,8224
lw	$19,8740($3)
addiu	$17,$17,%lo(get_cabac_noinline)
addu	$16,$21,$16
move	$25,$17
andi	$3,$19,0x4
movz	$7,$20,$3
move	$4,$16
andi	$19,$19,0x8
addu	$3,$7,$5
addu	$2,$2,$3
addiu	$5,$2,8345
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

move	$5,$0
andi	$3,$2,0x1
movz	$5,$20,$19
move	$22,$2
b	$L897
xori	$3,$3,0x1

$L1219:
lw	$24,10780($19)
sll	$24,$24,2
addu	$24,$8,$24
lw	$24,0($24)
andi	$24,$24,0x80
beq	$24,$0,$L801
sra	$24,$23,4

ori	$6,$15,0x1
lw	$25,168($19)
sll	$6,$6,1
lw	$7,7992($19)
lw	$21,124($sp)
addu	$6,$9,$6
sll	$7,$7,2
sll	$6,$6,1
addiu	$15,$7,-1
addu	$6,$6,$24
sra	$7,$15,2
addiu	$24,$6,-1
sra	$6,$24,2
mul	$9,$6,$25
addu	$6,$9,$7
sll	$6,$6,2
addu	$6,$8,$6
lw	$6,0($6)
and	$6,$21,$6
beq	$6,$0,$L981
lw	$25,68($sp)

lw	$7,11864($19)
lw	$8,84($sp)
sll	$6,$25,2
lw	$31,11868($19)
sra	$25,$15,1
addu	$14,$14,$6
mul	$6,$24,$7
sra	$24,$24,1
addu	$7,$6,$15
lw	$15,96($14)
sll	$6,$7,2
addu	$6,$15,$6
lh	$7,0($6)
sh	$7,0($8)
mul	$7,$24,$31
lhu	$6,2($6)
sll	$6,$6,1
sh	$6,2($8)
lw	$6,188($14)
addu	$6,$6,$25
addu	$6,$7,$6
lb	$7,0($6)
b	$L807
sra	$7,$7,1

$L1196:
addu	$2,$21,$2
lw	$2,8744($2)
andi	$5,$2,0x2
andi	$18,$2,0x8
sltu	$5,$5,1
b	$L894
sltu	$18,$18,1

$L1188:
lw	$5,11656($21)
addiu	$18,$2,8224
addu	$18,$21,$18
addu	$2,$2,$5
addiu	$17,$17,%lo(get_cabac_noinline)
addiu	$5,$2,8671
move	$4,$18
move	$25,$17
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
addu	$5,$21,$5

beq	$2,$0,$L1222
lw	$28,32($sp)

lw	$3,60($sp)
li	$2,16777216			# 0x1000000
or	$3,$3,$2
b	$L907
sw	$3,60($sp)

$L1209:
beq	$2,$0,$L831
addiu	$9,$sp,56

li	$10,65536			# 0x10000
move	$fp,$0
sw	$9,68($sp)
addu	$10,$21,$10
move	$23,$21
move	$22,$21
b	$L862
move	$7,$3

$L1225:
lw	$2,6632($10)
addiu	$fp,$fp,1
move	$21,$22
move	$7,$5
sltu	$2,$fp,$2
addiu	$23,$23,160
beq	$2,$0,$L1223
addiu	$22,$3,40

$L862:
addiu	$3,$sp,40
sll	$11,$fp,1
move	$20,$0
move	$16,$22
sw	$3,64($sp)
move	$3,$22
move	$22,$21
move	$21,$11
li	$9,4096			# 0x1000
addu	$2,$21,$20
li	$25,131072			# 0x20000
sll	$2,$9,$2
move	$5,$7
ori	$25,$25,0x2270
and	$2,$2,$5
addu	$18,$23,$25
bne	$2,$0,$L1224
move	$17,$23

move	$6,$0
$L1252:
move	$7,$0
sw	$6,0($18)
sw	$7,4($18)
sw	$6,8($18)
sw	$7,12($18)
sw	$6,32($18)
sw	$7,36($18)
sw	$6,40($18)
sw	$7,44($18)
sw	$6,11296($17)
sw	$7,11300($17)
sw	$6,11304($17)
sw	$7,11308($17)
sw	$6,11328($17)
sw	$7,11332($17)
sw	$6,11336($17)
sw	$7,11340($17)
$L860:
addiu	$18,$18,64
addiu	$17,$17,64
bne	$20,$0,$L1225
addiu	$16,$16,16

li	$20,1			# 0x1
addu	$2,$21,$20
sll	$2,$9,$2
and	$2,$2,$5
beq	$2,$0,$L1252
move	$6,$0

$L1224:
sll	$19,$20,3
bne	$19,$0,$L857
lb	$2,11580($16)

lb	$4,11572($3)
beq	$2,$4,$L1226
lw	$4,%got(pred_motion_hw)($28)

$L1253:
addiu	$5,$sp,56
addiu	$7,$sp,40
sw	$3,160($sp)
li	$6,4			# 0x4
sw	$9,156($sp)
addiu	$25,$4,%lo(pred_motion_hw)
sw	$5,20($sp)
sw	$7,24($sp)
move	$4,$22
sw	$10,148($sp)
move	$5,$19
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,pred_motion_hw
1:	jalr	$25
move	$7,$fp

lw	$28,32($sp)
lw	$10,148($sp)
lw	$9,156($sp)
lw	$3,160($sp)
$L859:
lw	$8,%got(decode_cabac_mb_mvd_hw)($28)
move	$7,$0
move	$6,$19
sw	$3,160($sp)
sw	$9,156($sp)
move	$4,$22
addiu	$25,$8,%lo(decode_cabac_mb_mvd_hw)
sw	$10,148($sp)
move	$5,$fp
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_hw
1:	jalr	$25
sw	$25,152($sp)

move	$6,$19
lw	$19,56($sp)
li	$7,1			# 0x1
lw	$25,152($sp)
move	$4,$22
move	$5,$fp
jalr	$25
addu	$19,$2,$19

lw	$4,40($sp)
sll	$6,$2,16
andi	$7,$19,0xffff
lw	$28,32($sp)
lw	$5,60($sp)
addu	$4,$2,$4
lw	$2,56($sp)
lw	$3,160($sp)
sll	$4,$4,16
lw	$9,156($sp)
subu	$2,$19,$2
lw	$10,148($sp)
addu	$4,$7,$4
andi	$2,$2,0xffff
addu	$2,$2,$6
li	$6,1			# 0x1
multu	$2,$6
mflo	$12
mfhi	$13
multu	$4,$6
mfhi	$7
mflo	$6
sw	$12,0($18)
addu	$13,$2,$13
sw	$12,8($18)
sw	$12,32($18)
sw	$12,40($18)
sw	$13,4($18)
sw	$13,12($18)
addu	$7,$4,$7
sw	$13,36($18)
sw	$13,44($18)
sw	$6,11296($17)
sw	$7,11300($17)
sw	$6,11304($17)
sw	$7,11308($17)
sw	$6,11328($17)
sw	$7,11332($17)
sw	$6,11336($17)
b	$L860
sw	$7,11340($17)

$L857:
lb	$4,11595($3)
bne	$2,$4,$L1253
lw	$4,%got(pred_motion_hw)($28)

lh	$4,11356($23)
lh	$2,11358($23)
sw	$4,56($sp)
b	$L859
sw	$2,40($sp)

$L1223:
b	$L831
move	$3,$5

$L1200:
beq	$2,$0,$L831
addiu	$8,$sp,56

li	$2,65536			# 0x10000
addiu	$19,$21,11288
sw	$8,68($sp)
move	$22,$0
move	$fp,$0
addu	$12,$21,$2
move	$11,$21
move	$7,$3
b	$L878
move	$8,$21

$L1229:
lw	$2,6632($12)
addiu	$fp,$fp,1
move	$8,$18
addiu	$22,$22,2
sltu	$2,$fp,$2
addiu	$11,$11,40
beq	$2,$0,$L1227
addiu	$19,$19,160

$L878:
move	$23,$0
li	$18,12288			# 0x3000
addu	$3,$22,$23
li	$2,4096			# 0x1000
sll	$18,$18,$22
li	$9,65536			# 0x10000
sll	$2,$2,$3
ori	$18,$18,0x40
ori	$9,$9,0xf658
and	$2,$2,$7
sw	$18,64($sp)
addu	$17,$19,$9
addiu	$16,$19,-11288
move	$21,$11
bne	$2,$0,$L1228
move	$18,$8

move	$2,$0
$L1254:
move	$3,$0
sw	$2,0($17)
sw	$3,4($17)
sw	$2,32($17)
sw	$3,36($17)
sw	$2,64($17)
sw	$3,68($17)
sw	$2,96($17)
sw	$3,100($17)
sw	$2,11296($16)
sw	$3,11300($16)
sw	$2,11328($16)
sw	$3,11332($16)
sw	$2,11360($16)
sw	$3,11364($16)
sw	$2,11392($16)
sw	$3,11396($16)
$L876:
addiu	$17,$17,8
addiu	$16,$16,8
bne	$23,$0,$L1229
addiu	$21,$21,2

li	$23,1			# 0x1
li	$2,4096			# 0x1000
addu	$3,$22,$23
sll	$2,$2,$3
and	$2,$2,$7
beq	$2,$0,$L1254
move	$2,$0

$L1228:
sll	$20,$23,2
bne	$20,$0,$L868
lb	$6,11580($21)

lb	$2,11579($11)
beq	$6,$2,$L1230
nop

$L869:
lw	$7,%got(pred_motion_hw)($28)
addiu	$5,$sp,56
addiu	$2,$sp,40
sw	$6,16($sp)
li	$6,2			# 0x2
sw	$11,164($sp)
addiu	$25,$7,%lo(pred_motion_hw)
sw	$5,20($sp)
sw	$12,156($sp)
move	$4,$18
sw	$2,24($sp)
move	$5,$20
.reloc	1f,R_MIPS_JALR,pred_motion_hw
1:	jalr	$25
move	$7,$fp

lw	$28,32($sp)
lw	$12,156($sp)
lw	$11,164($sp)
$L870:
lw	$8,%got(decode_cabac_mb_mvd_hw)($28)
move	$7,$0
move	$6,$20
sw	$11,164($sp)
sw	$12,156($sp)
move	$4,$18
addiu	$25,$8,%lo(decode_cabac_mb_mvd_hw)
move	$5,$fp
.reloc	1f,R_MIPS_JALR,decode_cabac_mb_mvd_hw
1:	jalr	$25
sw	$25,152($sp)

move	$6,$20
lw	$20,56($sp)
li	$7,1			# 0x1
lw	$25,152($sp)
move	$4,$18
move	$5,$fp
jalr	$25
addu	$20,$2,$20

lw	$3,40($sp)
sll	$6,$2,16
andi	$9,$20,0xffff
lw	$28,32($sp)
lw	$7,60($sp)
addu	$3,$2,$3
lw	$2,56($sp)
lw	$11,164($sp)
sll	$3,$3,16
lw	$12,156($sp)
subu	$2,$20,$2
addu	$9,$9,$3
andi	$2,$2,0xffff
addu	$6,$2,$6
li	$2,1			# 0x1
multu	$6,$2
mflo	$4
mfhi	$5
multu	$9,$2
mfhi	$3
mflo	$2
sw	$4,0($17)
addu	$5,$6,$5
sw	$4,32($17)
sw	$4,64($17)
sw	$4,96($17)
sw	$5,4($17)
sw	$5,36($17)
addu	$3,$9,$3
sw	$5,68($17)
sw	$5,100($17)
sw	$2,11296($16)
sw	$3,11300($16)
sw	$2,11328($16)
sw	$3,11332($16)
sw	$2,11360($16)
sw	$3,11364($16)
sw	$2,11392($16)
b	$L876
sw	$3,11396($16)

$L868:
lw	$3,-5252($12)
beq	$3,$0,$L871
lb	$2,11576($11)

lw	$9,2696($18)
lw	$10,104($9)
sw	$0,0($19)
lw	$3,-5248($12)
bne	$3,$0,$L871
move	$5,$19

lw	$7,7996($18)
andi	$3,$7,0x1
beq	$3,$0,$L1255
li	$3,-2			# 0xfffffffffffffffe

beq	$2,$3,$L872
addiu	$3,$7,-1

lw	$13,168($18)
lw	$4,7992($18)
mul	$8,$3,$13
addu	$3,$8,$4
sll	$3,$3,2
addu	$3,$10,$3
lw	$3,0($3)
andi	$3,$3,0x80
beq	$3,$0,$L875
lw	$25,64($sp)

sll	$2,$7,2
sll	$4,$4,2
addiu	$2,$2,-1
addiu	$7,$4,4
sra	$4,$2,2
sra	$3,$7,2
mul	$8,$13,$4
addu	$4,$8,$3
sll	$4,$4,2
addu	$10,$10,$4
lw	$3,0($10)
and	$3,$25,$3
beq	$3,$0,$L985
sll	$3,$fp,2

lw	$4,11864($18)
sra	$13,$7,1
lw	$10,11868($18)
addu	$3,$9,$3
mul	$8,$2,$4
sra	$2,$2,1
addu	$4,$8,$7
lw	$7,96($3)
sll	$4,$4,2
addu	$4,$7,$4
lh	$7,0($4)
sh	$7,0($19)
lhu	$4,2($4)
sll	$4,$4,1
sh	$4,2($19)
mul	$4,$2,$10
lw	$3,188($3)
addu	$3,$3,$13
addu	$2,$4,$3
lb	$2,0($2)
sra	$2,$2,1
$L874:
bne	$6,$2,$L869
nop

lh	$3,0($5)
lh	$2,2($5)
sw	$3,56($sp)
b	$L870
sw	$2,40($sp)

$L871:
li	$3,-2			# 0xfffffffffffffffe
$L1255:
beq	$2,$3,$L872
nop

$L875:
b	$L874
addiu	$5,$19,-8

$L872:
addiu	$5,$19,-20
b	$L874
lb	$2,11573($11)

$L1230:
lh	$3,4($19)
lh	$2,6($19)
sw	$3,56($sp)
b	$L870
sw	$2,40($sp)

$L1227:
move	$3,$7
b	$L831
move	$21,$18

$L1222:
b	$L907
lw	$3,60($sp)

$L1179:
lw	$2,7996($21)
li	$5,16			# 0x10
lw	$3,7992($21)
lw	$4,0($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC3)
sw	$2,20($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)

b	$L1031
li	$2,-1			# 0xffffffffffffffff

$L1226:
lh	$4,11264($23)
lh	$2,11266($23)
sw	$4,56($sp)
b	$L859
sw	$2,40($sp)

$L1178:
sll	$19,$2,1
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac_noinline
1:	jalr	$25
move	$5,$17

or	$2,$2,$19
lw	$28,32($sp)
b	$L712
addiu	$2,$2,-4

$L774:
b	$L775
li	$3,3			# 0x3

$L1213:
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$17

addiu	$2,$2,1
lw	$28,32($sp)
b	$L772
andi	$2,$2,0xffff

$L1212:
li	$16,65536			# 0x10000
addu	$16,$21,$16
lhu	$3,-5238($16)
lhu	$5,-5240($16)
lhu	$2,-5236($16)
lhu	$4,-5234($16)
or	$3,$3,$5
or	$2,$3,$2
or	$2,$2,$4
andi	$2,$2,0x100
beq	$2,$0,$L1239
li	$3,65536			# 0x10000

lw	$25,%call16(pred_direct_motion_hw)($28)
addiu	$5,$sp,60
.reloc	1f,R_MIPS_JALR,pred_direct_motion_hw
1:	jalr	$25
move	$4,$21

li	$2,-2			# 0xfffffffffffffffe
lw	$3,6624($16)
lw	$28,32($sp)
sb	$2,11638($21)
sltu	$3,$3,2
sb	$2,11598($21)
sb	$2,11622($21)
bne	$3,$0,$L1231
sb	$2,11582($21)

lw	$2,%got(scan8)($28)
$L1256:
li	$4,131072			# 0x20000
li	$5,257			# 0x101
addiu	$4,$4,9088
addiu	$2,$2,%lo(scan8)
$L783:
lhu	$3,0($19)
andi	$3,$3,0x100
beq	$3,$0,$L782
addiu	$19,$19,2

lbu	$3,0($2)
addu	$3,$3,$4
addu	$3,$21,$3
sh	$5,4($3)
sh	$5,12($3)
$L782:
bne	$20,$19,$L783
addiu	$2,$2,4

b	$L1239
li	$3,65536			# 0x10000

$L1177:
lw	$25,%got(decode_cabac_intra_mb_type_hw)($28)
li	$5,32			# 0x20
move	$6,$0
addiu	$25,$25,%lo(decode_cabac_intra_mb_type_hw)
.reloc	1f,R_MIPS_JALR,decode_cabac_intra_mb_type_hw
1:	jalr	$25
move	$4,$21

lw	$28,32($sp)
b	$L712
addiu	$2,$2,23

$L1180:
lw	$2,7996($21)
li	$5,16			# 0x10
lw	$3,7992($21)
lw	$4,0($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC4)
sw	$2,20($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)

b	$L1031
li	$2,-1			# 0xffffffffffffffff

$L1207:
lw	$3,-5236($4)
li	$4,7340032			# 0x700000
addiu	$4,$4,112
or	$3,$3,$5
and	$3,$3,$4
sltu	$3,$3,1
b	$L792
sw	$3,108($sp)

$L981:
b	$L807
li	$7,-1			# 0xffffffffffffffff

$L1231:
lw	$2,6628($16)
sltu	$2,$2,2
beq	$2,$0,$L1256
lw	$2,%got(scan8)($28)

b	$L1239
li	$3,65536			# 0x10000

$L1184:
lw	$2,7996($21)
lw	$6,%got($LC6)($28)
lw	$4,0($21)
lw	$7,7992($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC6)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L1031
li	$2,-1			# 0xffffffffffffffff

$L1193:
lw	$2,104($3)
beq	$2,$0,$L1257
lw	$2,68($sp)

lw	$4,%got(sde_res_di)($28)
move	$2,$0
lw	$3,0($4)
sll	$5,$3,1
lw	$7,%got(sde_v_dc)($28)
$L1260:
addu	$4,$2,$5
lw	$8,%got(sde_res_dct)($28)
li	$9,8			# 0x8
addu	$6,$7,$2
addu	$4,$8,$4
addiu	$2,$2,2
lhu	$6,0($6)
bne	$2,$9,$L1260
sh	$6,0($4)

addiu	$3,$3,4
lw	$25,%got(sde_res_di)($28)
b	$L938
sw	$3,0($25)

$L1214:
move	$4,$18
move	$25,$16
.reloc	1f,R_MIPS_JALR,get_cabac
1:	jalr	$25
move	$5,$17

addiu	$2,$2,11
lw	$28,32($sp)
b	$L772
andi	$2,$2,0xffff

$L985:
b	$L874
li	$2,-1			# 0xffffffffffffffff

$L1202:
lw	$2,7996($21)
lw	$6,%got($LC5)($28)
lw	$4,0($21)
lw	$7,7992($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC5)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L1237
li	$3,-1			# 0xffffffffffffffff

$L1185:
lw	$2,7996($21)
lw	$6,%got($LC7)($28)
lw	$4,0($21)
lw	$7,7992($21)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC7)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)

b	$L1031
li	$2,-1			# 0xffffffffffffffff

$L1206:
lw	$25,108($sp)
bne	$25,$0,$L1258
li	$4,65536			# 0x10000

b	$L831
lw	$3,60($sp)

.set	macro
.set	reorder
.end	ff_h264_decode_mb_cabac_hw
.size	ff_h264_decode_mb_cabac_hw, .-ff_h264_decode_mb_cabac_hw
.rdata
.align	2
.type	last_coeff_flag_offset_8x8_hw.7987, @object
.size	last_coeff_flag_offset_8x8_hw.7987, 63
last_coeff_flag_offset_8x8_hw.7987:
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	5
.byte	5
.byte	6
.byte	6
.byte	6
.byte	6
.byte	7
.byte	7
.byte	7
.byte	7
.byte	8
.byte	8
.byte	8
.align	2
.type	significant_coeff_flag_offset_8x8.7986, @object
.size	significant_coeff_flag_offset_8x8.7986, 126
significant_coeff_flag_offset_8x8.7986:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	5
.byte	4
.byte	4
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	4
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	6
.byte	7
.byte	7
.byte	7
.byte	8
.byte	9
.byte	10
.byte	9
.byte	8
.byte	7
.byte	7
.byte	6
.byte	11
.byte	12
.byte	13
.byte	11
.byte	6
.byte	7
.byte	8
.byte	9
.byte	14
.byte	10
.byte	9
.byte	8
.byte	6
.byte	11
.byte	12
.byte	13
.byte	11
.byte	6
.byte	9
.byte	14
.byte	10
.byte	9
.byte	11
.byte	12
.byte	13
.byte	11
.byte	14
.byte	10
.byte	12
.byte	0
.byte	1
.byte	1
.byte	2
.byte	2
.byte	3
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	7
.byte	7
.byte	8
.byte	4
.byte	5
.byte	6
.byte	9
.byte	10
.byte	10
.byte	8
.byte	11
.byte	12
.byte	11
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	11
.byte	12
.byte	11
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	11
.byte	12
.byte	11
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	13
.byte	13
.byte	9
.byte	9
.byte	10
.byte	10
.byte	8
.byte	13
.byte	13
.byte	9
.byte	9
.byte	10
.byte	10
.byte	14
.byte	14
.byte	14
.byte	14
.byte	14
.align	2
.type	coeff_abs_level_m1_offset.7985, @object
.size	coeff_abs_level_m1_offset.7985, 24
coeff_abs_level_m1_offset.7985:
.word	227
.word	237
.word	247
.word	257
.word	266
.word	426
.align	2
.type	last_coeff_flag_offset.7984, @object
.size	last_coeff_flag_offset.7984, 48
last_coeff_flag_offset.7984:
.word	166
.word	181
.word	195
.word	210
.word	213
.word	417
.word	338
.word	353
.word	367
.word	382
.word	385
.word	451
.align	2
.type	significant_coeff_flag_offset.7983, @object
.size	significant_coeff_flag_offset.7983, 48
significant_coeff_flag_offset.7983:
.word	105
.word	120
.word	134
.word	149
.word	152
.word	402
.word	277
.word	292
.word	306
.word	321
.word	324
.word	436
.align	2
.type	left.7828, @object
.size	left.7828, 7
left.7828:
.byte	5
.byte	-1
.byte	2
.byte	-1
.byte	6
.space	2
.align	2
.type	top.7827, @object
.size	top.7827, 7
top.7827:
.byte	4
.byte	1
.byte	-1
.byte	-1
.space	3
.align	2
.type	left.7812, @object
.size	left.7812, 12
left.7812:
.byte	0
.byte	-1
.byte	10
.byte	0
.byte	-1
.byte	-1
.byte	-1
.byte	0
.byte	-1
.byte	11
.space	2
.align	2
.type	top.7811, @object
.size	top.7811, 12
top.7811:
.byte	-1
.byte	0
.byte	9
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	0
.space	3
.align	2
.type	b_sub_mb_type_info, @object
.size	b_sub_mb_type_info, 52
b_sub_mb_type_info:
.half	256
.byte	1
.space	1
.half	4104
.byte	1
.space	1
.half	16392
.byte	1
.space	1
.half	20488
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	-16368
.byte	2
.space	1
.half	-16352
.byte	2
.space	1
.half	-4080
.byte	2
.space	1
.half	-4064
.byte	2
.space	1
.half	12352
.byte	4
.space	1
.half	-16320
.byte	4
.space	1
.half	-4032
.byte	4
.space	1
.align	2
.type	b_mb_type_info, @object
.size	b_mb_type_info, 92
b_mb_type_info:
.half	256
.byte	1
.space	1
.half	4104
.byte	1
.space	1
.half	16392
.byte	1
.space	1
.half	20488
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	-16368
.byte	2
.space	1
.half	-16352
.byte	2
.space	1
.half	-28656
.byte	2
.space	1
.half	-28640
.byte	2
.space	1
.half	24592
.byte	2
.space	1
.half	24608
.byte	2
.space	1
.half	-20464
.byte	2
.space	1
.half	-20448
.byte	2
.space	1
.half	-8176
.byte	2
.space	1
.half	-8160
.byte	2
.space	1
.half	28688
.byte	2
.space	1
.half	28704
.byte	2
.space	1
.half	-12272
.byte	2
.space	1
.half	-12256
.byte	2
.space	1
.half	-4080
.byte	2
.space	1
.half	-4064
.byte	2
.space	1
.half	-4032
.byte	4
.space	1
.align	2
.type	p_sub_mb_type_info, @object
.size	p_sub_mb_type_info, 16
p_sub_mb_type_info:
.half	4104
.byte	1
.space	1
.half	4112
.byte	2
.space	1
.half	4128
.byte	2
.space	1
.half	4160
.byte	4
.space	1
.align	2
.type	p_mb_type_info, @object
.size	p_mb_type_info, 20
p_mb_type_info:
.half	4104
.byte	1
.space	1
.half	12304
.byte	2
.space	1
.half	12320
.byte	2
.space	1
.half	12352
.byte	4
.space	1
.half	12864
.byte	4
.space	1
.align	2
.type	i_mb_type_info, @object
.size	i_mb_type_info, 104
i_mb_type_info:
.half	1
.byte	-1
.byte	-1
.half	2
.byte	2
.byte	0
.half	2
.byte	1
.byte	0
.half	2
.byte	0
.byte	0
.half	2
.byte	3
.byte	0
.half	2
.byte	2
.byte	16
.half	2
.byte	1
.byte	16
.half	2
.byte	0
.byte	16
.half	2
.byte	3
.byte	16
.half	2
.byte	2
.byte	32
.half	2
.byte	1
.byte	32
.half	2
.byte	0
.byte	32
.half	2
.byte	3
.byte	32
.half	2
.byte	2
.byte	15
.half	2
.byte	1
.byte	15
.half	2
.byte	0
.byte	15
.half	2
.byte	3
.byte	15
.half	2
.byte	2
.byte	31
.half	2
.byte	1
.byte	31
.half	2
.byte	0
.byte	31
.half	2
.byte	3
.byte	31
.half	2
.byte	2
.byte	47
.half	2
.byte	1
.byte	47
.half	2
.byte	0
.byte	47
.half	2
.byte	3
.byte	47
.half	4
.byte	-1
.byte	-1
.align	2
.type	chroma_dc_scan, @object
.size	chroma_dc_scan, 4
chroma_dc_scan:
.byte	0
.byte	16
.byte	32
.byte	48
.align	2
.type	luma_dc_field_scan, @object
.size	luma_dc_field_scan, 16
luma_dc_field_scan:
.byte	0
.byte	32
.byte	16
.byte	-128
.byte	-96
.byte	48
.byte	-112
.byte	-80
.byte	64
.byte	96
.byte	-64
.byte	-32
.byte	80
.byte	112
.byte	-48
.byte	-16
.align	2
.type	luma_dc_zigzag_scan, @object
.size	luma_dc_zigzag_scan, 16
luma_dc_zigzag_scan:
.byte	0
.byte	16
.byte	32
.byte	-128
.byte	48
.byte	64
.byte	80
.byte	96
.byte	-112
.byte	-96
.byte	-80
.byte	-64
.byte	112
.byte	-48
.byte	-32
.byte	-16
.align	2
.type	scan8, @object
.size	scan8, 24
scan8:
.byte	12
.byte	13
.byte	20
.byte	21
.byte	14
.byte	15
.byte	22
.byte	23
.byte	28
.byte	29
.byte	36
.byte	37
.byte	30
.byte	31
.byte	38
.byte	39
.byte	9
.byte	10
.byte	17
.byte	18
.byte	33
.byte	34
.byte	41
.byte	42

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
