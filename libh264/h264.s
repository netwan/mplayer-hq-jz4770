.file	1 "h264.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.implicit_weight_table,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	implicit_weight_table
.type	implicit_weight_table, @function
implicit_weight_table:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,196608			# 0x30000
addu	$2,$4,$2
sw	$0,-15172($2)
sw	$0,-15164($2)
sw	$0,-15168($2)
bltz	$5,$L31
sw	$0,-15160($2)

li	$3,65536			# 0x10000
lw	$6,2696($4)
addiu	$8,$5,66
addu	$3,$4,$3
sll	$8,$8,2
li	$7,16			# 0x10
lw	$2,6624($3)
addu	$6,$6,$8
lw	$3,6628($3)
addiu	$2,$2,8
lw	$24,4($6)
addiu	$3,$3,8
sll	$2,$2,1
sll	$3,$3,1
$L6:
li	$9,65536			# 0x10000
li	$11,2			# 0x2
li	$10,5			# 0x5
addu	$6,$4,$9
slt	$8,$7,$2
sb	$11,-5232($6)
sb	$11,-5231($6)
sb	$10,-5230($6)
beq	$8,$0,$L28
sb	$10,-5229($6)

sll	$14,$2,3
sll	$6,$7,3
sll	$8,$7,5
sll	$2,$2,5
addu	$8,$6,$8
addu	$2,$14,$2
sll	$10,$7,6
sll	$12,$7,4
addiu	$9,$9,6916
subu	$12,$10,$12
sll	$6,$8,4
addiu	$11,$3,15365
sll	$10,$2,4
subu	$8,$6,$8
subu	$14,$10,$2
addu	$6,$11,$12
addiu	$12,$9,28800
addiu	$sp,$sp,-16
addu	$11,$8,$9
sll	$6,$6,2
sw	$16,4($sp)
addu	$8,$8,$12
sw	$18,12($sp)
addu	$9,$14,$9
sw	$17,8($sp)
subu	$15,$7,$3
addu	$12,$4,$8
addu	$11,$4,$11
addu	$6,$4,$6
addu	$14,$4,$9
sll	$15,$15,2
sll	$8,$5,1
li	$16,-128			# 0xffffffffffffff80
li	$25,127			# 0x7f
li	$10,64			# 0x40
slt	$13,$7,$3
$L9:
beq	$13,$0,$L17
lw	$7,0($11)

subu	$9,$24,$7
addu	$3,$6,$15
slt	$2,$9,128
movz	$9,$25,$2
move	$4,$12
move	$2,$9
slt	$9,$9,-128
movn	$2,$16,$9
move	$9,$2
$L16:
lw	$2,0($4)
subu	$17,$2,$7
slt	$2,$17,-128
bne	$2,$0,$L19
li	$2,-128			# 0xffffffffffffff80

slt	$2,$17,128
beq	$2,$0,$L20
nop

bne	$17,$0,$L32
li	$2,32			# 0x20

$L12:
bltz	$5,$L33
nop

$L14:
addu	$17,$3,$8
sh	$2,0($17)
$L15:
addiu	$3,$3,4
bne	$6,$3,$L16
addiu	$4,$4,600

$L17:
addiu	$11,$11,600
bne	$11,$14,$L9
addiu	$6,$6,192

lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,16
$L28:
j	$31
nop

$L19:
$L18:
mul	$2,$9,$2
addiu	$2,$2,32
sra	$2,$2,8
addiu	$17,$2,64
sltu	$17,$17,193
beq	$17,$0,$L22
nop

$L34:
subu	$2,$10,$2
sll	$2,$2,16
bgez	$5,$L14
sra	$2,$2,16

$L33:
sh	$2,2($3)
b	$L15
sh	$2,0($3)

$L20:
li	$2,129			# 0x81
mul	$2,$9,$2
addiu	$2,$2,32
sra	$2,$2,8
addiu	$17,$2,64
sltu	$17,$17,193
bne	$17,$0,$L34
nop

$L22:
b	$L12
li	$2,32			# 0x20

$L31:
li	$6,65536			# 0x10000
lw	$2,2696($4)
li	$3,1			# 0x1
addu	$6,$4,$6
lw	$24,276($2)
lw	$2,6624($6)
beq	$2,$3,$L35
nop

lw	$3,6628($6)
$L4:
b	$L6
move	$7,$0

$L35:
lw	$3,6628($6)
bne	$3,$2,$L6
move	$7,$0

lw	$7,-5252($6)
bne	$7,$0,$L6
move	$7,$0

li	$9,131072			# 0x20000
lw	$8,6916($6)
sll	$7,$24,1
addu	$9,$4,$9
lw	$9,-29820($9)
addu	$8,$8,$9
bne	$8,$7,$L4
nop

sb	$0,-5232($6)
j	$31
sb	$0,-5231($6)

$L32:
subu	$18,$0,$17
slt	$2,$17,0
movz	$18,$17,$2
move	$2,$18
sra	$2,$2,1
addiu	$2,$2,16384
teq	$17,$0,7
div	$0,$2,$17
b	$L18
mflo	$2

.set	macro
.set	reorder
.end	implicit_weight_table
.size	implicit_weight_table, .-implicit_weight_table
.section	.text.free_tables,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	free_tables
.type	free_tables, @function
free_tables:
.frame	$sp,72,$31		# vars= 8, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-72
lw	$25,%call16(av_freep)($28)
sw	$16,32($sp)
move	$16,$4
addiu	$4,$4,10856
sw	$31,68($sp)
.cprestore	16
sw	$fp,64($sp)
sw	$23,60($sp)
li	$23,1			# 0x1
sw	$22,56($sp)
sw	$21,52($sp)
sw	$20,48($sp)
li	$20,1			# 0x1
sw	$19,44($sp)
li	$19,131072			# 0x20000
sw	$18,40($sp)
li	$18,16			# 0x10
sw	$17,36($sp)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
li	$17,131072			# 0x20000

li	$2,65536			# 0x10000
lw	$28,16($sp)
addiu	$4,$16,10860
addu	$fp,$16,$2
sw	$2,24($sp)
addiu	$22,$19,9480
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$21,$19,9476

addiu	$4,$17,8748
lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$4

addiu	$4,$17,8732
lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$4

addiu	$4,$17,8756
lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$4

addiu	$4,$17,8760
lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$4

addiu	$4,$17,9088
lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$4

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11232

addiu	$4,$17,10656
lw	$28,16($sp)
ori	$17,$17,0xc454
addu	$4,$16,$4
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$17,$16,$17

li	$4,60256			# 0xeb60
lw	$28,16($sp)
addu	$4,$16,$4
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
sw	$0,-5268($fp)

lw	$2,24($sp)
lw	$28,16($sp)
sw	$0,-5276($fp)
addiu	$4,$2,6636
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$4

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11852

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11856

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11860

lw	$28,16($sp)
$L37:
lw	$16,0($17)
lw	$25,%call16(av_freep)($28)
beq	$16,$0,$L38
addiu	$4,$16,11104

.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
nop

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,11100

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,2864

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$16,$22

addu	$4,$16,$21
lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$16,$16,$19

move	$4,$17
lw	$28,16($sp)
sw	$0,9484($16)
sw	$0,9488($16)
beq	$23,$20,$L39
lw	$25,%call16(av_freep)($28)

.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
nop

lw	$28,16($sp)
$L38:
beq	$23,$18,$L44
lw	$31,68($sp)

$L39:
addiu	$17,$17,4
b	$L37
addiu	$23,$23,1

$L44:
lw	$fp,64($sp)
lw	$23,60($sp)
lw	$22,56($sp)
lw	$21,52($sp)
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
j	$31
addiu	$sp,$sp,72

.set	macro
.set	reorder
.end	free_tables
.size	free_tables, .-free_tables
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC3:
.ascii	"hardware accelerator failed to decode picture\012\000"
.section	.text.field_end,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	field_end
.type	field_end, @function
field_end:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$5,2904($4)
addiu	$sp,$sp,-40
lw	$2,2696($4)
li	$6,2			# 0x2
lw	$3,2916($4)
.cprestore	16
sw	$17,28($sp)
sw	$16,24($sp)
move	$16,$4
sw	$31,36($sp)
sw	$18,32($sp)
sw	$0,7996($4)
lw	$17,0($4)
sw	$6,160($2)
beq	$3,$0,$L52
sw	$5,52($2)

$L46:
li	$2,131072			# 0x20000
lw	$3,872($17)
addu	$2,$16,$2
lw	$5,10692($2)
lw	$4,10680($2)
sw	$5,10696($2)
beq	$3,$0,$L48
sw	$4,10700($2)

lw	$25,32($3)
jalr	$25
move	$4,$17

bltz	$2,$L53
lw	$28,16($sp)

$L48:
lw	$3,10384($16)
li	$2,3			# 0x3
beq	$3,$2,$L54
lw	$25,%call16(ff_er_frame_end)($28)

$L50:
lw	$25,%call16(MPV_frame_end)($28)
.reloc	1f,R_MIPS_JALR,MPV_frame_end
1:	jalr	$25
move	$4,$16

li	$2,196608			# 0x30000
lw	$31,36($sp)
addu	$16,$16,$2
lw	$18,32($sp)
lw	$17,28($sp)
sw	$0,-15212($16)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

$L52:
li	$2,196608			# 0x30000
lw	$25,%call16(ff_h264_execute_ref_pic_marking)($28)
li	$18,131072			# 0x20000
addu	$2,$4,$2
ori	$5,$18,0xc12c
addu	$5,$4,$5
.reloc	1f,R_MIPS_JALR,ff_h264_execute_ref_pic_marking
1:	jalr	$25
lw	$6,-15292($2)

addu	$2,$16,$18
lw	$28,16($sp)
lw	$4,10664($2)
lw	$3,10660($2)
sw	$4,10684($2)
b	$L46
sw	$3,10688($2)

$L54:
.reloc	1f,R_MIPS_JALR,ff_er_frame_end
1:	jalr	$25
move	$4,$16

b	$L50
lw	$28,16($sp)

$L53:
lw	$6,%got($LC3)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$17
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)

b	$L48
lw	$28,16($sp)

.set	macro
.set	reorder
.end	field_end
.size	field_end, .-field_end
.section	.text.flush_dpb,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	flush_dpb
.type	flush_dpb, @function
flush_dpb:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-40
li	$2,131072			# 0x20000
sw	$16,28($sp)
ori	$2,$2,0xc0e0
sw	$17,32($sp)
.cprestore	16
addiu	$5,$2,64
sw	$31,36($sp)
move	$17,$4
lw	$16,136($4)
addu	$2,$16,$2
addu	$5,$16,$5
$L57:
lw	$3,0($2)
beq	$3,$0,$L56
nop

sw	$0,80($3)
$L56:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$5,$L57
lw	$25,%call16(ff_h264_remove_all_refs)($28)

li	$2,196608			# 0x30000
li	$3,-2147483648			# 0xffffffff80000000
addu	$2,$16,$2
move	$4,$16
sw	$3,-16088($2)
li	$3,1			# 0x1
.reloc	1f,R_MIPS_JALR,ff_h264_remove_all_refs
1:	jalr	$25
sw	$3,-15192($2)

li	$2,131072			# 0x20000
lw	$3,2696($16)
addu	$2,$16,$2
lw	$28,16($sp)
sw	$0,10700($2)
sw	$0,10696($2)
sw	$0,10688($2)
beq	$3,$0,$L58
sw	$0,10684($2)

sw	$0,80($3)
$L58:
lw	$25,%call16(ff_h264_reset_sei)($28)
move	$4,$16
.reloc	1f,R_MIPS_JALR,ff_h264_reset_sei
1:	jalr	$25
sw	$0,10456($16)

move	$4,$17
lw	$28,16($sp)
lw	$31,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
lw	$25,%call16(ff_mpeg_flush)($28)
.reloc	1f,R_MIPS_JALR,ff_mpeg_flush
1:	jr	$25
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	flush_dpb
.size	flush_dpb, .-flush_dpb
.section	.text.h264_luma_dc_dequant_idct_c.isra.5,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_luma_dc_dequant_idct_c.isra.5
.type	h264_luma_dc_dequant_idct_c.isra.5, @function
h264_luma_dc_dequant_idct_c.isra.5:
.frame	$sp,72,$31		# vars= 64, regs= 0/0, args= 0, gp= 8
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-72
lw	$10,%got(y_offset.8880)($28)
addiu	$9,$sp,8
.cprestore	0
addiu	$13,$sp,72
addiu	$10,$10,%lo(y_offset.8880)
move	$6,$9
$L68:
lw	$2,0($10)
addiu	$6,$6,16
addiu	$10,$10,4
sll	$2,$2,1
addu	$2,$4,$2
lh	$7,32($2)
lh	$12,160($2)
lh	$3,0($2)
lh	$11,128($2)
addu	$8,$3,$11
subu	$2,$3,$11
subu	$11,$7,$12
addu	$3,$7,$12
addu	$12,$2,$11
addu	$7,$8,$3
subu	$2,$2,$11
subu	$3,$8,$3
sw	$12,-12($6)
sw	$7,-16($6)
sw	$2,-8($6)
bne	$6,$13,$L68
sw	$3,-4($6)

lw	$10,%got(x_offset.8879)($28)
addiu	$11,$sp,24
addiu	$10,$10,%lo(x_offset.8879)
$L69:
lw	$2,0($9)
addiu	$9,$9,4
addiu	$10,$10,4
lw	$6,-4($10)
lw	$8,28($9)
sll	$6,$6,1
lw	$7,12($9)
lw	$12,44($9)
addu	$6,$4,$6
addu	$3,$2,$8
subu	$2,$2,$8
subu	$13,$7,$12
addu	$12,$7,$12
addu	$7,$2,$13
addu	$8,$3,$12
subu	$2,$2,$13
subu	$3,$3,$12
mul	$8,$8,$5
mul	$7,$5,$7
mul	$2,$5,$2
mul	$3,$5,$3
addiu	$8,$8,128
addiu	$7,$7,128
addiu	$2,$2,128
addiu	$3,$3,128
sra	$8,$8,8
sra	$7,$7,8
sra	$2,$2,8
sra	$3,$3,8
sh	$8,0($6)
sh	$7,64($6)
sh	$2,256($6)
bne	$9,$11,$L69
sh	$3,320($6)

j	$31
addiu	$sp,$sp,72

.set	macro
.set	reorder
.end	h264_luma_dc_dequant_idct_c.isra.5
.size	h264_luma_dc_dequant_idct_c.isra.5, .-h264_luma_dc_dequant_idct_c.isra.5
.section	.text.chroma_dc_dequant_idct_c.isra.6,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	chroma_dc_dequant_idct_c.isra.6
.type	chroma_dc_dequant_idct_c.isra.6, @function
chroma_dc_dequant_idct_c.isra.6:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lh	$3,0($4)
lh	$6,64($4)
lh	$2,32($4)
lh	$7,96($4)
subu	$8,$3,$2
subu	$9,$6,$7
addu	$2,$3,$2
addu	$7,$6,$7
addu	$3,$8,$9
addu	$6,$2,$7
subu	$8,$8,$9
subu	$2,$2,$7
mul	$6,$6,$5
mul	$3,$5,$3
mul	$2,$5,$2
mul	$5,$5,$8
sra	$6,$6,7
sra	$3,$3,7
sra	$2,$2,7
sra	$5,$5,7
sh	$6,0($4)
sh	$3,32($4)
sh	$2,64($4)
j	$31
sh	$5,96($4)

.set	macro
.set	reorder
.end	chroma_dc_dequant_idct_c.isra.6
.size	chroma_dc_dequant_idct_c.isra.6, .-chroma_dc_dequant_idct_c.isra.6
.section	.text.init_scan_tables,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	init_scan_tables
.type	init_scan_tables, @function
init_scan_tables:
.frame	$sp,80,$31		# vars= 16, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-80
lw	$9,%got(zigzag_scan)($28)
lw	$25,%call16(memcpy)($28)
li	$6,16			# 0x10
sw	$17,44($sp)
li	$17,131072			# 0x20000
addiu	$5,$9,%lo(zigzag_scan)
sw	$31,76($sp)
addiu	$8,$17,9132
sw	$9,36($sp)
.cprestore	16
addu	$8,$4,$8
sw	$fp,72($sp)
sw	$23,68($sp)
addiu	$fp,$17,9276
sw	$22,64($sp)
sw	$8,32($sp)
sw	$21,60($sp)
sw	$20,56($sp)
sw	$19,52($sp)
addiu	$19,$17,9292
sw	$18,48($sp)
addiu	$18,$17,9356
sw	$16,40($sp)
move	$16,$4
move	$4,$8
lw	$22,%got(field_scan)($28)
lw	$23,%got(zigzag_scan8x8_cavlc)($28)
addu	$fp,$16,$fp
lw	$21,%got(field_scan8x8)($28)
addu	$19,$16,$19
lw	$20,%got(field_scan8x8_cavlc)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$18,$16,$18

addiu	$5,$22,%lo(field_scan)
lw	$28,16($sp)
li	$6,16			# 0x10
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$4,$fp

addiu	$7,$17,9148
lw	$28,16($sp)
li	$6,64			# 0x40
addu	$7,$16,$7
move	$4,$7
lw	$25,%call16(memcpy)($28)
lw	$5,%got(ff_zigzag_direct)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$7,28($sp)

addiu	$3,$17,9212
lw	$28,16($sp)
addiu	$5,$23,%lo(zigzag_scan8x8_cavlc)
addu	$3,$16,$3
li	$6,64			# 0x40
move	$4,$3
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$3,24($sp)

addiu	$5,$21,%lo(field_scan8x8)
lw	$28,16($sp)
li	$6,64			# 0x40
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$4,$19

addiu	$5,$20,%lo(field_scan8x8_cavlc)
lw	$28,16($sp)
li	$6,64			# 0x40
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$4,$18

lw	$2,11900($16)
lw	$28,16($sp)
lw	$3,24($sp)
lw	$7,28($sp)
lw	$8,32($sp)
bne	$2,$0,$L78
lw	$9,36($sp)

lw	$31,76($sp)
addu	$16,$16,$17
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$17,44($sp)
sw	$fp,9432($16)
sw	$19,9436($16)
sw	$18,9440($16)
sw	$8,9420($16)
sw	$7,9424($16)
sw	$3,9428($16)
lw	$fp,72($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,80

$L78:
lw	$2,%got(ff_zigzag_direct)($28)
addiu	$9,$9,%lo(zigzag_scan)
lw	$31,76($sp)
addiu	$23,$23,%lo(zigzag_scan8x8_cavlc)
addiu	$22,$22,%lo(field_scan)
lw	$fp,72($sp)
addiu	$21,$21,%lo(field_scan8x8)
lw	$19,52($sp)
addiu	$20,$20,%lo(field_scan8x8_cavlc)
lw	$18,48($sp)
addu	$16,$16,$17
lw	$17,44($sp)
sw	$23,9428($16)
sw	$22,9432($16)
sw	$21,9436($16)
sw	$20,9440($16)
sw	$9,9420($16)
sw	$2,9424($16)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	init_scan_tables
.size	init_scan_tables, .-init_scan_tables
.section	.text.init_dequant_tables,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	init_dequant_tables
.type	init_dequant_tables, @function
init_dequant_tables:
.frame	$sp,200,$31		# vars= 136, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-200
lw	$2,10704($4)
lw	$8,%got(ff_h264_idct_add_c)($28)
li	$7,32896			# 0x8080
sw	$4,200($sp)
addiu	$10,$sp,24
lw	$4,%got(vmau_base)($28)
li	$6,16			# 0x10
subu	$8,$2,$8
sw	$16,160($sp)
li	$2,2			# 0x2
sw	$10,116($sp)
li	$10,32992			# 0x80e0
.cprestore	16
lw	$3,0($4)
li	$4,3			# 0x3
sw	$2,40($sp)
li	$2,5			# 0x5
addiu	$9,$sp,24
sw	$31,196($sp)
sw	$4,28($sp)
li	$4,1			# 0x1
addu	$7,$3,$7
sw	$2,44($sp)
addu	$10,$3,$10
sw	$fp,192($sp)
sw	$4,32($sp)
addiu	$3,$sp,88
li	$4,4			# 0x4
sw	$23,188($sp)
move	$2,$0
sw	$22,184($sp)
sw	$21,180($sp)
sltu	$16,$0,$8
sw	$20,176($sp)
sw	$19,172($sp)
sw	$18,168($sp)
sw	$17,164($sp)
sw	$0,24($sp)
sw	$4,36($sp)
sw	$3,104($sp)
$L83:
lw	$8,200($sp)
sll	$2,$2,4
move	$4,$0
.set	noreorder
.set	nomacro
b	$L81
addu	$5,$8,$2
.set	macro
.set	reorder

$L141:
sra	$2,$4,2
andi	$3,$3,0xf
or	$2,$3,$2
addu	$2,$5,$2
addiu	$8,$sp,88
lbu	$2,12892($2)
addu	$3,$8,$4
addiu	$4,$4,1
.set	noreorder
.set	nomacro
beq	$4,$6,$L140
sb	$2,0($3)
.set	macro
.set	reorder

$L81:
.set	noreorder
.set	nomacro
bne	$16,$0,$L141
sll	$3,$4,2
.set	macro
.set	reorder

move	$2,$4
addu	$2,$5,$2
addiu	$8,$sp,88
lbu	$2,12892($2)
addu	$3,$8,$4
addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$4,$6,$L81
sb	$2,0($3)
.set	macro
.set	reorder

$L140:
lw	$2,88($sp)
#APP
# 1154 "h264.c" 1
sw	 $2,0($7)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$7,4
lw	$2,92($sp)
#APP
# 1154 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$7,8
lw	$2,96($sp)
#APP
# 1154 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$7,12
lw	$2,100($sp)
#APP
# 1154 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$7,$7,16
.set	noreorder
.set	nomacro
beq	$7,$10,$L82
addiu	$9,$9,4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L83
lw	$2,0($9)
.set	macro
.set	reorder

$L82:
lw	$3,200($sp)
li	$22,60224			# 0xeb40
lw	$10,200($sp)
li	$21,16			# 0x10
lw	$13,%got(div6)($28)
li	$23,52			# 0x34
addu	$22,$3,$22
lw	$12,%got(rem6)($28)
addiu	$10,$10,13632
sw	$0,108($sp)
addiu	$17,$13,%lo(div6)
sw	$22,128($sp)
addiu	$18,$12,%lo(rem6)
sw	$10,120($sp)
move	$fp,$3
lw	$10,%got(dequant4_coeff_init)($28)
move	$22,$3
lw	$8,128($sp)
addiu	$20,$10,%lo(dequant4_coeff_init)
sw	$8,124($sp)
$L86:
lw	$3,120($sp)
lw	$10,124($sp)
lw	$8,108($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L115
sw	$3,0($10)
.set	macro
.set	reorder

addiu	$10,$22,12892
lw	$3,200($sp)
move	$19,$0
sw	$10,112($sp)
.set	noreorder
.set	nomacro
b	$L88
addiu	$15,$3,12892
.set	macro
.set	reorder

$L85:
lw	$8,108($sp)
addiu	$19,$19,1
beq	$19,$8,$L115
$L88:
lw	$25,%call16(memcmp)($28)
li	$6,16			# 0x10
lw	$5,112($sp)
move	$4,$15
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcmp
1:	jalr	$25
sw	$15,132($sp)
.set	macro
.set	reorder

lw	$15,132($sp)
lw	$28,16($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L85
addiu	$15,$15,16
.set	macro
.set	reorder

sll	$3,$19,2
lw	$8,108($sp)
sll	$2,$19,4
lw	$10,200($sp)
subu	$2,$2,$3
lw	$3,124($sp)
slt	$4,$19,$8
addu	$19,$2,$19
sll	$19,$19,8
addiu	$19,$19,13632
addu	$19,$10,$19
.set	noreorder
.set	nomacro
beq	$4,$0,$L84
sw	$19,0($3)
.set	macro
.set	reorder

lw	$8,108($sp)
$L150:
li	$4,6			# 0x6
lw	$10,120($sp)
addiu	$22,$22,16
lw	$3,124($sp)
addiu	$8,$8,1
addiu	$10,$10,3328
addiu	$3,$3,4
sw	$8,108($sp)
sw	$10,120($sp)
.set	noreorder
.set	nomacro
bne	$8,$4,$L86
sw	$3,124($sp)
.set	macro
.set	reorder

lw	$3,200($sp)
lw	$2,12888($3)
.set	noreorder
.set	nomacro
bne	$2,$0,$L142
li	$11,33600			# 0x8340
.set	macro
.set	reorder

$L94:
lw	$3,200($sp)
lw	$2,11900($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L79
lw	$8,200($sp)
.set	macro
.set	reorder

li	$5,60248			# 0xeb58
li	$3,64			# 0x40
addu	$5,$8,$5
$L108:
lw	$8,128($sp)
lw	$2,0($8)
addiu	$4,$2,64
$L109:
sw	$3,0($2)
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$4,$L109
lw	$10,128($sp)
.set	macro
.set	reorder

addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$10,$5,$L108
sw	$10,128($sp)
.set	macro
.set	reorder

lw	$10,200($sp)
lw	$2,12888($10)
.set	noreorder
.set	nomacro
beq	$2,$0,$L151
lw	$31,196($sp)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
li	$4,64			# 0x40
addu	$2,$10,$2
lw	$2,-5288($2)
addiu	$3,$2,256
$L112:
sw	$4,0($2)
addiu	$2,$2,4
bne	$2,$3,$L112
lw	$3,200($sp)
li	$2,65536			# 0x10000
li	$4,64			# 0x40
addu	$18,$3,$2
lw	$2,-5284($18)
addiu	$3,$2,256
$L113:
sw	$4,0($2)
addiu	$2,$2,4
bne	$2,$3,$L113
$L79:
lw	$31,196($sp)
$L151:
lw	$fp,192($sp)
lw	$23,188($sp)
lw	$22,184($sp)
lw	$21,180($sp)
lw	$20,176($sp)
lw	$19,172($sp)
lw	$18,168($sp)
lw	$17,164($sp)
lw	$16,160($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,200
.set	macro
.set	reorder

$L115:
lw	$19,120($sp)
$L84:
move	$24,$0
$L92:
addu	$3,$18,$24
addu	$2,$17,$24
move	$4,$0
lbu	$3,0($3)
lbu	$15,0($2)
sll	$6,$3,1
addiu	$15,$15,2
.set	noreorder
.set	nomacro
b	$L91
addu	$6,$6,$3
.set	macro
.set	reorder

$L144:
andi	$25,$4,0x1
andi	$5,$5,0x1
addu	$3,$25,$5
addu	$5,$22,$4
addu	$3,$6,$3
move	$2,$4
addu	$3,$20,$3
lbu	$5,12892($5)
sll	$2,$2,2
addiu	$4,$4,1
lbu	$3,0($3)
addu	$2,$19,$2
mul	$5,$3,$5
sll	$5,$5,$15
.set	noreorder
.set	nomacro
beq	$4,$21,$L143
sw	$5,0($2)
.set	macro
.set	reorder

$L91:
sll	$2,$4,2
.set	noreorder
.set	nomacro
beq	$16,$0,$L144
sra	$5,$4,2
.set	macro
.set	reorder

andi	$2,$2,0xf
or	$2,$2,$5
andi	$25,$4,0x1
andi	$5,$5,0x1
addu	$3,$25,$5
addu	$5,$22,$4
addu	$3,$6,$3
sll	$2,$2,2
addu	$3,$20,$3
lbu	$5,12892($5)
addu	$2,$19,$2
addiu	$4,$4,1
lbu	$3,0($3)
mul	$5,$3,$5
sll	$5,$5,$15
.set	noreorder
.set	nomacro
bne	$4,$21,$L91
sw	$5,0($2)
.set	macro
.set	reorder

$L143:
addiu	$24,$24,1
.set	noreorder
.set	nomacro
bne	$24,$23,$L92
addiu	$19,$19,64
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L150
lw	$8,108($sp)
.set	macro
.set	reorder

$L142:
lw	$8,200($sp)
lw	$4,10708($3)
li	$2,65536			# 0x10000
addu	$11,$3,$11
lw	$17,%got(ff_h264_idct8_add_c)($28)
li	$3,46912			# 0xb740
lw	$9,%got(div6)($28)
addu	$2,$8,$2
lw	$21,%got(dequant8_coeff_init_scan)($28)
addu	$3,$8,$3
lw	$19,%got(dequant8_coeff_init)($28)
li	$12,60252			# 0xeb5c
sw	$11,-5288($2)
subu	$17,$4,$17
sw	$3,-5284($2)
addu	$12,$8,$12
lw	$3,200($sp)
move	$25,$0
lw	$8,%got(rem6)($28)
addiu	$9,$9,%lo(div6)
addiu	$21,$21,%lo(dequant8_coeff_init_scan)
addiu	$3,$3,12988
addiu	$8,$8,%lo(rem6)
addiu	$19,$19,%lo(dequant8_coeff_init)
sw	$3,108($sp)
li	$23,64			# 0x40
lw	$3,200($sp)
li	$10,52			# 0x34
li	$13,1			# 0x1
sltu	$17,$0,$17
addiu	$3,$3,13052
move	$14,$11
sw	$3,112($sp)
$L95:
move	$24,$0
$L100:
addu	$2,$8,$24
addu	$4,$9,$24
move	$3,$0
lbu	$2,0($2)
lbu	$15,0($4)
sll	$4,$2,1
sll	$2,$2,3
.set	noreorder
.set	nomacro
b	$L99
subu	$6,$2,$4
.set	macro
.set	reorder

$L146:
sll	$2,$2,3
sra	$4,$3,3
or	$2,$2,$4
$L98:
sra	$4,$3,1
andi	$5,$3,0x3
andi	$4,$4,0xc
or	$4,$4,$5
addu	$4,$21,$4
addu	$5,$fp,$3
sll	$2,$2,2
lbu	$4,0($4)
addiu	$3,$3,1
lbu	$5,12988($5)
addu	$2,$14,$2
addu	$4,$6,$4
addu	$4,$19,$4
lbu	$4,0($4)
mul	$5,$4,$5
sll	$5,$5,$15
.set	noreorder
.set	nomacro
beq	$3,$23,$L145
sw	$5,0($2)
.set	macro
.set	reorder

$L99:
.set	noreorder
.set	nomacro
bne	$17,$0,$L146
andi	$2,$3,0x7
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L98
move	$2,$3
.set	macro
.set	reorder

$L145:
addiu	$24,$24,1
.set	noreorder
.set	nomacro
bne	$24,$10,$L100
addiu	$14,$14,256
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$25,$13,$L101
lw	$25,%call16(memcmp)($28)
.set	macro
.set	reorder

$L97:
lw	$3,%got(vmau_base)($28)
li	$7,32768			# 0x8000
lw	$10,116($sp)
move	$8,$0
li	$6,64			# 0x40
li	$9,1			# 0x1
lw	$2,0($3)
addu	$7,$2,$7
subu	$7,$7,$10
$L102:
lw	$10,200($sp)
sll	$5,$8,6
move	$3,$0
.set	noreorder
.set	nomacro
b	$L104
addu	$5,$10,$5
.set	macro
.set	reorder

$L148:
sra	$4,$3,3
sll	$2,$2,3
addiu	$10,$sp,24
or	$2,$2,$4
addu	$2,$5,$2
addu	$4,$10,$3
addiu	$3,$3,1
lbu	$2,12988($2)
.set	noreorder
.set	nomacro
beq	$3,$6,$L147
sb	$2,0($4)
.set	macro
.set	reorder

$L104:
.set	noreorder
.set	nomacro
bne	$17,$0,$L148
andi	$2,$3,0x7
.set	macro
.set	reorder

move	$2,$3
addu	$2,$5,$2
addiu	$10,$sp,24
lbu	$2,12988($2)
addu	$4,$10,$3
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$3,$6,$L104
sb	$2,0($4)
.set	macro
.set	reorder

$L147:
move	$2,$10
$L105:
lw	$3,0($2)
addu	$4,$2,$7
#APP
# 1119 "h264.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,104($sp)
addiu	$2,$2,4
bne	$3,$2,$L105
.set	noreorder
.set	nomacro
beq	$8,$9,$L94
addiu	$7,$7,64
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L102
li	$8,1			# 0x1
.set	macro
.set	reorder

$L101:
li	$6,64			# 0x40
lw	$4,108($sp)
lw	$5,112($sp)
sw	$8,144($sp)
sw	$9,140($sp)
sw	$10,148($sp)
sw	$11,132($sp)
sw	$12,136($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcmp
1:	jalr	$25
sw	$13,152($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$8,144($sp)
lw	$9,140($sp)
lw	$10,148($sp)
lw	$11,132($sp)
lw	$12,136($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L149
lw	$13,152($sp)
.set	macro
.set	reorder

lw	$14,0($12)
addiu	$fp,$fp,64
addiu	$12,$12,4
.set	noreorder
.set	nomacro
b	$L95
li	$25,1			# 0x1
.set	macro
.set	reorder

$L149:
lw	$8,200($sp)
li	$2,65536			# 0x10000
addu	$2,$8,$2
.set	noreorder
.set	nomacro
b	$L97
sw	$11,-5284($2)
.set	macro
.set	reorder

.end	init_dequant_tables
.size	init_dequant_tables, .-init_dequant_tables
.section	.text.get_se_golomb.isra.11,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_se_golomb.isra.11
.type	get_se_golomb.isra.11, @function
get_se_golomb.isra.11:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$8,0($5)
srl	$3,$8,3
andi	$9,$8,0x7
addu	$4,$4,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($4)  
lwr $2, 0($4)  

# 0 "" 2
#NO_APP
li	$4,16711680			# 0xff0000
srl	$6,$2,8
addiu	$4,$4,255
sll	$7,$2,8
and	$3,$6,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$4,$7,$4
or	$4,$3,$4
sll	$3,$4,16
srl	$2,$4,16
or	$2,$3,$2
sll	$2,$2,$9
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L157
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$3,$3
li	$4,31			# 0x1f
addiu	$8,$8,32
subu	$3,$4,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$8,$8,$3
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$3,$0,$L158
sw	$8,0($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$31
subu	$2,$0,$2
.set	macro
.set	reorder

$L158:
j	$31
$L157:
lw	$4,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$3,0($4)
lb	$2,0($2)
addu	$8,$3,$8
.set	noreorder
.set	nomacro
j	$31
sw	$8,0($5)
.set	macro
.set	reorder

.end	get_se_golomb.isra.11
.size	get_se_golomb.isra.11, .-get_se_golomb.isra.11
.section	.rodata.str1.4
.align	2
$LC4:
.ascii	"if edge\012\000"
.align	2
$LC5:
.ascii	"list1\012\000"
.align	2
$LC6:
.ascii	"list2\012\000"
.section	.text.mc_part,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	mc_part
.type	mc_part, @function
mc_part:
.frame	$sp,192,$31		# vars= 104, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-192
li	$2,65536			# 0x10000
sw	$fp,184($sp)
move	$fp,$4
sw	$23,180($sp)
addu	$2,$4,$2
sw	$22,176($sp)
sw	$19,164($sp)
.cprestore	40
sw	$31,188($sp)
sw	$21,172($sp)
sw	$20,168($sp)
sw	$18,160($sp)
sw	$17,156($sp)
sw	$16,152($sp)
lw	$25,232($sp)
lw	$3,208($sp)
lbu	$2,-5232($2)
lw	$9,212($sp)
lw	$19,216($sp)
lw	$8,220($sp)
lw	$4,228($sp)
lw	$22,240($sp)
lw	$23,244($sp)
lw	$11,248($sp)
lw	$10,252($sp)
sw	$25,72($sp)
sw	$6,80($sp)
li	$6,2			# 0x2
sw	$5,76($sp)
sw	$7,60($sp)
sw	$3,100($sp)
lw	$25,236($sp)
lw	$5,224($sp)
lw	$7,256($sp)
sw	$25,48($sp)
beq	$2,$6,$L292
lw	$17,260($sp)

li	$6,1			# 0x1
bne	$2,$6,$L161
lw	$6,%got(scan8)($28)

lw	$25,76($sp)
lw	$2,7996($fp)
addiu	$6,$6,%lo(scan8)
addu	$6,$25,$6
lbu	$13,0($6)
$L162:
lw	$14,11876($fp)
sll	$18,$4,1
lw	$6,11872($fp)
li	$15,65536			# 0x10000
lw	$25,0($11)
sll	$12,$5,1
mul	$3,$4,$14
lw	$11,12($11)
addu	$16,$fp,$15
lw	$21,7992($fp)
lw	$22,12($10)
sw	$25,84($sp)
sw	$11,104($sp)
sll	$21,$21,3
lw	$11,-5248($16)
lw	$25,0($10)
addu	$20,$3,$5
mul	$3,$18,$6
sra	$2,$2,$11
addu	$19,$19,$20
sw	$25,64($sp)
addu	$8,$8,$20
sll	$2,$2,3
sw	$19,92($sp)
addu	$5,$5,$21
sw	$8,88($sp)
addu	$4,$4,$2
addu	$18,$3,$12
addu	$18,$9,$18
beq	$7,$0,$L164
sw	$18,52($sp)

bne	$17,$0,$L293
addu	$2,$fp,$13

$L164:
sltu	$17,$0,$17
lw	$10,11880($fp)
sll	$2,$17,3
lw	$14,11884($fp)
sll	$7,$17,5
lw	$19,164($fp)
sll	$3,$17,7
addu	$7,$2,$7
sll	$20,$17,11
addu	$2,$fp,$7
sw	$3,68($sp)
addu	$7,$7,$13
lw	$8,68($sp)
addu	$13,$2,$13
lw	$3,160($fp)
addiu	$2,$7,2812
sll	$7,$7,2
lb	$18,11568($13)
sll	$2,$2,2
subu	$12,$20,$8
addu	$2,$fp,$2
sll	$25,$18,5
sll	$21,$18,3
addu	$7,$fp,$7
lh	$13,0($2)
addu	$23,$21,$25
sw	$25,56($sp)
sll	$15,$12,4
sll	$2,$23,4
lh	$9,11250($7)
sll	$8,$5,3
subu	$7,$2,$23
subu	$5,$15,$12
sll	$4,$4,3
addu	$5,$7,$5
addu	$16,$9,$4
addu	$22,$13,$8
addu	$4,$fp,$5
sra	$13,$16,2
li	$5,65536			# 0x10000
addiu	$2,$10,-3
addu	$4,$5,$4
mul	$5,$6,$13
andi	$8,$22,0x7
movn	$10,$2,$8
sra	$2,$22,2
andi	$12,$16,0x3
lw	$15,6648($4)
sll	$3,$3,4
sll	$19,$19,4
sw	$8,64($sp)
subu	$4,$0,$10
addu	$7,$5,$2
sw	$3,96($sp)
sll	$5,$12,2
addiu	$24,$14,-3
andi	$12,$22,0x3
andi	$23,$16,0x7
slt	$4,$2,$4
movn	$14,$24,$23
addu	$12,$12,$5
addu	$7,$15,$7
bne	$4,$0,$L186
sra	$19,$19,$11

subu	$4,$0,$14
slt	$4,$13,$4
bne	$4,$0,$L186
addiu	$4,$2,15

addu	$10,$3,$10
slt	$10,$4,$10
beq	$10,$0,$L314
subu	$5,$0,$6

addiu	$4,$13,15
addu	$14,$19,$14
slt	$14,$4,$14
bne	$14,$0,$L187
move	$10,$0

$L186:
subu	$5,$0,$6
$L314:
lw	$8,96($sp)
addiu	$2,$2,-2
lw	$4,2856($fp)
sll	$5,$5,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$13,$13,-2
sw	$12,148($sp)
sw	$2,20($sp)
addiu	$5,$5,-2
li	$2,21			# 0x15
sw	$8,28($sp)
addu	$5,$7,$5
sw	$19,32($sp)
li	$7,21			# 0x15
sw	$13,24($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,16($sp)

lw	$28,40($sp)
lw	$6,11872($fp)
lw	$7,2856($fp)
lw	$4,%got(mpFrame)($28)
sll	$2,$6,1
addiu	$2,$2,2
lh	$4,0($4)
addu	$7,$7,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L294
lw	$12,148($sp)

li	$10,1			# 0x1
$L187:
lw	$3,72($sp)
$L317:
sll	$12,$12,2
lw	$4,52($sp)
move	$5,$7
addu	$12,$3,$12
lw	$25,0($12)
sw	$7,132($sp)
sw	$10,128($sp)
jalr	$25
sw	$12,148($sp)

lw	$8,80($sp)
lw	$28,40($sp)
lw	$7,132($sp)
lw	$10,128($sp)
beq	$8,$0,$L295
lw	$12,148($sp)

$L188:
li	$6,65536			# 0x10000
addu	$2,$fp,$6
lw	$2,-5248($2)
bne	$2,$0,$L189
lw	$25,56($sp)

sra	$9,$16,3
$L190:
lw	$7,164($fp)
lw	$8,56($sp)
lw	$25,68($sp)
addiu	$7,$7,2
lw	$11,196($fp)
addu	$21,$21,$8
lw	$6,11876($fp)
subu	$20,$20,$25
sll	$7,$7,3
sll	$5,$21,4
sll	$16,$20,4
mul	$3,$7,$11
subu	$21,$5,$21
subu	$16,$16,$20
sll	$4,$11,4
addu	$16,$21,$16
mul	$2,$6,$9
li	$7,65536			# 0x10000
addiu	$4,$4,48
addu	$16,$fp,$16
sra	$21,$22,3
addu	$16,$7,$16
addu	$4,$3,$4
addu	$5,$2,$21
addu	$4,$4,$21
lw	$7,6652($16)
addu	$2,$4,$2
addu	$5,$7,$5
bne	$10,$0,$L296
addu	$16,$7,$2

lw	$3,64($sp)
lw	$4,92($sp)
lw	$7,60($sp)
lw	$25,48($sp)
sw	$3,16($sp)
jalr	$25
sw	$23,20($sp)

$L219:
lw	$3,64($sp)
sll	$18,$18,1
lw	$6,11876($fp)
move	$5,$16
lw	$4,88($sp)
addu	$17,$18,$17
lw	$7,60($sp)
li	$19,65536			# 0x10000
lw	$25,48($sp)
sw	$3,16($sp)
addu	$18,$fp,$19
jalr	$25
sw	$23,20($sp)

sll	$5,$17,2
addiu	$2,$17,15076
lb	$6,-5230($18)
addu	$5,$fp,$5
lw	$4,52($sp)
sll	$2,$2,2
lw	$25,84($sp)
addu	$5,$19,$5
addu	$2,$fp,$2
lh	$8,-5226($5)
lh	$7,4($2)
lw	$5,11872($fp)
jalr	$25
sw	$8,16($sp)

lbu	$2,-5231($18)
beq	$2,$0,$L315
lw	$31,188($sp)

sll	$16,$17,3
lb	$6,-5229($18)
addiu	$17,$17,7586
lw	$5,11876($fp)
addu	$16,$fp,$16
lw	$4,92($sp)
sll	$17,$17,3
lw	$25,104($sp)
addu	$16,$19,$16
addu	$17,$fp,$17
lh	$2,-4842($16)
lh	$7,4($17)
jalr	$25
sw	$2,16($sp)

lh	$2,-4838($16)
lh	$7,-4840($16)
lw	$5,11876($fp)
lb	$6,-5229($18)
lw	$28,40($sp)
lw	$4,88($sp)
lw	$25,104($sp)
lw	$31,188($sp)
lw	$fp,184($sp)
lw	$23,180($sp)
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$17,156($sp)
lw	$16,152($sp)
sw	$2,208($sp)
jr	$25
addiu	$sp,$sp,192

$L292:
bne	$7,$0,$L297
nop

$L161:
lw	$2,7996($fp)
lw	$20,11876($fp)
$L313:
li	$21,65536			# 0x10000
lw	$6,11872($fp)
sll	$10,$4,1
addu	$21,$fp,$21
lw	$12,7992($fp)
mul	$3,$4,$20
sll	$18,$5,1
lw	$11,-5248($21)
sra	$16,$2,$11
addu	$20,$3,$5
mul	$3,$10,$6
sll	$16,$16,3
sll	$10,$12,3
addu	$19,$19,$20
addu	$10,$5,$10
addu	$16,$4,$16
sw	$19,64($sp)
addu	$20,$8,$20
addu	$18,$3,$18
sw	$10,52($sp)
sw	$16,56($sp)
beq	$7,$0,$L195
addu	$18,$9,$18

lw	$10,%got(mpFrame)($28)
li	$4,-3			# 0xfffffffffffffffd
lh	$5,0($10)
beq	$5,$4,$L298
nop

$L196:
lw	$25,52($sp)
$L325:
lw	$2,%got(scan8)($28)
$L326:
lw	$7,164($fp)
sll	$8,$25,3
lw	$25,76($sp)
addiu	$2,$2,%lo(scan8)
lw	$3,56($sp)
sll	$7,$7,4
lw	$14,160($fp)
addu	$2,$25,$2
lw	$4,11880($fp)
sra	$7,$7,$11
lw	$5,11884($fp)
sll	$9,$3,3
lbu	$2,0($2)
sll	$14,$14,4
sw	$7,96($sp)
addiu	$12,$4,-3
addiu	$13,$5,-3
addu	$11,$fp,$2
sw	$14,84($sp)
addiu	$7,$2,2812
sll	$2,$2,2
lb	$11,11568($11)
sll	$7,$7,2
addu	$2,$fp,$2
addu	$7,$fp,$7
sll	$3,$11,3
sll	$11,$11,5
lh	$21,11250($2)
sw	$3,88($sp)
sw	$11,92($sp)
addu	$19,$21,$9
lw	$25,92($sp)
lh	$11,0($7)
sra	$14,$19,2
andi	$21,$19,0x7
addu	$7,$3,$25
movn	$5,$13,$21
addu	$16,$11,$8
sll	$2,$7,4
andi	$3,$16,0x7
subu	$2,$2,$7
movn	$4,$12,$3
li	$11,65536			# 0x10000
sw	$3,68($sp)
mul	$3,$14,$6
addu	$2,$fp,$2
sra	$7,$16,2
addu	$2,$11,$2
subu	$15,$0,$4
andi	$11,$19,0x3
lw	$24,6648($2)
sll	$2,$11,2
addu	$12,$3,$7
andi	$11,$16,0x3
slt	$15,$7,$15
addu	$11,$11,$2
bne	$15,$0,$L199
addu	$12,$24,$12

subu	$2,$0,$5
slt	$2,$14,$2
bne	$2,$0,$L199
lw	$8,84($sp)

addiu	$2,$7,15
addu	$4,$8,$4
slt	$4,$2,$4
beq	$4,$0,$L199
lw	$25,96($sp)

addiu	$2,$14,15
addu	$5,$25,$5
slt	$5,$2,$5
bne	$5,$0,$L240
nop

$L199:
subu	$5,$0,$6
lw	$3,84($sp)
addiu	$7,$7,-2
lw	$8,96($sp)
sll	$5,$5,1
lw	$4,2856($fp)
li	$2,21			# 0x15
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$14,$14,-2
sw	$7,20($sp)
addiu	$5,$5,-2
sw	$10,128($sp)
li	$7,21			# 0x15
sw	$2,16($sp)
addu	$5,$12,$5
sw	$11,140($sp)
sw	$3,28($sp)
sw	$8,32($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$14,24($sp)

lw	$6,11872($fp)
lw	$10,128($sp)
lw	$12,2856($fp)
sll	$2,$6,1
lw	$28,40($sp)
addiu	$2,$2,2
lh	$4,0($10)
addu	$12,$12,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L299
lw	$11,140($sp)

li	$7,1			# 0x1
$L200:
lw	$3,72($sp)
$L318:
sll	$2,$11,2
$L329:
move	$5,$12
move	$4,$18
addu	$2,$3,$2
lw	$25,0($2)
sw	$2,144($sp)
sw	$7,132($sp)
jalr	$25
sw	$12,148($sp)

lw	$8,80($sp)
lw	$28,40($sp)
lw	$2,144($sp)
lw	$7,132($sp)
beq	$8,$0,$L300
lw	$12,148($sp)

$L201:
li	$4,65536			# 0x10000
addu	$2,$fp,$4
lw	$2,-5248($2)
bne	$2,$0,$L202
lw	$8,88($sp)

sra	$9,$19,3
$L203:
lw	$5,164($fp)
lw	$8,88($sp)
lw	$25,92($sp)
addiu	$5,$5,2
lw	$12,196($fp)
lw	$6,11876($fp)
sll	$5,$5,3
addu	$10,$8,$25
mul	$3,$5,$12
sll	$11,$10,4
sll	$4,$12,4
subu	$10,$11,$10
mul	$2,$6,$9
li	$5,65536			# 0x10000
addiu	$4,$4,48
addu	$10,$fp,$10
sra	$8,$16,3
addu	$10,$5,$10
addu	$4,$3,$4
addu	$4,$4,$8
lw	$5,6652($10)
addu	$10,$2,$8
addu	$2,$4,$2
addu	$10,$5,$10
bne	$7,$0,$L301
addu	$2,$5,$2

lw	$3,68($sp)
move	$5,$10
lw	$4,64($sp)
lw	$7,60($sp)
lw	$25,48($sp)
sw	$2,144($sp)
sw	$3,16($sp)
jalr	$25
sw	$21,20($sp)

lw	$2,144($sp)
$L220:
lw	$3,68($sp)
move	$4,$20
lw	$6,11876($fp)
move	$5,$2
lw	$7,60($sp)
lw	$25,48($sp)
sw	$3,16($sp)
jalr	$25
sw	$21,20($sp)

lw	$28,40($sp)
sw	$22,72($sp)
sw	$23,48($sp)
$L195:
beq	$17,$0,$L159
lw	$10,%got(mpFrame)($28)

li	$2,-3			# 0xfffffffffffffffd
lh	$4,0($10)
beq	$4,$2,$L302
nop

$L207:
lw	$25,52($sp)
$L319:
li	$7,65536			# 0x10000
$L320:
lw	$2,%got(scan8)($28)
$L321:
lw	$17,164($fp)
addu	$7,$fp,$7
sll	$8,$25,3
lw	$25,76($sp)
addiu	$2,$2,%lo(scan8)
lw	$3,56($sp)
sll	$12,$17,4
lw	$17,-5248($7)
addu	$2,$25,$2
lw	$23,160($fp)
sll	$16,$3,3
lw	$4,11880($fp)
sra	$17,$12,$17
lw	$6,11872($fp)
lbu	$2,0($2)
sll	$19,$23,4
addiu	$11,$4,-3
lw	$5,11884($fp)
sw	$17,56($sp)
addu	$9,$fp,$2
addiu	$7,$2,2852
sll	$2,$2,2
lb	$22,11608($9)
sll	$7,$7,2
addu	$2,$fp,$2
addu	$7,$fp,$7
sll	$21,$22,3
lh	$9,11410($2)
sll	$22,$22,5
lh	$14,0($7)
addiu	$13,$5,-3
addu	$7,$21,$22
addu	$23,$9,$16
sll	$2,$7,4
addu	$17,$14,$8
sra	$14,$23,2
subu	$2,$2,$7
andi	$16,$17,0x7
mul	$3,$14,$6
movn	$4,$11,$16
addu	$2,$fp,$2
li	$11,131072			# 0x20000
sra	$7,$17,2
addu	$2,$11,$2
subu	$15,$0,$4
andi	$11,$23,0x3
lw	$24,-30088($2)
andi	$8,$23,0x7
sll	$2,$11,2
movn	$5,$13,$8
addu	$12,$3,$7
andi	$11,$17,0x3
sw	$8,52($sp)
slt	$15,$7,$15
addu	$11,$11,$2
bne	$15,$0,$L210
addu	$12,$24,$12

subu	$2,$0,$5
slt	$2,$14,$2
bne	$2,$0,$L210
addiu	$2,$7,15

addu	$4,$19,$4
slt	$4,$2,$4
beq	$4,$0,$L210
lw	$25,56($sp)

addiu	$2,$14,15
addu	$5,$25,$5
slt	$5,$2,$5
bne	$5,$0,$L246
nop

$L210:
subu	$5,$0,$6
lw	$3,56($sp)
addiu	$7,$7,-2
lw	$4,2856($fp)
sll	$5,$5,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
li	$2,21			# 0x15
sw	$10,128($sp)
addiu	$14,$14,-2
sw	$7,20($sp)
addiu	$5,$5,-2
sw	$11,140($sp)
li	$7,21			# 0x15
sw	$2,16($sp)
addu	$5,$12,$5
sw	$19,28($sp)
sw	$3,32($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$14,24($sp)

lw	$6,11872($fp)
lw	$10,128($sp)
lw	$12,2856($fp)
sll	$2,$6,1
lw	$28,40($sp)
addiu	$2,$2,2
lh	$4,0($10)
addu	$12,$12,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L303
lw	$11,140($sp)

li	$7,1			# 0x1
$L211:
lw	$25,72($sp)
$L316:
sll	$2,$11,2
$L328:
move	$5,$12
move	$4,$18
addu	$2,$25,$2
lw	$25,0($2)
sw	$2,144($sp)
sw	$7,132($sp)
jalr	$25
sw	$12,148($sp)

lw	$3,80($sp)
lw	$28,40($sp)
lw	$2,144($sp)
lw	$7,132($sp)
beq	$3,$0,$L304
lw	$12,148($sp)

$L212:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$2,-5248($2)
bne	$2,$0,$L213
addu	$4,$21,$22

sra	$18,$23,3
$L214:
lw	$5,164($fp)
addu	$21,$21,$22
lw	$9,196($fp)
sra	$8,$17,3
sll	$22,$21,4
lw	$6,11876($fp)
addiu	$5,$5,2
sll	$4,$9,4
sll	$5,$5,3
subu	$21,$22,$21
mul	$3,$5,$9
mul	$2,$6,$18
addiu	$4,$4,48
addu	$21,$fp,$21
li	$22,131072			# 0x20000
addu	$21,$22,$21
addu	$4,$3,$4
addu	$9,$2,$8
addu	$4,$4,$8
lw	$5,-30084($21)
addu	$2,$4,$2
addu	$9,$5,$9
bne	$7,$0,$L305
addu	$21,$5,$2

lw	$3,52($sp)
move	$5,$9
lw	$4,64($sp)
lw	$7,60($sp)
lw	$25,48($sp)
sw	$16,16($sp)
jalr	$25
sw	$3,20($sp)

lw	$28,40($sp)
$L221:
lw	$3,52($sp)
move	$4,$20
lw	$6,11876($fp)
move	$5,$21
lw	$7,60($sp)
lw	$25,48($sp)
lw	$31,188($sp)
lw	$fp,184($sp)
lw	$23,180($sp)
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$17,156($sp)
sw	$16,208($sp)
sw	$3,212($sp)
lw	$16,152($sp)
jr	$25
addiu	$sp,$sp,192

$L159:
lw	$31,188($sp)
$L315:
lw	$fp,184($sp)
lw	$23,180($sp)
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$17,156($sp)
lw	$16,152($sp)
j	$31
addiu	$sp,$sp,192

$L213:
lw	$2,7996($fp)
sll	$5,$4,4
andi	$2,$2,0x1
subu	$4,$5,$4
li	$5,131072			# 0x20000
addu	$4,$fp,$4
addu	$4,$5,$4
lw	$3,-30016($4)
subu	$2,$2,$3
addiu	$2,$2,1
sll	$2,$2,1
addu	$3,$23,$2
sra	$18,$3,3
bltz	$18,$L251
lw	$25,56($sp)

addiu	$2,$18,8
sra	$4,$25,1
slt	$2,$2,$4
xori	$2,$2,0x1
$L215:
andi	$3,$3,0x7
or	$7,$7,$2
b	$L214
sw	$3,52($sp)

$L189:
lw	$3,68($sp)
lw	$23,7996($fp)
addu	$4,$21,$25
subu	$7,$20,$3
sll	$2,$4,4
sll	$11,$7,4
subu	$5,$2,$4
subu	$4,$11,$7
andi	$2,$23,0x1
addu	$4,$5,$4
addu	$4,$fp,$4
addu	$4,$6,$4
lw	$23,6720($4)
subu	$23,$2,$23
addiu	$23,$23,1
sll	$23,$23,1
addu	$23,$16,$23
sra	$9,$23,3
bltz	$9,$L239
addiu	$2,$9,8

sra	$4,$19,1
andi	$23,$23,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L190
or	$10,$10,$2

$L202:
lw	$25,92($sp)
lw	$21,7996($fp)
addu	$5,$8,$25
andi	$2,$21,0x1
sll	$6,$5,4
subu	$5,$6,$5
addu	$5,$fp,$5
addu	$4,$4,$5
lw	$21,6720($4)
subu	$21,$2,$21
addiu	$21,$21,1
sll	$21,$21,1
addu	$21,$19,$21
sra	$9,$21,3
bltz	$9,$L245
lw	$3,96($sp)

addiu	$2,$9,8
andi	$21,$21,0x7
sra	$4,$3,1
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L203
or	$7,$7,$2

$L297:
beq	$17,$0,$L161
lw	$6,%got(scan8)($28)

lw	$3,76($sp)
lw	$2,7996($fp)
addiu	$6,$6,%lo(scan8)
addu	$6,$3,$6
andi	$14,$2,0x1
lbu	$13,0($6)
addu	$12,$fp,$13
lb	$6,11568($12)
lb	$15,11608($12)
sll	$16,$6,4
sll	$12,$6,6
subu	$12,$12,$16
addu	$6,$12,$15
sll	$6,$6,1
addu	$6,$6,$14
addiu	$6,$6,30728
sll	$6,$6,1
addu	$6,$fp,$6
lh	$12,4($6)
li	$6,32			# 0x20
bne	$12,$6,$L162
nop

b	$L313
lw	$20,11876($fp)

$L303:
lw	$2,7992($fp)
bne	$2,$0,$L211
li	$7,1			# 0x1

lw	$4,7996($fp)
li	$2,15			# 0xf
bne	$4,$2,$L316
lw	$25,72($sp)

lw	$8,76($sp)
li	$2,4			# 0x4
bne	$8,$2,$L328
sll	$2,$11,2

lw	$4,%got($LC4)($28)
lw	$25,%call16(printf)($28)
sw	$7,132($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$12,148($sp)

lw	$6,11872($fp)
lw	$7,132($sp)
lw	$11,140($sp)
b	$L211
lw	$12,148($sp)

$L294:
lw	$2,7992($fp)
bne	$2,$0,$L187
li	$10,1			# 0x1

lw	$4,7996($fp)
li	$2,15			# 0xf
bne	$4,$2,$L317
lw	$3,72($sp)

lw	$25,76($sp)
li	$2,4			# 0x4
bne	$25,$2,$L317
lw	$4,%got($LC4)($28)

lw	$25,%call16(printf)($28)
sw	$7,132($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$10,128($sp)

lw	$6,11872($fp)
lw	$7,132($sp)
lw	$10,128($sp)
b	$L187
lw	$12,148($sp)

$L299:
lw	$2,7992($fp)
bne	$2,$0,$L200
li	$7,1			# 0x1

lw	$4,7996($fp)
li	$2,15			# 0xf
bne	$4,$2,$L318
lw	$3,72($sp)

lw	$25,76($sp)
li	$2,4			# 0x4
bne	$25,$2,$L329
sll	$2,$11,2

lw	$4,%got($LC4)($28)
lw	$25,%call16(printf)($28)
sw	$7,132($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$12,148($sp)

lw	$6,11872($fp)
lw	$7,132($sp)
lw	$11,140($sp)
b	$L200
lw	$12,148($sp)

$L304:
lw	$8,100($sp)
lw	$25,0($2)
lw	$6,11872($fp)
addu	$4,$18,$8
jalr	$25
addu	$5,$12,$8

lw	$28,40($sp)
b	$L212
lw	$7,132($sp)

$L305:
lw	$25,56($sp)
sra	$23,$19,1
li	$22,9			# 0x9
lw	$4,2856($fp)
li	$7,9			# 0x9
sw	$8,20($sp)
sra	$17,$25,1
sw	$8,136($sp)
sw	$18,24($sp)
move	$5,$9
sw	$23,28($sp)
sw	$22,16($sp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$17,32($sp)

lw	$3,52($sp)
lw	$5,2856($fp)
lw	$6,11876($fp)
lw	$4,64($sp)
lw	$7,60($sp)
lw	$25,48($sp)
sw	$3,20($sp)
jalr	$25
sw	$16,16($sp)

li	$7,9			# 0x9
lw	$28,40($sp)
move	$5,$21
lw	$8,136($sp)
lw	$4,2856($fp)
lw	$6,11876($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$22,16($sp)
sw	$8,20($sp)
sw	$18,24($sp)
sw	$23,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$17,32($sp)

lw	$28,40($sp)
b	$L221
lw	$21,2856($fp)

$L302:
lw	$2,7992($fp)
bne	$2,$0,$L319
lw	$25,52($sp)

lw	$4,7996($fp)
li	$2,15			# 0xf
bne	$4,$2,$L320
li	$7,65536			# 0x10000

lw	$3,76($sp)
li	$2,4			# 0x4
bne	$3,$2,$L321
lw	$2,%got(scan8)($28)

lw	$4,%got($LC6)($28)
lw	$25,%call16(printf)($28)
sw	$10,128($sp)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC6)

lw	$28,40($sp)
b	$L207
lw	$10,128($sp)

$L295:
lw	$8,100($sp)
lw	$3,52($sp)
lw	$25,0($12)
lw	$6,11872($fp)
addu	$5,$7,$8
jalr	$25
addu	$4,$3,$8

lw	$28,40($sp)
b	$L188
lw	$10,128($sp)

$L296:
lw	$8,96($sp)
sra	$19,$19,1
li	$20,9			# 0x9
lw	$4,2856($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
li	$7,9			# 0x9
sra	$22,$8,1
sw	$9,24($sp)
sw	$9,132($sp)
sw	$21,20($sp)
sw	$22,28($sp)
sw	$20,16($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$19,32($sp)

lw	$25,64($sp)
lw	$5,2856($fp)
lw	$6,11876($fp)
lw	$4,92($sp)
lw	$7,60($sp)
sw	$25,16($sp)
sw	$23,20($sp)
lw	$25,48($sp)
jalr	$25
nop

li	$7,9			# 0x9
lw	$28,40($sp)
move	$5,$16
lw	$9,132($sp)
lw	$4,2856($fp)
lw	$6,11876($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$20,16($sp)
sw	$21,20($sp)
sw	$9,24($sp)
sw	$22,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$19,32($sp)

b	$L219
lw	$16,2856($fp)

$L300:
lw	$3,100($sp)
lw	$25,0($2)
lw	$6,11872($fp)
addu	$4,$18,$3
jalr	$25
addu	$5,$12,$3

lw	$28,40($sp)
b	$L201
lw	$7,132($sp)

$L301:
lw	$3,96($sp)
li	$13,9			# 0x9
lw	$25,84($sp)
li	$7,9			# 0x9
lw	$4,2856($fp)
move	$5,$10
sra	$11,$3,1
sw	$8,20($sp)
sra	$12,$25,1
sw	$9,24($sp)
sw	$13,16($sp)
sw	$11,32($sp)
sw	$12,28($sp)
sw	$2,144($sp)
sw	$8,136($sp)
sw	$9,132($sp)
sw	$11,140($sp)
sw	$12,148($sp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$13,128($sp)

lw	$25,68($sp)
lw	$5,2856($fp)
lw	$6,11876($fp)
lw	$4,64($sp)
lw	$7,60($sp)
sw	$25,16($sp)
sw	$21,20($sp)
lw	$25,48($sp)
jalr	$25
nop

li	$7,9			# 0x9
lw	$28,40($sp)
lw	$13,128($sp)
lw	$8,136($sp)
lw	$9,132($sp)
lw	$12,148($sp)
lw	$11,140($sp)
lw	$2,144($sp)
lw	$4,2856($fp)
lw	$6,11876($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
move	$5,$2
sw	$13,16($sp)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$12,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$11,32($sp)

b	$L220
lw	$2,2856($fp)

$L293:
lw	$3,2864($fp)
sll	$13,$13,2
lw	$12,11880($fp)
sll	$4,$4,3
lw	$24,11884($fp)
lb	$23,11568($2)
addu	$18,$fp,$13
sw	$3,56($sp)
sll	$5,$5,3
sw	$4,120($sp)
sll	$14,$14,3
sll	$8,$23,3
lh	$9,11250($18)
sll	$25,$23,5
lb	$2,11608($2)
sw	$13,124($sp)
sw	$8,68($sp)
lw	$3,68($sp)
lh	$8,11248($18)
sw	$5,108($sp)
addiu	$5,$12,-3
addu	$4,$3,$25
lw	$3,120($sp)
sw	$25,84($sp)
sw	$2,96($sp)
sll	$2,$4,4
addu	$17,$9,$3
lw	$25,108($sp)
subu	$4,$2,$4
lw	$19,164($fp)
sra	$13,$17,2
addu	$21,$8,$25
lw	$8,56($sp)
mul	$3,$6,$13
addu	$4,$fp,$4
sra	$2,$21,2
addiu	$25,$24,-3
andi	$20,$21,0x7
movn	$12,$5,$20
andi	$16,$17,0x7
addu	$15,$15,$4
addu	$7,$3,$2
movn	$24,$25,$16
andi	$10,$17,0x3
lw	$25,56($sp)
subu	$4,$0,$12
lw	$3,160($fp)
addiu	$8,$8,8
lw	$15,6648($15)
sll	$5,$10,2
addu	$14,$25,$14
sll	$3,$3,4
sw	$8,116($sp)
sll	$19,$19,4
andi	$10,$21,0x3
sw	$14,104($sp)
slt	$4,$2,$4
sw	$3,112($sp)
addu	$10,$10,$5
addu	$7,$15,$7
bne	$4,$0,$L167
sra	$19,$19,$11

subu	$4,$0,$24
slt	$4,$13,$4
bne	$4,$0,$L167
addiu	$4,$2,15

addu	$12,$3,$12
slt	$12,$4,$12
beq	$12,$0,$L322
subu	$5,$0,$6

addiu	$4,$13,15
addu	$24,$19,$24
slt	$24,$4,$24
beq	$24,$0,$L323
lw	$8,112($sp)

b	$L168
move	$11,$0

$L167:
subu	$5,$0,$6
$L322:
lw	$8,112($sp)
$L323:
addiu	$2,$2,-2
lw	$4,2856($fp)
sll	$5,$5,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$13,$13,-2
sw	$10,128($sp)
sw	$2,20($sp)
addiu	$5,$5,-2
li	$2,21			# 0x15
sw	$8,28($sp)
addu	$5,$7,$5
sw	$19,32($sp)
li	$7,21			# 0x15
sw	$13,24($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,16($sp)

lw	$28,40($sp)
lw	$6,11872($fp)
lw	$7,2856($fp)
lw	$4,%got(mpFrame)($28)
sll	$2,$6,1
addiu	$2,$2,2
lh	$4,0($4)
addu	$7,$7,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L306
lw	$10,128($sp)

li	$11,1			# 0x1
$L168:
lw	$3,72($sp)
$L327:
sll	$10,$10,2
lw	$4,52($sp)
move	$5,$7
addu	$10,$3,$10
lw	$25,0($10)
sw	$7,132($sp)
sw	$10,128($sp)
jalr	$25
sw	$11,140($sp)

lw	$8,80($sp)
lw	$28,40($sp)
lw	$7,132($sp)
lw	$10,128($sp)
beq	$8,$0,$L307
lw	$11,140($sp)

$L169:
li	$4,65536			# 0x10000
addu	$2,$fp,$4
lw	$2,-5248($2)
bne	$2,$0,$L170
lw	$25,68($sp)

sra	$9,$17,3
$L171:
lw	$5,164($fp)
lw	$8,68($sp)
lw	$25,84($sp)
addiu	$5,$5,2
lw	$10,196($fp)
lw	$6,11876($fp)
sll	$5,$5,3
addu	$17,$8,$25
mul	$3,$5,$10
sll	$7,$17,4
sll	$4,$10,4
subu	$17,$7,$17
mul	$2,$6,$9
li	$5,65536			# 0x10000
addiu	$4,$4,48
addu	$17,$fp,$17
sra	$8,$21,3
addu	$4,$3,$4
addu	$17,$5,$17
addu	$4,$4,$8
addu	$10,$2,$8
lw	$5,6652($17)
addu	$2,$4,$2
addu	$10,$5,$10
bne	$11,$0,$L308
addu	$2,$5,$2

lw	$4,92($sp)
move	$5,$10
lw	$7,60($sp)
lw	$25,48($sp)
sw	$2,144($sp)
sw	$20,16($sp)
jalr	$25
sw	$16,20($sp)

lw	$2,144($sp)
$L217:
lw	$3,96($sp)
move	$5,$2
lw	$6,11876($fp)
lw	$4,88($sp)
sll	$8,$3,5
lw	$7,60($sp)
lw	$25,48($sp)
sll	$17,$3,3
sw	$20,16($sp)
sw	$8,84($sp)
jalr	$25
sw	$16,20($sp)

li	$14,131072			# 0x20000
lw	$8,124($sp)
li	$4,65536			# 0x10000
lw	$3,84($sp)
lw	$25,108($sp)
addu	$4,$fp,$4
addu	$13,$fp,$8
lh	$8,11408($18)
addu	$7,$17,$3
lw	$3,120($sp)
lw	$12,11880($fp)
lh	$9,11410($13)
sll	$2,$7,4
lw	$6,11872($fp)
addu	$21,$25,$8
subu	$2,$2,$7
lw	$5,11884($fp)
addu	$19,$3,$9
lw	$3,160($fp)
addu	$2,$fp,$2
lw	$20,164($fp)
sra	$13,$19,2
lw	$15,-5248($4)
addu	$14,$14,$2
lw	$28,40($sp)
addiu	$7,$12,-3
mul	$2,$13,$6
andi	$18,$21,0x7
lw	$24,-30088($14)
movn	$12,$7,$18
sra	$10,$21,2
andi	$11,$19,0x3
sll	$3,$3,4
sll	$14,$20,4
subu	$4,$0,$12
addu	$7,$2,$10
sw	$3,108($sp)
sll	$2,$11,2
addiu	$20,$5,-3
andi	$11,$21,0x3
andi	$16,$19,0x7
slt	$4,$10,$4
movn	$5,$20,$16
addu	$11,$11,$2
addu	$7,$24,$7
bne	$4,$0,$L176
sra	$20,$14,$15

subu	$2,$0,$5
slt	$2,$13,$2
bne	$2,$0,$L176
addiu	$2,$10,15

addu	$12,$3,$12
slt	$12,$2,$12
beq	$12,$0,$L176
addiu	$2,$13,15

addu	$5,$20,$5
slt	$5,$2,$5
beq	$5,$0,$L324
subu	$5,$0,$6

move	$3,$0
$L177:
lw	$25,72($sp)
$L330:
sll	$2,$11,2
lw	$4,104($sp)
move	$5,$7
addu	$2,$25,$2
lw	$25,0($2)
sw	$2,144($sp)
sw	$3,128($sp)
jalr	$25
sw	$7,132($sp)

lw	$8,80($sp)
lw	$28,40($sp)
lw	$2,144($sp)
lw	$3,128($sp)
beq	$8,$0,$L309
lw	$7,132($sp)

$L178:
li	$2,65536			# 0x10000
addu	$2,$fp,$2
lw	$2,-5248($2)
bne	$2,$0,$L179
lw	$8,84($sp)

sra	$9,$19,3
$L180:
lw	$25,84($sp)
sra	$8,$21,3
lw	$7,164($fp)
lw	$11,196($fp)
addu	$19,$17,$25
lw	$6,11876($fp)
addiu	$7,$7,2
sll	$5,$19,4
sll	$7,$7,3
subu	$19,$5,$19
mul	$5,$7,$11
sll	$4,$11,4
mul	$2,$6,$9
addiu	$4,$4,48
addu	$19,$fp,$19
addu	$4,$5,$4
li	$5,131072			# 0x20000
addu	$4,$4,$8
addu	$19,$5,$19
addu	$11,$2,$8
addu	$2,$4,$2
lw	$5,-30084($19)
addu	$11,$5,$11
bne	$3,$0,$L310
addu	$2,$5,$2

lw	$4,56($sp)
move	$5,$11
lw	$7,60($sp)
lw	$25,48($sp)
sw	$2,144($sp)
sw	$18,16($sp)
jalr	$25
sw	$16,20($sp)

lw	$2,144($sp)
$L218:
li	$19,65536			# 0x10000
lw	$6,11876($fp)
lw	$4,116($sp)
move	$5,$2
lw	$7,60($sp)
lw	$25,48($sp)
sw	$18,16($sp)
addu	$18,$fp,$19
jalr	$25
sw	$16,20($sp)

li	$2,2			# 0x2
lbu	$4,-5232($18)
beq	$4,$2,$L311
addiu	$2,$23,7538

lw	$3,68($sp)
addu	$17,$fp,$17
lb	$7,-5230($18)
sll	$2,$2,3
lw	$6,11872($fp)
addu	$17,$19,$17
lw	$4,52($sp)
addu	$2,$fp,$2
lw	$25,64($sp)
addu	$5,$fp,$3
lh	$8,4($2)
addu	$2,$19,$5
lw	$5,104($sp)
sw	$8,16($sp)
lh	$8,-5224($17)
sw	$8,20($sp)
lh	$8,-5222($17)
lh	$2,-5226($2)
addu	$2,$2,$8
jalr	$25
sw	$2,24($sp)

addiu	$2,$23,3793
lw	$3,96($sp)
sll	$23,$23,4
sll	$2,$2,4
lw	$6,11876($fp)
lb	$7,-5229($18)
addu	$23,$fp,$23
addu	$2,$fp,$2
lw	$4,92($sp)
sll	$16,$3,4
lw	$5,56($sp)
move	$25,$22
lh	$2,4($2)
addu	$16,$fp,$16
addu	$16,$19,$16
addu	$19,$19,$23
sw	$2,16($sp)
lh	$2,-4836($16)
sw	$2,20($sp)
lh	$2,-4842($19)
lh	$8,-4834($16)
addu	$2,$2,$8
jalr	$25
sw	$2,24($sp)

lh	$2,-4840($19)
lw	$6,11876($fp)
lb	$7,-5229($18)
lw	$28,40($sp)
sw	$2,208($sp)
lh	$2,-4832($16)
lw	$4,88($sp)
lw	$5,116($sp)
sw	$2,212($sp)
lh	$2,-4838($19)
lh	$3,-4830($16)
addu	$2,$2,$3
sw	$2,216($sp)
$L291:
lw	$31,188($sp)
move	$25,$22
lw	$fp,184($sp)
lw	$23,180($sp)
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$17,156($sp)
lw	$16,152($sp)
jr	$25
addiu	$sp,$sp,192

$L298:
bne	$12,$0,$L325
lw	$25,52($sp)

li	$4,15			# 0xf
bne	$2,$4,$L326
lw	$2,%got(scan8)($28)

lw	$8,76($sp)
li	$2,4			# 0x4
bne	$8,$2,$L326
lw	$2,%got(scan8)($28)

lw	$4,%got($LC5)($28)
lw	$25,%call16(printf)($28)
sw	$10,128($sp)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC5)

lw	$28,40($sp)
lw	$6,11872($fp)
lw	$11,-5248($21)
b	$L196
lw	$10,128($sp)

$L170:
lw	$3,84($sp)
lw	$16,7996($fp)
addu	$5,$25,$3
andi	$2,$16,0x1
sll	$6,$5,4
subu	$5,$6,$5
addu	$5,$fp,$5
addu	$4,$4,$5
lw	$16,6720($4)
subu	$16,$2,$16
addiu	$16,$16,1
sll	$16,$16,1
addu	$16,$17,$16
sra	$9,$16,3
bltz	$9,$L227
sra	$4,$19,1

addiu	$2,$9,8
andi	$16,$16,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L171
or	$11,$11,$2

$L246:
b	$L211
move	$7,$0

$L240:
b	$L200
move	$7,$0

$L306:
lw	$2,7992($fp)
bne	$2,$0,$L168
li	$11,1			# 0x1

lw	$4,7996($fp)
li	$2,15			# 0xf
bne	$4,$2,$L327
lw	$3,72($sp)

lw	$25,76($sp)
li	$2,4			# 0x4
bne	$25,$2,$L327
lw	$4,%got($LC4)($28)

lw	$25,%call16(printf)($28)
sw	$7,132($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$11,140($sp)

lw	$6,11872($fp)
lw	$7,132($sp)
lw	$10,128($sp)
b	$L168
lw	$11,140($sp)

$L176:
subu	$5,$0,$6
$L324:
lw	$3,108($sp)
li	$2,21			# 0x15
lw	$4,2856($fp)
sll	$5,$5,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$10,$10,-2
sw	$11,140($sp)
addiu	$13,$13,-2
sw	$2,16($sp)
addiu	$5,$5,-2
sw	$3,28($sp)
sw	$20,32($sp)
addu	$5,$7,$5
sw	$10,20($sp)
li	$7,21			# 0x15
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$13,24($sp)

lw	$28,40($sp)
lw	$6,11872($fp)
lw	$7,2856($fp)
lw	$4,%got(mpFrame)($28)
sll	$2,$6,1
addiu	$2,$2,2
lh	$4,0($4)
addu	$7,$7,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L312
lw	$11,140($sp)

b	$L177
li	$3,1			# 0x1

$L312:
lw	$2,7992($fp)
bne	$2,$0,$L177
li	$3,1			# 0x1

lw	$4,7996($fp)
li	$2,15			# 0xf
bne	$4,$2,$L177
lw	$8,76($sp)

li	$2,4			# 0x4
bne	$8,$2,$L330
lw	$25,72($sp)

lw	$4,%got($LC4)($28)
lw	$25,%call16(printf)($28)
sw	$3,128($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$7,132($sp)

lw	$6,11872($fp)
lw	$3,128($sp)
lw	$7,132($sp)
b	$L177
lw	$11,140($sp)

$L251:
b	$L215
li	$2,1			# 0x1

$L239:
li	$2,1			# 0x1
andi	$23,$23,0x7
b	$L190
or	$10,$10,$2

$L179:
lw	$16,7996($fp)
addu	$4,$17,$8
andi	$2,$16,0x1
sll	$5,$4,4
subu	$4,$5,$4
li	$5,131072			# 0x20000
addu	$4,$fp,$4
addu	$4,$5,$4
lw	$16,-30016($4)
subu	$16,$2,$16
addiu	$16,$16,1
sll	$16,$16,1
addu	$16,$19,$16
sra	$9,$16,3
bltz	$9,$L233
sra	$4,$20,1

addiu	$2,$9,8
andi	$16,$16,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L180
or	$3,$3,$2

$L245:
li	$2,1			# 0x1
andi	$21,$21,0x7
b	$L203
or	$7,$7,$2

$L307:
lw	$8,100($sp)
lw	$3,52($sp)
lw	$25,0($10)
lw	$6,11872($fp)
addu	$5,$7,$8
jalr	$25
addu	$4,$3,$8

lw	$28,40($sp)
b	$L169
lw	$11,140($sp)

$L310:
lw	$25,108($sp)
sra	$20,$20,1
li	$19,9			# 0x9
lw	$4,2856($fp)
li	$7,9			# 0x9
sw	$8,20($sp)
sra	$21,$25,1
sw	$9,24($sp)
sw	$2,144($sp)
move	$5,$11
sw	$8,136($sp)
sw	$9,132($sp)
sw	$21,28($sp)
sw	$19,16($sp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$20,32($sp)

lw	$5,2856($fp)
lw	$6,11876($fp)
lw	$4,56($sp)
lw	$7,60($sp)
lw	$25,48($sp)
sw	$18,16($sp)
jalr	$25
sw	$16,20($sp)

li	$7,9			# 0x9
lw	$28,40($sp)
lw	$8,136($sp)
lw	$9,132($sp)
lw	$2,144($sp)
lw	$4,2856($fp)
lw	$6,11876($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
move	$5,$2
sw	$19,16($sp)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$21,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$20,32($sp)

b	$L218
lw	$2,2856($fp)

$L309:
lw	$25,0($2)
lw	$8,104($sp)
lw	$2,100($sp)
lw	$6,11872($fp)
addu	$4,$8,$2
jalr	$25
addu	$5,$7,$2

lw	$28,40($sp)
b	$L178
lw	$3,128($sp)

$L308:
lw	$25,112($sp)
li	$11,9			# 0x9
sra	$19,$19,1
lw	$4,2856($fp)
li	$7,9			# 0x9
sw	$8,20($sp)
sra	$17,$25,1
sw	$9,24($sp)
sw	$11,16($sp)
move	$5,$10
sw	$2,144($sp)
sw	$8,136($sp)
sw	$9,132($sp)
sw	$11,140($sp)
sw	$17,28($sp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$19,32($sp)

lw	$5,2856($fp)
lw	$6,11876($fp)
lw	$4,92($sp)
lw	$7,60($sp)
lw	$25,48($sp)
sw	$20,16($sp)
jalr	$25
sw	$16,20($sp)

li	$7,9			# 0x9
lw	$28,40($sp)
lw	$11,140($sp)
lw	$8,136($sp)
lw	$9,132($sp)
lw	$2,144($sp)
lw	$4,2856($fp)
lw	$6,11876($fp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
move	$5,$2
sw	$11,16($sp)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$17,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$19,32($sp)

b	$L217
lw	$2,2856($fp)

$L311:
sll	$2,$23,4
lw	$3,96($sp)
sll	$23,$23,6
lw	$5,7996($fp)
lw	$6,11872($fp)
li	$16,64			# 0x40
subu	$23,$23,$2
sw	$0,24($sp)
andi	$5,$5,0x1
lw	$4,52($sp)
addu	$2,$23,$3
lw	$25,64($sp)
li	$7,5			# 0x5
sll	$2,$2,1
addu	$2,$2,$5
lw	$5,104($sp)
addiu	$2,$2,30728
sll	$2,$2,1
addu	$2,$fp,$2
lh	$17,4($2)
subu	$16,$16,$17
sw	$17,16($sp)
jalr	$25
sw	$16,20($sp)

li	$7,5			# 0x5
lw	$6,11876($fp)
move	$25,$22
lw	$4,92($sp)
lw	$5,56($sp)
sw	$17,16($sp)
sw	$16,20($sp)
jalr	$25
sw	$0,24($sp)

li	$7,5			# 0x5
lw	$6,11876($fp)
lw	$28,40($sp)
lw	$4,88($sp)
lw	$5,116($sp)
sw	$17,208($sp)
sw	$16,212($sp)
b	$L291
sw	$0,216($sp)

$L227:
li	$2,1			# 0x1
andi	$16,$16,0x7
b	$L171
or	$11,$11,$2

$L233:
li	$2,1			# 0x1
andi	$16,$16,0x7
b	$L180
or	$3,$3,$2

.set	macro
.set	reorder
.end	mc_part
.size	mc_part, .-mc_part
.section	.text.hl_motion,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	hl_motion
.type	hl_motion, @function
hl_motion:
.frame	$sp,296,$31		# vars= 176, regs= 10/0, args= 72, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-296
sw	$fp,288($sp)
move	$fp,$4
li	$4,131072			# 0x20000
sw	$17,260($sp)
.cprestore	72
addu	$4,$fp,$4
sw	$31,292($sp)
sw	$23,284($sp)
sw	$22,280($sp)
sw	$21,276($sp)
sw	$20,272($sp)
sw	$19,268($sp)
sw	$18,264($sp)
sw	$16,256($sp)
lw	$8,316($sp)
lw	$9,320($sp)
lw	$4,9448($4)
lw	$3,312($sp)
lw	$2,2192($fp)
sll	$4,$4,2
lw	$25,324($sp)
sw	$8,124($sp)
sw	$9,184($sp)
addu	$2,$2,$4
sw	$3,180($sp)
sw	$5,132($sp)
sw	$6,136($sp)
sw	$7,140($sp)
sw	$25,128($sp)
lw	$8,328($sp)
lw	$9,332($sp)
lb	$3,11580($fp)
lw	$17,0($2)
sw	$8,160($sp)
bltz	$3,$L332
sw	$9,164($sp)

sll	$4,$3,3
lh	$16,11298($fp)
sll	$3,$3,5
lw	$8,7996($fp)
lw	$2,7992($fp)
addu	$3,$4,$3
lw	$4,11872($fp)
sll	$8,$8,4
lh	$7,11296($fp)
sll	$6,$3,4
lw	$5,192($fp)
sra	$16,$16,2
lw	$25,5744($fp)
subu	$3,$6,$3
li	$6,65536			# 0x10000
sll	$9,$2,4
andi	$2,$2,0x3
addu	$16,$16,$8
addiu	$6,$6,6640
sll	$2,$2,2
addu	$3,$3,$6
addu	$2,$16,$2
addu	$19,$fp,$3
mul	$3,$2,$4
sra	$7,$7,2
lw	$4,0($19)
li	$6,4			# 0x4
addu	$7,$7,$9
sra	$16,$16,1
addiu	$18,$7,8
addu	$2,$3,$18
addiu	$2,$2,64
jalr	$25
addu	$4,$4,$2

sra	$7,$18,1
lw	$3,7992($fp)
li	$6,2			# 0x2
lw	$2,196($fp)
lw	$4,4($19)
andi	$3,$3,0x7
lw	$5,8($19)
addu	$16,$16,$3
lw	$25,5744($fp)
mul	$3,$16,$2
subu	$5,$5,$4
addu	$2,$3,$7
addiu	$2,$2,64
jalr	$25
addu	$4,$4,$2

lw	$28,72($sp)
$L332:
andi	$2,$17,0x8
bne	$2,$0,$L459
andi	$2,$17,0x10

bne	$2,$0,$L460
andi	$2,$17,0x20

bne	$2,$0,$L461
li	$2,60296			# 0xeb88

sw	$0,112($sp)
sw	$0,96($sp)
addu	$2,$fp,$2
sw	$2,148($sp)
$L394:
lw	$8,96($sp)
lw	$3,148($sp)
andi	$4,$8,0x1
sll	$4,$4,2
lhu	$3,0($3)
andi	$2,$8,0x2
sll	$2,$2,1
sw	$4,116($sp)
andi	$4,$3,0x8
sw	$3,88($sp)
bne	$4,$0,$L462
sw	$2,120($sp)

lw	$8,88($sp)
andi	$2,$8,0x10
bne	$2,$0,$L463
lw	$3,88($sp)

andi	$2,$3,0x20
bne	$2,$0,$L464
move	$21,$0

lw	$9,180($sp)
move	$23,$fp
lw	$25,184($sp)
andi	$2,$3,0x4000
sw	$21,80($sp)
andi	$2,$2,0xffff
sltu	$8,$0,$2
sw	$2,192($sp)
andi	$2,$3,0x1000
sw	$8,168($sp)
sll	$5,$8,3
lw	$3,%got(scan8)($28)
sll	$4,$8,5
addiu	$9,$9,128
lw	$8,112($sp)
addu	$4,$5,$4
addiu	$3,$3,%lo(scan8)
sw	$9,172($sp)
addiu	$25,$25,128
addu	$3,$3,$8
sw	$4,204($sp)
addu	$9,$fp,$4
sw	$8,144($sp)
andi	$2,$2,0xffff
sw	$25,212($sp)
sw	$3,100($sp)
sw	$9,208($sp)
sw	$2,200($sp)
$L393:
lw	$25,124($sp)
li	$2,65536			# 0x10000
lw	$3,80($sp)
li	$4,2			# 0x2
addu	$2,$23,$2
lw	$8,128($sp)
lw	$9,116($sp)
lw	$25,8($25)
andi	$5,$3,0x1
lbu	$2,-5232($2)
sll	$5,$5,1
andi	$16,$3,0x2
lw	$fp,8($8)
addu	$5,$5,$9
sw	$25,84($sp)
lw	$25,120($sp)
beq	$2,$4,$L465
addu	$7,$16,$25

li	$4,1			# 0x1
bne	$2,$4,$L342
lw	$25,100($sp)

lw	$9,88($sp)
lw	$8,7996($23)
andi	$12,$9,0x5000
lbu	$9,0($25)
lw	$3,160($sp)
$L487:
sll	$13,$7,1
lw	$4,11876($23)
li	$10,65536			# 0x10000
lw	$25,160($sp)
sll	$15,$5,1
lw	$6,11872($23)
addu	$2,$23,$10
lw	$3,24($3)
lw	$11,7992($23)
lw	$25,36($25)
lw	$2,-5248($2)
sw	$3,196($sp)
mul	$3,$7,$4
sll	$11,$11,3
sw	$25,176($sp)
sra	$8,$8,$2
lw	$25,164($sp)
sll	$16,$8,3
lw	$8,140($sp)
addu	$7,$7,$16
addu	$14,$3,$5
lw	$25,24($25)
mul	$3,$13,$6
addu	$5,$5,$11
sw	$25,104($sp)
lw	$25,132($sp)
addu	$13,$3,$15
lw	$3,164($sp)
addu	$13,$25,$13
lw	$22,36($3)
lw	$3,136($sp)
sw	$13,92($sp)
addu	$3,$3,$14
addu	$14,$8,$14
li	$8,20480			# 0x5000
sw	$3,156($sp)
beq	$12,$8,$L466
sw	$14,152($sp)

lw	$3,208($sp)
sll	$7,$7,3
lw	$25,204($sp)
lw	$4,11880($23)
addu	$8,$3,$9
lw	$3,168($sp)
addu	$9,$25,$9
lw	$25,160($23)
lw	$12,11884($23)
sll	$3,$3,7
lb	$17,11568($8)
lw	$8,168($sp)
addiu	$13,$9,2812
sll	$9,$9,2
lw	$18,164($23)
sw	$3,104($sp)
sll	$13,$13,2
sll	$19,$8,11
lw	$8,104($sp)
sll	$3,$17,5
sll	$20,$17,3
addu	$13,$23,$13
subu	$14,$19,$8
sw	$3,108($sp)
addu	$15,$20,$3
addu	$8,$23,$9
lh	$3,0($13)
sll	$11,$15,4
sll	$13,$14,4
sll	$9,$5,3
lh	$8,11250($8)
subu	$11,$11,$15
subu	$5,$13,$14
addu	$fp,$3,$9
addu	$16,$8,$7
addu	$5,$11,$5
addiu	$11,$4,-3
andi	$21,$fp,0x7
movn	$4,$11,$21
sra	$11,$16,2
addu	$5,$23,$5
mul	$3,$6,$11
addu	$13,$10,$5
sra	$7,$fp,2
andi	$10,$16,0x3
lw	$14,6648($13)
sll	$25,$25,4
subu	$13,$0,$4
sll	$10,$10,2
addu	$5,$3,$7
sw	$25,188($sp)
sll	$18,$18,4
addiu	$24,$12,-3
andi	$15,$fp,0x3
andi	$22,$16,0x7
slt	$13,$7,$13
movn	$12,$24,$22
addu	$10,$15,$10
addu	$5,$14,$5
bne	$13,$0,$L366
sra	$18,$18,$2

subu	$2,$0,$12
slt	$2,$11,$2
bne	$2,$0,$L482
subu	$2,$0,$6

addiu	$2,$7,15
addu	$4,$25,$4
slt	$4,$2,$4
beq	$4,$0,$L482
subu	$2,$0,$6

addiu	$2,$11,15
addu	$12,$18,$12
slt	$12,$2,$12
beq	$12,$0,$L482
subu	$2,$0,$6

b	$L367
move	$11,$0

$L366:
subu	$2,$0,$6
$L482:
lw	$8,188($sp)
addiu	$7,$7,-2
lw	$4,2856($23)
sll	$2,$2,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$11,$11,-2
sw	$10,248($sp)
addiu	$2,$2,-2
sw	$7,20($sp)
li	$7,21			# 0x15
sw	$8,28($sp)
addu	$5,$5,$2
sw	$18,32($sp)
li	$2,21			# 0x15
sw	$11,24($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,16($sp)

lw	$28,72($sp)
lw	$6,11872($23)
lw	$5,2856($23)
lw	$4,%got(mpFrame)($28)
sll	$2,$6,1
addiu	$2,$2,2
lh	$4,0($4)
addu	$5,$5,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L467
lw	$10,248($sp)

li	$11,1			# 0x1
$L367:
lw	$25,172($sp)
$L483:
sll	$10,$10,2
lw	$4,92($sp)
addu	$10,$25,$10
lw	$25,0($10)
jalr	$25
sw	$11,240($sp)

li	$6,65536			# 0x10000
lw	$28,72($sp)
addu	$2,$23,$6
lw	$2,-5248($2)
bne	$2,$0,$L368
lw	$11,240($sp)

sra	$10,$16,3
$L369:
lw	$8,164($23)
lw	$9,108($sp)
lw	$25,104($sp)
addiu	$8,$8,2
lw	$7,196($23)
addu	$20,$20,$9
lw	$6,11876($23)
subu	$19,$19,$25
sll	$8,$8,3
sll	$5,$20,4
sll	$16,$19,4
mul	$3,$8,$7
subu	$20,$5,$20
subu	$16,$16,$19
sll	$4,$7,4
addu	$16,$20,$16
mul	$2,$6,$10
li	$7,65536			# 0x10000
addiu	$4,$4,48
addu	$16,$23,$16
sra	$19,$fp,3
addu	$16,$7,$16
addu	$4,$3,$4
addu	$5,$2,$19
addu	$4,$4,$19
lw	$7,6652($16)
addu	$2,$4,$2
addu	$5,$7,$5
bne	$11,$0,$L468
addu	$16,$7,$2

lw	$4,156($sp)
li	$7,2			# 0x2
lw	$25,84($sp)
sw	$21,16($sp)
jalr	$25
sw	$22,20($sp)

$L398:
lw	$6,11876($23)
li	$7,2			# 0x2
lw	$4,152($sp)
move	$5,$16
lw	$25,84($sp)
sll	$17,$17,1
sw	$21,16($sp)
li	$19,65536			# 0x10000
jalr	$25
sw	$22,20($sp)

addu	$16,$23,$19
lw	$3,168($sp)
lw	$4,92($sp)
lb	$6,-5230($16)
addu	$17,$17,$3
lw	$25,196($sp)
sll	$5,$17,2
addiu	$2,$17,15076
addu	$5,$23,$5
sll	$2,$2,2
addu	$5,$19,$5
addu	$2,$23,$2
lh	$8,-5226($5)
lh	$7,4($2)
lw	$5,11872($23)
jalr	$25
sw	$8,16($sp)

lbu	$2,-5231($16)
beq	$2,$0,$L363
lw	$28,72($sp)

sll	$18,$17,3
lb	$6,-5229($16)
addiu	$17,$17,7586
lw	$5,11876($23)
addu	$18,$23,$18
lw	$4,156($sp)
sll	$17,$17,3
lw	$25,176($sp)
addu	$18,$19,$18
addu	$17,$23,$17
lh	$2,-4842($18)
lh	$7,4($17)
jalr	$25
sw	$2,16($sp)

lh	$2,-4838($18)
lh	$7,-4840($18)
lw	$5,11876($23)
lb	$6,-5229($16)
lw	$4,152($sp)
lw	$25,176($sp)
jalr	$25
sw	$2,16($sp)

lw	$28,72($sp)
$L363:
lw	$3,80($sp)
li	$2,4			# 0x4
lw	$8,144($sp)
lw	$9,100($sp)
addiu	$3,$3,1
addiu	$8,$8,1
addiu	$9,$9,1
sw	$3,80($sp)
sw	$8,144($sp)
bne	$3,$2,$L393
sw	$9,100($sp)

move	$fp,$23
$L338:
lw	$25,96($sp)
li	$2,4			# 0x4
lw	$3,148($sp)
lw	$8,112($sp)
addiu	$25,$25,1
addiu	$3,$3,2
addiu	$8,$8,4
sw	$25,96($sp)
sw	$3,148($sp)
bne	$25,$2,$L394
sw	$8,112($sp)

lb	$4,11620($fp)
$L481:
bltz	$4,$L331
sll	$18,$4,3

lh	$2,11458($fp)
lw	$16,7996($fp)
sll	$4,$4,5
lw	$3,7992($fp)
lh	$17,11456($fp)
addu	$4,$18,$4
sra	$2,$2,2
lw	$8,11872($fp)
sll	$16,$16,4
lw	$5,192($fp)
sra	$6,$17,2
lw	$25,5744($fp)
sll	$17,$3,4
andi	$3,$3,0x3
sll	$7,$4,4
addu	$16,$2,$16
sll	$3,$3,2
li	$18,65536			# 0x10000
subu	$2,$7,$4
addu	$3,$16,$3
ori	$18,$18,0x8a70
addu	$18,$2,$18
mul	$2,$3,$8
addu	$17,$6,$17
addu	$18,$fp,$18
addiu	$17,$17,8
li	$6,4			# 0x4
lw	$4,0($18)
sra	$16,$16,1
addu	$3,$2,$17
sra	$17,$17,1
addiu	$3,$3,64
jalr	$25
addu	$4,$4,$3

li	$6,2			# 0x2
lw	$3,7992($fp)
lw	$2,196($fp)
lw	$4,4($18)
andi	$3,$3,0x7
lw	$5,8($18)
addu	$16,$16,$3
lw	$28,72($sp)
lw	$25,5744($fp)
mul	$3,$16,$2
lw	$31,292($sp)
lw	$fp,288($sp)
subu	$5,$5,$4
lw	$23,284($sp)
lw	$22,280($sp)
lw	$21,276($sp)
lw	$20,272($sp)
lw	$19,268($sp)
addu	$2,$3,$17
lw	$18,264($sp)
lw	$17,260($sp)
addiu	$2,$2,64
lw	$16,256($sp)
addiu	$sp,$sp,296
jr	$25
addu	$4,$4,$2

$L467:
lw	$2,7992($23)
bne	$2,$0,$L367
li	$11,1			# 0x1

lw	$4,7996($23)
li	$2,15			# 0xf
bne	$4,$2,$L483
lw	$25,172($sp)

lw	$9,144($sp)
li	$2,4			# 0x4
bne	$9,$2,$L483
lw	$4,%got($LC4)($28)

li	$11,1			# 0x1
lw	$25,%call16(printf)($28)
sw	$5,236($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$11,240($sp)

lw	$6,11872($23)
lw	$5,236($sp)
lw	$10,248($sp)
b	$L367
lw	$11,240($sp)

$L342:
lw	$8,7996($23)
lw	$2,11876($23)
$L480:
sll	$4,$7,1
lw	$6,11872($23)
li	$18,65536			# 0x10000
sll	$17,$5,1
lw	$10,7992($23)
mul	$3,$7,$2
lw	$9,132($sp)
addu	$18,$23,$18
lw	$25,136($sp)
lw	$14,-5248($18)
addu	$2,$3,$5
mul	$3,$4,$6
sll	$4,$10,3
sra	$16,$8,$14
addu	$25,$25,$2
addu	$4,$5,$4
sll	$16,$16,3
sw	$25,104($sp)
addu	$17,$3,$17
lw	$3,140($sp)
sw	$4,92($sp)
addu	$16,$7,$16
addu	$17,$9,$17
lw	$9,200($sp)
addu	$2,$3,$2
beq	$9,$0,$L419
sw	$2,108($sp)

lw	$9,%got(mpFrame)($28)
li	$2,-3			# 0xfffffffffffffffd
lh	$4,0($9)
beq	$4,$2,$L469
nop

$L374:
lw	$3,100($sp)
$L491:
sll	$10,$16,3
$L492:
lw	$25,92($sp)
$L493:
lw	$4,11880($23)
lw	$13,11884($23)
lbu	$2,0($3)
sll	$8,$25,3
lw	$18,164($23)
lw	$25,160($23)
addiu	$24,$13,-3
addiu	$5,$2,2812
addu	$7,$23,$2
sll	$5,$5,2
sll	$2,$2,2
lb	$20,11568($7)
addu	$5,$23,$5
addu	$2,$23,$2
sll	$18,$18,4
sll	$3,$20,3
lh	$11,0($5)
lh	$22,11250($2)
sll	$20,$20,5
addiu	$5,$4,-3
addu	$7,$3,$20
sw	$3,156($sp)
addu	$8,$11,$8
addu	$19,$22,$10
sll	$2,$7,4
andi	$3,$8,0x7
sra	$12,$19,2
movn	$4,$5,$3
subu	$2,$2,$7
sw	$3,152($sp)
mul	$3,$12,$6
li	$5,65536			# 0x10000
addu	$2,$23,$2
sra	$7,$8,2
addu	$2,$5,$2
andi	$11,$19,0x3
subu	$15,$0,$4
lw	$2,6648($2)
addu	$5,$3,$7
sll	$11,$11,2
andi	$21,$8,0x3
andi	$22,$19,0x7
slt	$15,$7,$15
movn	$13,$24,$22
addu	$11,$21,$11
addu	$5,$2,$5
sll	$21,$25,4
bne	$15,$0,$L377
sra	$18,$18,$14

subu	$2,$0,$13
slt	$2,$12,$2
bne	$2,$0,$L484
subu	$2,$0,$6

addiu	$2,$7,15
addu	$4,$21,$4
slt	$4,$2,$4
beq	$4,$0,$L484
subu	$2,$0,$6

addiu	$2,$12,15
addu	$13,$18,$13
slt	$13,$2,$13
beq	$13,$0,$L484
subu	$2,$0,$6

b	$L378
move	$7,$0

$L377:
subu	$2,$0,$6
$L484:
lw	$4,2856($23)
addiu	$7,$7,-2
lw	$25,%call16(ff_emulated_edge_mc)($28)
sll	$2,$2,1
sw	$8,252($sp)
addiu	$12,$12,-2
sw	$9,244($sp)
addiu	$2,$2,-2
sw	$7,20($sp)
li	$7,21			# 0x15
sw	$11,240($sp)
addu	$5,$5,$2
sw	$21,28($sp)
li	$2,21			# 0x15
sw	$18,32($sp)
sw	$12,24($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,16($sp)

lw	$6,11872($23)
lw	$9,244($sp)
lw	$5,2856($23)
sll	$2,$6,1
lw	$28,72($sp)
lw	$8,252($sp)
addiu	$2,$2,2
lh	$4,0($9)
addu	$5,$5,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L470
lw	$11,240($sp)

li	$7,1			# 0x1
$L378:
lw	$25,172($sp)
$L486:
sll	$11,$11,2
move	$4,$17
addu	$11,$25,$11
lw	$25,0($11)
sw	$7,244($sp)
jalr	$25
sw	$8,252($sp)

li	$4,65536			# 0x10000
lw	$28,72($sp)
addu	$2,$23,$4
lw	$7,244($sp)
lw	$2,-5248($2)
bne	$2,$0,$L379
lw	$8,252($sp)

sra	$10,$19,3
$L380:
lw	$5,164($23)
lw	$25,156($sp)
lw	$9,196($23)
addiu	$5,$5,2
lw	$6,11876($23)
addu	$19,$25,$20
sll	$5,$5,3
sll	$20,$19,4
mul	$3,$5,$9
sll	$4,$9,4
subu	$19,$20,$19
mul	$2,$6,$10
li	$5,65536			# 0x10000
addiu	$4,$4,48
addu	$19,$23,$19
sra	$20,$8,3
addu	$19,$5,$19
addu	$4,$3,$4
addu	$9,$2,$20
addu	$4,$4,$20
lw	$5,6652($19)
addu	$2,$4,$2
addu	$9,$5,$9
bne	$7,$0,$L471
addu	$19,$5,$2

lw	$3,152($sp)
li	$7,2			# 0x2
lw	$4,104($sp)
move	$5,$9
lw	$25,84($sp)
sw	$22,20($sp)
jalr	$25
sw	$3,16($sp)

$L399:
lw	$3,152($sp)
li	$7,2			# 0x2
lw	$6,11876($23)
move	$5,$19
lw	$4,108($sp)
lw	$25,84($sp)
sw	$3,16($sp)
jalr	$25
sw	$22,20($sp)

lw	$28,72($sp)
lw	$10,212($sp)
sw	$fp,84($sp)
$L373:
lw	$3,192($sp)
beq	$3,$0,$L363
lw	$9,%got(mpFrame)($28)

li	$2,-3			# 0xfffffffffffffffd
lh	$4,0($9)
beq	$4,$2,$L472
nop

$L384:
lw	$3,100($sp)
$L488:
sll	$16,$16,3
lw	$8,92($sp)
li	$13,131072			# 0x20000
lw	$4,11880($23)
li	$12,65536			# 0x10000
lw	$6,11872($23)
lbu	$2,0($3)
sll	$7,$8,3
addu	$12,$23,$12
lw	$11,11884($23)
lw	$18,164($23)
addu	$8,$23,$2
lw	$24,160($23)
addiu	$5,$2,2852
lw	$14,-5248($12)
sll	$2,$2,2
lb	$20,11608($8)
sll	$5,$5,2
addu	$2,$23,$2
addu	$5,$23,$5
sll	$19,$20,3
lh	$22,11410($2)
sll	$20,$20,5
lh	$fp,0($5)
addiu	$8,$4,-3
addu	$5,$19,$20
addu	$16,$22,$16
sll	$2,$5,4
addu	$fp,$fp,$7
sra	$7,$16,2
subu	$5,$2,$5
mul	$3,$7,$6
addu	$5,$23,$5
andi	$25,$fp,0x7
movn	$4,$8,$25
addu	$13,$13,$5
sra	$2,$fp,2
andi	$8,$16,0x3
sw	$25,92($sp)
lw	$15,-30088($13)
addiu	$21,$11,-3
subu	$12,$0,$4
sll	$13,$18,4
addu	$5,$3,$2
sll	$8,$8,2
andi	$18,$fp,0x3
andi	$22,$16,0x7
slt	$12,$2,$12
movn	$11,$21,$22
addu	$8,$18,$8
addu	$5,$15,$5
sll	$21,$24,4
bne	$12,$0,$L387
sra	$18,$13,$14

subu	$12,$0,$11
slt	$12,$7,$12
bne	$12,$0,$L387
addiu	$12,$2,15

addu	$4,$21,$4
slt	$4,$12,$4
beq	$4,$0,$L387
addiu	$4,$7,15

addu	$11,$18,$11
slt	$11,$4,$11
beq	$11,$0,$L485
subu	$11,$0,$6

b	$L388
move	$7,$0

$L470:
lw	$2,7992($23)
bne	$2,$0,$L378
li	$7,1			# 0x1

lw	$4,7996($23)
li	$2,15			# 0xf
bne	$4,$2,$L486
lw	$25,172($sp)

lw	$9,144($sp)
li	$2,4			# 0x4
bne	$9,$2,$L486
lw	$4,%got($LC4)($28)

li	$7,1			# 0x1
lw	$25,%call16(printf)($28)
sw	$5,236($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$7,244($sp)

lw	$6,11872($23)
lw	$5,236($sp)
lw	$7,244($sp)
lw	$8,252($sp)
b	$L378
lw	$11,240($sp)

$L387:
subu	$11,$0,$6
$L485:
lw	$4,2856($23)
addiu	$2,$2,-2
lw	$25,%call16(ff_emulated_edge_mc)($28)
sll	$11,$11,1
sw	$8,252($sp)
addiu	$7,$7,-2
sw	$9,244($sp)
addiu	$11,$11,-2
sw	$2,20($sp)
li	$2,21			# 0x15
sw	$10,248($sp)
addu	$5,$5,$11
sw	$7,24($sp)
li	$7,21			# 0x15
sw	$21,28($sp)
sw	$2,16($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

lw	$6,11872($23)
lw	$9,244($sp)
lw	$5,2856($23)
sll	$2,$6,1
lw	$28,72($sp)
lw	$8,252($sp)
addiu	$2,$2,2
lh	$4,0($9)
addu	$5,$5,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L473
lw	$10,248($sp)

li	$7,1			# 0x1
$L388:
sll	$8,$8,2
move	$4,$17
addu	$10,$10,$8
lw	$25,0($10)
jalr	$25
sw	$7,244($sp)

li	$2,65536			# 0x10000
lw	$28,72($sp)
addu	$2,$23,$2
lw	$2,-5248($2)
bne	$2,$0,$L389
lw	$7,244($sp)

sra	$16,$16,3
$L390:
lw	$5,164($23)
addu	$19,$19,$20
lw	$8,196($23)
sra	$fp,$fp,3
sll	$20,$19,4
lw	$6,11876($23)
addiu	$5,$5,2
sll	$4,$8,4
sll	$5,$5,3
subu	$19,$20,$19
mul	$3,$5,$8
mul	$2,$6,$16
addiu	$4,$4,48
addu	$19,$23,$19
li	$20,131072			# 0x20000
addu	$19,$20,$19
addu	$4,$3,$4
addu	$8,$2,$fp
addu	$4,$4,$fp
lw	$5,-30084($19)
addu	$2,$4,$2
addu	$8,$5,$8
bne	$7,$0,$L474
addu	$17,$5,$2

lw	$3,92($sp)
li	$7,2			# 0x2
lw	$4,104($sp)
move	$5,$8
lw	$25,84($sp)
sw	$22,20($sp)
jalr	$25
sw	$3,16($sp)

$L400:
lw	$3,92($sp)
li	$7,2			# 0x2
lw	$6,11876($23)
move	$5,$17
lw	$4,108($sp)
lw	$25,84($sp)
sw	$3,16($sp)
jalr	$25
sw	$22,20($sp)

b	$L363
lw	$28,72($sp)

$L473:
lw	$2,7992($23)
bne	$2,$0,$L388
li	$7,1			# 0x1

lw	$4,7996($23)
li	$2,15			# 0xf
bne	$4,$2,$L388
lw	$9,144($sp)

li	$2,4			# 0x4
bne	$9,$2,$L388
lw	$4,%got($LC4)($28)

li	$7,1			# 0x1
lw	$25,%call16(printf)($28)
sw	$5,236($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$7,244($sp)

lw	$6,11872($23)
lw	$5,236($sp)
lw	$7,244($sp)
lw	$8,252($sp)
b	$L388
lw	$10,248($sp)

$L419:
b	$L373
lw	$10,172($sp)

$L462:
lw	$25,180($sp)
li	$6,1			# 0x1
lw	$3,128($sp)
li	$7,4			# 0x4
lw	$9,124($sp)
addiu	$2,$25,64
lw	$25,164($sp)
lw	$8,184($sp)
lw	$12,4($3)
addiu	$10,$25,12
lw	$25,136($sp)
lw	$3,132($sp)
addiu	$5,$8,64
lw	$4,4($9)
lw	$9,160($sp)
lw	$8,88($sp)
sw	$25,24($sp)
sw	$3,20($sp)
addiu	$11,$9,12
lw	$25,140($sp)
andi	$9,$8,0x1000
lw	$3,116($sp)
andi	$8,$8,0x4000
sw	$5,48($sp)
sw	$4,44($sp)
move	$4,$fp
sw	$25,28($sp)
sw	$3,32($sp)
lw	$25,120($sp)
lw	$3,%got(mc_part)($28)
lw	$5,112($sp)
sw	$0,16($sp)
sw	$25,36($sp)
addiu	$25,$3,%lo(mc_part)
sw	$2,40($sp)
sw	$12,52($sp)
sw	$11,56($sp)
sw	$10,60($sp)
sw	$9,64($sp)
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
sw	$8,68($sp)

b	$L338
lw	$28,72($sp)

$L389:
addu	$4,$19,$20
lw	$22,7996($23)
sll	$5,$4,4
andi	$2,$22,0x1
subu	$4,$5,$4
li	$5,131072			# 0x20000
addu	$4,$23,$4
addu	$4,$5,$4
lw	$22,-30016($4)
subu	$22,$2,$22
addiu	$22,$22,1
sll	$22,$22,1
addu	$22,$16,$22
sra	$16,$22,3
bltz	$16,$L431
addiu	$2,$16,8

sra	$4,$18,1
andi	$22,$22,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L390
or	$7,$7,$2

$L368:
lw	$3,108($sp)
lw	$8,104($sp)
lw	$22,7996($23)
addu	$4,$20,$3
subu	$7,$19,$8
sll	$2,$4,4
sll	$10,$7,4
subu	$5,$2,$4
subu	$4,$10,$7
andi	$2,$22,0x1
addu	$4,$5,$4
addu	$4,$23,$4
addu	$4,$6,$4
lw	$22,6720($4)
subu	$22,$2,$22
addiu	$22,$22,1
sll	$22,$22,1
addu	$22,$16,$22
sra	$10,$22,3
bltz	$10,$L418
addiu	$2,$10,8

sra	$4,$18,1
andi	$22,$22,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L369
or	$11,$11,$2

$L379:
lw	$3,156($sp)
lw	$22,7996($23)
addu	$5,$3,$20
andi	$2,$22,0x1
sll	$6,$5,4
subu	$5,$6,$5
addu	$5,$23,$5
addu	$4,$4,$5
lw	$22,6720($4)
subu	$22,$2,$22
addiu	$22,$22,1
sll	$22,$22,1
addu	$22,$19,$22
sra	$10,$22,3
bltz	$10,$L425
addiu	$2,$10,8

sra	$4,$18,1
andi	$22,$22,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L380
or	$7,$7,$2

$L463:
lw	$9,124($sp)
li	$23,4			# 0x4
lw	$25,180($sp)
move	$6,$0
lw	$3,128($sp)
li	$7,2			# 0x2
lw	$8,184($sp)
lw	$4,4($9)
addiu	$21,$25,128
lw	$9,160($sp)
lw	$25,132($sp)
addiu	$20,$8,128
lw	$8,136($sp)
addiu	$19,$9,16
lw	$9,140($sp)
lw	$2,4($3)
lw	$3,164($sp)
lw	$5,112($sp)
sw	$25,20($sp)
sw	$9,28($sp)
addiu	$18,$3,16
sw	$8,24($sp)
lw	$25,88($sp)
lw	$9,%got(mc_part)($28)
lw	$3,116($sp)
lw	$8,120($sp)
andi	$17,$25,0x1000
andi	$16,$25,0x4000
sw	$4,44($sp)
addiu	$22,$9,%lo(mc_part)
sw	$2,52($sp)
andi	$17,$17,0xffff
sw	$3,32($sp)
andi	$16,$16,0xffff
sw	$8,36($sp)
move	$4,$fp
sw	$23,16($sp)
move	$25,$22
sw	$21,40($sp)
sw	$20,48($sp)
sw	$19,56($sp)
sw	$18,60($sp)
sw	$17,64($sp)
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
sw	$16,68($sp)

move	$6,$0
lw	$25,124($sp)
li	$7,2			# 0x2
lw	$3,120($sp)
move	$4,$fp
lw	$8,4($25)
addiu	$9,$3,2
lw	$25,128($sp)
lw	$3,112($sp)
lw	$2,4($25)
addiu	$5,$3,2
lw	$25,136($sp)
lw	$3,132($sp)
sw	$23,16($sp)
sw	$9,36($sp)
sw	$25,24($sp)
sw	$3,20($sp)
sw	$21,40($sp)
sw	$8,44($sp)
sw	$20,48($sp)
sw	$2,52($sp)
lw	$3,140($sp)
lw	$25,116($sp)
sw	$3,28($sp)
sw	$25,32($sp)
$L458:
sw	$19,56($sp)
move	$25,$22
sw	$18,60($sp)
sw	$17,64($sp)
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
sw	$16,68($sp)

b	$L338
lw	$28,72($sp)

$L465:
lw	$3,88($sp)
li	$2,20480			# 0x5000
andi	$12,$3,0x5000
bne	$12,$2,$L342
lw	$8,100($sp)

lbu	$9,0($8)
lw	$8,7996($23)
addu	$4,$23,$9
andi	$6,$8,0x1
lb	$2,11568($4)
lb	$10,11608($4)
sll	$11,$2,4
sll	$4,$2,6
subu	$4,$4,$11
addu	$2,$4,$10
sll	$2,$2,1
addu	$2,$2,$6
addiu	$2,$2,30728
sll	$2,$2,1
addu	$2,$23,$2
lh	$4,4($2)
li	$2,32			# 0x20
bne	$4,$2,$L487
lw	$3,160($sp)

b	$L480
lw	$2,11876($23)

$L331:
lw	$31,292($sp)
lw	$fp,288($sp)
lw	$23,284($sp)
lw	$22,280($sp)
lw	$21,276($sp)
lw	$20,272($sp)
lw	$19,268($sp)
lw	$18,264($sp)
lw	$17,260($sp)
lw	$16,256($sp)
j	$31
addiu	$sp,$sp,296

$L474:
sra	$21,$21,1
lw	$4,2856($23)
sra	$18,$18,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
li	$19,9			# 0x9
sw	$fp,20($sp)
li	$7,9			# 0x9
sw	$16,24($sp)
move	$5,$8
sw	$21,28($sp)
sw	$19,16($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

li	$7,2			# 0x2
lw	$8,92($sp)
lw	$5,2856($23)
lw	$6,11876($23)
lw	$4,104($sp)
lw	$25,84($sp)
sw	$8,16($sp)
jalr	$25
sw	$22,20($sp)

li	$7,9			# 0x9
lw	$28,72($sp)
move	$5,$17
lw	$4,2856($23)
lw	$6,11876($23)
sw	$19,16($sp)
sw	$fp,20($sp)
sw	$16,24($sp)
sw	$21,28($sp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

b	$L400
lw	$17,2856($23)

$L464:
lw	$9,124($sp)
move	$6,$0
lw	$8,180($sp)
li	$7,4			# 0x4
lw	$3,128($sp)
lw	$25,184($sp)
lw	$4,8($9)
addiu	$21,$8,128
lw	$8,160($sp)
lw	$5,8($3)
addiu	$20,$25,128
lw	$3,136($sp)
lw	$25,132($sp)
addiu	$19,$8,20
lw	$9,164($sp)
lw	$2,11872($fp)
lw	$8,88($sp)
sw	$4,44($sp)
addiu	$18,$9,20
sw	$25,20($sp)
sll	$2,$2,2
sw	$3,24($sp)
andi	$17,$8,0x1000
sw	$5,52($sp)
andi	$16,$8,0x4000
lw	$4,%got(mc_part)($28)
andi	$17,$17,0xffff
lw	$9,140($sp)
andi	$16,$16,0xffff
lw	$25,116($sp)
lw	$3,120($sp)
addiu	$22,$4,%lo(mc_part)
lw	$5,112($sp)
move	$4,$fp
sw	$9,28($sp)
sw	$25,32($sp)
move	$25,$22
sw	$2,16($sp)
sw	$3,36($sp)
sw	$21,40($sp)
sw	$20,48($sp)
sw	$19,56($sp)
sw	$18,60($sp)
sw	$17,64($sp)
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
sw	$16,68($sp)

move	$6,$0
lw	$3,116($sp)
li	$7,4			# 0x4
lw	$25,124($sp)
move	$4,$fp
lw	$8,112($sp)
addiu	$10,$3,2
lw	$3,128($sp)
lw	$2,11872($fp)
lw	$9,8($25)
addiu	$5,$8,1
lw	$25,132($sp)
lw	$8,8($3)
sll	$2,$2,2
lw	$3,136($sp)
sw	$10,32($sp)
sw	$25,20($sp)
sw	$2,16($sp)
sw	$3,24($sp)
sw	$21,40($sp)
sw	$9,44($sp)
sw	$20,48($sp)
sw	$8,52($sp)
lw	$25,140($sp)
lw	$3,120($sp)
sw	$25,28($sp)
b	$L458
sw	$3,36($sp)

$L472:
lw	$2,7992($23)
bne	$2,$0,$L488
lw	$3,100($sp)

lw	$4,7996($23)
li	$2,15			# 0xf
bne	$4,$2,$L488
lw	$8,144($sp)

li	$2,4			# 0x4
bne	$8,$2,$L488
lw	$2,%got($LC6)($28)

lw	$25,%call16(printf)($28)
sw	$9,244($sp)
addiu	$4,$2,%lo($LC6)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$10,248($sp)

lw	$28,72($sp)
lw	$10,248($sp)
b	$L384
lw	$9,244($sp)

$L468:
lw	$8,188($sp)
sra	$18,$18,1
li	$20,9			# 0x9
lw	$4,2856($23)
lw	$25,%call16(ff_emulated_edge_mc)($28)
li	$7,9			# 0x9
sra	$fp,$8,1
sw	$10,24($sp)
sw	$10,248($sp)
sw	$19,20($sp)
sw	$fp,28($sp)
sw	$20,16($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

li	$7,2			# 0x2
lw	$5,2856($23)
lw	$6,11876($23)
lw	$4,156($sp)
lw	$25,84($sp)
sw	$21,16($sp)
jalr	$25
sw	$22,20($sp)

li	$7,9			# 0x9
lw	$28,72($sp)
move	$5,$16
lw	$10,248($sp)
lw	$4,2856($23)
lw	$6,11876($23)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$20,16($sp)
sw	$19,20($sp)
sw	$10,24($sp)
sw	$fp,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

b	$L398
lw	$16,2856($23)

$L471:
li	$2,9			# 0x9
lw	$4,2856($23)
sra	$21,$21,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
sra	$18,$18,1
sw	$10,24($sp)
li	$7,9			# 0x9
sw	$2,16($sp)
move	$5,$9
sw	$2,236($sp)
sw	$10,248($sp)
sw	$20,20($sp)
sw	$21,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

li	$7,2			# 0x2
lw	$8,152($sp)
lw	$5,2856($23)
lw	$6,11876($23)
lw	$4,104($sp)
lw	$25,84($sp)
sw	$8,16($sp)
jalr	$25
sw	$22,20($sp)

li	$7,9			# 0x9
lw	$28,72($sp)
move	$5,$19
lw	$2,236($sp)
lw	$10,248($sp)
lw	$4,2856($23)
lw	$6,11876($23)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$2,16($sp)
sw	$20,20($sp)
sw	$10,24($sp)
sw	$21,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

b	$L399
lw	$19,2856($23)

$L466:
addu	$14,$23,$9
lw	$11,11880($23)
sll	$9,$9,2
lw	$13,11884($23)
sll	$5,$5,3
lw	$18,164($23)
addu	$19,$23,$9
lb	$fp,11568($14)
sw	$9,232($sp)
sll	$7,$7,3
lw	$9,2864($23)
addiu	$12,$11,-3
sll	$3,$fp,3
lh	$8,11248($19)
sw	$5,216($sp)
sll	$4,$4,3
sw	$7,228($sp)
sll	$18,$18,4
sw	$9,108($sp)
sll	$9,$fp,5
sw	$3,188($sp)
addiu	$24,$13,-3
lb	$14,11608($14)
sra	$18,$18,$2
sw	$9,176($sp)
lw	$5,176($sp)
lh	$9,11250($19)
lw	$25,160($23)
addu	$7,$3,$5
lw	$3,216($sp)
sw	$14,196($sp)
sll	$5,$7,4
addu	$8,$8,$3
lw	$3,228($sp)
subu	$5,$5,$7
andi	$20,$8,0x7
addu	$17,$9,$3
movn	$11,$12,$20
sra	$7,$8,2
sra	$12,$17,2
lw	$9,108($sp)
addu	$5,$23,$5
mul	$3,$6,$12
addu	$5,$10,$5
subu	$14,$0,$11
andi	$10,$17,0x3
lw	$5,6648($5)
addiu	$9,$9,8
sll	$10,$10,2
andi	$21,$8,0x3
addu	$15,$3,$7
lw	$3,108($sp)
andi	$16,$17,0x7
sw	$9,220($sp)
slt	$14,$7,$14
movn	$13,$24,$16
addu	$4,$3,$4
addu	$10,$21,$10
addu	$5,$5,$15
sw	$4,224($sp)
bne	$14,$0,$L348
sll	$21,$25,4

subu	$2,$0,$13
slt	$2,$12,$2
bne	$2,$0,$L489
subu	$2,$0,$6

addiu	$2,$7,15
addu	$11,$21,$11
slt	$11,$2,$11
beq	$11,$0,$L489
subu	$2,$0,$6

addiu	$2,$12,15
addu	$13,$18,$13
slt	$13,$2,$13
beq	$13,$0,$L489
subu	$2,$0,$6

b	$L349
move	$7,$0

$L348:
subu	$2,$0,$6
$L489:
lw	$4,2856($23)
addiu	$7,$7,-2
lw	$25,%call16(ff_emulated_edge_mc)($28)
sll	$2,$2,1
sw	$8,252($sp)
addiu	$12,$12,-2
sw	$10,248($sp)
addiu	$2,$2,-2
sw	$7,20($sp)
li	$7,21			# 0x15
sw	$21,28($sp)
addu	$5,$5,$2
sw	$18,32($sp)
li	$2,21			# 0x15
sw	$12,24($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,16($sp)

lw	$28,72($sp)
lw	$6,11872($23)
lw	$5,2856($23)
lw	$8,252($sp)
lw	$4,%got(mpFrame)($28)
sll	$2,$6,1
addiu	$2,$2,2
lh	$4,0($4)
addu	$5,$5,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L475
lw	$10,248($sp)

li	$7,1			# 0x1
$L349:
lw	$25,172($sp)
$L494:
sll	$10,$10,2
lw	$4,92($sp)
addu	$10,$25,$10
lw	$25,0($10)
sw	$7,244($sp)
jalr	$25
sw	$8,252($sp)

li	$4,65536			# 0x10000
lw	$28,72($sp)
addu	$2,$23,$4
lw	$7,244($sp)
lw	$2,-5248($2)
bne	$2,$0,$L350
lw	$8,252($sp)

sra	$9,$17,3
$L351:
lw	$5,164($23)
sra	$8,$8,3
lw	$3,176($sp)
lw	$25,188($sp)
addiu	$5,$5,2
lw	$11,196($23)
lw	$6,11876($23)
sll	$5,$5,3
addu	$17,$25,$3
mul	$3,$5,$11
sll	$10,$17,4
sll	$4,$11,4
subu	$17,$10,$17
mul	$2,$6,$9
li	$5,65536			# 0x10000
addiu	$4,$4,48
addu	$17,$23,$17
addu	$4,$3,$4
addu	$17,$5,$17
addu	$4,$4,$8
addu	$10,$2,$8
lw	$5,6652($17)
addu	$2,$4,$2
addu	$10,$5,$10
bne	$7,$0,$L476
addu	$17,$5,$2

lw	$4,156($sp)
li	$7,2			# 0x2
lw	$25,84($sp)
move	$5,$10
sw	$20,16($sp)
jalr	$25
sw	$16,20($sp)

$L396:
lw	$3,196($sp)
li	$7,2			# 0x2
lw	$6,11876($23)
move	$5,$17
lw	$4,152($sp)
sll	$8,$3,5
lw	$25,84($sp)
sw	$20,16($sp)
sll	$18,$3,3
sw	$16,20($sp)
jalr	$25
sw	$8,176($sp)

li	$10,131072			# 0x20000
lw	$8,232($sp)
li	$12,65536			# 0x10000
lw	$3,176($sp)
lw	$25,216($sp)
addu	$12,$23,$12
addu	$7,$23,$8
lh	$8,11408($19)
addu	$5,$18,$3
lw	$3,228($sp)
lw	$4,11880($23)
lh	$9,11410($7)
sll	$2,$5,4
lw	$6,11872($23)
addu	$21,$25,$8
subu	$5,$2,$5
lw	$11,11884($23)
addu	$20,$3,$9
lw	$3,160($23)
addiu	$2,$4,-3
lw	$19,164($23)
sra	$7,$20,2
lw	$14,-5248($12)
addu	$5,$23,$5
lw	$28,72($sp)
mul	$8,$7,$6
andi	$17,$21,0x7
movn	$4,$2,$17
addu	$5,$10,$5
sra	$2,$21,2
andi	$10,$20,0x3
lw	$5,-30088($5)
sll	$3,$3,4
subu	$12,$0,$4
sll	$13,$19,4
addu	$15,$8,$2
sw	$3,216($sp)
sll	$10,$10,2
addiu	$24,$11,-3
andi	$19,$21,0x3
andi	$16,$20,0x7
slt	$12,$2,$12
movn	$11,$24,$16
addu	$10,$19,$10
addu	$5,$5,$15
bne	$12,$0,$L356
sra	$19,$13,$14

subu	$12,$0,$11
slt	$12,$7,$12
bne	$12,$0,$L356
addiu	$12,$2,15

addu	$4,$3,$4
slt	$4,$12,$4
beq	$4,$0,$L356
addiu	$4,$7,15

addu	$11,$19,$11
slt	$11,$4,$11
beq	$11,$0,$L490
subu	$11,$0,$6

b	$L357
move	$7,$0

$L469:
bne	$10,$0,$L491
lw	$3,100($sp)

li	$2,15			# 0xf
bne	$8,$2,$L492
sll	$10,$16,3

lw	$25,144($sp)
li	$2,4			# 0x4
bne	$25,$2,$L493
lw	$25,92($sp)

lw	$4,%got($LC5)($28)
lw	$25,%call16(printf)($28)
sw	$9,244($sp)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC5)

lw	$28,72($sp)
lw	$6,11872($23)
lw	$14,-5248($18)
b	$L374
lw	$9,244($sp)

$L459:
lw	$25,128($sp)
andi	$2,$17,0x1000
lw	$9,124($sp)
andi	$17,$17,0x4000
move	$5,$0
li	$6,1			# 0x1
lw	$3,0($25)
li	$7,8			# 0x8
lw	$25,136($sp)
move	$4,$fp
lw	$8,0($9)
lw	$9,132($sp)
sw	$0,16($sp)
sw	$25,24($sp)
lw	$25,180($sp)
sw	$9,20($sp)
sw	$8,44($sp)
lw	$9,140($sp)
lw	$8,184($sp)
sw	$25,40($sp)
lw	$25,164($sp)
sw	$9,28($sp)
sw	$0,32($sp)
sw	$0,36($sp)
sw	$25,60($sp)
sw	$8,48($sp)
sw	$3,52($sp)
sw	$2,64($sp)
sw	$17,68($sp)
lw	$9,160($sp)
lw	$25,%got(mc_part)($28)
addiu	$25,$25,%lo(mc_part)
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
sw	$9,56($sp)

b	$L481
lb	$4,11620($fp)

$L461:
lw	$3,180($sp)
move	$5,$0
lw	$25,128($sp)
move	$6,$0
lw	$9,184($sp)
li	$7,8			# 0x8
lw	$8,124($sp)
addiu	$21,$3,64
lw	$3,160($sp)
addiu	$20,$9,64
lw	$9,4($25)
lw	$25,132($sp)
lw	$10,4($8)
addiu	$19,$3,8
lw	$2,11872($fp)
lw	$8,164($sp)
lw	$3,136($sp)
lw	$16,%got(mc_part)($28)
sll	$2,$2,3
sw	$25,20($sp)
addiu	$18,$8,8
sw	$10,44($sp)
andi	$8,$17,0x1000
sw	$3,24($sp)
addiu	$16,$16,%lo(mc_part)
sw	$2,16($sp)
andi	$3,$17,0x4000
sw	$9,52($sp)
sw	$8,64($sp)
sw	$3,68($sp)
sw	$0,32($sp)
sw	$0,36($sp)
sw	$21,40($sp)
sw	$20,48($sp)
sw	$19,56($sp)
sw	$18,60($sp)
lw	$25,140($sp)
sw	$25,28($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
move	$4,$fp

andi	$3,$17,0x2000
lw	$8,124($sp)
andi	$17,$17,0x8000
lw	$25,128($sp)
li	$5,4			# 0x4
lw	$2,11872($fp)
move	$6,$0
li	$7,8			# 0x8
lw	$9,4($8)
lw	$8,4($25)
sll	$2,$2,3
lw	$25,132($sp)
sw	$0,36($sp)
sw	$2,16($sp)
li	$2,4			# 0x4
sw	$21,40($sp)
sw	$25,20($sp)
lw	$25,136($sp)
sw	$2,32($sp)
sw	$9,44($sp)
sw	$20,48($sp)
sw	$25,24($sp)
sw	$8,52($sp)
sw	$19,56($sp)
sw	$18,60($sp)
sw	$3,64($sp)
sw	$17,68($sp)
lw	$25,140($sp)
sw	$25,28($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
move	$4,$fp

b	$L481
lb	$4,11620($fp)

$L350:
lw	$9,176($sp)
lw	$3,188($sp)
lw	$16,7996($23)
addu	$5,$3,$9
andi	$2,$16,0x1
sll	$6,$5,4
subu	$5,$6,$5
addu	$5,$23,$5
addu	$4,$4,$5
lw	$16,6720($4)
subu	$16,$2,$16
addiu	$16,$16,1
sll	$16,$16,1
addu	$16,$17,$16
sra	$9,$16,3
bltz	$9,$L406
addiu	$2,$9,8

sra	$4,$18,1
andi	$16,$16,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L351
or	$7,$7,$2

$L475:
lw	$2,7992($23)
bne	$2,$0,$L349
li	$7,1			# 0x1

lw	$4,7996($23)
li	$2,15			# 0xf
bne	$4,$2,$L494
lw	$25,172($sp)

lw	$9,144($sp)
li	$2,4			# 0x4
bne	$9,$2,$L494
lw	$4,%got($LC4)($28)

li	$7,1			# 0x1
lw	$25,%call16(printf)($28)
sw	$5,236($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$7,244($sp)

lw	$6,11872($23)
lw	$5,236($sp)
lw	$7,244($sp)
lw	$8,252($sp)
b	$L349
lw	$10,248($sp)

$L356:
subu	$11,$0,$6
$L490:
lw	$9,216($sp)
addiu	$2,$2,-2
lw	$4,2856($23)
sll	$11,$11,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
addiu	$7,$7,-2
sw	$10,248($sp)
addiu	$11,$11,-2
sw	$2,20($sp)
li	$2,21			# 0x15
sw	$9,28($sp)
addu	$5,$5,$11
sw	$7,24($sp)
li	$7,21			# 0x15
sw	$19,32($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$2,16($sp)

lw	$28,72($sp)
lw	$6,11872($23)
lw	$5,2856($23)
lw	$4,%got(mpFrame)($28)
sll	$2,$6,1
addiu	$2,$2,2
lh	$4,0($4)
addu	$5,$5,$2
li	$2,-3			# 0xfffffffffffffffd
beq	$4,$2,$L477
lw	$10,248($sp)

li	$7,1			# 0x1
$L357:
lw	$3,172($sp)
$L495:
sll	$10,$10,2
lw	$4,224($sp)
addu	$10,$3,$10
lw	$25,0($10)
jalr	$25
sw	$7,244($sp)

li	$2,65536			# 0x10000
lw	$28,72($sp)
addu	$2,$23,$2
lw	$2,-5248($2)
bne	$2,$0,$L358
lw	$7,244($sp)

sra	$9,$20,3
$L359:
lw	$5,164($23)
sra	$8,$21,3
lw	$25,176($sp)
lw	$11,196($23)
addiu	$5,$5,2
lw	$6,11876($23)
addu	$20,$18,$25
sll	$5,$5,3
sll	$10,$20,4
mul	$3,$5,$11
subu	$20,$10,$20
sll	$4,$11,4
mul	$2,$6,$9
li	$5,131072			# 0x20000
addiu	$4,$4,48
addu	$20,$23,$20
addu	$4,$3,$4
addu	$20,$5,$20
addu	$4,$4,$8
addu	$10,$2,$8
lw	$5,-30084($20)
addu	$2,$4,$2
addu	$10,$5,$10
bne	$7,$0,$L478
addu	$20,$5,$2

lw	$4,108($sp)
li	$7,2			# 0x2
lw	$25,84($sp)
move	$5,$10
sw	$17,16($sp)
jalr	$25
sw	$16,20($sp)

$L397:
li	$19,65536			# 0x10000
lw	$6,11876($23)
lw	$4,220($sp)
li	$7,2			# 0x2
lw	$25,84($sp)
move	$5,$20
sw	$17,16($sp)
addu	$17,$23,$19
jalr	$25
sw	$16,20($sp)

li	$2,2			# 0x2
lbu	$4,-5232($17)
beq	$4,$2,$L479
addiu	$2,$fp,7538

lw	$3,188($sp)
addu	$8,$23,$18
lb	$7,-5230($17)
sll	$2,$2,3
lw	$6,11872($23)
addu	$8,$19,$8
lw	$4,92($sp)
addu	$2,$23,$2
lw	$25,104($sp)
addu	$5,$23,$3
sll	$18,$fp,4
lh	$9,4($2)
addu	$2,$19,$5
lw	$5,224($sp)
addu	$18,$23,$18
addu	$18,$19,$18
sw	$9,16($sp)
lh	$9,-5224($8)
sw	$9,20($sp)
lh	$8,-5222($8)
lh	$2,-5226($2)
addu	$2,$2,$8
jalr	$25
sw	$2,24($sp)

addiu	$2,$fp,3793
lw	$3,196($sp)
move	$25,$22
sll	$2,$2,4
lw	$6,11876($23)
lb	$7,-5229($17)
addu	$2,$23,$2
lw	$4,156($sp)
sll	$16,$3,4
lw	$5,108($sp)
lh	$2,4($2)
addu	$16,$23,$16
addu	$16,$19,$16
sw	$2,16($sp)
lh	$2,-4836($16)
sw	$2,20($sp)
lh	$8,-4834($16)
lh	$2,-4842($18)
addu	$2,$2,$8
jalr	$25
sw	$2,24($sp)

move	$25,$22
lh	$2,-4840($18)
lw	$6,11876($23)
lb	$7,-5229($17)
lw	$4,152($sp)
sw	$2,16($sp)
lh	$2,-4832($16)
lw	$5,220($sp)
sw	$2,20($sp)
lh	$2,-4838($18)
lh	$8,-4830($16)
addu	$2,$2,$8
jalr	$25
sw	$2,24($sp)

b	$L363
lw	$28,72($sp)

$L477:
lw	$2,7992($23)
bne	$2,$0,$L357
li	$7,1			# 0x1

lw	$4,7996($23)
li	$2,15			# 0xf
bne	$4,$2,$L495
lw	$3,172($sp)

lw	$25,144($sp)
li	$2,4			# 0x4
bne	$25,$2,$L495
lw	$4,%got($LC4)($28)

li	$7,1			# 0x1
lw	$25,%call16(printf)($28)
sw	$5,236($sp)
addiu	$4,$4,%lo($LC4)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$7,244($sp)

lw	$6,11872($23)
lw	$5,236($sp)
lw	$7,244($sp)
b	$L357
lw	$10,248($sp)

$L460:
lw	$3,124($sp)
li	$22,8			# 0x8
lw	$9,128($sp)
andi	$2,$17,0x4000
lw	$8,180($sp)
move	$5,$0
lw	$25,184($sp)
move	$6,$0
lw	$4,0($3)
li	$7,4			# 0x4
lw	$3,160($sp)
addiu	$21,$8,64
lw	$8,0($9)
addiu	$20,$25,64
lw	$9,164($sp)
addiu	$19,$3,4
lw	$3,136($sp)
lw	$16,%got(mc_part)($28)
addiu	$18,$9,4
lw	$25,132($sp)
lw	$9,140($sp)
sw	$3,24($sp)
addiu	$16,$16,%lo(mc_part)
andi	$3,$17,0x1000
sw	$4,44($sp)
sw	$25,20($sp)
move	$4,$fp
sw	$9,28($sp)
move	$25,$16
sw	$8,52($sp)
sw	$3,64($sp)
sw	$2,68($sp)
sw	$0,32($sp)
sw	$22,16($sp)
sw	$0,36($sp)
sw	$21,40($sp)
sw	$20,48($sp)
sw	$19,56($sp)
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
sw	$18,60($sp)

andi	$2,$17,0x2000
lw	$3,124($sp)
andi	$17,$17,0x8000
lw	$9,128($sp)
li	$5,8			# 0x8
lw	$25,132($sp)
move	$6,$0
li	$7,4			# 0x4
lw	$8,0($3)
lw	$3,0($9)
lw	$9,136($sp)
sw	$25,20($sp)
sw	$22,16($sp)
sw	$0,32($sp)
sw	$9,24($sp)
li	$9,4			# 0x4
sw	$21,40($sp)
sw	$8,44($sp)
sw	$9,36($sp)
sw	$20,48($sp)
sw	$3,52($sp)
sw	$19,56($sp)
sw	$18,60($sp)
sw	$2,64($sp)
sw	$17,68($sp)
lw	$25,140($sp)
sw	$25,28($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,mc_part
1:	jalr	$25
move	$4,$fp

b	$L481
lb	$4,11620($fp)

$L431:
li	$2,1			# 0x1
andi	$22,$22,0x7
b	$L390
or	$7,$7,$2

$L418:
li	$2,1			# 0x1
andi	$22,$22,0x7
b	$L369
or	$11,$11,$2

$L358:
lw	$8,176($sp)
lw	$16,7996($23)
addu	$4,$18,$8
andi	$2,$16,0x1
sll	$5,$4,4
subu	$4,$5,$4
li	$5,131072			# 0x20000
addu	$4,$23,$4
addu	$4,$5,$4
lw	$16,-30016($4)
subu	$16,$2,$16
addiu	$16,$16,1
sll	$16,$16,1
addu	$16,$20,$16
sra	$9,$16,3
bltz	$9,$L412
addiu	$2,$9,8

sra	$4,$19,1
andi	$16,$16,0x7
slt	$2,$2,$4
xori	$2,$2,0x1
b	$L359
or	$7,$7,$2

$L425:
li	$2,1			# 0x1
andi	$22,$22,0x7
b	$L380
or	$7,$7,$2

$L478:
lw	$25,216($sp)
li	$2,9			# 0x9
sra	$19,$19,1
lw	$4,2856($23)
li	$7,9			# 0x9
sw	$8,20($sp)
sra	$21,$25,1
sw	$9,24($sp)
sw	$2,16($sp)
move	$5,$10
sw	$2,236($sp)
sw	$8,252($sp)
sw	$9,244($sp)
sw	$21,28($sp)
lw	$25,%call16(ff_emulated_edge_mc)($28)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$19,32($sp)

li	$7,2			# 0x2
lw	$5,2856($23)
lw	$6,11876($23)
lw	$4,108($sp)
lw	$25,84($sp)
sw	$17,16($sp)
jalr	$25
sw	$16,20($sp)

li	$7,9			# 0x9
lw	$28,72($sp)
move	$5,$20
lw	$2,236($sp)
lw	$8,252($sp)
lw	$9,244($sp)
lw	$4,2856($23)
lw	$6,11876($23)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$2,16($sp)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$21,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$19,32($sp)

b	$L397
lw	$20,2856($23)

$L476:
li	$2,9			# 0x9
lw	$4,2856($23)
sra	$21,$21,1
lw	$25,%call16(ff_emulated_edge_mc)($28)
sra	$18,$18,1
sw	$8,20($sp)
li	$7,9			# 0x9
sw	$9,24($sp)
move	$5,$10
sw	$2,16($sp)
sw	$2,236($sp)
sw	$8,252($sp)
sw	$9,244($sp)
sw	$21,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

li	$7,2			# 0x2
lw	$5,2856($23)
lw	$6,11876($23)
lw	$4,156($sp)
lw	$25,84($sp)
sw	$20,16($sp)
jalr	$25
sw	$16,20($sp)

li	$7,9			# 0x9
lw	$28,72($sp)
move	$5,$17
lw	$2,236($sp)
lw	$8,252($sp)
lw	$9,244($sp)
lw	$4,2856($23)
lw	$6,11876($23)
lw	$25,%call16(ff_emulated_edge_mc)($28)
sw	$2,16($sp)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$21,28($sp)
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
sw	$18,32($sp)

b	$L396
lw	$17,2856($23)

$L479:
sll	$2,$fp,4
lw	$3,196($sp)
sll	$fp,$fp,6
lw	$5,7996($23)
lw	$6,11872($23)
li	$16,64			# 0x40
subu	$fp,$fp,$2
sw	$0,24($sp)
andi	$5,$5,0x1
lw	$4,92($sp)
addu	$2,$fp,$3
lw	$25,104($sp)
li	$7,5			# 0x5
sll	$2,$2,1
addu	$2,$2,$5
lw	$5,224($sp)
addiu	$2,$2,30728
sll	$2,$2,1
addu	$2,$23,$2
lh	$17,4($2)
subu	$16,$16,$17
sw	$17,16($sp)
jalr	$25
sw	$16,20($sp)

li	$7,5			# 0x5
lw	$6,11876($23)
move	$25,$22
lw	$4,156($sp)
lw	$5,108($sp)
sw	$17,16($sp)
sw	$16,20($sp)
jalr	$25
sw	$0,24($sp)

li	$7,5			# 0x5
lw	$6,11876($23)
move	$25,$22
lw	$4,152($sp)
lw	$5,220($sp)
sw	$17,16($sp)
sw	$16,20($sp)
jalr	$25
sw	$0,24($sp)

b	$L363
lw	$28,72($sp)

$L412:
li	$2,1			# 0x1
andi	$16,$16,0x7
b	$L359
or	$7,$7,$2

$L406:
li	$2,1			# 0x1
andi	$16,$16,0x7
b	$L351
or	$7,$7,$2

.set	macro
.set	reorder
.end	hl_motion
.size	hl_motion, .-hl_motion
.section	.text.hl_decode_mb_complex,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	hl_decode_mb_complex
.type	hl_decode_mb_complex, @function
hl_decode_mb_complex:
.frame	$sp,144,$31		# vars= 56, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-144
sw	$22,128($sp)
move	$22,$4
li	$4,131072			# 0x20000
sw	$21,124($sp)
lw	$2,2192($22)
addu	$4,$22,$4
lw	$5,2872($22)
lw	$9,7992($22)
.cprestore	40
lw	$21,9448($4)
sw	$17,108($sp)
sw	$31,140($sp)
sll	$4,$21,2
sw	$fp,136($sp)
sw	$23,132($sp)
addu	$2,$2,$4
sw	$20,120($sp)
sw	$19,116($sp)
sw	$18,112($sp)
sw	$16,104($sp)
lw	$2,0($2)
lw	$17,7996($22)
bne	$5,$0,$L599
sw	$2,56($sp)

lw	$2,11900($22)
sltu	$2,$0,$2
sw	$2,60($sp)
$L497:
lw	$7,192($22)
andi	$4,$9,0x3
lw	$2,196($22)
sll	$4,$4,2
lw	$16,2088($22)
li	$6,4			# 0x4
mul	$3,$17,$7
lw	$18,2092($22)
lw	$19,2096($22)
move	$5,$7
lw	$25,5744($22)
lw	$20,52($22)
addu	$8,$3,$9
mul	$3,$17,$2
sll	$8,$8,4
addu	$16,$16,$8
addu	$2,$3,$9
mul	$3,$7,$4
sll	$2,$2,3
addu	$18,$18,$2
addu	$19,$19,$2
addu	$4,$3,$16
jalr	$25
addiu	$4,$4,64

subu	$5,$19,$18
lw	$2,7992($22)
li	$6,2			# 0x2
lw	$4,196($22)
lw	$25,5744($22)
andi	$2,$2,0x7
mul	$3,$2,$4
addu	$4,$3,$18
jalr	$25
addiu	$4,$4,64

li	$2,65536			# 0x10000
lw	$28,40($sp)
addu	$2,$22,$2
lw	$4,6636($2)
lw	$5,6632($2)
addu	$4,$4,$21
sb	$5,0($4)
lw	$2,-5248($2)
beq	$2,$0,$L498
addiu	$25,$22,11660

lw	$5,192($22)
andi	$2,$17,0x1
lw	$4,196($22)
addiu	$25,$22,11756
sll	$17,$5,1
sll	$21,$4,1
sw	$25,64($sp)
sw	$17,11872($22)
beq	$2,$0,$L499
sw	$21,11876($22)

sll	$2,$5,4
sll	$6,$4,3
subu	$5,$2,$5
subu	$4,$4,$6
subu	$16,$16,$5
addu	$18,$18,$4
addu	$19,$19,$4
$L499:
li	$10,65536			# 0x10000
addu	$10,$22,$10
lw	$2,-5252($10)
beq	$2,$0,$L742
lw	$3,56($sp)

lw	$2,6632($10)
beq	$2,$0,$L743
andi	$2,$3,0x4

addiu	$9,$22,11580
lw	$11,%got(scan8+16)($28)
move	$6,$0
lw	$15,%got(scan8)($28)
li	$13,12288			# 0x3000
andi	$14,$3,0x8
addiu	$11,$11,%lo(scan8+16)
b	$L507
move	$12,$3

$L716:
lb	$3,0($9)
lw	$2,7996($22)
addiu	$4,$3,16
andi	$2,$2,0x1
xor	$3,$4,$2
sll	$2,$3,8
addu	$2,$2,$3
sll	$3,$2,16
addu	$2,$2,$3
sw	$2,0($9)
sw	$2,8($9)
sw	$2,16($9)
sw	$2,24($9)
$L502:
lw	$2,6632($10)
addiu	$6,$6,1
sltu	$2,$6,$2
beq	$2,$0,$L500
addiu	$9,$9,40

$L507:
sll	$2,$6,1
sll	$2,$13,$2
and	$2,$2,$12
beq	$2,$0,$L502
nop

bne	$14,$0,$L716
sll	$7,$6,3

sll	$2,$6,5
addiu	$5,$15,%lo(scan8)
addu	$7,$7,$2
addu	$8,$22,$7
$L505:
lbu	$2,0($5)
addiu	$5,$5,4
addu	$3,$8,$2
addu	$4,$7,$2
lb	$2,11568($3)
addiu	$4,$4,11568
addu	$4,$22,$4
bltz	$2,$L504
addiu	$3,$2,16

lw	$2,7996($22)
andi	$2,$2,0x1
xor	$2,$3,$2
sll	$3,$2,8
addu	$2,$2,$3
andi	$2,$2,0xffff
sh	$2,0($4)
sh	$2,8($4)
$L504:
bne	$5,$11,$L505
nop

lw	$2,6632($10)
addiu	$6,$6,1
sltu	$2,$6,$2
bne	$2,$0,$L507
addiu	$9,$9,40

$L500:
lw	$3,56($sp)
$L742:
andi	$2,$3,0x4
$L743:
beq	$2,$0,$L508
lw	$25,56($sp)

li	$23,131072			# 0x20000
addiu	$20,$23,6944
addiu	$23,$23,7200
addu	$20,$22,$20
addu	$23,$22,$23
$L509:
lw	$25,%call16(memcpy)($28)
move	$5,$20
li	$6,16			# 0x10
addiu	$20,$20,16
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$4,$16

addu	$16,$16,$17
bne	$20,$23,$L509
lw	$28,40($sp)

li	$17,131072			# 0x20000
move	$16,$0
addiu	$17,$17,7264
addu	$17,$22,$17
$L510:
lw	$25,%call16(memcpy)($28)
addu	$4,$18,$16
li	$6,8			# 0x8
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$23

addu	$4,$19,$16
lw	$28,40($sp)
addiu	$5,$23,64
li	$6,8			# 0x8
addiu	$23,$23,8
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$16,$16,$21

bne	$23,$17,$L510
lw	$28,40($sp)

$L574:
li	$2,131072			# 0x20000
$L739:
addu	$2,$22,$2
lw	$2,8736($2)
$L511:
bne	$2,$0,$L588
lw	$25,56($sp)

andi	$2,$25,0x7
beq	$2,$0,$L744
lw	$31,140($sp)

$L588:
li	$4,131072			# 0x20000
lw	$25,3008($22)
addiu	$4,$4,6944
jalr	$25
addu	$4,$22,$4

lw	$31,140($sp)
$L744:
lw	$fp,136($sp)
lw	$23,132($sp)
lw	$22,128($sp)
lw	$21,124($sp)
lw	$20,120($sp)
lw	$19,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
j	$31
addiu	$sp,$sp,144

$L508:
xori	$20,$20,0x1c
sltu	$20,$20,1
andi	$25,$25,0x7
sw	$20,68($sp)
beq	$25,$0,$L512
sw	$25,72($sp)

li	$2,131072			# 0x20000
addu	$2,$22,$2
lw	$2,9456($2)
bne	$2,$0,$L717
li	$4,65536			# 0x10000

$L513:
lw	$2,10760($22)
$L741:
move	$4,$18
lw	$3,56($sp)
addiu	$2,$2,2742
andi	$3,$3,0x1
sll	$2,$2,2
sw	$3,76($sp)
addu	$2,$22,$2
lw	$25,4($2)
jalr	$25
move	$5,$21

move	$4,$19
lw	$2,10760($22)
addiu	$2,$2,2742
sll	$2,$2,2
addu	$2,$22,$2
lw	$25,4($2)
jalr	$25
move	$5,$21

lw	$25,76($sp)
beq	$25,$0,$L520
lw	$28,40($sp)

lw	$2,60($22)
bne	$2,$0,$L745
li	$2,131072			# 0x20000

lw	$3,56($sp)
li	$2,16777216			# 0x1000000
and	$2,$3,$2
bne	$2,$0,$L718
lw	$25,60($sp)

lw	$3,60($sp)
beq	$3,$0,$L529
nop

lw	$25,2988($22)
sw	$25,88($sp)
sw	$25,80($sp)
$L530:
lw	$20,%got(scan8)($28)
li	$2,4			# 0x4
li	$23,131072			# 0x20000
lw	$fp,64($sp)
subu	$2,$2,$17
sw	$21,92($sp)
move	$9,$0
sw	$19,100($sp)
addiu	$23,$23,6944
sw	$18,96($sp)
addiu	$20,$20,%lo(scan8)
sw	$2,84($sp)
move	$19,$fp
addu	$23,$22,$23
move	$21,$9
b	$L538
move	$fp,$20

$L601:
move	$5,$0
$L533:
addiu	$2,$2,2716
move	$4,$18
sll	$2,$2,2
addu	$2,$22,$2
lw	$25,0($2)
jalr	$25
move	$6,$17

lbu	$2,11184($20)
beq	$2,$0,$L532
lw	$28,40($sp)

lw	$3,68($sp)
beq	$3,$0,$L536
li	$5,1			# 0x1

bne	$2,$5,$L746
lw	$25,80($sp)

lh	$2,0($23)
bne	$2,$0,$L719
move	$4,$18

$L746:
move	$4,$18
move	$5,$23
jalr	$25
move	$6,$17

lw	$28,40($sp)
$L532:
addiu	$21,$21,1
li	$2,16			# 0x10
addiu	$19,$19,4
beq	$21,$2,$L720
addiu	$23,$23,32

$L538:
addu	$2,$fp,$21
lw	$8,0($19)
lbu	$3,0($2)
addu	$18,$16,$8
addu	$20,$22,$3
lw	$3,60($sp)
beq	$3,$0,$L531
lb	$2,10816($20)

lw	$5,11888($22)
li	$4,244			# 0xf4
beq	$5,$4,$L721
slt	$4,$2,2

$L531:
li	$5,-5			# 0xfffffffffffffffb
$L752:
li	$6,3			# 0x3
and	$4,$2,$5
bne	$4,$6,$L601
nop

lw	$4,11092($22)
sll	$4,$4,$21
andi	$4,$4,0x8000
bne	$4,$0,$L534
lw	$25,84($sp)

subu	$4,$6,$17
addiu	$5,$sp,48
addu	$4,$18,$4
lbu	$6,0($4)
sll	$4,$6,8
addu	$4,$4,$6
sll	$6,$4,16
addu	$4,$4,$6
b	$L533
sw	$4,48($sp)

$L498:
lw	$17,192($22)
lw	$21,196($22)
sw	$25,64($sp)
sw	$17,11872($22)
b	$L500
sw	$21,11876($22)

$L599:
b	$L497
sw	$0,60($sp)

$L512:
lw	$25,68($sp)
bne	$25,$0,$L722
lw	$25,56($sp)

andi	$2,$25,0x1
bne	$2,$0,$L747
li	$2,131072			# 0x20000

$L541:
li	$2,131072			# 0x20000
lw	$20,%got(scan8)($28)
$L750:
lw	$3,72($sp)
move	$23,$0
addiu	$2,$2,6944
lw	$9,64($sp)
addiu	$20,$20,%lo(scan8)
sw	$18,76($sp)
addu	$fp,$22,$2
sw	$19,80($sp)
sltu	$2,$0,$3
move	$19,$22
move	$18,$9
move	$22,$fp
move	$fp,$2
$L571:
addu	$2,$20,$23
lw	$25,%call16(ff_svq3_add_idct_c)($28)
move	$5,$22
addiu	$23,$23,1
lbu	$2,0($2)
addu	$2,$19,$2
lbu	$2,11184($2)
bne	$2,$0,$L569
move	$6,$17

lh	$2,0($22)
beq	$2,$0,$L570
nop

$L569:
lw	$4,0($18)
lw	$7,2872($19)
sw	$fp,16($sp)
.reloc	1f,R_MIPS_JALR,ff_svq3_add_idct_c
1:	jalr	$25
addu	$4,$16,$4

lw	$28,40($sp)
$L570:
li	$5,16			# 0x10
addiu	$22,$22,32
bne	$23,$5,$L571
addiu	$18,$18,4

move	$22,$19
lw	$18,76($sp)
lw	$19,80($sp)
$L709:
li	$2,131072			# 0x20000
$L747:
addu	$2,$22,$2
$L710:
lw	$2,8736($2)
$L551:
andi	$4,$2,0x30
beq	$4,$0,$L511
lw	$25,60($sp)

sw	$18,48($sp)
beq	$25,$0,$L572
sw	$19,52($sp)

$L597:
lw	$2,72($sp)
beq	$2,$0,$L573
li	$2,244			# 0xf4

lw	$4,11888($22)
beq	$4,$2,$L723
nop

$L573:
li	$17,131072			# 0x20000
lw	$20,%got(scan8)($28)
lw	$2,64($sp)
li	$16,16			# 0x10
addiu	$17,$17,7456
lw	$18,2988($22)
addiu	$20,$20,%lo(scan8)
addiu	$fp,$2,64
addu	$17,$22,$17
addiu	$23,$sp,48
li	$19,24			# 0x18
$L577:
addu	$4,$20,$16
andi	$2,$16,0x4
addu	$3,$23,$2
lbu	$2,0($4)
addiu	$16,$16,1
move	$5,$17
addu	$2,$22,$2
lbu	$2,11184($2)
bne	$2,$0,$L575
move	$6,$21

lh	$2,0($17)
beq	$2,$0,$L576
nop

$L575:
lw	$2,0($fp)
move	$25,$18
lw	$4,0($3)
jalr	$25
addu	$4,$4,$2

$L576:
addiu	$17,$17,32
bne	$16,$19,$L577
addiu	$fp,$fp,4

li	$2,131072			# 0x20000
addu	$2,$22,$2
b	$L511
lw	$2,8736($2)

$L722:
lw	$4,7964($22)
addiu	$2,$22,4400
lw	$8,7968($22)
addiu	$5,$22,4412
lw	$25,%got(hl_motion)($28)
addiu	$6,$22,10584
addiu	$7,$22,10624
sw	$2,20($sp)
sw	$4,16($sp)
move	$4,$22
addiu	$25,$25,%lo(hl_motion)
sw	$5,28($sp)
sw	$6,32($sp)
move	$5,$16
sw	$7,36($sp)
move	$6,$18
sw	$8,24($sp)
.reloc	1f,R_MIPS_JALR,hl_motion
1:	jalr	$25
move	$7,$19

lw	$3,56($sp)
andi	$2,$3,0x1
bne	$2,$0,$L709
lw	$28,40($sp)

lw	$25,56($sp)
$L749:
andi	$2,$25,0x2
beq	$2,$0,$L748
li	$4,131072			# 0x20000

lw	$25,60($sp)
$L751:
beq	$25,$0,$L555
li	$2,244			# 0xf4

lw	$4,11888($22)
beq	$4,$2,$L725
nop

$L556:
li	$2,131072			# 0x20000
lw	$20,%got(scan8)($28)
lw	$7,64($sp)
addiu	$23,$2,6944
sw	$18,60($sp)
addiu	$2,$2,7456
addiu	$fp,$20,%lo(scan8)
addu	$23,$22,$23
addu	$20,$22,$2
move	$18,$22
move	$22,$7
$L558:
lbu	$2,0($fp)
move	$5,$23
addiu	$fp,$fp,1
addu	$2,$18,$2
lbu	$2,11184($2)
bne	$2,$0,$L560
move	$6,$17

lh	$2,0($23)
beq	$2,$0,$L561
nop

$L560:
lw	$4,0($22)
lw	$25,2988($18)
jalr	$25
addu	$4,$16,$4

lw	$28,40($sp)
$L561:
addiu	$23,$23,32
bne	$23,$20,$L558
addiu	$22,$22,4

move	$22,$18
lw	$18,60($sp)
$L559:
li	$2,131072			# 0x20000
addu	$2,$22,$2
lw	$2,8736($2)
andi	$4,$2,0x30
beq	$4,$0,$L511
nop

sw	$18,48($sp)
b	$L597
sw	$19,52($sp)

$L520:
lw	$2,10764($22)
move	$4,$16
addiu	$2,$2,2754
sll	$2,$2,2
addu	$2,$22,$2
lw	$25,0($2)
jalr	$25
move	$5,$17

lw	$2,68($sp)
beq	$2,$0,$L539
lw	$28,40($sp)

lw	$3,60($sp)
beq	$3,$0,$L726
li	$4,65536			# 0x10000

$L540:
li	$2,131072			# 0x20000
addu	$2,$22,$2
lw	$4,9456($2)
beq	$4,$0,$L749
lw	$25,56($sp)

li	$2,65536			# 0x10000
$L737:
addu	$2,$22,$2
lw	$5,-5252($2)
bne	$5,$0,$L727
nop

$L602:
li	$2,1			# 0x1
$L542:
li	$5,2			# 0x2
beq	$4,$5,$L728
li	$6,65536			# 0x10000

lw	$4,7992($22)
lw	$5,7996($22)
addu	$6,$22,$6
slt	$10,$0,$4
lw	$6,-5248($6)
sltu	$6,$0,$6
slt	$5,$6,$5
$L546:
sll	$2,$2,2
addu	$2,$22,$2
beq	$5,$0,$L544
lw	$8,11100($2)

sll	$4,$4,5
nor	$9,$0,$17
addiu	$12,$4,-32
addu	$9,$16,$9
beq	$10,$0,$L547
addu	$11,$8,$12

lw	$6,8($11)
lw	$7,12($11)
lw	$4,-7($9)
lw	$5,-3($9)
sw	$4,8($11)
sw	$5,12($11)
sw	$6,-7($9)
sw	$7,-3($9)
$L547:
addiu	$12,$12,32
lw	$4,9($9)
lw	$5,13($9)
addu	$8,$8,$12
lw	$6,0($8)
lw	$7,4($8)
sw	$6,1($9)
sw	$7,5($9)
lw	$6,8($8)
lw	$7,12($8)
sw	$4,8($8)
sw	$5,12($8)
sw	$6,9($9)
sw	$7,13($9)
lw	$4,7992($22)
lw	$5,160($22)
addiu	$4,$4,1
slt	$5,$4,$5
beq	$5,$0,$L548
nop

lw	$6,11100($2)
sll	$2,$4,5
lw	$5,21($9)
lw	$4,17($9)
addu	$2,$6,$2
lw	$6,0($2)
lw	$7,4($2)
sw	$4,0($2)
sw	$5,4($2)
sw	$6,17($9)
sw	$7,21($9)
$L548:
nor	$2,$0,$21
addu	$6,$18,$2
beq	$10,$0,$L593
addu	$2,$19,$2

lw	$4,-7($6)
lw	$5,-3($6)
lw	$12,16($11)
lw	$13,20($11)
sw	$4,16($11)
sw	$5,20($11)
sw	$12,-7($6)
sw	$13,-3($6)
lw	$12,24($11)
lw	$13,28($11)
lw	$4,-7($2)
lw	$5,-3($2)
sw	$4,24($11)
sw	$5,28($11)
sw	$12,-7($2)
sw	$13,-3($2)
$L593:
lw	$4,1($6)
lw	$5,5($6)
lw	$10,16($8)
lw	$11,20($8)
sw	$4,16($8)
sw	$5,20($8)
sw	$10,1($6)
sw	$11,5($6)
lw	$6,24($8)
lw	$7,28($8)
lw	$4,1($2)
lw	$5,5($2)
sw	$4,24($8)
sw	$5,28($8)
sw	$6,1($2)
sw	$7,5($2)
$L544:
lw	$2,76($sp)
$L740:
bne	$2,$0,$L747
li	$2,131072			# 0x20000

lw	$3,68($sp)
beq	$3,$0,$L750
lw	$20,%got(scan8)($28)

lw	$25,56($sp)
andi	$2,$25,0x2
bne	$2,$0,$L751
lw	$25,60($sp)

li	$4,131072			# 0x20000
$L748:
addu	$2,$22,$4
lw	$2,8736($2)
andi	$5,$2,0xf
beq	$5,$0,$L551
nop

lw	$2,60($sp)
beq	$2,$0,$L564
lw	$3,56($sp)

li	$2,16777216			# 0x1000000
lw	$25,2984($22)
li	$6,1			# 0x1
lw	$5,2988($22)
li	$8,4			# 0x4
and	$2,$3,$2
lw	$20,%got(scan8)($28)
movz	$8,$6,$2
addiu	$4,$4,6944
movz	$25,$5,$2
addiu	$23,$20,%lo(scan8)
addu	$20,$22,$4
lw	$7,64($sp)
sll	$11,$8,2
sll	$10,$8,5
sw	$21,76($sp)
sw	$16,68($sp)
move	$fp,$23
sw	$18,80($sp)
move	$16,$22
sw	$19,84($sp)
move	$22,$20
sw	$25,60($sp)
move	$18,$10
move	$19,$11
move	$21,$8
b	$L567
move	$20,$7

$L566:
subu	$2,$fp,$23
addu	$20,$20,$19
slt	$2,$2,16
beq	$2,$0,$L729
addu	$22,$22,$18

$L567:
lbu	$2,0($fp)
addu	$2,$16,$2
lbu	$2,11184($2)
beq	$2,$0,$L566
addu	$fp,$fp,$21

lw	$4,0($20)
move	$5,$22
lw	$7,68($sp)
move	$6,$17
lw	$25,60($sp)
jalr	$25
addu	$4,$7,$4

b	$L566
lw	$28,40($sp)

$L717:
addu	$4,$22,$4
lw	$5,-5252($4)
bne	$5,$0,$L730
nop

li	$4,1			# 0x1
$L514:
li	$5,2			# 0x2
beq	$2,$5,$L731
nop

li	$6,65536			# 0x10000
lw	$5,7992($22)
lw	$2,7996($22)
addu	$6,$22,$6
slt	$9,$0,$5
lw	$6,-5248($6)
sltu	$6,$0,$6
slt	$6,$6,$2
$L517:
sll	$2,$4,2
addu	$2,$22,$2
beq	$6,$0,$L513
lw	$4,11100($2)

sll	$5,$5,5
nor	$8,$0,$17
addiu	$5,$5,-32
addu	$8,$16,$8
beq	$9,$0,$L518
addu	$12,$4,$5

lw	$10,8($12)
lw	$11,12($12)
lw	$6,-7($8)
lw	$7,-3($8)
sw	$6,8($12)
sw	$7,12($12)
sw	$10,-7($8)
sw	$11,-3($8)
$L518:
addiu	$5,$5,32
lw	$6,1($8)
lw	$7,5($8)
addu	$4,$4,$5
lw	$10,0($4)
lw	$11,4($4)
sw	$6,0($4)
sw	$7,4($4)
sw	$10,1($8)
sw	$11,5($8)
lw	$6,9($8)
lw	$7,13($8)
lw	$10,8($4)
lw	$11,12($4)
sw	$6,8($4)
sw	$7,12($4)
sw	$10,9($8)
sw	$11,13($8)
lw	$5,7992($22)
lw	$6,160($22)
addiu	$5,$5,1
slt	$6,$5,$6
beq	$6,$0,$L519
sll	$5,$5,5

lw	$2,11100($2)
lw	$6,17($8)
lw	$7,21($8)
addu	$5,$2,$5
lw	$10,0($5)
lw	$11,4($5)
sw	$6,0($5)
sw	$7,4($5)
sw	$10,17($8)
sw	$11,21($8)
$L519:
nor	$2,$0,$21
addu	$5,$18,$2
beq	$9,$0,$L591
addu	$2,$19,$2

lw	$6,-7($5)
lw	$7,-3($5)
lw	$8,16($12)
lw	$9,20($12)
sw	$6,16($12)
sw	$7,20($12)
sw	$8,-7($5)
sw	$9,-3($5)
lw	$8,24($12)
lw	$9,28($12)
lw	$6,-7($2)
lw	$7,-3($2)
sw	$6,24($12)
sw	$7,28($12)
sw	$8,-7($2)
sw	$9,-3($2)
$L591:
lw	$6,1($5)
lw	$7,5($5)
lw	$8,16($4)
lw	$9,20($4)
sw	$6,16($4)
sw	$7,20($4)
sw	$8,1($5)
sw	$9,5($5)
lw	$8,24($4)
lw	$9,28($4)
lw	$6,1($2)
lw	$7,5($2)
sw	$6,24($4)
sw	$7,28($4)
sw	$8,1($2)
b	$L513
sw	$9,5($2)

$L718:
beq	$25,$0,$L523
nop

lw	$25,2984($22)
sw	$25,96($sp)
sw	$25,80($sp)
$L524:
li	$10,131072			# 0x20000
lw	$20,%got(scan8)($28)
lw	$fp,64($sp)
move	$8,$0
addiu	$10,$10,6944
sw	$18,88($sp)
addiu	$20,$20,%lo(scan8)
sw	$19,92($sp)
addu	$23,$22,$10
sw	$21,84($sp)
move	$18,$fp
move	$19,$23
move	$fp,$8
move	$23,$20
$L528:
addu	$2,$23,$fp
lw	$8,0($18)
lw	$25,60($sp)
move	$7,$17
lbu	$3,0($2)
addu	$20,$16,$8
move	$4,$20
addu	$3,$22,$3
beq	$25,$0,$L525
lb	$2,10816($3)

lw	$5,11888($22)
li	$6,244			# 0xf4
beq	$5,$6,$L732
slt	$5,$2,2

$L525:
addiu	$2,$2,2730
lw	$5,11084($22)
lw	$6,11092($22)
sll	$2,$2,2
lbu	$21,11184($3)
sll	$5,$5,$fp
addu	$2,$22,$2
sll	$6,$6,$fp
andi	$5,$5,0x8000
lw	$25,4($2)
jalr	$25
andi	$6,$6,0x4000

move	$5,$19
beq	$21,$0,$L526
lw	$28,40($sp)

li	$7,1			# 0x1
move	$4,$20
bne	$21,$7,$L527
move	$6,$17

lh	$2,0($19)
bne	$2,$0,$L733
lw	$25,96($sp)

$L527:
lw	$25,80($sp)
jalr	$25
nop

lw	$28,40($sp)
$L526:
addiu	$fp,$fp,4
$L736:
li	$2,16			# 0x10
addiu	$18,$18,16
bne	$fp,$2,$L528
addiu	$19,$19,128

lw	$21,84($sp)
lw	$18,88($sp)
lw	$19,92($sp)
$L521:
li	$2,131072			# 0x20000
$L745:
addu	$2,$22,$2
lw	$4,9456($2)
beq	$4,$0,$L710
nop

b	$L737
li	$2,65536			# 0x10000

$L555:
li	$6,131072			# 0x20000
lw	$25,10736($22)
addiu	$2,$22,11184
addiu	$6,$6,6944
move	$4,$16
addu	$6,$22,$6
$L712:
lw	$5,64($sp)
move	$7,$17
jalr	$25
sw	$2,16($sp)

li	$2,131072			# 0x20000
addu	$2,$22,$2
lw	$2,8736($2)
andi	$4,$2,0x30
beq	$4,$0,$L511
lw	$28,40($sp)

sw	$18,48($sp)
sw	$19,52($sp)
$L572:
li	$16,131072			# 0x20000
lw	$2,72($sp)
addiu	$4,$16,7456
beq	$2,$0,$L734
addu	$4,$22,$4

li	$6,65536			# 0x10000
lw	$2,10740($22)
lw	$17,%got(chroma_dc_dequant_idct_c.isra.6)($28)
addu	$6,$22,$6
sll	$5,$2,6
addiu	$17,$17,%lo(chroma_dc_dequant_idct_c.isra.6)
lw	$2,-5308($6)
move	$25,$17
addu	$2,$2,$5
.reloc	1f,R_MIPS_JALR,chroma_dc_dequant_idct_c.isra.6
1:	jalr	$25
lw	$5,0($2)

addiu	$4,$16,7584
li	$2,2			# 0x2
addu	$4,$22,$4
$L594:
addiu	$2,$2,15056
lw	$5,10744($22)
move	$25,$17
sll	$2,$2,2
sll	$5,$5,6
addu	$2,$22,$2
lw	$2,0($2)
addu	$2,$2,$5
.reloc	1f,R_MIPS_JALR,chroma_dc_dequant_idct_c.isra.6
1:	jalr	$25
lw	$5,0($2)

lw	$28,40($sp)
lw	$2,68($sp)
beq	$2,$0,$L580
lw	$20,%got(scan8)($28)

lw	$25,10712($22)
li	$23,131072			# 0x20000
lw	$3,64($sp)
li	$16,16			# 0x10
addiu	$23,$23,7456
lw	$17,10704($22)
addiu	$20,$20,%lo(scan8)
addiu	$fp,$3,64
sw	$25,60($sp)
addu	$23,$22,$23
addiu	$19,$sp,48
b	$L583
li	$18,24			# 0x18

$L581:
lh	$2,0($23)
bne	$2,$0,$L735
andi	$2,$16,0x4

addiu	$16,$16,1
$L738:
addiu	$fp,$fp,4
beq	$16,$18,$L574
addiu	$23,$23,32

$L583:
addu	$2,$20,$16
lbu	$2,0($2)
addu	$2,$22,$2
lbu	$2,11184($2)
beq	$2,$0,$L581
andi	$2,$16,0x4

lw	$3,0($fp)
addu	$2,$19,$2
move	$5,$23
move	$6,$21
lw	$4,0($2)
move	$25,$17
jalr	$25
addu	$4,$4,$3

b	$L738
addiu	$16,$16,1

$L735:
lw	$3,0($fp)
addu	$2,$19,$2
lw	$25,60($sp)
move	$5,$23
move	$6,$21
lw	$4,0($2)
jalr	$25
addu	$4,$4,$3

b	$L738
addiu	$16,$16,1

$L580:
li	$fp,131072			# 0x20000
lw	$2,64($sp)
li	$16,16			# 0x10
addiu	$fp,$fp,7456
addiu	$19,$2,64
addu	$fp,$22,$fp
addiu	$20,$20,%lo(scan8)
addiu	$18,$sp,48
li	$23,2			# 0x2
li	$17,24			# 0x18
$L587:
addu	$4,$20,$16
lw	$25,%call16(ff_svq3_add_idct_c)($28)
andi	$2,$16,0x4
addu	$3,$18,$2
lbu	$2,0($4)
addiu	$16,$16,1
move	$5,$fp
addu	$2,$22,$2
lbu	$2,11184($2)
bne	$2,$0,$L585
move	$6,$21

lh	$2,0($fp)
beq	$2,$0,$L586
nop

$L585:
lw	$7,%got(ff_h264_chroma_qp)($28)
lw	$2,2872($22)
lw	$4,0($19)
lw	$3,0($3)
addu	$2,$7,$2
sw	$23,16($sp)
addu	$4,$3,$4
lbu	$7,12($2)
.reloc	1f,R_MIPS_JALR,ff_svq3_add_idct_c
1:	jalr	$25
addiu	$7,$7,-12

lw	$28,40($sp)
$L586:
addiu	$fp,$fp,32
bne	$16,$17,$L587
addiu	$19,$19,4

b	$L739
li	$2,131072			# 0x20000

$L734:
li	$6,65536			# 0x10000
lw	$2,10740($22)
lw	$17,%got(chroma_dc_dequant_idct_c.isra.6)($28)
addu	$6,$22,$6
sll	$5,$2,6
addiu	$17,$17,%lo(chroma_dc_dequant_idct_c.isra.6)
lw	$2,-5296($6)
move	$25,$17
addu	$2,$2,$5
.reloc	1f,R_MIPS_JALR,chroma_dc_dequant_idct_c.isra.6
1:	jalr	$25
lw	$5,0($2)

addiu	$4,$16,7584
li	$2,5			# 0x5
b	$L594
addu	$4,$22,$4

$L727:
lw	$5,7996($22)
andi	$5,$5,0x1
beq	$5,$0,$L543
lw	$2,-5244($2)

bne	$2,$0,$L602
lw	$2,76($sp)

b	$L740
nop

$L730:
lw	$5,7996($22)
andi	$5,$5,0x1
beq	$5,$0,$L515
lw	$4,-5244($4)

bne	$4,$0,$L514
li	$4,1			# 0x1

b	$L741
lw	$2,10760($22)

$L723:
lw	$2,10760($22)
addiu	$4,$2,-1
sltu	$4,$4,2
beq	$4,$0,$L573
addiu	$2,$2,2764

lw	$3,64($sp)
li	$16,131072			# 0x20000
sll	$2,$2,2
addiu	$6,$16,7456
addu	$2,$22,$2
addiu	$5,$3,64
addu	$6,$22,$6
lw	$25,4($2)
move	$4,$18
jalr	$25
move	$7,$21

addiu	$6,$16,7584
lw	$2,10760($22)
move	$4,$19
lw	$25,64($sp)
addu	$6,$22,$6
addiu	$2,$2,2764
addiu	$5,$25,80
sll	$2,$2,2
addu	$2,$22,$2
lw	$25,4($2)
jalr	$25
move	$7,$21

b	$L739
li	$2,131072			# 0x20000

$L726:
lw	$2,2872($22)
lw	$25,%got(h264_luma_dc_dequant_idct_c.isra.5)($28)
addu	$4,$22,$4
sll	$5,$2,6
addiu	$25,$25,%lo(h264_luma_dc_dequant_idct_c.isra.5)
lw	$2,-5312($4)
li	$4,131072			# 0x20000
addiu	$4,$4,6944
addu	$2,$2,$5
addu	$4,$22,$4
.reloc	1f,R_MIPS_JALR,h264_luma_dc_dequant_idct_c.isra.5
1:	jalr	$25
lw	$5,0($2)

b	$L540
lw	$28,40($sp)

$L534:
b	$L533
addu	$5,$18,$25

$L536:
lw	$25,%call16(ff_svq3_add_idct_c)($28)
move	$4,$18
lw	$7,2872($22)
move	$5,$23
sw	$0,16($sp)
.reloc	1f,R_MIPS_JALR,ff_svq3_add_idct_c
1:	jalr	$25
move	$6,$17

b	$L532
lw	$28,40($sp)

$L721:
beq	$4,$0,$L752
li	$5,-5			# 0xfffffffffffffffb

addiu	$2,$2,2760
move	$4,$18
sll	$2,$2,2
move	$5,$23
addu	$2,$22,$2
lw	$25,4($2)
jalr	$25
move	$6,$17

b	$L532
lw	$28,40($sp)

$L720:
lw	$21,92($sp)
lw	$18,96($sp)
b	$L521
lw	$19,100($sp)

$L719:
lw	$25,88($sp)
move	$5,$23
jalr	$25
move	$6,$17

b	$L532
lw	$28,40($sp)

$L732:
beq	$5,$0,$L525
move	$5,$19

addiu	$2,$2,2762
sll	$2,$2,2
addu	$2,$22,$2
lw	$25,4($2)
jalr	$25
move	$6,$17

b	$L526
lw	$28,40($sp)

$L523:
lw	$25,10716($22)
sw	$25,96($sp)
lw	$25,10708($22)
b	$L524
sw	$25,80($sp)

$L529:
lw	$25,10712($22)
sw	$25,88($sp)
lw	$25,10704($22)
b	$L530
sw	$25,80($sp)

$L539:
li	$20,131072			# 0x20000
lw	$25,%call16(ff_svq3_luma_dc_dequant_idct_c)($28)
lw	$5,2872($22)
addiu	$4,$20,6944
addu	$20,$22,$20
.reloc	1f,R_MIPS_JALR,ff_svq3_luma_dc_dequant_idct_c
1:	jalr	$25
addu	$4,$22,$4

lw	$4,9456($20)
beq	$4,$0,$L541
lw	$28,40($sp)

b	$L737
li	$2,65536			# 0x10000

$L731:
lw	$9,10800($22)
lw	$6,10792($22)
b	$L517
lw	$5,7992($22)

$L728:
lw	$10,10800($22)
lw	$5,10792($22)
b	$L546
lw	$4,7992($22)

$L543:
b	$L542
sltu	$2,$2,1

$L515:
b	$L514
sltu	$4,$4,1

$L564:
li	$2,16777216			# 0x1000000
and	$2,$3,$2
beq	$2,$0,$L568
addiu	$6,$4,6944

lw	$25,10728($22)
$L711:
addiu	$2,$22,11184
addu	$6,$22,$6
b	$L712
move	$4,$16

$L729:
lw	$21,76($sp)
move	$22,$16
lw	$18,80($sp)
b	$L559
lw	$19,84($sp)

$L725:
lw	$2,10764($22)
addiu	$4,$2,-1
sltu	$4,$4,2
beq	$4,$0,$L556
addiu	$2,$2,2768

lw	$5,64($sp)
li	$6,131072			# 0x20000
sll	$2,$2,2
addiu	$6,$6,6944
addu	$2,$22,$2
addu	$6,$22,$6
move	$4,$16
lw	$25,0($2)
jalr	$25
move	$7,$17

b	$L559
lw	$28,40($sp)

$L733:
jalr	$25
addiu	$fp,$fp,4

b	$L736
lw	$28,40($sp)

$L568:
b	$L711
lw	$25,10724($22)

.set	macro
.set	reorder
.end	hl_decode_mb_complex
.size	hl_decode_mb_complex, .-hl_decode_mb_complex
.section	.rodata.str1.4
.align	2
$LC7:
.ascii	"intra4x4_pred_mode0 = %x,%x, %d, mb_type_new = %d\012\000"
.align	2
$LC8:
.ascii	"mb_info_h264.intra16x16_pred_mode = %x,mb_info_h264.chro"
.ascii	"ma_pred_mode=%x\012\000"
.section	.text.gather_mb_info_h264,"ax",@progbits
.align	2
.align	5
.globl	gather_mb_info_h264
.set	nomips16
.set	nomicromips
.ent	gather_mb_info_h264
.type	gather_mb_info_h264, @function
gather_mb_info_h264:
.frame	$sp,240,$31		# vars= 168, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$10,7996($4)
addiu	$sp,$sp,-240
lw	$2,168($4)
li	$11,16777216			# 0x1000000
lw	$9,7992($4)
li	$8,1024			# 0x400
lw	$7,2192($4)
li	$5,4			# 0x4
mul	$3,$10,$2
sw	$16,200($sp)
move	$16,$4
sw	$18,208($sp)
sw	$17,204($sp)
sw	$22,224($sp)
.cprestore	24
sw	$31,236($sp)
sw	$fp,232($sp)
addu	$4,$3,$9
sw	$23,228($sp)
sw	$21,220($sp)
sll	$3,$4,2
sw	$20,216($sp)
sw	$19,212($sp)
addu	$3,$7,$3
lw	$17,%got(mb_info_h264)($28)
lw	$6,12828($16)
lw	$18,0($3)
and	$11,$18,$11
andi	$3,$18,0x3
movz	$8,$0,$11
andi	$22,$18,0x800
movz	$5,$0,$3
or	$8,$5,$8
or	$8,$8,$18
beq	$22,$0,$L756
sh	$8,4($17)

move	$3,$0
$L757:
lw	$5,2872($16)
subu	$2,$4,$2
sh	$3,8($17)
li	$3,65536			# 0x10000
addiu	$4,$4,-1
addu	$3,$16,$3
sb	$5,10($17)
addiu	$14,$2,-1
lw	$5,-5276($3)
lw	$3,-5272($3)
addu	$12,$5,$2
lbu	$12,0($12)
beq	$12,$3,$L909
addiu	$20,$2,1

$L831:
addu	$2,$5,$4
move	$13,$0
move	$21,$0
lbu	$2,0($2)
beq	$3,$2,$L910
move	$19,$0

$L832:
addu	$2,$5,$14
move	$12,$0
move	$24,$0
lbu	$2,0($2)
beq	$3,$2,$L911
move	$15,$0

$L833:
addu	$2,$5,$20
move	$4,$0
move	$23,$0
lbu	$2,0($2)
beq	$3,$2,$L912
move	$14,$0

$L834:
move	$2,$0
or	$3,$24,$21
or	$3,$23,$3
or	$2,$2,$3
move	$5,$0
move	$7,$0
bne	$13,$0,$L835
sw	$2,0($17)

$L924:
bne	$19,$0,$L913
nop

li	$2,1			# 0x1
$L763:
bne	$12,$0,$L764
nop

bne	$15,$0,$L914
nop

ori	$2,$2,0x2
$L764:
bne	$4,$0,$L766
nop

bne	$14,$0,$L915
nop

ori	$2,$2,0x4
$L766:
bne	$5,$0,$L927
lw	$4,%got(sde_nnz_dct)($28)

bne	$7,$0,$L916
nop

ori	$2,$2,0x8
$L927:
move	$5,$0
move	$3,$0
sb	$2,6($17)
li	$7,27			# 0x1b
$L770:
lw	$2,0($4)
addiu	$4,$4,4
sltu	$2,$0,$2
sll	$2,$2,$3
addiu	$3,$3,1
bne	$3,$7,$L770
or	$5,$5,$2

li	$2,2			# 0x2
li	$3,100663296			# 0x6000000
movz	$2,$0,$11
li	$12,16777216			# 0x1000000
li	$7,134217728			# 0x8000000
and	$3,$5,$3
and	$12,$5,$12
move	$11,$2
andi	$4,$18,0x2
andi	$2,$8,0x4
movn	$2,$7,$2
sll	$3,$3,5
bne	$4,$0,$L839
sll	$12,$12,4

sll	$11,$11,24
$L773:
li	$7,16711680			# 0xff0000
lw	$4,%got(mb_info_h264)($28)
ori	$7,$7,0xffff
and	$5,$5,$7
li	$7,603979776			# 0x24000000
or	$5,$5,$7
lw	$7,%got(sde_res_di)($28)
or	$3,$5,$3
or	$3,$3,$12
or	$2,$3,$2
lw	$7,0($7)
or	$2,$2,$11
sw	$2,16($17)
sll	$3,$7,1
beq	$7,$0,$L774
sh	$3,20($17)

addiu	$5,$7,18
lw	$3,%got(sde_res_dct)($28)
addiu	$2,$4,36
sll	$5,$5,1
addu	$5,$4,$5
$L775:
lhu	$4,0($3)
addiu	$2,$2,2
addiu	$3,$3,2
bne	$2,$5,$L775
sh	$4,-2($2)

$L774:
lw	$7,%got(bottom_pos)($28)
move	$4,$0
move	$3,$0
li	$5,8			# 0x8
$L776:
addu	$2,$7,$3
lb	$2,0($2)
addu	$2,$16,$2
lbu	$2,11184($2)
sltu	$2,$0,$2
sll	$2,$2,$3
addiu	$3,$3,1
bne	$3,$5,$L776
or	$4,$4,$2

lw	$5,%got(right_pos)($28)
li	$3,8			# 0x8
li	$11,16			# 0x10
$L777:
addu	$2,$5,$3
lb	$2,-8($2)
addu	$2,$16,$2
lbu	$2,11184($2)
sltu	$2,$0,$2
sll	$2,$2,$3
addiu	$3,$3,1
bne	$3,$11,$L777
or	$4,$4,$2

sh	$4,22($17)
move	$11,$0
move	$4,$0
li	$13,16			# 0x10
b	$L779
li	$12,8			# 0x8

$L918:
andi	$2,$2,0xf
sll	$2,$2,$3
addiu	$4,$4,1
beq	$4,$12,$L917
or	$11,$11,$2

$L779:
addu	$2,$7,$4
lb	$2,0($2)
addu	$2,$16,$2
lbu	$2,11184($2)
bne	$2,$13,$L918
sll	$3,$4,2

li	$2,15			# 0xf
addiu	$4,$4,1
sll	$2,$2,$3
bne	$4,$12,$L779
or	$11,$11,$2

$L917:
move	$7,$0
move	$4,$0
li	$13,16			# 0x10
b	$L781
li	$12,8			# 0x8

$L920:
andi	$2,$2,0xf
sll	$2,$2,$3
addiu	$4,$4,1
beq	$4,$12,$L919
or	$7,$7,$2

$L781:
addu	$2,$5,$4
lb	$2,0($2)
addu	$2,$16,$2
lbu	$2,11184($2)
bne	$2,$13,$L920
sll	$3,$4,2

li	$2,15			# 0xf
addiu	$4,$4,1
sll	$2,$2,$3
bne	$4,$12,$L781
or	$7,$7,$2

$L919:
sw	$11,28($17)
beq	$6,$0,$L782
sw	$7,32($17)

li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$2,8240($2)
$L783:
andi	$7,$18,0x1
beq	$7,$0,$L784
sw	$2,24($17)

lb	$3,10829($16)
lb	$6,10836($16)
lb	$2,10845($16)
lb	$12,10852($16)
sll	$3,$3,4
lb	$13,10853($16)
sll	$6,$6,8
lb	$11,10837($16)
sll	$2,$2,4
sll	$12,$12,8
lb	$5,10828($16)
lb	$21,10830($16)
or	$6,$3,$6
lb	$4,10844($16)
or	$2,$2,$12
lb	$20,10846($16)
sll	$11,$11,12
lb	$19,10831($16)
sll	$3,$13,12
lb	$15,10847($16)
or	$4,$2,$4
lb	$14,10838($16)
or	$5,$6,$5
lb	$13,10854($16)
sll	$21,$21,16
lb	$12,10839($16)
sll	$20,$20,16
lb	$6,10855($16)
or	$5,$5,$11
or	$3,$4,$3
lw	$11,10764($16)
sll	$19,$19,20
sll	$2,$15,20
or	$4,$5,$21
or	$3,$3,$20
sll	$14,$14,24
sll	$13,$13,24
or	$2,$3,$2
or	$4,$4,$19
sll	$5,$12,28
sll	$6,$6,28
or	$3,$4,$14
or	$2,$2,$13
or	$5,$3,$5
or	$6,$2,$6
$L785:
lw	$3,%got(sde_luma_neighbor)($28)
lw	$2,%got(sde_chroma_neighbor)($28)
lw	$12,10760($16)
lbu	$4,0($3)
lbu	$2,0($2)
lw	$3,%got(mpFrame)($28)
sw	$5,852($17)
sltu	$2,$0,$2
sw	$6,856($17)
sb	$11,860($17)
sb	$2,863($17)
li	$2,-1			# 0xffffffffffffffff
sb	$12,861($17)
sb	$4,862($17)
lh	$3,0($3)
beq	$3,$2,$L921
lw	$19,%got(mb_info_h264)($28)

$L786:
lw	$2,%got(scan8)($28)
$L928:
addiu	$12,$sp,160
$L929:
move	$8,$0
li	$7,-1			# 0xffffffffffffffff
addiu	$23,$2,%lo(scan8)
li	$9,-2			# 0xfffffffffffffffe
li	$13,18			# 0x12
li	$10,17			# 0x11
li	$6,4			# 0x4
li	$11,1			# 0x1
$L787:
sll	$5,$8,3
sll	$3,$8,5
move	$2,$5
addu	$5,$2,$3
move	$2,$0
addu	$5,$16,$5
move	$4,$12
$L791:
sll	$3,$2,2
addu	$3,$3,$23
lbu	$3,0($3)
addu	$3,$5,$3
lb	$3,11568($3)
beq	$3,$7,$L922
nop

beq	$3,$9,$L923
nop

bltz	$3,$L789
nop

sb	$3,0($4)
$L789:
addiu	$2,$2,1
bne	$2,$6,$L791
addiu	$4,$4,1

beq	$8,$11,$L792
addiu	$12,$12,4

b	$L787
li	$8,1			# 0x1

$L756:
beq	$6,$0,$L758
li	$3,131072			# 0x20000

li	$5,131072			# 0x20000
sll	$3,$4,1
addu	$5,$16,$5
subu	$2,$4,$2
addiu	$4,$4,-1
lw	$5,8732($5)
addiu	$14,$2,-1
addu	$3,$5,$3
lw	$5,2872($16)
lh	$3,0($3)
sb	$5,10($17)
sh	$3,8($17)
li	$3,65536			# 0x10000
addu	$3,$16,$3
lw	$5,-5276($3)
lw	$3,-5272($3)
addu	$12,$5,$2
lbu	$12,0($12)
bne	$12,$3,$L831
addiu	$20,$2,1

$L909:
sll	$2,$2,2
addu	$2,$7,$2
lw	$19,0($2)
addu	$2,$5,$4
lbu	$2,0($2)
sltu	$21,$0,$19
bne	$3,$2,$L832
andi	$13,$19,0x7

$L910:
sll	$4,$4,2
addu	$2,$5,$14
addu	$4,$7,$4
lbu	$2,0($2)
lw	$15,0($4)
sltu	$24,$0,$15
bne	$3,$2,$L833
andi	$12,$15,0x7

$L911:
sll	$2,$14,2
addu	$2,$7,$2
lw	$14,0($2)
addu	$2,$5,$20
lbu	$2,0($2)
sltu	$23,$0,$14
bne	$3,$2,$L834
andi	$4,$14,0x7

$L912:
sll	$20,$20,2
or	$3,$24,$21
addu	$7,$7,$20
or	$3,$23,$3
lw	$7,0($7)
sltu	$2,$0,$7
or	$2,$2,$3
andi	$5,$7,0x7
beq	$13,$0,$L924
sw	$2,0($17)

$L835:
b	$L763
move	$2,$0

$L922:
b	$L789
sb	$10,0($4)

$L923:
b	$L789
sb	$13,0($4)

$L792:
lbu	$8,160($sp)
li	$6,65536			# 0x10000
lw	$9,%got(mb_info_h264)($28)
addu	$6,$16,$6
sb	$8,864($17)
lbu	$8,161($sp)
lhu	$7,-5240($6)
lhu	$5,-5238($6)
lhu	$4,-5236($6)
sb	$8,865($17)
lbu	$8,162($sp)
lhu	$3,-5234($6)
lw	$2,6632($6)
sh	$7,872($17)
sb	$8,866($17)
lbu	$8,163($sp)
sh	$5,874($17)
sh	$4,876($17)
sh	$3,878($17)
sb	$8,867($17)
lbu	$8,164($sp)
sb	$8,868($17)
lbu	$8,165($sp)
sb	$8,869($17)
lbu	$8,166($sp)
sb	$8,870($17)
lbu	$8,167($sp)
beq	$2,$0,$L793
sb	$8,871($17)

li	$7,60296			# 0xeb88
sw	$22,168($sp)
andi	$24,$18,0x20
addu	$7,$16,$7
andi	$21,$18,0x8
andi	$25,$18,0x10
andi	$18,$18,0x40
sw	$7,176($sp)
addiu	$3,$16,11296
move	$4,$0
sw	$18,172($sp)
move	$5,$0
move	$fp,$0
move	$2,$0
addiu	$20,$sp,32
move	$31,$6
move	$18,$24
$L803:
beq	$21,$0,$L794
sll	$6,$4,2

lhu	$8,0($3)
lhu	$7,2($3)
addiu	$2,$2,1
addu	$6,$20,$6
addiu	$4,$4,1
andi	$2,$2,0xffff
sh	$8,0($6)
sh	$7,2($6)
$L795:
lw	$6,6632($31)
addiu	$5,$5,1
sltu	$6,$5,$6
bne	$6,$0,$L803
addiu	$3,$3,160

lw	$22,168($sp)
sh	$2,880($17)
beq	$2,$0,$L828
sh	$fp,882($17)

sll	$4,$2,2
lw	$3,%got(mb_info_h264)($28)
addiu	$2,$sp,34
addu	$4,$sp,$4
addiu	$3,$3,1012
addiu	$4,$4,34
$L805:
lhu	$6,-2($2)
addiu	$2,$2,4
addiu	$3,$3,4
lhu	$5,-4($2)
sh	$6,-4($3)
bne	$2,$4,$L805
sh	$5,-2($3)

$L828:
lw	$13,%got(bottom_pos)($28)
addiu	$17,$17,16
lw	$19,%got(mb_info_h264)($28)
move	$5,$13
sw	$13,168($sp)
move	$4,$19
$L806:
lb	$2,0($5)
addiu	$4,$4,4
addiu	$5,$5,1
addiu	$3,$2,2812
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$16,$2
addu	$3,$16,$3
lh	$3,0($3)
sh	$3,880($4)
lh	$2,11250($2)
bne	$4,$17,$L806
sh	$2,882($4)

lw	$3,%got(right_pos)($28)
lw	$4,%got(mb_info_h264)($28)
move	$5,$3
sw	$3,172($sp)
$L807:
lb	$2,0($5)
addiu	$4,$4,4
addiu	$5,$5,1
addiu	$3,$2,2812
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$16,$2
addu	$3,$16,$3
lh	$3,0($3)
sh	$3,896($4)
lh	$2,11250($2)
bne	$4,$17,$L807
sh	$2,898($4)

lw	$23,%got(mb_info_h264)($28)
li	$20,4			# 0x4
lw	$18,%got(bottom_pos)($28)
$L808:
lb	$21,0($18)
li	$6,34960			# 0x8890
move	$fp,$0
addu	$21,$21,$6
sll	$21,$21,2
addu	$21,$16,$21
$L811:
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($21)

move	$4,$0
andi	$3,$2,0x3f
lw	$28,24($sp)
andi	$2,$2,0xffc0
ori	$5,$3,0x40
bne	$22,$0,$L810
movn	$3,$5,$2

sll	$4,$3,16
sra	$4,$4,16
$L810:
addu	$2,$23,$fp
addiu	$fp,$fp,2
sh	$4,916($2)
bne	$fp,$20,$L811
addiu	$21,$21,2

addiu	$23,$23,4
bne	$17,$23,$L808
addiu	$18,$18,1

lw	$23,%got(mb_info_h264)($28)
li	$20,4			# 0x4
lw	$18,%got(right_pos)($28)
$L812:
lb	$21,0($18)
li	$11,34960			# 0x8890
move	$fp,$0
addu	$21,$21,$11
sll	$21,$21,2
addu	$21,$16,$21
$L815:
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($21)

move	$4,$0
andi	$3,$2,0x3f
lw	$28,24($sp)
andi	$2,$2,0xffc0
ori	$5,$3,0x40
bne	$22,$0,$L814
movn	$3,$5,$2

sll	$4,$3,16
sra	$4,$4,16
$L814:
addu	$2,$23,$fp
addiu	$fp,$fp,2
sh	$4,932($2)
bne	$fp,$20,$L815
addiu	$21,$21,2

addiu	$23,$23,4
bne	$17,$23,$L812
addiu	$18,$18,1

li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$3,-5264($2)
li	$2,3			# 0x3
beq	$3,$2,$L925
lw	$31,236($sp)

lw	$fp,232($sp)
lw	$23,228($sp)
lw	$22,224($sp)
lw	$21,220($sp)
lw	$20,216($sp)
lw	$19,212($sp)
lw	$18,208($sp)
lw	$17,204($sp)
lw	$16,200($sp)
j	$31
addiu	$sp,$sp,240

$L794:
beq	$25,$0,$L796
addiu	$6,$4,1

lhu	$11,0($3)
sll	$7,$4,2
lhu	$10,2($3)
sll	$6,$6,2
lhu	$9,64($3)
lhu	$8,66($3)
addu	$7,$20,$7
addu	$6,$20,$6
addiu	$2,$2,2
sh	$11,0($7)
sh	$10,2($7)
ori	$fp,$fp,0x1
andi	$2,$2,0xffff
sh	$9,0($6)
sh	$8,2($6)
b	$L795
addiu	$4,$4,2

$L796:
beq	$18,$0,$L797
lw	$6,172($sp)

addiu	$6,$4,1
lhu	$11,0($3)
sll	$7,$4,2
lhu	$10,2($3)
sll	$6,$6,2
lhu	$9,8($3)
lhu	$8,10($3)
addu	$7,$20,$7
addu	$6,$20,$6
addiu	$2,$2,2
sh	$11,0($7)
sh	$10,2($7)
ori	$fp,$fp,0x2
andi	$2,$2,0xffff
sh	$9,0($6)
sh	$8,2($6)
b	$L795
addiu	$4,$4,2

$L797:
beq	$6,$0,$L795
lw	$22,176($sp)

sll	$7,$5,3
sll	$6,$5,5
li	$15,2			# 0x2
addu	$24,$7,$6
lhu	$6,0($22)
li	$8,1			# 0x1
ori	$fp,$fp,0x3
andi	$9,$6,0x8
beq	$9,$0,$L798
move	$7,$23

$L926:
lbu	$6,0($7)
sll	$10,$4,2
addiu	$2,$2,1
addu	$10,$20,$10
addu	$6,$24,$6
addiu	$4,$4,1
addiu	$9,$6,2812
sll	$6,$6,2
sll	$9,$9,2
addu	$6,$16,$6
addu	$9,$16,$9
andi	$2,$2,0xffff
lhu	$11,11250($6)
lhu	$6,0($9)
sh	$11,2($10)
sh	$6,0($10)
$L799:
addiu	$8,$8,1
li	$13,5			# 0x5
addiu	$22,$22,2
addiu	$7,$7,4
beq	$8,$13,$L795
addiu	$15,$15,2

lhu	$6,0($22)
andi	$9,$6,0x8
bne	$9,$0,$L926
nop

$L798:
andi	$9,$6,0x10
beq	$9,$0,$L800
andi	$6,$6,0x20

lbu	$11,0($7)
addiu	$9,$4,1
lbu	$6,2($7)
sll	$10,$4,2
sll	$9,$9,2
addu	$11,$24,$11
addu	$6,$24,$6
addiu	$14,$11,2812
sll	$11,$11,2
sll	$14,$14,2
addiu	$12,$6,2812
addu	$13,$16,$11
addu	$14,$16,$14
sll	$12,$12,2
sll	$6,$6,2
lhu	$13,11250($13)
lhu	$14,0($14)
addu	$12,$16,$12
addu	$11,$16,$6
li	$19,1			# 0x1
$L906:
lhu	$12,0($12)
sll	$6,$19,$15
lhu	$11,11250($11)
addu	$10,$20,$10
addu	$9,$20,$9
addiu	$2,$2,2
sh	$14,0($10)
or	$fp,$6,$fp
sh	$13,2($10)
andi	$2,$2,0xffff
sh	$12,0($9)
andi	$fp,$fp,0xffff
sh	$11,2($9)
b	$L799
addiu	$4,$4,2

$L800:
beq	$6,$0,$L801
lbu	$11,0($7)

addu	$11,$24,$11
lbu	$6,1($7)
addiu	$9,$4,1
addiu	$14,$11,2812
addu	$6,$24,$6
sll	$11,$11,2
sll	$14,$14,2
addiu	$12,$6,2812
addu	$13,$16,$11
addu	$14,$16,$14
sll	$12,$12,2
sll	$6,$6,2
lhu	$13,11250($13)
sll	$10,$4,2
lhu	$14,0($14)
sll	$9,$9,2
addu	$12,$16,$12
addu	$11,$16,$6
b	$L906
li	$19,2			# 0x2

$L784:
lw	$11,10764($16)
move	$6,$0
b	$L785
move	$5,$11

$L782:
b	$L783
lw	$2,10340($16)

$L839:
b	$L773
li	$11,50331648			# 0x3000000

$L801:
addiu	$6,$4,1
lbu	$10,1($7)
lbu	$9,2($7)
addiu	$13,$4,2
addiu	$19,$4,3
sw	$6,180($sp)
addu	$10,$24,$10
sw	$13,184($sp)
addu	$11,$24,$11
sw	$19,188($sp)
addu	$9,$24,$9
lbu	$6,3($7)
addiu	$14,$10,2812
sll	$10,$10,2
addiu	$19,$11,2812
addu	$6,$24,$6
addiu	$13,$9,2812
sll	$11,$11,2
addu	$10,$16,$10
addiu	$12,$6,2812
sll	$13,$13,2
sll	$6,$6,2
lhu	$10,11250($10)
addu	$11,$16,$11
sll	$19,$19,2
sll	$9,$9,2
addu	$13,$16,$13
lhu	$11,11250($11)
addu	$6,$16,$6
sh	$10,192($sp)
sll	$12,$12,2
addu	$19,$16,$19
lhu	$13,0($13)
addu	$9,$16,$9
lhu	$6,11250($6)
addu	$12,$16,$12
sh	$11,198($sp)
lhu	$19,0($19)
sll	$10,$4,2
lhu	$9,11250($9)
sll	$14,$14,2
lw	$11,180($sp)
addu	$10,$20,$10
lhu	$12,0($12)
addu	$14,$16,$14
sh	$13,194($sp)
addiu	$2,$2,4
sh	$19,0($10)
addiu	$4,$4,4
sh	$9,196($sp)
sll	$9,$11,2
sh	$12,180($sp)
li	$11,3			# 0x3
lw	$13,184($sp)
addu	$9,$20,$9
lhu	$19,198($sp)
andi	$2,$2,0xffff
sh	$6,184($sp)
lw	$6,188($sp)
sll	$12,$13,2
lhu	$14,0($14)
sh	$19,2($10)
addu	$12,$20,$12
sll	$13,$6,2
lhu	$19,196($sp)
sll	$6,$11,$15
lhu	$11,194($sp)
sh	$14,0($9)
addu	$13,$20,$13
or	$fp,$6,$fp
lhu	$6,192($sp)
andi	$fp,$fp,0xffff
sh	$6,2($9)
sh	$11,0($12)
sh	$19,2($12)
lhu	$6,180($sp)
lhu	$11,184($sp)
sh	$6,0($13)
b	$L799
sh	$11,2($13)

$L915:
lw	$3,12880($16)
beq	$3,$0,$L766
nop

b	$L766
ori	$2,$2,0x4

$L914:
lw	$3,12880($16)
beq	$3,$0,$L764
nop

b	$L764
ori	$2,$2,0x2

$L916:
lw	$3,12880($16)
beq	$3,$0,$L927
lw	$4,%got(sde_nnz_dct)($28)

b	$L927
ori	$2,$2,0x8

$L758:
addu	$3,$16,$3
b	$L757
lh	$3,8736($3)

$L913:
lw	$2,12880($16)
b	$L763
sltu	$2,$0,$2

$L925:
lw	$4,%got(mb_info_h264)($28)
lw	$5,%got(bottom_pos)($28)
$L818:
lb	$2,0($5)
addiu	$4,$4,4
addiu	$5,$5,1
addiu	$3,$2,2852
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$16,$2
addu	$3,$16,$3
lh	$3,0($3)
sh	$3,944($4)
lh	$2,11410($2)
bne	$17,$4,$L818
sh	$2,946($4)

lw	$4,%got(mb_info_h264)($28)
lw	$5,%got(right_pos)($28)
$L819:
lb	$2,0($5)
addiu	$4,$4,4
addiu	$5,$5,1
addiu	$3,$2,2852
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$16,$2
addu	$3,$16,$3
lh	$3,0($3)
sh	$3,960($4)
lh	$2,11410($2)
bne	$17,$4,$L819
sh	$2,962($4)

lw	$23,%got(mb_info_h264)($28)
li	$21,35000			# 0x88b8
li	$20,4			# 0x4
lw	$fp,168($sp)
sw	$19,168($sp)
move	$19,$23
$L820:
lb	$18,0($fp)
move	$23,$0
addu	$18,$18,$21
sll	$18,$18,2
addu	$18,$16,$18
$L823:
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($18)

move	$4,$0
andi	$3,$2,0x3f
lw	$28,24($sp)
andi	$2,$2,0xffc0
ori	$6,$3,0x40
bne	$22,$0,$L822
movn	$3,$6,$2

sll	$4,$3,16
sra	$4,$4,16
$L822:
addu	$2,$19,$23
addiu	$23,$23,2
sh	$4,980($2)
bne	$23,$20,$L823
addiu	$18,$18,2

addiu	$19,$19,4
bne	$17,$19,$L820
addiu	$fp,$fp,1

lw	$19,168($sp)
li	$21,35000			# 0x88b8
lw	$23,172($sp)
li	$20,4			# 0x4
$L904:
lb	$18,0($23)
move	$fp,$0
addu	$18,$18,$21
sll	$18,$18,2
addu	$18,$16,$18
$L827:
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
lh	$4,0($18)

move	$4,$0
andi	$3,$2,0x3f
lw	$28,24($sp)
andi	$2,$2,0xffc0
ori	$5,$3,0x40
bne	$22,$0,$L826
movn	$3,$5,$2

sll	$4,$3,16
sra	$4,$4,16
$L826:
addu	$2,$19,$fp
addiu	$fp,$fp,2
sh	$4,996($2)
bne	$fp,$20,$L827
addiu	$18,$18,2

addiu	$19,$19,4
bne	$17,$19,$L904
addiu	$23,$23,1

lw	$31,236($sp)
lw	$fp,232($sp)
lw	$23,228($sp)
lw	$22,224($sp)
lw	$21,220($sp)
lw	$20,216($sp)
lw	$19,212($sp)
lw	$18,208($sp)
lw	$17,204($sp)
lw	$16,200($sp)
j	$31
addiu	$sp,$sp,240

$L921:
bne	$9,$0,$L928
lw	$2,%got(scan8)($28)

bne	$10,$0,$L929
addiu	$12,$sp,160

lw	$4,%got($LC7)($28)
andi	$8,$8,0x1
lw	$25,%call16(printf)($28)
sw	$8,16($sp)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC7)

lw	$28,24($sp)
lbu	$5,860($19)
lbu	$6,861($19)
lw	$4,%got($LC8)($28)
lw	$25,%call16(printf)($28)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC8)

b	$L786
lw	$28,24($sp)

$L793:
sh	$0,880($9)
b	$L828
sh	$0,882($9)

.set	macro
.set	reorder
.end	gather_mb_info_h264
.size	gather_mb_info_h264, .-gather_mb_info_h264
.section	.text.wait_aux_end,"ax",@progbits
.align	2
.align	5
.globl	wait_aux_end
.set	nomips16
.set	nomicromips
.ent	wait_aux_end
.type	wait_aux_end, @function
wait_aux_end:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
$L931:
lw	$2,0($4)
.set	noreorder
.set	nomacro
bne	$2,$5,$L931
move	$2,$0
.set	macro
.set	reorder

#APP
# 14 "jzsoc/eyer_driver.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
j	$31
.end	wait_aux_end
.size	wait_aux_end, .-wait_aux_end
.section	.rodata.str1.4
.align	2
$LC9:
.ascii	"r+b\000"
.align	2
$LC10:
.ascii	" error while open %s \012\000"
.section	.text.load_aux_pro_bin,"ax",@progbits
.align	2
.align	5
.globl	load_aux_pro_bin
.set	nomips16
.set	nomicromips
.ent	load_aux_pro_bin
.type	load_aux_pro_bin, @function
load_aux_pro_bin:
.frame	$sp,4136,$31		# vars= 4096, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-4136
lw	$25,%call16(fopen64)($28)
sw	$18,4128($sp)
move	$18,$5
.cprestore	16
sw	$17,4124($sp)
sw	$16,4120($sp)
move	$16,$4
sw	$31,4132($sp)
lw	$5,%got($LC9)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fopen64
1:	jalr	$25
addiu	$5,$5,%lo($LC9)
.set	macro
.set	reorder

lw	$28,16($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L941
move	$17,$2
.set	macro
.set	reorder

$L934:
addiu	$16,$sp,24
$L936:
lw	$25,%call16(fread)($28)
li	$5,4			# 0x4
li	$6,1024			# 0x400
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fread
1:	jalr	$25
move	$7,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L937
lw	$28,16($sp)
.set	macro
.set	reorder

$L942:
.set	noreorder
.set	nomacro
blez	$2,$L936
sll	$11,$2,2
.set	macro
.set	reorder

subu	$9,$18,$16
addu	$10,$16,$11
move	$3,$16
$L935:
lw	$6,0($3)
addu	$8,$9,$3
#APP
# 33 "jzsoc/eyer_driver.c" 1
sw	 $6,0($8)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$3,4
.set	noreorder
.set	nomacro
bne	$3,$10,$L935
lw	$25,%call16(fread)($28)
.set	macro
.set	reorder

li	$5,4			# 0x4
li	$6,1024			# 0x400
move	$4,$16
move	$7,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fread
1:	jalr	$25
addu	$18,$11,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L942
lw	$28,16($sp)
.set	macro
.set	reorder

$L937:
lw	$25,%call16(fclose)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,fclose
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

lw	$31,4132($sp)
lw	$18,4128($sp)
lw	$17,4124($sp)
lw	$16,4120($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,4136
.set	macro
.set	reorder

$L941:
lw	$4,%got($LC10)($28)
move	$5,$16
lw	$25,%call16(printf)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC10)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L934
lw	$28,16($sp)
.set	macro
.set	reorder

.end	load_aux_pro_bin
.size	load_aux_pro_bin, .-load_aux_pro_bin
.section	.text.do_get_phy_addr,"ax",@progbits
.align	2
.align	5
.globl	do_get_phy_addr
.set	nomips16
.set	nomicromips
.ent	do_get_phy_addr
.type	do_get_phy_addr, @function
do_get_phy_addr:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(tcsm0_base)($28)
lw	$3,0($2)
sltu	$2,$4,$3
bne	$2,$0,$L968
lw	$2,%got(tcsm1_base)($28)

addiu	$2,$3,16384
sltu	$2,$4,$2
bne	$2,$0,$L958
li	$2,321585152			# 0x132b0000

lw	$2,%got(tcsm1_base)($28)
$L968:
lw	$5,0($2)
sltu	$2,$4,$5
bne	$2,$0,$L969
lw	$2,%got(sram_base)($28)

li	$2,49152			# 0xc000
addu	$2,$5,$2
sltu	$2,$4,$2
bne	$2,$0,$L959
li	$2,321650688			# 0x132c0000

lw	$2,%got(sram_base)($28)
$L969:
lw	$6,0($2)
sltu	$2,$4,$6
bne	$2,$0,$L970
lw	$2,%got(vpu_base)($28)

addiu	$2,$6,28672
sltu	$2,$4,$2
bne	$2,$0,$L960
li	$2,321847296			# 0x132f0000

lw	$2,%got(vpu_base)($28)
$L970:
lw	$7,0($2)
sltu	$2,$4,$7
beq	$2,$0,$L961
addiu	$2,$7,4096

$L948:
lw	$2,%got(gp0_base)($28)
lw	$8,0($2)
sltu	$2,$4,$8
bne	$2,$0,$L971
lw	$2,%got(gp1_base)($28)

addiu	$2,$8,4096
sltu	$2,$4,$2
bne	$2,$0,$L962
li	$2,320929792			# 0x13210000

lw	$2,%got(gp1_base)($28)
$L971:
lw	$9,0($2)
sltu	$2,$4,$9
bne	$2,$0,$L972
lw	$2,%got(gp2_base)($28)

addiu	$2,$9,4096
sltu	$2,$4,$2
beq	$2,$0,$L950
subu	$9,$4,$9

li	$2,320995328			# 0x13220000
j	$31
addu	$2,$9,$2

$L961:
sltu	$2,$4,$2
beq	$2,$0,$L948
li	$2,320864256			# 0x13200000

subu	$7,$4,$7
j	$31
addu	$2,$7,$2

$L950:
lw	$2,%got(gp2_base)($28)
$L972:
lw	$10,0($2)
sltu	$2,$4,$10
bne	$2,$0,$L973
lw	$2,%got(mc_base)($28)

addiu	$2,$10,4096
sltu	$2,$4,$2
beq	$2,$0,$L951
subu	$10,$4,$10

li	$2,321060864			# 0x13230000
j	$31
addu	$2,$10,$2

$L951:
lw	$2,%got(mc_base)($28)
$L973:
lw	$11,0($2)
sltu	$2,$4,$11
beq	$2,$0,$L963
addiu	$2,$11,4096

$L952:
lw	$2,%got(dblk0_base)($28)
lw	$12,0($2)
sltu	$2,$4,$12
bne	$2,$0,$L974
lw	$2,%got(dblk1_base)($28)

addiu	$2,$12,4096
sltu	$2,$4,$2
bne	$2,$0,$L964
li	$2,321323008			# 0x13270000

lw	$2,%got(dblk1_base)($28)
$L974:
lw	$13,0($2)
sltu	$2,$4,$13
bne	$2,$0,$L975
lw	$2,%got(vmau_base)($28)

addiu	$2,$13,4096
sltu	$2,$4,$2
bne	$2,$0,$L965
li	$2,321716224			# 0x132d0000

lw	$2,%got(vmau_base)($28)
$L975:
lw	$14,0($2)
sltu	$2,$4,$14
bne	$2,$0,$L976
lw	$2,%got(aux_base)($28)

li	$2,61440			# 0xf000
addu	$2,$14,$2
sltu	$2,$4,$2
bne	$2,$0,$L966
li	$2,321388544			# 0x13280000

lw	$2,%got(aux_base)($28)
$L976:
lw	$15,0($2)
sltu	$2,$4,$15
bne	$2,$0,$L977
lw	$2,%got(sde_base)($28)

addiu	$2,$15,4096
sltu	$2,$4,$2
beq	$2,$0,$L956
subu	$15,$4,$15

li	$2,321519616			# 0x132a0000
j	$31
addu	$2,$15,$2

$L958:
subu	$3,$4,$3
j	$31
addu	$2,$3,$2

$L959:
subu	$5,$4,$5
j	$31
addu	$2,$5,$2

$L960:
subu	$6,$4,$6
j	$31
addu	$2,$6,$2

$L963:
sltu	$2,$4,$2
beq	$2,$0,$L952
li	$2,321191936			# 0x13250000

subu	$11,$4,$11
j	$31
addu	$2,$11,$2

$L962:
subu	$8,$4,$8
j	$31
addu	$2,$8,$2

$L956:
lw	$2,%got(sde_base)($28)
$L977:
lw	$24,0($2)
sltu	$2,$4,$24
bne	$2,$0,$L978
lw	$25,%call16(get_phy_addr)($28)

addiu	$2,$24,4096
sltu	$2,$4,$2
bne	$2,$0,$L967
subu	$24,$4,$24

$L978:
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jr	$25
nop

$L965:
subu	$13,$4,$13
j	$31
addu	$2,$13,$2

$L966:
subu	$14,$4,$14
j	$31
addu	$2,$14,$2

$L967:
li	$2,321454080			# 0x13290000
j	$31
addu	$2,$24,$2

$L964:
subu	$12,$4,$12
j	$31
addu	$2,$12,$2

.set	macro
.set	reorder
.end	do_get_phy_addr
.size	do_get_phy_addr, .-do_get_phy_addr
.section	.text.motion_init_h264,"ax",@progbits
.align	2
.align	5
.globl	motion_init_h264
.set	nomips16
.set	nomicromips
.ent	motion_init_h264
.type	motion_init_h264, @function
motion_init_h264:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$6,%got(tcsm1_base)($28)
sll	$3,$5,4
sll	$5,$5,6
lw	$11,%got(mc_base)($28)
sll	$7,$4,6
addu	$5,$3,$5
lw	$3,%got(IntpFMT)($28)
lw	$6,0($6)
sll	$2,$4,4
sll	$9,$5,3
addu	$2,$2,$7
addiu	$8,$6,10496
subu	$9,$9,$5
lw	$5,%got(motion_dsa)($28)
addiu	$sp,$sp,-8
sll	$7,$2,3
sw	$17,4($sp)
addiu	$3,$3,%lo(IntpFMT)
sw	$16,0($sp)
addiu	$6,$6,10240
sw	$8,0($5)
subu	$2,$7,$2
lw	$5,%got(motion_iwta)($28)
addu	$9,$3,$9
addu	$2,$3,$2
move	$10,$0
li	$13,128			# 0x80
sw	$6,0($5)
$L980:
lbu	$7,5($2)
lbu	$24,0($2)
lbu	$12,1($2)
lbu	$8,33($2)
sll	$7,$7,31
lbu	$17,7($2)
andi	$24,$24,0x3
lbu	$5,25($2)
sll	$24,$24,28
andi	$8,$8,0x1
lbu	$16,27($2)
andi	$6,$12,0x1
lbu	$15,29($2)
or	$7,$8,$7
lbu	$14,31($2)
sll	$6,$6,27
lw	$8,0($11)
andi	$17,$17,0x1
lw	$12,%got(mc_base)($28)
or	$7,$7,$24
sll	$17,$17,24
or	$6,$7,$6
sll	$5,$5,16
andi	$16,$16,0xf
or	$6,$6,$17
sll	$16,$16,8
andi	$3,$15,0x1
or	$5,$6,$5
andi	$7,$14,0x1
sll	$3,$3,2
or	$5,$5,$16
sll	$7,$7,1
addiu	$6,$8,1280
or	$3,$5,$3
or	$3,$3,$7
addu	$5,$6,$10
#APP
# 21 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lbu	$6,6($2)
lbu	$7,2($2)
lbu	$8,34($2)
lbu	$17,3($2)
sll	$6,$6,31
lbu	$14,4($2)
andi	$7,$7,0x1
lbu	$16,8($2)
andi	$8,$8,0x1
sll	$7,$7,27
lbu	$5,26($2)
andi	$17,$17,0x1
lbu	$25,28($2)
or	$8,$8,$6
lbu	$24,30($2)
sll	$17,$17,26
lbu	$15,32($2)
andi	$6,$14,0x1
lw	$14,0($11)
or	$7,$8,$7
sll	$6,$6,25
andi	$16,$16,0x1
or	$7,$7,$17
sll	$16,$16,24
or	$6,$7,$6
andi	$8,$25,0xf
sll	$5,$5,16
or	$6,$6,$16
sll	$8,$8,8
andi	$3,$24,0x1
or	$5,$6,$5
andi	$7,$15,0x1
sll	$3,$3,2
or	$5,$5,$8
sll	$7,$7,1
addu	$6,$14,$10
or	$3,$5,$3
or	$3,$3,$7
addiu	$5,$6,1284
#APP
# 21 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,16($2)
lbu	$3,15($2)
lbu	$6,13($2)
lbu	$8,14($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$8,$8,8
addu	$6,$7,$10
or	$3,$5,$3
or	$3,$3,$8
addiu	$5,$6,1028
#APP
# 42 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,12($2)
lbu	$3,11($2)
lbu	$6,9($2)
lbu	$8,10($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$8,$8,8
addiu	$6,$7,1024
or	$3,$5,$3
or	$3,$3,$8
addu	$5,$10,$6
#APP
# 42 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,24($2)
addiu	$14,$10,128
lbu	$3,23($2)
lbu	$6,21($2)
lbu	$8,22($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$8,$8,8
addu	$6,$7,$14
or	$3,$5,$3
or	$3,$3,$8
addiu	$6,$6,1028
#APP
# 52 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($6)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,20($2)
lbu	$3,19($2)
lbu	$6,17($2)
lbu	$8,18($2)
sll	$5,$5,24
lw	$7,0($11)
sll	$3,$3,16
or	$5,$6,$5
sll	$8,$8,8
addiu	$6,$7,1024
or	$3,$5,$3
or	$3,$3,$8
addu	$5,$6,$14
#APP
# 52 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lbu	$7,7($9)
lbu	$6,27($9)
lbu	$15,5($9)
lbu	$3,9($9)
sll	$7,$7,15
andi	$6,$6,0x7
lbu	$5,25($9)
lbu	$14,10($9)
sll	$6,$6,12
andi	$7,$7,0xffff
lw	$8,0($11)
sll	$15,$15,31
or	$6,$7,$6
andi	$7,$3,0x7
or	$6,$6,$15
sll	$3,$7,9
andi	$5,$5,0x3f
andi	$7,$14,0x7
or	$5,$6,$5
sll	$7,$7,6
addiu	$6,$8,3328
or	$3,$5,$3
or	$3,$3,$7
addu	$5,$10,$6
#APP
# 63 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
lbu	$7,8($9)
lbu	$6,28($9)
lbu	$15,6($9)
lbu	$3,17($9)
sll	$7,$7,15
andi	$6,$6,0x7
lbu	$5,26($9)
lbu	$14,18($9)
sll	$6,$6,12
andi	$7,$7,0xffff
lw	$8,0($11)
sll	$15,$15,31
or	$6,$7,$6
andi	$7,$3,0x7
or	$6,$6,$15
sll	$3,$7,9
andi	$5,$5,0x3f
andi	$7,$14,0x7
or	$5,$6,$5
sll	$7,$7,6
addu	$6,$8,$10
or	$3,$5,$3
or	$3,$3,$7
addiu	$5,$6,3332
#APP
# 63 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$10,$10,8
addiu	$2,$2,35
.set	noreorder
.set	nomacro
bne	$10,$13,$L980
addiu	$9,$9,35
.set	macro
.set	reorder

lw	$2,0($12)
li	$3,7			# 0x7
addiu	$2,$2,4
#APP
# 79 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($12)
addiu	$2,$2,2052
#APP
# 82 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,131072			# 0x20000
lw	$3,0($12)
ori	$2,$2,0xff89
#APP
# 85 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(SubPel)($28)
lw	$5,%got(AryFMT)($28)
lw	$3,0($12)
addiu	$2,$2,%lo(SubPel)
addiu	$5,$5,%lo(AryFMT)
addu	$2,$4,$2
addu	$4,$4,$5
addiu	$3,$3,48
lb	$2,0($2)
lbu	$4,0($4)
addiu	$2,$2,-1
sll	$4,$4,31
sll	$2,$2,14
andi	$2,$2,0xffff
or	$2,$2,$4
#APP
# 105 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($12)
move	$3,$0
addiu	$2,$2,2096
#APP
# 118 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,8
.set	macro
.set	reorder

.end	motion_init_h264
.size	motion_init_h264, .-motion_init_h264
.section	.text.motion_config,"ax",@progbits
.align	2
.align	5
.globl	motion_config
.set	nomips16
.set	nomicromips
.ent	motion_config
.type	motion_config, @function
motion_config:
.frame	$sp,72,$31		# vars= 8, regs= 9/0, args= 16, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-72
li	$2,131072			# 0x20000
sw	$17,40($sp)
.cprestore	16
ori	$2,$2,0xff89
sw	$19,48($sp)
move	$19,$4
sw	$31,68($sp)
sw	$23,64($sp)
sw	$22,60($sp)
sw	$21,56($sp)
sw	$20,52($sp)
sw	$18,44($sp)
sw	$16,36($sp)
lw	$17,%got(mc_base)($28)
lw	$3,0($17)
#APP
# 139 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$23,60308			# 0xeb94
li	$16,60696			# 0xed18
addiu	$20,$23,11868
addu	$16,$4,$16
addu	$23,$4,$23
addu	$20,$4,$20
move	$18,$0
li	$22,128			# 0x80
$L984:
lhu	$2,0($23)
lw	$3,0($17)
lbu	$4,2($23)
sll	$2,$2,8
addiu	$3,$3,768
andi	$2,$2,0xffff
or	$2,$2,$4
addu	$3,$3,$18
#APP
# 162 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,0($20)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addu	$3,$3,$18
addiu	$3,$3,772
#APP
# 162 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lhu	$2,4($23)
addiu	$21,$18,128
lw	$3,0($17)
lbu	$4,6($23)
sll	$2,$2,8
addiu	$3,$3,768
andi	$2,$2,0xffff
or	$2,$2,$4
addu	$3,$3,$21
#APP
# 164 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,28800($20)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addu	$3,$3,$21
addiu	$3,$3,772
#APP
# 164 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lhu	$3,0($16)
lbu	$6,2($16)
lhu	$2,-4($16)
lbu	$4,-2($16)
sll	$3,$3,24
lw	$5,0($17)
sll	$6,$6,16
sll	$2,$2,8
or	$3,$4,$3
addiu	$4,$5,2816
or	$3,$3,$6
andi	$2,$2,0xffff
or	$2,$3,$2
addu	$3,$18,$4
#APP
# 166 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,4($20)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addu	$3,$3,$18
addiu	$3,$3,2820
#APP
# 166 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lhu	$3,8($16)
lbu	$6,10($16)
lhu	$2,4($16)
lbu	$4,6($16)
sll	$3,$3,24
lw	$5,0($17)
sll	$6,$6,16
sll	$2,$2,8
or	$3,$4,$3
addiu	$4,$5,2816
or	$3,$3,$6
andi	$2,$2,0xffff
or	$2,$3,$2
addu	$3,$4,$21
#APP
# 169 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,28804($20)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addu	$21,$3,$21
addiu	$21,$21,2820
#APP
# 169 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($21)	#i_sw
# 0 "" 2
#NO_APP
addiu	$18,$18,8
addiu	$23,$23,8
addiu	$20,$20,600
.set	noreorder
.set	nomacro
bne	$18,$22,$L984
addiu	$16,$16,16
.set	macro
.set	reorder

li	$12,61460			# 0xf014
lw	$16,%got(motion_iwta)($28)
move	$10,$0
addu	$12,$19,$12
li	$11,16			# 0x10
li	$13,256			# 0x100
$L985:
move	$3,$0
move	$2,$12
$L986:
lhu	$9,0($2)
addu	$6,$10,$3
lhu	$8,4($2)
lhu	$7,8($2)
lhu	$4,12($2)
lw	$5,0($16)
sb	$9,24($sp)
sb	$8,25($sp)
sb	$7,26($sp)
addu	$5,$5,$6
sb	$4,27($sp)
lw	$6,24($sp)
#APP
# 183 "jzsoc/h264_p0_mc.c" 1
sw	 $6,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$3,4
.set	noreorder
.set	nomacro
bne	$3,$11,$L986
addiu	$2,$2,16
.set	macro
.set	reorder

addiu	$10,$10,16
.set	noreorder
.set	nomacro
bne	$10,$13,$L985
addiu	$12,$12,192
.set	macro
.set	reorder

lw	$2,0($17)
move	$3,$0
addiu	$2,$2,32
#APP
# 187 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
addiu	$2,$2,2080
#APP
# 192 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$4,65536			# 0x10000
lw	$7,0($17)
li	$5,1073741824			# 0x40000000
addu	$4,$19,$4
addiu	$7,$7,36
lbu	$2,-5232($4)
lbu	$6,-5230($4)
andi	$3,$2,0x3
xori	$2,$2,0x1
movn	$5,$0,$2
sll	$3,$3,28
andi	$2,$6,0x7
li	$6,134545408			# 0x8050000
sll	$2,$2,24
or	$3,$3,$6
or	$2,$3,$2
or	$2,$2,$5
#APP
# 199 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($7)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
li	$3,32			# 0x20
addiu	$2,$2,44
#APP
# 207 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lbu	$2,-5231($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L990
lbu	$3,-5232($4)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L991
li	$2,1073741824			# 0x40000000
.set	macro
.set	reorder

$L990:
move	$2,$0
$L991:
li	$6,65536			# 0x10000
lw	$5,0($17)
andi	$3,$3,0x3
addu	$6,$19,$6
sll	$4,$3,28
addiu	$5,$5,2084
lbu	$3,-5229($6)
li	$6,134545408			# 0x8050000
andi	$3,$3,0x7
sll	$3,$3,24
or	$3,$3,$6
or	$2,$3,$2
or	$2,$2,$4
#APP
# 209 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($5)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
li	$3,327680			# 0x50000
addiu	$2,$2,2088
#APP
# 217 "jzsoc/h264_p0_mc.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
li	$2,2097152			# 0x200000
addiu	$2,$2,32
addiu	$3,$3,2092
#APP
# 220 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,192($19)
lw	$3,0($17)
addiu	$4,$2,15
slt	$5,$2,0
movn	$2,$4,$5
addiu	$3,$3,76
sra	$2,$2,4
andi	$2,$2,0xfff
sll	$2,$2,16
ori	$2,$2,0x10
#APP
# 222 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,164($19)
lw	$3,160($19)
lw	$4,0($17)
sll	$2,$2,4
sll	$3,$3,4
andi	$2,$2,0xff0
sll	$2,$2,16
andi	$3,$3,0xff0
or	$2,$2,$3
addiu	$3,$4,80
#APP
# 223 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(motion_dsa)($28)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addiu	$3,$3,88
#APP
# 224 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,0($16)
.set	macro
.set	reorder

lw	$3,0($17)
addiu	$3,$3,12
#APP
# 225 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,192($19)
lw	$3,0($17)
addiu	$4,$2,15
slt	$5,$2,0
movn	$2,$4,$5
addiu	$3,$3,2124
sra	$2,$2,4
andi	$2,$2,0xfff
sll	$2,$2,16
ori	$2,$2,0x8
#APP
# 226 "jzsoc/h264_p0_mc.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$31,68($sp)
lw	$23,64($sp)
lw	$22,60($sp)
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

.end	motion_config
.size	motion_config, .-motion_config
.section	.text.sde_slice_end,"ax",@progbits
.align	2
.align	5
.globl	sde_slice_end
.set	nomips16
.set	nomicromips
.ent	sde_slice_end
.type	sde_slice_end, @function
sde_slice_end:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,%got(sde_slice_info_ptr)($28)
li	$10,16777216			# 0x1000000
lw	$9,%got(sde_base)($28)
lw	$7,0($2)
lw	$8,0($9)
lw	$6,0($7)
addiu	$8,$8,16
lw	$5,4($7)
lw	$4,12($7)
andi	$6,$6,0x7
lw	$3,16($7)
addu	$6,$6,$10
lw	$2,20($7)
li	$10,32			# 0x20
andi	$5,$5,0x3
movn	$4,$10,$4
sll	$5,$5,3
li	$10,64			# 0x40
addu	$5,$6,$5
lw	$6,28($7)
movn	$3,$10,$3
addu	$4,$5,$4
li	$5,128			# 0x80
lw	$10,24($7)
sll	$6,$6,9
movn	$2,$5,$2
addu	$4,$4,$3
li	$3,256			# 0x100
lw	$5,36($7)
andi	$6,$6,0xffff
movn	$10,$3,$10
addu	$3,$4,$2
lw	$2,32($7)
andi	$5,$5,0xf
sll	$5,$5,20
addu	$3,$3,$10
andi	$2,$2,0xf
sll	$2,$2,16
addu	$4,$5,$2
addu	$2,$3,$6
addu	$2,$4,$2
#APP
# 23 "./jzsoc/t_sde.c" 1
sw	 $2,0($8)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($9)
li	$3,16777216			# 0x1000000
$L1004:
#APP
# 26 "./jzsoc/t_sde.c" 1
lw	 $2,16($4)	#i_lw
# 0 "" 2
#NO_APP
and	$2,$2,$3
bne	$2,$0,$L1004
j	$31
.end	sde_slice_end
.size	sde_slice_end, .-sde_slice_end
.section	.text.sde_slice_init,"ax",@progbits
.align	2
.align	5
.globl	sde_slice_init
.set	nomips16
.set	nomicromips
.ent	sde_slice_init
.type	sde_slice_init, @function
sde_slice_init:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
li	$3,65536			# 0x10000
lw	$9,7996($4)
lw	$8,160($4)
addiu	$sp,$sp,-64
addu	$3,$4,$3
lw	$10,7992($4)
sw	$22,48($sp)
.cprestore	16
lw	$6,-5264($3)
mul	$3,$9,$8
lw	$2,%got(sde_slice_info)($28)
sw	$20,40($sp)
sw	$17,28($sp)
sw	$31,60($sp)
sw	$fp,56($sp)
sw	$23,52($sp)
addu	$22,$3,$10
sw	$21,44($sp)
sw	$19,36($sp)
sw	$18,32($sp)
sw	$16,24($sp)
lw	$3,%got(mb_num_sde)($28)
lw	$17,%got(sde_slice_info_ptr)($28)
sw	$0,0($3)
lw	$3,%got(frm_info_mv_cnt)($28)
sw	$2,0($17)
sw	$0,0($3)
lw	$3,%got(slice_mv_cnt)($28)
sw	$0,0($3)
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$6,$3,$L1041
move	$20,$4
.set	macro
.set	reorder

li	$7,1			# 0x1
li	$4,2			# 0x2
xori	$3,$6,0x2
movz	$7,$4,$3
$L1011:
li	$5,65536			# 0x10000
lw	$3,10384($20)
li	$4,131072			# 0x20000
lw	$14,2872($20)
addu	$5,$20,$5
lw	$13,12888($20)
addu	$4,$20,$4
lw	$12,12880($20)
lw	$11,11956($20)
xori	$3,$3,0x3
lw	$5,5140($5)
sltu	$3,$0,$3
lw	$4,9444($4)
sw	$3,4($2)
sw	$7,0($2)
slt	$3,$4,34
sw	$14,8($2)
sw	$13,12($2)
sw	$12,16($2)
sw	$11,20($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1043
sw	$5,24($2)
.set	macro
.set	reorder

sltu	$4,$4,1
$L1012:
li	$3,65536			# 0x10000
lw	$11,%got(frm_info_slice_start_mb_init)($28)
sw	$4,28($2)
li	$7,15			# 0xf
addu	$3,$20,$3
sw	$10,44($2)
sw	$9,48($2)
sw	$8,52($2)
lw	$5,6624($3)
lw	$4,6628($3)
lw	$9,6632($3)
xori	$13,$5,0x10
lw	$12,164($20)
xori	$10,$4,0x10
movz	$5,$7,$13
movz	$4,$7,$10
lw	$3,0($11)
sw	$5,32($2)
sw	$9,40($2)
sw	$4,36($2)
sw	$12,56($2)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1016
lw	$5,2312($20)
.set	macro
.set	reorder

li	$3,2147418112			# 0x7fff0000
addiu	$4,$5,128
ori	$3,$3,0xffff
move	$2,$5
$L1017:
sw	$3,0($2)
addiu	$2,$2,4
bne	$2,$4,$L1017
li	$2,65536			# 0x10000
li	$3,1			# 0x1
addu	$2,$20,$2
sw	$3,0($11)
lw	$6,-5264($2)
$L1016:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$6,$2,$L1018
lw	$6,%got(slice_num_sde)($28)
.set	macro
.set	reorder

move	$18,$0
move	$19,$0
move	$16,$0
lw	$4,0($6)
$L1019:
sll	$4,$4,2
lw	$21,%got(sde_base)($28)
li	$7,25			# 0x19
addu	$5,$5,$4
li	$2,57			# 0x39
lw	$4,0($21)
sw	$22,0($5)
lw	$5,0($6)
lw	$3,12828($20)
addiu	$4,$4,48
addiu	$5,$5,1
movn	$2,$7,$3
sw	$5,0($6)
#APP
# 135 "./jzsoc/t_sde.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(tcsm1_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$23,0($2)
addiu	$23,$23,13064
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$3,0($21)
lw	$28,16($sp)
addiu	$3,$3,8
#APP
# 142 "./jzsoc/t_sde.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
move	$2,$0
#APP
# 143 "./jzsoc/t_sde.c" 1
sw	 $2,0($23)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,12828($20)
beq	$2,$0,$L1024
lw	$2,10340($20)
lw	$3,10332($20)
addiu	$5,$2,7
slt	$4,$2,0
movz	$5,$2,$4
sra	$4,$5,3
addu	$3,$3,$4
li	$4,-4			# 0xfffffffffffffffc
andi	$23,$3,0x3
sll	$23,$23,3
and	$4,$3,$4
$L1026:
subu	$5,$2,$23
lw	$2,%got(curr_slice_info_mv)($28)
lw	$6,%got(sde_init_ofst)($28)
li	$3,512			# 0x200
lw	$2,0($2)
sw	$5,0($6)
addiu	$2,$2,-4
$L1027:
#APP
# 161 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 162 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 163 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 164 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 165 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 166 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 167 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 168 "./jzsoc/t_sde.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
bne	$3,$0,$L1027
lw	$25,%call16(do_get_phy_addr)($28)
.set	macro
.set	reorder

.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$3,0($21)
lw	$28,16($sp)
addiu	$3,$3,24
#APP
# 171 "./jzsoc/t_sde.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($21)
addiu	$2,$2,28
#APP
# 172 "./jzsoc/t_sde.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,131072			# 0x20000
lw	$4,2324($20)
sll	$fp,$22,3
lw	$3,2328($20)
addu	$2,$20,$2
lw	$25,%call16(do_get_phy_addr)($28)
addu	$fp,$4,$fp
sll	$23,$22,7
lw	$4,-29860($2)
lw	$2,-29856($2)
addu	$23,$3,$23
addu	$4,$4,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addu	$18,$2,$18
.set	macro
.set	reorder

lw	$3,0($21)
lw	$28,16($sp)
addiu	$3,$3,32
#APP
# 183 "./jzsoc/t_sde.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
move	$4,$18
.set	macro
.set	reorder

lw	$3,0($21)
lw	$28,16($sp)
addiu	$3,$3,36
#APP
# 184 "./jzsoc/t_sde.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$3,0($21)
lw	$28,16($sp)
addiu	$3,$3,40
#APP
# 185 "./jzsoc/t_sde.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$3,0($21)
lw	$28,16($sp)
addiu	$3,$3,44
#APP
# 186 "./jzsoc/t_sde.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
subu	$2,$22,$16
lw	$3,0($21)
andi	$22,$22,0xffff
sll	$2,$2,16
addiu	$3,$3,20
addu	$22,$2,$22
#APP
# 197 "./jzsoc/t_sde.c" 1
sw	 $22,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$5,0($17)
li	$8,-2147483648			# 0xffffffff80000000
lw	$7,0($21)
lw	$4,0($5)
addiu	$7,$7,16
lw	$6,4($5)
lw	$3,12($5)
andi	$4,$4,0x7
lw	$9,16($5)
addu	$4,$4,$8
lw	$2,20($5)
li	$8,32			# 0x20
andi	$6,$6,0x3
movn	$3,$8,$3
sll	$6,$6,3
li	$8,64			# 0x40
addu	$4,$4,$6
lw	$6,32($5)
movn	$9,$8,$9
addu	$3,$4,$3
li	$4,128			# 0x80
lw	$8,24($5)
andi	$6,$6,0xf
movn	$2,$4,$2
li	$4,256			# 0x100
addu	$3,$3,$9
movn	$8,$4,$8
lw	$4,36($5)
addu	$2,$3,$2
lw	$5,28($5)
addu	$2,$2,$8
andi	$3,$4,0xf
sll	$4,$5,9
sll	$3,$3,20
sll	$5,$6,16
andi	$4,$4,0xffff
addu	$3,$3,$5
addu	$2,$2,$4
addu	$2,$3,$2
#APP
# 214 "./jzsoc/t_sde.c" 1
sw	 $2,0($7)	#i_sw
# 0 "" 2
#NO_APP
lw	$6,0($21)
$L1032:
#APP
# 216 "./jzsoc/t_sde.c" 1
lw	 $2,16($6)	#i_lw
# 0 "" 2
#NO_APP
bltz	$2,$L1032
lw	$2,12828($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1033
addiu	$3,$6,8192
.set	macro
.set	reorder

li	$4,131072			# 0x20000
lw	$5,%got(lps_comb)($28)
addiu	$7,$6,10032
addiu	$4,$4,8273
addu	$4,$20,$4
$L1034:
lbu	$2,-1($4)
addiu	$3,$3,8
addiu	$4,$4,2
sll	$2,$2,2
addu	$2,$5,$2
lw	$2,0($2)
sw	$2,-8($3)
lbu	$2,-2($4)
sll	$2,$2,2
addu	$2,$5,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
bne	$3,$7,$L1034
sw	$2,-4($3)
.set	macro
.set	reorder

lw	$2,%got(lps_comb)($28)
lw	$2,504($2)
sw	$2,9296($6)
li	$2,65536			# 0x10000
$L1062:
move	$4,$0
addiu	$9,$2,5472
addiu	$8,$2,5664
addiu	$2,$2,5152
addu	$9,$20,$9
addu	$8,$20,$8
li	$10,64			# 0x40
addu	$20,$20,$2
$L1040:
addu	$3,$8,$4
addu	$2,$9,$4
addu	$5,$20,$4
lw	$3,0($3)
addu	$7,$6,$4
lw	$2,0($2)
addiu	$4,$4,4
lw	$5,0($5)
sll	$3,$3,5
sll	$5,$5,16
addu	$2,$3,$2
addu	$2,$2,$5
.set	noreorder
.set	nomacro
bne	$4,$10,$L1040
sw	$2,10112($7)
.set	macro
.set	reorder

lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L1033:
lw	$10,%got(sde_vlc2_sta)($28)
lw	$9,%got(sde_vlc2_table)($28)
addiu	$11,$10,84
$L1037:
lw	$7,4($10)
lw	$8,0($10)
addiu	$7,$7,1
sra	$8,$8,1
sra	$7,$7,1
.set	noreorder
.set	nomacro
blez	$7,$L1039
sll	$8,$8,2
.set	macro
.set	reorder

sll	$7,$7,2
move	$2,$0
$L1038:
addu	$3,$9,$2
addu	$4,$2,$8
addiu	$2,$2,4
lwl	$5,3($3)
addu	$4,$6,$4
lwr	$5,0($3)
.set	noreorder
.set	nomacro
bne	$2,$7,$L1038
sw	$5,8192($4)
.set	macro
.set	reorder

$L1039:
addiu	$10,$10,12
.set	noreorder
.set	nomacro
bne	$10,$11,$L1037
addiu	$9,$9,256
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1062
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L1024:
lw	$3,10332($20)
lw	$2,10340($20)
andi	$23,$3,0x3
sra	$4,$2,3
sll	$23,$23,3
addu	$3,$3,$4
addu	$23,$23,$2
li	$4,-4			# 0xfffffffffffffffc
andi	$23,$23,0x1f
.set	noreorder
.set	nomacro
b	$L1026
and	$4,$3,$4
.set	macro
.set	reorder

$L1043:
.set	noreorder
.set	nomacro
b	$L1012
li	$4,1			# 0x1
.set	macro
.set	reorder

$L1041:
.set	noreorder
.set	nomacro
b	$L1011
li	$7,4			# 0x4
.set	macro
.set	reorder

$L1018:
lw	$4,0($6)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1046
move	$18,$0
.set	macro
.set	reorder

li	$2,131072			# 0x20000
addu	$2,$20,$2
lw	$8,-29872($2)
addiu	$7,$8,128
move	$2,$8
$L1022:
lw	$16,0($2)
slt	$3,$22,$16
bne	$3,$0,$L1020
lw	$3,4($2)
slt	$3,$22,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L1063
sll	$19,$16,3
.set	macro
.set	reorder

$L1020:
addiu	$2,$2,4
bne	$2,$7,$L1022
lw	$16,128($8)
sll	$19,$16,3
$L1063:
.set	noreorder
.set	nomacro
b	$L1019
sll	$18,$16,7
.set	macro
.set	reorder

$L1046:
move	$19,$0
.set	noreorder
.set	nomacro
b	$L1019
move	$16,$0
.set	macro
.set	reorder

.end	sde_slice_init
.size	sde_slice_init, .-sde_slice_init
.section	.text.sde_frame_init,"ax",@progbits
.align	2
.align	5
.globl	sde_frame_init
.set	nomips16
.set	nomicromips
.ent	sde_frame_init
.type	sde_frame_init, @function
sde_frame_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(slice_num_sde)($28)
sw	$0,0($2)
lw	$2,%got(frm_info_slice_start_mb_init)($28)
j	$31
sw	$0,0($2)

.set	macro
.set	reorder
.end	sde_frame_init
.size	sde_frame_init, .-sde_frame_init
.rdata
.align	2
$LC0:
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	4
.word	4
.word	5
.word	6
.word	7
.word	8
.word	9
.word	10
.word	12
.word	13
.word	15
.word	17
.word	20
.word	22
.word	25
.word	28
.word	32
.word	36
.word	40
.word	45
.word	50
.word	56
.word	63
.word	71
.word	80
.word	90
.word	101
.word	113
.word	127
.word	144
.word	162
.word	182
.word	203
.word	226
.word	255
.word	255
.align	2
$LC1:
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	2
.word	2
.word	2
.word	3
.word	3
.word	3
.word	3
.word	4
.word	4
.word	4
.word	6
.word	6
.word	7
.word	7
.word	8
.word	8
.word	9
.word	9
.word	10
.word	10
.word	11
.word	11
.word	12
.word	12
.word	13
.word	13
.word	14
.word	14
.word	15
.word	15
.word	16
.word	16
.word	17
.word	17
.word	18
.word	18
.align	2
$LC2:
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	1
.word	0
.word	0
.word	1
.word	0
.word	0
.word	1
.word	0
.word	0
.word	1
.word	0
.word	1
.word	1
.word	0
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	2
.word	1
.word	1
.word	2
.word	1
.word	1
.word	2
.word	1
.word	1
.word	2
.word	1
.word	2
.word	3
.word	1
.word	2
.word	3
.word	2
.word	2
.word	3
.word	2
.word	2
.word	4
.word	2
.word	3
.word	4
.word	2
.word	3
.word	4
.word	3
.word	3
.word	5
.word	3
.word	4
.word	6
.word	3
.word	4
.word	6
.word	4
.word	5
.word	7
.word	4
.word	5
.word	8
.word	4
.word	6
.word	9
.word	5
.word	7
.word	10
.word	6
.word	8
.word	11
.word	6
.word	8
.word	13
.word	7
.word	10
.word	14
.word	8
.word	11
.word	16
.word	9
.word	12
.word	18
.word	10
.word	13
.word	20
.word	11
.word	15
.word	23
.word	13
.word	17
.word	25
.section	.text.init_dblk_chain_fifo,"ax",@progbits
.align	2
.align	5
.globl	init_dblk_chain_fifo
.set	nomips16
.set	nomicromips
.ent	init_dblk_chain_fifo
.type	init_dblk_chain_fifo, @function
init_dblk_chain_fifo:
.frame	$sp,1096,$31		# vars= 1040, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,%got($LC0)($28)
addiu	$sp,$sp,-1096
addiu	$3,$sp,856
sw	$18,1076($sp)
addiu	$2,$2,%lo($LC0)
.cprestore	16
sw	$31,1092($sp)
move	$18,$5
addiu	$4,$2,208
sw	$21,1088($sp)
sw	$20,1084($sp)
sw	$19,1080($sp)
sw	$17,1072($sp)
sw	$16,1068($sp)
$L1066:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1066
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$2,%got($LC1)($28)
addiu	$3,$sp,648
addiu	$2,$2,%lo($LC1)
addiu	$4,$2,208
$L1067:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1067
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$2,%got($LC2)($28)
addiu	$3,$sp,24
addiu	$2,$2,%lo($LC2)
addiu	$4,$2,624
$L1068:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1068
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$17,%got(dblk0_base)($28)
lw	$2,716($sp)
lw	$3,924($sp)
lw	$16,%got(DBLK_BASE)($28)
lw	$4,0($17)
sll	$2,$2,24
sll	$3,$3,16
addiu	$5,$4,160
sw	$4,0($16)
addu	$2,$2,$3
#APP
# 40 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$8,$sp,724
addiu	$9,$sp,932
addiu	$10,$sp,860
li	$12,4			# 0x4
move	$7,$9
move	$6,$8
$L1069:
lw	$3,0($6)
lw	$2,0($7)
lw	$11,-4($6)
sll	$4,$3,24
lw	$5,0($16)
sll	$3,$2,16
lw	$13,-4($7)
sll	$11,$11,8
addu	$3,$4,$3
addiu	$4,$5,160
addu	$2,$3,$11
addu	$3,$12,$4
addu	$2,$2,$13
#APP
# 44 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$6,$6,8
addiu	$7,$7,8
.set	noreorder
.set	nomacro
bne	$6,$10,$L1069
addiu	$12,$12,4
.set	macro
.set	reorder

addiu	$5,$sp,236
move	$12,$0
li	$15,72			# 0x48
move	$7,$5
$L1070:
lw	$4,0($7)
lw	$2,-4($7)
lw	$3,-8($7)
sll	$6,$4,25
lw	$14,-12($7)
sll	$4,$2,20
lw	$13,-16($7)
sll	$3,$3,16
lw	$11,0($16)
addu	$4,$6,$4
lw	$19,-20($7)
sll	$2,$14,9
addu	$3,$4,$3
sll	$6,$13,4
addu	$3,$3,$2
addiu	$4,$11,232
addu	$2,$3,$6
addu	$3,$12,$4
addu	$2,$2,$19
#APP
# 51 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$12,$12,4
.set	noreorder
.set	nomacro
bne	$12,$15,$L1070
addiu	$7,$7,24
.set	macro
.set	reorder

lw	$19,%got(dblk1_base)($28)
lw	$2,716($sp)
lw	$3,924($sp)
lw	$7,%got(DBLK_BASE)($28)
lw	$4,0($19)
sll	$2,$2,24
sll	$3,$3,16
addiu	$6,$4,160
sw	$4,0($7)
addu	$2,$2,$3
#APP
# 62 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($6)	#i_sw
# 0 "" 2
#NO_APP
li	$11,4			# 0x4
$L1071:
lw	$3,0($8)
lw	$2,0($9)
lw	$7,-4($8)
sll	$4,$3,24
lw	$6,0($16)
sll	$3,$2,16
lw	$12,-4($9)
sll	$7,$7,8
addu	$3,$4,$3
addiu	$4,$6,160
addu	$2,$3,$7
addu	$3,$11,$4
addu	$2,$2,$12
#APP
# 66 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$8,$8,8
addiu	$9,$9,8
.set	noreorder
.set	nomacro
bne	$8,$10,$L1071
addiu	$11,$11,4
.set	macro
.set	reorder

move	$8,$0
li	$12,72			# 0x48
$L1072:
lw	$4,0($5)
lw	$2,-4($5)
lw	$3,-8($5)
sll	$6,$4,25
lw	$10,-12($5)
sll	$4,$2,20
lw	$9,-16($5)
sll	$3,$3,16
lw	$7,0($16)
addu	$4,$6,$4
lw	$11,-20($5)
sll	$2,$10,9
addu	$3,$4,$3
sll	$6,$9,4
addu	$3,$3,$2
addiu	$4,$7,232
addu	$2,$3,$6
addu	$3,$4,$8
addu	$2,$2,$11
#APP
# 74 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$8,$8,4
.set	noreorder
.set	nomacro
bne	$8,$12,$L1072
addiu	$5,$5,24
.set	macro
.set	reorder

lw	$20,%got(tcsm1_base)($28)
li	$3,3			# 0x3
lw	$2,0($20)
addiu	$4,$2,20312
#APP
# 82 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,-2147483648			# 0xffffffff80000000
addiu	$4,$2,20316
#APP
# 97 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,67239936			# 0x4020000
addiu	$4,$2,20320
addiu	$3,$3,64
#APP
# 99 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
addiu	$4,$2,20324
addiu	$3,$3,85
#APP
# 107 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,671088640			# 0x28000000
addiu	$2,$2,20328
addiu	$3,$3,1
#APP
# 109 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($20)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,20176
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addiu	$3,$3,136
#APP
# 124 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
li	$21,1			# 0x1
addiu	$2,$2,132
#APP
# 125 "jzsoc/h264_vae_hwinit.c" 1
sw	 $21,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($20)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,20176
.set	macro
.set	reorder

lw	$3,0($19)
lw	$28,16($sp)
addiu	$3,$3,136
#APP
# 127 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($19)
addiu	$2,$2,132
#APP
# 128 "jzsoc/h264_vae_hwinit.c" 1
sw	 $21,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
$L1073:
#APP
# 131 "jzsoc/h264_vae_hwinit.c" 1
lw	 $2,0($3)	#i_lw
# 0 "" 2
#NO_APP
andi	$2,$2,0x4
beq	$2,$0,$L1073
lw	$4,0($19)
$L1074:
#APP
# 134 "jzsoc/h264_vae_hwinit.c" 1
lw	 $2,0($4)	#i_lw
# 0 "" 2
#NO_APP
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L1074
li	$2,16711680			# 0xff0000
.set	macro
.set	reorder

addiu	$3,$3,4
addiu	$2,$2,85
#APP
# 138 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($19)
addiu	$3,$3,4
#APP
# 139 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
blez	$18,$L1076
lw	$3,0($17)
.set	macro
.set	reorder

li	$6,-2145386496			# 0xffffffff80200000
move	$5,$0
addiu	$6,$6,1
$L1079:
#APP
# 141 "jzsoc/h264_vae_hwinit.c" 1
sw	 $6,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($19)
#APP
# 155 "jzsoc/h264_vae_hwinit.c" 1
sw	 $6,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
$L1077:
#APP
# 169 "jzsoc/h264_vae_hwinit.c" 1
lw	 $2,0($3)	#i_lw
# 0 "" 2
#NO_APP
andi	$2,$2,0x4
beq	$2,$0,$L1077
lw	$4,0($19)
$L1078:
#APP
# 171 "jzsoc/h264_vae_hwinit.c" 1
lw	 $2,0($4)	#i_lw
# 0 "" 2
#NO_APP
andi	$2,$2,0x4
beq	$2,$0,$L1078
addiu	$5,$5,1
bne	$5,$18,$L1079
$L1076:
lw	$4,0($20)
lw	$25,%call16(do_get_phy_addr)($28)
sw	$3,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,20332
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,136
#APP
# 175 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($20)
lw	$2,0($19)
lw	$25,%call16(do_get_phy_addr)($28)
addiu	$4,$4,20812
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
sw	$2,0($16)
.set	macro
.set	reorder

lw	$3,0($16)
addiu	$3,$3,136
#APP
# 177 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$31,1092($sp)
lw	$21,1088($sp)
lw	$20,1084($sp)
lw	$19,1080($sp)
lw	$18,1076($sp)
lw	$17,1072($sp)
lw	$16,1068($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1096
.set	macro
.set	reorder

.end	init_dblk_chain_fifo
.size	init_dblk_chain_fifo, .-init_dblk_chain_fifo
.section	.text.init_dblk_chain_frame,"ax",@progbits
.align	2
.align	5
.globl	init_dblk_chain_frame
.set	nomips16
.set	nomicromips
.ent	init_dblk_chain_frame
.type	init_dblk_chain_frame, @function
init_dblk_chain_frame:
.frame	$sp,72,$31		# vars= 8, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,%got(tcsm1_base)($28)
addiu	$sp,$sp,-72
sw	$16,32($sp)
.cprestore	16
sw	$31,68($sp)
sw	$fp,64($sp)
sw	$23,60($sp)
sw	$22,56($sp)
sw	$21,52($sp)
sw	$20,48($sp)
sw	$19,44($sp)
sw	$18,40($sp)
sw	$17,36($sp)
lw	$16,0($2)
li	$2,-2147483648			# 0xffffffff80000000
addiu	$3,$16,20176
#APP
# 184 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$2,-2145320960			# 0xffffffff80210000
addiu	$3,$16,20180
addiu	$2,$2,72
#APP
# 185 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$5,76			# 0x4c
addiu	$2,$16,20184
#APP
# 194 "jzsoc/h264_vae_hwinit.c" 1
sw	 $5,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,524288			# 0x80000
addiu	$6,$16,20188
addiu	$3,$2,148
#APP
# 196 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$6,$16,20192
#APP
# 198 "jzsoc/h264_vae_hwinit.c" 1
sw	 $5,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$5,$16,20196
#APP
# 200 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
li	$3,64			# 0x40
addiu	$5,$16,20200
#APP
# 202 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,128
addiu	$3,$16,20204
#APP
# 204 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$2,964			# 0x3c4
addiu	$3,$16,20208
#APP
# 206 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
andi	$2,$4,0xff
sll	$2,$2,16
addiu	$3,$16,20212
ori	$2,$2,0x784
#APP
# 208 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
move	$18,$0
addiu	$2,$16,20216
#APP
# 210 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20220
#APP
# 212 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20224
#APP
# 214 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20228
#APP
# 216 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20232
#APP
# 218 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20236
#APP
# 220 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$17,%got(sram_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2984
.set	macro
.set	reorder

addiu	$3,$16,20240
lw	$28,16($sp)
#APP
# 223 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2376
.set	macro
.set	reorder

addiu	$3,$16,20244
lw	$28,16($sp)
#APP
# 225 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,8
.set	macro
.set	reorder

addiu	$3,$16,20248
lw	$28,16($sp)
#APP
# 227 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,15208
.set	macro
.set	reorder

addiu	$3,$16,20252
lw	$28,16($sp)
#APP
# 229 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,11352
.set	macro
.set	reorder

addiu	$3,$16,20256
lw	$28,16($sp)
#APP
# 231 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,3656
.set	macro
.set	reorder

addiu	$3,$16,20260
lw	$28,16($sp)
#APP
# 233 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,19924
.set	macro
.set	reorder

addiu	$3,$16,20264
lw	$28,16($sp)
#APP
# 235 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,19636
.set	macro
.set	reorder

addiu	$3,$16,20268
lw	$28,16($sp)
#APP
# 237 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,19060
.set	macro
.set	reorder

addiu	$3,$16,20272
lw	$28,16($sp)
#APP
# 239 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2992
.set	macro
.set	reorder

addiu	$3,$16,20276
lw	$28,16($sp)
#APP
# 241 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2384
.set	macro
.set	reorder

addiu	$3,$16,20280
lw	$28,16($sp)
#APP
# 243 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,24
.set	macro
.set	reorder

addiu	$3,$16,20284
lw	$28,16($sp)
#APP
# 245 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20288
#APP
# 246 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,%got(tcsm1_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,32640
.set	macro
.set	reorder

addiu	$3,$16,20292
lw	$28,16($sp)
#APP
# 249 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20296
#APP
# 252 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20300
#APP
# 253 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,20304
#APP
# 254 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
addiu	$16,$16,20308
addiu	$2,$2,85
#APP
# 255 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(tcsm1_base)($28)
li	$21,403111936			# 0x18070000
lw	$25,%call16(do_get_phy_addr)($28)
move	$16,$0
lw	$23,%got(dblk0_base)($28)
li	$20,-2147483648			# 0xffffffff80000000
lw	$19,%got(vpu_base)($28)
addiu	$21,$21,72
lw	$4,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,32640
.set	macro
.set	reorder

lw	$28,16($sp)
sw	$2,24($sp)
li	$2,131072			# 0x20000
lw	$18,24($sp)
addiu	$fp,$2,104
lw	$3,%got(tcsm1_base)($28)
lw	$17,0($3)
addiu	$22,$17,20392
addiu	$17,$17,20380
$L1100:
addiu	$2,$22,-60
#APP
# 262 "jzsoc/h264_vae_hwinit.c" 1
sw	 $20,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$22,-56
#APP
# 263 "jzsoc/h264_vae_hwinit.c" 1
sw	 $21,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$22,-48
#APP
# 275 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,765460480			# 0x2da00000
addiu	$2,$22,-28
ori	$3,$3,0x1
#APP
# 284 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$22,-24
#APP
# 299 "jzsoc/h264_vae_hwinit.c" 1
sw	 $20,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$22,-20
#APP
# 300 "jzsoc/h264_vae_hwinit.c" 1
sw	 $fp,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($23)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,136
.set	macro
.set	reorder

addiu	$4,$22,-16
lw	$28,16($sp)
#APP
# 304 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$2,7			# 0x7
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
beq	$16,$2,$L1107
move	$4,$22
.set	macro
.set	reorder

.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$28,16($sp)
#APP
# 308 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($17)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($19)
$L1109:
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,116
.set	macro
.set	reorder

addiu	$4,$22,-8
lw	$28,16($sp)
#APP
# 311 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
move	$2,$0
addiu	$4,$22,-4
#APP
# 312 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$2,8			# 0x8
addiu	$16,$16,1
addiu	$18,$18,224
addiu	$22,$22,60
.set	noreorder
.set	nomacro
bne	$16,$2,$L1100
addiu	$17,$17,60
.set	macro
.set	reorder

lw	$2,%got(tcsm1_base)($28)
li	$22,403111936			# 0x18070000
li	$21,765460480			# 0x2da00000
lw	$23,%got(dblk1_base)($28)
lw	$18,24($sp)
move	$16,$0
li	$20,-2147483648			# 0xffffffff80000000
lw	$17,0($2)
addiu	$22,$22,72
addiu	$21,$21,1
addiu	$fp,$17,20872
addiu	$17,$17,20860
$L1103:
addiu	$2,$fp,-60
#APP
# 318 "jzsoc/h264_vae_hwinit.c" 1
sw	 $20,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,-56
#APP
# 319 "jzsoc/h264_vae_hwinit.c" 1
sw	 $22,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,-48
#APP
# 330 "jzsoc/h264_vae_hwinit.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,-28
#APP
# 339 "jzsoc/h264_vae_hwinit.c" 1
sw	 $21,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,-24
#APP
# 354 "jzsoc/h264_vae_hwinit.c" 1
sw	 $20,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,131072			# 0x20000
addiu	$2,$fp,-20
ori	$3,$3,0x68
#APP
# 355 "jzsoc/h264_vae_hwinit.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($23)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,136
.set	macro
.set	reorder

addiu	$4,$fp,-16
lw	$28,16($sp)
#APP
# 359 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$2,7			# 0x7
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
beq	$16,$2,$L1108
move	$4,$fp
.set	macro
.set	reorder

.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$28,16($sp)
#APP
# 363 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($17)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($19)
$L1110:
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,120
.set	macro
.set	reorder

addiu	$4,$fp,-8
lw	$28,16($sp)
#APP
# 366 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
move	$2,$0
addiu	$4,$fp,-4
#APP
# 367 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$2,8			# 0x8
addiu	$16,$16,1
addiu	$18,$18,224
addiu	$fp,$fp,60
.set	noreorder
.set	nomacro
bne	$16,$2,$L1103
addiu	$17,$17,60
.set	macro
.set	reorder

lw	$31,68($sp)
lw	$fp,64($sp)
lw	$23,60($sp)
lw	$22,56($sp)
lw	$21,52($sp)
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

$L1107:
lw	$3,%got(tcsm1_base)($28)
lw	$4,0($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,20332
.set	macro
.set	reorder

lw	$28,16($sp)
#APP
# 306 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($17)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1109
lw	$4,0($19)
.set	macro
.set	reorder

$L1108:
lw	$3,%got(tcsm1_base)($28)
lw	$4,0($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,20812
.set	macro
.set	reorder

lw	$28,16($sp)
#APP
# 361 "jzsoc/h264_vae_hwinit.c" 1
sw	 $2,0($17)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1110
lw	$4,0($19)
.set	macro
.set	reorder

.end	init_dblk_chain_frame
.size	init_dblk_chain_frame, .-init_dblk_chain_frame
.section	.text.ff_h264_write_back_intra_pred_mode,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_write_back_intra_pred_mode
.set	nomips16
.set	nomicromips
.ent	ff_h264_write_back_intra_pred_mode
.type	ff_h264_write_back_intra_pred_mode, @function
ff_h264_write_back_intra_pred_mode:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$3,131072			# 0x20000
lw	$5,11856($4)
lw	$2,10856($4)
addu	$3,$4,$3
lw	$6,10852($4)
lw	$3,9448($3)
sll	$3,$3,2
addu	$3,$5,$3
lw	$3,0($3)
addu	$2,$2,$3
sw	$6,0($2)
lb	$3,10847($4)
sb	$3,4($2)
lb	$3,10839($4)
sb	$3,5($2)
lb	$3,10831($4)
j	$31
sb	$3,6($2)

.set	macro
.set	reorder
.end	ff_h264_write_back_intra_pred_mode
.size	ff_h264_write_back_intra_pred_mode, .-ff_h264_write_back_intra_pred_mode
.section	.rodata.str1.4
.align	2
$LC11:
.ascii	"top block unavailable for requested intra4x4 mode %d at "
.ascii	"%d %d\012\000"
.align	2
$LC12:
.ascii	"left block unavailable for requested intra4x4 mode %d at"
.ascii	" %d %d\012\000"
.section	.text.ff_h264_check_intra4x4_pred_mode,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_check_intra4x4_pred_mode
.set	nomips16
.set	nomicromips
.ent	ff_h264_check_intra4x4_pred_mode
.type	ff_h264_check_intra4x4_pred_mode, @function
ff_h264_check_intra4x4_pred_mode:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,11088($4)
addiu	$sp,$sp,-40
.cprestore	24
andi	$2,$2,0x8000
sw	$31,36($sp)
bne	$2,$0,$L1120
addiu	$2,$4,10828

lw	$5,%got(top.8808)($28)
addiu	$6,$4,10832
addiu	$5,$5,%lo(top.8808)
$L1119:
lb	$3,0($2)
addu	$3,$5,$3
lb	$7,0($3)
bltz	$7,$L1134
lw	$25,%call16(av_log)($28)

beq	$7,$0,$L1118
nop

sb	$7,0($2)
$L1118:
addiu	$2,$2,1
bne	$2,$6,$L1119
nop

$L1120:
lw	$6,11096($4)
li	$2,34952			# 0x8888
andi	$3,$6,0x8888
beq	$3,$2,$L1136
lw	$31,36($sp)

lw	$5,%got(mask.8815)($28)
addiu	$3,$4,10828
lw	$8,%got(left.8809)($28)
addiu	$9,$4,10860
addiu	$5,$5,%lo(mask.8815)
addiu	$8,$8,%lo(left.8809)
$L1123:
lw	$2,0($5)
and	$2,$6,$2
bne	$2,$0,$L1121
addiu	$5,$5,4

lb	$2,0($3)
addu	$2,$2,$8
lb	$7,0($2)
bltz	$7,$L1135
lw	$25,%call16(av_log)($28)

beq	$7,$0,$L1121
nop

sb	$7,0($3)
$L1121:
addiu	$3,$3,8
bne	$3,$9,$L1123
lw	$31,36($sp)

$L1136:
move	$2,$0
j	$31
addiu	$sp,$sp,40

$L1134:
lw	$6,%got($LC11)($28)
li	$5,16			# 0x10
lw	$3,7992($4)
lw	$2,7996($4)
addiu	$6,$6,%lo($LC11)
lw	$4,0($4)
$L1133:
sw	$3,16($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,20($sp)

li	$2,-1			# 0xffffffffffffffff
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

$L1135:
lw	$6,%got($LC12)($28)
li	$5,16			# 0x10
lw	$3,7992($4)
lw	$2,7996($4)
addiu	$6,$6,%lo($LC12)
b	$L1133
lw	$4,0($4)

.set	macro
.set	reorder
.end	ff_h264_check_intra4x4_pred_mode
.size	ff_h264_check_intra4x4_pred_mode, .-ff_h264_check_intra4x4_pred_mode
.section	.rodata.str1.4
.align	2
$LC13:
.ascii	"out of range intra chroma pred mode at %d %d\012\000"
.align	2
$LC14:
.ascii	"top block unavailable for requested intra mode at %d %d\012"
.ascii	"\000"
.align	2
$LC15:
.ascii	"left block unavailable for requested intra mode at %d %d"
.ascii	"\012\000"
.section	.text.ff_h264_check_intra_pred_mode,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_check_intra_pred_mode
.set	nomips16
.set	nomicromips
.ent	ff_h264_check_intra_pred_mode
.type	ff_h264_check_intra_pred_mode, @function
ff_h264_check_intra_pred_mode:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-40
sltu	$2,$5,7
.cprestore	24
sw	$31,36($sp)
beq	$2,$0,$L1150
lw	$6,%got($LC13)($28)

lw	$2,11088($4)
andi	$2,$2,0x8000
beq	$2,$0,$L1151
lw	$2,%got(top.8825)($28)

$L1140:
lw	$3,11096($4)
li	$2,32896			# 0x8080
andi	$6,$3,0x8080
beq	$6,$2,$L1144
lw	$2,%got(left.8826)($28)

addiu	$2,$2,%lo(left.8826)
addu	$5,$5,$2
beq	$6,$0,$L1141
lb	$5,0($5)

andi	$6,$3,0x8000
li	$2,7			# 0x7
xori	$3,$5,0x6
li	$4,8			# 0x8
li	$5,2			# 0x2
movn	$4,$2,$6
movn	$5,$0,$3
addu	$2,$4,$5
$L1139:
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

$L1151:
addiu	$2,$2,%lo(top.8825)
addu	$5,$5,$2
lb	$5,0($5)
bgez	$5,$L1140
lw	$6,%got($LC14)($28)

li	$5,16			# 0x10
lw	$3,7996($4)
lw	$2,0($4)
lw	$7,7992($4)
addiu	$6,$6,%lo($LC14)
b	$L1149
lw	$25,%call16(av_log)($28)

$L1141:
bgez	$5,$L1139
move	$2,$5

lw	$6,%got($LC15)($28)
li	$5,16			# 0x10
lw	$3,7996($4)
lw	$2,0($4)
lw	$7,7992($4)
addiu	$6,$6,%lo($LC15)
lw	$25,%call16(av_log)($28)
$L1149:
move	$4,$2
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)

b	$L1139
li	$2,-1			# 0xffffffffffffffff

$L1144:
lw	$31,36($sp)
move	$2,$5
j	$31
addiu	$sp,$sp,40

$L1150:
li	$5,16			# 0x10
lw	$3,7996($4)
lw	$2,0($4)
lw	$7,7992($4)
addiu	$6,$6,%lo($LC13)
b	$L1149
lw	$25,%call16(av_log)($28)

.set	macro
.set	reorder
.end	ff_h264_check_intra_pred_mode
.size	ff_h264_check_intra_pred_mode, .-ff_h264_check_intra_pred_mode
.section	.rodata.str1.4
.align	2
$LC16:
.ascii	"ERROR: ESC_BUFFER_NUM OVERFLOW! escapes this frame\012\000"
.section	.text.ff_h264_decode_nal,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_nal
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_nal
.type	ff_h264_decode_nal, @function
ff_h264_decode_nal:
.frame	$sp,4072,$31		# vars= 4008, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-4072
li	$2,131072			# 0x20000
sw	$fp,4064($sp)
li	$11,3			# 0x3
sw	$17,4036($sp)
addu	$4,$4,$2
sw	$16,4032($sp)
addiu	$fp,$5,1
.cprestore	16
move	$16,$0
sw	$31,4068($sp)
addiu	$17,$sp,24
sw	$23,4060($sp)
li	$12,500			# 0x1f4
sw	$22,4056($sp)
sw	$21,4052($sp)
sw	$20,4048($sp)
sw	$19,4044($sp)
sw	$18,4040($sp)
lbu	$3,0($5)
lw	$10,4088($sp)
lw	$9,24($sp)
srl	$3,$3,5
sw	$6,4080($sp)
addiu	$2,$10,-1
sw	$7,4084($sp)
move	$6,$0
sw	$3,9468($4)
sw	$2,4024($sp)
move	$2,$0
lbu	$3,0($5)
andi	$3,$3,0x1f
sw	$3,9472($4)
$L1153:
lw	$5,4024($sp)
addiu	$4,$2,2
slt	$3,$4,$5
beq	$3,$0,$L1176
addu	$3,$fp,$2

$L1177:
lbu	$5,2($3)
sltu	$8,$5,4
bne	$8,$0,$L1154
nop

addiu	$2,$2,3
lw	$5,4024($sp)
addiu	$4,$2,2
slt	$3,$4,$5
bne	$3,$0,$L1177
addu	$3,$fp,$2

$L1176:
sll	$2,$16,2
sw	$9,24($sp)
addu	$2,$17,$2
sw	$10,2000($2)
$L1160:
beq	$16,$0,$L1162
lw	$10,2024($sp)

addiu	$8,$sp,2028
addiu	$5,$sp,28
move	$22,$0
move	$4,$0
$L1164:
lw	$9,0($8)
addiu	$4,$4,1
addiu	$5,$5,4
addiu	$8,$8,4
subu	$2,$9,$10
move	$10,$9
addiu	$2,$2,-1
slt	$9,$6,$2
sw	$2,-4($5)
movn	$22,$4,$9
bne	$4,$16,$L1164
movn	$6,$2,$9

beq	$22,$0,$L1165
move	$20,$0

addiu	$19,$22,-1
sw	$22,4028($sp)
sll	$18,$22,2
move	$21,$0
addiu	$23,$sp,2024
$L1166:
addu	$2,$20,$18
lw	$25,%call16(memmove)($28)
addiu	$21,$21,1
addu	$4,$17,$2
addu	$2,$23,$2
addiu	$19,$19,-1
lw	$5,-4($4)
addiu	$20,$20,-4
lw	$2,-4($2)
move	$6,$5
subu	$5,$2,$5
addu	$4,$5,$21
addu	$5,$fp,$5
.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
addu	$4,$fp,$4

li	$2,-1			# 0xffffffffffffffff
bne	$19,$2,$L1166
lw	$28,16($sp)

subu	$19,$16,$22
blez	$19,$L1178
lw	$3,4024($sp)

$L1167:
addu	$18,$17,$18
addiu	$2,$22,1
move	$23,$0
sll	$2,$2,2
lw	$18,2000($18)
addu	$17,$17,$2
$L1168:
addiu	$5,$18,1
lw	$20,0($17)
lw	$25,%call16(memcpy)($28)
addu	$4,$fp,$18
addu	$5,$5,$23
addiu	$23,$23,1
addu	$5,$fp,$5
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$6,$20

addu	$18,$18,$20
lw	$28,16($sp)
bne	$23,$19,$L1168
addiu	$17,$17,4

$L1169:
lw	$3,4024($sp)
$L1178:
subu	$16,$3,$16
addiu	$20,$3,1
lw	$3,4028($sp)
addu	$2,$fp,$3
lw	$3,4080($sp)
sw	$16,0($3)
lw	$3,4084($sp)
sw	$20,0($3)
$L1174:
lw	$31,4068($sp)
lw	$fp,4064($sp)
lw	$23,4060($sp)
lw	$22,4056($sp)
lw	$21,4052($sp)
lw	$20,4048($sp)
lw	$19,4044($sp)
lw	$18,4040($sp)
lw	$17,4036($sp)
lw	$16,4032($sp)
j	$31
addiu	$sp,$sp,4072

$L1154:
lbu	$8,0($3)
bne	$8,$0,$L1156
nop

lbu	$3,1($3)
bne	$3,$0,$L1156
nop

bne	$5,$11,$L1157
sll	$3,$16,2

bne	$16,$0,$L1158
addu	$3,$17,$3

move	$9,$4
sw	$4,2024($sp)
addiu	$2,$2,3
move	$6,$4
b	$L1153
li	$16,1			# 0x1

$L1156:
b	$L1153
addiu	$2,$2,1

$L1158:
addiu	$16,$16,1
addiu	$2,$2,3
bne	$16,$12,$L1153
sw	$4,2000($3)

lw	$4,%got($LC16)($28)
li	$2,-1			# 0xffffffffffffffff
lw	$3,4080($sp)
lw	$25,%call16(printf)($28)
addiu	$4,$4,%lo($LC16)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$2,0($3)

b	$L1174
move	$2,$0

$L1165:
sw	$0,4028($sp)
move	$19,$16
b	$L1167
move	$18,$0

$L1157:
sw	$2,4024($sp)
sw	$9,24($sp)
addu	$2,$17,$3
b	$L1160
sw	$4,2000($2)

$L1162:
b	$L1169
sw	$0,4028($sp)

.set	macro
.set	reorder
.end	ff_h264_decode_nal
.size	ff_h264_decode_nal, .-ff_h264_decode_nal
.section	.text.ff_h264_decode_rbsp_trailing,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_rbsp_trailing
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_rbsp_trailing
.type	ff_h264_decode_rbsp_trailing, @function
ff_h264_decode_rbsp_trailing:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$4,0($5)
li	$3,1			# 0x1
li	$5,9			# 0x9
$L1181:
andi	$2,$4,0x1
bne	$2,$0,$L1182
nop

addiu	$3,$3,1
bne	$3,$5,$L1181
sra	$4,$4,1

j	$31
nop

$L1182:
j	$31
move	$2,$3

.set	macro
.set	reorder
.end	ff_h264_decode_rbsp_trailing
.size	ff_h264_decode_rbsp_trailing, .-ff_h264_decode_rbsp_trailing
.section	.rodata.str1.4
.align	2
$LC17:
.ascii	"Cannot allocate memory.\012\000"
.section	.text.ff_h264_alloc_tables,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_alloc_tables
.set	nomips16
.set	nomicromips
.ent	ff_h264_alloc_tables
.type	ff_h264_alloc_tables, @function
ff_h264_alloc_tables:
.frame	$sp,56,$31		# vars= 8, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
lw	$2,0($4)
lw	$3,164($4)
sw	$17,36($sp)
sw	$18,40($sp)
lw	$17,168($4)
addiu	$3,$3,1
lw	$18,612($2)
lw	$25,%call16(av_mallocz)($28)
sll	$2,$17,1
sw	$16,32($sp)
move	$16,$4
.cprestore	16
mul	$18,$2,$18
sw	$31,52($sp)
sw	$20,48($sp)
mul	$17,$17,$3
sw	$19,44($sp)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$18,3

lw	$28,16($sp)
beq	$2,$0,$L1241
sw	$2,10856($16)

lw	$25,%call16(av_mallocz)($28)
$L1257:
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$17,3

lw	$28,16($sp)
beq	$2,$0,$L1242
sw	$2,10860($16)

lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$17,5

lw	$28,16($sp)
beq	$2,$0,$L1243
sw	$2,11232($16)

$L1189:
lw	$4,168($16)
lw	$25,%call16(av_mallocz)($28)
addu	$4,$17,$4
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$4,1

li	$3,131072			# 0x20000
lw	$28,16($sp)
addu	$3,$16,$3
beq	$2,$0,$L1244
sw	$2,10656($3)

lw	$25,%call16(av_mallocz)($28)
$L1259:
sll	$18,$17,1
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$18

li	$3,131072			# 0x20000
lw	$28,16($sp)
addu	$3,$16,$3
beq	$2,$0,$L1245
sw	$2,8732($3)

lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$17

li	$3,131072			# 0x20000
lw	$28,16($sp)
addu	$3,$16,$3
beq	$2,$0,$L1246
sw	$2,8748($3)

$L1192:
lw	$25,%call16(av_mallocz)($28)
$L1258:
sll	$19,$17,6
li	$18,131072			# 0x20000
move	$4,$19
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
addu	$18,$16,$18

lw	$28,16($sp)
beq	$2,$0,$L1247
sw	$2,8756($18)

lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$19

lw	$28,16($sp)
beq	$2,$0,$L1248
sw	$2,8760($18)

$L1195:
lw	$4,168($16)
lw	$25,%call16(av_mallocz)($28)
addu	$4,$17,$4
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$4,1

li	$3,65536			# 0x10000
lw	$28,16($sp)
addu	$3,$16,$3
beq	$2,$0,$L1249
sw	$2,-5280($3)

lw	$25,%call16(av_mallocz)($28)
$L1260:
sll	$19,$17,2
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$19

li	$3,131072			# 0x20000
lw	$28,16($sp)
addu	$3,$16,$3
beq	$2,$0,$L1250
sw	$2,9088($3)

lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$17

li	$3,65536			# 0x10000
lw	$28,16($sp)
addu	$3,$16,$3
beq	$2,$0,$L1251
sw	$2,6636($3)

$L1199:
li	$20,131072			# 0x20000
$L1261:
lw	$6,168($16)
lw	$25,%call16(memset)($28)
li	$5,-1			# 0xffffffffffffffff
addu	$20,$16,$20
addu	$6,$17,$6
li	$18,65536			# 0x10000
lw	$4,10656($20)
sll	$6,$6,1
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$18,$16,$18

li	$5,-1			# 0xffffffffffffffff
lw	$6,168($16)
lw	$28,16($sp)
lw	$2,10656($20)
sll	$3,$6,2
lw	$4,-5280($18)
addu	$6,$17,$6
addiu	$3,$3,2
lw	$25,%call16(memset)($28)
addu	$2,$2,$3
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$2,-5268($18)

move	$4,$19
lw	$2,168($16)
lw	$28,16($sp)
lw	$3,10656($20)
sll	$2,$2,2
addiu	$2,$2,2
lw	$25,%call16(av_mallocz)($28)
addu	$2,$3,$2
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sw	$2,-5276($18)

lw	$28,16($sp)
beq	$2,$0,$L1252
sw	$2,11852($16)

lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$19

lw	$28,16($sp)
beq	$2,$0,$L1253
sw	$2,11856($16)

lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$19

lw	$28,16($sp)
beq	$2,$0,$L1254
sw	$2,11860($16)

$L1204:
lw	$3,164($16)
blez	$3,$L1207
move	$10,$0

$L1231:
lw	$3,160($16)
blez	$3,$L1209
nop

lw	$13,11852($16)
sll	$14,$10,2
lw	$11,11856($16)
sll	$12,$10,1
move	$7,$0
$L1208:
lw	$5,168($16)
sll	$8,$7,2
lw	$6,11864($16)
sll	$4,$7,1
lw	$3,11868($16)
mul	$9,$10,$5
addu	$5,$9,$7
mul	$9,$14,$6
addiu	$7,$7,1
addu	$6,$9,$8
mul	$8,$12,$3
sll	$3,$5,2
addu	$9,$11,$3
addu	$4,$8,$4
addu	$8,$13,$3
sw	$6,0($8)
addu	$8,$2,$3
lw	$6,168($16)
sll	$3,$6,1
teq	$3,$0,7
div	$0,$5,$3
mfhi	$5
sll	$3,$5,3
sw	$3,0($9)
sw	$4,0($8)
lw	$3,160($16)
slt	$3,$7,$3
bne	$3,$0,$L1208
nop

$L1209:
lw	$3,164($16)
addiu	$10,$10,1
slt	$3,$10,$3
bne	$3,$0,$L1231
nop

$L1207:
li	$3,65536			# 0x10000
sw	$0,2864($16)
addu	$3,$16,$3
lw	$3,-5312($3)
beq	$3,$0,$L1255
move	$2,$0

$L1229:
lw	$31,52($sp)
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
j	$31
addiu	$sp,$sp,56

$L1241:
beq	$18,$0,$L1257
lw	$25,%call16(av_mallocz)($28)

$L1205:
$L1186 = .
lw	$6,%got($LC17)($28)
$L1256:
li	$5,16			# 0x10
$L1262:
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC17)

li	$2,-1			# 0xffffffffffffffff
lw	$28,16($sp)
move	$4,$16
lw	$25,%got(free_tables)($28)
addiu	$25,$25,%lo(free_tables)
.reloc	1f,R_MIPS_JALR,free_tables
1:	jalr	$25
sw	$2,24($sp)

b	$L1229
lw	$2,24($sp)

$L1242:
bne	$17,$0,$L1205
lw	$25,%call16(av_mallocz)($28)

.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$0

lw	$28,16($sp)
b	$L1189
sw	$2,11232($16)

$L1243:
beq	$17,$0,$L1189
lw	$6,%got($LC17)($28)

b	$L1262
li	$5,16			# 0x10

$L1245:
bne	$18,$0,$L1205
lw	$25,%call16(av_mallocz)($28)

.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$17

li	$3,131072			# 0x20000
lw	$28,16($sp)
addu	$3,$16,$3
bne	$2,$0,$L1192
sw	$2,8748($3)

$L1246:
beq	$17,$0,$L1258
lw	$25,%call16(av_mallocz)($28)

b	$L1256
lw	$6,%got($LC17)($28)

$L1244:
lw	$2,168($16)
addu	$2,$17,$2
sll	$2,$2,1
beq	$2,$0,$L1259
lw	$25,%call16(av_mallocz)($28)

b	$L1256
lw	$6,%got($LC17)($28)

$L1249:
lw	$2,168($16)
addu	$2,$17,$2
sll	$2,$2,1
beq	$2,$0,$L1260
lw	$25,%call16(av_mallocz)($28)

b	$L1256
lw	$6,%got($LC17)($28)

$L1248:
beq	$19,$0,$L1195
lw	$6,%got($LC17)($28)

b	$L1262
li	$5,16			# 0x10

$L1247:
bne	$19,$0,$L1205
lw	$25,%call16(av_mallocz)($28)

.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$0

lw	$28,16($sp)
b	$L1195
sw	$2,8760($18)

$L1253:
bne	$19,$0,$L1205
lw	$25,%call16(av_mallocz)($28)

.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$0

lw	$28,16($sp)
b	$L1204
sw	$2,11860($16)

$L1252:
bne	$19,$0,$L1205
lw	$25,%call16(av_mallocz)($28)

.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$0

move	$4,$19
lw	$28,16($sp)
lw	$25,%call16(av_mallocz)($28)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sw	$2,11856($16)

lw	$28,16($sp)
bne	$2,$0,$L1204
sw	$2,11860($16)

$L1254:
beq	$19,$0,$L1204
lw	$6,%got($LC17)($28)

b	$L1262
li	$5,16			# 0x10

$L1251:
beq	$17,$0,$L1261
li	$20,131072			# 0x20000

b	$L1256
lw	$6,%got($LC17)($28)

$L1250:
bne	$17,$0,$L1205
lw	$25,%call16(av_mallocz)($28)

.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
move	$4,$0

li	$3,65536			# 0x10000
lw	$28,16($sp)
addu	$3,$16,$3
b	$L1199
sw	$2,6636($3)

$L1255:
lw	$25,%got(init_dequant_tables)($28)
move	$4,$16
addiu	$25,$25,%lo(init_dequant_tables)
.reloc	1f,R_MIPS_JALR,init_dequant_tables
1:	jalr	$25
sw	$2,24($sp)

b	$L1229
lw	$2,24($sp)

.set	macro
.set	reorder
.end	ff_h264_alloc_tables
.size	ff_h264_alloc_tables, .-ff_h264_alloc_tables
.section	.text.ff_h264_frame_start,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_frame_start
.set	nomips16
.set	nomicromips
.ent	ff_h264_frame_start
.type	ff_h264_frame_start, @function
ff_h264_frame_start:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
lw	$25,%call16(MPV_frame_start)($28)
lw	$5,0($4)
.cprestore	16
sw	$18,36($sp)
move	$18,$4
sw	$31,44($sp)
sw	$19,40($sp)
sw	$17,32($sp)
.reloc	1f,R_MIPS_JALR,MPV_frame_start
1:	jalr	$25
sw	$16,28($sp)

bltz	$2,$L1274
lw	$28,16($sp)

lw	$25,%call16(ff_er_frame_start)($28)
.reloc	1f,R_MIPS_JALR,ff_er_frame_start
1:	jalr	$25
move	$4,$18

addiu	$11,$18,11756
lw	$28,16($sp)
lw	$7,192($18)
move	$6,$11
lw	$2,2696($18)
lw	$10,%got(scan8)($28)
sll	$9,$7,2
lw	$8,%got(scan8+16)($28)
sll	$7,$7,3
sw	$0,48($2)
addiu	$10,$10,%lo(scan8)
sw	$0,284($2)
addiu	$8,$8,%lo(scan8+16)
move	$5,$10
$L1265:
lbu	$2,0($5)
addiu	$6,$6,4
addiu	$5,$5,1
addiu	$2,$2,-12
andi	$3,$2,0x7
sra	$2,$2,3
sll	$3,$3,2
mul	$12,$2,$9
addu	$4,$12,$3
mul	$12,$2,$7
sw	$4,-100($6)
addu	$2,$12,$3
bne	$5,$8,$L1265
sw	$2,-4($6)

lw	$6,196($18)
addiu	$3,$18,11740
sll	$7,$6,2
sll	$6,$6,3
$L1266:
lbu	$2,0($10)
addiu	$3,$3,4
addiu	$10,$10,1
addiu	$2,$2,-12
andi	$4,$2,0x7
sra	$2,$2,3
sll	$4,$4,2
mul	$8,$2,$7
addu	$5,$8,$4
mul	$8,$2,$6
sw	$5,-4($3)
sw	$5,-20($3)
addu	$2,$8,$4
sw	$2,92($3)
bne	$11,$3,$L1266
sw	$2,76($3)

lw	$2,0($18)
lw	$2,612($2)
blez	$2,$L1272
li	$16,131072			# 0x20000

move	$17,$0
ori	$16,$16,0xc454
b	$L1271
addu	$16,$18,$16

$L1270:
lw	$2,0($18)
lw	$2,612($2)
slt	$2,$17,$2
beq	$2,$0,$L1272
nop

$L1271:
lw	$19,0($16)
addiu	$17,$17,1
lw	$2,2864($19)
bne	$2,$0,$L1270
addiu	$16,$16,4

lw	$2,192($18)
lw	$4,196($18)
lw	$25,%call16(av_malloc)($28)
sll	$2,$2,1
addu	$4,$2,$4
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
sll	$4,$4,4

lw	$28,16($sp)
sw	$2,2864($19)
lw	$2,0($18)
lw	$2,612($2)
slt	$2,$17,$2
bne	$2,$0,$L1271
nop

$L1272:
lw	$2,164($18)
li	$16,65536			# 0x10000
lw	$6,168($18)
li	$5,-1			# 0xffffffffffffffff
addu	$16,$18,$16
lw	$25,%call16(memset)($28)
mul	$6,$2,$6
lw	$4,-5268($16)
addiu	$6,$6,-1
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sll	$6,$6,1

li	$5,-1			# 0xffffffffffffffff
lw	$2,164($18)
lw	$6,168($18)
lw	$28,16($sp)
lw	$4,-5276($16)
mul	$6,$2,$6
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$6,$6,-1

li	$2,24			# 0x18
lw	$3,52($18)
beq	$3,$2,$L1281
nop

lw	$4,2696($18)
sw	$0,80($4)
$L1273:
li	$3,2147418112			# 0x7fff0000
move	$2,$0
ori	$3,$3,0xffff
sw	$3,272($4)
sw	$3,268($4)
$L1264:
lw	$31,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,48

$L1281:
b	$L1273
lw	$4,2696($18)

$L1274:
b	$L1264
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	ff_h264_frame_start
.size	ff_h264_frame_start, .-ff_h264_frame_start
.section	.rodata.str1.4
.align	2
$LC18:
.ascii	"B\000"
.align	2
$LC19:
.ascii	"F\000"
.align	2
$LC20:
.ascii	"T\000"
.align	2
$LC21:
.ascii	" fix\000"
.align	2
$LC22:
.ascii	"\000"
.align	2
$LC23:
.ascii	" IDR\000"
.align	2
$LC24:
.ascii	"c\000"
.align	2
$LC25:
.ascii	"TEMP\000"
.align	2
$LC26:
.ascii	"SPAT\000"
.align	2
$LC27:
.ascii	"slice type too large (%d) at %d %d\012\000"
.align	2
$LC28:
.ascii	"pps_id out of range\012\000"
.align	2
$LC29:
.ascii	"non-existing PPS %u referenced\012\000"
.align	2
$LC30:
.ascii	"non-existing SPS %u referenced\012\000"
.align	2
$LC31:
.ascii	"Frame num gap %d %d\012\000"
.align	2
$LC32:
.ascii	"first_mb_in_slice overflow\012\000"
.align	2
$LC33:
.ascii	"reference overflow\012\000"
.align	2
$LC34:
.ascii	"cabac_init_idc overflow\012\000"
.align	2
$LC35:
.ascii	"QP %u out of range\012\000"
.align	2
$LC36:
.ascii	"deblocking_filter_idc %u out of range\012\000"
.align	2
$LC37:
.ascii	"Cannot parallelize deblocking type 1, decoding such fram"
.ascii	"es in sequential order\012\000"
.align	2
$LC38:
.ascii	"Too many slices, increase MAX_SLICES and recompile\012\000"
.align	2
$LC39:
.ascii	"slice:%d %s mb:%d %c%s%s pps:%u frame:%d poc:%d/%d ref:%"
.ascii	"d/%d qp:%d loop:%d:%d:%d weight:%d%s %s\012\000"
.section	.text.decode_slice_header,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_slice_header
.type	decode_slice_header, @function
decode_slice_header:
.frame	$sp,256,$31		# vars= 120, regs= 10/0, args= 88, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
li	$2,131072			# 0x20000
lw	$3,0($4)
addiu	$sp,$sp,-256
addu	$2,$4,$2
.cprestore	88
sw	$fp,248($sp)
move	$fp,$5
lw	$2,9468($2)
lw	$3,596($3)
sw	$16,216($sp)
move	$16,$4
sltu	$2,$2,1
sw	$31,252($sp)
andi	$3,$3,0x1
sw	$23,244($sp)
sw	$22,240($sp)
sw	$21,236($sp)
sw	$20,232($sp)
sw	$19,228($sp)
sw	$18,224($sp)
sw	$17,220($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1283
sw	$2,2916($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L1624
addiu	$3,$4,4960
.set	macro
.set	reorder

$L1283:
addiu	$3,$16,4448
addiu	$2,$16,4704
sw	$3,7964($16)
sw	$2,7968($16)
$L1284:
lw	$4,10340($16)
li	$6,16711680			# 0xff0000
lw	$5,10332($16)
addiu	$6,$6,255
srl	$2,$4,3
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$4,0x7
or	$2,$3,$2
sll	$2,$2,$6
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1625
ori	$6,$2,0x1
.set	macro
.set	reorder

lw	$21,%got(ff_golomb_vlc_len)($28)
addiu	$3,$4,32
lw	$25,%got(ff_ue_golomb_vlc_code)($28)
clz	$6,$6
li	$4,31			# 0x1f
sw	$21,164($sp)
subu	$4,$4,$6
sw	$25,172($sp)
sll	$4,$4,1
addiu	$4,$4,-31
srl	$2,$2,$4
subu	$3,$3,$4
addiu	$2,$2,-1
sw	$3,10340($16)
sw	$2,180($sp)
$L1286:
lw	$3,180($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1287
li	$2,196608			# 0x30000
.set	macro
.set	reorder

addu	$2,$fp,$2
lw	$2,-15212($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1705
li	$2,196608			# 0x30000
.set	macro
.set	reorder

lw	$3,10384($16)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L1288
lw	$25,%got(field_end)($28)
.set	macro
.set	reorder

addiu	$25,$25,%lo(field_end)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,field_end
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,88($sp)
$L1288:
li	$2,196608			# 0x30000
$L1705:
lw	$3,10456($fp)
lw	$5,10332($16)
addu	$2,$fp,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L1626
sw	$0,-15212($2)
.set	macro
.set	reorder

$L1287:
lw	$4,10340($16)
li	$6,16711680			# 0xff0000
lw	$8,164($sp)
addiu	$6,$6,255
lw	$21,172($sp)
srl	$2,$4,3
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$4,0x7
or	$2,$3,$2
sll	$2,$2,$6
srl	$2,$2,23
addu	$3,$8,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$4,$3,$4
sltu	$3,$2,10
.set	noreorder
.set	nomacro
beq	$3,$0,$L1627
sw	$4,10340($16)
.set	macro
.set	reorder

sltu	$3,$2,5
.set	noreorder
.set	nomacro
bne	$3,$0,$L1292
li	$3,65536			# 0x10000
.set	macro
.set	reorder

li	$6,1			# 0x1
addu	$3,$16,$3
addiu	$2,$2,-5
sw	$6,-5256($3)
lw	$3,%got(golomb_to_pict_type)($28)
addiu	$3,$3,%lo(golomb_to_pict_type)
addu	$2,$2,$3
lbu	$2,0($2)
sw	$2,160($sp)
li	$2,1			# 0x1
lw	$25,160($sp)
.set	noreorder
.set	nomacro
beq	$25,$2,$L1706
li	$3,1			# 0x1
.set	macro
.set	reorder

li	$2,196608			# 0x30000
$L1707:
addu	$2,$fp,$2
lw	$3,-15212($2)
bne	$3,$0,$L1628
sw	$0,188($sp)
$L1294:
lw	$8,160($sp)
li	$2,65536			# 0x10000
srl	$3,$4,3
addu	$2,$16,$2
andi	$6,$8,0x3
sw	$8,-5264($2)
addu	$5,$5,$3
sw	$6,-5260($2)
sw	$8,2904($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
#NO_APP
li	$5,16711680			# 0xff0000
srl	$2,$3,8
addiu	$5,$5,255
sll	$3,$3,8
and	$2,$2,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$3,$3,$5
or	$2,$2,$3
sll	$3,$2,16
srl	$5,$2,16
andi	$2,$4,0x7
or	$3,$3,$5
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1629
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$5,$3
li	$3,31			# 0x1f
addiu	$4,$4,32
subu	$3,$3,$5
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$4,$3
addiu	$2,$2,-1
sw	$3,10340($16)
sw	$2,176($sp)
$L1296:
lw	$3,176($sp)
sltu	$2,$3,256
.set	noreorder
.set	nomacro
beq	$2,$0,$L1630
lw	$8,176($sp)
.set	macro
.set	reorder

li	$2,35174			# 0x8966
addu	$2,$8,$2
sll	$2,$2,2
addu	$2,$fp,$2
lw	$2,4($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1631
addiu	$21,$16,12824
.set	macro
.set	reorder

addiu	$4,$2,800
move	$3,$21
sw	$21,184($sp)
$L1299:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1299
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
li	$2,35142			# 0x8946
lw	$7,12824($16)
addu	$2,$7,$2
sll	$2,$2,2
addu	$2,$fp,$2
lw	$2,4($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1632
lw	$6,%got($LC30)($28)
.set	macro
.set	reorder

addiu	$25,$16,11888
addiu	$4,$2,928
move	$3,$25
sw	$25,168($sp)
$L1301:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1301
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
lw	$5,11888($16)
lw	$4,11892($16)
lw	$3,11932($16)
lw	$2,0($16)
sw	$5,648($2)
sw	$4,652($2)
.set	noreorder
.set	nomacro
beq	$16,$fp,$L1633
sw	$3,732($2)
.set	macro
.set	reorder

$L1302:
lw	$5,11968($16)
li	$7,7			# 0x7
lw	$6,11948($16)
li	$2,2			# 0x2
lw	$8,11944($16)
sltu	$4,$5,8
lw	$3,11940($16)
movz	$5,$7,$4
subu	$2,$2,$6
mul	$2,$2,$8
sw	$3,160($16)
sll	$8,$3,2
move	$4,$5
sll	$4,$4,1
sll	$5,$3,4
sw	$8,11864($16)
sll	$3,$3,1
subu	$5,$5,$4
sw	$2,164($16)
sll	$2,$2,4
sw	$3,11868($16)
sw	$5,8($16)
.set	noreorder
.set	nomacro
beq	$6,$0,$L1303
lw	$3,11976($16)
.set	macro
.set	reorder

sltu	$4,$3,8
movz	$3,$7,$4
sll	$3,$3,1
subu	$2,$2,$3
sw	$2,12($16)
$L1304:
lw	$2,124($16)
beq	$2,$0,$L1305
lw	$2,0($16)
lw	$3,40($2)
beq	$5,$3,$L1634
$L1306:
.set	noreorder
.set	nomacro
bne	$16,$fp,$L1554
li	$17,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$25,%got(free_tables)($28)
addiu	$25,$25,%lo(free_tables)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,free_tables
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$25,%got(flush_dpb)($28)
addiu	$25,$25,%lo(flush_dpb)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,flush_dpb
1:	jalr	$25
lw	$4,0($16)
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$25,%call16(MPV_common_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_common_end
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$2,124($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1635
lw	$28,88($sp)
.set	macro
.set	reorder

$L1618:
lw	$6,11948($16)
$L1307:
lw	$7,10340($16)
li	$8,16711680			# 0xff0000
lw	$5,10332($16)
li	$9,65536			# 0x10000
addiu	$8,$8,255
lw	$4,11904($16)
srl	$2,$7,3
addu	$9,$16,$9
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$3,$3,$8
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$7,0x7
or	$2,$3,$2
sll	$2,$2,$8
subu	$3,$0,$4
addu	$4,$7,$4
srl	$2,$2,$3
li	$3,131072			# 0x20000
sw	$4,10340($16)
addu	$3,$16,$3
lw	$22,10384($fp)
sw	$2,10680($3)
sw	$0,-5244($9)
.set	noreorder
.set	nomacro
beq	$6,$0,$L1330
sw	$0,-5252($9)
.set	macro
.set	reorder

li	$2,3			# 0x3
move	$6,$0
li	$8,3			# 0x3
sw	$2,10384($16)
$L1331:
li	$18,196608			# 0x30000
li	$2,65536			# 0x10000
addu	$3,$fp,$18
addu	$2,$16,$2
lw	$3,-15212($3)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1333
sw	$6,-5248($2)
.set	macro
.set	reorder

li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$7,10680($2)
lw	$4,10700($2)
.set	noreorder
.set	nomacro
beq	$7,$4,$L1334
li	$5,1			# 0x1
.set	macro
.set	reorder

lw	$2,11904($16)
addiu	$3,$4,1
sll	$2,$5,$2
teq	$2,$0,7
div	$0,$3,$2
mfhi	$3
.set	noreorder
.set	nomacro
beq	$7,$3,$L1514
addiu	$19,$18,-16084
.set	macro
.set	reorder

lw	$21,%got($LC31)($28)
li	$20,131072			# 0x20000
addu	$19,$16,$19
.set	noreorder
.set	nomacro
b	$L1336
addu	$18,$16,$18
.set	macro
.set	reorder

$L1637:
lw	$3,10700($23)
lw	$2,11904($16)
lw	$5,2696($16)
addiu	$3,$3,1
sll	$2,$17,$2
teq	$2,$0,7
div	$0,$3,$2
mfhi	$2
sw	$2,10700($23)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_generate_sliding_window_mmcos
1:	jalr	$25
sw	$2,280($5)
.set	macro
.set	reorder

move	$4,$16
lw	$28,88($sp)
move	$5,$19
lw	$25,%call16(ff_h264_execute_ref_pic_marking)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_execute_ref_pic_marking
1:	jalr	$25
lw	$6,-15292($18)
.set	macro
.set	reorder

lw	$4,10700($23)
lw	$7,10680($23)
lw	$28,88($sp)
.set	noreorder
.set	nomacro
beq	$7,$4,$L1619
addiu	$2,$4,1
.set	macro
.set	reorder

lw	$3,11904($16)
sll	$17,$17,$3
teq	$17,$0,7
div	$0,$2,$17
mfhi	$2
beq	$7,$2,$L1636
$L1336:
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
sw	$4,16($sp)
addiu	$6,$21,%lo($LC31)
move	$4,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addu	$23,$16,$20
.set	macro
.set	reorder

move	$4,$16
lw	$28,88($sp)
lw	$25,%call16(ff_h264_frame_start)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_frame_start
1:	jalr	$25
li	$17,1			# 0x1
.set	macro
.set	reorder

move	$4,$16
lw	$28,88($sp)
.set	noreorder
.set	nomacro
bgez	$2,$L1637
lw	$25,%call16(ff_generate_sliding_window_mmcos)($28)
.set	macro
.set	reorder

$L1621:
li	$17,-1			# 0xffffffffffffffff
$L1554:
lw	$31,252($sp)
$L1703:
move	$2,$17
lw	$fp,248($sp)
lw	$23,244($sp)
lw	$22,240($sp)
lw	$21,236($sp)
lw	$20,232($sp)
lw	$19,228($sp)
lw	$18,224($sp)
lw	$17,220($sp)
lw	$16,216($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,256
.set	macro
.set	reorder

$L1628:
lw	$2,-15200($2)
xor	$2,$25,$2
sltu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L1294
sw	$2,188($sp)
.set	macro
.set	reorder

$L1626:
.set	noreorder
.set	nomacro
b	$L1287
sw	$0,2696($16)
.set	macro
.set	reorder

$L1624:
addiu	$2,$4,5216
sw	$3,7964($4)
.set	noreorder
.set	nomacro
b	$L1284
sw	$2,7968($4)
.set	macro
.set	reorder

$L1292:
addu	$3,$16,$3
sw	$0,-5256($3)
lw	$3,%got(golomb_to_pict_type)($28)
addiu	$3,$3,%lo(golomb_to_pict_type)
addu	$2,$2,$3
lbu	$2,0($2)
sw	$2,160($sp)
li	$2,1			# 0x1
lw	$25,160($sp)
.set	noreorder
.set	nomacro
bne	$25,$2,$L1707
li	$2,196608			# 0x30000
.set	macro
.set	reorder

li	$3,1			# 0x1
$L1706:
.set	noreorder
.set	nomacro
b	$L1294
sw	$3,188($sp)
.set	macro
.set	reorder

$L1305:
.set	noreorder
.set	nomacro
bne	$16,$fp,$L1554
li	$17,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1506:
lw	$25,%call16(avcodec_set_dimensions)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,avcodec_set_dimensions
1:	jalr	$25
lw	$6,12($16)
.set	macro
.set	reorder

lw	$3,11984($16)
lw	$2,11988($16)
lw	$17,0($16)
lw	$28,88($sp)
sw	$3,392($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1311
sw	$2,396($17)
.set	macro
.set	reorder

li	$2,1			# 0x1
sw	$2,396($17)
$L1311:
lw	$2,11992($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1313
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$3,11996($16)
li	$5,2			# 0x2
lw	$4,12000($16)
movn	$2,$5,$3
.set	noreorder
.set	nomacro
beq	$4,$0,$L1313
sw	$2,896($17)
.set	macro
.set	reorder

lw	$4,12004($16)
lw	$3,12008($16)
lw	$2,12012($16)
sw	$4,884($17)
sw	$3,888($17)
sw	$2,892($17)
$L1313:
lw	$2,12016($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1639
li	$3,131072			# 0x20000
.set	macro
.set	reorder

$L1316:
lw	$2,132($17)
move	$4,$17
lw	$25,496($17)
.set	noreorder
.set	nomacro
jalr	$25
lw	$5,48($2)
.set	macro
.set	reorder

lw	$18,0($16)
lw	$28,88($sp)
sw	$2,52($17)
lw	$2,132($18)
lw	$25,%call16(ff_find_hwaccel)($28)
lw	$5,52($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_find_hwaccel
1:	jalr	$25
lw	$4,8($2)
.set	macro
.set	reorder

move	$4,$16
lw	$28,88($sp)
lw	$25,%call16(MPV_common_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_common_init
1:	jalr	$25
sw	$2,872($18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L1621
lw	$28,88($sp)
.set	macro
.set	reorder

lw	$25,%got(init_scan_tables)($28)
li	$17,196608			# 0x30000
li	$3,1			# 0x1
sw	$0,10456($16)
addu	$2,$16,$17
addiu	$25,$25,%lo(init_scan_tables)
sw	$3,-15192($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_scan_tables
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$25,%call16(ff_h264_alloc_tables)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_alloc_tables
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$2,0($16)
lw	$2,612($2)
sltu	$3,$2,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L1319
lw	$28,88($sp)
.set	macro
.set	reorder

li	$19,131072			# 0x20000
sw	$fp,208($sp)
li	$8,65536			# 0x10000
addiu	$17,$17,-15272
li	$3,1			# 0x1
addiu	$7,$16,13624
li	$20,-196608			# 0xfffffffffffd0000
addu	$8,$16,$8
sw	$3,200($sp)
addu	$25,$16,$19
sw	$7,192($sp)
addiu	$23,$16,10728
addu	$18,$16,$17
sw	$8,204($sp)
addiu	$21,$16,10584
sw	$25,196($sp)
addiu	$22,$16,12816
addiu	$20,$20,15496
move	$fp,$23
move	$23,$16
$L1323:
lw	$25,%call16(av_malloc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
ori	$4,$19,0xc570
.set	macro
.set	reorder

addu	$5,$18,$20
lw	$28,88($sp)
li	$6,10584			# 0x2958
sw	$2,0($18)
move	$4,$2
lw	$5,0($5)
addiu	$17,$2,10584
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$16,$2
.set	macro
.set	reorder

move	$5,$0
lw	$28,88($sp)
move	$4,$17
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
ori	$6,$19,0x9c18
.set	macro
.set	reorder

move	$2,$21
lw	$28,88($sp)
$L1320:
lw	$6,0($2)
addiu	$2,$2,16
addiu	$17,$17,16
lw	$5,-12($2)
lw	$4,-8($2)
lw	$3,-4($2)
sw	$6,-16($17)
sw	$5,-12($17)
sw	$4,-8($17)
.set	noreorder
.set	nomacro
bne	$2,$fp,$L1320
sw	$3,-4($17)
.set	macro
.set	reorder

lw	$8,0($fp)
addiu	$4,$16,11888
lw	$6,4($fp)
lw	$5,8($fp)
lw	$2,168($sp)
sw	$8,0($17)
sw	$6,4($17)
sw	$5,8($17)
$L1321:
lw	$7,0($2)
addiu	$2,$2,16
addiu	$4,$4,16
lw	$6,-12($2)
lw	$5,-8($2)
lw	$3,-4($2)
sw	$7,-16($4)
sw	$6,-12($4)
sw	$5,-8($4)
.set	noreorder
.set	nomacro
bne	$2,$22,$L1321
sw	$3,-4($4)
.set	macro
.set	reorder

lw	$8,0($22)
addiu	$5,$16,12824
lw	$6,4($22)
lw	$2,184($sp)
sw	$8,0($4)
sw	$6,4($4)
$L1322:
lw	$7,0($2)
addiu	$2,$2,16
addiu	$5,$5,16
lw	$3,-4($2)
lw	$6,-12($2)
lw	$4,-8($2)
sw	$7,-16($5)
sw	$3,-4($5)
sw	$6,-12($5)
sw	$4,-8($5)
lw	$3,192($sp)
.set	noreorder
.set	nomacro
bne	$2,$3,$L1322
move	$4,$16
.set	macro
.set	reorder

lw	$6,0($3)
lw	$2,4($3)
addiu	$18,$18,4
lw	$25,%got(init_scan_tables)($28)
sw	$6,0($5)
addiu	$25,$25,%lo(init_scan_tables)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_scan_tables
1:	jalr	$25
sw	$2,4($5)
.set	macro
.set	reorder

li	$4,65536			# 0x10000
lw	$3,200($sp)
addu	$2,$16,$19
lw	$8,168($23)
addu	$6,$16,$4
lw	$25,196($sp)
addiu	$4,$16,10864
sll	$9,$3,4
lw	$7,204($sp)
addiu	$3,$3,1
lw	$5,10856($23)
lw	$24,10860($23)
lw	$13,8732($25)
sw	$3,200($sp)
mul	$3,$9,$8
lw	$15,-5268($7)
lw	$14,-5276($7)
lw	$9,8748($25)
lw	$7,9088($25)
lw	$12,11852($23)
lw	$11,11856($23)
addu	$8,$3,$5
lw	$10,11860($23)
lw	$3,8760($25)
lw	$31,8756($25)
lw	$17,11232($23)
lw	$25,204($sp)
lw	$28,88($sp)
lw	$5,52($23)
sw	$8,10856($16)
sw	$24,10860($16)
sw	$17,11232($16)
sw	$3,212($sp)
sw	$15,-5268($6)
sw	$14,-5276($6)
sw	$13,8732($2)
sw	$12,11852($16)
sw	$11,11856($16)
sw	$10,11860($16)
sw	$9,8748($2)
lw	$3,6636($25)
lw	$25,%call16(ff_h264_pred_init)($28)
sw	$31,8756($2)
sw	$7,9088($2)
lw	$8,212($sp)
sw	$8,8760($2)
sw	$3,6636($6)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_pred_init
1:	jalr	$25
sw	$0,2864($16)
.set	macro
.set	reorder

lw	$2,0($23)
lw	$16,200($sp)
lw	$2,612($2)
sltu	$3,$16,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L1323
lw	$28,88($sp)
.set	macro
.set	reorder

lw	$fp,208($sp)
move	$16,$23
$L1319:
.set	noreorder
.set	nomacro
beq	$2,$0,$L1618
li	$17,131072			# 0x20000
.set	macro
.set	reorder

move	$18,$0
ori	$17,$17,0xc454
.set	noreorder
.set	nomacro
b	$L1329
addu	$17,$16,$17
.set	macro
.set	reorder

$L1326:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$4,5
.set	macro
.set	reorder

lw	$28,88($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1640
sw	$2,11104($19)
.set	macro
.set	reorder

$L1328:
lw	$2,0($16)
lw	$2,612($2)
sltu	$2,$18,$2
beq	$2,$0,$L1618
$L1329:
lw	$19,0($17)
addiu	$18,$18,1
lw	$25,%call16(av_mallocz)($28)
addiu	$17,$17,4
lw	$4,160($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$4,5
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$4,160($19)
sw	$2,11100($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1326
lw	$25,%call16(av_mallocz)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L1326
lw	$6,%got($LC17)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
$L1720:
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1554
li	$17,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1303:
li	$7,3			# 0x3
sltu	$4,$3,4
movz	$3,$7,$4
sll	$3,$3,2
subu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L1304
sw	$2,12($16)
.set	macro
.set	reorder

$L1625:
lw	$3,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$8,%got(ff_ue_golomb_vlc_code)($28)
sw	$3,164($sp)
addu	$3,$3,$2
addu	$2,$8,$2
sw	$8,172($sp)
lbu	$3,0($3)
lbu	$2,0($2)
addu	$3,$3,$4
sw	$2,180($sp)
.set	noreorder
.set	nomacro
b	$L1286
sw	$3,10340($16)
.set	macro
.set	reorder

$L1629:
lw	$21,164($sp)
srl	$2,$2,23
lw	$25,172($sp)
addu	$3,$21,$2
addu	$2,$25,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$4,$3,$4
sw	$2,176($sp)
.set	noreorder
.set	nomacro
b	$L1296
sw	$4,10340($16)
.set	macro
.set	reorder

$L1341:
sw	$0,10456($fp)
$L1333:
.set	noreorder
.set	nomacro
beq	$16,$fp,$L1345
lw	$25,%call16(memcpy)($28)
.set	macro
.set	reorder

addiu	$4,$16,11660
addiu	$5,$fp,11660
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
li	$6,192			# 0xc0
.set	macro
.set	reorder

addiu	$2,$fp,2088
lw	$4,2696($fp)
addiu	$3,$16,2088
addiu	$5,$fp,2680
lw	$28,88($sp)
sw	$4,2696($16)
$L1346:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$4,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$5,$L1346
sw	$4,-4($3)
.set	macro
.set	reorder

lw	$5,0($2)
li	$17,131072			# 0x20000
lw	$4,4($2)
li	$2,196608			# 0x30000
addu	$7,$fp,$17
lw	$25,%call16(memcpy)($28)
addu	$14,$fp,$2
sw	$5,0($3)
addiu	$5,$17,10716
sw	$4,4($3)
addu	$2,$16,$2
lw	$11,10684($7)
addu	$3,$16,$17
lw	$10,10688($7)
addu	$4,$16,$5
lw	$9,10696($7)
addu	$5,$fp,$5
lw	$8,10700($7)
li	$6,128			# 0x80
lw	$13,192($fp)
lw	$15,196($fp)
lw	$12,10456($fp)
lw	$7,-15284($14)
sw	$13,192($16)
sw	$15,196($16)
sw	$12,10456($16)
sw	$11,10684($3)
sw	$10,10688($3)
sw	$9,10696($3)
sw	$8,10700($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$7,-15284($2)
.set	macro
.set	reorder

addiu	$5,$17,10844
lw	$28,88($sp)
li	$6,128			# 0x80
addu	$4,$16,$5
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$fp,$5
.set	macro
.set	reorder

addiu	$5,$17,10976
lw	$28,88($sp)
li	$6,38400			# 0x9600
addu	$4,$16,$5
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$fp,$5
.set	macro
.set	reorder

li	$5,65536			# 0x10000
lw	$28,88($sp)
li	$6,57600			# 0xe100
addiu	$5,$5,6640
addu	$4,$16,$5
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$fp,$5
.set	macro
.set	reorder

li	$5,60224			# 0xeb40
lw	$28,88($sp)
li	$6,24			# 0x18
addu	$4,$16,$5
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$fp,$5
.set	macro
.set	reorder

li	$5,60248			# 0xeb58
lw	$28,88($sp)
li	$6,8			# 0x8
addu	$4,$16,$5
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$fp,$5
.set	macro
.set	reorder

lw	$28,88($sp)
$L1345:
li	$2,131072			# 0x20000
lw	$6,2696($16)
li	$3,65536			# 0x10000
addu	$2,$16,$2
addu	$3,$16,$3
lw	$5,10680($2)
lw	$4,-5252($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1641
sw	$5,280($6)
.set	macro
.set	reorder

li	$2,1			# 0x1
$L1347:
lw	$21,180($sp)
lw	$3,188($16)
sll	$2,$21,$2
sltu	$2,$2,$3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1348
sltu	$3,$21,$3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L1348
lw	$8,180($sp)
.set	macro
.set	reorder

lw	$2,160($16)
teq	$2,$0,7
divu	$0,$8,$2
mfhi	$3
mflo	$2
sw	$3,7992($16)
sw	$3,9784($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1350
lw	$3,10384($16)
.set	macro
.set	reorder

li	$4,1			# 0x1
sll	$2,$2,$4
li	$4,2			# 0x2
sw	$2,7996($16)
.set	noreorder
.set	nomacro
beq	$3,$4,$L1642
sw	$2,9788($16)
.set	macro
.set	reorder

$L1352:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L1643
li	$2,1			# 0x1
.set	macro
.set	reorder

$L1353:
lw	$4,11904($16)
li	$3,1			# 0x1
sll	$2,$5,1
addiu	$4,$4,1
addiu	$2,$2,1
sll	$4,$3,$4
li	$3,131072			# 0x20000
addu	$3,$16,$3
sw	$2,10704($3)
sw	$4,10708($3)
$L1354:
li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$3,9472($2)
li	$2,5			# 0x5
.set	noreorder
.set	nomacro
beq	$3,$2,$L1644
li	$4,16711680			# 0xff0000
.set	macro
.set	reorder

$L1355:
lw	$6,11908($16)
.set	noreorder
.set	nomacro
beq	$6,$0,$L1357
addiu	$17,$16,10340
.set	macro
.set	reorder

lw	$7,10384($16)
$L1358:
li	$2,1			# 0x1
beq	$6,$2,$L1645
$L1361:
li	$3,131072			# 0x20000
$L1719:
lw	$9,11904($16)
lw	$8,2696($16)
addu	$3,$16,$3
lw	$5,10696($3)
lw	$10,10680($3)
lw	$4,10700($3)
sw	$5,10692($3)
slt	$3,$10,$4
beq	$3,$0,$L1367
$L1509:
li	$2,1			# 0x1
li	$3,131072			# 0x20000
sll	$2,$2,$9
addu	$3,$16,$3
addu	$5,$2,$5
sw	$5,10692($3)
$L1367:
.set	noreorder
.set	nomacro
bne	$6,$0,$L1368
li	$2,1			# 0x1
.set	macro
.set	reorder

li	$6,131072			# 0x20000
lw	$2,11912($16)
li	$3,1			# 0x1
addu	$6,$16,$6
sll	$2,$3,$2
lw	$4,10688($6)
lw	$3,10660($6)
slt	$5,$3,$4
.set	noreorder
.set	nomacro
bne	$5,$0,$L1646
srl	$5,$2,31
.set	macro
.set	reorder

slt	$5,$4,$3
.set	noreorder
.set	nomacro
beq	$5,$0,$L1370
srl	$5,$2,31
.set	macro
.set	reorder

subu	$4,$4,$3
addu	$5,$5,$2
sra	$5,$5,1
subu	$5,$0,$5
slt	$4,$4,$5
bne	$4,$0,$L1647
$L1370:
li	$4,131072			# 0x20000
$L1712:
addu	$4,$16,$4
lw	$2,10684($4)
sw	$2,10664($4)
$L1371:
addu	$2,$3,$2
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$7,$3,$L1648
li	$3,131072			# 0x20000
.set	macro
.set	reorder

$L1519:
move	$3,$2
$L1372:
li	$4,2			# 0x2
.set	noreorder
.set	nomacro
beq	$7,$4,$L1649
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$4,$L1650
sw	$2,268($8)
.set	macro
.set	reorder

$L1384:
sw	$3,272($8)
$L1385:
slt	$4,$2,$3
lw	$5,12884($16)
movz	$2,$3,$4
.set	noreorder
.set	nomacro
bne	$5,$0,$L1651
sw	$2,276($8)
.set	macro
.set	reorder

$L1386:
li	$2,65536			# 0x10000
lw	$3,12848($16)
lw	$4,12844($16)
addu	$2,$16,$2
sw	$3,6628($2)
li	$3,1			# 0x1
sw	$4,6624($2)
lw	$5,-5260($2)
.set	noreorder
.set	nomacro
beq	$5,$3,$L1389
li	$3,3			# 0x3
.set	macro
.set	reorder

beq	$5,$3,$L1390
lw	$6,10332($16)
lw	$4,10340($16)
$L1391:
srl	$3,$4,3
andi	$2,$4,0x7
addu	$3,$6,$3
addiu	$7,$4,1
lbu	$3,0($3)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1652
sw	$7,10340($16)
.set	macro
.set	reorder

li	$2,3			# 0x3
$L1716:
.set	noreorder
.set	nomacro
beq	$5,$2,$L1653
li	$2,65536			# 0x10000
.set	macro
.set	reorder

li	$3,1			# 0x1
addu	$2,$16,$2
sw	$3,6632($2)
$L1400:
lw	$3,188($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1654
lw	$25,%call16(ff_h264_fill_default_ref_list)($28)
.set	macro
.set	reorder

$L1401:
li	$17,65536			# 0x10000
li	$19,1			# 0x1
addu	$18,$16,$17
lw	$2,-5260($18)
.set	noreorder
.set	nomacro
beq	$2,$19,$L1404
lw	$25,%call16(ff_h264_decode_ref_pic_list_reordering)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_decode_ref_pic_list_reordering
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L1621
lw	$28,88($sp)
.set	macro
.set	reorder

lw	$2,-5260($18)
.set	noreorder
.set	nomacro
beq	$2,$19,$L1404
lw	$25,%call16(ff_copy_picture)($28)
.set	macro
.set	reorder

addiu	$17,$17,6640
addiu	$4,$16,288
addu	$2,$16,$17
move	$5,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_copy_picture
1:	jalr	$25
sw	$2,2688($16)
.set	macro
.set	reorder

li	$2,3			# 0x3
lw	$3,-5260($18)
.set	noreorder
.set	nomacro
beq	$3,$2,$L1655
lw	$28,88($sp)
.set	macro
.set	reorder

$L1404:
lw	$2,12852($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1405
li	$2,65536			# 0x10000
.set	macro
.set	reorder

addu	$2,$16,$2
lw	$3,-5260($2)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L1708
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L1405:
lw	$2,12856($16)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L1656
li	$3,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L1657
li	$2,65536			# 0x10000
.set	macro
.set	reorder

li	$3,65536			# 0x10000
$L1714:
li	$2,196608			# 0x30000
addu	$3,$16,$3
addu	$2,$16,$2
sb	$0,-5232($3)
sw	$0,-15172($2)
sw	$0,-15164($2)
sw	$0,-15168($2)
sw	$0,-15160($2)
$L1440:
li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$2,9468($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1658
lw	$25,%call16(ff_h264_decode_ref_pic_marking)($28)
.set	macro
.set	reorder

$L1441:
li	$17,65536			# 0x10000
addu	$17,$16,$17
lw	$2,-5252($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1659
lw	$25,%call16(ff_h264_fill_mbaff_ref_list)($28)
.set	macro
.set	reorder

$L1443:
li	$2,65536			# 0x10000
li	$3,3			# 0x3
addu	$2,$16,$2
lw	$4,-5260($2)
beq	$4,$3,$L1660
$L1446:
lw	$25,%call16(ff_h264_direct_ref_list_init)($28)
$L1711:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_direct_ref_list_init
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$4,-5260($2)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$4,$2,$L1450
lw	$28,88($sp)
.set	macro
.set	reorder

lw	$2,12828($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1661
li	$7,16711680			# 0xff0000
.set	macro
.set	reorder

$L1450:
lw	$5,10332($16)
lw	$6,10340($16)
$L1449:
li	$3,131072			# 0x20000
lw	$7,12860($16)
srl	$2,$6,3
addu	$3,$16,$3
li	$8,16711680			# 0xff0000
addu	$2,$5,$2
sw	$0,8752($3)
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$3,$3,$8
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$6,0x7
or	$3,$3,$2
sll	$2,$3,$8
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1662
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$8,$3
li	$3,31			# 0x1f
addiu	$6,$6,32
subu	$3,$3,$8
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$6,$3
andi	$6,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$6,$0,$L1453
sw	$3,10340($16)
.set	macro
.set	reorder

subu	$2,$0,$2
$L1453:
addu	$7,$7,$2
sltu	$2,$7,52
.set	noreorder
.set	nomacro
beq	$2,$0,$L1663
li	$3,65536			# 0x10000
.set	macro
.set	reorder

sw	$7,2872($16)
addu	$2,$16,$7
addu	$3,$16,$3
lbu	$7,13116($2)
lbu	$6,13372($2)
lw	$2,-5264($3)
li	$3,6			# 0x6
sw	$7,10740($16)
.set	noreorder
.set	nomacro
beq	$2,$3,$L1664
sw	$6,10744($16)
.set	macro
.set	reorder

addiu	$2,$2,-5
sltu	$2,$2,2
bne	$2,$0,$L1665
$L1459:
li	$2,131072			# 0x20000
lw	$3,12876($16)
li	$6,1			# 0x1
addu	$2,$16,$2
sw	$6,9456($2)
sw	$0,9460($2)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1666
sw	$0,9464($2)
.set	macro
.set	reorder

$L1462:
lw	$22,0($16)
lw	$2,700($22)
slt	$3,$2,48
.set	noreorder
.set	nomacro
beq	$3,$0,$L1475
slt	$3,$2,32
.set	macro
.set	reorder

bne	$3,$0,$L1476
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$4,$2,$L1709
li	$2,131072			# 0x20000
.set	macro
.set	reorder

$L1475:
li	$2,131072			# 0x20000
addu	$2,$16,$2
$L1721:
sw	$0,9456($2)
$L1480:
li	$5,131072			# 0x20000
$L1713:
lw	$7,12872($16)
lw	$3,12868($16)
li	$4,196608			# 0x30000
addu	$5,$16,$5
lw	$21,160($sp)
addu	$4,$fp,$4
slt	$2,$7,$3
lw	$6,9460($5)
li	$17,65536			# 0x10000
lw	$5,9464($5)
movz	$3,$7,$2
addu	$17,$16,$17
slt	$7,$6,$5
lw	$2,-15212($4)
movz	$6,$5,$7
slt	$5,$3,0
movn	$3,$0,$5
li	$5,67			# 0x43
addiu	$2,$2,1
subu	$5,$5,$6
slt	$6,$2,16
subu	$3,$5,$3
sw	$3,10748($16)
sw	$21,-15200($4)
sw	$2,-15212($4)
.set	noreorder
.set	nomacro
beq	$6,$0,$L1667
sw	$2,-5272($17)
.set	macro
.set	reorder

$L1483:
li	$3,65536			# 0x10000
li	$14,196608			# 0x30000
ori	$23,$3,0xfaf0
addiu	$20,$3,16320
addiu	$18,$23,12140
addiu	$19,$23,12016
li	$17,131072			# 0x20000
addu	$3,$16,$3
addu	$20,$16,$20
move	$4,$0
addiu	$11,$sp,96
sw	$3,160($sp)
addiu	$25,$sp,160
li	$24,60			# 0x3c
addu	$14,$16,$14
addu	$18,$16,$18
addu	$17,$16,$17
addu	$19,$16,$19
li	$21,-1			# 0xffffffffffffffff
li	$15,64			# 0x40
li	$13,32			# 0x20
li	$fp,1			# 0x1
$L1498:
andi	$9,$2,0xf
sll	$9,$9,1
addiu	$8,$20,-9648
addu	$9,$9,$4
move	$7,$11
sll	$9,$9,8
addu	$9,$9,$23
addu	$9,$16,$9
$L1494:
lw	$2,-32($8)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1485
sw	$24,0($7)
.set	macro
.set	reorder

lw	$10,-15284($14)
.set	noreorder
.set	nomacro
blez	$10,$L1486
lw	$6,0($8)
.set	macro
.set	reorder

lw	$2,10716($17)
lw	$2,32($2)
beq	$2,$6,$L1668
move	$2,$0
.set	noreorder
.set	nomacro
b	$L1489
move	$3,$19
.set	macro
.set	reorder

$L1490:
lw	$5,0($3)
lw	$5,32($5)
.set	noreorder
.set	nomacro
beq	$5,$6,$L1487
addiu	$3,$3,4
.set	macro
.set	reorder

$L1489:
addiu	$2,$2,1
bne	$2,$10,$L1490
$L1486:
lw	$12,-15288($14)
.set	noreorder
.set	nomacro
blez	$12,$L1485
move	$2,$18
.set	macro
.set	reorder

move	$3,$0
$L1493:
lw	$5,0($2)
.set	noreorder
.set	nomacro
beq	$5,$0,$L1492
addiu	$2,$2,4
.set	macro
.set	reorder

lw	$5,32($5)
beq	$5,$6,$L1669
$L1492:
addiu	$3,$3,1
bne	$3,$12,$L1493
$L1485:
addiu	$7,$7,4
.set	noreorder
.set	nomacro
bne	$25,$7,$L1494
addiu	$8,$8,600
.set	macro
.set	reorder

addiu	$6,$20,-9600
$L1698:
sw	$21,4($9)
move	$5,$0
sw	$21,0($9)
$L1495:
addu	$2,$11,$5
lw	$3,0($6)
addu	$7,$9,$5
addiu	$5,$5,4
lw	$2,0($2)
andi	$3,$3,0x3
addiu	$6,$6,600
sll	$2,$2,2
addu	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$5,$15,$L1495
sw	$2,8($7)
.set	macro
.set	reorder

addiu	$6,$9,80
sw	$21,76($9)
move	$5,$0
sw	$21,72($9)
move	$7,$20
$L1496:
srl	$3,$5,1
lw	$2,0($7)
addiu	$6,$6,4
sll	$3,$3,2
andi	$8,$2,0x3
addu	$3,$11,$3
addiu	$5,$5,1
addiu	$7,$7,600
lw	$2,0($3)
sll	$2,$2,2
addu	$2,$2,$8
.set	noreorder
.set	nomacro
bne	$5,$13,$L1496
sw	$2,-4($6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$fp,$L1497
addiu	$20,$20,28800
.set	macro
.set	reorder

lw	$3,160($sp)
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
b	$L1498
lw	$2,-5272($3)
.set	macro
.set	reorder

$L1640:
lw	$2,160($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1328
lw	$6,%got($LC17)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1720
li	$5,16			# 0x10
.set	macro
.set	reorder

$L1330:
srl	$8,$4,3
andi	$3,$4,0x7
addu	$8,$5,$8
addiu	$2,$4,1
lbu	$7,0($8)
sll	$3,$7,$3
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L1332
sw	$2,10340($16)
.set	macro
.set	reorder

srl	$3,$2,3
andi	$8,$2,0x7
addu	$5,$5,$3
addiu	$2,$4,2
li	$6,1			# 0x1
lbu	$3,0($5)
sw	$2,10340($16)
sll	$8,$3,$8
andi	$8,$8,0x00ff
srl	$8,$8,7
addiu	$8,$8,1
.set	noreorder
.set	nomacro
b	$L1331
sw	$8,10384($16)
.set	macro
.set	reorder

$L1639:
lw	$2,12024($16)
addu	$3,$16,$3
lw	$3,9444($3)
sltu	$3,$3,44
.set	noreorder
.set	nomacro
beq	$3,$0,$L1317
move	$8,$0
.set	macro
.set	reorder

srl	$8,$2,31
sll	$2,$2,1
$L1317:
lw	$6,12020($16)
move	$3,$0
lw	$25,%call16(av_reduce)($28)
addiu	$4,$17,32
sw	$2,16($sp)
li	$2,1073741824			# 0x40000000
addiu	$5,$17,36
sw	$8,20($sp)
move	$7,$0
sw	$3,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_reduce
1:	jalr	$25
sw	$2,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1316
lw	$17,0($16)
.set	macro
.set	reorder

$L1641:
lw	$2,10384($16)
xori	$2,$2,0x3
.set	noreorder
.set	nomacro
b	$L1347
sltu	$2,$0,$2
.set	macro
.set	reorder

$L1634:
lw	$4,12($16)
lw	$3,44($2)
bne	$4,$3,$L1306
lw	$3,392($2)
lw	$7,11988($16)
lw	$5,396($2)
lw	$4,11984($16)
mult	$3,$7
mflo	$2
mfhi	$3
mult	$4,$5
mflo	$4
.set	noreorder
.set	nomacro
bne	$4,$2,$L1306
mfhi	$5
.set	macro
.set	reorder

beq	$5,$3,$L1307
b	$L1306
$L1332:
lw	$2,11952($16)
li	$3,3			# 0x3
li	$8,3			# 0x3
sw	$3,10384($16)
.set	noreorder
.set	nomacro
b	$L1331
sw	$2,-5252($9)
.set	macro
.set	reorder

$L1635:
.set	noreorder
.set	nomacro
b	$L1506
lw	$5,8($16)
.set	macro
.set	reorder

$L1661:
lw	$6,10340($16)
lw	$5,10332($16)
addiu	$7,$7,255
lw	$8,164($sp)
srl	$2,$6,3
lw	$21,172($sp)
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$3,$3,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$7,$6,0x7
or	$2,$3,$2
sll	$2,$2,$7
srl	$2,$2,23
addu	$3,$8,$2
addu	$2,$21,$2
lbu	$7,0($3)
lbu	$3,0($2)
addu	$6,$7,$6
sltu	$2,$3,3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1670
sw	$6,10340($16)
.set	macro
.set	reorder

li	$2,196608			# 0x30000
addu	$2,$16,$2
.set	noreorder
.set	nomacro
b	$L1449
sw	$3,-15280($2)
.set	macro
.set	reorder

$L1650:
.set	noreorder
.set	nomacro
b	$L1385
lw	$3,272($8)
.set	macro
.set	reorder

$L1648:
addu	$3,$16,$3
lw	$3,10668($3)
sw	$2,268($8)
.set	noreorder
.set	nomacro
b	$L1384
addu	$3,$2,$3
.set	macro
.set	reorder

$L1514:
move	$4,$7
$L1334:
lw	$2,10456($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1337
xori	$2,$8,0x3
.set	macro
.set	reorder

li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$8,$2,$L1710
xori	$2,$8,0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$22,$8,$L1338
li	$2,131072			# 0x20000
.set	macro
.set	reorder

addu	$2,$16,$2
lw	$2,9468($2)
beq	$2,$0,$L1341
lw	$2,2696($fp)
lw	$3,80($2)
beq	$3,$0,$L1341
lw	$2,280($2)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1341
li	$2,1			# 0x1
.set	macro
.set	reorder

sw	$0,2696($fp)
.set	noreorder
.set	nomacro
b	$L1342
sw	$2,10456($fp)
.set	macro
.set	reorder

$L1668:
move	$2,$0
$L1487:
.set	noreorder
.set	nomacro
b	$L1486
sw	$2,0($7)
.set	macro
.set	reorder

$L1669:
addu	$3,$10,$3
addiu	$7,$7,4
addiu	$8,$8,600
.set	noreorder
.set	nomacro
bne	$25,$7,$L1494
sw	$3,-4($7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1698
addiu	$6,$20,-9600
.set	macro
.set	reorder

$L1497:
lw	$2,64($16)
li	$3,65536			# 0x10000
li	$4,16			# 0x10
addu	$3,$16,$3
andi	$2,$2,0x4000
movn	$4,$0,$2
lw	$3,-5252($3)
move	$2,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L1525
sw	$4,11880($16)
.set	macro
.set	reorder

lw	$3,10384($16)
xori	$3,$3,0x3
movn	$2,$0,$3
move	$3,$2
$L1500:
lw	$17,404($22)
andi	$17,$17,0x1
.set	noreorder
.set	nomacro
beq	$17,$0,$L1554
sw	$3,11884($16)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
lw	$3,10384($16)
addu	$2,$16,$2
lw	$fp,-5272($2)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L1526
li	$2,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$2,$L1527
lw	$17,%got($LC20)($28)
.set	macro
.set	reorder

lw	$17,%got($LC18)($28)
addiu	$17,$17,%lo($LC18)
$L1501:
li	$18,65536			# 0x10000
lw	$25,%call16(av_get_pict_type_char)($28)
addu	$18,$16,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_get_pict_type_char
1:	jalr	$25
lw	$4,-5264($18)
.set	macro
.set	reorder

lw	$3,-5256($18)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1528
lw	$28,88($sp)
.set	macro
.set	reorder

lw	$12,%got($LC22)($28)
addiu	$12,$12,%lo($LC22)
$L1502:
li	$3,131072			# 0x20000
addu	$3,$16,$3
lw	$4,9472($3)
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$4,$3,$L1529
lw	$11,%got($LC23)($28)
.set	macro
.set	reorder

lw	$11,%got($LC22)($28)
addiu	$11,$11,%lo($LC22)
$L1503:
li	$3,131072			# 0x20000
lw	$7,2696($16)
li	$4,65536			# 0x10000
lw	$15,2872($16)
addu	$3,$16,$3
addu	$4,$16,$4
lw	$21,268($7)
lw	$10,9460($3)
lw	$9,9464($3)
lbu	$13,-5232($4)
srl	$6,$10,31
lw	$23,10680($3)
srl	$5,$9,31
lw	$14,9456($3)
addu	$10,$6,$10
lw	$20,272($7)
addu	$9,$5,$9
lw	$19,6624($4)
sra	$10,$10,1
lw	$18,6628($4)
sra	$9,$9,1
li	$3,1			# 0x1
addiu	$10,$10,-26
.set	noreorder
.set	nomacro
beq	$13,$3,$L1672
addiu	$9,$9,-26
.set	macro
.set	reorder

$L1531:
lw	$8,%got($LC22)($28)
addiu	$8,$8,%lo($LC22)
$L1504:
li	$3,65536			# 0x10000
addu	$16,$16,$3
li	$3,3			# 0x3
lw	$4,-5264($16)
.set	noreorder
.set	nomacro
beq	$4,$3,$L1673
lw	$3,%got($LC22)($28)
.set	macro
.set	reorder

addiu	$3,$3,%lo($LC22)
$L1505:
lw	$16,180($sp)
li	$5,48			# 0x30
lw	$6,%got($LC39)($28)
move	$4,$22
lw	$25,%call16(av_log)($28)
move	$7,$fp
sw	$17,16($sp)
move	$17,$0
sw	$16,20($sp)
addiu	$6,$6,%lo($LC39)
sw	$2,24($sp)
sw	$12,28($sp)
sw	$11,32($sp)
sw	$23,40($sp)
sw	$21,44($sp)
sw	$20,48($sp)
sw	$19,52($sp)
sw	$18,56($sp)
sw	$15,60($sp)
sw	$14,64($sp)
sw	$10,68($sp)
sw	$9,72($sp)
sw	$13,76($sp)
sw	$8,80($sp)
sw	$3,84($sp)
lw	$16,176($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$16,36($sp)
.set	macro
.set	reorder

move	$2,$17
lw	$31,252($sp)
lw	$fp,248($sp)
lw	$23,244($sp)
lw	$22,240($sp)
lw	$21,236($sp)
lw	$20,232($sp)
lw	$19,228($sp)
lw	$18,224($sp)
lw	$17,220($sp)
lw	$16,216($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,256
.set	macro
.set	reorder

$L1672:
lbu	$3,-5231($4)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1531
lw	$8,%got($LC24)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1504
addiu	$8,$8,%lo($LC24)
.set	macro
.set	reorder

$L1350:
xori	$4,$3,0x3
sltu	$4,$0,$4
sll	$2,$2,$4
li	$4,2			# 0x2
sw	$2,7996($16)
.set	noreorder
.set	nomacro
bne	$3,$4,$L1352
sw	$2,9788($16)
.set	macro
.set	reorder

$L1642:
addiu	$2,$2,1
sw	$2,7996($16)
.set	noreorder
.set	nomacro
b	$L1353
sw	$2,9788($16)
.set	macro
.set	reorder

$L1368:
.set	noreorder
.set	nomacro
beq	$6,$2,$L1510
li	$3,131072			# 0x20000
.set	macro
.set	reorder

addu	$3,$16,$3
lw	$2,10692($3)
lw	$3,9468($3)
addu	$2,$10,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L1519
sll	$2,$2,1
.set	macro
.set	reorder

addiu	$2,$2,-1
.set	noreorder
.set	nomacro
b	$L1372
move	$3,$2
.set	macro
.set	reorder

$L1633:
li	$2,131072			# 0x20000
lw	$8,176($sp)
addu	$2,$16,$2
lw	$3,10652($2)
.set	noreorder
.set	nomacro
beq	$3,$8,$L1302
lw	$25,%got(init_dequant_tables)($28)
.set	macro
.set	reorder

move	$4,$16
addiu	$25,$25,%lo(init_dequant_tables)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_dequant_tables
1:	jalr	$25
sw	$8,10652($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1302
lw	$28,88($sp)
.set	macro
.set	reorder

$L1525:
.set	noreorder
.set	nomacro
b	$L1500
move	$3,$0
.set	macro
.set	reorder

$L1636:
move	$4,$7
$L1619:
.set	noreorder
.set	nomacro
b	$L1334
lw	$8,10384($16)
.set	macro
.set	reorder

$L1662:
lw	$25,164($sp)
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$8,$25,$2
addu	$2,$3,$2
lbu	$3,0($8)
lb	$2,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
b	$L1453
sw	$6,10340($16)
.set	macro
.set	reorder

$L1659:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_fill_mbaff_ref_list
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$2,2			# 0x2
lw	$3,12856($16)
.set	noreorder
.set	nomacro
bne	$3,$2,$L1443
lw	$28,88($sp)
.set	macro
.set	reorder

lw	$3,-5260($17)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$3,$2,$L1711
lw	$25,%call16(ff_h264_direct_ref_list_init)($28)
.set	macro
.set	reorder

lw	$17,%got(implicit_weight_table)($28)
move	$5,$0
addiu	$17,$17,%lo(implicit_weight_table)
move	$25,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,implicit_weight_table
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$5,1			# 0x1
move	$25,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,implicit_weight_table
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1443
lw	$28,88($sp)
.set	macro
.set	reorder

$L1654:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_fill_default_ref_list
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1401
lw	$28,88($sp)
.set	macro
.set	reorder

$L1643:
lw	$3,11904($16)
sll	$3,$2,$3
li	$2,131072			# 0x20000
addu	$2,$16,$2
sw	$5,10704($2)
.set	noreorder
.set	nomacro
b	$L1354
sw	$3,10708($2)
.set	macro
.set	reorder

$L1658:
addiu	$5,$16,10332
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_decode_ref_pic_marking
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1441
lw	$28,88($sp)
.set	macro
.set	reorder

$L1645:
lw	$2,11916($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1674
li	$8,16711680			# 0xff0000
.set	macro
.set	reorder

li	$3,131072			# 0x20000
$L1718:
lw	$9,11904($16)
lw	$8,2696($16)
addu	$3,$16,$3
lw	$5,10696($3)
lw	$10,10680($3)
lw	$2,10700($3)
slt	$2,$10,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1509
sw	$5,10692($3)
.set	macro
.set	reorder

$L1510:
lw	$5,11928($16)
.set	noreorder
.set	nomacro
beq	$5,$0,$L1375
li	$3,131072			# 0x20000
.set	macro
.set	reorder

li	$2,131072			# 0x20000
addu	$2,$16,$2
lw	$3,10692($2)
lw	$11,9468($2)
.set	noreorder
.set	nomacro
bne	$11,$0,$L1376
addu	$10,$10,$3
.set	macro
.set	reorder

blez	$10,$L1377
addiu	$10,$10,-1
$L1376:
blez	$5,$L1517
addiu	$4,$16,12032
$L1717:
move	$2,$0
move	$3,$0
$L1379:
lh	$9,0($4)
addiu	$3,$3,1
addiu	$4,$4,2
slt	$6,$3,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L1379
addu	$2,$2,$9
.set	macro
.set	reorder

$L1378:
.set	noreorder
.set	nomacro
blez	$10,$L1518
addiu	$3,$10,-1
.set	macro
.set	reorder

teq	$5,$0,7
div	$0,$3,$5
mfhi	$3
mflo	$5
.set	noreorder
.set	nomacro
bltz	$3,$L1380
mul	$2,$2,$5
.set	macro
.set	reorder

addiu	$4,$3,6017
addiu	$3,$16,12032
sll	$4,$4,1
addu	$4,$16,$4
$L1381:
lh	$5,0($3)
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$4,$L1381
addu	$2,$2,$5
.set	macro
.set	reorder

$L1380:
beq	$11,$0,$L1508
$L1382:
li	$4,131072			# 0x20000
lw	$3,11924($16)
addu	$4,$16,$4
lw	$5,10672($4)
addu	$2,$2,$5
li	$5,3			# 0x3
.set	noreorder
.set	nomacro
bne	$7,$5,$L1372
addu	$3,$2,$3
.set	macro
.set	reorder

lw	$4,10676($4)
sw	$2,268($8)
.set	noreorder
.set	nomacro
b	$L1384
addu	$3,$3,$4
.set	macro
.set	reorder

$L1649:
.set	noreorder
.set	nomacro
b	$L1384
lw	$2,268($8)
.set	macro
.set	reorder

$L1389:
.set	noreorder
.set	nomacro
b	$L1400
sw	$0,6632($2)
.set	macro
.set	reorder

$L1651:
lw	$4,10340($16)
li	$5,16711680			# 0xff0000
lw	$2,10332($16)
addiu	$5,$5,255
srl	$3,$4,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$3,$3,$5
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$5,$4,0x7
or	$2,$3,$2
sll	$2,$2,$5
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1675
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$5,$3
li	$3,31			# 0x1f
addiu	$4,$4,32
subu	$3,$3,$5
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$4,$3
addiu	$2,$2,-1
sw	$3,10340($16)
$L1388:
li	$3,131072			# 0x20000
addu	$3,$16,$3
.set	noreorder
.set	nomacro
b	$L1386
sw	$2,10712($3)
.set	macro
.set	reorder

$L1357:
lw	$5,10340($16)
li	$9,16711680			# 0xff0000
lw	$4,10332($16)
li	$18,131072			# 0x20000
addiu	$9,$9,255
lw	$8,11912($16)
srl	$2,$5,3
lw	$7,12832($16)
addu	$18,$16,$18
addu	$2,$4,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$9
li	$9,-16777216			# 0xffffffffff000000
ori	$9,$9,0xff00
and	$3,$3,$9
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$9,$5,0x7
or	$2,$3,$2
sll	$2,$2,$9
subu	$3,$0,$8
addu	$5,$5,$8
srl	$2,$2,$3
sw	$5,10340($16)
sw	$2,10660($18)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$7,$2,$L1359
li	$2,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1361
lw	$7,10384($16)
.set	macro
.set	reorder

$L1528:
lw	$12,%got($LC21)($28)
.set	noreorder
.set	nomacro
b	$L1502
addiu	$12,$12,%lo($LC21)
.set	macro
.set	reorder

$L1646:
subu	$4,$4,$3
addu	$5,$5,$2
sra	$5,$5,1
slt	$4,$4,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L1712
li	$4,131072			# 0x20000
.set	macro
.set	reorder

lw	$4,10684($6)
addu	$2,$2,$4
.set	noreorder
.set	nomacro
b	$L1371
sw	$2,10664($6)
.set	macro
.set	reorder

$L1660:
lw	$2,5140($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1711
lw	$25,%call16(ff_h264_direct_ref_list_init)($28)
.set	macro
.set	reorder

lw	$25,%call16(ff_h264_direct_dist_scale_factor)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_direct_dist_scale_factor
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1446
lw	$28,88($sp)
.set	macro
.set	reorder

$L1667:
lw	$6,%got($LC38)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC38)
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$2,-5272($17)
.set	noreorder
.set	nomacro
b	$L1483
lw	$22,0($16)
.set	macro
.set	reorder

$L1666:
lw	$8,10340($16)
lw	$21,164($sp)
lw	$25,172($sp)
srl	$6,$8,3
addu	$6,$5,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($6)  
lwr $7, 0($6)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$6,$7,8
li	$7,16711680			# 0xff0000
addiu	$7,$7,255
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$6,$6,$7
or	$3,$3,$6
sll	$6,$3,16
srl	$3,$3,16
andi	$7,$8,0x7
or	$3,$6,$3
sll	$3,$3,$7
srl	$3,$3,23
addu	$6,$21,$3
addu	$3,$25,$3
lbu	$6,0($6)
lbu	$7,0($3)
addu	$8,$6,$8
sltu	$3,$7,3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1676
sw	$8,10340($16)
.set	macro
.set	reorder

slt	$3,$7,2
bne	$3,$0,$L1464
sw	$7,9456($2)
srl	$2,$8,3
$L1702:
li	$3,16711680			# 0xff0000
addu	$2,$5,$2
addiu	$3,$3,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$6,8
sll	$6,$6,8
and	$2,$2,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$6,$6,$3
or	$6,$2,$6
sll	$2,$6,16
srl	$6,$6,16
andi	$3,$8,0x7
or	$2,$2,$6
sll	$2,$2,$3
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1677
li	$6,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$2,0x1
clz	$3,$3
addiu	$7,$8,32
subu	$3,$6,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$7,$3
andi	$6,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$6,$0,$L1468
sw	$3,10340($16)
.set	macro
.set	reorder

subu	$2,$0,$2
$L1468:
li	$7,131072			# 0x20000
sll	$2,$2,1
srl	$6,$3,3
addu	$7,$16,$7
addu	$5,$5,$6
sw	$2,9460($7)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
srl	$2,$6,8
sll	$5,$6,8
li	$6,16711680			# 0xff0000
addiu	$6,$6,255
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$5,$5,$6
or	$5,$2,$5
sll	$2,$5,16
srl	$5,$5,16
andi	$6,$3,0x7
or	$2,$2,$5
sll	$2,$2,$6
li	$5,134217728			# 0x8000000
sltu	$5,$2,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L1678
lw	$8,164($sp)
.set	macro
.set	reorder

ori	$5,$2,0x1
clz	$6,$5
li	$5,31			# 0x1f
addiu	$3,$3,32
subu	$5,$5,$6
sll	$5,$5,1
addiu	$5,$5,-31
srl	$2,$2,$5
subu	$3,$3,$5
andi	$5,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$5,$0,$L1472
sw	$3,10340($16)
.set	macro
.set	reorder

subu	$2,$0,$2
$L1472:
li	$3,131072			# 0x20000
sll	$2,$2,1
addu	$3,$16,$3
.set	noreorder
.set	nomacro
b	$L1462
sw	$2,9464($3)
.set	macro
.set	reorder

$L1664:
lw	$2,10340($16)
addiu	$7,$2,1
sw	$7,10340($16)
$L1458:
srl	$2,$7,3
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$3,$6,8
addiu	$2,$2,255
sll	$6,$6,8
and	$3,$3,$2
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$6,$6,$2
or	$3,$3,$6
sll	$2,$3,16
srl	$3,$3,16
andi	$6,$7,0x7
or	$2,$2,$3
sll	$2,$2,$6
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1679
li	$3,31			# 0x1f
.set	macro
.set	reorder

ori	$2,$2,0x1
clz	$2,$2
subu	$2,$3,$2
sll	$2,$2,1
subu	$2,$7,$2
addiu	$2,$2,63
.set	noreorder
.set	nomacro
b	$L1459
sw	$2,10340($16)
.set	macro
.set	reorder

$L1476:
slt	$3,$2,16
.set	noreorder
.set	nomacro
beq	$3,$0,$L1680
slt	$2,$2,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L1709
li	$2,131072			# 0x20000
.set	macro
.set	reorder

$L1479:
li	$2,131072			# 0x20000
li	$3,1			# 0x1
addu	$2,$16,$2
lw	$17,9456($2)
.set	noreorder
.set	nomacro
bne	$17,$3,$L1480
li	$18,196608			# 0x30000
.set	macro
.set	reorder

addu	$18,$fp,$18
lw	$3,-15208($18)
slt	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L1713
li	$5,131072			# 0x20000
.set	macro
.set	reorder

lw	$3,596($22)
andi	$3,$3,0x1
.set	noreorder
.set	nomacro
beq	$3,$0,$L1481
li	$3,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1713
sw	$3,9456($2)
.set	macro
.set	reorder

$L1656:
li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$3,-5260($2)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$3,$2,$L1714
li	$3,65536			# 0x10000
.set	macro
.set	reorder

li	$2,65536			# 0x10000
$L1708:
li	$5,16711680			# 0xff0000
addu	$2,$16,$2
addiu	$5,$5,255
sb	$0,-5232($2)
sb	$0,-5231($2)
lw	$6,10340($16)
lw	$7,10332($16)
srl	$3,$6,3
andi	$8,$6,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
srl	$4,$4,8
sll	$3,$3,8
and	$4,$4,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$5,$3,$5
or	$5,$4,$5
sll	$4,$5,16
srl	$2,$5,16
li	$3,134217728			# 0x8000000
or	$2,$4,$2
sll	$2,$2,$8
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1681
li	$4,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$2,0x1
clz	$3,$3
addiu	$6,$6,32
subu	$3,$4,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$6,$3
addiu	$2,$2,-1
sw	$3,10340($16)
$L1410:
sll	$6,$2,24
li	$4,65536			# 0x10000
sra	$6,$6,24
srl	$2,$3,3
addu	$4,$16,$4
li	$5,16711680			# 0xff0000
addu	$7,$7,$2
sb	$6,-5230($4)
addiu	$5,$5,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($7)  
lwr $4, 0($7)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$4,$4,$5
or	$4,$2,$4
sll	$2,$4,16
srl	$4,$4,16
andi	$5,$3,0x7
or	$2,$2,$4
sll	$2,$2,$5
li	$4,134217728			# 0x8000000
sltu	$4,$2,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L1682
ori	$4,$2,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$3,$3,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$2,$2,$4
subu	$3,$3,$4
addiu	$2,$2,-1
sw	$3,10340($16)
$L1412:
sll	$2,$2,24
sw	$fp,188($sp)
li	$25,65536			# 0x10000
lw	$fp,164($sp)
sra	$2,$2,24
li	$22,131072			# 0x20000
li	$12,1			# 0x1
addu	$25,$16,$25
ori	$22,$22,0xc4bc
addiu	$3,$22,8
sb	$2,-5229($25)
sll	$6,$12,$6
sll	$12,$12,$2
li	$2,60692			# 0xed14
li	$17,60310			# 0xeb96
addu	$2,$16,$2
addu	$3,$16,$3
li	$14,16711680			# 0xff0000
li	$13,-16777216			# 0xffffffffff000000
sw	$2,168($sp)
addu	$22,$16,$22
sw	$3,184($sp)
addu	$17,$16,$17
li	$23,127			# 0x7f
addiu	$14,$14,255
li	$15,134217728			# 0x8000000
li	$31,1			# 0x1
li	$19,1			# 0x1
li	$24,31			# 0x1f
ori	$13,$13,0xff00
$L1439:
sw	$0,0($22)
sw	$0,8($22)
lw	$2,11850($17)
beq	$2,$0,$L1413
lw	$18,10332($16)
move	$9,$0
lw	$20,11896($16)
move	$7,$17
lw	$8,168($sp)
$L1437:
lw	$3,10340($16)
srl	$4,$3,3
andi	$10,$3,0x7
addu	$4,$18,$4
addiu	$5,$3,1
lbu	$2,0($4)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1683
sw	$5,10340($16)
.set	macro
.set	reorder

slt	$2,$6,128
sh	$0,0($7)
movz	$6,$23,$2
sh	$6,-2($7)
$L1424:
beq	$20,$0,$L1425
lw	$11,10340($16)
lw	$5,10332($16)
srl	$3,$11,3
andi	$4,$11,0x7
addu	$3,$5,$3
addiu	$11,$11,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1522
sw	$11,10340($16)
.set	macro
.set	reorder

slt	$2,$12,128
sh	$0,2($8)
movz	$12,$23,$2
sh	$0,6($8)
sll	$2,$12,16
sra	$2,$2,16
sh	$2,0($8)
sh	$2,4($8)
$L1425:
lw	$2,11850($17)
addiu	$9,$9,1
addiu	$8,$8,16
sltu	$2,$9,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1437
addiu	$7,$7,8
.set	macro
.set	reorder

$L1413:
lw	$2,-5260($25)
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
bne	$2,$3,$L1438
lw	$5,184($sp)
.set	macro
.set	reorder

lw	$4,168($sp)
addiu	$22,$22,4
addiu	$17,$17,4
addiu	$4,$4,8
.set	noreorder
.set	nomacro
bne	$22,$5,$L1439
sw	$4,168($sp)
.set	macro
.set	reorder

$L1438:
li	$2,65536			# 0x10000
lw	$fp,188($sp)
addu	$2,$16,$2
lhu	$3,-5232($2)
sltu	$3,$0,$3
.set	noreorder
.set	nomacro
b	$L1440
sb	$3,-5232($2)
.set	macro
.set	reorder

$L1337:
sltu	$2,$0,$2
sw	$2,10456($fp)
$L1340:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$8,$2,$L1715
lw	$25,%call16(ff_h264_frame_start)($28)
.set	macro
.set	reorder

$L1342:
lw	$2,10456($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1333
lw	$25,%call16(ff_h264_frame_start)($28)
.set	macro
.set	reorder

$L1715:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_frame_start
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L1333
lw	$28,88($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1621
sw	$0,10456($fp)
.set	macro
.set	reorder

$L1653:
li	$3,2			# 0x2
addu	$2,$16,$2
.set	noreorder
.set	nomacro
b	$L1400
sw	$3,6632($2)
.set	macro
.set	reorder

$L1652:
srl	$2,$7,3
li	$8,16711680			# 0xff0000
addu	$2,$6,$2
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$3,$3,$8
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
andi	$8,$7,0x7
or	$2,$2,$3
sll	$2,$2,$8
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1684
addiu	$3,$4,33
.set	macro
.set	reorder

ori	$7,$2,0x1
clz	$7,$7
li	$4,31			# 0x1f
subu	$7,$4,$7
sll	$7,$7,1
addiu	$7,$7,-31
srl	$4,$2,$7
subu	$2,$3,$7
addiu	$4,$4,-1
sw	$2,10340($16)
$L1394:
li	$2,65536			# 0x10000
addiu	$4,$4,1
addu	$2,$16,$2
sw	$4,6624($2)
li	$2,3			# 0x3
beq	$5,$2,$L1685
$L1395:
addiu	$2,$4,-1
sltu	$2,$2,32
.set	noreorder
.set	nomacro
beq	$2,$0,$L1398
li	$2,65536			# 0x10000
.set	macro
.set	reorder

addu	$2,$16,$2
lw	$2,6628($2)
addiu	$2,$2,-1
sltu	$2,$2,32
.set	noreorder
.set	nomacro
bne	$2,$0,$L1716
li	$2,3			# 0x3
.set	macro
.set	reorder

$L1398:
lw	$6,%got($LC33)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC33)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
li	$3,1			# 0x1
addu	$16,$16,$2
sw	$3,6628($16)
.set	noreorder
.set	nomacro
b	$L1554
sw	$3,6624($16)
.set	macro
.set	reorder

$L1657:
addu	$2,$16,$2
lw	$3,-5260($2)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$3,$2,$L1714
li	$3,65536			# 0x10000
.set	macro
.set	reorder

lw	$25,%got(implicit_weight_table)($28)
li	$5,-1			# 0xffffffffffffffff
addiu	$25,$25,%lo(implicit_weight_table)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,implicit_weight_table
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1440
lw	$28,88($sp)
.set	macro
.set	reorder

$L1390:
lw	$4,10340($16)
lw	$6,10332($16)
srl	$7,$4,3
andi	$8,$4,0x7
addu	$7,$6,$7
addiu	$4,$4,1
lbu	$3,0($7)
sw	$4,10340($16)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
b	$L1391
sw	$3,5140($2)
.set	macro
.set	reorder

$L1529:
.set	noreorder
.set	nomacro
b	$L1503
addiu	$11,$11,%lo($LC23)
.set	macro
.set	reorder

$L1673:
lw	$3,5140($16)
beq	$3,$0,$L1686
lw	$3,%got($LC26)($28)
.set	noreorder
.set	nomacro
b	$L1505
addiu	$3,$3,%lo($LC26)
.set	macro
.set	reorder

$L1526:
lw	$17,%got($LC19)($28)
.set	noreorder
.set	nomacro
b	$L1501
addiu	$17,$17,%lo($LC19)
.set	macro
.set	reorder

$L1644:
lw	$5,10340($16)
lw	$2,10332($16)
addiu	$4,$4,255
srl	$3,$5,3
andi	$6,$5,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($2)  
lwr $21, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$21,8
sll	$2,$21,8
and	$3,$3,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$4,$2,$4
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$6
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1687
li	$3,31			# 0x1f
.set	macro
.set	reorder

ori	$2,$2,0x1
clz	$2,$2
subu	$2,$3,$2
sll	$2,$2,1
subu	$2,$5,$2
addiu	$2,$2,63
.set	noreorder
.set	nomacro
b	$L1355
sw	$2,10340($16)
.set	macro
.set	reorder

$L1655:
addiu	$17,$17,28800
lw	$25,%call16(ff_copy_picture)($28)
addiu	$4,$16,888
addu	$2,$16,$17
move	$5,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_copy_picture
1:	jalr	$25
sw	$2,2692($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1404
lw	$28,88($sp)
.set	macro
.set	reorder

$L1481:
lw	$2,-15204($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1688
sw	$17,-15208($18)
.set	macro
.set	reorder

$L1482:
bne	$16,$fp,$L1554
.set	noreorder
.set	nomacro
b	$L1480
lw	$22,0($16)
.set	macro
.set	reorder

$L1684:
lw	$25,164($sp)
srl	$2,$2,23
lw	$8,172($sp)
addu	$3,$25,$2
addu	$2,$8,$2
lbu	$3,0($3)
lbu	$4,0($2)
addu	$7,$3,$7
.set	noreorder
.set	nomacro
b	$L1394
sw	$7,10340($16)
.set	macro
.set	reorder

$L1681:
lw	$8,164($sp)
srl	$2,$2,23
lw	$21,172($sp)
addu	$3,$8,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$3,$3,$6
.set	noreorder
.set	nomacro
b	$L1410
sw	$3,10340($16)
.set	macro
.set	reorder

$L1375:
addu	$3,$16,$3
lw	$11,9468($3)
.set	noreorder
.set	nomacro
bne	$11,$0,$L1382
move	$2,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1699
lw	$3,11920($16)
.set	macro
.set	reorder

$L1686:
lw	$3,%got($LC25)($28)
.set	noreorder
.set	nomacro
b	$L1505
addiu	$3,$3,%lo($LC25)
.set	macro
.set	reorder

$L1647:
lw	$4,10684($6)
subu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L1371
sw	$2,10664($6)
.set	macro
.set	reorder

$L1377:
.set	noreorder
.set	nomacro
bgtz	$5,$L1717
addiu	$4,$16,12032
.set	macro
.set	reorder

move	$2,$0
$L1508:
lw	$3,11920($16)
$L1699:
$L1704:
.set	noreorder
.set	nomacro
b	$L1382
addu	$2,$2,$3
.set	macro
.set	reorder

$L1679:
lw	$3,164($sp)
srl	$2,$2,23
addu	$2,$3,$2
lbu	$2,0($2)
addu	$2,$2,$7
.set	noreorder
.set	nomacro
b	$L1459
sw	$2,10340($16)
.set	macro
.set	reorder

$L1687:
lw	$25,164($sp)
srl	$2,$2,23
addu	$2,$25,$2
lbu	$2,0($2)
addu	$2,$2,$5
.set	noreorder
.set	nomacro
b	$L1355
sw	$2,10340($16)
.set	macro
.set	reorder

$L1338:
xori	$2,$8,0x3
$L1710:
sw	$0,2696($fp)
sltu	$2,$0,$2
.set	noreorder
.set	nomacro
b	$L1340
sw	$2,10456($fp)
.set	macro
.set	reorder

$L1680:
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$4,$2,$L1475
li	$2,131072			# 0x20000
.set	macro
.set	reorder

$L1709:
addu	$2,$16,$2
lw	$2,9468($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1479
li	$2,131072			# 0x20000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1721
addu	$2,$16,$2
.set	macro
.set	reorder

$L1688:
lw	$6,%got($LC37)($28)
li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
move	$4,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC37)
.set	macro
.set	reorder

lw	$28,88($sp)
.set	noreorder
.set	nomacro
b	$L1482
sw	$17,-15204($18)
.set	macro
.set	reorder

$L1677:
lw	$3,164($sp)
srl	$2,$2,23
addu	$6,$3,$2
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$2,$3,$2
lbu	$3,0($6)
addu	$3,$3,$8
lb	$2,0($2)
.set	noreorder
.set	nomacro
b	$L1468
sw	$3,10340($16)
.set	macro
.set	reorder

$L1683:
srl	$2,$5,3
andi	$10,$5,0x7
addu	$2,$18,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$14
and	$4,$4,$13
or	$2,$2,$4
sll	$4,$2,16
srl	$2,$2,16
or	$2,$4,$2
sll	$4,$2,$10
sltu	$2,$4,$15
.set	noreorder
.set	nomacro
beq	$2,$0,$L1689
addiu	$3,$3,33
.set	macro
.set	reorder

ori	$2,$4,0x1
clz	$2,$2
subu	$2,$24,$2
sll	$2,$2,1
addiu	$2,$2,-31
srl	$4,$4,$2
subu	$2,$3,$2
andi	$3,$4,0x1
srl	$4,$4,1
.set	noreorder
.set	nomacro
beq	$3,$0,$L1416
sw	$2,10340($16)
.set	macro
.set	reorder

subu	$4,$0,$4
$L1416:
lw	$10,10332($16)
srl	$3,$2,3
sh	$4,-2($7)
andi	$5,$2,0x7
addu	$3,$10,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sll	$3,$3,8
srl	$4,$4,8
and	$3,$3,$13
and	$4,$4,$14
or	$4,$4,$3
sll	$3,$4,16
srl	$4,$4,16
or	$4,$3,$4
sll	$3,$4,$5
sltu	$4,$3,$15
.set	noreorder
.set	nomacro
beq	$4,$0,$L1690
ori	$4,$3,0x1
.set	macro
.set	reorder

clz	$4,$4
addiu	$2,$2,32
subu	$4,$24,$4
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$2,$2,$4
andi	$4,$3,0x1
srl	$3,$3,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L1420
sw	$2,10340($16)
.set	macro
.set	reorder

subu	$3,$0,$3
$L1420:
sll	$2,$3,16
lh	$3,-2($7)
sra	$2,$2,16
.set	noreorder
.set	nomacro
beq	$3,$6,$L1691
sh	$2,0($7)
.set	macro
.set	reorder

sb	$31,-5232($25)
.set	noreorder
.set	nomacro
b	$L1424
sw	$19,0($22)
.set	macro
.set	reorder

$L1691:
beq	$2,$0,$L1424
sb	$31,-5232($25)
.set	noreorder
.set	nomacro
b	$L1424
sw	$19,0($22)
.set	macro
.set	reorder

$L1522:
move	$10,$0
move	$4,$8
$L1426:
srl	$3,$11,3
andi	$2,$11,0x7
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($3)  
lwr $21, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$21,8
sll	$21,$21,8
and	$3,$3,$14
and	$21,$21,$13
or	$21,$3,$21
sll	$3,$21,16
srl	$21,$21,16
or	$21,$3,$21
sll	$2,$21,$2
sltu	$3,$2,$15
.set	noreorder
.set	nomacro
beq	$3,$0,$L1692
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$3,$3
addiu	$11,$11,32
subu	$3,$24,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$11,$11,$3
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$3,$0,$L1428
sw	$11,10340($16)
.set	macro
.set	reorder

subu	$2,$0,$2
$L1428:
srl	$3,$11,3
sh	$2,0($4)
andi	$2,$11,0x7
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($3)  
lwr $21, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$21,8
sll	$21,$21,8
and	$3,$3,$14
and	$21,$21,$13
or	$21,$3,$21
sll	$3,$21,16
srl	$21,$21,16
or	$21,$3,$21
sll	$2,$21,$2
sltu	$3,$2,$15
.set	noreorder
.set	nomacro
beq	$3,$0,$L1693
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$3,$3
addiu	$11,$11,32
subu	$3,$24,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$11,$11,$3
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$3,$0,$L1432
sw	$11,10340($16)
.set	macro
.set	reorder

subu	$2,$0,$2
$L1432:
sll	$2,$2,16
lh	$3,0($4)
sra	$2,$2,16
.set	noreorder
.set	nomacro
beq	$3,$12,$L1694
sh	$2,2($4)
.set	macro
.set	reorder

sb	$31,-5231($25)
$L1701:
sw	$19,8($22)
$L1436:
.set	noreorder
.set	nomacro
beq	$10,$19,$L1425
addiu	$4,$4,4
.set	macro
.set	reorder

lw	$11,10340($16)
.set	noreorder
.set	nomacro
b	$L1426
li	$10,1			# 0x1
.set	macro
.set	reorder

$L1694:
beq	$2,$0,$L1436
.set	noreorder
.set	nomacro
b	$L1701
sb	$31,-5231($25)
.set	macro
.set	reorder

$L1693:
srl	$2,$2,23
lw	$21,%got(ff_se_golomb_vlc_code)($28)
addu	$3,$fp,$2
addu	$2,$21,$2
lbu	$3,0($3)
lb	$2,0($2)
addu	$11,$3,$11
.set	noreorder
.set	nomacro
b	$L1432
sw	$11,10340($16)
.set	macro
.set	reorder

$L1692:
srl	$2,$2,23
lw	$21,%got(ff_se_golomb_vlc_code)($28)
addu	$3,$fp,$2
addu	$2,$21,$2
lbu	$3,0($3)
lb	$2,0($2)
addu	$11,$3,$11
.set	noreorder
.set	nomacro
b	$L1428
sw	$11,10340($16)
.set	macro
.set	reorder

$L1690:
srl	$3,$3,23
lw	$5,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$fp,$3
addu	$3,$5,$3
lbu	$4,0($4)
lb	$3,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L1420
sw	$2,10340($16)
.set	macro
.set	reorder

$L1689:
srl	$4,$4,23
lw	$21,%got(ff_se_golomb_vlc_code)($28)
addu	$2,$fp,$4
addu	$4,$21,$4
lbu	$2,0($2)
lb	$4,0($4)
addu	$2,$2,$5
.set	noreorder
.set	nomacro
b	$L1416
sw	$2,10340($16)
.set	macro
.set	reorder

$L1675:
lw	$8,164($sp)
srl	$2,$2,23
lw	$21,172($sp)
addu	$3,$8,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$4,$3,$4
.set	noreorder
.set	nomacro
b	$L1388
sw	$4,10340($16)
.set	macro
.set	reorder

$L1527:
.set	noreorder
.set	nomacro
b	$L1501
addiu	$17,$17,%lo($LC20)
.set	macro
.set	reorder

$L1674:
lw	$5,10340($16)
lw	$4,10332($16)
addiu	$8,$8,255
srl	$2,$5,3
addu	$2,$4,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$3,$3,$8
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$5,0x7
or	$2,$3,$2
sll	$2,$2,$8
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1695
ori	$8,$2,0x1
.set	macro
.set	reorder

addiu	$3,$5,32
clz	$8,$8
li	$5,31			# 0x1f
subu	$8,$5,$8
sll	$8,$8,1
addiu	$8,$8,-31
srl	$2,$2,$8
subu	$3,$3,$8
andi	$5,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$5,$0,$L1364
sw	$3,10340($16)
.set	macro
.set	reorder

subu	$2,$0,$2
$L1364:
li	$18,131072			# 0x20000
lw	$3,12832($16)
addu	$18,$16,$18
sw	$2,10672($18)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L1718
li	$3,131072			# 0x20000
.set	macro
.set	reorder

li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$7,$2,$L1718
lw	$25,%got(get_se_golomb.isra.11)($28)
.set	macro
.set	reorder

addiu	$25,$25,%lo(get_se_golomb.isra.11)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_se_golomb.isra.11
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$6,11908($16)
lw	$7,10384($16)
.set	noreorder
.set	nomacro
b	$L1361
sw	$2,10676($18)
.set	macro
.set	reorder

$L1359:
lw	$7,10384($16)
.set	noreorder
.set	nomacro
bne	$7,$2,$L1719
li	$3,131072			# 0x20000
.set	macro
.set	reorder

lw	$25,%got(get_se_golomb.isra.11)($28)
addiu	$25,$25,%lo(get_se_golomb.isra.11)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_se_golomb.isra.11
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

lw	$28,88($sp)
lw	$6,11908($16)
lw	$7,10384($16)
.set	noreorder
.set	nomacro
b	$L1358
sw	$2,10668($18)
.set	macro
.set	reorder

$L1464:
xori	$7,$7,0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L1462
sw	$7,9456($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1702
srl	$2,$8,3
.set	macro
.set	reorder

$L1682:
lw	$25,164($sp)
srl	$2,$2,23
lw	$8,172($sp)
addu	$4,$25,$2
addu	$2,$8,$2
lbu	$4,0($4)
lbu	$2,0($2)
addu	$3,$4,$3
.set	noreorder
.set	nomacro
b	$L1412
sw	$3,10340($16)
.set	macro
.set	reorder

$L1348:
lw	$6,%got($LC32)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC32)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1630:
lw	$6,%got($LC28)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1627:
li	$2,65536			# 0x10000
lw	$8,7992($16)
lw	$3,7996($16)
li	$5,16			# 0x10
addu	$2,$16,$2
lw	$6,%got($LC27)($28)
lw	$4,0($16)
li	$17,-1			# 0xffffffffffffffff
lw	$25,%call16(av_log)($28)
lw	$7,-5264($2)
addiu	$6,$6,%lo($LC27)
sw	$8,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,20($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1663:
lw	$6,%got($LC35)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC35)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1695:
lw	$3,164($sp)
srl	$2,$2,23
addu	$8,$3,$2
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$2,$3,$2
lbu	$3,0($8)
addu	$3,$3,$5
lb	$2,0($2)
.set	noreorder
.set	nomacro
b	$L1364
sw	$3,10340($16)
.set	macro
.set	reorder

$L1685:
lw	$7,10340($16)
srl	$2,$7,3
addu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($6)  
lwr $3, 0($6)  

# 0 "" 2
#NO_APP
li	$6,16711680			# 0xff0000
srl	$2,$3,8
addiu	$6,$6,255
sll	$3,$3,8
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$7,0x7
or	$2,$3,$2
sll	$2,$2,$6
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1696
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$6,$3
li	$3,31			# 0x1f
addiu	$7,$7,32
subu	$3,$3,$6
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$7,$3
addiu	$2,$2,-1
sw	$3,10340($16)
$L1397:
li	$3,65536			# 0x10000
addiu	$2,$2,1
addu	$3,$16,$3
.set	noreorder
.set	nomacro
b	$L1395
sw	$2,6628($3)
.set	macro
.set	reorder

$L1678:
srl	$2,$2,23
lw	$5,%got(ff_se_golomb_vlc_code)($28)
addu	$6,$8,$2
addu	$2,$5,$2
lbu	$5,0($6)
lb	$2,0($2)
addu	$3,$5,$3
.set	noreorder
.set	nomacro
b	$L1472
sw	$3,10340($16)
.set	macro
.set	reorder

$L1517:
.set	noreorder
.set	nomacro
b	$L1378
move	$2,$0
.set	macro
.set	reorder

$L1676:
lw	$6,%got($LC36)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC36)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1670:
lw	$6,%got($LC34)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC34)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1696:
lw	$21,164($sp)
srl	$2,$2,23
lw	$25,172($sp)
addu	$3,$21,$2
addu	$2,$25,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$7,$3,$7
.set	noreorder
.set	nomacro
b	$L1397
sw	$7,10340($16)
.set	macro
.set	reorder

$L1631:
lw	$6,%got($LC29)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$7,$8
lw	$4,0($16)
li	$17,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC29)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1665:
.set	noreorder
.set	nomacro
b	$L1458
lw	$7,10340($16)
.set	macro
.set	reorder

$L1632:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1			# 0xffffffffffffffff
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC30)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1703
lw	$31,252($sp)
.set	macro
.set	reorder

$L1518:
.set	noreorder
.set	nomacro
bne	$11,$0,$L1382
move	$2,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1704
lw	$3,11920($16)
.set	macro
.set	reorder

.end	decode_slice_header
.size	decode_slice_header, .-decode_slice_header
.section	.text.ff_h264_hl_decode_mb,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_hl_decode_mb
.set	nomips16
.set	nomicromips
.ent	ff_h264_hl_decode_mb
.type	ff_h264_hl_decode_mb, @function
ff_h264_hl_decode_mb:
.frame	$sp,136,$31		# vars= 48, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-136
lw	$3,2192($4)
sw	$16,96($sp)
li	$16,131072			# 0x20000
sw	$17,100($sp)
addu	$16,$4,$16
sw	$23,124($sp)
move	$23,$4
.cprestore	40
sw	$31,132($sp)
lw	$17,9448($16)
lw	$4,9452($16)
sw	$fp,128($sp)
sll	$5,$17,2
sw	$22,120($sp)
sw	$21,116($sp)
addu	$3,$3,$5
sw	$20,112($sp)
sw	$19,108($sp)
sw	$18,104($sp)
lw	$3,0($3)
bne	$4,$0,$L1723
sw	$3,56($sp)

andi	$3,$3,0x4
beq	$3,$0,$L1829
nop

$L1723:
lw	$25,%got(hl_decode_mb_complex)($28)
$L1842:
addiu	$25,$25,%lo(hl_decode_mb_complex)
.reloc	1f,R_MIPS_JALR,hl_decode_mb_complex
1:	jalr	$25
move	$4,$23

lw	$31,132($sp)
$L1843:
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
j	$31
addiu	$sp,$sp,136

$L1829:
lw	$3,2872($23)
beq	$3,$0,$L1842
lw	$25,%got(hl_decode_mb_complex)($28)

lw	$4,192($23)
addiu	$2,$23,11660
lw	$7,7996($23)
li	$6,4			# 0x4
lw	$3,7992($23)
lw	$12,196($23)
move	$5,$4
sw	$2,88($sp)
mul	$2,$7,$4
andi	$8,$3,0x3
lw	$11,2088($23)
sll	$8,$8,2
lw	$25,56($sp)
lw	$10,2092($23)
lw	$9,2096($23)
andi	$25,$25,0x7
addu	$19,$2,$3
sw	$25,80($sp)
mul	$2,$7,$12
lw	$25,5744($23)
sll	$7,$19,4
addu	$19,$11,$7
addu	$3,$2,$3
mul	$2,$4,$8
sll	$3,$3,3
addu	$10,$10,$3
addu	$9,$9,$3
sw	$10,72($sp)
addu	$4,$2,$19
sw	$9,76($sp)
jalr	$25
addiu	$4,$4,64

li	$6,2			# 0x2
lw	$3,76($sp)
lw	$25,72($sp)
lw	$4,196($23)
lw	$2,72($sp)
subu	$5,$3,$25
lw	$3,7992($23)
lw	$25,5744($23)
andi	$3,$3,0x7
mul	$7,$3,$4
addu	$4,$7,$2
jalr	$25
addiu	$4,$4,64

li	$4,65536			# 0x10000
lw	$28,40($sp)
addu	$4,$23,$4
lw	$2,80($sp)
lw	$3,6636($4)
lw	$5,6632($4)
addu	$17,$3,$17
sb	$5,0($17)
lw	$25,196($23)
lw	$18,192($23)
sw	$25,60($sp)
sw	$18,11872($23)
beq	$2,$0,$L1830
sw	$25,11876($23)

lw	$3,9456($16)
bne	$3,$0,$L1831
nop

$L1725:
lw	$3,10760($23)
lw	$2,56($sp)
lw	$4,72($sp)
addiu	$3,$3,2742
lw	$5,60($sp)
andi	$2,$2,0x1
sll	$3,$3,2
sw	$2,64($sp)
addu	$3,$23,$3
lw	$25,4($3)
jalr	$25
nop

lw	$3,10760($23)
lw	$4,76($sp)
addiu	$3,$3,2742
sll	$3,$3,2
addu	$3,$23,$3
lw	$25,4($3)
jalr	$25
lw	$5,60($sp)

lw	$3,64($sp)
beq	$3,$0,$L1730
lw	$28,40($sp)

lw	$25,56($sp)
li	$3,16777216			# 0x1000000
and	$3,$25,$3
beq	$3,$0,$L1731
lw	$17,%got(scan8)($28)

lw	$25,10716($23)
li	$10,131072			# 0x20000
lw	$fp,88($sp)
move	$20,$0
addiu	$10,$10,6944
addiu	$17,$17,%lo(scan8)
sw	$25,84($sp)
addu	$22,$23,$10
lw	$25,10708($23)
sw	$25,68($sp)
$L1734:
addu	$3,$17,$20
lw	$11,0($fp)
lw	$5,11084($23)
move	$7,$18
lw	$6,11092($23)
lbu	$9,0($3)
addu	$21,$19,$11
sll	$5,$5,$20
sll	$6,$6,$20
addu	$9,$23,$9
andi	$5,$5,0x8000
move	$4,$21
lb	$3,10816($9)
andi	$6,$6,0x4000
addiu	$3,$3,2730
sll	$3,$3,2
addu	$3,$23,$3
lw	$25,4($3)
jalr	$25
lbu	$16,11184($9)

move	$5,$22
beq	$16,$0,$L1732
lw	$28,40($sp)

li	$2,1			# 0x1
move	$4,$21
bne	$16,$2,$L1733
move	$6,$18

lh	$3,0($22)
bne	$3,$0,$L1832
lw	$25,84($sp)

$L1733:
lw	$25,68($sp)
jalr	$25
nop

lw	$28,40($sp)
$L1732:
addiu	$20,$20,4
$L1840:
li	$2,16			# 0x10
addiu	$fp,$fp,16
bne	$20,$2,$L1734
addiu	$22,$22,128

$L1735:
li	$4,131072			# 0x20000
addu	$5,$23,$4
lw	$3,9456($5)
bne	$3,$0,$L1741
nop

lw	$3,8736($5)
andi	$5,$3,0x30
beq	$5,$0,$L1752
lw	$2,72($sp)

addiu	$4,$4,7456
lw	$3,76($sp)
addu	$4,$23,$4
sw	$2,48($sp)
b	$L1753
sw	$3,52($sp)

$L1834:
li	$3,131072			# 0x20000
addu	$3,$23,$3
lw	$3,8736($3)
$L1752:
bne	$3,$0,$L1758
lw	$2,80($sp)

beq	$2,$0,$L1843
lw	$31,132($sp)

$L1758:
li	$4,131072			# 0x20000
lw	$25,3008($23)
addiu	$4,$4,6944
jalr	$25
addu	$4,$23,$4

lw	$31,132($sp)
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
j	$31
addiu	$sp,$sp,136

$L1830:
addiu	$3,$23,4400
lw	$4,7964($23)
lw	$8,7968($23)
addiu	$6,$23,10584
addiu	$7,$23,10624
lw	$25,%got(hl_motion)($28)
sw	$3,20($sp)
addiu	$5,$23,4412
sw	$6,32($sp)
sw	$7,36($sp)
addiu	$25,$25,%lo(hl_motion)
lw	$3,56($sp)
lw	$6,72($sp)
lw	$7,76($sp)
andi	$3,$3,0x1
sw	$4,16($sp)
sw	$5,28($sp)
move	$4,$23
sw	$8,24($sp)
move	$5,$19
.reloc	1f,R_MIPS_JALR,hl_motion
1:	jalr	$25
sw	$3,64($sp)

lw	$28,40($sp)
$L1746:
lw	$25,64($sp)
beq	$25,$0,$L1844
lw	$2,56($sp)

li	$3,131072			# 0x20000
addu	$3,$23,$3
lw	$3,8736($3)
$L1750:
andi	$4,$3,0x30
$L1845:
beq	$4,$0,$L1752
li	$16,131072			# 0x20000

lw	$3,72($sp)
lw	$25,76($sp)
lw	$2,80($sp)
addiu	$4,$16,7456
sw	$3,48($sp)
addu	$4,$23,$4
beq	$2,$0,$L1822
sw	$25,52($sp)

lw	$17,%got(scan8)($28)
addiu	$17,$17,%lo(scan8)
$L1753:
li	$6,65536			# 0x10000
lw	$3,10740($23)
lw	$20,%got(chroma_dc_dequant_idct_c.isra.6)($28)
addu	$6,$23,$6
sll	$5,$3,6
addiu	$20,$20,%lo(chroma_dc_dequant_idct_c.isra.6)
lw	$3,-5308($6)
move	$25,$20
addu	$3,$3,$5
.reloc	1f,R_MIPS_JALR,chroma_dc_dequant_idct_c.isra.6
1:	jalr	$25
lw	$5,0($3)

li	$4,131072			# 0x20000
li	$3,2			# 0x2
addiu	$4,$4,7584
addu	$4,$23,$4
$L1766:
addiu	$3,$3,15056
lw	$5,10744($23)
li	$22,131072			# 0x20000
sll	$3,$3,2
sll	$5,$5,6
addu	$3,$23,$3
addiu	$22,$22,7456
move	$25,$20
lw	$3,0($3)
addiu	$fp,$23,11724
li	$16,16			# 0x10
addiu	$19,$sp,48
addu	$3,$3,$5
li	$18,24			# 0x18
addu	$22,$23,$22
.reloc	1f,R_MIPS_JALR,chroma_dc_dequant_idct_c.isra.6
1:	jalr	$25
lw	$5,0($3)

lw	$20,10704($23)
b	$L1757
lw	$21,10712($23)

$L1755:
lh	$3,0($22)
bne	$3,$0,$L1833
andi	$3,$16,0x4

addiu	$16,$16,1
$L1841:
addiu	$fp,$fp,4
beq	$16,$18,$L1834
addiu	$22,$22,32

$L1757:
addu	$3,$17,$16
lbu	$3,0($3)
addu	$3,$23,$3
lbu	$3,11184($3)
beq	$3,$0,$L1755
andi	$3,$16,0x4

lw	$4,0($fp)
addu	$3,$19,$3
lw	$6,60($sp)
move	$5,$22
move	$25,$20
lw	$3,0($3)
jalr	$25
addu	$4,$3,$4

b	$L1841
addiu	$16,$16,1

$L1833:
lw	$4,0($fp)
addu	$3,$19,$3
lw	$6,60($sp)
move	$5,$22
move	$25,$21
lw	$3,0($3)
jalr	$25
addu	$4,$3,$4

b	$L1841
addiu	$16,$16,1

$L1730:
lw	$3,10764($23)
move	$4,$19
move	$5,$18
addiu	$3,$3,2754
sll	$3,$3,2
addu	$3,$23,$3
lw	$25,0($3)
jalr	$25
li	$16,131072			# 0x20000

li	$6,65536			# 0x10000
lw	$3,2872($23)
addiu	$4,$16,6944
addu	$6,$23,$6
lw	$28,40($sp)
addu	$16,$23,$16
sll	$5,$3,6
lw	$3,-5312($6)
addu	$4,$23,$4
lw	$25,%got(h264_luma_dc_dequant_idct_c.isra.5)($28)
addu	$3,$3,$5
addiu	$25,$25,%lo(h264_luma_dc_dequant_idct_c.isra.5)
.reloc	1f,R_MIPS_JALR,h264_luma_dc_dequant_idct_c.isra.5
1:	jalr	$25
lw	$5,0($3)

lw	$3,9456($16)
bne	$3,$0,$L1741
lw	$28,40($sp)

lw	$2,56($sp)
$L1844:
andi	$3,$2,0x2
bne	$3,$0,$L1835
li	$6,131072			# 0x20000

addu	$16,$23,$6
lw	$3,8736($16)
andi	$4,$3,0xf
beq	$4,$0,$L1845
andi	$4,$3,0x30

lw	$2,56($sp)
li	$3,16777216			# 0x1000000
and	$21,$2,$3
beq	$21,$0,$L1751
addiu	$6,$6,6944

lw	$25,10728($23)
$L1825:
addiu	$3,$23,11184
lw	$5,88($sp)
addu	$6,$23,$6
move	$4,$19
sw	$3,16($sp)
jalr	$25
move	$7,$18

lw	$28,40($sp)
b	$L1750
lw	$3,8736($16)

$L1731:
lw	$25,10712($23)
li	$3,4			# 0x4
li	$16,131072			# 0x20000
lw	$fp,88($sp)
subu	$3,$3,$18
addiu	$16,$16,6944
sw	$25,92($sp)
move	$22,$0
sw	$3,84($sp)
addu	$16,$23,$16
lw	$25,10704($23)
addiu	$17,$17,%lo(scan8)
b	$L1740
sw	$25,68($sp)

$L1767:
move	$5,$0
$L1736:
addiu	$3,$3,2716
move	$4,$21
sll	$3,$3,2
addu	$3,$23,$3
lw	$25,0($3)
jalr	$25
move	$6,$18

lbu	$3,11184($20)
beq	$3,$0,$L1738
lw	$28,40($sp)

li	$2,1			# 0x1
move	$4,$21
move	$5,$16
bne	$3,$2,$L1739
move	$6,$18

lh	$3,0($16)
bne	$3,$0,$L1836
lw	$25,92($sp)

$L1739:
lw	$25,68($sp)
jalr	$25
nop

lw	$28,40($sp)
$L1738:
addiu	$22,$22,1
$L1839:
li	$3,16			# 0x10
addiu	$fp,$fp,4
beq	$22,$3,$L1735
addiu	$16,$16,32

$L1740:
addu	$3,$17,$22
lw	$8,0($fp)
li	$2,-5			# 0xfffffffffffffffb
lbu	$7,0($3)
addu	$20,$23,$7
li	$7,3			# 0x3
lb	$3,10816($20)
and	$4,$3,$2
bne	$4,$7,$L1767
addu	$21,$19,$8

lw	$4,11092($23)
sll	$4,$4,$22
andi	$4,$4,0x8000
bne	$4,$0,$L1737
lw	$25,84($sp)

subu	$4,$7,$18
addiu	$5,$sp,48
addu	$4,$21,$4
lbu	$6,0($4)
sll	$4,$6,8
addu	$4,$4,$6
sll	$6,$4,16
addu	$4,$4,$6
b	$L1736
sw	$4,48($sp)

$L1737:
b	$L1736
addu	$5,$21,$25

$L1836:
jalr	$25
addiu	$22,$22,1

b	$L1839
lw	$28,40($sp)

$L1822:
li	$6,65536			# 0x10000
lw	$3,10740($23)
lw	$20,%got(chroma_dc_dequant_idct_c.isra.6)($28)
addu	$6,$23,$6
sll	$5,$3,6
addiu	$20,$20,%lo(chroma_dc_dequant_idct_c.isra.6)
lw	$3,-5296($6)
move	$25,$20
addu	$3,$3,$5
.reloc	1f,R_MIPS_JALR,chroma_dc_dequant_idct_c.isra.6
1:	jalr	$25
lw	$5,0($3)

addiu	$4,$16,7584
lw	$28,40($sp)
li	$3,5			# 0x5
addu	$4,$23,$4
lw	$17,%got(scan8)($28)
b	$L1766
addiu	$17,$17,%lo(scan8)

$L1741:
li	$4,2			# 0x2
beq	$3,$4,$L1837
nop

li	$5,65536			# 0x10000
lw	$4,7992($23)
lw	$3,7996($23)
addu	$5,$23,$5
slt	$9,$0,$4
lw	$5,-5248($5)
sltu	$5,$0,$5
slt	$5,$5,$3
$L1745:
beq	$5,$0,$L1746
lw	$11,11104($23)

sll	$4,$4,5
nor	$8,$0,$18
addiu	$4,$4,-32
addu	$8,$19,$8
beq	$9,$0,$L1747
addu	$10,$11,$4

lw	$12,8($10)
lw	$13,12($10)
lw	$6,-7($8)
lw	$7,-3($8)
sw	$6,8($10)
sw	$7,12($10)
sw	$12,-7($8)
sw	$13,-3($8)
$L1747:
addiu	$3,$4,32
lw	$5,13($8)
lw	$4,9($8)
addu	$3,$11,$3
lw	$6,0($3)
lw	$7,4($3)
sw	$6,1($8)
sw	$7,5($8)
lw	$6,8($3)
lw	$7,12($3)
sw	$4,8($3)
sw	$5,12($3)
sw	$6,9($8)
sw	$7,13($8)
lw	$4,7992($23)
lw	$5,160($23)
addiu	$4,$4,1
slt	$5,$4,$5
beq	$5,$0,$L1846
lw	$2,60($sp)

lw	$7,11104($23)
sll	$6,$4,5
lw	$5,21($8)
lw	$4,17($8)
addu	$6,$7,$6
lw	$12,0($6)
lw	$13,4($6)
sw	$4,0($6)
sw	$5,4($6)
sw	$12,17($8)
sw	$13,21($8)
lw	$2,60($sp)
$L1846:
lw	$25,72($sp)
nor	$6,$0,$2
lw	$2,76($sp)
addu	$7,$25,$6
beq	$9,$0,$L1765
addu	$6,$2,$6

lw	$4,-7($7)
lw	$5,-3($7)
lw	$8,16($10)
lw	$9,20($10)
sw	$4,16($10)
sw	$5,20($10)
sw	$8,-7($7)
sw	$9,-3($7)
lw	$8,24($10)
lw	$9,28($10)
lw	$4,-7($6)
lw	$5,-3($6)
sw	$4,24($10)
sw	$5,28($10)
sw	$8,-7($6)
sw	$9,-3($6)
$L1765:
lw	$4,1($7)
lw	$5,5($7)
lw	$8,16($3)
lw	$9,20($3)
sw	$4,16($3)
sw	$5,20($3)
sw	$8,1($7)
sw	$9,5($7)
lw	$8,24($3)
lw	$9,28($3)
lw	$4,1($6)
lw	$5,5($6)
sw	$4,24($3)
sw	$5,28($3)
sw	$8,1($6)
b	$L1746
sw	$9,5($6)

$L1831:
li	$5,2			# 0x2
beq	$3,$5,$L1838
nop

lw	$5,-5248($4)
lw	$3,7996($23)
lw	$4,7992($23)
sltu	$5,$0,$5
slt	$5,$5,$3
slt	$8,$0,$4
$L1727:
beq	$5,$0,$L1725
lw	$3,11104($23)

sll	$4,$4,5
nor	$5,$0,$18
addiu	$4,$4,-32
addu	$5,$19,$5
beq	$8,$0,$L1728
addu	$9,$3,$4

lw	$10,8($9)
lw	$11,12($9)
lw	$6,-7($5)
lw	$7,-3($5)
sw	$6,8($9)
sw	$7,12($9)
sw	$10,-7($5)
sw	$11,-3($5)
$L1728:
addiu	$4,$4,32
lw	$6,1($5)
lw	$7,5($5)
addu	$3,$3,$4
lw	$10,0($3)
lw	$11,4($3)
sw	$6,0($3)
sw	$7,4($3)
sw	$10,1($5)
sw	$11,5($5)
lw	$6,9($5)
lw	$7,13($5)
lw	$10,8($3)
lw	$11,12($3)
sw	$6,8($3)
sw	$7,12($3)
sw	$10,9($5)
sw	$11,13($5)
lw	$4,7992($23)
lw	$6,160($23)
addiu	$4,$4,1
slt	$6,$4,$6
beq	$6,$0,$L1847
lw	$25,60($sp)

lw	$10,11104($23)
sll	$4,$4,5
lw	$6,17($5)
lw	$7,21($5)
addu	$4,$10,$4
lw	$10,0($4)
lw	$11,4($4)
sw	$6,0($4)
sw	$7,4($4)
sw	$10,17($5)
sw	$11,21($5)
lw	$25,60($sp)
$L1847:
lw	$2,72($sp)
nor	$6,$0,$25
lw	$25,76($sp)
addu	$7,$2,$6
beq	$8,$0,$L1763
addu	$6,$25,$6

lw	$4,-7($7)
lw	$5,-3($7)
lw	$10,16($9)
lw	$11,20($9)
sw	$4,16($9)
sw	$5,20($9)
sw	$10,-7($7)
sw	$11,-3($7)
lw	$10,24($9)
lw	$11,28($9)
lw	$4,-7($6)
lw	$5,-3($6)
sw	$4,24($9)
sw	$5,28($9)
sw	$10,-7($6)
sw	$11,-3($6)
$L1763:
lw	$4,1($7)
lw	$5,5($7)
lw	$8,16($3)
lw	$9,20($3)
sw	$4,16($3)
sw	$5,20($3)
sw	$8,1($7)
sw	$9,5($7)
lw	$8,24($3)
lw	$9,28($3)
lw	$4,1($6)
lw	$5,5($6)
sw	$4,24($3)
sw	$5,28($3)
sw	$8,1($6)
b	$L1725
sw	$9,5($6)

$L1835:
li	$16,131072			# 0x20000
lw	$25,10736($23)
addiu	$3,$23,11184
lw	$5,88($sp)
addiu	$6,$16,6944
addu	$16,$23,$16
addu	$6,$23,$6
sw	$3,16($sp)
move	$4,$19
jalr	$25
move	$7,$18

lw	$28,40($sp)
b	$L1750
lw	$3,8736($16)

$L1751:
b	$L1825
lw	$25,10724($23)

$L1832:
jalr	$25
addiu	$20,$20,4

b	$L1840
lw	$28,40($sp)

$L1838:
lw	$8,10800($23)
lw	$5,10792($23)
b	$L1727
lw	$4,7992($23)

$L1837:
lw	$9,10800($23)
lw	$5,10792($23)
b	$L1745
lw	$4,7992($23)

.set	macro
.set	reorder
.end	ff_h264_hl_decode_mb
.size	ff_h264_hl_decode_mb, .-ff_h264_hl_decode_mb
.section	.text.ff_h264_get_slice_type,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_get_slice_type
.set	nomips16
.set	nomicromips
.ent	ff_h264_get_slice_type
.type	ff_h264_get_slice_type, @function
ff_h264_get_slice_type:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$2,65536			# 0x10000
addu	$4,$4,$2
lw	$2,-5264($4)
addiu	$2,$2,-1
sltu	$3,$2,6
beq	$3,$0,$L1850
nop

lw	$3,%got(CSWTCH.364)($28)
sll	$2,$2,2
addiu	$3,$3,%lo(CSWTCH.364)
addu	$2,$2,$3
j	$31
lw	$2,0($2)

$L1850:
j	$31
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	ff_h264_get_slice_type
.size	ff_h264_get_slice_type, .-ff_h264_get_slice_type
.section	.text.aux_mem_init,"ax",@progbits
.align	2
.align	5
.globl	aux_mem_init
.set	nomips16
.set	nomicromips
.ent	aux_mem_init
.type	aux_mem_init, @function
aux_mem_init:
.frame	$sp,88,$31		# vars= 24, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-88
lw	$25,%call16(do_get_phy_addr)($28)
sw	$17,52($sp)
lw	$17,%got(sram_base)($28)
sw	$21,68($sp)
sw	$31,84($sp)
.cprestore	16
sw	$22,72($sp)
move	$22,$0
sw	$20,64($sp)
sw	$19,60($sp)
sw	$18,56($sp)
sw	$16,48($sp)
sw	$fp,80($sp)
sw	$23,76($sp)
lw	$4,0($17)
lw	$21,%got(tcsm1_base)($28)
addiu	$4,$4,19060
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$16,0($21)
.set	macro
.set	reorder

lw	$28,16($sp)
move	$20,$2
lw	$4,0($17)
addiu	$16,$16,12744
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,19636
.set	macro
.set	reorder

lw	$28,16($sp)
move	$18,$2
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,19924
.set	macro
.set	reorder

lw	$28,16($sp)
subu	$2,$2,$18
lw	$4,0($17)
sw	$2,32($sp)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,3620
.set	macro
.set	reorder

lw	$28,16($sp)
move	$19,$2
$L1859:
lw	$3,32($sp)
li	$2,7			# 0x7
addu	$3,$3,$18
.set	noreorder
.set	nomacro
beq	$22,$2,$L1862
sw	$3,28($sp)
.set	macro
.set	reorder

lw	$25,%call16(do_get_phy_addr)($28)
addiu	$fp,$16,40
addiu	$16,$16,12
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,16($sp)
#APP
# 3936 "h264.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
sll	$4,$22,4
lw	$25,%call16(do_get_phy_addr)($28)
sll	$23,$22,3
move	$16,$fp
addiu	$2,$2,24
move	$fp,$23
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addu	$4,$2,$4
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$4,0($17)
sw	$2,24($sp)
addiu	$4,$4,2384
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addu	$4,$4,$23
.set	macro
.set	reorder

lw	$28,16($sp)
move	$23,$2
lw	$4,0($17)
addiu	$4,$4,2992
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addu	$4,$4,$fp
.set	macro
.set	reorder

lw	$28,16($sp)
move	$9,$2
$L1853:
lw	$2,0($17)
sll	$7,$22,6
lw	$4,0($21)
sll	$3,$22,8
sll	$5,$22,2
lw	$25,%call16(do_get_phy_addr)($28)
addiu	$2,$2,3588
addiu	$4,$4,21292
subu	$3,$3,$7
addu	$5,$2,$5
li	$2,7			# 0x7
.set	noreorder
.set	nomacro
beq	$22,$2,$L1863
addu	$fp,$4,$3
.set	macro
.set	reorder

sw	$5,40($sp)
move	$4,$fp
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
sw	$9,36($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
addiu	$2,$2,192
lw	$5,40($sp)
#APP
# 3957 "h264.c" 1
sw	 $2,0($5)	#i_sw
# 0 "" 2
#NO_APP
lw	$9,36($sp)
$L1855:
lw	$6,24($sp)
#APP
# 3960 "h264.c" 1
sw	 $6,0($fp)	#i_sw
# 0 "" 2
#NO_APP
li	$3,9699328			# 0x940000
addiu	$2,$fp,8
ori	$3,$3,0x10
#APP
# 3961 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$4,1048576			# 0x100000
addiu	$2,$fp,12
ori	$4,$4,0x100
#APP
# 3962 "h264.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,16
#APP
# 3964 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$6,4980736			# 0x4c0000
addiu	$2,$fp,24
ori	$6,$6,0x10
#APP
# 3965 "h264.c" 1
sw	 $6,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$4,524288			# 0x80000
addiu	$2,$fp,28
addiu	$7,$4,64
#APP
# 3966 "h264.c" 1
sw	 $7,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,32
#APP
# 3968 "h264.c" 1
sw	 $9,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,40
#APP
# 3969 "h264.c" 1
sw	 $6,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,44
#APP
# 3970 "h264.c" 1
sw	 $7,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,48
#APP
# 3973 "h264.c" 1
sw	 $20,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,8388608			# 0x800000
addiu	$2,$fp,56
ori	$3,$3,0x10
#APP
# 3974 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$6,1048576			# 0x100000
addiu	$7,$fp,60
addiu	$2,$6,64
#APP
# 3975 "h264.c" 1
sw	 $2,0($7)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,64
#APP
# 3978 "h264.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$7,4194304			# 0x400000
addiu	$8,$fp,72
addiu	$7,$7,16
#APP
# 3979 "h264.c" 1
sw	 $7,0($8)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$4,32
addiu	$2,$fp,76
#APP
# 3980 "h264.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,80
lw	$3,28($sp)
#APP
# 3983 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,88
#APP
# 3984 "h264.c" 1
sw	 $7,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,92
#APP
# 3985 "h264.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,7			# 0x7
.set	noreorder
.set	nomacro
beq	$22,$2,$L1865
lw	$6,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$22,$0,$L1865
lw	$25,%call16(do_get_phy_addr)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
move	$4,$5
.set	macro
.set	reorder

addiu	$4,$fp,96
lw	$28,16($sp)
#APP
# 4015 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$6,%got(gp2_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,0($6)
.set	macro
.set	reorder

addiu	$4,$fp,100
lw	$28,16($sp)
#APP
# 4016 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$23,262144			# 0x40000
addiu	$2,$fp,104
addiu	$23,$23,4
#APP
# 4017 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,108
#APP
# 4018 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,112
#APP
# 4020 "h264.c" 1
sw	 $19,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($21)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,22840
.set	macro
.set	reorder

addiu	$4,$fp,116
lw	$28,16($sp)
#APP
# 4021 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,120
#APP
# 4022 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,124
#APP
# 4023 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,128
#APP
# 4025 "h264.c" 1
sw	 $19,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(vpu_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,124
.set	macro
.set	reorder

addiu	$4,$fp,132
lw	$28,16($sp)
#APP
# 4026 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,136
#APP
# 4027 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,-2147221504			# 0xffffffff80040000
addiu	$3,$fp,140
addiu	$2,$2,4
#APP
# 4028 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$22,$22,1
$L1864:
li	$2,8			# 0x8
addiu	$20,$20,16
addiu	$18,$18,8
.set	noreorder
.set	nomacro
bne	$22,$2,$L1859
addiu	$19,$19,4
.set	macro
.set	reorder

lw	$20,%got(tcsm0_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$16,0($20)
addiu	$4,$16,16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$17,$16,20
.set	macro
.set	reorder

lw	$28,16($sp)
#APP
# 4037 "h264.c" 1
sw	 $2,0($17)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(dblk0_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,132
.set	macro
.set	reorder

addiu	$3,$16,24
lw	$28,16($sp)
#APP
# 4038 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$18,262144			# 0x40000
addiu	$2,$16,28
addiu	$18,$18,4
#APP
# 4039 "h264.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$17,-2147221504			# 0xffffffff80040000
addiu	$16,$16,32
addiu	$17,$17,4
#APP
# 4040 "h264.c" 1
sw	 $17,0($16)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($21)
li	$19,34448			# 0x8690
lw	$25,%call16(do_get_phy_addr)($28)
li	$16,34432			# 0x8680
addu	$4,$2,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addu	$16,$2,$16
.set	macro
.set	reorder

lw	$28,16($sp)
#APP
# 4043 "h264.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(dblk1_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,132
.set	macro
.set	reorder

addiu	$3,$16,4
#APP
# 4044 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,8
#APP
# 4045 "h264.c" 1
sw	 $18,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$16,$16,12
#APP
# 4046 "h264.c" 1
sw	 $17,0($16)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($20)
li	$4,1			# 0x1
addiu	$3,$3,16
#APP
# 4048 "h264.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($21)
addu	$2,$2,$19
#APP
# 4049 "h264.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$31,84($sp)
lw	$fp,80($sp)
lw	$23,76($sp)
lw	$22,72($sp)
lw	$21,68($sp)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L1865:
addiu	$2,$fp,96
addiu	$3,$6,-4
#APP
# 3988 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,9699328			# 0x940000
addiu	$2,$fp,104
ori	$3,$3,0x10
#APP
# 3989 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$3,262144			# 0x40000
addiu	$4,$fp,108
addiu	$2,$3,64
#APP
# 3990 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$23,$23,-4
addiu	$2,$fp,112
#APP
# 3992 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$4,4980736			# 0x4c0000
addiu	$2,$fp,120
ori	$4,$4,0x10
#APP
# 3993 "h264.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$3,32
addiu	$4,$fp,124
#APP
# 3994 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$9,$9,-4
addiu	$4,$fp,128
#APP
# 3996 "h264.c" 1
sw	 $9,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$6,4980736			# 0x4c0000
addiu	$4,$fp,136
ori	$6,$6,0x10
#APP
# 3997 "h264.c" 1
sw	 $6,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$fp,140
#APP
# 3998 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
move	$4,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
sw	$3,36($sp)
.set	macro
.set	reorder

addiu	$4,$fp,144
lw	$28,16($sp)
#APP
# 4000 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(gp2_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

addiu	$4,$fp,148
lw	$28,16($sp)
#APP
# 4001 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,36($sp)
addiu	$2,$fp,152
addiu	$23,$3,4
#APP
# 4002 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,156
#APP
# 4003 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,160
#APP
# 4005 "h264.c" 1
sw	 $19,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($21)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,22840
.set	macro
.set	reorder

addiu	$4,$fp,164
lw	$28,16($sp)
#APP
# 4006 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,168
#APP
# 4007 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,172
#APP
# 4008 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,176
#APP
# 4010 "h264.c" 1
sw	 $19,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,%got(vpu_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,124
.set	macro
.set	reorder

addiu	$4,$fp,180
lw	$28,16($sp)
#APP
# 4011 "h264.c" 1
sw	 $2,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$fp,184
#APP
# 4012 "h264.c" 1
sw	 $23,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,-2147221504			# 0xffffffff80040000
addiu	$3,$fp,188
addiu	$2,$2,4
#APP
# 4013 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1864
addiu	$22,$22,1
.set	macro
.set	reorder

$L1863:
sw	$5,40($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
sw	$9,36($sp)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$5,40($sp)
#APP
# 3955 "h264.c" 1
sw	 $2,0($5)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L1855
lw	$9,36($sp)
.set	macro
.set	reorder

$L1862:
lw	$4,0($21)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,12744
.set	macro
.set	reorder

addiu	$3,$16,12
lw	$28,16($sp)
#APP
# 3934 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($17)
addiu	$16,$16,40
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,8
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$4,0($17)
sw	$2,24($sp)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2376
.set	macro
.set	reorder

lw	$28,16($sp)
move	$23,$2
lw	$4,0($17)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2984
.set	macro
.set	reorder

lw	$28,16($sp)
.set	noreorder
.set	nomacro
b	$L1853
move	$9,$2
.set	macro
.set	reorder

.end	aux_mem_init
.size	aux_mem_init, .-aux_mem_init
.section	.text.aux_hw_cfg_init,"ax",@progbits
.align	2
.align	5
.globl	aux_hw_cfg_init
.set	nomips16
.set	nomicromips
.ent	aux_hw_cfg_init
.type	aux_hw_cfg_init, @function
aux_hw_cfg_init:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-48
lw	$6,%got(tcsm1_word_bank_backup)($28)
li	$7,49152			# 0xc000
sw	$18,36($sp)
lw	$18,%got(tcsm1_base)($28)
move	$5,$6
sw	$17,32($sp)
move	$17,$4
.cprestore	16
sw	$31,44($sp)
sw	$19,40($sp)
sw	$16,28($sp)
lw	$3,0($18)
addu	$7,$3,$7
move	$2,$3
$L1867:
lw	$8,0($2)
addiu	$5,$5,4
addiu	$2,$2,8192
.set	noreorder
.set	nomacro
bne	$2,$7,$L1867
sw	$8,-4($5)
.set	macro
.set	reorder

lw	$2,%got(cpm_base)($28)
li	$8,268435456			# 0x10000000
lw	$5,0($2)
li	$2,-1073807360			# 0xffffffffbfff0000
ori	$2,$2,0xffff
sw	$0,52($5)
lw	$4,4($5)
and	$2,$4,$2
li	$4,536870912			# 0x20000000
sw	$2,4($5)
lw	$2,36($5)
or	$2,$2,$4
sw	$2,36($5)
$L1868:
lw	$2,36($5)
and	$2,$2,$8
beq	$2,$0,$L1868
lw	$8,36($5)
li	$2,-1610678272			# 0xffffffff9fff0000
li	$4,1073741824			# 0x40000000
ori	$2,$2,0xffff
and	$2,$8,$2
or	$2,$2,$4
sw	$2,36($5)
li	$2,-1073807360			# 0xffffffffbfff0000
lw	$4,36($5)
ori	$2,$2,0xffff
and	$4,$4,$2
sw	$4,36($5)
lw	$4,36($5)
and	$2,$4,$2
sw	$2,36($5)
$L1869:
lw	$2,0($6)
addiu	$3,$3,8192
addiu	$6,$6,4
.set	noreorder
.set	nomacro
bne	$3,$7,$L1869
sw	$2,-8192($3)
.set	macro
.set	reorder

lw	$16,%got(vmau_base)($28)
li	$3,4			# 0x4
lw	$2,0($16)
addiu	$2,$2,64
#APP
# 4063 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
li	$3,769			# 0x301
addiu	$2,$2,80
#APP
# 4064 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($18)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,12744
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,12
#APP
# 4065 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($16)
li	$2,320864256			# 0x13200000
addiu	$2,$2,112
addiu	$3,$3,88
#APP
# 4066 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$19,%got(sram_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,24
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,104
#APP
# 4067 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($19)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2384
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,108
#APP
# 4068 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($19)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,2992
.set	macro
.set	reorder

lw	$3,0($16)
lw	$28,16($sp)
addiu	$3,$3,112
#APP
# 4069 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($16)
li	$2,4980736			# 0x4c0000
addiu	$2,$2,148
addiu	$3,$3,116
#APP
# 4070 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
move	$3,$0
addiu	$2,$2,68
#APP
# 4071 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,160($17)
lw	$3,0($16)
sll	$2,$2,4
addiu	$3,$3,84
#APP
# 4072 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($16)
li	$3,17			# 0x11
addiu	$2,$2,68
#APP
# 4073 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(motion_init_h264)($28)
li	$5,3			# 0x3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_init_h264
1:	jalr	$25
li	$4,2			# 0x2
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$2,%got(tcsm0_base)($28)
lw	$25,%call16(do_get_phy_addr)($28)
lw	$4,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addiu	$4,$4,20
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$3,%got(gp0_base)($28)
lw	$3,0($3)
#APP
# 4077 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($18)
li	$2,34432			# 0x8680
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$3,%got(gp1_base)($28)
lw	$3,0($3)
#APP
# 4078 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(vpu_base)($28)
lw	$3,0($2)
li	$2,50462720			# 0x3020000
addiu	$2,$2,264
addiu	$3,$3,100
#APP
# 4080 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$31,44($sp)
move	$4,$17
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
lw	$25,%call16(motion_config)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_config
1:	jr	$25
addiu	$sp,$sp,48
.set	macro
.set	reorder

.end	aux_hw_cfg_init
.size	aux_hw_cfg_init, .-aux_hw_cfg_init
.section	.rodata.str1.4
.align	2
$LC40:
.ascii	"SDE CAVLC DCT8X8 UNSUPPORTED!\012\000"
.section	.text.decode_slice,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_slice
.type	decode_slice, @function
decode_slice:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-48
sw	$20,40($sp)
lw	$20,%got(slicenum)($28)
.cprestore	16
sw	$16,24($sp)
sw	$31,44($sp)
sw	$19,36($sp)
sw	$18,32($sp)
sw	$17,28($sp)
lw	$2,0($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1893
lw	$16,0($5)
.set	macro
.set	reorder

$L1876:
lw	$25,%call16(aux_hw_cfg_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aux_hw_cfg_init
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$2,65536			# 0x10000
li	$3,-1			# 0xffffffffffffffff
lw	$28,16($sp)
addu	$2,$16,$2
sw	$3,8000($16)
lw	$2,-5252($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1877
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$3,10384($16)
li	$2,3			# 0x3
beq	$3,$2,$L1894
li	$3,1			# 0x1
$L1877:
li	$2,131072			# 0x20000
lw	$4,12828($16)
addu	$2,$16,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L1878
sw	$3,9452($2)
.set	macro
.set	reorder

lw	$3,10340($16)
subu	$2,$0,$3
andi	$2,$2,0x7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1879
addu	$2,$2,$3
.set	macro
.set	reorder

sw	$2,10340($16)
$L1879:
li	$19,131072			# 0x20000
lw	$25,%call16(ff_init_cabac_states)($28)
lw	$18,%got(sde_slice_info_ptr)($28)
addiu	$17,$19,8224
addu	$17,$16,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_cabac_states
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

move	$4,$17
lw	$3,10340($16)
lw	$2,10344($16)
lw	$28,16($sp)
addiu	$8,$3,7
lw	$5,10332($16)
subu	$2,$2,$3
slt	$7,$3,0
addiu	$6,$2,7
movn	$3,$8,$7
addiu	$2,$2,14
slt	$7,$6,0
lw	$25,%call16(ff_init_cabac_decoder)($28)
movn	$6,$2,$7
sra	$2,$3,3
addu	$5,$5,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_cabac_decoder
1:	jalr	$25
sra	$6,$6,3
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%call16(ff_h264_init_cabac_states)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_init_cabac_states
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%call16(sde_slice_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sde_slice_init
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$3,0($18)
lw	$28,16($sp)
lw	$2,48($3)
lw	$4,44($3)
lw	$3,%got(vmau_base)($28)
sll	$2,$2,16
or	$2,$2,$4
lw	$3,0($3)
addiu	$3,$3,96
#APP
# 4121 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($18)
lw	$25,%call16(init_dblk_chain_fifo)($28)
lw	$4,160($16)
lw	$17,%got(tcsm1_base)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_dblk_chain_fifo
1:	jalr	$25
lw	$5,44($2)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,2088($16)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addiu	$3,$3,22832
#APP
# 4124 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,2092($16)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addiu	$3,$3,22836
#APP
# 4125 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,44($3)
addiu	$2,$2,22860
#APP
# 4126 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,48($3)
addiu	$2,$2,22864
#APP
# 4127 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,52($3)
addiu	$2,$2,22868
#APP
# 4128 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,56($3)
addiu	$2,$2,22872
#APP
# 4129 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,8($3)
addiu	$2,$2,22876
#APP
# 4130 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,40($3)
addiu	$2,$2,22880
#APP
# 4131 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,0($3)
addiu	$2,$2,22884
#APP
# 4132 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
addu	$16,$16,$19
lw	$2,0($17)
lw	$3,9460($16)
addiu	$2,$2,22888
#APP
# 4133 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
lw	$3,9464($16)
addiu	$2,$2,22892
#APP
# 4134 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
lw	$3,0($20)
addiu	$2,$2,22896
#APP
# 4135 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,32($3)
addiu	$2,$2,22900
#APP
# 4136 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($17)
lw	$3,9456($16)
addiu	$2,$2,22904
#APP
# 4137 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,36($3)
addiu	$2,$2,22908
#APP
# 4138 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(tcsm0_base)($28)
li	$17,1			# 0x1
lw	$16,%got(aux_base)($28)
lw	$4,0($2)
lw	$3,0($16)
sw	$0,36($4)
#APP
# 4151 "h264.c" 1
sw	 $17,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$3,2			# 0x2
lw	$4,0($16)
#APP
# 4151 "h264.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($2)
li	$5,23130			# 0x5a5a
lw	$25,%call16(wait_aux_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,wait_aux_end
1:	jalr	$25
addiu	$4,$4,36
.set	macro
.set	reorder

lw	$2,0($16)
#APP
# 4158 "h264.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$31,44($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L1878:
lw	$18,%got(sde_slice_info_ptr)($28)
lw	$25,%call16(sde_slice_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sde_slice_init
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$3,0($18)
lw	$2,12($3)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1895
lw	$28,16($sp)
.set	macro
.set	reorder

$L1883:
lw	$2,48($3)
lw	$4,44($3)
lw	$3,%got(vmau_base)($28)
sll	$2,$2,16
or	$2,$2,$4
lw	$3,0($3)
addiu	$3,$3,96
#APP
# 4230 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,0($18)
lw	$25,%call16(init_dblk_chain_fifo)($28)
lw	$4,160($16)
lw	$17,%got(tcsm1_base)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_dblk_chain_fifo
1:	jalr	$25
lw	$5,44($2)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,2088($16)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addiu	$3,$3,22832
#APP
# 4233 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$25,%call16(do_get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,do_get_phy_addr
1:	jalr	$25
lw	$4,2092($16)
.set	macro
.set	reorder

lw	$3,0($17)
lw	$28,16($sp)
addiu	$3,$3,22836
#APP
# 4234 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,44($3)
addiu	$2,$2,22860
#APP
# 4235 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,48($3)
addiu	$2,$2,22864
#APP
# 4236 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,52($3)
addiu	$2,$2,22868
#APP
# 4237 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,56($3)
addiu	$2,$2,22872
#APP
# 4238 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,8($3)
addiu	$2,$2,22876
#APP
# 4239 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,40($3)
addiu	$2,$2,22880
#APP
# 4240 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($18)
lw	$2,0($17)
lw	$3,0($3)
addiu	$2,$2,22884
#APP
# 4241 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,131072			# 0x20000
lw	$3,0($17)
addu	$2,$16,$2
addiu	$3,$3,22888
lw	$4,9460($2)
#APP
# 4242 "h264.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
lw	$4,9464($2)
addiu	$3,$3,22892
#APP
# 4243 "h264.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
lw	$4,0($20)
addiu	$3,$3,22896
#APP
# 4244 "h264.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
lw	$4,10344($16)
addiu	$3,$3,22900
#APP
# 4245 "h264.c" 1
sw	 $4,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,0($17)
lw	$4,9456($2)
addiu	$2,$3,22904
#APP
# 4246 "h264.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,%got(sde_init_ofst)($28)
lw	$2,0($17)
lw	$3,0($3)
addiu	$2,$2,22908
#APP
# 4247 "h264.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(tcsm0_base)($28)
lw	$3,%got(aux_base)($28)
lw	$4,0($2)
lw	$5,0($3)
sw	$0,36($4)
li	$4,1			# 0x1
#APP
# 4259 "h264.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($3)
li	$3,2			# 0x2
#APP
# 4259 "h264.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$4,0($2)
li	$5,23130			# 0x5a5a
lw	$25,%call16(wait_aux_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,wait_aux_end
1:	jalr	$25
addiu	$4,$4,36
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L1893:
lw	$25,%call16(init_dblk_chain_frame)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_dblk_chain_frame
1:	jalr	$25
lw	$4,160($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1876
lw	$28,16($sp)
.set	macro
.set	reorder

$L1894:
lw	$3,52($16)
xori	$3,$3,0x1c
.set	noreorder
.set	nomacro
b	$L1877
sltu	$3,$0,$3
.set	macro
.set	reorder

$L1895:
lw	$4,%got($LC40)($28)
lw	$25,%call16(printf)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC40)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%call16(exit_player_with_rc)($28)
.reloc	1f,R_MIPS_JALR,exit_player_with_rc
1:	jalr	$25
lw	$28,16($sp)
.set	noreorder
.set	nomacro
b	$L1883
lw	$3,0($18)
.set	macro
.set	reorder

.end	decode_slice
.size	decode_slice, .-decode_slice
.section	.text.execute_decode_slices,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	execute_decode_slices
.type	execute_decode_slices, @function
execute_decode_slices:
.frame	$sp,56,$31		# vars= 0, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$8,0($4)
addiu	$sp,$sp,-56
.cprestore	24
sw	$31,52($sp)
sw	$19,48($sp)
sw	$18,44($sp)
sw	$17,40($sp)
sw	$16,36($sp)
sw	$4,56($sp)
lw	$3,872($8)
beq	$3,$0,$L1913
nop

$L1896:
lw	$31,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,56

$L1913:
lw	$3,132($8)
lw	$3,32($3)
andi	$3,$3,0x80
bne	$3,$0,$L1896
li	$2,1			# 0x1

move	$19,$5
beq	$5,$2,$L1900
move	$18,$4

slt	$2,$5,2
bne	$2,$0,$L1914
li	$2,131072			# 0x20000

li	$5,45333			# 0xb115
lw	$6,252($8)
addu	$5,$19,$5
ori	$2,$2,0xc458
sll	$17,$5,2
addu	$2,$4,$2
addu	$4,$4,$17
$L1903:
lw	$3,0($2)
addiu	$2,$2,4
sw	$6,9816($3)
bne	$2,$4,$L1903
sw	$0,9776($3)

li	$2,4			# 0x4
lw	$5,%got(decode_slice)($28)
li	$16,131072			# 0x20000
sw	$19,16($sp)
move	$7,$0
sw	$2,20($sp)
ori	$16,$16,0xc454
lw	$25,616($8)
addiu	$5,$5,%lo(decode_slice)
addu	$6,$18,$16
jalr	$25
move	$4,$8

li	$4,45331			# 0xb113
lw	$6,56($sp)
addiu	$2,$16,4
addu	$4,$19,$4
sll	$4,$4,2
addu	$2,$6,$2
lw	$3,9776($6)
addu	$4,$6,$4
addu	$5,$6,$17
lw	$4,4($4)
lw	$9,7992($4)
lw	$8,7996($4)
lw	$7,2916($4)
lw	$4,10384($4)
sw	$9,7992($18)
sw	$8,7996($18)
sw	$7,2916($18)
sw	$4,10384($18)
$L1905:
lw	$4,0($2)
addiu	$2,$2,4
lw	$4,9776($4)
addu	$3,$3,$4
bne	$2,$5,$L1905
sw	$3,9776($6)

lw	$31,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,56

$L1900:
lw	$25,%got(decode_slice)($28)
addiu	$5,$sp,56
addiu	$25,$25,%lo(decode_slice)
.reloc	1f,R_MIPS_JALR,decode_slice
1:	jalr	$25
move	$4,$8

lw	$28,24($sp)
lw	$25,%call16(sde_slice_end)($28)
.reloc	1f,R_MIPS_JALR,sde_slice_end
1:	jalr	$25
lw	$4,56($sp)

lw	$28,24($sp)
lw	$3,%got(slicenum)($28)
lw	$2,0($3)
addiu	$2,$2,1
b	$L1896
sw	$2,0($3)

$L1914:
li	$2,4			# 0x4
sw	$5,16($sp)
li	$6,131072			# 0x20000
lw	$5,%got(decode_slice)($28)
move	$7,$0
sw	$2,20($sp)
ori	$6,$6,0xc454
lw	$25,616($8)
addiu	$5,$5,%lo(decode_slice)
addu	$6,$4,$6
jalr	$25
move	$4,$8

li	$2,45331			# 0xb113
lw	$3,56($sp)
addu	$2,$19,$2
sll	$2,$2,2
addu	$2,$3,$2
lw	$2,4($2)
lw	$5,7992($2)
lw	$4,7996($2)
lw	$3,2916($2)
lw	$2,10384($2)
sw	$5,7992($18)
sw	$4,7996($18)
sw	$3,2916($18)
b	$L1896
sw	$2,10384($18)

.set	macro
.set	reorder
.end	execute_decode_slices
.size	execute_decode_slices, .-execute_decode_slices
.section	.rodata.str1.4
.align	2
$LC41:
.ascii	"AVC: nal size %d\012\000"
.align	2
$LC42:
.ascii	"NAL %d at %d/%d length %d\012\000"
.align	2
$LC43:
.ascii	"AVC: Consumed only %d bytes instead of %d\012\000"
.align	2
$LC44:
.ascii	"Invalid mix of idr and non-idr slices\000"
.align	2
$LC45:
.ascii	"Unknown NAL code: %d (%d bits)\012\000"
.align	2
$LC46:
.ascii	"decode_slice_header error\012\000"
.section	.text.decode_nal_units,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_nal_units
.type	decode_nal_units, @function
decode_nal_units:
.frame	$sp,104,$31		# vars= 24, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
li	$7,131072			# 0x20000
lw	$3,0($4)
addiu	$sp,$sp,-104
addu	$7,$4,$7
sw	$6,112($sp)
li	$2,196608			# 0x30000
sw	$3,56($sp)
sw	$5,108($sp)
sw	$23,92($sp)
move	$23,$4
lw	$6,612($3)
addu	$2,$23,$2
lw	$3,9492($7)
lw	$5,112($sp)
lw	$4,68($4)
.cprestore	32
movn	$5,$0,$3
andi	$4,$4,0x8000
sw	$31,100($sp)
sw	$fp,96($sp)
sw	$22,88($sp)
sw	$21,84($sp)
sw	$20,80($sp)
sw	$19,76($sp)
sw	$18,72($sp)
sw	$17,68($sp)
sw	$16,64($sp)
sw	$5,52($sp)
bne	$4,$0,$L1917
sw	$6,-15208($2)

lw	$4,10456($23)
beq	$4,$0,$L2054
sw	$0,-15212($2)

$L1918:
lw	$25,%call16(ff_h264_reset_sei)($28)
.reloc	1f,R_MIPS_JALR,ff_h264_reset_sei
1:	jalr	$25
move	$4,$23

lw	$28,32($sp)
$L1917:
lw	$20,%got($L1945)($28)
move	$17,$0
move	$fp,$0
addiu	$20,$20,%lo($L1945)
$L1919:
lw	$3,52($sp)
$L2072:
slt	$2,$fp,$3
$L2081:
beq	$2,$0,$L1920
addiu	$5,$fp,3

slt	$2,$5,$3
beq	$2,$0,$L2055
li	$7,1			# 0x1

move	$4,$fp
addiu	$6,$3,-3
lw	$3,108($sp)
addu	$2,$3,$fp
move	$fp,$5
$L1932:
lbu	$5,0($2)
bne	$5,$0,$L1930
nop

lbu	$5,1($2)
bne	$5,$0,$L1930
nop

lbu	$5,2($2)
beq	$5,$7,$L1931
lw	$3,112($sp)

$L1930:
addiu	$4,$4,1
addiu	$fp,$fp,1
bne	$4,$6,$L1932
addiu	$2,$2,1

lw	$3,112($sp)
slt	$2,$fp,$3
bne	$2,$0,$L2072
lw	$3,52($sp)

move	$fp,$6
$L1923:
bne	$17,$0,$L2056
lw	$25,%got(execute_decode_slices)($28)

$L2048:
lw	$31,100($sp)
$L2071:
move	$2,$fp
lw	$fp,96($sp)
lw	$23,92($sp)
lw	$22,88($sp)
lw	$21,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
j	$31
addiu	$sp,$sp,104

$L1920:
lw	$3,112($sp)
slt	$2,$fp,$3
beq	$2,$0,$L1923
li	$2,131072			# 0x20000

addu	$2,$23,$2
lw	$2,9496($2)
blez	$2,$L1983
lw	$3,108($sp)

addu	$6,$2,$fp
move	$4,$0
addu	$8,$3,$fp
addu	$5,$3,$6
$L1925:
lbu	$2,0($8)
sll	$4,$4,8
addiu	$8,$8,1
bne	$8,$5,$L1925
or	$4,$2,$4

slt	$2,$4,2
move	$18,$4
bne	$2,$0,$L1926
move	$fp,$6

lw	$3,112($sp)
subu	$2,$3,$6
slt	$2,$2,$4
beq	$2,$0,$L2057
addu	$3,$4,$6

$L1924:
lw	$6,%got($LC41)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$7,$18
lw	$4,0($23)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC41)

beq	$17,$0,$L2048
lw	$28,32($sp)

b	$L2069
lw	$25,%got(execute_decode_slices)($28)

$L2057:
sw	$3,52($sp)
$L1929:
li	$2,45332			# 0xb114
lw	$3,52($sp)
lw	$25,%call16(ff_h264_decode_nal)($28)
addiu	$6,$sp,40
addu	$2,$17,$2
subu	$4,$3,$fp
lw	$3,108($sp)
sll	$2,$2,2
addiu	$7,$sp,44
addu	$2,$23,$2
sw	$4,16($sp)
addu	$5,$3,$fp
lw	$22,4($2)
.reloc	1f,R_MIPS_JALR,ff_h264_decode_nal
1:	jalr	$25
move	$4,$22

lw	$28,32($sp)
beq	$2,$0,$L1955
move	$19,$2

lw	$16,40($sp)
bltz	$16,$L2068
li	$2,-1			# 0xffffffffffffffff

lw	$2,88($23)
andi	$5,$2,0x1
beq	$5,$0,$L1934
lw	$4,44($sp)

addu	$4,$fp,$4
lw	$3,52($sp)
addiu	$5,$4,3
slt	$5,$5,$3
bne	$5,$0,$L2058
lw	$3,108($sp)

$L1934:
andi	$2,$2,0x4000
bne	$2,$0,$L1935
addu	$2,$19,$16

lbu	$2,-1($2)
bne	$2,$0,$L1935
nop

bne	$16,$0,$L2073
addiu	$16,$16,-1

addiu	$16,$16,1
b	$L1936
move	$16,$0

$L2059:
beq	$16,$0,$L1936
nop

addiu	$16,$16,-1
$L2073:
addu	$2,$19,$16
sw	$16,40($sp)
lbu	$2,-1($2)
beq	$2,$0,$L2059
nop

$L1935:
bne	$16,$0,$L2060
addiu	$5,$16,-1

move	$16,$0
$L1936:
lw	$4,0($23)
lw	$2,404($4)
andi	$2,$2,0x100
bne	$2,$0,$L2074
li	$2,131072			# 0x20000

$L1938:
li	$2,131072			# 0x20000
addu	$2,$23,$2
lw	$2,9492($2)
beq	$2,$0,$L1940
lw	$7,44($sp)

beq	$18,$7,$L1940
nop

bne	$18,$0,$L2062
lw	$6,%got($LC43)($28)

$L1940:
lw	$4,7988($23)
li	$2,1			# 0x1
beq	$4,$2,$L1941
addu	$fp,$fp,$7

lw	$3,56($sp)
lw	$2,708($3)
slt	$2,$2,8
bne	$2,$0,$L2075
addiu	$21,$16,7

$L1941:
li	$2,131072			# 0x20000
addu	$2,$23,$2
lw	$2,9468($2)
beq	$2,$0,$L2072
lw	$3,52($sp)

$L1942 = .
addiu	$21,$16,7
$L2075:
li	$2,131072			# 0x20000
sra	$21,$21,3
addu	$2,$22,$2
addu	$3,$19,$21
slt	$18,$16,0
movn	$3,$0,$18
lw	$7,9472($2)
sltu	$2,$7,20
beq	$2,$0,$L1943
sw	$3,48($sp)

$L2066:
sll	$2,$7,2
addu	$2,$20,$2
lw	$2,0($2)
addu	$2,$2,$28
j	$2
nop

.rdata
.align	2
.align	2
$L1945:
.gpword	$L1943
.gpword	$L1944
.gpword	$L1946
.gpword	$L1947
.gpword	$L1948
.gpword	$L1949
.gpword	$L1950
.gpword	$L1951
.gpword	$L1952
.gpword	$L1987
.gpword	$L1987
.gpword	$L1987
.gpword	$L1987
.gpword	$L1987
.gpword	$L1943
.gpword	$L1943
.gpword	$L1943
.gpword	$L1943
.gpword	$L1943
.gpword	$L1987
.section	.text.decode_nal_units
$L2060:
lw	$25,%call16(ff_h264_decode_rbsp_trailing)($28)
move	$4,$23
addu	$5,$19,$5
.reloc	1f,R_MIPS_JALR,ff_h264_decode_rbsp_trailing
1:	jalr	$25
sll	$16,$16,3

lw	$4,0($23)
subu	$16,$16,$2
lw	$2,404($4)
andi	$2,$2,0x100
beq	$2,$0,$L1938
lw	$28,32($sp)

li	$2,131072			# 0x20000
$L2074:
lw	$6,%got($LC42)($28)
lw	$3,112($sp)
li	$5,48			# 0x30
addu	$2,$22,$2
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC42)
lw	$7,9472($2)
lw	$2,40($sp)
sw	$fp,16($sp)
sw	$3,20($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,24($sp)

b	$L1938
lw	$28,32($sp)

$L1949:
li	$2,131072			# 0x20000
li	$4,5			# 0x5
addu	$2,$23,$2
lw	$5,9472($2)
bne	$5,$4,$L2063
lw	$25,%call16(ff_h264_remove_all_refs)($28)

move	$4,$23
.reloc	1f,R_MIPS_JALR,ff_h264_remove_all_refs
1:	jalr	$25
sw	$2,60($sp)

lw	$2,60($sp)
lw	$28,32($sp)
sw	$0,10700($2)
sw	$0,10696($2)
sw	$0,10688($2)
sw	$0,10684($2)
$L1944:
bltz	$21,$L1988
move	$4,$0

move	$2,$0
lw	$4,48($sp)
move	$5,$0
movz	$2,$16,$18
movz	$5,$19,$18
$L1956:
lw	$3,%got(decode_slice_header)($28)
sw	$2,10344($22)
li	$2,131072			# 0x20000
sw	$5,10332($22)
addiu	$5,$22,10332
addu	$2,$22,$2
sw	$4,10336($22)
addiu	$25,$3,%lo(decode_slice_header)
sw	$0,10340($22)
move	$4,$22
sw	$5,6932($2)
sw	$5,6928($2)
move	$5,$23
.reloc	1f,R_MIPS_JALR,decode_slice_header
1:	jalr	$25
sw	$0,10080($22)

bne	$2,$0,$L1953
lw	$28,32($sp)

li	$2,196608			# 0x30000
addu	$2,$23,$2
lw	$4,-15212($2)
li	$2,1			# 0x1
beq	$4,$2,$L1957
nop

$L1960:
li	$4,131072			# 0x20000
lw	$5,2696($23)
li	$2,5			# 0x5
addu	$4,$22,$4
lw	$4,9472($4)
beq	$4,$2,$L2064
lw	$6,48($5)

$L2045:
li	$4,196608			# 0x30000
addu	$4,$23,$4
lw	$2,-15176($4)
nor	$2,$0,$2
srl	$2,$2,31
$L1958:
li	$4,131072			# 0x20000
or	$2,$2,$6
addu	$4,$22,$4
sw	$2,48($5)
lw	$2,10712($4)
bne	$2,$0,$L2076
li	$2,196608			# 0x30000

lw	$2,7988($22)
slt	$2,$2,5
beq	$2,$0,$L2076
li	$2,196608			# 0x30000

lw	$3,56($sp)
lw	$2,708($3)
slt	$5,$2,8
beq	$5,$0,$L2065
nop

$L1962:
lw	$3,56($sp)
$L2070:
lw	$2,872($3)
beq	$2,$0,$L1970
lw	$6,44($sp)

move	$4,$3
lw	$3,108($sp)
lw	$25,28($2)
subu	$5,$fp,$6
jalr	$25
addu	$5,$3,$5

bgez	$2,$L1961
lw	$28,32($sp)

$L1955:
li	$2,-1			# 0xffffffffffffffff
$L2068:
lw	$31,100($sp)
lw	$fp,96($sp)
lw	$23,92($sp)
lw	$22,88($sp)
lw	$21,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
j	$31
addiu	$sp,$sp,104

$L1987:
move	$2,$0
$L1953:
li	$4,196608			# 0x30000
addu	$4,$23,$4
lw	$5,-15208($4)
beq	$17,$5,$L2077
lw	$3,%got(execute_decode_slices)($28)

$L1975:
bltz	$2,$L2078
lw	$6,%got($LC46)($28)

li	$4,1			# 0x1
$L2080:
bne	$2,$4,$L1919
li	$2,131072			# 0x20000

addu	$4,$22,$2
addu	$2,$23,$2
lw	$7,9472($4)
lw	$4,9468($4)
sw	$7,9472($2)
sw	$4,9468($2)
sltu	$2,$7,20
bne	$2,$0,$L2066
move	$22,$23

$L1943:
lw	$6,%got($LC45)($28)
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
lw	$4,56($sp)
addiu	$6,$6,%lo($LC45)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$16,16($sp)

li	$2,196608			# 0x30000
addu	$2,$23,$2
lw	$5,-15208($2)
bne	$5,$17,$L1919
lw	$28,32($sp)

move	$2,$0
$L1966:
lw	$3,%got(execute_decode_slices)($28)
$L2077:
move	$4,$23
sw	$2,60($sp)
addiu	$25,$3,%lo(execute_decode_slices)
.reloc	1f,R_MIPS_JALR,execute_decode_slices
1:	jalr	$25
move	$17,$0

lw	$28,32($sp)
b	$L1975
lw	$2,60($sp)

$L1951:
bltz	$21,$L1999
move	$2,$0

move	$5,$0
lw	$2,48($sp)
move	$6,$0
movz	$5,$16,$18
movz	$6,$19,$18
$L1972:
lw	$25,%call16(ff_h264_decode_seq_parameter_set)($28)
move	$4,$23
sw	$6,10332($23)
sw	$5,10344($23)
sw	$2,10336($23)
.reloc	1f,R_MIPS_JALR,ff_h264_decode_seq_parameter_set
1:	jalr	$25
sw	$0,10340($23)

li	$4,524288			# 0x80000
lw	$2,64($23)
and	$2,$2,$4
beq	$2,$0,$L1973
lw	$28,32($sp)

li	$2,1			# 0x1
sw	$2,10096($23)
$L1973:
lw	$3,56($sp)
lw	$2,264($3)
slt	$2,$2,2
beq	$2,$0,$L2076
li	$2,196608			# 0x30000

lw	$2,10096($23)
sltu	$2,$2,1
sw	$2,264($3)
$L1961:
li	$2,196608			# 0x30000
$L2076:
addu	$2,$23,$2
$L2079:
lw	$5,-15208($2)
bne	$5,$17,$L2072
lw	$3,52($sp)

b	$L1966
move	$2,$0

$L1950:
bltz	$21,$L1997
move	$2,$0

move	$5,$0
lw	$2,48($sp)
move	$6,$0
movz	$5,$16,$18
movz	$6,$19,$18
$L1971:
lw	$25,%call16(ff_h264_decode_sei)($28)
move	$4,$23
sw	$6,10332($23)
sw	$5,10344($23)
sw	$2,10336($23)
.reloc	1f,R_MIPS_JALR,ff_h264_decode_sei
1:	jalr	$25
sw	$0,10340($23)

b	$L1961
lw	$28,32($sp)

$L1952:
bltz	$21,$L2001
move	$2,$0

move	$6,$0
lw	$2,48($sp)
move	$5,$0
movz	$6,$16,$18
movz	$5,$19,$18
$L1974:
lw	$25,%call16(ff_h264_decode_picture_parameter_set)($28)
move	$4,$23
sw	$5,10332($23)
move	$5,$16
sw	$6,10344($23)
sw	$2,10336($23)
.reloc	1f,R_MIPS_JALR,ff_h264_decode_picture_parameter_set
1:	jalr	$25
sw	$0,10340($23)

b	$L1961
lw	$28,32($sp)

$L1948:
bltz	$21,$L1995
move	$5,$0

move	$6,$0
lw	$5,48($sp)
move	$7,$0
movz	$6,$16,$18
movz	$7,$19,$18
$L1969:
li	$4,131072			# 0x20000
addu	$2,$22,$4
addiu	$4,$4,6912
sw	$7,6912($2)
addu	$4,$22,$4
sw	$6,6924($2)
sw	$5,6916($2)
sw	$0,6920($2)
sw	$4,6932($2)
lw	$4,10712($2)
bne	$4,$0,$L1961
nop

lw	$4,6928($2)
beq	$4,$0,$L1961
nop

lw	$4,10080($22)
beq	$4,$0,$L1961
nop

lw	$4,124($23)
beq	$4,$0,$L1961
nop

lw	$4,7988($23)
slt	$4,$4,5
beq	$4,$0,$L1961
lw	$3,56($sp)

lw	$4,708($3)
slt	$5,$4,8
bne	$5,$0,$L1970
nop

lw	$2,9468($2)
beq	$2,$0,$L1961
slt	$2,$4,16

bne	$2,$0,$L1970
li	$2,65536			# 0x10000

li	$5,3			# 0x3
addu	$2,$22,$2
lw	$2,-5260($2)
beq	$2,$5,$L1961
slt	$5,$4,32

bne	$5,$0,$L1970
li	$5,1			# 0x1

bne	$2,$5,$L2076
li	$2,196608			# 0x30000

slt	$4,$4,48
beq	$4,$0,$L2079
addu	$2,$23,$2

$L1970:
b	$L1961
addiu	$17,$17,1

$L1947:
bltz	$21,$L1993
move	$5,$0

move	$6,$0
lw	$5,48($sp)
move	$7,$0
movz	$6,$16,$18
movz	$7,$19,$18
$L1968:
li	$2,131072			# 0x20000
addiu	$4,$2,6896
addu	$2,$22,$2
addu	$4,$22,$4
sw	$7,6896($2)
sw	$6,6908($2)
sw	$5,6900($2)
sw	$0,6904($2)
b	$L1961
sw	$4,6928($2)

$L1946:
bltz	$21,$L1991
move	$2,$0

lw	$4,48($sp)
move	$5,$0
movz	$2,$16,$18
movz	$5,$19,$18
$L1964:
sw	$5,10332($22)
sw	$2,10344($22)
li	$2,131072			# 0x20000
sw	$4,10336($22)
move	$4,$22
sw	$0,10340($22)
addu	$2,$22,$2
lw	$5,%got(decode_slice_header)($28)
sw	$0,6932($2)
sw	$0,6928($2)
addiu	$25,$5,%lo(decode_slice_header)
.reloc	1f,R_MIPS_JALR,decode_slice_header
1:	jalr	$25
move	$5,$23

li	$4,196608			# 0x30000
lw	$28,32($sp)
bltz	$2,$L1965
addu	$4,$23,$4

li	$5,1			# 0x1
sw	$5,10080($22)
lw	$5,-15208($4)
bne	$17,$5,$L2080
li	$4,1			# 0x1

lw	$3,%got(execute_decode_slices)($28)
move	$4,$23
sw	$2,60($sp)
addiu	$25,$3,%lo(execute_decode_slices)
.reloc	1f,R_MIPS_JALR,execute_decode_slices
1:	jalr	$25
move	$17,$0

lw	$28,32($sp)
b	$L1975
lw	$2,60($sp)

$L1988:
move	$2,$0
b	$L1956
move	$5,$0

$L1957:
lw	$4,0($23)
lw	$2,872($4)
beq	$2,$0,$L1960
move	$5,$0

lw	$25,24($2)
jalr	$25
move	$6,$0

bltz	$2,$L1955
lw	$28,32($sp)

li	$4,131072			# 0x20000
lw	$5,2696($23)
li	$2,5			# 0x5
addu	$4,$22,$4
lw	$4,9472($4)
bne	$4,$2,$L2045
lw	$6,48($5)

$L2064:
b	$L1958
li	$2,1			# 0x1

$L1999:
move	$5,$0
b	$L1972
move	$6,$0

$L1995:
move	$6,$0
b	$L1969
move	$7,$0

$L1965:
lw	$5,-15208($4)
beq	$17,$5,$L1966
lw	$6,%got($LC46)($28)

$L2078:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($23)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC46)

b	$L1919
lw	$28,32($sp)

$L1991:
move	$4,$0
b	$L1964
move	$5,$0

$L1926:
li	$2,1			# 0x1
bne	$4,$2,$L1924
nop

b	$L1919
addiu	$fp,$6,1

$L2058:
addu	$4,$3,$4
lbu	$5,0($4)
bne	$5,$0,$L1934
nop

lbu	$5,1($4)
bne	$5,$0,$L1934
li	$5,1			# 0x1

lbu	$6,2($4)
bne	$6,$5,$L1934
nop

lbu	$5,3($4)
li	$4,224			# 0xe0
bne	$5,$4,$L1934
nop

ori	$2,$2,0x4000
b	$L1934
sw	$2,88($23)

$L2065:
lw	$4,9468($4)
beq	$4,$0,$L1961
slt	$4,$2,16

bne	$4,$0,$L1962
li	$5,3			# 0x3

li	$4,65536			# 0x10000
addu	$4,$22,$4
lw	$4,-5260($4)
beq	$4,$5,$L1961
slt	$5,$2,32

bne	$5,$0,$L1962
li	$5,1			# 0x1

bne	$4,$5,$L1961
slt	$2,$2,48

beq	$2,$0,$L2076
li	$2,196608			# 0x30000

b	$L2070
lw	$3,56($sp)

$L2054:
b	$L1918
sw	$0,2696($23)

$L2056:
$L2069:
move	$4,$23
addiu	$25,$25,%lo(execute_decode_slices)
.reloc	1f,R_MIPS_JALR,execute_decode_slices
1:	jalr	$25
move	$5,$17

b	$L2071
lw	$31,100($sp)

$L2062:
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
lw	$4,0($23)
addiu	$6,$6,%lo($LC43)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$18,16($sp)

lw	$28,32($sp)
b	$L1940
lw	$7,44($sp)

$L1931:
slt	$2,$fp,$3
beq	$2,$0,$L1984
lw	$3,52($sp)

slt	$2,$fp,$3
beq	$2,$0,$L2081
slt	$2,$fp,$3

b	$L1929
move	$18,$0

$L2001:
move	$6,$0
b	$L1974
move	$5,$0

$L1993:
move	$6,$0
b	$L1968
move	$7,$0

$L1997:
move	$5,$0
b	$L1971
move	$6,$0

$L1983:
b	$L1924
move	$18,$0

$L2055:
lw	$3,112($sp)
move	$6,$fp
move	$fp,$5
slt	$2,$fp,$3
bne	$2,$0,$L2072
lw	$3,52($sp)

b	$L1923
move	$fp,$6

$L2063:
lw	$6,%got($LC44)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($23)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC44)

b	$L2068
li	$2,-1			# 0xffffffffffffffff

$L1984:
beq	$17,$0,$L2048
move	$fp,$4

b	$L2069
lw	$25,%got(execute_decode_slices)($28)

.set	macro
.set	reorder
.end	decode_nal_units
.size	decode_nal_units, .-decode_nal_units
.section	.rodata.str1.4
.align	2
$LC47:
.ascii	"avcC too short\012\000"
.align	2
$LC48:
.ascii	"Decoding sps %d from avcC failed\012\000"
.align	2
$LC49:
.ascii	"Decoding pps %d from avcC failed\012\000"
.section	.text.unlikely.ff_h264_decode_init,"ax",@progbits
.align	2
.globl	ff_h264_decode_init
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_init
.type	ff_h264_decode_init, @function
ff_h264_decode_init:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-56
lw	$2,%got(init_every_movie)($28)
li	$3,1			# 0x1
.cprestore	16
sw	$20,40($sp)
move	$20,$4
sw	$16,24($sp)
sw	$31,52($sp)
sw	$22,48($sp)
sw	$21,44($sp)
sw	$19,36($sp)
sw	$18,32($sp)
sw	$17,28($sp)
sw	$3,0($2)
li	$2,3			# 0x3
lw	$16,136($4)
#APP
# 1327 "h264.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
lw	$3,%got(tlb_i)($28)
li	$2,64			# 0x40
lw	$6,%got(vpu_base)($28)
move	$5,$0
li	$4,96			# 0x60
sw	$0,0($3)
$L2083:
lw	$3,0($6)
addu	$3,$3,$2
#APP
# 1333 "h264.c" 1
sw	 $5,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
bne	$2,$4,$L2083
lw	$2,%got(aux_base)($28)
lw	$3,0($2)
li	$2,1			# 0x1
#APP
# 1336 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(tcsm0_base)($28)
lw	$2,0($2)
addiu	$3,$2,16384
$L2084:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L2084
lw	$2,%got(tcsm1_base)($28)
li	$3,49152			# 0xc000
lw	$2,0($2)
addu	$3,$2,$3
$L2085:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L2085
lw	$2,%got(sram_base)($28)
lw	$2,0($2)
addiu	$3,$2,28672
$L2086:
sw	$0,0($2)
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$3,$L2086
lw	$25,%call16(av_malloc)($28)
.set	macro
.set	reorder

li	$4,524288			# 0x80000
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
addiu	$4,$4,32
.set	macro
.set	reorder

li	$3,-4			# 0xfffffffffffffffc
lw	$28,16($sp)
and	$2,$2,$3
lw	$4,%got(curr_slice_info_mv)($28)
lw	$3,%got(vpu_base)($28)
sw	$2,0($4)
lw	$2,%got(sde_frm_mv_cnt)($28)
lw	$3,0($3)
sw	$0,0($2)
li	$2,-2147483648			# 0xffffffff80000000
addiu	$2,$2,68
#APP
# 1357 "h264.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
li	$17,1			# 0x1
lw	$2,%got(use_jz_buf)($28)
lw	$25,%call16(MPV_decode_defaults)($28)
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_decode_defaults
1:	jalr	$25
sw	$17,0($2)
.set	macro
.set	reorder

addiu	$4,$16,10584
lw	$2,132($20)
lw	$28,16($sp)
lw	$3,44($20)
lw	$5,40($20)
lw	$2,8($2)
lw	$25,%call16(ff_h264dsp_init)($28)
sw	$3,12($16)
sw	$5,8($16)
sw	$2,52($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264dsp_init
1:	jalr	$25
sw	$20,0($16)
.set	macro
.set	reorder

addiu	$4,$16,10864
lw	$28,16($sp)
lw	$25,%call16(ff_h264_pred_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_pred_init
1:	jalr	$25
lw	$5,52($16)
.set	macro
.set	reorder

li	$2,131072			# 0x20000
lw	$28,16($sp)
li	$3,-1			# 0xffffffffffffffff
lw	$5,0($16)
addu	$2,$16,$2
addiu	$4,$16,2960
sw	$3,10652($2)
sw	$17,2948($16)
lw	$25,%call16(dsputil_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
sw	$17,2956($16)
.set	macro
.set	reorder

addiu	$4,$16,12892
lw	$28,16($sp)
li	$5,16			# 0x10
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,96			# 0x60
.set	macro
.set	reorder

addiu	$4,$16,12988
lw	$28,16($sp)
li	$5,16			# 0x10
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,128			# 0x80
.set	macro
.set	reorder

li	$4,4			# 0x4
lw	$3,232($20)
lw	$2,264($20)
lw	$28,16($sp)
sw	$4,28($16)
sw	$17,10044($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L2087
sw	$3,88($16)
.set	macro
.set	reorder

sw	$17,10096($16)
$L2087:
li	$17,1			# 0x1
lw	$25,%call16(ff_h264_decode_init_vlc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_decode_init_vlc
1:	jalr	$25
sw	$17,900($20)
.set	macro
.set	reorder

li	$3,196608			# 0x30000
lw	$28,16($sp)
li	$5,-2147483648			# 0xffffffff80000000
addu	$3,$16,$3
li	$2,131072			# 0x20000
sw	$16,-15276($3)
move	$4,$16
sw	$5,-16088($3)
li	$3,65536			# 0x10000
addu	$2,$16,$2
lw	$25,%call16(ff_h264_reset_sei)($28)
sw	$3,10684($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_reset_sei
1:	jalr	$25
sw	$0,9444($2)
.set	macro
.set	reorder

li	$2,28			# 0x1c
lw	$3,224($20)
.set	noreorder
.set	nomacro
bne	$3,$2,$L2088
lw	$28,16($sp)
.set	macro
.set	reorder

lw	$2,876($20)
.set	noreorder
.set	nomacro
bne	$2,$17,$L2115
li	$2,2			# 0x2
.set	macro
.set	reorder

lw	$3,0($16)
lw	$2,36($3)
sll	$2,$2,1
sw	$2,36($3)
li	$2,2			# 0x2
$L2115:
sw	$2,876($20)
$L2088:
lw	$3,28($20)
blez	$3,$L2090
lw	$2,24($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L2090
li	$5,1			# 0x1
.set	macro
.set	reorder

lb	$4,0($2)
.set	noreorder
.set	nomacro
bne	$4,$5,$L2090
li	$5,131072			# 0x20000
.set	macro
.set	reorder

slt	$3,$3,7
addu	$5,$16,$5
.set	noreorder
.set	nomacro
beq	$3,$0,$L2091
sw	$4,9492($5)
.set	macro
.set	reorder

lw	$6,%got($LC47)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC47)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2092
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L2091:
li	$3,2			# 0x2
lw	$19,%got(decode_nal_units)($28)
addiu	$21,$2,6
move	$22,$0
sw	$3,9496($5)
lbu	$18,5($2)
andi	$18,$18,0x1f
$L2093:
slt	$2,$22,$18
move	$5,$21
.set	noreorder
.set	nomacro
beq	$2,$0,$L2113
move	$4,$16
.set	macro
.set	reorder

lbu	$2,1($21)
addiu	$25,$19,%lo(decode_nal_units)
lbu	$17,0($21)
sll	$2,$2,8
or	$17,$2,$17
sll	$2,$17,8
srl	$17,$17,8
or	$17,$2,$17
andi	$17,$17,0xffff
addiu	$17,$17,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_nal_units
1:	jalr	$25
move	$6,$17
.set	macro
.set	reorder

addu	$21,$21,$17
.set	noreorder
.set	nomacro
bgez	$2,$L2094
lw	$28,16($sp)
.set	macro
.set	reorder

lw	$6,%got($LC48)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$20
move	$7,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC48)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2092
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L2094:
.set	noreorder
.set	nomacro
b	$L2093
addiu	$22,$22,1
.set	macro
.set	reorder

$L2113:
lw	$18,%got(decode_nal_units)($28)
addiu	$22,$21,1
lbu	$19,0($21)
move	$21,$0
$L2096:
slt	$2,$21,$19
move	$5,$22
.set	noreorder
.set	nomacro
beq	$2,$0,$L2114
move	$4,$16
.set	macro
.set	reorder

lbu	$2,1($22)
addiu	$25,$18,%lo(decode_nal_units)
lbu	$3,0($22)
sll	$2,$2,8
or	$3,$2,$3
sll	$2,$3,8
srl	$17,$3,8
or	$17,$2,$17
andi	$17,$17,0xffff
addiu	$17,$17,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_nal_units
1:	jalr	$25
move	$6,$17
.set	macro
.set	reorder

lw	$28,16($sp)
.set	noreorder
.set	nomacro
beq	$2,$17,$L2097
addu	$22,$22,$2
.set	macro
.set	reorder

lw	$6,%got($LC49)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$4,$20
move	$7,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC49)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L2092
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L2097:
.set	noreorder
.set	nomacro
b	$L2096
addiu	$21,$21,1
.set	macro
.set	reorder

$L2114:
lw	$2,24($20)
li	$3,131072			# 0x20000
addu	$3,$16,$3
lbu	$2,4($2)
andi	$2,$2,0x3
addiu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L2099
sw	$2,9496($3)
.set	macro
.set	reorder

$L2090:
lw	$3,0($16)
li	$2,131072			# 0x20000
lw	$25,%got(decode_nal_units)($28)
move	$4,$16
addu	$2,$16,$2
lw	$5,24($3)
addiu	$25,$25,%lo(decode_nal_units)
lw	$6,28($3)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_nal_units
1:	jalr	$25
sw	$0,9492($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L2092
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L2099:
lw	$2,12544($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L2116
lw	$31,52($sp)
.set	macro
.set	reorder

lw	$3,0($16)
lw	$4,12548($16)
lw	$2,264($3)
slt	$2,$2,$4
.set	noreorder
.set	nomacro
beq	$2,$0,$L2101
move	$2,$0
.set	macro
.set	reorder

sw	$4,264($3)
sw	$0,10096($16)
$L2101:
$L2092:
lw	$31,52($sp)
$L2116:
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

.end	ff_h264_decode_init
.size	ff_h264_decode_init, .-ff_h264_decode_init
.section	.rodata.str1.4
.align	2
$LC50:
.ascii	"no frame!\012\000"
.align	2
$LC51:
.ascii	"no picture\012\000"
.section	.text.decode_frame,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_frame
.type	decode_frame, @function
decode_frame:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
lw	$3,12($4)
lw	$2,596($4)
sw	$21,44($sp)
sw	$20,40($sp)
sw	$16,24($sp)
.cprestore	16
sw	$31,52($sp)
sw	$22,48($sp)
sw	$19,36($sp)
sw	$18,32($sp)
sw	$17,28($sp)
lw	$20,20($7)
lw	$21,16($7)
lw	$16,136($4)
lw	$7,%got(slicenum)($28)
sw	$3,64($16)
sw	$0,0($7)
bne	$20,$0,$L2118
sw	$2,68($16)

li	$9,196608			# 0x30000
addu	$2,$16,$9
lw	$3,-16156($2)
beq	$3,$0,$L2200
lw	$8,-16160($2)

lw	$2,48($3)
bne	$2,$0,$L2200
nop

lw	$2,284($3)
bne	$2,$0,$L2200
addiu	$9,$9,-16152

move	$11,$0
addu	$9,$16,$9
b	$L2124
li	$10,1			# 0x1

$L2249:
lw	$7,48($3)
bne	$7,$0,$L2257
li	$2,45112			# 0xb038

lw	$7,284($3)
bne	$7,$0,$L2248
addiu	$9,$9,4

$L2124:
lw	$7,276($3)
lw	$12,276($8)
slt	$7,$7,$12
movn	$8,$3,$7
lw	$3,0($9)
movn	$11,$10,$7
bne	$3,$0,$L2249
addiu	$10,$10,1

li	$2,45112			# 0xb038
$L2257:
move	$3,$11
addu	$2,$11,$2
sll	$2,$2,2
addu	$2,$16,$2
lw	$2,0($2)
$L2122:
beq	$2,$0,$L2258
li	$2,216			# 0xd8

$L2125:
li	$2,45113			# 0xb039
addu	$3,$3,$2
sll	$3,$3,2
addu	$2,$16,$3
$L2128:
lw	$3,0($2)
addiu	$2,$2,4
sw	$3,-8($2)
lw	$3,-4($2)
bne	$3,$0,$L2128
nop

beq	$8,$0,$L2127
li	$2,216			# 0xd8

$L2258:
addiu	$3,$8,208
sw	$2,0($6)
$L2131:
lw	$7,0($8)
addiu	$8,$8,16
addiu	$5,$5,16
lw	$6,-12($8)
lw	$4,-8($8)
lw	$2,-4($8)
sw	$7,-16($5)
sw	$6,-12($5)
sw	$4,-8($5)
bne	$8,$3,$L2131
sw	$2,-4($5)

lw	$4,0($8)
move	$2,$0
lw	$3,4($8)
sw	$4,0($5)
sw	$3,4($5)
$L2240:
lw	$31,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L2118:
lw	$25,%call16(sde_frame_init)($28)
move	$17,$4
move	$4,$16
move	$18,$6
.reloc	1f,R_MIPS_JALR,sde_frame_init
1:	jalr	$25
move	$19,$5

lw	$28,16($sp)
lw	$25,%call16(aux_mem_init)($28)
.reloc	1f,R_MIPS_JALR,aux_mem_init
1:	jalr	$25
nop

move	$5,$21
lw	$28,16($sp)
move	$4,$16
lw	$25,%got(decode_nal_units)($28)
addiu	$25,$25,%lo(decode_nal_units)
.reloc	1f,R_MIPS_JALR,decode_nal_units
1:	jalr	$25
move	$6,$20

lw	$28,16($sp)
bltz	$2,$L2201
move	$21,$2

lw	$2,68($16)
andi	$2,$2,0x8000
bne	$2,$0,$L2132
nop

lw	$22,2696($16)
beq	$22,$0,$L2250
nop

$L2133:
lw	$25,%got(field_end)($28)
addiu	$25,$25,%lo(field_end)
.reloc	1f,R_MIPS_JALR,field_end
1:	jalr	$25
move	$4,$16

li	$2,2147418112			# 0x7fff0000
lw	$4,268($22)
ori	$2,$2,0xffff
beq	$4,$2,$L2135
lw	$28,16($sp)

lw	$3,272($22)
beq	$3,$2,$L2135
nop

lw	$2,12788($16)
sw	$0,164($22)
beq	$2,$0,$L2137
sw	$0,156($22)

li	$2,196608			# 0x30000
addu	$2,$16,$2
lw	$6,-15196($2)
sltu	$2,$6,9
beq	$2,$0,$L2138
lw	$5,%got($L2140)($28)

sll	$2,$6,2
addiu	$5,$5,%lo($L2140)
addu	$2,$5,$2
lw	$2,0($2)
addu	$2,$2,$28
j	$2
nop

.rdata
.align	2
.align	2
$L2140:
.gpword	$L2138
.gpword	$L2145
.gpword	$L2145
.gpword	$L2141
.gpword	$L2141
.gpword	$L2142
.gpword	$L2142
.gpword	$L2143
.gpword	$L2144
.section	.text.decode_frame
$L2248:
li	$7,45112			# 0xb038
move	$3,$11
addu	$7,$11,$7
sll	$7,$7,2
addu	$7,$16,$7
b	$L2122
lw	$2,0($7)

$L2132:
lw	$3,164($16)
lw	$2,7996($16)
slt	$2,$2,$3
bne	$2,$0,$L2259
lw	$25,%call16(ff_print_debug_info)($28)

bne	$3,$0,$L2251
nop

$L2134:
lw	$25,%call16(ff_print_debug_info)($28)
$L2259:
move	$4,$16
.reloc	1f,R_MIPS_JALR,ff_print_debug_info
1:	jalr	$25
move	$5,$19

beq	$21,$0,$L2205
li	$3,10			# 0xa

addiu	$3,$21,9
$L2189:
lw	$31,52($sp)
slt	$3,$3,$20
move	$2,$20
lw	$22,48($sp)
movn	$2,$21,$3
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L2205:
b	$L2189
li	$21,1			# 0x1

$L2200:
bne	$8,$0,$L2125
move	$3,$0

$L2127:
lw	$31,52($sp)
$L2267:
move	$2,$0
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L2135:
b	$L2134
sw	$0,0($18)

$L2251:
b	$L2133
lw	$22,2696($16)

$L2137:
li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$2,-5252($2)
bne	$2,$0,$L2151
li	$2,1			# 0x1

lw	$2,10384($16)
xori	$2,$2,0x3
sltu	$2,$0,$2
$L2151:
sw	$2,164($22)
$L2149:
li	$5,196608			# 0x30000
addu	$5,$16,$5
beq	$4,$3,$L2152
sw	$2,-15192($5)

$L2190:
slt	$3,$4,$3
sw	$3,168($22)
$L2153:
lw	$12,12544($16)
beq	$12,$0,$L2158
lw	$8,0($16)

lw	$3,12548($16)
lw	$2,264($8)
slt	$2,$2,$3
beq	$2,$0,$L2193
nop

sw	$3,264($8)
sw	$0,10096($16)
$L2193:
li	$3,196608			# 0x30000
addu	$2,$16,$3
lw	$2,-16160($2)
beq	$2,$0,$L2203
addiu	$3,$3,-16156

move	$4,$0
addu	$3,$16,$3
$L2161:
addiu	$3,$3,4
lw	$5,-4($3)
bne	$5,$0,$L2161
addiu	$4,$4,1

addiu	$9,$4,1
$L2160:
li	$2,45112			# 0xb038
lw	$3,80($22)
addu	$2,$4,$2
sll	$2,$2,2
addu	$2,$16,$2
bne	$3,$0,$L2162
sw	$22,0($2)

li	$2,4			# 0x4
sw	$2,80($22)
$L2162:
li	$5,196608			# 0x30000
addu	$2,$16,$5
lw	$3,-16156($2)
beq	$3,$0,$L2244
lw	$14,-16160($2)

lw	$2,48($3)
bne	$2,$0,$L2244
nop

lw	$2,284($3)
bne	$2,$0,$L2244
li	$2,1			# 0x1

addiu	$5,$5,-16152
lw	$13,276($14)
move	$10,$0
addu	$5,$16,$5
b	$L2168
move	$11,$14

$L2252:
lw	$6,48($3)
bne	$6,$0,$L2164
nop

lw	$6,284($3)
bne	$6,$0,$L2164
addiu	$5,$5,4

$L2168:
lw	$6,276($3)
slt	$7,$6,$13
beq	$7,$0,$L2167
nop

move	$13,$6
move	$10,$2
move	$11,$3
$L2167:
lw	$3,0($5)
bne	$3,$0,$L2252
addiu	$2,$2,1

$L2164:
lw	$3,264($8)
bne	$3,$0,$L2260
li	$2,196608			# 0x30000

lw	$2,48($14)
bne	$2,$0,$L2261
li	$2,196608			# 0x30000

lw	$2,284($14)
beq	$2,$0,$L2169
li	$2,196608			# 0x30000

$L2261:
li	$3,-2147483648			# 0xffffffff80000000
addu	$2,$16,$2
beq	$12,$0,$L2171
sw	$3,-16088($2)

lw	$2,264($8)
lw	$3,12548($16)
slt	$2,$2,$3
beq	$2,$0,$L2172
li	$2,-2147483648			# 0xffffffff80000000

$L2176:
lw	$3,10096($16)
beq	$3,$0,$L2172
li	$3,-2147483648			# 0xffffffff80000000

move	$5,$0
$L2266:
beq	$2,$3,$L2180
addiu	$2,$2,2

slt	$13,$2,$13
bne	$13,$0,$L2178
nop

$L2180:
lw	$3,52($22)
li	$2,3			# 0x3
beq	$3,$2,$L2178
nop

$L2175:
beq	$5,$0,$L2172
li	$2,45112			# 0xb038

$L2255:
lw	$3,80($11)
li	$4,-5			# 0xfffffffffffffffb
addu	$2,$10,$2
and	$3,$3,$4
sll	$2,$2,2
sw	$3,80($11)
addu	$2,$16,$2
lw	$2,0($2)
beq	$2,$0,$L2262
lw	$6,%got($LC51)($28)

$L2195:
li	$3,45113			# 0xb039
addu	$3,$10,$3
sll	$3,$3,2
addu	$3,$16,$3
$L2184:
lw	$2,0($3)
addiu	$3,$3,4
sw	$2,-8($3)
lw	$2,-4($3)
bne	$2,$0,$L2184
nop

bne	$5,$0,$L2262
lw	$6,%got($LC51)($28)

lw	$2,264($8)
$L2256:
slt	$2,$2,$9
beq	$2,$0,$L2183
li	$2,216			# 0xd8

bne	$10,$0,$L2185
sw	$2,0($18)

li	$2,196608			# 0x30000
addu	$2,$16,$2
lw	$2,-16160($2)
beq	$2,$0,$L2185
nop

lw	$3,48($2)
bne	$3,$0,$L2186
nop

lw	$2,284($2)
beq	$2,$0,$L2185
nop

$L2186:
li	$2,196608			# 0x30000
li	$3,-2147483648			# 0xffffffff80000000
addu	$2,$16,$2
sw	$3,-16088($2)
$L2187:
addiu	$3,$11,208
move	$2,$19
$L2188:
lw	$7,0($11)
addiu	$11,$11,16
addiu	$2,$2,16
lw	$6,-12($11)
lw	$5,-8($11)
lw	$4,-4($11)
sw	$7,-16($2)
sw	$6,-12($2)
sw	$5,-8($2)
bne	$11,$3,$L2188
sw	$4,-4($2)

lw	$4,0($11)
lw	$3,4($11)
sw	$4,0($2)
b	$L2134
sw	$3,4($2)

$L2152:
bne	$2,$0,$L2263
li	$2,196608			# 0x30000

lw	$2,12788($16)
$L2254:
beq	$2,$0,$L2155
li	$2,196608			# 0x30000

$L2263:
li	$3,3			# 0x3
addu	$2,$16,$2
lw	$2,-15196($2)
beq	$2,$3,$L2156
li	$3,5			# 0x5

beq	$2,$3,$L2264
li	$2,1			# 0x1

$L2155:
b	$L2153
sw	$0,168($22)

$L2145:
li	$2,1			# 0x1
$L2265:
sw	$2,164($22)
$L2138:
li	$2,196608			# 0x30000
addu	$2,$16,$2
lw	$2,-15188($2)
andi	$5,$2,0x3
beq	$5,$0,$L2243
sltu	$6,$6,5

beq	$6,$0,$L2243
nop

srl	$2,$2,1
andi	$2,$2,0x1
b	$L2149
sw	$2,164($22)

$L2144:
li	$2,4			# 0x4
sw	$2,156($22)
$L2147:
li	$2,196608			# 0x30000
addu	$2,$16,$2
bne	$4,$3,$L2190
sw	$0,-15192($2)

b	$L2254
lw	$2,12788($16)

$L2141:
li	$2,65536			# 0x10000
addu	$2,$16,$2
lw	$2,-5252($2)
bne	$2,$0,$L2265
li	$2,1			# 0x1

lw	$5,10384($16)
li	$2,3			# 0x3
bne	$5,$2,$L2265
li	$2,1			# 0x1

li	$2,196608			# 0x30000
addu	$2,$16,$2
lw	$2,-15192($2)
b	$L2138
sw	$2,164($22)

$L2143:
li	$2,2			# 0x2
b	$L2147
sw	$2,156($22)

$L2142:
li	$2,1			# 0x1
b	$L2138
sw	$2,156($22)

$L2253:
slt	$4,$4,16
beq	$4,$0,$L2177
nop

$L2178:
lw	$2,264($8)
sw	$0,10096($16)
addiu	$2,$2,1
b	$L2175
sw	$2,264($8)

$L2156:
li	$2,1			# 0x1
$L2264:
b	$L2153
sw	$2,168($22)

$L2169:
$L2260:
addu	$2,$16,$2
lw	$2,-16088($2)
beq	$12,$0,$L2174
slt	$5,$13,$2

lw	$6,12548($16)
slt	$3,$3,$6
beq	$3,$0,$L2175
nop

$L2174:
beq	$5,$0,$L2176
nop

lw	$3,264($8)
beq	$3,$4,$L2253
nop

$L2177:
lw	$3,10096($16)
bne	$3,$0,$L2266
li	$3,-2147483648			# 0xffffffff80000000

b	$L2255
li	$2,45112			# 0xb038

$L2171:
lw	$2,10096($16)
beq	$2,$0,$L2172
li	$2,3			# 0x3

lw	$3,52($22)
beq	$3,$2,$L2178
move	$5,$0

$L2172:
lw	$2,264($8)
slt	$2,$2,$9
beq	$2,$0,$L2262
lw	$6,%got($LC51)($28)

li	$2,45112			# 0xb038
lw	$3,80($11)
li	$4,-5			# 0xfffffffffffffffb
addu	$2,$10,$2
and	$3,$3,$4
sll	$2,$2,2
sw	$3,80($11)
addu	$2,$16,$2
lw	$2,0($2)
bne	$2,$0,$L2195
move	$5,$0

b	$L2256
lw	$2,264($8)

$L2183:
lw	$6,%got($LC51)($28)
$L2262:
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
move	$4,$17
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC51)

b	$L2134
lw	$28,16($sp)

$L2158:
lw	$2,244($8)
blez	$2,$L2193
li	$2,16			# 0x10

sw	$2,264($8)
b	$L2193
sw	$0,10096($16)

$L2243:
b	$L2149
lw	$2,164($22)

$L2250:
lw	$2,708($17)
slt	$2,$2,8
beq	$2,$0,$L2267
lw	$31,52($sp)

lw	$2,7988($16)
bne	$2,$0,$L2267
lw	$25,%call16(av_log)($28)

lw	$6,%got($LC50)($28)
li	$5,16			# 0x10
move	$4,$17
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC50)

b	$L2240
li	$2,-1			# 0xffffffffffffffff

$L2185:
lw	$3,276($11)
li	$2,196608			# 0x30000
addu	$2,$16,$2
b	$L2187
sw	$3,-16088($2)

$L2244:
lw	$13,276($14)
move	$11,$14
b	$L2164
move	$10,$0

$L2203:
li	$9,1			# 0x1
b	$L2160
move	$4,$0

$L2201:
b	$L2240
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	decode_frame
.size	decode_frame, .-decode_frame
.section	.text.unlikely.ff_h264_free_context,"ax",@progbits
.align	2
.globl	ff_h264_free_context
.set	nomips16
.set	nomicromips
.ent	ff_h264_free_context
.type	ff_h264_free_context, @function
ff_h264_free_context:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(free_tables)($28)
addiu	$sp,$sp,-40
.cprestore	16
addiu	$25,$25,%lo(free_tables)
sw	$18,32($sp)
sw	$17,28($sp)
sw	$16,24($sp)
sw	$31,36($sp)
.reloc	1f,R_MIPS_JALR,free_tables
1:	jalr	$25
move	$18,$4

li	$2,131072			# 0x20000
lw	$28,16($sp)
addiu	$17,$2,9500
addiu	$2,$2,9628
addu	$17,$18,$17
addu	$16,$18,$2
$L2269:
lw	$25,%call16(av_freep)($28)
move	$4,$17
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$17,$17,4

bne	$17,$16,$L2269
lw	$28,16($sp)

li	$17,131072			# 0x20000
addiu	$17,$17,10652
addu	$17,$18,$17
$L2270:
lw	$25,%call16(av_freep)($28)
move	$4,$16
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$16,$16,4

bne	$16,$17,$L2270
lw	$28,16($sp)

lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_h264_free_context
.size	ff_h264_free_context, .-ff_h264_free_context
.section	.text.unlikely.ff_h264_decode_end,"ax",@progbits
.align	2
.globl	ff_h264_decode_end
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_end
.type	ff_h264_decode_end, @function
ff_h264_decode_end:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-40
lw	$2,%got(use_jz_buf)($28)
lw	$25,%call16(av_freep)($28)
sw	$17,32($sp)
sw	$16,28($sp)
lw	$17,%got(curr_slice_info_mv)($28)
lw	$16,136($4)
.cprestore	16
sw	$31,36($sp)
sw	$0,0($2)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
lw	$4,0($17)

move	$4,$16
lw	$28,16($sp)
lw	$25,%call16(ff_h264_free_context)($28)
.reloc	1f,R_MIPS_JALR,ff_h264_free_context
1:	jalr	$25
sw	$0,0($17)

lw	$28,16($sp)
lw	$25,%call16(MPV_common_end)($28)
.reloc	1f,R_MIPS_JALR,MPV_common_end
1:	jalr	$25
move	$4,$16

move	$2,$0
lw	$31,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_h264_decode_end
.size	ff_h264_decode_end, .-ff_h264_decode_end
.rdata
.align	2
.type	CSWTCH.364, @object
.size	CSWTCH.364, 24
CSWTCH.364:
.word	2
.word	0
.word	1
.word	-1
.word	4
.word	3
.align	2
.type	x_offset.8879, @object
.size	x_offset.8879, 16
x_offset.8879:
.word	0
.word	16
.word	64
.word	80
.align	2
.type	y_offset.8880, @object
.size	y_offset.8880, 16
y_offset.8880:
.word	0
.word	32
.word	128
.word	160
.align	2
.type	left.8826, @object
.size	left.8826, 7
left.8826:
.byte	5
.byte	-1
.byte	2
.byte	-1
.byte	6
.space	2
.align	2
.type	top.8825, @object
.size	top.8825, 7
top.8825:
.byte	4
.byte	1
.byte	-1
.byte	-1
.space	3
.align	2
.type	left.8809, @object
.size	left.8809, 12
left.8809:
.byte	0
.byte	-1
.byte	10
.byte	0
.byte	-1
.byte	-1
.byte	-1
.byte	0
.byte	-1
.byte	11
.space	2
.align	2
.type	mask.8815, @object
.size	mask.8815, 16
mask.8815:
.word	32768
.word	8192
.word	128
.word	32
.align	2
.type	top.8808, @object
.size	top.8808, 12
top.8808:
.byte	-1
.byte	0
.byte	9
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	0
.space	3
.globl	h264_decoder
.section	.rodata.str1.4
.align	2
$LC52:
.ascii	"h264\000"
.align	2
$LC53:
.ascii	"H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	h264_decoder, @object
.size	h264_decoder, 72
h264_decoder:
.word	$LC52
.word	0
.word	28
.word	181616
.word	ff_h264_decode_init
.word	0
.word	ff_h264_decode_end
.word	decode_frame
.word	34
.space	4
.word	flush_dpb
.space	4
.word	ff_hwaccel_pixfmt_list_420
.word	$LC53
.space	16
.rdata
.align	2
.type	div6, @object
.size	div6, 52
div6:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	7
.byte	7
.byte	7
.byte	7
.byte	7
.byte	7
.byte	8
.byte	8
.byte	8
.byte	8
.align	2
.type	rem6, @object
.size	rem6, 52
rem6:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	0
.byte	1
.byte	2
.byte	3
.globl	slicenum
.section	.bss,"aw",@nobits
.align	2
.type	slicenum, @object
.size	slicenum, 4
slicenum:
.space	4

.comm	DBLK_BASE,4,4
.rdata
.align	2
.type	IntpFMT, @object
.size	IntpFMT, 6720
IntpFMT:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.space	7
.byte	0
.space	7
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	7
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	SubPel, @object
.size	SubPel, 12
SubPel:
.byte	1
.byte	2
.byte	2
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.align	2
.type	AryFMT, @object
.size	AryFMT, 16
AryFMT:
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0

.comm	motion_iwta,4,4

.comm	motion_dsa,4,4
.globl	read_cnt
.section	.bss
.align	2
.type	read_cnt, @object
.size	read_cnt, 4
read_cnt:
.space	4
.globl	write_cnt
.align	2
.type	write_cnt, @object
.size	write_cnt, 4
write_cnt:
.space	4

.comm	qp_div_tab,64,4

.comm	vqscale,4,4
.globl	sde_vlc2_table
.data
.align	2
.type	sde_vlc2_table, @object
.size	sde_vlc2_table, 1792
sde_vlc2_table:
.half	-24544
.half	-30596
.half	-32660
.half	8207
.half	4106
.half	4106
.half	4106
.half	4106
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	-24512
.half	-28560
.half	-32648
.half	-32646
.half	8227
.half	8218
.half	8213
.half	8208
.half	6175
.half	6175
.half	6166
.half	6166
.half	6161
.half	6161
.half	6156
.half	6156
.half	4123
.half	4123
.half	4123
.half	4123
.half	4114
.half	4114
.half	4114
.half	4114
.half	4109
.half	4109
.half	4109
.half	4109
.half	4104
.half	4104
.half	4104
.half	4104
.half	16385
.half	8245
.half	-32672
.half	-32670
.half	-32668
.half	-32666
.half	-32664
.half	-32662
.half	8251
.half	8246
.half	8241
.half	8240
.half	8247
.half	8242
.half	8237
.half	8236
.half	6195
.half	6195
.half	6190
.half	6190
.half	6185
.half	6185
.half	6184
.half	6184
.half	6191
.half	6191
.half	6186
.half	6186
.half	6181
.half	6181
.half	6180
.half	6180
.half	64
.half	66
.half	65
.half	60
.half	67
.half	62
.half	61
.half	56
.half	63
.half	58
.half	57
.half	52
.half	9
.half	4
.half	16384
.half	16384
.half	4128
.half	4134
.half	4129
.half	4124
.half	4139
.half	4130
.half	4125
.half	4120
.half	39
.half	30
.half	25
.half	20
.half	2071
.half	2062
.half	19
.half	19
.half	-24544
.half	-30616
.half	-32660
.half	-32658
.half	-32656
.half	-32654
.half	8215
.half	8201
.half	6163
.half	6163
.half	6159
.half	6159
.half	4106
.half	4106
.half	4106
.half	4106
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2053
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	-26560
.half	-28592
.half	-30632
.half	-30628
.half	-32672
.half	-32670
.half	-32668
.half	-32666
.half	6183
.half	6183
.half	6174
.half	6174
.half	6173
.half	6173
.half	6168
.half	6168
.half	4116
.half	4116
.half	4116
.half	4116
.half	4122
.half	4122
.half	4122
.half	4122
.half	4121
.half	4121
.half	4121
.half	4121
.half	4112
.half	4112
.half	4112
.half	4112
.half	16385
.half	16385
.half	4159
.half	4159
.half	6211
.half	6210
.half	6209
.half	6208
.half	6205
.half	6204
.half	6206
.half	6201
.half	4154
.half	4154
.half	4152
.half	4152
.half	4155
.half	4150
.half	4149
.half	4148
.half	4151
.half	4146
.half	4145
.half	4144
.half	2092
.half	2094
.half	2093
.half	2088
.half	2099
.half	2090
.half	2089
.half	2084
.half	47
.half	38
.half	37
.half	32
.half	43
.half	34
.half	33
.half	28
.half	2083
.half	2070
.half	2069
.half	2060
.half	31
.half	18
.half	17
.half	8
.half	27
.half	14
.half	13
.half	4
.space	24
.half	-26560
.half	-28592
.half	-30632
.half	-30628
.half	-32672
.half	-32670
.half	-32668
.half	-32666
.half	10252
.half	10270
.half	10269
.half	10248
.half	10279
.half	10266
.half	10265
.half	10244
.half	8213
.half	8213
.half	8214
.half	8214
.half	8209
.half	8209
.half	8210
.half	8210
.half	8205
.half	8205
.half	8227
.half	8227
.half	8206
.half	8206
.half	8201
.half	8201
.half	6175
.half	6175
.half	6175
.half	6175
.half	6171
.half	6171
.half	6171
.half	6171
.half	6167
.half	6167
.half	6167
.half	6167
.half	6163
.half	6163
.half	6163
.half	6163
.half	6159
.half	6159
.half	6159
.half	6159
.half	6154
.half	6154
.half	6154
.half	6154
.half	6149
.half	6149
.half	6149
.half	6149
.half	6144
.half	6144
.half	6144
.half	6144
.half	16385
.half	6208
.half	6211
.half	6210
.half	6209
.half	6204
.half	6207
.half	6206
.half	6205
.half	6200
.half	6203
.half	6202
.half	6201
.half	6196
.half	4149
.half	4149
.half	4144
.half	4150
.half	4145
.half	4140
.half	4151
.half	4146
.half	4141
.half	4136
.half	2099
.half	2094
.half	2089
.half	2084
.half	2095
.half	2090
.half	2085
.half	2080
.half	28
.half	24
.half	38
.half	20
.half	43
.half	34
.half	33
.half	16
.space	48
.half	10244
.half	10245
.half	16385
.half	10240
.half	10248
.half	10249
.half	10250
.half	16385
.half	10252
.half	10253
.half	10254
.half	10255
.half	10256
.half	10257
.half	10258
.half	10259
.half	10260
.half	10261
.half	10262
.half	10263
.half	10264
.half	10265
.half	10266
.half	10267
.half	10268
.half	10269
.half	10270
.half	10271
.half	10272
.half	10273
.half	10274
.half	10275
.half	10276
.half	10277
.half	10278
.half	10279
.half	10280
.half	10281
.half	10282
.half	10283
.half	10284
.half	10285
.half	10286
.half	10287
.half	10288
.half	10289
.half	10290
.half	10291
.half	10292
.half	10293
.half	10294
.half	10295
.half	10296
.half	10297
.half	10298
.half	10299
.half	10300
.half	10301
.half	10302
.half	10303
.half	10304
.half	10305
.half	10306
.half	10307
.space	128
.half	-30656
.half	-32700
.half	10256
.half	10252
.half	10248
.half	10255
.half	10249
.half	10244
.half	4106
.half	4106
.half	4106
.half	4106
.half	4106
.half	4106
.half	4106
.half	4106
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	5
.half	19
.half	19
.half	2066
.half	2065
.half	14
.half	13
.space	116
.half	-28608
.half	-32696
.half	10248
.half	10247
.half	8198
.half	8198
.half	8197
.half	8197
.half	6148
.half	6148
.half	6148
.half	6148
.half	6147
.half	6147
.half	6147
.half	6147
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	16385
.half	4111
.half	4110
.half	4109
.half	2060
.half	2060
.half	2059
.half	2059
.half	10
.half	9
.space	108
.half	-24512
.half	10249
.half	8200
.half	8200
.half	6151
.half	6151
.half	6151
.half	6151
.half	4102
.half	4102
.half	4102
.half	4102
.half	4102
.half	4102
.half	4102
.half	4102
.half	4101
.half	4101
.half	4101
.half	4101
.half	4101
.half	4101
.half	4101
.half	4101
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	16385
.half	8206
.half	6157
.half	6157
.half	4108
.half	4108
.half	4108
.half	4108
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	2059
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.half	10
.space	64
.globl	sde_vlc2_sta
.align	2
.type	sde_vlc2_sta, @object
.size	sde_vlc2_sta, 84
sde_vlc2_sta:
.word	0
.word	128
.word	6
.word	128
.word	116
.word	5
.word	256
.word	104
.word	6
.word	384
.word	64
.word	6
.word	512
.word	70
.word	6
.word	640
.word	74
.word	6
.word	768
.word	96
.word	6

.comm	last2_qscal_sde,4,4

.comm	last_qscal_sde,4,4

.comm	qscal_sde,4,4

.comm	check_qscal_sde,4,4

.comm	sde_init_ofst,4,4

.comm	sde_mb_terminate,4,4

.comm	mx_rem_sde,4,4

.comm	my_sde,4,4

.comm	mx_sde,4,4

.comm	mb_num_sde,4,4

.comm	frm_info_mv_cnt,4,4

.comm	slice_num_sde,4,4

.comm	frm_info_slice_start_mb_init,4,4

.comm	slice_mv_cnt,4,4

.comm	curr_slice_info_mv,4,4

.comm	curr_slice_info_ctrl,4,4

.comm	sde_top_info_tmp,76,4

.comm	sde_top_info,9120,4

.comm	sde_left_info,76,4

.comm	sde_slice_info_ptr,4,4

.comm	sde_slice_info,64,4
.globl	lps_comb
.align	2
.type	lps_comb, @object
.size	lps_comb, 512
lps_comb:
.word	-271601666
.word	-271601665
.word	-490428674
.word	-490428673
.word	-675635714
.word	-675635713
.word	-860776972
.word	-860776971
.word	-1029141018
.word	-1029141017
.word	-1197504804
.word	-1197504803
.word	-1365803056
.word	-1365803055
.word	-1517323834
.word	-1517323833
.word	-1652001860
.word	-1652001859
.word	-1786679886
.word	-1786679885
.word	-1921357912
.word	-1921357911
.word	-2039192928
.word	-2039192927
.word	2137939352
.word	2137939353
.word	2036881552
.word	2036881553
.word	1935889544
.word	1935889545
.word	1834897282
.word	1834897283
.word	1733905274
.word	1733905275
.word	1649756020
.word	1649756021
.word	1565541486
.word	1565541487
.word	1481392232
.word	1481392233
.word	1414020452
.word	1414020453
.word	1329871454
.word	1329871455
.word	1262565210
.word	1262565211
.word	1195258964
.word	1195258965
.word	1144664400
.word	1144664401
.word	1077358412
.word	1077358413
.word	1026894920
.word	1026894921
.word	976366148
.word	976366149
.word	925837376
.word	925837377
.word	875374142
.word	875374143
.word	824845370
.word	824845371
.word	791159352
.word	791159353
.word	740696116
.word	740696117
.word	707010098
.word	707010099
.word	673324334
.word	673324335
.word	639638316
.word	639638317
.word	606018090
.word	606018091
.word	572332328
.word	572332329
.word	538711846
.word	538711847
.word	505026084
.word	505026085
.word	488183074
.word	488183075
.word	454562848
.word	454562849
.word	437654302
.word	437654303
.word	404034076
.word	404034077
.word	387191066
.word	387191067
.word	370348058
.word	370348059
.word	353505048
.word	353505049
.word	336662038
.word	336662039
.word	319819030
.word	319819031
.word	302976276
.word	302976277
.word	286133268
.word	286133269
.word	269355794
.word	269355795
.word	252513042
.word	252513043
.word	235670032
.word	235670033
.word	218827280
.word	218827281
.word	218827022
.word	218827023
.word	201984014
.word	201984015
.word	185206796
.word	185206797
.word	185141260
.word	185141261
.word	168363788
.word	168363789
.word	168298250
.word	168298251
.word	151520778
.word	151520779
.word	134678026
.word	134678027
.word	16843010
.word	16843011

.comm	sde_v_dc,8,4

.comm	sde_nnz_dct,108,4

.comm	sde_tmp_blk,128,4

.comm	sde_res_dct,816,4

.comm	sde_res_di,4,4

.comm	sde_res_index,4,4

.comm	sde_chroma_neighbor,1,1

.comm	sde_luma_neighbor,1,1

.comm	sde_ga_dqp,4,4

.comm	mb_info_sde,1140,4

.comm	mb_info_h264,1140,4
.globl	scan4
.align	2
.type	scan4, @object
.size	scan4, 24
scan4:
.byte	0
.byte	1
.byte	4
.byte	5
.byte	2
.byte	3
.byte	6
.byte	7
.byte	8
.byte	9
.byte	12
.byte	13
.byte	10
.byte	11
.byte	14
.byte	15
.byte	16
.byte	17
.byte	18
.byte	19
.byte	20
.byte	21
.byte	22
.byte	23
.globl	bottom_pos
.align	2
.type	bottom_pos, @object
.size	bottom_pos, 8
bottom_pos:
.byte	36
.byte	37
.byte	38
.byte	39
.byte	17
.byte	18
.byte	41
.byte	42
.globl	right_pos
.align	2
.type	right_pos, @object
.size	right_pos, 8
right_pos:
.byte	15
.byte	23
.byte	31
.byte	39
.byte	10
.byte	18
.byte	34
.byte	42
.globl	scan4_to_bits
.align	2
.type	scan4_to_bits, @object
.size	scan4_to_bits, 16
scan4_to_bits:
.byte	4
.byte	5
.byte	6
.byte	7
.byte	1
.byte	2
.byte	25
.byte	26
.byte	11
.byte	19
.byte	27
.byte	35
.byte	8
.byte	16
.byte	32
.byte	40
.globl	cache_to_bits
.align	2
.type	cache_to_bits, @object
.size	cache_to_bits, 16
cache_to_bits:
.byte	4
.byte	5
.byte	6
.byte	7
.byte	1
.byte	2
.byte	25
.byte	26
.byte	11
.byte	19
.byte	27
.byte	35
.byte	8
.byte	16
.byte	32
.byte	40
.globl	scan4x4
.align	2
.type	scan4x4, @object
.size	scan4x4, 24
scan4x4:
.byte	0
.byte	1
.byte	4
.byte	5
.byte	2
.byte	3
.byte	6
.byte	7
.byte	8
.byte	9
.byte	12
.byte	13
.byte	10
.byte	11
.byte	14
.byte	15
.byte	16
.byte	17
.byte	18
.byte	19
.byte	20
.byte	21
.byte	22
.byte	23
.globl	y_dc_table
.rdata
.align	2
.type	y_dc_table, @object
.size	y_dc_table, 64
y_dc_table:
.word	0
.word	1
.word	4
.word	5
.word	2
.word	3
.word	6
.word	7
.word	8
.word	9
.word	12
.word	13
.word	10
.word	11
.word	14
.word	15

.comm	vdd,4,4

.comm	udd,4,4

.comm	ydd,4,4

.comm	tlb_i,4,4

.comm	tcsm1_word_bank_backup,24,4

.comm	vpu_base,4,4
.align	2
.type	dequant8_coeff_init, @object
.size	dequant8_coeff_init, 36
dequant8_coeff_init:
.byte	20
.byte	18
.byte	32
.byte	19
.byte	25
.byte	24
.byte	22
.byte	19
.byte	35
.byte	21
.byte	28
.byte	26
.byte	26
.byte	23
.byte	42
.byte	24
.byte	33
.byte	31
.byte	28
.byte	25
.byte	45
.byte	26
.byte	35
.byte	33
.byte	32
.byte	28
.byte	51
.byte	30
.byte	40
.byte	38
.byte	36
.byte	32
.byte	58
.byte	34
.byte	46
.byte	43
.align	2
.type	dequant8_coeff_init_scan, @object
.size	dequant8_coeff_init_scan, 16
dequant8_coeff_init_scan:
.byte	0
.byte	3
.byte	4
.byte	3
.byte	3
.byte	1
.byte	5
.byte	1
.byte	4
.byte	5
.byte	2
.byte	5
.byte	3
.byte	1
.byte	5
.byte	1
.align	2
.type	dequant4_coeff_init, @object
.size	dequant4_coeff_init, 18
dequant4_coeff_init:
.byte	10
.byte	13
.byte	16
.byte	11
.byte	14
.byte	18
.byte	13
.byte	16
.byte	20
.byte	14
.byte	18
.byte	23
.byte	16
.byte	20
.byte	25
.byte	18
.byte	23
.byte	29
.align	2
.type	field_scan8x8_cavlc, @object
.size	field_scan8x8_cavlc, 64
field_scan8x8_cavlc:
.byte	0
.byte	9
.byte	2
.byte	56
.byte	18
.byte	26
.byte	34
.byte	27
.byte	35
.byte	28
.byte	36
.byte	29
.byte	45
.byte	7
.byte	54
.byte	39
.byte	8
.byte	24
.byte	25
.byte	33
.byte	41
.byte	11
.byte	42
.byte	12
.byte	43
.byte	13
.byte	44
.byte	14
.byte	53
.byte	15
.byte	62
.byte	47
.byte	16
.byte	32
.byte	40
.byte	10
.byte	49
.byte	4
.byte	50
.byte	5
.byte	51
.byte	6
.byte	52
.byte	22
.byte	61
.byte	38
.byte	23
.byte	55
.byte	1
.byte	17
.byte	48
.byte	3
.byte	57
.byte	19
.byte	58
.byte	20
.byte	59
.byte	21
.byte	60
.byte	37
.byte	30
.byte	46
.byte	31
.byte	63
.align	2
.type	field_scan8x8, @object
.size	field_scan8x8, 64
field_scan8x8:
.byte	0
.byte	8
.byte	16
.byte	1
.byte	9
.byte	24
.byte	32
.byte	17
.byte	2
.byte	25
.byte	40
.byte	48
.byte	56
.byte	33
.byte	10
.byte	3
.byte	18
.byte	41
.byte	49
.byte	57
.byte	26
.byte	11
.byte	4
.byte	19
.byte	34
.byte	42
.byte	50
.byte	58
.byte	27
.byte	12
.byte	5
.byte	20
.byte	35
.byte	43
.byte	51
.byte	59
.byte	28
.byte	13
.byte	6
.byte	21
.byte	36
.byte	44
.byte	52
.byte	60
.byte	29
.byte	14
.byte	22
.byte	37
.byte	45
.byte	53
.byte	61
.byte	30
.byte	7
.byte	15
.byte	38
.byte	46
.byte	54
.byte	62
.byte	23
.byte	31
.byte	39
.byte	47
.byte	55
.byte	63
.align	2
.type	zigzag_scan8x8_cavlc, @object
.size	zigzag_scan8x8_cavlc, 64
zigzag_scan8x8_cavlc:
.byte	0
.byte	9
.byte	17
.byte	18
.byte	12
.byte	40
.byte	27
.byte	7
.byte	35
.byte	57
.byte	29
.byte	30
.byte	58
.byte	38
.byte	53
.byte	47
.byte	1
.byte	2
.byte	24
.byte	11
.byte	19
.byte	48
.byte	20
.byte	14
.byte	42
.byte	50
.byte	22
.byte	37
.byte	59
.byte	31
.byte	60
.byte	55
.byte	8
.byte	3
.byte	32
.byte	4
.byte	26
.byte	41
.byte	13
.byte	21
.byte	49
.byte	43
.byte	15
.byte	44
.byte	52
.byte	39
.byte	61
.byte	62
.byte	16
.byte	10
.byte	25
.byte	5
.byte	33
.byte	34
.byte	6
.byte	28
.byte	56
.byte	36
.byte	23
.byte	51
.byte	45
.byte	46
.byte	54
.byte	63
.align	2
.type	field_scan, @object
.size	field_scan, 16
field_scan:
.byte	0
.byte	4
.byte	1
.byte	8
.byte	12
.byte	5
.byte	9
.byte	13
.byte	2
.byte	6
.byte	10
.byte	14
.byte	3
.byte	7
.byte	11
.byte	15
.align	2
.type	zigzag_scan, @object
.size	zigzag_scan, 16
zigzag_scan:
.byte	0
.byte	1
.byte	4
.byte	8
.byte	5
.byte	2
.byte	3
.byte	6
.byte	9
.byte	12
.byte	13
.byte	10
.byte	7
.byte	11
.byte	14
.byte	15
.align	2
.type	golomb_to_pict_type, @object
.size	golomb_to_pict_type, 5
golomb_to_pict_type:
.byte	2
.byte	3
.byte	1
.byte	6
.byte	5
.align	2
.type	scan8, @object
.size	scan8, 24
scan8:
.byte	12
.byte	13
.byte	20
.byte	21
.byte	14
.byte	15
.byte	22
.byte	23
.byte	28
.byte	29
.byte	36
.byte	37
.byte	30
.byte	31
.byte	38
.byte	39
.byte	9
.byte	10
.byte	17
.byte	18
.byte	33
.byte	34
.byte	41
.byte	42

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
