.file	1 "h264dsp.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.weight_h264_pixels16x16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels16x16_c
.type	weight_h264_pixels16x16_c, @function
weight_h264_pixels16x16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,16($sp)
beq	$6,$0,$L2
sll	$2,$2,$6

addiu	$8,$6,-1
li	$3,1			# 0x1
sll	$3,$3,$8
addu	$2,$2,$3
$L2:
li	$10,16			# 0x10
b	$L35
li	$3,-256			# 0xffffffffffffff00

$L42:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,1($4)
sb	$9,0($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L58
andi	$9,$8,0x00ff

$L43:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,2($4)
sb	$9,1($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L59
andi	$9,$8,0x00ff

$L44:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,3($4)
sb	$9,2($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L60
andi	$9,$8,0x00ff

$L45:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,4($4)
sb	$9,3($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L61
andi	$9,$8,0x00ff

$L46:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,5($4)
sb	$9,4($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L62
andi	$9,$8,0x00ff

$L47:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,6($4)
sb	$9,5($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L63
andi	$9,$8,0x00ff

$L48:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,7($4)
sb	$9,6($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L64
andi	$9,$8,0x00ff

$L49:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,8($4)
sb	$9,7($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L65
andi	$9,$8,0x00ff

$L50:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,9($4)
sb	$9,8($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L66
andi	$9,$8,0x00ff

$L51:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,10($4)
sb	$9,9($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L67
andi	$9,$8,0x00ff

$L52:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,11($4)
sb	$9,10($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L68
andi	$9,$8,0x00ff

$L53:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,12($4)
sb	$9,11($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L69
andi	$9,$8,0x00ff

$L54:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,13($4)
sb	$9,12($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L70
andi	$9,$8,0x00ff

$L55:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,14($4)
sb	$9,13($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L71
andi	$9,$8,0x00ff

$L56:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,15($4)
sb	$9,14($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L33
nop

$L57:
subu	$8,$0,$8
addiu	$10,$10,-1
sra	$8,$8,31
andi	$8,$8,0x00ff
sb	$8,15($4)
beq	$10,$0,$L72
addu	$4,$4,$5

$L35:
lbu	$8,0($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L42
andi	$9,$8,0x00ff

lbu	$8,1($4)
sb	$9,0($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L43
andi	$9,$8,0x00ff

$L58:
lbu	$8,2($4)
sb	$9,1($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L44
andi	$9,$8,0x00ff

$L59:
lbu	$8,3($4)
sb	$9,2($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L45
andi	$9,$8,0x00ff

$L60:
lbu	$8,4($4)
sb	$9,3($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L46
andi	$9,$8,0x00ff

$L61:
lbu	$8,5($4)
sb	$9,4($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L47
andi	$9,$8,0x00ff

$L62:
lbu	$8,6($4)
sb	$9,5($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L48
andi	$9,$8,0x00ff

$L63:
lbu	$8,7($4)
sb	$9,6($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L49
andi	$9,$8,0x00ff

$L64:
lbu	$8,8($4)
sb	$9,7($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L50
andi	$9,$8,0x00ff

$L65:
lbu	$8,9($4)
sb	$9,8($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L51
andi	$9,$8,0x00ff

$L66:
lbu	$8,10($4)
sb	$9,9($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L52
andi	$9,$8,0x00ff

$L67:
lbu	$8,11($4)
sb	$9,10($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L53
andi	$9,$8,0x00ff

$L68:
lbu	$8,12($4)
sb	$9,11($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L54
andi	$9,$8,0x00ff

$L69:
lbu	$8,13($4)
sb	$9,12($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L55
andi	$9,$8,0x00ff

$L70:
lbu	$8,14($4)
sb	$9,13($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L56
andi	$9,$8,0x00ff

$L71:
lbu	$8,15($4)
sb	$9,14($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L57
nop

$L33:
andi	$8,$8,0x00ff
addiu	$10,$10,-1
sb	$8,15($4)
bne	$10,$0,$L35
addu	$4,$4,$5

$L72:
j	$31
nop

.set	macro
.set	reorder
.end	weight_h264_pixels16x16_c
.size	weight_h264_pixels16x16_c, .-weight_h264_pixels16x16_c
.section	.text.biweight_h264_pixels16x16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels16x16_c
.type	biweight_h264_pixels16x16_c, @function
biweight_h264_pixels16x16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$10,24($sp)
addiu	$8,$7,1
lw	$9,16($sp)
li	$12,16			# 0x10
lw	$3,20($sp)
li	$2,-256			# 0xffffffffffffff00
addiu	$10,$10,1
ori	$10,$10,0x1
b	$L106
sll	$7,$10,$7

$L109:
subu	$10,$0,$10
lbu	$13,1($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,0($4)
lbu	$11,1($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L76
nop

$L110:
subu	$10,$0,$10
lbu	$13,2($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,1($4)
lbu	$11,2($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L78
nop

$L111:
subu	$10,$0,$10
lbu	$13,3($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,2($4)
lbu	$11,3($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L80
nop

$L112:
subu	$10,$0,$10
lbu	$13,4($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,3($4)
lbu	$11,4($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L82
nop

$L113:
subu	$10,$0,$10
lbu	$13,5($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,4($4)
lbu	$11,5($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L84
nop

$L114:
subu	$10,$0,$10
lbu	$13,6($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,5($4)
lbu	$11,6($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L86
nop

$L115:
subu	$10,$0,$10
lbu	$13,7($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,6($4)
lbu	$11,7($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L88
nop

$L116:
subu	$10,$0,$10
lbu	$13,8($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,7($4)
lbu	$11,8($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L90
nop

$L117:
subu	$10,$0,$10
lbu	$13,9($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,8($4)
lbu	$11,9($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L92
nop

$L118:
subu	$10,$0,$10
lbu	$13,10($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,9($4)
lbu	$11,10($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L94
nop

$L119:
subu	$10,$0,$10
lbu	$13,11($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,10($4)
lbu	$11,11($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L96
nop

$L120:
subu	$10,$0,$10
lbu	$13,12($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,11($4)
lbu	$11,12($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L98
nop

$L121:
subu	$10,$0,$10
lbu	$13,13($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,12($4)
lbu	$11,13($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L100
nop

$L122:
subu	$10,$0,$10
lbu	$13,14($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,13($4)
lbu	$11,14($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L102
nop

$L123:
subu	$10,$0,$10
lbu	$13,15($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,14($4)
lbu	$11,15($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L104
nop

$L124:
subu	$10,$0,$10
addiu	$12,$12,-1
sra	$10,$10,31
addu	$5,$5,$6
andi	$10,$10,0x00ff
sb	$10,15($4)
beq	$12,$0,$L125
addu	$4,$4,$6

$L106:
lbu	$10,0($4)
lbu	$11,0($5)
mult	$10,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L109
nop

andi	$10,$10,0x00ff
lbu	$13,1($4)
sb	$10,0($4)
lbu	$11,1($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L110
nop

$L76:
andi	$10,$10,0x00ff
lbu	$13,2($4)
sb	$10,1($4)
lbu	$11,2($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L111
nop

$L78:
andi	$10,$10,0x00ff
lbu	$13,3($4)
sb	$10,2($4)
lbu	$11,3($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L112
nop

$L80:
andi	$10,$10,0x00ff
lbu	$13,4($4)
sb	$10,3($4)
lbu	$11,4($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L113
nop

$L82:
andi	$10,$10,0x00ff
lbu	$13,5($4)
sb	$10,4($4)
lbu	$11,5($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L114
nop

$L84:
andi	$10,$10,0x00ff
lbu	$13,6($4)
sb	$10,5($4)
lbu	$11,6($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L115
nop

$L86:
andi	$10,$10,0x00ff
lbu	$13,7($4)
sb	$10,6($4)
lbu	$11,7($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L116
nop

$L88:
andi	$10,$10,0x00ff
lbu	$13,8($4)
sb	$10,7($4)
lbu	$11,8($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L117
nop

$L90:
andi	$10,$10,0x00ff
lbu	$13,9($4)
sb	$10,8($4)
lbu	$11,9($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L118
nop

$L92:
andi	$10,$10,0x00ff
lbu	$13,10($4)
sb	$10,9($4)
lbu	$11,10($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L119
nop

$L94:
andi	$10,$10,0x00ff
lbu	$13,11($4)
sb	$10,10($4)
lbu	$11,11($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L120
nop

$L96:
andi	$10,$10,0x00ff
lbu	$13,12($4)
sb	$10,11($4)
lbu	$11,12($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L121
nop

$L98:
andi	$10,$10,0x00ff
lbu	$13,13($4)
sb	$10,12($4)
lbu	$11,13($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L122
nop

$L100:
andi	$10,$10,0x00ff
lbu	$13,14($4)
sb	$10,13($4)
lbu	$11,14($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L123
nop

$L102:
andi	$10,$10,0x00ff
lbu	$13,15($4)
sb	$10,14($4)
lbu	$11,15($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L124
nop

$L104:
andi	$10,$10,0x00ff
addiu	$12,$12,-1
sb	$10,15($4)
addu	$5,$5,$6
bne	$12,$0,$L106
addu	$4,$4,$6

$L125:
j	$31
nop

.set	macro
.set	reorder
.end	biweight_h264_pixels16x16_c
.size	biweight_h264_pixels16x16_c, .-biweight_h264_pixels16x16_c
.section	.text.weight_h264_pixels16x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels16x8_c
.type	weight_h264_pixels16x8_c, @function
weight_h264_pixels16x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,16($sp)
beq	$6,$0,$L127
sll	$2,$2,$6

addiu	$8,$6,-1
li	$3,1			# 0x1
sll	$3,$3,$8
addu	$2,$2,$3
$L127:
li	$10,8			# 0x8
b	$L160
li	$3,-256			# 0xffffffffffffff00

$L166:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,1($4)
sb	$9,0($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L182
andi	$9,$8,0x00ff

$L167:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,2($4)
sb	$9,1($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L183
andi	$9,$8,0x00ff

$L168:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,3($4)
sb	$9,2($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L184
andi	$9,$8,0x00ff

$L169:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,4($4)
sb	$9,3($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L185
andi	$9,$8,0x00ff

$L170:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,5($4)
sb	$9,4($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L186
andi	$9,$8,0x00ff

$L171:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,6($4)
sb	$9,5($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L187
andi	$9,$8,0x00ff

$L172:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,7($4)
sb	$9,6($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L188
andi	$9,$8,0x00ff

$L173:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,8($4)
sb	$9,7($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L189
andi	$9,$8,0x00ff

$L174:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,9($4)
sb	$9,8($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L190
andi	$9,$8,0x00ff

$L175:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,10($4)
sb	$9,9($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L191
andi	$9,$8,0x00ff

$L176:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,11($4)
sb	$9,10($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L192
andi	$9,$8,0x00ff

$L177:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,12($4)
sb	$9,11($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L193
andi	$9,$8,0x00ff

$L178:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,13($4)
sb	$9,12($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L194
andi	$9,$8,0x00ff

$L179:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,14($4)
sb	$9,13($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L195
andi	$9,$8,0x00ff

$L180:
subu	$8,$0,$8
sra	$8,$8,31
andi	$9,$8,0x00ff
lbu	$8,15($4)
sb	$9,14($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
beq	$9,$0,$L158
nop

$L181:
subu	$8,$0,$8
addiu	$10,$10,-1
sra	$8,$8,31
andi	$8,$8,0x00ff
sb	$8,15($4)
beq	$10,$0,$L196
addu	$4,$4,$5

$L160:
lbu	$8,0($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L166
andi	$9,$8,0x00ff

lbu	$8,1($4)
sb	$9,0($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L167
andi	$9,$8,0x00ff

$L182:
lbu	$8,2($4)
sb	$9,1($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L168
andi	$9,$8,0x00ff

$L183:
lbu	$8,3($4)
sb	$9,2($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L169
andi	$9,$8,0x00ff

$L184:
lbu	$8,4($4)
sb	$9,3($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L170
andi	$9,$8,0x00ff

$L185:
lbu	$8,5($4)
sb	$9,4($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L171
andi	$9,$8,0x00ff

$L186:
lbu	$8,6($4)
sb	$9,5($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L172
andi	$9,$8,0x00ff

$L187:
lbu	$8,7($4)
sb	$9,6($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L173
andi	$9,$8,0x00ff

$L188:
lbu	$8,8($4)
sb	$9,7($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L174
andi	$9,$8,0x00ff

$L189:
lbu	$8,9($4)
sb	$9,8($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L175
andi	$9,$8,0x00ff

$L190:
lbu	$8,10($4)
sb	$9,9($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L176
andi	$9,$8,0x00ff

$L191:
lbu	$8,11($4)
sb	$9,10($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L177
andi	$9,$8,0x00ff

$L192:
lbu	$8,12($4)
sb	$9,11($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L178
andi	$9,$8,0x00ff

$L193:
lbu	$8,13($4)
sb	$9,12($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L179
andi	$9,$8,0x00ff

$L194:
lbu	$8,14($4)
sb	$9,13($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L180
andi	$9,$8,0x00ff

$L195:
lbu	$8,15($4)
sb	$9,14($4)
mul	$9,$8,$7
addu	$8,$9,$2
sra	$8,$8,$6
and	$9,$8,$3
bne	$9,$0,$L181
nop

$L158:
andi	$8,$8,0x00ff
addiu	$10,$10,-1
sb	$8,15($4)
bne	$10,$0,$L160
addu	$4,$4,$5

$L196:
j	$31
nop

.set	macro
.set	reorder
.end	weight_h264_pixels16x8_c
.size	weight_h264_pixels16x8_c, .-weight_h264_pixels16x8_c
.section	.text.biweight_h264_pixels16x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels16x8_c
.type	biweight_h264_pixels16x8_c, @function
biweight_h264_pixels16x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$10,24($sp)
addiu	$8,$7,1
lw	$9,16($sp)
li	$12,8			# 0x8
lw	$3,20($sp)
li	$2,-256			# 0xffffffffffffff00
addiu	$10,$10,1
ori	$10,$10,0x1
b	$L230
sll	$7,$10,$7

$L233:
subu	$10,$0,$10
lbu	$13,1($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,0($4)
lbu	$11,1($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L200
nop

$L234:
subu	$10,$0,$10
lbu	$13,2($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,1($4)
lbu	$11,2($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L202
nop

$L235:
subu	$10,$0,$10
lbu	$13,3($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,2($4)
lbu	$11,3($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L204
nop

$L236:
subu	$10,$0,$10
lbu	$13,4($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,3($4)
lbu	$11,4($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L206
nop

$L237:
subu	$10,$0,$10
lbu	$13,5($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,4($4)
lbu	$11,5($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L208
nop

$L238:
subu	$10,$0,$10
lbu	$13,6($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,5($4)
lbu	$11,6($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L210
nop

$L239:
subu	$10,$0,$10
lbu	$13,7($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,6($4)
lbu	$11,7($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L212
nop

$L240:
subu	$10,$0,$10
lbu	$13,8($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,7($4)
lbu	$11,8($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L214
nop

$L241:
subu	$10,$0,$10
lbu	$13,9($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,8($4)
lbu	$11,9($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L216
nop

$L242:
subu	$10,$0,$10
lbu	$13,10($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,9($4)
lbu	$11,10($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L218
nop

$L243:
subu	$10,$0,$10
lbu	$13,11($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,10($4)
lbu	$11,11($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L220
nop

$L244:
subu	$10,$0,$10
lbu	$13,12($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,11($4)
lbu	$11,12($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L222
nop

$L245:
subu	$10,$0,$10
lbu	$13,13($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,12($4)
lbu	$11,13($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L224
nop

$L246:
subu	$10,$0,$10
lbu	$13,14($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,13($4)
lbu	$11,14($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L226
nop

$L247:
subu	$10,$0,$10
lbu	$13,15($4)
sra	$10,$10,31
mult	$13,$9
andi	$10,$10,0x00ff
sb	$10,14($4)
lbu	$11,15($5)
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
beq	$11,$0,$L228
nop

$L248:
subu	$10,$0,$10
addiu	$12,$12,-1
sra	$10,$10,31
addu	$5,$5,$6
andi	$10,$10,0x00ff
sb	$10,15($4)
beq	$12,$0,$L249
addu	$4,$4,$6

$L230:
lbu	$10,0($4)
lbu	$11,0($5)
mult	$10,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L233
nop

andi	$10,$10,0x00ff
lbu	$13,1($4)
sb	$10,0($4)
lbu	$11,1($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L234
nop

$L200:
andi	$10,$10,0x00ff
lbu	$13,2($4)
sb	$10,1($4)
lbu	$11,2($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L235
nop

$L202:
andi	$10,$10,0x00ff
lbu	$13,3($4)
sb	$10,2($4)
lbu	$11,3($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L236
nop

$L204:
andi	$10,$10,0x00ff
lbu	$13,4($4)
sb	$10,3($4)
lbu	$11,4($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L237
nop

$L206:
andi	$10,$10,0x00ff
lbu	$13,5($4)
sb	$10,4($4)
lbu	$11,5($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L238
nop

$L208:
andi	$10,$10,0x00ff
lbu	$13,6($4)
sb	$10,5($4)
lbu	$11,6($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L239
nop

$L210:
andi	$10,$10,0x00ff
lbu	$13,7($4)
sb	$10,6($4)
lbu	$11,7($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L240
nop

$L212:
andi	$10,$10,0x00ff
lbu	$13,8($4)
sb	$10,7($4)
lbu	$11,8($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L241
nop

$L214:
andi	$10,$10,0x00ff
lbu	$13,9($4)
sb	$10,8($4)
lbu	$11,9($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L242
nop

$L216:
andi	$10,$10,0x00ff
lbu	$13,10($4)
sb	$10,9($4)
lbu	$11,10($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L243
nop

$L218:
andi	$10,$10,0x00ff
lbu	$13,11($4)
sb	$10,10($4)
lbu	$11,11($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L244
nop

$L220:
andi	$10,$10,0x00ff
lbu	$13,12($4)
sb	$10,11($4)
lbu	$11,12($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L245
nop

$L222:
andi	$10,$10,0x00ff
lbu	$13,13($4)
sb	$10,12($4)
lbu	$11,13($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L246
nop

$L224:
andi	$10,$10,0x00ff
lbu	$13,14($4)
sb	$10,13($4)
lbu	$11,14($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L247
nop

$L226:
andi	$10,$10,0x00ff
lbu	$13,15($4)
sb	$10,14($4)
lbu	$11,15($5)
mult	$13,$9
madd	$11,$3
mflo	$10
addu	$10,$10,$7
sra	$10,$10,$8
and	$11,$10,$2
bne	$11,$0,$L248
nop

$L228:
andi	$10,$10,0x00ff
addiu	$12,$12,-1
sb	$10,15($4)
addu	$5,$5,$6
bne	$12,$0,$L230
addu	$4,$4,$6

$L249:
j	$31
nop

.set	macro
.set	reorder
.end	biweight_h264_pixels16x8_c
.size	biweight_h264_pixels16x8_c, .-biweight_h264_pixels16x8_c
.section	.text.weight_h264_pixels8x16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels8x16_c
.type	weight_h264_pixels8x16_c, @function
weight_h264_pixels8x16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L251
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L251:
li	$10,16			# 0x10
b	$L268
li	$9,-256			# 0xffffffffffffff00

$L274:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L282
andi	$3,$2,0x00ff

$L275:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L283
andi	$3,$2,0x00ff

$L276:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L284
andi	$3,$2,0x00ff

$L277:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,4($4)
sb	$3,3($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L285
andi	$3,$2,0x00ff

$L278:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,5($4)
sb	$3,4($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L286
andi	$3,$2,0x00ff

$L279:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,6($4)
sb	$3,5($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L287
andi	$3,$2,0x00ff

$L280:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,7($4)
sb	$3,6($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L266
nop

$L281:
subu	$2,$0,$2
addiu	$10,$10,-1
sra	$2,$2,31
andi	$2,$2,0x00ff
sb	$2,7($4)
beq	$10,$0,$L288
addu	$4,$4,$5

$L268:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L274
andi	$3,$2,0x00ff

lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L275
andi	$3,$2,0x00ff

$L282:
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L276
andi	$3,$2,0x00ff

$L283:
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L277
andi	$3,$2,0x00ff

$L284:
lbu	$2,4($4)
sb	$3,3($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L278
andi	$3,$2,0x00ff

$L285:
lbu	$2,5($4)
sb	$3,4($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L279
andi	$3,$2,0x00ff

$L286:
lbu	$2,6($4)
sb	$3,5($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L280
andi	$3,$2,0x00ff

$L287:
lbu	$2,7($4)
sb	$3,6($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L281
nop

$L266:
andi	$2,$2,0x00ff
addiu	$10,$10,-1
sb	$2,7($4)
bne	$10,$0,$L268
addu	$4,$4,$5

$L288:
j	$31
nop

.set	macro
.set	reorder
.end	weight_h264_pixels8x16_c
.size	weight_h264_pixels8x16_c, .-weight_h264_pixels8x16_c
.section	.text.biweight_h264_pixels8x16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels8x16_c
.type	biweight_h264_pixels8x16_c, @function
biweight_h264_pixels8x16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,24($sp)
addiu	$10,$7,1
lw	$11,16($sp)
li	$12,16			# 0x10
lw	$9,20($sp)
li	$8,-256			# 0xffffffffffffff00
addiu	$2,$2,1
ori	$2,$2,0x1
b	$L306
sll	$7,$2,$7

$L309:
subu	$2,$0,$2
lbu	$13,1($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,0($4)
lbu	$3,1($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L292
nop

$L310:
subu	$2,$0,$2
lbu	$13,2($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,1($4)
lbu	$3,2($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L294
nop

$L311:
subu	$2,$0,$2
lbu	$13,3($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,2($4)
lbu	$3,3($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L296
nop

$L312:
subu	$2,$0,$2
lbu	$13,4($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,3($4)
lbu	$3,4($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L298
nop

$L313:
subu	$2,$0,$2
lbu	$13,5($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,4($4)
lbu	$3,5($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L300
nop

$L314:
subu	$2,$0,$2
lbu	$13,6($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,5($4)
lbu	$3,6($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L302
nop

$L315:
subu	$2,$0,$2
lbu	$13,7($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,6($4)
lbu	$3,7($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L304
nop

$L316:
subu	$2,$0,$2
addiu	$12,$12,-1
sra	$2,$2,31
addu	$5,$5,$6
andi	$2,$2,0x00ff
sb	$2,7($4)
beq	$12,$0,$L317
addu	$4,$4,$6

$L306:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L309
nop

andi	$2,$2,0x00ff
lbu	$13,1($4)
sb	$2,0($4)
lbu	$3,1($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L310
nop

$L292:
andi	$2,$2,0x00ff
lbu	$13,2($4)
sb	$2,1($4)
lbu	$3,2($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L311
nop

$L294:
andi	$2,$2,0x00ff
lbu	$13,3($4)
sb	$2,2($4)
lbu	$3,3($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L312
nop

$L296:
andi	$2,$2,0x00ff
lbu	$13,4($4)
sb	$2,3($4)
lbu	$3,4($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L313
nop

$L298:
andi	$2,$2,0x00ff
lbu	$13,5($4)
sb	$2,4($4)
lbu	$3,5($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L314
nop

$L300:
andi	$2,$2,0x00ff
lbu	$13,6($4)
sb	$2,5($4)
lbu	$3,6($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L315
nop

$L302:
andi	$2,$2,0x00ff
lbu	$13,7($4)
sb	$2,6($4)
lbu	$3,7($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L316
nop

$L304:
andi	$2,$2,0x00ff
addiu	$12,$12,-1
sb	$2,7($4)
addu	$5,$5,$6
bne	$12,$0,$L306
addu	$4,$4,$6

$L317:
j	$31
nop

.set	macro
.set	reorder
.end	biweight_h264_pixels8x16_c
.size	biweight_h264_pixels8x16_c, .-biweight_h264_pixels8x16_c
.section	.text.weight_h264_pixels8x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels8x8_c
.type	weight_h264_pixels8x8_c, @function
weight_h264_pixels8x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L319
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L319:
li	$10,8			# 0x8
b	$L336
li	$9,-256			# 0xffffffffffffff00

$L342:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L350
andi	$3,$2,0x00ff

$L343:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L351
andi	$3,$2,0x00ff

$L344:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L352
andi	$3,$2,0x00ff

$L345:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,4($4)
sb	$3,3($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L353
andi	$3,$2,0x00ff

$L346:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,5($4)
sb	$3,4($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L354
andi	$3,$2,0x00ff

$L347:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,6($4)
sb	$3,5($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L355
andi	$3,$2,0x00ff

$L348:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,7($4)
sb	$3,6($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L334
nop

$L349:
subu	$2,$0,$2
addiu	$10,$10,-1
sra	$2,$2,31
andi	$2,$2,0x00ff
sb	$2,7($4)
beq	$10,$0,$L356
addu	$4,$4,$5

$L336:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L342
andi	$3,$2,0x00ff

lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L343
andi	$3,$2,0x00ff

$L350:
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L344
andi	$3,$2,0x00ff

$L351:
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L345
andi	$3,$2,0x00ff

$L352:
lbu	$2,4($4)
sb	$3,3($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L346
andi	$3,$2,0x00ff

$L353:
lbu	$2,5($4)
sb	$3,4($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L347
andi	$3,$2,0x00ff

$L354:
lbu	$2,6($4)
sb	$3,5($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L348
andi	$3,$2,0x00ff

$L355:
lbu	$2,7($4)
sb	$3,6($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L349
nop

$L334:
andi	$2,$2,0x00ff
addiu	$10,$10,-1
sb	$2,7($4)
bne	$10,$0,$L336
addu	$4,$4,$5

$L356:
j	$31
nop

.set	macro
.set	reorder
.end	weight_h264_pixels8x8_c
.size	weight_h264_pixels8x8_c, .-weight_h264_pixels8x8_c
.section	.text.biweight_h264_pixels8x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels8x8_c
.type	biweight_h264_pixels8x8_c, @function
biweight_h264_pixels8x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,24($sp)
addiu	$10,$7,1
lw	$11,16($sp)
li	$12,8			# 0x8
lw	$9,20($sp)
li	$8,-256			# 0xffffffffffffff00
addiu	$2,$2,1
ori	$2,$2,0x1
b	$L374
sll	$7,$2,$7

$L377:
subu	$2,$0,$2
lbu	$13,1($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,0($4)
lbu	$3,1($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L360
nop

$L378:
subu	$2,$0,$2
lbu	$13,2($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,1($4)
lbu	$3,2($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L362
nop

$L379:
subu	$2,$0,$2
lbu	$13,3($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,2($4)
lbu	$3,3($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L364
nop

$L380:
subu	$2,$0,$2
lbu	$13,4($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,3($4)
lbu	$3,4($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L366
nop

$L381:
subu	$2,$0,$2
lbu	$13,5($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,4($4)
lbu	$3,5($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L368
nop

$L382:
subu	$2,$0,$2
lbu	$13,6($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,5($4)
lbu	$3,6($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L370
nop

$L383:
subu	$2,$0,$2
lbu	$13,7($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,6($4)
lbu	$3,7($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L372
nop

$L384:
subu	$2,$0,$2
addiu	$12,$12,-1
sra	$2,$2,31
addu	$5,$5,$6
andi	$2,$2,0x00ff
sb	$2,7($4)
beq	$12,$0,$L385
addu	$4,$4,$6

$L374:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L377
nop

andi	$2,$2,0x00ff
lbu	$13,1($4)
sb	$2,0($4)
lbu	$3,1($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L378
nop

$L360:
andi	$2,$2,0x00ff
lbu	$13,2($4)
sb	$2,1($4)
lbu	$3,2($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L379
nop

$L362:
andi	$2,$2,0x00ff
lbu	$13,3($4)
sb	$2,2($4)
lbu	$3,3($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L380
nop

$L364:
andi	$2,$2,0x00ff
lbu	$13,4($4)
sb	$2,3($4)
lbu	$3,4($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L381
nop

$L366:
andi	$2,$2,0x00ff
lbu	$13,5($4)
sb	$2,4($4)
lbu	$3,5($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L382
nop

$L368:
andi	$2,$2,0x00ff
lbu	$13,6($4)
sb	$2,5($4)
lbu	$3,6($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L383
nop

$L370:
andi	$2,$2,0x00ff
lbu	$13,7($4)
sb	$2,6($4)
lbu	$3,7($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L384
nop

$L372:
andi	$2,$2,0x00ff
addiu	$12,$12,-1
sb	$2,7($4)
addu	$5,$5,$6
bne	$12,$0,$L374
addu	$4,$4,$6

$L385:
j	$31
nop

.set	macro
.set	reorder
.end	biweight_h264_pixels8x8_c
.size	biweight_h264_pixels8x8_c, .-biweight_h264_pixels8x8_c
.section	.text.weight_h264_pixels8x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels8x4_c
.type	weight_h264_pixels8x4_c, @function
weight_h264_pixels8x4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L387
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L387:
li	$10,4			# 0x4
li	$9,-256			# 0xffffffffffffff00
$L404:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L389
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L389:
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L391
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L391:
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L393
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L393:
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L395
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L395:
lbu	$2,4($4)
sb	$3,3($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L397
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L397:
lbu	$2,5($4)
sb	$3,4($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L399
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L399:
lbu	$2,6($4)
sb	$3,5($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L401
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L401:
lbu	$2,7($4)
sb	$3,6($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L402
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L403:
addiu	$10,$10,-1
sb	$2,7($4)
bne	$10,$0,$L404
addu	$4,$4,$5

j	$31
nop

$L402:
b	$L403
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	weight_h264_pixels8x4_c
.size	weight_h264_pixels8x4_c, .-weight_h264_pixels8x4_c
.section	.text.biweight_h264_pixels8x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels8x4_c
.type	biweight_h264_pixels8x4_c, @function
biweight_h264_pixels8x4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,24($sp)
addiu	$10,$7,1
lw	$11,16($sp)
li	$12,4			# 0x4
lw	$9,20($sp)
li	$8,-256			# 0xffffffffffffff00
addiu	$2,$2,1
ori	$2,$2,0x1
sll	$7,$2,$7
$L426:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L410
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L411:
lbu	$13,1($4)
sb	$2,0($4)
lbu	$3,1($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L412
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L413:
lbu	$13,2($4)
sb	$2,1($4)
lbu	$3,2($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L414
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L415:
lbu	$13,3($4)
sb	$2,2($4)
lbu	$3,3($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L416
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L417:
lbu	$13,4($4)
sb	$2,3($4)
lbu	$3,4($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L418
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L419:
lbu	$13,5($4)
sb	$2,4($4)
lbu	$3,5($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L420
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L421:
lbu	$13,6($4)
sb	$2,5($4)
lbu	$3,6($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L422
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L423:
lbu	$13,7($4)
sb	$2,6($4)
lbu	$3,7($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L424
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L425:
addiu	$12,$12,-1
sb	$2,7($4)
addu	$5,$5,$6
bne	$12,$0,$L426
addu	$4,$4,$6

j	$31
nop

$L424:
b	$L425
andi	$2,$2,0x00ff

$L422:
b	$L423
andi	$2,$2,0x00ff

$L420:
b	$L421
andi	$2,$2,0x00ff

$L418:
b	$L419
andi	$2,$2,0x00ff

$L416:
b	$L417
andi	$2,$2,0x00ff

$L414:
b	$L415
andi	$2,$2,0x00ff

$L412:
b	$L413
andi	$2,$2,0x00ff

$L410:
b	$L411
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	biweight_h264_pixels8x4_c
.size	biweight_h264_pixels8x4_c, .-biweight_h264_pixels8x4_c
.section	.text.weight_h264_pixels4x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels4x8_c
.type	weight_h264_pixels4x8_c, @function
weight_h264_pixels4x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L429
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L429:
li	$10,8			# 0x8
b	$L438
li	$9,-256			# 0xffffffffffffff00

$L444:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L448
andi	$3,$2,0x00ff

$L445:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L449
andi	$3,$2,0x00ff

$L446:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L436
nop

$L447:
subu	$2,$0,$2
addiu	$10,$10,-1
sra	$2,$2,31
andi	$2,$2,0x00ff
sb	$2,3($4)
beq	$10,$0,$L450
addu	$4,$4,$5

$L438:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L444
andi	$3,$2,0x00ff

lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L445
andi	$3,$2,0x00ff

$L448:
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L446
andi	$3,$2,0x00ff

$L449:
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L447
nop

$L436:
andi	$2,$2,0x00ff
addiu	$10,$10,-1
sb	$2,3($4)
bne	$10,$0,$L438
addu	$4,$4,$5

$L450:
j	$31
nop

.set	macro
.set	reorder
.end	weight_h264_pixels4x8_c
.size	weight_h264_pixels4x8_c, .-weight_h264_pixels4x8_c
.section	.text.biweight_h264_pixels4x8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels4x8_c
.type	biweight_h264_pixels4x8_c, @function
biweight_h264_pixels4x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,24($sp)
addiu	$10,$7,1
lw	$11,16($sp)
li	$12,8			# 0x8
lw	$9,20($sp)
li	$8,-256			# 0xffffffffffffff00
addiu	$2,$2,1
ori	$2,$2,0x1
b	$L460
sll	$7,$2,$7

$L463:
subu	$2,$0,$2
lbu	$13,1($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,0($4)
lbu	$3,1($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L454
nop

$L464:
subu	$2,$0,$2
lbu	$13,2($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,1($4)
lbu	$3,2($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L456
nop

$L465:
subu	$2,$0,$2
lbu	$13,3($4)
sra	$2,$2,31
mult	$13,$11
andi	$2,$2,0x00ff
sb	$2,2($4)
lbu	$3,3($5)
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L458
nop

$L466:
subu	$2,$0,$2
addiu	$12,$12,-1
sra	$2,$2,31
addu	$5,$5,$6
andi	$2,$2,0x00ff
sb	$2,3($4)
beq	$12,$0,$L467
addu	$4,$4,$6

$L460:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L463
nop

andi	$2,$2,0x00ff
lbu	$13,1($4)
sb	$2,0($4)
lbu	$3,1($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L464
nop

$L454:
andi	$2,$2,0x00ff
lbu	$13,2($4)
sb	$2,1($4)
lbu	$3,2($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L465
nop

$L456:
andi	$2,$2,0x00ff
lbu	$13,3($4)
sb	$2,2($4)
lbu	$3,3($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
bne	$3,$0,$L466
nop

$L458:
andi	$2,$2,0x00ff
addiu	$12,$12,-1
sb	$2,3($4)
addu	$5,$5,$6
bne	$12,$0,$L460
addu	$4,$4,$6

$L467:
j	$31
nop

.set	macro
.set	reorder
.end	biweight_h264_pixels4x8_c
.size	biweight_h264_pixels4x8_c, .-biweight_h264_pixels4x8_c
.section	.text.weight_h264_pixels4x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels4x4_c
.type	weight_h264_pixels4x4_c, @function
weight_h264_pixels4x4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L469
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L469:
li	$10,4			# 0x4
li	$9,-256			# 0xffffffffffffff00
$L478:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L471
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L471:
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L473
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L473:
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L475
andi	$3,$2,0x00ff

subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
$L475:
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L476
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L477:
addiu	$10,$10,-1
sb	$2,3($4)
bne	$10,$0,$L478
addu	$4,$4,$5

j	$31
nop

$L476:
b	$L477
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	weight_h264_pixels4x4_c
.size	weight_h264_pixels4x4_c, .-weight_h264_pixels4x4_c
.section	.text.biweight_h264_pixels4x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels4x4_c
.type	biweight_h264_pixels4x4_c, @function
biweight_h264_pixels4x4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,24($sp)
addiu	$10,$7,1
lw	$11,16($sp)
li	$12,4			# 0x4
lw	$9,20($sp)
li	$8,-256			# 0xffffffffffffff00
addiu	$2,$2,1
ori	$2,$2,0x1
sll	$7,$2,$7
$L492:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L484
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L485:
lbu	$13,1($4)
sb	$2,0($4)
lbu	$3,1($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L486
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L487:
lbu	$13,2($4)
sb	$2,1($4)
lbu	$3,2($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L488
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L489:
lbu	$13,3($4)
sb	$2,2($4)
lbu	$3,3($5)
mult	$13,$11
madd	$3,$9
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$10
and	$3,$2,$8
beq	$3,$0,$L490
nop

subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L491:
addiu	$12,$12,-1
sb	$2,3($4)
addu	$5,$5,$6
bne	$12,$0,$L492
addu	$4,$4,$6

j	$31
nop

$L490:
b	$L491
andi	$2,$2,0x00ff

$L488:
b	$L489
andi	$2,$2,0x00ff

$L486:
b	$L487
andi	$2,$2,0x00ff

$L484:
b	$L485
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	biweight_h264_pixels4x4_c
.size	biweight_h264_pixels4x4_c, .-biweight_h264_pixels4x4_c
.section	.text.weight_h264_pixels4x2_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels4x2_c
.type	weight_h264_pixels4x2_c, @function
weight_h264_pixels4x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L495
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L495:
lbu	$2,0($4)
li	$9,-256			# 0xffffffffffffff00
li	$10,2			# 0x2
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L496
li	$11,1			# 0x1

$L509:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L513
andi	$3,$2,0x00ff

$L510:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L514
andi	$3,$2,0x00ff

$L511:
subu	$2,$0,$2
sra	$2,$2,31
andi	$3,$2,0x00ff
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
beq	$3,$0,$L502
nop

$L512:
subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L503:
sb	$2,3($4)
bne	$10,$11,$L505
addu	$4,$4,$5

j	$31
nop

$L505:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L509
li	$10,1			# 0x1

$L496:
andi	$3,$2,0x00ff
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L510
andi	$3,$2,0x00ff

$L513:
lbu	$2,2($4)
sb	$3,1($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L511
andi	$3,$2,0x00ff

$L514:
lbu	$2,3($4)
sb	$3,2($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
and	$3,$2,$9
bne	$3,$0,$L512
nop

$L502:
b	$L503
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	weight_h264_pixels4x2_c
.size	weight_h264_pixels4x2_c, .-weight_h264_pixels4x2_c
.section	.text.biweight_h264_pixels4x2_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels4x2_c
.type	biweight_h264_pixels4x2_c, @function
biweight_h264_pixels4x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,0($4)
li	$9,-256			# 0xffffffffffffff00
lw	$11,16($sp)
li	$12,2			# 0x2
lbu	$3,0($5)
lw	$10,20($sp)
mult	$2,$11
lw	$8,24($sp)
madd	$3,$10
addiu	$8,$8,1
ori	$8,$8,0x1
mflo	$2
sll	$8,$8,$7
addiu	$7,$7,1
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
beq	$3,$0,$L518
li	$13,1			# 0x1

$L528:
subu	$2,$0,$2
lbu	$14,1($4)
sra	$2,$2,31
mult	$14,$11
andi	$2,$2,0x00ff
sb	$2,0($4)
lbu	$3,1($5)
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
beq	$3,$0,$L520
nop

$L529:
subu	$2,$0,$2
lbu	$14,2($4)
sra	$2,$2,31
mult	$14,$11
andi	$2,$2,0x00ff
sb	$2,1($4)
lbu	$3,2($5)
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
beq	$3,$0,$L522
nop

$L530:
subu	$2,$0,$2
lbu	$14,3($4)
sra	$2,$2,31
mult	$14,$11
andi	$2,$2,0x00ff
sb	$2,2($4)
lbu	$3,3($5)
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
beq	$3,$0,$L524
nop

$L531:
subu	$2,$0,$2
sra	$2,$2,31
andi	$2,$2,0x00ff
$L525:
sb	$2,3($4)
addu	$5,$5,$6
bne	$12,$13,$L527
addu	$4,$4,$6

j	$31
nop

$L527:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$11
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
bne	$3,$0,$L528
li	$12,1			# 0x1

$L518:
andi	$2,$2,0x00ff
lbu	$14,1($4)
sb	$2,0($4)
lbu	$3,1($5)
mult	$14,$11
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
bne	$3,$0,$L529
nop

$L520:
andi	$2,$2,0x00ff
lbu	$14,2($4)
sb	$2,1($4)
lbu	$3,2($5)
mult	$14,$11
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
bne	$3,$0,$L530
nop

$L522:
andi	$2,$2,0x00ff
lbu	$14,3($4)
sb	$2,2($4)
lbu	$3,3($5)
mult	$14,$11
madd	$3,$10
mflo	$2
addu	$2,$2,$8
sra	$2,$2,$7
and	$3,$2,$9
bne	$3,$0,$L531
nop

$L524:
b	$L525
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	biweight_h264_pixels4x2_c
.size	biweight_h264_pixels4x2_c, .-biweight_h264_pixels4x2_c
.section	.text.weight_h264_pixels2x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels2x4_c
.type	weight_h264_pixels2x4_c, @function
weight_h264_pixels2x4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$9,16($sp)
beq	$6,$0,$L535
sll	$9,$9,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$9,$9,$2
$L535:
li	$8,4			# 0x4
li	$10,-256			# 0xffffffffffffff00
$L540:
lbu	$2,0($4)
mul	$3,$2,$7
addu	$2,$3,$9
sra	$2,$2,$6
subu	$3,$0,$2
and	$11,$2,$10
beq	$11,$0,$L536
sra	$3,$3,31

andi	$3,$3,0x00ff
$L537:
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$9
sra	$2,$2,$6
subu	$3,$0,$2
and	$11,$2,$10
beq	$11,$0,$L538
sra	$3,$3,31

andi	$2,$3,0x00ff
$L539:
addiu	$8,$8,-1
sb	$2,1($4)
bne	$8,$0,$L540
addu	$4,$4,$5

j	$31
nop

$L538:
b	$L539
andi	$2,$2,0x00ff

$L536:
b	$L537
andi	$3,$2,0x00ff

.set	macro
.set	reorder
.end	weight_h264_pixels2x4_c
.size	weight_h264_pixels2x4_c, .-weight_h264_pixels2x4_c
.section	.text.biweight_h264_pixels2x4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels2x4_c
.type	biweight_h264_pixels2x4_c, @function
biweight_h264_pixels2x4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,24($sp)
addiu	$12,$7,1
lw	$13,16($sp)
li	$9,4			# 0x4
lw	$11,20($sp)
li	$10,-256			# 0xffffffffffffff00
addiu	$2,$2,1
ori	$2,$2,0x1
sll	$7,$2,$7
$L550:
lbu	$2,0($4)
lbu	$3,0($5)
mult	$2,$13
madd	$3,$11
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$12
subu	$8,$0,$2
and	$3,$2,$10
beq	$3,$0,$L546
sra	$8,$8,31

andi	$8,$8,0x00ff
$L547:
lbu	$2,1($4)
sb	$8,0($4)
lbu	$3,1($5)
mult	$2,$13
madd	$3,$11
mflo	$2
addu	$2,$2,$7
sra	$2,$2,$12
subu	$3,$0,$2
and	$8,$2,$10
beq	$8,$0,$L548
sra	$3,$3,31

andi	$2,$3,0x00ff
$L549:
addiu	$9,$9,-1
sb	$2,1($4)
addu	$5,$5,$6
bne	$9,$0,$L550
addu	$4,$4,$6

j	$31
nop

$L548:
b	$L549
andi	$2,$2,0x00ff

$L546:
b	$L547
andi	$8,$2,0x00ff

.set	macro
.set	reorder
.end	biweight_h264_pixels2x4_c
.size	biweight_h264_pixels2x4_c, .-biweight_h264_pixels2x4_c
.section	.text.weight_h264_pixels2x2_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	weight_h264_pixels2x2_c
.type	weight_h264_pixels2x2_c, @function
weight_h264_pixels2x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
beq	$6,$0,$L553
sll	$8,$8,$6

addiu	$3,$6,-1
li	$2,1			# 0x1
sll	$2,$2,$3
addu	$8,$8,$2
$L553:
lbu	$2,0($4)
li	$9,-256			# 0xffffffffffffff00
li	$10,2			# 0x2
li	$12,1			# 0x1
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
subu	$3,$0,$2
and	$11,$2,$9
beq	$11,$0,$L554
sra	$3,$3,31

$L563:
lbu	$2,1($4)
andi	$3,$3,0x00ff
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
subu	$3,$0,$2
and	$11,$2,$9
beq	$11,$0,$L556
sra	$3,$3,31

$L564:
andi	$2,$3,0x00ff
$L557:
sb	$2,1($4)
bne	$10,$12,$L559
addu	$4,$4,$5

j	$31
nop

$L559:
lbu	$2,0($4)
li	$10,1			# 0x1
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
subu	$3,$0,$2
and	$11,$2,$9
bne	$11,$0,$L563
sra	$3,$3,31

$L554:
andi	$3,$2,0x00ff
lbu	$2,1($4)
sb	$3,0($4)
mul	$3,$2,$7
addu	$2,$3,$8
sra	$2,$2,$6
subu	$3,$0,$2
and	$11,$2,$9
bne	$11,$0,$L564
sra	$3,$3,31

$L556:
b	$L557
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	weight_h264_pixels2x2_c
.size	weight_h264_pixels2x2_c, .-weight_h264_pixels2x2_c
.section	.text.biweight_h264_pixels2x2_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	biweight_h264_pixels2x2_c
.type	biweight_h264_pixels2x2_c, @function
biweight_h264_pixels2x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,0($4)
li	$10,-256			# 0xffffffffffffff00
lw	$12,16($sp)
li	$13,2			# 0x2
lbu	$3,0($5)
li	$14,1			# 0x1
lw	$11,20($sp)
mult	$2,$12
lw	$9,24($sp)
madd	$3,$11
addiu	$9,$9,1
ori	$9,$9,0x1
mflo	$2
sll	$9,$9,$7
addiu	$7,$7,1
addu	$2,$2,$9
sra	$2,$2,$7
subu	$8,$0,$2
and	$3,$2,$10
beq	$3,$0,$L568
sra	$8,$8,31

$L574:
andi	$8,$8,0x00ff
lbu	$2,1($4)
sb	$8,0($4)
lbu	$3,1($5)
mult	$2,$12
madd	$3,$11
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$7
subu	$3,$0,$2
and	$8,$2,$10
beq	$8,$0,$L570
sra	$3,$3,31

$L575:
andi	$2,$3,0x00ff
$L571:
sb	$2,1($4)
addu	$5,$5,$6
bne	$13,$14,$L573
addu	$4,$4,$6

j	$31
nop

$L573:
lbu	$2,0($4)
li	$13,1			# 0x1
lbu	$3,0($5)
mult	$2,$12
madd	$3,$11
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$7
subu	$8,$0,$2
and	$3,$2,$10
bne	$3,$0,$L574
sra	$8,$8,31

$L568:
andi	$8,$2,0x00ff
lbu	$2,1($4)
sb	$8,0($4)
lbu	$3,1($5)
mult	$2,$12
madd	$3,$11
mflo	$2
addu	$2,$2,$9
sra	$2,$2,$7
subu	$3,$0,$2
and	$8,$2,$10
bne	$8,$0,$L575
sra	$3,$3,31

$L570:
b	$L571
andi	$2,$2,0x00ff

.set	macro
.set	reorder
.end	biweight_h264_pixels2x2_c
.size	biweight_h264_pixels2x2_c, .-biweight_h264_pixels2x2_c
.section	.text.h264_v_loop_filter_luma_intra_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_v_loop_filter_luma_intra_c
.type	h264_v_loop_filter_luma_intra_c, @function
h264_v_loop_filter_luma_intra_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-32
sra	$25,$6,2
sw	$16,4($sp)
sll	$16,$5,1
sw	$17,8($sp)
addu	$9,$4,$5
addu	$16,$16,$5
sw	$22,28($sp)
subu	$17,$0,$5
sw	$21,24($sp)
subu	$11,$4,$16
sw	$20,20($sp)
sw	$19,16($sp)
addiu	$25,$25,1
addu	$10,$11,$5
sw	$18,12($sp)
sll	$17,$17,2
addu	$8,$10,$5
addiu	$24,$4,16
b	$L589
addu	$5,$9,$5

$L593:
subu	$3,$0,$2
slt	$18,$2,0
movn	$2,$3,$18
slt	$2,$2,$7
beq	$2,$0,$L584
sll	$2,$15,1

addu	$3,$4,$17
sll	$22,$20,1
lbu	$3,0($3)
addu	$22,$22,$20
addu	$20,$20,$2
sll	$18,$12,1
sll	$2,$3,1
addu	$21,$20,$18
addu	$2,$2,$22
sll	$18,$13,1
subu	$3,$20,$15
addu	$2,$15,$2
addu	$18,$21,$18
addu	$3,$12,$3
addu	$2,$12,$2
addu	$18,$14,$18
addu	$3,$13,$3
addu	$2,$13,$2
addiu	$18,$18,4
addiu	$3,$3,2
addiu	$2,$2,4
sra	$18,$18,3
sra	$3,$3,2
sra	$2,$2,3
sb	$18,0($8)
sb	$3,0($10)
sb	$2,0($11)
$L586:
subu	$2,$19,$13
subu	$3,$0,$2
slt	$18,$2,0
movn	$2,$3,$18
slt	$2,$2,$7
beq	$2,$0,$L587
sll	$2,$14,1

addu	$2,$4,$16
sll	$3,$19,1
sll	$18,$12,1
lbu	$2,0($2)
addu	$3,$3,$19
addu	$15,$15,$18
sll	$18,$13,1
sll	$2,$2,1
addu	$15,$15,$18
addu	$3,$2,$3
sll	$18,$14,1
addu	$2,$12,$13
addu	$3,$14,$3
addu	$15,$15,$18
addu	$3,$13,$3
addu	$2,$14,$2
addu	$3,$12,$3
addu	$13,$19,$15
addu	$2,$19,$2
addiu	$12,$13,4
addiu	$2,$2,2
addiu	$3,$3,4
sra	$12,$12,3
sra	$2,$2,2
sra	$3,$3,3
sb	$12,0($4)
sb	$2,0($9)
sb	$3,0($5)
$L580:
addiu	$4,$4,1
addiu	$11,$11,1
addiu	$10,$10,1
addiu	$8,$8,1
addiu	$9,$9,1
beq	$4,$24,$L592
addiu	$5,$5,1

$L589:
lbu	$12,0($8)
lbu	$13,0($4)
lbu	$20,0($11)
lbu	$15,0($10)
subu	$2,$12,$13
lbu	$14,0($9)
subu	$3,$0,$2
slt	$18,$2,0
movn	$2,$3,$18
slt	$3,$2,$6
beq	$3,$0,$L580
lbu	$19,0($5)

subu	$3,$15,$12
subu	$18,$0,$3
slt	$21,$3,0
movn	$3,$18,$21
slt	$3,$3,$7
beq	$3,$0,$L580
subu	$3,$14,$13

subu	$18,$0,$3
slt	$21,$3,0
movn	$3,$18,$21
slt	$3,$3,$7
beq	$3,$0,$L580
slt	$2,$25,$2

beq	$2,$0,$L593
subu	$2,$20,$12

sll	$3,$15,1
sll	$2,$14,1
addu	$12,$12,$3
addu	$13,$13,$2
addu	$12,$14,$12
addu	$13,$15,$13
addiu	$3,$12,2
addiu	$2,$13,2
sra	$3,$3,2
sra	$2,$2,2
addiu	$4,$4,1
sb	$3,0($8)
addiu	$11,$11,1
sb	$2,-1($4)
addiu	$10,$10,1
addiu	$8,$8,1
addiu	$9,$9,1
bne	$4,$24,$L589
addiu	$5,$5,1

$L592:
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

$L587:
addu	$2,$13,$2
addu	$2,$15,$2
addiu	$2,$2,2
sra	$2,$2,2
b	$L580
sb	$2,0($4)

$L584:
addu	$2,$12,$2
addu	$2,$14,$2
addiu	$2,$2,2
sra	$2,$2,2
b	$L586
sb	$2,0($8)

.set	macro
.set	reorder
.end	h264_v_loop_filter_luma_intra_c
.size	h264_v_loop_filter_luma_intra_c, .-h264_v_loop_filter_luma_intra_c
.section	.text.h264_h_loop_filter_luma_intra_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_h_loop_filter_luma_intra_c
.type	h264_h_loop_filter_luma_intra_c, @function
h264_h_loop_filter_luma_intra_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sra	$13,$6,2
li	$8,16			# 0x10
b	$L605
addiu	$13,$13,1

$L608:
subu	$3,$0,$2
slt	$12,$2,0
movn	$2,$3,$12
slt	$2,$2,$7
beq	$2,$0,$L600
sll	$2,$14,1

lbu	$2,-4($4)
sll	$3,$15,1
sll	$12,$14,1
addu	$3,$3,$15
sll	$2,$2,1
addu	$15,$15,$12
addu	$3,$2,$3
sll	$12,$9,1
subu	$2,$15,$14
addu	$25,$15,$12
addu	$3,$14,$3
sll	$12,$10,1
addu	$2,$9,$2
addu	$12,$25,$12
addu	$3,$9,$3
addu	$12,$11,$12
addu	$2,$10,$2
addu	$3,$10,$3
addiu	$12,$12,4
addiu	$2,$2,2
addiu	$3,$3,4
sra	$12,$12,3
sra	$2,$2,2
sra	$3,$3,3
sb	$12,-1($4)
sb	$2,-2($4)
sb	$3,-3($4)
$L602:
subu	$2,$24,$10
subu	$3,$0,$2
slt	$12,$2,0
movn	$2,$3,$12
slt	$2,$2,$7
beq	$2,$0,$L603
sll	$2,$11,1

lbu	$2,3($4)
sll	$3,$24,1
sll	$12,$9,1
addu	$15,$3,$24
sll	$3,$2,1
addu	$14,$14,$12
addu	$3,$3,$15
sll	$12,$10,1
addu	$2,$9,$10
addu	$14,$14,$12
addu	$3,$11,$3
sll	$12,$11,1
addu	$3,$10,$3
addu	$12,$14,$12
addu	$2,$11,$2
addu	$3,$9,$3
addu	$10,$24,$12
addu	$2,$24,$2
addiu	$9,$10,4
addiu	$2,$2,2
addiu	$3,$3,4
sra	$9,$9,3
sra	$2,$2,2
sra	$3,$3,3
sb	$9,0($4)
sb	$2,1($4)
sb	$3,2($4)
$L596:
addiu	$8,$8,-1
beq	$8,$0,$L609
addu	$4,$4,$5

$L605:
lbu	$9,-1($4)
lbu	$10,0($4)
lbu	$15,-3($4)
lbu	$14,-2($4)
subu	$2,$9,$10
lbu	$11,1($4)
subu	$3,$0,$2
slt	$12,$2,0
movn	$2,$3,$12
slt	$3,$2,$6
beq	$3,$0,$L596
lbu	$24,2($4)

subu	$3,$14,$9
subu	$12,$0,$3
slt	$25,$3,0
movn	$3,$12,$25
slt	$3,$3,$7
beq	$3,$0,$L596
subu	$3,$11,$10

subu	$12,$0,$3
slt	$25,$3,0
movn	$3,$12,$25
slt	$3,$3,$7
beq	$3,$0,$L596
slt	$2,$13,$2

beq	$2,$0,$L608
subu	$2,$15,$9

sll	$3,$14,1
sll	$2,$11,1
addu	$9,$9,$3
addu	$10,$10,$2
addu	$9,$11,$9
addu	$10,$14,$10
addiu	$3,$9,2
addiu	$2,$10,2
sra	$3,$3,2
sra	$2,$2,2
addiu	$8,$8,-1
sb	$3,-1($4)
sb	$2,0($4)
bne	$8,$0,$L605
addu	$4,$4,$5

$L609:
j	$31
nop

$L603:
addu	$2,$10,$2
addu	$2,$14,$2
addiu	$2,$2,2
sra	$2,$2,2
b	$L596
sb	$2,0($4)

$L600:
addu	$2,$9,$2
addu	$2,$11,$2
addiu	$2,$2,2
sra	$2,$2,2
b	$L602
sb	$2,-1($4)

.set	macro
.set	reorder
.end	h264_h_loop_filter_luma_intra_c
.size	h264_h_loop_filter_luma_intra_c, .-h264_h_loop_filter_luma_intra_c
.section	.text.h264_v_loop_filter_chroma_intra_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_v_loop_filter_chroma_intra_c
.type	h264_v_loop_filter_chroma_intra_c, @function
h264_v_loop_filter_chroma_intra_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
subu	$13,$0,$5
addiu	$sp,$sp,-8
sll	$13,$13,1
subu	$11,$4,$5
sw	$16,4($sp)
addiu	$14,$4,8
$L615:
lbu	$9,0($11)
addu	$2,$4,$13
lbu	$12,0($4)
addu	$3,$4,$5
lbu	$8,0($2)
subu	$2,$9,$12
lbu	$10,0($3)
subu	$25,$0,$2
slt	$16,$2,0
movn	$2,$25,$16
subu	$3,$8,$9
subu	$15,$0,$3
slt	$24,$3,0
slt	$2,$2,$6
beq	$2,$0,$L611
movn	$3,$15,$24

subu	$2,$10,$12
slt	$3,$3,$7
beq	$3,$0,$L611
subu	$15,$0,$2

sll	$16,$8,1
sll	$3,$10,1
addu	$9,$9,$16
addu	$12,$12,$3
slt	$3,$2,0
movn	$2,$15,$3
addu	$9,$10,$9
addu	$8,$8,$12
addiu	$9,$9,2
addiu	$8,$8,2
slt	$2,$2,$7
sra	$9,$9,2
beq	$2,$0,$L611
sra	$8,$8,2

sb	$9,0($11)
sb	$8,0($4)
$L611:
addiu	$4,$4,1
bne	$4,$14,$L615
addiu	$11,$11,1

lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	h264_v_loop_filter_chroma_intra_c
.size	h264_v_loop_filter_chroma_intra_c, .-h264_v_loop_filter_chroma_intra_c
.section	.text.h264_h_loop_filter_chroma_intra_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_h_loop_filter_chroma_intra_c
.type	h264_h_loop_filter_chroma_intra_c, @function
h264_h_loop_filter_chroma_intra_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$12,8			# 0x8
$L623:
lbu	$9,-1($4)
addiu	$12,$12,-1
lbu	$11,0($4)
lbu	$8,-2($4)
lbu	$10,1($4)
subu	$2,$9,$11
subu	$3,$8,$9
subu	$15,$0,$2
slt	$24,$2,0
movn	$2,$15,$24
subu	$13,$0,$3
slt	$14,$3,0
slt	$2,$2,$6
beq	$2,$0,$L619
movn	$3,$13,$14

subu	$2,$10,$11
slt	$3,$3,$7
beq	$3,$0,$L619
subu	$13,$0,$2

sll	$14,$8,1
sll	$3,$10,1
addu	$9,$9,$14
addu	$11,$11,$3
slt	$3,$2,0
movn	$2,$13,$3
addu	$9,$10,$9
addu	$8,$8,$11
addiu	$9,$9,2
addiu	$8,$8,2
slt	$2,$2,$7
sra	$9,$9,2
beq	$2,$0,$L619
sra	$8,$8,2

sb	$9,-1($4)
sb	$8,0($4)
$L619:
bne	$12,$0,$L623
addu	$4,$4,$5

j	$31
nop

.set	macro
.set	reorder
.end	h264_h_loop_filter_chroma_intra_c
.size	h264_h_loop_filter_chroma_intra_c, .-h264_h_loop_filter_chroma_intra_c
.section	.text.h264_h_loop_filter_luma_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_h_loop_filter_luma_c
.type	h264_h_loop_filter_luma_c, @function
h264_h_loop_filter_luma_c:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-16
sll	$13,$5,2
li	$14,-256			# 0xffffffffffffff00
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
lw	$11,32($sp)
addiu	$12,$11,4
$L646:
lb	$2,0($11)
bltz	$2,$L656
nop

li	$10,4			# 0x4
move	$2,$4
$L645:
lbu	$8,-1($2)
lbu	$9,0($2)
lbu	$25,-2($2)
lbu	$18,-3($2)
subu	$3,$8,$9
lbu	$16,1($2)
subu	$15,$0,$3
slt	$24,$3,0
movn	$3,$15,$24
slt	$3,$3,$6
beq	$3,$0,$L628
lbu	$17,2($2)

subu	$3,$25,$8
subu	$15,$0,$3
slt	$24,$3,0
movn	$3,$15,$24
slt	$3,$3,$7
beq	$3,$0,$L628
subu	$3,$16,$9

subu	$15,$0,$3
slt	$24,$3,0
movn	$3,$15,$24
slt	$3,$3,$7
beq	$3,$0,$L628
subu	$3,$18,$8

lb	$24,0($11)
subu	$15,$0,$3
slt	$19,$3,0
movn	$3,$15,$19
slt	$3,$3,$7
beq	$3,$0,$L632
move	$15,$24

bne	$24,$0,$L657
addu	$3,$8,$9

$L634:
addiu	$15,$24,1
$L632:
subu	$3,$17,$9
subu	$18,$0,$3
slt	$24,$3,0
movn	$3,$18,$24
slt	$3,$3,$7
beq	$3,$0,$L661
subu	$3,$9,$8

lb	$19,0($11)
bne	$19,$0,$L658
addu	$3,$8,$9

$L638:
addiu	$15,$15,1
subu	$3,$9,$8
$L661:
subu	$25,$25,$16
sll	$3,$3,2
subu	$16,$0,$15
addu	$25,$3,$25
addiu	$25,$25,4
sra	$25,$25,3
slt	$3,$25,$16
bne	$3,$0,$L640
nop

slt	$16,$25,$15
movz	$25,$15,$16
move	$16,$25
$L640:
addu	$8,$8,$16
and	$3,$8,$14
bne	$3,$0,$L659
nop

andi	$8,$8,0x00ff
$L642:
subu	$9,$9,$16
and	$3,$9,$14
bne	$3,$0,$L660
sb	$8,-1($2)

andi	$9,$9,0x00ff
$L644:
sb	$9,0($2)
$L628:
addiu	$10,$10,-1
bne	$10,$0,$L645
addu	$2,$2,$5

$L656:
addiu	$11,$11,1
bne	$11,$12,$L646
addu	$4,$4,$13

lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

$L660:
subu	$9,$0,$9
sra	$9,$9,31
b	$L644
andi	$9,$9,0x00ff

$L659:
subu	$8,$0,$8
sra	$8,$8,31
b	$L642
andi	$8,$8,0x00ff

$L658:
subu	$18,$0,$19
addiu	$3,$3,1
sra	$3,$3,1
addu	$3,$17,$3
sra	$3,$3,1
subu	$3,$3,$16
slt	$17,$3,$18
bne	$17,$0,$L662
addu	$18,$16,$18

slt	$18,$19,$3
movz	$19,$3,$18
move	$18,$19
addu	$18,$16,$18
$L662:
b	$L638
sb	$18,1($2)

$L657:
subu	$15,$0,$24
addiu	$3,$3,1
sra	$3,$3,1
addu	$3,$18,$3
sra	$3,$3,1
subu	$3,$3,$25
slt	$18,$3,$15
bne	$18,$0,$L663
addu	$15,$25,$15

slt	$15,$24,$3
movn	$3,$24,$15
move	$15,$3
addu	$15,$25,$15
$L663:
b	$L634
sb	$15,-2($2)

.set	macro
.set	reorder
.end	h264_h_loop_filter_luma_c
.size	h264_h_loop_filter_luma_c, .-h264_h_loop_filter_luma_c
.section	.text.h264_h_loop_filter_chroma_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_h_loop_filter_chroma_c
.type	h264_h_loop_filter_chroma_c, @function
h264_h_loop_filter_chroma_c:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-24
sll	$15,$5,1
li	$24,-256			# 0xffffffffffffff00
sw	$16,4($sp)
sw	$20,20($sp)
li	$13,1			# 0x1
sw	$19,16($sp)
move	$16,$15
sw	$18,12($sp)
sw	$17,8($sp)
lw	$10,40($sp)
addiu	$14,$10,4
$L677:
lb	$11,0($10)
blez	$11,$L681
li	$12,2			# 0x2

subu	$25,$0,$11
move	$3,$4
$L676:
lbu	$8,-1($3)
lbu	$9,0($3)
lbu	$19,-2($3)
subu	$2,$8,$9
subu	$17,$0,$2
slt	$18,$2,0
movn	$2,$17,$18
slt	$2,$2,$6
beq	$2,$0,$L667
lbu	$20,1($3)

subu	$2,$19,$8
subu	$17,$0,$2
slt	$18,$2,0
movn	$2,$17,$18
slt	$2,$2,$7
beq	$2,$0,$L667
subu	$2,$20,$9

subu	$17,$0,$2
slt	$18,$2,0
movn	$2,$17,$18
slt	$2,$2,$7
beq	$2,$0,$L667
subu	$2,$9,$8

subu	$19,$19,$20
sll	$20,$2,2
addu	$19,$20,$19
addiu	$19,$19,4
sra	$19,$19,3
slt	$17,$19,$25
bne	$17,$0,$L671
move	$2,$25

slt	$2,$11,$19
movn	$19,$11,$2
move	$2,$19
$L671:
addu	$8,$8,$2
and	$17,$8,$24
bne	$17,$0,$L682
nop

subu	$9,$9,$2
andi	$8,$8,0x00ff
and	$2,$9,$24
bne	$2,$0,$L683
sb	$8,-1($3)

$L674:
andi	$9,$9,0x00ff
sb	$9,0($3)
$L667:
bne	$12,$13,$L678
addu	$3,$3,$5

addu	$4,$4,$16
$L666:
addiu	$10,$10,1
bne	$10,$14,$L677
lw	$20,20($sp)

lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,24

$L678:
b	$L676
li	$12,1			# 0x1

$L681:
b	$L666
addu	$4,$4,$15

$L682:
subu	$8,$0,$8
subu	$9,$9,$2
sra	$8,$8,31
and	$2,$9,$24
andi	$8,$8,0x00ff
beq	$2,$0,$L674
sb	$8,-1($3)

$L683:
subu	$9,$0,$9
sra	$9,$9,31
andi	$9,$9,0x00ff
b	$L667
sb	$9,0($3)

.set	macro
.set	reorder
.end	h264_h_loop_filter_chroma_c
.size	h264_h_loop_filter_chroma_c, .-h264_h_loop_filter_chroma_c
.section	.text.h264_v_loop_filter_chroma_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_v_loop_filter_chroma_c
.type	h264_v_loop_filter_chroma_c, @function
h264_v_loop_filter_chroma_c:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-16
subu	$15,$0,$5
li	$24,-2			# 0xfffffffffffffffe
sw	$19,12($sp)
sw	$18,8($sp)
addiu	$8,$4,2
sw	$17,4($sp)
sll	$15,$15,1
sw	$16,0($sp)
addiu	$4,$4,10
lw	$13,32($sp)
subu	$24,$24,$5
li	$25,-256			# 0xffffffffffffff00
$L696:
lb	$14,0($13)
blez	$14,$L685
addiu	$3,$8,-2

addu	$10,$24,$8
subu	$16,$0,$14
$L695:
addu	$2,$3,$15
lbu	$11,0($10)
lbu	$12,0($3)
addu	$9,$3,$5
lbu	$18,0($2)
subu	$2,$11,$12
lbu	$19,0($9)
subu	$9,$0,$2
slt	$17,$2,0
movn	$2,$9,$17
slt	$2,$2,$6
beq	$2,$0,$L686
subu	$2,$18,$11

subu	$9,$0,$2
slt	$17,$2,0
movn	$2,$9,$17
slt	$2,$2,$7
beq	$2,$0,$L686
subu	$2,$19,$12

subu	$9,$0,$2
slt	$17,$2,0
movn	$2,$9,$17
slt	$2,$2,$7
beq	$2,$0,$L686
subu	$2,$12,$11

subu	$18,$18,$19
sll	$2,$2,2
addu	$18,$2,$18
addiu	$18,$18,4
sra	$18,$18,3
slt	$2,$18,$16
bne	$2,$0,$L690
move	$9,$16

slt	$9,$14,$18
movn	$18,$14,$9
move	$9,$18
$L690:
addu	$11,$11,$9
and	$2,$11,$25
bne	$2,$0,$L700
nop

subu	$12,$12,$9
andi	$11,$11,0x00ff
and	$2,$12,$25
bne	$2,$0,$L701
sb	$11,0($10)

$L693:
andi	$12,$12,0x00ff
sb	$12,0($3)
$L686:
addiu	$3,$3,1
bne	$3,$8,$L695
addiu	$10,$10,1

$L685:
addiu	$8,$8,2
bne	$8,$4,$L696
addiu	$13,$13,1

lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

$L700:
subu	$11,$0,$11
subu	$12,$12,$9
sra	$11,$11,31
and	$2,$12,$25
andi	$11,$11,0x00ff
beq	$2,$0,$L693
sb	$11,0($10)

$L701:
subu	$12,$0,$12
sra	$12,$12,31
andi	$12,$12,0x00ff
b	$L686
sb	$12,0($3)

.set	macro
.set	reorder
.end	h264_v_loop_filter_chroma_c
.size	h264_v_loop_filter_chroma_c, .-h264_v_loop_filter_chroma_c
.section	.text.h264_v_loop_filter_luma_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	h264_v_loop_filter_luma_c
.type	h264_v_loop_filter_luma_c, @function
h264_v_loop_filter_luma_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
move	$14,$4
sw	$18,12($sp)
sll	$18,$5,1
sw	$19,16($sp)
subu	$19,$0,$5
sw	$17,8($sp)
addu	$17,$18,$5
sw	$16,4($sp)
sll	$19,$19,1
sw	$21,24($sp)
subu	$17,$0,$17
sw	$20,20($sp)
addiu	$4,$4,4
sw	$fp,36($sp)
addiu	$20,$14,20
sw	$23,32($sp)
li	$21,-256			# 0xffffffffffffff00
sw	$22,28($sp)
lw	$16,56($sp)
$L722:
lb	$2,0($16)
bltz	$2,$L703
move	$3,$14

subu	$11,$14,$5
addu	$13,$14,$19
addu	$12,$14,$5
$L721:
addu	$2,$3,$17
lbu	$9,0($11)
lbu	$10,0($3)
addu	$8,$3,$18
lbu	$15,0($13)
lbu	$25,0($2)
subu	$2,$9,$10
lbu	$24,0($12)
subu	$22,$0,$2
slt	$23,$2,0
movn	$2,$22,$23
slt	$2,$2,$6
beq	$2,$0,$L704
lbu	$8,0($8)

subu	$2,$15,$9
subu	$22,$0,$2
slt	$23,$2,0
movn	$2,$22,$23
slt	$2,$2,$7
beq	$2,$0,$L704
subu	$2,$24,$10

subu	$22,$0,$2
slt	$23,$2,0
movn	$2,$22,$23
slt	$2,$2,$7
beq	$2,$0,$L704
subu	$2,$25,$9

lb	$23,0($16)
subu	$22,$0,$2
slt	$fp,$2,0
movn	$2,$22,$fp
slt	$2,$2,$7
beq	$2,$0,$L708
move	$22,$23

bne	$23,$0,$L732
addu	$2,$9,$10

$L710:
addiu	$22,$23,1
$L708:
subu	$2,$8,$10
subu	$23,$0,$2
slt	$25,$2,0
movn	$2,$23,$25
slt	$2,$2,$7
beq	$2,$0,$L736
subu	$2,$10,$9

lb	$25,0($16)
bne	$25,$0,$L733
addu	$2,$9,$10

$L714:
addiu	$22,$22,1
subu	$2,$10,$9
$L736:
subu	$15,$15,$24
sll	$24,$2,2
subu	$2,$0,$22
addu	$15,$24,$15
addiu	$15,$15,4
sra	$15,$15,3
slt	$8,$15,$2
bne	$8,$0,$L716
nop

slt	$2,$15,$22
movz	$15,$22,$2
move	$2,$15
$L716:
addu	$9,$9,$2
and	$8,$9,$21
bne	$8,$0,$L734
nop

andi	$9,$9,0x00ff
$L718:
subu	$10,$10,$2
and	$2,$10,$21
bne	$2,$0,$L735
sb	$9,0($11)

andi	$10,$10,0x00ff
$L720:
sb	$10,0($3)
$L704:
addiu	$3,$3,1
addiu	$11,$11,1
addiu	$13,$13,1
bne	$3,$4,$L721
addiu	$12,$12,1

$L703:
addiu	$4,$4,4
addiu	$16,$16,1
bne	$4,$20,$L722
addiu	$14,$14,4

lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

$L735:
subu	$10,$0,$10
sra	$10,$10,31
b	$L720
andi	$10,$10,0x00ff

$L734:
subu	$9,$0,$9
sra	$9,$9,31
b	$L718
andi	$9,$9,0x00ff

$L733:
subu	$23,$0,$25
addiu	$2,$2,1
sra	$2,$2,1
addu	$2,$8,$2
sra	$2,$2,1
subu	$2,$2,$24
slt	$8,$2,$23
bne	$8,$0,$L737
addu	$23,$24,$23

slt	$23,$25,$2
movz	$25,$2,$23
move	$23,$25
addu	$23,$24,$23
$L737:
b	$L714
sb	$23,0($12)

$L732:
subu	$22,$0,$23
addiu	$2,$2,1
sra	$2,$2,1
addu	$2,$25,$2
sra	$2,$2,1
subu	$2,$2,$15
slt	$25,$2,$22
bne	$25,$0,$L738
addu	$22,$15,$22

slt	$22,$23,$2
movn	$2,$23,$22
move	$22,$2
addu	$22,$15,$22
$L738:
b	$L710
sb	$22,0($13)

.set	macro
.set	reorder
.end	h264_v_loop_filter_luma_c
.size	h264_v_loop_filter_luma_c, .-h264_v_loop_filter_luma_c
.section	.text.ff_h264dsp_init,"ax",@progbits
.align	2
.align	5
.globl	ff_h264dsp_init
.set	nomips16
.set	nomicromips
.ent	ff_h264dsp_init
.type	ff_h264dsp_init, @function
ff_h264dsp_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(ff_h264_idct_add_c)($28)
sw	$2,120($4)
lw	$2,%got(ff_h264_idct8_add_c)($28)
sw	$2,124($4)
lw	$2,%got(ff_h264_idct_dc_add_c)($28)
sw	$2,128($4)
lw	$2,%got(ff_h264_idct8_dc_add_c)($28)
sw	$2,132($4)
lw	$2,%got(ff_h264_idct_add16_c)($28)
sw	$2,140($4)
lw	$2,%got(ff_h264_idct8_add4_c)($28)
sw	$2,144($4)
lw	$2,%got(ff_h264_idct_add8_c)($28)
sw	$2,148($4)
lw	$2,%got(ff_h264_idct_add16intra_c)($28)
sw	$2,152($4)
lw	$2,%got(weight_h264_pixels16x16_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels16x16_c)
sw	$2,0($4)
lw	$2,%got(weight_h264_pixels16x8_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels16x8_c)
sw	$2,4($4)
lw	$2,%got(weight_h264_pixels8x16_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels8x16_c)
sw	$2,8($4)
lw	$2,%got(weight_h264_pixels8x8_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels8x8_c)
sw	$2,12($4)
lw	$2,%got(weight_h264_pixels8x4_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels8x4_c)
sw	$2,16($4)
lw	$2,%got(weight_h264_pixels4x8_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels4x8_c)
sw	$2,20($4)
lw	$2,%got(weight_h264_pixels4x4_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels4x4_c)
sw	$2,24($4)
lw	$2,%got(weight_h264_pixels4x2_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels4x2_c)
sw	$2,28($4)
lw	$2,%got(weight_h264_pixels2x4_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels2x4_c)
sw	$2,32($4)
lw	$2,%got(weight_h264_pixels2x2_c)($28)
addiu	$2,$2,%lo(weight_h264_pixels2x2_c)
sw	$2,36($4)
lw	$2,%got(biweight_h264_pixels16x16_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels16x16_c)
sw	$2,40($4)
lw	$2,%got(biweight_h264_pixels16x8_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels16x8_c)
sw	$2,44($4)
lw	$2,%got(biweight_h264_pixels8x16_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels8x16_c)
sw	$2,48($4)
lw	$2,%got(biweight_h264_pixels8x8_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels8x8_c)
sw	$2,52($4)
lw	$2,%got(biweight_h264_pixels8x4_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels8x4_c)
sw	$2,56($4)
lw	$2,%got(biweight_h264_pixels4x8_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels4x8_c)
sw	$2,60($4)
lw	$2,%got(biweight_h264_pixels4x4_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels4x4_c)
sw	$2,64($4)
lw	$2,%got(biweight_h264_pixels4x2_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels4x2_c)
sw	$2,68($4)
lw	$2,%got(biweight_h264_pixels2x4_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels2x4_c)
sw	$2,72($4)
lw	$2,%got(biweight_h264_pixels2x2_c)($28)
addiu	$2,$2,%lo(biweight_h264_pixels2x2_c)
sw	$2,76($4)
lw	$2,%got(h264_v_loop_filter_luma_c)($28)
addiu	$2,$2,%lo(h264_v_loop_filter_luma_c)
sw	$2,80($4)
lw	$2,%got(h264_h_loop_filter_luma_c)($28)
addiu	$2,$2,%lo(h264_h_loop_filter_luma_c)
sw	$2,84($4)
lw	$2,%got(h264_v_loop_filter_luma_intra_c)($28)
addiu	$2,$2,%lo(h264_v_loop_filter_luma_intra_c)
sw	$2,88($4)
lw	$2,%got(h264_h_loop_filter_luma_intra_c)($28)
addiu	$2,$2,%lo(h264_h_loop_filter_luma_intra_c)
sw	$2,92($4)
lw	$2,%got(h264_v_loop_filter_chroma_c)($28)
addiu	$2,$2,%lo(h264_v_loop_filter_chroma_c)
sw	$2,96($4)
lw	$2,%got(h264_h_loop_filter_chroma_c)($28)
addiu	$2,$2,%lo(h264_h_loop_filter_chroma_c)
sw	$2,100($4)
lw	$2,%got(h264_v_loop_filter_chroma_intra_c)($28)
sw	$0,112($4)
sw	$0,116($4)
addiu	$2,$2,%lo(h264_v_loop_filter_chroma_intra_c)
sw	$2,104($4)
lw	$2,%got(h264_h_loop_filter_chroma_intra_c)($28)
addiu	$2,$2,%lo(h264_h_loop_filter_chroma_intra_c)
j	$31
sw	$2,108($4)

.set	macro
.set	reorder
.end	ff_h264dsp_init
.size	ff_h264dsp_init, .-ff_h264dsp_init
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
