.file	1 "h264_sei.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"x264 - core %d\000"
.align	2
$LC1:
.ascii	"user data:\"%s\"\012\000"
.section	.text.decode_unregistered_user_data,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_unregistered_user_data
.type	decode_unregistered_user_data, @function
decode_unregistered_user_data:
.frame	$sp,328,$31		# vars= 280, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
slt	$2,$5,16
bne	$2,$0,$L10
addiu	$sp,$sp,-328
lw	$14,10332($4)
li	$12,16711680			# 0xff0000
lw	$7,10340($4)
li	$11,-16777216			# 0xffffffffff000000
.cprestore	16
move	$8,$0
sw	$31,324($sp)
addiu	$10,$sp,24
sw	$19,320($sp)
addiu	$12,$12,255
sw	$18,316($sp)
li	$13,271			# 0x10f
sw	$17,312($sp)
sw	$16,308($sp)
ori	$11,$11,0xff00
srl	$3,$7,3
$L23:
andi	$9,$7,0x7
addu	$3,$14,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$6,8
sll	$6,$6,8
and	$3,$3,$12
and	$6,$6,$11
or	$2,$3,$6
sll	$3,$2,16
srl	$2,$2,16
addiu	$7,$7,8
or	$2,$3,$2
sll	$2,$2,$9
addu	$3,$10,$8
sw	$7,10340($4)
srl	$2,$2,24
addiu	$8,$8,1
.set	noreorder
.set	nomacro
beq	$8,$13,$L11
sb	$2,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$8,$5,$L23
srl	$3,$7,3
.set	macro
.set	reorder

move	$17,$5
$L3:
move	$18,$5
lw	$5,%got($LC0)($28)
lw	$25,%call16(sscanf)($28)
addu	$10,$10,$17
addiu	$19,$sp,40
addiu	$6,$sp,296
sb	$0,0($10)
addiu	$5,$5,%lo($LC0)
move	$16,$4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
move	$4,$19
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L20
lw	$28,16($sp)
.set	macro
.set	reorder

$L5:
lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x1000
.set	noreorder
.set	nomacro
bne	$2,$0,$L21
lw	$6,%got($LC1)($28)
.set	macro
.set	reorder

slt	$2,$17,$18
$L22:
.set	noreorder
.set	nomacro
beq	$2,$0,$L24
lw	$31,324($sp)
.set	macro
.set	reorder

lw	$2,10340($16)
$L8:
addiu	$2,$2,8
addiu	$17,$17,1
.set	noreorder
.set	nomacro
bne	$17,$18,$L8
sw	$2,10340($16)
.set	macro
.set	reorder

lw	$31,324($sp)
$L24:
move	$2,$0
lw	$19,320($sp)
lw	$18,316($sp)
lw	$17,312($sp)
lw	$16,308($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,328
.set	macro
.set	reorder

$L21:
li	$5,48			# 0x30
lw	$25,%call16(av_log)($28)
move	$7,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC1)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L22
slt	$2,$17,$18
.set	macro
.set	reorder

$L20:
lw	$3,296($sp)
.set	noreorder
.set	nomacro
blez	$3,$L5
li	$2,131072			# 0x20000
.set	macro
.set	reorder

addu	$2,$16,$2
.set	noreorder
.set	nomacro
b	$L5
sw	$3,9444($2)
.set	macro
.set	reorder

$L11:
.set	noreorder
.set	nomacro
b	$L3
li	$17,271			# 0x10f
.set	macro
.set	reorder

$L10:
.set	noreorder
.set	nomacro
j	$31
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	decode_unregistered_user_data
.size	decode_unregistered_user_data, .-decode_unregistered_user_data
.section	.text.ff_h264_reset_sei,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_reset_sei
.set	nomips16
.set	nomicromips
.ent	ff_h264_reset_sei
.type	ff_h264_reset_sei, @function
ff_h264_reset_sei:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,196608			# 0x30000
li	$3,-1			# 0xffffffffffffffff
addu	$4,$4,$2
sw	$3,-15176($4)
sw	$0,-15184($4)
sw	$3,-15180($4)
j	$31
sw	$0,-15156($4)

.set	macro
.set	reorder
.end	ff_h264_reset_sei
.size	ff_h264_reset_sei, .-ff_h264_reset_sei
.section	.rodata.str1.4
.align	2
$LC2:
.ascii	"ct_type:%X pic_struct:%d\012\000"
.align	2
$LC3:
.ascii	"non-existing SPS %d referenced in buffering period\012\000"
.section	.text.ff_h264_decode_sei,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_sei
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_sei
.type	ff_h264_decode_sei, @function
ff_h264_decode_sei:
.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-56
lw	$2,10340($4)
lw	$10,10344($4)
sw	$19,44($sp)
sw	$18,40($sp)
sw	$16,32($sp)
li	$16,131072			# 0x20000
sw	$17,36($sp)
sw	$20,48($sp)
ori	$16,$16,0xc4d0
.cprestore	24
addu	$16,$4,$16
sw	$31,52($sp)
move	$20,$4
lw	$18,%got(sei_num_clock_ts_table)($28)
lw	$19,%got($LC2)($28)
lw	$17,%got(decode_unregistered_user_data)($28)
addiu	$18,$18,%lo(sei_num_clock_ts_table)
addiu	$19,$19,%lo($LC2)
$L63:
addiu	$3,$2,16
slt	$3,$3,$10
.set	noreorder
.set	nomacro
beq	$3,$0,$L102
lw	$31,52($sp)
.set	macro
.set	reorder

li	$11,16711680			# 0xff0000
$L103:
lw	$8,10332($20)
li	$7,-16777216			# 0xffffffffff000000
move	$9,$0
addiu	$11,$11,255
li	$12,255			# 0xff
ori	$7,$7,0xff00
$L28:
srl	$4,$2,3
andi	$13,$2,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$6,8
sll	$6,$6,8
and	$4,$4,$11
and	$6,$6,$7
or	$6,$4,$6
sll	$5,$6,16
srl	$6,$6,16
addiu	$2,$2,8
or	$3,$5,$6
sll	$3,$3,$13
sw	$2,10340($20)
srl	$3,$3,24
.set	noreorder
.set	nomacro
beq	$3,$12,$L28
addu	$9,$3,$9
.set	macro
.set	reorder

li	$12,16711680			# 0xff0000
li	$11,-16777216			# 0xffffffffff000000
move	$5,$0
addiu	$12,$12,255
li	$13,255			# 0xff
ori	$11,$11,0xff00
$L29:
srl	$4,$2,3
andi	$14,$2,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$7,$6,8
sll	$6,$6,8
and	$7,$7,$12
and	$6,$6,$11
or	$6,$7,$6
sll	$7,$6,16
srl	$6,$6,16
addiu	$2,$2,8
or	$3,$7,$6
sll	$3,$3,$14
sw	$2,10340($20)
srl	$3,$3,24
.set	noreorder
.set	nomacro
beq	$3,$13,$L29
addu	$5,$3,$5
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$9,$3,$L31
slt	$3,$9,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L96
li	$3,5			# 0x5
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$9,$3,$L34
li	$3,6			# 0x6
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$9,$3,$L30
srl	$4,$2,3
.set	macro
.set	reorder

andi	$3,$2,0x7
addu	$8,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($8)  
lwr $5, 0($8)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$12,$4,$12
and	$4,$5,$11
or	$4,$12,$4
sll	$5,$4,16
srl	$4,$4,16
or	$4,$5,$4
sll	$3,$4,$3
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L97
lw	$5,%got(ff_golomb_vlc_len)($28)
.set	macro
.set	reorder

ori	$4,$3,0x1
clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$4,$2,$4
addiu	$3,$3,-1
sw	$4,10340($20)
$L55:
li	$5,196608			# 0x30000
addiu	$4,$4,4
addu	$5,$20,$5
move	$2,$4
sw	$3,-15176($5)
sw	$4,10340($20)
$L53:
subu	$3,$0,$2
$L108:
andi	$3,$3,0x7
.set	noreorder
.set	nomacro
beq	$3,$0,$L63
addu	$3,$3,$2
.set	macro
.set	reorder

move	$2,$3
sw	$3,10340($20)
addiu	$3,$2,16
slt	$3,$3,$10
.set	noreorder
.set	nomacro
bne	$3,$0,$L103
li	$11,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$31,52($sp)
$L102:
move	$2,$0
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L96:
.set	noreorder
.set	nomacro
bne	$9,$0,$L30
srl	$4,$2,3
.set	macro
.set	reorder

andi	$6,$2,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$12
and	$5,$5,$11
or	$5,$4,$5
sll	$4,$5,16
srl	$5,$5,16
or	$3,$4,$5
lw	$5,%got(ff_golomb_vlc_len)($28)
sll	$3,$3,$6
lw	$4,%got(ff_ue_golomb_vlc_code)($28)
srl	$3,$3,23
addu	$5,$5,$3
addu	$3,$4,$3
lbu	$6,0($5)
lbu	$7,0($3)
addu	$6,$6,$2
sltu	$2,$7,32
.set	noreorder
.set	nomacro
beq	$2,$0,$L57
sw	$6,10340($20)
.set	macro
.set	reorder

li	$2,35142			# 0x8946
addu	$2,$7,$2
sll	$2,$2,2
addu	$2,$20,$2
lw	$5,4($2)
beq	$5,$0,$L57
lw	$2,892($5)
beq	$2,$0,$L66
lw	$2,908($5)
.set	noreorder
.set	nomacro
blez	$2,$L59
li	$14,32			# 0x20
.set	macro
.set	reorder

move	$13,$0
move	$9,$16
srl	$3,$6,3
$L104:
lw	$2,912($5)
andi	$15,$6,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$7,8
sll	$7,$7,8
and	$4,$4,$12
and	$7,$7,$11
or	$3,$4,$7
sll	$4,$3,16
srl	$3,$3,16
subu	$7,$14,$2
or	$3,$4,$3
sll	$3,$3,$15
addu	$2,$6,$2
srl	$3,$3,$7
addiu	$9,$9,4
sw	$2,10340($20)
addiu	$13,$13,1
sw	$3,-4($9)
lw	$6,912($5)
addu	$6,$2,$6
sw	$6,10340($20)
lw	$2,908($5)
slt	$2,$13,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L104
srl	$3,$6,3
.set	macro
.set	reorder

$L66:
lw	$3,896($5)
.set	noreorder
.set	nomacro
beq	$3,$0,$L61
move	$2,$6
.set	macro
.set	reorder

lw	$3,908($5)
.set	noreorder
.set	nomacro
blez	$3,$L105
li	$3,196608			# 0x30000
.set	macro
.set	reorder

li	$11,16711680			# 0xff0000
li	$13,-16777216			# 0xffffffffff000000
move	$9,$0
addiu	$11,$11,255
li	$12,32			# 0x20
move	$7,$16
ori	$13,$13,0xff00
srl	$3,$2,3
$L106:
lw	$14,912($5)
andi	$15,$2,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$6,8
sll	$6,$6,8
and	$4,$4,$11
and	$6,$6,$13
or	$3,$4,$6
sll	$4,$3,16
srl	$3,$3,16
subu	$6,$12,$14
or	$3,$4,$3
sll	$3,$3,$15
addu	$2,$2,$14
srl	$3,$3,$6
addiu	$7,$7,4
sw	$2,10340($20)
addiu	$9,$9,1
sw	$3,-4($7)
lw	$3,912($5)
addu	$2,$2,$3
sw	$2,10340($20)
lw	$3,908($5)
slt	$3,$9,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L106
srl	$3,$2,3
.set	macro
.set	reorder

$L61:
li	$3,196608			# 0x30000
$L105:
li	$4,1			# 0x1
addu	$3,$20,$3
.set	noreorder
.set	nomacro
b	$L53
sw	$4,-15156($3)
.set	macro
.set	reorder

$L34:
addiu	$25,$17,%lo(decode_unregistered_user_data)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_unregistered_user_data
1:	jalr	$25
move	$4,$20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L94
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$2,10340($20)
.set	noreorder
.set	nomacro
b	$L53
lw	$10,10344($20)
.set	macro
.set	reorder

$L30:
sll	$5,$5,3
addu	$5,$5,$2
move	$2,$5
.set	noreorder
.set	nomacro
b	$L53
sw	$5,10340($20)
.set	macro
.set	reorder

$L31:
lw	$3,12780($20)
.set	noreorder
.set	nomacro
bne	$3,$0,$L107
srl	$3,$2,3
.set	macro
.set	reorder

lw	$3,12784($20)
.set	noreorder
.set	nomacro
beq	$3,$0,$L37
srl	$3,$2,3
.set	macro
.set	reorder

$L107:
lw	$12,12804($20)
li	$11,16711680			# 0xff0000
lw	$6,12808($20)
addu	$3,$8,$3
li	$9,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
addiu	$11,$11,255
srl	$4,$4,8
sll	$3,$3,8
ori	$9,$9,0xff00
and	$3,$3,$9
and	$4,$4,$11
or	$4,$4,$3
sll	$5,$4,16
srl	$4,$4,16
li	$7,32			# 0x20
andi	$3,$2,0x7
or	$4,$5,$4
sll	$4,$4,$3
subu	$3,$7,$12
addu	$2,$2,$12
li	$5,196608			# 0x30000
srl	$4,$4,$3
addu	$5,$20,$5
sw	$2,10340($20)
srl	$3,$2,3
sw	$4,-15180($5)
andi	$12,$2,0x7
addu	$4,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
move	$4,$3
sll	$4,$4,8
srl	$3,$3,8
and	$4,$4,$9
and	$3,$3,$11
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
subu	$7,$7,$6
or	$3,$4,$3
sll	$3,$3,$12
addu	$2,$2,$6
srl	$3,$3,$7
sw	$2,10340($20)
sw	$3,-15184($5)
$L37:
lw	$3,12788($20)
.set	noreorder
.set	nomacro
beq	$3,$0,$L108
subu	$3,$0,$2
.set	macro
.set	reorder

srl	$3,$2,3
li	$9,16711680			# 0xff0000
li	$7,-16777216			# 0xffffffffff000000
addu	$3,$8,$3
addiu	$9,$9,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
ori	$7,$7,0xff00
and	$4,$4,$7
and	$3,$3,$9
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$2,0x7
or	$3,$4,$3
sll	$3,$3,$5
li	$10,196608			# 0x30000
srl	$3,$3,28
addiu	$2,$2,4
addu	$10,$20,$10
sltu	$4,$3,9
sw	$2,10340($20)
sw	$3,-15196($10)
.set	noreorder
.set	nomacro
beq	$4,$0,$L94
sw	$0,-15188($10)
.set	macro
.set	reorder

addu	$3,$3,$18
move	$6,$0
lbu	$11,0($3)
.set	noreorder
.set	nomacro
bne	$11,$0,$L91
li	$12,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L101
lw	$4,0($20)
.set	macro
.set	reorder

$L45:
addiu	$6,$6,1
beq	$11,$6,$L51
$L99:
lw	$8,10332($20)
lw	$2,10340($20)
$L91:
srl	$3,$2,3
addiu	$5,$2,1
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
srl	$4,$4,8
sll	$3,$3,8
and	$4,$4,$9
and	$3,$3,$7
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
andi	$13,$2,0x7
or	$4,$4,$3
sll	$3,$4,$13
.set	noreorder
.set	nomacro
bgez	$3,$L45
sw	$5,10340($20)
.set	macro
.set	reorder

srl	$3,$5,3
addiu	$13,$2,3
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sw	$13,10340($20)
srl	$4,$4,8
lw	$14,-15188($10)
sll	$3,$3,8
and	$13,$4,$9
and	$4,$3,$7
or	$4,$13,$4
sll	$13,$4,16
srl	$4,$4,16
andi	$5,$5,0x7
or	$4,$13,$4
sll	$3,$4,$5
addiu	$4,$2,9
srl	$3,$3,30
srl	$5,$4,3
sll	$3,$12,$3
addu	$5,$8,$5
or	$3,$3,$14
andi	$14,$4,0x7
sw	$3,-15188($10)
addiu	$13,$2,20
sw	$4,10340($20)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($5)  
lwr $4, 0($5)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$5,$4,8
and	$4,$3,$9
and	$3,$5,$7
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$3,$3,$14
.set	noreorder
.set	nomacro
bgez	$3,$L46
sw	$13,10340($20)
.set	macro
.set	reorder

addiu	$2,$2,37
sw	$2,10340($20)
$L47:
lw	$3,12792($20)
blez	$3,$L45
lw	$2,10340($20)
addiu	$6,$6,1
addu	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$11,$6,$L99
sw	$2,10340($20)
.set	macro
.set	reorder

$L51:
lw	$4,0($20)
$L101:
lw	$2,404($4)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L100
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

lw	$10,10344($20)
.set	noreorder
.set	nomacro
b	$L53
lw	$2,10340($20)
.set	macro
.set	reorder

$L97:
srl	$3,$3,23
lw	$4,%got(ff_ue_golomb_vlc_code)($28)
addu	$5,$5,$3
addu	$3,$4,$3
lbu	$4,0($5)
lbu	$3,0($3)
addu	$4,$4,$2
.set	noreorder
.set	nomacro
b	$L55
sw	$4,10340($20)
.set	macro
.set	reorder

$L100:
li	$2,196608			# 0x30000
li	$5,48			# 0x30
addu	$2,$20,$2
move	$6,$19
lw	$3,-15196($2)
lw	$7,-15188($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$10,10344($20)
.set	noreorder
.set	nomacro
b	$L53
lw	$2,10340($20)
.set	macro
.set	reorder

$L46:
srl	$3,$13,3
addiu	$5,$2,21
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$9
and	$4,$4,$7
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
andi	$13,$13,0x7
or	$3,$4,$3
sll	$13,$3,$13
.set	noreorder
.set	nomacro
bgez	$13,$L47
sw	$5,10340($20)
.set	macro
.set	reorder

addiu	$3,$2,27
addiu	$13,$2,28
srl	$4,$3,3
sw	$3,10340($20)
andi	$5,$3,0x7
addu	$3,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$9
and	$4,$4,$7
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$3,$3,$5
.set	noreorder
.set	nomacro
bgez	$3,$L47
sw	$13,10340($20)
.set	macro
.set	reorder

addiu	$3,$2,34
addiu	$13,$2,35
srl	$4,$3,3
sw	$3,10340($20)
andi	$5,$3,0x7
addu	$8,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($8)  
lwr $4, 0($8)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$9
and	$4,$4,$7
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$3,$3,$5
.set	noreorder
.set	nomacro
bgez	$3,$L47
sw	$13,10340($20)
.set	macro
.set	reorder

addiu	$2,$2,40
.set	noreorder
.set	nomacro
b	$L47
sw	$2,10340($20)
.set	macro
.set	reorder

$L57:
lw	$6,%got($LC3)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($20)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

$L94:
lw	$31,52($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L59:
li	$3,196608			# 0x30000
li	$4,1			# 0x1
addu	$3,$20,$3
move	$2,$6
.set	noreorder
.set	nomacro
b	$L53
sw	$4,-15156($3)
.set	macro
.set	reorder

.end	ff_h264_decode_sei
.size	ff_h264_decode_sei, .-ff_h264_decode_sei
.rdata
.align	2
.type	sei_num_clock_ts_table, @object
.size	sei_num_clock_ts_table, 9
sei_num_clock_ts_table:
.byte	1
.byte	1
.byte	1
.byte	2
.byte	2
.byte	3
.byte	3
.byte	2
.byte	3

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
