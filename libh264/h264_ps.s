.file	1 "h264_ps.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.decode_scaling_list,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_scaling_list
.type	decode_scaling_list, @function
decode_scaling_list:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-16
li	$2,16			# 0x10
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
.set	noreorder
.set	nomacro
beq	$6,$2,$L20
lw	$10,32($sp)
.set	macro
.set	reorder

lw	$3,10340($4)
lw	$8,10332($4)
lw	$25,%got(ff_zigzag_direct)($28)
srl	$2,$3,3
andi	$9,$3,0x7
addu	$8,$8,$2
addiu	$3,$3,1
lbu	$2,0($8)
sll	$2,$2,$9
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L3
sw	$3,10340($4)
.set	macro
.set	reorder

$L35:
.set	noreorder
.set	nomacro
blez	$6,$L36
lw	$18,12($sp)
.set	macro
.set	reorder

li	$12,16711680			# 0xff0000
lw	$24,%got(ff_golomb_vlc_len)($28)
li	$11,-16777216			# 0xffffffffff000000
lw	$15,%got(ff_se_golomb_vlc_code)($28)
li	$2,8			# 0x8
li	$8,8			# 0x8
move	$9,$0
addiu	$12,$12,255
li	$13,134217728			# 0x8000000
li	$14,31			# 0x1f
move	$10,$25
ori	$11,$11,0xff00
$L15:
bne	$2,$0,$L32
beq	$9,$0,$L17
lbu	$3,0($10)
addu	$3,$5,$3
$L13:
move	$2,$0
andi	$8,$8,0x00ff
$L14:
addiu	$9,$9,1
sb	$8,0($3)
.set	noreorder
.set	nomacro
bne	$9,$6,$L15
addiu	$10,$10,1
.set	macro
.set	reorder

lw	$18,12($sp)
$L36:
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,16
.set	macro
.set	reorder

$L32:
lw	$17,10340($4)
lw	$2,10332($4)
srl	$3,$17,3
andi	$18,$17,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $16, 3($2)  
lwr $16, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$16,8
sll	$16,$16,8
and	$3,$3,$12
and	$16,$16,$11
or	$16,$3,$16
sll	$3,$16,16
srl	$2,$16,16
or	$2,$3,$2
sll	$2,$2,$18
sltu	$3,$2,$13
beq	$3,$0,$L33
ori	$3,$2,0x1
clz	$3,$3
addiu	$17,$17,32
subu	$3,$14,$3
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$17,$3
andi	$16,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$16,$0,$L8
sw	$3,10340($4)
.set	macro
.set	reorder

subu	$2,$0,$2
$L8:
addu	$2,$8,$2
.set	noreorder
.set	nomacro
beq	$9,$0,$L34
andi	$2,$2,0xff
.set	macro
.set	reorder

lbu	$3,0($10)
.set	noreorder
.set	nomacro
beq	$2,$0,$L13
addu	$3,$5,$3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L14
andi	$8,$2,0x00ff
.set	macro
.set	reorder

$L33:
srl	$2,$2,23
addu	$3,$24,$2
addu	$2,$15,$2
lbu	$3,0($3)
lb	$2,0($2)
addu	$17,$3,$17
.set	noreorder
.set	nomacro
b	$L8
sw	$17,10340($4)
.set	macro
.set	reorder

$L20:
lw	$3,10340($4)
lw	$8,10332($4)
lw	$25,%got(zigzag_scan)($28)
srl	$2,$3,3
andi	$9,$3,0x7
addu	$8,$8,$2
addiu	$3,$3,1
addiu	$25,$25,%lo(zigzag_scan)
lbu	$2,0($8)
sll	$2,$2,$9
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L35
sw	$3,10340($4)
.set	macro
.set	reorder

$L3:
lw	$25,%call16(memcpy)($28)
move	$4,$5
move	$5,$10
$L30:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jr	$25
addiu	$sp,$sp,16
.set	macro
.set	reorder

$L34:
.set	noreorder
.set	nomacro
beq	$2,$0,$L17
andi	$8,$2,0x00ff
.set	macro
.set	reorder

lbu	$3,0($25)
.set	noreorder
.set	nomacro
b	$L14
addu	$3,$5,$3
.set	macro
.set	reorder

$L17:
move	$4,$5
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
b	$L30
move	$5,$7
.set	macro
.set	reorder

.end	decode_scaling_list
.size	decode_scaling_list, .-decode_scaling_list
.section	.text.decode_scaling_matrices.isra.6,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_scaling_matrices.isra.6
.type	decode_scaling_matrices.isra.6, @function
decode_scaling_matrices.isra.6:
.frame	$sp,88,$31		# vars= 16, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-88
.cprestore	24
sw	$19,60($sp)
sw	$31,84($sp)
sw	$fp,80($sp)
sw	$23,76($sp)
sw	$22,72($sp)
sw	$21,68($sp)
sw	$20,64($sp)
sw	$18,56($sp)
sw	$17,52($sp)
sw	$16,48($sp)
lw	$19,104($sp)
bne	$7,$0,$L44
lw	$9,108($sp)

lw	$2,664($5)
bne	$2,$0,$L39
addiu	$11,$5,668

$L44:
lw	$3,10340($4)
lw	$8,10332($4)
lw	$20,%got(default_scaling8)($28)
srl	$2,$3,3
lw	$10,%got(default_scaling4+16)($28)
andi	$12,$3,0x7
lw	$11,%got(default_scaling4)($28)
addu	$8,$8,$2
lw	$17,%got(default_scaling8+64)($28)
addiu	$3,$3,1
addiu	$20,$20,%lo(default_scaling8)
lbu	$2,0($8)
addiu	$21,$10,%lo(default_scaling4+16)
addiu	$11,$11,%lo(default_scaling4)
sw	$3,10340($4)
sll	$2,$2,$12
andi	$2,$2,0x00ff
srl	$2,$2,7
bne	$2,$0,$L49
addiu	$17,$17,%lo(default_scaling8+64)

$L37:
lw	$31,84($sp)
$L50:
lw	$fp,80($sp)
lw	$23,76($sp)
lw	$22,72($sp)
lw	$21,68($sp)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,88

$L39:
lw	$3,10340($4)
lw	$8,10332($4)
addiu	$21,$5,716
addiu	$20,$5,764
srl	$2,$3,3
andi	$12,$3,0x7
addu	$8,$8,$2
addiu	$3,$3,1
addiu	$17,$5,828
lbu	$2,0($8)
sll	$2,$2,$12
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L37
sw	$3,10340($4)

$L49:
lw	$18,%got(decode_scaling_list)($28)
move	$3,$7
lw	$2,664($5)
move	$8,$5
lw	$fp,%got(default_scaling4)($28)
addiu	$23,$19,16
addiu	$18,$18,%lo(decode_scaling_list)
lw	$22,%got(default_scaling4+16)($28)
or	$2,$2,$7
sw	$6,32($sp)
addiu	$7,$fp,%lo(default_scaling4)
sw	$11,16($sp)
li	$6,16			# 0x10
sw	$2,664($8)
move	$5,$19
sw	$3,36($sp)
sw	$9,40($sp)
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
move	$16,$4

addiu	$7,$fp,%lo(default_scaling4)
sw	$19,16($sp)
li	$6,16			# 0x10
move	$5,$23
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
move	$4,$16

addiu	$7,$fp,%lo(default_scaling4)
addiu	$5,$19,32
sw	$23,16($sp)
li	$6,16			# 0x10
move	$4,$16
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
addiu	$fp,$19,48

li	$6,16			# 0x10
addiu	$7,$22,%lo(default_scaling4+16)
sw	$21,16($sp)
move	$4,$16
move	$5,$fp
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
addiu	$23,$19,64

li	$6,16			# 0x10
addiu	$7,$22,%lo(default_scaling4+16)
sw	$fp,16($sp)
move	$4,$16
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
move	$5,$23

addiu	$5,$19,80
li	$6,16			# 0x10
sw	$23,16($sp)
addiu	$7,$22,%lo(default_scaling4+16)
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
move	$4,$16

lw	$3,36($sp)
lw	$28,24($sp)
bne	$3,$0,$L42
lw	$9,40($sp)

lw	$3,32($sp)
lw	$2,0($3)
beq	$2,$0,$L50
lw	$31,84($sp)

$L42:
lw	$7,%got(default_scaling8)($28)
li	$6,64			# 0x40
move	$4,$16
sw	$20,16($sp)
move	$5,$9
sw	$9,40($sp)
move	$25,$18
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jalr	$25
addiu	$7,$7,%lo(default_scaling8)

move	$4,$16
lw	$28,24($sp)
move	$25,$18
lw	$9,40($sp)
li	$6,64			# 0x40
lw	$31,84($sp)
lw	$fp,80($sp)
lw	$7,%got(default_scaling8+64)($28)
addiu	$5,$9,64
lw	$23,76($sp)
lw	$22,72($sp)
lw	$21,68($sp)
addiu	$7,$7,%lo(default_scaling8+64)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$16,48($sp)
sw	$17,104($sp)
lw	$17,52($sp)
.reloc	1f,R_MIPS_JALR,decode_scaling_list
1:	jr	$25
addiu	$sp,$sp,88

.set	macro
.set	reorder
.end	decode_scaling_matrices.isra.6
.size	decode_scaling_matrices.isra.6, .-decode_scaling_matrices.isra.6
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"PIC-AFF\000"
.align	2
$LC1:
.ascii	"FRM\000"
.align	2
$LC2:
.ascii	"MB-AFF\000"
.align	2
$LC3:
.ascii	"8B8\000"
.align	2
$LC4:
.ascii	"\000"
.align	2
$LC5:
.ascii	"VUI\000"
.align	2
$LC6:
.ascii	"sps_id (%d) out of range\012\000"
.align	2
$LC7:
.ascii	"poc_cycle_length overflow %u\012\000"
.align	2
$LC8:
.ascii	"illegal POC type %d\012\000"
.align	2
$LC9:
.ascii	"too many reference frames\012\000"
.align	2
$LC10:
.ascii	"mb_width/height overflow\012\000"
.align	2
$LC11:
.ascii	"This stream was generated by a broken encoder, invalid 8"
.ascii	"x8 inference\012\000"
.align	2
$LC12:
.ascii	"insane cropping not completely supported, this could loo"
.ascii	"k slightly wrong ...\012\000"
.align	2
$LC13:
.ascii	"brainfart cropping not supported, this could look slight"
.ascii	"ly wrong ...\012\000"
.align	2
$LC14:
.ascii	"illegal aspect ratio\012\000"
.align	2
$LC15:
.ascii	"time_scale/num_units_in_tick invalid or unsupported (%d/"
.ascii	"%d)\012\000"
.align	2
$LC16:
.ascii	"cpb_count %d invalid\012\000"
.align	2
$LC17:
.ascii	"Overread VUI by %d bits\012\000"
.align	2
$LC18:
.ascii	"illegal num_reorder_frames %d\012\000"
.align	2
$LC19:
.ascii	"Gray\000"
.align	2
$LC20:
.ascii	"420\000"
.align	2
$LC21:
.ascii	"422\000"
.align	2
$LC22:
.ascii	"444\000"
.align	2
$LC23:
.ascii	"sps:%u profile:%d/%d poc:%d ref:%d %dx%d %s %s crop:%d/%"
.ascii	"d/%d/%d %s %s %d/%d\012\000"
.section	.text.ff_h264_decode_seq_parameter_set,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_seq_parameter_set
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_seq_parameter_set
.type	ff_h264_decode_seq_parameter_set, @function
ff_h264_decode_seq_parameter_set:
.frame	$sp,152,$31		# vars= 24, regs= 10/0, args= 80, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$9,10340($4)
addiu	$sp,$sp,-152
lw	$2,10332($4)
sw	$22,136($sp)
li	$22,16711680			# 0xff0000
addiu	$8,$9,24
sw	$18,120($sp)
addiu	$7,$9,16
sw	$21,132($sp)
move	$18,$4
.cprestore	80
srl	$5,$9,3
sw	$fp,144($sp)
srl	$4,$7,3
sw	$23,140($sp)
srl	$3,$8,3
sw	$20,128($sp)
li	$21,-16777216			# 0xffffffffff000000
sw	$17,116($sp)
sw	$16,112($sp)
addiu	$22,$22,255
sw	$31,148($sp)
addu	$5,$2,$5
sw	$19,124($sp)
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
addu	$3,$2,$3
sw	$7,10340($18)
move	$5,$6
lw	$20,%got(ff_golomb_vlc_len)($28)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($4)  
lwr $10, 0($4)  

# 0 "" 2
#NO_APP
sw	$8,10340($18)
ori	$21,$21,0xff00
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$6,$4,8
lw	$17,%got(ff_ue_golomb_vlc_code)($28)
sll	$4,$4,8
and	$6,$6,$22
and	$4,$4,$21
or	$4,$6,$4
sll	$3,$4,16
srl	$4,$4,16
andi	$11,$8,0x7
or	$2,$3,$4
sll	$2,$2,$11
srl	$6,$5,8
srl	$2,$2,23
srl	$4,$10,8
sll	$3,$10,8
sll	$5,$5,8
addu	$10,$20,$2
addu	$2,$17,$2
and	$4,$4,$22
and	$6,$6,$22
lbu	$10,0($10)
and	$5,$5,$21
lbu	$16,0($2)
and	$3,$3,$21
or	$3,$4,$3
or	$5,$6,$5
sll	$2,$3,16
sll	$4,$5,16
srl	$3,$3,16
srl	$5,$5,16
or	$3,$2,$3
or	$5,$4,$5
andi	$9,$9,0x7
andi	$7,$7,0x7
addu	$8,$10,$8
sll	$5,$5,$9
sll	$3,$3,$7
sltu	$2,$16,32
sw	$8,10340($18)
srl	$fp,$5,24
.set	noreorder
.set	nomacro
beq	$2,$0,$L215
srl	$23,$3,24
.set	macro
.set	reorder

lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
li	$4,936			# 0x3a8
.set	macro
.set	reorder

lw	$28,80($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L168
move	$19,$2
.set	macro
.set	reorder

addiu	$8,$2,668
lw	$25,%call16(memset)($28)
li	$5,16			# 0x10
sw	$23,4($2)
li	$6,96			# 0x60
sw	$fp,0($2)
move	$4,$8
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$8,104($sp)
.set	macro
.set	reorder

addiu	$23,$19,764
lw	$28,80($sp)
li	$5,16			# 0x10
li	$6,128			# 0x80
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

lw	$2,0($19)
lw	$28,80($sp)
lw	$8,104($sp)
slt	$2,$2,100
.set	noreorder
.set	nomacro
beq	$2,$0,$L216
sw	$0,664($19)
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,1			# 0x1
sw	$2,924($19)
sw	$3,8($19)
sw	$2,928($19)
$L60:
lw	$9,10340($18)
lw	$5,10332($18)
srl	$2,$9,3
addu	$2,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$4,$6,8
addiu	$2,$2,255
sll	$6,$6,8
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$4,$9,0x7
ori	$2,$2,0xff00
and	$2,$6,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$4
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L217
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$4,$3
li	$3,31			# 0x1f
addiu	$9,$9,32
subu	$3,$3,$4
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$6,$9,$3
addiu	$2,$2,-1
sw	$6,10340($18)
$L62:
addiu	$2,$2,4
lw	$8,%got(ff_golomb_vlc_len)($28)
srl	$3,$6,3
li	$4,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
sw	$2,16($19)
addiu	$11,$4,255
addu	$2,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$4,$3,8
srl	$2,$3,8
ori	$10,$10,0xff00
and	$3,$2,$11
and	$2,$4,$10
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$6,0x7
or	$2,$3,$2
sll	$2,$2,$4
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$9,0($3)
lbu	$7,0($2)
addu	$9,$9,$6
sw	$7,20($19)
.set	noreorder
.set	nomacro
beq	$7,$0,$L218
sw	$9,10340($18)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$7,$2,$L219
li	$2,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$7,$2,$L220
lw	$6,%got($LC8)($28)
.set	macro
.set	reorder

$L66:
srl	$2,$9,3
li	$4,16711680			# 0xff0000
li	$8,-16777216			# 0xffffffffff000000
addu	$2,$5,$2
addiu	$4,$4,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$6,8
sll	$6,$6,8
ori	$8,$8,0xff00
and	$3,$2,$4
and	$2,$6,$8
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$9,0x7
or	$2,$3,$2
sll	$2,$2,$6
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$9,$3,$9
sltu	$3,$2,31
sw	$2,44($19)
.set	noreorder
.set	nomacro
beq	$3,$0,$L221
sw	$9,10340($18)
.set	macro
.set	reorder

srl	$2,$9,3
andi	$6,$9,0x7
addu	$2,$5,$2
addiu	$10,$9,1
lbu	$7,0($2)
srl	$3,$10,3
sw	$10,10340($18)
andi	$11,$10,0x7
addu	$3,$5,$3
sll	$6,$7,$6
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,48($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$6,8
sll	$6,$6,8
and	$3,$3,$4
and	$4,$6,$8
or	$4,$3,$4
sll	$3,$4,16
srl	$2,$4,16
or	$2,$3,$2
sll	$2,$2,$11
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L222
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$4,$3
li	$3,31			# 0x1f
addiu	$9,$9,33
subu	$3,$3,$4
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$9,$9,$3
addiu	$4,$2,-1
sw	$9,10340($18)
$L89:
addiu	$4,$4,1
srl	$2,$9,3
sw	$4,52($19)
addu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
#NO_APP
sll	$2,$3,8
srl	$6,$3,8
li	$3,16711680			# 0xff0000
addiu	$3,$3,255
and	$5,$6,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$2,$3
or	$3,$5,$3
sll	$5,$3,16
srl	$3,$3,16
andi	$2,$9,0x7
or	$3,$5,$3
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L223
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$5,$3
li	$3,31			# 0x1f
addiu	$9,$9,32
subu	$3,$3,$5
sll	$3,$3,1
addiu	$3,$3,-31
srl	$5,$2,$3
subu	$9,$9,$3
addiu	$5,$5,-1
sw	$9,10340($18)
$L91:
li	$2,134152192			# 0x7ff0000
addiu	$5,$5,1
ori	$2,$2,0xffff
sltu	$3,$4,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L93
sw	$5,56($19)
.set	macro
.set	reorder

sltu	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L93
lw	$25,%call16(av_image_check_size)($28)
.set	macro
.set	reorder

sll	$4,$4,4
lw	$7,0($18)
sll	$5,$5,4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_image_check_size
1:	jalr	$25
move	$6,$0
.set	macro
.set	reorder

lw	$28,80($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L93
move	$21,$2
.set	macro
.set	reorder

lw	$3,10340($18)
lw	$6,10332($18)
srl	$5,$3,3
andi	$7,$3,0x7
addu	$5,$6,$5
addiu	$4,$3,1
lbu	$2,0($5)
sw	$4,10340($18)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L224
sw	$2,60($19)
.set	macro
.set	reorder

sw	$0,64($19)
$L95:
srl	$7,$4,3
andi	$3,$4,0x7
addu	$7,$6,$7
addiu	$5,$4,1
lbu	$7,0($7)
sw	$5,10340($18)
sll	$3,$7,$3
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L96
sw	$3,68($19)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L225
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

$L96:
srl	$2,$5,3
andi	$3,$5,0x7
addu	$5,$6,$2
addiu	$2,$4,2
lbu	$5,0($5)
sw	$2,10340($18)
sll	$5,$5,$3
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
bne	$5,$0,$L226
sw	$5,72($19)
.set	macro
.set	reorder

sw	$0,88($19)
sw	$0,84($19)
sw	$0,80($19)
sw	$0,76($19)
$L110:
srl	$3,$2,3
andi	$4,$2,0x7
addu	$3,$6,$3
addiu	$7,$2,1
lbu	$5,0($3)
sw	$7,10340($18)
sll	$4,$5,$4
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L227
sw	$4,92($19)
.set	macro
.set	reorder

lw	$4,0($18)
$L167:
lw	$2,404($4)
andi	$2,$2,0x1
beq	$2,$0,$L161
lw	$2,60($19)
lw	$6,0($19)
lw	$22,4($19)
lw	$20,20($19)
lw	$17,44($19)
lw	$15,52($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L170
lw	$14,56($19)
.set	macro
.set	reorder

lw	$2,64($19)
beq	$2,$0,$L228
lw	$9,%got($LC2)($28)
lw	$2,68($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L229
addiu	$9,$9,%lo($LC2)
.set	macro
.set	reorder

lw	$8,%got($LC3)($28)
$L264:
lw	$2,92($19)
lw	$13,76($19)
addiu	$8,$8,%lo($LC3)
lw	$12,80($19)
lw	$11,84($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L230
lw	$10,88($19)
.set	macro
.set	reorder

$L173:
lw	$3,%got($LC5)($28)
.set	noreorder
.set	nomacro
b	$L164
addiu	$3,$3,%lo($LC5)
.set	macro
.set	reorder

$L224:
srl	$5,$4,3
andi	$7,$4,0x7
addu	$5,$6,$5
addiu	$4,$3,2
lbu	$3,0($5)
sw	$4,10340($18)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
b	$L95
sw	$3,64($19)
.set	macro
.set	reorder

$L170:
lw	$9,%got($LC1)($28)
addiu	$9,$9,%lo($LC1)
$L162:
lw	$2,68($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L264
lw	$8,%got($LC3)($28)
.set	macro
.set	reorder

$L229:
lw	$8,%got($LC4)($28)
lw	$2,92($19)
lw	$13,76($19)
addiu	$8,$8,%lo($LC4)
lw	$12,80($19)
lw	$11,84($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L173
lw	$10,88($19)
.set	macro
.set	reorder

$L230:
lw	$3,%got($LC4)($28)
addiu	$3,$3,%lo($LC4)
$L164:
lw	$5,%got($LC19)($28)
lw	$2,8($19)
lw	$7,128($19)
addiu	$5,$5,%lo($LC19)
sll	$2,$2,2
sw	$5,88($sp)
addu	$2,$sp,$2
lw	$5,%got($LC20)($28)
addiu	$5,$5,%lo($LC20)
sw	$5,92($sp)
lw	$5,%got($LC21)($28)
addiu	$5,$5,%lo($LC21)
sw	$5,96($sp)
lw	$5,%got($LC22)($28)
addiu	$5,$5,%lo($LC22)
sw	$5,100($sp)
.set	noreorder
.set	nomacro
beq	$7,$0,$L174
lw	$23,88($2)
.set	macro
.set	reorder

lw	$24,132($19)
lw	$2,136($19)
$L165:
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
sw	$6,16($sp)
move	$7,$16
sw	$22,20($sp)
sw	$20,24($sp)
sw	$17,28($sp)
sw	$15,32($sp)
sw	$14,36($sp)
sw	$9,40($sp)
sw	$8,44($sp)
sw	$13,48($sp)
sw	$12,52($sp)
sw	$11,56($sp)
sw	$10,60($sp)
sw	$3,64($sp)
sw	$23,68($sp)
sw	$24,72($sp)
sw	$2,76($sp)
lw	$6,%got($LC23)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC23)
.set	macro
.set	reorder

lw	$28,80($sp)
$L161:
sll	$16,$16,2
lw	$25,%call16(av_free)($28)
li	$2,131072			# 0x20000
addu	$16,$18,$16
addiu	$18,$18,11888
addu	$16,$16,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
lw	$4,9500($16)
.set	macro
.set	reorder

addiu	$3,$19,928
move	$2,$19
sw	$19,9500($16)
$L166:
lw	$7,0($2)
addiu	$2,$2,16
addiu	$18,$18,16
lw	$6,-12($2)
lw	$5,-8($2)
lw	$4,-4($2)
sw	$7,-16($18)
sw	$6,-12($18)
sw	$5,-8($18)
.set	noreorder
.set	nomacro
bne	$2,$3,$L166
sw	$4,-4($18)
.set	macro
.set	reorder

lw	$3,0($2)
lw	$2,4($2)
sw	$3,0($18)
sw	$2,4($18)
$L53:
lw	$31,148($sp)
$L263:
move	$2,$21
lw	$fp,144($sp)
lw	$23,140($sp)
lw	$22,136($sp)
lw	$21,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

$L217:
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$6,0($3)
lbu	$2,0($2)
addu	$6,$6,$9
.set	noreorder
.set	nomacro
b	$L62
sw	$6,10340($18)
.set	macro
.set	reorder

$L223:
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$3,0($3)
lbu	$5,0($2)
addu	$9,$3,$9
.set	noreorder
.set	nomacro
b	$L91
sw	$9,10340($18)
.set	macro
.set	reorder

$L222:
lw	$4,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$3,%got(ff_ue_golomb_vlc_code)($28)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$9,0($4)
lbu	$4,0($2)
addu	$9,$9,$10
.set	noreorder
.set	nomacro
b	$L89
sw	$9,10340($18)
.set	macro
.set	reorder

$L216:
lw	$5,10340($18)
lw	$6,10332($18)
srl	$4,$5,3
andi	$7,$5,0x7
addu	$4,$6,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($4)  
lwr $2, 0($4)  

# 0 "" 2
#NO_APP
srl	$3,$2,8
sll	$4,$2,8
and	$22,$3,$22
and	$21,$4,$21
or	$21,$22,$21
sll	$3,$21,16
srl	$21,$21,16
or	$2,$3,$21
sll	$2,$2,$7
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$9,$3,$5
li	$3,3			# 0x3
sw	$2,8($19)
.set	noreorder
.set	nomacro
beq	$2,$3,$L231
sw	$9,10340($18)
.set	macro
.set	reorder

$L55:
srl	$2,$9,3
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$4,$3,8
addiu	$2,$2,255
sll	$5,$3,8
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$4,$2,16
srl	$3,$2,16
andi	$2,$9,0x7
or	$3,$4,$3
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L232
li	$4,31			# 0x1f
.set	macro
.set	reorder

ori	$7,$2,0x1
clz	$7,$7
addiu	$3,$9,32
subu	$7,$4,$7
sll	$7,$7,1
addiu	$7,$7,-31
srl	$2,$2,$7
subu	$7,$3,$7
addiu	$2,$2,-1
sw	$7,10340($18)
$L57:
addiu	$2,$2,8
srl	$3,$7,3
sw	$2,924($19)
addu	$2,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$5,$4,8
addiu	$2,$2,255
sll	$3,$4,8
and	$4,$5,$2
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$2,$3,$2
or	$2,$4,$2
sll	$5,$2,16
srl	$4,$2,16
andi	$2,$7,0x7
or	$3,$5,$4
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L233
ori	$4,$2,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$3,$7,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$2,$2,$4
subu	$3,$3,$4
addiu	$2,$2,-1
sw	$3,10340($18)
$L59:
srl	$4,$3,3
lw	$25,%got(decode_scaling_matrices.isra.6)($28)
addiu	$2,$2,8
addu	$6,$6,$4
andi	$4,$3,0x7
sw	$2,928($19)
addiu	$3,$3,1
lbu	$2,0($6)
li	$7,1			# 0x1
li	$6,64			# 0x40
sw	$8,16($sp)
addiu	$25,$25,%lo(decode_scaling_matrices.isra.6)
sw	$3,10340($18)
sll	$2,$2,$4
sw	$23,20($sp)
move	$4,$18
andi	$2,$2,0x00ff
srl	$2,$2,7
move	$5,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_scaling_matrices.isra.6
1:	jalr	$25
sw	$2,12($19)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L60
lw	$28,80($sp)
.set	macro
.set	reorder

$L218:
srl	$3,$9,3
andi	$2,$9,0x7
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sll	$3,$3,8
srl	$4,$4,8
and	$3,$3,$10
and	$4,$4,$11
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L234
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$4,$3
li	$3,31			# 0x1f
addiu	$9,$9,32
subu	$3,$3,$4
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$9,$9,$3
addiu	$2,$2,-1
addiu	$2,$2,4
sw	$9,10340($18)
.set	noreorder
.set	nomacro
b	$L66
sw	$2,24($19)
.set	macro
.set	reorder

$L174:
move	$24,$0
.set	noreorder
.set	nomacro
b	$L165
move	$2,$0
.set	macro
.set	reorder

$L227:
srl	$4,$7,3
andi	$7,$7,0x7
addu	$4,$6,$4
addiu	$3,$2,2
lbu	$4,0($4)
sll	$7,$4,$7
andi	$4,$7,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L235
sw	$3,10340($18)
.set	macro
.set	reorder

sw	$0,100($19)
sw	$0,96($19)
$L114:
srl	$4,$3,3
andi	$2,$3,0x7
addu	$4,$6,$4
lbu	$4,0($4)
sll	$2,$4,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L117
addiu	$5,$3,2
.set	macro
.set	reorder

addiu	$5,$3,1
sw	$5,10340($18)
$L118:
srl	$4,$5,3
andi	$2,$5,0x7
addu	$4,$6,$4
addiu	$7,$5,1
lbu	$3,0($4)
sw	$7,10340($18)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L236
sw	$2,104($19)
.set	macro
.set	reorder

$L119:
srl	$4,$7,3
$L266:
andi	$3,$7,0x7
addu	$4,$6,$4
addiu	$5,$7,1
lbu	$4,0($4)
sll	$3,$4,$3
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L122
sw	$5,10340($18)
.set	macro
.set	reorder

lw	$4,0($18)
$L123:
srl	$3,$5,3
andi	$2,$5,0x7
addu	$3,$6,$3
addiu	$7,$5,1
lbu	$3,0($3)
sw	$7,10340($18)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L237
sw	$2,128($19)
.set	macro
.set	reorder

$L127:
srl	$2,$7,3
andi	$8,$7,0x7
addu	$2,$6,$2
addiu	$7,$7,1
lbu	$3,0($2)
move	$2,$7
sw	$7,10340($18)
sll	$8,$3,$8
andi	$8,$8,0x00ff
srl	$8,$8,7
.set	noreorder
.set	nomacro
bne	$8,$0,$L238
sw	$8,892($19)
.set	macro
.set	reorder

$L130:
srl	$3,$2,3
andi	$9,$2,0x7
addu	$3,$6,$3
addiu	$5,$2,1
lbu	$3,0($3)
move	$2,$5
sw	$5,10340($18)
sll	$9,$3,$9
andi	$9,$9,0x00ff
srl	$9,$9,7
.set	noreorder
.set	nomacro
bne	$9,$0,$L239
sw	$9,896($19)
.set	macro
.set	reorder

$L137:
bne	$8,$0,$L144
bne	$9,$0,$L144
$L145:
srl	$5,$2,3
addiu	$3,$2,1
addu	$5,$6,$5
andi	$10,$2,0x7
srl	$9,$3,3
lbu	$7,0($5)
andi	$5,$3,0x7
sw	$3,10340($18)
addu	$9,$6,$9
addiu	$8,$2,2
sll	$3,$7,$10
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,900($19)
lbu	$3,0($9)
sw	$8,10340($18)
sll	$3,$3,$5
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L167
sw	$3,656($19)
.set	macro
.set	reorder

addiu	$7,$2,3
li	$8,16711680			# 0xff0000
srl	$3,$7,3
addiu	$8,$8,255
sw	$7,10340($18)
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$5,$5,$8
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$8,$7,0x7
or	$3,$5,$3
sll	$3,$3,$8
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L240
li	$5,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$3,0x1
clz	$3,$3
subu	$3,$5,$3
sll	$3,$3,1
subu	$2,$2,$3
addiu	$2,$2,66
sw	$2,10340($18)
$L148:
srl	$3,$2,3
li	$7,16711680			# 0xff0000
addu	$3,$6,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$5,$5,$7
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$7,$2,0x7
or	$5,$5,$3
sll	$3,$5,$7
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L241
li	$5,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$3,0x1
clz	$3,$3
subu	$3,$5,$3
sll	$3,$3,1
subu	$2,$2,$3
addiu	$2,$2,63
sw	$2,10340($18)
$L150:
srl	$3,$2,3
li	$7,16711680			# 0xff0000
addu	$3,$6,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$5,$5,$7
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$7,$2,0x7
or	$5,$5,$3
sll	$3,$5,$7
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L242
li	$5,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$3,0x1
clz	$3,$3
subu	$3,$5,$3
sll	$3,$3,1
subu	$2,$2,$3
addiu	$2,$2,63
sw	$2,10340($18)
$L152:
srl	$3,$2,3
li	$7,16711680			# 0xff0000
addu	$3,$6,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$5,$5,$7
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$7,$2,0x7
or	$5,$5,$3
sll	$3,$5,$7
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L243
li	$5,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$3,0x1
clz	$3,$3
subu	$3,$5,$3
sll	$3,$3,1
subu	$2,$2,$3
addiu	$2,$2,63
sw	$2,10340($18)
$L154:
srl	$3,$2,3
li	$7,16711680			# 0xff0000
addu	$3,$6,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$5,$5,$7
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$7,$2,0x7
or	$5,$5,$3
sll	$3,$5,$7
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L244
ori	$5,$3,0x1
.set	macro
.set	reorder

clz	$7,$5
li	$5,31			# 0x1f
addiu	$2,$2,32
subu	$5,$5,$7
sll	$5,$5,1
addiu	$5,$5,-31
srl	$3,$3,$5
subu	$2,$2,$5
addiu	$7,$3,-1
sw	$2,10340($18)
$L156:
srl	$3,$2,3
sw	$7,660($19)
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
sll	$3,$6,8
srl	$5,$6,8
li	$6,16711680			# 0xff0000
addiu	$6,$6,255
and	$5,$5,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$6,$3,$6
or	$6,$5,$6
sll	$5,$6,16
srl	$6,$6,16
andi	$3,$2,0x7
or	$5,$5,$6
sll	$3,$5,$3
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L245
li	$5,31			# 0x1f
.set	macro
.set	reorder

ori	$3,$3,0x1
clz	$3,$3
subu	$3,$5,$3
sll	$3,$3,1
subu	$2,$2,$3
addiu	$2,$2,63
move	$3,$2
sw	$2,10340($18)
$L158:
lw	$2,10344($18)
slt	$5,$2,$3
.set	noreorder
.set	nomacro
bne	$5,$0,$L159
lw	$6,%got($LC17)($28)
.set	macro
.set	reorder

sltu	$2,$7,17
.set	noreorder
.set	nomacro
bne	$2,$0,$L167
lw	$6,%got($LC18)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L226:
srl	$3,$2,3
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
srl	$7,$8,8
addiu	$3,$3,255
sll	$8,$8,8
and	$5,$7,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$8,$3
or	$3,$5,$3
sll	$7,$3,16
srl	$5,$3,16
andi	$3,$2,0x7
or	$5,$7,$5
sll	$3,$5,$3
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L246
addiu	$4,$4,34
.set	macro
.set	reorder

ori	$2,$3,0x1
clz	$5,$2
li	$2,31			# 0x1f
subu	$2,$2,$5
sll	$2,$2,1
addiu	$2,$2,-31
srl	$3,$3,$2
subu	$2,$4,$2
addiu	$7,$3,-1
sw	$2,10340($18)
$L99:
srl	$3,$2,3
sw	$7,76($19)
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
srl	$5,$4,8
addiu	$3,$3,255
sll	$8,$4,8
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$8,$3
or	$3,$4,$3
sll	$5,$3,16
srl	$4,$3,16
andi	$3,$2,0x7
or	$4,$5,$4
sll	$3,$4,$3
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L247
ori	$4,$3,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$2,$2,$4
addiu	$3,$3,-1
sw	$2,10340($18)
$L101:
move	$9,$3
li	$4,16711680			# 0xff0000
srl	$3,$2,3
sw	$9,80($19)
addiu	$4,$4,255
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$5
srl	$5,$5,8
sll	$8,$3,8
and	$3,$5,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$4,$8,$4
or	$4,$3,$4
sll	$8,$4,16
srl	$5,$4,16
andi	$4,$2,0x7
or	$5,$8,$5
sll	$4,$5,$4
li	$3,134217728			# 0x8000000
sltu	$3,$4,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L248
ori	$3,$4,0x1
.set	macro
.set	reorder

clz	$5,$3
li	$3,31			# 0x1f
addiu	$2,$2,32
subu	$3,$3,$5
sll	$3,$3,1
addiu	$3,$3,-31
srl	$4,$4,$3
subu	$2,$2,$3
addiu	$8,$4,-1
sw	$2,10340($18)
$L103:
srl	$3,$2,3
sw	$8,84($19)
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$6
sll	$4,$3,8
li	$3,16711680			# 0xff0000
srl	$6,$6,8
addiu	$3,$3,255
and	$5,$6,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$4,$3
or	$3,$5,$3
sll	$5,$3,16
srl	$4,$3,16
andi	$3,$2,0x7
or	$4,$5,$4
sll	$3,$4,$3
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L249
ori	$4,$3,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$2,$2,$4
addiu	$3,$3,-1
sw	$2,10340($18)
$L105:
.set	noreorder
.set	nomacro
bne	$7,$0,$L106
sw	$3,88($19)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$8,$0,$L265
sltu	$3,$9,8
.set	macro
.set	reorder

$L106:
lw	$6,%got($LC12)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC12)
.set	macro
.set	reorder

lw	$28,80($sp)
lw	$9,80($19)
sltu	$3,$9,8
$L265:
.set	noreorder
.set	nomacro
beq	$3,$0,$L108
li	$5,4			# 0x4
.set	macro
.set	reorder

lw	$3,60($19)
li	$2,8			# 0x8
lw	$4,88($19)
movz	$2,$5,$3
sltu	$2,$4,$2
bne	$2,$0,$L211
$L108:
lw	$6,%got($LC13)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)
.set	macro
.set	reorder

lw	$28,80($sp)
$L211:
lw	$2,10340($18)
.set	noreorder
.set	nomacro
b	$L110
lw	$6,10332($18)
.set	macro
.set	reorder

$L228:
lw	$9,%got($LC0)($28)
.set	noreorder
.set	nomacro
b	$L162
addiu	$9,$9,%lo($LC0)
.set	macro
.set	reorder

$L234:
lw	$3,%got(ff_ue_golomb_vlc_code)($28)
srl	$2,$2,23
addu	$8,$8,$2
addu	$2,$3,$2
lbu	$3,0($8)
lbu	$2,0($2)
addu	$9,$3,$9
addiu	$2,$2,4
sw	$9,10340($18)
.set	noreorder
.set	nomacro
b	$L66
sw	$2,24($19)
.set	macro
.set	reorder

$L232:
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$7,$3,$9
.set	noreorder
.set	nomacro
b	$L57
sw	$7,10340($18)
.set	macro
.set	reorder

$L246:
srl	$3,$3,23
addu	$4,$20,$3
addu	$3,$17,$3
lbu	$4,0($4)
lbu	$7,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L99
sw	$2,10340($18)
.set	macro
.set	reorder

$L231:
srl	$3,$9,3
andi	$4,$9,0x7
addu	$3,$6,$3
addiu	$9,$9,1
lbu	$2,0($3)
sw	$9,10340($18)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
b	$L55
sw	$2,932($19)
.set	macro
.set	reorder

$L235:
srl	$4,$3,3
li	$8,16711680			# 0xff0000
li	$7,-16777216			# 0xffffffffff000000
addu	$4,$6,$4
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
ori	$7,$7,0xff00
and	$5,$5,$7
and	$4,$4,$8
or	$4,$4,$5
sll	$5,$4,16
srl	$4,$4,16
andi	$3,$3,0x7
or	$4,$5,$4
sll	$4,$4,$3
addiu	$3,$2,10
srl	$4,$4,24
li	$5,255			# 0xff
.set	noreorder
.set	nomacro
beq	$4,$5,$L250
sw	$3,10340($18)
.set	macro
.set	reorder

sltu	$2,$4,17
.set	noreorder
.set	nomacro
beq	$2,$0,$L115
lw	$2,%got(pixel_aspect)($28)
.set	macro
.set	reorder

sll	$4,$4,3
addiu	$2,$2,%lo(pixel_aspect)
addu	$2,$4,$2
lw	$4,0($2)
lw	$2,4($2)
sw	$4,96($19)
.set	noreorder
.set	nomacro
b	$L114
sw	$2,100($19)
.set	macro
.set	reorder

$L239:
srl	$2,$5,3
li	$12,16711680			# 0xff0000
li	$11,-16777216			# 0xffffffffff000000
addu	$2,$6,$2
addiu	$12,$12,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
ori	$11,$11,0xff00
and	$3,$3,$11
and	$2,$2,$12
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$7,$5,0x7
or	$2,$3,$2
sll	$2,$2,$7
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$15,0($3)
lbu	$7,0($2)
addu	$2,$15,$5
addiu	$7,$7,1
slt	$3,$7,33
.set	noreorder
.set	nomacro
beq	$3,$0,$L212
sw	$2,10340($18)
.set	macro
.set	reorder

addiu	$15,$2,8
move	$10,$0
li	$14,134217728			# 0x8000000
li	$13,31			# 0x1f
.set	noreorder
.set	nomacro
b	$L143
sw	$15,10340($18)
.set	macro
.set	reorder

$L139:
sll	$3,$3,1
subu	$3,$15,$3
addiu	$3,$3,63
sw	$3,10340($18)
$L140:
srl	$2,$3,3
andi	$22,$3,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($2)  
lwr $15, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$15,8
sll	$15,$15,8
and	$5,$5,$12
and	$15,$15,$11
or	$2,$5,$15
sll	$5,$2,16
srl	$2,$2,16
or	$5,$5,$2
sll	$2,$5,$22
ori	$5,$2,0x1
clz	$5,$5
sltu	$15,$2,$14
.set	noreorder
.set	nomacro
beq	$15,$0,$L251
subu	$5,$13,$5
.set	macro
.set	reorder

sll	$5,$5,1
subu	$5,$3,$5
addiu	$5,$5,63
$L142:
addiu	$23,$5,1
addiu	$10,$10,1
move	$15,$23
slt	$22,$10,$7
.set	noreorder
.set	nomacro
beq	$22,$0,$L252
sw	$23,10340($18)
.set	macro
.set	reorder

$L143:
srl	$2,$15,3
andi	$22,$15,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
sll	$3,$3,8
and	$5,$5,$12
and	$3,$3,$11
or	$2,$5,$3
sll	$5,$2,16
srl	$2,$2,16
or	$5,$5,$2
sll	$2,$5,$22
ori	$3,$2,0x1
clz	$3,$3
sltu	$5,$2,$14
.set	noreorder
.set	nomacro
bne	$5,$0,$L139
subu	$3,$13,$3
.set	macro
.set	reorder

srl	$2,$2,23
addu	$2,$20,$2
lbu	$3,0($2)
addu	$3,$3,$15
.set	noreorder
.set	nomacro
b	$L140
sw	$3,10340($18)
.set	macro
.set	reorder

$L144:
addiu	$2,$2,1
.set	noreorder
.set	nomacro
b	$L145
sw	$2,10340($18)
.set	macro
.set	reorder

$L251:
srl	$2,$2,23
addu	$2,$20,$2
lbu	$5,0($2)
.set	noreorder
.set	nomacro
b	$L142
addu	$5,$5,$3
.set	macro
.set	reorder

$L93:
lw	$6,%got($LC10)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

lw	$28,80($sp)
$L81:
lw	$25,%call16(av_free)($28)
move	$4,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
li	$21,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

move	$2,$21
lw	$31,148($sp)
lw	$fp,144($sp)
lw	$23,140($sp)
lw	$22,136($sp)
lw	$21,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

$L117:
.set	noreorder
.set	nomacro
b	$L118
sw	$5,10340($18)
.set	macro
.set	reorder

$L233:
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$3,$3,$7
.set	noreorder
.set	nomacro
b	$L59
sw	$3,10340($18)
.set	macro
.set	reorder

$L219:
srl	$2,$9,3
andi	$3,$9,0x7
addu	$2,$5,$2
addiu	$6,$9,1
lbu	$4,0($2)
srl	$7,$6,3
sw	$6,10340($18)
andi	$12,$6,0x7
addu	$7,$5,$7
sll	$3,$4,$3
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,28($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($7)  
lwr $3, 0($7)  

# 0 "" 2
#NO_APP
srl	$4,$3,8
sll	$3,$3,8
and	$4,$4,$11
and	$10,$3,$10
or	$4,$4,$10
sll	$3,$4,16
srl	$2,$4,16
or	$2,$3,$2
sll	$2,$2,$12
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L253
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$8,$3
li	$3,31			# 0x1f
addiu	$4,$9,33
subu	$3,$3,$8
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$7,$4,$3
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
bne	$3,$0,$L254
sw	$7,10340($18)
.set	macro
.set	reorder

$L69:
srl	$3,$7,3
sw	$2,32($19)
li	$4,16711680			# 0xff0000
addu	$2,$5,$3
addiu	$4,$4,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$3,$3,$4
or	$2,$2,$3
sll	$3,$2,16
srl	$6,$2,16
andi	$2,$7,0x7
or	$3,$3,$6
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L255
addiu	$3,$7,32
.set	macro
.set	reorder

ori	$4,$2,0x1
clz	$6,$4
li	$4,31			# 0x1f
subu	$4,$4,$6
sll	$4,$4,1
addiu	$4,$4,-31
srl	$2,$2,$4
subu	$4,$3,$4
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
bne	$3,$0,$L256
sw	$4,10340($18)
.set	macro
.set	reorder

$L73:
srl	$3,$4,3
sw	$2,36($19)
li	$6,16711680			# 0xff0000
addu	$2,$5,$3
addiu	$6,$6,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$6,$2,16
andi	$2,$4,0x7
or	$3,$3,$6
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L257
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$6,$3
li	$3,31			# 0x1f
addiu	$4,$4,32
subu	$3,$3,$6
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$4,$3
addiu	$7,$2,-1
sw	$3,10340($18)
$L77:
sltu	$2,$7,256
.set	noreorder
.set	nomacro
beq	$2,$0,$L78
sw	$7,40($19)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$0,$L258
addiu	$4,$7,72
.set	macro
.set	reorder

lw	$9,10340($18)
li	$11,16711680			# 0xff0000
lw	$14,%got(ff_se_golomb_vlc_code)($28)
sll	$4,$4,1
li	$10,-16777216			# 0xffffffffff000000
addiu	$2,$19,144
addu	$4,$19,$4
addiu	$11,$11,255
li	$13,134217728			# 0x8000000
li	$12,31			# 0x1f
.set	noreorder
.set	nomacro
b	$L86
ori	$10,$10,0xff00
.set	macro
.set	reorder

$L82:
sll	$9,$6,1
addiu	$9,$9,-31
srl	$3,$3,$9
subu	$9,$15,$9
srl	$7,$3,1
andi	$6,$3,0x1
sw	$9,10340($18)
.set	noreorder
.set	nomacro
beq	$6,$0,$L83
move	$3,$7
.set	macro
.set	reorder

subu	$3,$0,$7
$L83:
addiu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$4,$L66
sh	$3,-2($2)
.set	macro
.set	reorder

$L86:
srl	$3,$9,3
addiu	$15,$9,32
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$7,$6,8
sll	$3,$6,8
and	$7,$7,$11
and	$3,$3,$10
or	$3,$7,$3
sll	$8,$3,16
srl	$6,$3,16
andi	$3,$9,0x7
or	$6,$8,$6
sll	$3,$6,$3
ori	$6,$3,0x1
clz	$6,$6
sltu	$7,$3,$13
.set	noreorder
.set	nomacro
bne	$7,$0,$L82
subu	$6,$12,$6
.set	macro
.set	reorder

srl	$3,$3,23
addu	$6,$20,$3
addu	$3,$14,$3
lbu	$6,0($6)
lb	$3,0($3)
addu	$9,$6,$9
.set	noreorder
.set	nomacro
b	$L83
sw	$9,10340($18)
.set	macro
.set	reorder

$L248:
srl	$4,$4,23
addu	$3,$20,$4
addu	$4,$17,$4
lbu	$3,0($3)
lbu	$8,0($4)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L103
sw	$2,10340($18)
.set	macro
.set	reorder

$L247:
srl	$3,$3,23
addu	$4,$20,$3
addu	$3,$17,$3
lbu	$4,0($4)
lbu	$3,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L101
sw	$2,10340($18)
.set	macro
.set	reorder

$L249:
srl	$3,$3,23
addu	$4,$20,$3
addu	$3,$17,$3
lbu	$4,0($4)
lbu	$3,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L105
sw	$2,10340($18)
.set	macro
.set	reorder

$L236:
addiu	$2,$5,4
addiu	$7,$5,5
srl	$3,$2,3
sw	$2,10340($18)
andi	$4,$2,0x7
addu	$3,$6,$3
srl	$8,$7,3
andi	$9,$7,0x7
lbu	$3,0($3)
addu	$8,$6,$8
sw	$7,10340($18)
addiu	$7,$5,6
sll	$3,$3,$4
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,108($19)
lbu	$2,0($8)
sw	$7,10340($18)
sll	$2,$2,$9
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L119
sw	$2,112($19)
.set	macro
.set	reorder

addiu	$12,$5,14
addiu	$11,$5,22
srl	$3,$7,3
li	$14,16711680			# 0xff0000
srl	$8,$12,3
addu	$3,$6,$3
srl	$2,$11,3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
li	$13,-16777216			# 0xffffffffff000000
sw	$12,10340($18)
move	$3,$4
addiu	$14,$14,255
sll	$10,$3,8
srl	$4,$4,8
ori	$13,$13,0xff00
addu	$8,$6,$8
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
#NO_APP
sw	$11,10340($18)
srl	$3,$9,8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
sll	$9,$9,8
srl	$2,$8,8
sll	$8,$8,8
and	$10,$10,$13
and	$4,$4,$14
or	$4,$4,$10
and	$9,$9,$13
and	$8,$8,$13
and	$3,$3,$14
and	$2,$2,$14
or	$3,$3,$9
or	$2,$2,$8
sll	$9,$4,16
srl	$8,$4,16
srl	$10,$3,16
sll	$4,$3,16
sll	$3,$2,16
srl	$2,$2,16
andi	$7,$7,0x7
or	$8,$9,$8
or	$3,$3,$2
sll	$8,$8,$7
or	$4,$4,$10
andi	$12,$12,0x7
andi	$2,$11,0x7
sll	$2,$3,$2
sll	$4,$4,$12
srl	$8,$8,24
srl	$3,$4,24
srl	$2,$2,24
addiu	$7,$5,30
sw	$8,116($19)
sltu	$8,$8,9
sw	$3,120($19)
sw	$2,124($19)
.set	noreorder
.set	nomacro
bne	$8,$0,$L120
sw	$7,10340($18)
.set	macro
.set	reorder

li	$4,2			# 0x2
sw	$4,116($19)
$L120:
sltu	$3,$3,6
.set	noreorder
.set	nomacro
bne	$3,$0,$L121
li	$3,2			# 0x2
.set	macro
.set	reorder

sw	$3,120($19)
$L121:
sltu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$0,$L266
srl	$4,$7,3
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
b	$L266
sw	$2,124($19)
.set	macro
.set	reorder

$L237:
addiu	$11,$5,17
srl	$2,$7,3
li	$10,16711680			# 0xff0000
li	$9,-16777216			# 0xffffffffff000000
addiu	$10,$10,255
srl	$12,$11,3
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$11,10340($18)
sll	$8,$3,8
ori	$9,$9,0xff00
and	$2,$2,$10
and	$8,$8,$9
addu	$3,$6,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($3)  
lwr $12, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$12,8
sll	$12,$12,8
or	$8,$2,$8
and	$12,$12,$9
sll	$2,$8,16
and	$3,$3,$10
srl	$8,$8,16
or	$3,$3,$12
or	$2,$2,$8
andi	$7,$7,0x7
sll	$8,$3,16
srl	$3,$3,16
sll	$2,$2,$7
or	$3,$8,$3
andi	$11,$11,0x7
sll	$3,$3,$11
srl	$2,$2,16
addiu	$12,$5,33
sll	$2,$2,16
srl	$3,$3,16
addiu	$11,$5,49
sw	$12,10340($18)
or	$3,$2,$3
srl	$2,$12,3
sw	$3,132($19)
srl	$7,$11,3
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$8
sw	$11,10340($18)
sll	$2,$2,8
srl	$8,$8,8
and	$14,$2,$9
and	$8,$8,$10
addu	$2,$6,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$7,8
sll	$13,$7,8
or	$8,$8,$14
and	$7,$2,$10
and	$9,$13,$9
sll	$2,$8,16
srl	$8,$8,16
or	$7,$7,$9
or	$2,$2,$8
andi	$12,$12,0x7
sll	$8,$7,16
srl	$7,$7,16
sll	$2,$2,$12
or	$7,$8,$7
andi	$11,$11,0x7
srl	$2,$2,16
sll	$7,$7,$11
sll	$2,$2,16
srl	$7,$7,16
addiu	$8,$5,65
or	$7,$2,$7
sw	$8,10340($18)
.set	noreorder
.set	nomacro
beq	$3,$0,$L128
sw	$7,136($19)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$0,$L128
srl	$2,$8,3
.set	macro
.set	reorder

andi	$8,$8,0x7
addu	$2,$6,$2
addiu	$7,$5,66
lbu	$2,0($2)
sw	$7,10340($18)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
b	$L127
sw	$2,140($19)
.set	macro
.set	reorder

$L122:
srl	$2,$5,3
lw	$4,0($18)
li	$8,16711680			# 0xff0000
addu	$2,$6,$2
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$3,$3,$8
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
andi	$8,$5,0x7
or	$2,$2,$3
sll	$3,$2,$8
li	$2,134217728			# 0x8000000
sltu	$2,$3,$2
beq	$2,$0,$L259
ori	$5,$3,0x1
clz	$8,$5
li	$5,31			# 0x1f
addiu	$2,$7,33
subu	$5,$5,$8
sll	$5,$5,1
addiu	$5,$5,-31
srl	$3,$3,$5
subu	$2,$2,$5
addiu	$3,$3,-1
sw	$2,10340($18)
$L125:
srl	$5,$2,3
li	$8,16711680			# 0xff0000
addu	$5,$6,$5
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$7,8
sll	$7,$7,8
and	$5,$5,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$7,$7,$8
or	$7,$5,$7
sll	$5,$7,16
srl	$7,$7,16
andi	$8,$2,0x7
or	$5,$5,$7
addiu	$7,$3,1
sll	$3,$5,$8
li	$5,134217728			# 0x8000000
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L260
sw	$7,900($4)
.set	macro
.set	reorder

ori	$3,$3,0x1
li	$5,31			# 0x1f
clz	$3,$3
subu	$3,$5,$3
sll	$5,$3,1
subu	$2,$2,$5
addiu	$5,$2,63
.set	noreorder
.set	nomacro
b	$L123
sw	$5,10340($18)
.set	macro
.set	reorder

$L238:
srl	$2,$7,3
li	$11,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
addu	$2,$6,$2
addiu	$11,$11,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
ori	$10,$10,0xff00
and	$3,$3,$10
and	$2,$2,$11
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$5,$7,0x7
or	$2,$3,$2
sll	$2,$2,$5
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$14,0($3)
lbu	$3,0($2)
addu	$2,$14,$7
addiu	$7,$3,1
slt	$3,$7,33
.set	noreorder
.set	nomacro
beq	$3,$0,$L212
sw	$2,10340($18)
.set	macro
.set	reorder

addiu	$14,$2,8
move	$9,$0
li	$13,134217728			# 0x8000000
li	$12,31			# 0x1f
.set	noreorder
.set	nomacro
b	$L136
sw	$14,10340($18)
.set	macro
.set	reorder

$L132:
sll	$2,$2,1
subu	$2,$14,$2
addiu	$2,$2,63
sw	$2,10340($18)
$L133:
srl	$3,$2,3
andi	$15,$2,0x7
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($3)  
lwr $14, 0($3)  

# 0 "" 2
#NO_APP
srl	$5,$14,8
sll	$14,$14,8
and	$5,$5,$11
and	$14,$14,$10
or	$3,$5,$14
sll	$5,$3,16
srl	$3,$3,16
or	$5,$5,$3
sll	$3,$5,$15
ori	$5,$3,0x1
clz	$5,$5
sltu	$14,$3,$13
.set	noreorder
.set	nomacro
beq	$14,$0,$L261
subu	$5,$12,$5
.set	macro
.set	reorder

sll	$5,$5,1
subu	$2,$2,$5
addiu	$2,$2,63
$L135:
addiu	$3,$2,1
addiu	$9,$9,1
move	$14,$3
slt	$5,$9,$7
.set	noreorder
.set	nomacro
beq	$5,$0,$L262
sw	$3,10340($18)
.set	macro
.set	reorder

$L136:
srl	$2,$14,3
andi	$15,$14,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
sll	$2,$3,8
and	$5,$5,$11
and	$2,$2,$10
or	$3,$5,$2
sll	$5,$3,16
srl	$3,$3,16
or	$5,$5,$3
sll	$3,$5,$15
ori	$2,$3,0x1
clz	$2,$2
sltu	$5,$3,$13
.set	noreorder
.set	nomacro
bne	$5,$0,$L132
subu	$2,$12,$2
.set	macro
.set	reorder

srl	$3,$3,23
addu	$3,$20,$3
lbu	$2,0($3)
addu	$2,$2,$14
.set	noreorder
.set	nomacro
b	$L133
sw	$2,10340($18)
.set	macro
.set	reorder

$L261:
srl	$3,$3,23
addu	$3,$20,$3
lbu	$3,0($3)
.set	noreorder
.set	nomacro
b	$L135
addu	$2,$3,$2
.set	macro
.set	reorder

$L262:
srl	$5,$3,3
li	$10,16711680			# 0xff0000
addu	$5,$6,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($5)  
lwr $9, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$9,8
sll	$11,$9,8
li	$9,-16777216			# 0xffffffffff000000
addiu	$10,$10,255
ori	$9,$9,0xff00
and	$5,$5,$10
and	$11,$11,$9
or	$11,$5,$11
sll	$5,$11,16
srl	$11,$11,16
andi	$3,$3,0x7
or	$5,$5,$11
sll	$3,$5,$3
addiu	$5,$2,6
srl	$3,$3,27
srl	$11,$5,3
addiu	$3,$3,1
sw	$5,10340($18)
addu	$11,$6,$11
andi	$5,$5,0x7
sw	$3,912($19)
addiu	$13,$2,11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($11)  
lwr $3, 0($11)  

# 0 "" 2
#NO_APP
move	$11,$3
sll	$11,$11,8
sw	$13,10340($18)
srl	$3,$3,8
and	$11,$11,$9
and	$3,$3,$10
or	$11,$3,$11
sll	$3,$11,16
srl	$11,$11,16
srl	$12,$13,3
or	$3,$3,$11
sll	$5,$3,$5
addu	$3,$6,$12
srl	$5,$5,27
andi	$13,$13,0x7
addiu	$5,$5,1
addiu	$12,$2,16
addiu	$2,$2,21
sw	$5,916($19)
srl	$14,$12,3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($3)  
lwr $11, 0($3)  

# 0 "" 2
#NO_APP
srl	$5,$11,8
sw	$12,10340($18)
sll	$11,$11,8
and	$5,$5,$10
and	$11,$11,$9
or	$11,$5,$11
sll	$5,$11,16
srl	$11,$11,16
addu	$14,$6,$14
or	$3,$5,$11
sll	$3,$3,$13
andi	$12,$12,0x7
srl	$3,$3,27
addiu	$3,$3,1
sw	$3,920($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($14)  
lwr $5, 0($14)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sw	$2,10340($18)
sll	$5,$5,8
sw	$7,908($19)
and	$10,$3,$10
and	$9,$5,$9
or	$3,$10,$9
sll	$5,$3,16
srl	$3,$3,16
or	$3,$5,$3
sll	$3,$3,$12
srl	$3,$3,27
.set	noreorder
.set	nomacro
b	$L130
sw	$3,904($19)
.set	macro
.set	reorder

$L252:
srl	$2,$23,3
li	$12,16711680			# 0xff0000
li	$11,-16777216			# 0xffffffffff000000
addiu	$12,$12,255
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
ori	$11,$11,0xff00
and	$2,$2,$12
and	$3,$3,$11
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
andi	$10,$23,0x7
or	$2,$2,$3
sll	$2,$2,$10
addiu	$10,$5,6
srl	$2,$2,27
srl	$3,$10,3
addiu	$2,$2,1
sw	$10,10340($18)
addu	$3,$6,$3
andi	$10,$10,0x7
sw	$2,912($19)
addiu	$14,$5,11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 3($3)  
lwr $13, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$13,8
sll	$3,$13,8
sw	$14,10340($18)
and	$2,$2,$12
and	$3,$3,$11
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
srl	$13,$14,3
or	$2,$2,$3
sll	$10,$2,$10
addiu	$15,$5,16
srl	$10,$10,27
addu	$13,$6,$13
addiu	$10,$10,1
addiu	$2,$5,21
srl	$3,$15,3
sw	$10,916($19)
andi	$14,$14,0x7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($13)  
lwr $5, 0($13)  

# 0 "" 2
#NO_APP
move	$13,$5
sw	$15,10340($18)
sll	$10,$13,8
srl	$5,$5,8
and	$10,$10,$11
and	$5,$5,$12
or	$10,$5,$10
sll	$5,$10,16
srl	$10,$10,16
addu	$13,$6,$3
or	$3,$5,$10
sll	$3,$3,$14
andi	$10,$15,0x7
srl	$3,$3,27
addiu	$3,$3,1
sw	$3,920($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($13)  
lwr $5, 0($13)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sw	$2,10340($18)
sll	$5,$5,8
sw	$7,908($19)
and	$12,$3,$12
and	$11,$5,$11
or	$3,$12,$11
sll	$5,$3,16
srl	$3,$3,16
or	$3,$5,$3
sll	$3,$3,$10
srl	$3,$3,27
.set	noreorder
.set	nomacro
b	$L137
sw	$3,904($19)
.set	macro
.set	reorder

$L256:
.set	noreorder
.set	nomacro
b	$L73
subu	$2,$0,$2
.set	macro
.set	reorder

$L254:
.set	noreorder
.set	nomacro
b	$L69
subu	$2,$0,$2
.set	macro
.set	reorder

$L259:
srl	$3,$3,23
addu	$2,$20,$3
addu	$3,$17,$3
lbu	$2,0($2)
lbu	$3,0($3)
addu	$2,$2,$5
.set	noreorder
.set	nomacro
b	$L125
sw	$2,10340($18)
.set	macro
.set	reorder

$L215:
lw	$6,%got($LC6)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$7,$16
lw	$4,0($18)
li	$21,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L263
lw	$31,148($sp)
.set	macro
.set	reorder

$L221:
lw	$6,%got($LC9)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC9)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L253:
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$8,$8,$2
addu	$2,$3,$2
lbu	$3,0($8)
lb	$2,0($2)
addu	$7,$3,$6
.set	noreorder
.set	nomacro
b	$L69
sw	$7,10340($18)
.set	macro
.set	reorder

$L255:
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$20,$2
addu	$2,$3,$2
lbu	$4,0($4)
lb	$2,0($2)
addu	$4,$4,$7
.set	noreorder
.set	nomacro
b	$L73
sw	$4,10340($18)
.set	macro
.set	reorder

$L257:
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$17,$2
lbu	$3,0($3)
lbu	$7,0($2)
addu	$4,$3,$4
.set	noreorder
.set	nomacro
b	$L77
sw	$4,10340($18)
.set	macro
.set	reorder

$L245:
srl	$3,$3,23
addu	$20,$20,$3
lbu	$3,0($20)
addu	$2,$3,$2
move	$3,$2
.set	noreorder
.set	nomacro
b	$L158
sw	$2,10340($18)
.set	macro
.set	reorder

$L244:
srl	$3,$3,23
addu	$5,$20,$3
addu	$17,$17,$3
lbu	$3,0($5)
lbu	$7,0($17)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L156
sw	$2,10340($18)
.set	macro
.set	reorder

$L243:
srl	$3,$3,23
addu	$3,$20,$3
lbu	$3,0($3)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L154
sw	$2,10340($18)
.set	macro
.set	reorder

$L242:
srl	$3,$3,23
addu	$3,$20,$3
lbu	$3,0($3)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L152
sw	$2,10340($18)
.set	macro
.set	reorder

$L241:
srl	$3,$3,23
addu	$3,$20,$3
lbu	$3,0($3)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L150
sw	$2,10340($18)
.set	macro
.set	reorder

$L240:
srl	$3,$3,23
addu	$3,$20,$3
lbu	$2,0($3)
addu	$2,$2,$7
.set	noreorder
.set	nomacro
b	$L148
sw	$2,10340($18)
.set	macro
.set	reorder

$L260:
srl	$3,$3,23
addu	$3,$20,$3
lbu	$5,0($3)
addu	$5,$5,$2
.set	noreorder
.set	nomacro
b	$L123
sw	$5,10340($18)
.set	macro
.set	reorder

$L220:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC8)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L159:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
subu	$7,$3,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC17)
.set	macro
.set	reorder

lw	$28,80($sp)
lw	$4,0($18)
sw	$0,660($19)
.set	noreorder
.set	nomacro
b	$L167
sw	$0,656($19)
.set	macro
.set	reorder

$L168:
.set	noreorder
.set	nomacro
b	$L53
li	$21,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L250:
srl	$4,$3,3
andi	$3,$3,0x7
addu	$4,$6,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$8
and	$5,$5,$7
or	$4,$4,$5
sll	$5,$4,16
srl	$4,$4,16
addiu	$9,$2,26
or	$4,$5,$4
sll	$3,$4,$3
srl	$4,$9,3
sw	$9,10340($18)
srl	$3,$3,16
addu	$4,$6,$4
andi	$9,$9,0x7
sw	$3,96($19)
addiu	$3,$2,42
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($4)  
lwr $2, 0($4)  

# 0 "" 2
#NO_APP
move	$4,$2
sll	$4,$4,8
sw	$3,10340($18)
srl	$2,$2,8
and	$7,$4,$7
and	$8,$2,$8
or	$2,$8,$7
sll	$4,$2,16
srl	$2,$2,16
or	$2,$4,$2
sll	$2,$2,$9
srl	$2,$2,16
.set	noreorder
.set	nomacro
b	$L114
sw	$2,100($19)
.set	macro
.set	reorder

$L225:
lw	$6,%got($LC11)($28)
li	$5,16			# 0x10
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L212:
lw	$6,%got($LC16)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L128:
lw	$6,%got($LC15)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
sw	$3,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC15)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L78:
lw	$6,%got($LC7)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

$L258:
.set	noreorder
.set	nomacro
b	$L66
lw	$9,10340($18)
.set	macro
.set	reorder

$L115:
lw	$6,%got($LC14)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L81
lw	$28,80($sp)
.set	macro
.set	reorder

.end	ff_h264_decode_seq_parameter_set
.size	ff_h264_decode_seq_parameter_set, .-ff_h264_decode_seq_parameter_set
.section	.rodata.str1.4
.align	2
$LC24:
.ascii	"CABAC\000"
.align	2
$LC25:
.ascii	"CAVLC\000"
.align	2
$LC26:
.ascii	"weighted\000"
.align	2
$LC27:
.ascii	"LPAR\000"
.align	2
$LC28:
.ascii	"CONSTR\000"
.align	2
$LC29:
.ascii	"REDU\000"
.align	2
$LC30:
.ascii	"8x8DCT\000"
.align	2
$LC31:
.ascii	"pps_id (%d) out of range\012\000"
.align	2
$LC32:
.ascii	"sps_id out of range\012\000"
.align	2
$LC33:
.ascii	"h264 len of aux task = %d\012\000"
.align	2
$LC34:
.ascii	"h264 len of cavlc aux task = %d\012\000"
.align	2
$LC35:
.ascii	"FMO not supported\012\000"
.align	2
$LC36:
.ascii	"reference overflow (pps)\012\000"
.align	2
$LC37:
.ascii	"pps:%u sps:%u %s slice_groups:%d ref:%d/%d %s qp:%d/%d/%"
.ascii	"d/%d %s %s %s %s\012\000"
.section	.text.ff_h264_decode_picture_parameter_set,"ax",@progbits
.align	2
.align	5
.globl	ff_h264_decode_picture_parameter_set
.set	nomips16
.set	nomicromips
.ent	ff_h264_decode_picture_parameter_set
.type	ff_h264_decode_picture_parameter_set, @function
ff_h264_decode_picture_parameter_set:
.frame	$sp,120,$31		# vars= 0, regs= 9/0, args= 72, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$6,10340($4)
addiu	$sp,$sp,-120
lw	$3,10332($4)
li	$7,16711680			# 0xff0000
sw	$18,92($sp)
move	$18,$4
srl	$2,$6,3
.cprestore	72
addiu	$7,$7,255
sw	$19,96($sp)
addu	$3,$3,$2
sw	$31,116($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sw	$23,112($sp)
sll	$4,$4,8
sw	$22,108($sp)
and	$3,$3,$7
sw	$21,104($sp)
li	$7,-16777216			# 0xffffffffff000000
sw	$20,100($sp)
andi	$8,$6,0x7
sw	$17,88($sp)
ori	$7,$7,0xff00
sw	$16,84($sp)
and	$4,$4,$7
or	$4,$3,$4
sll	$3,$4,16
srl	$2,$4,16
or	$2,$3,$2
sll	$2,$2,$8
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L339
move	$19,$5
.set	macro
.set	reorder

ori	$3,$2,0x1
clz	$4,$3
li	$3,31			# 0x1f
addiu	$6,$6,32
subu	$3,$3,$4
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$6,$3
addiu	$16,$2,-1
sw	$3,10340($18)
$L269:
sltu	$2,$16,256
.set	noreorder
.set	nomacro
beq	$2,$0,$L340
lw	$6,%got($LC31)($28)
.set	macro
.set	reorder

lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
li	$4,808			# 0x328
.set	macro
.set	reorder

lw	$28,72($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L320
move	$17,$2
.set	macro
.set	reorder

lw	$5,10340($18)
li	$3,16711680			# 0xff0000
lw	$6,10332($18)
addiu	$3,$3,255
lw	$20,%got(ff_golomb_vlc_len)($28)
srl	$2,$5,3
lw	$21,%got(ff_ue_golomb_vlc_code)($28)
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$4,$4,$3
or	$2,$2,$4
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$5,0x7
or	$2,$3,$2
sll	$2,$2,$4
srl	$2,$2,23
addu	$3,$20,$2
addu	$2,$21,$2
lbu	$3,0($3)
lbu	$2,0($2)
addu	$3,$3,$5
sltu	$4,$2,32
sw	$2,0($17)
.set	noreorder
.set	nomacro
beq	$4,$0,$L272
sw	$3,10340($18)
.set	macro
.set	reorder

li	$4,35142			# 0x8946
addu	$2,$2,$4
sll	$2,$2,2
addu	$2,$18,$2
lw	$2,4($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L272
lw	$5,%got(init_every_movie)($28)
.set	macro
.set	reorder

srl	$4,$3,3
andi	$8,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lw	$7,0($5)
lbu	$2,0($4)
sw	$3,10340($18)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$7,$0,$L275
sw	$2,4($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L276
sw	$0,0($5)
.set	macro
.set	reorder

lw	$22,%got(h264_auxcodes_len)($28)
lw	$4,%got($LC33)($28)
lw	$25,%call16(printf)($28)
lw	$5,0($22)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC33)
.set	macro
.set	reorder

lw	$28,72($sp)
lw	$2,0($22)
lw	$3,%got(tcsm1_base)($28)
srl	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L338
lw	$3,0($3)
.set	macro
.set	reorder

lw	$5,%got(h264_aux_task_codes)($28)
move	$4,$0
$L278:
lw	$2,0($5)
addiu	$3,$3,4
addiu	$4,$4,1
sw	$2,-4($3)
lw	$2,0($22)
srl	$2,$2,2
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L278
addiu	$5,$5,4
.set	macro
.set	reorder

$L338:
lw	$3,10340($18)
lw	$6,10332($18)
$L275:
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$2,$3,1
li	$8,16711680			# 0xff0000
lbu	$4,0($4)
srl	$7,$2,3
addiu	$8,$8,255
sw	$2,10340($18)
addu	$7,$6,$7
sll	$5,$4,$5
andi	$9,$2,0x7
andi	$5,$5,0x00ff
srl	$5,$5,7
sw	$5,8($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($7)  
lwr $4, 0($7)  

# 0 "" 2
#NO_APP
srl	$5,$4,8
sll	$7,$4,8
and	$5,$5,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$7,$7,$8
or	$7,$5,$7
sll	$5,$7,16
srl	$4,$7,16
or	$4,$5,$4
sll	$4,$4,$9
li	$5,134217728			# 0x8000000
sltu	$5,$4,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L341
ori	$5,$4,0x1
.set	macro
.set	reorder

addiu	$2,$3,33
clz	$5,$5
li	$3,31			# 0x1f
subu	$5,$3,$5
sll	$5,$5,1
addiu	$5,$5,-31
srl	$3,$4,$5
subu	$2,$2,$5
addiu	$3,$3,-1
sw	$2,10340($18)
$L282:
addiu	$3,$3,1
slt	$4,$3,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L283
sw	$3,12($17)
.set	macro
.set	reorder

srl	$3,$2,3
li	$5,16711680			# 0xff0000
addu	$6,$6,$3
addiu	$5,$5,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($6)  
lwr $4, 0($6)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$4,$4,$5
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$2,0x7
or	$4,$4,$3
sll	$3,$4,$5
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L342
ori	$4,$3,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$4,$2,$4
addiu	$3,$3,-1
sw	$4,10340($18)
$L285:
lw	$6,%got($LC35)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
addiu	$6,$6,%lo($LC35)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($17)
.set	macro
.set	reorder

lw	$28,72($sp)
lw	$6,10332($18)
lw	$2,10340($18)
$L283:
srl	$3,$2,3
li	$5,16711680			# 0xff0000
addu	$3,$6,$3
addiu	$5,$5,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$4,$4,$5
or	$3,$3,$4
sll	$4,$3,16
srl	$5,$3,16
andi	$3,$2,0x7
or	$4,$4,$5
sll	$3,$4,$3
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L343
ori	$4,$3,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$2,$2,$4
addiu	$3,$3,-1
sw	$2,10340($18)
$L287:
addiu	$7,$3,1
srl	$4,$2,3
li	$3,16711680			# 0xff0000
addu	$4,$6,$4
sw	$7,20($17)
addiu	$3,$3,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$5,$5,$3
or	$5,$4,$5
sll	$4,$5,16
srl	$5,$5,16
andi	$3,$2,0x7
or	$4,$4,$5
sll	$3,$4,$3
li	$4,134217728			# 0x8000000
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L344
ori	$4,$3,0x1
.set	macro
.set	reorder

clz	$5,$4
li	$4,31			# 0x1f
addiu	$2,$2,32
subu	$4,$4,$5
sll	$4,$4,1
addiu	$4,$4,-31
srl	$3,$3,$4
subu	$4,$2,$4
addiu	$3,$3,-1
sw	$4,10340($18)
$L289:
addiu	$4,$3,1
addiu	$2,$7,-1
sltu	$2,$2,32
.set	noreorder
.set	nomacro
beq	$2,$0,$L290
sw	$4,24($17)
.set	macro
.set	reorder

sltu	$3,$3,32
.set	noreorder
.set	nomacro
beq	$3,$0,$L290
li	$3,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$7,10340($18)
li	$4,-16777216			# 0xffffffffff000000
addiu	$11,$3,255
srl	$2,$7,3
andi	$8,$7,0x7
addu	$2,$6,$2
addiu	$3,$7,1
ori	$4,$4,0xff00
lbu	$2,0($2)
srl	$5,$3,3
sw	$3,10340($18)
andi	$3,$3,0x7
addu	$5,$6,$5
sll	$8,$2,$8
addiu	$9,$7,3
andi	$8,$8,0x00ff
srl	$8,$8,7
srl	$12,$9,3
andi	$10,$9,0x7
sw	$8,28($17)
addu	$12,$6,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$2,8
sw	$9,10340($18)
sll	$2,$2,8
and	$5,$5,$11
and	$2,$2,$4
or	$5,$5,$2
sll	$8,$5,16
srl	$5,$5,16
or	$5,$8,$5
sll	$5,$5,$3
srl	$5,$5,30
sw	$5,32($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($12)  
lwr $5, 0($12)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$11
and	$4,$5,$4
or	$4,$3,$4
sll	$3,$4,16
srl	$2,$4,16
or	$2,$3,$2
sll	$2,$2,$10
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L345
li	$3,31			# 0x1f
.set	macro
.set	reorder

ori	$9,$2,0x1
clz	$9,$9
addiu	$7,$7,35
subu	$9,$3,$9
sll	$9,$9,1
addiu	$9,$9,-31
srl	$2,$2,$9
subu	$9,$7,$9
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
bne	$3,$0,$L346
sw	$9,10340($18)
.set	macro
.set	reorder

$L293:
addiu	$2,$2,26
srl	$3,$9,3
li	$4,16711680			# 0xff0000
sw	$2,36($17)
addu	$2,$6,$3
addiu	$4,$4,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$3,$3,$4
or	$2,$2,$3
sll	$3,$2,16
srl	$4,$2,16
andi	$2,$9,0x7
or	$3,$3,$4
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L347
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$4,$3
li	$3,31			# 0x1f
addiu	$9,$9,32
subu	$3,$3,$4
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$5,$9,$3
andi	$3,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
bne	$3,$0,$L348
sw	$5,10340($18)
.set	macro
.set	reorder

$L297:
addiu	$2,$2,26
srl	$3,$5,3
li	$4,16711680			# 0xff0000
sw	$2,40($17)
addu	$2,$6,$3
addiu	$4,$4,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$3,$3,$4
or	$2,$2,$3
sll	$3,$2,16
srl	$4,$2,16
andi	$2,$5,0x7
or	$3,$3,$4
sll	$2,$3,$2
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L349
ori	$4,$2,0x1
.set	macro
.set	reorder

clz	$7,$4
li	$4,31			# 0x1f
addiu	$3,$5,32
subu	$4,$4,$7
sll	$4,$4,1
addiu	$4,$4,-31
srl	$2,$2,$4
subu	$3,$3,$4
andi	$4,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
bne	$4,$0,$L350
sw	$3,10340($18)
.set	macro
.set	reorder

$L301:
srl	$4,$3,3
sw	$2,44($17)
andi	$7,$3,0x7
lw	$5,0($17)
addu	$2,$6,$4
lw	$25,%call16(memcpy)($28)
addiu	$4,$3,1
addiu	$9,$3,2
lbu	$2,0($2)
srl	$10,$4,3
sw	$4,10340($18)
andi	$8,$4,0x7
addu	$4,$6,$10
sll	$7,$2,$7
srl	$2,$9,3
andi	$7,$7,0x00ff
srl	$7,$7,7
li	$21,35142			# 0x8946
addu	$2,$6,$2
sw	$7,52($17)
addu	$5,$5,$21
lbu	$4,0($4)
andi	$10,$9,0x7
sw	$9,10340($18)
sll	$5,$5,2
addiu	$3,$3,3
sll	$4,$4,$8
addu	$5,$18,$5
andi	$4,$4,0x00ff
srl	$4,$4,7
lw	$5,4($5)
li	$7,131072			# 0x20000
addiu	$23,$17,68
sw	$4,56($17)
addu	$7,$18,$7
lbu	$2,0($2)
addiu	$5,$5,668
sw	$3,10340($18)
li	$3,-1			# 0xffffffffffffffff
li	$6,96			# 0x60
sw	$0,64($17)
sll	$2,$2,$10
sw	$3,10652($7)
move	$4,$23
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$22,$17,164
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$2,60($17)
.set	macro
.set	reorder

li	$6,128			# 0x80
lw	$2,0($17)
move	$4,$22
lw	$28,72($sp)
addu	$2,$2,$21
sll	$2,$2,2
lw	$25,%call16(memcpy)($28)
addu	$2,$18,$2
lw	$5,4($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addiu	$5,$5,764
.set	macro
.set	reorder

lw	$2,10340($18)
slt	$19,$2,$19
.set	noreorder
.set	nomacro
bne	$19,$0,$L351
lw	$28,72($sp)
.set	macro
.set	reorder

lw	$10,44($17)
move	$4,$10
sw	$10,48($17)
$L309:
subu	$9,$10,$17
lw	$8,%got(ff_h264_chroma_qp)($28)
addiu	$5,$17,292
addiu	$11,$17,547
addiu	$9,$9,-292
li	$2,51			# 0x33
move	$6,$5
$L310:
addu	$3,$9,$6
addiu	$6,$6,1
slt	$7,$3,52
movz	$3,$2,$7
slt	$7,$3,0
movn	$3,$0,$7
addu	$3,$8,$3
lbu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$6,$11,$L310
sb	$3,-1($6)
.set	macro
.set	reorder

subu	$9,$4,$17
addiu	$19,$17,548
addiu	$11,$17,803
addiu	$9,$9,-548
li	$2,51			# 0x33
move	$6,$19
$L311:
addu	$3,$9,$6
addiu	$6,$6,1
slt	$7,$3,52
movz	$3,$2,$7
slt	$7,$3,0
movn	$3,$0,$7
addu	$3,$8,$3
lbu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$6,$11,$L311
sb	$3,-1($6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$10,$4,$L312
li	$2,1			# 0x1
.set	macro
.set	reorder

sw	$2,804($17)
$L312:
lw	$20,%got(tcsm1_base)($28)
li	$6,256			# 0x100
lw	$25,%call16(memcpy)($28)
lw	$4,0($20)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addiu	$4,$4,19664
.set	macro
.set	reorder

li	$6,256			# 0x100
lw	$28,72($sp)
move	$5,$19
lw	$4,0($20)
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addiu	$4,$4,19920
.set	macro
.set	reorder

lw	$4,0($18)
lw	$2,404($4)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L313
lw	$28,72($sp)
.set	macro
.set	reorder

lw	$2,4($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L321
lw	$6,0($17)
.set	macro
.set	reorder

lw	$11,%got($LC25)($28)
lw	$2,28($17)
lw	$21,12($17)
addiu	$11,$11,%lo($LC25)
lw	$20,20($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L322
lw	$19,24($17)
.set	macro
.set	reorder

$L352:
lw	$10,%got($LC4)($28)
lw	$2,52($17)
lw	$15,36($17)
addiu	$10,$10,%lo($LC4)
lw	$14,40($17)
lw	$13,44($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L323
lw	$12,48($17)
.set	macro
.set	reorder

$L353:
lw	$9,%got($LC4)($28)
lw	$2,56($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L324
addiu	$9,$9,%lo($LC4)
.set	macro
.set	reorder

$L354:
lw	$8,%got($LC4)($28)
lw	$2,60($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L325
addiu	$8,$8,%lo($LC4)
.set	macro
.set	reorder

$L355:
lw	$3,%got($LC4)($28)
lw	$2,64($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L326
addiu	$3,$3,%lo($LC4)
.set	macro
.set	reorder

$L356:
lw	$2,%got($LC4)($28)
addiu	$2,$2,%lo($LC4)
$L319:
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
sw	$6,16($sp)
move	$7,$16
sw	$11,20($sp)
sw	$21,24($sp)
sw	$20,28($sp)
sw	$19,32($sp)
sw	$10,36($sp)
sw	$15,40($sp)
sw	$14,44($sp)
sw	$13,48($sp)
sw	$12,52($sp)
sw	$9,56($sp)
sw	$8,60($sp)
sw	$3,64($sp)
sw	$2,68($sp)
lw	$6,%got($LC37)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC37)
.set	macro
.set	reorder

lw	$28,72($sp)
$L313:
sll	$16,$16,2
lw	$25,%call16(av_free)($28)
addu	$18,$18,$16
li	$16,131072			# 0x20000
addu	$16,$18,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
lw	$4,9628($16)
.set	macro
.set	reorder

move	$2,$0
sw	$17,9628($16)
$L271:
lw	$31,116($sp)
lw	$23,112($sp)
lw	$22,108($sp)
lw	$21,104($sp)
lw	$20,100($sp)
lw	$19,96($sp)
lw	$18,92($sp)
lw	$17,88($sp)
lw	$16,84($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

$L321:
lw	$11,%got($LC24)($28)
lw	$2,28($17)
lw	$21,12($17)
addiu	$11,$11,%lo($LC24)
lw	$20,20($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L352
lw	$19,24($17)
.set	macro
.set	reorder

$L322:
lw	$10,%got($LC26)($28)
lw	$2,52($17)
lw	$15,36($17)
addiu	$10,$10,%lo($LC26)
lw	$14,40($17)
lw	$13,44($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L353
lw	$12,48($17)
.set	macro
.set	reorder

$L323:
lw	$9,%got($LC27)($28)
lw	$2,56($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L354
addiu	$9,$9,%lo($LC27)
.set	macro
.set	reorder

$L324:
lw	$8,%got($LC28)($28)
lw	$2,60($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L355
addiu	$8,$8,%lo($LC28)
.set	macro
.set	reorder

$L325:
lw	$3,%got($LC29)($28)
lw	$2,64($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L356
addiu	$3,$3,%lo($LC29)
.set	macro
.set	reorder

$L326:
lw	$2,%got($LC30)($28)
.set	noreorder
.set	nomacro
b	$L319
addiu	$2,$2,%lo($LC30)
.set	macro
.set	reorder

$L348:
.set	noreorder
.set	nomacro
b	$L297
subu	$2,$0,$2
.set	macro
.set	reorder

$L346:
.set	noreorder
.set	nomacro
b	$L293
subu	$2,$0,$2
.set	macro
.set	reorder

$L350:
.set	noreorder
.set	nomacro
b	$L301
subu	$2,$0,$2
.set	macro
.set	reorder

$L339:
lw	$4,%got(ff_golomb_vlc_len)($28)
srl	$2,$2,23
lw	$3,%got(ff_ue_golomb_vlc_code)($28)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$3,0($4)
lbu	$16,0($2)
addu	$6,$3,$6
.set	noreorder
.set	nomacro
b	$L269
sw	$6,10340($18)
.set	macro
.set	reorder

$L341:
srl	$4,$4,23
addu	$3,$20,$4
addu	$4,$21,$4
lbu	$5,0($3)
lbu	$3,0($4)
addu	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L282
sw	$2,10340($18)
.set	macro
.set	reorder

$L344:
srl	$3,$3,23
addu	$4,$20,$3
addu	$3,$21,$3
lbu	$4,0($4)
lbu	$3,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L289
sw	$2,10340($18)
.set	macro
.set	reorder

$L343:
srl	$3,$3,23
addu	$4,$20,$3
addu	$3,$21,$3
lbu	$4,0($4)
lbu	$3,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L287
sw	$2,10340($18)
.set	macro
.set	reorder

$L345:
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$20,$2
addu	$2,$3,$2
lbu	$5,0($4)
lb	$2,0($2)
addu	$9,$5,$9
.set	noreorder
.set	nomacro
b	$L293
sw	$9,10340($18)
.set	macro
.set	reorder

$L349:
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$20,$2
addu	$2,$3,$2
lbu	$3,0($4)
lb	$2,0($2)
addu	$3,$3,$5
.set	noreorder
.set	nomacro
b	$L301
sw	$3,10340($18)
.set	macro
.set	reorder

$L347:
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
addu	$4,$20,$2
addu	$2,$3,$2
lbu	$3,0($4)
lb	$2,0($2)
addu	$5,$3,$9
.set	noreorder
.set	nomacro
b	$L297
sw	$5,10340($18)
.set	macro
.set	reorder

$L351:
lw	$3,10332($18)
srl	$6,$2,3
lw	$4,0($17)
andi	$5,$2,0x7
addiu	$2,$2,1
lw	$25,%got(decode_scaling_matrices.isra.6)($28)
addu	$3,$3,$6
addu	$4,$4,$21
addiu	$6,$17,64
lbu	$3,0($3)
sll	$4,$4,2
sw	$2,10340($18)
move	$7,$0
addu	$4,$18,$4
sw	$23,16($sp)
sll	$2,$3,$5
sw	$22,20($sp)
addiu	$25,$25,%lo(decode_scaling_matrices.isra.6)
andi	$2,$2,0x00ff
lw	$5,4($4)
srl	$2,$2,7
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_scaling_matrices.isra.6
1:	jalr	$25
sw	$2,64($17)
.set	macro
.set	reorder

li	$6,16711680			# 0xff0000
lw	$5,10340($18)
lw	$3,10332($18)
addiu	$6,$6,255
lw	$28,72($sp)
srl	$2,$5,3
andi	$7,$5,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$4,$4,$6
or	$4,$3,$4
sll	$3,$4,16
srl	$2,$4,16
or	$2,$3,$2
sll	$2,$2,$7
li	$3,134217728			# 0x8000000
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L357
ori	$3,$2,0x1
.set	macro
.set	reorder

clz	$4,$3
li	$3,31			# 0x1f
addiu	$5,$5,32
subu	$3,$3,$4
sll	$3,$3,1
addiu	$3,$3,-31
srl	$2,$2,$3
subu	$3,$5,$3
andi	$4,$2,0x1
srl	$2,$2,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L308
sw	$3,10340($18)
.set	macro
.set	reorder

subu	$2,$0,$2
$L308:
move	$4,$2
lw	$10,44($17)
.set	noreorder
.set	nomacro
b	$L309
sw	$4,48($17)
.set	macro
.set	reorder

$L276:
lw	$22,%got(h264_cavlc_auxcodes_len)($28)
lw	$4,%got($LC34)($28)
lw	$25,%call16(printf)($28)
lw	$5,0($22)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC34)
.set	macro
.set	reorder

lw	$28,72($sp)
lw	$2,0($22)
lw	$3,%got(tcsm1_base)($28)
srl	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L338
lw	$3,0($3)
.set	macro
.set	reorder

lw	$5,%got(h264_cavlc_aux_task_codes)($28)
move	$4,$0
$L280:
lw	$2,0($5)
addiu	$3,$3,4
addiu	$4,$4,1
sw	$2,-4($3)
lw	$2,0($22)
srl	$2,$2,2
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L280
addiu	$5,$5,4
.set	macro
.set	reorder

lw	$3,10340($18)
.set	noreorder
.set	nomacro
b	$L275
lw	$6,10332($18)
.set	macro
.set	reorder

$L357:
srl	$2,$2,23
lw	$3,%got(ff_se_golomb_vlc_code)($28)
lw	$10,44($17)
addu	$20,$20,$2
addu	$2,$3,$2
lbu	$3,0($20)
lb	$4,0($2)
addu	$5,$3,$5
sw	$4,48($17)
.set	noreorder
.set	nomacro
b	$L309
sw	$5,10340($18)
.set	macro
.set	reorder

$L342:
srl	$3,$3,23
addu	$4,$20,$3
addu	$3,$21,$3
lbu	$4,0($4)
lbu	$3,0($3)
addu	$2,$4,$2
.set	noreorder
.set	nomacro
b	$L285
sw	$2,10340($18)
.set	macro
.set	reorder

$L290:
lw	$6,%got($LC36)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC36)
.set	macro
.set	reorder

lw	$28,72($sp)
$L274:
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,116($sp)
lw	$23,112($sp)
lw	$22,108($sp)
lw	$21,104($sp)
lw	$20,100($sp)
lw	$19,96($sp)
lw	$18,92($sp)
lw	$17,88($sp)
lw	$16,84($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

$L340:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
move	$7,$16
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC31)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L271
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L272:
lw	$6,%got($LC32)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC32)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L274
lw	$28,72($sp)
.set	macro
.set	reorder

$L320:
.set	noreorder
.set	nomacro
b	$L271
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	ff_h264_decode_picture_parameter_set
.size	ff_h264_decode_picture_parameter_set, .-ff_h264_decode_picture_parameter_set
.rdata
.align	2
.type	default_scaling8, @object
.size	default_scaling8, 128
default_scaling8:
.byte	6
.byte	10
.byte	13
.byte	16
.byte	18
.byte	23
.byte	25
.byte	27
.byte	10
.byte	11
.byte	16
.byte	18
.byte	23
.byte	25
.byte	27
.byte	29
.byte	13
.byte	16
.byte	18
.byte	23
.byte	25
.byte	27
.byte	29
.byte	31
.byte	16
.byte	18
.byte	23
.byte	25
.byte	27
.byte	29
.byte	31
.byte	33
.byte	18
.byte	23
.byte	25
.byte	27
.byte	29
.byte	31
.byte	33
.byte	36
.byte	23
.byte	25
.byte	27
.byte	29
.byte	31
.byte	33
.byte	36
.byte	38
.byte	25
.byte	27
.byte	29
.byte	31
.byte	33
.byte	36
.byte	38
.byte	40
.byte	27
.byte	29
.byte	31
.byte	33
.byte	36
.byte	38
.byte	40
.byte	42
.byte	9
.byte	13
.byte	15
.byte	17
.byte	19
.byte	21
.byte	22
.byte	24
.byte	13
.byte	13
.byte	17
.byte	19
.byte	21
.byte	22
.byte	24
.byte	25
.byte	15
.byte	17
.byte	19
.byte	21
.byte	22
.byte	24
.byte	25
.byte	27
.byte	17
.byte	19
.byte	21
.byte	22
.byte	24
.byte	25
.byte	27
.byte	28
.byte	19
.byte	21
.byte	22
.byte	24
.byte	25
.byte	27
.byte	28
.byte	30
.byte	21
.byte	22
.byte	24
.byte	25
.byte	27
.byte	28
.byte	30
.byte	32
.byte	22
.byte	24
.byte	25
.byte	27
.byte	28
.byte	30
.byte	32
.byte	33
.byte	24
.byte	25
.byte	27
.byte	28
.byte	30
.byte	32
.byte	33
.byte	35
.align	2
.type	default_scaling4, @object
.size	default_scaling4, 32
default_scaling4:
.byte	6
.byte	13
.byte	20
.byte	28
.byte	13
.byte	20
.byte	28
.byte	32
.byte	20
.byte	28
.byte	32
.byte	37
.byte	28
.byte	32
.byte	37
.byte	42
.byte	10
.byte	14
.byte	20
.byte	24
.byte	14
.byte	20
.byte	24
.byte	27
.byte	20
.byte	24
.byte	27
.byte	30
.byte	24
.byte	27
.byte	30
.byte	34
.globl	ff_h264_chroma_qp
.align	2
.type	ff_h264_chroma_qp, @object
.size	ff_h264_chroma_qp, 52
ff_h264_chroma_qp:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.byte	17
.byte	18
.byte	19
.byte	20
.byte	21
.byte	22
.byte	23
.byte	24
.byte	25
.byte	26
.byte	27
.byte	28
.byte	29
.byte	29
.byte	30
.byte	31
.byte	32
.byte	32
.byte	33
.byte	34
.byte	34
.byte	35
.byte	35
.byte	36
.byte	36
.byte	37
.byte	37
.byte	37
.byte	38
.byte	38
.byte	38
.byte	39
.byte	39
.byte	39
.byte	39
.align	2
.type	pixel_aspect, @object
.size	pixel_aspect, 136
pixel_aspect:
.word	0
.word	1
.word	1
.word	1
.word	12
.word	11
.word	10
.word	11
.word	16
.word	11
.word	40
.word	33
.word	24
.word	11
.word	20
.word	11
.word	32
.word	11
.word	80
.word	33
.word	18
.word	11
.word	15
.word	11
.word	64
.word	33
.word	160
.word	99
.word	4
.word	3
.word	3
.word	2
.word	2
.word	1

.comm	init_every_movie,4,4
.align	2
.type	zigzag_scan, @object
.size	zigzag_scan, 16
zigzag_scan:
.byte	0
.byte	1
.byte	4
.byte	8
.byte	5
.byte	2
.byte	3
.byte	6
.byte	9
.byte	12
.byte	13
.byte	10
.byte	7
.byte	11
.byte	14
.byte	15

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
