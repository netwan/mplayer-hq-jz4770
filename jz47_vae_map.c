#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "config.h"

#define MC__OFFSET 0x13250000
#define VPU__OFFSET 0x13200000
#define CPM__OFFSET 0x10000000
#define AUX__OFFSET 0x132A0000
#define TCSM0__OFFSET 0x132B0000
#define TCSM1__OFFSET 0x132C0000
#define SRAM__OFFSET 0x132F0000
#define GP0__OFFSET 0x13210000
#define GP1__OFFSET 0x13220000
#define GP2__OFFSET 0x13230000
#define DBLK0__OFFSET 0x13270000
#define DBLK1__OFFSET 0x132D0000
#define SDE__OFFSET 0x13290000
#define VMAU__OFFSET 0x13280000

#define MC__SIZE   0x00001000
#define VPU__SIZE 0x00001000
#define CPM__SIZE 0x00001000
#define AUX__SIZE 0x00004000
#define TCSM0__SIZE 0x00004000
#define TCSM1__SIZE 0x0000C000
#define SRAM__SIZE 0x00007000

#define GP0__SIZE   0x00001000
#define GP1__SIZE   0x00001000
#define GP2__SIZE   0x00001000
#define DBLK0__SIZE   0x00001000
#define DBLK1__SIZE   0x00001000
#define SDE__SIZE   0x00004000
#define VMAU__SIZE 0x0000F000

int vae_fd;
int tcsm_fd;
volatile unsigned char *ipu_base;
volatile unsigned char *mc_base;
volatile unsigned char *vpu_base;
volatile unsigned char *cpm_base;
volatile unsigned char *lcd_base;
volatile unsigned char *gp0_base;
volatile unsigned char *gp1_base;
volatile unsigned char *gp2_base;
volatile unsigned char *vmau_base;
volatile unsigned char *dblk0_base;
volatile unsigned char *dblk1_base;
volatile unsigned char *sde_base;
volatile unsigned char *tcsm0_base;
volatile unsigned char *tcsm1_base;
volatile unsigned char *sram_base;
volatile unsigned char *ahb1_base;
volatile unsigned char *ddr_base;
volatile unsigned char *aux_base;

#define PMON_ON 887
#define PMON_OFF 889
void VAE_map() {
	/* open and map flash device */
	vae_fd = open("/dev/mem", O_RDWR);
	//asm("break %0"::"i"(PMON_ON));
	// tricky appoach to use TCSM
	tcsm_fd = open("/dev/tcsm", O_RDWR);
	if (vae_fd < 0) {
	  printf("open /dev/mem error.\n");
	  exit(1);
	}

        if (tcsm_fd < 0) {
	  printf("open /dev/tcsm error.\n");
	  exit(1);
	}

	mc_base = mmap((void *)0, MC__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
		       MC__OFFSET);
	sde_base = mmap((void *)0, SDE__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
		       SDE__OFFSET);
	dblk1_base = mmap((void *)0, DBLK1__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
		       DBLK1__OFFSET);
	dblk0_base = mmap((void *)0, DBLK0__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
		       DBLK0__OFFSET);
	vmau_base = mmap((void *)0, VMAU__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
		       VMAU__OFFSET);
	vpu_base = mmap((void *)0, VPU__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
			   VPU__OFFSET);
	aux_base = mmap((void *)0, AUX__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
			   AUX__OFFSET);
	cpm_base = mmap((void *)0, CPM__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
			   CPM__OFFSET);
 	gp0_base = mmap((void *)0, GP0__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd,  
 			   GP0__OFFSET); 
 	gp1_base = mmap((void *)0, GP1__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd,  
 			   GP1__OFFSET); 
 	gp2_base = mmap((void *)0, GP2__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd,  
 			   GP2__OFFSET); 
 	tcsm0_base = mmap((void *)0, TCSM0__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd,  
 			   TCSM0__OFFSET); 
 	tcsm1_base = mmap((void *)0, TCSM1__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd,  
 			   TCSM1__OFFSET); 
	sram_base = mmap((void *)0, SRAM__SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, vae_fd, 
			   SRAM__OFFSET);

	printf("VAE mmap successfully done!\n");
}

void VAE_unmap() {
  munmap(mc_base, MC__SIZE);
  munmap(sde_base, SDE__SIZE);
  munmap(dblk1_base, DBLK1__SIZE);
  munmap(dblk0_base, DBLK0__SIZE);
  munmap(vmau_base, VMAU__SIZE);
  munmap(vpu_base, VPU__SIZE);
  munmap(aux_base, AUX__SIZE);
  munmap(cpm_base, CPM__SIZE);
  munmap(gp0_base, GP0__SIZE);
  munmap(gp1_base, GP1__SIZE);
  munmap(gp2_base, GP2__SIZE);
  munmap(tcsm0_base, TCSM0__SIZE);
  munmap(tcsm1_base, TCSM1__SIZE);
  munmap(sram_base, SRAM__SIZE);
  sram_base = 0 ;  
  //asm("break %0"::"i"(PMON_OFF));
  printf("VAE unmap successfully done!\n");
  close(vae_fd);
  close(tcsm_fd);
}
