.file	1 "vp8.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.vp8_rac_get_uint,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_rac_get_uint
.type	vp8_rac_get_uint, @function
vp8_rac_get_uint:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
beq	$5,$0,$L7
addiu	$10,$5,-1

lw	$3,4($4)
move	$2,$0
lw	$8,16($4)
li	$12,-1			# 0xffffffffffffffff
b	$L5
lw	$11,%got(ff_vp56_norm_shift)($28)

$L12:
subu	$8,$7,$13
sw	$5,0($4)
addiu	$10,$10,-1
or	$2,$6,$2
sw	$8,16($4)
beq	$10,$12,$L13
move	$3,$9

$L5:
lw	$5,0($4)
sll	$2,$2,1
addu	$6,$11,$5
lbu	$7,0($6)
sll	$5,$5,$7
addu	$9,$7,$3
addiu	$3,$5,-1
sw	$5,0($4)
sll	$7,$8,$7
bltz	$9,$L3
sll	$3,$3,7

lw	$6,8($4)
lw	$8,12($4)
sltu	$8,$6,$8
beq	$8,$0,$L3
addiu	$13,$6,2

sw	$13,8($4)
lbu	$8,1($6)
lbu	$6,0($6)
sll	$8,$8,8
or	$8,$8,$6
sll	$6,$8,8
srl	$8,$8,8
or	$8,$6,$8
andi	$8,$8,0xffff
sll	$8,$8,$9
addiu	$9,$9,-16
or	$7,$7,$8
$L3:
sra	$3,$3,8
sw	$9,4($4)
move	$8,$7
addiu	$3,$3,1
sll	$13,$3,16
sltu	$6,$7,$13
xori	$6,$6,0x1
bne	$6,$0,$L12
subu	$5,$5,$3

addiu	$10,$10,-1
sw	$3,0($4)
or	$2,$6,$2
sw	$8,16($4)
bne	$10,$12,$L5
move	$3,$9

$L13:
j	$31
nop

$L7:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	vp8_rac_get_uint
.size	vp8_rac_get_uint, .-vp8_rac_get_uint
.section	.text.vp8_rac_get_sint,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_rac_get_sint
.type	vp8_rac_get_sint, @function
vp8_rac_get_sint:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-40
lw	$3,0($4)
lw	$6,4($4)
sw	$17,32($sp)
lw	$17,%got(ff_vp56_norm_shift)($28)
lw	$2,16($4)
.cprestore	16
addu	$7,$17,$3
sw	$31,36($sp)
sw	$16,28($sp)
lbu	$7,0($7)
sll	$3,$3,$7
addu	$6,$7,$6
sll	$7,$2,$7
bltz	$6,$L15
sw	$3,0($4)

lw	$8,8($4)
lw	$2,12($4)
sltu	$2,$8,$2
bne	$2,$0,$L24
addiu	$2,$8,2

$L15:
addiu	$2,$3,-1
sw	$6,4($4)
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$6,$2,16
sltu	$8,$7,$6
bne	$8,$0,$L16
lw	$31,36($sp)

lw	$25,%got(vp8_rac_get_uint)($28)
subu	$7,$7,$6
subu	$2,$3,$2
move	$16,$4
addiu	$25,$25,%lo(vp8_rac_get_uint)
sw	$7,16($16)
.reloc	1f,R_MIPS_JALR,vp8_rac_get_uint
1:	jalr	$25
sw	$2,0($16)

lw	$3,0($16)
lw	$6,4($16)
lw	$7,16($16)
addu	$4,$17,$3
lbu	$5,0($4)
sll	$4,$3,$5
addu	$6,$5,$6
sll	$5,$7,$5
bltz	$6,$L17
sw	$4,0($16)

lw	$7,8($16)
lw	$3,12($16)
sltu	$3,$7,$3
beq	$3,$0,$L25
addiu	$3,$4,-1

addiu	$3,$7,2
sw	$3,8($16)
lbu	$3,1($7)
lbu	$7,0($7)
sll	$3,$3,8
or	$3,$3,$7
sll	$7,$3,8
srl	$3,$3,8
or	$3,$7,$3
andi	$3,$3,0xffff
sll	$3,$3,$6
addiu	$6,$6,-16
or	$5,$5,$3
$L17:
addiu	$3,$4,-1
$L25:
sw	$6,4($16)
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$7,$3,16
sltu	$6,$5,$7
xori	$6,$6,0x1
beq	$6,$0,$L26
lw	$31,36($sp)

subu	$3,$4,$3
subu	$5,$5,$7
$L26:
lw	$17,32($sp)
sw	$3,0($16)
subu	$3,$0,$6
sw	$5,16($16)
xor	$2,$3,$2
lw	$16,28($sp)
addu	$2,$2,$6
j	$31
addiu	$sp,$sp,40

$L16:
lw	$17,32($sp)
lw	$16,28($sp)
sw	$2,0($4)
move	$2,$0
sw	$7,16($4)
j	$31
addiu	$sp,$sp,40

$L24:
sw	$2,8($4)
lbu	$2,1($8)
lbu	$8,0($8)
sll	$2,$2,8
or	$2,$2,$8
sll	$8,$2,8
srl	$2,$2,8
or	$2,$8,$2
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
b	$L15
or	$7,$7,$2

.set	macro
.set	reorder
.end	vp8_rac_get_sint
.size	vp8_rac_get_sint, .-vp8_rac_get_sint
.section	.text.crc,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	crc
.type	crc, @function
crc:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$12,$5,15
slt	$2,$5,0
movz	$12,$5,$2
addiu	$sp,$sp,-24
move	$2,$6
sw	$20,20($sp)
sw	$19,16($sp)
sra	$12,$12,4
sw	$18,12($sp)
sw	$17,8($sp)
blez	$12,$L33
sw	$16,4($sp)

lw	$7,%got(crc16_tab)($28)
sll	$11,$12,4
addu	$11,$4,$11
addiu	$7,$7,%lo(crc16_tab)
$L30:
srl	$3,$2,6
lbu	$6,0($4)
sll	$2,$2,8
lbu	$19,1($4)
andi	$3,$3,0x3fc
lbu	$18,2($4)
addu	$3,$7,$3
lbu	$17,3($4)
or	$2,$2,$6
lbu	$16,4($4)
lbu	$25,5($4)
addiu	$4,$4,16
lw	$6,0($3)
xor	$2,$6,$2
lbu	$24,-10($4)
sll	$2,$2,16
lbu	$15,-9($4)
lbu	$14,-8($4)
sra	$2,$2,16
lbu	$13,-7($4)
lbu	$6,-6($4)
srl	$20,$2,6
lbu	$10,-5($4)
sll	$2,$2,8
lbu	$9,-4($4)
andi	$20,$20,0x3fc
lbu	$8,-3($4)
addu	$20,$7,$20
lbu	$3,-2($4)
or	$19,$2,$19
lbu	$2,-1($4)
lw	$20,0($20)
xor	$19,$20,$19
sll	$19,$19,16
sra	$19,$19,16
srl	$20,$19,6
sll	$19,$19,8
andi	$20,$20,0x3fc
addu	$20,$7,$20
or	$18,$19,$18
lw	$19,0($20)
xor	$18,$19,$18
sll	$18,$18,16
sra	$18,$18,16
srl	$19,$18,6
sll	$18,$18,8
andi	$19,$19,0x3fc
addu	$19,$7,$19
or	$17,$18,$17
lw	$18,0($19)
xor	$17,$18,$17
sll	$17,$17,16
sra	$17,$17,16
srl	$18,$17,6
sll	$17,$17,8
andi	$18,$18,0x3fc
addu	$18,$7,$18
or	$16,$17,$16
lw	$17,0($18)
xor	$16,$17,$16
sll	$16,$16,16
sra	$16,$16,16
srl	$17,$16,6
sll	$16,$16,8
andi	$17,$17,0x3fc
addu	$17,$7,$17
or	$25,$16,$25
lw	$16,0($17)
xor	$25,$16,$25
sll	$25,$25,16
sra	$25,$25,16
srl	$16,$25,6
sll	$25,$25,8
andi	$16,$16,0x3fc
addu	$16,$7,$16
or	$24,$25,$24
lw	$25,0($16)
xor	$24,$25,$24
sll	$24,$24,16
sra	$24,$24,16
srl	$16,$24,6
sll	$24,$24,8
andi	$16,$16,0x3fc
addu	$16,$7,$16
or	$15,$24,$15
lw	$24,0($16)
xor	$15,$24,$15
sll	$15,$15,16
sra	$15,$15,16
srl	$16,$15,6
sll	$15,$15,8
andi	$16,$16,0x3fc
addu	$16,$7,$16
or	$14,$15,$14
lw	$15,0($16)
xor	$14,$15,$14
sll	$14,$14,16
sra	$14,$14,16
srl	$15,$14,6
sll	$14,$14,8
andi	$15,$15,0x3fc
addu	$15,$7,$15
or	$13,$14,$13
lw	$14,0($15)
xor	$13,$14,$13
sll	$13,$13,16
sra	$13,$13,16
srl	$14,$13,6
sll	$13,$13,8
andi	$14,$14,0x3fc
addu	$14,$7,$14
or	$6,$13,$6
lw	$13,0($14)
xor	$6,$13,$6
sll	$6,$6,16
sra	$6,$6,16
srl	$13,$6,6
sll	$6,$6,8
andi	$13,$13,0x3fc
addu	$13,$7,$13
or	$10,$6,$10
lw	$13,0($13)
xor	$10,$13,$10
sll	$10,$10,16
sra	$10,$10,16
srl	$6,$10,6
sll	$10,$10,8
andi	$6,$6,0x3fc
addu	$6,$7,$6
or	$9,$10,$9
lw	$10,0($6)
xor	$9,$10,$9
sll	$9,$9,16
sra	$9,$9,16
srl	$6,$9,6
sll	$9,$9,8
andi	$6,$6,0x3fc
addu	$6,$7,$6
or	$8,$9,$8
lw	$9,0($6)
xor	$8,$9,$8
sll	$8,$8,16
sra	$8,$8,16
srl	$6,$8,6
sll	$8,$8,8
andi	$6,$6,0x3fc
addu	$6,$7,$6
or	$3,$8,$3
lw	$8,0($6)
xor	$3,$8,$3
sll	$3,$3,16
sra	$3,$3,16
srl	$6,$3,6
sll	$3,$3,8
andi	$6,$6,0x3fc
addu	$6,$7,$6
or	$2,$3,$2
lw	$3,0($6)
xor	$2,$3,$2
sll	$2,$2,16
bne	$4,$11,$L30
sra	$2,$2,16

$L29:
sll	$12,$12,4
subu	$5,$5,$12
blez	$5,$L37
lw	$20,20($sp)

lw	$7,%got(crc16_tab)($28)
addu	$5,$11,$5
addiu	$7,$7,%lo(crc16_tab)
$L32:
srl	$3,$2,6
lbu	$6,0($11)
sll	$2,$2,8
andi	$3,$3,0x3fc
addu	$3,$7,$3
or	$2,$2,$6
addiu	$11,$11,1
lw	$6,0($3)
xor	$2,$6,$2
sll	$2,$2,16
bne	$11,$5,$L32
sra	$2,$2,16

lw	$20,20($sp)
$L37:
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,24

$L33:
b	$L29
move	$11,$4

.set	macro
.set	reorder
.end	crc
.size	crc, .-crc
.section	.text.read_mv_component,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	read_mv_component
.type	read_mv_component, @function
read_mv_component:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$3,0($4)
lw	$11,%got(ff_vp56_norm_shift)($28)
lw	$2,4($4)
lw	$8,16($4)
addu	$7,$11,$3
lbu	$6,0($5)
lbu	$7,0($7)
sll	$9,$3,$7
addu	$2,$7,$2
sll	$7,$8,$7
bltz	$2,$L39
sw	$9,0($4)

lw	$8,8($4)
lw	$3,12($4)
sltu	$3,$8,$3
bne	$3,$0,$L79
addiu	$3,$8,2

$L39:
addiu	$3,$9,-1
mul	$3,$6,$3
sra	$3,$3,8
addiu	$3,$3,1
sll	$6,$3,16
sltu	$8,$7,$6
bne	$8,$0,$L40
sw	$2,4($4)

subu	$6,$7,$6
subu	$3,$9,$3
move	$10,$0
move	$9,$0
sw	$6,16($4)
li	$13,3			# 0x3
sw	$3,0($4)
move	$8,$6
$L41:
addu	$7,$11,$3
addu	$6,$5,$9
lbu	$7,0($7)
lbu	$14,9($6)
sll	$12,$3,$7
addu	$6,$7,$2
addiu	$2,$12,-1
sw	$12,0($4)
sll	$7,$8,$7
bltz	$6,$L44
mul	$3,$2,$14

lw	$8,8($4)
lw	$2,12($4)
sltu	$2,$8,$2
beq	$2,$0,$L44
addiu	$14,$8,2

sw	$14,8($4)
lbu	$2,1($8)
lbu	$8,0($8)
sll	$2,$2,8
or	$2,$2,$8
sll	$8,$2,8
srl	$2,$2,8
or	$2,$8,$2
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
or	$7,$7,$2
$L44:
sra	$3,$3,8
sw	$6,4($4)
addiu	$3,$3,1
sll	$14,$3,16
sltu	$2,$7,$14
xori	$2,$2,0x1
beq	$2,$0,$L74
move	$8,$7

subu	$3,$12,$3
subu	$8,$7,$14
$L74:
sll	$2,$2,$9
sw	$3,0($4)
addiu	$9,$9,1
sw	$8,16($4)
addu	$10,$10,$2
bne	$9,$13,$L41
move	$2,$6

li	$2,9			# 0x9
li	$13,3			# 0x3
move	$7,$6
$L48:
addu	$9,$11,$3
addu	$6,$5,$2
lbu	$9,0($9)
lbu	$14,9($6)
sll	$12,$3,$9
addu	$6,$9,$7
addiu	$3,$12,-1
sw	$12,0($4)
sll	$9,$8,$9
bltz	$6,$L46
mul	$3,$3,$14

lw	$8,8($4)
lw	$7,12($4)
sltu	$7,$8,$7
beq	$7,$0,$L46
addiu	$14,$8,2

sw	$14,8($4)
lbu	$7,1($8)
lbu	$8,0($8)
sll	$7,$7,8
or	$7,$7,$8
sll	$8,$7,8
srl	$7,$7,8
or	$7,$8,$7
andi	$7,$7,0xffff
sll	$7,$7,$6
addiu	$6,$6,-16
or	$9,$9,$7
$L46:
sra	$3,$3,8
sw	$6,4($4)
addiu	$3,$3,1
sll	$14,$3,16
sltu	$7,$9,$14
xori	$7,$7,0x1
beq	$7,$0,$L75
move	$8,$9

subu	$3,$12,$3
subu	$8,$9,$14
$L75:
sll	$7,$7,$2
sw	$3,0($4)
addiu	$2,$2,-1
sw	$8,16($4)
addu	$10,$10,$7
bne	$2,$13,$L48
move	$7,$6

andi	$2,$10,0xfff0
bne	$2,$0,$L80
addu	$2,$11,$3

$L49:
addiu	$10,$10,8
$L61:
addu	$11,$11,$3
lbu	$9,1($5)
lbu	$2,0($11)
sll	$3,$3,$2
addu	$6,$2,$6
sll	$8,$8,$2
bltz	$6,$L59
sw	$3,0($4)

lw	$7,8($4)
lw	$2,12($4)
sltu	$2,$7,$2
bne	$2,$0,$L81
addiu	$2,$7,2

$L59:
addiu	$5,$3,-1
sw	$6,4($4)
mul	$5,$5,$9
sra	$5,$5,8
addiu	$5,$5,1
sll	$7,$5,16
sltu	$2,$8,$7
xori	$6,$2,0x1
beq	$6,$0,$L78
nop

subu	$5,$3,$5
subu	$8,$8,$7
$L78:
subu	$2,$0,$6
sw	$5,0($4)
sw	$8,16($4)
xor	$10,$2,$10
j	$31
addu	$2,$10,$6

$L40:
addu	$6,$11,$3
sw	$3,0($4)
sw	$7,16($4)
lbu	$10,2($5)
lbu	$8,0($6)
sll	$3,$3,$8
addu	$2,$2,$8
sll	$8,$7,$8
bltz	$2,$L43
sw	$3,0($4)

lw	$9,8($4)
lw	$7,12($4)
sltu	$7,$9,$7
beq	$7,$0,$L84
addiu	$6,$3,-1

addiu	$6,$9,2
sw	$6,8($4)
lbu	$7,1($9)
lbu	$6,0($9)
sll	$7,$7,8
or	$7,$7,$6
sll	$9,$7,8
srl	$7,$7,8
or	$7,$9,$7
andi	$7,$7,0xffff
sll	$7,$7,$2
addiu	$2,$2,-16
or	$8,$8,$7
$L43:
addiu	$6,$3,-1
$L84:
mul	$6,$6,$10
sra	$6,$6,8
addiu	$6,$6,1
sll	$7,$6,16
sltu	$10,$8,$7
xori	$10,$10,0x1
beq	$10,$0,$L76
sw	$2,4($4)

subu	$6,$3,$6
subu	$8,$8,$7
$L76:
sll	$3,$10,1
sw	$6,0($4)
addu	$7,$11,$6
sw	$8,16($4)
addu	$3,$3,$10
sll	$13,$10,2
lbu	$9,0($7)
addiu	$3,$3,3
addu	$12,$5,$3
sll	$6,$6,$9
addu	$2,$2,$9
lbu	$10,0($12)
sll	$8,$8,$9
bltz	$2,$L54
sw	$6,0($4)

lw	$9,8($4)
lw	$7,12($4)
sltu	$7,$9,$7
beq	$7,$0,$L85
addiu	$3,$6,-1

addiu	$3,$9,2
sw	$3,8($4)
lbu	$7,1($9)
lbu	$3,0($9)
sll	$7,$7,8
or	$7,$7,$3
sll	$9,$7,8
srl	$7,$7,8
or	$7,$9,$7
andi	$7,$7,0xffff
sll	$7,$7,$2
addiu	$2,$2,-16
or	$8,$8,$7
$L54:
addiu	$3,$6,-1
$L85:
mul	$3,$3,$10
sra	$3,$3,8
addiu	$3,$3,1
sll	$9,$3,16
sltu	$7,$8,$9
xori	$7,$7,0x1
beq	$7,$0,$L77
sw	$2,4($4)

subu	$3,$6,$3
subu	$8,$8,$9
$L77:
addu	$6,$11,$3
sw	$3,0($4)
addu	$12,$12,$7
sw	$8,16($4)
sll	$10,$7,1
lbu	$9,0($6)
lbu	$12,1($12)
addu	$10,$13,$10
sll	$3,$3,$9
addu	$6,$2,$9
sll	$8,$8,$9
bltz	$6,$L56
sw	$3,0($4)

lw	$7,8($4)
lw	$2,12($4)
sltu	$2,$7,$2
beq	$2,$0,$L56
addiu	$2,$7,2

sw	$2,8($4)
lbu	$2,1($7)
lbu	$7,0($7)
sll	$2,$2,8
or	$2,$2,$7
sll	$9,$2,8
srl	$2,$2,8
or	$2,$9,$2
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
or	$8,$8,$2
$L56:
addiu	$7,$3,-1
mul	$7,$7,$12
sra	$7,$7,8
addiu	$7,$7,1
sll	$9,$7,16
sltu	$2,$8,$9
xori	$2,$2,0x1
bne	$2,$0,$L82
sw	$6,4($4)

sw	$7,0($4)
$L66:
addu	$10,$10,$2
sw	$8,16($4)
bne	$10,$0,$L83
move	$2,$0

j	$31
nop

$L79:
sw	$3,8($4)
lbu	$3,1($8)
lbu	$8,0($8)
sll	$3,$3,8
or	$3,$3,$8
sll	$8,$3,8
srl	$3,$3,8
or	$3,$8,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
b	$L39
or	$7,$7,$3

$L82:
subu	$3,$3,$7
subu	$8,$8,$9
b	$L66
sw	$3,0($4)

$L80:
lbu	$12,12($5)
lbu	$2,0($2)
sll	$7,$3,$2
addu	$6,$2,$6
sll	$2,$8,$2
bltz	$6,$L50
sw	$7,0($4)

lw	$9,8($4)
lw	$8,12($4)
sltu	$8,$9,$8
beq	$8,$0,$L86
addiu	$3,$7,-1

addiu	$3,$9,2
sw	$3,8($4)
lbu	$8,1($9)
lbu	$3,0($9)
sll	$8,$8,8
or	$8,$8,$3
sll	$9,$8,8
srl	$8,$8,8
or	$8,$9,$8
andi	$8,$8,0xffff
sll	$8,$8,$6
addiu	$6,$6,-16
or	$2,$2,$8
$L50:
addiu	$3,$7,-1
$L86:
mul	$3,$3,$12
sra	$3,$3,8
addiu	$3,$3,1
sll	$8,$3,16
sltu	$9,$2,$8
bne	$9,$0,$L51
sw	$6,4($4)

subu	$3,$7,$3
subu	$8,$2,$8
sw	$3,0($4)
b	$L49
sw	$8,16($4)

$L81:
sw	$2,8($4)
lbu	$2,1($7)
lbu	$5,0($7)
sll	$2,$2,8
or	$2,$2,$5
sll	$7,$2,8
srl	$2,$2,8
or	$2,$7,$2
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
b	$L59
or	$8,$8,$2

$L51:
sw	$3,0($4)
move	$8,$2
b	$L61
sw	$2,16($4)

$L83:
b	$L61
lw	$3,0($4)

.set	macro
.set	reorder
.end	read_mv_component
.size	read_mv_component, .-read_mv_component
.section	.text.vp8_decode_flush,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_decode_flush
.type	vp8_decode_flush, @function
vp8_decode_flush:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$17,32($sp)
sw	$19,40($sp)
sw	$16,28($sp)
sw	$18,36($sp)
move	$18,$4
.cprestore	16
sw	$31,44($sp)
lw	$17,136($4)
addiu	$16,$17,4736
addiu	$19,$17,5600
$L89:
lw	$2,0($16)
move	$5,$16
move	$4,$18
beq	$2,$0,$L88
addiu	$16,$16,216

lw	$25,260($18)
jalr	$25
nop

lw	$28,16($sp)
$L88:
bne	$19,$16,$L89
lw	$25,%call16(memset)($28)

move	$5,$0
li	$6,16			# 0x10
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$19

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5856

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5860

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5864

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5880

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5616

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5876

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$17,5872

lw	$31,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$16,28($sp)
sw	$0,5852($17)
lw	$17,32($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	vp8_decode_flush
.size	vp8_decode_flush, .-vp8_decode_flush
.section	.text.unlikely.vp8_decode_free,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	vp8_decode_free
.type	vp8_decode_free, @function
vp8_decode_free:
.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$25,%got(vp8_decode_flush)($28)
addiu	$sp,$sp,-32
sw	$31,28($sp)
addiu	$25,$25,%lo(vp8_decode_flush)
.cprestore	16
.reloc	1f,R_MIPS_JALR,vp8_decode_flush
1:	jalr	$25
nop

move	$2,$0
lw	$28,16($sp)
lw	$31,28($sp)
lw	$3,%got(use_jz_buf)($28)
sw	$0,0($3)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	vp8_decode_free
.size	vp8_decode_free, .-vp8_decode_free
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"Edge emulation not supported\012\000"
.section	.text.unlikely.vp8_decode_init,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	vp8_decode_init
.type	vp8_decode_init, @function
vp8_decode_init:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-40
lw	$2,%got(use_jz_buf)($28)
lw	$25,%call16(dsputil_init)($28)
move	$5,$4
sw	$17,28($sp)
sw	$18,32($sp)
li	$18,1			# 0x1
sw	$16,24($sp)
move	$16,$4
.cprestore	16
sw	$31,36($sp)
sw	$18,0($2)
lw	$17,136($4)
addiu	$4,$17,4
sw	$16,0($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
sw	$0,52($16)
.set	macro
.set	reorder

addiu	$4,$17,4404
lw	$28,16($sp)
lw	$25,%call16(ff_h264_pred_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h264_pred_init
1:	jalr	$25
li	$5,146			# 0x92
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$25,%call16(ff_vp8dsp_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_vp8dsp_init
1:	jalr	$25
addiu	$4,$17,4124
.set	macro
.set	reorder

lw	$17,12($16)
andi	$17,$17,0x4000
.set	noreorder
.set	nomacro
beq	$17,$0,$L98
lw	$28,16($sp)
.set	macro
.set	reorder

lw	$6,%got($LC0)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$17,-1163395072			# 0xffffffffbaa80000
move	$4,$16
addiu	$6,$6,%lo($LC0)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
ori	$17,$17,0xbeb0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L107
lw	$31,36($sp)
.set	macro
.set	reorder

$L98:
li	$2,7			# 0x7
#APP
# 2996 "vp8.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
lw	$2,%got(aux_base)($28)
li	$3,-201326592			# 0xfffffffff4000000
addiu	$4,$3,16384
lw	$2,0($2)
sw	$18,0($2)
$L100:
sw	$0,0($3)
addiu	$3,$3,4
.set	noreorder
.set	nomacro
bne	$3,$4,$L100
lw	$2,%got(tcsm1_base)($28)
.set	macro
.set	reorder

li	$4,49152			# 0xc000
lw	$3,0($2)
addu	$4,$3,$4
$L101:
sw	$0,0($3)
addiu	$3,$3,4
.set	noreorder
.set	nomacro
bne	$3,$4,$L101
lw	$2,%got(sram_base)($28)
.set	macro
.set	reorder

lw	$3,0($2)
addiu	$4,$3,12288
$L102:
sw	$0,0($3)
addiu	$3,$3,4
.set	noreorder
.set	nomacro
bne	$3,$4,$L102
lw	$2,%got(vpu_base)($28)
.set	macro
.set	reorder

lw	$3,0($2)
li	$2,-2147483648			# 0xffffffff80000000
addiu	$2,$2,68
#APP
# 3001 "vp8.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,%got(vpFrame)($28)
li	$4,256			# 0x100
lw	$25,%call16(jz4740_alloc_frame)($28)
li	$5,84			# 0x54
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
sw	$0,0($2)
.set	macro
.set	reorder

li	$4,256			# 0x100
lw	$28,16($sp)
li	$5,84			# 0x54
lw	$3,%got(mb0)($28)
lw	$25,%call16(jz4740_alloc_frame)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
sw	$2,%lo(mb0)($3)
.set	macro
.set	reorder

li	$4,256			# 0x100
lw	$28,16($sp)
li	$5,84			# 0x54
lw	$3,%got(mb1)($28)
lw	$25,%call16(jz4740_alloc_frame)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
sw	$2,%lo(mb1)($3)
.set	macro
.set	reorder

li	$4,-1288962048			# 0xffffffffb32c0000
lw	$28,16($sp)
move	$5,$0
addiu	$4,$4,18276
li	$6,800			# 0x320
lw	$3,%got(mb2)($28)
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$2,%lo(mb2)($3)
.set	macro
.set	reorder

li	$3,1			# 0x1
lw	$28,16($sp)
li	$2,-201326592			# 0xfffffffff4000000
sw	$3,6736($2)
lw	$2,%got(crc_code)($28)
sh	$0,0($2)
lw	$31,36($sp)
$L107:
move	$2,$17
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

.end	vp8_decode_init
.size	vp8_decode_init, .-vp8_decode_init
.section	.text.decode_block_coeffs_internal.isra.2,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_block_coeffs_internal.isra.2
.type	decode_block_coeffs_internal.isra.2, @function
decode_block_coeffs_internal.isra.2:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$7,$7,1
lw	$10,%got(zigzag_scan)($28)
addiu	$sp,$sp,-16
lw	$12,%got(vp8_dct_cat_prob)($28)
sll	$2,$7,5
lw	$8,0($4)
lw	$15,16($4)
li	$11,16			# 0x10
addu	$3,$2,$7
lw	$14,32($sp)
lw	$2,4($4)
addiu	$10,$10,%lo(zigzag_scan)
lw	$9,%got(ff_vp56_norm_shift)($28)
addu	$6,$6,$3
li	$13,8			# 0x8
sw	$18,12($sp)
addiu	$12,$12,%lo(vp8_dct_cat_prob)
sw	$17,8($sp)
sw	$16,4($sp)
$L109:
addu	$3,$9,$8
lbu	$16,1($14)
lbu	$24,0($3)
sll	$3,$8,$24
addu	$2,$24,$2
sll	$15,$15,$24
bltz	$2,$L114
sw	$3,0($4)

lw	$24,8($4)
lw	$8,12($4)
sltu	$8,$24,$8
beq	$8,$0,$L172
addiu	$8,$3,-1

addiu	$8,$24,2
sw	$8,8($4)
lbu	$25,1($24)
lbu	$8,0($24)
sll	$25,$25,8
or	$25,$25,$8
sll	$8,$25,8
srl	$25,$25,8
or	$25,$8,$25
andi	$25,$25,0xffff
sll	$25,$25,$2
addiu	$2,$2,-16
or	$15,$15,$25
$L114:
addiu	$8,$3,-1
$L172:
mul	$8,$16,$8
sra	$8,$8,8
addiu	$8,$8,1
sll	$24,$8,16
sltu	$16,$15,$24
bne	$16,$0,$L115
sw	$2,4($4)

subu	$8,$3,$8
subu	$15,$15,$24
addu	$3,$9,$8
sw	$8,0($4)
sw	$15,16($4)
lbu	$3,0($3)
lbu	$16,2($14)
sll	$24,$8,$3
addu	$2,$2,$3
sll	$8,$15,$3
bltz	$2,$L117
sw	$24,0($4)

lw	$15,8($4)
lw	$3,12($4)
sltu	$3,$15,$3
beq	$3,$0,$L173
addiu	$3,$24,-1

addiu	$3,$15,2
sw	$3,8($4)
lbu	$3,1($15)
lbu	$15,0($15)
sll	$3,$3,8
or	$3,$3,$15
sll	$15,$3,8
srl	$3,$3,8
or	$3,$15,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$8,$8,$3
$L117:
addiu	$3,$24,-1
$L173:
mul	$3,$16,$3
sra	$3,$3,8
addiu	$3,$3,1
sll	$15,$3,16
sltu	$16,$8,$15
bne	$16,$0,$L118
sw	$2,4($4)

subu	$3,$24,$3
subu	$8,$8,$15
addu	$15,$9,$3
sw	$3,0($4)
sw	$8,16($4)
lbu	$15,0($15)
lbu	$16,3($14)
sll	$3,$3,$15
addu	$2,$2,$15
sll	$8,$8,$15
bltz	$2,$L120
sw	$3,0($4)

lw	$17,8($4)
lw	$15,12($4)
sltu	$15,$17,$15
beq	$15,$0,$L174
addiu	$15,$3,-1

addiu	$15,$17,2
sw	$15,8($4)
lbu	$15,1($17)
lbu	$17,0($17)
sll	$15,$15,8
or	$15,$15,$17
sll	$17,$15,8
srl	$15,$15,8
or	$15,$17,$15
andi	$15,$15,0xffff
sll	$15,$15,$2
addiu	$2,$2,-16
or	$8,$8,$15
$L120:
addiu	$15,$3,-1
$L174:
mul	$15,$16,$15
sra	$15,$15,8
addiu	$15,$15,1
sll	$16,$15,16
sltu	$17,$8,$16
bne	$17,$0,$L122
sw	$2,4($4)

subu	$3,$3,$15
subu	$8,$8,$16
addu	$15,$9,$3
sw	$3,0($4)
sw	$8,16($4)
lbu	$18,0($15)
lbu	$17,6($14)
sll	$3,$3,$18
addu	$2,$2,$18
sll	$8,$8,$18
bltz	$2,$L124
sw	$3,0($4)

lw	$16,8($4)
lw	$15,12($4)
sltu	$15,$16,$15
beq	$15,$0,$L124
addiu	$15,$16,2

sw	$15,8($4)
lbu	$15,1($16)
lbu	$16,0($16)
sll	$15,$15,8
or	$15,$15,$16
sll	$16,$15,8
srl	$15,$15,8
or	$15,$16,$15
andi	$15,$15,0xffff
sll	$15,$15,$2
addiu	$2,$2,-16
or	$8,$8,$15
$L124:
addiu	$16,$3,-1
mul	$16,$17,$16
sra	$16,$16,8
addiu	$16,$16,1
sll	$17,$16,16
sltu	$15,$8,$17
bne	$15,$0,$L132
sw	$2,4($4)

subu	$15,$3,$16
subu	$17,$8,$17
addu	$3,$9,$15
sw	$15,0($4)
sw	$17,16($4)
lbu	$8,0($3)
lbu	$18,8($14)
sll	$16,$15,$8
addu	$2,$2,$8
sll	$8,$17,$8
bltz	$2,$L134
sw	$16,0($4)

lw	$3,8($4)
lw	$15,12($4)
sltu	$15,$3,$15
beq	$15,$0,$L134
addiu	$15,$3,2

sw	$15,8($4)
lbu	$15,1($3)
lbu	$3,0($3)
sll	$15,$15,8
or	$15,$15,$3
sll	$3,$15,8
srl	$15,$15,8
or	$15,$3,$15
andi	$3,$15,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$8,$8,$3
$L134:
addiu	$3,$16,-1
mul	$3,$3,$18
sra	$3,$3,8
addiu	$3,$3,1
sll	$15,$3,16
sltu	$17,$8,$15
xori	$17,$17,0x1
beq	$17,$0,$L168
sw	$2,4($4)

subu	$3,$16,$3
subu	$8,$8,$15
$L168:
addu	$15,$9,$3
sw	$3,0($4)
addu	$14,$14,$17
sw	$8,16($4)
lbu	$15,0($15)
lbu	$14,9($14)
sll	$16,$3,$15
addu	$18,$2,$15
sll	$8,$8,$15
bltz	$18,$L145
sw	$16,0($4)

lw	$3,8($4)
lw	$2,12($4)
sltu	$2,$3,$2
beq	$2,$0,$L145
addiu	$2,$3,2

sw	$2,8($4)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$18
addiu	$18,$18,-16
or	$8,$8,$2
$L145:
addiu	$3,$16,-1
mul	$3,$3,$14
sra	$3,$3,8
addiu	$3,$3,1
sll	$2,$3,16
sltu	$14,$8,$2
xori	$14,$14,0x1
beq	$14,$0,$L169
sw	$18,4($4)

subu	$3,$16,$3
subu	$8,$8,$2
$L169:
sll	$17,$17,1
sw	$3,0($4)
sw	$8,16($4)
move	$15,$0
addu	$14,$14,$17
sll	$2,$14,2
sll	$14,$13,$14
addu	$2,$2,$12
addiu	$17,$14,3
lw	$25,0($2)
lbu	$14,0($25)
$L149:
addu	$2,$9,$3
sll	$15,$15,1
addiu	$25,$25,1
lbu	$16,0($2)
sll	$24,$3,$16
addu	$2,$16,$18
addiu	$3,$24,-1
sw	$24,0($4)
sll	$16,$8,$16
bltz	$2,$L147
mul	$3,$3,$14

lw	$14,8($4)
lw	$8,12($4)
sltu	$8,$14,$8
beq	$8,$0,$L147
addiu	$18,$14,2

sw	$18,8($4)
lbu	$8,1($14)
lbu	$14,0($14)
sll	$8,$8,8
or	$8,$8,$14
sll	$14,$8,8
srl	$8,$8,8
or	$8,$14,$8
andi	$8,$8,0xffff
sll	$8,$8,$2
addiu	$2,$2,-16
or	$16,$16,$8
$L147:
sra	$3,$3,8
sw	$2,4($4)
addiu	$3,$3,1
sll	$18,$3,16
sltu	$14,$16,$18
xori	$14,$14,0x1
beq	$14,$0,$L170
move	$8,$16

subu	$3,$24,$3
subu	$8,$16,$18
$L170:
sw	$3,0($4)
addu	$15,$15,$14
sw	$8,16($4)
lbu	$14,0($25)
bne	$14,$0,$L149
move	$18,$2

addu	$15,$17,$15
$L131:
addiu	$14,$6,22
$L121:
addu	$16,$9,$3
addu	$17,$10,$7
lbu	$16,0($16)
lbu	$25,-1($17)
sll	$17,$3,$16
sll	$25,$25,1
addu	$3,$16,$2
addu	$25,$5,$25
sw	$17,0($4)
bltz	$3,$L150
sll	$8,$8,$16

lw	$16,8($4)
lw	$2,12($4)
sltu	$2,$16,$2
beq	$2,$0,$L175
addiu	$24,$17,-1

addiu	$2,$16,2
sw	$2,8($4)
lbu	$2,1($16)
lbu	$16,0($16)
sll	$2,$2,8
or	$2,$2,$16
sll	$16,$2,8
srl	$2,$2,8
or	$2,$16,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$8,$8,$2
$L150:
addiu	$24,$17,-1
$L175:
sll	$24,$24,7
sra	$24,$24,8
addiu	$24,$24,1
sll	$2,$24,16
sltu	$16,$8,$2
bne	$16,$0,$L151
sw	$3,4($4)

subu	$15,$0,$15
subu	$24,$17,$24
sll	$15,$15,16
subu	$16,$8,$2
sra	$15,$15,16
sw	$24,0($4)
slt	$8,$7,16
sw	$16,16($4)
move	$17,$7
beq	$8,$0,$L163
sh	$15,0($25)

$L152:
addu	$2,$9,$24
lbu	$15,0($14)
lbu	$8,0($2)
sll	$24,$24,$8
addu	$2,$8,$3
sll	$3,$16,$8
bltz	$2,$L110
sw	$24,0($4)

lw	$16,8($4)
lw	$8,12($4)
sltu	$8,$16,$8
beq	$8,$0,$L176
addiu	$8,$24,-1

addiu	$8,$16,2
sw	$8,8($4)
lbu	$8,1($16)
lbu	$16,0($16)
sll	$8,$8,8
or	$8,$8,$16
sll	$16,$8,8
srl	$8,$8,8
or	$8,$16,$8
andi	$8,$8,0xffff
sll	$8,$8,$2
addiu	$2,$2,-16
or	$3,$3,$8
$L110:
addiu	$8,$24,-1
$L176:
mul	$8,$15,$8
sra	$8,$8,8
addiu	$8,$8,1
sll	$15,$8,16
sltu	$16,$3,$15
bne	$16,$0,$L111
sw	$2,4($4)

subu	$3,$3,$15
subu	$8,$24,$8
move	$15,$3
sw	$3,16($4)
addiu	$7,$7,1
sw	$8,0($4)
b	$L109
addiu	$6,$6,33

$L115:
sw	$8,0($4)
beq	$7,$11,$L160
sw	$15,16($4)

move	$14,$6
addiu	$7,$7,1
b	$L109
addiu	$6,$6,33

$L151:
sll	$15,$15,16
sw	$8,16($4)
move	$16,$8
sw	$24,0($4)
sra	$15,$15,16
slt	$8,$7,16
move	$17,$7
bne	$8,$0,$L152
sh	$15,0($25)

$L163:
move	$2,$17
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

$L118:
sw	$3,0($4)
addiu	$14,$6,11
sw	$8,16($4)
b	$L121
li	$15,1			# 0x1

$L122:
addu	$3,$9,$15
sw	$15,0($4)
sw	$8,16($4)
lbu	$17,4($14)
lbu	$16,0($3)
sll	$15,$15,$16
addu	$2,$2,$16
sll	$16,$8,$16
bltz	$2,$L125
sw	$15,0($4)

lw	$8,8($4)
lw	$3,12($4)
sltu	$3,$8,$3
beq	$3,$0,$L177
addiu	$3,$15,-1

addiu	$3,$8,2
sw	$3,8($4)
lbu	$3,1($8)
lbu	$8,0($8)
sll	$3,$3,8
or	$3,$3,$8
sll	$8,$3,8
srl	$3,$3,8
or	$3,$8,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$16,$16,$3
$L125:
addiu	$3,$15,-1
$L177:
mul	$3,$17,$3
sra	$3,$3,8
addiu	$3,$3,1
sll	$17,$3,16
sltu	$8,$16,$17
bne	$8,$0,$L126
sw	$2,4($4)

subu	$15,$15,$3
subu	$17,$16,$17
addu	$3,$9,$15
sw	$15,0($4)
sw	$17,16($4)
lbu	$8,0($3)
lbu	$3,5($14)
sll	$16,$15,$8
addu	$2,$2,$8
sll	$8,$17,$8
bltz	$2,$L128
sw	$16,0($4)

lw	$14,8($4)
lw	$15,12($4)
sltu	$15,$14,$15
beq	$15,$0,$L128
addiu	$15,$14,2

sw	$15,8($4)
lbu	$15,1($14)
lbu	$14,0($14)
sll	$15,$15,8
or	$15,$15,$14
sll	$14,$15,8
srl	$15,$15,8
or	$15,$14,$15
andi	$14,$15,0xffff
sll	$14,$14,$2
addiu	$2,$2,-16
or	$8,$8,$14
$L128:
addiu	$14,$16,-1
mul	$14,$14,$3
sra	$14,$14,8
addiu	$3,$14,1
sll	$14,$3,16
sltu	$15,$8,$14
xori	$15,$15,0x1
beq	$15,$0,$L165
sw	$2,4($4)

subu	$3,$16,$3
subu	$8,$8,$14
$L165:
sw	$3,0($4)
addiu	$15,$15,3
b	$L131
sw	$8,16($4)

$L126:
sw	$3,0($4)
move	$8,$16
sw	$16,16($4)
b	$L131
li	$15,2			# 0x2

$L132:
addu	$3,$9,$16
sw	$16,0($4)
sw	$8,16($4)
lbu	$14,7($14)
lbu	$17,0($3)
sll	$16,$16,$17
addu	$2,$2,$17
sll	$8,$8,$17
bltz	$2,$L135
sw	$16,0($4)

lw	$15,8($4)
lw	$3,12($4)
sltu	$3,$15,$3
beq	$3,$0,$L178
addiu	$3,$16,-1

addiu	$3,$15,2
sw	$3,8($4)
lbu	$3,1($15)
lbu	$15,0($15)
sll	$3,$3,8
or	$3,$3,$15
sll	$15,$3,8
srl	$3,$3,8
or	$3,$15,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$8,$8,$3
$L135:
addiu	$3,$16,-1
$L178:
mul	$3,$14,$3
sra	$3,$3,8
addiu	$3,$3,1
sll	$14,$3,16
sltu	$15,$8,$14
bne	$15,$0,$L136
sw	$2,4($4)

subu	$16,$16,$3
subu	$14,$8,$14
addu	$3,$9,$16
sw	$14,16($4)
lbu	$8,0($3)
sll	$16,$16,$8
addu	$2,$2,$8
sll	$8,$14,$8
bltz	$2,$L138
sw	$16,0($4)

lw	$14,8($4)
lw	$3,12($4)
sltu	$3,$14,$3
beq	$3,$0,$L179
addiu	$3,$16,-1

addiu	$3,$14,2
sw	$3,8($4)
lbu	$3,1($14)
lbu	$14,0($14)
sll	$3,$3,8
or	$3,$3,$14
sll	$14,$3,8
srl	$3,$3,8
or	$3,$14,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$8,$8,$3
$L138:
addiu	$3,$16,-1
$L179:
sll	$14,$3,2
addu	$14,$14,$3
sll	$17,$14,5
addu	$17,$14,$17
sra	$17,$17,8
addiu	$17,$17,1
sll	$3,$17,16
sltu	$15,$8,$3
xori	$15,$15,0x1
bne	$15,$0,$L171
sw	$2,4($4)

$L141:
addu	$3,$9,$17
sw	$8,16($4)
sll	$15,$15,1
lbu	$3,0($3)
addiu	$15,$15,7
sll	$17,$17,$3
addu	$2,$2,$3
sll	$8,$8,$3
bltz	$2,$L142
sw	$17,0($4)

lw	$14,8($4)
lw	$3,12($4)
sltu	$3,$14,$3
beq	$3,$0,$L142
addiu	$3,$14,2

sw	$3,8($4)
lbu	$3,1($14)
lbu	$14,0($14)
sll	$3,$3,8
or	$3,$3,$14
sll	$14,$3,8
srl	$3,$3,8
or	$3,$14,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$8,$8,$3
$L142:
addiu	$14,$17,-1
sll	$16,$14,4
sll	$3,$14,7
addu	$3,$16,$3
addu	$3,$3,$14
sra	$3,$3,8
addiu	$3,$3,1
sll	$16,$3,16
sltu	$14,$8,$16
xori	$14,$14,0x1
beq	$14,$0,$L167
sw	$2,4($4)

subu	$3,$17,$3
subu	$8,$8,$16
$L167:
sw	$3,0($4)
addu	$15,$15,$14
b	$L131
sw	$8,16($4)

$L111:
move	$2,$17
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,16
sw	$8,0($4)
j	$31
sw	$3,16($4)

$L136:
addu	$14,$9,$3
sw	$8,16($4)
lbu	$14,0($14)
sll	$17,$3,$14
addu	$2,$2,$14
sll	$8,$8,$14
bltz	$2,$L139
sw	$17,0($4)

lw	$14,8($4)
lw	$3,12($4)
sltu	$3,$14,$3
beq	$3,$0,$L180
addiu	$15,$17,-1

addiu	$3,$14,2
sw	$3,8($4)
lbu	$3,1($14)
lbu	$14,0($14)
sll	$3,$3,8
or	$3,$3,$14
sll	$14,$3,8
srl	$3,$3,8
or	$3,$14,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$8,$8,$3
$L139:
addiu	$15,$17,-1
$L180:
sll	$14,$15,5
sll	$3,$15,7
addu	$3,$14,$3
subu	$3,$3,$15
sra	$3,$3,8
addiu	$3,$3,1
sll	$14,$3,16
sltu	$15,$8,$14
xori	$15,$15,0x1
beq	$15,$0,$L166
sw	$2,4($4)

subu	$3,$17,$3
subu	$8,$8,$14
$L166:
sw	$3,0($4)
addiu	$15,$15,5
b	$L131
sw	$8,16($4)

$L171:
subu	$17,$16,$17
b	$L141
subu	$8,$8,$3

$L160:
li	$17,16			# 0x10
lw	$18,12($sp)
lw	$16,4($sp)
move	$2,$17
lw	$17,8($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	decode_block_coeffs_internal.isra.2
.size	decode_block_coeffs_internal.isra.2, .-decode_block_coeffs_internal.isra.2
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"0x%02x,\000"
.align	2
$LC2:
.ascii	"\012\000"
.section	.text.ptr_square.constprop.8,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	ptr_square.constprop.8
.type	ptr_square.constprop.8, @function
ptr_square.constprop.8:
.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
blez	$5,$L189
nop

addiu	$sp,$sp,-64
sw	$23,56($sp)
sw	$18,36($sp)
sw	$20,44($sp)
move	$20,$0
sw	$22,52($sp)
move	$22,$5
sw	$21,48($sp)
move	$21,$6
sw	$19,40($sp)
move	$19,$4
.cprestore	16
sw	$31,60($sp)
sw	$17,32($sp)
sw	$16,28($sp)
lw	$23,%got($LC2)($28)
lw	$18,%got($LC1)($28)
addiu	$23,$23,%lo($LC2)
addiu	$18,$18,%lo($LC1)
$L183:
blez	$21,$L190
lw	$25,%call16(printf)($28)

addu	$17,$19,$21
move	$16,$19
$L184:
lw	$25,%call16(printf)($28)
addiu	$16,$16,1
lbu	$5,-1($16)
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
move	$4,$18

bne	$17,$16,$L184
lw	$28,16($sp)

lw	$25,%call16(printf)($28)
$L190:
addiu	$20,$20,1
move	$4,$23
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$19,$19,16

bne	$20,$22,$L183
lw	$28,16($sp)

lw	$31,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
addiu	$sp,$sp,64
$L189:
j	$31
nop

.set	macro
.set	reorder
.end	ptr_square.constprop.8
.size	ptr_square.constprop.8, .-ptr_square.constprop.8
.section	.text.motion_init_vp8,"ax",@progbits
.align	2
.align	5
.globl	motion_init_vp8
.set	nomips16
.set	nomicromips
.ent	motion_init_vp8
.type	motion_init_vp8, @function
motion_init_vp8:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
sll	$3,$5,4
sll	$6,$4,6
sll	$2,$4,4
sll	$5,$5,6
addu	$2,$2,$6
addu	$5,$3,$5
lw	$3,%got(IntpFMT)($28)
sll	$6,$2,3
sll	$9,$5,3
addiu	$sp,$sp,-32
li	$11,-1289420800			# 0xffffffffb3250000
addiu	$3,$3,%lo(IntpFMT)
sw	$17,4($sp)
subu	$2,$6,$2
sw	$16,0($sp)
subu	$9,$9,$5
sw	$23,28($sp)
addiu	$17,$11,1280
sw	$22,24($sp)
addiu	$16,$11,1284
sw	$21,20($sp)
addiu	$25,$11,1028
sw	$20,16($sp)
addiu	$24,$11,1024
sw	$19,12($sp)
addiu	$15,$11,1156
sw	$18,8($sp)
addiu	$14,$11,1152
addiu	$13,$11,3328
addu	$2,$3,$2
addu	$9,$3,$9
move	$10,$0
addiu	$11,$11,3332
li	$12,128			# 0x80
$L192:
lbu	$7,5($2)
addu	$18,$10,$17
lbu	$6,0($2)
lbu	$8,33($2)
lbu	$5,1($2)
sll	$7,$7,31
lbu	$21,7($2)
andi	$6,$6,0x3
lbu	$3,25($2)
andi	$8,$8,0x1
sll	$6,$6,28
lbu	$20,27($2)
andi	$5,$5,0x1
lbu	$19,29($2)
or	$7,$8,$7
lbu	$8,31($2)
sll	$5,$5,27
andi	$21,$21,0x1
or	$7,$7,$6
sll	$21,$21,24
or	$6,$7,$5
sll	$5,$3,16
andi	$7,$20,0xf
or	$6,$6,$21
sll	$7,$7,8
andi	$3,$19,0x1
or	$5,$6,$5
sll	$3,$3,2
andi	$6,$8,0x1
or	$5,$5,$7
sll	$6,$6,1
or	$3,$5,$3
or	$3,$3,$6
#APP
# 328 "vp8.c" 1
sw	 $3,0($18)	#i_sw
# 0 "" 2
#NO_APP
lbu	$23,2($2)
addu	$18,$10,$16
lbu	$19,6($2)
lbu	$8,34($2)
lbu	$6,3($2)
andi	$7,$23,0x1
lbu	$5,4($2)
sll	$19,$19,31
lbu	$22,8($2)
sll	$23,$7,27
andi	$8,$8,0x1
lbu	$3,26($2)
andi	$6,$6,0x1
lbu	$21,28($2)
or	$8,$8,$19
lbu	$20,30($2)
sll	$6,$6,26
lbu	$19,32($2)
andi	$5,$5,0x1
or	$7,$8,$23
sll	$5,$5,25
andi	$8,$22,0x1
or	$7,$7,$6
sll	$8,$8,24
or	$6,$7,$5
sll	$5,$3,16
andi	$7,$21,0xf
or	$6,$6,$8
sll	$7,$7,8
andi	$3,$20,0x1
or	$5,$6,$5
sll	$3,$3,2
andi	$6,$19,0x1
or	$5,$5,$7
sll	$6,$6,1
or	$3,$5,$3
or	$3,$3,$6
#APP
# 328 "vp8.c" 1
sw	 $3,0($18)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,16($2)
addu	$8,$10,$25
lbu	$3,15($2)
lbu	$6,13($2)
lbu	$7,14($2)
sll	$5,$5,24
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$7,8
or	$3,$5,$3
or	$3,$3,$6
#APP
# 349 "vp8.c" 1
sw	 $3,0($8)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,12($2)
addu	$8,$10,$24
lbu	$3,11($2)
lbu	$6,9($2)
lbu	$7,10($2)
sll	$5,$5,24
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$7,8
or	$3,$5,$3
or	$3,$3,$6
#APP
# 349 "vp8.c" 1
sw	 $3,0($8)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,24($2)
addu	$8,$10,$15
lbu	$3,23($2)
lbu	$6,21($2)
lbu	$7,22($2)
sll	$5,$5,24
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$7,8
or	$3,$5,$3
or	$3,$3,$6
#APP
# 359 "vp8.c" 1
sw	 $3,0($8)	#i_sw
# 0 "" 2
#NO_APP
lb	$5,20($2)
addu	$8,$10,$14
lbu	$3,19($2)
lbu	$6,17($2)
lbu	$7,18($2)
sll	$5,$5,24
sll	$3,$3,16
or	$5,$6,$5
sll	$6,$7,8
or	$3,$5,$3
or	$3,$3,$6
#APP
# 359 "vp8.c" 1
sw	 $3,0($8)	#i_sw
# 0 "" 2
#NO_APP
lbu	$7,7($9)
addu	$18,$10,$13
lbu	$6,27($9)
lbu	$19,5($9)
lbu	$3,9($9)
sll	$7,$7,15
andi	$6,$6,0x7
lbu	$5,25($9)
lbu	$8,10($9)
sll	$6,$6,12
andi	$7,$7,0xffff
sll	$19,$19,31
or	$6,$7,$6
andi	$7,$3,0x7
or	$6,$6,$19
sll	$3,$7,9
andi	$5,$5,0x3f
andi	$7,$8,0x7
or	$5,$6,$5
sll	$6,$7,6
or	$3,$5,$3
or	$3,$3,$6
#APP
# 370 "vp8.c" 1
sw	 $3,0($18)	#i_sw
# 0 "" 2
#NO_APP
lbu	$7,8($9)
addu	$18,$10,$11
lbu	$6,28($9)
lbu	$19,6($9)
lbu	$3,17($9)
sll	$7,$7,15
andi	$6,$6,0x7
lbu	$5,26($9)
lbu	$8,18($9)
sll	$6,$6,12
andi	$7,$7,0xffff
sll	$19,$19,31
or	$6,$7,$6
andi	$7,$3,0x7
or	$6,$6,$19
sll	$3,$7,9
andi	$5,$5,0x3f
andi	$7,$8,0x7
or	$5,$6,$5
sll	$6,$7,6
or	$3,$5,$3
or	$3,$3,$6
#APP
# 370 "vp8.c" 1
sw	 $3,0($18)	#i_sw
# 0 "" 2
#NO_APP
addiu	$10,$10,8
addiu	$2,$2,35
.set	noreorder
.set	nomacro
bne	$10,$12,$L192
addiu	$9,$9,35
.set	macro
.set	reorder

li	$2,-1289420800			# 0xffffffffb3250000
li	$3,7			# 0x7
addiu	$5,$2,4
#APP
# 386 "vp8.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$5,$2,2052
#APP
# 389 "vp8.c" 1
sw	 $3,0($5)	#i_sw
# 0 "" 2
#NO_APP
li	$3,65289			# 0xff09
#APP
# 394 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,%got(SubPel)($28)
addiu	$6,$2,48
lw	$5,%got(AryFMT)($28)
addiu	$3,$3,%lo(SubPel)
addiu	$5,$5,%lo(AryFMT)
addu	$3,$4,$3
addu	$4,$4,$5
lb	$3,0($3)
lbu	$4,0($4)
addiu	$3,$3,-1
sll	$4,$4,31
sll	$3,$3,14
andi	$3,$3,0xffff
or	$3,$3,$4
#APP
# 396 "vp8.c" 1
sw	 $3,0($6)	#i_sw
# 0 "" 2
#NO_APP
move	$3,$0
addiu	$4,$2,2096
#APP
# 409 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,32
#APP
# 423 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,2080
#APP
# 428 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,36
#APP
# 434 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,44
#APP
# 442 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,2084
#APP
# 444 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,2088
#APP
# 452 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,2092
#APP
# 455 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,32
.set	macro
.set	reorder

.end	motion_init_vp8
.size	motion_init_vp8, .-motion_init_vp8
.section	.text.motion_config_vp8,"ax",@progbits
.align	2
.align	5
.globl	motion_config_vp8
.set	nomips16
.set	nomicromips
.ent	motion_config_vp8
.type	motion_config_vp8, @function
motion_config_vp8:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,131072			# 0x20000
li	$5,-1289420800			# 0xffffffffb3250000
ori	$2,$2,0xff89
#APP
# 461 "vp8.c" 1
sw	 $2,0($5)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5644($4)
addiu	$3,$5,76
addiu	$2,$2,4
sll	$2,$2,4
andi	$2,$2,0xff0
sll	$2,$2,16
ori	$2,$2,0x10
#APP
# 465 "vp8.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,5648($4)
addiu	$6,$5,80
lw	$2,5644($4)
sll	$3,$3,4
sll	$2,$2,4
andi	$3,$3,0xff0
sll	$3,$3,16
andi	$2,$2,0xff0
or	$2,$3,$2
#APP
# 466 "vp8.c" 1
sw	 $2,0($6)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5644($4)
addiu	$5,$5,2124
addiu	$2,$2,4
sll	$2,$2,4
andi	$2,$2,0xff0
sll	$2,$2,16
ori	$2,$2,0x8
#APP
# 468 "vp8.c" 1
sw	 $2,0($5)	#i_sw
# 0 "" 2
#NO_APP
j	$31
.end	motion_config_vp8
.size	motion_config_vp8, .-motion_config_vp8
.section	.rodata.str1.4
.align	2
$LC3:
.ascii	"decode_frame start\012\000"
.align	2
$LC4:
.ascii	"decode_frame %d\012\000"
.align	2
$LC5:
.ascii	"Unknown profile %d\012\000"
.align	2
$LC6:
.ascii	"Header size larger than data provided\012\000"
.align	2
$LC7:
.ascii	"Invalid start code 0x%x\012\000"
.align	2
$LC8:
.ascii	"Upscaling\000"
.align	2
$LC9:
.ascii	"Unspecified colorspace\012\000"
.align	2
$LC10:
.ascii	"get_buffer() failed!\012\000"
.align	2
$LC11:
.ascii	"Discarding interframe without a prior keyframe!\012\000"
.align	2
$LC15:
.ascii	"PMON:p0test:%d  mbmd:%d  p0allfrm:%d,%d\012\000"
.align	2
$LC16:
.ascii	"PMON:mcpoll:%d,%d  gp0poll:%d,%d  vmaupoll:%d,%d\012\000"
.align	2
$LC17:
.ascii	"mbx:%d mby:%d\012\000"
.align	2
$LC18:
.ascii	"frame: %d, crc_code: 0x%x\012\000"
.align	2
$LC19:
.ascii	"decode_frame end\012\000"
.align	2
$LC20:
.ascii	"Invalid partitions\012\000"
.section	.text.vp8_decode_frame,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_decode_frame
.type	vp8_decode_frame, @function
vp8_decode_frame:
.frame	$sp,288,$31		# vars= 208, regs= 10/0, args= 32, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$3,%got(vpFrame)($28)
addiu	$sp,$sp,-288
lw	$25,%call16(av_log)($28)
sw	$fp,280($sp)
sw	$6,296($sp)
sw	$4,288($sp)
lw	$2,0($3)
lw	$fp,136($4)
lw	$6,%got($LC3)($28)
addiu	$2,$2,1
.cprestore	32
sw	$5,292($sp)
li	$5,16			# 0x10
sw	$31,284($sp)
addiu	$6,$6,%lo($LC3)
sw	$23,276($sp)
sw	$22,272($sp)
sw	$21,268($sp)
sw	$20,264($sp)
sw	$19,260($sp)
sw	$18,256($sp)
sw	$17,252($sp)
sw	$16,248($sp)
sw	$7,300($sp)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,0($3)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,%got(vpFrame)($28)
lw	$25,%call16(printf)($28)
lw	$5,0($4)
lw	$4,%got($LC4)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC4)
.set	macro
.set	reorder

move	$2,$0
lw	$28,32($sp)
#APP
# 2307 "vp8.c" 1
mtc0	$2,$16,4
# 0 "" 2
# 2307 "vp8.c" 1
mtc0	$2,$16,5
# 0 "" 2
# 2307 "vp8.c" 1
mtc0	$2,$16,6
# 0 "" 2
# 2307 "vp8.c" 1
mfc0	$2,$16,7
# 0 "" 2
#NO_APP
li	$3,-65536			# 0xffffffffffff0000
addiu	$3,$3,3839
and	$2,$2,$3
ori	$2,$2,0x100
#APP
# 2307 "vp8.c" 1
mtc0	$2,$16,7
# 0 "" 2
#NO_APP
lw	$8,300($sp)
addiu	$9,$fp,5620
lw	$4,0($fp)
sw	$9,168($sp)
lw	$18,16($8)
lw	$17,20($8)
lw	$22,40($4)
lw	$21,44($4)
lbu	$2,0($18)
addiu	$10,$17,-3
andi	$2,$2,0x1
sw	$10,80($sp)
xori	$2,$2,0x1
sw	$2,5660($fp)
lbu	$7,0($18)
srl	$7,$7,1
andi	$7,$7,0x7
sltu	$5,$7,4
sw	$7,5640($fp)
lbu	$2,0($18)
srl	$2,$2,4
xori	$2,$2,0x1
andi	$2,$2,0x1
sw	$2,5664($fp)
lbu	$2,2($18)
lbu	$3,1($18)
lbu	$20,0($18)
sll	$2,$2,16
sll	$3,$3,8
or	$2,$2,$3
or	$20,$2,$20
sra	$20,$20,5
.set	noreorder
.set	nomacro
bne	$5,$0,$L197
sw	$20,92($sp)
.set	macro
.set	reorder

lw	$6,%got($LC5)($28)
li	$5,24			# 0x18
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC5)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$7,5640($fp)
$L197:
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
bne	$7,$0,$L198
addiu	$4,$fp,4624
.set	macro
.set	reorder

addiu	$5,$fp,4188
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
li	$6,108			# 0x6c
.set	macro
.set	reorder

li	$4,11			# 0xb
lw	$28,32($sp)
lw	$25,%call16(motion_init_vp8)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_init_vp8
1:	jalr	$25
li	$5,3			# 0x3
.set	macro
.set	reorder

lw	$3,5660($fp)
lw	$11,80($sp)
lw	$12,92($sp)
sll	$2,$3,3
subu	$2,$3,$2
addu	$2,$11,$2
slt	$2,$2,$12
.set	noreorder
.set	nomacro
bne	$2,$0,$L839
lw	$28,32($sp)
.set	macro
.set	reorder

$L200:
bne	$3,$0,$L202
addiu	$18,$18,3
sw	$18,76($sp)
$L203:
lw	$2,5856($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L211
lw	$7,0($fp)
.set	macro
.set	reorder

lw	$2,40($7)
beq	$2,$22,$L840
$L211:
lw	$25,%call16(av_image_check_size)($28)
move	$6,$0
move	$4,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_image_check_size
1:	jalr	$25
move	$5,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L303
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$25,%got(vp8_decode_flush)($28)
addiu	$25,$25,%lo(vp8_decode_flush)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_decode_flush
1:	jalr	$25
lw	$4,0($fp)
.set	macro
.set	reorder

move	$5,$22
lw	$28,32($sp)
move	$6,$21
lw	$25,%call16(avcodec_set_dimensions)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,avcodec_set_dimensions
1:	jalr	$25
lw	$4,0($fp)
.set	macro
.set	reorder

lw	$3,0($fp)
lw	$28,32($sp)
lw	$2,664($3)
lw	$4,660($3)
lw	$25,%call16(av_mallocz)($28)
addiu	$3,$2,15
addiu	$5,$2,30
slt	$6,$3,0
addiu	$2,$4,15
movn	$3,$5,$6
addiu	$4,$4,30
slt	$5,$2,0
movn	$2,$4,$5
sra	$4,$3,4
sra	$3,$2,4
sw	$4,5648($fp)
sll	$2,$4,1
addu	$2,$3,$2
sw	$3,5644($fp)
addiu	$2,$2,1
sll	$4,$2,3
sll	$2,$2,6
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$3,5644($fp)
lw	$28,32($sp)
sw	$2,5856($fp)
sll	$4,$3,1
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
addu	$4,$4,$3
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,5644($fp)
sw	$2,5860($fp)
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$4,2
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$3,5644($fp)
sw	$2,5864($fp)
sll	$4,$3,3
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
addu	$4,$4,$3
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$3,5644($fp)
sw	$2,5880($fp)
addiu	$4,$3,1
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
sll	$4,$4,5
.set	macro
.set	reorder

lw	$3,5648($fp)
lw	$4,5644($fp)
lw	$28,32($sp)
sw	$2,5876($fp)
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
mul	$4,$4,$3
.set	macro
.set	reorder

lw	$3,5856($fp)
lw	$28,32($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L303
sw	$2,5872($fp)
.set	macro
.set	reorder

lw	$4,5860($fp)
beq	$4,$0,$L303
lw	$4,5864($fp)
beq	$4,$0,$L303
lw	$4,5880($fp)
beq	$4,$0,$L303
lw	$4,5876($fp)
beq	$4,$0,$L303
.set	noreorder
.set	nomacro
beq	$2,$0,$L303
addiu	$3,$3,72
.set	macro
.set	reorder

sw	$3,5852($fp)
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
lw	$4,168($sp)
$L979:
lw	$5,76($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
lw	$6,92($sp)
.set	macro
.set	reorder

lw	$8,92($sp)
lw	$3,76($sp)
lw	$9,80($sp)
lw	$2,5660($fp)
lw	$28,32($sp)
addu	$16,$3,$8
.set	noreorder
.set	nomacro
beq	$2,$0,$L841
subu	$20,$9,$8
.set	macro
.set	reorder

lw	$6,5620($fp)
lw	$17,%got(ff_vp56_norm_shift)($28)
lw	$3,5624($fp)
lw	$4,5636($fp)
addu	$2,$17,$6
lbu	$2,0($2)
sll	$6,$6,$2
addu	$3,$2,$3
sll	$2,$4,$2
.set	noreorder
.set	nomacro
bltz	$3,$L218
sw	$6,5620($fp)
.set	macro
.set	reorder

lw	$5,5628($fp)
lw	$4,5632($fp)
sltu	$4,$5,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L910
addiu	$4,$6,-1
.set	macro
.set	reorder

addiu	$4,$5,2
sw	$4,5628($fp)
lbu	$4,1($5)
lbu	$5,0($5)
sll	$4,$4,8
or	$4,$4,$5
sll	$5,$4,8
srl	$4,$4,8
or	$4,$5,$4
andi	$4,$4,0xffff
sll	$4,$4,$3
addiu	$3,$3,-16
or	$2,$2,$4
$L218:
addiu	$4,$6,-1
$L910:
sll	$4,$4,7
sra	$4,$4,8
addiu	$4,$4,1
sll	$5,$4,16
sltu	$7,$2,$5
.set	noreorder
.set	nomacro
bne	$7,$0,$L219
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$3,$6,$4
lw	$6,%got($LC9)($28)
subu	$2,$2,$5
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
li	$5,24			# 0x18
addiu	$6,$6,%lo($LC9)
sw	$3,5620($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,5636($fp)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$4,5620($fp)
lw	$3,5624($fp)
lw	$2,5636($fp)
$L590:
addu	$5,$17,$4
lbu	$5,0($5)
sll	$4,$4,$5
addu	$6,$5,$3
sll	$3,$2,$5
.set	noreorder
.set	nomacro
bltz	$6,$L220
sw	$4,5620($fp)
.set	macro
.set	reorder

lw	$2,5628($fp)
lw	$5,5632($fp)
sltu	$5,$2,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L911
addiu	$5,$4,-1
.set	macro
.set	reorder

addiu	$5,$2,2
sw	$5,5628($fp)
lbu	$5,1($2)
lbu	$2,0($2)
sll	$5,$5,8
or	$5,$5,$2
sll	$2,$5,8
srl	$5,$5,8
or	$2,$2,$5
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
or	$3,$3,$2
$L220:
addiu	$5,$4,-1
$L911:
sll	$5,$5,7
sra	$5,$5,8
addiu	$5,$5,1
sll	$2,$5,16
sltu	$7,$3,$2
.set	noreorder
.set	nomacro
bne	$7,$0,$L221
sw	$6,5624($fp)
.set	macro
.set	reorder

subu	$5,$4,$5
subu	$3,$3,$2
$L221:
sw	$3,5636($fp)
.set	noreorder
.set	nomacro
b	$L217
move	$2,$6
.set	macro
.set	reorder

$L303:
lw	$2,5668($fp)
bne	$2,$0,$L305
lw	$2,5672($fp)
bne	$2,$0,$L630
$L305:
lw	$3,5660($fp)
li	$2,48			# 0x30
li	$4,32			# 0x20
li	$17,1			# 0x1
movz	$2,$4,$3
$L588:
lw	$8,288($sp)
lw	$3,708($8)
slt	$3,$3,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L306
addiu	$9,$fp,4736
.set	macro
.set	reorder

addiu	$10,$fp,5600
li	$2,1			# 0x1
sw	$9,204($sp)
sw	$10,200($sp)
sw	$2,5664($fp)
$L307:
lw	$2,5684($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L575
addiu	$2,$fp,9180
.set	macro
.set	reorder

addiu	$3,$fp,6884
addiu	$4,$fp,11468
$L576:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L576
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
$L575:
lw	$3,5676($fp)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L842
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$2,$L843
addiu	$3,$3,1400
.set	macro
.set	reorder

lw	$2,5672($fp)
$L966:
sll	$3,$3,2
addu	$3,$fp,$3
lw	$3,0($3)
sw	$3,5612($fp)
$L581:
li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
beq	$2,$3,$L579
addiu	$2,$2,1400
.set	macro
.set	reorder

sll	$2,$2,2
addu	$2,$fp,$2
lw	$2,0($2)
sw	$2,5608($fp)
$L579:
lw	$2,5668($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L912
lw	$16,204($sp)
.set	macro
.set	reorder

lw	$2,5600($fp)
sw	$2,5604($fp)
lw	$16,204($sp)
$L912:
lw	$17,288($sp)
$L584:
lw	$2,0($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L583
move	$5,$16
.set	macro
.set	reorder

lw	$2,5600($fp)
.set	noreorder
.set	nomacro
beq	$16,$2,$L583
move	$4,$17
.set	macro
.set	reorder

lw	$2,5604($fp)
.set	noreorder
.set	nomacro
beq	$16,$2,$L913
lw	$8,200($sp)
.set	macro
.set	reorder

lw	$2,5608($fp)
beq	$16,$2,$L913
lw	$2,5612($fp)
beq	$16,$2,$L913
lw	$25,260($17)
jalr	$25
lw	$28,32($sp)
$L583:
lw	$8,200($sp)
$L913:
addiu	$16,$16,216
bne	$8,$16,$L584
lw	$2,5664($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L914
lw	$6,%got($LC19)($28)
.set	macro
.set	reorder

lw	$2,5600($fp)
lw	$3,292($sp)
addiu	$4,$2,208
$L586:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L586
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
li	$2,216			# 0xd8
lw	$9,296($sp)
sw	$2,0($9)
lw	$6,%got($LC19)($28)
$L914:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC19)
.set	macro
.set	reorder

lw	$10,300($sp)
lw	$31,284($sp)
lw	$fp,280($sp)
lw	$23,276($sp)
lw	$2,20($10)
lw	$22,272($sp)
lw	$21,268($sp)
lw	$20,264($sp)
lw	$19,260($sp)
lw	$18,256($sp)
lw	$17,252($sp)
lw	$16,248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,288
.set	macro
.set	reorder

$L202:
lbu	$2,5($18)
lbu	$3,4($18)
lbu	$7,3($18)
sll	$2,$2,16
sll	$3,$3,8
or	$2,$2,$3
or	$7,$2,$7
li	$2,2752512			# 0x2a0000
addiu	$2,$2,413
.set	noreorder
.set	nomacro
bne	$7,$2,$L844
addiu	$13,$18,10
.set	macro
.set	reorder

lbu	$3,7($18)
lbu	$2,9($18)
addiu	$17,$17,-10
lbu	$22,6($18)
lbu	$21,8($18)
sll	$5,$3,8
sll	$4,$2,8
sw	$13,76($sp)
srl	$3,$3,6
sw	$17,80($sp)
or	$22,$5,$22
or	$21,$4,$21
andi	$22,$22,0x3fff
.set	noreorder
.set	nomacro
bne	$3,$0,$L205
andi	$21,$21,0x3fff
.set	macro
.set	reorder

srl	$2,$2,6
.set	noreorder
.set	nomacro
beq	$2,$0,$L915
lw	$18,%got(vp8_token_default_probs)($28)
.set	macro
.set	reorder

$L205:
lw	$5,%got($LC8)($28)
li	$6,1			# 0x1
lw	$25,%call16(av_log_missing_feature)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log_missing_feature
1:	jalr	$25
addiu	$5,$5,%lo($LC8)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$18,%got(vp8_token_default_probs)($28)
$L915:
addiu	$14,$fp,6898
move	$23,$0
sw	$0,5676($fp)
sw	$0,5672($fp)
addiu	$18,$18,%lo(vp8_token_default_probs)
sw	$14,72($sp)
$L207:
lw	$25,72($sp)
sll	$16,$23,3
lw	$19,%got(vp8_coeff_band)($28)
sll	$2,$23,8
addiu	$17,$25,528
addiu	$19,$19,%lo(vp8_coeff_band)
addu	$16,$16,$2
move	$20,$25
$L208:
lbu	$5,0($19)
move	$4,$20
lw	$25,%call16(memcpy)($28)
li	$6,33			# 0x21
addiu	$20,$20,33
sll	$2,$5,5
addiu	$19,$19,1
addu	$5,$2,$5
addu	$5,$5,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$5,$18,$5
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$17,$20,$L208
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$15,72($sp)
addiu	$23,$23,1
li	$16,4			# 0x4
addiu	$15,$15,561
.set	noreorder
.set	nomacro
bne	$23,$16,$L207
sw	$15,72($sp)
.set	macro
.set	reorder

lw	$5,%got(vp8_pred16x16_prob_inter)($28)
addiu	$4,$fp,6891
lw	$25,%call16(memcpy)($28)
li	$6,4			# 0x4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addiu	$5,$5,%lo(vp8_pred16x16_prob_inter)
.set	macro
.set	reorder

addiu	$4,$fp,6895
lw	$28,32($sp)
li	$6,3			# 0x3
lw	$5,%got(vp8_pred8x8c_prob_inter)($28)
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addiu	$5,$5,%lo(vp8_pred8x8c_prob_inter)
.set	macro
.set	reorder

addiu	$4,$fp,9142
lw	$28,32($sp)
li	$6,38			# 0x26
lw	$5,%got(vp8_mv_default_prob)($28)
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addiu	$5,$5,%lo(vp8_mv_default_prob)
.set	macro
.set	reorder

addiu	$4,$fp,6792
lw	$28,32($sp)
move	$5,$0
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,20			# 0x14
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L203
lw	$28,32($sp)
.set	macro
.set	reorder

$L198:
addiu	$5,$fp,4296
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
li	$6,108			# 0x6c
.set	macro
.set	reorder

li	$4,12			# 0xc
lw	$28,32($sp)
lw	$25,%call16(motion_init_vp8)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_init_vp8
1:	jalr	$25
li	$5,3			# 0x3
.set	macro
.set	reorder

lw	$3,5660($fp)
lw	$11,80($sp)
lw	$12,92($sp)
sll	$2,$3,3
subu	$2,$3,$2
addu	$2,$11,$2
slt	$2,$2,$12
.set	noreorder
.set	nomacro
beq	$2,$0,$L200
lw	$28,32($sp)
.set	macro
.set	reorder

$L839:
lw	$6,%got($LC6)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L899
lw	$31,284($sp)
.set	macro
.set	reorder

$L306:
lw	$3,6864($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L916
addiu	$12,$fp,4736
.set	macro
.set	reorder

lw	$11,288($sp)
lw	$3,700($11)
slt	$3,$3,$2
$L916:
lw	$4,5604($fp)
addiu	$13,$fp,5600
sw	$3,5680($fp)
move	$2,$12
sw	$12,204($sp)
sw	$13,200($sp)
$L311:
.set	noreorder
.set	nomacro
beq	$2,$4,$L917
lw	$14,200($sp)
.set	macro
.set	reorder

lw	$3,5608($fp)
beq	$2,$3,$L917
lw	$3,5612($fp)
bne	$2,$3,$L845
$L917:
addiu	$2,$2,216
.set	noreorder
.set	nomacro
bne	$14,$2,$L311
lw	$15,180($sp)
.set	macro
.set	reorder

lw	$2,0($15)
.set	noreorder
.set	nomacro
beq	$2,$0,$L918
lw	$16,288($sp)
.set	macro
.set	reorder

$L312:
lw	$2,5660($fp)
lw	$18,180($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L847
sw	$2,48($18)
.set	macro
.set	reorder

li	$2,1			# 0x1
lw	$16,180($sp)
.set	noreorder
.set	nomacro
beq	$17,$0,$L848
sw	$2,52($16)
.set	macro
.set	reorder

li	$2,3			# 0x3
lw	$15,180($sp)
sw	$2,80($15)
lw	$19,180($sp)
$L971:
lw	$3,5616($fp)
lw	$2,16($19)
lw	$4,20($19)
sw	$2,5652($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L849
sw	$4,5656($fp)
.set	macro
.set	reorder

$L318:
lw	$2,5644($fp)
move	$5,$0
lw	$25,%call16(memset)($28)
lw	$4,5880($fp)
sll	$6,$2,3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$6,$6,$2
.set	macro
.set	reorder

move	$5,$0
lw	$6,5648($fp)
lw	$2,5644($fp)
lw	$28,32($sp)
sll	$3,$6,4
lw	$4,5852($fp)
sll	$6,$6,7
addiu	$2,$2,1
addu	$3,$3,$6
lw	$25,%call16(memset)($28)
sll	$6,$2,3
addiu	$3,$3,-72
sll	$2,$2,6
addu	$4,$4,$3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$6,$6,$2
.set	macro
.set	reorder

li	$5,127			# 0x7f
lw	$28,32($sp)
lw	$6,5644($fp)
lw	$4,5876($fp)
addiu	$6,$6,1
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sll	$6,$6,5
.set	macro
.set	reorder

addiu	$4,$fp,6780
lw	$28,32($sp)
move	$5,$0
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,12			# 0xc
.set	macro
.set	reorder

lw	$2,5660($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L319
lw	$28,32($sp)
.set	macro
.set	reorder

lw	$6,5644($fp)
li	$5,2			# 0x2
lw	$25,%call16(memset)($28)
lw	$4,5864($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sll	$6,$6,2
.set	macro
.set	reorder

lw	$28,32($sp)
$L319:
li	$7,1310720			# 0x140000
lw	$2,%got(count)($28)
li	$3,-201326592			# 0xfffffffff4000000
lw	$19,%got(motion_dha1)($28)
addiu	$7,$7,16
lw	$16,%got(block1)($28)
sw	$0,%lo(count)($2)
li	$5,786432			# 0xc0000
li	$4,524288			# 0x80000
lw	$17,%got(block2)($28)
sw	$7,9308($3)
li	$7,1048576			# 0x100000
addiu	$5,$5,16
lw	$18,%got(motion_dha0)($28)
addiu	$4,$4,64
lw	$25,%call16(memset)($28)
addiu	$7,$7,256
li	$6,321585152			# 0x132b0000
addiu	$12,$3,9868
sw	$7,9312($3)
addiu	$8,$6,6736
sw	$5,9324($3)
addiu	$6,$6,9300
sw	$4,9328($3)
addiu	$14,$3,9428
sw	$5,9340($3)
addiu	$13,$3,9648
sw	$4,9344($3)
li	$4,321650688			# 0x132c0000
sw	$8,9348($3)
addiu	$11,$3,4096
addiu	$4,$4,18268
lw	$5,%got(mau_reg_ptr0)($28)
addiu	$10,$3,4896
addiu	$9,$3,5696
sw	$4,9352($3)
li	$4,262144			# 0x40000
li	$2,-1288962048			# 0xffffffffb32c0000
addiu	$4,$4,4
addiu	$7,$2,16904
addiu	$8,$2,16416
sw	$4,9356($3)
li	$4,-2147221504			# 0xffffffff80040000
addiu	$15,$2,16872
addiu	$4,$4,4
sw	$4,9360($3)
li	$3,-1289682944			# 0xffffffffb3210000
addiu	$4,$2,17392
sw	$6,0($3)
li	$3,1			# 0x1
lw	$6,%got(mau_reg_ptr1)($28)
sw	$3,18268($2)
addiu	$3,$2,17360
sw	$14,%lo(mau_reg_ptr0)($5)
addiu	$2,$2,17848
sw	$7,%lo(motion_dha1)($19)
move	$5,$0
lw	$14,%got(block0)($28)
lw	$7,%got(motion_dha2)($28)
sw	$13,%lo(mau_reg_ptr1)($6)
li	$6,84			# 0x54
sw	$11,%lo(block0)($14)
sw	$10,%lo(block1)($16)
sw	$9,%lo(block2)($17)
sw	$8,%lo(motion_dha0)($18)
sw	$4,%lo(motion_dha2)($7)
lw	$11,%got(mb0)($28)
lw	$8,%got(motion_dsa0)($28)
lw	$9,%got(motion_dsa1)($28)
lw	$10,%got(motion_dsa2)($28)
lw	$13,%got(mau_reg_ptr2)($28)
lw	$4,%lo(mb0)($11)
sw	$15,%lo(motion_dsa0)($8)
sw	$12,%lo(mau_reg_ptr2)($13)
sw	$3,%lo(motion_dsa1)($9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$2,%lo(motion_dsa2)($10)
.set	macro
.set	reorder

move	$5,$0
lw	$28,32($sp)
li	$6,84			# 0x54
lw	$12,%got(mb1)($28)
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
lw	$4,%lo(mb1)($12)
.set	macro
.set	reorder

move	$5,$0
lw	$28,32($sp)
li	$6,84			# 0x54
lw	$13,%got(mb2)($28)
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
lw	$4,%lo(mb2)($13)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$25,%call16(motion_config_vp8)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,motion_config_vp8
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

li	$7,1374355456			# 0x51eb0000
lw	$28,32($sp)
ori	$7,$7,0x851f
lw	$4,%got(ac_qlookup_raw)($28)
lw	$5,%got(ac2q_lookup)($28)
addiu	$8,$4,512
$L321:
lw	$3,0($4)
sll	$2,$3,2
addu	$3,$2,$3
sll	$2,$3,5
subu	$2,$2,$3
mult	$2,$7
sra	$2,$2,31
mfhi	$3
sra	$3,$3,5
subu	$2,$3,$2
slt	$3,$2,8
.set	noreorder
.set	nomacro
bne	$3,$0,$L320
li	$6,8			# 0x8
.set	macro
.set	reorder

andi	$6,$2,0x00ff
$L320:
addiu	$4,$4,4
sb	$6,0($5)
.set	noreorder
.set	nomacro
bne	$4,$8,$L321
addiu	$5,$5,1
.set	macro
.set	reorder

lw	$3,%got($LC12)($28)
li	$2,-1289224192			# 0xffffffffb3280000
ori	$2,$2,0x8000
addiu	$5,$2,128
lw	$4,%lo($LC12)($3)
addu	$3,$2,$4
$L919:
lw	$3,0($3)
#APP
# 488 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$5,$L919
addu	$3,$2,$4
.set	macro
.set	reorder

lw	$3,%got($LC13)($28)
li	$4,-1289224192			# 0xffffffffb3280000
ori	$4,$4,0x8100
lw	$5,%lo($LC13)($3)
addu	$3,$2,$5
$L920:
lw	$3,0($3)
#APP
# 494 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$4,$L920
addu	$3,$2,$5
.set	macro
.set	reorder

lw	$3,%got($LC14)($28)
li	$4,-1289224192			# 0xffffffffb3280000
ori	$4,$4,0x8180
lw	$5,%lo($LC14)($3)
addu	$3,$2,$5
$L921:
lw	$3,0($3)
#APP
# 500 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$4,$L921
addu	$3,$2,$5
.set	macro
.set	reorder

lw	$3,180($sp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,0($3)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$8,180($sp)
sw	$2,144($sp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,4($8)
.set	macro
.set	reorder

lw	$28,32($sp)
lw	$9,180($sp)
sw	$2,116($sp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,4($9)
.set	macro
.set	reorder

lw	$3,5660($fp)
lw	$28,32($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L326
sw	$2,64($sp)
.set	macro
.set	reorder

lw	$2,5604($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L327
li	$16,-1289420800			# 0xffffffffb3250000
.set	macro
.set	reorder

move	$17,$0
addiu	$2,$16,776
#APP
# 2447 "vp8.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5604($fp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

addiu	$3,$16,780
lw	$28,32($sp)
#APP
# 2447 "vp8.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,2824
#APP
# 2448 "vp8.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5604($fp)
addiu	$16,$16,2828
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,4($2)
.set	macro
.set	reorder

lw	$28,32($sp)
#APP
# 2448 "vp8.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
$L327:
lw	$2,5608($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L328
li	$16,-1289420800			# 0xffffffffb3250000
.set	macro
.set	reorder

move	$17,$0
addiu	$2,$16,784
#APP
# 2452 "vp8.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5608($fp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

addiu	$3,$16,788
lw	$28,32($sp)
#APP
# 2452 "vp8.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,2832
#APP
# 2453 "vp8.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5608($fp)
addiu	$16,$16,2836
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,4($2)
.set	macro
.set	reorder

lw	$28,32($sp)
#APP
# 2453 "vp8.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
$L328:
lw	$2,5612($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L922
li	$2,-1289224192			# 0xffffffffb3280000
.set	macro
.set	reorder

li	$16,-1289420800			# 0xffffffffb3250000
move	$17,$0
addiu	$2,$16,792
#APP
# 2457 "vp8.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5612($fp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,0($2)
.set	macro
.set	reorder

addiu	$3,$16,796
lw	$28,32($sp)
#APP
# 2457 "vp8.c" 1
sw	 $2,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$2,$16,2840
#APP
# 2458 "vp8.c" 1
sw	 $17,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$2,5612($fp)
addiu	$16,$16,2844
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,4($2)
.set	macro
.set	reorder

lw	$28,32($sp)
#APP
# 2458 "vp8.c" 1
sw	 $2,0($16)	#i_sw
# 0 "" 2
#NO_APP
$L326:
li	$2,-1289224192			# 0xffffffffb3280000
$L922:
li	$3,4			# 0x4
addiu	$4,$2,64
#APP
# 2463 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,6			# 0x6
addiu	$4,$2,80
#APP
# 2464 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,786432			# 0xc0000
addiu	$4,$2,116
addiu	$3,$3,20
#APP
# 2466 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$3,5644($fp)
addiu	$4,$2,84
sll	$3,$3,4
#APP
# 2467 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$4,1			# 0x1
addiu	$2,$2,68
#APP
# 2468 "vp8.c" 1
sw	 $4,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,-2147483648			# 0xffffffff80000000
li	$5,-201326592			# 0xfffffffff4000000
addiu	$3,$2,16384
sw	$4,9644($5)
$L330:
#APP
# 2473 "vp8.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L330
#APP
# 2473 "vp8.c" 1
sync
# 0 "" 2
#NO_APP
lw	$2,5648($fp)
.set	noreorder
.set	nomacro
blez	$2,$L641
li	$3,321585152			# 0x132b0000
.set	macro
.set	reorder

lw	$12,%got(vp8_qpar)($28)
addiu	$10,$fp,5888
sw	$0,136($sp)
addiu	$11,$3,9044
addiu	$13,$3,8532
addiu	$14,$3,8276
sw	$10,156($sp)
addiu	$15,$3,7764
sw	$11,108($sp)
addiu	$16,$3,7508
sw	$12,120($sp)
addiu	$3,$3,6996
sw	$13,124($sp)
sw	$14,112($sp)
sw	$15,128($sp)
sw	$16,140($sp)
sw	$3,148($sp)
lw	$17,136($sp)
$L953:
move	$5,$0
lw	$3,5688($fp)
lw	$7,5644($fp)
subu	$2,$2,$17
lw	$6,5852($fp)
addiu	$3,$3,-1
lw	$25,%call16(memset)($28)
addiu	$2,$2,-1
and	$3,$17,$3
sll	$18,$3,2
sll	$3,$3,4
sll	$4,$2,4
sll	$2,$2,7
sw	$18,160($sp)
sw	$3,164($sp)
addu	$3,$18,$3
addu	$2,$4,$2
addu	$6,$6,$2
addu	$2,$fp,$3
mul	$3,$17,$7
addiu	$2,$2,5692
sw	$6,72($sp)
addiu	$4,$6,-72
li	$6,72			# 0x48
sw	$2,100($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$3,172($sp)
.set	macro
.set	reorder

move	$5,$0
lw	$28,32($sp)
li	$6,9			# 0x9
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
lw	$4,156($sp)
.set	macro
.set	reorder

li	$2,33685504			# 0x2020000
lw	$3,5644($fp)
addiu	$2,$2,514
lw	$28,32($sp)
.set	noreorder
.set	nomacro
blez	$3,$L332
sw	$2,5868($fp)
.set	macro
.set	reorder

sll	$5,$17,6
lw	$4,108($sp)
li	$6,-64			# 0xffffffffffffffc0
lw	$3,124($sp)
sll	$2,$17,8
lw	$8,140($sp)
subu	$6,$6,$5
lw	$9,148($sp)
li	$5,-830144512			# 0xffffffffce850000
lw	$10,112($sp)
andi	$2,$2,0xffff
lw	$11,128($sp)
or	$5,$2,$5
lw	$23,%got(ff_vp56_norm_shift)($28)
sw	$8,108($sp)
sw	$5,212($sp)
li	$5,-1073741824			# 0xffffffffc0000000
sw	$9,124($sp)
or	$5,$2,$5
sw	$10,140($sp)
sw	$11,148($sp)
sw	$5,192($sp)
li	$5,-838598656			# 0xffffffffce040000
sw	$6,184($sp)
or	$5,$2,$5
sw	$4,112($sp)
sw	$3,128($sp)
sw	$5,208($sp)
li	$5,-838729728			# 0xffffffffce020000
sw	$0,92($sp)
or	$5,$2,$5
sw	$5,196($sp)
li	$5,-838795264			# 0xffffffffce010000
or	$5,$2,$5
sw	$5,188($sp)
$L527:
lw	$16,92($sp)
lw	$17,172($sp)
lw	$3,5872($fp)
lw	$2,6800($fp)
addu	$9,$16,$17
.set	noreorder
.set	nomacro
bne	$2,$0,$L333
addu	$9,$3,$9
.set	macro
.set	reorder

lbu	$2,0($9)
lw	$3,6760($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L642
sw	$2,6756($fp)
.set	macro
.set	reorder

$L860:
lw	$5,5620($fp)
lw	$6,5624($fp)
lw	$4,5636($fp)
addu	$2,$23,$5
lbu	$7,6887($fp)
lbu	$3,0($2)
sll	$5,$5,$3
addu	$6,$3,$6
sll	$4,$4,$3
.set	noreorder
.set	nomacro
bltz	$6,$L339
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$8,5628($fp)
lw	$2,5632($fp)
sltu	$2,$8,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L923
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$8,2
sw	$2,5628($fp)
lbu	$3,1($8)
lbu	$2,0($8)
sll	$3,$3,8
or	$3,$3,$2
sll	$2,$3,8
srl	$3,$3,8
or	$3,$2,$3
andi	$3,$3,0xffff
sll	$3,$3,$6
addiu	$6,$6,-16
or	$4,$4,$3
$L339:
addiu	$2,$5,-1
$L923:
sw	$6,5624($fp)
mul	$2,$2,$7
sra	$2,$2,8
addiu	$2,$2,1
sll	$6,$2,16
sltu	$3,$4,$6
xori	$3,$3,0x1
beq	$3,$0,$L827
subu	$2,$5,$2
subu	$4,$4,$6
$L827:
sw	$2,5620($fp)
move	$2,$3
sw	$4,5636($fp)
lw	$3,72($sp)
sb	$2,0($3)
lw	$5,5660($fp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L341
lw	$9,%got(vp8_pred16x16_tree_intra)($28)
.set	macro
.set	reorder

$L935:
move	$5,$0
lw	$8,%got(vp8_pred16x16_prob_intra)($28)
lw	$7,5624($fp)
lw	$10,5636($fp)
addiu	$9,$9,%lo(vp8_pred16x16_tree_intra)
.set	noreorder
.set	nomacro
b	$L344
addiu	$8,$8,%lo(vp8_pred16x16_prob_intra)
.set	macro
.set	reorder

$L851:
subu	$10,$6,$12
sw	$3,5620($fp)
addu	$4,$11,$4
sw	$10,5636($fp)
lb	$5,0($4)
blez	$5,$L850
$L344:
lw	$3,5620($fp)
addu	$4,$8,$5
sll	$5,$5,1
addu	$2,$23,$3
lbu	$4,0($4)
addu	$11,$9,$5
lbu	$6,0($2)
sll	$3,$3,$6
addu	$7,$6,$7
addiu	$2,$3,-1
sw	$3,5620($fp)
sll	$6,$10,$6
.set	noreorder
.set	nomacro
bltz	$7,$L342
mul	$2,$2,$4
.set	macro
.set	reorder

lw	$4,5628($fp)
lw	$5,5632($fp)
sltu	$5,$4,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L342
addiu	$10,$4,2
.set	macro
.set	reorder

sw	$10,5628($fp)
lbu	$10,1($4)
lbu	$4,0($4)
sll	$10,$10,8
or	$10,$10,$4
sll	$4,$10,8
srl	$10,$10,8
or	$10,$4,$10
andi	$4,$10,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$6,$6,$4
$L342:
sra	$2,$2,8
sw	$7,5624($fp)
move	$10,$6
addiu	$2,$2,1
sll	$12,$2,16
sltu	$5,$6,$12
xori	$4,$5,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L851
subu	$3,$3,$2
.set	macro
.set	reorder

addu	$4,$11,$4
sw	$2,5620($fp)
sw	$10,5636($fp)
lb	$5,0($4)
bgtz	$5,$L344
$L850:
subu	$5,$0,$5
lw	$3,72($sp)
li	$2,4			# 0x4
lw	$8,92($sp)
.set	noreorder
.set	nomacro
beq	$5,$2,$L852
sb	$5,1($3)
.set	macro
.set	reorder

lw	$2,5864($fp)
sll	$3,$8,2
addu	$3,$2,$3
lw	$2,%got(vp8_pred4x4_mode)($28)
addiu	$2,$2,%lo(vp8_pred4x4_mode)
addu	$5,$5,$2
lbu	$4,0($5)
sll	$2,$4,8
addu	$2,$2,$4
sll	$4,$2,16
addu	$2,$2,$4
sw	$2,0($3)
sw	$2,5868($fp)
lw	$9,%got(vp8_pred8x8c_tree)($28)
$L903:
move	$5,$0
lw	$7,5624($fp)
lw	$10,5636($fp)
addiu	$8,$9,%lo(vp8_pred8x8c_tree)
lw	$9,%got(vp8_pred8x8c_prob_intra)($28)
.set	noreorder
.set	nomacro
b	$L354
addiu	$9,$9,%lo(vp8_pred8x8c_prob_intra)
.set	macro
.set	reorder

$L854:
subu	$10,$6,$12
sw	$3,5620($fp)
addu	$4,$11,$4
sw	$10,5636($fp)
lb	$5,0($4)
blez	$5,$L853
$L354:
lw	$3,5620($fp)
addu	$4,$9,$5
sll	$5,$5,1
addu	$2,$23,$3
lbu	$4,0($4)
addu	$11,$8,$5
lbu	$6,0($2)
sll	$3,$3,$6
addu	$7,$6,$7
addiu	$2,$3,-1
sw	$3,5620($fp)
sll	$6,$10,$6
.set	noreorder
.set	nomacro
bltz	$7,$L352
mul	$2,$2,$4
.set	macro
.set	reorder

lw	$4,5628($fp)
lw	$5,5632($fp)
sltu	$5,$4,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L352
addiu	$10,$4,2
.set	macro
.set	reorder

sw	$10,5628($fp)
lbu	$10,1($4)
lbu	$4,0($4)
sll	$10,$10,8
or	$10,$10,$4
sll	$4,$10,8
srl	$10,$10,8
or	$10,$4,$10
andi	$4,$10,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$6,$6,$4
$L352:
sra	$2,$2,8
sw	$7,5624($fp)
move	$10,$6
addiu	$2,$2,1
sll	$12,$2,16
sltu	$5,$6,$12
xori	$4,$5,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L854
subu	$3,$3,$2
.set	macro
.set	reorder

addu	$4,$11,$4
sw	$2,5620($fp)
sw	$10,5636($fp)
lb	$5,0($4)
bgtz	$5,$L354
$L853:
subu	$5,$0,$5
lw	$3,72($sp)
sw	$5,6752($fp)
sb	$0,2($3)
$L355:
lw	$4,%got(block0)($28)
$L957:
lw	$2,%lo(block0)($4)
addiu	$3,$2,800
$L449:
#APP
# 2276 "vp8.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 2277 "vp8.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 2278 "vp8.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 2280 "vp8.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 2281 "vp8.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 2282 "vp8.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 2283 "vp8.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 2284 "vp8.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
addiu	$2,$2,32
.set	noreorder
.set	nomacro
bne	$2,$3,$L449
lw	$5,%got(mau_reg_ptr1)($28)
.set	macro
.set	reorder

li	$2,768			# 0x300
lw	$8,72($sp)
lw	$16,%lo(mau_reg_ptr1)($5)
sw	$0,0($16)
sw	$2,16($16)
lbu	$2,0($8)
.set	noreorder
.set	nomacro
bne	$2,$0,$L451
lw	$9,92($sp)
.set	macro
.set	reorder

lw	$4,5880($fp)
lbu	$3,1($8)
sll	$2,$9,3
addu	$2,$2,$9
addu	$4,$4,$2
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$3,$2,$L454
sw	$4,104($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
.set	noreorder
.set	nomacro
beq	$3,$2,$L454
lw	$11,160($sp)
.set	macro
.set	reorder

addiu	$6,$fp,7459
lw	$12,164($sp)
lw	$13,104($sp)
lbu	$4,5896($fp)
addu	$5,$11,$12
lw	$14,%got(block0)($28)
addu	$5,$fp,$5
lbu	$2,8($13)
lw	$14,%lo(block0)($14)
lw	$11,5692($5)
addu	$4,$2,$4
lw	$9,5696($5)
sll	$3,$4,4
lw	$7,5708($5)
sll	$2,$4,2
sw	$14,152($sp)
addu	$8,$23,$11
subu	$2,$3,$2
lbu	$10,0($8)
subu	$2,$2,$4
addu	$8,$6,$2
sll	$4,$11,$10
addu	$9,$10,$9
lbu	$3,0($8)
sll	$7,$7,$10
.set	noreorder
.set	nomacro
bltz	$9,$L455
sw	$4,5692($5)
.set	macro
.set	reorder

lw	$10,5700($5)
lw	$2,5704($5)
sltu	$2,$10,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L924
addiu	$2,$4,-1
.set	macro
.set	reorder

addiu	$2,$10,2
lw	$15,100($sp)
sw	$2,8($15)
lbu	$2,1($10)
lbu	$5,0($10)
sll	$2,$2,8
or	$2,$2,$5
sll	$5,$2,8
srl	$2,$2,8
or	$2,$5,$2
andi	$2,$2,0xffff
sll	$2,$2,$9
addiu	$9,$9,-16
or	$7,$7,$2
$L455:
addiu	$2,$4,-1
$L924:
lw	$17,160($sp)
lw	$18,164($sp)
mul	$2,$3,$2
addu	$5,$17,$18
addu	$3,$fp,$5
sw	$9,5696($3)
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$9,$7,$5
.set	noreorder
.set	nomacro
bne	$9,$0,$L456
lw	$9,104($sp)
.set	macro
.set	reorder

subu	$5,$7,$5
lw	$25,%got(decode_block_coeffs_internal.isra.2)($28)
subu	$2,$4,$2
lw	$4,100($sp)
move	$7,$0
sw	$5,5708($3)
addiu	$25,$25,%lo(decode_block_coeffs_internal.isra.2)
sw	$2,5692($3)
sw	$8,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_block_coeffs_internal.isra.2
1:	jalr	$25
lw	$5,152($sp)
.set	macro
.set	reorder

lw	$3,104($sp)
move	$18,$2
sltu	$2,$0,$2
lw	$28,32($sp)
sb	$2,8($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L855
sb	$2,5896($fp)
.set	macro
.set	reorder

lw	$2,0($16)
li	$3,268435456			# 0x10000000
li	$11,1			# 0x1
li	$12,1			# 0x1
or	$2,$2,$3
li	$3,800			# 0x320
sw	$11,84($sp)
li	$4,33			# 0x21
sw	$2,0($16)
sw	$12,80($sp)
sw	$3,16($16)
move	$3,$0
$L453:
sll	$2,$3,4
lw	$13,160($sp)
lw	$14,164($sp)
addu	$2,$2,$3
lw	$15,152($sp)
lw	$17,152($sp)
sll	$22,$2,5
lw	$19,104($sp)
addu	$16,$13,$14
lw	$21,156($sp)
addu	$22,$2,$22
sw	$fp,96($sp)
addiu	$15,$15,32
sw	$fp,176($sp)
addu	$22,$fp,$22
addiu	$17,$17,544
addiu	$22,$22,6898
sw	$15,88($sp)
addiu	$24,$19,4
addu	$4,$22,$4
sw	$17,132($sp)
addu	$16,$fp,$16
lw	$fp,100($sp)
move	$2,$23
sw	$4,76($sp)
move	$23,$24
move	$24,$2
$L458:
lw	$17,104($sp)
lw	$20,88($sp)
lw	$19,96($sp)
$L462:
lbu	$2,0($17)
move	$4,$fp
lbu	$8,0($21)
move	$5,$20
lw	$9,5692($16)
move	$6,$22
lw	$25,76($sp)
move	$13,$0
addu	$8,$8,$2
lw	$11,5696($16)
addu	$7,$24,$9
lw	$14,5708($16)
sll	$3,$8,2
sll	$2,$8,4
lbu	$10,0($7)
move	$12,$0
subu	$2,$2,$3
lw	$7,80($sp)
subu	$8,$2,$8
sll	$9,$9,$10
addu	$8,$25,$8
addu	$11,$10,$11
addiu	$25,$9,-1
lbu	$3,0($8)
move	$2,$0
sw	$9,5692($16)
.set	noreorder
.set	nomacro
bltz	$11,$L459
sll	$10,$14,$10
.set	macro
.set	reorder

lw	$14,5700($16)
lw	$15,5704($16)
sltu	$15,$14,$15
.set	noreorder
.set	nomacro
beq	$15,$0,$L459
addiu	$31,$14,2
.set	macro
.set	reorder

sw	$31,8($fp)
lbu	$15,1($14)
lbu	$31,0($14)
sll	$14,$15,8
or	$14,$14,$31
sll	$15,$14,8
srl	$14,$14,8
or	$14,$15,$14
andi	$14,$14,0xffff
sll	$14,$14,$11
addiu	$11,$11,-16
or	$10,$10,$14
$L459:
mul	$3,$3,$25
sw	$11,5696($16)
sra	$3,$3,8
addiu	$3,$3,1
sll	$11,$3,16
subu	$14,$10,$11
sltu	$11,$10,$11
.set	noreorder
.set	nomacro
bne	$11,$0,$L460
subu	$9,$9,$3
.set	macro
.set	reorder

lw	$2,%got(decode_block_coeffs_internal.isra.2)($28)
sw	$9,5692($16)
sw	$14,5708($16)
addiu	$25,$2,%lo(decode_block_coeffs_internal.isra.2)
sw	$24,216($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_block_coeffs_internal.isra.2
1:	jalr	$25
sw	$8,16($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
andi	$12,$2,0x00ff
lw	$24,216($sp)
sltu	$13,$0,$2
$L461:
lw	$3,84($sp)
addiu	$17,$17,1
addu	$18,$18,$2
addiu	$20,$20,32
addu	$12,$12,$3
addiu	$19,$19,1
sb	$12,5903($19)
sb	$13,0($21)
.set	noreorder
.set	nomacro
bne	$23,$17,$L462
sb	$13,-1($17)
.set	macro
.set	reorder

lw	$8,88($sp)
addiu	$21,$21,1
lw	$9,96($sp)
lw	$10,132($sp)
addiu	$8,$8,128
addiu	$9,$9,4
sw	$8,88($sp)
.set	noreorder
.set	nomacro
bne	$8,$10,$L458
sw	$9,96($sp)
.set	macro
.set	reorder

lw	$11,160($sp)
li	$13,4			# 0x4
lw	$12,164($sp)
lw	$fp,176($sp)
lw	$31,152($sp)
addu	$16,$11,$12
sw	$13,88($sp)
addiu	$17,$fp,8020
sw	$24,84($sp)
addu	$16,$fp,$16
sw	$fp,80($sp)
$L463:
lw	$8,88($sp)
move	$20,$0
lw	$9,104($sp)
lw	$3,156($sp)
sw	$0,76($sp)
sll	$fp,$8,2
lw	$13,76($sp)
addu	$9,$9,$8
move	$2,$fp
addu	$21,$3,$8
sll	$23,$13,1
sw	$9,96($sp)
lw	$19,96($sp)
addu	$24,$23,$fp
move	$fp,$23
move	$23,$24
move	$24,$2
$L469:
lbu	$2,0($19)
move	$7,$0
lbu	$8,0($21)
move	$6,$17
lw	$9,5692($16)
move	$13,$0
lw	$15,84($sp)
move	$22,$0
addu	$8,$8,$2
lw	$12,5696($16)
lw	$11,5708($16)
addu	$10,$20,$fp
sll	$3,$8,2
lw	$4,100($sp)
addu	$5,$15,$9
sll	$2,$8,4
subu	$2,$2,$3
lbu	$5,0($5)
subu	$8,$2,$8
sll	$9,$9,$5
addu	$8,$17,$8
addu	$12,$5,$12
addiu	$3,$9,-1
lbu	$25,0($8)
move	$2,$0
sll	$11,$11,$5
.set	noreorder
.set	nomacro
bltz	$12,$L464
sw	$9,5692($16)
.set	macro
.set	reorder

lw	$15,5700($16)
lw	$14,5704($16)
sltu	$14,$15,$14
.set	noreorder
.set	nomacro
beq	$14,$0,$L464
addiu	$5,$15,2
.set	macro
.set	reorder

sw	$5,8($4)
lbu	$14,1($15)
lbu	$15,0($15)
sll	$5,$14,8
or	$5,$5,$15
sll	$14,$5,8
srl	$5,$5,8
or	$5,$14,$5
andi	$5,$5,0xffff
sll	$5,$5,$12
addiu	$12,$12,-16
or	$11,$11,$5
$L464:
mul	$3,$25,$3
sw	$12,5696($16)
addu	$5,$24,$10
addiu	$5,$5,1
sll	$5,$5,5
sra	$3,$3,8
addu	$5,$31,$5
addiu	$3,$3,1
sll	$10,$3,16
subu	$12,$11,$10
sltu	$10,$11,$10
.set	noreorder
.set	nomacro
bne	$10,$0,$L465
subu	$9,$9,$3
.set	macro
.set	reorder

lw	$2,%got(decode_block_coeffs_internal.isra.2)($28)
sw	$9,5692($16)
sw	$12,5708($16)
addiu	$25,$2,%lo(decode_block_coeffs_internal.isra.2)
sw	$24,216($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_block_coeffs_internal.isra.2
1:	jalr	$25
sw	$8,16($sp)
.set	macro
.set	reorder

lw	$28,32($sp)
andi	$22,$2,0x00ff
lw	$24,216($sp)
sltu	$13,$0,$2
$L466:
lw	$8,80($sp)
addu	$3,$20,$23
addu	$18,$18,$2
addiu	$19,$19,2
addu	$2,$8,$3
li	$9,1			# 0x1
sb	$22,5904($2)
sb	$13,0($21)
.set	noreorder
.set	nomacro
bne	$20,$9,$L467
sb	$13,-2($19)
.set	macro
.set	reorder

lw	$10,76($sp)
move	$fp,$24
.set	noreorder
.set	nomacro
beq	$10,$9,$L468
addiu	$21,$21,2
.set	macro
.set	reorder

li	$12,1			# 0x1
lw	$11,%got(block0)($28)
move	$2,$fp
lw	$19,96($sp)
move	$20,$0
sw	$12,76($sp)
lw	$13,76($sp)
lw	$31,%lo(block0)($11)
sll	$23,$13,1
addu	$24,$23,$fp
move	$fp,$23
move	$23,$24
.set	noreorder
.set	nomacro
b	$L469
move	$24,$2
.set	macro
.set	reorder

$L841:
lw	$2,5624($fp)
lw	$3,5636($fp)
lw	$5,5620($fp)
lw	$17,%got(ff_vp56_norm_shift)($28)
$L217:
addu	$4,$17,$5
lbu	$6,0($4)
sll	$5,$5,$6
addu	$2,$6,$2
sll	$6,$3,$6
.set	noreorder
.set	nomacro
bltz	$2,$L222
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$3,5628($fp)
lw	$4,5632($fp)
sltu	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L222
addiu	$4,$3,2
.set	macro
.set	reorder

sw	$4,5628($fp)
lbu	$4,1($3)
lbu	$3,0($3)
sll	$4,$4,8
or	$4,$4,$3
sll	$3,$4,8
srl	$4,$4,8
or	$3,$3,$4
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$6,$6,$3
$L222:
addiu	$3,$5,-1
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$4,$3,16
sltu	$7,$6,$4
xori	$7,$7,0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L223
sw	$2,5624($fp)
.set	macro
.set	reorder

subu	$5,$5,$3
sw	$7,6792($fp)
subu	$3,$6,$4
addu	$4,$17,$5
sw	$3,5636($fp)
lbu	$4,0($4)
sll	$5,$5,$4
addu	$2,$2,$4
sll	$4,$3,$4
.set	noreorder
.set	nomacro
bltz	$2,$L224
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$3,5632($fp)
sltu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L925
addiu	$3,$5,-1
.set	macro
.set	reorder

addiu	$3,$6,2
sw	$3,5628($fp)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$4,$4,$3
$L224:
addiu	$3,$5,-1
$L925:
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$6,$3,16
sltu	$7,$4,$6
xori	$7,$7,0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L225
sw	$2,5624($fp)
.set	macro
.set	reorder

subu	$3,$5,$3
subu	$4,$4,$6
$L225:
addu	$5,$17,$3
sw	$4,5636($fp)
sw	$7,6800($fp)
lbu	$5,0($5)
sll	$6,$3,$5
addu	$2,$2,$5
sll	$4,$4,$5
.set	noreorder
.set	nomacro
bltz	$2,$L226
sw	$6,5620($fp)
.set	macro
.set	reorder

lw	$3,5628($fp)
lw	$5,5632($fp)
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L226
addiu	$5,$3,2
.set	macro
.set	reorder

sw	$5,5628($fp)
lbu	$5,1($3)
lbu	$3,0($3)
sll	$5,$5,8
or	$5,$5,$3
sll	$3,$5,8
srl	$5,$5,8
or	$3,$3,$5
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$4,$4,$3
$L226:
addiu	$3,$6,-1
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$5,$3,16
sltu	$8,$4,$5
.set	noreorder
.set	nomacro
bne	$8,$0,$L227
sw	$2,5624($fp)
.set	macro
.set	reorder

subu	$6,$6,$3
subu	$4,$4,$5
addu	$3,$17,$6
sw	$4,5636($fp)
lbu	$3,0($3)
sll	$6,$6,$3
addu	$7,$2,$3
sll	$3,$4,$3
.set	noreorder
.set	nomacro
bltz	$7,$L228
sw	$6,5620($fp)
.set	macro
.set	reorder

lw	$5,5628($fp)
lw	$2,5632($fp)
sltu	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L926
addiu	$2,$6,-1
.set	macro
.set	reorder

addiu	$2,$5,2
sw	$2,5628($fp)
lbu	$4,1($5)
lbu	$2,0($5)
sll	$4,$4,8
or	$4,$4,$2
sll	$5,$4,8
srl	$4,$4,8
or	$4,$5,$4
andi	$4,$4,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$3,$3,$4
$L228:
addiu	$2,$6,-1
$L926:
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$4,$3,$5
xori	$4,$4,0x1
.set	noreorder
.set	nomacro
beq	$4,$0,$L229
sw	$7,5624($fp)
.set	macro
.set	reorder

subu	$6,$6,$2
subu	$3,$3,$5
sw	$6,5620($fp)
$L592:
lw	$19,%got(vp8_rac_get_sint)($28)
addiu	$21,$fp,6804
addiu	$18,$fp,6808
sw	$3,5636($fp)
sw	$4,6796($fp)
addiu	$19,$19,%lo(vp8_rac_get_sint)
$L230:
lw	$4,168($sp)
li	$5,7			# 0x7
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
addiu	$21,$21,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$21,$18,$L230
sb	$2,-1($21)
.set	macro
.set	reorder

addiu	$21,$fp,6812
$L231:
lw	$4,168($sp)
li	$5,6			# 0x6
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
addiu	$18,$18,1
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
bne	$18,$21,$L231
sb	$2,-1($18)
.set	macro
.set	reorder

lw	$7,6800($fp)
lw	$3,5620($fp)
lw	$2,5624($fp)
lw	$8,5636($fp)
.set	noreorder
.set	nomacro
bne	$7,$0,$L856
lw	$18,%got(vp8_rac_get_uint)($28)
.set	macro
.set	reorder

$L824:
addiu	$18,$18,%lo(vp8_rac_get_uint)
sw	$18,72($sp)
$L232:
addu	$4,$17,$3
lbu	$4,0($4)
sll	$5,$3,$4
addu	$6,$4,$2
sll	$8,$8,$4
.set	noreorder
.set	nomacro
bltz	$6,$L236
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$3,5628($fp)
lw	$2,5632($fp)
sltu	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L927
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$3,2
sw	$2,5628($fp)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$4,$2,8
srl	$2,$2,8
or	$2,$4,$2
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
or	$8,$8,$2
$L236:
addiu	$2,$5,-1
$L927:
sw	$6,5624($fp)
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$3,$2,16
sltu	$6,$8,$3
xori	$6,$6,0x1
beq	$6,$0,$L237
subu	$2,$5,$2
subu	$3,$8,$3
sw	$2,5620($fp)
$L594:
lw	$4,168($sp)
li	$5,6			# 0x6
lw	$25,72($sp)
sw	$3,5636($fp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$6,6860($fp)
.set	macro
.set	reorder

li	$5,3			# 0x3
lw	$4,168($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$2,6864($fp)
.set	macro
.set	reorder

lw	$3,5620($fp)
lw	$28,32($sp)
lw	$4,5624($fp)
addu	$6,$17,$3
lw	$5,5636($fp)
sw	$2,6868($fp)
lbu	$2,0($6)
sll	$3,$3,$2
addu	$4,$2,$4
sll	$5,$5,$2
.set	noreorder
.set	nomacro
bltz	$4,$L238
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$2,5632($fp)
sltu	$2,$6,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L928
addiu	$2,$3,-1
.set	macro
.set	reorder

addiu	$2,$6,2
sw	$2,5628($fp)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$2,$2,$6
sll	$6,$2,8
srl	$2,$2,8
or	$2,$6,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L238:
addiu	$2,$3,-1
$L928:
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$6,$2,16
sltu	$7,$5,$6
xori	$7,$7,0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L239
sw	$4,5624($fp)
.set	macro
.set	reorder

subu	$2,$3,$2
sw	$7,6872($fp)
subu	$5,$5,$6
addu	$3,$17,$2
sw	$5,5636($fp)
lbu	$3,0($3)
sll	$6,$2,$3
addu	$4,$4,$3
sll	$5,$5,$3
.set	noreorder
.set	nomacro
bltz	$4,$L240
sw	$6,5620($fp)
.set	macro
.set	reorder

lw	$3,5628($fp)
lw	$2,5632($fp)
sltu	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L929
addiu	$2,$6,-1
.set	macro
.set	reorder

addiu	$2,$3,2
sw	$2,5628($fp)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L240:
addiu	$2,$6,-1
$L929:
sw	$4,5624($fp)
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$3,$2,16
sltu	$4,$5,$3
.set	noreorder
.set	nomacro
bne	$4,$0,$L241
lw	$19,%got(vp8_rac_get_sint)($28)
.set	macro
.set	reorder

subu	$2,$6,$2
subu	$5,$5,$3
addiu	$21,$fp,6880
addiu	$23,$fp,6884
sw	$2,5620($fp)
addiu	$19,$19,%lo(vp8_rac_get_sint)
sw	$5,5636($fp)
move	$22,$21
$L242:
lw	$4,168($sp)
li	$5,6			# 0x6
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
addiu	$22,$22,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$22,$23,$L242
sb	$2,-1($22)
.set	macro
.set	reorder

addiu	$22,$fp,6876
$L243:
lw	$4,168($sp)
li	$5,6			# 0x6
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
addiu	$22,$22,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$21,$22,$L243
sb	$2,-1($22)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L900
lw	$4,168($sp)
.set	macro
.set	reorder

$L635:
sll	$4,$22,2
$L907:
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
sll	$22,$22,4
move	$6,$20
addu	$22,$4,$22
move	$5,$21
addu	$4,$fp,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
addiu	$4,$4,5692
.set	macro
.set	reorder

li	$5,7			# 0x7
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
lw	$4,168($sp)
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$28,32($sp)
move	$19,$2
lw	$16,%got(vp8_rac_get_sint)($28)
addiu	$16,$16,%lo(vp8_rac_get_sint)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
lw	$4,168($sp)
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$4,168($sp)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
move	$20,$2
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$4,168($sp)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
sw	$2,92($sp)
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$4,168($sp)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
sw	$2,76($sp)
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$4,168($sp)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,vp8_rac_get_sint
1:	jalr	$25
sw	$2,80($sp)
.set	macro
.set	reorder

addiu	$6,$fp,6818
lw	$28,32($sp)
move	$23,$2
lw	$31,6792($fp)
li	$2,1374355456			# 0x51eb0000
addiu	$14,$fp,6804
addiu	$25,$fp,6866
lw	$7,%got(vp8_qpar)($28)
li	$12,127			# 0x7f
lw	$13,%got(vp8_dc_qlookup)($28)
ori	$24,$2,0x851f
lw	$21,%got(vp8_ac_qlookup)($28)
move	$22,$7
addiu	$13,$13,%lo(vp8_dc_qlookup)
sw	$7,120($sp)
addiu	$21,$21,%lo(vp8_ac_qlookup)
$L251:
.set	noreorder
.set	nomacro
beq	$31,$0,$L248
move	$2,$19
.set	macro
.set	reorder

lb	$2,0($14)
lw	$4,6796($fp)
addu	$3,$2,$19
movz	$2,$3,$4
$L248:
lw	$8,76($sp)
addu	$10,$20,$2
lw	$9,92($sp)
addu	$5,$23,$2
lw	$15,80($sp)
addiu	$22,$22,4
addu	$3,$8,$2
sw	$2,-4($22)
slt	$8,$2,128
slt	$4,$3,128
movz	$3,$12,$4
addu	$11,$9,$2
addu	$9,$15,$2
movz	$2,$12,$8
slt	$16,$5,128
addiu	$14,$14,1
slt	$4,$3,0
movz	$5,$12,$16
movn	$3,$0,$4
slt	$4,$10,128
move	$8,$2
movz	$10,$12,$4
slt	$2,$11,128
slt	$15,$8,0
sll	$3,$3,1
movz	$11,$12,$2
slt	$2,$9,128
addu	$3,$21,$3
movz	$9,$12,$2
slt	$16,$5,0
movn	$8,$0,$15
slt	$18,$11,0
slt	$2,$10,0
lhu	$3,0($3)
slt	$15,$9,0
movn	$11,$0,$18
sll	$8,$8,1
sll	$4,$3,2
movn	$9,$0,$15
addu	$8,$21,$8
addu	$3,$4,$3
movn	$5,$0,$16
sll	$4,$3,5
movn	$10,$0,$2
addu	$2,$13,$11
subu	$4,$4,$3
lhu	$8,0($8)
sll	$5,$5,1
mult	$4,$24
lbu	$11,0($2)
sra	$4,$4,31
addu	$3,$13,$10
sh	$8,-4($6)
addu	$9,$13,$9
mfhi	$15
addu	$5,$21,$5
lbu	$10,0($3)
lbu	$9,0($9)
lhu	$5,0($5)
sh	$10,-6($6)
sh	$9,2($6)
sra	$2,$15,5
sh	$5,4($6)
subu	$2,$2,$4
sll	$4,$11,1
sll	$3,$2,16
sh	$2,0($6)
sra	$3,$3,16
slt	$3,$3,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L249
sh	$4,-2($6)
.set	macro
.set	reorder

li	$2,8			# 0x8
$L249:
lhu	$3,2($6)
sh	$2,0($6)
sll	$2,$3,16
sra	$2,$2,16
slt	$2,$2,133
bne	$2,$0,$L250
li	$3,132			# 0x84
$L250:
addiu	$6,$6,12
.set	noreorder
.set	nomacro
bne	$6,$25,$L251
sh	$3,-10($6)
.set	macro
.set	reorder

lw	$3,120($sp)
li	$9,127			# 0x7f
addiu	$10,$3,16
$L252:
lw	$2,0($7)
addiu	$7,$7,4
lw	$11,92($sp)
lw	$12,76($sp)
lw	$13,80($sp)
addu	$8,$20,$2
addu	$6,$11,$2
addu	$5,$12,$2
addu	$4,$13,$2
addu	$3,$23,$2
slt	$11,$8,128
slt	$12,$6,128
movz	$8,$9,$11
slt	$13,$4,128
movz	$6,$9,$12
slt	$11,$5,128
slt	$12,$3,128
movz	$5,$9,$11
slt	$11,$8,0
movz	$4,$9,$13
slt	$14,$6,0
movz	$3,$9,$12
slt	$13,$5,0
movn	$8,$0,$11
slt	$12,$4,0
slt	$11,$3,0
movn	$6,$0,$14
movn	$5,$0,$13
subu	$8,$8,$2
movn	$4,$0,$12
subu	$6,$6,$2
movn	$3,$0,$11
subu	$5,$5,$2
subu	$4,$4,$2
sw	$8,12($7)
sw	$6,28($7)
subu	$2,$3,$2
sw	$5,44($7)
sw	$4,60($7)
.set	noreorder
.set	nomacro
bne	$7,$10,$L252
sw	$2,76($7)
.set	macro
.set	reorder

lw	$2,5660($fp)
beq	$2,$0,$L253
lw	$4,5624($fp)
lw	$2,5636($fp)
lw	$3,5620($fp)
$L254:
addu	$5,$17,$3
lbu	$5,0($5)
sll	$3,$3,$5
addu	$10,$5,$4
sll	$2,$2,$5
.set	noreorder
.set	nomacro
bltz	$10,$L271
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$4,5628($fp)
lw	$5,5632($fp)
sltu	$5,$4,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L930
addiu	$6,$3,-1
.set	macro
.set	reorder

addiu	$5,$4,2
sw	$5,5628($fp)
lbu	$5,1($4)
lbu	$4,0($4)
sll	$5,$5,8
or	$5,$5,$4
sll	$4,$5,8
srl	$5,$5,8
or	$4,$4,$5
andi	$4,$4,0xffff
sll	$4,$4,$10
addiu	$10,$10,-16
or	$2,$2,$4
$L271:
addiu	$6,$3,-1
$L930:
sll	$6,$6,7
sra	$6,$6,8
addiu	$6,$6,1
sll	$5,$6,16
sltu	$4,$2,$5
xori	$4,$4,0x1
.set	noreorder
.set	nomacro
beq	$4,$0,$L272
sw	$10,5624($fp)
.set	macro
.set	reorder

subu	$6,$3,$6
sw	$4,5684($fp)
subu	$2,$2,$5
sw	$6,5620($fp)
sw	$2,5636($fp)
$L599:
lw	$3,5660($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L273
addu	$3,$17,$6
.set	macro
.set	reorder

lw	$10,5624($fp)
li	$3,1			# 0x1
lw	$11,5636($fp)
lw	$2,5620($fp)
$L274:
lw	$19,168($sp)
li	$23,11			# 0xb
sw	$3,5668($fp)
lw	$3,%got(vp8_token_update_probs)($28)
sw	$0,84($sp)
addiu	$3,$3,%lo(vp8_token_update_probs)
sw	$3,88($sp)
$L277:
lw	$13,84($sp)
move	$20,$17
lw	$22,%got(vp8_coeff_band_indexes+1)($28)
move	$9,$10
lw	$14,88($sp)
sll	$3,$13,4
addiu	$22,$22,%lo(vp8_coeff_band_indexes+1)
addu	$3,$3,$13
sw	$14,80($sp)
move	$17,$22
sll	$21,$3,5
addu	$21,$3,$21
sw	$21,76($sp)
$L288:
lw	$21,80($sp)
move	$3,$20
move	$18,$0
move	$22,$17
move	$20,$21
move	$21,$3
$L286:
sll	$3,$18,4
sw	$18,92($sp)
sll	$16,$18,2
move	$17,$0
subu	$16,$3,$16
subu	$16,$16,$18
lw	$18,76($sp)
move	$3,$16
move	$16,$22
move	$22,$20
move	$20,$21
move	$21,$17
move	$17,$3
addu	$3,$20,$2
$L932:
addu	$4,$22,$21
lbu	$3,0($3)
lbu	$4,0($4)
sll	$5,$2,$3
addu	$9,$3,$9
sll	$3,$11,$3
.set	noreorder
.set	nomacro
bltz	$9,$L278
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$8,5628($fp)
lw	$2,5632($fp)
sltu	$2,$8,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L931
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$8,2
sw	$2,5628($fp)
lbu	$2,1($8)
lbu	$8,0($8)
sll	$2,$2,8
or	$2,$2,$8
sll	$8,$2,8
srl	$2,$2,8
or	$2,$8,$2
andi	$2,$2,0xffff
sll	$2,$2,$9
addiu	$9,$9,-16
or	$3,$3,$2
$L278:
addiu	$2,$5,-1
$L931:
mul	$2,$4,$2
sra	$2,$2,8
addiu	$2,$2,1
sll	$4,$2,16
sltu	$8,$3,$4
.set	noreorder
.set	nomacro
bne	$8,$0,$L279
sw	$9,5624($fp)
.set	macro
.set	reorder

subu	$3,$3,$4
lw	$25,72($sp)
subu	$2,$5,$2
li	$5,8			# 0x8
sw	$3,5636($fp)
move	$4,$19
.set	noreorder
.set	nomacro
jalr	$25
sw	$2,5620($fp)
.set	macro
.set	reorder

lb	$3,-1($16)
.set	noreorder
.set	nomacro
bltz	$3,$L826
lw	$28,32($sp)
.set	macro
.set	reorder

addu	$8,$17,$18
andi	$2,$2,0x00ff
move	$5,$16
$L282:
sll	$4,$3,5
addiu	$5,$5,1
addu	$3,$4,$3
addu	$3,$8,$3
addu	$3,$fp,$3
addu	$3,$3,$21
sb	$2,6898($3)
lb	$3,-1($5)
bgez	$3,$L282
$L826:
lw	$2,5620($fp)
lw	$9,5624($fp)
lw	$11,5636($fp)
$L281:
addiu	$21,$21,1
.set	noreorder
.set	nomacro
bne	$21,$23,$L932
addu	$3,$20,$2
.set	macro
.set	reorder

lw	$18,92($sp)
li	$3,3			# 0x3
move	$21,$20
move	$20,$22
addiu	$18,$18,1
move	$22,$16
.set	noreorder
.set	nomacro
bne	$18,$3,$L286
addiu	$20,$20,11
.set	macro
.set	reorder

lw	$6,80($sp)
addiu	$17,$16,10
lw	$7,%got(vp8_coeff_band_indexes+81)($28)
move	$20,$21
addiu	$6,$6,33
addiu	$7,$7,%lo(vp8_coeff_band_indexes+81)
.set	noreorder
.set	nomacro
bne	$17,$7,$L288
sw	$6,80($sp)
.set	macro
.set	reorder

lw	$8,84($sp)
move	$10,$9
lw	$9,88($sp)
li	$12,4			# 0x4
move	$17,$21
addiu	$8,$8,1
addiu	$9,$9,264
sw	$8,84($sp)
.set	noreorder
.set	nomacro
bne	$8,$12,$L277
sw	$9,88($sp)
.set	macro
.set	reorder

addu	$3,$21,$2
lbu	$3,0($3)
sll	$2,$2,$3
addu	$10,$3,$10
sll	$11,$11,$3
.set	noreorder
.set	nomacro
bltz	$10,$L289
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$4,5628($fp)
lw	$3,5632($fp)
sltu	$3,$4,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L933
addiu	$3,$2,-1
.set	macro
.set	reorder

addiu	$3,$4,2
sw	$3,5628($fp)
lbu	$3,1($4)
lbu	$4,0($4)
sll	$3,$3,8
or	$3,$3,$4
sll	$4,$3,8
srl	$3,$3,8
or	$3,$4,$3
andi	$3,$3,0xffff
sll	$3,$3,$10
addiu	$10,$10,-16
or	$11,$11,$3
$L289:
addiu	$3,$2,-1
$L933:
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$4,$3,16
sltu	$5,$11,$4
xori	$5,$5,0x1
.set	noreorder
.set	nomacro
beq	$5,$0,$L290
sw	$10,5624($fp)
.set	macro
.set	reorder

subu	$11,$11,$4
lw	$25,72($sp)
subu	$3,$2,$3
lw	$4,168($sp)
sw	$5,6760($fp)
li	$5,8			# 0x8
sw	$11,5636($fp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$28,32($sp)
sb	$2,6887($fp)
lw	$2,5660($fp)
beq	$2,$0,$L857
$L291:
lw	$2,5668($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L656
li	$17,1			# 0x1
.set	macro
.set	reorder

lw	$2,5672($fp)
beq	$2,$0,$L934
$L630:
lw	$2,5676($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L305
move	$17,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L588
li	$2,8			# 0x8
.set	macro
.set	reorder

$L333:
lw	$7,5624($fp)
addiu	$8,$fp,6884
lw	$10,5636($fp)
.set	noreorder
.set	nomacro
b	$L337
move	$5,$0
.set	macro
.set	reorder

$L859:
subu	$10,$6,$12
sw	$3,5620($fp)
addu	$4,$11,$4
sw	$10,5636($fp)
lb	$5,0($4)
blez	$5,$L858
$L337:
lw	$3,5620($fp)
addu	$4,$8,$5
lw	$18,%got(vp8_segmentid_tree)($28)
sll	$5,$5,1
addu	$2,$23,$3
lbu	$4,0($4)
addiu	$18,$18,%lo(vp8_segmentid_tree)
lbu	$6,0($2)
addu	$11,$18,$5
sll	$3,$3,$6
addu	$7,$6,$7
addiu	$2,$3,-1
sw	$3,5620($fp)
sll	$6,$10,$6
.set	noreorder
.set	nomacro
bltz	$7,$L335
mul	$2,$2,$4
.set	macro
.set	reorder

lw	$4,5628($fp)
lw	$5,5632($fp)
sltu	$5,$4,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L335
addiu	$10,$4,2
.set	macro
.set	reorder

sw	$10,5628($fp)
lbu	$10,1($4)
lbu	$4,0($4)
sll	$10,$10,8
or	$10,$10,$4
sll	$4,$10,8
srl	$10,$10,8
or	$10,$4,$10
andi	$4,$10,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$6,$6,$4
$L335:
sra	$2,$2,8
sw	$7,5624($fp)
move	$10,$6
addiu	$2,$2,1
sll	$12,$2,16
sltu	$5,$6,$12
xori	$4,$5,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L859
subu	$3,$3,$2
.set	macro
.set	reorder

addu	$4,$11,$4
sw	$2,5620($fp)
sw	$10,5636($fp)
lb	$5,0($4)
bgtz	$5,$L337
$L858:
subu	$2,$0,$5
andi	$2,$2,0x00ff
sb	$2,0($9)
sw	$2,6756($fp)
lw	$3,6760($fp)
bne	$3,$0,$L860
$L642:
move	$2,$0
lw	$3,72($sp)
sb	$2,0($3)
lw	$5,5660($fp)
.set	noreorder
.set	nomacro
bne	$5,$0,$L935
lw	$9,%got(vp8_pred16x16_tree_intra)($28)
.set	macro
.set	reorder

$L341:
lw	$3,5620($fp)
lw	$2,5624($fp)
lw	$8,5636($fp)
addu	$4,$23,$3
lbu	$7,6888($fp)
lbu	$6,0($4)
sll	$3,$3,$6
addu	$2,$6,$2
sll	$8,$8,$6
.set	noreorder
.set	nomacro
bltz	$2,$L356
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$9,5628($fp)
lw	$4,5632($fp)
sltu	$4,$9,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L936
addiu	$4,$3,-1
.set	macro
.set	reorder

addiu	$4,$9,2
sw	$4,5628($fp)
lbu	$6,1($9)
lbu	$4,0($9)
sll	$6,$6,8
or	$6,$6,$4
sll	$4,$6,8
srl	$6,$6,8
or	$4,$4,$6
andi	$4,$4,0xffff
sll	$4,$4,$2
addiu	$2,$2,-16
or	$8,$8,$4
$L356:
addiu	$4,$3,-1
$L936:
mul	$4,$7,$4
sra	$4,$4,8
addiu	$4,$4,1
sll	$6,$4,16
sltu	$7,$8,$6
.set	noreorder
.set	nomacro
bne	$7,$0,$L357
sw	$2,5624($fp)
.set	macro
.set	reorder

subu	$3,$3,$4
lbu	$7,6889($fp)
subu	$8,$8,$6
sw	$0,68($sp)
addu	$4,$23,$3
sw	$8,5636($fp)
lbu	$6,0($4)
sll	$3,$3,$6
addu	$2,$2,$6
sll	$6,$8,$6
.set	noreorder
.set	nomacro
bltz	$2,$L359
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$8,5628($fp)
lw	$4,5632($fp)
sltu	$4,$8,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L937
addiu	$4,$3,-1
.set	macro
.set	reorder

addiu	$4,$8,2
sw	$4,5628($fp)
lbu	$4,1($8)
lbu	$8,0($8)
sll	$4,$4,8
or	$4,$4,$8
sll	$8,$4,8
srl	$4,$4,8
or	$4,$8,$4
andi	$4,$4,0xffff
sll	$4,$4,$2
addiu	$2,$2,-16
or	$6,$6,$4
$L359:
addiu	$4,$3,-1
$L937:
mul	$4,$7,$4
sra	$4,$4,8
addiu	$4,$4,1
sll	$7,$4,16
sltu	$8,$6,$7
.set	noreorder
.set	nomacro
bne	$8,$0,$L361
sw	$2,5624($fp)
.set	macro
.set	reorder

subu	$3,$3,$4
lbu	$8,6890($fp)
subu	$6,$6,$7
addu	$4,$23,$3
sw	$6,5636($fp)
lbu	$4,0($4)
sll	$3,$3,$4
addu	$5,$2,$4
sll	$4,$6,$4
.set	noreorder
.set	nomacro
bltz	$5,$L363
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$2,5632($fp)
sltu	$2,$6,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L938
addiu	$2,$3,-1
.set	macro
.set	reorder

addiu	$2,$6,2
sw	$2,5628($fp)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$6,$2,$6
sll	$2,$6,8
srl	$6,$6,8
or	$2,$2,$6
andi	$2,$2,0xffff
sll	$2,$2,$5
addiu	$5,$5,-16
or	$4,$4,$2
$L363:
addiu	$2,$3,-1
$L938:
sw	$5,5624($fp)
mul	$2,$2,$8
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$6,$4,$5
bne	$6,$0,$L365
subu	$2,$3,$2
subu	$4,$4,$5
li	$3,3			# 0x3
li	$5,2			# 0x2
sw	$2,5620($fp)
sw	$4,5636($fp)
$L615:
lw	$8,72($sp)
sb	$3,2($8)
$L364:
sll	$2,$5,2
lw	$9,72($sp)
sw	$0,40($sp)
addiu	$4,$fp,6764
addu	$2,$fp,$2
sw	$0,44($sp)
sw	$0,48($sp)
sw	$0,52($sp)
lw	$3,6780($2)
addiu	$3,$3,1
sw	$3,6780($2)
lbu	$2,2($9)
lbu	$3,146($9)
addiu	$2,$2,1690
sll	$2,$2,2
addu	$2,$fp,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L643
lw	$6,4($2)
.set	macro
.set	reorder

lw	$5,148($9)
.set	noreorder
.set	nomacro
beq	$5,$0,$L367
lbu	$2,68($sp)
.set	macro
.set	reorder

sll	$2,$3,2
addu	$2,$4,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$6,$2,$L368
li	$3,2147418112			# 0x7fff0000
.set	macro
.set	reorder

nor	$2,$0,$5
li	$5,65536			# 0x10000
addiu	$3,$3,32767
addiu	$5,$5,1
and	$3,$2,$3
addu	$3,$3,$5
li	$5,-2147483648			# 0xffffffff80000000
ori	$5,$5,0x8000
and	$2,$2,$5
xor	$5,$3,$2
$L368:
lbu	$2,69($sp)
li	$3,1			# 0x1
sw	$5,44($sp)
addiu	$2,$2,2
sb	$2,69($sp)
$L366:
lw	$10,72($sp)
lbu	$2,-70($10)
.set	noreorder
.set	nomacro
bne	$2,$0,$L939
lw	$11,72($sp)
.set	macro
.set	reorder

$L878:
addiu	$8,$sp,40
$L369:
lw	$12,72($sp)
lbu	$5,74($12)
.set	noreorder
.set	nomacro
beq	$5,$0,$L374
lbu	$13,68($sp)
.set	macro
.set	reorder

lw	$13,72($sp)
lw	$2,76($13)
.set	noreorder
.set	nomacro
beq	$2,$0,$L375
sll	$5,$5,2
.set	macro
.set	reorder

addu	$5,$4,$5
lw	$4,0($5)
.set	noreorder
.set	nomacro
beq	$6,$4,$L376
li	$4,2147418112			# 0x7fff0000
.set	macro
.set	reorder

li	$5,65536			# 0x10000
addiu	$4,$4,32767
nor	$2,$0,$2
addiu	$5,$5,1
and	$4,$2,$4
addu	$4,$4,$5
li	$5,-2147483648			# 0xffffffff80000000
ori	$5,$5,0x8000
and	$2,$2,$5
xor	$2,$4,$2
$L376:
sll	$4,$3,2
addu	$4,$8,$4
lw	$4,0($4)
beq	$4,$2,$L377
addiu	$3,$3,1
sll	$4,$3,2
addu	$4,$8,$4
sw	$2,0($4)
$L377:
addiu	$2,$sp,68
addu	$3,$2,$3
lbu	$2,0($3)
addiu	$2,$2,1
sb	$2,0($3)
lbu	$13,68($sp)
$L374:
lbu	$2,71($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L379
lbu	$3,69($sp)
.set	macro
.set	reorder

lw	$2,52($sp)
lw	$3,44($sp)
.set	noreorder
.set	nomacro
beq	$3,$2,$L862
lbu	$3,69($sp)
.set	macro
.set	reorder

$L379:
lw	$14,72($sp)
lbu	$6,70($sp)
lbu	$2,-71($14)
lbu	$5,145($14)
lbu	$4,73($14)
xori	$2,$2,0x9
xori	$5,$5,0x9
sltu	$5,$5,1
sltu	$2,$2,1
addu	$2,$2,$5
xori	$4,$4,0x9
sll	$2,$2,1
sltu	$4,$4,1
addu	$2,$4,$2
sltu	$5,$3,$6
andi	$4,$2,0x00ff
.set	noreorder
.set	nomacro
bne	$5,$0,$L381
sb	$4,71($sp)
.set	macro
.set	reorder

lh	$12,44($sp)
move	$6,$3
lh	$11,46($sp)
$L382:
lw	$5,5620($fp)
sltu	$14,$6,$13
xori	$14,$14,0x1
lw	$3,5624($fp)
sll	$14,$14,2
lw	$7,5636($fp)
addu	$2,$23,$5
lw	$15,72($sp)
addu	$14,$8,$14
lw	$8,%got(vp8_mode_contexts)($28)
sll	$13,$13,4
lh	$10,48($sp)
lbu	$2,0($2)
addiu	$8,$8,%lo(vp8_mode_contexts)
lw	$14,0($14)
lh	$9,50($sp)
addu	$13,$8,$13
sll	$5,$5,$2
addu	$3,$2,$3
sw	$14,4($15)
lw	$13,0($13)
sll	$7,$7,$2
.set	noreorder
.set	nomacro
bltz	$3,$L383
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$14,5628($fp)
lw	$2,5632($fp)
sltu	$2,$14,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L940
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$14,2
sw	$2,5628($fp)
lbu	$2,1($14)
lbu	$14,0($14)
sll	$2,$2,8
or	$2,$2,$14
sll	$14,$2,8
srl	$2,$2,8
or	$2,$14,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$7,$7,$2
$L383:
addiu	$2,$5,-1
$L940:
mul	$2,$13,$2
sra	$2,$2,8
addiu	$2,$2,1
sll	$13,$2,16
sltu	$14,$7,$13
.set	noreorder
.set	nomacro
bne	$14,$0,$L384
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$2,$5,$2
sll	$6,$6,4
addu	$5,$23,$2
subu	$7,$7,$13
addu	$6,$8,$6
lbu	$5,0($5)
sw	$7,5636($fp)
lw	$14,4($6)
sll	$2,$2,$5
addu	$3,$3,$5
sll	$7,$7,$5
.set	noreorder
.set	nomacro
bltz	$3,$L386
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$5,5632($fp)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L386
addiu	$5,$6,2
.set	macro
.set	reorder

sw	$5,5628($fp)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$3
addiu	$3,$3,-16
or	$7,$7,$5
$L386:
addiu	$6,$2,-1
mul	$6,$14,$6
sra	$6,$6,8
addiu	$6,$6,1
sll	$5,$6,16
sltu	$13,$7,$5
.set	noreorder
.set	nomacro
bne	$13,$0,$L388
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$2,$2,$6
lbu	$6,70($sp)
subu	$5,$7,$5
addu	$7,$23,$2
sll	$6,$6,4
sw	$5,5636($fp)
lbu	$7,0($7)
addu	$6,$8,$6
sll	$2,$2,$7
lw	$6,8($6)
addu	$3,$3,$7
sll	$5,$5,$7
.set	noreorder
.set	nomacro
bltz	$3,$L390
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$11,5628($fp)
lw	$7,5632($fp)
sltu	$7,$11,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L941
addiu	$7,$2,-1
.set	macro
.set	reorder

addiu	$7,$11,2
sw	$7,5628($fp)
lbu	$7,1($11)
lbu	$11,0($11)
sll	$7,$7,8
or	$7,$7,$11
sll	$11,$7,8
srl	$7,$7,8
or	$7,$11,$7
andi	$7,$7,0xffff
sll	$7,$7,$3
addiu	$3,$3,-16
or	$5,$5,$7
$L390:
addiu	$7,$2,-1
$L941:
mul	$6,$6,$7
sra	$6,$6,8
addiu	$6,$6,1
sll	$7,$6,16
sltu	$11,$5,$7
.set	noreorder
.set	nomacro
bne	$11,$0,$L393
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$2,$2,$6
sll	$4,$4,4
addu	$6,$23,$2
subu	$5,$5,$7
addu	$8,$8,$4
lbu	$6,0($6)
sw	$5,5636($fp)
lw	$4,12($8)
sll	$2,$2,$6
addu	$7,$3,$6
sll	$3,$5,$6
.set	noreorder
.set	nomacro
bltz	$7,$L395
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$5,5632($fp)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L942
addiu	$5,$2,-1
.set	macro
.set	reorder

addiu	$5,$6,2
sw	$5,5628($fp)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$7
addiu	$7,$7,-16
or	$3,$3,$5
$L395:
addiu	$5,$2,-1
$L942:
mul	$4,$4,$5
sra	$4,$4,8
addiu	$4,$4,1
sll	$5,$4,16
sltu	$6,$3,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L398
sw	$7,5624($fp)
.set	macro
.set	reorder

lw	$8,72($sp)
subu	$2,$2,$4
subu	$3,$3,$5
lw	$9,92($sp)
lh	$6,4($8)
sll	$4,$9,6
sw	$2,5620($fp)
li	$2,9			# 0x9
sw	$3,5636($fp)
sb	$2,1($8)
li	$2,-64			# 0xffffffffffffffc0
lw	$3,5644($fp)
subu	$4,$2,$4
lw	$2,5648($fp)
addiu	$3,$3,-1
slt	$5,$6,$4
subu	$3,$3,$9
sll	$3,$3,6
.set	noreorder
.set	nomacro
bne	$5,$0,$L399
addiu	$3,$3,64
.set	macro
.set	reorder

slt	$4,$6,$3
movz	$6,$3,$4
move	$4,$6
$L399:
lw	$12,72($sp)
lw	$13,184($sp)
sh	$4,4($12)
lh	$5,6($12)
slt	$3,$5,$13
bne	$3,$0,$L644
lw	$14,136($sp)
addiu	$2,$2,-1
subu	$2,$2,$14
sll	$2,$2,6
addiu	$2,$2,64
slt	$3,$2,$5
movz	$2,$5,$3
$L403:
lw	$6,5620($fp)
lw	$15,72($sp)
lw	$3,5624($fp)
addu	$4,$23,$6
lw	$20,%got(vp8_mbsplits)($28)
lw	$7,5636($fp)
addiu	$18,$15,8
lbu	$12,-69($15)
lbu	$5,0($4)
addiu	$20,$20,%lo(vp8_mbsplits)
lbu	$11,147($15)
sll	$12,$12,4
sh	$2,6($15)
sll	$6,$6,$5
sll	$11,$11,4
addu	$4,$5,$3
addu	$12,$20,$12
sw	$6,5620($fp)
addu	$11,$20,$11
.set	noreorder
.set	nomacro
bltz	$4,$L404
sll	$5,$7,$5
.set	macro
.set	reorder

lw	$3,5628($fp)
lw	$2,5632($fp)
sltu	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L943
addiu	$7,$6,-1
.set	macro
.set	reorder

addiu	$2,$3,2
sw	$2,5628($fp)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L404:
addiu	$7,$6,-1
$L943:
sll	$3,$7,6
sll	$2,$7,3
subu	$2,$3,$2
subu	$2,$2,$7
sll	$2,$2,1
sra	$2,$2,8
addiu	$2,$2,1
sll	$3,$2,16
sltu	$7,$5,$3
.set	noreorder
.set	nomacro
bne	$7,$0,$L405
sw	$4,5624($fp)
.set	macro
.set	reorder

subu	$2,$6,$2
subu	$5,$5,$3
addu	$3,$23,$2
sw	$5,5636($fp)
lbu	$6,0($3)
sll	$2,$2,$6
addu	$3,$4,$6
sll	$4,$5,$6
.set	noreorder
.set	nomacro
bltz	$3,$L407
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$5,5632($fp)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L944
addiu	$7,$2,-1
.set	macro
.set	reorder

addiu	$5,$6,2
sw	$5,5628($fp)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$3
addiu	$3,$3,-16
or	$4,$4,$5
$L407:
addiu	$7,$2,-1
$L944:
sll	$6,$7,7
sll	$5,$7,4
subu	$5,$6,$5
subu	$5,$5,$7
sra	$5,$5,8
addiu	$5,$5,1
sll	$6,$5,16
sltu	$7,$4,$6
.set	noreorder
.set	nomacro
bne	$7,$0,$L409
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$2,$2,$5
subu	$4,$4,$6
addu	$5,$23,$2
sw	$4,5636($fp)
lbu	$5,0($5)
sll	$2,$2,$5
addu	$3,$3,$5
sll	$5,$4,$5
.set	noreorder
.set	nomacro
bltz	$3,$L411
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$4,5632($fp)
sltu	$4,$6,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L945
addiu	$4,$2,-1
.set	macro
.set	reorder

addiu	$4,$6,2
sw	$4,5628($fp)
lbu	$4,1($6)
lbu	$6,0($6)
sll	$4,$4,8
or	$4,$4,$6
sll	$6,$4,8
srl	$4,$4,8
or	$4,$6,$4
andi	$4,$4,0xffff
sll	$4,$4,$3
addiu	$3,$3,-16
or	$5,$5,$4
$L411:
addiu	$4,$2,-1
$L945:
sw	$3,5624($fp)
move	$3,$4
sll	$3,$3,3
sll	$4,$4,1
addu	$4,$4,$3
sll	$3,$4,4
subu	$3,$3,$4
sra	$3,$3,8
addiu	$3,$3,1
sll	$7,$3,16
sltu	$4,$5,$7
xori	$4,$4,0x1
.set	noreorder
.set	nomacro
beq	$4,$0,$L412
move	$6,$4
.set	macro
.set	reorder

subu	$2,$2,$3
subu	$5,$5,$7
sw	$2,5620($fp)
move	$2,$4
.set	noreorder
.set	nomacro
b	$L408
sw	$5,5636($fp)
.set	macro
.set	reorder

$L862:
addiu	$3,$3,1
andi	$3,$3,0x00ff
.set	noreorder
.set	nomacro
b	$L379
sb	$3,69($sp)
.set	macro
.set	reorder

$L460:
sw	$3,5692($16)
.set	noreorder
.set	nomacro
b	$L461
sw	$10,5708($16)
.set	macro
.set	reorder

$L467:
lw	$14,%got(block0)($28)
li	$20,1			# 0x1
.set	noreorder
.set	nomacro
b	$L469
lw	$31,%lo(block0)($14)
.set	macro
.set	reorder

$L465:
sw	$3,5692($16)
.set	noreorder
.set	nomacro
b	$L466
sw	$11,5708($16)
.set	macro
.set	reorder

$L468:
lw	$14,88($sp)
li	$15,5			# 0x5
.set	noreorder
.set	nomacro
beq	$14,$15,$L470
lw	$19,%got(block0)($28)
.set	macro
.set	reorder

li	$25,5			# 0x5
sw	$25,88($sp)
.set	noreorder
.set	nomacro
b	$L463
lw	$31,%lo(block0)($19)
.set	macro
.set	reorder

$L470:
lw	$fp,80($sp)
.set	noreorder
.set	nomacro
bne	$18,$0,$L451
lw	$23,84($sp)
.set	macro
.set	reorder

lw	$11,76($sp)
lw	$10,72($sp)
sb	$11,0($10)
$L451:
lw	$12,%got(mb2)($28)
lw	$2,%lo(mb2)($12)
lbu	$2,9($2)
sltu	$2,$2,5
.set	noreorder
.set	nomacro
bne	$2,$0,$L946
lw	$14,72($sp)
.set	macro
.set	reorder

lw	$13,%got(motion_dsa2)($28)
li	$3,-2147483648			# 0xffffffff80000000
ori	$3,$3,0xffff
lw	$4,%lo(motion_dsa2)($13)
$L474:
lw	$2,0($4)
bne	$2,$3,$L474
sw	$0,0($4)
lw	$14,72($sp)
$L946:
lbu	$2,1($14)
sltu	$3,$2,5
.set	noreorder
.set	nomacro
beq	$3,$0,$L475
li	$3,4			# 0x4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L476
lbu	$4,0($14)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L947
lw	$12,%got(mau_reg_ptr1)($28)
.set	macro
.set	reorder

lw	$15,92($sp)
.set	noreorder
.set	nomacro
beq	$15,$0,$L948
lw	$11,%got(mau_reg_ptr1)($28)
.set	macro
.set	reorder

lw	$16,136($sp)
.set	noreorder
.set	nomacro
bne	$16,$0,$L949
li	$6,771686400			# 0x2dff0000
.set	macro
.set	reorder

lw	$11,%got(mau_reg_ptr1)($28)
$L948:
li	$4,771686400			# 0x2dff0000
lw	$17,92($sp)
ori	$4,$4,0xffff
lw	$5,%lo(mau_reg_ptr1)($11)
sw	$0,28($5)
lw	$3,0($5)
lw	$2,16($5)
sw	$0,32($5)
or	$3,$3,$4
li	$4,65536			# 0x10000
sw	$3,0($5)
or	$2,$2,$4
.set	noreorder
.set	nomacro
beq	$17,$0,$L863
sw	$2,16($5)
.set	macro
.set	reorder

$L480:
lw	$19,136($sp)
bne	$19,$0,$L483
$L482:
lw	$3,6752($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L622
li	$2,131072			# 0x20000
.set	macro
.set	reorder

$L623:
slt	$4,$3,4
$L902:
.set	noreorder
.set	nomacro
beq	$4,$0,$L622
li	$4,3			# 0x3
.set	macro
.set	reorder

beq	$3,$4,$L864
or	$2,$2,$3
$L622:
sw	$2,24($5)
$L492:
lw	$17,%got(mb0)($28)
$L901:
li	$6,72			# 0x48
lw	$3,6756($fp)
lw	$18,92($sp)
lw	$19,136($sp)
lw	$2,%lo(mb0)($17)
lw	$25,%call16(memcpy)($28)
lw	$5,72($sp)
addiu	$4,$2,8
sw	$18,4($2)
sw	$19,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$3,80($2)
.set	macro
.set	reorder

lw	$25,72($sp)
lbu	$2,0($25)
.set	noreorder
.set	nomacro
beq	$2,$0,$L508
lw	$28,32($sp)
.set	macro
.set	reorder

move	$3,$0
move	$2,$0
move	$4,$0
sw	$3,5892($fp)
sll	$3,$18,3
sw	$2,5888($fp)
lw	$2,5880($fp)
addu	$3,$3,$18
addu	$2,$2,$3
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 3($2)  
swr $4, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 7($2)  
swr $4, 4($2)  

# 0 "" 2
#NO_APP
lbu	$2,1($25)
li	$4,4			# 0x4
.set	noreorder
.set	nomacro
beq	$2,$4,$L508
li	$4,9			# 0x9
.set	macro
.set	reorder

beq	$2,$4,$L508
lw	$2,5880($fp)
sb	$0,5896($fp)
addu	$3,$2,$3
sb	$0,8($3)
$L508:
lw	$3,%got(count)($28)
lw	$2,%lo(count)($3)
.set	noreorder
.set	nomacro
blez	$2,$L950
lw	$11,%got(mb1)($28)
.set	macro
.set	reorder

lw	$4,%got(mb2)($28)
lw	$3,%lo(mb2)($4)
lbu	$2,9($3)
sltu	$2,$2,5
.set	noreorder
.set	nomacro
beq	$2,$0,$L511
lw	$17,120($sp)
.set	macro
.set	reorder

lbu	$2,8($3)
.set	noreorder
.set	nomacro
bne	$2,$0,$L512
lw	$7,%got(mau_reg_ptr0)($28)
.set	macro
.set	reorder

lw	$6,%got(mau_reg_ptr0)($28)
li	$2,268435456			# 0x10000000
lw	$5,%lo(mau_reg_ptr0)($6)
lw	$6,0($5)
and	$6,$6,$2
$L513:
lw	$8,80($3)
lw	$9,120($sp)
lw	$2,%got(vp8_qpar)($28)
sll	$3,$8,2
sw	$0,4($5)
addu	$3,$9,$3
lw	$7,0($3)
li	$3,122			# 0x7a
.set	noreorder
.set	nomacro
beq	$7,$3,$L865
addiu	$4,$8,20
.set	macro
.set	reorder

$L515:
addiu	$3,$8,4
sll	$4,$4,2
addiu	$10,$8,8
addu	$4,$2,$4
addiu	$9,$8,12
sll	$3,$3,2
addiu	$8,$8,16
lw	$11,0($4)
sll	$10,$10,2
addu	$3,$2,$3
sll	$9,$9,2
addu	$10,$2,$10
sll	$4,$8,2
lw	$3,0($3)
addu	$9,$2,$9
addu	$2,$2,$4
lw	$8,0($10)
sll	$4,$11,27
lw	$10,0($9)
andi	$3,$3,0x1f
lw	$9,0($2)
andi	$7,$7,0x7f
sll	$3,$3,7
andi	$8,$8,0x1f
or	$4,$4,$7
andi	$2,$10,0x1f
sll	$7,$8,12
or	$3,$4,$3
sll	$2,$2,17
andi	$4,$9,0x1f
or	$3,$3,$7
sll	$4,$4,22
or	$2,$3,$2
or	$2,$2,$4
sw	$2,4($5)
$L516:
.set	noreorder
.set	nomacro
beq	$6,$0,$L517
lw	$11,%got(block2)($28)
.set	macro
.set	reorder

lw	$10,%got(block2)($28)
li	$3,321585152			# 0x132b0000
lhu	$2,%lo(block2)($10)
or	$2,$2,$3
sw	$2,8($5)
li	$3,321585152			# 0x132b0000
$L976:
sw	$0,20($5)
andi	$2,$5,0xffff
or	$3,$2,$3
li	$2,-201326592			# 0xfffffffff4000000
sw	$3,12($5)
$L519:
lw	$3,9644($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L519
addiu	$4,$2,9644
.set	macro
.set	reorder

li	$2,-1289224192			# 0xffffffffb3280000
lw	$12,124($sp)
sw	$0,0($4)
addiu	$3,$2,104
#APP
# 2613 "vp8.c" 1
sw	 $12,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$2,108
lw	$13,108($sp)
#APP
# 2614 "vp8.c" 1
sw	 $13,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$13,96
addiu	$4,$2,112
#APP
# 2615 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$14,%got(mau_reg_ptr0)($28)
li	$3,321585152			# 0x132b0000
addiu	$5,$2,12
lhu	$4,%lo(mau_reg_ptr0)($14)
or	$4,$4,$3
#APP
# 2626 "vp8.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$3,9644
addiu	$4,$2,88
#APP
# 2627 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
addiu	$2,$2,64
#APP
# 2628 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$15,%got(count)($28)
lw	$2,%lo(count)($15)
slt	$4,$2,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L632
li	$3,-1288962048			# 0xffffffffb32c0000
.set	macro
.set	reorder

$L769:
lw	$2,18268($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L769
lw	$12,112($sp)
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
lw	$13,116($sp)
lw	$14,128($sp)
lw	$15,144($sp)
addiu	$5,$12,96
lw	$16,%got(mb1)($28)
sw	$0,18268($3)
addiu	$3,$13,8
sw	$14,9300($2)
sw	$15,9304($2)
sw	$12,9316($2)
sw	$13,9320($2)
sw	$5,9332($2)
sw	$3,9336($2)
li	$3,1			# 0x1
li	$2,-1289682944			# 0xffffffffb3210000
lw	$4,%lo(mb1)($16)
sw	$3,4($2)
lw	$3,5644($fp)
lw	$5,4($4)
addiu	$2,$3,-1
.set	noreorder
.set	nomacro
beq	$5,$2,$L526
lw	$17,%got(count)($28)
.set	macro
.set	reorder

addiu	$15,$15,256
addiu	$13,$13,128
sw	$15,144($sp)
sw	$13,116($sp)
lw	$2,%lo(count)($17)
$L525:
lw	$9,%got(mau_reg_ptr2)($28)
addiu	$2,$2,1
lw	$11,%got(block0)($28)
lw	$15,%got(block1)($28)
lw	$25,%got(motion_dha1)($28)
lw	$6,%got(mau_reg_ptr1)($28)
lw	$13,%lo(mau_reg_ptr2)($9)
lw	$9,%lo(block0)($11)
lw	$11,%lo(block1)($15)
lw	$15,%lo(motion_dha1)($25)
lw	$25,%got(count)($28)
lw	$14,%lo(mau_reg_ptr1)($6)
lw	$8,92($sp)
lw	$5,%got(mau_reg_ptr0)($28)
lw	$17,%got(mb0)($28)
lw	$18,%got(mb2)($28)
addiu	$8,$8,1
lw	$19,%got(motion_dha0)($28)
lw	$10,72($sp)
slt	$3,$8,$3
lw	$12,%lo(mau_reg_ptr0)($5)
lw	$16,%got(block2)($28)
lw	$5,%got(motion_dha2)($28)
addiu	$10,$10,72
sw	$2,%lo(count)($25)
lw	$2,%got(mau_reg_ptr0)($28)
lw	$7,%lo(mb0)($17)
lw	$6,%lo(motion_dha0)($19)
lw	$17,%got(motion_dsa0)($28)
lw	$19,%got(motion_dsa2)($28)
sw	$14,%lo(mau_reg_ptr0)($2)
sw	$8,92($sp)
lw	$14,%got(mau_reg_ptr1)($28)
lw	$8,%lo(mb2)($18)
lw	$18,%got(motion_dsa1)($28)
lw	$25,%got(block0)($28)
lw	$2,%got(block1)($28)
sw	$13,%lo(mau_reg_ptr1)($14)
sw	$10,72($sp)
lw	$14,%got(motion_dha0)($28)
lw	$10,%lo(block2)($16)
lw	$16,%lo(motion_dha2)($5)
lw	$5,%lo(motion_dsa0)($17)
lw	$17,%lo(motion_dsa1)($18)
lw	$18,%lo(motion_dsa2)($19)
lw	$19,%got(mau_reg_ptr2)($28)
lw	$13,%got(mb2)($28)
sw	$15,%lo(motion_dha0)($14)
sw	$12,%lo(mau_reg_ptr2)($19)
sw	$11,%lo(block0)($25)
sw	$10,%lo(block1)($2)
lw	$15,%got(motion_dha1)($28)
lw	$10,%got(block2)($28)
lw	$11,%got(mb0)($28)
lw	$12,%got(mb1)($28)
lw	$19,%got(motion_dsa0)($28)
lw	$25,%got(motion_dsa1)($28)
lw	$2,%got(motion_dsa2)($28)
sw	$16,%lo(motion_dha1)($15)
sw	$9,%lo(block2)($10)
sw	$4,%lo(mb0)($11)
sw	$8,%lo(mb1)($12)
sw	$7,%lo(mb2)($13)
sw	$17,%lo(motion_dsa0)($19)
sw	$18,%lo(motion_dsa1)($25)
lw	$16,%got(motion_dha2)($28)
sw	$6,%lo(motion_dha2)($16)
.set	noreorder
.set	nomacro
beq	$3,$0,$L332
sw	$5,%lo(motion_dsa2)($2)
.set	macro
.set	reorder

lw	$3,108($sp)
lw	$2,124($sp)
lw	$12,140($sp)
lw	$13,148($sp)
lw	$14,112($sp)
lw	$15,128($sp)
sw	$12,108($sp)
sw	$13,124($sp)
sw	$14,140($sp)
sw	$15,148($sp)
sw	$3,112($sp)
.set	noreorder
.set	nomacro
b	$L527
sw	$2,128($sp)
.set	macro
.set	reorder

$L475:
sltu	$2,$2,9
.set	noreorder
.set	nomacro
beq	$2,$0,$L494
lw	$15,72($sp)
.set	macro
.set	reorder

lw	$11,72($sp)
lw	$12,%got(motion_dha0)($28)
lbu	$5,92($sp)
lw	$14,188($sp)
lbu	$2,2($11)
lh	$4,4($11)
lh	$8,6($11)
or	$5,$5,$14
sll	$2,$2,12
lw	$6,%lo(motion_dha0)($12)
andi	$3,$4,0x7
lw	$13,%got(motion_dsa0)($28)
andi	$2,$2,0xffff
or	$3,$2,$3
sw	$5,0($6)
li	$2,33619968			# 0x2010000
andi	$9,$8,0x7
lhu	$7,%lo(motion_dsa0)($13)
addiu	$2,$2,240
sll	$8,$8,16
sll	$9,$9,20
andi	$4,$4,0xffff
or	$2,$3,$2
or	$2,$2,$9
or	$3,$4,$8
li	$4,-1610612736			# 0xffffffffa0000000
sw	$3,4($6)
li	$3,321650688			# 0x132c0000
sw	$2,8($6)
li	$2,-1073741824			# 0xffffffffc0000000
ori	$4,$4,0xffff
ori	$2,$2,0xffff
or	$7,$7,$3
sw	$2,12($6)
li	$2,-1289420800			# 0xffffffffb3250000
sw	$4,16($6)
addiu	$4,$2,88
#APP
# 779 "vp8.c" 1
sw	 $7,0($4)	#i_sw
# 0 "" 2
#NO_APP
lhu	$4,%lo(motion_dha0)($12)
addiu	$3,$3,1
addiu	$2,$2,84
or	$3,$4,$3
#APP
# 780 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L901
lw	$17,%got(mb0)($28)
.set	macro
.set	reorder

$L511:
lw	$2,80($3)
lw	$16,%got(mau_reg_ptr0)($28)
addiu	$4,$2,20
addiu	$3,$2,4
addiu	$7,$2,8
sll	$4,$4,2
lw	$5,%lo(mau_reg_ptr0)($16)
addiu	$9,$2,12
sll	$3,$3,2
sll	$10,$2,2
sll	$7,$7,2
lw	$6,0($5)
addiu	$8,$2,16
addu	$3,$17,$3
addu	$2,$17,$4
sll	$9,$9,2
addu	$4,$17,$10
lw	$3,0($3)
addu	$7,$17,$7
lw	$2,0($2)
sll	$8,$8,2
addu	$9,$17,$9
lw	$4,0($4)
lw	$7,0($7)
addu	$8,$17,$8
sll	$2,$2,27
lw	$9,0($9)
andi	$3,$3,0x1f
lw	$8,0($8)
andi	$4,$4,0x7f
sll	$3,$3,7
andi	$7,$7,0x1f
or	$4,$2,$4
sll	$7,$7,12
andi	$2,$9,0x1f
or	$3,$4,$3
sll	$2,$2,17
or	$3,$3,$7
andi	$4,$8,0x1f
sll	$4,$4,22
or	$2,$3,$2
li	$3,620691456			# 0x24ff0000
or	$2,$2,$4
ori	$3,$3,0xffff
li	$4,268435456			# 0x10000000
sw	$2,4($5)
or	$3,$6,$3
and	$6,$6,$4
.set	noreorder
.set	nomacro
beq	$6,$0,$L521
sw	$3,0($5)
.set	macro
.set	reorder

lw	$18,%got(block2)($28)
li	$3,321585152			# 0x132b0000
lhu	$2,%lo(block2)($18)
or	$2,$2,$3
li	$3,65536			# 0x10000
sw	$2,8($5)
addiu	$3,$3,800
sw	$3,16($5)
li	$3,321585152			# 0x132b0000
$L975:
sw	$0,24($5)
andi	$2,$5,0xffff
sw	$0,28($5)
or	$3,$2,$3
sw	$0,32($5)
li	$2,-201326592			# 0xfffffffff4000000
sw	$3,12($5)
$L523:
lw	$3,9644($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L523
addiu	$4,$2,9644
.set	macro
.set	reorder

li	$2,-1289224192			# 0xffffffffb3280000
lw	$25,124($sp)
sw	$0,0($4)
addiu	$3,$2,104
#APP
# 2675 "vp8.c" 1
sw	 $25,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$2,108
lw	$8,108($sp)
#APP
# 2676 "vp8.c" 1
sw	 $8,0($3)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$8,96
addiu	$4,$2,112
#APP
# 2677 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$9,%got(mau_reg_ptr0)($28)
li	$3,321585152			# 0x132b0000
addiu	$5,$2,12
lhu	$4,%lo(mau_reg_ptr0)($9)
or	$4,$4,$3
#APP
# 2687 "vp8.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$3,9644
addiu	$4,$2,88
#APP
# 2688 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
addiu	$2,$2,64
#APP
# 2689 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
lw	$10,%got(count)($28)
lw	$2,%lo(count)($10)
slt	$4,$2,2
.set	noreorder
.set	nomacro
beq	$4,$0,$L769
li	$3,-1288962048			# 0xffffffffb32c0000
.set	macro
.set	reorder

$L632:
lw	$11,%got(mb1)($28)
$L950:
lw	$3,5644($fp)
.set	noreorder
.set	nomacro
b	$L525
lw	$4,%lo(mb1)($11)
.set	macro
.set	reorder

$L476:
.set	noreorder
.set	nomacro
beq	$4,$0,$L484
addiu	$12,$fp,6736
.set	macro
.set	reorder

move	$2,$0
move	$3,$0
sw	$2,5904($fp)
sw	$3,5908($fp)
sw	$2,5912($fp)
sw	$3,5916($fp)
$L484:
lw	$3,%got(mau_reg_ptr1)($28)
li	$6,16			# 0x10
lw	$9,%got(vp8_4x4_pred_lookup)($28)
move	$10,$0
move	$7,$0
li	$11,4			# 0x4
lw	$5,%lo(mau_reg_ptr1)($3)
sw	$0,28($5)
sw	$0,32($5)
$L485:
addu	$8,$12,$10
sll	$4,$7,4
slt	$13,$7,2
$L488:
.set	noreorder
.set	nomacro
bne	$13,$0,$L866
lbu	$2,0($8)
.set	macro
.set	reorder

addu	$2,$9,$2
lw	$3,32($5)
lbu	$2,0($2)
sll	$2,$2,$4
or	$2,$3,$2
sw	$2,32($5)
$L487:
addiu	$4,$4,4
.set	noreorder
.set	nomacro
bne	$6,$4,$L488
addiu	$8,$8,1
.set	macro
.set	reorder

addiu	$7,$7,1
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$7,$11,$L485
addiu	$6,$6,16
.set	macro
.set	reorder

lw	$3,0($5)
li	$4,788463616			# 0x2eff0000
lw	$2,16($5)
ori	$4,$4,0xffff
lw	$8,92($sp)
or	$3,$3,$4
li	$4,65536			# 0x10000
sw	$3,0($5)
or	$2,$2,$4
.set	noreorder
.set	nomacro
beq	$8,$0,$L867
sw	$2,16($5)
.set	macro
.set	reorder

lw	$10,136($sp)
beq	$10,$0,$L482
$L483:
lw	$3,6752($fp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L622
li	$2,196608			# 0x30000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L902
slt	$4,$3,4
.set	macro
.set	reorder

$L866:
addu	$2,$9,$2
lw	$3,28($5)
lbu	$2,0($2)
sll	$2,$2,$4
or	$2,$3,$2
.set	noreorder
.set	nomacro
b	$L487
sw	$2,28($5)
.set	macro
.set	reorder

$L357:
sw	$4,5620($fp)
addiu	$9,$fp,6891
sw	$8,5636($fp)
.set	noreorder
.set	nomacro
b	$L360
move	$7,$0
.set	macro
.set	reorder

$L869:
subu	$8,$5,$11
sw	$4,5620($fp)
addu	$6,$10,$6
sw	$8,5636($fp)
lb	$7,0($6)
blez	$7,$L868
$L439:
lw	$4,5620($fp)
$L360:
addu	$5,$23,$4
lw	$11,%got(vp8_pred16x16_tree_inter)($28)
addu	$3,$9,$7
sll	$7,$7,1
lbu	$5,0($5)
addiu	$11,$11,%lo(vp8_pred16x16_tree_inter)
lbu	$6,0($3)
addu	$10,$11,$7
sll	$4,$4,$5
addu	$2,$5,$2
addiu	$3,$4,-1
sw	$4,5620($fp)
sll	$5,$8,$5
.set	noreorder
.set	nomacro
bltz	$2,$L437
mul	$3,$3,$6
.set	macro
.set	reorder

lw	$7,5628($fp)
lw	$6,5632($fp)
sltu	$6,$7,$6
.set	noreorder
.set	nomacro
beq	$6,$0,$L437
addiu	$8,$7,2
.set	macro
.set	reorder

sw	$8,5628($fp)
lbu	$6,1($7)
lbu	$7,0($7)
sll	$6,$6,8
or	$6,$6,$7
sll	$7,$6,8
srl	$6,$6,8
or	$6,$7,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$5,$5,$6
$L437:
sra	$3,$3,8
sw	$2,5624($fp)
move	$8,$5
addiu	$3,$3,1
sll	$11,$3,16
sltu	$7,$5,$11
xori	$6,$7,0x1
.set	noreorder
.set	nomacro
bne	$6,$0,$L869
subu	$4,$4,$3
.set	macro
.set	reorder

addu	$6,$10,$6
sw	$3,5620($fp)
sw	$8,5636($fp)
lb	$7,0($6)
bgtz	$7,$L439
$L868:
subu	$7,$0,$7
lw	$3,72($sp)
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$7,$2,$L440
sb	$7,1($3)
.set	macro
.set	reorder

lw	$4,%got(vp8_pred8x8c_tree)($28)
$L904:
addiu	$9,$fp,6895
lw	$7,5624($fp)
move	$5,$0
lw	$10,5636($fp)
.set	noreorder
.set	nomacro
b	$L441
addiu	$8,$4,%lo(vp8_pred8x8c_tree)
.set	macro
.set	reorder

$L871:
subu	$10,$6,$12
sw	$3,5620($fp)
addu	$4,$11,$4
sw	$10,5636($fp)
lb	$5,0($4)
.set	noreorder
.set	nomacro
blez	$5,$L951
lw	$3,72($sp)
.set	macro
.set	reorder

$L441:
lw	$3,5620($fp)
addu	$4,$9,$5
sll	$5,$5,1
addu	$2,$23,$3
lbu	$4,0($4)
addu	$11,$8,$5
lbu	$6,0($2)
sll	$3,$3,$6
addu	$7,$6,$7
addiu	$2,$3,-1
sw	$3,5620($fp)
sll	$6,$10,$6
.set	noreorder
.set	nomacro
bltz	$7,$L447
mul	$2,$2,$4
.set	macro
.set	reorder

lw	$5,5628($fp)
lw	$4,5632($fp)
sltu	$4,$5,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L447
addiu	$10,$5,2
.set	macro
.set	reorder

sw	$10,5628($fp)
lbu	$10,1($5)
lbu	$4,0($5)
sll	$10,$10,8
or	$10,$10,$4
sll	$4,$10,8
srl	$10,$10,8
or	$10,$4,$10
andi	$4,$10,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$6,$6,$4
$L447:
sra	$2,$2,8
sw	$7,5624($fp)
move	$10,$6
addiu	$2,$2,1
sll	$12,$2,16
sltu	$5,$6,$12
xori	$4,$5,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L871
subu	$3,$3,$2
.set	macro
.set	reorder

addu	$4,$11,$4
sw	$2,5620($fp)
sw	$10,5636($fp)
lb	$5,0($4)
.set	noreorder
.set	nomacro
bgtz	$5,$L441
lw	$3,72($sp)
.set	macro
.set	reorder

$L951:
subu	$5,$0,$5
li	$2,4			# 0x4
sw	$5,6752($fp)
sb	$0,2($3)
sb	$2,3($3)
.set	noreorder
.set	nomacro
b	$L355
sw	$0,8($3)
.set	macro
.set	reorder

$L494:
li	$4,3			# 0x3
lw	$16,%got(motion_dha0)($28)
lbu	$3,3($15)
lbu	$2,2($15)
.set	noreorder
.set	nomacro
beq	$3,$4,$L872
lw	$13,%lo(motion_dha0)($16)
.set	macro
.set	reorder

li	$4,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$4,$L873
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$4,$L874
lw	$10,72($sp)
.set	macro
.set	reorder

lw	$9,72($sp)
sll	$2,$2,12
lw	$11,72($sp)
andi	$2,$2,0xffff
lw	$12,196($sp)
lh	$8,14($9)
li	$9,65536			# 0x10000
lh	$6,12($10)
addiu	$5,$9,2224
lh	$7,8($11)
andi	$4,$8,0x7
lh	$10,10($10)
andi	$3,$6,0x7
lw	$14,192($sp)
or	$3,$3,$5
lbu	$5,92($sp)
sll	$4,$4,20
or	$3,$3,$2
or	$11,$12,$5
or	$4,$3,$4
addiu	$3,$9,176
sw	$11,0($13)
sw	$4,16($13)
andi	$11,$10,0x7
andi	$4,$7,0x7
lw	$9,16($13)
sll	$10,$10,16
or	$3,$4,$3
sll	$11,$11,20
andi	$4,$7,0xffff
or	$4,$4,$10
or	$3,$3,$11
or	$2,$3,$2
sw	$4,4($13)
sll	$8,$8,16
li	$4,33554432			# 0x2000000
sw	$2,8($13)
andi	$6,$6,0xffff
or	$3,$6,$8
or	$9,$9,$4
or	$5,$14,$5
$L829:
li	$2,-1610612736			# 0xffffffffa0000000
sw	$9,16($13)
sw	$3,12($13)
ori	$2,$2,0xffff
sw	$5,20($13)
sw	$2,24($13)
lw	$15,%got(motion_dsa0)($28)
$L905:
li	$3,321650688			# 0x132c0000
li	$2,-1289420800			# 0xffffffffb3250000
addiu	$5,$2,88
lhu	$4,%lo(motion_dsa0)($15)
or	$4,$4,$3
#APP
# 719 "vp8.c" 1
sw	 $4,0($5)	#i_sw
# 0 "" 2
#NO_APP
lw	$16,%got(motion_dha0)($28)
addiu	$3,$3,1
addiu	$2,$2,84
lhu	$4,%lo(motion_dha0)($16)
or	$3,$4,$3
#APP
# 720 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L901
lw	$17,%got(mb0)($28)
.set	macro
.set	reorder

$L454:
lw	$10,%got(block0)($28)
move	$4,$0
sw	$0,84($sp)
move	$18,$0
li	$3,3			# 0x3
sw	$0,80($sp)
lw	$10,%lo(block0)($10)
.set	noreorder
.set	nomacro
b	$L453
sw	$10,152($sp)
.set	macro
.set	reorder

$L526:
lw	$19,144($sp)
lw	$25,116($sp)
lw	$18,%got(count)($28)
addiu	$19,$19,1280
addiu	$25,$25,640
lw	$2,%lo(count)($18)
sw	$19,144($sp)
.set	noreorder
.set	nomacro
b	$L525
sw	$25,116($sp)
.set	macro
.set	reorder

$L521:
lw	$19,%got(block2)($28)
li	$3,65536			# 0x10000
addiu	$3,$3,768
lw	$2,%lo(block2)($19)
sw	$3,16($5)
li	$3,321585152			# 0x132b0000
addiu	$2,$2,32
andi	$2,$2,0xffff
or	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L975
sw	$2,8($5)
.set	macro
.set	reorder

$L517:
li	$3,321585152			# 0x132b0000
lw	$2,%lo(block2)($11)
addiu	$2,$2,32
andi	$2,$2,0xffff
or	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L976
sw	$2,8($5)
.set	macro
.set	reorder

$L512:
li	$6,2			# 0x2
lw	$5,%lo(mau_reg_ptr0)($7)
lw	$4,0($5)
srl	$2,$4,24
andi	$2,$2,0x3
.set	noreorder
.set	nomacro
beq	$2,$6,$L875
li	$2,788529152			# 0x2f000000
.set	macro
.set	reorder

lw	$8,80($3)
lw	$9,120($sp)
move	$6,$0
sw	$0,4($5)
sll	$3,$8,2
sw	$2,0($5)
li	$2,65536			# 0x10000
addu	$3,$9,$3
addiu	$4,$8,20
sw	$2,16($5)
lw	$7,0($3)
li	$3,122			# 0x7a
.set	noreorder
.set	nomacro
bne	$7,$3,$L515
lw	$2,%got(vp8_qpar)($28)
.set	macro
.set	reorder

$L865:
addiu	$10,$8,4
addiu	$3,$8,8
addiu	$9,$8,12
sll	$4,$4,2
sll	$10,$10,2
sll	$3,$3,2
addiu	$7,$8,16
addu	$4,$2,$4
addu	$10,$2,$10
sll	$9,$9,2
addu	$8,$2,$3
lw	$4,0($4)
sll	$7,$7,2
lw	$3,0($10)
addu	$9,$2,$9
addu	$7,$2,$7
lw	$8,0($8)
addiu	$4,$4,1
lw	$2,0($9)
addiu	$3,$3,1
lw	$7,0($7)
addiu	$8,$8,1
sll	$4,$4,27
andi	$3,$3,0x1f
addiu	$2,$2,1
sll	$3,$3,7
andi	$8,$8,0x1f
addiu	$7,$7,1
ori	$4,$4,0x79
sll	$8,$8,12
andi	$2,$2,0x1f
or	$3,$4,$3
sll	$2,$2,17
andi	$4,$7,0x1f
or	$3,$3,$8
sll	$4,$4,22
or	$2,$3,$2
or	$2,$2,$4
.set	noreorder
.set	nomacro
b	$L516
sw	$2,4($5)
.set	macro
.set	reorder

$L947:
li	$6,771686400			# 0x2dff0000
$L949:
lw	$3,%got(vp8_16x16_pred_lookup)($28)
ori	$6,$6,0xffff
lw	$17,92($sp)
lw	$5,%lo(mau_reg_ptr1)($12)
addu	$2,$3,$2
sw	$0,32($5)
lw	$4,0($5)
lw	$3,16($5)
or	$4,$4,$6
li	$6,65536			# 0x10000
sw	$4,0($5)
lbu	$2,0($2)
or	$3,$3,$6
sw	$3,16($5)
.set	noreorder
.set	nomacro
bne	$17,$0,$L480
sw	$2,28($5)
.set	macro
.set	reorder

$L863:
lw	$18,136($sp)
sltu	$2,$0,$18
$L481:
lw	$3,6752($fp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L623
sll	$2,$2,16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L492
sw	$2,24($5)
.set	macro
.set	reorder

$L852:
sll	$19,$8,2
lw	$2,5864($fp)
lw	$15,%got(vp8_pred4x4_prob_intra)($28)
addiu	$16,$fp,6736
lw	$9,%got(vp8_pred4x4_tree)($28)
addiu	$14,$19,4
addiu	$13,$fp,5868
addiu	$18,$fp,6752
addu	$19,$2,$19
addu	$14,$2,$14
addiu	$15,$15,%lo(vp8_pred4x4_prob_intra)
addiu	$10,$9,%lo(vp8_pred4x4_tree)
$L346:
lbu	$3,0($13)
move	$9,$19
move	$11,$16
$L350:
lbu	$4,0($9)
sll	$8,$3,3
lw	$7,5624($fp)
move	$5,$0
addu	$3,$8,$3
lw	$12,5636($fp)
sll	$2,$4,1
sll	$4,$4,3
subu	$2,$4,$2
sll	$8,$2,4
subu	$8,$8,$2
addu	$8,$3,$8
.set	noreorder
.set	nomacro
b	$L349
addu	$8,$15,$8
.set	macro
.set	reorder

$L877:
subu	$12,$6,$20
sw	$3,5620($fp)
addu	$4,$17,$4
sw	$12,5636($fp)
lb	$5,0($4)
blez	$5,$L876
$L349:
lw	$3,5620($fp)
addu	$2,$8,$5
sll	$5,$5,1
addu	$4,$23,$3
lbu	$2,0($2)
addu	$17,$10,$5
lbu	$6,0($4)
sll	$3,$3,$6
addu	$7,$6,$7
addiu	$4,$3,-1
sw	$3,5620($fp)
sll	$6,$12,$6
.set	noreorder
.set	nomacro
bltz	$7,$L347
mul	$2,$4,$2
.set	macro
.set	reorder

lw	$5,5628($fp)
lw	$4,5632($fp)
sltu	$4,$5,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L347
addiu	$12,$5,2
.set	macro
.set	reorder

sw	$12,5628($fp)
lbu	$4,1($5)
lbu	$5,0($5)
sll	$4,$4,8
or	$4,$4,$5
sll	$5,$4,8
srl	$4,$4,8
or	$4,$5,$4
andi	$4,$4,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$6,$6,$4
$L347:
sra	$2,$2,8
sw	$7,5624($fp)
move	$12,$6
addiu	$2,$2,1
sll	$20,$2,16
sltu	$5,$6,$20
xori	$4,$5,0x1
.set	noreorder
.set	nomacro
bne	$4,$0,$L877
subu	$3,$3,$2
.set	macro
.set	reorder

addu	$4,$17,$4
sw	$2,5620($fp)
sw	$12,5636($fp)
lb	$5,0($4)
bgtz	$5,$L349
$L876:
subu	$5,$0,$5
addiu	$9,$9,1
andi	$3,$5,0x00ff
addiu	$11,$11,1
sb	$3,-1($11)
sb	$3,-1($9)
.set	noreorder
.set	nomacro
bne	$9,$14,$L350
sb	$3,0($13)
.set	macro
.set	reorder

addiu	$16,$16,4
.set	noreorder
.set	nomacro
bne	$16,$18,$L346
addiu	$13,$13,1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L903
lw	$9,%got(vp8_pred8x8c_tree)($28)
.set	macro
.set	reorder

$L456:
li	$10,1			# 0x1
sw	$2,5692($3)
li	$4,33			# 0x21
sw	$7,5708($3)
move	$18,$0
sb	$0,8($9)
move	$3,$0
sw	$0,84($sp)
sb	$0,5896($fp)
.set	noreorder
.set	nomacro
b	$L453
sw	$10,80($sp)
.set	macro
.set	reorder

$L384:
lw	$3,72($sp)
sw	$2,5620($fp)
li	$2,7			# 0x7
sw	$7,5636($fp)
sw	$0,4($3)
sb	$2,1($3)
$L631:
lw	$3,72($sp)
$L906:
lw	$8,72($sp)
lw	$2,4($3)
li	$3,4			# 0x4
sb	$3,3($8)
.set	noreorder
.set	nomacro
b	$L355
sw	$2,8($8)
.set	macro
.set	reorder

$L643:
lw	$10,72($sp)
lbu	$2,-70($10)
.set	noreorder
.set	nomacro
beq	$2,$0,$L878
move	$3,$0
.set	macro
.set	reorder

lw	$11,72($sp)
$L939:
lw	$5,-68($11)
.set	noreorder
.set	nomacro
beq	$5,$0,$L370
addiu	$8,$sp,40
.set	macro
.set	reorder

sll	$2,$2,2
addu	$2,$4,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$6,$2,$L371
li	$2,2147418112			# 0x7fff0000
.set	macro
.set	reorder

li	$7,65536			# 0x10000
addiu	$2,$2,32767
nor	$5,$0,$5
addiu	$7,$7,1
and	$2,$5,$2
addu	$2,$2,$7
li	$7,-2147483648			# 0xffffffff80000000
ori	$7,$7,0x8000
and	$5,$5,$7
xor	$5,$2,$5
$L371:
sll	$2,$3,2
addiu	$8,$sp,40
addu	$2,$8,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$5,$L952
addiu	$2,$sp,68
.set	macro
.set	reorder

addiu	$3,$3,1
sll	$2,$3,2
addu	$2,$8,$2
sw	$5,0($2)
addiu	$2,$sp,68
$L952:
addu	$2,$2,$3
lbu	$5,0($2)
addiu	$5,$5,2
.set	noreorder
.set	nomacro
b	$L369
sb	$5,0($2)
.set	macro
.set	reorder

$L361:
li	$2,1			# 0x1
lw	$3,72($sp)
sw	$4,5620($fp)
sw	$6,5636($fp)
.set	noreorder
.set	nomacro
b	$L364
sb	$2,2($3)
.set	macro
.set	reorder

$L867:
lw	$9,136($sp)
.set	noreorder
.set	nomacro
b	$L481
sltu	$2,$0,$9
.set	macro
.set	reorder

$L381:
lh	$5,48($sp)
lh	$2,50($sp)
lw	$7,44($sp)
sb	$3,70($sp)
move	$12,$5
sb	$6,69($sp)
move	$11,$2
sh	$5,44($sp)
sw	$7,48($sp)
.set	noreorder
.set	nomacro
b	$L382
sh	$2,46($sp)
.set	macro
.set	reorder

$L332:
li	$2,-2147483648			# 0xffffffff80000000
addiu	$3,$2,16384
$L528:
#APP
# 2763 "vp8.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L528
#APP
# 2763 "vp8.c" 1
sync
# 0 "" 2
#NO_APP
lw	$3,136($sp)
lw	$2,5648($fp)
addiu	$3,$3,1
sw	$3,136($sp)
slt	$3,$3,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L953
lw	$17,136($sp)
.set	macro
.set	reorder

lw	$8,140($sp)
lw	$10,108($sp)
lw	$5,124($sp)
addiu	$6,$8,96
lw	$9,148($sp)
addiu	$11,$10,96
move	$7,$6
$L331:
li	$2,-2147483648			# 0xffffffff80000000
addiu	$3,$2,16384
$L530:
#APP
# 2773 "vp8.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L530
#APP
# 2773 "vp8.c" 1
sync
# 0 "" 2
#NO_APP
lw	$12,%got(mb2)($28)
lw	$4,%lo(mb2)($12)
lbu	$2,9($4)
sltu	$2,$2,5
.set	noreorder
.set	nomacro
bne	$2,$0,$L533
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

lw	$13,%got(motion_dsa2)($28)
li	$2,-2147483648			# 0xffffffff80000000
ori	$2,$2,0xffff
lw	$3,%lo(motion_dsa2)($13)
$L532:
lw	$12,0($3)
bne	$12,$2,$L532
sw	$0,0($3)
li	$2,-201326592			# 0xfffffffff4000000
$L533:
lw	$3,9644($2)
beq	$3,$0,$L533
sw	$0,9644($2)
lbu	$3,9($4)
sltu	$3,$3,5
.set	noreorder
.set	nomacro
beq	$3,$0,$L534
lw	$2,80($4)
.set	macro
.set	reorder

lbu	$3,8($4)
.set	noreorder
.set	nomacro
bne	$3,$0,$L535
lw	$15,%got(mau_reg_ptr0)($28)
.set	macro
.set	reorder

lw	$14,%got(mau_reg_ptr0)($28)
li	$3,268435456			# 0x10000000
lw	$12,%lo(mau_reg_ptr0)($14)
lw	$13,0($12)
and	$13,$13,$3
$L536:
lw	$16,120($sp)
sll	$4,$2,2
sw	$0,4($12)
addu	$4,$16,$4
lw	$17,0($4)
li	$4,122			# 0x7a
.set	noreorder
.set	nomacro
beq	$17,$4,$L879
lw	$3,%got(vp8_qpar)($28)
.set	macro
.set	reorder

$L538:
addiu	$16,$2,20
addiu	$15,$2,4
addiu	$14,$2,8
addiu	$4,$2,12
sll	$16,$16,2
sll	$15,$15,2
sll	$14,$14,2
addiu	$2,$2,16
addu	$16,$3,$16
addu	$15,$3,$15
sll	$4,$4,2
addu	$14,$3,$14
lw	$16,0($16)
sll	$2,$2,2
lw	$15,0($15)
addu	$4,$3,$4
addu	$2,$3,$2
lw	$14,0($14)
sll	$16,$16,27
lw	$18,0($4)
andi	$3,$15,0x1f
lw	$19,0($2)
andi	$4,$17,0x7f
sll	$3,$3,7
andi	$14,$14,0x1f
or	$4,$16,$4
sll	$14,$14,12
andi	$2,$18,0x1f
or	$3,$4,$3
sll	$2,$2,17
andi	$4,$19,0x1f
or	$3,$3,$14
sll	$4,$4,22
or	$2,$3,$2
or	$2,$2,$4
.set	noreorder
.set	nomacro
beq	$13,$0,$L540
sw	$2,4($12)
.set	macro
.set	reorder

$L892:
lw	$17,%got(block2)($28)
li	$3,321585152			# 0x132b0000
lhu	$2,%lo(block2)($17)
or	$2,$2,$3
sw	$2,8($12)
li	$3,321585152			# 0x132b0000
$L977:
lw	$19,148($sp)
andi	$4,$12,0xffff
sw	$0,20($12)
or	$4,$4,$3
li	$2,-1289224192			# 0xffffffffb3280000
sw	$4,12($12)
addiu	$4,$2,104
#APP
# 2820 "vp8.c" 1
sw	 $19,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,108
lw	$25,140($sp)
#APP
# 2821 "vp8.c" 1
sw	 $25,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,112
#APP
# 2822 "vp8.c" 1
sw	 $6,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$6,%got(mau_reg_ptr0)($28)
lhu	$4,%lo(mau_reg_ptr0)($6)
$L830:
addiu	$6,$2,12
or	$4,$4,$3
#APP
# 2849 "vp8.c" 1
sw	 $4,0($6)	#i_sw
# 0 "" 2
#NO_APP
addiu	$3,$3,9644
addiu	$4,$2,88
#APP
# 2850 "vp8.c" 1
sw	 $3,0($4)	#i_sw
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
addiu	$2,$2,64
#APP
# 2851 "vp8.c" 1
sw	 $3,0($2)	#i_sw
# 0 "" 2
#NO_APP
li	$2,-1288962048			# 0xffffffffb32c0000
$L545:
lw	$3,18268($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L545
lw	$15,116($sp)
.set	macro
.set	reorder

li	$4,-201326592			# 0xfffffffff4000000
lw	$16,144($sp)
lw	$18,%got(mau_reg_ptr1)($28)
addiu	$3,$15,8
lw	$19,%got(mau_reg_ptr2)($28)
lw	$25,%got(block0)($28)
addiu	$6,$16,256
lw	$17,%got(mau_reg_ptr0)($28)
sw	$0,18268($2)
li	$2,-201326592			# 0xfffffffff4000000
sw	$5,9300($2)
addiu	$5,$15,128
sw	$16,9304($2)
sw	$10,9316($2)
sw	$15,9320($2)
sw	$11,9332($2)
sw	$3,9336($2)
li	$3,1			# 0x1
li	$2,-1289682944			# 0xffffffffb3210000
lw	$16,%lo(mau_reg_ptr1)($18)
lw	$18,%got(mb2)($28)
lw	$15,%lo(mau_reg_ptr2)($19)
lw	$11,%lo(block0)($25)
lw	$25,%got(mau_reg_ptr1)($28)
lw	$14,%lo(mau_reg_ptr0)($17)
lw	$10,%got(mb0)($28)
lw	$17,%got(mb1)($28)
sw	$3,4($2)
lw	$3,%got(block2)($28)
lw	$2,%got(block1)($28)
lw	$19,%got(mau_reg_ptr0)($28)
sw	$15,%lo(mau_reg_ptr1)($25)
lw	$12,%lo(block2)($3)
lw	$3,%lo(mb2)($18)
lw	$25,%got(mb1)($28)
lw	$13,%lo(block1)($2)
lw	$2,%lo(mb0)($10)
lw	$10,%lo(mb1)($17)
lw	$15,%got(mau_reg_ptr2)($28)
lw	$17,%got(block1)($28)
lw	$18,%got(block2)($28)
sw	$16,%lo(mau_reg_ptr0)($19)
sw	$3,%lo(mb1)($25)
sw	$14,%lo(mau_reg_ptr2)($15)
sw	$12,%lo(block1)($17)
sw	$11,%lo(block2)($18)
lw	$16,%got(block0)($28)
lw	$19,%got(mb0)($28)
lw	$3,%got(mb2)($28)
sw	$13,%lo(block0)($16)
sw	$10,%lo(mb0)($19)
sw	$2,%lo(mb2)($3)
$L546:
lw	$2,9644($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L546
li	$2,-1288962048			# 0xffffffffb32c0000
.set	macro
.set	reorder

sw	$0,9644($4)
$L547:
lw	$3,18268($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L547
addiu	$4,$2,18268
.set	macro
.set	reorder

lw	$10,116($sp)
li	$3,-201326592			# 0xfffffffff4000000
sw	$0,0($4)
li	$4,-1288962048			# 0xffffffffb32c0000
sw	$9,9300($3)
addiu	$2,$10,136
sw	$6,9304($3)
sw	$8,9316($3)
sw	$5,9320($3)
sw	$7,9332($3)
sw	$2,9336($3)
li	$3,1			# 0x1
li	$2,-1289682944			# 0xffffffffb3210000
sw	$3,4($2)
$L548:
lw	$2,18268($4)
beq	$2,$0,$L548
sw	$0,18268($4)
#APP
# 2914 "vp8.c" 1
mfc0	$2,$16,7
# 0 "" 2
#NO_APP
li	$3,-257			# 0xfffffffffffffeff
and	$2,$2,$3
#APP
# 2914 "vp8.c" 1
mtc0	$2,$16,7
# 0 "" 2
# 2914 "vp8.c" 1
mfc0	$2,$16,6
# 0 "" 2
#NO_APP
lw	$3,%got(p0allfrm_pmon_val)($28)
lw	$4,0($3)
addu	$2,$2,$4
sw	$2,0($3)
#APP
# 2914 "vp8.c" 1
mfc0	$3,$16,5
# 0 "" 2
#NO_APP
lw	$4,5648($fp)
move	$7,$2
lw	$16,5644($fp)
lw	$17,%got(mbmd_pmon_val)($28)
lw	$10,%got(p0allfrm_pmon_val_ex)($28)
mul	$16,$16,$4
lw	$4,%got(vpFrame)($28)
lw	$20,%got(mcpoll_pmon_val)($28)
lw	$6,0($17)
lw	$11,0($10)
lw	$8,0($4)
lw	$4,%got(p0test_pmon_val)($28)
addu	$3,$3,$11
lw	$25,%call16(printf)($28)
lw	$19,%got(gp0poll_pmon_val)($28)
lw	$18,%got(vmaupoll_pmon_val)($28)
lw	$4,0($4)
sw	$3,0($10)
teq	$16,$0,7
divu	$0,$4,$16
lw	$4,%got($LC15)($28)
addiu	$4,$4,%lo($LC15)
mflo	$5
teq	$16,$0,7
divu	$0,$2,$16
mflo	$2
teq	$8,$0,7
divu	$0,$5,$8
mflo	$5
teq	$16,$0,7
divu	$0,$6,$16
mflo	$6
teq	$8,$0,7
divu	$0,$2,$8
mflo	$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$5,0($20)
lw	$3,0($19)
lw	$2,0($18)
teq	$16,$0,7
divu	$0,$5,$16
lw	$28,32($sp)
move	$7,$3
sw	$2,20($sp)
lw	$4,%got($LC16)($28)
lw	$25,%call16(printf)($28)
addiu	$4,$4,%lo($LC16)
mflo	$6
teq	$16,$0,7
divu	$0,$3,$16
mflo	$3
teq	$16,$0,7
divu	$0,$2,$16
sw	$3,16($sp)
mflo	$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
sw	$2,24($sp)
.set	macro
.set	reorder

li	$2,-2147483648			# 0xffffffff80000000
lw	$28,32($sp)
addiu	$3,$2,16384
sw	$0,0($20)
sw	$0,0($19)
sw	$0,0($18)
lw	$4,%got(mcpoll_pmon_val_ex)($28)
sw	$0,0($17)
sw	$0,0($4)
lw	$4,%got(gp0poll_pmon_val_ex)($28)
sw	$0,0($4)
lw	$4,%got(vmaupoll_pmon_val_ex)($28)
sw	$0,0($4)
lw	$4,%got(mbmd_pmon_val_ex)($28)
sw	$0,0($4)
$L549:
#APP
# 2928 "vp8.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L549
#APP
# 2928 "vp8.c" 1
sync
# 0 "" 2
#NO_APP
lw	$2,%got(vpFrame)($28)
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L880
sw	$2,92($sp)
.set	macro
.set	reorder

lw	$10,5648($fp)
lw	$11,180($sp)
lw	$16,5644($fp)
sw	$10,72($sp)
lw	$12,72($sp)
lw	$19,4($11)
.set	noreorder
.set	nomacro
blez	$12,$L551
lw	$20,0($11)
.set	macro
.set	reorder

$L893:
lw	$17,%got(crc_code)($28)
sll	$13,$16,8
move	$22,$0
sw	$13,76($sp)
lh	$6,0($17)
$L566:
.set	noreorder
.set	nomacro
blez	$16,$L954
lw	$7,72($sp)
.set	macro
.set	reorder

lw	$14,%got(crc)($28)
move	$23,$0
move	$21,$20
addiu	$18,$14,%lo(crc)
$L567:
li	$5,256			# 0x100
move	$4,$21
move	$25,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,crc
1:	jalr	$25
addiu	$23,$23,1
.set	macro
.set	reorder

lw	$28,32($sp)
move	$6,$2
addiu	$21,$21,256
.set	noreorder
.set	nomacro
bne	$23,$16,$L567
sh	$2,0($17)
.set	macro
.set	reorder

lw	$3,76($sp)
addu	$20,$20,$3
lw	$7,72($sp)
$L954:
addiu	$22,$22,1
.set	noreorder
.set	nomacro
bne	$22,$7,$L566
addiu	$20,$20,1024
.set	macro
.set	reorder

sll	$8,$16,7
lw	$21,%got(crc)($28)
move	$20,$0
sw	$8,76($sp)
$L568:
.set	noreorder
.set	nomacro
blez	$16,$L955
lw	$7,72($sp)
.set	macro
.set	reorder

move	$23,$0
addiu	$18,$21,%lo(crc)
move	$22,$19
$L571:
li	$5,128			# 0x80
move	$4,$22
move	$25,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,crc
1:	jalr	$25
addiu	$23,$23,1
.set	macro
.set	reorder

lw	$28,32($sp)
move	$6,$2
addiu	$22,$22,128
.set	noreorder
.set	nomacro
bne	$23,$16,$L571
sh	$2,0($17)
.set	macro
.set	reorder

lw	$3,76($sp)
addu	$19,$19,$3
lw	$7,72($sp)
$L955:
addiu	$20,$20,1
.set	noreorder
.set	nomacro
bne	$20,$7,$L568
addiu	$19,$19,512
.set	macro
.set	reorder

lw	$4,%got($LC18)($28)
lw	$25,%call16(printf)($28)
lw	$5,92($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L307
lw	$28,32($sp)
.set	macro
.set	reorder

$L440:
lw	$5,%got(vp8_pred4x4_tree)($28)
addiu	$8,$fp,6736
addiu	$9,$fp,6752
addiu	$10,$5,%lo(vp8_pred4x4_tree)
$L445:
lw	$3,5624($fp)
move	$5,$0
.set	noreorder
.set	nomacro
b	$L444
lw	$7,5636($fp)
.set	macro
.set	reorder

$L882:
subu	$7,$11,$13
sw	$4,5620($fp)
addu	$5,$12,$5
sw	$7,5636($fp)
lb	$5,0($5)
blez	$5,$L881
$L444:
lw	$11,%got(vp8_pred4x4_prob_inter)($28)
$L956:
lw	$4,5620($fp)
addiu	$11,$11,%lo(vp8_pred4x4_prob_inter)
addu	$2,$23,$4
addu	$6,$11,$5
sll	$5,$5,1
lbu	$13,0($6)
addu	$12,$10,$5
lbu	$6,0($2)
sll	$4,$4,$6
addu	$3,$6,$3
addiu	$2,$4,-1
sw	$4,5620($fp)
sll	$11,$7,$6
.set	noreorder
.set	nomacro
bltz	$3,$L442
mul	$2,$2,$13
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$5,5632($fp)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L442
addiu	$7,$6,2
.set	macro
.set	reorder

sw	$7,5628($fp)
lbu	$7,1($6)
lbu	$5,0($6)
sll	$7,$7,8
or	$7,$7,$5
sll	$5,$7,8
srl	$7,$7,8
or	$7,$5,$7
andi	$7,$7,0xffff
sll	$7,$7,$3
addiu	$3,$3,-16
or	$11,$11,$7
$L442:
sra	$2,$2,8
sw	$3,5624($fp)
move	$7,$11
addiu	$2,$2,1
sll	$13,$2,16
sltu	$6,$11,$13
xori	$5,$6,0x1
.set	noreorder
.set	nomacro
bne	$5,$0,$L882
subu	$4,$4,$2
.set	macro
.set	reorder

addu	$5,$12,$5
sw	$2,5620($fp)
sw	$7,5636($fp)
lb	$5,0($5)
.set	noreorder
.set	nomacro
bgtz	$5,$L956
lw	$11,%got(vp8_pred4x4_prob_inter)($28)
.set	macro
.set	reorder

$L881:
subu	$5,$0,$5
addiu	$8,$8,1
.set	noreorder
.set	nomacro
bne	$8,$9,$L445
sb	$5,-1($8)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L904
lw	$4,%got(vp8_pred8x8c_tree)($28)
.set	macro
.set	reorder

$L864:
ori	$2,$2,0xf
.set	noreorder
.set	nomacro
b	$L492
sw	$2,24($5)
.set	macro
.set	reorder

$L872:
li	$3,65536			# 0x10000
lbu	$17,92($sp)
lw	$18,212($sp)
sll	$22,$2,12
addiu	$31,$3,160
lw	$21,%got(SblkIdx)($28)
addiu	$3,$3,80
andi	$22,$22,0xffff
sw	$17,76($sp)
addiu	$24,$15,10
move	$19,$0
li	$20,4			# 0x4
or	$25,$17,$18
or	$31,$22,$31
move	$18,$13
or	$22,$22,$3
move	$16,$15
$L503:
lw	$2,%got(BlkIdx)($28)
andi	$14,$19,0x1
addiu	$11,$18,4
sw	$25,0($18)
sll	$14,$14,1
move	$9,$0
move	$8,$0
move	$7,$0
addu	$15,$2,$19
andi	$12,$19,0x2
move	$10,$24
$L496:
addu	$17,$21,$7
lbu	$2,0($15)
andi	$4,$7,0x2
lhu	$5,0($10)
andi	$6,$7,0x1
lhu	$3,-2($10)
lbu	$17,0($17)
sra	$4,$4,1
addu	$6,$6,$14
addu	$4,$4,$12
addu	$2,$2,$17
sll	$6,$6,8
sll	$2,$2,2
sll	$4,$4,10
addiu	$2,$2,8
or	$6,$22,$6
addu	$2,$16,$2
or	$6,$6,$4
andi	$5,$5,0x7
lh	$17,2($2)
andi	$3,$3,0x7
lh	$4,0($2)
or	$3,$6,$3
sll	$5,$5,20
sll	$6,$17,16
andi	$2,$4,0xffff
or	$6,$2,$6
addiu	$7,$7,1
or	$2,$3,$5
sw	$6,0($11)
addu	$8,$8,$4
sw	$2,4($11)
addu	$9,$9,$17
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$7,$20,$L496
addiu	$11,$11,8
.set	macro
.set	reorder

lw	$2,32($18)
li	$3,16777216			# 0x1000000
or	$2,$2,$3
sw	$2,32($18)
bltz	$9,$L883
addiu	$3,$9,2
.set	noreorder
.set	nomacro
bltz	$8,$L884
sra	$3,$3,2
.set	macro
.set	reorder

$L500:
addiu	$2,$8,2
sra	$2,$2,2
$L502:
sll	$5,$12,10
andi	$6,$3,0x7
sll	$4,$14,8
or	$5,$5,$31
sll	$6,$6,20
or	$4,$5,$4
sll	$3,$3,16
andi	$5,$2,0xffff
or	$4,$4,$6
andi	$2,$2,0x7
or	$3,$5,$3
or	$2,$4,$2
addiu	$19,$19,1
sw	$3,36($18)
addiu	$24,$24,16
sw	$2,40($18)
.set	noreorder
.set	nomacro
beq	$19,$20,$L885
addiu	$3,$18,44
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L503
move	$18,$3
.set	macro
.set	reorder

$L883:
addiu	$3,$9,-2
addiu	$9,$9,1
slt	$2,$3,0
movn	$3,$9,$2
.set	noreorder
.set	nomacro
bgez	$8,$L500
sra	$3,$3,2
.set	macro
.set	reorder

$L884:
addiu	$2,$8,-2
addiu	$8,$8,1
slt	$4,$2,0
movn	$2,$8,$4
.set	noreorder
.set	nomacro
b	$L502
sra	$2,$2,2
.set	macro
.set	reorder

$L885:
lw	$4,172($13)
li	$5,33554432			# 0x2000000
lw	$6,76($sp)
lw	$8,192($sp)
or	$4,$4,$5
sw	$4,172($13)
or	$2,$6,$8
lw	$3,164($13)
or	$3,$3,$5
sw	$3,164($13)
sw	$2,176($13)
li	$2,-1610612736			# 0xffffffffa0000000
ori	$2,$2,0xffff
sw	$2,180($13)
.set	noreorder
.set	nomacro
b	$L905
lw	$15,%got(motion_dsa0)($28)
.set	macro
.set	reorder

$L370:
lbu	$2,68($sp)
addiu	$2,$2,2
.set	noreorder
.set	nomacro
b	$L369
sb	$2,68($sp)
.set	macro
.set	reorder

$L365:
sw	$2,5620($fp)
li	$5,1			# 0x1
sw	$4,5636($fp)
.set	noreorder
.set	nomacro
b	$L615
li	$3,2			# 0x2
.set	macro
.set	reorder

$L375:
lbu	$13,68($sp)
addiu	$13,$13,1
andi	$13,$13,0x00ff
.set	noreorder
.set	nomacro
b	$L374
sb	$13,68($sp)
.set	macro
.set	reorder

$L388:
lw	$8,92($sp)
li	$2,-64			# 0xffffffffffffffc0
lw	$9,72($sp)
sw	$6,5620($fp)
sll	$3,$8,6
sw	$7,5636($fp)
subu	$4,$2,$3
li	$2,5			# 0x5
slt	$5,$12,$4
sb	$2,1($9)
lw	$3,5644($fp)
.set	noreorder
.set	nomacro
bne	$5,$0,$L391
lw	$2,5648($fp)
.set	macro
.set	reorder

lw	$16,92($sp)
addiu	$3,$3,-1
subu	$3,$3,$16
sll	$3,$3,6
addiu	$3,$3,64
slt	$4,$3,$12
movz	$3,$12,$4
move	$4,$3
$L391:
lw	$17,184($sp)
lw	$18,72($sp)
slt	$3,$11,$17
.set	noreorder
.set	nomacro
bne	$3,$0,$L649
sh	$4,4($18)
.set	macro
.set	reorder

lw	$19,136($sp)
addiu	$2,$2,-1
lw	$25,72($sp)
subu	$2,$2,$19
sll	$2,$2,6
addiu	$2,$2,64
slt	$3,$2,$11
movz	$2,$11,$3
.set	noreorder
.set	nomacro
b	$L631
sh	$2,6($25)
.set	macro
.set	reorder

$L367:
move	$3,$0
addiu	$2,$2,2
.set	noreorder
.set	nomacro
b	$L366
sb	$2,68($sp)
.set	macro
.set	reorder

$L875:
li	$6,268435456			# 0x10000000
.set	noreorder
.set	nomacro
b	$L513
and	$6,$4,$6
.set	macro
.set	reorder

$L873:
lw	$10,72($sp)
li	$7,65536			# 0x10000
lw	$14,72($sp)
sll	$2,$2,12
addiu	$3,$7,2720
lbu	$15,92($sp)
lw	$17,208($sp)
andi	$2,$2,0xffff
lh	$9,22($10)
addiu	$19,$7,2208
lh	$8,20($10)
lh	$12,10($10)
andi	$16,$9,0x7
lh	$6,8($10)
andi	$4,$8,0x7
lh	$11,14($10)
or	$3,$4,$3
lh	$5,12($10)
sll	$16,$16,20
lh	$4,16($14)
lh	$10,18($10)
or	$3,$3,$2
or	$14,$15,$17
or	$3,$3,$16
sw	$14,0($13)
andi	$18,$4,0x7
sw	$3,32($13)
addiu	$14,$7,160
andi	$3,$12,0x7
lw	$22,32($13)
andi	$20,$10,0x7
addiu	$16,$7,672
sll	$21,$3,20
andi	$7,$6,0x7
or	$18,$18,$19
sll	$20,$20,20
andi	$3,$5,0x7
or	$18,$18,$2
or	$14,$7,$14
or	$18,$18,$20
or	$16,$3,$16
or	$14,$14,$21
sw	$18,24($13)
or	$16,$16,$2
lw	$18,192($sp)
or	$14,$14,$2
li	$2,33554432			# 0x2000000
andi	$17,$11,0x7
sw	$14,8($13)
or	$22,$22,$2
sll	$12,$12,16
sll	$11,$11,16
sw	$22,32($13)
sll	$17,$17,20
sll	$10,$10,16
sll	$9,$9,16
li	$2,-1610612736			# 0xffffffffa0000000
andi	$6,$6,0xffff
andi	$5,$5,0xffff
andi	$4,$4,0xffff
andi	$8,$8,0xffff
or	$6,$6,$12
or	$5,$5,$11
or	$16,$16,$17
sw	$6,4($13)
or	$4,$4,$10
sw	$5,12($13)
or	$8,$8,$9
sw	$16,16($13)
or	$15,$18,$15
sw	$4,20($13)
ori	$2,$2,0xffff
sw	$8,28($13)
sw	$15,36($13)
sw	$2,40($13)
.set	noreorder
.set	nomacro
b	$L905
lw	$15,%got(motion_dsa0)($28)
.set	macro
.set	reorder

$L874:
lw	$19,72($sp)
li	$9,65536			# 0x10000
sll	$2,$2,12
lw	$25,196($sp)
addiu	$5,$9,736
andi	$2,$2,0xffff
lh	$6,12($19)
lh	$8,14($19)
lh	$10,10($19)
andi	$3,$6,0x7
lh	$7,8($19)
or	$3,$3,$5
lbu	$5,92($sp)
andi	$4,$8,0x7
sll	$4,$4,20
or	$3,$3,$2
or	$11,$5,$25
or	$4,$3,$4
sw	$11,0($13)
addiu	$3,$9,224
andi	$11,$10,0x7
sw	$4,16($13)
sll	$11,$11,20
lw	$9,16($13)
andi	$4,$7,0x7
sll	$10,$10,16
or	$3,$4,$3
sll	$8,$8,16
andi	$4,$7,0xffff
or	$3,$3,$11
or	$4,$4,$10
andi	$6,$6,0xffff
or	$2,$3,$2
sw	$4,4($13)
or	$3,$6,$8
lw	$8,192($sp)
li	$4,33554432			# 0x2000000
sw	$2,8($13)
or	$9,$9,$4
.set	noreorder
.set	nomacro
b	$L829
or	$5,$8,$5
.set	macro
.set	reorder

$L393:
lw	$8,92($sp)
li	$2,-64			# 0xffffffffffffffc0
lw	$11,72($sp)
sw	$5,5636($fp)
sll	$3,$8,6
sw	$6,5620($fp)
subu	$4,$2,$3
li	$2,6			# 0x6
slt	$5,$10,$4
sb	$2,1($11)
lw	$3,5644($fp)
.set	noreorder
.set	nomacro
bne	$5,$0,$L396
lw	$2,5648($fp)
.set	macro
.set	reorder

lw	$11,92($sp)
addiu	$3,$3,-1
subu	$3,$3,$11
sll	$3,$3,6
addiu	$3,$3,64
slt	$4,$10,$3
movz	$10,$3,$4
move	$4,$10
$L396:
lw	$12,184($sp)
lw	$13,72($sp)
slt	$3,$9,$12
.set	noreorder
.set	nomacro
bne	$3,$0,$L648
sh	$4,4($13)
.set	macro
.set	reorder

lw	$14,136($sp)
addiu	$2,$2,-1
lw	$15,72($sp)
subu	$2,$2,$14
sll	$2,$2,6
addiu	$2,$2,64
slt	$3,$9,$2
movn	$2,$9,$3
.set	noreorder
.set	nomacro
b	$L631
sh	$2,6($15)
.set	macro
.set	reorder

$L649:
lw	$2,184($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
b	$L631
sh	$2,6($25)
.set	macro
.set	reorder

$L648:
lw	$2,184($sp)
lw	$15,72($sp)
.set	noreorder
.set	nomacro
b	$L631
sh	$2,6($15)
.set	macro
.set	reorder

$L398:
lw	$10,92($sp)
li	$2,-64			# 0xffffffffffffffc0
lw	$11,72($sp)
sll	$6,$10,6
lh	$5,4($11)
sw	$4,5620($fp)
subu	$4,$2,$6
li	$2,8			# 0x8
sw	$3,5636($fp)
slt	$6,$5,$4
sb	$2,1($11)
lw	$3,5644($fp)
.set	noreorder
.set	nomacro
bne	$6,$0,$L401
lw	$2,5648($fp)
.set	macro
.set	reorder

lw	$8,92($sp)
addiu	$3,$3,-1
subu	$3,$3,$8
sll	$3,$3,6
addiu	$3,$3,64
slt	$4,$5,$3
movz	$5,$3,$4
move	$4,$5
$L401:
lw	$9,72($sp)
lw	$10,184($sp)
sh	$4,4($9)
lh	$5,6($9)
slt	$3,$5,$10
.set	noreorder
.set	nomacro
bne	$3,$0,$L647
lw	$11,136($sp)
.set	macro
.set	reorder

addiu	$2,$2,-1
subu	$2,$2,$11
sll	$2,$2,6
addiu	$2,$2,64
slt	$3,$2,$5
movz	$2,$5,$3
$L433:
lw	$16,%got(read_mv_component)($28)
addiu	$5,$fp,9142
lw	$12,72($sp)
lw	$4,168($sp)
addiu	$16,$16,%lo(read_mv_component)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,read_mv_component
1:	jalr	$25
sh	$2,6($12)
.set	macro
.set	reorder

addiu	$5,$fp,9161
lw	$8,72($sp)
move	$25,$16
lw	$4,168($sp)
lhu	$3,6($8)
addu	$2,$2,$3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,read_mv_component
1:	jalr	$25
sh	$2,6($8)
.set	macro
.set	reorder

lw	$8,72($sp)
lw	$28,32($sp)
lhu	$3,4($8)
lbu	$4,1($8)
addu	$2,$2,$3
sh	$2,4($8)
$L414:
li	$2,9			# 0x9
.set	noreorder
.set	nomacro
beq	$4,$2,$L957
lw	$4,%got(block0)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L906
lw	$3,72($sp)
.set	macro
.set	reorder

$L405:
sw	$2,5620($fp)
li	$6,3			# 0x3
li	$2,3			# 0x3
sw	$5,5636($fp)
$L408:
lw	$17,72($sp)
sll	$16,$6,4
addu	$20,$20,$16
sb	$2,3($17)
lw	$2,%got(vp8_mbsplit_count)($28)
addiu	$2,$2,%lo(vp8_mbsplit_count)
addu	$3,$6,$2
lw	$2,%got(vp8_mbfirstidx)($28)
lbu	$21,0($3)
addiu	$2,$2,%lo(vp8_mbfirstidx)
.set	noreorder
.set	nomacro
beq	$21,$0,$L432
addu	$16,$2,$16
.set	macro
.set	reorder

lw	$22,%got(vp8_submv_prob)($28)
addu	$19,$16,$21
move	$17,$18
lw	$13,%got(vp8_submv_prob+6)($28)
move	$7,$21
lw	$14,%got(read_mv_component)($28)
move	$21,$18
lw	$18,72($sp)
addiu	$15,$fp,9142
addiu	$22,$22,%lo(vp8_submv_prob)
$L431:
lbu	$2,0($16)
andi	$3,$2,0x3
.set	noreorder
.set	nomacro
bne	$3,$0,$L415
addu	$3,$20,$2
.set	macro
.set	reorder

addu	$3,$12,$2
lbu	$3,3($3)
sll	$3,$3,2
addu	$3,$18,$3
lw	$9,-64($3)
slt	$3,$2,4
beq	$3,$0,$L417
$L887:
addu	$2,$11,$2
lbu	$2,12($2)
sll	$2,$2,2
addu	$2,$18,$2
lw	$8,152($2)
.set	noreorder
.set	nomacro
beq	$9,$8,$L958
li	$3,4			# 0x4
.set	macro
.set	reorder

$L419:
.set	noreorder
.set	nomacro
beq	$8,$0,$L646
li	$10,179			# 0xb3
.set	macro
.set	reorder

sltu	$5,$9,1
sll	$2,$5,1
addu	$5,$2,$5
addu	$5,$22,$5
lbu	$10,0($5)
$L421:
lw	$2,5620($fp)
lw	$6,5624($fp)
lw	$3,5636($fp)
addu	$4,$23,$2
lbu	$4,0($4)
sll	$2,$2,$4
addu	$6,$4,$6
sll	$4,$3,$4
.set	noreorder
.set	nomacro
bltz	$6,$L422
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$24,5628($fp)
lw	$3,5632($fp)
sltu	$3,$24,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L959
addiu	$3,$2,-1
.set	macro
.set	reorder

addiu	$3,$24,2
sw	$3,5628($fp)
lbu	$3,1($24)
lbu	$24,0($24)
sll	$3,$3,8
or	$3,$3,$24
sll	$24,$3,8
srl	$3,$3,8
or	$3,$24,$3
andi	$3,$3,0xffff
sll	$3,$3,$6
addiu	$6,$6,-16
or	$4,$4,$3
$L422:
addiu	$3,$2,-1
$L959:
mul	$3,$3,$10
sra	$3,$3,8
addiu	$3,$3,1
sll	$10,$3,16
sltu	$24,$4,$10
.set	noreorder
.set	nomacro
bne	$24,$0,$L423
sw	$6,5624($fp)
.set	macro
.set	reorder

subu	$2,$2,$3
subu	$4,$4,$10
addu	$3,$23,$2
sw	$2,5620($fp)
sw	$4,5636($fp)
lbu	$3,0($3)
lbu	$9,1($5)
sll	$2,$2,$3
addu	$6,$6,$3
sll	$4,$4,$3
.set	noreorder
.set	nomacro
bltz	$6,$L425
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$10,5628($fp)
lw	$3,5632($fp)
sltu	$3,$10,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L960
addiu	$3,$2,-1
.set	macro
.set	reorder

addiu	$3,$10,2
sw	$3,5628($fp)
lbu	$3,1($10)
lbu	$10,0($10)
sll	$3,$3,8
or	$3,$3,$10
sll	$10,$3,8
srl	$3,$3,8
or	$3,$10,$3
andi	$3,$3,0xffff
sll	$3,$3,$6
addiu	$6,$6,-16
or	$4,$4,$3
$L425:
addiu	$3,$2,-1
$L960:
mul	$3,$9,$3
sra	$3,$3,8
addiu	$3,$3,1
sll	$9,$3,16
sltu	$10,$4,$9
.set	noreorder
.set	nomacro
bne	$10,$0,$L427
sw	$6,5624($fp)
.set	macro
.set	reorder

subu	$2,$2,$3
subu	$4,$4,$9
addu	$3,$23,$2
sw	$2,5620($fp)
sw	$4,5636($fp)
lbu	$3,0($3)
lbu	$5,2($5)
sll	$2,$2,$3
addu	$6,$6,$3
sll	$3,$4,$3
.set	noreorder
.set	nomacro
bltz	$6,$L429
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$8,5628($fp)
lw	$4,5632($fp)
sltu	$4,$8,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L961
addiu	$4,$2,-1
.set	macro
.set	reorder

addiu	$4,$8,2
sw	$4,5628($fp)
lbu	$4,1($8)
lbu	$8,0($8)
sll	$4,$4,8
or	$4,$4,$8
sll	$8,$4,8
srl	$4,$4,8
or	$4,$8,$4
andi	$4,$4,0xffff
sll	$4,$4,$6
addiu	$6,$6,-16
or	$3,$3,$4
$L429:
addiu	$4,$2,-1
$L961:
sw	$6,5624($fp)
mul	$4,$5,$4
sra	$4,$4,8
addiu	$4,$4,1
sll	$5,$4,16
sltu	$6,$3,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L430
addiu	$25,$14,%lo(read_mv_component)
.set	macro
.set	reorder

lhu	$6,6($18)
subu	$3,$3,$5
subu	$2,$2,$4
lw	$4,168($sp)
move	$5,$15
sw	$3,5636($fp)
sw	$2,5620($fp)
sw	$7,240($sp)
sw	$11,236($sp)
sw	$12,232($sp)
sw	$13,224($sp)
sw	$14,228($sp)
sw	$15,244($sp)
sw	$6,216($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,read_mv_component
1:	jalr	$25
sw	$25,220($sp)
.set	macro
.set	reorder

addiu	$5,$fp,9161
lw	$6,216($sp)
lw	$4,168($sp)
lw	$25,220($sp)
addu	$2,$6,$2
sh	$2,2($17)
lhu	$3,4($18)
.set	noreorder
.set	nomacro
jalr	$25
sw	$3,216($sp)
.set	macro
.set	reorder

lw	$3,216($sp)
lw	$7,240($sp)
lw	$11,236($sp)
addu	$2,$3,$2
lw	$12,232($sp)
lw	$13,224($sp)
lw	$14,228($sp)
lw	$15,244($sp)
lw	$28,32($sp)
sh	$2,0($17)
$L426:
addiu	$16,$16,1
.set	noreorder
.set	nomacro
bne	$16,$19,$L431
addiu	$17,$17,4
.set	macro
.set	reorder

move	$21,$7
$L432:
addiu	$2,$21,1
lw	$18,72($sp)
sll	$2,$2,2
addu	$2,$18,$2
lbu	$4,1($18)
lw	$2,0($2)
.set	noreorder
.set	nomacro
b	$L414
sw	$2,4($18)
.set	macro
.set	reorder

$L423:
sw	$3,5620($fp)
sw	$4,5636($fp)
.set	noreorder
.set	nomacro
b	$L426
sw	$9,0($17)
.set	macro
.set	reorder

$L415:
lbu	$3,-1($3)
sll	$3,$3,2
addu	$3,$21,$3
lw	$9,0($3)
slt	$3,$2,4
bne	$3,$0,$L887
$L417:
addu	$2,$20,$2
lbu	$2,-4($2)
sll	$2,$2,2
addu	$2,$21,$2
lw	$8,0($2)
.set	noreorder
.set	nomacro
bne	$9,$8,$L419
li	$3,4			# 0x4
.set	macro
.set	reorder

$L958:
li	$2,3			# 0x3
movz	$2,$3,$9
sll	$5,$2,1
addu	$2,$5,$2
addu	$5,$22,$2
.set	noreorder
.set	nomacro
b	$L421
lbu	$10,0($5)
.set	macro
.set	reorder

$L646:
.set	noreorder
.set	nomacro
b	$L421
addiu	$5,$13,%lo(vp8_submv_prob+6)
.set	macro
.set	reorder

$L427:
sw	$3,5620($fp)
sw	$4,5636($fp)
.set	noreorder
.set	nomacro
b	$L426
sw	$8,0($17)
.set	macro
.set	reorder

$L430:
sw	$4,5620($fp)
sw	$3,5636($fp)
.set	noreorder
.set	nomacro
b	$L426
sw	$0,0($17)
.set	macro
.set	reorder

$L644:
.set	noreorder
.set	nomacro
b	$L403
lw	$2,184($sp)
.set	macro
.set	reorder

$L223:
lw	$18,%got(vp8_rac_get_uint)($28)
move	$8,$6
sw	$6,5636($fp)
sw	$0,6792($fp)
.set	noreorder
.set	nomacro
b	$L824
sw	$0,6800($fp)
.set	macro
.set	reorder

$L239:
sw	$2,5620($fp)
sw	$5,5636($fp)
sw	$0,6872($fp)
$L595:
lw	$4,168($sp)
$L900:
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
li	$5,2			# 0x2
.set	macro
.set	reorder

li	$3,1			# 0x1
lw	$28,32($sp)
sll	$2,$3,$2
subu	$3,$3,$2
addiu	$4,$2,-1
sw	$2,5688($fp)
sll	$2,$3,1
sll	$21,$4,1
addu	$3,$2,$3
addu	$21,$21,$4
addu	$20,$20,$3
.set	noreorder
.set	nomacro
bltz	$20,$L244
addu	$21,$16,$21
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$4,$L635
move	$22,$0
.set	macro
.set	reorder

lbu	$2,2($16)
lbu	$3,1($16)
lbu	$19,0($16)
sll	$2,$2,16
sll	$3,$3,8
or	$2,$2,$3
or	$19,$2,$19
subu	$20,$20,$19
.set	noreorder
.set	nomacro
bltz	$20,$L244
addiu	$23,$fp,5692
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L246
addiu	$16,$16,5
.set	macro
.set	reorder

$L247:
lbu	$2,0($16)
addiu	$16,$16,3
sll	$2,$2,16
lbu	$3,-4($16)
lbu	$19,-5($16)
sll	$3,$3,8
or	$2,$2,$3
or	$19,$2,$19
subu	$20,$20,$19
.set	noreorder
.set	nomacro
bltz	$20,$L962
lw	$6,%got($LC20)($28)
.set	macro
.set	reorder

$L246:
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
move	$4,$23
move	$5,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
move	$6,$19
.set	macro
.set	reorder

addiu	$22,$22,1
lw	$2,5688($fp)
addu	$21,$21,$19
lw	$28,32($sp)
addiu	$2,$2,-1
slt	$2,$22,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L247
addiu	$23,$23,20
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L907
sll	$4,$22,2
.set	macro
.set	reorder

$L656:
$L934:
.set	noreorder
.set	nomacro
b	$L588
li	$2,48			# 0x30
.set	macro
.set	reorder

$L279:
sw	$2,5620($fp)
move	$11,$3
.set	noreorder
.set	nomacro
b	$L281
sw	$3,5636($fp)
.set	macro
.set	reorder

$L273:
lbu	$11,0($3)
sll	$6,$6,$11
addu	$10,$10,$11
sll	$11,$2,$11
.set	noreorder
.set	nomacro
bltz	$10,$L275
sw	$6,5620($fp)
.set	macro
.set	reorder

lw	$2,5628($fp)
lw	$3,5632($fp)
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L275
addiu	$3,$2,2
.set	macro
.set	reorder

sw	$3,5628($fp)
lbu	$3,1($2)
lbu	$2,0($2)
sll	$3,$3,8
or	$3,$3,$2
sll	$2,$3,8
srl	$3,$3,8
or	$2,$2,$3
andi	$2,$2,0xffff
sll	$2,$2,$10
addiu	$10,$10,-16
or	$11,$11,$2
$L275:
addiu	$2,$6,-1
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$4,$2,16
sltu	$3,$11,$4
xori	$3,$3,0x1
.set	noreorder
.set	nomacro
beq	$3,$0,$L825
sw	$10,5624($fp)
.set	macro
.set	reorder

subu	$2,$6,$2
subu	$11,$11,$4
$L825:
sw	$2,5620($fp)
.set	noreorder
.set	nomacro
b	$L274
sw	$11,5636($fp)
.set	macro
.set	reorder

$L237:
sw	$2,5620($fp)
.set	noreorder
.set	nomacro
b	$L594
move	$3,$8
.set	macro
.set	reorder

$L290:
lw	$2,5660($fp)
sw	$3,5620($fp)
sw	$11,5636($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L291
sw	$0,6760($fp)
.set	macro
.set	reorder

$L857:
lw	$4,168($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
li	$5,8			# 0x8
.set	macro
.set	reorder

li	$5,8			# 0x8
lw	$4,168($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
sb	$2,6888($fp)
.set	macro
.set	reorder

li	$5,8			# 0x8
lw	$4,168($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
sb	$2,6889($fp)
.set	macro
.set	reorder

lw	$6,5620($fp)
lw	$3,5624($fp)
lw	$5,5636($fp)
addu	$4,$17,$6
lw	$28,32($sp)
sb	$2,6890($fp)
lbu	$4,0($4)
sll	$6,$6,$4
addu	$3,$4,$3
sll	$4,$5,$4
.set	noreorder
.set	nomacro
bltz	$3,$L292
sw	$6,5620($fp)
.set	macro
.set	reorder

lw	$5,5628($fp)
lw	$2,5632($fp)
sltu	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L963
addiu	$2,$6,-1
.set	macro
.set	reorder

addiu	$2,$5,2
sw	$2,5628($fp)
lbu	$2,1($5)
lbu	$5,0($5)
sll	$2,$2,8
or	$2,$2,$5
sll	$5,$2,8
srl	$2,$2,8
or	$2,$5,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$4,$4,$2
$L292:
addiu	$2,$6,-1
$L963:
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$7,$4,$5
.set	noreorder
.set	nomacro
bne	$7,$0,$L293
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$2,$6,$2
subu	$4,$4,$5
addiu	$16,$fp,6891
addiu	$19,$fp,6895
sw	$2,5620($fp)
sw	$4,5636($fp)
$L294:
lw	$4,168($sp)
li	$5,8			# 0x8
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
addiu	$16,$16,1
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
bne	$16,$19,$L294
sb	$2,-1($16)
.set	macro
.set	reorder

lw	$2,5620($fp)
lw	$3,5624($fp)
lw	$4,5636($fp)
$L602:
addu	$5,$17,$2
lbu	$5,0($5)
sll	$2,$2,$5
addu	$6,$5,$3
sll	$4,$4,$5
.set	noreorder
.set	nomacro
bltz	$6,$L295
sw	$2,5620($fp)
.set	macro
.set	reorder

lw	$3,5628($fp)
lw	$5,5632($fp)
sltu	$5,$3,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L295
addiu	$5,$3,2
.set	macro
.set	reorder

sw	$5,5628($fp)
lbu	$5,1($3)
lbu	$3,0($3)
sll	$5,$5,8
or	$5,$5,$3
sll	$3,$5,8
srl	$5,$5,8
or	$3,$3,$5
andi	$3,$3,0xffff
sll	$3,$3,$6
addiu	$6,$6,-16
or	$4,$4,$3
$L295:
addiu	$3,$2,-1
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$5,$3,16
sltu	$7,$4,$5
.set	noreorder
.set	nomacro
bne	$7,$0,$L296
sw	$6,5624($fp)
.set	macro
.set	reorder

subu	$2,$2,$3
subu	$4,$4,$5
addiu	$16,$fp,6895
addiu	$19,$fp,6898
sw	$2,5620($fp)
sw	$4,5636($fp)
$L297:
lw	$4,168($sp)
li	$5,8			# 0x8
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
addiu	$16,$16,1
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
bne	$16,$19,$L297
sb	$2,-1($16)
.set	macro
.set	reorder

lw	$3,5620($fp)
lw	$2,5624($fp)
lw	$9,5636($fp)
$L603:
lw	$22,%got(vp8_mv_update_prob)($28)
move	$21,$0
lw	$18,168($sp)
li	$23,19			# 0x13
li	$16,38			# 0x26
addiu	$22,$22,%lo(vp8_mv_update_prob)
$L298:
.set	noreorder
.set	nomacro
b	$L304
move	$19,$0
.set	macro
.set	reorder

$L889:
lw	$25,72($sp)
addu	$20,$fp,$20
sw	$3,5620($fp)
addiu	$19,$19,1
.set	noreorder
.set	nomacro
jalr	$25
sw	$8,5636($fp)
.set	macro
.set	reorder

sll	$2,$2,1
lw	$28,32($sp)
sltu	$3,$2,1
addu	$2,$2,$3
.set	noreorder
.set	nomacro
beq	$19,$23,$L888
sb	$2,9142($20)
.set	macro
.set	reorder

$L302:
lw	$3,5620($fp)
lw	$2,5624($fp)
lw	$9,5636($fp)
$L304:
addu	$6,$17,$3
addu	$20,$19,$21
li	$5,7			# 0x7
lbu	$6,0($6)
addu	$8,$22,$20
move	$4,$18
sll	$3,$3,$6
addu	$7,$6,$2
lbu	$2,0($8)
addiu	$10,$3,-1
sll	$6,$9,$6
.set	noreorder
.set	nomacro
bltz	$7,$L299
sw	$3,5620($fp)
.set	macro
.set	reorder

lw	$9,5628($fp)
lw	$8,5632($fp)
sltu	$8,$9,$8
.set	noreorder
.set	nomacro
beq	$8,$0,$L299
addiu	$11,$9,2
.set	macro
.set	reorder

sw	$11,5628($fp)
lbu	$8,1($9)
lbu	$9,0($9)
sll	$8,$8,8
or	$8,$8,$9
sll	$9,$8,8
srl	$8,$8,8
or	$8,$9,$8
andi	$8,$8,0xffff
sll	$8,$8,$7
addiu	$7,$7,-16
or	$6,$6,$8
$L299:
mul	$2,$2,$10
sw	$7,5624($fp)
sra	$2,$2,8
addiu	$2,$2,1
sll	$7,$2,16
subu	$8,$6,$7
sltu	$7,$6,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L889
subu	$3,$3,$2
.set	macro
.set	reorder

addiu	$19,$19,1
sw	$2,5620($fp)
.set	noreorder
.set	nomacro
bne	$19,$23,$L302
sw	$6,5636($fp)
.set	macro
.set	reorder

$L888:
addiu	$21,$21,19
beq	$21,$16,$L303
lw	$3,5620($fp)
lw	$2,5624($fp)
.set	noreorder
.set	nomacro
b	$L298
lw	$9,5636($fp)
.set	macro
.set	reorder

$L272:
addiu	$3,$fp,6884
sw	$6,5620($fp)
addiu	$5,$fp,9180
sw	$2,5636($fp)
addiu	$4,$fp,9172
sw	$0,5684($fp)
$L598:
lw	$11,0($3)
addiu	$3,$3,16
addiu	$5,$5,16
lw	$9,-12($3)
lw	$8,-8($3)
lw	$7,-4($3)
sw	$11,-16($5)
sw	$9,-12($5)
sw	$8,-8($5)
.set	noreorder
.set	nomacro
bne	$3,$4,$L598
sw	$7,-4($5)
.set	macro
.set	reorder

lw	$4,0($3)
lw	$3,4($3)
sw	$4,0($5)
.set	noreorder
.set	nomacro
b	$L599
sw	$3,4($5)
.set	macro
.set	reorder

$L253:
lw	$4,5620($fp)
lw	$3,5624($fp)
lw	$5,5636($fp)
addu	$2,$17,$4
lbu	$2,0($2)
sll	$4,$4,$2
addu	$3,$2,$3
sll	$5,$5,$2
.set	noreorder
.set	nomacro
bltz	$3,$L255
sw	$4,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$2,5632($fp)
sltu	$2,$6,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L964
addiu	$2,$4,-1
.set	macro
.set	reorder

addiu	$2,$6,2
sw	$2,5628($fp)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$2,$2,$6
sll	$6,$2,8
srl	$2,$2,8
or	$2,$6,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$5,$5,$2
$L255:
addiu	$2,$4,-1
$L964:
sll	$2,$2,7
sra	$2,$2,8
addiu	$2,$2,1
sll	$6,$2,16
sltu	$7,$5,$6
xori	$7,$7,0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L256
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$2,$4,$2
subu	$5,$5,$6
$L256:
addu	$4,$17,$2
sw	$5,5636($fp)
lbu	$4,0($4)
sll	$8,$2,$4
addu	$3,$3,$4
sll	$5,$5,$4
.set	noreorder
.set	nomacro
bltz	$3,$L257
sw	$8,5620($fp)
.set	macro
.set	reorder

lw	$2,5628($fp)
lw	$4,5632($fp)
sltu	$4,$2,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L965
addiu	$4,$8,-1
.set	macro
.set	reorder

addiu	$4,$2,2
sw	$4,5628($fp)
lbu	$6,1($2)
lbu	$2,0($2)
sll	$6,$6,8
or	$6,$6,$2
sll	$2,$6,8
srl	$6,$6,8
or	$2,$2,$6
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$5,$5,$2
$L257:
addiu	$4,$8,-1
$L965:
sll	$4,$4,7
sra	$4,$4,8
addiu	$4,$4,1
sll	$2,$4,16
sltu	$16,$5,$2
xori	$16,$16,0x1
.set	noreorder
.set	nomacro
beq	$16,$0,$L258
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$4,$8,$4
subu	$2,$5,$2
sw	$4,5620($fp)
.set	noreorder
.set	nomacro
beq	$7,$0,$L890
sw	$2,5636($fp)
.set	macro
.set	reorder

sw	$0,5672($fp)
.set	noreorder
.set	nomacro
b	$L263
move	$5,$0
.set	macro
.set	reorder

$L293:
.set	noreorder
.set	nomacro
b	$L602
sw	$4,5636($fp)
.set	macro
.set	reorder

$L244:
lw	$6,%got($LC20)($28)
$L962:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC20)
.set	macro
.set	reorder

lw	$31,284($sp)
$L899:
li	$2,-22			# 0xffffffffffffffea
lw	$fp,280($sp)
lw	$23,276($sp)
lw	$22,272($sp)
lw	$21,268($sp)
lw	$20,264($sp)
lw	$19,260($sp)
lw	$18,256($sp)
lw	$17,252($sp)
lw	$16,248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,288
.set	macro
.set	reorder

$L241:
sw	$2,5620($fp)
.set	noreorder
.set	nomacro
b	$L595
sw	$5,5636($fp)
.set	macro
.set	reorder

$L296:
sw	$3,5620($fp)
move	$9,$4
sw	$4,5636($fp)
.set	noreorder
.set	nomacro
b	$L603
move	$2,$6
.set	macro
.set	reorder

$L647:
.set	noreorder
.set	nomacro
b	$L433
lw	$2,184($sp)
.set	macro
.set	reorder

$L535:
li	$13,2			# 0x2
lw	$12,%lo(mau_reg_ptr0)($15)
lw	$4,0($12)
srl	$3,$4,24
andi	$3,$3,0x3
.set	noreorder
.set	nomacro
beq	$3,$13,$L891
lw	$16,120($sp)
.set	macro
.set	reorder

sll	$4,$2,2
li	$3,788529152			# 0x2f000000
sw	$0,4($12)
move	$13,$0
addu	$4,$16,$4
sw	$3,0($12)
li	$3,65536			# 0x10000
lw	$17,0($4)
li	$4,122			# 0x7a
sw	$3,16($12)
.set	noreorder
.set	nomacro
bne	$17,$4,$L538
lw	$3,%got(vp8_qpar)($28)
.set	macro
.set	reorder

$L879:
addiu	$4,$2,20
addiu	$16,$2,4
addiu	$15,$2,8
sll	$4,$4,2
sll	$16,$16,2
addiu	$17,$2,12
addiu	$14,$2,16
sll	$15,$15,2
addu	$4,$3,$4
addu	$16,$3,$16
sll	$2,$17,2
addu	$15,$3,$15
lw	$4,0($4)
sll	$14,$14,2
lw	$16,0($16)
addu	$2,$3,$2
addu	$14,$3,$14
lw	$15,0($15)
addiu	$4,$4,1
lw	$2,0($2)
addiu	$3,$16,1
lw	$14,0($14)
addiu	$15,$15,1
sll	$4,$4,27
andi	$3,$3,0x1f
addiu	$2,$2,1
sll	$3,$3,7
andi	$15,$15,0x1f
addiu	$14,$14,1
ori	$4,$4,0x79
sll	$15,$15,12
andi	$2,$2,0x1f
or	$3,$4,$3
sll	$2,$2,17
andi	$4,$14,0x1f
or	$3,$3,$15
sll	$4,$4,22
or	$2,$3,$2
or	$2,$2,$4
.set	noreorder
.set	nomacro
bne	$13,$0,$L892
sw	$2,4($12)
.set	macro
.set	reorder

$L540:
lw	$18,%got(block2)($28)
li	$3,321585152			# 0x132b0000
lw	$2,%lo(block2)($18)
addiu	$2,$2,32
andi	$2,$2,0xffff
or	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L977
sw	$2,8($12)
.set	macro
.set	reorder

$L534:
addiu	$17,$2,20
lw	$18,120($sp)
addiu	$3,$2,4
lw	$13,%got(mau_reg_ptr0)($28)
addiu	$14,$2,8
addiu	$16,$2,12
sll	$17,$17,2
sll	$3,$3,2
lw	$12,%lo(mau_reg_ptr0)($13)
sll	$4,$2,2
sll	$14,$14,2
addiu	$15,$2,16
addu	$3,$18,$3
lw	$13,0($12)
addu	$2,$18,$17
sll	$16,$16,2
addu	$4,$18,$4
lw	$3,0($3)
addu	$14,$18,$14
lw	$2,0($2)
sll	$15,$15,2
addu	$16,$18,$16
lw	$4,0($4)
lw	$14,0($14)
addu	$15,$18,$15
sll	$2,$2,27
lw	$16,0($16)
andi	$3,$3,0x1f
lw	$15,0($15)
andi	$4,$4,0x7f
sll	$3,$3,7
andi	$14,$14,0x1f
or	$4,$2,$4
sll	$14,$14,12
andi	$2,$16,0x1f
or	$3,$4,$3
sll	$2,$2,17
or	$3,$3,$14
andi	$4,$15,0x1f
sll	$4,$4,22
or	$2,$3,$2
li	$3,620691456			# 0x24ff0000
or	$2,$2,$4
ori	$3,$3,0xffff
li	$4,268435456			# 0x10000000
sw	$2,4($12)
or	$3,$13,$3
and	$13,$13,$4
.set	noreorder
.set	nomacro
beq	$13,$0,$L543
sw	$3,0($12)
.set	macro
.set	reorder

lw	$19,%got(block2)($28)
li	$3,321585152			# 0x132b0000
lhu	$2,%lo(block2)($19)
or	$2,$2,$3
li	$3,65536			# 0x10000
sw	$2,8($12)
addiu	$3,$3,800
sw	$3,16($12)
li	$3,321585152			# 0x132b0000
$L978:
sw	$0,24($12)
andi	$4,$12,0xffff
sw	$0,28($12)
or	$4,$4,$3
sw	$0,32($12)
li	$2,-1289224192			# 0xffffffffb3280000
sw	$4,12($12)
addiu	$4,$2,104
lw	$12,148($sp)
#APP
# 2845 "vp8.c" 1
sw	 $12,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,108
lw	$13,140($sp)
#APP
# 2846 "vp8.c" 1
sw	 $13,0($4)	#i_sw
# 0 "" 2
#NO_APP
addiu	$4,$2,112
#APP
# 2847 "vp8.c" 1
sw	 $6,0($4)	#i_sw
# 0 "" 2
#NO_APP
lw	$14,%got(mau_reg_ptr0)($28)
.set	noreorder
.set	nomacro
b	$L830
lhu	$4,%lo(mau_reg_ptr0)($14)
.set	macro
.set	reorder

$L843:
.set	noreorder
.set	nomacro
b	$L581
lw	$2,5672($fp)
.set	macro
.set	reorder

$L842:
lw	$2,5672($fp)
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
bne	$2,$4,$L966
addiu	$3,$3,1400
.set	macro
.set	reorder

lw	$3,5612($fp)
lw	$2,5608($fp)
sw	$3,5608($fp)
.set	noreorder
.set	nomacro
b	$L579
sw	$2,5612($fp)
.set	macro
.set	reorder

$L543:
lw	$25,%got(block2)($28)
li	$3,65536			# 0x10000
addiu	$3,$3,768
lw	$2,%lo(block2)($25)
sw	$3,16($12)
li	$3,321585152			# 0x132b0000
addiu	$2,$2,32
andi	$2,$2,0xffff
or	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L978
sw	$2,8($12)
.set	macro
.set	reorder

$L880:
lw	$8,180($sp)
lw	$3,5648($fp)
lw	$16,5644($fp)
lw	$9,4($8)
lw	$21,0($8)
sw	$3,72($sp)
.set	noreorder
.set	nomacro
blez	$3,$L551
sw	$9,76($sp)
.set	macro
.set	reorder

lw	$20,%got($LC17)($28)
sll	$23,$16,8
move	$18,$0
addiu	$20,$20,%lo($LC17)
$L552:
.set	noreorder
.set	nomacro
blez	$16,$L556
lw	$10,%got(ptr_square.constprop.8)($28)
.set	macro
.set	reorder

move	$17,$0
move	$19,$21
addiu	$22,$10,%lo(ptr_square.constprop.8)
$L553:
lw	$25,%call16(printf)($28)
move	$5,$17
move	$4,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

addiu	$17,$17,1
li	$5,16			# 0x10
li	$6,16			# 0x10
move	$25,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ptr_square.constprop.8
1:	jalr	$25
move	$4,$19
.set	macro
.set	reorder

addiu	$19,$19,256
.set	noreorder
.set	nomacro
bne	$17,$16,$L553
lw	$28,32($sp)
.set	macro
.set	reorder

addu	$21,$21,$23
$L556:
lw	$3,72($sp)
addiu	$18,$18,1
.set	noreorder
.set	nomacro
bne	$18,$3,$L552
addiu	$21,$21,1024
.set	macro
.set	reorder

lw	$20,%got($LC17)($28)
sll	$23,$16,7
lw	$22,76($sp)
move	$18,$0
addiu	$20,$20,%lo($LC17)
$L554:
.set	noreorder
.set	nomacro
blez	$16,$L967
lw	$3,72($sp)
.set	macro
.set	reorder

lw	$4,%got(ptr_square.constprop.8)($28)
move	$17,$0
move	$19,$22
addiu	$21,$4,%lo(ptr_square.constprop.8)
$L557:
lw	$25,%call16(printf)($28)
move	$5,$17
move	$4,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

addiu	$17,$17,1
li	$5,8			# 0x8
li	$6,8			# 0x8
move	$25,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ptr_square.constprop.8
1:	jalr	$25
move	$4,$19
.set	macro
.set	reorder

addiu	$19,$19,128
.set	noreorder
.set	nomacro
bne	$17,$16,$L557
lw	$28,32($sp)
.set	macro
.set	reorder

addu	$22,$22,$23
lw	$3,72($sp)
$L967:
addiu	$18,$18,1
.set	noreorder
.set	nomacro
bne	$18,$3,$L554
addiu	$22,$22,512
.set	macro
.set	reorder

lw	$17,76($sp)
move	$18,$0
lw	$20,%got($LC17)($28)
addiu	$19,$17,8
addiu	$20,$20,%lo($LC17)
$L562:
.set	noreorder
.set	nomacro
blez	$16,$L968
lw	$3,72($sp)
.set	macro
.set	reorder

lw	$4,%got(ptr_square.constprop.8)($28)
move	$17,$0
move	$21,$19
addiu	$22,$4,%lo(ptr_square.constprop.8)
$L561:
lw	$25,%call16(printf)($28)
move	$5,$17
move	$4,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

addiu	$17,$17,1
li	$5,8			# 0x8
li	$6,8			# 0x8
move	$25,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ptr_square.constprop.8
1:	jalr	$25
move	$4,$21
.set	macro
.set	reorder

addiu	$21,$21,128
.set	noreorder
.set	nomacro
bne	$17,$16,$L561
lw	$28,32($sp)
.set	macro
.set	reorder

addu	$19,$19,$23
lw	$3,72($sp)
$L968:
addiu	$18,$18,1
slt	$2,$18,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L562
addiu	$19,$19,512
.set	macro
.set	reorder

lw	$7,5648($fp)
lw	$8,%got(vpFrame)($28)
lw	$9,180($sp)
lw	$16,5644($fp)
sw	$7,72($sp)
lw	$8,0($8)
lw	$12,72($sp)
lw	$19,4($9)
lw	$20,0($9)
.set	noreorder
.set	nomacro
bgtz	$12,$L893
sw	$8,92($sp)
.set	macro
.set	reorder

$L551:
lw	$2,%got(crc_code)($28)
lw	$4,%got($LC18)($28)
lw	$25,%call16(printf)($28)
lw	$5,92($sp)
lh	$6,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L307
lw	$28,32($sp)
.set	macro
.set	reorder

$L412:
sw	$3,5620($fp)
move	$2,$4
.set	noreorder
.set	nomacro
b	$L408
sw	$5,5636($fp)
.set	macro
.set	reorder

$L409:
sw	$5,5620($fp)
li	$2,2			# 0x2
sw	$4,5636($fp)
.set	noreorder
.set	nomacro
b	$L408
li	$6,2			# 0x2
.set	macro
.set	reorder

$L848:
lw	$19,180($sp)
lw	$8,180($sp)
lw	$3,5616($fp)
lw	$2,16($19)
lw	$4,20($19)
sw	$0,80($8)
sw	$2,5652($fp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L318
sw	$4,5656($fp)
.set	macro
.set	reorder

$L849:
sll	$3,$2,2
lw	$25,%call16(av_malloc)($28)
sll	$4,$2,4
addu	$4,$3,$4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
b	$L318
sw	$2,5616($fp)
.set	macro
.set	reorder

$L847:
li	$2,2			# 0x2
lw	$14,180($sp)
.set	noreorder
.set	nomacro
beq	$17,$0,$L894
sw	$2,52($14)
.set	macro
.set	reorder

li	$2,3			# 0x3
lw	$13,180($sp)
sw	$2,80($13)
$L604:
lw	$2,5604($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L969
lw	$6,%got($LC11)($28)
.set	macro
.set	reorder

lw	$2,5608($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L970
li	$5,24			# 0x18
.set	macro
.set	reorder

lw	$2,5612($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L971
lw	$19,180($sp)
.set	macro
.set	reorder

lw	$6,%got($LC11)($28)
$L969:
li	$5,24			# 0x18
$L970:
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)
.set	macro
.set	reorder

li	$2,-22			# 0xffffffffffffffea
lw	$31,284($sp)
lw	$fp,280($sp)
lw	$23,276($sp)
lw	$22,272($sp)
lw	$21,268($sp)
lw	$20,264($sp)
lw	$19,260($sp)
lw	$18,256($sp)
lw	$17,252($sp)
lw	$16,248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,288
.set	macro
.set	reorder

$L840:
lw	$2,44($7)
.set	noreorder
.set	nomacro
bne	$2,$21,$L211
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L979
lw	$4,168($sp)
.set	macro
.set	reorder

$L894:
lw	$3,180($sp)
.set	noreorder
.set	nomacro
b	$L604
sw	$0,80($3)
.set	macro
.set	reorder

$L219:
.set	noreorder
.set	nomacro
b	$L590
sw	$2,5636($fp)
.set	macro
.set	reorder

$L258:
sw	$4,5620($fp)
.set	noreorder
.set	nomacro
beq	$7,$0,$L895
sw	$5,5636($fp)
.set	macro
.set	reorder

sw	$0,5672($fp)
$L262:
lw	$4,168($sp)
$L909:
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
li	$5,2			# 0x2
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L265
lw	$28,32($sp)
.set	macro
.set	reorder

li	$3,2			# 0x2
.set	noreorder
.set	nomacro
bne	$2,$3,$L896
li	$5,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$4,5620($fp)
li	$5,2			# 0x2
lw	$3,5624($fp)
lw	$2,5636($fp)
$L263:
addu	$6,$17,$4
sw	$5,5676($fp)
lbu	$5,0($6)
sll	$4,$4,$5
addu	$3,$5,$3
sll	$2,$2,$5
.set	noreorder
.set	nomacro
bltz	$3,$L267
sw	$4,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$5,5632($fp)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L972
addiu	$5,$4,-1
.set	macro
.set	reorder

addiu	$5,$6,2
sw	$5,5628($fp)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$3
addiu	$3,$3,-16
or	$2,$2,$5
$L267:
addiu	$5,$4,-1
$L972:
sll	$5,$5,7
sra	$5,$5,8
addiu	$5,$5,1
sll	$7,$5,16
sltu	$6,$2,$7
xori	$6,$6,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L268
sw	$3,5624($fp)
.set	macro
.set	reorder

subu	$5,$4,$5
subu	$2,$2,$7
$L268:
addu	$4,$17,$5
sw	$6,6772($fp)
sw	$2,5636($fp)
lbu	$6,0($4)
sll	$5,$5,$6
addu	$4,$3,$6
sll	$2,$2,$6
.set	noreorder
.set	nomacro
bltz	$4,$L269
sw	$5,5620($fp)
.set	macro
.set	reorder

lw	$6,5628($fp)
lw	$3,5632($fp)
sltu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L973
addiu	$3,$5,-1
.set	macro
.set	reorder

addiu	$3,$6,2
sw	$3,5628($fp)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$2,$2,$3
$L269:
addiu	$3,$5,-1
$L973:
sll	$3,$3,7
sra	$3,$3,8
addiu	$3,$3,1
sll	$7,$3,16
sltu	$6,$2,$7
xori	$6,$6,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L270
sw	$4,5624($fp)
.set	macro
.set	reorder

subu	$3,$5,$3
subu	$2,$2,$7
$L270:
sw	$2,5636($fp)
.set	noreorder
.set	nomacro
b	$L254
sw	$6,6776($fp)
.set	macro
.set	reorder

$L227:
lw	$18,%got(vp8_rac_get_uint)($28)
move	$8,$4
sw	$3,5620($fp)
.set	noreorder
.set	nomacro
beq	$7,$0,$L824
sw	$4,5636($fp)
.set	macro
.set	reorder

$L856:
addiu	$18,$18,%lo(vp8_rac_get_uint)
addiu	$19,$fp,6884
addiu	$21,$fp,6887
sw	$18,72($sp)
.set	noreorder
.set	nomacro
b	$L235
move	$7,$2
.set	macro
.set	reorder

$L898:
lw	$25,72($sp)
addiu	$19,$19,1
sw	$10,5620($fp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$11,5636($fp)
.set	macro
.set	reorder

andi	$2,$2,0x00ff
lw	$3,5620($fp)
lw	$7,5624($fp)
lw	$8,5636($fp)
.set	noreorder
.set	nomacro
beq	$19,$21,$L897
sb	$2,-1($19)
.set	macro
.set	reorder

$L235:
addu	$2,$17,$3
lw	$4,168($sp)
li	$5,8			# 0x8
lbu	$6,0($2)
li	$2,255			# 0xff
sll	$10,$3,$6
addu	$7,$6,$7
addiu	$3,$10,-1
sw	$10,5620($fp)
sll	$6,$8,$6
.set	noreorder
.set	nomacro
bltz	$7,$L233
sll	$3,$3,7
.set	macro
.set	reorder

lw	$9,5628($fp)
lw	$8,5632($fp)
sltu	$8,$9,$8
.set	noreorder
.set	nomacro
beq	$8,$0,$L233
addiu	$11,$9,2
.set	macro
.set	reorder

sw	$11,5628($fp)
lbu	$8,1($9)
lbu	$9,0($9)
sll	$8,$8,8
or	$8,$8,$9
sll	$9,$8,8
srl	$8,$8,8
or	$8,$9,$8
andi	$8,$8,0xffff
sll	$8,$8,$7
addiu	$7,$7,-16
or	$6,$6,$8
$L233:
sra	$3,$3,8
sw	$7,5624($fp)
move	$8,$6
addiu	$3,$3,1
sll	$9,$3,16
subu	$11,$6,$9
sltu	$9,$6,$9
.set	noreorder
.set	nomacro
beq	$9,$0,$L898
subu	$10,$10,$3
.set	macro
.set	reorder

sw	$3,5620($fp)
addiu	$19,$19,1
sw	$6,5636($fp)
.set	noreorder
.set	nomacro
bne	$19,$21,$L235
sb	$2,-1($19)
.set	macro
.set	reorder

$L897:
.set	noreorder
.set	nomacro
b	$L232
move	$2,$7
.set	macro
.set	reorder

$L845:
sw	$2,180($sp)
sw	$2,5600($fp)
lw	$15,180($sp)
lw	$2,0($15)
.set	noreorder
.set	nomacro
bne	$2,$0,$L312
lw	$16,288($sp)
.set	macro
.set	reorder

$L918:
move	$5,$15
lw	$25,256($16)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,32($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L312
move	$16,$2
.set	macro
.set	reorder

lw	$6,%got($LC10)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

move	$2,$16
lw	$31,284($sp)
lw	$fp,280($sp)
lw	$23,276($sp)
lw	$22,272($sp)
lw	$21,268($sp)
lw	$20,264($sp)
lw	$19,260($sp)
lw	$18,256($sp)
lw	$17,252($sp)
lw	$16,248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,288
.set	macro
.set	reorder

$L844:
lw	$6,%got($LC7)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L899
lw	$31,284($sp)
.set	macro
.set	reorder

$L229:
.set	noreorder
.set	nomacro
b	$L592
sw	$2,5620($fp)
.set	macro
.set	reorder

$L896:
lw	$4,5620($fp)
lw	$3,5624($fp)
.set	noreorder
.set	nomacro
b	$L263
lw	$2,5636($fp)
.set	macro
.set	reorder

$L265:
lw	$4,5620($fp)
li	$5,1			# 0x1
lw	$3,5624($fp)
.set	noreorder
.set	nomacro
b	$L263
lw	$2,5636($fp)
.set	macro
.set	reorder

$L895:
lw	$4,168($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
li	$5,2			# 0x2
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L626
lw	$28,32($sp)
.set	macro
.set	reorder

li	$3,2			# 0x2
.set	noreorder
.set	nomacro
bne	$2,$3,$L974
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

li	$2,3			# 0x3
.set	noreorder
.set	nomacro
b	$L262
sw	$2,5672($fp)
.set	macro
.set	reorder

$L890:
lw	$4,168($sp)
lw	$25,72($sp)
.set	noreorder
.set	nomacro
jalr	$25
li	$5,2			# 0x2
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L629
lw	$28,32($sp)
.set	macro
.set	reorder

li	$3,2			# 0x2
.set	noreorder
.set	nomacro
bne	$2,$3,$L261
li	$2,3			# 0x3
.set	macro
.set	reorder

sw	$2,5672($fp)
$L628:
lw	$4,5620($fp)
move	$5,$0
lw	$3,5624($fp)
.set	noreorder
.set	nomacro
b	$L263
lw	$2,5636($fp)
.set	macro
.set	reorder

$L261:
li	$2,-1			# 0xffffffffffffffff
$L974:
.set	noreorder
.set	nomacro
bne	$16,$0,$L628
sw	$2,5672($fp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L909
lw	$4,168($sp)
.set	macro
.set	reorder

$L626:
.set	noreorder
.set	nomacro
b	$L262
sw	$2,5672($fp)
.set	macro
.set	reorder

$L629:
.set	noreorder
.set	nomacro
b	$L628
sw	$2,5672($fp)
.set	macro
.set	reorder

$L891:
li	$13,268435456			# 0x10000000
.set	noreorder
.set	nomacro
b	$L536
and	$13,$4,$13
.set	macro
.set	reorder

$L855:
li	$8,1			# 0x1
sw	$0,84($sp)
li	$4,33			# 0x21
move	$3,$0
.set	noreorder
.set	nomacro
b	$L453
sw	$8,80($sp)
.set	macro
.set	reorder

$L641:
lw	$9,%got(vp8_qpar)($28)
li	$5,321585152			# 0x132b0000
addiu	$8,$5,7508
addiu	$7,$5,7604
sw	$9,120($sp)
addiu	$9,$5,6996
addiu	$11,$5,9140
sw	$8,140($sp)
addiu	$10,$5,9044
move	$6,$7
sw	$9,148($sp)
.set	noreorder
.set	nomacro
b	$L331
addiu	$5,$5,8532
.set	macro
.set	reorder

.end	vp8_decode_frame
.size	vp8_decode_frame, .-vp8_decode_frame
.globl	vp8_decoder
.section	.rodata.str1.4
.align	2
$LC21:
.ascii	"vp8\000"
.align	2
$LC22:
.ascii	"On2 VP8\000"
.section	.data.rel.local,"aw",@progbits
.align	2
.type	vp8_decoder, @object
.size	vp8_decoder, 72
vp8_decoder:
.word	$LC21
.word	0
.word	146
.word	11488
.word	vp8_decode_init
.word	0
.word	vp8_decode_free
.word	vp8_decode_frame
.word	2
.space	4
.word	vp8_decode_flush
.space	8
.word	$LC22
.space	16
.globl	SblkIdx
.data
.align	2
.type	SblkIdx, @object
.size	SblkIdx, 4
SblkIdx:
.byte	0
.byte	1
.byte	4
.byte	5
.globl	BlkIdx
.align	2
.type	BlkIdx, @object
.size	BlkIdx, 4
BlkIdx:
.byte	0
.byte	2
.byte	8
.byte	10

.comm	vp8_qpar,96,4
.local	motion_dsa2
.comm	motion_dsa2,4,4
.local	motion_dsa1
.comm	motion_dsa1,4,4
.local	motion_dsa0
.comm	motion_dsa0,4,4
.local	motion_dha2
.comm	motion_dha2,4,4
.local	motion_dha1
.comm	motion_dha1,4,4
.local	motion_dha0
.comm	motion_dha0,4,4
.local	mb2
.comm	mb2,4,4
.local	mb1
.comm	mb1,4,4
.local	mb0
.comm	mb0,4,4
.local	mau_reg_ptr2
.comm	mau_reg_ptr2,4,4
.local	mau_reg_ptr1
.comm	mau_reg_ptr1,4,4
.local	mau_reg_ptr0
.comm	mau_reg_ptr0,4,4
.local	block2
.comm	block2,4,4
.local	block1
.comm	block1,4,4
.local	block0
.comm	block0,4,4
.local	count
.comm	count,4,4
.globl	vp8_16x16_pred_lookup
.align	2
.type	vp8_16x16_pred_lookup, @object
.size	vp8_16x16_pred_lookup, 4
vp8_16x16_pred_lookup:
.byte	0
.byte	1
.byte	2
.byte	15
.globl	vp8_4x4_pred_lookup
.align	2
.type	vp8_4x4_pred_lookup, @object
.size	vp8_4x4_pred_lookup, 10
vp8_4x4_pred_lookup:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	15
.globl	ac2q_lookup
.section	.bss,"aw",@nobits
.align	2
.type	ac2q_lookup, @object
.size	ac2q_lookup, 128
ac2q_lookup:
.space	128
.globl	ac_qlookup_raw
.data
.align	2
.type	ac_qlookup_raw, @object
.size	ac_qlookup_raw, 512
ac_qlookup_raw:
.word	4
.word	5
.word	6
.word	7
.word	8
.word	9
.word	10
.word	11
.word	12
.word	13
.word	14
.word	15
.word	16
.word	17
.word	18
.word	19
.word	20
.word	21
.word	22
.word	23
.word	24
.word	25
.word	26
.word	27
.word	28
.word	29
.word	30
.word	31
.word	32
.word	33
.word	34
.word	35
.word	36
.word	37
.word	38
.word	39
.word	40
.word	41
.word	42
.word	43
.word	44
.word	45
.word	46
.word	47
.word	48
.word	49
.word	50
.word	51
.word	52
.word	53
.word	54
.word	55
.word	56
.word	57
.word	58
.word	60
.word	62
.word	64
.word	66
.word	68
.word	70
.word	72
.word	74
.word	76
.word	78
.word	80
.word	82
.word	84
.word	86
.word	88
.word	90
.word	92
.word	94
.word	96
.word	98
.word	100
.word	102
.word	104
.word	106
.word	108
.word	110
.word	112
.word	114
.word	116
.word	119
.word	122
.word	125
.word	128
.word	131
.word	134
.word	137
.word	140
.word	143
.word	146
.word	149
.word	152
.word	155
.word	158
.word	161
.word	164
.word	167
.word	170
.word	173
.word	177
.word	181
.word	185
.word	189
.word	193
.word	197
.word	201
.word	205
.word	209
.word	213
.word	217
.word	221
.word	225
.word	229
.word	234
.word	239
.word	245
.word	249
.word	254
.word	259
.word	264
.word	269
.word	274
.word	279
.word	284
.globl	ac_qlookup
.align	2
.type	ac_qlookup, @object
.size	ac_qlookup, 128
ac_qlookup:
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.byte	17
.byte	18
.byte	19
.byte	20
.byte	21
.byte	22
.byte	23
.byte	24
.byte	25
.byte	26
.byte	27
.byte	28
.byte	29
.byte	30
.byte	31
.byte	32
.byte	33
.byte	34
.byte	35
.byte	36
.byte	37
.byte	38
.byte	39
.byte	40
.byte	41
.byte	42
.byte	43
.byte	44
.byte	45
.byte	46
.byte	47
.byte	48
.byte	49
.byte	50
.byte	51
.byte	52
.byte	53
.byte	54
.byte	55
.byte	56
.byte	57
.byte	58
.byte	60
.byte	62
.byte	64
.byte	66
.byte	68
.byte	70
.byte	72
.byte	74
.byte	76
.byte	78
.byte	80
.byte	82
.byte	84
.byte	86
.byte	88
.byte	90
.byte	92
.byte	94
.byte	96
.byte	98
.byte	100
.byte	102
.byte	104
.byte	106
.byte	108
.byte	110
.byte	112
.byte	114
.byte	116
.byte	119
.byte	122
.byte	125
.byte	-128
.byte	-125
.byte	-122
.byte	-119
.byte	-116
.byte	-113
.byte	-110
.byte	-107
.byte	-104
.byte	-101
.byte	-98
.byte	-95
.byte	-92
.byte	-89
.byte	-86
.byte	-83
.byte	-79
.byte	-75
.byte	-71
.byte	-67
.byte	-63
.byte	-59
.byte	-55
.byte	-51
.byte	-47
.byte	-43
.byte	-39
.byte	-35
.byte	-31
.byte	-27
.byte	-22
.byte	-17
.byte	-11
.byte	-7
.byte	-2
.byte	3
.byte	8
.byte	13
.byte	18
.byte	23
.byte	28
.globl	dc_qlookup
.align	2
.type	dc_qlookup, @object
.size	dc_qlookup, 128
dc_qlookup:
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.byte	17
.byte	17
.byte	18
.byte	19
.byte	20
.byte	20
.byte	21
.byte	21
.byte	22
.byte	22
.byte	23
.byte	23
.byte	24
.byte	25
.byte	25
.byte	26
.byte	27
.byte	28
.byte	29
.byte	30
.byte	31
.byte	32
.byte	33
.byte	34
.byte	35
.byte	36
.byte	37
.byte	37
.byte	38
.byte	39
.byte	40
.byte	41
.byte	42
.byte	43
.byte	44
.byte	45
.byte	46
.byte	46
.byte	47
.byte	48
.byte	49
.byte	50
.byte	51
.byte	52
.byte	53
.byte	54
.byte	55
.byte	56
.byte	57
.byte	58
.byte	59
.byte	60
.byte	61
.byte	62
.byte	63
.byte	64
.byte	65
.byte	66
.byte	67
.byte	68
.byte	69
.byte	70
.byte	71
.byte	72
.byte	73
.byte	74
.byte	75
.byte	76
.byte	76
.byte	77
.byte	78
.byte	79
.byte	80
.byte	81
.byte	82
.byte	83
.byte	84
.byte	85
.byte	86
.byte	87
.byte	88
.byte	89
.byte	91
.byte	93
.byte	95
.byte	96
.byte	98
.byte	100
.byte	101
.byte	102
.byte	104
.byte	106
.byte	108
.byte	110
.byte	112
.byte	114
.byte	116
.byte	118
.byte	122
.byte	124
.byte	126
.byte	-128
.byte	-126
.byte	-124
.byte	-122
.byte	-120
.byte	-118
.byte	-116
.byte	-113
.byte	-111
.byte	-108
.byte	-105
.byte	-102
.byte	-99
.globl	vpFrame
.section	.bss
.align	2
.type	vpFrame, @object
.size	vpFrame, 4
vpFrame:
.space	4

.comm	crc_code,2,2
.rdata
.align	2
.type	crc16_tab, @object
.size	crc16_tab, 1024
crc16_tab:
.word	0
.word	32773
.word	32783
.word	10
.word	32795
.word	30
.word	20
.word	32785
.word	32819
.word	54
.word	60
.word	32825
.word	40
.word	32813
.word	32807
.word	34
.word	32867
.word	102
.word	108
.word	32873
.word	120
.word	32893
.word	32887
.word	114
.word	80
.word	32853
.word	32863
.word	90
.word	32843
.word	78
.word	68
.word	32833
.word	32963
.word	198
.word	204
.word	32969
.word	216
.word	32989
.word	32983
.word	210
.word	240
.word	33013
.word	33023
.word	250
.word	33003
.word	238
.word	228
.word	32993
.word	160
.word	32933
.word	32943
.word	170
.word	32955
.word	190
.word	180
.word	32945
.word	32915
.word	150
.word	156
.word	32921
.word	136
.word	32909
.word	32903
.word	130
.word	33155
.word	390
.word	396
.word	33161
.word	408
.word	33181
.word	33175
.word	402
.word	432
.word	33205
.word	33215
.word	442
.word	33195
.word	430
.word	420
.word	33185
.word	480
.word	33253
.word	33263
.word	490
.word	33275
.word	510
.word	500
.word	33265
.word	33235
.word	470
.word	476
.word	33241
.word	456
.word	33229
.word	33223
.word	450
.word	320
.word	33093
.word	33103
.word	330
.word	33115
.word	350
.word	340
.word	33105
.word	33139
.word	374
.word	380
.word	33145
.word	360
.word	33133
.word	33127
.word	354
.word	33059
.word	294
.word	300
.word	33065
.word	312
.word	33085
.word	33079
.word	306
.word	272
.word	33045
.word	33055
.word	282
.word	33035
.word	270
.word	260
.word	33025
.word	33539
.word	774
.word	780
.word	33545
.word	792
.word	33565
.word	33559
.word	786
.word	816
.word	33589
.word	33599
.word	826
.word	33579
.word	814
.word	804
.word	33569
.word	864
.word	33637
.word	33647
.word	874
.word	33659
.word	894
.word	884
.word	33649
.word	33619
.word	854
.word	860
.word	33625
.word	840
.word	33613
.word	33607
.word	834
.word	960
.word	33733
.word	33743
.word	970
.word	33755
.word	990
.word	980
.word	33745
.word	33779
.word	1014
.word	1020
.word	33785
.word	1000
.word	33773
.word	33767
.word	994
.word	33699
.word	934
.word	940
.word	33705
.word	952
.word	33725
.word	33719
.word	946
.word	912
.word	33685
.word	33695
.word	922
.word	33675
.word	910
.word	900
.word	33665
.word	640
.word	33413
.word	33423
.word	650
.word	33435
.word	670
.word	660
.word	33425
.word	33459
.word	694
.word	700
.word	33465
.word	680
.word	33453
.word	33447
.word	674
.word	33507
.word	742
.word	748
.word	33513
.word	760
.word	33533
.word	33527
.word	754
.word	720
.word	33493
.word	33503
.word	730
.word	33483
.word	718
.word	708
.word	33473
.word	33347
.word	582
.word	588
.word	33353
.word	600
.word	33373
.word	33367
.word	594
.word	624
.word	33397
.word	33407
.word	634
.word	33387
.word	622
.word	612
.word	33377
.word	544
.word	33317
.word	33327
.word	554
.word	33339
.word	574
.word	564
.word	33329
.word	33299
.word	534
.word	540
.word	33305
.word	520
.word	33293
.word	33287
.word	514
.globl	vmaupoll_pmon_val_1f
.section	.bss
.align	3
.type	vmaupoll_pmon_val_1f, @object
.size	vmaupoll_pmon_val_1f, 8
vmaupoll_pmon_val_1f:
.space	8
.globl	vmaupoll_pmon_val_ex
.align	2
.type	vmaupoll_pmon_val_ex, @object
.size	vmaupoll_pmon_val_ex, 4
vmaupoll_pmon_val_ex:
.space	4
.globl	vmaupoll_pmon_val
.align	2
.type	vmaupoll_pmon_val, @object
.size	vmaupoll_pmon_val, 4
vmaupoll_pmon_val:
.space	4
.globl	gp0poll_pmon_val_1f
.align	3
.type	gp0poll_pmon_val_1f, @object
.size	gp0poll_pmon_val_1f, 8
gp0poll_pmon_val_1f:
.space	8
.globl	gp0poll_pmon_val_ex
.align	2
.type	gp0poll_pmon_val_ex, @object
.size	gp0poll_pmon_val_ex, 4
gp0poll_pmon_val_ex:
.space	4
.globl	gp0poll_pmon_val
.align	2
.type	gp0poll_pmon_val, @object
.size	gp0poll_pmon_val, 4
gp0poll_pmon_val:
.space	4
.globl	mcpoll_pmon_val_1f
.align	3
.type	mcpoll_pmon_val_1f, @object
.size	mcpoll_pmon_val_1f, 8
mcpoll_pmon_val_1f:
.space	8
.globl	mcpoll_pmon_val_ex
.align	2
.type	mcpoll_pmon_val_ex, @object
.size	mcpoll_pmon_val_ex, 4
mcpoll_pmon_val_ex:
.space	4
.globl	mcpoll_pmon_val
.align	2
.type	mcpoll_pmon_val, @object
.size	mcpoll_pmon_val, 4
mcpoll_pmon_val:
.space	4
.globl	mbmd_pmon_val_1f
.align	3
.type	mbmd_pmon_val_1f, @object
.size	mbmd_pmon_val_1f, 8
mbmd_pmon_val_1f:
.space	8
.globl	mbmd_pmon_val_ex
.align	2
.type	mbmd_pmon_val_ex, @object
.size	mbmd_pmon_val_ex, 4
mbmd_pmon_val_ex:
.space	4
.globl	mbmd_pmon_val
.align	2
.type	mbmd_pmon_val, @object
.size	mbmd_pmon_val, 4
mbmd_pmon_val:
.space	4
.globl	p0allfrm_pmon_val_1f
.align	3
.type	p0allfrm_pmon_val_1f, @object
.size	p0allfrm_pmon_val_1f, 8
p0allfrm_pmon_val_1f:
.space	8
.globl	p0allfrm_pmon_val_ex
.align	2
.type	p0allfrm_pmon_val_ex, @object
.size	p0allfrm_pmon_val_ex, 4
p0allfrm_pmon_val_ex:
.space	4
.globl	p0allfrm_pmon_val
.align	2
.type	p0allfrm_pmon_val, @object
.size	p0allfrm_pmon_val, 4
p0allfrm_pmon_val:
.space	4
.globl	p0test_pmon_val_1f
.align	3
.type	p0test_pmon_val_1f, @object
.size	p0test_pmon_val_1f, 8
p0test_pmon_val_1f:
.space	8
.globl	p0test_pmon_val_ex
.align	2
.type	p0test_pmon_val_ex, @object
.size	p0test_pmon_val_ex, 4
p0test_pmon_val_ex:
.space	4
.globl	p0test_pmon_val
.align	2
.type	p0test_pmon_val, @object
.size	p0test_pmon_val, 4
p0test_pmon_val:
.space	4

.comm	tcsm1_word_bank_backup,24,4

.comm	vpu_base,4,4

.comm	sram_base,4,4

.comm	tcsm1_base,4,4
.rdata
.align	2
.type	IntpFMT, @object
.size	IntpFMT, 7280
IntpFMT:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	4
.byte	3
.byte	3
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	7
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	2
.byte	2
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	2
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	2
.byte	2
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	2
.byte	2
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	2
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	2
.byte	2
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	SubPel, @object
.size	SubPel, 12
SubPel:
.byte	1
.byte	2
.byte	2
.byte	3
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.align	2
.type	AryFMT, @object
.size	AryFMT, 16
AryFMT:
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	vp8_mv_default_prob, @object
.size	vp8_mv_default_prob, 38
vp8_mv_default_prob:
.byte	-94
.byte	-128
.byte	-31
.byte	-110
.byte	-84
.byte	-109
.byte	-42
.byte	39
.byte	-100
.byte	-128
.byte	-127
.byte	-124
.byte	75
.byte	-111
.byte	-78
.byte	-50
.byte	-17
.byte	-2
.byte	-2
.byte	-92
.byte	-128
.byte	-52
.byte	-86
.byte	119
.byte	-21
.byte	-116
.byte	-26
.byte	-28
.byte	-128
.byte	-126
.byte	-126
.byte	74
.byte	-108
.byte	-76
.byte	-53
.byte	-20
.byte	-2
.byte	-2
.align	2
.type	vp8_mv_update_prob, @object
.size	vp8_mv_update_prob, 38
vp8_mv_update_prob:
.byte	-19
.byte	-10
.byte	-3
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-6
.byte	-6
.byte	-4
.byte	-2
.byte	-2
.byte	-25
.byte	-13
.byte	-11
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-5
.byte	-5
.byte	-2
.byte	-2
.byte	-2
.align	2
.type	vp8_ac_qlookup, @object
.size	vp8_ac_qlookup, 256
vp8_ac_qlookup:
.half	4
.half	5
.half	6
.half	7
.half	8
.half	9
.half	10
.half	11
.half	12
.half	13
.half	14
.half	15
.half	16
.half	17
.half	18
.half	19
.half	20
.half	21
.half	22
.half	23
.half	24
.half	25
.half	26
.half	27
.half	28
.half	29
.half	30
.half	31
.half	32
.half	33
.half	34
.half	35
.half	36
.half	37
.half	38
.half	39
.half	40
.half	41
.half	42
.half	43
.half	44
.half	45
.half	46
.half	47
.half	48
.half	49
.half	50
.half	51
.half	52
.half	53
.half	54
.half	55
.half	56
.half	57
.half	58
.half	60
.half	62
.half	64
.half	66
.half	68
.half	70
.half	72
.half	74
.half	76
.half	78
.half	80
.half	82
.half	84
.half	86
.half	88
.half	90
.half	92
.half	94
.half	96
.half	98
.half	100
.half	102
.half	104
.half	106
.half	108
.half	110
.half	112
.half	114
.half	116
.half	119
.half	122
.half	125
.half	128
.half	131
.half	134
.half	137
.half	140
.half	143
.half	146
.half	149
.half	152
.half	155
.half	158
.half	161
.half	164
.half	167
.half	170
.half	173
.half	177
.half	181
.half	185
.half	189
.half	193
.half	197
.half	201
.half	205
.half	209
.half	213
.half	217
.half	221
.half	225
.half	229
.half	234
.half	239
.half	245
.half	249
.half	254
.half	259
.half	264
.half	269
.half	274
.half	279
.half	284
.align	2
.type	vp8_dc_qlookup, @object
.size	vp8_dc_qlookup, 128
vp8_dc_qlookup:
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.byte	17
.byte	17
.byte	18
.byte	19
.byte	20
.byte	20
.byte	21
.byte	21
.byte	22
.byte	22
.byte	23
.byte	23
.byte	24
.byte	25
.byte	25
.byte	26
.byte	27
.byte	28
.byte	29
.byte	30
.byte	31
.byte	32
.byte	33
.byte	34
.byte	35
.byte	36
.byte	37
.byte	37
.byte	38
.byte	39
.byte	40
.byte	41
.byte	42
.byte	43
.byte	44
.byte	45
.byte	46
.byte	46
.byte	47
.byte	48
.byte	49
.byte	50
.byte	51
.byte	52
.byte	53
.byte	54
.byte	55
.byte	56
.byte	57
.byte	58
.byte	59
.byte	60
.byte	61
.byte	62
.byte	63
.byte	64
.byte	65
.byte	66
.byte	67
.byte	68
.byte	69
.byte	70
.byte	71
.byte	72
.byte	73
.byte	74
.byte	75
.byte	76
.byte	76
.byte	77
.byte	78
.byte	79
.byte	80
.byte	81
.byte	82
.byte	83
.byte	84
.byte	85
.byte	86
.byte	87
.byte	88
.byte	89
.byte	91
.byte	93
.byte	95
.byte	96
.byte	98
.byte	100
.byte	101
.byte	102
.byte	104
.byte	106
.byte	108
.byte	110
.byte	112
.byte	114
.byte	116
.byte	118
.byte	122
.byte	124
.byte	126
.byte	-128
.byte	-126
.byte	-124
.byte	-122
.byte	-120
.byte	-118
.byte	-116
.byte	-113
.byte	-111
.byte	-108
.byte	-105
.byte	-102
.byte	-99
.align	2
.type	zigzag_scan, @object
.size	zigzag_scan, 16
zigzag_scan:
.byte	0
.byte	1
.byte	4
.byte	8
.byte	5
.byte	2
.byte	3
.byte	6
.byte	9
.byte	12
.byte	13
.byte	10
.byte	7
.byte	11
.byte	14
.byte	15
.align	2
.type	vp8_token_update_probs, @object
.size	vp8_token_update_probs, 1056
vp8_token_update_probs:
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-80
.byte	-10
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-33
.byte	-15
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-7
.byte	-3
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-12
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-22
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-10
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-17
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-3
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-6
.byte	-1
.byte	-2
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-39
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-31
.byte	-4
.byte	-15
.byte	-3
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-22
.byte	-6
.byte	-15
.byte	-6
.byte	-3
.byte	-1
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-33
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-18
.byte	-3
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-7
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-9
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-6
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-70
.byte	-5
.byte	-6
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-22
.byte	-5
.byte	-12
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-5
.byte	-13
.byte	-3
.byte	-2
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-20
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-3
.byte	-3
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-6
.byte	-2
.byte	-4
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-2
.byte	-7
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-10
.byte	-3
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-4
.byte	-2
.byte	-5
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-2
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-2
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-11
.byte	-5
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-4
.byte	-3
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-7
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-6
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-2
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.align	2
.type	vp8_token_default_probs, @object
.size	vp8_token_default_probs, 1056
vp8_token_default_probs:
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-3
.byte	-120
.byte	-2
.byte	-1
.byte	-28
.byte	-37
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-67
.byte	-127
.byte	-14
.byte	-1
.byte	-29
.byte	-43
.byte	-1
.byte	-37
.byte	-128
.byte	-128
.byte	-128
.byte	106
.byte	126
.byte	-29
.byte	-4
.byte	-42
.byte	-47
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	98
.byte	-8
.byte	-1
.byte	-20
.byte	-30
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-75
.byte	-123
.byte	-18
.byte	-2
.byte	-35
.byte	-22
.byte	-1
.byte	-102
.byte	-128
.byte	-128
.byte	-128
.byte	78
.byte	-122
.byte	-54
.byte	-9
.byte	-58
.byte	-76
.byte	-1
.byte	-37
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-71
.byte	-7
.byte	-1
.byte	-13
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-72
.byte	-106
.byte	-9
.byte	-1
.byte	-20
.byte	-32
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	77
.byte	110
.byte	-40
.byte	-1
.byte	-20
.byte	-26
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	101
.byte	-5
.byte	-1
.byte	-15
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-86
.byte	-117
.byte	-15
.byte	-4
.byte	-20
.byte	-47
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	37
.byte	116
.byte	-60
.byte	-13
.byte	-28
.byte	-1
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-52
.byte	-2
.byte	-1
.byte	-11
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-49
.byte	-96
.byte	-6
.byte	-1
.byte	-18
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	102
.byte	103
.byte	-25
.byte	-1
.byte	-45
.byte	-85
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-104
.byte	-4
.byte	-1
.byte	-16
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-79
.byte	-121
.byte	-13
.byte	-1
.byte	-22
.byte	-31
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	80
.byte	-127
.byte	-45
.byte	-1
.byte	-62
.byte	-32
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-10
.byte	1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-58
.byte	35
.byte	-19
.byte	-33
.byte	-63
.byte	-69
.byte	-94
.byte	-96
.byte	-111
.byte	-101
.byte	62
.byte	-125
.byte	45
.byte	-58
.byte	-35
.byte	-84
.byte	-80
.byte	-36
.byte	-99
.byte	-4
.byte	-35
.byte	1
.byte	68
.byte	47
.byte	-110
.byte	-48
.byte	-107
.byte	-89
.byte	-35
.byte	-94
.byte	-1
.byte	-33
.byte	-128
.byte	1
.byte	-107
.byte	-15
.byte	-1
.byte	-35
.byte	-32
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-72
.byte	-115
.byte	-22
.byte	-3
.byte	-34
.byte	-36
.byte	-1
.byte	-57
.byte	-128
.byte	-128
.byte	-128
.byte	81
.byte	99
.byte	-75
.byte	-14
.byte	-80
.byte	-66
.byte	-7
.byte	-54
.byte	-1
.byte	-1
.byte	-128
.byte	1
.byte	-127
.byte	-24
.byte	-3
.byte	-42
.byte	-59
.byte	-14
.byte	-60
.byte	-1
.byte	-1
.byte	-128
.byte	99
.byte	121
.byte	-46
.byte	-6
.byte	-55
.byte	-58
.byte	-1
.byte	-54
.byte	-128
.byte	-128
.byte	-128
.byte	23
.byte	91
.byte	-93
.byte	-14
.byte	-86
.byte	-69
.byte	-9
.byte	-46
.byte	-1
.byte	-1
.byte	-128
.byte	1
.byte	-56
.byte	-10
.byte	-1
.byte	-22
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	109
.byte	-78
.byte	-15
.byte	-1
.byte	-25
.byte	-11
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	44
.byte	-126
.byte	-55
.byte	-3
.byte	-51
.byte	-64
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-124
.byte	-17
.byte	-5
.byte	-37
.byte	-47
.byte	-1
.byte	-91
.byte	-128
.byte	-128
.byte	-128
.byte	94
.byte	-120
.byte	-31
.byte	-5
.byte	-38
.byte	-66
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	22
.byte	100
.byte	-82
.byte	-11
.byte	-70
.byte	-95
.byte	-1
.byte	-57
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-74
.byte	-7
.byte	-1
.byte	-24
.byte	-21
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	124
.byte	-113
.byte	-15
.byte	-1
.byte	-29
.byte	-22
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	35
.byte	77
.byte	-75
.byte	-5
.byte	-63
.byte	-45
.byte	-1
.byte	-51
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-99
.byte	-9
.byte	-1
.byte	-20
.byte	-25
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	121
.byte	-115
.byte	-21
.byte	-1
.byte	-31
.byte	-29
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	45
.byte	99
.byte	-68
.byte	-5
.byte	-61
.byte	-39
.byte	-1
.byte	-32
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	1
.byte	-5
.byte	-1
.byte	-43
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-53
.byte	1
.byte	-8
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-119
.byte	1
.byte	-79
.byte	-1
.byte	-32
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-3
.byte	9
.byte	-8
.byte	-5
.byte	-49
.byte	-48
.byte	-1
.byte	-64
.byte	-128
.byte	-128
.byte	-128
.byte	-81
.byte	13
.byte	-32
.byte	-13
.byte	-63
.byte	-71
.byte	-7
.byte	-58
.byte	-1
.byte	-1
.byte	-128
.byte	73
.byte	17
.byte	-85
.byte	-35
.byte	-95
.byte	-77
.byte	-20
.byte	-89
.byte	-1
.byte	-22
.byte	-128
.byte	1
.byte	95
.byte	-9
.byte	-3
.byte	-44
.byte	-73
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-17
.byte	90
.byte	-12
.byte	-6
.byte	-45
.byte	-47
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-101
.byte	77
.byte	-61
.byte	-8
.byte	-68
.byte	-61
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	24
.byte	-17
.byte	-5
.byte	-38
.byte	-37
.byte	-1
.byte	-51
.byte	-128
.byte	-128
.byte	-128
.byte	-55
.byte	51
.byte	-37
.byte	-1
.byte	-60
.byte	-70
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	69
.byte	46
.byte	-66
.byte	-17
.byte	-55
.byte	-38
.byte	-1
.byte	-28
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-65
.byte	-5
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-33
.byte	-91
.byte	-7
.byte	-1
.byte	-43
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-115
.byte	124
.byte	-8
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	16
.byte	-8
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-66
.byte	36
.byte	-26
.byte	-1
.byte	-20
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-107
.byte	1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-30
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-9
.byte	-64
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-16
.byte	-128
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-122
.byte	-4
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-43
.byte	62
.byte	-6
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	55
.byte	93
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-54
.byte	24
.byte	-43
.byte	-21
.byte	-70
.byte	-65
.byte	-36
.byte	-96
.byte	-16
.byte	-81
.byte	-1
.byte	126
.byte	38
.byte	-74
.byte	-24
.byte	-87
.byte	-72
.byte	-28
.byte	-82
.byte	-1
.byte	-69
.byte	-128
.byte	61
.byte	46
.byte	-118
.byte	-37
.byte	-105
.byte	-78
.byte	-16
.byte	-86
.byte	-1
.byte	-40
.byte	-128
.byte	1
.byte	112
.byte	-26
.byte	-6
.byte	-57
.byte	-65
.byte	-9
.byte	-97
.byte	-1
.byte	-1
.byte	-128
.byte	-90
.byte	109
.byte	-28
.byte	-4
.byte	-45
.byte	-41
.byte	-1
.byte	-82
.byte	-128
.byte	-128
.byte	-128
.byte	39
.byte	77
.byte	-94
.byte	-24
.byte	-84
.byte	-76
.byte	-11
.byte	-78
.byte	-1
.byte	-1
.byte	-128
.byte	1
.byte	52
.byte	-36
.byte	-10
.byte	-58
.byte	-57
.byte	-7
.byte	-36
.byte	-1
.byte	-1
.byte	-128
.byte	124
.byte	74
.byte	-65
.byte	-13
.byte	-73
.byte	-63
.byte	-6
.byte	-35
.byte	-1
.byte	-1
.byte	-128
.byte	24
.byte	71
.byte	-126
.byte	-37
.byte	-102
.byte	-86
.byte	-13
.byte	-74
.byte	-1
.byte	-1
.byte	-128
.byte	1
.byte	-74
.byte	-31
.byte	-7
.byte	-37
.byte	-16
.byte	-1
.byte	-32
.byte	-128
.byte	-128
.byte	-128
.byte	-107
.byte	-106
.byte	-30
.byte	-4
.byte	-40
.byte	-51
.byte	-1
.byte	-85
.byte	-128
.byte	-128
.byte	-128
.byte	28
.byte	108
.byte	-86
.byte	-14
.byte	-73
.byte	-62
.byte	-2
.byte	-33
.byte	-1
.byte	-1
.byte	-128
.byte	1
.byte	81
.byte	-26
.byte	-4
.byte	-52
.byte	-53
.byte	-1
.byte	-64
.byte	-128
.byte	-128
.byte	-128
.byte	123
.byte	102
.byte	-47
.byte	-9
.byte	-68
.byte	-60
.byte	-1
.byte	-23
.byte	-128
.byte	-128
.byte	-128
.byte	20
.byte	95
.byte	-103
.byte	-13
.byte	-92
.byte	-83
.byte	-1
.byte	-53
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	-34
.byte	-8
.byte	-1
.byte	-40
.byte	-43
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-88
.byte	-81
.byte	-10
.byte	-4
.byte	-21
.byte	-51
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	47
.byte	116
.byte	-41
.byte	-1
.byte	-45
.byte	-44
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	121
.byte	-20
.byte	-3
.byte	-44
.byte	-42
.byte	-1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-115
.byte	84
.byte	-43
.byte	-4
.byte	-55
.byte	-54
.byte	-1
.byte	-37
.byte	-128
.byte	-128
.byte	-128
.byte	42
.byte	80
.byte	-96
.byte	-16
.byte	-94
.byte	-71
.byte	-1
.byte	-51
.byte	-128
.byte	-128
.byte	-128
.byte	1
.byte	1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-12
.byte	1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-18
.byte	1
.byte	-1
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.byte	-128
.section	.data.rel.ro.local,"aw",@progbits
.align	2
.type	vp8_dct_cat_prob, @object
.size	vp8_dct_cat_prob, 16
vp8_dct_cat_prob:
.word	vp8_dct_cat3_prob
.word	vp8_dct_cat4_prob
.word	vp8_dct_cat5_prob
.word	vp8_dct_cat6_prob
.rdata
.align	2
.type	vp8_dct_cat6_prob, @object
.size	vp8_dct_cat6_prob, 12
vp8_dct_cat6_prob:
.byte	-2
.byte	-2
.byte	-13
.byte	-26
.byte	-60
.byte	-79
.byte	-103
.byte	-116
.byte	-123
.byte	-126
.byte	-127
.byte	0
.align	2
.type	vp8_dct_cat5_prob, @object
.size	vp8_dct_cat5_prob, 6
vp8_dct_cat5_prob:
.byte	-76
.byte	-99
.byte	-115
.byte	-122
.byte	-126
.byte	0
.align	2
.type	vp8_dct_cat4_prob, @object
.size	vp8_dct_cat4_prob, 5
vp8_dct_cat4_prob:
.byte	-80
.byte	-101
.byte	-116
.byte	-121
.byte	0
.align	2
.type	vp8_dct_cat3_prob, @object
.size	vp8_dct_cat3_prob, 4
vp8_dct_cat3_prob:
.byte	-83
.byte	-108
.byte	-116
.byte	0
.align	2
.type	vp8_coeff_band_indexes, @object
.size	vp8_coeff_band_indexes, 80
vp8_coeff_band_indexes:
.byte	0
.byte	-1
.space	8
.byte	1
.byte	-1
.space	8
.byte	2
.byte	-1
.space	8
.byte	3
.byte	-1
.space	8
.byte	5
.byte	-1
.space	8
.byte	6
.byte	-1
.space	8
.byte	4
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	-1
.byte	15
.byte	-1
.space	8
.align	2
.type	vp8_coeff_band, @object
.size	vp8_coeff_band, 16
vp8_coeff_band:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	6
.byte	4
.byte	5
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	6
.byte	7
.align	2
.type	vp8_segmentid_tree, @object
.size	vp8_segmentid_tree, 6
vp8_segmentid_tree:
.byte	1
.byte	2
.byte	0
.byte	-1
.byte	-2
.byte	-3
.align	2
.type	vp8_pred4x4_prob_intra, @object
.size	vp8_pred4x4_prob_intra, 900
vp8_pred4x4_prob_intra:
.byte	39
.byte	53
.byte	-56
.byte	87
.byte	26
.byte	21
.byte	43
.byte	-24
.byte	-85
.byte	56
.byte	34
.byte	51
.byte	104
.byte	114
.byte	102
.byte	29
.byte	93
.byte	77
.byte	88
.byte	88
.byte	-109
.byte	-106
.byte	42
.byte	46
.byte	45
.byte	-60
.byte	-51
.byte	107
.byte	54
.byte	32
.byte	26
.byte	51
.byte	1
.byte	81
.byte	43
.byte	31
.byte	39
.byte	28
.byte	85
.byte	-85
.byte	58
.byte	-91
.byte	90
.byte	98
.byte	64
.byte	34
.byte	22
.byte	116
.byte	-50
.byte	23
.byte	34
.byte	43
.byte	-90
.byte	73
.byte	34
.byte	19
.byte	21
.byte	102
.byte	-124
.byte	-68
.byte	16
.byte	76
.byte	124
.byte	68
.byte	25
.byte	106
.byte	22
.byte	64
.byte	-85
.byte	36
.byte	-31
.byte	114
.byte	62
.byte	18
.byte	78
.byte	95
.byte	85
.byte	57
.byte	50
.byte	48
.byte	51
.byte	43
.byte	97
.byte	-73
.byte	117
.byte	85
.byte	38
.byte	35
.byte	-77
.byte	61
.byte	112
.byte	113
.byte	77
.byte	85
.byte	-77
.byte	-1
.byte	38
.byte	120
.byte	114
.byte	40
.byte	42
.byte	1
.byte	-60
.byte	-11
.byte	-47
.byte	10
.byte	25
.byte	109
.byte	-63
.byte	101
.byte	35
.byte	-97
.byte	-41
.byte	111
.byte	89
.byte	46
.byte	111
.byte	100
.byte	80
.byte	8
.byte	43
.byte	-102
.byte	1
.byte	51
.byte	26
.byte	71
.byte	88
.byte	43
.byte	29
.byte	-116
.byte	-90
.byte	-43
.byte	37
.byte	43
.byte	-102
.byte	61
.byte	63
.byte	30
.byte	-101
.byte	67
.byte	45
.byte	68
.byte	1
.byte	-47
.byte	41
.byte	40
.byte	5
.byte	102
.byte	-45
.byte	-73
.byte	4
.byte	1
.byte	-35
.byte	-114
.byte	78
.byte	78
.byte	16
.byte	-1
.byte	-128
.byte	34
.byte	-59
.byte	-85
.byte	51
.byte	50
.byte	17
.byte	-88
.byte	-47
.byte	-64
.byte	23
.byte	25
.byte	82
.byte	60
.byte	-108
.byte	31
.byte	-84
.byte	-37
.byte	-28
.byte	21
.byte	18
.byte	111
.byte	-81
.byte	69
.byte	-113
.byte	80
.byte	85
.byte	82
.byte	72
.byte	-101
.byte	103
.byte	56
.byte	58
.byte	10
.byte	-85
.byte	-38
.byte	-67
.byte	17
.byte	13
.byte	-104
.byte	-25
.byte	120
.byte	48
.byte	89
.byte	115
.byte	113
.byte	120
.byte	-104
.byte	112
.byte	-112
.byte	71
.byte	10
.byte	38
.byte	-85
.byte	-43
.byte	-112
.byte	34
.byte	26
.byte	114
.byte	26
.byte	17
.byte	-93
.byte	44
.byte	-61
.byte	21
.byte	10
.byte	-83
.byte	121
.byte	24
.byte	80
.byte	-61
.byte	26
.byte	62
.byte	44
.byte	64
.byte	85
.byte	63
.byte	20
.byte	8
.byte	114
.byte	114
.byte	-48
.byte	12
.byte	9
.byte	-30
.byte	-86
.byte	46
.byte	55
.byte	19
.byte	-120
.byte	-96
.byte	33
.byte	-50
.byte	71
.byte	81
.byte	40
.byte	11
.byte	96
.byte	-74
.byte	84
.byte	29
.byte	16
.byte	36
.byte	-104
.byte	-77
.byte	64
.byte	126
.byte	-86
.byte	118
.byte	46
.byte	70
.byte	95
.byte	75
.byte	79
.byte	123
.byte	47
.byte	51
.byte	-128
.byte	81
.byte	-85
.byte	1
.byte	57
.byte	17
.byte	5
.byte	71
.byte	102
.byte	57
.byte	53
.byte	41
.byte	49
.byte	125
.byte	98
.byte	42
.byte	88
.byte	104
.byte	85
.byte	117
.byte	-81
.byte	82
.byte	115
.byte	21
.byte	2
.byte	10
.byte	102
.byte	-1
.byte	-90
.byte	23
.byte	6
.byte	38
.byte	33
.byte	13
.byte	121
.byte	57
.byte	73
.byte	26
.byte	1
.byte	85
.byte	41
.byte	10
.byte	67
.byte	-118
.byte	77
.byte	110
.byte	90
.byte	47
.byte	114
.byte	57
.byte	18
.byte	10
.byte	102
.byte	102
.byte	-43
.byte	34
.byte	20
.byte	43
.byte	101
.byte	29
.byte	16
.byte	10
.byte	85
.byte	-128
.byte	101
.byte	-60
.byte	26
.byte	117
.byte	20
.byte	15
.byte	36
.byte	-93
.byte	-128
.byte	68
.byte	1
.byte	26
.byte	95
.byte	84
.byte	53
.byte	89
.byte	-128
.byte	100
.byte	113
.byte	101
.byte	45
.byte	63
.byte	59
.byte	90
.byte	-76
.byte	59
.byte	-90
.byte	93
.byte	73
.byte	-102
.byte	40
.byte	40
.byte	21
.byte	116
.byte	-113
.byte	-47
.byte	34
.byte	39
.byte	-81
.byte	-118
.byte	31
.byte	36
.byte	-85
.byte	27
.byte	-90
.byte	38
.byte	44
.byte	-27
.byte	57
.byte	46
.byte	22
.byte	24
.byte	-128
.byte	1
.byte	54
.byte	17
.byte	37
.byte	47
.byte	15
.byte	16
.byte	-73
.byte	34
.byte	-33
.byte	49
.byte	45
.byte	-73
.byte	46
.byte	17
.byte	33
.byte	-73
.byte	6
.byte	98
.byte	15
.byte	32
.byte	-73
.byte	40
.byte	3
.byte	9
.byte	115
.byte	51
.byte	-64
.byte	18
.byte	6
.byte	-33
.byte	65
.byte	32
.byte	73
.byte	115
.byte	28
.byte	-128
.byte	23
.byte	-128
.byte	-51
.byte	87
.byte	37
.byte	9
.byte	115
.byte	59
.byte	77
.byte	64
.byte	21
.byte	47
.byte	67
.byte	87
.byte	58
.byte	-87
.byte	82
.byte	115
.byte	26
.byte	59
.byte	-77
.byte	54
.byte	57
.byte	112
.byte	-72
.byte	5
.byte	41
.byte	38
.byte	-90
.byte	-43
.byte	30
.byte	34
.byte	26
.byte	-123
.byte	-104
.byte	116
.byte	10
.byte	32
.byte	-122
.byte	104
.byte	55
.byte	44
.byte	-38
.byte	9
.byte	54
.byte	53
.byte	-126
.byte	-30
.byte	75
.byte	32
.byte	12
.byte	51
.byte	-64
.byte	-1
.byte	-96
.byte	43
.byte	51
.byte	39
.byte	19
.byte	53
.byte	-35
.byte	26
.byte	114
.byte	32
.byte	73
.byte	-1
.byte	31
.byte	9
.byte	65
.byte	-22
.byte	2
.byte	15
.byte	1
.byte	118
.byte	73
.byte	56
.byte	21
.byte	23
.byte	111
.byte	59
.byte	-51
.byte	45
.byte	37
.byte	-64
.byte	88
.byte	31
.byte	35
.byte	67
.byte	102
.byte	85
.byte	55
.byte	-70
.byte	85
.byte	55
.byte	38
.byte	70
.byte	124
.byte	73
.byte	102
.byte	1
.byte	34
.byte	98
.byte	64
.byte	90
.byte	70
.byte	-51
.byte	40
.byte	41
.byte	23
.byte	26
.byte	57
.byte	86
.byte	40
.byte	64
.byte	-121
.byte	-108
.byte	-32
.byte	45
.byte	-73
.byte	-128
.byte	22
.byte	26
.byte	17
.byte	-125
.byte	-16
.byte	-102
.byte	14
.byte	1
.byte	-47
.byte	-92
.byte	50
.byte	31
.byte	-119
.byte	-102
.byte	-123
.byte	25
.byte	35
.byte	-38
.byte	83
.byte	12
.byte	13
.byte	54
.byte	-64
.byte	-1
.byte	68
.byte	47
.byte	28
.byte	45
.byte	16
.byte	21
.byte	91
.byte	64
.byte	-34
.byte	7
.byte	1
.byte	-59
.byte	56
.byte	21
.byte	39
.byte	-101
.byte	60
.byte	-118
.byte	23
.byte	102
.byte	-43
.byte	18
.byte	11
.byte	7
.byte	63
.byte	-112
.byte	-85
.byte	4
.byte	4
.byte	-10
.byte	85
.byte	26
.byte	85
.byte	85
.byte	-128
.byte	-128
.byte	32
.byte	-110
.byte	-85
.byte	35
.byte	27
.byte	10
.byte	-110
.byte	-82
.byte	-85
.byte	12
.byte	26
.byte	-128
.byte	51
.byte	103
.byte	44
.byte	-125
.byte	-125
.byte	123
.byte	31
.byte	6
.byte	-98
.byte	68
.byte	45
.byte	-128
.byte	34
.byte	1
.byte	47
.byte	11
.byte	-11
.byte	-85
.byte	62
.byte	17
.byte	19
.byte	70
.byte	-110
.byte	85
.byte	55
.byte	62
.byte	70
.byte	102
.byte	61
.byte	71
.byte	37
.byte	34
.byte	53
.byte	31
.byte	-13
.byte	-64
.byte	75
.byte	15
.byte	9
.byte	9
.byte	64
.byte	-1
.byte	-72
.byte	119
.byte	16
.byte	37
.byte	43
.byte	37
.byte	-102
.byte	100
.byte	-93
.byte	85
.byte	-96
.byte	1
.byte	63
.byte	9
.byte	92
.byte	-120
.byte	28
.byte	64
.byte	32
.byte	-55
.byte	85
.byte	56
.byte	8
.byte	17
.byte	-124
.byte	-119
.byte	-1
.byte	55
.byte	116
.byte	-128
.byte	86
.byte	6
.byte	28
.byte	5
.byte	64
.byte	-1
.byte	25
.byte	-8
.byte	1
.byte	58
.byte	15
.byte	20
.byte	82
.byte	-121
.byte	57
.byte	26
.byte	121
.byte	40
.byte	69
.byte	60
.byte	71
.byte	38
.byte	73
.byte	119
.byte	28
.byte	-34
.byte	37
.byte	101
.byte	75
.byte	-128
.byte	-117
.byte	118
.byte	-110
.byte	116
.byte	-128
.byte	85
.byte	56
.byte	41
.byte	15
.byte	-80
.byte	-20
.byte	85
.byte	37
.byte	9
.byte	62
.byte	-66
.byte	80
.byte	35
.byte	99
.byte	-76
.byte	80
.byte	126
.byte	54
.byte	45
.byte	-110
.byte	36
.byte	19
.byte	30
.byte	-85
.byte	-1
.byte	97
.byte	27
.byte	20
.byte	71
.byte	30
.byte	17
.byte	119
.byte	118
.byte	-1
.byte	17
.byte	18
.byte	-118
.byte	101
.byte	38
.byte	60
.byte	-118
.byte	55
.byte	70
.byte	43
.byte	26
.byte	-114
.byte	32
.byte	41
.byte	20
.byte	117
.byte	-105
.byte	-114
.byte	20
.byte	21
.byte	-93
.byte	-118
.byte	45
.byte	61
.byte	62
.byte	-37
.byte	1
.byte	81
.byte	-68
.byte	64
.byte	112
.byte	19
.byte	12
.byte	61
.byte	-61
.byte	-128
.byte	48
.byte	4
.byte	24
.byte	85
.byte	126
.byte	47
.byte	87
.byte	-80
.byte	51
.byte	41
.byte	20
.byte	32
.byte	66
.byte	102
.byte	-89
.byte	99
.byte	74
.byte	62
.byte	40
.byte	-22
.byte	-128
.byte	41
.byte	53
.byte	9
.byte	-78
.byte	-15
.byte	-115
.byte	26
.byte	8
.byte	107
.byte	-122
.byte	-73
.byte	89
.byte	-119
.byte	98
.byte	101
.byte	106
.byte	-91
.byte	-108
.byte	104
.byte	79
.byte	12
.byte	27
.byte	-39
.byte	-1
.byte	87
.byte	17
.byte	7
.byte	74
.byte	43
.byte	26
.byte	-110
.byte	73
.byte	-90
.byte	49
.byte	23
.byte	-99
.byte	65
.byte	38
.byte	105
.byte	-96
.byte	51
.byte	52
.byte	31
.byte	115
.byte	-128
.byte	47
.byte	41
.byte	14
.byte	110
.byte	-74
.byte	-73
.byte	21
.byte	17
.byte	-62
.byte	87
.byte	68
.byte	71
.byte	44
.byte	114
.byte	51
.byte	15
.byte	-70
.byte	23
.byte	66
.byte	45
.byte	25
.byte	102
.byte	-59
.byte	-67
.byte	23
.byte	18
.byte	22
.byte	72
.byte	-69
.byte	100
.byte	-126
.byte	-99
.byte	111
.byte	32
.byte	75
.byte	80
.align	2
.type	vp8_pred4x4_prob_inter, @object
.size	vp8_pred4x4_prob_inter, 9
vp8_pred4x4_prob_inter:
.byte	120
.byte	90
.byte	79
.byte	-123
.byte	87
.byte	85
.byte	80
.byte	111
.byte	-105
.align	2
.type	vp8_pred8x8c_prob_inter, @object
.size	vp8_pred8x8c_prob_inter, 3
vp8_pred8x8c_prob_inter:
.byte	-94
.byte	101
.byte	-52
.align	2
.type	vp8_pred8x8c_prob_intra, @object
.size	vp8_pred8x8c_prob_intra, 3
vp8_pred8x8c_prob_intra:
.byte	-114
.byte	114
.byte	-73
.align	2
.type	vp8_pred8x8c_tree, @object
.size	vp8_pred8x8c_tree, 6
vp8_pred8x8c_tree:
.byte	0
.byte	1
.byte	-2
.byte	2
.byte	-1
.byte	-3
.align	2
.type	vp8_pred4x4_tree, @object
.size	vp8_pred4x4_tree, 18
vp8_pred4x4_tree:
.byte	-2
.byte	1
.byte	-9
.byte	2
.byte	0
.byte	3
.byte	4
.byte	6
.byte	-1
.byte	5
.byte	-4
.byte	-5
.byte	-3
.byte	7
.byte	-7
.byte	8
.byte	-6
.byte	-8
.align	2
.type	vp8_pred16x16_prob_inter, @object
.size	vp8_pred16x16_prob_inter, 4
vp8_pred16x16_prob_inter:
.byte	112
.byte	86
.byte	-116
.byte	37
.align	2
.type	vp8_pred16x16_prob_intra, @object
.size	vp8_pred16x16_prob_intra, 4
vp8_pred16x16_prob_intra:
.byte	-111
.byte	-100
.byte	-93
.byte	-128
.align	2
.type	vp8_submv_prob, @object
.size	vp8_submv_prob, 15
vp8_submv_prob:
.byte	-109
.byte	-120
.byte	18
.byte	106
.byte	-111
.byte	1
.byte	-77
.byte	121
.byte	1
.byte	-33
.byte	1
.byte	34
.byte	-48
.byte	1
.byte	1
.align	2
.type	vp8_mbsplit_count, @object
.size	vp8_mbsplit_count, 4
vp8_mbsplit_count:
.byte	2
.byte	2
.byte	4
.byte	16
.align	2
.type	vp8_mbfirstidx, @object
.size	vp8_mbfirstidx, 64
vp8_mbfirstidx:
.byte	0
.byte	8
.space	14
.byte	0
.byte	2
.space	14
.byte	0
.byte	2
.byte	8
.byte	10
.space	12
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.align	2
.type	vp8_mbsplits, @object
.size	vp8_mbsplits, 80
vp8_mbsplits:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	2
.byte	2
.byte	3
.byte	3
.byte	2
.byte	2
.byte	3
.byte	3
.byte	0
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	vp8_mode_contexts, @object
.size	vp8_mode_contexts, 96
vp8_mode_contexts:
.word	7
.word	1
.word	1
.word	143
.word	14
.word	18
.word	14
.word	107
.word	135
.word	64
.word	57
.word	68
.word	60
.word	56
.word	128
.word	65
.word	159
.word	134
.word	128
.word	34
.word	234
.word	188
.word	128
.word	28
.align	2
.type	vp8_pred16x16_tree_inter, @object
.size	vp8_pred16x16_tree_inter, 8
vp8_pred16x16_tree_inter:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	-2
.byte	-1
.byte	-3
.byte	-4
.align	2
.type	vp8_pred16x16_tree_intra, @object
.size	vp8_pred16x16_tree_intra, 8
vp8_pred16x16_tree_intra:
.byte	-4
.byte	1
.byte	2
.byte	3
.byte	0
.byte	-2
.byte	-1
.byte	-3
.align	2
.type	vp8_pred4x4_mode, @object
.size	vp8_pred4x4_mode, 4
vp8_pred4x4_mode:
.byte	2
.byte	1
.byte	0
.byte	9
.section	.data.rel.ro,"aw",@progbits
.align	2
$LC12:
.word	ac_qlookup+1289191424
.align	2
$LC13:
.word	ac2q_lookup+1289191296
.align	2
$LC14:
.word	dc_qlookup+1289191168
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
