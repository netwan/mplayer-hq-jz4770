.file	1 "vp8dsp.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.vp8_luma_dc_wht_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_luma_dc_wht_c
.type	vp8_luma_dc_wht_c, @function
vp8_luma_dc_wht_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$9,$5,8
move	$2,$5
$L2:
lh	$6,0($2)
addiu	$2,$2,2
lh	$3,22($2)
lh	$11,14($2)
lh	$7,6($2)
addu	$8,$6,$3
subu	$6,$6,$3
addu	$10,$7,$11
subu	$7,$7,$11
andi	$3,$6,0xffff
andi	$8,$8,0xffff
andi	$10,$10,0xffff
andi	$6,$7,0xffff
addu	$11,$10,$8
addu	$7,$6,$3
subu	$8,$8,$10
subu	$3,$3,$6
sh	$11,-2($2)
sh	$7,6($2)
sh	$8,14($2)
bne	$2,$9,$L2
sh	$3,22($2)

addiu	$10,$4,512
$L3:
lh	$8,6($5)
addiu	$4,$4,128
lh	$3,0($5)
lh	$7,2($5)
lh	$2,4($5)
addu	$6,$3,$8
sh	$0,0($5)
subu	$3,$3,$8
sh	$0,2($5)
sh	$0,4($5)
addu	$8,$7,$2
sh	$0,6($5)
addiu	$6,$6,3
subu	$5,$7,$2
addiu	$2,$3,3
addu	$7,$6,$8
addu	$3,$2,$5
subu	$6,$6,$8
subu	$2,$2,$5
sra	$3,$3,3
sra	$5,$7,3
sra	$6,$6,3
sra	$2,$2,3
sh	$3,-96($4)
sh	$5,-128($4)
move	$5,$9
sh	$6,-64($4)
addiu	$9,$9,8
bne	$4,$10,$L3
sh	$2,-32($4)

j	$31
nop

.set	macro
.set	reorder
.end	vp8_luma_dc_wht_c
.size	vp8_luma_dc_wht_c, .-vp8_luma_dc_wht_c
.section	.text.vp8_luma_dc_wht_dc_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_luma_dc_wht_dc_c
.type	vp8_luma_dc_wht_dc_c, @function
vp8_luma_dc_wht_dc_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lh	$2,0($5)
addiu	$3,$4,512
sh	$0,0($5)
addiu	$2,$2,3
sra	$2,$2,3
$L9:
sh	$2,0($4)
addiu	$4,$4,128
sh	$2,-96($4)
sh	$2,-64($4)
bne	$4,$3,$L9
sh	$2,-32($4)

j	$31
nop

.set	macro
.set	reorder
.end	vp8_luma_dc_wht_dc_c
.size	vp8_luma_dc_wht_dc_c, .-vp8_luma_dc_wht_dc_c
.section	.text.vp8_idct_add_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_idct_add_c
.type	vp8_idct_add_c, @function
vp8_idct_add_c:
.frame	$sp,56,$31		# vars= 32, regs= 4/0, args= 0, gp= 8
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
li	$13,20091			# 0x4e7b
addiu	$8,$sp,8
.cprestore	0
addiu	$14,$sp,40
sw	$19,52($sp)
li	$12,35468			# 0x8a8c
sw	$18,48($sp)
sw	$17,44($sp)
move	$9,$8
sw	$16,40($sp)
$L12:
lh	$11,24($5)
addiu	$5,$5,2
lh	$2,6($5)
addiu	$9,$9,8
mul	$3,$11,$13
mul	$16,$2,$13
mul	$15,$11,$12
lh	$18,-2($5)
mul	$7,$2,$12
lh	$17,14($5)
sh	$0,-2($5)
sh	$0,6($5)
addu	$10,$18,$17
sh	$0,14($5)
sra	$3,$3,16
sh	$0,22($5)
sra	$16,$16,16
sra	$15,$15,16
sra	$7,$7,16
addu	$3,$11,$3
addu	$2,$2,$16
subu	$3,$7,$3
addu	$2,$2,$15
subu	$17,$18,$17
andi	$10,$10,0xffff
andi	$2,$2,0xffff
andi	$7,$17,0xffff
andi	$3,$3,0xffff
addu	$15,$2,$10
addu	$11,$3,$7
subu	$2,$10,$2
subu	$3,$7,$3
sh	$15,-8($9)
sh	$11,-6($9)
sh	$2,-2($9)
bne	$9,$14,$L12
sh	$3,-4($9)

lw	$12,%got(ff_cropTbl)($28)
addiu	$15,$sp,16
li	$14,35468			# 0x8a8c
li	$13,20091			# 0x4e7b
addiu	$12,$12,1024
$L13:
lh	$16,8($8)
addiu	$8,$8,2
lbu	$17,0($4)
lbu	$24,1($4)
mul	$9,$16,$13
lbu	$11,2($4)
lh	$18,22($8)
mul	$5,$16,$14
lh	$19,-2($8)
lh	$2,14($8)
mul	$25,$18,$14
lbu	$10,3($4)
mul	$3,$18,$13
addu	$7,$19,$2
sra	$9,$9,16
sra	$5,$5,16
addu	$16,$16,$9
subu	$2,$19,$2
sra	$25,$25,16
sra	$3,$3,16
addu	$9,$16,$25
addu	$3,$18,$3
addu	$25,$7,$9
subu	$3,$5,$3
addiu	$25,$25,4
addu	$5,$2,$3
sra	$25,$25,3
addiu	$5,$5,4
addu	$25,$17,$25
sra	$5,$5,3
addu	$25,$12,$25
subu	$3,$2,$3
addu	$5,$24,$5
lbu	$2,0($25)
addiu	$3,$3,4
addu	$5,$12,$5
sra	$3,$3,3
sb	$2,0($4)
subu	$2,$7,$9
lbu	$5,0($5)
addu	$3,$11,$3
addiu	$2,$2,4
addu	$3,$12,$3
sra	$2,$2,3
sb	$5,1($4)
lbu	$3,0($3)
addu	$2,$10,$2
addu	$2,$12,$2
sb	$3,2($4)
lbu	$2,0($2)
sb	$2,3($4)
bne	$8,$15,$L13
addu	$4,$4,$6

lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	vp8_idct_add_c
.size	vp8_idct_add_c, .-vp8_idct_add_c
.section	.text.vp8_idct_dc_add_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_idct_dc_add_c
.type	vp8_idct_dc_add_c, @function
vp8_idct_dc_add_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lh	$2,0($5)
li	$9,4			# 0x4
sh	$0,0($5)
addiu	$2,$2,4
sra	$2,$2,3
addiu	$3,$2,1024
lw	$2,%got(ff_cropTbl)($28)
addu	$2,$2,$3
$L18:
lbu	$8,0($4)
addiu	$9,$9,-1
lbu	$7,1($4)
lbu	$5,2($4)
addu	$8,$2,$8
lbu	$3,3($4)
addu	$7,$2,$7
addu	$5,$2,$5
lbu	$8,0($8)
addu	$3,$2,$3
sb	$8,0($4)
lbu	$7,0($7)
sb	$7,1($4)
lbu	$5,0($5)
sb	$5,2($4)
lbu	$3,0($3)
sb	$3,3($4)
bne	$9,$0,$L18
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	vp8_idct_dc_add_c
.size	vp8_idct_dc_add_c, .-vp8_idct_dc_add_c
.section	.text.vp8_idct_dc_add4uv_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_idct_dc_add4uv_c
.type	vp8_idct_dc_add4uv_c, @function
vp8_idct_dc_add4uv_c:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$16,24($sp)
lw	$16,%got(vp8_idct_dc_add_c)($28)
.cprestore	16
sw	$31,44($sp)
addiu	$16,$16,%lo(vp8_idct_dc_add_c)
sw	$20,40($sp)
sw	$19,36($sp)
move	$20,$5
move	$19,$4
sw	$18,32($sp)
sw	$17,28($sp)
move	$25,$16
move	$17,$6
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jalr	$25
sll	$18,$6,2

addiu	$4,$19,4
addiu	$5,$20,32
move	$25,$16
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jalr	$25
move	$6,$17

addu	$4,$19,$18
addiu	$5,$20,64
move	$25,$16
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jalr	$25
move	$6,$17

addiu	$4,$18,4
lw	$28,16($sp)
addiu	$5,$20,96
lw	$31,44($sp)
addu	$4,$19,$4
lw	$20,40($sp)
move	$6,$17
lw	$19,36($sp)
move	$25,$16
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jr	$25
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	vp8_idct_dc_add4uv_c
.size	vp8_idct_dc_add4uv_c, .-vp8_idct_dc_add4uv_c
.section	.text.vp8_idct_dc_add4y_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_idct_dc_add4y_c
.type	vp8_idct_dc_add4y_c, @function
vp8_idct_dc_add4y_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-48
sw	$16,28($sp)
lw	$16,%got(vp8_idct_dc_add_c)($28)
.cprestore	16
sw	$31,44($sp)
addiu	$16,$16,%lo(vp8_idct_dc_add_c)
sw	$19,40($sp)
sw	$18,36($sp)
move	$19,$4
move	$18,$5
sw	$17,32($sp)
move	$25,$16
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jalr	$25
move	$17,$6

addiu	$4,$19,4
addiu	$5,$18,32
move	$25,$16
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jalr	$25
move	$6,$17

addiu	$4,$19,8
addiu	$5,$18,64
move	$25,$16
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jalr	$25
move	$6,$17

addiu	$4,$19,12
lw	$28,16($sp)
addiu	$5,$18,96
lw	$31,44($sp)
move	$6,$17
lw	$19,40($sp)
move	$25,$16
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.reloc	1f,R_MIPS_JALR,vp8_idct_dc_add_c
1:	jr	$25
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	vp8_idct_dc_add4y_c
.size	vp8_idct_dc_add4y_c, .-vp8_idct_dc_add4y_c
.section	.text.vp8_v_loop_filter16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_v_loop_filter16_c
.type	vp8_v_loop_filter16_c, @function
vp8_v_loop_filter16_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
move	$15,$4
sw	$fp,52($sp)
sll	$fp,$5,1
subu	$3,$0,$5
sw	$22,44($sp)
addu	$fp,$fp,$5
sw	$16,20($sp)
subu	$2,$15,$5
.cprestore	0
subu	$4,$4,$fp
sw	$23,48($sp)
sw	$21,40($sp)
addu	$24,$15,$5
sw	$20,36($sp)
addu	$25,$4,$5
sw	$19,32($sp)
sll	$3,$3,2
sw	$18,28($sp)
addiu	$2,$2,16
sw	$17,24($sp)
addu	$13,$25,$5
sw	$3,12($sp)
addu	$16,$24,$5
sw	$2,8($sp)
lw	$22,%got(ff_cropTbl)($28)
addiu	$22,$22,1024
$L39:
lw	$3,12($sp)
addu	$2,$13,$5
lbu	$11,0($25)
lbu	$10,0($13)
addu	$8,$2,$3
lbu	$12,0($15)
lbu	$17,0($24)
addu	$3,$2,$fp
lbu	$14,0($4)
subu	$2,$10,$12
lbu	$9,0($8)
subu	$8,$11,$17
lbu	$23,0($3)
subu	$19,$0,$2
subu	$3,$0,$8
slt	$20,$2,0
slt	$18,$8,0
movn	$2,$19,$20
movz	$3,$8,$18
sll	$2,$2,1
sra	$3,$3,1
addu	$2,$2,$3
slt	$2,$6,$2
bne	$2,$0,$L25
lbu	$21,0($16)

subu	$9,$9,$14
subu	$2,$0,$9
slt	$3,$9,0
movn	$9,$2,$3
slt	$9,$7,$9
bne	$9,$0,$L25
subu	$2,$14,$11

subu	$3,$0,$2
slt	$9,$2,0
movn	$2,$3,$9
slt	$2,$7,$2
bne	$2,$0,$L25
subu	$3,$11,$10

subu	$2,$0,$3
slt	$9,$3,0
movn	$3,$2,$9
slt	$2,$7,$3
bne	$2,$0,$L58
lw	$18,8($sp)

subu	$23,$23,$21
subu	$2,$0,$23
slt	$9,$23,0
movn	$23,$2,$9
slt	$23,$7,$23
bne	$23,$0,$L58
subu	$2,$21,$17

subu	$9,$0,$2
slt	$18,$2,0
movn	$2,$9,$18
slt	$2,$7,$2
bne	$2,$0,$L58
lw	$18,8($sp)

subu	$9,$17,$12
subu	$2,$0,$9
slt	$18,$9,0
movn	$9,$2,$18
slt	$2,$7,$9
bne	$2,$0,$L58
lw	$18,8($sp)

lw	$18,72($sp)
slt	$2,$18,$3
bne	$2,$0,$L59
lw	$18,%got(ff_cropTbl)($28)

lw	$18,72($sp)
slt	$2,$18,$9
bne	$2,$0,$L35
lw	$18,%got(ff_cropTbl)($28)

lw	$3,%got(ff_cropTbl)($28)
subu	$2,$12,$10
addiu	$3,$3,1152
addu	$8,$3,$8
sll	$3,$2,1
lbu	$8,0($8)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$22,$2
lbu	$18,0($2)
addiu	$18,$18,-128
sll	$3,$18,3
sll	$2,$18,1
addu	$3,$3,$18
sll	$8,$18,4
addiu	$3,$3,63
addu	$8,$2,$8
sra	$3,$3,7
sll	$9,$18,2
sll	$2,$18,5
addu	$14,$14,$3
addiu	$8,$8,63
subu	$2,$2,$9
addu	$14,$22,$14
sra	$8,$8,7
subu	$2,$2,$18
lbu	$9,0($14)
addu	$11,$11,$8
addiu	$2,$2,63
addu	$11,$22,$11
sra	$2,$2,7
sb	$9,0($4)
subu	$17,$17,$8
addu	$10,$10,$2
lbu	$8,0($11)
subu	$12,$12,$2
addu	$10,$22,$10
addu	$12,$22,$12
sb	$8,0($25)
addu	$17,$22,$17
lbu	$2,0($10)
subu	$21,$21,$3
addu	$21,$22,$21
sb	$2,0($13)
lbu	$2,0($12)
sb	$2,0($15)
lbu	$2,0($17)
sb	$2,0($24)
lbu	$2,0($21)
sb	$2,0($16)
$L25:
lw	$18,8($sp)
$L58:
addiu	$13,$13,1
addiu	$4,$4,1
addiu	$25,$25,1
addiu	$15,$15,1
addiu	$24,$24,1
bne	$13,$18,$L39
addiu	$16,$16,1

lw	$fp,52($sp)
$L57:
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

$L35:
$L59:
subu	$2,$12,$10
sll	$3,$2,1
addiu	$18,$18,1152
addu	$2,$3,$2
addu	$8,$18,$8
lbu	$8,0($8)
addu	$2,$2,$8
addu	$2,$22,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L54
li	$8,124			# 0x7c

addiu	$3,$2,-124
sra	$3,$3,3
$L37:
addiu	$2,$2,-125
sra	$2,$2,3
$L38:
addu	$10,$10,$2
lw	$18,8($sp)
subu	$12,$12,$3
addu	$10,$22,$10
addu	$12,$22,$12
addiu	$13,$13,1
lbu	$2,0($10)
addiu	$4,$4,1
addiu	$25,$25,1
addiu	$15,$15,1
addiu	$24,$24,1
sb	$2,-1($13)
addiu	$16,$16,1
lbu	$2,0($12)
bne	$13,$18,$L39
sb	$2,-1($15)

b	$L57
lw	$fp,52($sp)

$L54:
beq	$3,$8,$L56
nop

li	$3,15			# 0xf
b	$L38
li	$2,15			# 0xf

$L56:
b	$L37
li	$3,15			# 0xf

.set	macro
.set	reorder
.end	vp8_v_loop_filter16_c
.size	vp8_v_loop_filter16_c, .-vp8_v_loop_filter16_c
.section	.text.vp8_v_loop_filter16_inner_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_v_loop_filter16_inner_c
.type	vp8_v_loop_filter16_inner_c, @function
vp8_v_loop_filter16_inner_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
subu	$2,$4,$5
sw	$fp,52($sp)
sll	$fp,$5,1
sw	$22,44($sp)
subu	$22,$0,$5
sw	$23,48($sp)
addu	$23,$fp,$5
sw	$16,20($sp)
sll	$16,$22,1
sw	$18,28($sp)
subu	$3,$0,$23
sw	$17,24($sp)
addu	$16,$4,$16
.cprestore	0
addiu	$2,$2,16
sw	$21,40($sp)
sll	$22,$22,2
sw	$20,36($sp)
addu	$12,$16,$5
sw	$19,32($sp)
addu	$17,$4,$5
sw	$3,12($sp)
move	$25,$4
sw	$2,8($sp)
lw	$18,%got(ff_cropTbl)($28)
addiu	$18,$18,1024
$L77:
lw	$4,12($sp)
addu	$3,$12,$5
lbu	$11,0($16)
lbu	$13,0($12)
addu	$8,$3,$22
lbu	$14,0($25)
addu	$10,$3,$4
lbu	$15,0($17)
addu	$4,$3,$fp
addu	$3,$3,$23
lbu	$8,0($8)
subu	$2,$13,$14
lbu	$10,0($10)
subu	$9,$11,$15
lbu	$24,0($4)
subu	$20,$0,$2
subu	$4,$0,$9
slt	$21,$2,0
slt	$19,$9,0
movn	$2,$20,$21
movz	$4,$9,$19
sll	$2,$2,1
sra	$4,$4,1
addu	$2,$2,$4
slt	$2,$6,$2
bne	$2,$0,$L61
lbu	$3,0($3)

subu	$8,$8,$10
subu	$2,$0,$8
slt	$4,$8,0
movn	$8,$2,$4
slt	$8,$7,$8
bne	$8,$0,$L103
lw	$2,8($sp)

subu	$10,$10,$11
subu	$2,$0,$10
slt	$4,$10,0
movn	$10,$2,$4
slt	$10,$7,$10
bne	$10,$0,$L103
lw	$2,8($sp)

subu	$2,$11,$13
subu	$4,$0,$2
slt	$8,$2,0
movn	$2,$4,$8
slt	$4,$7,$2
bne	$4,$0,$L61
subu	$3,$3,$24

subu	$4,$0,$3
slt	$8,$3,0
movn	$3,$4,$8
slt	$3,$7,$3
bne	$3,$0,$L61
subu	$24,$24,$15

subu	$3,$0,$24
slt	$4,$24,0
movn	$24,$3,$4
slt	$24,$7,$24
bne	$24,$0,$L61
subu	$3,$15,$14

subu	$4,$0,$3
slt	$8,$3,0
movn	$3,$4,$8
slt	$4,$7,$3
bne	$4,$0,$L61
lw	$4,72($sp)

slt	$2,$4,$2
bne	$2,$0,$L104
lw	$4,%got(ff_cropTbl)($28)

lw	$4,72($sp)
slt	$2,$4,$3
bne	$2,$0,$L71
lw	$4,%got(ff_cropTbl)($28)

subu	$3,$14,$13
sll	$2,$3,1
addu	$2,$2,$3
lw	$3,%got(ff_cropTbl)($28)
addiu	$3,$3,1152
addu	$2,$3,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L97
li	$4,124			# 0x7c

addiu	$4,$2,-124
sra	$4,$4,3
addiu	$3,$4,1
sra	$3,$3,1
$L75:
addiu	$2,$2,-125
sra	$2,$2,3
$L76:
addu	$13,$13,$2
subu	$14,$14,$4
addu	$13,$18,$13
addu	$14,$18,$14
addu	$11,$11,$3
lbu	$2,0($13)
subu	$15,$15,$3
addu	$11,$18,$11
addu	$15,$18,$15
sb	$2,0($12)
lbu	$2,0($14)
sb	$2,0($25)
lbu	$2,0($11)
sb	$2,0($16)
lbu	$2,0($15)
sb	$2,0($17)
$L61:
lw	$2,8($sp)
$L103:
addiu	$12,$12,1
addiu	$16,$16,1
addiu	$25,$25,1
bne	$12,$2,$L77
addiu	$17,$17,1

lw	$fp,52($sp)
$L102:
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

$L71:
$L104:
subu	$2,$14,$13
sll	$3,$2,1
addiu	$4,$4,1152
addu	$2,$3,$2
addu	$9,$4,$9
lbu	$4,0($9)
addu	$2,$2,$4
addu	$2,$18,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L98
li	$4,124			# 0x7c

addiu	$3,$2,-124
sra	$3,$3,3
$L73:
addiu	$2,$2,-125
sra	$2,$2,3
$L74:
addu	$13,$13,$2
subu	$14,$14,$3
addu	$13,$18,$13
addu	$14,$18,$14
addiu	$12,$12,1
lbu	$2,0($13)
addiu	$16,$16,1
addiu	$25,$25,1
sb	$2,-1($12)
lbu	$2,0($14)
sb	$2,-1($25)
lw	$2,8($sp)
bne	$12,$2,$L77
addiu	$17,$17,1

b	$L102
lw	$fp,52($sp)

$L98:
beq	$3,$4,$L100
nop

li	$3,15			# 0xf
b	$L74
li	$2,15			# 0xf

$L97:
beq	$3,$4,$L101
nop

li	$3,8			# 0x8
li	$4,15			# 0xf
b	$L76
li	$2,15			# 0xf

$L100:
b	$L73
li	$3,15			# 0xf

$L101:
li	$3,8			# 0x8
b	$L75
li	$4,15			# 0xf

.set	macro
.set	reorder
.end	vp8_v_loop_filter16_inner_c
.size	vp8_v_loop_filter16_inner_c, .-vp8_v_loop_filter16_inner_c
.section	.text.vp8_h_loop_filter16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_h_loop_filter16_c
.type	vp8_h_loop_filter16_c, @function
vp8_h_loop_filter16_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
lw	$15,%got(ff_cropTbl)($28)
li	$10,16			# 0x10
sw	$17,8($sp)
sw	$16,4($sp)
addiu	$17,$15,1152
sw	$22,28($sp)
li	$16,124			# 0x7c
sw	$21,24($sp)
addiu	$15,$15,1024
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
lw	$25,48($sp)
$L120:
lbu	$11,-2($4)
lbu	$13,1($4)
lbu	$9,-1($4)
lbu	$12,0($4)
subu	$8,$11,$13
lbu	$19,-4($4)
lbu	$14,-3($4)
subu	$2,$9,$12
lbu	$24,2($4)
subu	$20,$0,$8
slt	$3,$8,0
subu	$21,$0,$2
movz	$20,$8,$3
slt	$22,$2,0
movn	$2,$21,$22
move	$3,$20
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$6,$2
bne	$2,$0,$L106
lbu	$18,3($4)

subu	$2,$19,$14
subu	$3,$0,$2
slt	$19,$2,0
movn	$2,$3,$19
slt	$2,$7,$2
bne	$2,$0,$L106
subu	$2,$14,$11

subu	$3,$0,$2
slt	$19,$2,0
movn	$2,$3,$19
slt	$2,$7,$2
bne	$2,$0,$L106
subu	$3,$11,$9

subu	$2,$0,$3
slt	$19,$3,0
movn	$3,$2,$19
slt	$2,$7,$3
bne	$2,$0,$L106
subu	$18,$18,$24

subu	$2,$0,$18
slt	$19,$18,0
movn	$18,$2,$19
slt	$18,$7,$18
bne	$18,$0,$L106
subu	$2,$24,$13

subu	$18,$0,$2
slt	$19,$2,0
movn	$2,$18,$19
slt	$2,$7,$2
bne	$2,$0,$L106
subu	$2,$13,$12

subu	$18,$0,$2
slt	$19,$2,0
movn	$2,$18,$19
slt	$18,$7,$2
bne	$18,$0,$L106
slt	$3,$25,$3

bne	$3,$0,$L116
slt	$2,$25,$2

bne	$2,$0,$L139
subu	$2,$12,$9

addu	$8,$17,$8
sll	$3,$2,1
lbu	$8,0($8)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$15,$2
lbu	$2,0($2)
addiu	$18,$2,-128
sll	$3,$18,3
sll	$2,$18,1
addu	$3,$3,$18
sll	$19,$18,4
addiu	$3,$3,63
addu	$19,$2,$19
sra	$3,$3,7
sll	$8,$18,5
sll	$2,$18,2
addu	$14,$14,$3
addiu	$19,$19,63
subu	$2,$8,$2
addu	$14,$15,$14
sra	$8,$19,7
subu	$2,$2,$18
lbu	$14,0($14)
addu	$11,$11,$8
addiu	$2,$2,63
addu	$11,$15,$11
sra	$2,$2,7
sb	$14,-3($4)
subu	$13,$13,$8
addu	$9,$9,$2
lbu	$8,0($11)
subu	$12,$12,$2
addu	$9,$15,$9
addu	$12,$15,$12
sb	$8,-2($4)
addu	$13,$15,$13
lbu	$2,0($9)
subu	$24,$24,$3
addu	$24,$15,$24
sb	$2,-1($4)
lbu	$2,0($12)
sb	$2,0($4)
lbu	$2,0($13)
sb	$2,1($4)
lbu	$2,0($24)
sb	$2,2($4)
$L106:
addiu	$10,$10,-1
bne	$10,$0,$L120
addu	$4,$4,$5

lw	$22,28($sp)
$L138:
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

$L116:
subu	$2,$12,$9
$L139:
addu	$8,$17,$8
sll	$3,$2,1
lbu	$8,0($8)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$15,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L135
nop

addiu	$3,$2,-124
sra	$3,$3,3
$L118:
addiu	$2,$2,-125
sra	$2,$2,3
$L119:
addu	$9,$9,$2
subu	$12,$12,$3
addu	$9,$15,$9
addu	$12,$15,$12
addiu	$10,$10,-1
lbu	$2,0($9)
sb	$2,-1($4)
lbu	$2,0($12)
sb	$2,0($4)
bne	$10,$0,$L120
addu	$4,$4,$5

b	$L138
lw	$22,28($sp)

$L135:
beq	$3,$16,$L137
nop

li	$3,15			# 0xf
b	$L119
li	$2,15			# 0xf

$L137:
b	$L118
li	$3,15			# 0xf

.set	macro
.set	reorder
.end	vp8_h_loop_filter16_c
.size	vp8_h_loop_filter16_c, .-vp8_h_loop_filter16_c
.section	.text.vp8_h_loop_filter16_inner_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_h_loop_filter16_inner_c
.type	vp8_h_loop_filter16_inner_c, @function
vp8_h_loop_filter16_inner_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
lw	$15,%got(ff_cropTbl)($28)
li	$12,16			# 0x10
sw	$17,8($sp)
sw	$16,4($sp)
addiu	$17,$15,1152
sw	$22,28($sp)
li	$16,124			# 0x7c
sw	$21,24($sp)
addiu	$15,$15,1024
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
lw	$25,48($sp)
$L157:
lbu	$10,-2($4)
lbu	$14,1($4)
lbu	$11,-1($4)
lbu	$13,0($4)
subu	$8,$10,$14
lbu	$19,-4($4)
lbu	$9,-3($4)
subu	$2,$11,$13
lbu	$18,2($4)
subu	$20,$0,$8
slt	$3,$8,0
subu	$21,$0,$2
movz	$20,$8,$3
slt	$22,$2,0
movn	$2,$21,$22
move	$3,$20
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$6,$2
bne	$2,$0,$L141
lbu	$24,3($4)

subu	$2,$19,$9
subu	$3,$0,$2
slt	$19,$2,0
movn	$2,$3,$19
slt	$2,$7,$2
bne	$2,$0,$L141
subu	$9,$9,$10

subu	$2,$0,$9
slt	$3,$9,0
movn	$9,$2,$3
slt	$9,$7,$9
bne	$9,$0,$L141
subu	$3,$10,$11

subu	$2,$0,$3
slt	$9,$3,0
movn	$3,$2,$9
slt	$2,$7,$3
bne	$2,$0,$L141
subu	$24,$24,$18

subu	$2,$0,$24
slt	$9,$24,0
movn	$24,$2,$9
slt	$24,$7,$24
bne	$24,$0,$L141
subu	$18,$18,$14

subu	$2,$0,$18
slt	$9,$18,0
movn	$18,$2,$9
slt	$18,$7,$18
bne	$18,$0,$L141
subu	$2,$14,$13

subu	$9,$0,$2
slt	$18,$2,0
movn	$2,$9,$18
slt	$9,$7,$2
bne	$9,$0,$L141
slt	$3,$25,$3

bne	$3,$0,$L151
slt	$2,$25,$2

bne	$2,$0,$L183
subu	$2,$13,$11

subu	$3,$13,$11
sll	$2,$3,1
addu	$2,$2,$3
addu	$2,$17,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L177
addiu	$8,$2,-124

sra	$8,$8,3
addiu	$3,$8,1
sra	$3,$3,1
$L155:
addiu	$2,$2,-125
sra	$2,$2,3
$L156:
addu	$11,$11,$2
subu	$13,$13,$8
addu	$11,$15,$11
addu	$13,$15,$13
addu	$10,$10,$3
lbu	$2,0($11)
subu	$14,$14,$3
addu	$10,$15,$10
addu	$14,$15,$14
sb	$2,-1($4)
lbu	$2,0($13)
sb	$2,0($4)
lbu	$2,0($10)
sb	$2,-2($4)
lbu	$2,0($14)
sb	$2,1($4)
$L141:
addiu	$12,$12,-1
bne	$12,$0,$L157
addu	$4,$4,$5

lw	$22,28($sp)
$L182:
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

$L151:
subu	$2,$13,$11
$L183:
addu	$8,$17,$8
sll	$3,$2,1
lbu	$8,0($8)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$15,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L178
nop

addiu	$3,$2,-124
sra	$3,$3,3
$L153:
addiu	$2,$2,-125
sra	$2,$2,3
$L154:
addu	$11,$11,$2
subu	$13,$13,$3
addu	$11,$15,$11
addu	$13,$15,$13
addiu	$12,$12,-1
lbu	$2,0($11)
sb	$2,-1($4)
lbu	$2,0($13)
sb	$2,0($4)
bne	$12,$0,$L157
addu	$4,$4,$5

b	$L182
lw	$22,28($sp)

$L178:
beq	$3,$16,$L180
nop

li	$3,15			# 0xf
b	$L154
li	$2,15			# 0xf

$L177:
beq	$3,$16,$L181
nop

li	$3,8			# 0x8
li	$8,15			# 0xf
b	$L156
li	$2,15			# 0xf

$L180:
b	$L153
li	$3,15			# 0xf

$L181:
li	$3,8			# 0x8
b	$L155
li	$8,15			# 0xf

.set	macro
.set	reorder
.end	vp8_h_loop_filter16_inner_c
.size	vp8_h_loop_filter16_inner_c, .-vp8_h_loop_filter16_inner_c
.section	.text.vp8_v_loop_filter8uv_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_v_loop_filter8uv_c
.type	vp8_v_loop_filter8uv_c, @function
vp8_v_loop_filter8uv_c:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-64
move	$8,$4
sw	$16,28($sp)
sll	$16,$6,1
sw	$20,44($sp)
addu	$13,$8,$6
addu	$16,$16,$6
sw	$fp,60($sp)
.cprestore	0
addu	$15,$13,$6
subu	$2,$0,$16
sw	$23,56($sp)
sw	$22,52($sp)
addiu	$25,$8,8
sw	$21,48($sp)
move	$fp,$7
sw	$2,12($sp)
subu	$2,$0,$6
lw	$3,12($sp)
lw	$20,%got(ff_cropTbl)($28)
sll	$2,$2,2
sw	$19,40($sp)
addu	$4,$4,$3
sw	$18,36($sp)
addiu	$20,$20,1024
sw	$17,32($sp)
addu	$14,$4,$6
sw	$2,8($sp)
sw	$5,68($sp)
move	$24,$2
addu	$12,$14,$6
sw	$6,16($sp)
$L199:
addu	$2,$8,$24
lbu	$9,0($14)
lbu	$7,0($12)
addu	$3,$8,$16
lbu	$10,0($8)
lbu	$17,0($13)
lbu	$6,0($2)
subu	$2,$7,$10
lbu	$23,0($3)
subu	$5,$9,$17
lbu	$11,0($4)
subu	$19,$0,$2
subu	$3,$0,$5
slt	$21,$2,0
slt	$18,$5,0
movn	$2,$19,$21
movz	$3,$5,$18
sll	$2,$2,1
sra	$3,$3,1
addu	$2,$2,$3
slt	$2,$fp,$2
bne	$2,$0,$L185
lbu	$22,0($15)

subu	$6,$6,$11
lw	$18,80($sp)
subu	$2,$0,$6
slt	$3,$6,0
movn	$6,$2,$3
slt	$6,$18,$6
bne	$6,$0,$L185
subu	$2,$11,$9

subu	$3,$0,$2
slt	$6,$2,0
movn	$2,$3,$6
slt	$2,$18,$2
bne	$2,$0,$L185
subu	$3,$9,$7

subu	$2,$0,$3
slt	$6,$3,0
movn	$3,$2,$6
slt	$2,$18,$3
bne	$2,$0,$L185
subu	$23,$23,$22

subu	$2,$0,$23
slt	$6,$23,0
movn	$23,$2,$6
slt	$23,$18,$23
bne	$23,$0,$L185
subu	$2,$22,$17

lw	$19,80($sp)
subu	$6,$0,$2
slt	$18,$2,0
movn	$2,$6,$18
slt	$2,$19,$2
bne	$2,$0,$L185
subu	$6,$17,$10

subu	$2,$0,$6
slt	$18,$6,0
movn	$6,$2,$18
slt	$2,$19,$6
bne	$2,$0,$L185
lw	$18,84($sp)

slt	$2,$18,$3
beq	$2,$0,$L240
lw	$3,84($sp)

lw	$3,%got(ff_cropTbl)($28)
$L251:
subu	$2,$10,$7
addiu	$3,$3,1152
addu	$5,$3,$5
sll	$3,$2,1
lbu	$5,0($5)
addu	$2,$3,$2
addu	$2,$2,$5
addu	$2,$20,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$5,$3,124
beq	$5,$0,$L241
li	$5,124			# 0x7c

addiu	$3,$2,-124
sra	$3,$3,3
$L197:
addiu	$2,$2,-125
sra	$2,$2,3
$L198:
addu	$7,$7,$2
subu	$10,$10,$3
addu	$7,$20,$7
addu	$10,$20,$10
lbu	$2,0($7)
sb	$2,0($12)
lbu	$2,0($10)
sb	$2,0($8)
$L185:
addiu	$8,$8,1
addiu	$4,$4,1
addiu	$14,$14,1
addiu	$12,$12,1
addiu	$13,$13,1
bne	$8,$25,$L199
addiu	$15,$15,1

lw	$6,68($sp)
$L249:
lw	$18,12($sp)
lw	$24,16($sp)
move	$5,$6
addu	$12,$6,$18
lw	$18,%got(ff_cropTbl)($28)
addu	$14,$6,$24
addu	$15,$12,$24
addiu	$23,$18,1152
addu	$13,$15,$24
addu	$6,$14,$24
addiu	$22,$5,8
addiu	$18,$18,1024
$L214:
lw	$19,8($sp)
addu	$3,$5,$16
lbu	$9,0($15)
lbu	$8,0($13)
addu	$2,$5,$19
lbu	$10,0($5)
lbu	$17,0($14)
lbu	$25,0($3)
lbu	$7,0($2)
subu	$2,$8,$10
subu	$4,$9,$17
lbu	$11,0($12)
subu	$20,$0,$2
subu	$3,$0,$4
slt	$21,$2,0
slt	$19,$4,0
movn	$2,$20,$21
movz	$3,$4,$19
sll	$2,$2,1
sra	$3,$3,1
addu	$2,$2,$3
slt	$2,$fp,$2
bne	$2,$0,$L200
lbu	$24,0($6)

subu	$7,$7,$11
subu	$2,$0,$7
slt	$3,$7,0
movn	$7,$2,$3
lw	$2,80($sp)
slt	$7,$2,$7
bne	$7,$0,$L200
subu	$2,$11,$9

subu	$3,$0,$2
slt	$7,$2,0
movn	$2,$3,$7
lw	$3,80($sp)
slt	$2,$3,$2
bne	$2,$0,$L200
subu	$3,$9,$8

lw	$19,80($sp)
subu	$2,$0,$3
slt	$7,$3,0
movn	$3,$2,$7
slt	$2,$19,$3
bne	$2,$0,$L200
subu	$25,$25,$24

subu	$2,$0,$25
slt	$7,$25,0
movn	$25,$2,$7
slt	$25,$19,$25
bne	$25,$0,$L200
subu	$2,$24,$17

subu	$7,$0,$2
slt	$19,$2,0
movn	$2,$7,$19
lw	$19,80($sp)
slt	$2,$19,$2
bne	$2,$0,$L200
nop

subu	$7,$17,$10
subu	$2,$0,$7
slt	$19,$7,0
movn	$7,$2,$19
lw	$19,80($sp)
slt	$2,$19,$7
bne	$2,$0,$L200
lw	$19,84($sp)

slt	$2,$19,$3
beq	$2,$0,$L242
lw	$3,84($sp)

subu	$2,$10,$8
$L250:
addu	$4,$23,$4
sll	$3,$2,1
lbu	$4,0($4)
addu	$2,$3,$2
addu	$2,$2,$4
addu	$2,$18,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L243
li	$4,124			# 0x7c

addiu	$3,$2,-124
sra	$3,$3,3
$L212:
addiu	$2,$2,-125
sra	$2,$2,3
$L213:
addu	$8,$8,$2
subu	$10,$10,$3
addu	$8,$18,$8
addu	$10,$18,$10
lbu	$2,0($8)
sb	$2,0($13)
lbu	$2,0($10)
sb	$2,0($5)
$L200:
addiu	$5,$5,1
addiu	$12,$12,1
addiu	$15,$15,1
addiu	$13,$13,1
addiu	$14,$14,1
bne	$5,$22,$L214
addiu	$6,$6,1

lw	$fp,60($sp)
$L248:
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L242:
slt	$2,$3,$7
bne	$2,$0,$L250
subu	$2,$10,$8

addu	$4,$23,$4
sll	$3,$2,1
addiu	$5,$5,1
lbu	$4,0($4)
addu	$2,$3,$2
addiu	$12,$12,1
addiu	$15,$15,1
addu	$2,$2,$4
addiu	$13,$13,1
addu	$2,$18,$2
addiu	$14,$14,1
addiu	$6,$6,1
lbu	$19,0($2)
addiu	$19,$19,-128
sll	$3,$19,3
sll	$2,$19,1
addu	$3,$3,$19
sll	$4,$19,4
addiu	$3,$3,63
addu	$4,$2,$4
sra	$3,$3,7
sll	$7,$19,2
sll	$2,$19,5
addu	$11,$11,$3
addiu	$4,$4,63
subu	$2,$2,$7
addu	$11,$18,$11
sra	$4,$4,7
subu	$2,$2,$19
lbu	$7,0($11)
addu	$9,$9,$4
addiu	$2,$2,63
addu	$9,$18,$9
sra	$2,$2,7
sb	$7,-1($12)
subu	$17,$17,$4
addu	$8,$8,$2
lbu	$4,0($9)
subu	$10,$10,$2
addu	$8,$18,$8
addu	$10,$18,$10
sb	$4,-1($15)
addu	$17,$18,$17
lbu	$2,0($8)
subu	$24,$24,$3
addu	$24,$18,$24
sb	$2,-1($13)
lbu	$2,0($10)
sb	$2,-1($5)
lbu	$2,0($17)
sb	$2,-1($14)
lbu	$2,0($24)
bne	$5,$22,$L214
sb	$2,-1($6)

b	$L248
lw	$fp,60($sp)

$L240:
slt	$2,$3,$6
bne	$2,$0,$L251
lw	$3,%got(ff_cropTbl)($28)

lw	$19,%got(ff_cropTbl)($28)
subu	$2,$10,$7
addiu	$8,$8,1
sll	$3,$2,1
addiu	$19,$19,1152
addu	$2,$3,$2
addu	$5,$19,$5
addiu	$4,$4,1
addiu	$14,$14,1
lbu	$5,0($5)
addiu	$12,$12,1
addiu	$13,$13,1
addiu	$15,$15,1
addu	$2,$2,$5
addu	$2,$20,$2
lbu	$18,0($2)
addiu	$18,$18,-128
sll	$3,$18,3
sll	$2,$18,1
addu	$3,$3,$18
sll	$5,$18,4
addiu	$3,$3,63
addu	$5,$2,$5
sra	$3,$3,7
sll	$6,$18,2
sll	$2,$18,5
addu	$11,$11,$3
addiu	$5,$5,63
subu	$2,$2,$6
addu	$11,$20,$11
sra	$5,$5,7
subu	$2,$2,$18
lbu	$6,0($11)
addu	$9,$9,$5
addiu	$2,$2,63
addu	$9,$20,$9
sra	$2,$2,7
sb	$6,-1($4)
subu	$17,$17,$5
addu	$7,$7,$2
lbu	$5,0($9)
subu	$10,$10,$2
addu	$7,$20,$7
addu	$10,$20,$10
sb	$5,-1($14)
addu	$17,$20,$17
lbu	$2,0($7)
subu	$22,$22,$3
addu	$22,$20,$22
sb	$2,-1($12)
lbu	$2,0($10)
sb	$2,-1($8)
lbu	$2,0($17)
sb	$2,-1($13)
lbu	$2,0($22)
bne	$8,$25,$L199
sb	$2,-1($15)

b	$L249
lw	$6,68($sp)

$L243:
beq	$3,$4,$L246
nop

li	$3,15			# 0xf
b	$L213
li	$2,15			# 0xf

$L241:
beq	$3,$5,$L247
nop

li	$3,15			# 0xf
b	$L198
li	$2,15			# 0xf

$L246:
b	$L212
li	$3,15			# 0xf

$L247:
b	$L197
li	$3,15			# 0xf

.set	macro
.set	reorder
.end	vp8_v_loop_filter8uv_c
.size	vp8_v_loop_filter8uv_c, .-vp8_v_loop_filter8uv_c
.section	.text.vp8_v_loop_filter8uv_inner_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_v_loop_filter8uv_inner_c
.type	vp8_v_loop_filter8uv_inner_c, @function
vp8_v_loop_filter8uv_inner_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
subu	$2,$0,$6
sw	$16,20($sp)
sll	$16,$6,1
sll	$3,$2,1
sw	$19,32($sp)
sw	$17,24($sp)
addu	$17,$16,$6
sw	$fp,52($sp)
addu	$15,$4,$3
sw	$23,48($sp)
subu	$fp,$0,$17
sw	$18,28($sp)
sll	$23,$2,2
.cprestore	0
addu	$13,$15,$6
sw	$22,44($sp)
addu	$18,$4,$6
sw	$21,40($sp)
addiu	$24,$4,8
sw	$20,36($sp)
sw	$6,64($sp)
sw	$3,8($sp)
sw	$5,60($sp)
lw	$19,%got(ff_cropTbl)($28)
addiu	$19,$19,1024
$L269:
addu	$6,$4,$fp
lbu	$9,0($15)
lbu	$12,0($18)
addu	$2,$4,$23
lbu	$10,0($13)
addu	$14,$4,$16
lbu	$11,0($4)
addu	$3,$4,$17
lbu	$8,0($6)
subu	$6,$9,$12
lbu	$5,0($2)
subu	$2,$10,$11
lbu	$25,0($14)
subu	$14,$0,$6
lbu	$22,0($3)
subu	$20,$0,$2
slt	$3,$6,0
slt	$21,$2,0
movz	$14,$6,$3
movn	$2,$20,$21
move	$3,$14
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$7,$2
bne	$2,$0,$L253
subu	$5,$5,$8

subu	$2,$0,$5
slt	$3,$5,0
movn	$5,$2,$3
lw	$2,72($sp)
slt	$5,$2,$5
bne	$5,$0,$L253
subu	$8,$8,$9

subu	$2,$0,$8
slt	$3,$8,0
movn	$8,$2,$3
lw	$3,72($sp)
slt	$8,$3,$8
bne	$8,$0,$L253
subu	$3,$9,$10

lw	$8,72($sp)
subu	$2,$0,$3
slt	$5,$3,0
movn	$3,$2,$5
slt	$2,$8,$3
bne	$2,$0,$L253
subu	$22,$22,$25

subu	$2,$0,$22
slt	$5,$22,0
movn	$22,$2,$5
slt	$22,$8,$22
bne	$22,$0,$L253
subu	$25,$25,$12

subu	$2,$0,$25
slt	$5,$25,0
movn	$25,$2,$5
slt	$25,$8,$25
bne	$25,$0,$L253
subu	$2,$12,$11

subu	$5,$0,$2
slt	$8,$2,0
movn	$2,$5,$8
lw	$8,72($sp)
slt	$5,$8,$2
bne	$5,$0,$L253
lw	$8,76($sp)

slt	$3,$8,$3
bne	$3,$0,$L332
lw	$5,%got(ff_cropTbl)($28)

lw	$3,76($sp)
slt	$2,$3,$2
bne	$2,$0,$L333
subu	$2,$11,$10

subu	$3,$11,$10
sll	$2,$3,1
addu	$2,$2,$3
lw	$3,%got(ff_cropTbl)($28)
addiu	$3,$3,1152
addu	$2,$3,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$5,$3,124
beq	$5,$0,$L320
li	$8,124			# 0x7c

addiu	$5,$2,-124
sra	$5,$5,3
$L267:
addiu	$3,$2,-125
addiu	$2,$5,1
sra	$3,$3,3
sra	$2,$2,1
$L268:
addu	$10,$10,$3
subu	$11,$11,$5
addu	$10,$19,$10
addu	$11,$19,$11
addu	$9,$9,$2
lbu	$3,0($10)
subu	$12,$12,$2
addu	$9,$19,$9
addu	$12,$19,$12
sb	$3,0($13)
lbu	$2,0($11)
sb	$2,0($4)
lbu	$2,0($9)
sb	$2,0($15)
lbu	$2,0($12)
sb	$2,0($18)
$L253:
addiu	$4,$4,1
addiu	$15,$15,1
addiu	$13,$13,1
bne	$4,$24,$L269
addiu	$18,$18,1

lw	$9,60($sp)
$L330:
lw	$2,8($sp)
lw	$3,64($sp)
lw	$25,%got(ff_cropTbl)($28)
addiu	$22,$9,8
addu	$13,$9,$2
addu	$6,$9,$3
addu	$15,$13,$3
addiu	$25,$25,1024
move	$5,$9
$L286:
addu	$8,$5,$fp
lbu	$10,0($13)
lbu	$14,0($6)
addu	$2,$5,$23
lbu	$11,0($15)
addu	$18,$5,$16
lbu	$12,0($5)
addu	$3,$5,$17
lbu	$9,0($8)
subu	$8,$10,$14
lbu	$4,0($2)
subu	$2,$11,$12
lbu	$24,0($18)
subu	$18,$0,$8
lbu	$21,0($3)
subu	$19,$0,$2
slt	$3,$8,0
slt	$20,$2,0
movz	$18,$8,$3
movn	$2,$19,$20
move	$3,$18
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$7,$2
bne	$2,$0,$L270
subu	$4,$4,$9

subu	$2,$0,$4
slt	$3,$4,0
movn	$4,$2,$3
lw	$2,72($sp)
slt	$4,$2,$4
bne	$4,$0,$L270
subu	$9,$9,$10

subu	$2,$0,$9
slt	$3,$9,0
movn	$9,$2,$3
lw	$3,72($sp)
slt	$9,$3,$9
bne	$9,$0,$L270
subu	$3,$10,$11

lw	$9,72($sp)
subu	$2,$0,$3
slt	$4,$3,0
movn	$3,$2,$4
slt	$2,$9,$3
bne	$2,$0,$L270
subu	$21,$21,$24

subu	$2,$0,$21
slt	$4,$21,0
movn	$21,$2,$4
slt	$21,$9,$21
bne	$21,$0,$L270
subu	$24,$24,$14

subu	$2,$0,$24
slt	$4,$24,0
movn	$24,$2,$4
slt	$24,$9,$24
bne	$24,$0,$L270
subu	$2,$14,$12

subu	$4,$0,$2
slt	$9,$2,0
movn	$2,$4,$9
lw	$9,72($sp)
slt	$4,$9,$2
bne	$4,$0,$L270
lw	$9,76($sp)

slt	$3,$9,$3
bne	$3,$0,$L334
lw	$3,%got(ff_cropTbl)($28)

lw	$3,76($sp)
slt	$2,$3,$2
bne	$2,$0,$L280
lw	$3,%got(ff_cropTbl)($28)

subu	$3,$12,$11
lw	$4,%got(ff_cropTbl)($28)
sll	$2,$3,1
addiu	$4,$4,1152
addu	$2,$2,$3
addu	$2,$4,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L321
addiu	$4,$2,-124

sra	$4,$4,3
$L284:
addiu	$3,$2,-125
addiu	$2,$4,1
sra	$3,$3,3
sra	$2,$2,1
$L285:
addu	$11,$11,$3
subu	$12,$12,$4
addu	$11,$25,$11
addu	$12,$25,$12
addu	$10,$10,$2
lbu	$3,0($11)
subu	$14,$14,$2
addu	$10,$25,$10
addu	$14,$25,$14
sb	$3,0($15)
lbu	$2,0($12)
sb	$2,0($5)
lbu	$2,0($10)
sb	$2,0($13)
lbu	$2,0($14)
sb	$2,0($6)
$L270:
addiu	$5,$5,1
addiu	$13,$13,1
addiu	$15,$15,1
bne	$5,$22,$L286
addiu	$6,$6,1

lw	$fp,52($sp)
$L331:
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

$L332:
subu	$2,$11,$10
$L333:
sll	$3,$2,1
addiu	$5,$5,1152
addu	$2,$3,$2
addu	$6,$5,$6
lbu	$5,0($6)
addu	$2,$2,$5
addu	$2,$19,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$5,$3,124
beq	$5,$0,$L322
li	$5,124			# 0x7c

addiu	$3,$2,-124
sra	$3,$3,3
$L265:
addiu	$2,$2,-125
sra	$2,$2,3
$L266:
addu	$10,$10,$2
subu	$11,$11,$3
addu	$10,$19,$10
addu	$11,$19,$11
addiu	$4,$4,1
lbu	$2,0($10)
addiu	$15,$15,1
addiu	$13,$13,1
addiu	$18,$18,1
sb	$2,-1($13)
lbu	$2,0($11)
bne	$4,$24,$L269
sb	$2,-1($4)

b	$L330
lw	$9,60($sp)

$L280:
$L334:
subu	$2,$12,$11
addiu	$3,$3,1152
addu	$8,$3,$8
sll	$3,$2,1
lbu	$4,0($8)
addu	$2,$3,$2
addu	$2,$2,$4
addu	$2,$25,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L324
li	$4,124			# 0x7c

addiu	$3,$2,-124
sra	$3,$3,3
$L282:
addiu	$2,$2,-125
sra	$2,$2,3
$L283:
addu	$11,$11,$2
subu	$12,$12,$3
addu	$11,$25,$11
addu	$12,$25,$12
addiu	$5,$5,1
lbu	$2,0($11)
addiu	$13,$13,1
addiu	$15,$15,1
addiu	$6,$6,1
sb	$2,-1($15)
lbu	$2,0($12)
bne	$5,$22,$L286
sb	$2,-1($5)

b	$L331
lw	$fp,52($sp)

$L324:
beq	$3,$4,$L326
nop

li	$3,15			# 0xf
b	$L283
li	$2,15			# 0xf

$L320:
beq	$3,$8,$L327
li	$3,15			# 0xf

li	$2,8			# 0x8
b	$L268
li	$5,15			# 0xf

$L322:
beq	$3,$5,$L328
nop

li	$3,15			# 0xf
b	$L266
li	$2,15			# 0xf

$L321:
li	$8,124			# 0x7c
beq	$3,$8,$L329
li	$3,15			# 0xf

li	$2,8			# 0x8
b	$L285
li	$4,15			# 0xf

$L326:
b	$L282
li	$3,15			# 0xf

$L329:
b	$L284
li	$4,15			# 0xf

$L328:
b	$L265
li	$3,15			# 0xf

$L327:
b	$L267
li	$5,15			# 0xf

.set	macro
.set	reorder
.end	vp8_v_loop_filter8uv_inner_c
.size	vp8_v_loop_filter8uv_inner_c, .-vp8_v_loop_filter8uv_inner_c
.section	.text.vp8_h_loop_filter8uv_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_h_loop_filter8uv_c
.type	vp8_h_loop_filter8uv_c, @function
vp8_h_loop_filter8uv_c:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
lw	$25,%got(ff_cropTbl)($28)
li	$13,8			# 0x8
sw	$17,4($sp)
sw	$16,0($sp)
addiu	$17,$25,1152
sw	$23,28($sp)
li	$16,124			# 0x7c
sw	$22,24($sp)
addiu	$25,$25,1024
sw	$21,20($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
lw	$11,48($sp)
lw	$12,52($sp)
$L350:
lbu	$14,-2($4)
lbu	$24,1($4)
lbu	$10,-1($4)
lbu	$15,0($4)
subu	$8,$14,$24
lbu	$9,-4($4)
lbu	$18,-3($4)
subu	$2,$10,$15
lbu	$19,2($4)
subu	$21,$0,$8
slt	$3,$8,0
subu	$22,$0,$2
movz	$21,$8,$3
slt	$23,$2,0
movn	$2,$22,$23
move	$3,$21
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$7,$2
bne	$2,$0,$L336
lbu	$20,3($4)

subu	$9,$9,$18
subu	$2,$0,$9
slt	$3,$9,0
movn	$9,$2,$3
slt	$9,$11,$9
bne	$9,$0,$L336
subu	$2,$18,$14

subu	$3,$0,$2
slt	$9,$2,0
movn	$2,$3,$9
slt	$2,$11,$2
bne	$2,$0,$L336
subu	$3,$14,$10

subu	$2,$0,$3
slt	$9,$3,0
movn	$3,$2,$9
slt	$2,$11,$3
bne	$2,$0,$L336
subu	$20,$20,$19

subu	$2,$0,$20
slt	$9,$20,0
movn	$20,$2,$9
slt	$20,$11,$20
bne	$20,$0,$L336
subu	$2,$19,$24

subu	$9,$0,$2
slt	$20,$2,0
movn	$2,$9,$20
slt	$2,$11,$2
bne	$2,$0,$L336
subu	$2,$24,$15

subu	$9,$0,$2
slt	$20,$2,0
movn	$2,$9,$20
slt	$9,$11,$2
bne	$9,$0,$L336
slt	$3,$12,$3

beq	$3,$0,$L391
slt	$2,$12,$2

subu	$2,$15,$10
$L402:
addu	$8,$17,$8
sll	$3,$2,1
lbu	$8,0($8)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$25,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L392
nop

addiu	$3,$2,-124
sra	$3,$3,3
$L348:
addiu	$2,$2,-125
sra	$2,$2,3
$L349:
addu	$10,$10,$2
subu	$15,$15,$3
addu	$10,$25,$10
addu	$15,$25,$15
lbu	$2,0($10)
sb	$2,-1($4)
lbu	$2,0($15)
sb	$2,0($4)
$L336:
addiu	$13,$13,-1
bne	$13,$0,$L350
addu	$4,$4,$6

lw	$16,%got(ff_cropTbl)($28)
$L400:
li	$9,8			# 0x8
li	$19,124			# 0x7c
addiu	$20,$16,1152
addiu	$16,$16,1024
$L365:
lbu	$10,-2($5)
lbu	$14,1($5)
lbu	$8,-1($5)
lbu	$13,0($5)
subu	$4,$10,$14
lbu	$24,-4($5)
lbu	$15,-3($5)
subu	$2,$8,$13
lbu	$17,2($5)
subu	$25,$0,$4
slt	$3,$4,0
subu	$21,$0,$2
movz	$25,$4,$3
slt	$22,$2,0
movn	$2,$21,$22
move	$3,$25
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$7,$2
bne	$2,$0,$L351
lbu	$18,3($5)

subu	$2,$24,$15
subu	$3,$0,$2
slt	$21,$2,0
movn	$2,$3,$21
slt	$2,$11,$2
bne	$2,$0,$L351
subu	$2,$15,$10

subu	$3,$0,$2
slt	$21,$2,0
movn	$2,$3,$21
slt	$2,$11,$2
bne	$2,$0,$L351
subu	$3,$10,$8

subu	$2,$0,$3
slt	$21,$3,0
movn	$3,$2,$21
slt	$2,$11,$3
bne	$2,$0,$L351
subu	$18,$18,$17

subu	$2,$0,$18
slt	$21,$18,0
movn	$18,$2,$21
slt	$18,$11,$18
bne	$18,$0,$L351
subu	$2,$17,$14

subu	$18,$0,$2
slt	$21,$2,0
movn	$2,$18,$21
slt	$2,$11,$2
bne	$2,$0,$L351
subu	$2,$14,$13

subu	$18,$0,$2
slt	$21,$2,0
movn	$2,$18,$21
slt	$18,$11,$2
bne	$18,$0,$L351
slt	$3,$12,$3

beq	$3,$0,$L393
slt	$2,$12,$2

subu	$2,$13,$8
$L401:
addu	$4,$20,$4
sll	$3,$2,1
lbu	$4,0($4)
addu	$2,$3,$2
addu	$2,$2,$4
addu	$2,$16,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L394
nop

addiu	$3,$2,-124
sra	$3,$3,3
$L363:
addiu	$2,$2,-125
sra	$2,$2,3
$L364:
addu	$8,$8,$2
subu	$13,$13,$3
addu	$8,$16,$8
addu	$13,$16,$13
lbu	$2,0($8)
sb	$2,-1($5)
lbu	$2,0($13)
sb	$2,0($5)
$L351:
addiu	$9,$9,-1
bne	$9,$0,$L365
addu	$5,$5,$6

lw	$23,28($sp)
$L399:
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

$L393:
bne	$2,$0,$L401
subu	$2,$13,$8

addu	$4,$20,$4
sll	$3,$2,1
addiu	$9,$9,-1
lbu	$4,0($4)
addu	$2,$3,$2
addu	$2,$2,$4
addu	$2,$16,$2
lbu	$2,0($2)
addiu	$18,$2,-128
sll	$3,$18,3
sll	$2,$18,1
addu	$3,$3,$18
sll	$21,$18,4
addiu	$3,$3,63
addu	$21,$2,$21
sra	$3,$3,7
sll	$4,$18,5
sll	$2,$18,2
addu	$15,$15,$3
addiu	$21,$21,63
subu	$2,$4,$2
addu	$15,$16,$15
sra	$4,$21,7
subu	$2,$2,$18
lbu	$15,0($15)
addu	$10,$10,$4
addiu	$2,$2,63
addu	$10,$16,$10
sra	$2,$2,7
sb	$15,-3($5)
subu	$14,$14,$4
addu	$8,$8,$2
lbu	$4,0($10)
subu	$13,$13,$2
addu	$8,$16,$8
addu	$13,$16,$13
sb	$4,-2($5)
addu	$14,$16,$14
lbu	$2,0($8)
subu	$17,$17,$3
addu	$17,$16,$17
sb	$2,-1($5)
lbu	$2,0($13)
sb	$2,0($5)
lbu	$2,0($14)
sb	$2,1($5)
lbu	$2,0($17)
sb	$2,2($5)
bne	$9,$0,$L365
addu	$5,$5,$6

b	$L399
lw	$23,28($sp)

$L391:
bne	$2,$0,$L402
subu	$2,$15,$10

addu	$8,$17,$8
sll	$3,$2,1
addiu	$13,$13,-1
lbu	$8,0($8)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$25,$2
lbu	$2,0($2)
addiu	$9,$2,-128
sll	$3,$9,3
sll	$2,$9,1
addu	$3,$3,$9
sll	$20,$9,4
addiu	$3,$3,63
addu	$20,$2,$20
sra	$3,$3,7
sll	$8,$9,5
sll	$2,$9,2
addu	$18,$18,$3
addiu	$20,$20,63
subu	$2,$8,$2
addu	$18,$25,$18
sra	$8,$20,7
subu	$2,$2,$9
addu	$14,$14,$8
lbu	$9,0($18)
addiu	$2,$2,63
addu	$14,$25,$14
sra	$2,$2,7
sb	$9,-3($4)
subu	$24,$24,$8
addu	$10,$10,$2
lbu	$8,0($14)
subu	$15,$15,$2
addu	$10,$25,$10
addu	$15,$25,$15
sb	$8,-2($4)
addu	$24,$25,$24
lbu	$2,0($10)
subu	$19,$19,$3
addu	$19,$25,$19
sb	$2,-1($4)
lbu	$2,0($15)
sb	$2,0($4)
lbu	$2,0($24)
sb	$2,1($4)
lbu	$2,0($19)
sb	$2,2($4)
bne	$13,$0,$L350
addu	$4,$4,$6

b	$L400
lw	$16,%got(ff_cropTbl)($28)

$L394:
beq	$3,$19,$L397
nop

li	$3,15			# 0xf
b	$L364
li	$2,15			# 0xf

$L392:
beq	$3,$16,$L398
nop

li	$3,15			# 0xf
b	$L349
li	$2,15			# 0xf

$L397:
b	$L363
li	$3,15			# 0xf

$L398:
b	$L348
li	$3,15			# 0xf

.set	macro
.set	reorder
.end	vp8_h_loop_filter8uv_c
.size	vp8_h_loop_filter8uv_c, .-vp8_h_loop_filter8uv_c
.section	.text.vp8_h_loop_filter8uv_inner_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_h_loop_filter8uv_inner_c
.type	vp8_h_loop_filter8uv_inner_c, @function
vp8_h_loop_filter8uv_inner_c:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
lw	$25,%got(ff_cropTbl)($28)
li	$14,8			# 0x8
sw	$17,4($sp)
sw	$16,0($sp)
addiu	$17,$25,1152
sw	$23,28($sp)
li	$16,124			# 0x7c
sw	$22,24($sp)
addiu	$25,$25,1024
sw	$21,20($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
lw	$11,48($sp)
lw	$12,52($sp)
$L420:
lbu	$13,-2($4)
lbu	$18,1($4)
lbu	$15,-1($4)
lbu	$24,0($4)
subu	$9,$13,$18
lbu	$8,-4($4)
lbu	$10,-3($4)
subu	$2,$15,$24
lbu	$20,2($4)
subu	$21,$0,$9
slt	$3,$9,0
subu	$22,$0,$2
movz	$21,$9,$3
slt	$23,$2,0
movn	$2,$22,$23
move	$3,$21
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$7,$2
bne	$2,$0,$L404
lbu	$19,3($4)

subu	$8,$8,$10
subu	$2,$0,$8
slt	$3,$8,0
movn	$8,$2,$3
slt	$8,$11,$8
bne	$8,$0,$L404
subu	$10,$10,$13

subu	$2,$0,$10
slt	$3,$10,0
movn	$10,$2,$3
slt	$10,$11,$10
bne	$10,$0,$L404
subu	$3,$13,$15

subu	$2,$0,$3
slt	$8,$3,0
movn	$3,$2,$8
slt	$2,$11,$3
bne	$2,$0,$L404
subu	$19,$19,$20

subu	$2,$0,$19
slt	$8,$19,0
movn	$19,$2,$8
slt	$19,$11,$19
bne	$19,$0,$L404
subu	$20,$20,$18

subu	$2,$0,$20
slt	$8,$20,0
movn	$20,$2,$8
slt	$20,$11,$20
bne	$20,$0,$L404
subu	$2,$18,$24

subu	$8,$0,$2
slt	$10,$2,0
movn	$2,$8,$10
slt	$8,$11,$2
bne	$8,$0,$L404
slt	$3,$12,$3

bne	$3,$0,$L414
slt	$2,$12,$2

bne	$2,$0,$L483
subu	$2,$24,$15

subu	$3,$24,$15
sll	$2,$3,1
addu	$2,$2,$3
addu	$2,$17,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L471
addiu	$8,$2,-124

sra	$8,$8,3
$L418:
addiu	$3,$2,-125
addiu	$2,$8,1
sra	$3,$3,3
sra	$2,$2,1
$L419:
addu	$15,$15,$3
subu	$24,$24,$8
addu	$15,$25,$15
addu	$24,$25,$24
addu	$13,$13,$2
lbu	$3,0($15)
subu	$18,$18,$2
addu	$13,$25,$13
addu	$18,$25,$18
sb	$3,-1($4)
lbu	$2,0($24)
sb	$2,0($4)
lbu	$2,0($13)
sb	$2,-2($4)
lbu	$2,0($18)
sb	$2,1($4)
$L404:
addiu	$14,$14,-1
bne	$14,$0,$L420
addu	$4,$4,$6

lw	$15,%got(ff_cropTbl)($28)
$L481:
li	$10,8			# 0x8
li	$19,124			# 0x7c
addiu	$20,$15,1152
addiu	$15,$15,1024
$L437:
lbu	$9,-2($5)
lbu	$16,1($5)
lbu	$13,-1($5)
lbu	$14,0($5)
subu	$4,$9,$16
lbu	$24,-4($5)
lbu	$8,-3($5)
subu	$2,$13,$14
lbu	$18,2($5)
subu	$25,$0,$4
slt	$3,$4,0
subu	$21,$0,$2
movz	$25,$4,$3
slt	$22,$2,0
movn	$2,$21,$22
move	$3,$25
sra	$3,$3,1
sll	$2,$2,1
addu	$2,$2,$3
slt	$2,$7,$2
bne	$2,$0,$L421
lbu	$17,3($5)

subu	$2,$24,$8
subu	$3,$0,$2
slt	$21,$2,0
movn	$2,$3,$21
slt	$2,$11,$2
bne	$2,$0,$L421
subu	$8,$8,$9

subu	$2,$0,$8
slt	$3,$8,0
movn	$8,$2,$3
slt	$8,$11,$8
bne	$8,$0,$L421
subu	$3,$9,$13

subu	$2,$0,$3
slt	$8,$3,0
movn	$3,$2,$8
slt	$2,$11,$3
bne	$2,$0,$L421
subu	$17,$17,$18

subu	$2,$0,$17
slt	$8,$17,0
movn	$17,$2,$8
slt	$17,$11,$17
bne	$17,$0,$L421
subu	$18,$18,$16

subu	$2,$0,$18
slt	$8,$18,0
movn	$18,$2,$8
slt	$18,$11,$18
bne	$18,$0,$L421
subu	$2,$16,$14

subu	$8,$0,$2
slt	$17,$2,0
movn	$2,$8,$17
slt	$8,$11,$2
bne	$8,$0,$L421
slt	$3,$12,$3

bne	$3,$0,$L431
slt	$2,$12,$2

bne	$2,$0,$L484
subu	$2,$14,$13

subu	$3,$14,$13
sll	$2,$3,1
addu	$2,$2,$3
addu	$2,$20,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L472
addiu	$4,$2,-124

sra	$4,$4,3
$L435:
addiu	$3,$2,-125
addiu	$2,$4,1
sra	$3,$3,3
sra	$2,$2,1
$L436:
addu	$13,$13,$3
subu	$14,$14,$4
addu	$13,$15,$13
addu	$14,$15,$14
addu	$9,$9,$2
lbu	$3,0($13)
subu	$16,$16,$2
addu	$9,$15,$9
addu	$16,$15,$16
sb	$3,-1($5)
lbu	$2,0($14)
sb	$2,0($5)
lbu	$2,0($9)
sb	$2,-2($5)
lbu	$2,0($16)
sb	$2,1($5)
$L421:
addiu	$10,$10,-1
bne	$10,$0,$L437
addu	$5,$5,$6

lw	$23,28($sp)
$L482:
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

$L414:
subu	$2,$24,$15
$L483:
addu	$9,$17,$9
sll	$3,$2,1
lbu	$8,0($9)
addu	$2,$3,$2
addu	$2,$2,$8
addu	$2,$25,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$8,$3,124
beq	$8,$0,$L473
nop

addiu	$3,$2,-124
sra	$3,$3,3
$L416:
addiu	$2,$2,-125
sra	$2,$2,3
$L417:
addu	$15,$15,$2
subu	$24,$24,$3
addu	$15,$25,$15
addu	$24,$25,$24
addiu	$14,$14,-1
lbu	$2,0($15)
sb	$2,-1($4)
lbu	$2,0($24)
sb	$2,0($4)
bne	$14,$0,$L420
addu	$4,$4,$6

b	$L481
lw	$15,%got(ff_cropTbl)($28)

$L431:
subu	$2,$14,$13
$L484:
addu	$4,$20,$4
sll	$3,$2,1
lbu	$4,0($4)
addu	$2,$3,$2
addu	$2,$2,$4
addu	$2,$15,$2
lbu	$2,0($2)
addiu	$3,$2,-128
slt	$4,$3,124
beq	$4,$0,$L475
nop

addiu	$3,$2,-124
sra	$3,$3,3
$L433:
addiu	$2,$2,-125
sra	$2,$2,3
$L434:
addu	$13,$13,$2
subu	$14,$14,$3
addu	$13,$15,$13
addu	$14,$15,$14
addiu	$10,$10,-1
lbu	$2,0($13)
sb	$2,-1($5)
lbu	$2,0($14)
sb	$2,0($5)
bne	$10,$0,$L437
addu	$5,$5,$6

b	$L482
lw	$23,28($sp)

$L475:
beq	$3,$19,$L477
nop

li	$3,15			# 0xf
b	$L434
li	$2,15			# 0xf

$L471:
beq	$3,$16,$L478
li	$3,15			# 0xf

li	$2,8			# 0x8
b	$L419
li	$8,15			# 0xf

$L473:
beq	$3,$16,$L479
nop

li	$3,15			# 0xf
b	$L417
li	$2,15			# 0xf

$L472:
beq	$3,$19,$L480
li	$3,15			# 0xf

li	$2,8			# 0x8
b	$L436
li	$4,15			# 0xf

$L477:
b	$L433
li	$3,15			# 0xf

$L480:
b	$L435
li	$4,15			# 0xf

$L479:
b	$L416
li	$3,15			# 0xf

$L478:
b	$L418
li	$8,15			# 0xf

.set	macro
.set	reorder
.end	vp8_h_loop_filter8uv_inner_c
.size	vp8_h_loop_filter8uv_inner_c, .-vp8_h_loop_filter8uv_inner_c
.section	.text.vp8_v_loop_filter_simple_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_v_loop_filter_simple_c
.type	vp8_v_loop_filter_simple_c, @function
vp8_v_loop_filter_simple_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$11,%got(ff_cropTbl)($28)
subu	$12,$4,$5
subu	$13,$0,$5
addiu	$sp,$sp,-8
move	$10,$4
addiu	$14,$11,1152
sw	$16,4($sp)
move	$4,$12
sll	$13,$13,1
addiu	$12,$12,16
addiu	$11,$11,1024
li	$15,124			# 0x7c
$L492:
addu	$2,$4,$5
lbu	$9,0($4)
lbu	$8,0($10)
addu	$7,$2,$5
addu	$3,$2,$13
subu	$2,$9,$8
lbu	$25,0($7)
subu	$24,$8,$9
lbu	$3,0($3)
subu	$7,$0,$2
sll	$16,$24,1
subu	$3,$3,$25
slt	$25,$2,0
movn	$2,$7,$25
subu	$25,$0,$3
slt	$7,$3,0
addu	$24,$16,$24
addu	$16,$14,$3
movn	$3,$25,$7
sll	$2,$2,1
sra	$3,$3,1
addu	$2,$2,$3
slt	$2,$6,$2
bne	$2,$0,$L486
li	$3,15			# 0xf

lbu	$2,0($16)
li	$7,15			# 0xf
addu	$2,$24,$2
addu	$2,$11,$2
lbu	$2,0($2)
addiu	$16,$2,-128
slt	$24,$16,124
beq	$24,$0,$L489
addiu	$25,$2,-124

sra	$3,$25,3
$L490:
addiu	$2,$2,-125
sra	$7,$2,3
addu	$9,$9,$7
$L496:
subu	$8,$8,$3
addu	$9,$11,$9
addu	$8,$11,$8
lbu	$2,0($9)
sb	$2,0($4)
lbu	$2,0($8)
sb	$2,0($10)
$L486:
addiu	$4,$4,1
bne	$4,$12,$L492
addiu	$10,$10,1

lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

$L489:
bne	$16,$15,$L496
addu	$9,$9,$7

subu	$9,$9,$7
b	$L490
li	$3,15			# 0xf

.set	macro
.set	reorder
.end	vp8_v_loop_filter_simple_c
.size	vp8_v_loop_filter_simple_c, .-vp8_v_loop_filter_simple_c
.section	.text.vp8_h_loop_filter_simple_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	vp8_h_loop_filter_simple_c
.type	vp8_h_loop_filter_simple_c, @function
vp8_h_loop_filter_simple_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$12,%got(ff_cropTbl)($28)
addiu	$sp,$sp,-8
li	$11,16			# 0x10
li	$14,124			# 0x7c
sw	$16,4($sp)
addiu	$13,$12,1152
addiu	$12,$12,1024
$L504:
lbu	$9,-1($4)
lbu	$8,0($4)
lbu	$15,1($4)
lbu	$3,-2($4)
subu	$10,$8,$9
subu	$2,$9,$8
subu	$3,$3,$15
sll	$7,$10,1
subu	$24,$0,$2
subu	$15,$0,$3
addu	$10,$7,$10
slt	$25,$2,0
slt	$7,$3,0
movn	$2,$24,$25
addu	$16,$13,$3
movn	$3,$15,$7
sll	$2,$2,1
sra	$3,$3,1
addu	$2,$2,$3
slt	$2,$6,$2
bne	$2,$0,$L498
li	$3,15			# 0xf

lbu	$7,0($16)
li	$15,15			# 0xf
addu	$7,$10,$7
addu	$7,$12,$7
lbu	$2,0($7)
addiu	$7,$2,-128
slt	$10,$7,124
beq	$10,$0,$L501
addiu	$16,$2,-124

sra	$3,$16,3
$L502:
addiu	$2,$2,-125
sra	$15,$2,3
addu	$9,$9,$15
$L508:
subu	$8,$8,$3
addu	$9,$12,$9
addu	$8,$12,$8
lbu	$2,0($9)
sb	$2,-1($4)
lbu	$2,0($8)
sb	$2,0($4)
$L498:
addiu	$11,$11,-1
bne	$11,$0,$L504
addu	$4,$4,$5

lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

$L501:
bne	$7,$14,$L508
addu	$9,$9,$15

subu	$9,$9,$15
b	$L502
li	$3,15			# 0xf

.set	macro
.set	reorder
.end	vp8_h_loop_filter_simple_c
.size	vp8_h_loop_filter_simple_c, .-vp8_h_loop_filter_simple_c
.section	.text.put_vp8_epel16_h4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_h4_c
.type	put_vp8_epel16_h4_c, @function
put_vp8_epel16_h4_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$2,28($sp)
lw	$25,24($sp)
addiu	$2,$2,-1
sll	$10,$2,1
sll	$2,$2,3
subu	$2,$2,$10
lw	$10,%got(subpel_filters)($28)
addiu	$10,$10,%lo(subpel_filters)
blez	$25,$L509
addu	$10,$10,$2

lw	$24,%got(ff_cropTbl)($28)
move	$16,$0
addiu	$24,$24,1024
$L512:
addiu	$15,$6,16
move	$8,$6
move	$11,$4
$L511:
lbu	$3,0($8)
addiu	$8,$8,1
lbu	$9,2($10)
addiu	$11,$11,1
lbu	$2,1($10)
lbu	$14,-2($8)
mult	$9,$3
lbu	$13,0($8)
lbu	$3,3($10)
lbu	$12,1($8)
msub	$2,$14
lbu	$2,4($10)
madd	$3,$13
msub	$2,$12
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$24,$2
lbu	$2,0($2)
bne	$15,$8,$L511
sb	$2,-1($11)

addiu	$16,$16,1
addu	$4,$4,$5
bne	$16,$25,$L512
addu	$6,$6,$7

$L509:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_vp8_epel16_h4_c
.size	put_vp8_epel16_h4_c, .-put_vp8_epel16_h4_c
.section	.text.put_vp8_epel8_h4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_h4_c
.type	put_vp8_epel8_h4_c, @function
put_vp8_epel8_h4_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$2,28($sp)
lw	$25,24($sp)
addiu	$2,$2,-1
sll	$10,$2,1
sll	$2,$2,3
subu	$2,$2,$10
lw	$10,%got(subpel_filters)($28)
addiu	$10,$10,%lo(subpel_filters)
blez	$25,$L517
addu	$10,$10,$2

lw	$24,%got(ff_cropTbl)($28)
move	$16,$0
addiu	$24,$24,1024
$L520:
addiu	$15,$6,8
move	$8,$6
move	$11,$4
$L519:
lbu	$3,0($8)
addiu	$8,$8,1
lbu	$9,2($10)
addiu	$11,$11,1
lbu	$2,1($10)
lbu	$14,-2($8)
mult	$9,$3
lbu	$13,0($8)
lbu	$3,3($10)
lbu	$12,1($8)
msub	$2,$14
lbu	$2,4($10)
madd	$3,$13
msub	$2,$12
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$24,$2
lbu	$2,0($2)
bne	$15,$8,$L519
sb	$2,-1($11)

addiu	$16,$16,1
addu	$4,$4,$5
bne	$16,$25,$L520
addu	$6,$6,$7

$L517:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_vp8_epel8_h4_c
.size	put_vp8_epel8_h4_c, .-put_vp8_epel8_h4_c
.section	.text.put_vp8_epel4_h4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_h4_c
.type	put_vp8_epel4_h4_c, @function
put_vp8_epel4_h4_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$2,28($sp)
lw	$25,24($sp)
addiu	$2,$2,-1
sll	$10,$2,1
sll	$2,$2,3
subu	$2,$2,$10
lw	$10,%got(subpel_filters)($28)
addiu	$10,$10,%lo(subpel_filters)
blez	$25,$L525
addu	$10,$10,$2

lw	$24,%got(ff_cropTbl)($28)
move	$16,$0
addiu	$24,$24,1024
$L528:
addiu	$15,$6,4
move	$8,$6
move	$11,$4
$L527:
lbu	$3,0($8)
addiu	$8,$8,1
lbu	$9,2($10)
addiu	$11,$11,1
lbu	$2,1($10)
lbu	$14,-2($8)
mult	$9,$3
lbu	$13,0($8)
lbu	$3,3($10)
lbu	$12,1($8)
msub	$2,$14
lbu	$2,4($10)
madd	$3,$13
msub	$2,$12
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$24,$2
lbu	$2,0($2)
bne	$15,$8,$L527
sb	$2,-1($11)

addiu	$16,$16,1
addu	$4,$4,$5
bne	$16,$25,$L528
addu	$6,$6,$7

$L525:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_vp8_epel4_h4_c
.size	put_vp8_epel4_h4_c, .-put_vp8_epel4_h4_c
.section	.text.put_vp8_epel16_h6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_h6_c
.type	put_vp8_epel16_h6_c, @function
put_vp8_epel16_h6_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$2,28($sp)
lw	$24,24($sp)
addiu	$2,$2,-1
sll	$9,$2,1
sll	$2,$2,3
subu	$2,$2,$9
lw	$9,%got(subpel_filters)($28)
addiu	$9,$9,%lo(subpel_filters)
blez	$24,$L533
addu	$9,$9,$2

lw	$15,%got(ff_cropTbl)($28)
move	$25,$0
addiu	$15,$15,1024
$L536:
addiu	$14,$6,16
move	$8,$6
move	$13,$4
$L535:
lbu	$3,0($8)
addiu	$8,$8,1
lbu	$11,2($9)
addiu	$13,$13,1
lbu	$10,1($9)
lbu	$12,0($9)
lbu	$16,-2($8)
mult	$11,$3
lbu	$2,-3($8)
lbu	$3,0($8)
msub	$10,$16
lbu	$10,3($9)
lbu	$16,1($8)
madd	$12,$2
lbu	$2,4($9)
lbu	$11,2($8)
madd	$10,$3
lbu	$3,5($9)
msub	$2,$16
madd	$3,$11
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$15,$2
lbu	$2,0($2)
bne	$14,$8,$L535
sb	$2,-1($13)

addiu	$25,$25,1
addu	$4,$4,$5
bne	$25,$24,$L536
addu	$6,$6,$7

$L533:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_vp8_epel16_h6_c
.size	put_vp8_epel16_h6_c, .-put_vp8_epel16_h6_c
.section	.text.put_vp8_epel8_h6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_h6_c
.type	put_vp8_epel8_h6_c, @function
put_vp8_epel8_h6_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$2,28($sp)
lw	$24,24($sp)
addiu	$2,$2,-1
sll	$9,$2,1
sll	$2,$2,3
subu	$2,$2,$9
lw	$9,%got(subpel_filters)($28)
addiu	$9,$9,%lo(subpel_filters)
blez	$24,$L541
addu	$9,$9,$2

lw	$15,%got(ff_cropTbl)($28)
move	$25,$0
addiu	$15,$15,1024
$L544:
addiu	$14,$6,8
move	$8,$6
move	$13,$4
$L543:
lbu	$3,0($8)
addiu	$8,$8,1
lbu	$11,2($9)
addiu	$13,$13,1
lbu	$10,1($9)
lbu	$12,0($9)
lbu	$16,-2($8)
mult	$11,$3
lbu	$2,-3($8)
lbu	$3,0($8)
msub	$10,$16
lbu	$10,3($9)
lbu	$16,1($8)
madd	$12,$2
lbu	$2,4($9)
lbu	$11,2($8)
madd	$10,$3
lbu	$3,5($9)
msub	$2,$16
madd	$3,$11
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$15,$2
lbu	$2,0($2)
bne	$14,$8,$L543
sb	$2,-1($13)

addiu	$25,$25,1
addu	$4,$4,$5
bne	$25,$24,$L544
addu	$6,$6,$7

$L541:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_vp8_epel8_h6_c
.size	put_vp8_epel8_h6_c, .-put_vp8_epel8_h6_c
.section	.text.put_vp8_epel4_h6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_h6_c
.type	put_vp8_epel4_h6_c, @function
put_vp8_epel4_h6_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$2,28($sp)
lw	$24,24($sp)
addiu	$2,$2,-1
sll	$9,$2,1
sll	$2,$2,3
subu	$2,$2,$9
lw	$9,%got(subpel_filters)($28)
addiu	$9,$9,%lo(subpel_filters)
blez	$24,$L549
addu	$9,$9,$2

lw	$15,%got(ff_cropTbl)($28)
move	$25,$0
addiu	$15,$15,1024
$L552:
addiu	$14,$6,4
move	$8,$6
move	$13,$4
$L551:
lbu	$3,0($8)
addiu	$8,$8,1
lbu	$11,2($9)
addiu	$13,$13,1
lbu	$10,1($9)
lbu	$12,0($9)
lbu	$16,-2($8)
mult	$11,$3
lbu	$2,-3($8)
lbu	$3,0($8)
msub	$10,$16
lbu	$10,3($9)
lbu	$16,1($8)
madd	$12,$2
lbu	$2,4($9)
lbu	$11,2($8)
madd	$10,$3
lbu	$3,5($9)
msub	$2,$16
madd	$3,$11
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$15,$2
lbu	$2,0($2)
bne	$14,$8,$L551
sb	$2,-1($13)

addiu	$25,$25,1
addu	$4,$4,$5
bne	$25,$24,$L552
addu	$6,$6,$7

$L549:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	put_vp8_epel4_h6_c
.size	put_vp8_epel4_h6_c, .-put_vp8_epel4_h6_c
.section	.text.put_vp8_epel16_v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_v4_c
.type	put_vp8_epel16_v4_c, @function
put_vp8_epel16_v4_c:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-16
sw	$19,12($sp)
lw	$2,40($sp)
lw	$15,32($sp)
sw	$18,8($sp)
addiu	$2,$2,-1
sw	$17,4($sp)
sw	$16,0($sp)
sll	$10,$2,1
sll	$2,$2,3
subu	$2,$2,$10
lw	$10,%got(subpel_filters)($28)
addiu	$10,$10,%lo(subpel_filters)
blez	$15,$L557
addu	$10,$10,$2

lw	$14,%got(ff_cropTbl)($28)
move	$25,$0
li	$24,16			# 0x10
addiu	$14,$14,1024
$L560:
move	$8,$0
addu	$13,$6,$7
$L559:
subu	$12,$8,$7
lbu	$9,2($10)
addu	$17,$6,$8
lbu	$18,1($10)
addu	$11,$8,$7
lbu	$3,3($10)
addu	$12,$6,$12
lbu	$16,4($10)
addu	$2,$13,$8
lbu	$19,0($17)
addu	$11,$13,$11
lbu	$17,0($12)
addu	$12,$4,$8
lbu	$2,0($2)
mult	$9,$19
lbu	$11,0($11)
addiu	$8,$8,1
msub	$18,$17
madd	$3,$2
msub	$16,$11
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$14,$2
lbu	$2,0($2)
bne	$8,$24,$L559
sb	$2,0($12)

addiu	$25,$25,1
move	$6,$13
bne	$25,$15,$L560
addu	$4,$4,$5

$L557:
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	put_vp8_epel16_v4_c
.size	put_vp8_epel16_v4_c, .-put_vp8_epel16_v4_c
.section	.text.put_vp8_epel8_v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_v4_c
.type	put_vp8_epel8_v4_c, @function
put_vp8_epel8_v4_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-16
sw	$18,12($sp)
lw	$2,40($sp)
lw	$15,32($sp)
sw	$17,8($sp)
addiu	$2,$2,-1
sw	$16,4($sp)
sll	$10,$2,1
sll	$2,$2,3
subu	$2,$2,$10
lw	$10,%got(subpel_filters)($28)
addiu	$10,$10,%lo(subpel_filters)
blez	$15,$L565
addu	$10,$10,$2

lw	$14,%got(ff_cropTbl)($28)
move	$25,$0
li	$24,8			# 0x8
addiu	$14,$14,1024
$L568:
move	$8,$0
addu	$12,$6,$7
$L567:
subu	$3,$6,$7
lbu	$2,0($6)
addu	$11,$8,$7
lbu	$9,2($10)
addu	$13,$12,$8
lbu	$18,1($10)
addu	$11,$12,$11
lbu	$17,0($3)
mult	$9,$2
lbu	$3,3($10)
lbu	$2,0($13)
addu	$13,$4,$8
lbu	$16,4($10)
msub	$18,$17
lbu	$9,0($11)
addiu	$8,$8,1
madd	$3,$2
addiu	$6,$6,1
msub	$16,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$14,$2
lbu	$2,0($2)
bne	$8,$24,$L567
sb	$2,0($13)

addiu	$25,$25,1
move	$6,$12
bne	$25,$15,$L568
addu	$4,$4,$5

$L565:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	put_vp8_epel8_v4_c
.size	put_vp8_epel8_v4_c, .-put_vp8_epel8_v4_c
.section	.text.put_vp8_epel4_v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_v4_c
.type	put_vp8_epel4_v4_c, @function
put_vp8_epel4_v4_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-16
sw	$18,12($sp)
lw	$2,40($sp)
lw	$15,32($sp)
sw	$17,8($sp)
addiu	$2,$2,-1
sw	$16,4($sp)
sll	$10,$2,1
sll	$2,$2,3
subu	$2,$2,$10
lw	$10,%got(subpel_filters)($28)
addiu	$10,$10,%lo(subpel_filters)
blez	$15,$L573
addu	$10,$10,$2

lw	$14,%got(ff_cropTbl)($28)
sll	$24,$7,1
move	$25,$0
addiu	$14,$14,1024
$L576:
addiu	$13,$6,4
move	$8,$6
move	$11,$4
$L575:
subu	$3,$8,$7
lbu	$2,0($8)
addu	$12,$8,$7
lbu	$9,2($10)
addu	$16,$8,$24
lbu	$18,1($10)
lbu	$17,0($3)
addiu	$11,$11,1
mult	$9,$2
lbu	$3,3($10)
lbu	$2,0($12)
addiu	$8,$8,1
lbu	$12,4($10)
msub	$18,$17
lbu	$9,0($16)
madd	$3,$2
msub	$12,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$14,$2
lbu	$2,0($2)
bne	$13,$8,$L575
sb	$2,-1($11)

addiu	$25,$25,1
addu	$4,$4,$5
bne	$25,$15,$L576
addu	$6,$6,$7

$L573:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	put_vp8_epel4_v4_c
.size	put_vp8_epel4_v4_c, .-put_vp8_epel4_v4_c
.section	.text.put_vp8_epel16_v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_v6_c
.type	put_vp8_epel16_v6_c, @function
put_vp8_epel16_v6_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-40
sw	$fp,36($sp)
lw	$2,64($sp)
lw	$25,56($sp)
sw	$23,32($sp)
addiu	$2,$2,-1
sw	$22,28($sp)
sw	$21,24($sp)
sll	$9,$2,1
sw	$20,20($sp)
sll	$2,$2,3
sw	$19,16($sp)
sw	$18,12($sp)
subu	$2,$2,$9
lw	$9,%got(subpel_filters)($28)
sw	$17,8($sp)
sw	$16,4($sp)
addiu	$9,$9,%lo(subpel_filters)
blez	$25,$L581
addu	$9,$9,$2

lw	$15,%got(ff_cropTbl)($28)
subu	$24,$0,$7
move	$17,$0
sll	$24,$24,1
addiu	$15,$15,1024
li	$16,16			# 0x10
$L584:
addu	$14,$6,$7
move	$2,$0
addu	$13,$14,$7
$L583:
subu	$3,$2,$7
lbu	$11,2($9)
addu	$18,$6,$2
lbu	$23,1($9)
addu	$8,$2,$24
lbu	$12,0($9)
addu	$3,$6,$3
lbu	$10,3($9)
addu	$8,$6,$8
lbu	$fp,0($18)
addu	$21,$14,$2
lbu	$20,4($9)
addu	$18,$2,$7
lbu	$3,0($3)
addu	$19,$13,$2
lbu	$22,0($8)
addu	$18,$13,$18
lbu	$21,0($21)
mult	$11,$fp
lbu	$8,5($9)
lbu	$19,0($19)
msub	$23,$3
madd	$12,$22
lbu	$3,0($18)
madd	$10,$21
msub	$20,$19
madd	$8,$3
addu	$8,$4,$2
addiu	$2,$2,1
mflo	$3
addiu	$3,$3,64
sra	$3,$3,7
addu	$3,$15,$3
lbu	$3,0($3)
bne	$2,$16,$L583
sb	$3,0($8)

addiu	$17,$17,1
move	$6,$14
bne	$17,$25,$L584
addu	$4,$4,$5

$L581:
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vp8_epel16_v6_c
.size	put_vp8_epel16_v6_c, .-put_vp8_epel16_v6_c
.section	.text.put_vp8_epel8_v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_v6_c
.type	put_vp8_epel8_v6_c, @function
put_vp8_epel8_v6_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-40
sw	$16,4($sp)
lw	$2,64($sp)
lw	$16,56($sp)
sw	$fp,36($sp)
addiu	$2,$2,-1
sw	$23,32($sp)
sw	$22,28($sp)
sll	$9,$2,1
sw	$21,24($sp)
sll	$2,$2,3
sw	$20,20($sp)
sw	$19,16($sp)
subu	$2,$2,$9
lw	$9,%got(subpel_filters)($28)
sw	$18,12($sp)
sw	$17,8($sp)
addiu	$9,$9,%lo(subpel_filters)
blez	$16,$L589
addu	$9,$9,$2

lw	$25,%got(ff_cropTbl)($28)
subu	$24,$0,$7
move	$18,$0
sll	$24,$24,1
addiu	$25,$25,1024
li	$17,8			# 0x8
$L592:
addu	$15,$6,$7
move	$8,$0
addu	$14,$15,$7
move	$12,$6
addu	$13,$14,$7
$L591:
addu	$2,$8,$24
lbu	$fp,0($12)
subu	$3,$12,$7
lbu	$11,2($9)
addu	$2,$6,$2
lbu	$23,1($9)
addu	$22,$15,$8
lbu	$19,0($9)
lbu	$21,0($3)
addu	$3,$14,$8
lbu	$2,0($2)
addu	$20,$13,$8
mult	$11,$fp
lbu	$10,3($9)
lbu	$22,0($22)
msub	$23,$21
madd	$19,$2
lbu	$21,4($9)
lbu	$19,0($3)
addu	$11,$4,$8
lbu	$2,0($20)
madd	$10,$22
lbu	$3,5($9)
addiu	$8,$8,1
msub	$21,$19
addiu	$12,$12,1
madd	$3,$2
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$25,$2
lbu	$2,0($2)
bne	$8,$17,$L591
sb	$2,0($11)

addiu	$18,$18,1
addu	$4,$4,$5
bne	$18,$16,$L592
addu	$6,$13,$24

$L589:
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_vp8_epel8_v6_c
.size	put_vp8_epel8_v6_c, .-put_vp8_epel8_v6_c
.section	.text.put_vp8_epel4_v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_v6_c
.type	put_vp8_epel4_v6_c, @function
put_vp8_epel4_v6_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
sw	$22,28($sp)
lw	$2,56($sp)
lw	$25,48($sp)
sw	$21,24($sp)
addiu	$2,$2,-1
sw	$20,20($sp)
sw	$19,16($sp)
sll	$9,$2,1
sw	$18,12($sp)
sll	$2,$2,3
sw	$17,8($sp)
sw	$16,4($sp)
subu	$2,$2,$9
lw	$9,%got(subpel_filters)($28)
addiu	$9,$9,%lo(subpel_filters)
blez	$25,$L597
addu	$9,$9,$2

lw	$15,%got(ff_cropTbl)($28)
subu	$14,$0,$7
sll	$24,$7,1
sll	$14,$14,1
addu	$16,$24,$7
move	$17,$0
addiu	$15,$15,1024
$L600:
addiu	$13,$6,4
move	$8,$6
move	$12,$4
$L599:
subu	$2,$8,$7
lbu	$10,0($8)
addu	$21,$8,$14
lbu	$11,2($9)
addu	$20,$8,$7
lbu	$22,1($9)
lbu	$2,0($2)
addu	$3,$8,$24
lbu	$18,0($9)
mult	$11,$10
lbu	$21,0($21)
addu	$19,$8,$16
lbu	$10,3($9)
msub	$22,$2
lbu	$20,0($20)
addiu	$12,$12,1
lbu	$2,4($9)
madd	$18,$21
lbu	$18,0($3)
addiu	$8,$8,1
lbu	$3,5($9)
madd	$10,$20
lbu	$11,0($19)
msub	$2,$18
madd	$3,$11
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$15,$2
lbu	$2,0($2)
bne	$13,$8,$L599
sb	$2,-1($12)

addiu	$17,$17,1
addu	$4,$4,$5
bne	$17,$25,$L600
addu	$6,$6,$7

$L597:
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	put_vp8_epel4_v6_c
.size	put_vp8_epel4_v6_c, .-put_vp8_epel4_v6_c
.section	.text.put_vp8_epel16_h4v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_h4v4_c
.type	put_vp8_epel16_h4v4_c, @function
put_vp8_epel16_h4v4_c:
.frame	$sp,624,$31		# vars= 592, regs= 5/0, args= 0, gp= 8
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-624
sll	$8,$7,1
sw	$17,608($sp)
lw	$2,644($sp)
subu	$6,$6,$8
lw	$17,%got(subpel_filters)($28)
lw	$12,640($sp)
addiu	$2,$2,-1
.cprestore	0
addiu	$17,$17,%lo(subpel_filters)
sw	$20,620($sp)
sll	$9,$2,1
sw	$19,616($sp)
sll	$2,$2,3
sw	$18,612($sp)
slt	$3,$12,-4
sw	$16,604($sp)
subu	$2,$2,$9
bne	$3,$0,$L611
addu	$2,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$19,$12,5
lbu	$25,2($2)
addiu	$18,$sp,8
lbu	$24,1($2)
move	$16,$0
lbu	$15,3($2)
addiu	$11,$11,1024
lbu	$14,4($2)
$L610:
addiu	$13,$6,16
move	$8,$6
move	$10,$18
$L609:
lbu	$9,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$9,$25
lbu	$20,-2($8)
lbu	$3,0($8)
lbu	$2,1($8)
msub	$20,$24
madd	$3,$15
msub	$2,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$8,$L609
sb	$2,-1($10)

addiu	$16,$16,1
addiu	$18,$18,16
bne	$16,$19,$L610
addu	$6,$6,$7

$L611:
lw	$2,648($sp)
addiu	$2,$2,-1
sll	$3,$2,1
sll	$2,$2,3
subu	$2,$2,$3
blez	$12,$L605
addu	$17,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,40
move	$15,$0
addiu	$11,$11,1024
$L613:
move	$6,$14
addiu	$14,$14,16
move	$8,$4
$L612:
addiu	$6,$6,1
lbu	$7,2($17)
lbu	$2,-1($6)
addiu	$8,$8,1
lbu	$13,1($17)
lbu	$10,-17($6)
mult	$7,$2
lbu	$3,3($17)
lbu	$2,15($6)
lbu	$9,4($17)
msub	$13,$10
lbu	$7,31($6)
madd	$3,$2
msub	$9,$7
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L612
sb	$2,-1($8)

addiu	$15,$15,1
bne	$15,$12,$L613
addu	$4,$4,$5

$L605:
lw	$20,620($sp)
lw	$19,616($sp)
lw	$18,612($sp)
lw	$17,608($sp)
lw	$16,604($sp)
j	$31
addiu	$sp,$sp,624

.set	macro
.set	reorder
.end	put_vp8_epel16_h4v4_c
.size	put_vp8_epel16_h4v4_c, .-put_vp8_epel16_h4v4_c
.section	.text.put_vp8_epel8_h4v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_h4v4_c
.type	put_vp8_epel8_h4v4_c, @function
put_vp8_epel8_h4v4_c:
.frame	$sp,200,$31		# vars= 168, regs= 5/0, args= 0, gp= 8
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-200
sll	$8,$7,1
sw	$17,184($sp)
lw	$2,220($sp)
subu	$6,$6,$8
lw	$17,%got(subpel_filters)($28)
lw	$12,216($sp)
addiu	$2,$2,-1
.cprestore	0
addiu	$17,$17,%lo(subpel_filters)
sw	$20,196($sp)
sll	$9,$2,1
sw	$19,192($sp)
sll	$2,$2,3
sw	$18,188($sp)
slt	$3,$12,-4
sw	$16,180($sp)
subu	$2,$2,$9
bne	$3,$0,$L625
addu	$2,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$19,$12,5
lbu	$25,2($2)
addiu	$18,$sp,8
lbu	$24,1($2)
move	$16,$0
lbu	$15,3($2)
addiu	$11,$11,1024
lbu	$14,4($2)
$L624:
addiu	$13,$6,8
move	$8,$6
move	$10,$18
$L623:
lbu	$9,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$9,$25
lbu	$20,-2($8)
lbu	$3,0($8)
lbu	$2,1($8)
msub	$20,$24
madd	$3,$15
msub	$2,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$8,$L623
sb	$2,-1($10)

addiu	$16,$16,1
addiu	$18,$18,8
bne	$16,$19,$L624
addu	$6,$6,$7

$L625:
lw	$2,224($sp)
addiu	$2,$2,-1
sll	$3,$2,1
sll	$2,$2,3
subu	$2,$2,$3
blez	$12,$L619
addu	$17,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,24
move	$15,$0
addiu	$11,$11,1024
$L627:
move	$6,$14
addiu	$14,$14,8
move	$8,$4
$L626:
addiu	$6,$6,1
lbu	$7,2($17)
lbu	$2,-1($6)
addiu	$8,$8,1
lbu	$13,1($17)
lbu	$10,-9($6)
mult	$7,$2
lbu	$3,3($17)
lbu	$2,7($6)
lbu	$9,4($17)
msub	$13,$10
lbu	$7,15($6)
madd	$3,$2
msub	$9,$7
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L626
sb	$2,-1($8)

addiu	$15,$15,1
bne	$15,$12,$L627
addu	$4,$4,$5

$L619:
lw	$20,196($sp)
lw	$19,192($sp)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
j	$31
addiu	$sp,$sp,200

.set	macro
.set	reorder
.end	put_vp8_epel8_h4v4_c
.size	put_vp8_epel8_h4v4_c, .-put_vp8_epel8_h4v4_c
.section	.text.put_vp8_epel4_h4v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_h4v4_c
.type	put_vp8_epel4_h4v4_c, @function
put_vp8_epel4_h4v4_c:
.frame	$sp,88,$31		# vars= 56, regs= 5/0, args= 0, gp= 8
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-88
sll	$9,$7,1
sw	$17,72($sp)
lw	$3,108($sp)
subu	$6,$6,$9
lw	$17,%got(subpel_filters)($28)
lw	$12,104($sp)
addiu	$3,$3,-1
.cprestore	0
addiu	$17,$17,%lo(subpel_filters)
sw	$20,84($sp)
sll	$2,$3,1
sw	$19,80($sp)
sll	$3,$3,3
sw	$18,76($sp)
slt	$8,$12,-4
sw	$16,68($sp)
subu	$2,$3,$2
bne	$8,$0,$L639
addu	$2,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$19,$12,5
lbu	$25,2($2)
addiu	$18,$sp,8
lbu	$24,1($2)
move	$16,$0
lbu	$15,3($2)
addiu	$11,$11,1024
lbu	$14,4($2)
$L638:
addiu	$13,$6,4
move	$8,$6
move	$10,$18
$L637:
lbu	$9,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$9,$25
lbu	$20,-2($8)
lbu	$3,0($8)
lbu	$2,1($8)
msub	$20,$24
madd	$3,$15
msub	$2,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$8,$L637
sb	$2,-1($10)

addiu	$16,$16,1
addiu	$18,$18,4
bne	$16,$19,$L638
addu	$6,$6,$7

$L639:
lw	$2,112($sp)
addiu	$2,$2,-1
sll	$3,$2,1
sll	$2,$2,3
subu	$2,$2,$3
blez	$12,$L633
addu	$17,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,16
move	$15,$0
addiu	$11,$11,1024
$L641:
addiu	$6,$14,-4
move	$8,$4
$L640:
addiu	$6,$6,1
lbu	$7,2($17)
lbu	$2,3($6)
addiu	$8,$8,1
lbu	$13,1($17)
lbu	$10,-1($6)
mult	$7,$2
lbu	$3,3($17)
lbu	$2,7($6)
lbu	$9,4($17)
msub	$13,$10
lbu	$7,11($6)
madd	$3,$2
msub	$9,$7
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L640
sb	$2,-1($8)

addiu	$15,$15,1
addu	$4,$4,$5
bne	$15,$12,$L641
addiu	$14,$14,4

$L633:
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,88

.set	macro
.set	reorder
.end	put_vp8_epel4_h4v4_c
.size	put_vp8_epel4_h4v4_c, .-put_vp8_epel4_h4v4_c
.section	.text.put_vp8_epel16_h4v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_h4v6_c
.type	put_vp8_epel16_h4v6_c, @function
put_vp8_epel16_h4v6_c:
.frame	$sp,624,$31		# vars= 592, regs= 5/0, args= 0, gp= 8
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-624
sll	$8,$7,1
sw	$17,608($sp)
lw	$2,644($sp)
subu	$6,$6,$8
lw	$17,%got(subpel_filters)($28)
lw	$10,640($sp)
addiu	$2,$2,-1
.cprestore	0
addiu	$17,$17,%lo(subpel_filters)
sw	$20,620($sp)
sll	$9,$2,1
sw	$19,616($sp)
sll	$2,$2,3
sw	$18,612($sp)
slt	$3,$10,-4
sw	$16,604($sp)
subu	$2,$2,$9
bne	$3,$0,$L653
addu	$2,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$19,$10,5
lbu	$25,2($2)
addiu	$18,$sp,8
lbu	$24,1($2)
move	$16,$0
lbu	$15,3($2)
addiu	$11,$11,1024
lbu	$14,4($2)
$L652:
addiu	$13,$6,16
move	$8,$6
move	$12,$18
$L651:
lbu	$9,0($8)
addiu	$8,$8,1
addiu	$12,$12,1
mult	$9,$25
lbu	$20,-2($8)
lbu	$3,0($8)
lbu	$2,1($8)
msub	$20,$24
madd	$3,$15
msub	$2,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$8,$L651
sb	$2,-1($12)

addiu	$16,$16,1
addiu	$18,$18,16
bne	$16,$19,$L652
addu	$6,$6,$7

$L653:
lw	$2,648($sp)
addiu	$2,$2,-1
sll	$7,$2,1
sll	$2,$2,3
subu	$7,$2,$7
blez	$10,$L647
addu	$7,$17,$7

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,40
move	$15,$0
addiu	$11,$11,1024
$L655:
move	$6,$14
addiu	$14,$14,16
move	$13,$4
$L654:
addiu	$6,$6,1
lbu	$9,2($7)
lbu	$8,-1($6)
addiu	$13,$13,1
lbu	$16,1($7)
lbu	$2,-17($6)
lbu	$12,0($7)
mult	$9,$8
lbu	$3,-33($6)
msub	$16,$2
lbu	$8,3($7)
lbu	$16,15($6)
lbu	$2,4($7)
madd	$12,$3
lbu	$12,31($6)
lbu	$3,5($7)
madd	$8,$16
lbu	$9,47($6)
msub	$2,$12
madd	$3,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L654
sb	$2,-1($13)

addiu	$15,$15,1
bne	$15,$10,$L655
addu	$4,$4,$5

$L647:
lw	$20,620($sp)
lw	$19,616($sp)
lw	$18,612($sp)
lw	$17,608($sp)
lw	$16,604($sp)
j	$31
addiu	$sp,$sp,624

.set	macro
.set	reorder
.end	put_vp8_epel16_h4v6_c
.size	put_vp8_epel16_h4v6_c, .-put_vp8_epel16_h4v6_c
.section	.text.put_vp8_epel8_h4v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_h4v6_c
.type	put_vp8_epel8_h4v6_c, @function
put_vp8_epel8_h4v6_c:
.frame	$sp,200,$31		# vars= 168, regs= 5/0, args= 0, gp= 8
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-200
sll	$8,$7,1
sw	$17,184($sp)
lw	$2,220($sp)
subu	$6,$6,$8
lw	$17,%got(subpel_filters)($28)
lw	$10,216($sp)
addiu	$2,$2,-1
.cprestore	0
addiu	$17,$17,%lo(subpel_filters)
sw	$20,196($sp)
sll	$9,$2,1
sw	$19,192($sp)
sll	$2,$2,3
sw	$18,188($sp)
slt	$3,$10,-4
sw	$16,180($sp)
subu	$2,$2,$9
bne	$3,$0,$L667
addu	$2,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$19,$10,5
lbu	$25,2($2)
addiu	$18,$sp,8
lbu	$24,1($2)
move	$16,$0
lbu	$15,3($2)
addiu	$11,$11,1024
lbu	$14,4($2)
$L666:
addiu	$13,$6,8
move	$8,$6
move	$12,$18
$L665:
lbu	$9,0($8)
addiu	$8,$8,1
addiu	$12,$12,1
mult	$9,$25
lbu	$20,-2($8)
lbu	$3,0($8)
lbu	$2,1($8)
msub	$20,$24
madd	$3,$15
msub	$2,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$8,$L665
sb	$2,-1($12)

addiu	$16,$16,1
addiu	$18,$18,8
bne	$16,$19,$L666
addu	$6,$6,$7

$L667:
lw	$2,224($sp)
addiu	$2,$2,-1
sll	$7,$2,1
sll	$2,$2,3
subu	$7,$2,$7
blez	$10,$L661
addu	$7,$17,$7

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,24
move	$15,$0
addiu	$11,$11,1024
$L669:
move	$6,$14
addiu	$14,$14,8
move	$13,$4
$L668:
addiu	$6,$6,1
lbu	$9,2($7)
lbu	$8,-1($6)
addiu	$13,$13,1
lbu	$16,1($7)
lbu	$2,-9($6)
lbu	$12,0($7)
mult	$9,$8
lbu	$3,-17($6)
msub	$16,$2
lbu	$8,3($7)
lbu	$16,7($6)
lbu	$2,4($7)
madd	$12,$3
lbu	$12,15($6)
lbu	$3,5($7)
madd	$8,$16
lbu	$9,23($6)
msub	$2,$12
madd	$3,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L668
sb	$2,-1($13)

addiu	$15,$15,1
bne	$15,$10,$L669
addu	$4,$4,$5

$L661:
lw	$20,196($sp)
lw	$19,192($sp)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
j	$31
addiu	$sp,$sp,200

.set	macro
.set	reorder
.end	put_vp8_epel8_h4v6_c
.size	put_vp8_epel8_h4v6_c, .-put_vp8_epel8_h4v6_c
.section	.text.put_vp8_epel4_h4v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_h4v6_c
.type	put_vp8_epel4_h4v6_c, @function
put_vp8_epel4_h4v6_c:
.frame	$sp,88,$31		# vars= 56, regs= 5/0, args= 0, gp= 8
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-88
sll	$9,$7,1
sw	$17,72($sp)
lw	$3,108($sp)
subu	$6,$6,$9
lw	$17,%got(subpel_filters)($28)
lw	$10,104($sp)
addiu	$3,$3,-1
.cprestore	0
addiu	$17,$17,%lo(subpel_filters)
sw	$20,84($sp)
sll	$2,$3,1
sw	$19,80($sp)
sll	$3,$3,3
sw	$18,76($sp)
slt	$8,$10,-4
sw	$16,68($sp)
subu	$2,$3,$2
bne	$8,$0,$L681
addu	$2,$17,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$19,$10,5
lbu	$25,2($2)
addiu	$18,$sp,8
lbu	$24,1($2)
move	$16,$0
lbu	$15,3($2)
addiu	$11,$11,1024
lbu	$14,4($2)
$L680:
addiu	$13,$6,4
move	$8,$6
move	$12,$18
$L679:
lbu	$9,0($8)
addiu	$8,$8,1
addiu	$12,$12,1
mult	$9,$25
lbu	$20,-2($8)
lbu	$3,0($8)
lbu	$2,1($8)
msub	$20,$24
madd	$3,$15
msub	$2,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$8,$L679
sb	$2,-1($12)

addiu	$16,$16,1
addiu	$18,$18,4
bne	$16,$19,$L680
addu	$6,$6,$7

$L681:
lw	$2,112($sp)
addiu	$2,$2,-1
sll	$7,$2,1
sll	$2,$2,3
subu	$7,$2,$7
blez	$10,$L675
addu	$7,$17,$7

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,16
move	$15,$0
addiu	$11,$11,1024
$L683:
addiu	$6,$14,-4
move	$13,$4
$L682:
addiu	$6,$6,1
lbu	$9,2($7)
lbu	$8,3($6)
addiu	$13,$13,1
lbu	$16,1($7)
lbu	$2,-1($6)
lbu	$12,0($7)
mult	$9,$8
lbu	$3,-5($6)
msub	$16,$2
lbu	$8,3($7)
lbu	$16,7($6)
lbu	$2,4($7)
madd	$12,$3
lbu	$12,11($6)
lbu	$3,5($7)
madd	$8,$16
lbu	$9,15($6)
msub	$2,$12
madd	$3,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L682
sb	$2,-1($13)

addiu	$15,$15,1
addu	$4,$4,$5
bne	$15,$10,$L683
addiu	$14,$14,4

$L675:
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,88

.set	macro
.set	reorder
.end	put_vp8_epel4_h4v6_c
.size	put_vp8_epel4_h4v6_c, .-put_vp8_epel4_h4v6_c
.section	.text.put_vp8_epel16_h6v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_h6v4_c
.type	put_vp8_epel16_h6v4_c, @function
put_vp8_epel16_h6v4_c:
.frame	$sp,632,$31		# vars= 592, regs= 8/0, args= 0, gp= 8
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-632
sw	$19,612($sp)
lw	$2,652($sp)
lw	$19,%got(subpel_filters)($28)
lw	$14,648($sp)
addiu	$2,$2,-1
sw	$17,604($sp)
addiu	$19,$19,%lo(subpel_filters)
.cprestore	0
sll	$8,$2,1
sw	$23,628($sp)
sll	$2,$2,3
sw	$22,624($sp)
sll	$17,$7,1
sw	$21,620($sp)
subu	$2,$2,$8
sw	$20,616($sp)
slt	$3,$14,-4
sw	$18,608($sp)
sw	$16,600($sp)
addu	$2,$19,$2
bne	$3,$0,$L695
subu	$17,$6,$17

lw	$11,%got(ff_cropTbl)($28)
addiu	$21,$14,5
lbu	$16,2($2)
addiu	$20,$sp,8
lbu	$6,1($2)
move	$18,$0
lbu	$25,0($2)
addiu	$11,$11,1024
lbu	$24,3($2)
lbu	$15,4($2)
lbu	$13,5($2)
$L694:
addiu	$12,$17,16
move	$8,$17
move	$10,$20
$L693:
lbu	$3,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$3,$16
lbu	$3,2($8)
lbu	$23,-2($8)
lbu	$22,-3($8)
lbu	$9,0($8)
lbu	$2,1($8)
msub	$23,$6
madd	$22,$25
madd	$9,$24
msub	$2,$15
madd	$3,$13
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$12,$8,$L693
sb	$2,-1($10)

addiu	$18,$18,1
addiu	$20,$20,16
bne	$18,$21,$L694
addu	$17,$17,$7

$L695:
lw	$2,656($sp)
addiu	$2,$2,-1
sll	$3,$2,1
sll	$2,$2,3
subu	$2,$2,$3
blez	$14,$L689
addu	$19,$19,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$13,$sp,40
move	$15,$0
addiu	$11,$11,1024
$L697:
move	$6,$13
addiu	$13,$13,16
move	$8,$4
$L696:
addiu	$6,$6,1
lbu	$7,2($19)
lbu	$2,-1($6)
addiu	$8,$8,1
lbu	$12,1($19)
lbu	$10,-17($6)
mult	$7,$2
lbu	$3,3($19)
lbu	$2,15($6)
lbu	$9,4($19)
msub	$12,$10
lbu	$7,31($6)
madd	$3,$2
msub	$9,$7
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$6,$L696
sb	$2,-1($8)

addiu	$15,$15,1
bne	$15,$14,$L697
addu	$4,$4,$5

$L689:
lw	$23,628($sp)
lw	$22,624($sp)
lw	$21,620($sp)
lw	$20,616($sp)
lw	$19,612($sp)
lw	$18,608($sp)
lw	$17,604($sp)
lw	$16,600($sp)
j	$31
addiu	$sp,$sp,632

.set	macro
.set	reorder
.end	put_vp8_epel16_h6v4_c
.size	put_vp8_epel16_h6v4_c, .-put_vp8_epel16_h6v4_c
.section	.text.put_vp8_epel8_h6v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_h6v4_c
.type	put_vp8_epel8_h6v4_c, @function
put_vp8_epel8_h6v4_c:
.frame	$sp,208,$31		# vars= 168, regs= 8/0, args= 0, gp= 8
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-208
sw	$19,188($sp)
lw	$2,228($sp)
lw	$19,%got(subpel_filters)($28)
lw	$14,224($sp)
addiu	$2,$2,-1
sw	$17,180($sp)
addiu	$19,$19,%lo(subpel_filters)
.cprestore	0
sll	$8,$2,1
sw	$23,204($sp)
sll	$2,$2,3
sw	$22,200($sp)
sll	$17,$7,1
sw	$21,196($sp)
subu	$2,$2,$8
sw	$20,192($sp)
slt	$3,$14,-4
sw	$18,184($sp)
sw	$16,176($sp)
addu	$2,$19,$2
bne	$3,$0,$L709
subu	$17,$6,$17

lw	$11,%got(ff_cropTbl)($28)
addiu	$21,$14,5
lbu	$16,2($2)
addiu	$20,$sp,8
lbu	$6,1($2)
move	$18,$0
lbu	$25,0($2)
addiu	$11,$11,1024
lbu	$24,3($2)
lbu	$15,4($2)
lbu	$13,5($2)
$L708:
addiu	$12,$17,8
move	$8,$17
move	$10,$20
$L707:
lbu	$3,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$3,$16
lbu	$3,2($8)
lbu	$23,-2($8)
lbu	$22,-3($8)
lbu	$9,0($8)
lbu	$2,1($8)
msub	$23,$6
madd	$22,$25
madd	$9,$24
msub	$2,$15
madd	$3,$13
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$12,$8,$L707
sb	$2,-1($10)

addiu	$18,$18,1
addiu	$20,$20,8
bne	$18,$21,$L708
addu	$17,$17,$7

$L709:
lw	$2,232($sp)
addiu	$2,$2,-1
sll	$3,$2,1
sll	$2,$2,3
subu	$2,$2,$3
blez	$14,$L703
addu	$19,$19,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$13,$sp,24
move	$15,$0
addiu	$11,$11,1024
$L711:
move	$6,$13
addiu	$13,$13,8
move	$8,$4
$L710:
addiu	$6,$6,1
lbu	$7,2($19)
lbu	$2,-1($6)
addiu	$8,$8,1
lbu	$12,1($19)
lbu	$10,-9($6)
mult	$7,$2
lbu	$3,3($19)
lbu	$2,7($6)
lbu	$9,4($19)
msub	$12,$10
lbu	$7,15($6)
madd	$3,$2
msub	$9,$7
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$13,$6,$L710
sb	$2,-1($8)

addiu	$15,$15,1
bne	$15,$14,$L711
addu	$4,$4,$5

$L703:
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
j	$31
addiu	$sp,$sp,208

.set	macro
.set	reorder
.end	put_vp8_epel8_h6v4_c
.size	put_vp8_epel8_h6v4_c, .-put_vp8_epel8_h6v4_c
.section	.text.put_vp8_epel4_h6v4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_h6v4_c
.type	put_vp8_epel4_h6v4_c, @function
put_vp8_epel4_h6v4_c:
.frame	$sp,96,$31		# vars= 56, regs= 8/0, args= 0, gp= 8
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-96
sll	$9,$7,1
sw	$19,76($sp)
lw	$3,116($sp)
subu	$6,$6,$9
lw	$19,%got(subpel_filters)($28)
lw	$13,112($sp)
addiu	$3,$3,-1
.cprestore	0
addiu	$19,$19,%lo(subpel_filters)
sw	$23,92($sp)
sll	$2,$3,1
sw	$22,88($sp)
sll	$3,$3,3
sw	$21,84($sp)
slt	$8,$13,-4
sw	$20,80($sp)
subu	$2,$3,$2
sw	$18,72($sp)
sw	$17,68($sp)
sw	$16,64($sp)
bne	$8,$0,$L723
addu	$2,$19,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$21,$13,5
lbu	$17,2($2)
addiu	$20,$sp,8
lbu	$16,1($2)
move	$18,$0
lbu	$25,0($2)
addiu	$11,$11,1024
lbu	$24,3($2)
lbu	$15,4($2)
lbu	$14,5($2)
$L722:
addiu	$12,$6,4
move	$8,$6
move	$10,$20
$L721:
lbu	$3,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$3,$17
lbu	$3,2($8)
lbu	$23,-2($8)
lbu	$22,-3($8)
lbu	$9,0($8)
lbu	$2,1($8)
msub	$23,$16
madd	$22,$25
madd	$9,$24
msub	$2,$15
madd	$3,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$12,$8,$L721
sb	$2,-1($10)

addiu	$18,$18,1
addiu	$20,$20,4
bne	$18,$21,$L722
addu	$6,$6,$7

$L723:
lw	$2,120($sp)
addiu	$2,$2,-1
sll	$3,$2,1
sll	$2,$2,3
subu	$2,$2,$3
blez	$13,$L717
addu	$19,$19,$2

lw	$11,%got(ff_cropTbl)($28)
addiu	$14,$sp,16
move	$15,$0
addiu	$11,$11,1024
$L725:
addiu	$6,$14,-4
move	$8,$4
$L724:
addiu	$6,$6,1
lbu	$7,2($19)
lbu	$2,3($6)
addiu	$8,$8,1
lbu	$12,1($19)
lbu	$10,-1($6)
mult	$7,$2
lbu	$3,3($19)
lbu	$2,7($6)
lbu	$9,4($19)
msub	$12,$10
lbu	$7,11($6)
madd	$3,$2
msub	$9,$7
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$11,$2
lbu	$2,0($2)
bne	$14,$6,$L724
sb	$2,-1($8)

addiu	$15,$15,1
addu	$4,$4,$5
bne	$15,$13,$L725
addiu	$14,$14,4

$L717:
lw	$23,92($sp)
lw	$22,88($sp)
lw	$21,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
j	$31
addiu	$sp,$sp,96

.set	macro
.set	reorder
.end	put_vp8_epel4_h6v4_c
.size	put_vp8_epel4_h6v4_c, .-put_vp8_epel4_h6v4_c
.section	.text.put_vp8_epel16_h6v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel16_h6v6_c
.type	put_vp8_epel16_h6v6_c, @function
put_vp8_epel16_h6v6_c:
.frame	$sp,632,$31		# vars= 592, regs= 8/0, args= 0, gp= 8
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-632
sw	$19,612($sp)
lw	$2,652($sp)
lw	$19,%got(subpel_filters)($28)
lw	$11,648($sp)
addiu	$2,$2,-1
sw	$17,604($sp)
addiu	$19,$19,%lo(subpel_filters)
.cprestore	0
sll	$8,$2,1
sw	$23,628($sp)
sll	$2,$2,3
sw	$22,624($sp)
sll	$17,$7,1
sw	$21,620($sp)
subu	$2,$2,$8
sw	$20,616($sp)
slt	$3,$11,-4
sw	$18,608($sp)
sw	$16,600($sp)
addu	$2,$19,$2
bne	$3,$0,$L737
subu	$17,$6,$17

lw	$12,%got(ff_cropTbl)($28)
addiu	$21,$11,5
lbu	$16,2($2)
addiu	$20,$sp,8
lbu	$6,1($2)
move	$18,$0
lbu	$25,0($2)
addiu	$12,$12,1024
lbu	$24,3($2)
lbu	$15,4($2)
lbu	$14,5($2)
$L736:
addiu	$13,$17,16
move	$8,$17
move	$10,$20
$L735:
lbu	$3,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$3,$16
lbu	$3,2($8)
lbu	$23,-2($8)
lbu	$22,-3($8)
lbu	$9,0($8)
lbu	$2,1($8)
msub	$23,$6
madd	$22,$25
madd	$9,$24
msub	$2,$15
madd	$3,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$12,$2
lbu	$2,0($2)
bne	$13,$8,$L735
sb	$2,-1($10)

addiu	$18,$18,1
addiu	$20,$20,16
bne	$18,$21,$L736
addu	$17,$17,$7

$L737:
lw	$2,656($sp)
addiu	$2,$2,-1
sll	$7,$2,1
sll	$2,$2,3
subu	$7,$2,$7
blez	$11,$L731
addu	$7,$19,$7

lw	$12,%got(ff_cropTbl)($28)
addiu	$14,$sp,40
move	$15,$0
addiu	$12,$12,1024
$L739:
move	$6,$14
addiu	$14,$14,16
move	$13,$4
$L738:
addiu	$6,$6,1
lbu	$9,2($7)
lbu	$8,-1($6)
addiu	$13,$13,1
lbu	$16,1($7)
lbu	$2,-17($6)
lbu	$10,0($7)
mult	$9,$8
lbu	$3,-33($6)
msub	$16,$2
lbu	$8,3($7)
lbu	$16,15($6)
lbu	$2,4($7)
madd	$10,$3
lbu	$10,31($6)
lbu	$3,5($7)
madd	$8,$16
lbu	$9,47($6)
msub	$2,$10
madd	$3,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$12,$2
lbu	$2,0($2)
bne	$14,$6,$L738
sb	$2,-1($13)

addiu	$15,$15,1
bne	$15,$11,$L739
addu	$4,$4,$5

$L731:
lw	$23,628($sp)
lw	$22,624($sp)
lw	$21,620($sp)
lw	$20,616($sp)
lw	$19,612($sp)
lw	$18,608($sp)
lw	$17,604($sp)
lw	$16,600($sp)
j	$31
addiu	$sp,$sp,632

.set	macro
.set	reorder
.end	put_vp8_epel16_h6v6_c
.size	put_vp8_epel16_h6v6_c, .-put_vp8_epel16_h6v6_c
.section	.text.put_vp8_epel8_h6v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel8_h6v6_c
.type	put_vp8_epel8_h6v6_c, @function
put_vp8_epel8_h6v6_c:
.frame	$sp,208,$31		# vars= 168, regs= 8/0, args= 0, gp= 8
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-208
sw	$19,188($sp)
lw	$2,228($sp)
lw	$19,%got(subpel_filters)($28)
lw	$11,224($sp)
addiu	$2,$2,-1
sw	$17,180($sp)
addiu	$19,$19,%lo(subpel_filters)
.cprestore	0
sll	$8,$2,1
sw	$23,204($sp)
sll	$2,$2,3
sw	$22,200($sp)
sll	$17,$7,1
sw	$21,196($sp)
subu	$2,$2,$8
sw	$20,192($sp)
slt	$3,$11,-4
sw	$18,184($sp)
sw	$16,176($sp)
addu	$2,$19,$2
bne	$3,$0,$L751
subu	$17,$6,$17

lw	$12,%got(ff_cropTbl)($28)
addiu	$21,$11,5
lbu	$16,2($2)
addiu	$20,$sp,8
lbu	$6,1($2)
move	$18,$0
lbu	$25,0($2)
addiu	$12,$12,1024
lbu	$24,3($2)
lbu	$15,4($2)
lbu	$14,5($2)
$L750:
addiu	$13,$17,8
move	$8,$17
move	$10,$20
$L749:
lbu	$3,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$3,$16
lbu	$3,2($8)
lbu	$23,-2($8)
lbu	$22,-3($8)
lbu	$9,0($8)
lbu	$2,1($8)
msub	$23,$6
madd	$22,$25
madd	$9,$24
msub	$2,$15
madd	$3,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$12,$2
lbu	$2,0($2)
bne	$13,$8,$L749
sb	$2,-1($10)

addiu	$18,$18,1
addiu	$20,$20,8
bne	$18,$21,$L750
addu	$17,$17,$7

$L751:
lw	$2,232($sp)
addiu	$2,$2,-1
sll	$7,$2,1
sll	$2,$2,3
subu	$7,$2,$7
blez	$11,$L745
addu	$7,$19,$7

lw	$12,%got(ff_cropTbl)($28)
addiu	$14,$sp,24
move	$15,$0
addiu	$12,$12,1024
$L753:
move	$6,$14
addiu	$14,$14,8
move	$13,$4
$L752:
addiu	$6,$6,1
lbu	$9,2($7)
lbu	$8,-1($6)
addiu	$13,$13,1
lbu	$16,1($7)
lbu	$2,-9($6)
lbu	$10,0($7)
mult	$9,$8
lbu	$3,-17($6)
msub	$16,$2
lbu	$8,3($7)
lbu	$16,7($6)
lbu	$2,4($7)
madd	$10,$3
lbu	$10,15($6)
lbu	$3,5($7)
madd	$8,$16
lbu	$9,23($6)
msub	$2,$10
madd	$3,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$12,$2
lbu	$2,0($2)
bne	$14,$6,$L752
sb	$2,-1($13)

addiu	$15,$15,1
bne	$15,$11,$L753
addu	$4,$4,$5

$L745:
lw	$23,204($sp)
lw	$22,200($sp)
lw	$21,196($sp)
lw	$20,192($sp)
lw	$19,188($sp)
lw	$18,184($sp)
lw	$17,180($sp)
lw	$16,176($sp)
j	$31
addiu	$sp,$sp,208

.set	macro
.set	reorder
.end	put_vp8_epel8_h6v6_c
.size	put_vp8_epel8_h6v6_c, .-put_vp8_epel8_h6v6_c
.section	.text.put_vp8_epel4_h6v6_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_epel4_h6v6_c
.type	put_vp8_epel4_h6v6_c, @function
put_vp8_epel4_h6v6_c:
.frame	$sp,96,$31		# vars= 56, regs= 8/0, args= 0, gp= 8
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-96
sll	$9,$7,1
sw	$19,76($sp)
lw	$3,116($sp)
subu	$6,$6,$9
lw	$19,%got(subpel_filters)($28)
lw	$11,112($sp)
addiu	$3,$3,-1
.cprestore	0
addiu	$19,$19,%lo(subpel_filters)
sw	$23,92($sp)
sll	$2,$3,1
sw	$22,88($sp)
sll	$3,$3,3
sw	$21,84($sp)
slt	$8,$11,-4
sw	$20,80($sp)
subu	$2,$3,$2
sw	$18,72($sp)
sw	$17,68($sp)
sw	$16,64($sp)
bne	$8,$0,$L765
addu	$2,$19,$2

lw	$12,%got(ff_cropTbl)($28)
addiu	$21,$11,5
lbu	$17,2($2)
addiu	$20,$sp,8
lbu	$16,1($2)
move	$18,$0
lbu	$25,0($2)
addiu	$12,$12,1024
lbu	$24,3($2)
lbu	$15,4($2)
lbu	$14,5($2)
$L764:
addiu	$13,$6,4
move	$8,$6
move	$10,$20
$L763:
lbu	$3,0($8)
addiu	$8,$8,1
addiu	$10,$10,1
mult	$3,$17
lbu	$3,2($8)
lbu	$23,-2($8)
lbu	$22,-3($8)
lbu	$9,0($8)
lbu	$2,1($8)
msub	$23,$16
madd	$22,$25
madd	$9,$24
msub	$2,$15
madd	$3,$14
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$12,$2
lbu	$2,0($2)
bne	$13,$8,$L763
sb	$2,-1($10)

addiu	$18,$18,1
addiu	$20,$20,4
bne	$18,$21,$L764
addu	$6,$6,$7

$L765:
lw	$2,120($sp)
addiu	$2,$2,-1
sll	$7,$2,1
sll	$2,$2,3
subu	$7,$2,$7
blez	$11,$L759
addu	$7,$19,$7

lw	$12,%got(ff_cropTbl)($28)
addiu	$14,$sp,16
move	$15,$0
addiu	$12,$12,1024
$L767:
addiu	$6,$14,-4
move	$13,$4
$L766:
addiu	$6,$6,1
lbu	$9,2($7)
lbu	$8,3($6)
addiu	$13,$13,1
lbu	$16,1($7)
lbu	$2,-1($6)
lbu	$10,0($7)
mult	$9,$8
lbu	$3,-5($6)
msub	$16,$2
lbu	$8,3($7)
lbu	$16,7($6)
lbu	$2,4($7)
madd	$10,$3
lbu	$10,11($6)
lbu	$3,5($7)
madd	$8,$16
lbu	$9,15($6)
msub	$2,$10
madd	$3,$9
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
addu	$2,$12,$2
lbu	$2,0($2)
bne	$14,$6,$L766
sb	$2,-1($13)

addiu	$15,$15,1
addu	$4,$4,$5
bne	$15,$11,$L767
addiu	$14,$14,4

$L759:
lw	$23,92($sp)
lw	$22,88($sp)
lw	$21,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
j	$31
addiu	$sp,$sp,96

.set	macro
.set	reorder
.end	put_vp8_epel4_h6v6_c
.size	put_vp8_epel4_h6v6_c, .-put_vp8_epel4_h6v6_c
.section	.text.put_vp8_bilinear16_h_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear16_h_c
.type	put_vp8_bilinear16_h_c, @function
put_vp8_bilinear16_h_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,20($sp)
li	$10,8			# 0x8
lw	$13,16($sp)
move	$12,$0
blez	$13,$L783
subu	$10,$10,$11

$L778:
addiu	$9,$6,16
move	$7,$6
move	$8,$4
$L775:
lbu	$2,1($7)
addiu	$7,$7,1
addiu	$8,$8,1
mult	$11,$2
lbu	$3,-1($7)
madd	$3,$10
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$9,$7,$L775
sb	$2,-1($8)

addiu	$12,$12,1
addu	$4,$4,$5
bne	$12,$13,$L778
addu	$6,$6,$5

$L783:
j	$31
nop

.set	macro
.set	reorder
.end	put_vp8_bilinear16_h_c
.size	put_vp8_bilinear16_h_c, .-put_vp8_bilinear16_h_c
.section	.text.put_vp8_bilinear16_v_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear16_v_c
.type	put_vp8_bilinear16_v_c, @function
put_vp8_bilinear16_v_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,24($sp)
li	$10,8			# 0x8
lw	$14,16($sp)
move	$13,$0
li	$12,16			# 0x10
blez	$14,$L794
subu	$10,$10,$11

$L789:
move	$7,$0
addu	$9,$6,$5
$L786:
addu	$2,$9,$7
addu	$3,$6,$7
addu	$8,$4,$7
lbu	$2,0($2)
addiu	$7,$7,1
lbu	$3,0($3)
mult	$11,$2
madd	$3,$10
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$7,$12,$L786
sb	$2,0($8)

addiu	$13,$13,1
move	$6,$9
bne	$13,$14,$L789
addu	$4,$4,$5

$L794:
j	$31
nop

.set	macro
.set	reorder
.end	put_vp8_bilinear16_v_c
.size	put_vp8_bilinear16_v_c, .-put_vp8_bilinear16_v_c
.section	.text.put_vp8_bilinear16_hv_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear16_hv_c
.type	put_vp8_bilinear16_hv_c, @function
put_vp8_bilinear16_hv_c:
.frame	$sp,544,$31		# vars= 528, regs= 2/0, args= 0, gp= 8
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-544
li	$11,8			# 0x8
sw	$17,540($sp)
sw	$16,536($sp)
lw	$14,564($sp)
lw	$12,568($sp)
lw	$13,560($sp)
subu	$15,$11,$14
bltz	$13,$L795
subu	$11,$11,$12

addiu	$9,$sp,8
addiu	$16,$13,1
move	$25,$0
move	$24,$9
$L800:
addiu	$10,$6,16
move	$7,$6
move	$8,$24
$L799:
lbu	$2,1($7)
addiu	$7,$7,1
addiu	$8,$8,1
mult	$14,$2
lbu	$3,-1($7)
madd	$3,$15
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$7,$10,$L799
sb	$2,-1($8)

addiu	$25,$25,1
addiu	$24,$24,16
bne	$25,$16,$L800
addu	$6,$6,$5

blez	$13,$L811
lw	$17,540($sp)

move	$10,$0
move	$8,$9
$L803:
move	$6,$8
addiu	$8,$8,16
move	$7,$4
$L802:
lbu	$2,16($6)
addiu	$6,$6,1
addiu	$7,$7,1
mult	$12,$2
lbu	$3,-1($6)
madd	$3,$11
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$8,$6,$L802
sb	$2,-1($7)

addiu	$10,$10,1
bne	$10,$13,$L803
addu	$4,$4,$5

$L795:
lw	$17,540($sp)
$L811:
lw	$16,536($sp)
j	$31
addiu	$sp,$sp,544

.set	macro
.set	reorder
.end	put_vp8_bilinear16_hv_c
.size	put_vp8_bilinear16_hv_c, .-put_vp8_bilinear16_hv_c
.section	.text.put_vp8_bilinear8_h_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear8_h_c
.type	put_vp8_bilinear8_h_c, @function
put_vp8_bilinear8_h_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,20($sp)
li	$10,8			# 0x8
lw	$13,16($sp)
move	$12,$0
blez	$13,$L822
subu	$10,$10,$11

$L817:
addiu	$9,$6,8
move	$7,$6
move	$8,$4
$L814:
lbu	$2,1($7)
addiu	$7,$7,1
addiu	$8,$8,1
mult	$11,$2
lbu	$3,-1($7)
madd	$3,$10
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$9,$7,$L814
sb	$2,-1($8)

addiu	$12,$12,1
addu	$4,$4,$5
bne	$12,$13,$L817
addu	$6,$6,$5

$L822:
j	$31
nop

.set	macro
.set	reorder
.end	put_vp8_bilinear8_h_c
.size	put_vp8_bilinear8_h_c, .-put_vp8_bilinear8_h_c
.section	.text.put_vp8_bilinear8_v_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear8_v_c
.type	put_vp8_bilinear8_v_c, @function
put_vp8_bilinear8_v_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,24($sp)
li	$10,8			# 0x8
lw	$14,16($sp)
move	$13,$0
li	$12,8			# 0x8
blez	$14,$L833
subu	$10,$10,$11

$L828:
move	$7,$0
addu	$9,$6,$5
$L825:
addu	$2,$9,$7
addu	$3,$6,$7
addu	$8,$4,$7
lbu	$2,0($2)
addiu	$7,$7,1
lbu	$3,0($3)
mult	$11,$2
madd	$3,$10
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$7,$12,$L825
sb	$2,0($8)

addiu	$13,$13,1
move	$6,$9
bne	$13,$14,$L828
addu	$4,$4,$5

$L833:
j	$31
nop

.set	macro
.set	reorder
.end	put_vp8_bilinear8_v_c
.size	put_vp8_bilinear8_v_c, .-put_vp8_bilinear8_v_c
.section	.text.put_vp8_bilinear8_hv_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear8_hv_c
.type	put_vp8_bilinear8_hv_c, @function
put_vp8_bilinear8_hv_c:
.frame	$sp,152,$31		# vars= 136, regs= 2/0, args= 0, gp= 8
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-152
li	$11,8			# 0x8
sw	$17,148($sp)
sw	$16,144($sp)
lw	$14,172($sp)
lw	$12,176($sp)
lw	$13,168($sp)
subu	$15,$11,$14
bltz	$13,$L834
subu	$11,$11,$12

addiu	$9,$sp,8
addiu	$16,$13,1
move	$25,$0
move	$24,$9
$L839:
addiu	$10,$6,8
move	$7,$6
move	$8,$24
$L838:
lbu	$2,1($7)
addiu	$7,$7,1
addiu	$8,$8,1
mult	$14,$2
lbu	$3,-1($7)
madd	$3,$15
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$7,$10,$L838
sb	$2,-1($8)

addiu	$25,$25,1
addiu	$24,$24,8
bne	$25,$16,$L839
addu	$6,$6,$5

blez	$13,$L850
lw	$17,148($sp)

move	$10,$0
move	$8,$9
$L842:
move	$6,$8
addiu	$8,$8,8
move	$7,$4
$L841:
lbu	$2,8($6)
addiu	$6,$6,1
addiu	$7,$7,1
mult	$12,$2
lbu	$3,-1($6)
madd	$3,$11
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$8,$6,$L841
sb	$2,-1($7)

addiu	$10,$10,1
bne	$10,$13,$L842
addu	$4,$4,$5

$L834:
lw	$17,148($sp)
$L850:
lw	$16,144($sp)
j	$31
addiu	$sp,$sp,152

.set	macro
.set	reorder
.end	put_vp8_bilinear8_hv_c
.size	put_vp8_bilinear8_hv_c, .-put_vp8_bilinear8_hv_c
.section	.text.put_vp8_bilinear4_h_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear4_h_c
.type	put_vp8_bilinear4_h_c, @function
put_vp8_bilinear4_h_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,20($sp)
li	$10,8			# 0x8
lw	$13,16($sp)
move	$12,$0
blez	$13,$L861
subu	$10,$10,$11

$L856:
addiu	$9,$6,4
move	$7,$6
move	$8,$4
$L853:
lbu	$2,1($7)
addiu	$7,$7,1
addiu	$8,$8,1
mult	$11,$2
lbu	$3,-1($7)
madd	$3,$10
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$9,$7,$L853
sb	$2,-1($8)

addiu	$12,$12,1
addu	$4,$4,$5
bne	$12,$13,$L856
addu	$6,$6,$5

$L861:
j	$31
nop

.set	macro
.set	reorder
.end	put_vp8_bilinear4_h_c
.size	put_vp8_bilinear4_h_c, .-put_vp8_bilinear4_h_c
.section	.text.put_vp8_bilinear4_v_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear4_v_c
.type	put_vp8_bilinear4_v_c, @function
put_vp8_bilinear4_v_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,24($sp)
li	$10,8			# 0x8
lw	$14,16($sp)
move	$13,$0
li	$12,4			# 0x4
blez	$14,$L872
subu	$10,$10,$11

$L867:
move	$7,$0
addu	$9,$6,$5
$L864:
addu	$2,$9,$7
addu	$3,$6,$7
addu	$8,$4,$7
lbu	$2,0($2)
addiu	$7,$7,1
lbu	$3,0($3)
mult	$11,$2
madd	$3,$10
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$7,$12,$L864
sb	$2,0($8)

addiu	$13,$13,1
move	$6,$9
bne	$13,$14,$L867
addu	$4,$4,$5

$L872:
j	$31
nop

.set	macro
.set	reorder
.end	put_vp8_bilinear4_v_c
.size	put_vp8_bilinear4_v_c, .-put_vp8_bilinear4_v_c
.section	.text.put_vp8_bilinear4_hv_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_bilinear4_hv_c
.type	put_vp8_bilinear4_hv_c, @function
put_vp8_bilinear4_hv_c:
.frame	$sp,56,$31		# vars= 40, regs= 2/0, args= 0, gp= 8
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-56
li	$11,8			# 0x8
sw	$17,52($sp)
sw	$16,48($sp)
lw	$14,76($sp)
lw	$12,80($sp)
lw	$13,72($sp)
subu	$15,$11,$14
bltz	$13,$L873
subu	$11,$11,$12

addiu	$9,$sp,8
addiu	$16,$13,1
move	$25,$0
move	$24,$9
$L878:
addiu	$10,$6,4
move	$7,$6
move	$8,$24
$L877:
lbu	$2,1($7)
addiu	$7,$7,1
addiu	$8,$8,1
mult	$14,$2
lbu	$3,-1($7)
madd	$3,$15
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$7,$10,$L877
sb	$2,-1($8)

addiu	$25,$25,1
addiu	$24,$24,4
bne	$25,$16,$L878
addu	$6,$6,$5

blez	$13,$L889
lw	$17,52($sp)

move	$10,$0
move	$8,$9
$L881:
move	$6,$8
addiu	$8,$8,4
move	$7,$4
$L880:
lbu	$2,4($6)
addiu	$6,$6,1
addiu	$7,$7,1
mult	$12,$2
lbu	$3,-1($6)
madd	$3,$11
mflo	$2
addiu	$2,$2,4
sra	$2,$2,3
bne	$8,$6,$L880
sb	$2,-1($7)

addiu	$10,$10,1
bne	$10,$13,$L881
addu	$4,$4,$5

$L873:
lw	$17,52($sp)
$L889:
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_vp8_bilinear4_hv_c
.size	put_vp8_bilinear4_hv_c, .-put_vp8_bilinear4_hv_c
.section	.text.put_vp8_pixels4_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_pixels4_c
.type	put_vp8_pixels4_c, @function
put_vp8_pixels4_c:
.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
sw	$19,40($sp)
.cprestore	16
sw	$21,48($sp)
move	$21,$5
sw	$20,44($sp)
move	$20,$7
sw	$18,36($sp)
move	$18,$4
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,52($sp)
lw	$19,72($sp)
blez	$19,$L890
move	$16,$0

$L894:
lw	$25,%call16(memcpy)($28)
li	$6,4			# 0x4
addiu	$16,$16,1
move	$4,$18
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$17

addu	$18,$18,$21
lw	$28,16($sp)
bne	$16,$19,$L894
addu	$17,$17,$20

$L890:
lw	$31,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_vp8_pixels4_c
.size	put_vp8_pixels4_c, .-put_vp8_pixels4_c
.section	.text.put_vp8_pixels8_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_pixels8_c
.type	put_vp8_pixels8_c, @function
put_vp8_pixels8_c:
.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
sw	$19,40($sp)
.cprestore	16
sw	$21,48($sp)
move	$21,$5
sw	$20,44($sp)
move	$20,$7
sw	$18,36($sp)
move	$18,$4
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,52($sp)
lw	$19,72($sp)
blez	$19,$L897
move	$16,$0

$L901:
lw	$25,%call16(memcpy)($28)
li	$6,8			# 0x8
addiu	$16,$16,1
move	$4,$18
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$17

addu	$18,$18,$21
lw	$28,16($sp)
bne	$16,$19,$L901
addu	$17,$17,$20

$L897:
lw	$31,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_vp8_pixels8_c
.size	put_vp8_pixels8_c, .-put_vp8_pixels8_c
.section	.text.put_vp8_pixels16_c,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	put_vp8_pixels16_c
.type	put_vp8_pixels16_c, @function
put_vp8_pixels16_c:
.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-56
sw	$19,40($sp)
.cprestore	16
sw	$21,48($sp)
move	$21,$5
sw	$20,44($sp)
move	$20,$7
sw	$18,36($sp)
move	$18,$4
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,52($sp)
lw	$19,72($sp)
blez	$19,$L904
move	$16,$0

$L908:
lw	$25,%call16(memcpy)($28)
li	$6,16			# 0x10
addiu	$16,$16,1
move	$4,$18
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$17

addu	$18,$18,$21
lw	$28,16($sp)
bne	$16,$19,$L908
addu	$17,$17,$20

$L904:
lw	$31,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_vp8_pixels16_c
.size	put_vp8_pixels16_c, .-put_vp8_pixels16_c
.section	.text.unlikely.ff_vp8dsp_init,"ax",@progbits
.align	2
.globl	ff_vp8dsp_init
.set	nomips16
.set	nomicromips
.ent	ff_vp8dsp_init
.type	ff_vp8dsp_init, @function
ff_vp8dsp_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$15,%got(vp8_idct_add_c)($28)
lw	$2,%got(vp8_luma_dc_wht_c)($28)
lw	$14,%got(put_vp8_pixels16_c)($28)
addiu	$15,$15,%lo(vp8_idct_add_c)
lw	$11,%got(put_vp8_pixels8_c)($28)
addiu	$2,$2,%lo(vp8_luma_dc_wht_c)
lw	$8,%got(put_vp8_pixels4_c)($28)
addiu	$14,$14,%lo(put_vp8_pixels16_c)
lw	$13,%got(put_vp8_bilinear16_h_c)($28)
sw	$15,8($4)
addiu	$11,$11,%lo(put_vp8_pixels8_c)
lw	$15,%got(vp8_idct_dc_add_c)($28)
addiu	$8,$8,%lo(put_vp8_pixels4_c)
sw	$2,0($4)
addiu	$13,$13,%lo(put_vp8_bilinear16_h_c)
lw	$2,%got(vp8_luma_dc_wht_dc_c)($28)
addiu	$15,$15,%lo(vp8_idct_dc_add_c)
sw	$14,64($4)
sw	$11,100($4)
addiu	$2,$2,%lo(vp8_luma_dc_wht_dc_c)
lw	$12,%got(put_vp8_bilinear16_v_c)($28)
sw	$15,12($4)
lw	$15,%got(vp8_idct_dc_add4y_c)($28)
sw	$2,4($4)
addiu	$12,$12,%lo(put_vp8_bilinear16_v_c)
lw	$5,%got(put_vp8_bilinear16_hv_c)($28)
addiu	$15,$15,%lo(vp8_idct_dc_add4y_c)
lw	$10,%got(put_vp8_bilinear8_h_c)($28)
lw	$9,%got(put_vp8_bilinear8_v_c)($28)
lw	$3,%got(put_vp8_bilinear8_hv_c)($28)
addiu	$5,$5,%lo(put_vp8_bilinear16_hv_c)
sw	$15,16($4)
addiu	$10,$10,%lo(put_vp8_bilinear8_h_c)
lw	$15,%got(vp8_idct_dc_add4uv_c)($28)
addiu	$9,$9,%lo(put_vp8_bilinear8_v_c)
lw	$7,%got(put_vp8_bilinear4_h_c)($28)
addiu	$3,$3,%lo(put_vp8_bilinear8_hv_c)
lw	$6,%got(put_vp8_bilinear4_v_c)($28)
addiu	$15,$15,%lo(vp8_idct_dc_add4uv_c)
lw	$2,%got(put_vp8_bilinear4_hv_c)($28)
addiu	$7,$7,%lo(put_vp8_bilinear4_h_c)
addiu	$6,$6,%lo(put_vp8_bilinear4_v_c)
sw	$15,20($4)
addiu	$2,$2,%lo(put_vp8_bilinear4_hv_c)
lw	$15,%got(vp8_v_loop_filter16_c)($28)
addiu	$15,$15,%lo(vp8_v_loop_filter16_c)
sw	$15,24($4)
lw	$15,%got(vp8_h_loop_filter16_c)($28)
addiu	$15,$15,%lo(vp8_h_loop_filter16_c)
sw	$15,28($4)
lw	$15,%got(vp8_v_loop_filter8uv_c)($28)
addiu	$15,$15,%lo(vp8_v_loop_filter8uv_c)
sw	$15,32($4)
lw	$15,%got(vp8_h_loop_filter8uv_c)($28)
addiu	$15,$15,%lo(vp8_h_loop_filter8uv_c)
sw	$15,36($4)
lw	$15,%got(vp8_v_loop_filter16_inner_c)($28)
addiu	$15,$15,%lo(vp8_v_loop_filter16_inner_c)
sw	$15,40($4)
lw	$15,%got(vp8_h_loop_filter16_inner_c)($28)
addiu	$15,$15,%lo(vp8_h_loop_filter16_inner_c)
sw	$15,44($4)
lw	$15,%got(vp8_v_loop_filter8uv_inner_c)($28)
addiu	$15,$15,%lo(vp8_v_loop_filter8uv_inner_c)
sw	$15,48($4)
lw	$15,%got(vp8_h_loop_filter8uv_inner_c)($28)
addiu	$15,$15,%lo(vp8_h_loop_filter8uv_inner_c)
sw	$15,52($4)
lw	$15,%got(vp8_v_loop_filter_simple_c)($28)
addiu	$15,$15,%lo(vp8_v_loop_filter_simple_c)
sw	$15,56($4)
lw	$15,%got(vp8_h_loop_filter_simple_c)($28)
addiu	$15,$15,%lo(vp8_h_loop_filter_simple_c)
sw	$15,60($4)
lw	$15,%got(put_vp8_epel16_h4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_h4_c)
sw	$15,68($4)
lw	$15,%got(put_vp8_epel16_h6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_h6_c)
sw	$15,72($4)
lw	$15,%got(put_vp8_epel16_v4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_v4_c)
sw	$15,76($4)
lw	$15,%got(put_vp8_epel16_h4v4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_h4v4_c)
sw	$15,80($4)
lw	$15,%got(put_vp8_epel16_h6v4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_h6v4_c)
sw	$15,84($4)
lw	$15,%got(put_vp8_epel16_v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_v6_c)
sw	$15,88($4)
lw	$15,%got(put_vp8_epel16_h4v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_h4v6_c)
sw	$15,92($4)
lw	$15,%got(put_vp8_epel16_h6v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel16_h6v6_c)
sw	$15,96($4)
lw	$15,%got(put_vp8_epel8_h4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_h4_c)
sw	$15,104($4)
lw	$15,%got(put_vp8_epel8_h6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_h6_c)
sw	$15,108($4)
lw	$15,%got(put_vp8_epel8_v4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_v4_c)
sw	$15,112($4)
lw	$15,%got(put_vp8_epel8_h4v4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_h4v4_c)
sw	$15,116($4)
lw	$15,%got(put_vp8_epel8_h6v4_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_h6v4_c)
sw	$15,120($4)
lw	$15,%got(put_vp8_epel8_v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_v6_c)
sw	$15,124($4)
lw	$15,%got(put_vp8_epel8_h4v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_h4v6_c)
sw	$15,128($4)
lw	$15,%got(put_vp8_epel8_h6v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel8_h6v6_c)
sw	$15,132($4)
lw	$15,%got(put_vp8_epel4_h4_c)($28)
sw	$8,136($4)
sw	$14,172($4)
addiu	$15,$15,%lo(put_vp8_epel4_h4_c)
sw	$13,176($4)
sw	$13,180($4)
sw	$12,184($4)
sw	$15,140($4)
lw	$15,%got(put_vp8_epel4_h6_c)($28)
sw	$5,188($4)
sw	$5,192($4)
addiu	$15,$15,%lo(put_vp8_epel4_h6_c)
sw	$12,196($4)
sw	$5,200($4)
sw	$5,204($4)
sw	$15,144($4)
lw	$15,%got(put_vp8_epel4_v4_c)($28)
sw	$11,208($4)
sw	$10,212($4)
addiu	$15,$15,%lo(put_vp8_epel4_v4_c)
sw	$10,216($4)
sw	$9,220($4)
sw	$3,224($4)
sw	$15,148($4)
lw	$15,%got(put_vp8_epel4_h4v4_c)($28)
sw	$3,228($4)
sw	$9,232($4)
addiu	$15,$15,%lo(put_vp8_epel4_h4v4_c)
sw	$3,236($4)
sw	$3,240($4)
sw	$8,244($4)
sw	$15,152($4)
lw	$15,%got(put_vp8_epel4_h6v4_c)($28)
sw	$7,248($4)
sw	$7,252($4)
addiu	$15,$15,%lo(put_vp8_epel4_h6v4_c)
sw	$6,256($4)
sw	$2,260($4)
sw	$2,264($4)
sw	$15,156($4)
lw	$15,%got(put_vp8_epel4_v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel4_v6_c)
sw	$15,160($4)
lw	$15,%got(put_vp8_epel4_h4v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel4_h4v6_c)
sw	$15,164($4)
lw	$15,%got(put_vp8_epel4_h6v6_c)($28)
addiu	$15,$15,%lo(put_vp8_epel4_h6v6_c)
sw	$15,168($4)
sw	$6,268($4)
sw	$2,272($4)
j	$31
sw	$2,276($4)

.set	macro
.set	reorder
.end	ff_vp8dsp_init
.size	ff_vp8dsp_init, .-ff_vp8dsp_init
.rdata
.align	2
.type	subpel_filters, @object
.size	subpel_filters, 42
subpel_filters:
.byte	0
.byte	6
.byte	123
.byte	12
.byte	1
.byte	0
.byte	2
.byte	11
.byte	108
.byte	36
.byte	8
.byte	1
.byte	0
.byte	9
.byte	93
.byte	50
.byte	6
.byte	0
.byte	3
.byte	16
.byte	77
.byte	77
.byte	16
.byte	3
.byte	0
.byte	6
.byte	50
.byte	93
.byte	9
.byte	0
.byte	1
.byte	8
.byte	36
.byte	108
.byte	11
.byte	2
.byte	0
.byte	1
.byte	12
.byte	123
.byte	6
.byte	0
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
