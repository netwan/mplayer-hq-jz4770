	.file	1 "rv30dsp.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.abicalls
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_h_lowpass
	.type	put_rv30_tpel8_h_lowpass, @function
put_rv30_tpel8_h_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	addiu	$2,$sp,16
#APP
 # 57 "rv30dsp.c" 1
	S8LDD xr13,$2,0,ptn7
 # 0 "" 2
#NO_APP
	addiu	$2,$sp,20
#APP
 # 58 "rv30dsp.c" 1
	S8LDD xr14,$2,0,ptn7
 # 0 "" 2
#NO_APP
	li	$2,524288			# 0x80000
	addiu	$2,$2,8
#APP
 # 60 "rv30dsp.c" 1
	S32I2M xr15,$2
 # 0 "" 2
#NO_APP
	li	$10,8			# 0x8
$L2:
#APP
 # 63 "rv30dsp.c" 1
	S8LDD xr1,$5,-1,ptn3
 # 0 "" 2
 # 63 "rv30dsp.c" 1
	S8LDD xr3,$5,1,ptn3
 # 0 "" 2
 # 64 "rv30dsp.c" 1
	S8LDD xr1,$5,0,ptn2
 # 0 "" 2
 # 64 "rv30dsp.c" 1
	S8LDD xr3,$5,2,ptn2
 # 0 "" 2
 # 65 "rv30dsp.c" 1
	S8LDD xr1,$5,1,ptn1
 # 0 "" 2
 # 65 "rv30dsp.c" 1
	S8LDD xr3,$5,3,ptn1
 # 0 "" 2
 # 66 "rv30dsp.c" 1
	S8LDD xr1,$5,2,ptn0
 # 0 "" 2
 # 66 "rv30dsp.c" 1
	S8LDD xr3,$5,4,ptn0
 # 0 "" 2
 # 67 "rv30dsp.c" 1
	D32SLL xr2,xr1,xr1,xr2,8
 # 0 "" 2
 # 68 "rv30dsp.c" 1
	D32SLL xr4,xr3,xr3,xr4,8
 # 0 "" 2
 # 69 "rv30dsp.c" 1
	S8LDD xr2,$5,3,ptn0
 # 0 "" 2
 # 70 "rv30dsp.c" 1
	S8LDD xr4,$5,5,ptn0
 # 0 "" 2
 # 71 "rv30dsp.c" 1
	Q8MUL xr5,xr2,xr13,xr6
 # 0 "" 2
 # 72 "rv30dsp.c" 1
	Q8MUL xr7,xr3,xr14,xr8
 # 0 "" 2
 # 74 "rv30dsp.c" 1
	Q8ADDE xr9,xr1,xr4,xr10,AA
 # 0 "" 2
 # 75 "rv30dsp.c" 1
	Q16ACCM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 76 "rv30dsp.c" 1
	Q16ACCM xr7,xr9,xr10,xr8,SS
 # 0 "" 2
 # 78 "rv30dsp.c" 1
	S8LDD xr1,$5,3,ptn3
 # 0 "" 2
 # 78 "rv30dsp.c" 1
	S8LDD xr3,$5,5,ptn3
 # 0 "" 2
 # 79 "rv30dsp.c" 1
	Q16ACCM xr5,xr7,xr8,xr6,AA
 # 0 "" 2
 # 80 "rv30dsp.c" 1
	S8LDD xr1,$5,4,ptn2
 # 0 "" 2
 # 80 "rv30dsp.c" 1
	S8LDD xr3,$5,6,ptn2
 # 0 "" 2
 # 81 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr6,xr6,4
 # 0 "" 2
 # 82 "rv30dsp.c" 1
	S8LDD xr1,$5,5,ptn1
 # 0 "" 2
 # 82 "rv30dsp.c" 1
	S8LDD xr3,$5,7,ptn1
 # 0 "" 2
 # 83 "rv30dsp.c" 1
	Q16SAT xr9,xr5,xr6
 # 0 "" 2
 # 84 "rv30dsp.c" 1
	S8LDD xr1,$5,6,ptn0
 # 0 "" 2
 # 84 "rv30dsp.c" 1
	S8LDD xr3,$5,8,ptn0
 # 0 "" 2
 # 85 "rv30dsp.c" 1
	S32STDR xr9,$4,0
 # 0 "" 2
 # 87 "rv30dsp.c" 1
	D32SLL xr2,xr1,xr1,xr2,8
 # 0 "" 2
 # 88 "rv30dsp.c" 1
	D32SLL xr4,xr3,xr3,xr4,8
 # 0 "" 2
 # 89 "rv30dsp.c" 1
	S8LDD xr2,$5,7,ptn0
 # 0 "" 2
 # 90 "rv30dsp.c" 1
	S8LDD xr4,$5,9,ptn0
 # 0 "" 2
 # 92 "rv30dsp.c" 1
	Q8MUL xr5,xr2,xr13,xr6
 # 0 "" 2
 # 93 "rv30dsp.c" 1
	Q8MUL xr7,xr3,xr14,xr8
 # 0 "" 2
 # 95 "rv30dsp.c" 1
	Q8ADDE xr9,xr1,xr4,xr10,AA
 # 0 "" 2
 # 96 "rv30dsp.c" 1
	Q16ACCM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 97 "rv30dsp.c" 1
	Q16ACCM xr7,xr9,xr10,xr8,SS
 # 0 "" 2
 # 98 "rv30dsp.c" 1
	Q16ACCM xr5,xr7,xr8,xr6,AA
 # 0 "" 2
 # 99 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr6,xr6,4
 # 0 "" 2
 # 100 "rv30dsp.c" 1
	Q16SAT xr9,xr5,xr6
 # 0 "" 2
 # 101 "rv30dsp.c" 1
	S32STDR xr9,$4,4
 # 0 "" 2
#NO_APP
	lbu	$3,6($5)
	addiu	$10,$10,-1
	lw	$8,16($sp)
	lbu	$9,5($5)
	lbu	$2,8($5)
	mul	$8,$3,$8
	lbu	$3,7($5)
	addu	$5,$5,$7
	addu	$2,$9,$2
	subu	$2,$8,$2
	lw	$8,20($sp)
	mul	$9,$3,$8
	addu	$2,$9,$2
	addiu	$2,$2,8
	sra	$2,$2,4
	sb	$2,6($4)
	.set	noreorder
	.set	nomacro
	bne	$10,$0,$L2
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	j	$31
	.end	put_rv30_tpel8_h_lowpass
	.size	put_rv30_tpel8_h_lowpass, .-put_rv30_tpel8_h_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_v_lowpass
	.type	put_rv30_tpel8_v_lowpass, @function
put_rv30_tpel8_v_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	addiu	$2,$sp,16
#APP
 # 133 "rv30dsp.c" 1
	S8LDD xr13,$2,0,ptn7
 # 0 "" 2
#NO_APP
	addiu	$2,$sp,20
#APP
 # 134 "rv30dsp.c" 1
	S8LDD xr14,$2,0,ptn7
 # 0 "" 2
#NO_APP
	li	$2,524288			# 0x80000
	addiu	$2,$2,8
#APP
 # 135 "rv30dsp.c" 1
	S32I2M xr15,$2
 # 0 "" 2
#NO_APP
	subu	$10,$0,$7
	addiu	$9,$4,8
$L6:
	addu	$2,$5,$10
#APP
 # 146 "rv30dsp.c" 1
	S8LDD xr1,$2,0,ptn3
 # 0 "" 2
#NO_APP
	addu	$3,$5,$7
#APP
 # 146 "rv30dsp.c" 1
	S8LDD xr3,$3,0,ptn3
 # 0 "" 2
 # 147 "rv30dsp.c" 1
	S8LDD xr1,$5,0,ptn2
 # 0 "" 2
#NO_APP
	addu	$2,$3,$7
#APP
 # 147 "rv30dsp.c" 1
	S8LDD xr3,$2,0,ptn2
 # 0 "" 2
 # 148 "rv30dsp.c" 1
	S8LDD xr1,$3,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$3,$2,$7
#APP
 # 148 "rv30dsp.c" 1
	S8LDD xr3,$3,0,ptn1
 # 0 "" 2
 # 149 "rv30dsp.c" 1
	S8LDD xr1,$2,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$8,$3,$7
#APP
 # 149 "rv30dsp.c" 1
	S8LDD xr3,$8,0,ptn0
 # 0 "" 2
 # 150 "rv30dsp.c" 1
	D32SLL xr2,xr1,xr3,xr4,8
 # 0 "" 2
 # 151 "rv30dsp.c" 1
	S8LDD xr2,$3,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$2,$8,$7
#APP
 # 151 "rv30dsp.c" 1
	S8LDD xr4,$2,0,ptn0
 # 0 "" 2
 # 153 "rv30dsp.c" 1
	Q8MUL xr5,xr2,xr13,xr6
 # 0 "" 2
 # 154 "rv30dsp.c" 1
	Q8MUL xr7,xr3,xr14,xr8
 # 0 "" 2
 # 156 "rv30dsp.c" 1
	Q8ADDE xr9,xr1,xr4,xr10,AA
 # 0 "" 2
 # 157 "rv30dsp.c" 1
	Q16ACCM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 158 "rv30dsp.c" 1
	Q16ACCM xr7,xr9,xr10,xr8,SS
 # 0 "" 2
 # 160 "rv30dsp.c" 1
	S8LDD xr1,$3,0,ptn3
 # 0 "" 2
 # 160 "rv30dsp.c" 1
	S8LDD xr3,$2,0,ptn3
 # 0 "" 2
 # 161 "rv30dsp.c" 1
	Q16ACCM xr5,xr7,xr8,xr6,AA
 # 0 "" 2
 # 162 "rv30dsp.c" 1
	S8LDD xr1,$8,0,ptn2
 # 0 "" 2
#NO_APP
	addu	$3,$2,$7
#APP
 # 162 "rv30dsp.c" 1
	S8LDD xr3,$3,0,ptn2
 # 0 "" 2
 # 163 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr6,xr6,4
 # 0 "" 2
 # 164 "rv30dsp.c" 1
	S8LDD xr1,$2,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$8,$3,$7
#APP
 # 164 "rv30dsp.c" 1
	S8LDD xr3,$8,0,ptn1
 # 0 "" 2
 # 165 "rv30dsp.c" 1
	Q16SAT xr9,xr5,xr6
 # 0 "" 2
 # 166 "rv30dsp.c" 1
	S8LDD xr1,$3,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$3,$8,$7
#APP
 # 166 "rv30dsp.c" 1
	S8LDD xr3,$3,0,ptn0
 # 0 "" 2
 # 168 "rv30dsp.c" 1
	S8STD xr9,$4,0,ptn3
 # 0 "" 2
 # 169 "rv30dsp.c" 1
	D32SLL xr2,xr1,xr3,xr4,8
 # 0 "" 2
#NO_APP
	addu	$2,$4,$6
#APP
 # 170 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn2
 # 0 "" 2
 # 171 "rv30dsp.c" 1
	S8LDD xr2,$8,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$2,$2,$6
#APP
 # 172 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$3,$3,$7
#APP
 # 173 "rv30dsp.c" 1
	S8LDD xr4,$3,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$2,$2,$6
#APP
 # 174 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn0
 # 0 "" 2
 # 176 "rv30dsp.c" 1
	Q8MUL xr5,xr2,xr13,xr6
 # 0 "" 2
 # 177 "rv30dsp.c" 1
	Q8MUL xr7,xr3,xr14,xr8
 # 0 "" 2
 # 179 "rv30dsp.c" 1
	Q8ADDE xr9,xr1,xr4,xr10,AA
 # 0 "" 2
 # 180 "rv30dsp.c" 1
	Q16ACCM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 181 "rv30dsp.c" 1
	Q16ACCM xr7,xr9,xr10,xr8,SS
 # 0 "" 2
 # 182 "rv30dsp.c" 1
	Q16ACCM xr5,xr7,xr8,xr6,AA
 # 0 "" 2
 # 183 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr6,xr6,4
 # 0 "" 2
 # 184 "rv30dsp.c" 1
	Q16SAT xr9,xr5,xr6
 # 0 "" 2
#NO_APP
	addu	$2,$2,$6
#APP
 # 185 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn3
 # 0 "" 2
#NO_APP
	addu	$2,$2,$6
#APP
 # 186 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn2
 # 0 "" 2
#NO_APP
	addu	$2,$2,$6
#APP
 # 187 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$2,$2,$6
#APP
 # 188 "rv30dsp.c" 1
	S8STD xr9,$2,0,ptn0
 # 0 "" 2
#NO_APP
	addiu	$4,$4,1
	.set	noreorder
	.set	nomacro
	bne	$4,$9,$L6
	addiu	$5,$5,1
	.set	macro
	.set	reorder

	j	$31
	.end	put_rv30_tpel8_v_lowpass
	.size	put_rv30_tpel8_v_lowpass, .-put_rv30_tpel8_v_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_hv_lowpass
	.type	put_rv30_tpel8_hv_lowpass, @function
put_rv30_tpel8_hv_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$2,%hi(mc_coef)
	addiu	$2,$2,%lo(mc_coef)
#APP
 # 221 "rv30dsp.c" 1
	S32LDD xr13,$2,0
 # 0 "" 2
 # 222 "rv30dsp.c" 1
	S32LDD xr14,$2,4
 # 0 "" 2
 # 223 "rv30dsp.c" 1
	S32LDD xr15,$2,8
 # 0 "" 2
#NO_APP
	li	$13,8			# 0x8
	sll	$12,$7,1
$L9:
	addiu	$11,$5,8
	move	$2,$5
	move	$10,$4
$L10:
	subu	$9,$2,$7
#APP
 # 235 "rv30dsp.c" 1
	S8LDD xr1,$9,-1,ptn0
 # 0 "" 2
 # 235 "rv30dsp.c" 1
	S8LDD xr2,$9,0,ptn0
 # 0 "" 2
 # 236 "rv30dsp.c" 1
	S8LDD xr1,$9,2,ptn1
 # 0 "" 2
 # 236 "rv30dsp.c" 1
	S8LDD xr2,$2,-1,ptn1
 # 0 "" 2
#NO_APP
	addu	$3,$2,$12
#APP
 # 237 "rv30dsp.c" 1
	S8LDD xr1,$3,-1,ptn2
 # 0 "" 2
 # 237 "rv30dsp.c" 1
	S8LDD xr2,$2,2,ptn2
 # 0 "" 2
 # 238 "rv30dsp.c" 1
	S8LDD xr1,$3,2,ptn3
 # 0 "" 2
 # 238 "rv30dsp.c" 1
	S8LDD xr2,$3,0,ptn3
 # 0 "" 2
 # 240 "rv30dsp.c" 1
	S8LDD xr3,$9,1,ptn0
 # 0 "" 2
 # 240 "rv30dsp.c" 1
	S8LDD xr4,$2,0,ptn0
 # 0 "" 2
#NO_APP
	subu	$8,$3,$7
#APP
 # 241 "rv30dsp.c" 1
	S8LDD xr3,$8,-1,ptn1
 # 0 "" 2
 # 241 "rv30dsp.c" 1
	S8LDD xr4,$2,1,ptn1
 # 0 "" 2
 # 242 "rv30dsp.c" 1
	S8LDD xr3,$8,2,ptn2
 # 0 "" 2
 # 242 "rv30dsp.c" 1
	S8LDD xr4,$8,0,ptn2
 # 0 "" 2
 # 243 "rv30dsp.c" 1
	S8LDD xr3,$3,1,ptn3
 # 0 "" 2
 # 243 "rv30dsp.c" 1
	S8LDD xr4,$8,1,ptn3
 # 0 "" 2
 # 245 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 246 "rv30dsp.c" 1
	D8SUM xr1,xr0,xr1
 # 0 "" 2
 # 247 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 249 "rv30dsp.c" 1
	Q8MUL xr5,xr4,xr13,xr4
 # 0 "" 2
 # 250 "rv30dsp.c" 1
	S8LDD xr6,$9,0,ptn0
 # 0 "" 2
 # 251 "rv30dsp.c" 1
	Q16ACCM xr5,xr3,xr2,xr4,SS
 # 0 "" 2
 # 253 "rv30dsp.c" 1
	S8LDD xr2,$9,1,ptn0
 # 0 "" 2
 # 254 "rv30dsp.c" 1
	S8LDD xr6,$9,3,ptn1
 # 0 "" 2
 # 255 "rv30dsp.c" 1
	S8LDD xr2,$2,0,ptn1
 # 0 "" 2
 # 256 "rv30dsp.c" 1
	S8LDD xr6,$3,0,ptn2
 # 0 "" 2
 # 257 "rv30dsp.c" 1
	S8LDD xr2,$2,3,ptn2
 # 0 "" 2
 # 258 "rv30dsp.c" 1
	S8LDD xr6,$3,3,ptn3
 # 0 "" 2
 # 259 "rv30dsp.c" 1
	S8LDD xr2,$3,1,ptn3
 # 0 "" 2
 # 261 "rv30dsp.c" 1
	S8LDD xr3,$9,2,ptn0
 # 0 "" 2
 # 262 "rv30dsp.c" 1
	S8LDD xr7,$2,1,ptn0
 # 0 "" 2
 # 263 "rv30dsp.c" 1
	S8LDD xr3,$8,0,ptn1
 # 0 "" 2
 # 264 "rv30dsp.c" 1
	S8LDD xr7,$2,2,ptn1
 # 0 "" 2
 # 265 "rv30dsp.c" 1
	S8LDD xr3,$8,3,ptn2
 # 0 "" 2
 # 266 "rv30dsp.c" 1
	S8LDD xr7,$8,1,ptn2
 # 0 "" 2
 # 267 "rv30dsp.c" 1
	S8LDD xr3,$3,2,ptn3
 # 0 "" 2
 # 268 "rv30dsp.c" 1
	S8LDD xr7,$8,2,ptn3
 # 0 "" 2
 # 270 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 271 "rv30dsp.c" 1
	D8SUM xr6,xr0,xr6
 # 0 "" 2
 # 272 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 274 "rv30dsp.c" 1
	Q8MUL xr8,xr7,xr13,xr7
 # 0 "" 2
 # 275 "rv30dsp.c" 1
	Q16ACCM xr8,xr3,xr2,xr7,SS
 # 0 "" 2
 # 277 "rv30dsp.c" 1
	D32SLL xr3,xr0,xr0,xr2,0
 # 0 "" 2
 # 278 "rv30dsp.c" 1
	D16ASUM xr1,xr4,xr7,xr6,AA
 # 0 "" 2
 # 279 "rv30dsp.c" 1
	D16ASUM xr2,xr5,xr8,xr3,AA
 # 0 "" 2
 # 280 "rv30dsp.c" 1
	D32ASUM xr2,xr1,xr6,xr3,AA
 # 0 "" 2
 # 282 "rv30dsp.c" 1
	D32ASUM xr2,xr15,xr15,xr3,AA
 # 0 "" 2
 # 283 "rv30dsp.c" 1
	D32SAR xr2,xr2,xr3,xr3,8
 # 0 "" 2
 # 284 "rv30dsp.c" 1
	Q16SAT xr2,xr3,xr2
 # 0 "" 2
 # 285 "rv30dsp.c" 1
	S8STD xr2,$10,0,ptn0
 # 0 "" 2
 # 286 "rv30dsp.c" 1
	S8STD xr2,$10,1,ptn2
 # 0 "" 2
#NO_APP
	addiu	$2,$2,2
	.set	noreorder
	.set	nomacro
	bne	$11,$2,$L10
	addiu	$10,$10,2
	.set	macro
	.set	reorder

	addiu	$13,$13,-1
	addu	$5,$5,$7
	.set	noreorder
	.set	nomacro
	bne	$13,$0,$L9
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	j	$31
	.end	put_rv30_tpel8_hv_lowpass
	.size	put_rv30_tpel8_hv_lowpass, .-put_rv30_tpel8_hv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_hhv_lowpass
	.type	put_rv30_tpel8_hhv_lowpass, @function
put_rv30_tpel8_hhv_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$2,%hi(mc_coef)
	addiu	$2,$2,%lo(mc_coef)
#APP
 # 317 "rv30dsp.c" 1
	S32LDD xr13,$2,0
 # 0 "" 2
 # 318 "rv30dsp.c" 1
	S32LDD xr14,$2,4
 # 0 "" 2
 # 319 "rv30dsp.c" 1
	S32LDD xr15,$2,8
 # 0 "" 2
#NO_APP
	li	$13,8			# 0x8
	sll	$12,$7,1
$L14:
	addiu	$11,$5,8
	move	$2,$5
	move	$10,$4
$L15:
	subu	$9,$2,$7
#APP
 # 323 "rv30dsp.c" 1
	S8LDD xr1,$9,-1,ptn0
 # 0 "" 2
 # 323 "rv30dsp.c" 1
	S8LDD xr1,$9,2,ptn1
 # 0 "" 2
#NO_APP
	addu	$3,$2,$12
#APP
 # 324 "rv30dsp.c" 1
	S8LDD xr1,$3,-1,ptn2
 # 0 "" 2
 # 324 "rv30dsp.c" 1
	S8LDD xr1,$3,2,ptn3
 # 0 "" 2
 # 326 "rv30dsp.c" 1
	S8LDD xr2,$9,1,ptn0
 # 0 "" 2
 # 326 "rv30dsp.c" 1
	S8LDD xr2,$2,-1,ptn1
 # 0 "" 2
 # 327 "rv30dsp.c" 1
	S8LDD xr2,$2,2,ptn2
 # 0 "" 2
 # 327 "rv30dsp.c" 1
	S8LDD xr2,$3,1,ptn3
 # 0 "" 2
 # 329 "rv30dsp.c" 1
	S8LDD xr3,$9,0,ptn0
 # 0 "" 2
#NO_APP
	subu	$8,$3,$7
#APP
 # 329 "rv30dsp.c" 1
	S8LDD xr3,$8,-1,ptn1
 # 0 "" 2
 # 330 "rv30dsp.c" 1
	S8LDD xr3,$8,2,ptn2
 # 0 "" 2
 # 330 "rv30dsp.c" 1
	S8LDD xr3,$3,0,ptn3
 # 0 "" 2
 # 331 "rv30dsp.c" 1
	S8LDD xr4,$2,1,ptn0
 # 0 "" 2
 # 332 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 333 "rv30dsp.c" 1
	D8SUM xr1,xr0,xr1
 # 0 "" 2
 # 334 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 336 "rv30dsp.c" 1
	S8LDD xr4,$2,0,ptn1
 # 0 "" 2
 # 337 "rv30dsp.c" 1
	S8LDD xr4,$8,1,ptn2
 # 0 "" 2
 # 337 "rv30dsp.c" 1
	S8LDD xr4,$8,0,ptn3
 # 0 "" 2
 # 338 "rv30dsp.c" 1
	S8LDD xr6,$9,0,ptn0
 # 0 "" 2
 # 339 "rv30dsp.c" 1
	Q8MUL xr5,xr4,xr13,xr4
 # 0 "" 2
 # 340 "rv30dsp.c" 1
	Q16ACCM xr5,xr3,xr2,xr4,SS
 # 0 "" 2
 # 342 "rv30dsp.c" 1
	S8LDD xr6,$9,3,ptn1
 # 0 "" 2
 # 343 "rv30dsp.c" 1
	S8LDD xr6,$3,0,ptn2
 # 0 "" 2
 # 343 "rv30dsp.c" 1
	S8LDD xr6,$3,3,ptn3
 # 0 "" 2
 # 345 "rv30dsp.c" 1
	S8LDD xr2,$9,2,ptn0
 # 0 "" 2
 # 345 "rv30dsp.c" 1
	S8LDD xr2,$2,0,ptn1
 # 0 "" 2
 # 346 "rv30dsp.c" 1
	S8LDD xr2,$2,3,ptn2
 # 0 "" 2
 # 346 "rv30dsp.c" 1
	S8LDD xr2,$3,2,ptn3
 # 0 "" 2
 # 348 "rv30dsp.c" 1
	S8LDD xr3,$9,1,ptn0
 # 0 "" 2
 # 348 "rv30dsp.c" 1
	S8LDD xr3,$8,0,ptn1
 # 0 "" 2
 # 349 "rv30dsp.c" 1
	S8LDD xr3,$8,3,ptn2
 # 0 "" 2
 # 349 "rv30dsp.c" 1
	S8LDD xr3,$3,1,ptn3
 # 0 "" 2
 # 350 "rv30dsp.c" 1
	S8LDD xr7,$2,2,ptn0
 # 0 "" 2
 # 351 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 352 "rv30dsp.c" 1
	D8SUM xr6,xr0,xr6
 # 0 "" 2
 # 353 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 355 "rv30dsp.c" 1
	S8LDD xr7,$2,1,ptn1
 # 0 "" 2
 # 356 "rv30dsp.c" 1
	S8LDD xr7,$8,2,ptn2
 # 0 "" 2
 # 356 "rv30dsp.c" 1
	S8LDD xr7,$8,1,ptn3
 # 0 "" 2
 # 357 "rv30dsp.c" 1
	Q8MUL xr8,xr7,xr13,xr7
 # 0 "" 2
 # 358 "rv30dsp.c" 1
	Q16ACCM xr8,xr3,xr2,xr7,SS
 # 0 "" 2
 # 360 "rv30dsp.c" 1
	D32SLL xr3,xr0,xr0,xr2,0
 # 0 "" 2
 # 361 "rv30dsp.c" 1
	D16ASUM xr1,xr4,xr7,xr6,AA
 # 0 "" 2
 # 362 "rv30dsp.c" 1
	D16ASUM xr2,xr5,xr8,xr3,AA
 # 0 "" 2
 # 363 "rv30dsp.c" 1
	D32ASUM xr2,xr1,xr6,xr3,AA
 # 0 "" 2
 # 365 "rv30dsp.c" 1
	D32ASUM xr2,xr15,xr15,xr3,AA
 # 0 "" 2
 # 366 "rv30dsp.c" 1
	D32SAR xr2,xr2,xr3,xr3,8
 # 0 "" 2
 # 367 "rv30dsp.c" 1
	Q16SAT xr2,xr3,xr2
 # 0 "" 2
 # 368 "rv30dsp.c" 1
	S8STD xr2,$10,0,ptn0
 # 0 "" 2
 # 369 "rv30dsp.c" 1
	S8STD xr2,$10,1,ptn2
 # 0 "" 2
#NO_APP
	addiu	$2,$2,2
	.set	noreorder
	.set	nomacro
	bne	$11,$2,$L15
	addiu	$10,$10,2
	.set	macro
	.set	reorder

	addiu	$13,$13,-1
	addu	$5,$5,$7
	.set	noreorder
	.set	nomacro
	bne	$13,$0,$L14
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	j	$31
	.end	put_rv30_tpel8_hhv_lowpass
	.size	put_rv30_tpel8_hhv_lowpass, .-put_rv30_tpel8_hhv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_hvv_lowpass
	.type	put_rv30_tpel8_hvv_lowpass, @function
put_rv30_tpel8_hvv_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$2,%hi(mc_coef)
	addiu	$2,$2,%lo(mc_coef)
#APP
 # 400 "rv30dsp.c" 1
	S32LDD xr13,$2,0
 # 0 "" 2
 # 401 "rv30dsp.c" 1
	S32LDD xr14,$2,4
 # 0 "" 2
 # 402 "rv30dsp.c" 1
	S32LDD xr15,$2,8
 # 0 "" 2
#NO_APP
	li	$13,8			# 0x8
	sll	$12,$7,1
$L19:
	addiu	$11,$5,8
	move	$2,$5
	move	$10,$4
$L20:
	subu	$9,$2,$7
#APP
 # 406 "rv30dsp.c" 1
	S8LDD xr1,$9,-1,ptn0
 # 0 "" 2
 # 406 "rv30dsp.c" 1
	S8LDD xr1,$9,2,ptn1
 # 0 "" 2
#NO_APP
	addu	$3,$2,$12
#APP
 # 407 "rv30dsp.c" 1
	S8LDD xr1,$3,-1,ptn2
 # 0 "" 2
 # 407 "rv30dsp.c" 1
	S8LDD xr1,$3,2,ptn3
 # 0 "" 2
 # 409 "rv30dsp.c" 1
	S8LDD xr2,$9,0,ptn0
 # 0 "" 2
#NO_APP
	subu	$8,$3,$7
#APP
 # 409 "rv30dsp.c" 1
	S8LDD xr2,$8,-1,ptn1
 # 0 "" 2
 # 410 "rv30dsp.c" 1
	S8LDD xr2,$8,2,ptn2
 # 0 "" 2
 # 410 "rv30dsp.c" 1
	S8LDD xr2,$3,0,ptn3
 # 0 "" 2
 # 411 "rv30dsp.c" 1
	D8SUM xr1,xr0,xr1
 # 0 "" 2
 # 413 "rv30dsp.c" 1
	S8LDD xr3,$9,1,ptn0
 # 0 "" 2
 # 413 "rv30dsp.c" 1
	S8LDD xr3,$2,-1,ptn1
 # 0 "" 2
 # 414 "rv30dsp.c" 1
	S8LDD xr3,$2,2,ptn2
 # 0 "" 2
 # 414 "rv30dsp.c" 1
	S8LDD xr3,$3,1,ptn3
 # 0 "" 2
 # 415 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 416 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 418 "rv30dsp.c" 1
	S8LDD xr4,$8,0,ptn0
 # 0 "" 2
 # 418 "rv30dsp.c" 1
	S8LDD xr4,$8,1,ptn1
 # 0 "" 2
 # 419 "rv30dsp.c" 1
	S8LDD xr4,$2,0,ptn2
 # 0 "" 2
 # 419 "rv30dsp.c" 1
	S8LDD xr4,$2,1,ptn3
 # 0 "" 2
 # 420 "rv30dsp.c" 1
	Q8MUL xr5,xr4,xr13,xr4
 # 0 "" 2
 # 421 "rv30dsp.c" 1
	Q16ACCM xr5,xr3,xr2,xr4,SS
 # 0 "" 2
 # 423 "rv30dsp.c" 1
	S8LDD xr6,$9,0,ptn0
 # 0 "" 2
 # 423 "rv30dsp.c" 1
	S8LDD xr6,$9,3,ptn1
 # 0 "" 2
 # 424 "rv30dsp.c" 1
	S8LDD xr6,$3,0,ptn2
 # 0 "" 2
 # 424 "rv30dsp.c" 1
	S8LDD xr6,$3,3,ptn3
 # 0 "" 2
 # 426 "rv30dsp.c" 1
	S8LDD xr2,$9,1,ptn0
 # 0 "" 2
 # 426 "rv30dsp.c" 1
	S8LDD xr2,$8,0,ptn1
 # 0 "" 2
 # 427 "rv30dsp.c" 1
	S8LDD xr2,$8,3,ptn2
 # 0 "" 2
 # 427 "rv30dsp.c" 1
	S8LDD xr2,$3,1,ptn3
 # 0 "" 2
 # 428 "rv30dsp.c" 1
	D8SUM xr6,xr0,xr6
 # 0 "" 2
 # 430 "rv30dsp.c" 1
	S8LDD xr3,$9,2,ptn0
 # 0 "" 2
 # 430 "rv30dsp.c" 1
	S8LDD xr3,$2,0,ptn1
 # 0 "" 2
 # 431 "rv30dsp.c" 1
	S8LDD xr3,$2,3,ptn2
 # 0 "" 2
 # 431 "rv30dsp.c" 1
	S8LDD xr3,$3,2,ptn3
 # 0 "" 2
 # 432 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 433 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 435 "rv30dsp.c" 1
	S8LDD xr7,$8,1,ptn0
 # 0 "" 2
 # 435 "rv30dsp.c" 1
	S8LDD xr7,$8,2,ptn1
 # 0 "" 2
 # 436 "rv30dsp.c" 1
	S8LDD xr7,$2,1,ptn2
 # 0 "" 2
 # 436 "rv30dsp.c" 1
	S8LDD xr7,$2,2,ptn3
 # 0 "" 2
 # 437 "rv30dsp.c" 1
	Q8MUL xr8,xr7,xr13,xr7
 # 0 "" 2
 # 438 "rv30dsp.c" 1
	Q16ACCM xr8,xr3,xr2,xr7,SS
 # 0 "" 2
 # 440 "rv30dsp.c" 1
	D32SLL xr3,xr0,xr0,xr2,0
 # 0 "" 2
 # 441 "rv30dsp.c" 1
	D16ASUM xr1,xr4,xr7,xr6,AA
 # 0 "" 2
 # 442 "rv30dsp.c" 1
	D16ASUM xr2,xr5,xr8,xr3,AA
 # 0 "" 2
 # 443 "rv30dsp.c" 1
	D32ASUM xr2,xr1,xr6,xr3,AA
 # 0 "" 2
 # 445 "rv30dsp.c" 1
	D32ASUM xr2,xr15,xr15,xr3,AA
 # 0 "" 2
 # 446 "rv30dsp.c" 1
	D32SAR xr2,xr2,xr3,xr3,8
 # 0 "" 2
 # 447 "rv30dsp.c" 1
	Q16SAT xr2,xr3,xr2
 # 0 "" 2
 # 448 "rv30dsp.c" 1
	S8STD xr2,$10,0,ptn0
 # 0 "" 2
 # 449 "rv30dsp.c" 1
	S8STD xr2,$10,1,ptn2
 # 0 "" 2
#NO_APP
	addiu	$2,$2,2
	.set	noreorder
	.set	nomacro
	bne	$11,$2,$L20
	addiu	$10,$10,2
	.set	macro
	.set	reorder

	addiu	$13,$13,-1
	addu	$5,$5,$7
	.set	noreorder
	.set	nomacro
	bne	$13,$0,$L19
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	j	$31
	.end	put_rv30_tpel8_hvv_lowpass
	.size	put_rv30_tpel8_hvv_lowpass, .-put_rv30_tpel8_hvv_lowpass
	.rdata
	.align	2
$LC0:
	.byte	36
	.byte	54
	.byte	6
	.byte	54
	.byte	81
	.byte	9
	.byte	6
	.byte	9
	.byte	-128
	.byte	0
	.byte	0
	.byte	0
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_hhvv_lowpass
	.type	put_rv30_tpel8_hhvv_lowpass, @function
put_rv30_tpel8_hhvv_lowpass:
	.frame	$sp,24,$31		# vars= 16, regs= 0/0, args= 0, gp= 8
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$3,%hi($LC0)
	addiu	$sp,$sp,-24
	addiu	$2,$3,%lo($LC0)
	lwl	$10,3($2)
	lwl	$9,7($2)
	lwl	$8,11($2)
	lwr	$10,%lo($LC0)($3)
	move	$3,$9
	lwr	$8,8($2)
	lwr	$3,4($2)
	addiu	$2,$sp,8
	sw	$10,8($sp)
	sw	$8,16($sp)
	sw	$3,12($sp)
#APP
 # 481 "rv30dsp.c" 1
	S32LDD xr13,$2,0
 # 0 "" 2
 # 482 "rv30dsp.c" 1
	S32LDD xr14,$2,4
 # 0 "" 2
 # 483 "rv30dsp.c" 1
	S32LDD xr15,$2,8
 # 0 "" 2
#NO_APP
	li	$12,8			# 0x8
	sll	$11,$7,1
	li	$10,8			# 0x8
$L24:
	move	$8,$0
$L25:
	addu	$9,$5,$8
#APP
 # 487 "rv30dsp.c" 1
	S8LDD xr1,$9,0,ptn0
 # 0 "" 2
 # 487 "rv30dsp.c" 1
	S8LDD xr1,$9,1,ptn1
 # 0 "" 2
 # 488 "rv30dsp.c" 1
	S8LDD xr1,$9,2,ptn2
 # 0 "" 2
#NO_APP
	addu	$3,$7,$8
	addu	$3,$5,$3
#APP
 # 488 "rv30dsp.c" 1
	S8LDD xr1,$3,0,ptn3
 # 0 "" 2
 # 489 "rv30dsp.c" 1
	Q8MUL xr2,xr1,xr13,xr1
 # 0 "" 2
 # 491 "rv30dsp.c" 1
	S8LDD xr3,$3,1,ptn0
 # 0 "" 2
 # 491 "rv30dsp.c" 1
	S8LDD xr3,$3,2,ptn1
 # 0 "" 2
#NO_APP
	addu	$2,$11,$8
	addu	$2,$5,$2
#APP
 # 492 "rv30dsp.c" 1
	S8LDD xr3,$2,0,ptn2
 # 0 "" 2
 # 492 "rv30dsp.c" 1
	S8LDD xr3,$2,1,ptn3
 # 0 "" 2
 # 493 "rv30dsp.c" 1
	Q8MUL xr4,xr3,xr14,xr3
 # 0 "" 2
 # 495 "rv30dsp.c" 1
	S8LDD xr7,$9,1,ptn0
 # 0 "" 2
 # 495 "rv30dsp.c" 1
	S8LDD xr7,$9,2,ptn1
 # 0 "" 2
 # 496 "rv30dsp.c" 1
	S8LDD xr7,$9,3,ptn2
 # 0 "" 2
 # 496 "rv30dsp.c" 1
	S8LDD xr7,$3,1,ptn3
 # 0 "" 2
 # 497 "rv30dsp.c" 1
	Q8MUL xr8,xr7,xr13,xr7
 # 0 "" 2
 # 499 "rv30dsp.c" 1
	S8LDD xr9,$3,2,ptn0
 # 0 "" 2
 # 499 "rv30dsp.c" 1
	S8LDD xr9,$3,3,ptn1
 # 0 "" 2
 # 500 "rv30dsp.c" 1
	S8LDD xr9,$2,1,ptn2
 # 0 "" 2
 # 500 "rv30dsp.c" 1
	S8LDD xr9,$2,2,ptn3
 # 0 "" 2
 # 501 "rv30dsp.c" 1
	Q8MUL xr10,xr9,xr14,xr9
 # 0 "" 2
 # 503 "rv30dsp.c" 1
	D32ADD xr5,xr0,xr0,xr6,AA
 # 0 "" 2
 # 504 "rv30dsp.c" 1
	S8LDD xr5,$2,2,ptn0
 # 0 "" 2
 # 505 "rv30dsp.c" 1
	S8LDD xr6,$2,3,ptn0
 # 0 "" 2
 # 506 "rv30dsp.c" 1
	Q16ACCM xr1,xr2,xr4,xr3,AA
 # 0 "" 2
 # 507 "rv30dsp.c" 1
	Q16ACCM xr7,xr8,xr10,xr9,AA
 # 0 "" 2
 # 508 "rv30dsp.c" 1
	Q16ACCM xr1,xr3,xr9,xr7,AA
 # 0 "" 2
 # 509 "rv30dsp.c" 1
	D16ASUM xr5,xr1,xr7,xr6,AA
 # 0 "" 2
 # 510 "rv30dsp.c" 1
	D32ASUM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 511 "rv30dsp.c" 1
	D32SLR xr5,xr5,xr6,xr6,8
 # 0 "" 2
 # 513 "rv30dsp.c" 1
	Q16SAT xr5,xr6,xr5
 # 0 "" 2
#NO_APP
	addu	$2,$4,$8
#APP
 # 514 "rv30dsp.c" 1
	S8STD xr5,$2,0,ptn0
 # 0 "" 2
 # 515 "rv30dsp.c" 1
	S8STD xr5,$2,1,ptn2
 # 0 "" 2
#NO_APP
	addiu	$8,$8,2
	bne	$8,$10,$L25
	addiu	$12,$12,-1
	addu	$5,$5,$7
	.set	noreorder
	.set	nomacro
	bne	$12,$0,$L24
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	put_rv30_tpel8_hhvv_lowpass
	.size	put_rv30_tpel8_hhvv_lowpass, .-put_rv30_tpel8_hhvv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_h_lowpass
	.type	avg_rv30_tpel8_h_lowpass, @function
avg_rv30_tpel8_h_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	addiu	$2,$sp,16
#APP
 # 544 "rv30dsp.c" 1
	S8LDD xr13,$2,0,ptn7
 # 0 "" 2
#NO_APP
	addiu	$2,$sp,20
#APP
 # 545 "rv30dsp.c" 1
	S8LDD xr14,$2,0,ptn7
 # 0 "" 2
#NO_APP
	li	$2,524288			# 0x80000
	addiu	$2,$2,8
#APP
 # 547 "rv30dsp.c" 1
	S32I2M xr15,$2
 # 0 "" 2
#NO_APP
	li	$2,8			# 0x8
$L30:
#APP
 # 550 "rv30dsp.c" 1
	S8LDD xr1,$5,-1,ptn3
 # 0 "" 2
 # 550 "rv30dsp.c" 1
	S8LDD xr3,$5,1,ptn3
 # 0 "" 2
 # 551 "rv30dsp.c" 1
	S8LDD xr1,$5,0,ptn2
 # 0 "" 2
 # 551 "rv30dsp.c" 1
	S8LDD xr3,$5,2,ptn2
 # 0 "" 2
 # 552 "rv30dsp.c" 1
	S8LDD xr1,$5,1,ptn1
 # 0 "" 2
 # 552 "rv30dsp.c" 1
	S8LDD xr3,$5,3,ptn1
 # 0 "" 2
 # 553 "rv30dsp.c" 1
	S8LDD xr1,$5,2,ptn0
 # 0 "" 2
 # 553 "rv30dsp.c" 1
	S8LDD xr3,$5,4,ptn0
 # 0 "" 2
 # 554 "rv30dsp.c" 1
	D32SLL xr2,xr1,xr1,xr2,8
 # 0 "" 2
 # 555 "rv30dsp.c" 1
	D32SLL xr4,xr3,xr3,xr4,8
 # 0 "" 2
 # 556 "rv30dsp.c" 1
	S8LDD xr2,$5,3,ptn0
 # 0 "" 2
 # 557 "rv30dsp.c" 1
	S8LDD xr4,$5,5,ptn0
 # 0 "" 2
 # 558 "rv30dsp.c" 1
	Q8MUL xr5,xr2,xr13,xr6
 # 0 "" 2
 # 559 "rv30dsp.c" 1
	Q8MUL xr7,xr3,xr14,xr8
 # 0 "" 2
 # 561 "rv30dsp.c" 1
	Q8ADDE xr9,xr1,xr4,xr10,AA
 # 0 "" 2
 # 562 "rv30dsp.c" 1
	Q16ACCM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 563 "rv30dsp.c" 1
	Q16ACCM xr7,xr9,xr10,xr8,SS
 # 0 "" 2
 # 565 "rv30dsp.c" 1
	S8LDD xr1,$5,3,ptn3
 # 0 "" 2
 # 565 "rv30dsp.c" 1
	S8LDD xr3,$5,5,ptn3
 # 0 "" 2
 # 566 "rv30dsp.c" 1
	Q16ACCM xr5,xr7,xr8,xr6,AA
 # 0 "" 2
 # 567 "rv30dsp.c" 1
	S8LDD xr1,$5,4,ptn2
 # 0 "" 2
 # 567 "rv30dsp.c" 1
	S8LDD xr3,$5,6,ptn2
 # 0 "" 2
 # 568 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr6,xr6,4
 # 0 "" 2
 # 569 "rv30dsp.c" 1
	S8LDD xr1,$5,5,ptn1
 # 0 "" 2
 # 569 "rv30dsp.c" 1
	S8LDD xr3,$5,7,ptn1
 # 0 "" 2
 # 570 "rv30dsp.c" 1
	Q16SAT xr9,xr5,xr6
 # 0 "" 2
 # 571 "rv30dsp.c" 1
	S8LDD xr1,$5,6,ptn0
 # 0 "" 2
 # 571 "rv30dsp.c" 1
	S8LDD xr3,$5,8,ptn0
 # 0 "" 2
 # 572 "rv30dsp.c" 1
	S32LDDR xr8,$4,0
 # 0 "" 2
 # 573 "rv30dsp.c" 1
	Q8AVGR xr9,xr8,xr9
 # 0 "" 2
 # 574 "rv30dsp.c" 1
	S32STDR xr9,$4,0
 # 0 "" 2
 # 576 "rv30dsp.c" 1
	D32SLL xr2,xr1,xr1,xr2,8
 # 0 "" 2
 # 577 "rv30dsp.c" 1
	D32SLL xr4,xr3,xr3,xr4,8
 # 0 "" 2
 # 578 "rv30dsp.c" 1
	S8LDD xr2,$5,7,ptn0
 # 0 "" 2
 # 579 "rv30dsp.c" 1
	S8LDD xr4,$5,9,ptn0
 # 0 "" 2
 # 581 "rv30dsp.c" 1
	Q8MUL xr5,xr2,xr13,xr6
 # 0 "" 2
 # 582 "rv30dsp.c" 1
	Q8MUL xr7,xr3,xr14,xr8
 # 0 "" 2
 # 584 "rv30dsp.c" 1
	Q8ADDE xr9,xr1,xr4,xr10,AA
 # 0 "" 2
 # 585 "rv30dsp.c" 1
	Q16ACCM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 586 "rv30dsp.c" 1
	Q16ACCM xr7,xr9,xr10,xr8,SS
 # 0 "" 2
 # 587 "rv30dsp.c" 1
	Q16ACCM xr5,xr7,xr8,xr6,AA
 # 0 "" 2
 # 588 "rv30dsp.c" 1
	S32LDDR xr8,$4,4
 # 0 "" 2
 # 589 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr6,xr6,4
 # 0 "" 2
 # 590 "rv30dsp.c" 1
	Q16SAT xr9,xr5,xr6
 # 0 "" 2
 # 591 "rv30dsp.c" 1
	Q8AVGR xr9,xr8,xr9
 # 0 "" 2
 # 592 "rv30dsp.c" 1
	S32STDR xr9,$4,4
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-1
	addu	$4,$4,$6
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L30
	addu	$5,$5,$7
	.set	macro
	.set	reorder

	j	$31
	.end	avg_rv30_tpel8_h_lowpass
	.size	avg_rv30_tpel8_h_lowpass, .-avg_rv30_tpel8_h_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_v_lowpass
	.type	avg_rv30_tpel8_v_lowpass, @function
avg_rv30_tpel8_v_lowpass:
	.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x00010000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-8
	addiu	$2,$sp,24
	sw	$16,4($sp)
#APP
 # 625 "rv30dsp.c" 1
	S8LDD xr14,$2,0,ptn7
 # 0 "" 2
#NO_APP
	addiu	$2,$sp,28
#APP
 # 626 "rv30dsp.c" 1
	S8LDD xr14,$2,0,ptn2
 # 0 "" 2
 # 627 "rv30dsp.c" 1
	S8LDD xr14,$2,0,ptn3
 # 0 "" 2
#NO_APP
	li	$2,524288			# 0x80000
	addiu	$2,$2,8
#APP
 # 628 "rv30dsp.c" 1
	S32I2M xr15,$2
 # 0 "" 2
#NO_APP
	li	$15,-201326592			# 0xfffffffff4000000
	subu	$25,$0,$7
	addiu	$24,$4,8
	addiu	$2,$15,1028
$L33:
	addu	$9,$5,$25
	addu	$8,$5,$7
	lbu	$9,0($9)
	addu	$14,$8,$7
	addu	$13,$14,$7
	sb	$9,0($2)
	addu	$12,$13,$7
	lbu	$9,0($5)
	addu	$11,$12,$7
	addu	$10,$11,$7
	sb	$9,1029($15)
	lbu	$16,0($8)
	addu	$9,$10,$7
	addu	$8,$9,$7
	sb	$16,1030($15)
	addu	$16,$8,$7
	lbu	$14,0($14)
	sb	$14,1031($15)
	lbu	$13,0($13)
	sb	$13,1032($15)
	lbu	$12,0($12)
	sb	$12,1033($15)
	lbu	$11,0($11)
	sb	$11,1034($15)
	lbu	$10,0($10)
	sb	$10,1035($15)
	lbu	$9,0($9)
	sb	$9,1036($15)
	lbu	$8,0($8)
	sb	$8,1037($15)
	lbu	$8,0($16)
	sb	$8,1038($15)
#APP
 # 644 "rv30dsp.c" 1
	S8LDD xr1,$2,2,ptn7
 # 0 "" 2
 # 644 "rv30dsp.c" 1
	S8LDD xr1,$2,1,ptn0
 # 0 "" 2
 # 644 "rv30dsp.c" 1
	S8LDD xr1,$2,3,ptn3
 # 0 "" 2
 # 645 "rv30dsp.c" 1
	S8LDD xr2,$2,4,ptn7
 # 0 "" 2
 # 645 "rv30dsp.c" 1
	S8LDD xr2,$2,3,ptn0
 # 0 "" 2
 # 645 "rv30dsp.c" 1
	S8LDD xr2,$2,5,ptn3
 # 0 "" 2
 # 646 "rv30dsp.c" 1
	Q8MUL xr6,xr1,xr14,xr5
 # 0 "" 2
 # 647 "rv30dsp.c" 1
	S32LDD xr3,$2,0
 # 0 "" 2
 # 648 "rv30dsp.c" 1
	S8LDD xr4,$2,3,ptn0
 # 0 "" 2
 # 648 "rv30dsp.c" 1
	S8LDD xr4,$2,4,ptn1
 # 0 "" 2
 # 648 "rv30dsp.c" 1
	S8LDD xr4,$2,5,ptn2
 # 0 "" 2
 # 648 "rv30dsp.c" 1
	S8LDD xr4,$2,6,ptn3
 # 0 "" 2
 # 649 "rv30dsp.c" 1
	Q8MUL xr8,xr2,xr14,xr7
 # 0 "" 2
 # 650 "rv30dsp.c" 1
	Q8ADDE xr10,xr3,xr4,xr9,AA
 # 0 "" 2
 # 652 "rv30dsp.c" 1
	Q16ACCM xr5,xr9,xr10,xr7,SS
 # 0 "" 2
 # 653 "rv30dsp.c" 1
	Q16ACCM xr6,xr15,xr15,xr8,AA
 # 0 "" 2
 # 654 "rv30dsp.c" 1
	Q16ACCM xr5,xr6,xr8,xr7,AA
 # 0 "" 2
 # 655 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr7,xr7,4
 # 0 "" 2
 # 656 "rv30dsp.c" 1
	Q16SAT xr5,xr7,xr5
 # 0 "" 2
 # 658 "rv30dsp.c" 1
	S8LDD xr2,$4,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$9,$4,$6
#APP
 # 659 "rv30dsp.c" 1
	S8LDD xr2,$9,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$8,$9,$6
#APP
 # 660 "rv30dsp.c" 1
	S8LDD xr2,$8,0,ptn2
 # 0 "" 2
#NO_APP
	addu	$3,$8,$6
#APP
 # 661 "rv30dsp.c" 1
	S8LDD xr2,$3,0,ptn3
 # 0 "" 2
 # 662 "rv30dsp.c" 1
	Q8AVGR xr5,xr2,xr5
 # 0 "" 2
 # 664 "rv30dsp.c" 1
	S8STD xr5,$4,0,ptn0
 # 0 "" 2
 # 665 "rv30dsp.c" 1
	S8STD xr5,$9,0,ptn1
 # 0 "" 2
 # 666 "rv30dsp.c" 1
	S8STD xr5,$8,0,ptn2
 # 0 "" 2
 # 667 "rv30dsp.c" 1
	S8STD xr5,$3,0,ptn3
 # 0 "" 2
 # 669 "rv30dsp.c" 1
	S8LDD xr1,$2,6,ptn7
 # 0 "" 2
 # 669 "rv30dsp.c" 1
	S8LDD xr1,$2,5,ptn0
 # 0 "" 2
 # 669 "rv30dsp.c" 1
	S8LDD xr1,$2,7,ptn3
 # 0 "" 2
 # 670 "rv30dsp.c" 1
	S8LDD xr2,$2,8,ptn7
 # 0 "" 2
 # 670 "rv30dsp.c" 1
	S8LDD xr2,$2,7,ptn0
 # 0 "" 2
 # 670 "rv30dsp.c" 1
	S8LDD xr2,$2,9,ptn3
 # 0 "" 2
 # 671 "rv30dsp.c" 1
	Q8MUL xr6,xr1,xr14,xr5
 # 0 "" 2
 # 672 "rv30dsp.c" 1
	S32LDD xr3,$2,4
 # 0 "" 2
 # 673 "rv30dsp.c" 1
	S8LDD xr4,$2,7,ptn0
 # 0 "" 2
 # 673 "rv30dsp.c" 1
	S8LDD xr4,$2,8,ptn1
 # 0 "" 2
 # 673 "rv30dsp.c" 1
	S8LDD xr4,$2,9,ptn2
 # 0 "" 2
 # 673 "rv30dsp.c" 1
	S8LDD xr4,$2,10,ptn3
 # 0 "" 2
 # 674 "rv30dsp.c" 1
	Q8MUL xr8,xr2,xr14,xr7
 # 0 "" 2
 # 675 "rv30dsp.c" 1
	Q8ADDE xr10,xr3,xr4,xr9,AA
 # 0 "" 2
 # 677 "rv30dsp.c" 1
	Q16ACCM xr5,xr9,xr10,xr7,SS
 # 0 "" 2
 # 678 "rv30dsp.c" 1
	Q16ACCM xr6,xr15,xr15,xr8,AA
 # 0 "" 2
 # 679 "rv30dsp.c" 1
	Q16ACCM xr5,xr6,xr8,xr7,AA
 # 0 "" 2
 # 680 "rv30dsp.c" 1
	Q16SAR xr5,xr5,xr7,xr7,4
 # 0 "" 2
 # 681 "rv30dsp.c" 1
	Q16SAT xr5,xr7,xr5
 # 0 "" 2
#NO_APP
	addu	$3,$3,$6
#APP
 # 683 "rv30dsp.c" 1
	S8LDD xr2,$3,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$9,$3,$6
#APP
 # 684 "rv30dsp.c" 1
	S8LDD xr2,$9,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$8,$9,$6
#APP
 # 685 "rv30dsp.c" 1
	S8LDD xr2,$8,0,ptn2
 # 0 "" 2
#NO_APP
	addu	$10,$8,$6
#APP
 # 686 "rv30dsp.c" 1
	S8LDD xr2,$10,0,ptn3
 # 0 "" 2
 # 687 "rv30dsp.c" 1
	Q8AVGR xr5,xr2,xr5
 # 0 "" 2
 # 689 "rv30dsp.c" 1
	S8STD xr5,$3,0,ptn0
 # 0 "" 2
 # 690 "rv30dsp.c" 1
	S8STD xr5,$9,0,ptn1
 # 0 "" 2
 # 691 "rv30dsp.c" 1
	S8STD xr5,$8,0,ptn2
 # 0 "" 2
 # 692 "rv30dsp.c" 1
	S8STD xr5,$10,0,ptn3
 # 0 "" 2
#NO_APP
	addiu	$4,$4,1
	.set	noreorder
	.set	nomacro
	bne	$4,$24,$L33
	addiu	$5,$5,1
	.set	macro
	.set	reorder

	lw	$16,4($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,8
	.set	macro
	.set	reorder

	.end	avg_rv30_tpel8_v_lowpass
	.size	avg_rv30_tpel8_v_lowpass, .-avg_rv30_tpel8_v_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_hv_lowpass
	.type	avg_rv30_tpel8_hv_lowpass, @function
avg_rv30_tpel8_hv_lowpass:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$2,%hi(mc_coef)
	addiu	$2,$2,%lo(mc_coef)
#APP
 # 715 "rv30dsp.c" 1
	S32LDD xr13,$2,0
 # 0 "" 2
 # 716 "rv30dsp.c" 1
	S32LDD xr14,$2,4
 # 0 "" 2
 # 717 "rv30dsp.c" 1
	S32LDD xr15,$2,8
 # 0 "" 2
#NO_APP
	li	$13,8			# 0x8
	sll	$12,$7,1
$L37:
	addiu	$11,$5,8
	move	$2,$5
	move	$10,$4
$L38:
	subu	$9,$2,$7
#APP
 # 721 "rv30dsp.c" 1
	S8LDD xr1,$9,-1,ptn0
 # 0 "" 2
 # 721 "rv30dsp.c" 1
	S8LDD xr1,$9,2,ptn1
 # 0 "" 2
#NO_APP
	addu	$3,$2,$12
#APP
 # 722 "rv30dsp.c" 1
	S8LDD xr1,$3,-1,ptn2
 # 0 "" 2
 # 722 "rv30dsp.c" 1
	S8LDD xr1,$3,2,ptn3
 # 0 "" 2
 # 724 "rv30dsp.c" 1
	S8LDD xr2,$9,0,ptn0
 # 0 "" 2
 # 724 "rv30dsp.c" 1
	S8LDD xr2,$2,-1,ptn1
 # 0 "" 2
 # 725 "rv30dsp.c" 1
	S8LDD xr2,$2,2,ptn2
 # 0 "" 2
 # 725 "rv30dsp.c" 1
	S8LDD xr2,$3,0,ptn3
 # 0 "" 2
 # 726 "rv30dsp.c" 1
	D8SUM xr1,xr0,xr1
 # 0 "" 2
 # 728 "rv30dsp.c" 1
	S8LDD xr3,$9,1,ptn0
 # 0 "" 2
#NO_APP
	subu	$8,$3,$7
#APP
 # 728 "rv30dsp.c" 1
	S8LDD xr3,$8,-1,ptn1
 # 0 "" 2
 # 729 "rv30dsp.c" 1
	S8LDD xr3,$8,2,ptn2
 # 0 "" 2
 # 729 "rv30dsp.c" 1
	S8LDD xr3,$3,1,ptn3
 # 0 "" 2
 # 730 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 731 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 733 "rv30dsp.c" 1
	S8LDD xr4,$2,0,ptn0
 # 0 "" 2
 # 733 "rv30dsp.c" 1
	S8LDD xr4,$2,1,ptn1
 # 0 "" 2
 # 734 "rv30dsp.c" 1
	S8LDD xr4,$8,0,ptn2
 # 0 "" 2
 # 734 "rv30dsp.c" 1
	S8LDD xr4,$8,1,ptn3
 # 0 "" 2
 # 735 "rv30dsp.c" 1
	Q8MUL xr5,xr4,xr13,xr4
 # 0 "" 2
 # 736 "rv30dsp.c" 1
	Q16ACCM xr5,xr3,xr2,xr4,SS
 # 0 "" 2
 # 738 "rv30dsp.c" 1
	S8LDD xr6,$9,0,ptn0
 # 0 "" 2
 # 738 "rv30dsp.c" 1
	S8LDD xr6,$9,3,ptn1
 # 0 "" 2
 # 739 "rv30dsp.c" 1
	S8LDD xr6,$3,0,ptn2
 # 0 "" 2
 # 739 "rv30dsp.c" 1
	S8LDD xr6,$3,3,ptn3
 # 0 "" 2
 # 741 "rv30dsp.c" 1
	S8LDD xr2,$9,1,ptn0
 # 0 "" 2
 # 741 "rv30dsp.c" 1
	S8LDD xr2,$2,0,ptn1
 # 0 "" 2
 # 742 "rv30dsp.c" 1
	S8LDD xr2,$2,3,ptn2
 # 0 "" 2
 # 742 "rv30dsp.c" 1
	S8LDD xr2,$3,1,ptn3
 # 0 "" 2
 # 743 "rv30dsp.c" 1
	D8SUM xr6,xr0,xr6
 # 0 "" 2
 # 745 "rv30dsp.c" 1
	S8LDD xr3,$9,2,ptn0
 # 0 "" 2
 # 745 "rv30dsp.c" 1
	S8LDD xr3,$8,0,ptn1
 # 0 "" 2
 # 746 "rv30dsp.c" 1
	S8LDD xr3,$8,3,ptn2
 # 0 "" 2
 # 746 "rv30dsp.c" 1
	S8LDD xr3,$3,2,ptn3
 # 0 "" 2
 # 747 "rv30dsp.c" 1
	D8SUM xr2,xr3,xr2
 # 0 "" 2
 # 748 "rv30dsp.c" 1
	D16MUL xr3,xr2,xr14,xr2,WW
 # 0 "" 2
 # 750 "rv30dsp.c" 1
	S8LDD xr7,$2,1,ptn0
 # 0 "" 2
 # 750 "rv30dsp.c" 1
	S8LDD xr7,$2,2,ptn1
 # 0 "" 2
 # 751 "rv30dsp.c" 1
	S8LDD xr7,$8,1,ptn2
 # 0 "" 2
 # 751 "rv30dsp.c" 1
	S8LDD xr7,$8,2,ptn3
 # 0 "" 2
 # 752 "rv30dsp.c" 1
	Q8MUL xr8,xr7,xr13,xr7
 # 0 "" 2
 # 753 "rv30dsp.c" 1
	Q16ACCM xr8,xr3,xr2,xr7,SS
 # 0 "" 2
 # 755 "rv30dsp.c" 1
	D32SLL xr3,xr0,xr0,xr2,0
 # 0 "" 2
 # 756 "rv30dsp.c" 1
	D16ASUM xr1,xr4,xr7,xr6,AA
 # 0 "" 2
 # 757 "rv30dsp.c" 1
	D16ASUM xr2,xr5,xr8,xr3,AA
 # 0 "" 2
 # 758 "rv30dsp.c" 1
	D32ASUM xr2,xr1,xr6,xr3,AA
 # 0 "" 2
 # 760 "rv30dsp.c" 1
	D32ASUM xr2,xr15,xr15,xr3,AA
 # 0 "" 2
 # 761 "rv30dsp.c" 1
	D32SAR xr2,xr2,xr3,xr3,8
 # 0 "" 2
 # 762 "rv30dsp.c" 1
	Q16SAT xr2,xr3,xr2
 # 0 "" 2
 # 763 "rv30dsp.c" 1
	S8LDD xr4,$10,0,ptn0
 # 0 "" 2
 # 764 "rv30dsp.c" 1
	S8LDD xr4,$10,1,ptn2
 # 0 "" 2
 # 765 "rv30dsp.c" 1
	Q8AVGR xr2,xr2,xr4
 # 0 "" 2
 # 767 "rv30dsp.c" 1
	S8STD xr2,$10,0,ptn0
 # 0 "" 2
 # 768 "rv30dsp.c" 1
	S8STD xr2,$10,1,ptn2
 # 0 "" 2
#NO_APP
	addiu	$2,$2,2
	.set	noreorder
	.set	nomacro
	bne	$11,$2,$L38
	addiu	$10,$10,2
	.set	macro
	.set	reorder

	addiu	$13,$13,-1
	addu	$5,$5,$7
	.set	noreorder
	.set	nomacro
	bne	$13,$0,$L37
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	j	$31
	.end	avg_rv30_tpel8_hv_lowpass
	.size	avg_rv30_tpel8_hv_lowpass, .-avg_rv30_tpel8_hv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_hhv_lowpass
	.type	avg_rv30_tpel8_hhv_lowpass, @function
avg_rv30_tpel8_hhv_lowpass:
	.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
	.mask	0x40ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-64
	addiu	$28,$28,%lo(__gnu_local_gp)
	nor	$3,$0,$7
	sw	$22,52($sp)
	sll	$2,$7,1
	sw	$fp,60($sp)
	sw	$3,8($sp)
	addiu	$3,$7,-1
	sw	$23,56($sp)
	addiu	$2,$2,-1
	sw	$21,48($sp)
	addiu	$23,$5,1
	sw	$20,44($sp)
	li	$fp,8			# 0x8
	sw	$19,40($sp)
	sw	$18,36($sp)
	sw	$17,32($sp)
	sw	$16,28($sp)
	.cprestore	0
	sw	$7,76($sp)
	sw	$6,72($sp)
	sw	$3,16($sp)
	sw	$2,12($sp)
	lw	$22,%got(ff_cropTbl)($28)
	addiu	$22,$22,1024
$L42:
	lw	$2,8($sp)
	addiu	$25,$23,8
	lw	$3,16($sp)
	move	$24,$4
	move	$18,$23
	addu	$21,$2,$23
	lw	$2,12($sp)
	addu	$20,$3,$23
	addu	$19,$2,$23
$L43:
	lbu	$7,1($21)
	addiu	$18,$18,1
	lbu	$6,0($21)
	addiu	$20,$20,1
	lbu	$5,-1($21)
	addiu	$24,$24,1
	sll	$3,$7,2
	lbu	$2,-3($18)
	sll	$7,$7,4
	lbu	$12,-1($18)
	sll	$15,$6,1
	lbu	$11,2($21)
	subu	$7,$3,$7
	lbu	$10,-2($18)
	sll	$6,$6,3
	lbu	$9,0($18)
	addu	$3,$5,$7
	lbu	$8,-2($20)
	subu	$15,$15,$6
	lbu	$7,0($20)
	sll	$14,$2,2
	lbu	$6,-1($20)
	addu	$13,$3,$15
	lbu	$5,1($20)
	sll	$2,$2,4
	lbu	$3,1($19)
	addu	$15,$13,$11
	lbu	$17,0($19)
	sll	$13,$12,4
	lbu	$16,2($19)
	subu	$14,$14,$2
	lbu	$2,-1($19)
	sll	$11,$12,7
	lbu	$12,-1($24)
	addu	$14,$15,$14
	addu	$11,$13,$11
	sll	$15,$10,6
	sll	$13,$10,3
	addu	$11,$14,$11
	addu	$10,$13,$15
	sll	$14,$9,2
	sll	$13,$9,4
	addu	$10,$11,$10
	subu	$9,$14,$13
	sll	$11,$8,3
	sll	$14,$8,1
	addu	$9,$10,$9
	subu	$8,$14,$11
	sll	$10,$7,6
	sll	$13,$7,3
	addu	$8,$9,$8
	addu	$7,$13,$10
	sll	$11,$6,2
	sll	$9,$6,5
	addu	$7,$8,$7
	addu	$6,$11,$9
	sll	$10,$5,1
	sll	$8,$5,3
	addu	$6,$7,$6
	subu	$5,$10,$8
	sll	$7,$3,2
	addu	$5,$6,$5
	sll	$3,$3,4
	addu	$5,$5,$2
	subu	$3,$7,$3
	sll	$2,$17,1
	sll	$17,$17,3
	addu	$3,$5,$3
	subu	$2,$2,$17
	addiu	$21,$21,1
	addu	$2,$3,$2
	addiu	$19,$19,1
	addu	$2,$2,$16
	addiu	$2,$2,128
	sra	$2,$2,8
	addu	$2,$22,$2
	lbu	$2,0($2)
	addu	$12,$12,$2
	addiu	$12,$12,1
	sra	$12,$12,1
	bne	$25,$18,$L43
	sb	$12,-1($24)

	lw	$2,72($sp)
	addiu	$fp,$fp,-1
	lw	$3,76($sp)
	addu	$4,$4,$2
	bne	$fp,$0,$L42
	addu	$23,$23,$3

	lw	$fp,60($sp)
	lw	$23,56($sp)
	lw	$22,52($sp)
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	j	$31
	addiu	$sp,$sp,64

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_hhv_lowpass
	.size	avg_rv30_tpel8_hhv_lowpass, .-avg_rv30_tpel8_hhv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_hvv_lowpass
	.type	avg_rv30_tpel8_hvv_lowpass, @function
avg_rv30_tpel8_hvv_lowpass:
	.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
	.mask	0x40ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-64
	addiu	$28,$28,%lo(__gnu_local_gp)
	nor	$3,$0,$7
	sw	$22,52($sp)
	sll	$2,$7,1
	sw	$fp,60($sp)
	sw	$3,8($sp)
	addiu	$3,$7,-1
	sw	$23,56($sp)
	addiu	$2,$2,-1
	sw	$21,48($sp)
	addiu	$23,$5,1
	sw	$20,44($sp)
	li	$fp,8			# 0x8
	sw	$19,40($sp)
	sw	$18,36($sp)
	sw	$17,32($sp)
	sw	$16,28($sp)
	.cprestore	0
	sw	$7,76($sp)
	sw	$6,72($sp)
	sw	$3,16($sp)
	sw	$2,12($sp)
	lw	$22,%got(ff_cropTbl)($28)
	addiu	$22,$22,1024
$L48:
	lw	$2,8($sp)
	addiu	$25,$23,8
	lw	$3,16($sp)
	move	$24,$4
	move	$18,$23
	addu	$21,$2,$23
	lw	$2,12($sp)
	addu	$20,$3,$23
	addu	$19,$2,$23
$L49:
	lbu	$7,0($21)
	addiu	$18,$18,1
	lbu	$6,1($21)
	addiu	$20,$20,1
	lbu	$5,-1($21)
	addiu	$24,$24,1
	sll	$3,$7,2
	lbu	$2,-3($18)
	sll	$7,$7,4
	lbu	$12,-2($18)
	sll	$15,$6,1
	lbu	$11,2($21)
	subu	$7,$3,$7
	lbu	$10,-1($18)
	sll	$6,$6,3
	lbu	$9,0($18)
	addu	$3,$5,$7
	lbu	$8,-2($20)
	subu	$15,$15,$6
	lbu	$7,-1($20)
	sll	$14,$2,1
	lbu	$6,0($20)
	addu	$13,$3,$15
	lbu	$5,1($20)
	sll	$2,$2,3
	lbu	$3,0($19)
	addu	$15,$13,$11
	lbu	$17,1($19)
	sll	$13,$12,3
	lbu	$16,2($19)
	subu	$14,$14,$2
	lbu	$2,-1($19)
	sll	$11,$12,6
	lbu	$12,-1($24)
	addu	$14,$15,$14
	addu	$11,$13,$11
	sll	$15,$10,5
	sll	$13,$10,2
	addu	$11,$14,$11
	addu	$10,$13,$15
	sll	$14,$9,1
	sll	$13,$9,3
	addu	$10,$11,$10
	subu	$9,$14,$13
	sll	$11,$8,4
	sll	$14,$8,2
	addu	$9,$10,$9
	subu	$8,$14,$11
	sll	$10,$7,7
	sll	$13,$7,4
	addu	$8,$9,$8
	addu	$7,$13,$10
	sll	$11,$6,3
	sll	$9,$6,6
	addu	$7,$8,$7
	addu	$6,$11,$9
	sll	$10,$5,2
	sll	$8,$5,4
	addu	$6,$7,$6
	subu	$5,$10,$8
	sll	$7,$3,2
	addu	$5,$6,$5
	sll	$3,$3,4
	addu	$5,$5,$2
	subu	$3,$7,$3
	sll	$2,$17,1
	sll	$17,$17,3
	addu	$3,$5,$3
	subu	$2,$2,$17
	addiu	$21,$21,1
	addu	$2,$3,$2
	addiu	$19,$19,1
	addu	$2,$2,$16
	addiu	$2,$2,128
	sra	$2,$2,8
	addu	$2,$22,$2
	lbu	$2,0($2)
	addu	$12,$12,$2
	addiu	$12,$12,1
	sra	$12,$12,1
	bne	$25,$18,$L49
	sb	$12,-1($24)

	lw	$2,72($sp)
	addiu	$fp,$fp,-1
	lw	$3,76($sp)
	addu	$4,$4,$2
	bne	$fp,$0,$L48
	addu	$23,$23,$3

	lw	$fp,60($sp)
	lw	$23,56($sp)
	lw	$22,52($sp)
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	j	$31
	addiu	$sp,$sp,64

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_hvv_lowpass
	.size	avg_rv30_tpel8_hvv_lowpass, .-avg_rv30_tpel8_hvv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_hhvv_lowpass
	.type	avg_rv30_tpel8_hhvv_lowpass, @function
avg_rv30_tpel8_hhvv_lowpass:
	.frame	$sp,24,$31		# vars= 16, regs= 0/0, args= 0, gp= 8
	.mask	0x00000000,0
	.fmask	0x00000000,0
	lui	$3,%hi($LC0)
	addiu	$sp,$sp,-24
	addiu	$2,$3,%lo($LC0)
	lwl	$10,3($2)
	lwl	$9,7($2)
	lwl	$8,11($2)
	lwr	$10,%lo($LC0)($3)
	move	$3,$9
	lwr	$8,8($2)
	lwr	$3,4($2)
	addiu	$2,$sp,8
	sw	$10,8($sp)
	sw	$8,16($sp)
	sw	$3,12($sp)
#APP
 # 968 "rv30dsp.c" 1
	S32LDD xr13,$2,0
 # 0 "" 2
 # 969 "rv30dsp.c" 1
	S32LDD xr14,$2,4
 # 0 "" 2
 # 970 "rv30dsp.c" 1
	S32LDD xr15,$2,8
 # 0 "" 2
#NO_APP
	li	$12,8			# 0x8
	sll	$11,$7,1
	li	$10,8			# 0x8
$L54:
	move	$8,$0
$L55:
	addu	$9,$5,$8
#APP
 # 974 "rv30dsp.c" 1
	S8LDD xr1,$9,0,ptn0
 # 0 "" 2
 # 974 "rv30dsp.c" 1
	S8LDD xr1,$9,1,ptn1
 # 0 "" 2
 # 975 "rv30dsp.c" 1
	S8LDD xr1,$9,2,ptn2
 # 0 "" 2
#NO_APP
	addu	$3,$7,$8
	addu	$3,$5,$3
#APP
 # 975 "rv30dsp.c" 1
	S8LDD xr1,$3,0,ptn3
 # 0 "" 2
 # 976 "rv30dsp.c" 1
	Q8MUL xr2,xr1,xr13,xr1
 # 0 "" 2
 # 978 "rv30dsp.c" 1
	S8LDD xr3,$3,1,ptn0
 # 0 "" 2
 # 978 "rv30dsp.c" 1
	S8LDD xr3,$3,2,ptn1
 # 0 "" 2
#NO_APP
	addu	$2,$11,$8
	addu	$2,$5,$2
#APP
 # 979 "rv30dsp.c" 1
	S8LDD xr3,$2,0,ptn2
 # 0 "" 2
 # 979 "rv30dsp.c" 1
	S8LDD xr3,$2,1,ptn3
 # 0 "" 2
 # 980 "rv30dsp.c" 1
	Q8MUL xr4,xr3,xr14,xr3
 # 0 "" 2
 # 982 "rv30dsp.c" 1
	S8LDD xr7,$9,1,ptn0
 # 0 "" 2
 # 982 "rv30dsp.c" 1
	S8LDD xr7,$9,2,ptn1
 # 0 "" 2
 # 983 "rv30dsp.c" 1
	S8LDD xr7,$9,3,ptn2
 # 0 "" 2
 # 983 "rv30dsp.c" 1
	S8LDD xr7,$3,1,ptn3
 # 0 "" 2
 # 984 "rv30dsp.c" 1
	Q8MUL xr8,xr7,xr13,xr7
 # 0 "" 2
 # 986 "rv30dsp.c" 1
	S8LDD xr9,$3,2,ptn0
 # 0 "" 2
 # 986 "rv30dsp.c" 1
	S8LDD xr9,$3,3,ptn1
 # 0 "" 2
 # 987 "rv30dsp.c" 1
	S8LDD xr9,$2,1,ptn2
 # 0 "" 2
 # 987 "rv30dsp.c" 1
	S8LDD xr9,$2,2,ptn3
 # 0 "" 2
 # 988 "rv30dsp.c" 1
	Q8MUL xr10,xr9,xr14,xr9
 # 0 "" 2
 # 990 "rv30dsp.c" 1
	D32ADD xr5,xr0,xr0,xr6,AA
 # 0 "" 2
 # 991 "rv30dsp.c" 1
	S8LDD xr5,$2,2,ptn0
 # 0 "" 2
 # 992 "rv30dsp.c" 1
	S8LDD xr6,$2,3,ptn0
 # 0 "" 2
 # 993 "rv30dsp.c" 1
	Q16ACCM xr1,xr2,xr4,xr3,AA
 # 0 "" 2
 # 994 "rv30dsp.c" 1
	Q16ACCM xr7,xr8,xr10,xr9,AA
 # 0 "" 2
 # 995 "rv30dsp.c" 1
	Q16ACCM xr1,xr3,xr9,xr7,AA
 # 0 "" 2
 # 996 "rv30dsp.c" 1
	D16ASUM xr5,xr1,xr7,xr6,AA
 # 0 "" 2
 # 997 "rv30dsp.c" 1
	D32ASUM xr5,xr15,xr15,xr6,AA
 # 0 "" 2
 # 998 "rv30dsp.c" 1
	D32SLR xr5,xr5,xr6,xr6,8
 # 0 "" 2
 # 1000 "rv30dsp.c" 1
	Q16SAT xr5,xr6,xr5
 # 0 "" 2
#NO_APP
	addu	$2,$4,$8
#APP
 # 1001 "rv30dsp.c" 1
	S8LDD xr4,$2,0,ptn0
 # 0 "" 2
 # 1002 "rv30dsp.c" 1
	S8LDD xr4,$2,1,ptn2
 # 0 "" 2
 # 1003 "rv30dsp.c" 1
	Q8AVGR xr5,xr5,xr4
 # 0 "" 2
 # 1004 "rv30dsp.c" 1
	S8STD xr5,$2,0,ptn0
 # 0 "" 2
 # 1005 "rv30dsp.c" 1
	S8STD xr5,$2,1,ptn2
 # 0 "" 2
#NO_APP
	addiu	$8,$8,2
	bne	$8,$10,$L55
	addiu	$12,$12,-1
	addu	$5,$5,$7
	.set	noreorder
	.set	nomacro
	bne	$12,$0,$L54
	addu	$4,$4,$6
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,24
	.set	macro
	.set	reorder

	.end	avg_rv30_tpel8_hhvv_lowpass
	.size	avg_rv30_tpel8_hhvv_lowpass, .-avg_rv30_tpel8_hhvv_lowpass
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc10_c
	.type	put_rv30_tpel8_mc10_c, @function
put_rv30_tpel8_mc10_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,12			# 0xc
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,6			# 0x6
	sw	$31,36($sp)
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc10_c
	.size	put_rv30_tpel8_mc10_c, .-put_rv30_tpel8_mc10_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc20_c
	.type	put_rv30_tpel8_mc20_c, @function
put_rv30_tpel8_mc20_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,6			# 0x6
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,12			# 0xc
	sw	$31,36($sp)
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc20_c
	.size	put_rv30_tpel8_mc20_c, .-put_rv30_tpel8_mc20_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc01_c
	.type	put_rv30_tpel8_mc01_c, @function
put_rv30_tpel8_mc01_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,12			# 0xc
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,6			# 0x6
	sw	$31,36($sp)
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc01_c
	.size	put_rv30_tpel8_mc01_c, .-put_rv30_tpel8_mc01_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc02_c
	.type	put_rv30_tpel8_mc02_c, @function
put_rv30_tpel8_mc02_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,6			# 0x6
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,12			# 0xc
	sw	$31,36($sp)
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc02_c
	.size	put_rv30_tpel8_mc02_c, .-put_rv30_tpel8_mc02_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc11_c
	.type	put_rv30_tpel8_mc11_c, @function
put_rv30_tpel8_mc11_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	put_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc11_c
	.size	put_rv30_tpel8_mc11_c, .-put_rv30_tpel8_mc11_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc12_c
	.type	put_rv30_tpel8_mc12_c, @function
put_rv30_tpel8_mc12_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	put_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc12_c
	.size	put_rv30_tpel8_mc12_c, .-put_rv30_tpel8_mc12_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc21_c
	.type	put_rv30_tpel8_mc21_c, @function
put_rv30_tpel8_mc21_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	put_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc21_c
	.size	put_rv30_tpel8_mc21_c, .-put_rv30_tpel8_mc21_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel8_mc22_c
	.type	put_rv30_tpel8_mc22_c, @function
put_rv30_tpel8_mc22_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	put_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	put_rv30_tpel8_mc22_c
	.size	put_rv30_tpel8_mc22_c, .-put_rv30_tpel8_mc22_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc10_c
	.type	put_rv30_tpel16_mc10_c, @function
put_rv30_tpel16_mc10_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,12			# 0xc
	sw	$19,44($sp)
	li	$19,6			# 0x6
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc10_c
	.size	put_rv30_tpel16_mc10_c, .-put_rv30_tpel16_mc10_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc02_c
	.type	put_rv30_tpel16_mc02_c, @function
put_rv30_tpel16_mc02_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,6			# 0x6
	sw	$19,44($sp)
	li	$19,12			# 0xc
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc02_c
	.size	put_rv30_tpel16_mc02_c, .-put_rv30_tpel16_mc02_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc11_c
	.type	put_rv30_tpel16_mc11_c, @function
put_rv30_tpel16_mc11_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	put_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	put_rv30_tpel8_hv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc11_c
	.size	put_rv30_tpel16_mc11_c, .-put_rv30_tpel16_mc11_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc12_c
	.type	put_rv30_tpel16_mc12_c, @function
put_rv30_tpel16_mc12_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	put_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	put_rv30_tpel8_hvv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc12_c
	.size	put_rv30_tpel16_mc12_c, .-put_rv30_tpel16_mc12_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc21_c
	.type	put_rv30_tpel16_mc21_c, @function
put_rv30_tpel16_mc21_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	put_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	put_rv30_tpel8_hhv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc21_c
	.size	put_rv30_tpel16_mc21_c, .-put_rv30_tpel16_mc21_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc22_c
	.type	put_rv30_tpel16_mc22_c, @function
put_rv30_tpel16_mc22_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	put_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	put_rv30_tpel8_hhvv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc22_c
	.size	put_rv30_tpel16_mc22_c, .-put_rv30_tpel16_mc22_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc10_c
	.type	avg_rv30_tpel8_mc10_c, @function
avg_rv30_tpel8_mc10_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,12			# 0xc
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,6			# 0x6
	sw	$31,36($sp)
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc10_c
	.size	avg_rv30_tpel8_mc10_c, .-avg_rv30_tpel8_mc10_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc20_c
	.type	avg_rv30_tpel8_mc20_c, @function
avg_rv30_tpel8_mc20_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,6			# 0x6
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,12			# 0xc
	sw	$31,36($sp)
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc20_c
	.size	avg_rv30_tpel8_mc20_c, .-avg_rv30_tpel8_mc20_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc01_c
	.type	avg_rv30_tpel8_mc01_c, @function
avg_rv30_tpel8_mc01_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,12			# 0xc
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,6			# 0x6
	sw	$31,36($sp)
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc01_c
	.size	avg_rv30_tpel8_mc01_c, .-avg_rv30_tpel8_mc01_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc02_c
	.type	avg_rv30_tpel8_mc02_c, @function
avg_rv30_tpel8_mc02_c:
	.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,6			# 0x6
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$2,16($sp)
	li	$2,12			# 0xc
	sw	$31,36($sp)
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	sw	$2,20($sp)

	lw	$31,36($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc02_c
	.size	avg_rv30_tpel8_mc02_c, .-avg_rv30_tpel8_mc02_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc11_c
	.type	avg_rv30_tpel8_mc11_c, @function
avg_rv30_tpel8_mc11_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	avg_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc11_c
	.size	avg_rv30_tpel8_mc11_c, .-avg_rv30_tpel8_mc11_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc12_c
	.type	avg_rv30_tpel8_mc12_c, @function
avg_rv30_tpel8_mc12_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	avg_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc12_c
	.size	avg_rv30_tpel8_mc12_c, .-avg_rv30_tpel8_mc12_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc21_c
	.type	avg_rv30_tpel8_mc21_c, @function
avg_rv30_tpel8_mc21_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	avg_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc21_c
	.size	avg_rv30_tpel8_mc21_c, .-avg_rv30_tpel8_mc21_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel8_mc22_c
	.type	avg_rv30_tpel8_mc22_c, @function
avg_rv30_tpel8_mc22_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	.option	pic0
	j	avg_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$7,$6

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel8_mc22_c
	.size	avg_rv30_tpel8_mc22_c, .-avg_rv30_tpel8_mc22_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc10_c
	.type	avg_rv30_tpel16_mc10_c, @function
avg_rv30_tpel16_mc10_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,12			# 0xc
	sw	$19,44($sp)
	li	$19,6			# 0x6
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc10_c
	.size	avg_rv30_tpel16_mc10_c, .-avg_rv30_tpel16_mc10_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc02_c
	.type	avg_rv30_tpel16_mc02_c, @function
avg_rv30_tpel16_mc02_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,6			# 0x6
	sw	$19,44($sp)
	li	$19,12			# 0xc
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc02_c
	.size	avg_rv30_tpel16_mc02_c, .-avg_rv30_tpel16_mc02_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc11_c
	.type	avg_rv30_tpel16_mc11_c, @function
avg_rv30_tpel16_mc11_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	avg_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_hv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	avg_rv30_tpel8_hv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc11_c
	.size	avg_rv30_tpel16_mc11_c, .-avg_rv30_tpel16_mc11_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc12_c
	.type	avg_rv30_tpel16_mc12_c, @function
avg_rv30_tpel16_mc12_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	avg_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_hvv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	avg_rv30_tpel8_hvv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc12_c
	.size	avg_rv30_tpel16_mc12_c, .-avg_rv30_tpel16_mc12_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc21_c
	.type	avg_rv30_tpel16_mc21_c, @function
avg_rv30_tpel16_mc21_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	avg_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_hhv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	avg_rv30_tpel8_hhv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc21_c
	.size	avg_rv30_tpel16_mc21_c, .-avg_rv30_tpel16_mc21_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc22_c
	.type	avg_rv30_tpel16_mc22_c, @function
avg_rv30_tpel16_mc22_c:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	move	$7,$6
	sw	$18,32($sp)
	move	$18,$4
	sw	$17,28($sp)
	move	$17,$5
	sw	$31,36($sp)
	sw	$16,24($sp)
	.option	pic0
	jal	avg_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	addu	$17,$17,$2
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_hhvv_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	lw	$31,36($sp)
	addiu	$5,$17,8
	lw	$18,32($sp)
	move	$6,$16
	lw	$17,28($sp)
	move	$7,$16
	lw	$16,24($sp)
	.option	pic0
	j	avg_rv30_tpel8_hhvv_lowpass
	.option	pic2
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc22_c
	.size	avg_rv30_tpel16_mc22_c, .-avg_rv30_tpel16_mc22_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc20_c
	.type	avg_rv30_tpel16_mc20_c, @function
avg_rv30_tpel16_mc20_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,6			# 0x6
	sw	$19,44($sp)
	li	$19,12			# 0xc
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc20_c
	.size	avg_rv30_tpel16_mc20_c, .-avg_rv30_tpel16_mc20_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	avg_rv30_tpel16_mc01_c
	.type	avg_rv30_tpel16_mc01_c, @function
avg_rv30_tpel16_mc01_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,12			# 0xc
	sw	$19,44($sp)
	li	$19,6			# 0x6
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	avg_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	avg_rv30_tpel16_mc01_c
	.size	avg_rv30_tpel16_mc01_c, .-avg_rv30_tpel16_mc01_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc20_c
	.type	put_rv30_tpel16_mc20_c, @function
put_rv30_tpel16_mc20_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,6			# 0x6
	sw	$19,44($sp)
	li	$19,12			# 0xc
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_h_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc20_c
	.size	put_rv30_tpel16_mc20_c, .-put_rv30_tpel16_mc20_c
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	put_rv30_tpel16_mc01_c
	.type	put_rv30_tpel16_mc01_c, @function
put_rv30_tpel16_mc01_c:
	.frame	$sp,56,$31		# vars= 0, regs= 6/0, args= 24, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	move	$7,$6
	sw	$20,48($sp)
	li	$20,12			# 0xc
	sw	$19,44($sp)
	li	$19,6			# 0x6
	sw	$18,40($sp)
	move	$18,$4
	sw	$17,36($sp)
	move	$17,$5
	sw	$20,16($sp)
	sw	$19,20($sp)
	sw	$31,52($sp)
	sw	$16,32($sp)
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$16,$6

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	move	$6,$16
	sw	$19,20($sp)
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	sll	$2,$16,3
	move	$6,$16
	sw	$20,16($sp)
	addu	$17,$17,$2
	sw	$19,20($sp)
	addu	$18,$18,$2
	move	$5,$17
	move	$4,$18
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	addiu	$4,$18,8
	addiu	$5,$17,8
	sw	$20,16($sp)
	sw	$19,20($sp)
	move	$6,$16
	.option	pic0
	jal	put_rv30_tpel8_v_lowpass
	.option	pic2
	move	$7,$16

	lw	$31,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	put_rv30_tpel16_mc01_c
	.size	put_rv30_tpel16_mc01_c, .-put_rv30_tpel16_mc01_c
	.section	.text.unlikely,"ax",@progbits
	.align	2
	.globl	ff_rv30dsp_init
	.set	nomips16
	.set	nomicromips
	.ent	ff_rv30dsp_init
	.type	ff_rv30dsp_init, @function
ff_rv30dsp_init:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lw	$2,1808($4)
	lw	$6,1488($4)
	lw	$5,1744($4)
	lw	$3,1552($4)
	sw	$2,3380($4)
	lui	$2,%hi(put_rv30_tpel16_mc10_c)
	sw	$6,3060($4)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc10_c)
	sw	$5,3316($4)
	sw	$3,3124($4)
	sw	$2,3064($4)
	lui	$2,%hi(put_rv30_tpel16_mc20_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc20_c)
	sw	$2,3068($4)
	lui	$2,%hi(put_rv30_tpel16_mc01_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc01_c)
	sw	$2,3076($4)
	lui	$2,%hi(put_rv30_tpel16_mc11_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc11_c)
	sw	$2,3080($4)
	lui	$2,%hi(put_rv30_tpel16_mc21_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc21_c)
	sw	$2,3084($4)
	lui	$2,%hi(put_rv30_tpel16_mc02_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc02_c)
	sw	$2,3092($4)
	lui	$2,%hi(put_rv30_tpel16_mc12_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc12_c)
	sw	$2,3096($4)
	lui	$2,%hi(put_rv30_tpel16_mc22_c)
	addiu	$2,$2,%lo(put_rv30_tpel16_mc22_c)
	sw	$2,3100($4)
	lui	$2,%hi(avg_rv30_tpel16_mc10_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc10_c)
	sw	$2,3320($4)
	lui	$2,%hi(avg_rv30_tpel16_mc20_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc20_c)
	sw	$2,3324($4)
	lui	$2,%hi(avg_rv30_tpel16_mc01_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc01_c)
	sw	$2,3332($4)
	lui	$2,%hi(avg_rv30_tpel16_mc11_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc11_c)
	sw	$2,3336($4)
	lui	$2,%hi(avg_rv30_tpel16_mc21_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc21_c)
	sw	$2,3340($4)
	lui	$2,%hi(avg_rv30_tpel16_mc02_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc02_c)
	sw	$2,3348($4)
	lui	$2,%hi(avg_rv30_tpel16_mc12_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc12_c)
	sw	$2,3352($4)
	lui	$2,%hi(avg_rv30_tpel16_mc22_c)
	addiu	$2,$2,%lo(avg_rv30_tpel16_mc22_c)
	sw	$2,3356($4)
	lui	$2,%hi(put_rv30_tpel8_mc10_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc10_c)
	sw	$2,3128($4)
	lui	$2,%hi(put_rv30_tpel8_mc20_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc20_c)
	sw	$2,3132($4)
	lui	$2,%hi(put_rv30_tpel8_mc01_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc01_c)
	sw	$2,3140($4)
	lui	$2,%hi(put_rv30_tpel8_mc11_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc11_c)
	sw	$2,3144($4)
	lui	$2,%hi(put_rv30_tpel8_mc21_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc21_c)
	sw	$2,3148($4)
	lui	$2,%hi(put_rv30_tpel8_mc02_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc02_c)
	sw	$2,3156($4)
	lui	$2,%hi(put_rv30_tpel8_mc12_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc12_c)
	sw	$2,3160($4)
	lui	$2,%hi(put_rv30_tpel8_mc22_c)
	addiu	$2,$2,%lo(put_rv30_tpel8_mc22_c)
	sw	$2,3164($4)
	lui	$2,%hi(avg_rv30_tpel8_mc10_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc10_c)
	sw	$2,3384($4)
	lui	$2,%hi(avg_rv30_tpel8_mc20_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc20_c)
	sw	$2,3388($4)
	lui	$2,%hi(avg_rv30_tpel8_mc01_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc01_c)
	sw	$2,3396($4)
	lui	$2,%hi(avg_rv30_tpel8_mc11_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc11_c)
	sw	$2,3400($4)
	lui	$2,%hi(avg_rv30_tpel8_mc21_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc21_c)
	sw	$2,3404($4)
	lui	$2,%hi(avg_rv30_tpel8_mc02_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc02_c)
	sw	$2,3412($4)
	lui	$2,%hi(avg_rv30_tpel8_mc12_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc12_c)
	sw	$2,3416($4)
	lui	$2,%hi(avg_rv30_tpel8_mc22_c)
	addiu	$2,$2,%lo(avg_rv30_tpel8_mc22_c)
	j	$31
	sw	$2,3420($4)

	.set	macro
	.set	reorder
	.end	ff_rv30dsp_init
	.size	ff_rv30dsp_init, .-ff_rv30dsp_init
	.globl	mc_coef
	.rdata
	.align	2
	.type	mc_coef, @object
	.size	mc_coef, 12
mc_coef:
	.byte	-112
	.byte	72
	.byte	72
	.byte	36
	.byte	12
	.byte	0
	.byte	6
	.byte	0
	.byte	-128
	.byte	0
	.byte	0
	.byte	0
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
