.file	1 "simple_idct.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.globl	ff_simple_idct_put
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct_put
.type	ff_simple_idct_put, @function
ff_simple_idct_put:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,1518469120			# 0x5a820000
addiu	$2,$2,23170
#APP
# 382 "simple_idct.c" 1
.word	0b01110000000000100000000101101111	#S32I2M XR5,$2
# 0 "" 2
#NO_APP
li	$2,1984036864			# 0x76420000
addiu	$2,$2,12540
#APP
# 383 "simple_idct.c" 1
.word	0b01110000000000100000000110101111	#S32I2M XR6,$2
# 0 "" 2
#NO_APP
li	$2,2106195968			# 0x7d8a0000
addiu	$2,$2,27246
#APP
# 384 "simple_idct.c" 1
.word	0b01110000000000100000000111101111	#S32I2M XR7,$2
# 0 "" 2
#NO_APP
li	$2,1193082880			# 0x471d0000
addiu	$2,$2,6393
#APP
# 385 "simple_idct.c" 1
.word	0b01110000000000100000001000101111	#S32I2M XR8,$2
# 0 "" 2
#NO_APP
addiu	$3,$6,16
#APP
# 389 "simple_idct.c" 1
.word	0b01110000110000000000000001010000	#S32LDD XR1,$6,0
# 0 "" 2
#NO_APP
move	$2,$6
$L2:
#APP
# 392 "simple_idct.c" 1
.word	0b01110000010000000010000010010000	#S32LDD XR2,$2,32
# 0 "" 2
# 393 "simple_idct.c" 1
.word	0b01110000010000000100000011010000	#S32LDD XR3,$2,64
# 0 "" 2
# 394 "simple_idct.c" 1
.word	0b01110000010000000110000100010000	#S32LDD XR4,$2,96
# 0 "" 2
# 397 "simple_idct.c" 1
.word	0b01110000101101000101011011001000	#D16MUL XR11,XR5,XR1,XR13,HW
# 0 "" 2
# 398 "simple_idct.c" 1
.word	0b01110000101101001101011011001010	#D16MAC XR11,XR5,XR3,XR13,AA,HW
# 0 "" 2
# 399 "simple_idct.c" 1
.word	0b01110000101110001001101100001000	#D16MUL XR12,XR6,XR2,XR14,HW
# 0 "" 2
# 400 "simple_idct.c" 1
.word	0b01110000011110010001101100001010	#D16MAC XR12,XR6,XR4,XR14,AA,LW
# 0 "" 2
# 401 "simple_idct.c" 1
.word	0b01110001001100110010111011011000	#D32ADD XR11,XR11,XR12,XR12,AS
# 0 "" 2
# 403 "simple_idct.c" 1
.word	0b01110001001110111011011101011000	#D32ADD XR13,XR13,XR14,XR14,AS
# 0 "" 2
# 405 "simple_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 406 "simple_idct.c" 1
.word	0b01110000001110000000001100001011	#D16MACF XR12,XR0,XR0,XR14,AA,WW
# 0 "" 2
# 408 "simple_idct.c" 1
.word	0b01110000101010000101011101001000	#D16MUL XR13,XR5,XR1,XR10,HW
# 0 "" 2
# 409 "simple_idct.c" 1
.word	0b01110011101010001101011101001010	#D16MAC XR13,XR5,XR3,XR10,SS,HW
# 0 "" 2
# 411 "simple_idct.c" 1
.word	0b01110000011001001001101110001000	#D16MUL XR14,XR6,XR2,XR9,LW
# 0 "" 2
# 412 "simple_idct.c" 1
.word	0b01110011101001010001101110001010	#D16MAC XR14,XR6,XR4,XR9,SS,HW
# 0 "" 2
# 413 "simple_idct.c" 1
.word	0b01110001001110111011011101011000	#D32ADD XR13,XR13,XR14,XR14,AS
# 0 "" 2
# 415 "simple_idct.c" 1
.word	0b01110001001001100110101010011000	#D32ADD XR10,XR10,XR9,XR9,AS
# 0 "" 2
# 417 "simple_idct.c" 1
.word	0b01110000001010000000001101001011	#D16MACF XR13,XR0,XR0,XR10,AA,WW
# 0 "" 2
# 418 "simple_idct.c" 1
.word	0b01110000001001000000001110001011	#D16MACF XR14,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 421 "simple_idct.c" 1
.word	0b01110000010000000001000001010000	#S32LDD XR1,$2,16
# 0 "" 2
# 422 "simple_idct.c" 1
.word	0b01110000010000000011000010010000	#S32LDD XR2,$2,48
# 0 "" 2
# 423 "simple_idct.c" 1
.word	0b01110000010000000101000011010000	#S32LDD XR3,$2,80
# 0 "" 2
# 424 "simple_idct.c" 1
.word	0b01110000010000000111000100010000	#S32LDD XR4,$2,112
# 0 "" 2
# 428 "simple_idct.c" 1
.word	0b01110000101010000101111001001000	#D16MUL XR9,XR7,XR1,XR10,HW
# 0 "" 2
# 429 "simple_idct.c" 1
.word	0b01110000011010001001111001001010	#D16MAC XR9,XR7,XR2,XR10,AA,LW
# 0 "" 2
# 430 "simple_idct.c" 1
.word	0b01110000101010001110001001001010	#D16MAC XR9,XR8,XR3,XR10,AA,HW
# 0 "" 2
# 432 "simple_idct.c" 1
.word	0b01110000011010010010001001001010	#D16MAC XR9,XR8,XR4,XR10,AA,LW
# 0 "" 2
# 434 "simple_idct.c" 1
.word	0b01110000001010000000001001001011	#D16MACF XR9,XR0,XR0,XR10,AA,WW
# 0 "" 2
# 436 "simple_idct.c" 1
.word	0b01110000011111000101111010001000	#D16MUL XR10,XR7,XR1,XR15,LW
# 0 "" 2
# 437 "simple_idct.c" 1
.word	0b01110011011111001010001010001010	#D16MAC XR10,XR8,XR2,XR15,SS,LW
# 0 "" 2
# 438 "simple_idct.c" 1
.word	0b01110011101111001101111010001010	#D16MAC XR10,XR7,XR3,XR15,SS,HW
# 0 "" 2
# 440 "simple_idct.c" 1
.word	0b01110011101111010010001010001010	#D16MAC XR10,XR8,XR4,XR15,SS,HW
# 0 "" 2
# 442 "simple_idct.c" 1
.word	0b01110000001111000000001010001011	#D16MACF XR10,XR0,XR0,XR15,AA,WW
# 0 "" 2
# 444 "simple_idct.c" 1
.word	0b01110001001001100110111011001110	#Q16ADD XR11,XR11,XR9,XR9,AS,WW
# 0 "" 2
# 445 "simple_idct.c" 1
.word	0b01110000010000000000001011010001	#S32STD XR11,$2,0
# 0 "" 2
# 446 "simple_idct.c" 1
.word	0b01110000010000000111001001010001	#S32STD XR9,$2,112
# 0 "" 2
# 447 "simple_idct.c" 1
.word	0b01110001001010101011011101001110	#Q16ADD XR13,XR13,XR10,XR10,AS,WW
# 0 "" 2
# 448 "simple_idct.c" 1
.word	0b01110000010000000001001101010001	#S32STD XR13,$2,16
# 0 "" 2
# 449 "simple_idct.c" 1
.word	0b01110000010000000110001010010001	#S32STD XR10,$2,96
# 0 "" 2
# 451 "simple_idct.c" 1
.word	0b01110000101010000110001001001000	#D16MUL XR9,XR8,XR1,XR10,HW
# 0 "" 2
# 452 "simple_idct.c" 1
.word	0b01110011101010001001111001001010	#D16MAC XR9,XR7,XR2,XR10,SS,HW
# 0 "" 2
# 453 "simple_idct.c" 1
.word	0b01110000011010001110001001001010	#D16MAC XR9,XR8,XR3,XR10,AA,LW
# 0 "" 2
# 455 "simple_idct.c" 1
.word	0b01110000011010010001111001001010	#D16MAC XR9,XR7,XR4,XR10,AA,LW
# 0 "" 2
# 457 "simple_idct.c" 1
.word	0b01110000001010000000001001001011	#D16MACF XR9,XR0,XR0,XR10,AA,WW
# 0 "" 2
# 459 "simple_idct.c" 1
.word	0b01110000011111000110001010001000	#D16MUL XR10,XR8,XR1,XR15,LW
# 0 "" 2
# 460 "simple_idct.c" 1
.word	0b01110011101111001010001010001010	#D16MAC XR10,XR8,XR2,XR15,SS,HW
# 0 "" 2
# 461 "simple_idct.c" 1
.word	0b01110000011111001101111010001010	#D16MAC XR10,XR7,XR3,XR15,AA,LW
# 0 "" 2
# 463 "simple_idct.c" 1
.word	0b01110011101111010001111010001010	#D16MAC XR10,XR7,XR4,XR15,SS,HW
# 0 "" 2
# 465 "simple_idct.c" 1
.word	0b01110000001111000000001010001011	#D16MACF XR10,XR0,XR0,XR15,AA,WW
# 0 "" 2
# 467 "simple_idct.c" 1
.word	0b01110001001001100111101110001110	#Q16ADD XR14,XR14,XR9,XR9,AS,WW
# 0 "" 2
# 468 "simple_idct.c" 1
.word	0b01110000010000000010001110010001	#S32STD XR14,$2,32
# 0 "" 2
# 469 "simple_idct.c" 1
.word	0b01110000010000000101001001010001	#S32STD XR9,$2,80
# 0 "" 2
# 470 "simple_idct.c" 1
.word	0b01110001001010101011001100001110	#Q16ADD XR12,XR12,XR10,XR10,AS,WW
# 0 "" 2
# 471 "simple_idct.c" 1
.word	0b01110000010000000000010001010100	#S32LDI XR1,$2,4
# 0 "" 2
# 472 "simple_idct.c" 1
.word	0b01110000010000000010111100010001	#S32STD XR12,$2,44
# 0 "" 2
# 473 "simple_idct.c" 1
.word	0b01110000010000000011111010010001	#S32STD XR10,$2,60
# 0 "" 2
#NO_APP
bne	$2,$3,$L2
addiu	$2,$6,128
#APP
# 479 "simple_idct.c" 1
.word	0b01110000110000000000000001010000	#S32LDD XR1,$6,0
# 0 "" 2
#NO_APP
$L3:
#APP
# 481 "simple_idct.c" 1
.word	0b01110000110000000000010010010000	#S32LDD XR2,$6,4
# 0 "" 2
# 482 "simple_idct.c" 1
.word	0b01110000110000000000100011010000	#S32LDD XR3,$6,8
# 0 "" 2
# 483 "simple_idct.c" 1
.word	0b01110000110000000000110100010000	#S32LDD XR4,$6,12
# 0 "" 2
# 486 "simple_idct.c" 1
.word	0b01110000101100011100011011001000	#D16MUL XR11,XR1,XR7,XR12,HW
# 0 "" 2
# 487 "simple_idct.c" 1
.word	0b01110000101110100000011101001000	#D16MUL XR13,XR1,XR8,XR14,HW
# 0 "" 2
# 488 "simple_idct.c" 1
.word	0b01110010101011011100101101001010	#D16MAC XR13,XR2,XR7,XR11,SA,HW
# 0 "" 2
# 489 "simple_idct.c" 1
.word	0b01110011101100100000101110001010	#D16MAC XR14,XR2,XR8,XR12,SS,HW
# 0 "" 2
# 490 "simple_idct.c" 1
.word	0b01110010101110011100111100001010	#D16MAC XR12,XR3,XR7,XR14,SA,HW
# 0 "" 2
# 492 "simple_idct.c" 1
.word	0b01110000101101100000111011001010	#D16MAC XR11,XR3,XR8,XR13,AA,HW
# 0 "" 2
# 495 "simple_idct.c" 1
.word	0b01110000011010010100011001001000	#D16MUL XR9,XR1,XR5,XR10,LW
# 0 "" 2
# 496 "simple_idct.c" 1
.word	0b01110001011010010100111001001010	#D16MAC XR9,XR3,XR5,XR10,AS,LW
# 0 "" 2
# 497 "simple_idct.c" 1
.word	0b01110000010011011000100001001000	#D16MUL XR1,XR2,XR6,XR3,LW
# 0 "" 2
# 498 "simple_idct.c" 1
.word	0b01110010010001011001000011001010	#D16MAC XR3,XR4,XR6,XR1,SA,LW
# 0 "" 2
# 500 "simple_idct.c" 1
.word	0b01110010101011100001001100001010	#D16MAC XR12,XR4,XR8,XR11,SA,HW
# 0 "" 2
# 502 "simple_idct.c" 1
.word	0b01110010101101011101001110001010	#D16MAC XR14,XR4,XR7,XR13,SA,HW
# 0 "" 2
# 505 "simple_idct.c" 1
.word	0b01110001000100000110010010011000	#D32ADD XR2,XR9,XR1,XR4,AS
# 0 "" 2
# 507 "simple_idct.c" 1
.word	0b01110001000001001110101001011000	#D32ADD XR9,XR10,XR3,XR1,AS
# 0 "" 2
# 510 "simple_idct.c" 1
.word	0b01110001001011101100100010011000	#D32ADD XR2,XR2,XR11,XR11,AS
# 0 "" 2
# 511 "simple_idct.c" 1
.word	0b01110001001110111001000100011000	#D32ADD XR4,XR4,XR14,XR14,AS
# 0 "" 2
# 512 "simple_idct.c" 1
.word	0b01110001001100110010011001011000	#D32ADD XR9,XR9,XR12,XR12,AS
# 0 "" 2
# 513 "simple_idct.c" 1
.word	0b01110001001101110100010001011000	#D32ADD XR1,XR1,XR13,XR13,AS
# 0 "" 2
# 515 "simple_idct.c" 1
.word	0b01110000000010000000001001001011	#D16MACF XR9,XR0,XR0,XR2,AA,WW
# 0 "" 2
# 516 "simple_idct.c" 1
.word	0b01110000000001000000000100001011	#D16MACF XR4,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 517 "simple_idct.c" 1
.word	0b01110000001110000000001101001011	#D16MACF XR13,XR0,XR0,XR14,AA,WW
# 0 "" 2
# 518 "simple_idct.c" 1
.word	0b01110000001100000000001011001011	#D16MACF XR11,XR0,XR0,XR12,AA,WW
# 0 "" 2
# 520 "simple_idct.c" 1
.word	0b01110000000110100101000100000111	#Q16SAT XR4,XR4,XR9
# 0 "" 2
# 521 "simple_idct.c" 1
.word	0b01110000000110110110111011000111	#Q16SAT XR11,XR11,XR13
# 0 "" 2
# 522 "simple_idct.c" 1
.word	0b01110000110000000001000001010100	#S32LDI XR1,$6,16
# 0 "" 2
# 523 "simple_idct.c" 1
.word	0b01110000100000000000000100010001	#S32STD XR4,$4,0
# 0 "" 2
# 524 "simple_idct.c" 1
.word	0b01110000100000000000011011010001	#S32STD XR11,$4,4
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$6,$2,$L3
addu	$4,$4,$5
.set	macro
.set	reorder

j	$31
.end	ff_simple_idct_put
.size	ff_simple_idct_put, .-ff_simple_idct_put
.align	2
.globl	ff_simple_idct_add
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct_add
.type	ff_simple_idct_add, @function
ff_simple_idct_add:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,1518469120			# 0x5a820000
addiu	$2,$2,23170
#APP
# 545 "simple_idct.c" 1
.word	0b01110000000000100000000101101111	#S32I2M XR5,$2
# 0 "" 2
#NO_APP
li	$2,1984036864			# 0x76420000
addiu	$2,$2,12540
#APP
# 546 "simple_idct.c" 1
.word	0b01110000000000100000000110101111	#S32I2M XR6,$2
# 0 "" 2
#NO_APP
li	$2,2106195968			# 0x7d8a0000
addiu	$2,$2,27246
#APP
# 547 "simple_idct.c" 1
.word	0b01110000000000100000000111101111	#S32I2M XR7,$2
# 0 "" 2
#NO_APP
li	$2,1193082880			# 0x471d0000
addiu	$2,$2,6393
#APP
# 548 "simple_idct.c" 1
.word	0b01110000000000100000001000101111	#S32I2M XR8,$2
# 0 "" 2
#NO_APP
addiu	$3,$6,16
#APP
# 552 "simple_idct.c" 1
.word	0b01110000110000000000000001010000	#S32LDD XR1,$6,0
# 0 "" 2
#NO_APP
move	$2,$6
$L8:
#APP
# 555 "simple_idct.c" 1
.word	0b01110000010000000010000010010000	#S32LDD XR2,$2,32
# 0 "" 2
# 556 "simple_idct.c" 1
.word	0b01110000010000000100000011010000	#S32LDD XR3,$2,64
# 0 "" 2
# 557 "simple_idct.c" 1
.word	0b01110000010000000110000100010000	#S32LDD XR4,$2,96
# 0 "" 2
# 560 "simple_idct.c" 1
.word	0b01110000101101000101011011001000	#D16MUL XR11,XR5,XR1,XR13,HW
# 0 "" 2
# 561 "simple_idct.c" 1
.word	0b01110000101101001101011011001010	#D16MAC XR11,XR5,XR3,XR13,AA,HW
# 0 "" 2
# 562 "simple_idct.c" 1
.word	0b01110000101110001001101100001000	#D16MUL XR12,XR6,XR2,XR14,HW
# 0 "" 2
# 563 "simple_idct.c" 1
.word	0b01110000011110010001101100001010	#D16MAC XR12,XR6,XR4,XR14,AA,LW
# 0 "" 2
# 564 "simple_idct.c" 1
.word	0b01110001001100110010111011011000	#D32ADD XR11,XR11,XR12,XR12,AS
# 0 "" 2
# 566 "simple_idct.c" 1
.word	0b01110001001110111011011101011000	#D32ADD XR13,XR13,XR14,XR14,AS
# 0 "" 2
# 568 "simple_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 569 "simple_idct.c" 1
.word	0b01110000001110000000001100001011	#D16MACF XR12,XR0,XR0,XR14,AA,WW
# 0 "" 2
# 571 "simple_idct.c" 1
.word	0b01110000101010000101011101001000	#D16MUL XR13,XR5,XR1,XR10,HW
# 0 "" 2
# 572 "simple_idct.c" 1
.word	0b01110011101010001101011101001010	#D16MAC XR13,XR5,XR3,XR10,SS,HW
# 0 "" 2
# 574 "simple_idct.c" 1
.word	0b01110000011001001001101110001000	#D16MUL XR14,XR6,XR2,XR9,LW
# 0 "" 2
# 575 "simple_idct.c" 1
.word	0b01110011101001010001101110001010	#D16MAC XR14,XR6,XR4,XR9,SS,HW
# 0 "" 2
# 576 "simple_idct.c" 1
.word	0b01110001001110111011011101011000	#D32ADD XR13,XR13,XR14,XR14,AS
# 0 "" 2
# 578 "simple_idct.c" 1
.word	0b01110001001001100110101010011000	#D32ADD XR10,XR10,XR9,XR9,AS
# 0 "" 2
# 580 "simple_idct.c" 1
.word	0b01110000001010000000001101001011	#D16MACF XR13,XR0,XR0,XR10,AA,WW
# 0 "" 2
# 581 "simple_idct.c" 1
.word	0b01110000001001000000001110001011	#D16MACF XR14,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 584 "simple_idct.c" 1
.word	0b01110000010000000001000001010000	#S32LDD XR1,$2,16
# 0 "" 2
# 585 "simple_idct.c" 1
.word	0b01110000010000000011000010010000	#S32LDD XR2,$2,48
# 0 "" 2
# 586 "simple_idct.c" 1
.word	0b01110000010000000101000011010000	#S32LDD XR3,$2,80
# 0 "" 2
# 587 "simple_idct.c" 1
.word	0b01110000010000000111000100010000	#S32LDD XR4,$2,112
# 0 "" 2
# 591 "simple_idct.c" 1
.word	0b01110000101010000101111001001000	#D16MUL XR9,XR7,XR1,XR10,HW
# 0 "" 2
# 592 "simple_idct.c" 1
.word	0b01110000011010001001111001001010	#D16MAC XR9,XR7,XR2,XR10,AA,LW
# 0 "" 2
# 593 "simple_idct.c" 1
.word	0b01110000101010001110001001001010	#D16MAC XR9,XR8,XR3,XR10,AA,HW
# 0 "" 2
# 595 "simple_idct.c" 1
.word	0b01110000011010010010001001001010	#D16MAC XR9,XR8,XR4,XR10,AA,LW
# 0 "" 2
# 597 "simple_idct.c" 1
.word	0b01110000001010000000001001001011	#D16MACF XR9,XR0,XR0,XR10,AA,WW
# 0 "" 2
# 599 "simple_idct.c" 1
.word	0b01110000011111000101111010001000	#D16MUL XR10,XR7,XR1,XR15,LW
# 0 "" 2
# 600 "simple_idct.c" 1
.word	0b01110011011111001010001010001010	#D16MAC XR10,XR8,XR2,XR15,SS,LW
# 0 "" 2
# 601 "simple_idct.c" 1
.word	0b01110011101111001101111010001010	#D16MAC XR10,XR7,XR3,XR15,SS,HW
# 0 "" 2
# 603 "simple_idct.c" 1
.word	0b01110011101111010010001010001010	#D16MAC XR10,XR8,XR4,XR15,SS,HW
# 0 "" 2
# 605 "simple_idct.c" 1
.word	0b01110000001111000000001010001011	#D16MACF XR10,XR0,XR0,XR15,AA,WW
# 0 "" 2
# 607 "simple_idct.c" 1
.word	0b01110001001001100110111011001110	#Q16ADD XR11,XR11,XR9,XR9,AS,WW
# 0 "" 2
# 608 "simple_idct.c" 1
.word	0b01110000010000000000001011010001	#S32STD XR11,$2,0
# 0 "" 2
# 609 "simple_idct.c" 1
.word	0b01110000010000000111001001010001	#S32STD XR9,$2,112
# 0 "" 2
# 610 "simple_idct.c" 1
.word	0b01110001001010101011011101001110	#Q16ADD XR13,XR13,XR10,XR10,AS,WW
# 0 "" 2
# 611 "simple_idct.c" 1
.word	0b01110000010000000001001101010001	#S32STD XR13,$2,16
# 0 "" 2
# 612 "simple_idct.c" 1
.word	0b01110000010000000110001010010001	#S32STD XR10,$2,96
# 0 "" 2
# 614 "simple_idct.c" 1
.word	0b01110000101010000110001001001000	#D16MUL XR9,XR8,XR1,XR10,HW
# 0 "" 2
# 615 "simple_idct.c" 1
.word	0b01110011101010001001111001001010	#D16MAC XR9,XR7,XR2,XR10,SS,HW
# 0 "" 2
# 616 "simple_idct.c" 1
.word	0b01110000011010001110001001001010	#D16MAC XR9,XR8,XR3,XR10,AA,LW
# 0 "" 2
# 618 "simple_idct.c" 1
.word	0b01110000011010010001111001001010	#D16MAC XR9,XR7,XR4,XR10,AA,LW
# 0 "" 2
# 620 "simple_idct.c" 1
.word	0b01110000001010000000001001001011	#D16MACF XR9,XR0,XR0,XR10,AA,WW
# 0 "" 2
# 622 "simple_idct.c" 1
.word	0b01110000011111000110001010001000	#D16MUL XR10,XR8,XR1,XR15,LW
# 0 "" 2
# 623 "simple_idct.c" 1
.word	0b01110011101111001010001010001010	#D16MAC XR10,XR8,XR2,XR15,SS,HW
# 0 "" 2
# 624 "simple_idct.c" 1
.word	0b01110000011111001101111010001010	#D16MAC XR10,XR7,XR3,XR15,AA,LW
# 0 "" 2
# 626 "simple_idct.c" 1
.word	0b01110011101111010001111010001010	#D16MAC XR10,XR7,XR4,XR15,SS,HW
# 0 "" 2
# 628 "simple_idct.c" 1
.word	0b01110000001111000000001010001011	#D16MACF XR10,XR0,XR0,XR15,AA,WW
# 0 "" 2
# 630 "simple_idct.c" 1
.word	0b01110001001001100111101110001110	#Q16ADD XR14,XR14,XR9,XR9,AS,WW
# 0 "" 2
# 631 "simple_idct.c" 1
.word	0b01110000010000000010001110010001	#S32STD XR14,$2,32
# 0 "" 2
# 632 "simple_idct.c" 1
.word	0b01110000010000000101001001010001	#S32STD XR9,$2,80
# 0 "" 2
# 633 "simple_idct.c" 1
.word	0b01110001001010101011001100001110	#Q16ADD XR12,XR12,XR10,XR10,AS,WW
# 0 "" 2
# 634 "simple_idct.c" 1
.word	0b01110000010000000000010001010100	#S32LDI XR1,$2,4
# 0 "" 2
# 635 "simple_idct.c" 1
.word	0b01110000010000000010111100010001	#S32STD XR12,$2,44
# 0 "" 2
# 636 "simple_idct.c" 1
.word	0b01110000010000000011111010010001	#S32STD XR10,$2,60
# 0 "" 2
#NO_APP
bne	$2,$3,$L8
addiu	$2,$6,128
#APP
# 641 "simple_idct.c" 1
.word	0b01110000110000000000000001010000	#S32LDD XR1,$6,0
# 0 "" 2
#NO_APP
$L9:
#APP
# 644 "simple_idct.c" 1
.word	0b01110000110000000000010010010000	#S32LDD XR2,$6,4
# 0 "" 2
# 645 "simple_idct.c" 1
.word	0b01110000110000000000100011010000	#S32LDD XR3,$6,8
# 0 "" 2
# 646 "simple_idct.c" 1
.word	0b01110000110000000000110100010000	#S32LDD XR4,$6,12
# 0 "" 2
# 649 "simple_idct.c" 1
.word	0b01110000101100011100011011001000	#D16MUL XR11,XR1,XR7,XR12,HW
# 0 "" 2
# 650 "simple_idct.c" 1
.word	0b01110000101110100000011101001000	#D16MUL XR13,XR1,XR8,XR14,HW
# 0 "" 2
# 651 "simple_idct.c" 1
.word	0b01110010101011011100101101001010	#D16MAC XR13,XR2,XR7,XR11,SA,HW
# 0 "" 2
# 652 "simple_idct.c" 1
.word	0b01110011101100100000101110001010	#D16MAC XR14,XR2,XR8,XR12,SS,HW
# 0 "" 2
# 653 "simple_idct.c" 1
.word	0b01110010101110011100111100001010	#D16MAC XR12,XR3,XR7,XR14,SA,HW
# 0 "" 2
# 655 "simple_idct.c" 1
.word	0b01110000101101100000111011001010	#D16MAC XR11,XR3,XR8,XR13,AA,HW
# 0 "" 2
# 658 "simple_idct.c" 1
.word	0b01110000011010010100011001001000	#D16MUL XR9,XR1,XR5,XR10,LW
# 0 "" 2
# 659 "simple_idct.c" 1
.word	0b01110001011010010100111001001010	#D16MAC XR9,XR3,XR5,XR10,AS,LW
# 0 "" 2
# 660 "simple_idct.c" 1
.word	0b01110000010011011000100001001000	#D16MUL XR1,XR2,XR6,XR3,LW
# 0 "" 2
# 661 "simple_idct.c" 1
.word	0b01110010010001011001000011001010	#D16MAC XR3,XR4,XR6,XR1,SA,LW
# 0 "" 2
# 663 "simple_idct.c" 1
.word	0b01110010101011100001001100001010	#D16MAC XR12,XR4,XR8,XR11,SA,HW
# 0 "" 2
# 665 "simple_idct.c" 1
.word	0b01110010101101011101001110001010	#D16MAC XR14,XR4,XR7,XR13,SA,HW
# 0 "" 2
# 668 "simple_idct.c" 1
.word	0b01110001000100000110010010011000	#D32ADD XR2,XR9,XR1,XR4,AS
# 0 "" 2
# 670 "simple_idct.c" 1
.word	0b01110001000001001110101001011000	#D32ADD XR9,XR10,XR3,XR1,AS
# 0 "" 2
# 673 "simple_idct.c" 1
.word	0b01110001001011101100100010011000	#D32ADD XR2,XR2,XR11,XR11,AS
# 0 "" 2
# 674 "simple_idct.c" 1
.word	0b01110001001110111001000100011000	#D32ADD XR4,XR4,XR14,XR14,AS
# 0 "" 2
# 675 "simple_idct.c" 1
.word	0b01110001001100110010011001011000	#D32ADD XR9,XR9,XR12,XR12,AS
# 0 "" 2
# 676 "simple_idct.c" 1
.word	0b01110001001101110100010001011000	#D32ADD XR1,XR1,XR13,XR13,AS
# 0 "" 2
# 678 "simple_idct.c" 1
.word	0b01110000000010000000001001001011	#D16MACF XR9,XR0,XR0,XR2,AA,WW
# 0 "" 2
# 679 "simple_idct.c" 1
.word	0b01110000000001000000000100001011	#D16MACF XR4,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 680 "simple_idct.c" 1
.word	0b01110000001110000000001101001011	#D16MACF XR13,XR0,XR0,XR14,AA,WW
# 0 "" 2
# 681 "simple_idct.c" 1
.word	0b01110000001100000000001011001011	#D16MACF XR11,XR0,XR0,XR12,AA,WW
# 0 "" 2
# 682 "simple_idct.c" 1
.word	0b01110000100000000000000001010000	#S32LDD XR1,$4,0
# 0 "" 2
# 683 "simple_idct.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 684 "simple_idct.c" 1
.word	0b01110000001001000000010100011101	#Q8ACCE XR4,XR1,XR0,XR9,AA
# 0 "" 2
# 685 "simple_idct.c" 1
.word	0b01110000001101000000101011011101	#Q8ACCE XR11,XR2,XR0,XR13,AA
# 0 "" 2
# 686 "simple_idct.c" 1
.word	0b01110000110000000001000001010100	#S32LDI XR1,$6,16
# 0 "" 2
# 688 "simple_idct.c" 1
.word	0b01110000000110100101000100000111	#Q16SAT XR4,XR4,XR9
# 0 "" 2
# 689 "simple_idct.c" 1
.word	0b01110000000110110110111011000111	#Q16SAT XR11,XR11,XR13
# 0 "" 2
# 690 "simple_idct.c" 1
.word	0b01110000100000000000000100010001	#S32STD XR4,$4,0
# 0 "" 2
# 691 "simple_idct.c" 1
.word	0b01110000100000000000011011010001	#S32STD XR11,$4,4
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$6,$2,$L9
addu	$4,$4,$5
.set	macro
.set	reorder

j	$31
.end	ff_simple_idct_add
.size	ff_simple_idct_add, .-ff_simple_idct_add
.align	2
.globl	ff_simple_idct
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct
.type	ff_simple_idct, @function
ff_simple_idct:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
addiu	$3,$4,4
sw	$23,56($sp)
li	$24,21407			# 0x539f
sw	$22,52($sp)
li	$23,8867			# 0x22a3
sw	$21,48($sp)
li	$22,-8867			# 0xffffffffffffdd5d
sw	$20,44($sp)
li	$21,-21407			# 0xffffffffffffac61
sw	$19,40($sp)
li	$14,19266			# 0x4b42
sw	$18,36($sp)
addiu	$19,$4,132
sw	$17,32($sp)
li	$20,12873			# 0x3249
sw	$fp,60($sp)
li	$18,-22725			# 0xffffffffffffa73b
sw	$16,28($sp)
li	$17,4520			# 0x11a8
sw	$4,64($sp)
.option	pic0
j	$L16
.option	pic2
li	$25,-12873			# 0xffffffffffffcdb7

$L13:
lh	$7,-4($3)
lh	$9,2($3)
lh	$12,0($3)
sll	$2,$7,14
mul	$10,$9,$25
subu	$7,$2,$7
li	$2,-4520			# 0xffffffffffffee58
mul	$6,$9,$14
mul	$5,$9,$2
li	$2,22725			# 0x58c5
mul	$4,$9,$18
mul	$9,$8,$2
mul	$2,$8,$14
addiu	$7,$7,1024
mul	$13,$12,$22
addu	$6,$9,$6
addu	$5,$2,$5
mul	$9,$8,$17
mul	$2,$8,$20
mul	$8,$12,$24
addu	$4,$2,$4
addu	$2,$9,$10
addu	$10,$8,$7
mul	$8,$12,$23
addu	$9,$8,$7
addu	$8,$13,$7
mul	$13,$12,$21
beq	$11,$0,$L15
addu	$7,$13,$7

lh	$12,6($3)
lh	$11,4($3)
lh	$16,8($3)
mul	$fp,$12,$20
lh	$15,10($3)
sll	$13,$11,14
subu	$11,$13,$11
addu	$13,$fp,$6
mul	$fp,$16,$23
sw	$13,16($sp)
subu	$13,$0,$11
addu	$6,$fp,$11
mul	$fp,$16,$21
sw	$6,8($sp)
addu	$6,$fp,$13
lw	$fp,8($sp)
sw	$6,12($sp)
mul	$6,$16,$24
addu	$10,$10,$fp
addu	$13,$6,$13
mul	$6,$16,$22
addu	$8,$8,$13
addu	$11,$6,$11
mul	$6,$12,$18
addu	$7,$7,$11
addu	$5,$6,$5
mul	$6,$12,$17
addu	$16,$6,$4
mul	$4,$12,$14
mul	$12,$15,$17
addu	$2,$4,$2
lw	$4,16($sp)
addu	$6,$12,$4
mul	$4,$15,$25
mul	$12,$15,$14
addu	$5,$4,$5
addu	$4,$12,$16
mul	$12,$15,$18
addu	$2,$12,$2
lw	$12,12($sp)
addu	$9,$9,$12
$L15:
addu	$13,$10,$6
subu	$6,$10,$6
addu	$11,$7,$2
sra	$6,$6,11
addu	$15,$9,$5
addu	$12,$8,$4
subu	$2,$7,$2
sh	$6,10($3)
subu	$5,$9,$5
subu	$4,$8,$4
sra	$13,$13,11
sra	$15,$15,11
sra	$5,$5,11
sra	$7,$12,11
sh	$13,-4($3)
sra	$4,$4,11
sh	$15,-2($3)
sra	$6,$11,11
sh	$5,8($3)
sra	$2,$2,11
sh	$7,0($3)
sh	$4,6($3)
addiu	$3,$3,16
sh	$6,-14($3)
beq	$3,$19,$L24
sh	$2,-12($3)

$L16:
lw	$2,4($3)
lw	$4,0($3)
lw	$11,8($3)
lh	$8,-2($3)
or	$4,$2,$4
or	$4,$4,$11
or	$4,$4,$8
bne	$4,$0,$L13
or	$11,$11,$2

lh	$2,-4($3)
addiu	$3,$3,16
sll	$2,$2,3
andi	$4,$2,0xffff
sll	$2,$4,16
addu	$2,$2,$4
sw	$2,-8($3)
sw	$2,-12($3)
sw	$2,-16($3)
bne	$3,$19,$L16
sw	$2,-20($3)

$L24:
lw	$4,64($sp)
li	$23,21407			# 0x539f
li	$22,8867			# 0x22a3
li	$21,-8867			# 0xffffffffffffdd5d
addiu	$fp,$4,16
li	$20,-21407			# 0xffffffffffffac61
li	$25,19266			# 0x4b42
li	$19,12873			# 0x3249
sw	$fp,12($sp)
li	$17,4520			# 0x11a8
li	$16,-22725			# 0xffffffffffffa73b
li	$18,-12873			# 0xffffffffffffcdb7
$L17:
lh	$3,0($4)
addiu	$4,$4,2
addiu	$3,$3,32
lh	$10,30($4)
sll	$5,$3,14
lh	$13,62($4)
lh	$12,46($4)
subu	$3,$5,$3
lh	$11,14($4)
mul	$5,$10,$23
lh	$15,78($4)
mul	$24,$10,$21
lh	$14,94($4)
sll	$7,$13,14
lh	$8,110($4)
subu	$7,$7,$13
subu	$9,$0,$7
addu	$2,$5,$3
mul	$5,$10,$22
addu	$6,$5,$3
addu	$5,$24,$3
mul	$24,$10,$20
addu	$10,$24,$3
addu	$3,$2,$7
addu	$7,$10,$7
movn	$2,$3,$13
mul	$3,$12,$25
addu	$24,$6,$9
movz	$7,$10,$13
li	$10,22725			# 0x58c5
addu	$9,$5,$9
movz	$24,$6,$13
mul	$fp,$11,$10
mul	$10,$11,$25
movz	$9,$5,$13
mul	$5,$12,$16
sw	$3,8($sp)
li	$3,-4520			# 0xffffffffffffee58
lw	$13,8($sp)
mul	$6,$12,$3
mul	$12,$12,$18
addu	$3,$fp,$13
addu	$6,$10,$6
mul	$10,$11,$19
addu	$5,$10,$5
mul	$10,$11,$17
addu	$11,$10,$12
mul	$10,$15,$19
addu	$3,$10,$3
mul	$10,$15,$16
addu	$6,$10,$6
mul	$10,$15,$17
addu	$5,$10,$5
mul	$10,$15,$25
addu	$15,$10,$11
mul	$10,$14,$22
addu	$11,$10,$2
mul	$2,$14,$20
mul	$10,$8,$16
addu	$24,$2,$24
mul	$2,$14,$23
addu	$9,$2,$9
mul	$2,$14,$21
addu	$7,$2,$7
mul	$2,$8,$17
addu	$3,$2,$3
mul	$2,$8,$18
addu	$6,$2,$6
mul	$2,$8,$25
addu	$5,$2,$5
addu	$2,$10,$15
addu	$12,$9,$5
addu	$10,$11,$3
sra	$12,$12,20
addu	$15,$24,$6
addu	$8,$7,$2
subu	$5,$9,$5
sh	$12,30($4)
subu	$7,$7,$2
lw	$12,12($sp)
subu	$6,$24,$6
subu	$3,$11,$3
sra	$10,$10,20
sra	$15,$15,20
sra	$8,$8,20
sra	$7,$7,20
sh	$10,-2($4)
sra	$5,$5,20
sh	$15,14($4)
sra	$6,$6,20
sh	$8,46($4)
sra	$3,$3,20
sh	$7,62($4)
sh	$5,78($4)
sh	$6,94($4)
bne	$4,$12,$L17
sh	$3,110($4)

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	ff_simple_idct
.size	ff_simple_idct, .-ff_simple_idct
.align	2
.globl	ff_simple_idct248_put
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct248_put
.type	ff_simple_idct248_put, @function
ff_simple_idct248_put:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-56
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,20($sp)
addiu	$16,$6,128
sw	$fp,52($sp)
move	$2,$6
sw	$23,48($sp)
sw	$22,44($sp)
sw	$21,40($sp)
sw	$20,36($sp)
sw	$19,32($sp)
sw	$18,28($sp)
sw	$17,24($sp)
.cprestore	0
sw	$6,64($sp)
sw	$4,56($sp)
$L26:
lhu	$14,0($2)
addiu	$2,$2,32
lhu	$4,-16($2)
lhu	$3,-14($2)
lhu	$13,-30($2)
lhu	$9,-28($2)
addu	$18,$4,$14
lhu	$6,-12($2)
subu	$14,$14,$4
lhu	$8,-26($2)
addu	$17,$3,$13
lhu	$4,-10($2)
subu	$13,$13,$3
lhu	$7,-8($2)
addu	$15,$6,$9
lhu	$3,-24($2)
subu	$12,$9,$6
lhu	$10,-6($2)
addu	$19,$4,$8
lhu	$9,-4($2)
subu	$11,$8,$4
lhu	$6,-22($2)
addu	$20,$7,$3
lhu	$4,-20($2)
subu	$7,$3,$7
lhu	$8,-2($2)
lhu	$3,-18($2)
addu	$21,$10,$6
subu	$6,$6,$10
sh	$18,-32($2)
addu	$10,$9,$4
sh	$14,-16($2)
subu	$4,$4,$9
sh	$17,-30($2)
addu	$9,$8,$3
sh	$13,-14($2)
subu	$3,$3,$8
sh	$15,-28($2)
sh	$12,-12($2)
sh	$19,-26($2)
sh	$11,-10($2)
sh	$20,-24($2)
sh	$7,-8($2)
sh	$21,-22($2)
sh	$6,-6($2)
sh	$10,-20($2)
sh	$4,-4($2)
sh	$9,-18($2)
bne	$2,$16,$L26
sh	$3,-2($2)

lw	$2,64($sp)
li	$22,21407			# 0x539f
li	$21,8867			# 0x22a3
sw	$5,8($sp)
li	$20,-8867			# 0xffffffffffffdd5d
addiu	$3,$2,4
addiu	$18,$2,132
li	$19,-21407			# 0xffffffffffffac61
li	$15,19266			# 0x4b42
li	$17,-22725			# 0xffffffffffffa73b
li	$16,4520			# 0x11a8
.option	pic0
j	$L30
.option	pic2
li	$fp,-12873			# 0xffffffffffffcdb7

$L27:
lh	$7,-4($3)
lh	$8,2($3)
lh	$12,0($3)
sll	$4,$7,14
mul	$10,$8,$fp
subu	$7,$4,$7
li	$4,-4520			# 0xffffffffffffee58
mul	$6,$8,$15
mul	$5,$8,$4
mul	$4,$8,$17
li	$8,22725			# 0x58c5
addiu	$7,$7,1024
mul	$9,$2,$8
mul	$8,$2,$15
mul	$13,$12,$20
addu	$6,$9,$6
addu	$5,$8,$5
li	$8,12873			# 0x3249
mul	$9,$2,$8
mul	$8,$2,$16
addu	$4,$9,$4
addu	$2,$8,$10
mul	$8,$12,$22
addu	$10,$8,$7
mul	$8,$12,$21
addu	$9,$8,$7
addu	$8,$13,$7
mul	$13,$12,$19
beq	$11,$0,$L29
addu	$7,$13,$7

lh	$11,4($3)
lh	$23,8($3)
lh	$12,6($3)
sll	$13,$11,14
lh	$14,10($3)
mul	$25,$23,$21
subu	$11,$13,$11
li	$13,12873			# 0x3249
mul	$24,$12,$13
subu	$13,$0,$11
addu	$6,$24,$6
addu	$24,$25,$11
mul	$25,$23,$19
sw	$24,12($sp)
addu	$24,$25,$13
mul	$25,$23,$22
addu	$9,$9,$24
addu	$13,$25,$13
mul	$25,$23,$20
mul	$23,$12,$17
addu	$8,$8,$13
addu	$11,$25,$11
addu	$5,$23,$5
mul	$23,$12,$16
addu	$7,$7,$11
addu	$25,$23,$4
mul	$4,$12,$15
mul	$12,$14,$15
addu	$2,$4,$2
mul	$4,$14,$16
addu	$6,$4,$6
mul	$4,$14,$fp
addu	$5,$4,$5
addu	$4,$12,$25
mul	$12,$14,$17
lw	$14,12($sp)
addu	$10,$10,$14
addu	$2,$12,$2
$L29:
addu	$13,$10,$6
subu	$6,$10,$6
addu	$12,$8,$4
sra	$6,$6,11
addu	$11,$7,$2
addu	$14,$9,$5
subu	$4,$8,$4
sh	$6,10($3)
subu	$2,$7,$2
subu	$5,$9,$5
sra	$13,$13,11
sra	$8,$14,11
sra	$5,$5,11
sra	$7,$12,11
sh	$13,-4($3)
sra	$4,$4,11
sh	$8,-2($3)
sra	$6,$11,11
sh	$5,8($3)
sra	$2,$2,11
sh	$7,0($3)
sh	$4,6($3)
addiu	$3,$3,16
sh	$6,-14($3)
beq	$3,$18,$L39
sh	$2,-12($3)

$L30:
lw	$5,4($3)
lw	$4,0($3)
lw	$6,8($3)
lh	$2,-2($3)
or	$4,$5,$4
or	$4,$4,$6
or	$4,$4,$2
bne	$4,$0,$L27
or	$11,$6,$5

lh	$2,-4($3)
addiu	$3,$3,16
sll	$2,$2,3
andi	$4,$2,0xffff
sll	$2,$4,16
addu	$2,$2,$4
sw	$2,-8($3)
sw	$2,-12($3)
sw	$2,-16($3)
bne	$3,$18,$L30
sw	$2,-20($3)

$L39:
lw	$5,8($sp)
li	$12,65536			# 0x10000
lw	$4,56($sp)
li	$17,2676			# 0xa74
lw	$9,%got(ff_cropTbl)($28)
li	$11,1108			# 0x454
sll	$10,$5,2
lw	$6,64($sp)
sll	$13,$5,1
addu	$10,$4,$10
addiu	$19,$4,8
addiu	$9,$9,1024
li	$16,-2676			# 0xfffffffffffff58c
$L31:
lh	$15,96($6)
subu	$21,$10,$13
lh	$18,32($6)
addu	$20,$10,$13
lh	$2,64($6)
addu	$7,$10,$5
mult	$15,$11
lh	$3,0($6)
madd	$18,$17
addiu	$10,$10,1
addu	$23,$3,$2
subu	$3,$3,$2
mflo	$22
mult	$15,$16
madd	$18,$11
sll	$14,$23,11
sll	$3,$3,11
addu	$14,$14,$12
mflo	$18
addu	$15,$14,$22
addu	$2,$3,$12
sra	$15,$15,17
subu	$14,$14,$22
addu	$15,$9,$15
sra	$14,$14,17
addu	$3,$2,$18
lbu	$15,0($15)
subu	$2,$2,$18
sra	$3,$3,17
sra	$2,$2,17
addu	$3,$9,$3
sb	$15,0($4)
addu	$2,$9,$2
addu	$14,$9,$14
lbu	$3,0($3)
addiu	$6,$6,2
addu	$8,$4,$5
addu	$15,$7,$13
addu	$18,$8,$13
sb	$3,0($21)
addiu	$4,$4,1
lbu	$2,0($2)
sb	$2,-1($10)
lbu	$2,0($14)
sb	$2,0($20)
lh	$20,110($6)
lh	$21,46($6)
lh	$2,78($6)
mult	$20,$11
lh	$3,14($6)
madd	$21,$17
addu	$23,$3,$2
subu	$3,$3,$2
mflo	$22
mult	$20,$16
madd	$21,$11
sll	$14,$23,11
sll	$3,$3,11
addu	$14,$14,$12
mflo	$21
addu	$20,$14,$22
addu	$2,$3,$12
sra	$20,$20,17
subu	$14,$14,$22
addu	$20,$9,$20
sra	$14,$14,17
addu	$3,$2,$21
lbu	$20,0($20)
subu	$2,$2,$21
sra	$3,$3,17
sra	$2,$2,17
addu	$3,$9,$3
sb	$20,0($8)
addu	$2,$9,$2
addu	$14,$9,$14
lbu	$3,0($3)
sb	$3,0($18)
lbu	$2,0($2)
sb	$2,0($7)
lbu	$2,0($14)
bne	$4,$19,$L31
sb	$2,0($15)

lw	$fp,52($sp)
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	ff_simple_idct248_put
.size	ff_simple_idct248_put, .-ff_simple_idct248_put
.align	2
.globl	ff_simple_idct84_add
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct84_add
.type	ff_simple_idct84_add, @function
ff_simple_idct84_add:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-64
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$fp,60($sp)
addiu	$7,$6,4
sw	$22,52($sp)
li	$24,19266			# 0x4b42
sw	$21,48($sp)
li	$22,21407			# 0x539f
sw	$20,44($sp)
li	$21,8867			# 0x22a3
sw	$19,40($sp)
li	$20,-8867			# 0xffffffffffffdd5d
sw	$18,36($sp)
addiu	$19,$6,68
sw	$17,32($sp)
li	$18,-22725			# 0xffffffffffffa73b
li	$17,4520			# 0x11a8
sw	$23,56($sp)
li	$fp,-12873			# 0xffffffffffffcdb7
sw	$16,28($sp)
.cprestore	0
sw	$4,64($sp)
sw	$5,12($sp)
sw	$6,16($sp)
$L44:
lw	$4,4($7)
lw	$3,0($7)
lw	$8,8($7)
lh	$2,-2($7)
or	$3,$4,$3
or	$3,$3,$8
or	$3,$3,$2
beq	$3,$0,$L52
or	$13,$8,$4

lh	$9,-4($7)
li	$6,22725			# 0x58c5
lh	$10,2($7)
lh	$14,0($7)
sll	$3,$9,14
mul	$5,$10,$fp
subu	$9,$3,$9
li	$3,-4520			# 0xffffffffffffee58
mul	$8,$10,$24
mul	$4,$10,$3
mul	$3,$10,$18
mul	$10,$2,$6
mul	$6,$2,$24
addiu	$9,$9,1024
addu	$8,$10,$8
addu	$4,$6,$4
li	$6,12873			# 0x3249
mul	$10,$2,$6
mul	$6,$2,$17
addu	$3,$10,$3
addu	$2,$6,$5
mul	$5,$14,$22
addu	$12,$5,$9
mul	$5,$14,$21
addu	$11,$5,$9
mul	$5,$14,$20
addu	$10,$5,$9
li	$5,-21407			# 0xffffffffffffac61
mul	$6,$14,$5
beq	$13,$0,$L43
addu	$9,$6,$9

lh	$14,6($7)
li	$5,12873			# 0x3249
lh	$23,8($7)
lh	$13,4($7)
mul	$6,$14,$5
lh	$16,10($7)
sll	$15,$13,14
subu	$13,$15,$13
subu	$15,$0,$13
addu	$8,$6,$8
mul	$6,$23,$21
addu	$5,$6,$13
sw	$5,8($sp)
li	$5,-21407			# 0xffffffffffffac61
mul	$6,$23,$5
mul	$5,$23,$22
addu	$25,$6,$15
addu	$15,$5,$15
mul	$5,$23,$20
addu	$11,$11,$25
addu	$10,$10,$15
addu	$13,$5,$13
mul	$5,$14,$18
addu	$9,$9,$13
addu	$4,$5,$4
mul	$5,$14,$17
addu	$6,$5,$3
mul	$3,$14,$24
mul	$5,$16,$24
addu	$2,$3,$2
mul	$3,$16,$17
addu	$8,$3,$8
mul	$3,$16,$fp
addu	$4,$3,$4
addu	$3,$5,$6
lw	$6,8($sp)
mul	$5,$16,$18
addu	$12,$12,$6
addu	$2,$5,$2
$L43:
addu	$15,$12,$8
subu	$8,$12,$8
addu	$14,$10,$3
sra	$8,$8,11
addu	$13,$9,$2
addu	$16,$11,$4
subu	$3,$10,$3
sh	$8,10($7)
subu	$2,$9,$2
subu	$4,$11,$4
sra	$15,$15,11
sra	$10,$16,11
sra	$4,$4,11
sra	$9,$14,11
sh	$15,-4($7)
sra	$3,$3,11
sh	$10,-2($7)
sra	$8,$13,11
sh	$4,8($7)
sra	$2,$2,11
sh	$9,0($7)
sh	$3,6($7)
sh	$8,2($7)
sh	$2,4($7)
$L42:
addiu	$7,$7,16
bne	$7,$19,$L44
lw	$5,12($sp)

li	$14,2896			# 0xb50
lw	$4,64($sp)
li	$13,65536			# 0x10000
lw	$10,%got(ff_cropTbl)($28)
li	$16,3784			# 0xec8
sll	$9,$5,1
lw	$6,16($sp)
addiu	$17,$4,8
addu	$11,$9,$5
addiu	$10,$10,1024
addu	$5,$4,$5
addu	$9,$4,$9
addu	$11,$4,$11
li	$12,1567			# 0x61f
li	$15,-3784			# 0xfffffffffffff138
$L45:
lh	$8,32($6)
addiu	$4,$4,1
lh	$2,0($6)
addiu	$5,$5,1
lh	$19,48($6)
addiu	$9,$9,1
lh	$7,16($6)
addiu	$11,$11,1
addu	$3,$2,$8
lbu	$20,-1($4)
mul	$22,$19,$12
mul	$21,$7,$16
mul	$18,$3,$14
subu	$2,$2,$8
addiu	$6,$6,2
mul	$8,$2,$14
addu	$3,$18,$13
addu	$18,$21,$22
mul	$21,$19,$15
addu	$2,$8,$13
mul	$19,$7,$12
addu	$8,$3,$18
subu	$3,$3,$18
sra	$8,$8,17
sra	$3,$3,17
addu	$8,$20,$8
addu	$8,$10,$8
addu	$7,$19,$21
addu	$18,$2,$7
subu	$2,$2,$7
lbu	$7,0($8)
sra	$18,$18,17
sra	$2,$2,17
sb	$7,-1($4)
lbu	$8,-1($5)
addu	$7,$8,$18
addu	$7,$10,$7
lbu	$7,0($7)
sb	$7,-1($5)
lbu	$7,-1($9)
addu	$2,$7,$2
addu	$2,$10,$2
lbu	$2,0($2)
sb	$2,-1($9)
lbu	$2,-1($11)
addu	$3,$2,$3
addu	$3,$10,$3
lbu	$2,0($3)
bne	$4,$17,$L45
sb	$2,-1($11)

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

$L52:
lh	$2,-4($7)
sll	$2,$2,3
andi	$3,$2,0xffff
sll	$2,$3,16
addu	$2,$2,$3
sw	$2,8($7)
sw	$2,4($7)
sw	$2,0($7)
.option	pic0
j	$L42
.option	pic2
sw	$2,-4($7)

.set	macro
.set	reorder
.end	ff_simple_idct84_add
.size	ff_simple_idct84_add, .-ff_simple_idct84_add
.align	2
.globl	ff_simple_idct48_add
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct48_add
.type	ff_simple_idct48_add, @function
ff_simple_idct48_add:
.frame	$sp,80,$31		# vars= 32, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-80
addiu	$28,$28,%lo(__gnu_local_gp)
addiu	$12,$6,128
sw	$fp,76($sp)
li	$9,23170			# 0x5a82
sw	$23,72($sp)
li	$11,30274			# 0x7642
sw	$22,68($sp)
li	$8,12540			# 0x30fc
sw	$21,64($sp)
li	$10,-30274			# 0xffffffffffff89be
sw	$20,60($sp)
sw	$19,56($sp)
move	$24,$6
sw	$18,52($sp)
move	$13,$6
sw	$17,48($sp)
sw	$16,44($sp)
.cprestore	0
sw	$5,84($sp)
$L54:
lh	$3,0($13)
addiu	$13,$13,16
lh	$2,-12($13)
lh	$15,-10($13)
lh	$14,-14($13)
addu	$5,$3,$2
mul	$16,$15,$8
mul	$7,$5,$9
mul	$5,$14,$11
subu	$3,$3,$2
mul	$2,$15,$10
mul	$3,$3,$9
addu	$15,$5,$16
mul	$5,$14,$8
addu	$14,$5,$2
addiu	$5,$7,1024
addiu	$2,$3,1024
addu	$7,$5,$15
addu	$3,$2,$14
subu	$5,$5,$15
subu	$2,$2,$14
sra	$7,$7,11
sra	$3,$3,11
sra	$2,$2,11
sra	$5,$5,11
sh	$7,-16($13)
sh	$3,-14($13)
sh	$2,-12($13)
bne	$13,$12,$L54
sh	$5,-10($13)

lw	$7,84($sp)
addiu	$6,$6,8
lw	$16,%got(ff_cropTbl)($28)
li	$21,19266			# 0x4b42
sll	$2,$7,1
sw	$6,20($sp)
sll	$8,$7,2
addu	$17,$7,$2
addu	$19,$4,$7
addu	$18,$7,$17
sw	$8,24($sp)
addu	$20,$4,$2
addu	$18,$18,$7
addu	$17,$4,$17
addu	$18,$4,$18
addiu	$16,$16,1024
$L55:
lh	$14,0($24)
addiu	$24,$24,2
lh	$8,46($24)
addiu	$4,$4,1
lh	$10,30($24)
addiu	$19,$19,1
addiu	$3,$14,32
lw	$23,84($sp)
mul	$7,$8,$21
lw	$25,84($sp)
sll	$14,$3,14
lw	$fp,24($sp)
addu	$23,$17,$23
lh	$22,14($24)
subu	$14,$14,$3
lbu	$3,-1($4)
addu	$25,$18,$25
lh	$13,78($24)
addu	$fp,$17,$fp
lh	$9,110($24)
sw	$23,16($sp)
li	$23,22725			# 0x58c5
sw	$3,28($sp)
li	$3,21407			# 0x539f
sw	$25,12($sp)
li	$25,12873			# 0x3249
mul	$6,$10,$3
sw	$fp,8($sp)
mtlo	$7
lh	$12,62($24)
li	$fp,4520			# 0x11a8
lh	$15,94($24)
addiu	$17,$17,1
sll	$2,$12,14
addiu	$18,$18,1
subu	$2,$2,$12
addu	$5,$6,$14
madd	$22,$23
madd	$13,$25
madd	$9,$fp
li	$6,8867			# 0x22a3
li	$25,-4520			# 0xffffffffffffee58
addu	$11,$5,$2
mflo	$3
mul	$7,$10,$6
movz	$11,$5,$12
mult	$8,$25
madd	$22,$21
subu	$5,$0,$2
addiu	$20,$20,1
mflo	$25
addu	$23,$7,$14
li	$7,8867			# 0x22a3
addu	$6,$23,$5
mul	$fp,$15,$7
li	$7,-22725			# 0xffffffffffffa73b
movz	$6,$23,$12
li	$23,-12873			# 0xffffffffffffcdb7
mtlo	$25
addu	$11,$fp,$11
madd	$13,$7
madd	$9,$23
li	$23,-8867			# 0xffffffffffffdd5d
mflo	$7
mul	$25,$10,$23
addu	$23,$11,$3
subu	$3,$11,$3
sw	$23,32($sp)
li	$23,-21407			# 0xffffffffffffac61
sra	$3,$3,20
addu	$fp,$25,$14
mul	$25,$15,$23
addu	$5,$fp,$5
addu	$6,$25,$6
lw	$25,32($sp)
sw	$5,32($sp)
lw	$5,28($sp)
sra	$23,$25,20
addu	$25,$5,$23
lw	$5,32($sp)
addu	$23,$6,$7
addu	$25,$16,$25
sra	$23,$23,20
movz	$5,$fp,$12
li	$fp,4520			# 0x11a8
lbu	$25,0($25)
subu	$6,$6,$7
sw	$23,32($sp)
li	$23,-22725			# 0xffffffffffffa73b
li	$7,-8867			# 0xffffffffffffdd5d
mult	$8,$23
sw	$25,36($sp)
li	$25,12873			# 0x3249
li	$23,21407			# 0x539f
madd	$22,$25
madd	$13,$fp
sra	$6,$6,20
mflo	$fp
mul	$25,$15,$23
mul	$23,$9,$21
sw	$fp,28($sp)
addu	$5,$25,$5
lw	$25,36($sp)
addu	$fp,$23,$fp
li	$23,-21407			# 0xffffffffffffac61
sb	$25,-1($4)
mul	$25,$10,$23
li	$23,-12873			# 0xffffffffffffcdb7
mult	$8,$23
lw	$8,32($sp)
addu	$10,$25,$14
lbu	$14,-1($19)
li	$25,4520			# 0x11a8
addu	$2,$10,$2
addu	$23,$14,$8
madd	$22,$25
movn	$10,$2,$12
li	$8,-22725			# 0xffffffffffffa73b
madd	$13,$21
addu	$23,$16,$23
madd	$9,$8
addu	$25,$5,$fp
lbu	$23,0($23)
subu	$5,$5,$fp
sra	$25,$25,20
mflo	$13
mul	$8,$15,$7
sb	$23,-1($19)
sra	$5,$5,20
lbu	$2,-1($20)
addu	$25,$2,$25
addu	$10,$8,$10
addu	$25,$16,$25
addu	$2,$10,$13
subu	$10,$10,$13
sra	$7,$2,20
lbu	$2,0($25)
sra	$10,$10,20
sb	$2,-1($20)
lbu	$2,-1($17)
lw	$23,16($sp)
lw	$25,12($sp)
addu	$2,$2,$7
addu	$2,$16,$2
lbu	$2,0($2)
sb	$2,-1($17)
lbu	$13,0($23)
addu	$10,$13,$10
addu	$10,$16,$10
lbu	$2,0($10)
sb	$2,0($23)
lbu	$fp,-1($18)
addu	$5,$fp,$5
lw	$fp,8($sp)
addu	$5,$16,$5
lbu	$2,0($5)
sb	$2,-1($18)
lbu	$7,0($25)
addu	$6,$7,$6
addu	$6,$16,$6
lbu	$2,0($6)
sb	$2,0($25)
lbu	$2,0($fp)
addu	$3,$2,$3
addu	$3,$16,$3
lbu	$2,0($3)
sb	$2,0($fp)
lw	$2,20($sp)
bne	$24,$2,$L55
lw	$fp,76($sp)

lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	ff_simple_idct48_add
.size	ff_simple_idct48_add, .-ff_simple_idct48_add
.align	2
.globl	ff_simple_idct44_add
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct44_add
.type	ff_simple_idct44_add, @function
ff_simple_idct44_add:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lh	$10,0($6)
addiu	$sp,$sp,-56
lh	$3,4($6)
li	$15,12540			# 0x30fc
lh	$9,16($6)
lui	$28,%hi(__gnu_local_gp)
lh	$7,32($6)
lh	$8,20($6)
addu	$13,$10,$3
lh	$2,36($6)
subu	$10,$10,$3
lh	$14,48($6)
addiu	$28,$28,%lo(__gnu_local_gp)
lh	$3,52($6)
addu	$11,$9,$8
sw	$17,24($sp)
subu	$8,$9,$8
sw	$16,20($sp)
addu	$9,$7,$2
lh	$17,54($6)
li	$16,23170			# 0x5a82
subu	$2,$7,$2
lh	$25,6($6)
addu	$12,$14,$3
sw	$22,44($sp)
subu	$14,$14,$3
sw	$19,32($sp)
mul	$3,$2,$16
lh	$22,2($6)
mul	$2,$17,$15
lh	$24,22($6)
li	$19,30274			# 0x7642
sw	$18,28($sp)
mul	$7,$25,$15
sw	$20,36($sp)
lh	$18,38($6)
li	$20,-30274			# 0xffffffffffff89be
mul	$12,$12,$16
sw	$23,48($sp)
mul	$14,$14,$16
lh	$23,18($6)
sw	$3,8($sp)
mul	$13,$13,$16
sw	$2,12($sp)
mul	$2,$22,$19
mul	$3,$18,$15
sw	$21,40($sp)
mul	$10,$10,$16
lh	$21,34($6)
mul	$11,$11,$16
sw	$fp,52($sp)
mul	$8,$8,$16
lh	$fp,50($6)
mul	$9,$9,$16
.cprestore	0
mul	$16,$24,$15
mul	$25,$25,$20
mul	$24,$24,$20
mul	$18,$18,$20
mul	$17,$17,$20
addu	$20,$2,$7
mul	$2,$22,$15
addiu	$10,$10,1024
addiu	$8,$8,1024
addiu	$13,$13,1024
addiu	$9,$9,1024
addiu	$7,$14,1024
addiu	$11,$11,1024
addu	$25,$2,$25
mul	$2,$23,$19
addu	$16,$2,$16
mul	$2,$23,$15
addu	$23,$11,$16
subu	$16,$11,$16
sra	$23,$23,11
sra	$16,$16,11
li	$11,1567			# 0x61f
addu	$24,$2,$24
mul	$2,$21,$19
addu	$22,$2,$3
mul	$2,$21,$15
mul	$3,$fp,$19
addu	$21,$8,$24
subu	$24,$8,$24
addu	$8,$9,$22
subu	$9,$9,$22
sra	$24,$24,11
addu	$18,$2,$18
lw	$2,12($sp)
sra	$9,$9,11
sra	$21,$21,11
addu	$19,$3,$2
lw	$3,8($sp)
mul	$2,$fp,$15
addu	$fp,$13,$20
addu	$15,$10,$25
subu	$25,$10,$25
sra	$15,$15,11
sra	$fp,$fp,11
sra	$25,$25,11
addu	$17,$2,$17
sh	$15,2($6)
addiu	$2,$3,1024
sh	$fp,0($6)
addiu	$3,$12,1024
sh	$25,4($6)
addu	$14,$7,$17
sh	$9,38($6)
addu	$10,$3,$19
sh	$24,20($6)
subu	$13,$13,$20
sh	$23,16($6)
sra	$10,$10,11
sh	$21,18($6)
sll	$12,$5,1
sh	$16,22($6)
addu	$20,$2,$18
lw	$9,%got(ff_cropTbl)($28)
subu	$18,$2,$18
sh	$10,48($6)
subu	$7,$7,$17
subu	$3,$3,$19
sra	$13,$13,11
sra	$2,$8,11
sra	$8,$14,11
addu	$10,$12,$5
sh	$13,6($6)
sra	$20,$20,11
sh	$2,32($6)
sra	$18,$18,11
sh	$8,50($6)
sra	$7,$7,11
sra	$3,$3,11
sh	$20,34($6)
addu	$8,$4,$12
sh	$18,36($6)
addu	$5,$4,$5
sh	$7,52($6)
addu	$10,$4,$10
sh	$3,54($6)
addiu	$24,$4,4
addiu	$9,$9,1024
li	$13,2896			# 0xb50
li	$12,65536			# 0x10000
li	$15,3784			# 0xec8
li	$14,-3784			# 0xfffffffffffff138
$L60:
lh	$25,32($6)
addiu	$4,$4,1
lh	$2,0($6)
addiu	$5,$5,1
lh	$17,48($6)
addiu	$8,$8,1
lh	$7,16($6)
addiu	$10,$10,1
addu	$3,$2,$25
lbu	$18,-1($4)
mul	$20,$17,$11
mul	$19,$7,$15
mul	$16,$3,$13
subu	$2,$2,$25
addiu	$6,$6,2
addu	$3,$16,$12
addu	$16,$19,$20
mul	$19,$17,$14
mul	$17,$2,$13
addu	$25,$3,$16
subu	$3,$3,$16
sra	$25,$25,17
sra	$3,$3,17
addu	$25,$18,$25
addu	$2,$17,$12
mul	$17,$7,$11
addu	$25,$9,$25
addu	$7,$17,$19
addu	$16,$2,$7
subu	$2,$2,$7
lbu	$7,0($25)
sra	$16,$16,17
sra	$2,$2,17
sb	$7,-1($4)
lbu	$25,-1($5)
addu	$7,$25,$16
addu	$7,$9,$7
lbu	$7,0($7)
sb	$7,-1($5)
lbu	$7,-1($8)
addu	$2,$7,$2
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,-1($8)
lbu	$2,-1($10)
addu	$3,$2,$3
addu	$3,$9,$3
lbu	$2,0($3)
bne	$4,$24,$L60
sb	$2,-1($10)

lw	$fp,52($sp)
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	ff_simple_idct44_add
.size	ff_simple_idct44_add, .-ff_simple_idct44_add
.align	2
.globl	disable_jz4740_mxu
.set	nomips16
.set	nomicromips
.ent	disable_jz4740_mxu
.type	disable_jz4740_mxu, @function
disable_jz4740_mxu:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
#APP
# 903 "simple_idct.c" 1
.word	0b01110000000001000000010000101111	#S32I2M XR16,$4
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
j	$31
move	$2,$0
.set	macro
.set	reorder

.end	disable_jz4740_mxu
.size	disable_jz4740_mxu, .-disable_jz4740_mxu
.align	2
.globl	enable_jz4740_mxu
.set	nomips16
.set	nomicromips
.ent	enable_jz4740_mxu
.type	enable_jz4740_mxu, @function
enable_jz4740_mxu:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
#APP
# 910 "simple_idct.c" 1
.word	0b01110000000000100000010000101110	#S32M2I XR16, $2
# 0 "" 2
#NO_APP
ori	$3,$2,0x7
#APP
# 912 "simple_idct.c" 1
.word	0b01110000000000110000010000101111	#S32I2M XR16,$3
# 0 "" 2
#NO_APP
j	$31
.end	enable_jz4740_mxu
.size	enable_jz4740_mxu, .-enable_jz4740_mxu
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
