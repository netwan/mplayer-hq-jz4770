	.file	1 "cook.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.abicalls
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	scalar_dequant_float
	.type	scalar_dequant_float, @function
scalar_dequant_float:
	.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
	.mask	0x001f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$9,$6,63
	lui	$2,%hi(rootpow2tab)
	sll	$3,$9,2
	addiu	$2,$2,%lo(rootpow2tab)
	sll	$6,$6,2
	addu	$2,$2,$3
	lui	$14,%hi(dither_tab)
	lwc1	$f1,0($2)
	lui	$2,%hi(sp32_rootpow2tab)
	lui	$9,%hi(rootpow2tab_sft)
	addiu	$sp,$sp,-24
	lw	$10,%lo(sp32_rootpow2tab)($2)
	sll	$15,$5,2
	sll	$11,$5,1
	sw	$16,4($sp)
	sll	$5,$5,4
	sw	$20,20($sp)
	addu	$10,$10,$6
	sw	$19,16($sp)
	lui	$6,%hi(s32_dither_tab_L20)
	sw	$18,12($sp)
	addiu	$14,$14,%lo(dither_tab)
	sw	$17,8($sp)
	addiu	$6,$6,%lo(s32_dither_tab_L20)
	lw	$13,40($sp)
	addiu	$9,$9,%lo(rootpow2tab_sft)
	lw	$12,44($sp)
	lui	$25,%hi(quant_centroid_tab)
	lw	$8,48($sp)
	lui	$24,%hi(s32_quant_centroid_tab_L14)
	addu	$14,$15,$14
	subu	$5,$5,$11
	move	$2,$0
	addu	$15,$15,$6
	addu	$9,$9,$3
	addiu	$25,$25,%lo(quant_centroid_tab)
	addiu	$24,$24,%lo(s32_quant_centroid_tab_L14)
	li	$11,80			# 0x50
	.option	pic0
	j	$L6
	.option	pic2
	move	$16,$7

$L14:
	sll	$3,$3,2
	lw	$6,0($7)
	addu	$7,$3,$24
	addu	$3,$3,$25
	lw	$7,0($7)
	beq	$6,$0,$L3
	lwc1	$f0,0($3)

	lw	$17,0($10)
	subu	$6,$0,$7
	neg.s	$f0,$f0
	lw	$3,0($9)
	addiu	$8,$8,4
	mult	$6,$17
	addiu	$3,$3,7
	nor	$18,$0,$3
	mfhi	$7
	mflo	$6
	mul.s	$f0,$f0,$f1
	sll	$17,$7,1
	srl	$6,$6,$3
	sll	$17,$17,$18
	sra	$7,$7,$3
	or	$6,$17,$6
	andi	$3,$3,0x20
	movn	$6,$7,$3
	addu	$3,$12,$2
	addiu	$2,$2,4
	sw	$6,-4($8)
	beq	$2,$11,$L13
	swc1	$f0,0($3)

$L6:
	addu	$3,$16,$2
	addu	$7,$13,$2
	lw	$6,0($3)
	bne	$6,$0,$L14
	addu	$3,$5,$6

	lw	$17,316($4)
	lw	$18,0($15)
	lwc1	$f0,0($14)
	addiu	$7,$17,-55
	addiu	$3,$17,-24
	andi	$7,$7,0x3f
	andi	$3,$3,0x3f
	sll	$7,$7,2
	sll	$3,$3,2
	addu	$7,$4,$7
	addu	$3,$4,$3
	andi	$19,$17,0x3f
	lw	$20,60($7)
	sll	$7,$19,2
	lw	$19,60($3)
	addiu	$17,$17,1
	addu	$3,$4,$7
	subu	$6,$0,$18
	addu	$7,$20,$19
	sw	$7,60($3)
	bltz	$7,$L5
	sw	$17,316($4)

	neg.s	$f0,$f0
	lw	$7,0($10)
	lw	$3,0($9)
	mult	$6,$7
$L10:
	mflo	$6
	mfhi	$7
	addiu	$3,$3,13
	nor	$18,$0,$3
$L15:
	sll	$17,$7,1
	mul.s	$f0,$f0,$f1
	srl	$6,$6,$3
	sll	$17,$17,$18
	sra	$7,$7,$3
	or	$6,$17,$6
	andi	$3,$3,0x20
	movn	$6,$7,$3
	addu	$3,$12,$2
	addiu	$2,$2,4
	addiu	$8,$8,4
	sw	$6,-4($8)
	bne	$2,$11,$L6
	swc1	$f0,0($3)

$L13:
	lw	$20,20($sp)
	lw	$19,16($sp)
	lw	$18,12($sp)
	lw	$17,8($sp)
	lw	$16,4($sp)
	j	$31
	addiu	$sp,$sp,24

$L3:
	lw	$6,0($10)
	lw	$3,0($9)
	mult	$7,$6
	addiu	$3,$3,7
	nor	$18,$0,$3
	mflo	$6
	.option	pic0
	j	$L15
	.option	pic2
	mfhi	$7

$L5:
	lw	$6,0($10)
	lw	$3,0($9)
	.option	pic0
	j	$L10
	.option	pic2
	mult	$18,$6

	.set	macro
	.set	reorder
	.end	scalar_dequant_float
	.size	scalar_dequant_float, .-scalar_dequant_float
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	interpolate_float
	.type	interpolate_float, @function
interpolate_float:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$2,$6,63
	lui	$3,%hi(pow2tab)
	sll	$2,$2,2
	addiu	$3,$3,%lo(pow2tab)
	addu	$2,$2,$3
	beq	$6,$7,$L24
	lwc1	$f1,0($2)

	subu	$7,$7,$6
	lw	$2,708($4)
	addiu	$7,$7,189
	sll	$7,$7,2
	addu	$4,$4,$7
	blez	$2,$L26
	lwc1	$f2,0($4)

	sll	$2,$2,2
	addu	$2,$5,$2
$L21:
	lwc1	$f0,0($5)
	addiu	$5,$5,4
	mul.s	$f0,$f0,$f1
	mul.s	$f1,$f1,$f2
	bne	$5,$2,$L21
	swc1	$f0,-4($5)

$L26:
	j	$31
	nop

$L24:
	lw	$2,708($4)
	blez	$2,$L26
	sll	$2,$2,2

	addu	$2,$5,$2
$L19:
	lwc1	$f0,0($5)
	addiu	$5,$5,4
	mul.s	$f0,$f0,$f1
	bne	$5,$2,$L19
	swc1	$f0,-4($5)

	j	$31
	nop

	.set	macro
	.set	reorder
	.end	interpolate_float
	.size	interpolate_float, .-interpolate_float
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	imlt_window_float
	.type	imlt_window_float, @function
imlt_window_float:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lw	$2,4($6)
	lui	$3,%hi(pow2tab)
	lw	$8,56($4)
	addiu	$3,$3,%lo(pow2tab)
	lw	$2,0($2)
	addiu	$2,$2,63
	sll	$2,$2,2
	addu	$2,$2,$3
	blez	$8,$L32
	lwc1	$f4,0($2)

	lw	$9,384($4)
	sll	$8,$8,2
	move	$2,$0
	addu	$3,$9,$8
$L29:
	addu	$6,$9,$2
	lwc1	$f1,0($5)
	addu	$4,$7,$2
	lwc1	$f3,0($6)
	addiu	$5,$5,4
	addiu	$2,$2,4
	lwc1	$f2,-4($3)
	addiu	$3,$3,-4
	lwc1	$f0,0($4)
	mul.s	$f1,$f1,$f3
	mul.s	$f0,$f2,$f0
	mul.s	$f1,$f1,$f4
	sub.s	$f0,$f1,$f0
	bne	$8,$2,$L29
	swc1	$f0,-4($5)

$L32:
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	imlt_window_float
	.size	imlt_window_float, .-imlt_window_float
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	decouple_float
	.type	decouple_float, @function
decouple_float:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lw	$3,24($5)
	sll	$2,$6,2
	mtc1	$7,$f2
	sll	$4,$6,4
	lwc1	$f1,16($sp)
	lw	$10,24($sp)
	li	$8,80			# 0x50
	addu	$3,$6,$3
	lw	$9,28($sp)
	addu	$6,$2,$4
	sll	$2,$3,2
	sll	$3,$3,4
	sll	$6,$6,2
	addu	$3,$2,$3
	lw	$2,20($sp)
	move	$4,$0
	sll	$3,$3,2
	addu	$3,$2,$3
$L34:
	lwc1	$f0,0($3)
	addu	$2,$4,$6
	addiu	$3,$3,4
	addu	$5,$10,$2
	addiu	$4,$4,4
	addu	$2,$9,$2
	mul.s	$f0,$f0,$f2
	swc1	$f0,0($5)
	lwc1	$f0,-4($3)
	mul.s	$f0,$f0,$f1
	bne	$4,$8,$L34
	swc1	$f0,0($2)

	j	$31
	nop

	.set	macro
	.set	reorder
	.end	decouple_float
	.size	decouple_float, .-decouple_float
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align	2
$LC0:
	.ascii	"Deallocating memory.\012\000"
	.align	2
$LC1:
	.ascii	"Memory deallocated.\012\000"
	.section	.text.unlikely,"ax",@progbits
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	cook_decode_close
	.type	cook_decode_close, @function
cook_decode_close:
	.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
	.mask	0x800f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	lui	$6,%hi($LC0)
	addiu	$28,$28,%lo(__gnu_local_gp)
	addiu	$sp,$sp,-48
	li	$5,48			# 0x30
	lw	$25,%call16(av_log)($28)
	addiu	$6,$6,%lo($LC0)
	sw	$31,44($sp)
	sw	$19,40($sp)
	move	$19,$4
	sw	$18,36($sp)
	sw	$17,32($sp)
	sw	$16,28($sp)
	.cprestore	16
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	lw	$18,136($4)

	lw	$28,16($sp)
	addiu	$16,$18,388
	lw	$4,384($18)
	lw	$25,%call16(av_free)($28)
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	addiu	$17,$18,596

	lui	$2,%hi(s32_mlt_window)
	lw	$28,16($sp)
	lw	$25,%call16(av_free)($28)
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,%lo(s32_mlt_window)($2)

	lw	$28,16($sp)
	lw	$25,%call16(av_free)($28)
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,804($18)

	lw	$28,16($sp)
	lw	$25,%call16(ff_mdct_end)($28)
	.reloc	1f,R_MIPS_JALR,ff_mdct_end
1:	jalr	$25
	addiu	$4,$18,320

	lw	$28,16($sp)
$L37:
	lw	$25,%call16(free_vlc)($28)
	move	$4,$16
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$16,$16,16

	bne	$16,$17,$L37
	lw	$28,16($sp)

	addiu	$16,$18,708
$L38:
	lw	$25,%call16(free_vlc)($28)
	move	$4,$17
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$17,$17,16

	bne	$17,$16,$L38
	lw	$28,16($sp)

	addiu	$17,$18,29700
	move	$16,$0
$L39:
	lw	$2,29652($18)
	move	$4,$17
	lw	$25,%call16(free_vlc)($28)
	addiu	$17,$17,8432
	slt	$2,$16,$2
	beq	$2,$0,$L44
	addiu	$16,$16,1

	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	nop

	.option	pic0
	j	$L39
	.option	pic2
	lw	$28,16($sp)

$L44:
	lui	$6,%hi($LC1)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC1)
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$19

	move	$2,$0
	lw	$31,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	j	$31
	addiu	$sp,$sp,48

	.set	macro
	.set	reorder
	.end	cook_decode_close
	.size	cook_decode_close, .-cook_decode_close
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	saturate_output_float
	.type	saturate_output_float, @function
saturate_output_float:
	.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
	.mask	0x803f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-56
	lui	$28,%hi(__gnu_local_gp)
	sw	$17,32($sp)
	sw	$16,28($sp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	lw	$17,56($4)
	sw	$31,52($sp)
	sw	$21,48($sp)
	addiu	$16,$17,204
	sw	$20,44($sp)
	sw	$19,40($sp)
	sll	$16,$16,2
	sw	$18,36($sp)
	.cprestore	16
	blez	$17,$L45
	addu	$16,$4,$16

	lw	$18,40($4)
	sll	$21,$5,1
	sll	$17,$17,2
	addu	$21,$6,$21
	sll	$18,$18,1
	addu	$17,$16,$17
	li	$20,32768			# 0x8000
	li	$19,-65536			# 0xffffffffffff0000
$L47:
	lwc1	$f12,0($16)
	lw	$25,%call16(lrintf)($28)
	.reloc	1f,R_MIPS_JALR,lrintf
1:	jalr	$25
	addiu	$16,$16,4

	sra	$4,$2,31
	lw	$28,16($sp)
	addu	$3,$2,$20
	xori	$4,$4,0x7fff
	and	$3,$3,$19
	movn	$2,$4,$3
	sh	$2,0($21)
	bne	$16,$17,$L47
	addu	$21,$21,$18

$L45:
	lw	$31,52($sp)
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	j	$31
	addiu	$sp,$sp,56

	.set	macro
	.set	reorder
	.end	saturate_output_float
	.size	saturate_output_float, .-saturate_output_float
	.section	.rodata.str1.4
	.align	2
$LC2:
	.ascii	"Necessary extradata missing!\012\000"
	.align	2
$LC3:
	.ascii	"codecdata_length=%d\012\000"
	.align	2
$LC4:
	.ascii	"subpacket[%i].cookversion=%x\012\000"
	.align	2
$LC5:
	.ascii	"Container channels != 1, report sample!\012\000"
	.align	2
$LC6:
	.ascii	"MONO\012\000"
	.align	2
$LC7:
	.ascii	"STEREO\012\000"
	.align	2
$LC8:
	.ascii	"Container channels != 2, report sample!\012\000"
	.align	2
$LC9:
	.ascii	"JOINT_STEREO\012\000"
	.align	2
$LC10:
	.ascii	"MULTI_CHANNEL\012\000"
	.align	2
$LC11:
	.ascii	"Unknown Cook version, report sample!\012\000"
	.align	2
$LC12:
	.ascii	"different number of samples per channel!\012\000"
	.align	2
$LC13:
	.ascii	"total_subbands > 53, report sample!\012\000"
	.align	2
$LC14:
	.ascii	"js_vlc_bits = %d, only >= 0 and <= 6 allowed!\012\000"
	.align	2
$LC15:
	.ascii	"subbands > 50, report sample!\012\000"
	.align	2
$LC16:
	.ascii	"Too many subpackets > 5, report file!\012\000"
	.align	2
$LC23:
	.ascii	"sqvh VLC init\012\000"
	.align	2
$LC24:
	.ascii	"subpacket %i Joint-stereo VLC used.\012\000"
	.align	2
$LC25:
	.ascii	"VLC tables initialized.\012\000"
	.align	2
$LC26:
	.ascii	"MDCT initialized, order = %d.\012\000"
	.align	2
$LC27:
	.ascii	"unknown amount of samples_per_channel = %d, report sampl"
	.ascii	"e!\012\000"
	.section	.text.unlikely
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	cook_decode_init
	.type	cook_decode_init, @function
cook_decode_init:
	.frame	$sp,144,$31		# vars= 16, regs= 10/6, args= 56, gp= 8
	.mask	0xc0ff0000,-28
	.fmask	0x03f00000,-8
	addiu	$sp,$sp,-144
	lui	$28,%hi(__gnu_local_gp)
	sw	$fp,112($sp)
	li	$2,3			# 0x3
	sw	$23,108($sp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$19,92($sp)
	sw	$21,100($sp)
	move	$21,$4
	sw	$18,88($sp)
	sw	$31,116($sp)
	sw	$22,104($sp)
	sw	$20,96($sp)
	sw	$17,84($sp)
	sw	$16,80($sp)
	.cprestore	56
	lw	$fp,24($4)
	lw	$19,28($4)
	lw	$23,136($4)
	sdc1	$f24,136($sp)
	addu	$18,$fp,$19
	sdc1	$f22,128($sp)
	sdc1	$f20,120($sp)
	sw	$4,20($23)
#APP
 # 2116 "cook.c" 1
	S32I2M xr16,$2
 # 0 "" 2
#NO_APP
	lw	$2,%got(mpFrame)($28)
	lw	$25,%call16(av_log)($28)
	.set	noreorder
	.set	nomacro
	bgtz	$19,$L52
	sw	$0,0($2)
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
	lui	$6,%hi($LC2)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC2)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,68($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	lw	$2,68($sp)
	.set	macro
	.set	reorder

$L52:
	lui	$6,%hi($LC3)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC3)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$19
	.set	macro
	.set	reorder

	addiu	$4,$23,60
	lw	$28,56($sp)
	move	$5,$0
	lw	$6,64($21)
	lui	$17,%hi($LC4)
	lw	$3,68($21)
	move	$22,$0
	lw	$2,4($21)
	addiu	$17,$17,%lo($LC4)
	lw	$25,%call16(av_lfg_init)($28)
	move	$16,$23
	sw	$0,64($sp)
	sw	$6,48($23)
	sw	$3,40($23)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_lfg_init
1:	jalr	$25
	sw	$2,44($23)
	.set	macro
	.set	reorder

	lw	$28,56($sp)
$L54:
	sltu	$2,$fp,$18
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L130
	slt	$2,$19,8
	.set	macro
	.set	reorder

	bne	$2,$0,$L55
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $2, 3($fp)  
	lwr $2, 0($fp)  
	
 # 0 "" 2
#NO_APP
	srl	$4,$2,8
	sll	$5,$2,8
	li	$2,16711680			# 0xff0000
	addiu	$fp,$fp,8
	addiu	$2,$2,255
	addiu	$19,$19,-8
	and	$3,$4,$2
	li	$2,-16777216			# 0xffffffffff000000
	ori	$2,$2,0xff00
	and	$2,$5,$2
	or	$2,$3,$2
	sll	$3,$2,16
	srl	$2,$2,16
	or	$2,$3,$2
	sw	$2,29668($16)
	lbu	$2,-3($fp)
	lbu	$3,-4($fp)
	sll	$2,$2,8
	or	$2,$2,$3
	sll	$3,$2,8
	srl	$2,$2,8
	or	$2,$3,$2
	andi	$2,$2,0xffff
	sw	$2,29672($16)
	lbu	$2,-1($fp)
	lbu	$3,-2($fp)
	sll	$2,$2,8
	or	$2,$2,$3
	sll	$3,$2,8
	srl	$2,$2,8
	or	$2,$3,$2
	andi	$2,$2,0xffff
	sw	$2,29676($16)
$L55:
	lw	$2,28($21)
	slt	$2,$2,8
	bne	$2,$0,$L56
	lbu	$2,5($fp)
	addiu	$fp,$fp,8
	lbu	$3,-4($fp)
	addiu	$19,$19,-8
	sll	$2,$2,8
	or	$2,$2,$3
	sll	$3,$2,8
	srl	$2,$2,8
	or	$2,$3,$2
	andi	$2,$2,0xffff
	sw	$2,29680($16)
	lbu	$2,-1($fp)
	lbu	$3,-2($fp)
	sll	$2,$2,8
	or	$2,$2,$3
	sll	$3,$2,8
	srl	$2,$2,8
	or	$2,$3,$2
	andi	$2,$2,0xffff
	sw	$2,29684($16)
$L56:
	lw	$3,40($23)
	li	$20,1			# 0x1
	lw	$2,29672($16)
	li	$5,48			# 0x30
	lw	$14,29676($16)
	move	$4,$21
	lw	$13,29668($16)
	move	$6,$17
	teq	$3,$0,7
	div	$0,$2,$3
	lw	$25,%call16(av_log)($28)
	li	$3,5			# 0x5
	move	$7,$22
	mflo	$2
	sw	$2,29688($16)
	lw	$2,268($21)
	sw	$3,29692($16)
	sw	$14,29728($16)
	sll	$2,$2,3
	sw	$20,29664($16)
	sw	$2,29720($16)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$13,16($sp)
	.set	macro
	.set	reorder

	li	$3,16777216			# 0x1000000
	lw	$28,56($sp)
	lw	$2,29668($16)
	addiu	$4,$3,2
	sw	$0,29716($16)
	.set	noreorder
	.set	nomacro
	beq	$2,$4,$L58
	lw	$25,%call16(av_log)($28)
	.set	macro
	.set	reorder

	addiu	$4,$3,3
	slt	$5,$2,$4
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L59
	addiu	$3,$3,1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$3,$L60
	lui	$6,%hi($LC11)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L137
	.option	pic2
	li	$5,16			# 0x10
	.set	macro
	.set	reorder

$L59:
	.set	noreorder
	.set	nomacro
	beq	$2,$4,$L61
	li	$3,33554432			# 0x2000000
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$3,$L62
	li	$5,48			# 0x30
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L57
	.option	pic2
	lui	$6,%hi($LC11)
	.set	macro
	.set	reorder

$L60:
	lw	$2,40($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$20,$L63
	li	$5,48			# 0x30
	.set	macro
	.set	reorder

	lui	$6,%hi($LC5)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC5)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L63:
	lui	$2,%hi($LC6)
	addiu	$6,$2,%lo($LC6)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L64
	.option	pic2
	lw	$28,56($sp)
	.set	macro
	.set	reorder

$L58:
	lw	$2,40($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$20,$L65
	li	$2,2			# 0x2
	.set	macro
	.set	reorder

	sw	$20,29724($16)
	sw	$2,29664($16)
$L65:
	lui	$6,%hi($LC7)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC7)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L64
	.option	pic2
	lw	$28,56($sp)
	.set	macro
	.set	reorder

$L61:
	lw	$3,40($23)
	li	$2,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L66
	li	$5,48			# 0x30
	.set	macro
	.set	reorder

	lui	$6,%hi($LC8)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC8)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L66:
	lui	$2,%hi($LC9)
	sw	$3,72($sp)
	addiu	$6,$2,%lo($LC9)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	lw	$2,28($21)
	lw	$28,56($sp)
	slt	$2,$2,16
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L67
	lw	$3,72($sp)
	.set	macro
	.set	reorder

	lw	$2,29676($16)
	lw	$4,29680($16)
	sw	$20,29716($16)
	sw	$3,29664($16)
	addu	$2,$2,$4
	sw	$2,29728($16)
$L67:
	lw	$2,29688($16)
	slt	$2,$2,257
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L72
	li	$2,6			# 0x6
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L72
	.option	pic2
	sw	$2,29692($16)
	.set	macro
	.set	reorder

$L62:
	lui	$6,%hi($LC10)
	addiu	$6,$6,%lo($LC10)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	slt	$2,$19,4
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L69
	lw	$28,56($sp)
	.set	macro
	.set	reorder

	li	$4,16711680			# 0xff0000
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($fp)  
	lwr $3, 0($fp)  
	
 # 0 "" 2
#NO_APP
	addiu	$4,$4,255
	srl	$2,$3,8
	sll	$3,$3,8
	and	$2,$2,$4
	li	$4,-16777216			# 0xffffffffff000000
	addiu	$fp,$fp,4
	ori	$4,$4,0xff00
	and	$3,$3,$4
	or	$2,$2,$3
	sll	$3,$2,16
	srl	$2,$2,16
	or	$2,$2,$3
	lw	$3,64($sp)
	sw	$2,29696($16)
	or	$3,$3,$2
	sw	$3,64($sp)
$L69:
	lw	$7,29696($16)
	move	$5,$0
	move	$2,$0
	li	$10,1			# 0x1
	li	$4,32			# 0x20
$L70:
	sll	$3,$10,$2
	addiu	$6,$5,1
	addiu	$2,$2,1
	and	$3,$3,$7
	.set	noreorder
	.set	nomacro
	bne	$2,$4,$L70
	movn	$5,$6,$3
	.set	macro
	.set	reorder

	slt	$5,$5,2
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L71
	li	$5,1			# 0x1
	.set	macro
	.set	reorder

	lw	$4,29680($16)
	lw	$3,29676($16)
	lw	$2,29672($16)
	sw	$5,29716($16)
	addu	$3,$3,$4
	li	$4,2			# 0x2
	sra	$2,$2,1
	sw	$3,29728($16)
	sw	$4,29664($16)
	slt	$4,$2,257
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L72
	sw	$2,29688($16)
	.set	macro
	.set	reorder

	li	$2,6			# 0x6
	sw	$2,29692($16)
$L72:
	lw	$2,29688($16)
	slt	$2,$2,513
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L135
	slt	$2,$22,2
	.set	macro
	.set	reorder

	li	$2,7			# 0x7
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L64
	.option	pic2
	sw	$2,29692($16)
	.set	macro
	.set	reorder

$L71:
	lw	$2,29672($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L64
	.option	pic2
	sw	$2,29688($16)
	.set	macro
	.set	reorder

$L57:
	li	$5,16			# 0x10
$L137:
	addiu	$6,$6,%lo($LC11)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L64:
	slt	$2,$22,2
$L135:
	bne	$2,$0,$L73
	lw	$3,29688($16)
	lw	$2,56($23)
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L73
	lui	$6,%hi($LC12)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC12)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L73:
	lw	$2,29688($23)
	li	$3,1			# 0x1
	sw	$2,56($23)
	lw	$4,29692($16)
	lw	$2,29728($16)
	sll	$3,$3,$4
	slt	$2,$2,54
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L74
	sw	$3,29732($16)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC13)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC13)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L74:
	lw	$7,29684($16)
	sltu	$2,$7,7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L75
	lui	$6,%hi($LC14)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC14)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L75:
	lw	$2,29676($16)
	slt	$2,$2,51
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L76
	li	$13,37944			# 0x9438
	.set	macro
	.set	reorder

	lui	$6,%hi($LC15)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC15)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L76:
	lw	$2,29652($23)
	li	$3,37928			# 0x9428
	addu	$13,$16,$13
	addu	$3,$16,$3
	li	$10,37980			# 0x945c
	li	$6,38016			# 0x9480
	sw	$13,0($3)
	li	$4,38052			# 0x94a4
	li	$7,37932			# 0x942c
	li	$5,37936			# 0x9430
	li	$3,37940			# 0x9434
	addu	$10,$16,$10
	addu	$6,$16,$6
	addu	$4,$16,$4
	addiu	$2,$2,1
	addu	$7,$16,$7
	addu	$5,$16,$5
	addu	$3,$16,$3
	sw	$10,0($7)
	addiu	$22,$22,1
	sw	$6,0($5)
	sw	$4,0($3)
	sw	$2,29652($23)
	li	$2,6			# 0x6
	.set	noreorder
	.set	nomacro
	bne	$22,$2,$L54
	addiu	$16,$16,8432
	.set	macro
	.set	reorder

	lui	$6,%hi($LC16)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC16)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L130:
	lui	$2,%hi($LC17)
	lui	$19,%hi(pow2tab)
	ldc1	$f22,%lo($LC17)($2)
	lui	$2,%hi($LC18)
	lui	$22,%hi(rootpow2tab)
	ldc1	$f24,%lo($LC18)($2)
	li	$16,-63			# 0xffffffffffffffc1
	addiu	$19,$19,%lo(pow2tab)
	addiu	$22,$22,%lo(rootpow2tab)
	li	$20,64			# 0x40
$L78:
	mtc1	$16,$f0
	lw	$25,%call16(pow)($28)
	sll	$17,$16,2
	addiu	$16,$16,1
	mov.d	$f12,$f22
	move	$18,$19
	cvt.d.w	$f20,$f0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,pow
1:	jalr	$25
	mov.d	$f14,$f20
	.set	macro
	.set	reorder

	mul.d	$f14,$f20,$f24
	addu	$2,$19,$17
	lw	$28,56($sp)
	addu	$17,$22,$17
	cvt.s.d	$f0,$f0
	lw	$25,%call16(pow)($28)
	mov.d	$f12,$f22
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,pow
1:	jalr	$25
	swc1	$f0,252($2)
	.set	macro
	.set	reorder

	move	$5,$22
	cvt.s.d	$f0,$f0
	lw	$28,56($sp)
	.set	noreorder
	.set	nomacro
	bne	$16,$20,$L78
	swc1	$f0,252($17)
	.set	macro
	.set	reorder

	lui	$13,%hi(s32_pow2tab)
	lui	$12,%hi(s32_rootpow2tab)
	lui	$4,%hi(pow2tab_sft)
	lui	$3,%hi(rootpow2tab_sft)
	move	$2,$0
	addiu	$13,$13,%lo(s32_pow2tab)
	addiu	$12,$12,%lo(s32_rootpow2tab)
	addiu	$4,$4,%lo(pow2tab_sft)
	addiu	$3,$3,%lo(rootpow2tab_sft)
	li	$14,88			# 0x58
$L79:
	addu	$6,$13,$2
	addu	$10,$12,$2
	addu	$7,$4,$2
	sw	$0,0($6)
	addu	$6,$3,$2
	sw	$0,0($10)
	addiu	$2,$2,4
	sw	$0,0($7)
	sw	$0,0($6)
	move	$7,$12
	move	$6,$13
	move	$10,$4
	.set	noreorder
	.set	nomacro
	bne	$2,$14,$L79
	move	$11,$3
	.set	macro
	.set	reorder

	lui	$3,%hi($LC20)
	lui	$2,%hi($LC19)
	lwc1	$f0,%lo($LC20)($3)
	li	$19,20			# 0x14
	li	$17,14			# 0xe
	lwc1	$f1,%lo($LC19)($2)
	li	$16,164			# 0xa4
	move	$2,$0
$L80:
	addu	$4,$18,$2
	addu	$3,$5,$2
	lwc1	$f3,88($4)
	addu	$13,$6,$2
	addu	$12,$7,$2
	lwc1	$f2,88($3)
	addu	$15,$10,$2
	addu	$14,$11,$2
	addiu	$2,$2,4
	sw	$19,88($15)
	sw	$17,88($14)
	mul.s	$f3,$f3,$f1
	mul.s	$f2,$f2,$f0
	trunc.w.s $f3,$f3
	trunc.w.s $f2,$f2
	swc1	$f3,88($13)
	.set	noreorder
	.set	nomacro
	bne	$2,$16,$L80
	swc1	$f2,88($12)
	.set	macro
	.set	reorder

	lui	$2,%hi($LC20)
	li	$17,14			# 0xe
	lwc1	$f0,%lo($LC20)($2)
	li	$16,136			# 0x88
	move	$2,$0
$L81:
	addu	$3,$5,$2
	addu	$4,$18,$2
	lwc1	$f1,252($3)
	addu	$12,$7,$2
	addu	$15,$11,$2
	lwc1	$f2,252($4)
	addu	$13,$6,$2
	addu	$14,$10,$2
	sw	$17,252($15)
	addiu	$2,$2,4
	sw	$0,252($14)
	mul.s	$f1,$f1,$f0
	trunc.w.s $f2,$f2
	trunc.w.s $f1,$f1
	swc1	$f2,252($13)
	.set	noreorder
	.set	nomacro
	bne	$2,$16,$L81
	swc1	$f1,252($12)
	.set	macro
	.set	reorder

	move	$2,$0
	li	$12,120			# 0x78
$L82:
	addu	$4,$5,$2
	addu	$3,$18,$2
	lwc1	$f1,388($4)
	addu	$14,$7,$2
	addu	$16,$11,$2
	lwc1	$f0,388($3)
	addu	$13,$6,$2
	addu	$15,$10,$2
	sw	$0,388($16)
	addiu	$2,$2,4
	sw	$0,388($15)
	trunc.w.s $f1,$f1
	trunc.w.s $f0,$f0
	swc1	$f1,388($14)
	.set	noreorder
	.set	nomacro
	bne	$2,$12,$L82
	swc1	$f0,388($13)
	.set	macro
	.set	reorder

	lw	$2,56($23)
	lui	$3,%hi($LC19)
	lui	$16,%hi($LC21)
	lwc1	$f22,%lo($LC19)($3)
	lui	$19,%hi(s32_gain_table_L20)
	addiu	$3,$2,7
	slt	$4,$2,0
	ldc1	$f0,%lo($LC21)($16)
	movn	$2,$3,$4
	lui	$3,%hi(s32_pow2tab+252)
	lui	$4,%hi(sp32_pow2tab)
	addiu	$3,$3,%lo(s32_pow2tab+252)
	move	$17,$0
	sra	$2,$2,3
	sw	$3,%lo(sp32_pow2tab)($4)
	lui	$3,%hi(s32_rootpow2tab+252)
	mtc1	$2,$f2
	lui	$4,%hi(sp32_rootpow2tab)
	addiu	$3,$3,%lo(s32_rootpow2tab+252)
	sw	$2,708($23)
	addiu	$19,$19,%lo(s32_gain_table_L20)
	li	$20,92			# 0x5c
	sw	$3,%lo(sp32_rootpow2tab)($4)
	cvt.d.w	$f20,$f2
	div.d	$f20,$f0,$f20
$L84:
	addu	$2,$18,$17
	lw	$25,%call16(pow)($28)
	lwc1	$f12,208($2)
	mov.d	$f14,$f20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,pow
1:	jalr	$25
	cvt.d.s	$f12,$f12
	.set	macro
	.set	reorder

	addu	$4,$23,$17
	cvt.s.d	$f0,$f0
	addu	$3,$19,$17
	lw	$28,56($sp)
	addiu	$17,$17,4
	mul.s	$f1,$f0,$f22
	swc1	$f0,712($4)
	trunc.w.s $f1,$f1
	.set	noreorder
	.set	nomacro
	bne	$17,$20,$L84
	swc1	$f1,0($3)
	.set	macro
	.set	reorder

	lui	$3,%hi(cplscale2)
	lui	$7,%hi(s32_cplscale2)
	addiu	$3,$3,%lo(cplscale2)
	addiu	$11,$7,%lo(s32_cplscale2)
	lui	$2,%hi($LC22)
	sw	$3,29632($23)
	li	$3,1023279104			# 0x3cfe0000
	lwc1	$f0,%lo($LC22)($2)
	lui	$6,%hi(cplscale4)
	addiu	$3,$3,19008
	lui	$2,%hi(cplscale3)
	lui	$5,%hi(cplscale5)
	sw	$3,%lo(s32_cplscale2)($7)
	li	$7,759234560			# 0x2d410000
	lui	$4,%hi(cplscale6)
	addiu	$7,$7,15552
	addiu	$6,$6,%lo(cplscale4)
	addiu	$5,$5,%lo(cplscale5)
	sw	$7,4($11)
	li	$7,325189632			# 0x13620000
	addiu	$2,$2,%lo(cplscale3)
	sw	$6,29640($23)
	ori	$7,$7,0xcd80
	sw	$5,29644($23)
	addiu	$4,$4,%lo(cplscale6)
	lui	$10,%hi(s32_cplscale3)
	sw	$7,8($11)
	move	$3,$0
	sw	$2,29636($23)
	addiu	$10,$10,%lo(s32_cplscale3)
	sw	$4,29648($23)
	li	$11,28			# 0x1c
	move	$7,$6
	move	$6,$5
$L85:
	addu	$5,$2,$3
	addu	$12,$10,$3
	lwc1	$f1,0($5)
	addiu	$3,$3,4
	lui	$17,%hi($LC22)
	mul.s	$f1,$f1,$f0
	trunc.w.s $f1,$f1
	.set	noreorder
	.set	nomacro
	bne	$3,$11,$L85
	swc1	$f1,0($12)
	.set	macro
	.set	reorder

	lwc1	$f0,%lo($LC22)($17)
	lui	$5,%hi(s32_cplscale4)
	move	$2,$0
	addiu	$5,$5,%lo(s32_cplscale4)
	li	$10,60			# 0x3c
$L86:
	addu	$3,$7,$2
	addu	$11,$5,$2
	lwc1	$f1,0($3)
	addiu	$2,$2,4
	mul.s	$f1,$f1,$f0
	trunc.w.s $f1,$f1
	.set	noreorder
	.set	nomacro
	bne	$2,$10,$L86
	swc1	$f1,0($11)
	.set	macro
	.set	reorder

	lwc1	$f0,%lo($LC22)($17)
	lui	$5,%hi(s32_cplscale5)
	move	$2,$0
	addiu	$5,$5,%lo(s32_cplscale5)
	li	$7,124			# 0x7c
$L87:
	addu	$3,$6,$2
	addu	$10,$5,$2
	lwc1	$f1,0($3)
	addiu	$2,$2,4
	mul.s	$f1,$f1,$f0
	trunc.w.s $f1,$f1
	.set	noreorder
	.set	nomacro
	bne	$2,$7,$L87
	swc1	$f1,0($10)
	.set	macro
	.set	reorder

	lwc1	$f0,%lo($LC22)($17)
	lui	$5,%hi(s32_cplscale6)
	move	$3,$0
	addiu	$5,$5,%lo(s32_cplscale6)
	li	$7,252			# 0xfc
$L88:
	addu	$2,$4,$3
	addu	$6,$5,$3
	lwc1	$f1,0($2)
	addiu	$3,$3,4
	mul.s	$f1,$f1,$f0
	trunc.w.s $f1,$f1
	.set	noreorder
	.set	nomacro
	bne	$3,$7,$L88
	swc1	$f1,0($6)
	.set	macro
	.set	reorder

	lui	$22,%hi(envelope_quant_index_huffbits)
	lui	$20,%hi(envelope_quant_index_huffcodes)
	addiu	$3,$23,388
	move	$18,$0
	move	$19,$0
	li	$10,24			# 0x18
	addiu	$22,$22,%lo(envelope_quant_index_huffbits)
	li	$fp,1			# 0x1
	li	$16,48			# 0x30
	addiu	$20,$20,%lo(envelope_quant_index_huffcodes)
$L89:
	mul	$2,$19,$16
	lw	$25,%call16(init_vlc_sparse)($28)
	mul	$5,$19,$10
	sw	$10,68($sp)
	li	$6,24			# 0x18
	sw	$fp,16($sp)
	sw	$fp,20($sp)
	addiu	$19,$19,1
	sw	$0,36($sp)
	sw	$0,40($sp)
	sw	$0,44($sp)
	addu	$4,$2,$20
	sw	$0,48($sp)
	li	$2,2			# 0x2
	addu	$7,$5,$22
	sw	$4,24($sp)
	move	$4,$3
	addiu	$3,$3,16
	sw	$2,28($sp)
	li	$5,9			# 0x9
	sw	$2,32($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
	sw	$3,72($sp)
	.set	macro
	.set	reorder

	or	$18,$18,$2
	lw	$28,56($sp)
	li	$2,13			# 0xd
	lw	$3,72($sp)
	.set	noreorder
	.set	nomacro
	bne	$19,$2,$L89
	lw	$10,68($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC23)
	lw	$25,%call16(av_log)($28)
	lw	$4,20($23)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC23)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	lui	$fp,%hi(vhvlcsize_tab)
	.set	macro
	.set	reorder

	lui	$22,%hi(cvh_huffbits)
	lui	$2,%hi(vhsize_tab)
	lw	$28,56($sp)
	lui	$20,%hi(cvh_huffcodes)
	addiu	$16,$23,596
	move	$19,$0
	addiu	$fp,$fp,%lo(vhvlcsize_tab)
	addiu	$3,$2,%lo(vhsize_tab)
	addiu	$22,$22,%lo(cvh_huffbits)
	li	$10,1			# 0x1
	addiu	$20,$20,%lo(cvh_huffcodes)
$L90:
	li	$2,2			# 0x2
	lw	$25,%call16(init_vlc_sparse)($28)
	addu	$11,$20,$19
	sw	$10,16($sp)
	sw	$10,20($sp)
	addu	$6,$3,$19
	sw	$0,36($sp)
	addu	$5,$fp,$19
	sw	$0,40($sp)
	addu	$7,$22,$19
	sw	$0,44($sp)
	move	$4,$16
	sw	$0,48($sp)
	addiu	$19,$19,4
	sw	$2,28($sp)
	addiu	$16,$16,16
	sw	$2,32($sp)
	lw	$2,0($11)
	lw	$5,0($5)
	lw	$6,0($6)
	lw	$7,0($7)
	sw	$3,72($sp)
	sw	$10,68($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
	sw	$2,24($sp)
	.set	macro
	.set	reorder

	or	$18,$18,$2
	lw	$28,56($sp)
	li	$2,28			# 0x1c
	lw	$3,72($sp)
	.set	noreorder
	.set	nomacro
	bne	$19,$2,$L90
	lw	$10,68($sp)
	.set	macro
	.set	reorder

	lui	$fp,%hi(ccpl_huffbits)
	lui	$2,%hi(ccpl_huffcodes)
	addiu	$20,$23,29700
	move	$22,$0
	addiu	$fp,$fp,%lo(ccpl_huffbits)
	addiu	$16,$2,%lo(ccpl_huffcodes)
	lui	$19,%hi($LC24)
$L91:
	lw	$2,29652($23)
	slt	$2,$22,$2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L131
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	lw	$3,16($20)
	bne	$3,$2,$L92
	lw	$7,-16($20)
	li	$6,2			# 0x2
	sw	$3,16($sp)
	li	$5,6			# 0x6
	sw	$3,20($sp)
	move	$4,$20
	addiu	$2,$7,-2
	sw	$6,28($sp)
	sll	$3,$3,$7
	sw	$6,32($sp)
	sll	$2,$2,2
	sw	$0,36($sp)
	addiu	$6,$3,-1
	sw	$0,40($sp)
	addu	$3,$2,$16
	sw	$0,44($sp)
	addu	$2,$2,$fp
	sw	$0,48($sp)
	lw	$25,%call16(init_vlc_sparse)($28)
	lw	$3,0($3)
	lw	$7,0($2)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
	sw	$3,24($sp)
	.set	macro
	.set	reorder

	li	$5,48			# 0x30
	lw	$28,56($sp)
	addiu	$6,$19,%lo($LC24)
	lw	$4,20($23)
	move	$7,$22
	lw	$25,%call16(av_log)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	or	$18,$18,$2
	.set	macro
	.set	reorder

	lw	$28,56($sp)
$L92:
	addiu	$22,$22,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L91
	.option	pic2
	addiu	$20,$20,8432
	.set	macro
	.set	reorder

$L131:
	lui	$6,%hi($LC25)
	lw	$25,%call16(av_log)($28)
	lw	$4,20($23)
	li	$5,48			# 0x30
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC25)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$18,$0,$L96
	lw	$28,56($sp)
	.set	macro
	.set	reorder

	lw	$2,268($21)
	li	$3,2147418112			# 0x7fff0000
	ori	$3,$3,0xffff
	sltu	$3,$2,$3
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L96
	addiu	$3,$2,3
	.set	macro
	.set	reorder

	lw	$25,%call16(av_mallocz)($28)
	nor	$3,$0,$3
	andi	$3,$3,0x3
	addu	$4,$2,$3
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
	addiu	$4,$4,8
	.set	macro
	.set	reorder

	lw	$28,56($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L96
	sw	$2,804($23)
	.set	macro
	.set	reorder

	lw	$19,56($23)
	lw	$25,%call16(av_malloc)($28)
	sll	$18,$19,2
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	move	$4,$18
	.set	macro
	.set	reorder

	lw	$28,56($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L96
	sw	$2,384($23)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_malloc)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	move	$4,$18
	.set	macro
	.set	reorder

	lui	$3,%hi(s32_mlt_window)
	lw	$28,56($sp)
	move	$18,$3
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L97
	sw	$2,%lo(s32_mlt_window)($3)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_free)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,384($23)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L97:
	lw	$25,%call16(ff_sine_window_init)($28)
	move	$5,$19
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,ff_sine_window_init
1:	jalr	$25
	lw	$4,384($23)
	.set	macro
	.set	reorder

	lui	$3,%hi($LC17)
	lwc1	$f4,%lo($LC22)($17)
	move	$2,$0
	lw	$28,56($sp)
	ldc1	$f6,%lo($LC17)($3)
	lw	$5,%lo(s32_mlt_window)($18)
$L98:
	sll	$7,$2,2
	slt	$4,$2,$19
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L132
	addu	$6,$5,$7
	.set	macro
	.set	reorder

	lwc1	$f2,56($23)
	addiu	$2,$2,1
	lw	$3,384($23)
	addu	$3,$3,$7
	cvt.d.w	$f0,$f2
	lwc1	$f2,0($3)
	div.d	$f0,$f6,$f0
	cvt.d.s	$f2,$f2
	sqrt.d	$f0,$f0
	mul.d	$f0,$f0,$f2
	cvt.s.d	$f0,$f0
	mul.s	$f1,$f0,$f4
	swc1	$f0,0($3)
	trunc.w.s $f1,$f1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L98
	.option	pic2
	swc1	$f1,0($6)
	.set	macro
	.set	reorder

$L132:
	lui	$2,%hi($LC21)
	lw	$25,%call16(cook_mdct_init)($28)
	li	$16,32			# 0x20
	ldc1	$f0,%lo($LC21)($2)
	ori	$2,$19,0x1
	clz	$2,$2
	addiu	$4,$23,320
	subu	$16,$16,$2
	li	$6,1			# 0x1
	move	$5,$16
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,cook_mdct_init
1:	jalr	$25
	sdc1	$f0,16($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L100
	lw	$28,56($sp)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_free)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,384($23)
	.set	macro
	.set	reorder

	lw	$28,56($sp)
	lw	$25,%call16(av_free)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,%lo(s32_mlt_window)($18)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L100:
	lui	$6,%hi($LC26)
	lw	$25,%call16(av_log)($28)
	lw	$4,20($23)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC26)
	sw	$2,68($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$16
	.set	macro
	.set	reorder

	lui	$3,%hi(scalar_dequant_float)
	lw	$28,56($sp)
	addiu	$3,$3,%lo(scalar_dequant_float)
	lw	$7,56($23)
	lw	$2,68($sp)
	sw	$3,0($23)
	lui	$3,%hi(decouple_float)
	addiu	$3,$3,%lo(decouple_float)
	sw	$3,4($23)
	lui	$3,%hi(imlt_window_float)
	addiu	$3,$3,%lo(imlt_window_float)
	sw	$3,8($23)
	lui	$3,%hi(interpolate_float)
	addiu	$3,$3,%lo(interpolate_float)
	sw	$3,12($23)
	lui	$3,%hi(saturate_output_float)
	addiu	$3,$3,%lo(saturate_output_float)
	sw	$3,16($23)
	li	$3,256			# 0x100
	.set	noreorder
	.set	nomacro
	beq	$7,$3,$L101
	li	$3,512			# 0x200
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$7,$3,$L101
	li	$3,1024			# 0x400
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$7,$3,$L136
	li	$3,1			# 0x1
	.set	macro
	.set	reorder

	lui	$6,%hi($LC27)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC27)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L101:
	li	$3,1			# 0x1
$L136:
	sw	$3,72($21)
	lw	$3,64($sp)
	beq	$3,$0,$L102
	sw	$3,848($21)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	sw	$0,852($21)
	.set	macro
	.set	reorder

$L102:
	lw	$4,68($21)
	li	$3,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$4,$3,$L104
	li	$4,4			# 0x4
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L103
	.option	pic2
	move	$5,$0
	.set	macro
	.set	reorder

$L104:
	li	$4,3			# 0x3
	move	$5,$0
$L103:
	sw	$4,848($21)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L125
	.option	pic2
	sw	$5,852($21)
	.set	macro
	.set	reorder

$L96:
	li	$2,-1			# 0xffffffffffffffff
$L125:
	lw	$31,116($sp)
	lw	$fp,112($sp)
	lw	$23,108($sp)
	lw	$22,104($sp)
	lw	$21,100($sp)
	lw	$20,96($sp)
	lw	$19,92($sp)
	lw	$18,88($sp)
	lw	$17,84($sp)
	lw	$16,80($sp)
	ldc1	$f24,136($sp)
	ldc1	$f22,128($sp)
	ldc1	$f20,120($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,144
	.set	macro
	.set	reorder

	.end	cook_decode_init
	.size	cook_decode_init, .-cook_decode_init
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	s32_imlt_gain.isra.12
	.type	s32_imlt_gain.isra.12, @function
s32_imlt_gain.isra.12:
	.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
	.mask	0x80ff0000,-4
	.fmask	0x00000000,0
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-64
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$18,36($sp)
	addiu	$18,$4,9008
	move	$2,$5
	sw	$20,44($sp)
	sw	$17,32($sp)
	move	$20,$6
	sw	$16,28($sp)
	move	$16,$4
	sw	$21,48($sp)
	addiu	$4,$4,320
	sw	$19,40($sp)
	move	$5,$18
	.cprestore	16
	move	$6,$2
	sw	$31,60($sp)
	move	$19,$7
	sw	$23,56($sp)
	sw	$22,52($sp)
	lw	$25,%call16(aac_imdct_calc_c)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,aac_imdct_calc_c
1:	jalr	$25
	lw	$17,-264($4)
	.set	macro
	.set	reorder

	lui	$4,%hi(pow2tab_sft)
	lw	$2,4($20)
	lui	$5,%hi(s32_pow2tab)
	addiu	$4,$4,%lo(pow2tab_sft)
	lw	$28,16($sp)
	addiu	$5,$5,%lo(s32_pow2tab)
	addiu	$17,$17,2252
	lw	$2,0($2)
	sll	$17,$17,2
	addiu	$2,$2,63
	addu	$17,$16,$17
	sll	$2,$2,2
	addu	$3,$4,$2
	addu	$2,$5,$2
	lw	$21,0($3)
	lw	$8,0($2)
	.set	noreorder
	.set	nomacro
	bne	$21,$0,$L139
	lw	$2,56($16)
	.set	macro
	.set	reorder

	addiu	$3,$2,-3
	.set	noreorder
	.set	nomacro
	blez	$3,$L141
	move	$7,$0
	.set	macro
	.set	reorder

	lui	$6,%hi(s32_mlt_window)
	li	$14,3			# 0x3
	li	$13,31			# 0x1f
	lw	$12,%lo(s32_mlt_window)($6)
	move	$11,$19
	move	$6,$17
	move	$10,$12
$L148:
	lw	$9,0($6)
#APP
 # 1314 "cook.c" 1
	S32MUL xr1,xr2,$9,$8
 # 0 "" 2
#NO_APP
	lw	$9,4($6)
#APP
 # 1315 "cook.c" 1
	S32MUL xr3,xr4,$9,$8
 # 0 "" 2
#NO_APP
	lw	$9,8($6)
#APP
 # 1316 "cook.c" 1
	S32MUL xr5,xr6,$9,$8
 # 0 "" 2
#NO_APP
	lw	$9,12($6)
#APP
 # 1317 "cook.c" 1
	S32MUL xr7,xr8,$9,$8
 # 0 "" 2
 # 1319 "cook.c" 1
	S32M2I xr2, $24
 # 0 "" 2
 # 1320 "cook.c" 1
	S32M2I xr4, $21
 # 0 "" 2
 # 1321 "cook.c" 1
	S32M2I xr6, $15
 # 0 "" 2
 # 1322 "cook.c" 1
	S32M2I xr8, $9
 # 0 "" 2
#NO_APP
	lw	$22,0($10)
#APP
 # 1324 "cook.c" 1
	S32MUL xr1,xr2,$22,$24
 # 0 "" 2
#NO_APP
	lw	$22,4($10)
#APP
 # 1325 "cook.c" 1
	S32MUL xr3,xr4,$22,$21
 # 0 "" 2
#NO_APP
	lw	$21,8($10)
#APP
 # 1326 "cook.c" 1
	S32MUL xr5,xr6,$21,$15
 # 0 "" 2
#NO_APP
	lw	$15,12($10)
#APP
 # 1327 "cook.c" 1
	S32MUL xr7,xr8,$15,$9
 # 0 "" 2
#NO_APP
	addiu	$9,$2,-1
	lw	$15,0($11)
	subu	$9,$9,$7
	sll	$9,$9,2
	addu	$9,$12,$9
	lw	$9,0($9)
#APP
 # 1329 "cook.c" 1
	S32MSUB xr1,xr2,$15,$9
 # 0 "" 2
#NO_APP
	addiu	$9,$2,-2
	lw	$15,4($11)
	subu	$9,$9,$7
	sll	$9,$9,2
	addu	$9,$12,$9
	lw	$9,0($9)
#APP
 # 1330 "cook.c" 1
	S32MSUB xr3,xr4,$15,$9
 # 0 "" 2
#NO_APP
	subu	$3,$3,$7
	lw	$9,8($11)
	sll	$3,$3,2
	addu	$3,$12,$3
	lw	$3,0($3)
#APP
 # 1331 "cook.c" 1
	S32MSUB xr5,xr6,$9,$3
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-4
	lw	$3,12($11)
	subu	$2,$2,$7
	sll	$2,$2,2
	addu	$2,$12,$2
	lw	$2,0($2)
#APP
 # 1332 "cook.c" 1
	S32MSUB xr7,xr8,$3,$2
 # 0 "" 2
 # 1334 "cook.c" 1
	S32EXTRV xr1,xr2,$14,$13
 # 0 "" 2
 # 1335 "cook.c" 1
	S32EXTRV xr3,xr4,$14,$13
 # 0 "" 2
 # 1336 "cook.c" 1
	S32EXTRV xr5,xr6,$14,$13
 # 0 "" 2
 # 1337 "cook.c" 1
	S32EXTRV xr7,xr8,$14,$13
 # 0 "" 2
 # 1339 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1340 "cook.c" 1
	D32SLL xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1341 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1342 "cook.c" 1
	D32SAR xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1344 "cook.c" 1
	S32M2I xr1, $2
 # 0 "" 2
#NO_APP
	sw	$2,0($6)
#APP
 # 1345 "cook.c" 1
	S32M2I xr3, $2
 # 0 "" 2
#NO_APP
	sw	$2,4($6)
#APP
 # 1346 "cook.c" 1
	S32M2I xr5, $2
 # 0 "" 2
#NO_APP
	sw	$2,8($6)
#APP
 # 1347 "cook.c" 1
	S32M2I xr7, $2
 # 0 "" 2
#NO_APP
	sw	$2,12($6)
	addiu	$7,$7,4
	lw	$2,56($16)
	addiu	$6,$6,16
	addiu	$10,$10,16
	addiu	$3,$2,-3
	slt	$9,$7,$3
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L148
	addiu	$11,$11,16
	.set	macro
	.set	reorder

$L141:
	slt	$3,$7,$2
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L147
	lui	$3,%hi(s32_mlt_window)
	.set	macro
	.set	reorder

	sll	$14,$7,2
	move	$10,$0
	lw	$13,%lo(s32_mlt_window)($3)
	addu	$9,$17,$14
	li	$12,3			# 0x3
	li	$11,31			# 0x1f
$L149:
	lw	$3,0($9)
#APP
 # 224 "cook.c" 1
	S32MUL xr1,xr2,$3,$8
 # 0 "" 2
 # 225 "cook.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addu	$6,$10,$14
	addu	$15,$13,$6
	lw	$15,0($15)
#APP
 # 241 "cook.c" 1
	S32MUL xr1,xr2,$3,$15
 # 0 "" 2
 # 242 "cook.c" 1
	S32EXTRV xr1,xr2,$12,$11
 # 0 "" 2
 # 243 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 244 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 245 "cook.c" 1
	S32M2I xr1, $3
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-1
	addu	$6,$19,$6
	subu	$2,$2,$7
	sll	$2,$2,2
	lw	$6,0($6)
	addu	$2,$13,$2
	lw	$2,0($2)
#APP
 # 241 "cook.c" 1
	S32MUL xr1,xr2,$6,$2
 # 0 "" 2
 # 242 "cook.c" 1
	S32EXTRV xr1,xr2,$12,$11
 # 0 "" 2
 # 243 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 244 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 245 "cook.c" 1
	S32M2I xr1, $6
 # 0 "" 2
#NO_APP
	subu	$2,$3,$6
	addiu	$9,$9,4
	addiu	$7,$7,1
	sw	$2,-4($9)
	lw	$2,56($16)
	slt	$3,$7,$2
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L149
	addiu	$10,$10,4
	.set	macro
	.set	reorder

$L147:
	lw	$11,0($20)
$L170:
	lui	$24,%hi(s32_gain_table_L20)
	move	$14,$0
	addiu	$24,$24,%lo(s32_gain_table_L20)
	addiu	$11,$11,4
	li	$15,8			# 0x8
$L145:
	lw	$7,-4($11)
	.set	noreorder
	.set	nomacro
	bne	$7,$0,$L150
	addiu	$6,$7,63
	.set	macro
	.set	reorder

	lw	$3,0($11)
	beq	$3,$0,$L154
	lw	$8,708($16)
	lw	$6,252($5)
	lw	$10,252($4)
	mul	$2,$14,$8
	sll	$2,$2,2
	addu	$2,$17,$2
$L153:
	subu	$3,$3,$7
	addiu	$3,$3,11
	sll	$3,$3,2
	addu	$3,$3,$24
	.set	noreorder
	.set	nomacro
	blez	$8,$L154
	lw	$13,0($3)
	.set	macro
	.set	reorder

	move	$12,$0
	andi	$20,$10,0x20
	nor	$25,$0,$10
$L156:
	mult	$6,$13
	lw	$3,0($2)
	addiu	$2,$2,4
	addiu	$12,$12,1
	mfhi	$9
	mflo	$8
	mult	$3,$6
	mfhi	$7
	mflo	$6
	sll	$21,$9,12
	srl	$8,$8,20
	sll	$9,$7,1
	srl	$3,$6,$10
	sll	$9,$9,$25
	sra	$6,$7,$10
	or	$3,$9,$3
	movn	$3,$6,$20
	sw	$3,-4($2)
	lw	$3,708($16)
	slt	$3,$12,$3
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L156
	or	$6,$21,$8
	.set	macro
	.set	reorder

$L154:
	addiu	$14,$14,1
	.set	noreorder
	.set	nomacro
	bne	$14,$15,$L145
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	lw	$6,56($16)
$L169:
	move	$4,$19
	lw	$31,60($sp)
	move	$5,$18
	lw	$23,56($sp)
	lw	$22,52($sp)
	sll	$6,$6,2
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	lw	$25,%call16(memcpy)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memcpy
1:	jr	$25
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

$L150:
	lw	$8,708($16)
	lw	$3,0($11)
	sll	$6,$6,2
	mul	$2,$14,$8
	addu	$10,$5,$6
	addu	$9,$4,$6
	lw	$6,0($10)
	lw	$10,0($9)
	sll	$2,$2,2
	.set	noreorder
	.set	nomacro
	bne	$7,$3,$L153
	addu	$2,$17,$2
	.set	macro
	.set	reorder

	blez	$8,$L154
	move	$8,$0
	andi	$20,$10,0x20
	nor	$25,$0,$10
$L155:
	lw	$12,0($2)
	addiu	$2,$2,4
	addiu	$8,$8,1
	mult	$12,$6
	mfhi	$13
	mflo	$12
	sll	$7,$13,1
	srl	$3,$12,$10
	sll	$7,$7,$25
	sra	$9,$13,$10
	or	$3,$7,$3
	movn	$3,$9,$20
	sw	$3,-4($2)
	lw	$3,708($16)
	slt	$3,$8,$3
	bne	$3,$0,$L155
	addiu	$14,$14,1
	.set	noreorder
	.set	nomacro
	bne	$14,$15,$L145
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L169
	.option	pic2
	lw	$6,56($16)
	.set	macro
	.set	reorder

$L139:
	addiu	$7,$2,-3
	.set	noreorder
	.set	nomacro
	blez	$7,$L142
	move	$3,$0
	.set	macro
	.set	reorder

	lui	$6,%hi(s32_mlt_window)
	li	$15,13			# 0xd
	li	$9,31			# 0x1f
	lw	$13,%lo(s32_mlt_window)($6)
	li	$14,3			# 0x3
	move	$6,$17
	move	$11,$19
	move	$12,$13
$L143:
	lw	$10,0($6)
#APP
 # 1259 "cook.c" 1
	S32MUL xr1,xr2,$10,$8
 # 0 "" 2
#NO_APP
	lw	$10,4($6)
#APP
 # 1260 "cook.c" 1
	S32MUL xr3,xr4,$10,$8
 # 0 "" 2
#NO_APP
	lw	$10,8($6)
#APP
 # 1261 "cook.c" 1
	S32MUL xr5,xr6,$10,$8
 # 0 "" 2
#NO_APP
	lw	$10,12($6)
#APP
 # 1262 "cook.c" 1
	S32MUL xr7,xr8,$10,$8
 # 0 "" 2
 # 1264 "cook.c" 1
	S32EXTRV xr1,xr2,$15,$9
 # 0 "" 2
 # 1265 "cook.c" 1
	S32EXTRV xr3,xr4,$15,$9
 # 0 "" 2
 # 1266 "cook.c" 1
	S32EXTRV xr5,xr6,$15,$9
 # 0 "" 2
 # 1267 "cook.c" 1
	S32EXTRV xr7,xr8,$15,$9
 # 0 "" 2
 # 1269 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1270 "cook.c" 1
	D32SLL xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1271 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1272 "cook.c" 1
	D32SAR xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1274 "cook.c" 1
	S32M2I xr1, $22
 # 0 "" 2
 # 1275 "cook.c" 1
	S32M2I xr3, $25
 # 0 "" 2
 # 1276 "cook.c" 1
	S32M2I xr5, $24
 # 0 "" 2
 # 1277 "cook.c" 1
	S32M2I xr7, $10
 # 0 "" 2
#NO_APP
	lw	$23,0($12)
#APP
 # 1279 "cook.c" 1
	S32MUL xr1,xr2,$23,$22
 # 0 "" 2
#NO_APP
	lw	$22,4($12)
#APP
 # 1280 "cook.c" 1
	S32MUL xr3,xr4,$22,$25
 # 0 "" 2
#NO_APP
	lw	$22,8($12)
#APP
 # 1281 "cook.c" 1
	S32MUL xr5,xr6,$22,$24
 # 0 "" 2
#NO_APP
	lw	$22,12($12)
#APP
 # 1282 "cook.c" 1
	S32MUL xr7,xr8,$22,$10
 # 0 "" 2
#NO_APP
	addiu	$10,$2,-1
	lw	$22,0($11)
	subu	$10,$10,$3
	sll	$10,$10,2
	addu	$10,$13,$10
	lw	$10,0($10)
#APP
 # 1284 "cook.c" 1
	S32MSUB xr1,xr2,$22,$10
 # 0 "" 2
#NO_APP
	addiu	$10,$2,-2
	lw	$22,4($11)
	subu	$10,$10,$3
	sll	$10,$10,2
	addu	$10,$13,$10
	lw	$10,0($10)
#APP
 # 1285 "cook.c" 1
	S32MSUB xr3,xr4,$22,$10
 # 0 "" 2
#NO_APP
	subu	$7,$7,$3
	lw	$10,8($11)
	sll	$7,$7,2
	addu	$7,$13,$7
	lw	$7,0($7)
#APP
 # 1286 "cook.c" 1
	S32MSUB xr5,xr6,$10,$7
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-4
	lw	$7,12($11)
	subu	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$13,$2
	lw	$2,0($2)
#APP
 # 1287 "cook.c" 1
	S32MSUB xr7,xr8,$7,$2
 # 0 "" 2
 # 1289 "cook.c" 1
	S32EXTRV xr1,xr2,$14,$9
 # 0 "" 2
 # 1290 "cook.c" 1
	S32EXTRV xr3,xr4,$14,$9
 # 0 "" 2
 # 1291 "cook.c" 1
	S32EXTRV xr5,xr6,$14,$9
 # 0 "" 2
 # 1292 "cook.c" 1
	S32EXTRV xr7,xr8,$14,$9
 # 0 "" 2
 # 1294 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1295 "cook.c" 1
	D32SLL xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1296 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1297 "cook.c" 1
	D32SAR xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1299 "cook.c" 1
	S32M2I xr1, $2
 # 0 "" 2
#NO_APP
	sw	$2,0($6)
#APP
 # 1300 "cook.c" 1
	S32M2I xr3, $2
 # 0 "" 2
#NO_APP
	sw	$2,4($6)
#APP
 # 1301 "cook.c" 1
	S32M2I xr5, $2
 # 0 "" 2
#NO_APP
	sw	$2,8($6)
#APP
 # 1302 "cook.c" 1
	S32M2I xr7, $2
 # 0 "" 2
#NO_APP
	sw	$2,12($6)
	addiu	$3,$3,4
	lw	$2,56($16)
	addiu	$6,$6,16
	addiu	$12,$12,16
	addiu	$7,$2,-3
	slt	$10,$3,$7
	.set	noreorder
	.set	nomacro
	bne	$10,$0,$L143
	addiu	$11,$11,16
	.set	macro
	.set	reorder

$L142:
	slt	$6,$3,$2
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L147
	lui	$6,%hi(s32_mlt_window)
	.set	macro
	.set	reorder

	sll	$14,$3,2
	move	$11,$0
	lw	$13,%lo(s32_mlt_window)($6)
	li	$6,33			# 0x21
	addu	$9,$17,$14
	subu	$21,$6,$21
	li	$10,31			# 0x1f
	li	$12,3			# 0x3
$L146:
	lw	$6,0($9)
#APP
 # 231 "cook.c" 1
	S32MUL xr1,xr2,$6,$8
 # 0 "" 2
 # 232 "cook.c" 1
	S32EXTRV xr1,xr2,$21,$10
 # 0 "" 2
 # 233 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 234 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 235 "cook.c" 1
	S32M2I xr1, $6
 # 0 "" 2
#NO_APP
	addu	$7,$11,$14
	addu	$15,$13,$7
	lw	$15,0($15)
#APP
 # 241 "cook.c" 1
	S32MUL xr1,xr2,$6,$15
 # 0 "" 2
 # 242 "cook.c" 1
	S32EXTRV xr1,xr2,$12,$10
 # 0 "" 2
 # 243 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 244 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 245 "cook.c" 1
	S32M2I xr1, $6
 # 0 "" 2
#NO_APP
	addiu	$2,$2,-1
	addu	$7,$19,$7
	subu	$2,$2,$3
	sll	$2,$2,2
	lw	$7,0($7)
	addu	$2,$13,$2
	lw	$2,0($2)
#APP
 # 241 "cook.c" 1
	S32MUL xr1,xr2,$7,$2
 # 0 "" 2
 # 242 "cook.c" 1
	S32EXTRV xr1,xr2,$12,$10
 # 0 "" 2
 # 243 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 244 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 245 "cook.c" 1
	S32M2I xr1, $7
 # 0 "" 2
#NO_APP
	subu	$2,$6,$7
	addiu	$9,$9,4
	addiu	$3,$3,1
	sw	$2,-4($9)
	lw	$2,56($16)
	slt	$6,$3,$2
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L146
	addiu	$11,$11,4
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L170
	.option	pic2
	lw	$11,0($20)
	.set	macro
	.set	reorder

	.end	s32_imlt_gain.isra.12
	.size	s32_imlt_gain.isra.12, .-s32_imlt_gain.isra.12
	.section	.rodata.str1.4
	.align	2
$LC28:
	.ascii	"decode_vectors end %d %d %f\012\000"
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	s32_mono_decode
	.type	s32_mono_decode, @function
s32_mono_decode:
	.frame	$sp,3376,$31		# vars= 3312, regs= 10/0, args= 16, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-3376
	addiu	$28,$28,%lo(__gnu_local_gp)
	addiu	$3,$sp,1048
	sw	$23,3364($sp)
	sw	$6,3384($sp)
	move	$23,$5
	sw	$31,3372($sp)
	move	$5,$0
	sw	$3,3300($sp)
	li	$6,512			# 0x200
	sw	$fp,3368($sp)
	move	$fp,$4
	sw	$21,3356($sp)
	move	$4,$3
	.cprestore	16
	addiu	$21,$sp,1560
	sw	$22,3360($sp)
	sw	$20,3352($sp)
	sw	$19,3348($sp)
	sw	$18,3344($sp)
	sw	$17,3340($sp)
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$16,3336($sp)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,16($sp)
	li	$6,512			# 0x200
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	li	$10,16711680			# 0xff0000
	lw	$4,32($fp)
	li	$9,-16777216			# 0xffffffffff000000
	lw	$8,24($fp)
	addiu	$10,$10,255
	ori	$9,$9,0xff00
	lw	$28,16($sp)
	srl	$2,$4,3
	addiu	$5,$4,6
	addu	$2,$8,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($2)  
	lwr $3, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sw	$5,32($fp)
	sll	$3,$3,8
	lw	$6,72($23)
	and	$2,$2,$10
	and	$3,$3,$9
	or	$2,$2,$3
	sll	$3,$2,16
	srl	$2,$2,16
	andi	$4,$4,0x7
	or	$2,$3,$2
	sll	$2,$2,$4
	slt	$3,$6,2
	srl	$2,$2,26
	addiu	$2,$2,-6
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L172
	sw	$2,2888($sp)
	.set	macro
	.set	reorder

	addiu	$7,$sp,2888
	li	$6,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L179
	.option	pic2
	li	$11,32			# 0x20
	.set	macro
	.set	reorder

$L268:
	slt	$3,$2,14
$L278:
	bne	$3,$0,$L266
	li	$2,12			# 0xc
$L176:
	srl	$3,$5,3
	sll	$2,$2,4
	addu	$3,$8,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($3)  
	lwr $4, 0($3)  
	
 # 0 "" 2
#NO_APP
	srl	$3,$4,8
	sll	$4,$4,8
	addu	$2,$fp,$2
	and	$3,$3,$10
	and	$4,$4,$9
	or	$4,$3,$4
	lw	$13,388($2)
	sll	$3,$4,16
	lw	$12,392($2)
	srl	$4,$4,16
	andi	$14,$5,0x7
	or	$2,$3,$4
	sll	$2,$2,$14
	subu	$3,$11,$13
	srl	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$12,$2
	lh	$3,2($2)
	.set	noreorder
	.set	nomacro
	bltz	$3,$L267
	lh	$2,0($2)
	.set	macro
	.set	reorder

	lw	$4,0($7)
	addu	$5,$3,$5
	addiu	$6,$6,1
	addiu	$7,$7,4
	sw	$5,32($fp)
	addu	$2,$2,$4
	lw	$3,72($23)
	addiu	$2,$2,-12
	slt	$3,$6,$3
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L172
	sw	$2,0($7)
	.set	macro
	.set	reorder

$L179:
	lw	$2,24($23)
	sll	$3,$2,1
	slt	$3,$6,$3
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L268
	subu	$2,$6,$2
	.set	macro
	.set	reorder

	srl	$2,$6,31
	addu	$2,$2,$6
	sra	$2,$2,1
	.set	noreorder
	.set	nomacro
	bgtz	$2,$L278
	slt	$3,$2,14
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L176
	.option	pic2
	move	$2,$0
	.set	macro
	.set	reorder

$L266:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L176
	.option	pic2
	addiu	$2,$2,-1
	.set	macro
	.set	reorder

$L267:
	addu	$5,$5,$13
	addiu	$6,$6,1
	srl	$13,$5,3
	andi	$15,$5,0x7
	addu	$13,$8,$13
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $14, 3($13)  
	lwr $14, 0($13)  
	
 # 0 "" 2
#NO_APP
	srl	$13,$14,8
	sll	$14,$14,8
	and	$13,$13,$10
	and	$14,$14,$9
	or	$14,$13,$14
	sll	$13,$14,16
	srl	$4,$14,16
	addiu	$7,$7,4
	or	$4,$13,$4
	sll	$4,$4,$15
	srl	$3,$4,$3
	lw	$4,-4($7)
	addu	$2,$3,$2
	sll	$2,$2,2
	addu	$12,$12,$2
	lh	$3,2($12)
	lh	$2,0($12)
	addu	$5,$3,$5
	addu	$2,$2,$4
	sw	$5,32($fp)
	addiu	$2,$2,-12
	lw	$3,72($23)
	slt	$3,$6,$3
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L179
	sw	$2,0($7)
	.set	macro
	.set	reorder

$L172:
	srl	$2,$5,3
	lw	$6,36($23)
	li	$7,16711680			# 0xff0000
	lw	$4,56($fp)
	addu	$8,$8,$2
	addiu	$7,$7,255
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($8)  
	lwr $3, 0($8)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sll	$3,$3,8
	and	$2,$2,$7
	li	$7,-16777216			# 0xffffffffff000000
	addu	$8,$5,$6
	ori	$7,$7,0xff00
	and	$3,$3,$7
	or	$2,$2,$3
	sw	$8,32($fp)
	sll	$3,$2,16
	lw	$11,64($23)
	srl	$2,$2,16
	lw	$24,76($23)
	andi	$5,$5,0x7
	or	$2,$3,$2
	sll	$5,$2,$5
	subu	$6,$0,$6
	subu	$18,$11,$8
	srl	$6,$5,$6
	slt	$2,$4,$18
	sw	$6,3304($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L180
	sw	$6,52($fp)
	.set	macro
	.set	reorder

	subu	$11,$18,$4
	sll	$2,$11,2
	addu	$2,$2,$11
	addiu	$3,$2,7
	slt	$5,$2,0
	movn	$2,$3,$5
	sra	$2,$2,3
	addu	$18,$4,$2
$L180:
	lw	$25,%call16(memset)($28)
	addiu	$20,$sp,2072
	move	$5,$0
	sw	$24,3332($sp)
	li	$6,408			# 0x198
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	addiu	$17,$sp,2480
	lw	$28,16($sp)
	move	$5,$0
	li	$6,408			# 0x198
	move	$4,$17
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	addiu	$19,$sp,24
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,16($sp)
	li	$6,1024			# 0x400
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$4,$19
	.set	macro
	.set	reorder

	lui	$7,%hi(expbits_tab)
	lw	$5,72($23)
	addiu	$13,$18,-32
	lw	$28,16($sp)
	li	$12,6			# 0x6
	lw	$24,3332($sp)
	li	$8,-32			# 0xffffffffffffffe0
	sll	$11,$5,2
	li	$6,32			# 0x20
	addiu	$10,$sp,2888
	addiu	$7,$7,%lo(expbits_tab)
	li	$9,7			# 0x7
$L185:
	.set	noreorder
	.set	nomacro
	blez	$5,$L240
	move	$4,$0
	.set	macro
	.set	reorder

	move	$14,$0
$L183:
	addu	$2,$10,$4
	addiu	$4,$4,4
	lw	$3,0($2)
	subu	$3,$6,$3
	addu	$3,$3,$8
	srl	$2,$3,31
	addu	$2,$2,$3
	sra	$3,$2,1
	slt	$2,$3,8
	movz	$3,$9,$2
	move	$2,$3
	slt	$3,$3,0
	movn	$2,$0,$3
	sll	$2,$2,2
	addu	$2,$7,$2
	lw	$2,0($2)
	.set	noreorder
	.set	nomacro
	bne	$4,$11,$L183
	addu	$14,$14,$2
	.set	macro
	.set	reorder

	addu	$2,$8,$6
	addiu	$12,$12,-1
	slt	$14,$14,$13
	movz	$8,$2,$14
	.set	noreorder
	.set	nomacro
	bne	$12,$0,$L185
	sra	$6,$6,1
	.set	macro
	.set	reorder

$L277:
	blez	$5,$L241
	lui	$11,%hi(expbits_tab)
	move	$3,$0
	move	$9,$0
	move	$7,$0
	addiu	$13,$sp,2888
	addiu	$11,$11,%lo(expbits_tab)
	li	$12,7			# 0x7
$L187:
	addu	$2,$13,$3
	addu	$10,$20,$3
	addu	$6,$17,$3
	lw	$4,0($2)
	addiu	$7,$7,1
	addiu	$3,$3,4
	slt	$14,$7,$5
	subu	$4,$8,$4
	srl	$2,$4,31
	addu	$2,$2,$4
	sra	$4,$2,1
	slt	$2,$4,8
	movz	$4,$12,$2
	move	$2,$4
	slt	$4,$4,0
	movn	$2,$0,$4
	sll	$4,$2,2
	sw	$2,0($10)
	addu	$4,$11,$4
	sw	$2,0($6)
	lw	$2,0($4)
	.set	noreorder
	.set	nomacro
	bne	$14,$0,$L187
	addu	$9,$9,$2
	.set	macro
	.set	reorder

$L186:
	slt	$2,$24,2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L269
	lw	$16,72($23)
	.set	macro
	.set	reorder

	lui	$4,%hi(expbits_tab)
	li	$2,-1048576			# 0xfffffffffff00000
	li	$5,1			# 0x1
	addiu	$4,$4,%lo(expbits_tab)
	li	$13,983040			# 0xf0000
	sll	$11,$18,1
	move	$12,$24
	move	$18,$24
	move	$10,$9
	ori	$25,$2,0xbdc1
	addu	$2,$10,$9
$L279:
	slt	$2,$11,$2
	bne	$2,$0,$L190
	.set	noreorder
	.set	nomacro
	blez	$16,$L204
	addiu	$14,$sp,2888
	.set	macro
	.set	reorder

	li	$22,-1			# 0xffffffffffffffff
	addiu	$15,$13,16959
	move	$6,$0
	move	$7,$17
$L198:
	lw	$2,0($7)
	addiu	$7,$7,4
	.set	noreorder
	.set	nomacro
	blez	$2,$L197
	subu	$3,$0,$2
	.set	macro
	.set	reorder

	sll	$2,$3,1
	lw	$3,0($14)
	subu	$2,$2,$3
	addu	$2,$2,$8
	slt	$3,$2,$15
	movn	$22,$6,$3
	movn	$15,$2,$3
$L197:
	addiu	$6,$6,1
	.set	noreorder
	.set	nomacro
	bne	$6,$16,$L198
	addiu	$14,$14,4
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
	.set	noreorder
	.set	nomacro
	beq	$22,$2,$L200
	sll	$3,$22,2
	.set	macro
	.set	reorder

	addiu	$24,$24,-1
	addu	$3,$19,$3
	sll	$6,$24,2
	lw	$2,2456($3)
	addu	$6,$19,$6
	sw	$22,0($6)
	addiu	$7,$2,-1
	sll	$2,$2,2
	sll	$6,$7,2
	addu	$2,$2,$4
	sw	$7,2456($3)
	addu	$3,$6,$4
	lw	$6,0($2)
	lw	$2,0($3)
	subu	$2,$2,$6
	addu	$9,$9,$2
$L196:
	lw	$18,76($23)
	addiu	$5,$5,1
	slt	$2,$5,$18
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L279
	addu	$2,$10,$9
	.set	macro
	.set	reorder

$L189:
	.set	noreorder
	.set	nomacro
	blez	$16,$L280
	slt	$2,$18,2
	.set	macro
	.set	reorder

$L200:
	lw	$25,%call16(memcpy)($28)
	sll	$6,$16,2
	addiu	$4,$sp,1048
	sw	$24,3332($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
	move	$5,$17
	.set	macro
	.set	reorder

	lw	$28,16($sp)
	lw	$24,3332($sp)
$L204:
	slt	$2,$18,2
$L280:
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L202
	sll	$5,$24,2
	.set	macro
	.set	reorder

	lw	$25,%call16(memcpy)($28)
	sll	$6,$18,2
	addu	$5,$19,$5
	addiu	$6,$6,-4
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	lw	$28,16($sp)
$L202:
	lw	$7,3304($sp)
	.set	noreorder
	.set	nomacro
	blez	$7,$L206
	move	$3,$0
	.set	macro
	.set	reorder

	sll	$5,$7,2
	addiu	$6,$sp,1048
$L209:
	addu	$2,$21,$3
	addiu	$3,$3,4
	lw	$2,0($2)
	sll	$2,$2,2
	addu	$2,$6,$2
	lw	$4,0($2)
	addiu	$4,$4,1
	.set	noreorder
	.set	nomacro
	bne	$3,$5,$L209
	sw	$4,0($2)
	.set	macro
	.set	reorder

$L206:
	.set	noreorder
	.set	nomacro
	blez	$16,$L208
	lw	$7,3384($sp)
	.set	macro
	.set	reorder

	addiu	$9,$sp,2888
	addiu	$3,$sp,1048
	sw	$0,3304($sp)
	sw	$9,3308($sp)
	sw	$3,3300($sp)
	sw	$7,3296($sp)
$L232:
	lw	$8,3300($sp)
	lw	$16,0($8)
	slt	$2,$16,7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L270
	sll	$24,$16,2
	.set	macro
	.set	reorder

$L210:
	lw	$25,%call16(memset)($28)
	move	$5,$0
	li	$6,80			# 0x50
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,16($sp)
	li	$6,80			# 0x50
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$4,$19
	.set	macro
	.set	reorder

	lw	$28,16($sp)
$L237:
	lw	$9,3308($sp)
	lui	$5,%hi(sp32_rootpow2tab)
	lui	$6,%hi(rootpow2tab_sft)
	lui	$8,%hi(s32_dither_tab_L20)
	lw	$4,%lo(sp32_rootpow2tab)($5)
	addiu	$6,$6,%lo(rootpow2tab_sft)
	lw	$3,0($9)
	sll	$14,$16,2
	sll	$7,$16,4
	li	$13,20			# 0x14
	addiu	$2,$3,63
	sll	$3,$3,2
	sll	$2,$2,2
	addu	$3,$4,$3
	addu	$2,$6,$2
	lw	$6,3296($sp)
	sll	$4,$16,1
	lw	$10,0($3)
	lui	$12,%hi(s32_quant_centroid_tab_L14)
	lw	$2,0($2)
	addiu	$3,$8,%lo(s32_dither_tab_L20)
	li	$11,26			# 0x1a
	move	$5,$0
	addu	$14,$14,$3
	subu	$13,$13,$2
	li	$9,31			# 0x1f
	subu	$7,$7,$4
	addiu	$12,$12,%lo(s32_quant_centroid_tab_L14)
	subu	$11,$11,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L231
	.option	pic2
	li	$8,80			# 0x50
	.set	macro
	.set	reorder

$L272:
	sll	$2,$2,2
	lw	$4,0($4)
	addu	$2,$2,$12
	sltu	$4,$0,$4
	subu	$3,$0,$4
	lw	$2,0($2)
	xor	$2,$3,$2
	addu	$2,$2,$4
#APP
 # 231 "cook.c" 1
	S32MUL xr1,xr2,$2,$10
 # 0 "" 2
 # 232 "cook.c" 1
	S32EXTRV xr1,xr2,$11,$9
 # 0 "" 2
 # 233 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 234 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 235 "cook.c" 1
	S32M2I xr1, $2
 # 0 "" 2
#NO_APP
	addiu	$5,$5,4
	sw	$2,0($6)
	.set	noreorder
	.set	nomacro
	beq	$5,$8,$L271
	addiu	$6,$6,4
	.set	macro
	.set	reorder

$L231:
	addu	$2,$20,$5
	addu	$4,$19,$5
	lw	$3,0($2)
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L272
	addu	$2,$7,$3
	.set	macro
	.set	reorder

	lw	$4,316($fp)
	lw	$15,0($14)
	addiu	$3,$4,-55
	addiu	$2,$4,-24
	andi	$3,$3,0x3f
	andi	$2,$2,0x3f
	sll	$3,$3,2
	sll	$2,$2,2
	addu	$3,$fp,$3
	addu	$2,$fp,$2
	andi	$16,$4,0x3f
	lw	$17,60($3)
	sll	$3,$16,2
	lw	$2,60($2)
	addiu	$4,$4,1
	addu	$3,$fp,$3
	addu	$17,$17,$2
	nor	$2,$0,$17
	srl	$16,$2,31
	sw	$17,60($3)
	sw	$4,316($fp)
	subu	$2,$0,$16
	xor	$2,$15,$2
	addu	$2,$2,$16
#APP
 # 231 "cook.c" 1
	S32MUL xr1,xr2,$2,$10
 # 0 "" 2
 # 232 "cook.c" 1
	S32EXTRV xr1,xr2,$13,$9
 # 0 "" 2
 # 233 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 234 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 235 "cook.c" 1
	S32M2I xr1, $2
 # 0 "" 2
#NO_APP
	addiu	$5,$5,4
	sw	$2,0($6)
	.set	noreorder
	.set	nomacro
	bne	$5,$8,$L231
	addiu	$6,$6,4
	.set	macro
	.set	reorder

$L271:
	lw	$9,3304($sp)
	lw	$3,3300($sp)
	lw	$7,3296($sp)
	lw	$8,3308($sp)
	addiu	$9,$9,1
	lw	$2,72($23)
	addiu	$3,$3,4
	addiu	$7,$7,80
	addiu	$8,$8,4
	sw	$9,3304($sp)
	slt	$2,$9,$2
	sw	$3,3300($sp)
	sw	$7,3296($sp)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L232
	sw	$8,3308($sp)
	.set	macro
	.set	reorder

$L208:
	lw	$2,%got(mpFrame)($28)
	lw	$3,0($2)
	li	$2,-1			# 0xffffffffffffffff
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L273
	lw	$31,3372($sp)
	.set	macro
	.set	reorder

	lw	$fp,3368($sp)
	lw	$23,3364($sp)
	lw	$22,3360($sp)
	lw	$21,3356($sp)
	lw	$20,3352($sp)
	lw	$19,3348($sp)
	lw	$18,3344($sp)
	lw	$17,3340($sp)
	lw	$16,3336($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,3376
	.set	macro
	.set	reorder

$L190:
	.set	noreorder
	.set	nomacro
	blez	$16,$L204
	addiu	$7,$sp,2888
	.set	macro
	.set	reorder

	li	$15,-1			# 0xffffffffffffffff
	move	$3,$0
	move	$6,$20
	move	$14,$25
$L194:
	lw	$22,0($6)
	addiu	$6,$6,4
	subu	$2,$0,$22
	slt	$22,$22,7
	.set	noreorder
	.set	nomacro
	beq	$22,$0,$L193
	sll	$2,$2,1
	.set	macro
	.set	reorder

	lw	$22,0($7)
	subu	$2,$2,$22
	addu	$2,$2,$8
	slt	$22,$2,$14
	movz	$15,$3,$22
	movz	$14,$2,$22
$L193:
	addiu	$3,$3,1
	.set	noreorder
	.set	nomacro
	bne	$3,$16,$L194
	addiu	$7,$7,4
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
	.set	noreorder
	.set	nomacro
	beq	$15,$2,$L200
	sll	$3,$15,2
	.set	macro
	.set	reorder

	sll	$6,$12,2
	addu	$3,$19,$3
	addu	$6,$19,$6
	addiu	$12,$12,1
	lw	$2,2048($3)
	sw	$15,0($6)
	addiu	$7,$2,1
	sll	$2,$2,2
	sll	$6,$7,2
	addu	$2,$2,$4
	sw	$7,2048($3)
	addu	$3,$6,$4
	lw	$6,0($2)
	lw	$2,0($3)
	subu	$2,$2,$6
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L196
	.option	pic2
	addu	$10,$10,$2
	.set	macro
	.set	reorder

$L270:
	lui	$9,%hi(vpr_tab)
	addiu	$9,$9,%lo(vpr_tab)
	lui	$4,%hi(vd_tab)
	addu	$2,$9,$24
	addiu	$4,$4,%lo(vd_tab)
	lw	$2,0($2)
	addu	$3,$4,$24
	.set	noreorder
	.set	nomacro
	blez	$2,$L237
	lw	$5,0($3)
	.set	macro
	.set	reorder

	sll	$3,$16,4
	lw	$4,24($fp)
	lui	$7,%hi(invradix_tab)
	sw	$20,3324($sp)
	addu	$3,$fp,$3
	sw	$19,3328($sp)
	lui	$8,%hi(kmax_tab)
	sw	$16,3320($sp)
	li	$13,983040			# 0xf0000
	lw	$6,596($3)
	addiu	$8,$8,%lo(kmax_tab)
	lw	$25,600($3)
	li	$3,32			# 0x20
	addiu	$22,$5,-1
	sll	$11,$5,2
	sw	$6,3316($sp)
	subu	$6,$3,$6
	addiu	$3,$7,%lo(invradix_tab)
	move	$15,$0
	addu	$3,$3,$24
	move	$17,$0
	addu	$24,$8,$24
	li	$14,-1			# 0xffffffffffffffff
	sw	$3,3312($sp)
	move	$18,$19
	move	$10,$20
	ori	$13,$13,0xffff
	move	$20,$6
	move	$19,$5
	move	$21,$2
$L225:
	lw	$5,32($fp)
	li	$7,16711680			# 0xff0000
	li	$8,-16777216			# 0xffffffffff000000
	ori	$7,$7,0xff
	srl	$2,$5,3
	ori	$8,$8,0xff00
	addu	$2,$4,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $9, 3($2)  
	lwr $9, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$6,$9,8
	sll	$2,$9,8
	and	$6,$6,$7
	and	$2,$2,$8
	or	$2,$6,$2
	sll	$6,$2,16
	srl	$2,$2,16
	andi	$3,$5,0x7
	or	$2,$6,$2
	sll	$3,$2,$3
	srl	$3,$3,$20
	sll	$3,$3,2
	addu	$3,$25,$3
	lh	$2,2($3)
	.set	noreorder
	.set	nomacro
	bltz	$2,$L274
	lh	$3,0($3)
	.set	macro
	.set	reorder

$L213:
	addu	$2,$2,$5
	sw	$2,32($fp)
	lw	$5,64($23)
	slt	$2,$5,$2
	li	$5,1			# 0x1
	movn	$3,$0,$2
	.set	noreorder
	.set	nomacro
	bltz	$22,$L275
	movn	$15,$5,$2
	.set	macro
	.set	reorder

$L216:
	lw	$6,3312($sp)
	addu	$16,$11,$10
	lw	$9,0($24)
	move	$5,$16
	lw	$12,0($6)
	nor	$9,$0,$9
	move	$6,$22
$L220:
	mul	$2,$3,$12
	addiu	$5,$5,-4
	addiu	$6,$6,-1
	addu	$7,$2,$13
	slt	$8,$2,0
	movn	$2,$7,$8
	sra	$2,$2,20
	mul	$8,$2,$9
	addu	$7,$8,$3
	move	$3,$2
	.set	noreorder
	.set	nomacro
	bne	$6,$14,$L220
	sw	$7,0($5)
	.set	macro
	.set	reorder

$L221:
	blez	$19,$L218
	move	$3,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L217
	.option	pic2
	move	$5,$18
	.set	macro
	.set	reorder

$L276:
	lw	$2,32($fp)
	lw	$9,64($23)
	srl	$6,$2,3
	andi	$7,$2,0x7
	addiu	$8,$2,1
	slt	$2,$2,$9
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L223
	addu	$6,$4,$6
	.set	macro
	.set	reorder

	lbu	$2,0($6)
	sw	$8,32($fp)
	sll	$2,$2,$7
	andi	$2,$2,0x00ff
	srl	$2,$2,7
	sw	$2,0($5)
$L224:
	addiu	$3,$3,4
	.set	noreorder
	.set	nomacro
	beq	$3,$11,$L218
	addiu	$5,$5,4
	.set	macro
	.set	reorder

$L217:
	addu	$2,$10,$3
	lw	$2,0($2)
	bne	$2,$0,$L276
	addiu	$3,$3,4
	sw	$0,0($5)
	.set	noreorder
	.set	nomacro
	bne	$3,$11,$L217
	addiu	$5,$5,4
	.set	macro
	.set	reorder

$L218:
	addiu	$17,$17,1
	addu	$18,$18,$11
	slt	$2,$17,$21
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L225
	move	$10,$16
	.set	macro
	.set	reorder

	lw	$16,3320($sp)
	lw	$20,3324($sp)
	.set	noreorder
	.set	nomacro
	beq	$15,$0,$L237
	lw	$19,3328($sp)
	.set	macro
	.set	reorder

	lw	$4,72($23)
	.set	noreorder
	.set	nomacro
	blez	$4,$L228
	sll	$4,$4,2
	.set	macro
	.set	reorder

	lw	$6,3300($sp)
	move	$2,$0
	li	$5,7			# 0x7
$L227:
	addu	$3,$6,$2
	addiu	$2,$2,4
	.set	noreorder
	.set	nomacro
	bne	$2,$4,$L227
	sw	$5,0($3)
	.set	macro
	.set	reorder

$L228:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L210
	.option	pic2
	li	$16,7			# 0x7
	.set	macro
	.set	reorder

$L223:
	sw	$0,0($5)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	li	$15,1			# 0x1
	.set	macro
	.set	reorder

$L274:
	lw	$9,3316($sp)
	addu	$5,$5,$9
	li	$9,16711680			# 0xff0000
	srl	$6,$5,3
	ori	$9,$9,0xff
	addu	$6,$4,$6
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $7, 3($6)  
	lwr $7, 0($6)  
	
 # 0 "" 2
#NO_APP
	move	$6,$7
	srl	$7,$7,8
	sll	$6,$6,8
	and	$7,$7,$9
	li	$9,-16777216			# 0xffffffffff000000
	andi	$8,$5,0x7
	ori	$9,$9,0xff00
	and	$6,$6,$9
	or	$7,$7,$6
	sll	$6,$7,16
	srl	$7,$7,16
	or	$7,$6,$7
	sll	$6,$7,$8
	srl	$6,$6,$2
	addu	$3,$6,$3
	sll	$3,$3,2
	addu	$3,$25,$3
	lh	$6,2($3)
	.set	noreorder
	.set	nomacro
	bltz	$6,$L214
	lh	$3,0($3)
	.set	macro
	.set	reorder

	move	$2,$6
	addu	$2,$2,$5
	sw	$2,32($fp)
	lw	$5,64($23)
	slt	$2,$5,$2
	li	$5,1			# 0x1
	movn	$3,$0,$2
	.set	noreorder
	.set	nomacro
	bgez	$22,$L216
	movn	$15,$5,$2
	.set	macro
	.set	reorder

$L275:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L221
	.option	pic2
	addu	$16,$11,$10
	.set	macro
	.set	reorder

$L214:
	subu	$5,$5,$2
	li	$9,16711680			# 0xff0000
	srl	$2,$5,3
	ori	$9,$9,0xff
	addu	$2,$4,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $7, 3($2)  
	lwr $7, 0($2)  
	
 # 0 "" 2
#NO_APP
	move	$2,$7
	srl	$7,$7,8
	sll	$2,$2,8
	and	$7,$7,$9
	li	$9,-16777216			# 0xffffffffff000000
	andi	$8,$5,0x7
	ori	$9,$9,0xff00
	and	$2,$2,$9
	or	$7,$7,$2
	sll	$2,$7,16
	srl	$7,$7,16
	or	$7,$2,$7
	sll	$2,$7,$8
	srl	$2,$2,$6
	addu	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$25,$2
	lh	$3,0($2)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L213
	.option	pic2
	lh	$2,2($2)
	.set	macro
	.set	reorder

$L240:
	move	$14,$0
	addu	$2,$8,$6
	addiu	$12,$12,-1
	slt	$14,$14,$13
	movz	$8,$2,$14
	.set	noreorder
	.set	nomacro
	bne	$12,$0,$L185
	sra	$6,$6,1
	.set	macro
	.set	reorder

	.option	pic0
	j	$L277
	.option	pic2
$L273:
	lui	$19,%hi($LC28)
	lw	$18,%got(s32_decode_buffer_0)($28)
	lw	$17,3384($sp)
	move	$16,$0
	addiu	$19,$19,%lo($LC28)
	li	$20,1060			# 0x424
$L234:
	lw	$25,%call16(printf)($28)
	move	$5,$16
	lw	$6,0($18)
	addiu	$16,$16,1
	lw	$7,0($17)
	move	$4,$19
	addiu	$18,$18,4
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
	addiu	$17,$17,4
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$16,$20,$L234
	lw	$28,16($sp)
	.set	macro
	.set	reorder

	lw	$31,3372($sp)
	lw	$fp,3368($sp)
	lw	$23,3364($sp)
	lw	$22,3360($sp)
	lw	$21,3356($sp)
	lw	$20,3352($sp)
	lw	$19,3348($sp)
	lw	$18,3344($sp)
	lw	$17,3340($sp)
	lw	$16,3336($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,3376
	.set	macro
	.set	reorder

$L269:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L189
	.option	pic2
	move	$18,$24
	.set	macro
	.set	reorder

$L241:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L186
	.option	pic2
	move	$9,$0
	.set	macro
	.set	reorder

	.end	s32_mono_decode
	.size	s32_mono_decode, .-s32_mono_decode
	.section	.rodata.str1.4
	.align	2
$LC29:
	.ascii	"frame subpacket size total > avctx->block_align!\012\000"
	.align	2
$LC30:
	.ascii	"subpacket[%i] size %i js %i %i block_align %i\012\000"
	.align	2
$LC31:
	.ascii	"subpacket[%i] %i %i\012\000"
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	cook_decode_frame
	.type	cook_decode_frame, @function
cook_decode_frame:
	.frame	$sp,208,$31		# vars= 128, regs= 10/0, args= 32, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	lw	$2,20($7)
	lui	$28,%hi(__gnu_local_gp)
	lw	$7,16($7)
	addiu	$sp,$sp,-208
	lw	$3,268($4)
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$16,168($sp)
	sw	$31,204($sp)
	sw	$7,136($sp)
	slt	$7,$2,$3
	sw	$fp,200($sp)
	sw	$23,196($sp)
	sw	$22,192($sp)
	sw	$21,188($sp)
	sw	$20,184($sp)
	sw	$19,180($sp)
	sw	$18,176($sp)
	sw	$17,172($sp)
	.cprestore	32
	.set	noreorder
	.set	nomacro
	bne	$7,$0,$L378
	lw	$16,136($4)
	.set	macro
	.set	reorder

	sw	$6,160($sp)
	sw	$5,148($sp)
	sw	$4,128($sp)
	sw	$3,29660($16)
	lw	$6,29652($16)
	slt	$2,$6,2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L290
	lw	$8,136($sp)
	.set	macro
	.set	reorder

	subu	$2,$3,$6
	li	$5,65536			# 0x10000
	addu	$4,$8,$2
	addu	$5,$16,$5
	lbu	$4,1($4)
	sll	$4,$4,1
	nor	$7,$0,$4
	addu	$3,$3,$7
	sw	$4,-27444($5)
	.set	noreorder
	.set	nomacro
	bltz	$3,$L287
	sw	$3,29660($16)
	.set	macro
	.set	reorder

	addiu	$4,$2,2
	li	$5,46524			# 0xb5bc
	addu	$2,$2,$6
	addu	$4,$8,$4
	addu	$5,$16,$5
	addu	$6,$8,$2
$L288:
	.set	noreorder
	.set	nomacro
	beq	$4,$6,$L290
	addiu	$5,$5,8432
	.set	macro
	.set	reorder

	lbu	$3,0($4)
	addiu	$4,$4,1
	sll	$3,$3,1
	nor	$2,$0,$3
	sw	$3,-8432($5)
	lw	$3,29660($16)
	addu	$2,$2,$3
	.set	noreorder
	.set	nomacro
	bgez	$2,$L288
	sw	$2,29660($16)
	.set	macro
	.set	reorder

$L287:
	lui	$6,%hi($LC29)
	lw	$25,%call16(av_log)($28)
	lw	$4,128($sp)
	li	$5,48			# 0x30
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC29)
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
$L378:
	lw	$31,204($sp)
	lw	$fp,200($sp)
	lw	$23,196($sp)
	lw	$22,192($sp)
	lw	$21,188($sp)
	lw	$20,184($sp)
	lw	$19,180($sp)
	lw	$18,176($sp)
	lw	$17,172($sp)
	lw	$16,168($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,208
	.set	macro
	.set	reorder

$L290:
	lw	$3,160($sp)
	sw	$0,0($3)
	lw	$2,29652($16)
	.set	noreorder
	.set	nomacro
	blez	$2,$L285
	lw	$22,%got(s32_decode_buffer_0)($28)
	.set	macro
	.set	reorder

	addiu	$12,$16,17200
	addiu	$17,$16,29656
	sw	$0,132($sp)
	sw	$0,124($sp)
	addiu	$fp,$22,4
	sw	$12,152($sp)
	sw	$0,120($sp)
$L347:
	lw	$3,4($17)
	lui	$13,%hi($LC30)
	lw	$8,68($17)
	addiu	$12,$17,8272
	lw	$14,132($sp)
	addiu	$6,$13,%lo($LC30)
	sll	$2,$3,3
	lw	$4,128($sp)
	lw	$9,124($sp)
	li	$5,48			# 0x30
	sra	$2,$2,$8
	lw	$25,%call16(av_log)($28)
	lw	$8,136($sp)
	lw	$7,120($sp)
	sw	$14,0($17)
	sw	$2,64($17)
	addu	$19,$8,$9
	sw	$3,16($sp)
	sw	$12,140($sp)
	lw	$3,60($17)
	lw	$2,268($4)
	sw	$9,24($sp)
	sw	$3,20($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,28($sp)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,32($sp)
	li	$6,4096			# 0x1000
	lw	$4,152($sp)
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	lw	$18,4($17)
	.set	macro
	.set	reorder

	li	$2,4			# 0x4
	lw	$7,64($17)
	andi	$6,$19,0x3
	addiu	$5,$6,3
	lw	$28,32($sp)
	subu	$2,$2,$6
	lw	$8,804($16)
	addiu	$3,$7,7
	slt	$4,$7,0
	movz	$3,$7,$4
	li	$14,935657472			# 0x37c50000
	sll	$2,$2,3
	sll	$4,$6,3
	ori	$14,$14,0x11f2
	sra	$3,$3,3
	sll	$2,$14,$2
	addu	$3,$3,$5
	sra	$4,$14,$4
	addiu	$5,$3,3
	slt	$9,$3,0
	or	$4,$2,$4
	movn	$3,$5,$9
	li	$5,16711680			# 0xff0000
	srl	$2,$4,8
	ori	$5,$5,0xff
	and	$2,$2,$5
	li	$5,-16777216			# 0xffffffffff000000
	sll	$4,$4,8
	ori	$5,$5,0xff00
	and	$5,$4,$5
	or	$5,$2,$5
	sll	$2,$5,16
	sra	$3,$3,2
	srl	$5,$5,16
	.set	noreorder
	.set	nomacro
	blez	$3,$L294
	or	$5,$5,$2
	.set	macro
	.set	reorder

	sll	$7,$3,2
	move	$2,$0
$L295:
	subu	$3,$2,$6
	addu	$4,$8,$2
	addu	$3,$19,$3
	addiu	$2,$2,4
	lw	$3,0($3)
	xor	$3,$3,$5
	.set	noreorder
	.set	nomacro
	bne	$2,$7,$L295
	sw	$3,0($4)
	.set	macro
	.set	reorder

	lw	$7,64($17)
$L294:
	addiu	$2,$7,7
	sra	$2,$2,3
	bltz	$2,$L355
	bltz	$7,$L355
	addu	$6,$8,$6
	addu	$2,$6,$2
$L296:
	move	$5,$0
	lw	$20,8272($17)
	sw	$6,24($16)
	sw	$7,36($16)
	sw	$2,28($16)
	sw	$0,32($16)
$L297:
	srl	$3,$5,3
	andi	$4,$5,0x7
	addu	$3,$6,$3
	addiu	$5,$5,1
	lbu	$2,0($3)
	sll	$2,$2,$4
	andi	$2,$2,0x00ff
	srl	$2,$2,7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L297
	sw	$5,32($16)
	.set	macro
	.set	reorder

	li	$7,-16777216			# 0xffffffffff000000
	addiu	$5,$5,-1
	move	$4,$0
	li	$8,-1			# 0xffffffffffffffff
	ori	$7,$7,0xff00
$L298:
	addiu	$5,$5,-1
	.set	noreorder
	.set	nomacro
	beq	$5,$8,$L393
	slt	$2,$4,9
	.set	macro
	.set	reorder

$L302:
	lw	$11,32($16)
	li	$13,16711680			# 0xff0000
	ori	$13,$13,0xff
	addiu	$10,$11,3
	srl	$2,$11,3
	srl	$9,$10,3
	addu	$2,$6,$2
	addu	$9,$6,$9
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $12, 3($2)  
	lwr $12, 0($2)  
	
 # 0 "" 2
#NO_APP
	sw	$10,32($16)
	srl	$3,$12,8
	sll	$2,$12,8
	lbu	$9,0($9)
	and	$3,$3,$13
	and	$2,$2,$7
	or	$2,$3,$2
	andi	$10,$10,0x7
	sll	$3,$2,16
	sll	$10,$9,$10
	srl	$2,$2,16
	andi	$10,$10,0x00ff
	or	$2,$3,$2
	addiu	$9,$11,4
	andi	$3,$11,0x7
	sll	$3,$2,$3
	srl	$10,$10,7
	sw	$9,32($16)
	.set	noreorder
	.set	nomacro
	bne	$10,$0,$L385
	srl	$3,$3,29
	.set	macro
	.set	reorder

	li	$9,-1			# 0xffffffffffffffff
$L299:
	slt	$2,$3,$4
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L298
	sll	$2,$4,2
	.set	macro
	.set	reorder

	addiu	$3,$3,1
	addu	$2,$20,$2
$L301:
	addiu	$4,$4,1
	sw	$9,0($2)
	.set	noreorder
	.set	nomacro
	bne	$4,$3,$L301
	addiu	$2,$2,4
	.set	macro
	.set	reorder

	addiu	$5,$5,-1
	.set	noreorder
	.set	nomacro
	bne	$5,$8,$L302
	slt	$2,$4,9
	.set	macro
	.set	reorder

$L393:
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L306
	lw	$25,%call16(memset)($28)
	.set	macro
	.set	reorder

	subu	$6,$0,$4
	sll	$4,$4,2
	sll	$6,$6,2
	addu	$4,$20,$4
	move	$5,$0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	addiu	$6,$6,36
	.set	macro
	.set	reorder

	lw	$28,32($sp)
$L306:
	lw	$2,8276($17)
	lw	$21,60($17)
	sw	$20,8276($17)
	.set	noreorder
	.set	nomacro
	beq	$21,$0,$L386
	sw	$2,8272($17)
	.set	macro
	.set	reorder

	addiu	$14,$sp,40
	lw	$25,%call16(memset)($28)
	move	$5,$0
	li	$6,80			# 0x50
	move	$4,$14
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$14,144($sp)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,32($sp)
	li	$6,4096			# 0x1000
	lw	$25,%call16(memset)($28)
	lw	$4,%got(s32_decode_buffer_1)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$0,0($22)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,32($sp)
	lw	$25,%call16(memset)($28)
	lw	$4,%got(s32_decode_buffer_2)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	li	$6,4096			# 0x1000
	.set	macro
	.set	reorder

	lw	$3,32($16)
	lw	$5,24($16)
	lw	$28,32($sp)
	srl	$4,$3,3
	andi	$6,$3,0x7
	addu	$4,$5,$4
	addiu	$3,$3,1
	lbu	$2,0($4)
	sll	$2,$2,$6
	andi	$2,$2,0x00ff
	srl	$2,$2,7
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L307
	sw	$3,32($16)
	.set	macro
	.set	reorder

	lw	$2,20($17)
	lui	$19,%hi(cplband)
	lw	$4,24($17)
	addiu	$19,$19,%lo(cplband)
	addiu	$2,$2,-1
	sll	$4,$4,2
	sll	$2,$2,2
	addu	$4,$19,$4
	addu	$2,$19,$2
	lw	$8,0($4)
	lw	$2,0($2)
	slt	$4,$2,$8
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L311
	subu	$2,$2,$8
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$2,$L311
	addiu	$2,$2,1
	.set	macro
	.set	reorder

	lw	$11,44($17)
	li	$9,32			# 0x20
	lw	$10,48($17)
	li	$7,-16777216			# 0xffffffffff000000
	subu	$9,$9,$11
	sll	$6,$2,2
	sll	$8,$8,2
	move	$4,$0
	ori	$7,$7,0xff00
	move	$13,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L314
	.option	pic2
	addiu	$14,$sp,40
	.set	macro
	.set	reorder

$L313:
	addu	$2,$4,$8
	addu	$13,$12,$13
	addu	$2,$14,$2
	addiu	$4,$4,4
	sw	$13,32($16)
	.set	noreorder
	.set	nomacro
	beq	$4,$6,$L311
	sw	$3,0($2)
	.set	macro
	.set	reorder

$L314:
	srl	$3,$13,3
	li	$2,16711680			# 0xff0000
	addu	$3,$5,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $12, 3($3)  
	lwr $12, 0($3)  
	
 # 0 "" 2
#NO_APP
	srl	$3,$12,8
	sll	$12,$12,8
	ori	$2,$2,0xff
	and	$3,$3,$2
	and	$12,$12,$7
	or	$12,$3,$12
	sll	$3,$12,16
	srl	$12,$12,16
	andi	$15,$13,0x7
	or	$2,$3,$12
	sll	$2,$2,$15
	srl	$2,$2,$9
	sll	$2,$2,2
	addu	$2,$10,$2
	lh	$12,2($2)
	.set	noreorder
	.set	nomacro
	bgez	$12,$L313
	lh	$3,0($2)
	.set	macro
	.set	reorder

	addu	$13,$13,$11
	srl	$2,$13,3
	andi	$20,$13,0x7
	addu	$2,$5,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $18, 3($2)  
	lwr $18, 0($2)  
	
 # 0 "" 2
#NO_APP
	li	$2,16711680			# 0xff0000
	srl	$15,$18,8
	sll	$18,$18,8
	ori	$2,$2,0xff
	and	$15,$15,$2
	and	$18,$18,$7
	or	$18,$15,$18
	sll	$15,$18,16
	srl	$18,$18,16
	or	$2,$15,$18
	sll	$2,$2,$20
	srl	$2,$2,$12
	addu	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$10,$2
	lh	$12,2($2)
	lh	$3,0($2)
	addu	$2,$4,$8
	addiu	$4,$4,4
	addu	$13,$12,$13
	addu	$2,$14,$2
	sw	$13,32($16)
	.set	noreorder
	.set	nomacro
	bne	$4,$6,$L314
	sw	$3,0($2)
	.set	macro
	.set	reorder

$L311:
	lw	$6,%got(s32_decode_buffer_0)($28)
$L392:
	move	$4,$16
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	s32_mono_decode
	.option	pic2
	move	$5,$17
	.set	macro
	.set	reorder

	lw	$18,24($17)
	.set	noreorder
	.set	nomacro
	blez	$18,$L310
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	lw	$4,%got(s32_decode_buffer_0)($28)
	move	$7,$0
	lw	$23,%got(s32_decode_buffer_1)($28)
	move	$8,$0
	lw	$31,%got(s32_decode_buffer_2)($28)
$L350:
	addu	$3,$23,$7
	addiu	$11,$4,80
	move	$2,$4
$L348:
	lwl	$10,3($2)
	lwl	$9,7($2)
	lwl	$6,11($2)
	lwl	$5,15($2)
	lwr	$10,0($2)
	lwr	$9,4($2)
	lwr	$6,8($2)
	lwr	$5,12($2)
	addiu	$2,$2,16
	swl	$10,3($3)
	swr	$10,0($3)
	swl	$9,7($3)
	swr	$9,4($3)
	swl	$6,11($3)
	swr	$6,8($3)
	swl	$5,15($3)
	swr	$5,12($3)
	.set	noreorder
	.set	nomacro
	bne	$2,$11,$L348
	addiu	$3,$3,16
	.set	macro
	.set	reorder

	addu	$3,$31,$7
	addiu	$4,$4,160
$L349:
	lwl	$10,3($2)
	lwl	$9,7($2)
	lwl	$6,11($2)
	lwl	$5,15($2)
	lwr	$10,0($2)
	lwr	$9,4($2)
	lwr	$6,8($2)
	lwr	$5,12($2)
	addiu	$2,$2,16
	swl	$10,3($3)
	swr	$10,0($3)
	swl	$9,7($3)
	swr	$9,4($3)
	swl	$6,11($3)
	swr	$6,8($3)
	swl	$5,15($3)
	swr	$5,12($3)
	.set	noreorder
	.set	nomacro
	bne	$2,$4,$L349
	addiu	$3,$3,16
	.set	macro
	.set	reorder

	lw	$18,24($17)
	addiu	$8,$8,1
	move	$4,$2
	slt	$2,$8,$18
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L350
	addiu	$7,$7,80
	.set	macro
	.set	reorder

$L310:
	lw	$8,28($17)
	li	$11,1			# 0x1
	lw	$2,20($17)
	sll	$11,$11,$8
	slt	$2,$18,$2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L317
	addiu	$11,$11,-1
	.set	macro
	.set	reorder

	sll	$2,$18,6
	lw	$23,%got(s32_decode_buffer_1)($28)
	sll	$4,$18,2
	lw	$31,%got(s32_decode_buffer_2)($28)
	sll	$24,$18,4
	sw	$16,156($sp)
	li	$25,1073676288			# 0x3fff0000
	addu	$4,$19,$4
	addu	$24,$24,$2
	move	$21,$0
	addiu	$20,$22,8
	addiu	$19,$22,12
	li	$3,3			# 0x3
	li	$2,31			# 0x1f
	li	$5,20			# 0x14
	addiu	$16,$sp,40
	ori	$25,$25,0xffff
$L319:
	lw	$10,0($4)
	addiu	$8,$8,-2
	lui	$7,%hi(s32_cplscales)
	sll	$8,$8,2
	addiu	$7,$7,%lo(s32_cplscales)
	sll	$10,$10,2
	addu	$8,$7,$8
	addu	$10,$16,$10
	addu	$6,$21,$24
	lw	$9,0($8)
	lw	$8,0($10)
	addu	$7,$23,$6
	move	$10,$0
	addu	$6,$31,$6
	subu	$11,$11,$8
	sll	$8,$8,2
	addu	$11,$11,$25
	addu	$8,$9,$8
	sll	$11,$11,2
	addu	$11,$9,$11
	lw	$9,0($8)
	lw	$8,0($11)
$L318:
	lw	$12,24($17)
	addu	$12,$18,$12
	sll	$11,$12,2
	sll	$12,$12,4
	addu	$12,$11,$12
	addu	$11,$12,$10
	sll	$11,$11,2
	addu	$12,$22,$11
	lw	$14,0($12)
#APP
 # 1605 "cook.c" 1
	S32MUL xr1,xr2,$9,$14
 # 0 "" 2
#NO_APP
	addu	$12,$fp,$11
	lw	$13,0($12)
#APP
 # 1606 "cook.c" 1
	S32MUL xr3,xr4,$9,$13
 # 0 "" 2
#NO_APP
	addu	$12,$20,$11
	lw	$12,0($12)
#APP
 # 1607 "cook.c" 1
	S32MUL xr5,xr6,$9,$12
 # 0 "" 2
#NO_APP
	addu	$11,$19,$11
	lw	$11,0($11)
#APP
 # 1608 "cook.c" 1
	S32MUL xr7,xr8,$9,$11
 # 0 "" 2
 # 1610 "cook.c" 1
	S32EXTRV xr1,xr2,$3,$2
 # 0 "" 2
 # 1611 "cook.c" 1
	S32EXTRV xr3,xr4,$3,$2
 # 0 "" 2
 # 1612 "cook.c" 1
	S32EXTRV xr5,xr6,$3,$2
 # 0 "" 2
 # 1613 "cook.c" 1
	S32EXTRV xr7,xr8,$3,$2
 # 0 "" 2
 # 1615 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1616 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1617 "cook.c" 1
	D32SLL xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1618 "cook.c" 1
	D32SAR xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1620 "cook.c" 1
	S32M2I xr1, $15
 # 0 "" 2
#NO_APP
	sw	$15,0($7)
#APP
 # 1621 "cook.c" 1
	S32M2I xr3, $15
 # 0 "" 2
#NO_APP
	sw	$15,4($7)
#APP
 # 1622 "cook.c" 1
	S32M2I xr5, $15
 # 0 "" 2
#NO_APP
	sw	$15,8($7)
#APP
 # 1623 "cook.c" 1
	S32M2I xr7, $15
 # 0 "" 2
#NO_APP
	sw	$15,12($7)
#APP
 # 1626 "cook.c" 1
	S32MUL xr1,xr2,$8,$14
 # 0 "" 2
 # 1627 "cook.c" 1
	S32MUL xr3,xr4,$8,$13
 # 0 "" 2
 # 1628 "cook.c" 1
	S32MUL xr5,xr6,$8,$12
 # 0 "" 2
 # 1629 "cook.c" 1
	S32MUL xr7,xr8,$8,$11
 # 0 "" 2
 # 1631 "cook.c" 1
	S32EXTRV xr1,xr2,$3,$2
 # 0 "" 2
 # 1632 "cook.c" 1
	S32EXTRV xr3,xr4,$3,$2
 # 0 "" 2
 # 1633 "cook.c" 1
	S32EXTRV xr5,xr6,$3,$2
 # 0 "" 2
 # 1634 "cook.c" 1
	S32EXTRV xr7,xr8,$3,$2
 # 0 "" 2
 # 1636 "cook.c" 1
	D32SLL xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1637 "cook.c" 1
	D32SAR xr1,xr1,xr3,xr3,1
 # 0 "" 2
 # 1638 "cook.c" 1
	D32SLL xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1639 "cook.c" 1
	D32SAR xr5,xr5,xr7,xr7,1
 # 0 "" 2
 # 1641 "cook.c" 1
	S32M2I xr1, $11
 # 0 "" 2
#NO_APP
	sw	$11,0($6)
#APP
 # 1642 "cook.c" 1
	S32M2I xr3, $11
 # 0 "" 2
#NO_APP
	sw	$11,4($6)
#APP
 # 1643 "cook.c" 1
	S32M2I xr5, $11
 # 0 "" 2
#NO_APP
	sw	$11,8($6)
#APP
 # 1644 "cook.c" 1
	S32M2I xr7, $11
 # 0 "" 2
#NO_APP
	addiu	$10,$10,4
	sw	$11,12($6)
	addiu	$7,$7,16
	.set	noreorder
	.set	nomacro
	bne	$10,$5,$L318
	addiu	$6,$6,16
	.set	macro
	.set	reorder

	lw	$8,28($17)
	li	$9,1			# 0x1
	lw	$6,20($17)
	addiu	$18,$18,1
	addiu	$4,$4,4
	sll	$11,$9,$8
	slt	$6,$18,$6
	addiu	$21,$21,80
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L319
	addiu	$11,$11,-1
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L317
	.option	pic2
	lw	$16,156($sp)
	.set	macro
	.set	reorder

$L385:
	srl	$2,$9,3
	andi	$9,$9,0x7
	addu	$2,$6,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $10, 3($2)  
	lwr $10, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$10,8
	sll	$12,$10,8
	and	$10,$2,$13
	and	$2,$12,$7
	or	$2,$10,$2
	sll	$10,$2,16
	srl	$2,$2,16
	addiu	$11,$11,8
	or	$2,$10,$2
	sll	$9,$2,$9
	sw	$11,32($16)
	srl	$9,$9,28
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L299
	.option	pic2
	addiu	$9,$9,-7
	.set	macro
	.set	reorder

$L355:
	move	$2,$0
	move	$7,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L296
	.option	pic2
	move	$6,$0
	.set	macro
	.set	reorder

$L386:
	lw	$6,%got(s32_decode_buffer_1)($28)
	move	$4,$16
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	s32_mono_decode
	.option	pic2
	move	$5,$17
	.set	macro
	.set	reorder

	li	$2,2			# 0x2
	lw	$3,8($17)
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L387
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L317:
	lw	$5,%got(s32_decode_buffer_1)($28)
	move	$4,$16
	lw	$7,%got(s32_mono_previous_buffer1)($28)
	lw	$6,140($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	s32_imlt_gain.isra.12
	.option	pic2
	lw	$18,0($17)
	.set	macro
	.set	reorder

	lw	$3,56($16)
	lw	$28,32($sp)
	addiu	$2,$3,2252
	sll	$2,$2,2
	.set	noreorder
	.set	nomacro
	blez	$3,$L339
	addu	$5,$16,$2
	.set	macro
	.set	reorder

	lw	$9,148($sp)
	sll	$6,$18,1
	lw	$8,40($16)
	sll	$3,$3,2
	li	$10,32768			# 0x8000
	addu	$6,$9,$6
	sll	$8,$8,1
	addu	$7,$5,$3
	li	$9,-65536			# 0xffffffffffff0000
$L338:
	lw	$3,0($5)
	addiu	$5,$5,4
	addiu	$2,$3,512
	addiu	$3,$3,639
	slt	$4,$2,0
	movn	$2,$3,$4
	sra	$4,$2,7
	sra	$2,$2,31
	addu	$3,$4,$10
	xori	$2,$2,0x7fff
	and	$3,$3,$9
	movz	$2,$4,$3
	sh	$2,0($6)
	.set	noreorder
	.set	nomacro
	bne	$5,$7,$L338
	addu	$6,$6,$8
	.set	macro
	.set	reorder

$L339:
	lw	$2,8($17)
	li	$3,2			# 0x2
	beq	$2,$3,$L388
$L336:
	lw	$3,4($17)
	lui	$14,%hi($LC31)
	lw	$12,132($sp)
	li	$5,48			# 0x30
	lw	$7,120($sp)
	addiu	$6,$14,%lo($LC31)
	sll	$8,$3,3
	lw	$9,124($sp)
	addu	$12,$12,$2
	lw	$25,%call16(av_log)($28)
	addiu	$13,$7,1
	lw	$4,128($sp)
	sw	$8,16($sp)
	addu	$9,$9,$3
	sw	$12,132($sp)
	addiu	$17,$17,8432
	sw	$13,120($sp)
	sw	$9,124($sp)
	lw	$2,32($16)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,20($sp)
	.set	macro
	.set	reorder

	lw	$2,29652($16)
	lw	$14,120($sp)
	slt	$2,$14,$2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L347
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L285:
	lw	$3,56($16)
	lw	$2,40($16)
	lw	$8,128($sp)
	lw	$9,160($sp)
	mul	$2,$2,$3
	lw	$3,160($sp)
	lw	$31,204($sp)
	lw	$fp,200($sp)
	lw	$23,196($sp)
	lw	$22,192($sp)
	lw	$21,188($sp)
	lw	$20,184($sp)
	sll	$2,$2,1
	lw	$19,180($sp)
	lw	$18,176($sp)
	lw	$17,172($sp)
	sw	$2,0($3)
	lw	$3,80($8)
	lw	$16,168($sp)
	slt	$3,$3,2
	movn	$2,$0,$3
	sw	$2,0($9)
	lw	$2,268($8)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,208
	.set	macro
	.set	reorder

$L388:
	lw	$2,60($17)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L389
	lw	$5,%got(s32_decode_buffer_2)($28)
	.set	macro
	.set	reorder

	lw	$7,%got(s32_mono_previous_buffer2)($28)
	addiu	$6,$17,8280
	move	$4,$16
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	s32_imlt_gain.isra.12
	.option	pic2
	lw	$18,0($17)
	.set	macro
	.set	reorder

	lw	$2,56($16)
	lw	$28,32($sp)
	addiu	$5,$2,2252
	sll	$5,$5,2
	.set	noreorder
	.set	nomacro
	blez	$2,$L381
	addu	$5,$16,$5
	.set	macro
	.set	reorder

	lw	$8,40($16)
	sll	$6,$18,1
	lw	$13,148($sp)
	sll	$2,$2,2
	li	$10,32768			# 0x8000
	sll	$8,$8,1
	addu	$6,$13,$6
	addu	$7,$5,$2
	li	$9,-65536			# 0xffffffffffff0000
$L346:
	lw	$3,0($5)
	addiu	$5,$5,4
	addiu	$2,$3,512
	addiu	$3,$3,639
	slt	$4,$2,0
	movn	$2,$3,$4
	sra	$4,$2,7
	sra	$2,$2,31
	addu	$3,$4,$10
	xori	$2,$2,0x7fff
	and	$3,$3,$9
	movz	$2,$4,$3
	sh	$2,2($6)
	.set	noreorder
	.set	nomacro
	bne	$5,$7,$L346
	addu	$6,$6,$8
	.set	macro
	.set	reorder

$L381:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L336
	.option	pic2
	lw	$2,8($17)
	.set	macro
	.set	reorder

$L307:
	lw	$2,20($17)
	lui	$19,%hi(cplband)
	lw	$4,24($17)
	addiu	$19,$19,%lo(cplband)
	addiu	$2,$2,-1
	sll	$4,$4,2
	sll	$2,$2,2
	addu	$4,$19,$4
	addu	$2,$19,$2
	lw	$8,0($4)
	lw	$2,0($2)
	slt	$4,$2,$8
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L311
	subu	$2,$2,$8
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$2,$L311
	addiu	$2,$2,1
	.set	macro
	.set	reorder

	lw	$11,28($17)
	li	$10,32			# 0x20
	li	$9,-16777216			# 0xffffffffff000000
	subu	$10,$10,$11
	sll	$7,$2,2
	sll	$8,$8,2
	move	$6,$0
	addiu	$12,$sp,40
	ori	$9,$9,0xff00
$L315:
	srl	$4,$3,3
	andi	$13,$3,0x7
	addu	$4,$5,$4
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $14, 3($4)  
	lwr $14, 0($4)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$14,8
	sll	$4,$14,8
	li	$14,16711680			# 0xff0000
	and	$4,$4,$9
	ori	$14,$14,0xff
	and	$2,$2,$14
	or	$2,$2,$4
	sll	$4,$2,16
	srl	$2,$2,16
	addu	$14,$6,$8
	or	$2,$4,$2
	sll	$2,$2,$13
	addu	$3,$3,$11
	srl	$2,$2,$10
	addu	$4,$12,$14
	addiu	$6,$6,4
	sw	$3,32($16)
	.set	noreorder
	.set	nomacro
	bne	$6,$7,$L315
	sw	$2,0($4)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L392
	.option	pic2
	lw	$6,%got(s32_decode_buffer_0)($28)
	.set	macro
	.set	reorder

$L389:
	lw	$7,%got(s32_mono_previous_buffer2)($28)
	move	$4,$16
	lw	$6,140($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	s32_imlt_gain.isra.12
	.option	pic2
	lw	$18,0($17)
	.set	macro
	.set	reorder

	lw	$2,56($16)
	lw	$28,32($sp)
	addiu	$5,$2,2252
	sll	$5,$5,2
	.set	noreorder
	.set	nomacro
	blez	$2,$L381
	addu	$5,$16,$5
	.set	macro
	.set	reorder

	lw	$8,40($16)
	sll	$6,$18,1
	lw	$12,148($sp)
	sll	$2,$2,2
	li	$10,32768			# 0x8000
	sll	$8,$8,1
	addu	$6,$12,$6
	addu	$7,$5,$2
	li	$9,-65536			# 0xffffffffffff0000
$L343:
	lw	$3,0($5)
	addiu	$5,$5,4
	addiu	$2,$3,512
	addiu	$3,$3,639
	slt	$4,$2,0
	movn	$2,$3,$4
	sra	$4,$2,7
	sra	$2,$2,31
	addu	$3,$4,$10
	xori	$2,$2,0x7fff
	and	$3,$3,$9
	movz	$2,$4,$3
	sh	$2,2($6)
	.set	noreorder
	.set	nomacro
	bne	$5,$7,$L343
	addu	$6,$6,$8
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L336
	.option	pic2
	lw	$2,8($17)
	.set	macro
	.set	reorder

$L387:
	lw	$5,64($17)
	srl	$3,$18,31
	li	$12,935657472			# 0x37c50000
	lw	$4,804($16)
	addu	$3,$3,$18
	addiu	$6,$5,7
	slt	$2,$5,0
	sra	$3,$3,1
	movz	$6,$5,$2
	ori	$12,$12,0x11f2
	addu	$19,$19,$3
	li	$3,4			# 0x4
	move	$2,$6
	andi	$6,$19,0x3
	addiu	$8,$6,3
	subu	$3,$3,$6
	sra	$2,$2,3
	sll	$3,$3,3
	addu	$2,$2,$8
	sll	$7,$6,3
	addiu	$8,$2,3
	sra	$7,$12,$7
	sll	$3,$12,$3
	slt	$9,$2,0
	or	$3,$3,$7
	movn	$2,$8,$9
	li	$13,16711680			# 0xff0000
	li	$8,-16777216			# 0xffffffffff000000
	srl	$7,$3,8
	sll	$3,$3,8
	ori	$8,$8,0xff00
	ori	$13,$13,0xff
	and	$7,$7,$13
	and	$3,$3,$8
	or	$3,$7,$3
	sll	$8,$3,16
	srl	$7,$3,16
	sra	$2,$2,2
	.set	noreorder
	.set	nomacro
	blez	$2,$L323
	or	$7,$7,$8
	.set	macro
	.set	reorder

	sll	$5,$2,2
	move	$2,$0
$L324:
	subu	$3,$2,$6
	addu	$8,$4,$2
	addu	$3,$19,$3
	addiu	$2,$2,4
	lw	$3,0($3)
	xor	$3,$3,$7
	.set	noreorder
	.set	nomacro
	bne	$2,$5,$L324
	sw	$3,0($8)
	.set	macro
	.set	reorder

	lw	$5,64($17)
$L323:
	addiu	$2,$5,7
	sra	$2,$2,3
	bltz	$2,$L359
	bltz	$5,$L359
	addu	$6,$4,$6
	addu	$2,$6,$2
$L325:
	lw	$18,8280($17)
	sw	$6,24($16)
	sw	$5,36($16)
	sw	$2,28($16)
	sw	$0,32($16)
$L326:
	srl	$3,$21,3
	andi	$4,$21,0x7
	addu	$3,$6,$3
	addiu	$21,$21,1
	lbu	$2,0($3)
	sll	$2,$2,$4
	andi	$2,$2,0x00ff
	srl	$2,$2,7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L326
	sw	$21,32($16)
	.set	macro
	.set	reorder

	li	$7,-16777216			# 0xffffffffff000000
	addiu	$5,$21,-1
	move	$4,$0
	li	$8,-1			# 0xffffffffffffffff
	ori	$7,$7,0xff00
$L327:
	addiu	$5,$5,-1
	.set	noreorder
	.set	nomacro
	beq	$5,$8,$L394
	slt	$2,$4,9
	.set	macro
	.set	reorder

$L331:
	lw	$10,32($16)
	li	$14,16711680			# 0xff0000
	ori	$14,$14,0xff
	addiu	$11,$10,3
	srl	$2,$10,3
	srl	$12,$11,3
	addu	$2,$6,$2
	addu	$12,$6,$12
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($2)  
	lwr $3, 0($2)  
	
 # 0 "" 2
#NO_APP
	sw	$11,32($16)
	srl	$2,$3,8
	sll	$9,$3,8
	lbu	$3,0($12)
	and	$2,$2,$14
	and	$9,$9,$7
	or	$2,$2,$9
	andi	$9,$11,0x7
	sll	$9,$3,$9
	sll	$11,$2,16
	srl	$2,$2,16
	andi	$3,$10,0x7
	or	$2,$11,$2
	andi	$9,$9,0x00ff
	addiu	$11,$10,4
	sll	$3,$2,$3
	srl	$2,$9,7
	srl	$3,$3,29
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L391
	sw	$11,32($16)
	.set	macro
	.set	reorder

	li	$9,-1			# 0xffffffffffffffff
$L328:
	slt	$2,$3,$4
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L327
	sll	$2,$4,2
	.set	macro
	.set	reorder

	addiu	$3,$3,1
	addu	$2,$18,$2
$L330:
	addiu	$4,$4,1
	sw	$9,0($2)
	.set	noreorder
	.set	nomacro
	bne	$4,$3,$L330
	addiu	$2,$2,4
	.set	macro
	.set	reorder

	addiu	$5,$5,-1
	.set	noreorder
	.set	nomacro
	bne	$5,$8,$L331
	slt	$2,$4,9
	.set	macro
	.set	reorder

$L394:
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L333
	subu	$6,$0,$4
	.set	macro
	.set	reorder

	lw	$25,%call16(memset)($28)
	sll	$4,$4,2
	sll	$6,$6,2
	addu	$4,$18,$4
	move	$5,$0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	addiu	$6,$6,36
	.set	macro
	.set	reorder

	lw	$28,32($sp)
$L333:
	lw	$2,8284($17)
	move	$4,$16
	lw	$6,%got(s32_decode_buffer_2)($28)
	move	$5,$17
	sw	$18,8284($17)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	s32_mono_decode
	.option	pic2
	sw	$2,8280($17)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L317
	.option	pic2
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L391:
	srl	$2,$11,3
	andi	$9,$11,0x7
	addu	$2,$6,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $11, 3($2)  
	lwr $11, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$11,8
	sll	$11,$11,8
	and	$2,$2,$14
	and	$11,$11,$7
	or	$11,$2,$11
	sll	$2,$11,16
	srl	$11,$11,16
	addiu	$10,$10,8
	or	$2,$2,$11
	sll	$9,$2,$9
	sw	$10,32($16)
	srl	$9,$9,28
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L328
	.option	pic2
	addiu	$9,$9,-7
	.set	macro
	.set	reorder

$L359:
	move	$2,$0
	move	$5,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L325
	.option	pic2
	move	$6,$0
	.set	macro
	.set	reorder

	.end	cook_decode_frame
	.size	cook_decode_frame, .-cook_decode_frame
	.globl	cook_decoder
	.section	.rodata.str1.4
	.align	2
$LC32:
	.ascii	"cook\000"
	.align	2
$LC33:
	.ascii	"COOK\000"
	.section	.data.rel.local,"aw",@progbits
	.align	2
	.type	cook_decoder, @object
	.size	cook_decoder, 72
cook_decoder:
	.word	$LC32
	.word	1
	.word	86038
	.word	71824
	.word	cook_decode_init
	.space	4
	.word	cook_decode_close
	.word	cook_decode_frame
	.space	20
	.word	$LC33
	.space	16
	.globl	s32_mlt_window
	.section	.bss,"aw",@nobits
	.align	2
	.type	s32_mlt_window, @object
	.size	s32_mlt_window, 4
s32_mlt_window:
	.space	4

	.comm	s32_mono_previous_buffer2,4096,4

	.comm	s32_mono_previous_buffer1,4096,4

	.comm	s32_decode_buffer_2,4096,4

	.comm	s32_decode_buffer_1,4096,4

	.comm	s32_decode_buffer_0,4240,4
	.local	s32_gain_table_L20
	.comm	s32_gain_table_L20,92,4
	.local	rootpow2tab_sft
	.comm	rootpow2tab_sft,508,4
	.local	pow2tab_sft
	.comm	pow2tab_sft,508,4
	.local	sp32_rootpow2tab
	.comm	sp32_rootpow2tab,4,4
	.local	sp32_pow2tab
	.comm	sp32_pow2tab,4,4
	.local	s32_rootpow2tab
	.comm	s32_rootpow2tab,508,4
	.local	s32_pow2tab
	.comm	s32_pow2tab,508,4
	.local	rootpow2tab
	.comm	rootpow2tab,508,4
	.local	pow2tab
	.comm	pow2tab,508,4

	.comm	mpFrame,4,4
	.section	.data.rel.ro.local,"aw",@progbits
	.align	2
	.type	s32_cplscales, @object
	.size	s32_cplscales, 20
s32_cplscales:
	.word	s32_cplscale2
	.word	s32_cplscale3
	.word	s32_cplscale4
	.word	s32_cplscale5
	.word	s32_cplscale6
	.local	s32_cplscale6
	.comm	s32_cplscale6,252,4
	.local	s32_cplscale5
	.comm	s32_cplscale5,124,4
	.local	s32_cplscale4
	.comm	s32_cplscale4,60,4
	.local	s32_cplscale3
	.comm	s32_cplscale3,28,4
	.local	s32_cplscale2
	.comm	s32_cplscale2,12,4
	.rdata
	.align	2
	.type	cplscale6, @object
	.size	cplscale6, 252
cplscale6:
	.word	1065319761
	.word	1065251827
	.word	1065182466
	.word	1065111600
	.word	1065039143
	.word	1064965003
	.word	1064889075
	.word	1064811247
	.word	1064731393
	.word	1064649374
	.word	1064565033
	.word	1064478195
	.word	1064388663
	.word	1064296211
	.word	1064200583
	.word	1064101483
	.word	1063998567
	.word	1063891433
	.word	1063779602
	.word	1063662502
	.word	1063539437
	.word	1063409546
	.word	1063271742
	.word	1063124626
	.word	1062966336
	.word	1062794311
	.word	1062604851
	.word	1062392261
	.word	1062146926
	.word	1061850035
	.word	1061452850
	.word	1060439283
	.word	1059330613
	.word	1058836359
	.word	1058440327
	.word	1058093664
	.word	1057777431
	.word	1057481875
	.word	1057201149
	.word	1056898162
	.word	1056375058
	.word	1055864240
	.word	1055362440
	.word	1054866959
	.word	1054375480
	.word	1053885931
	.word	1053396388
	.word	1052904994
	.word	1052409892
	.word	1051909157
	.word	1051400719
	.word	1050882282
	.word	1050351212
	.word	1049804384
	.word	1049237972
	.word	1048647116
	.word	1047474786
	.word	1046151803
	.word	1044723111
	.word	1043147023
	.word	1041348867
	.word	1038143560
	.word	1031881976
	.align	2
	.type	cplscale5, @object
	.size	cplscale5, 124
cplscale5:
	.word	1065284872
	.word	1065143796
	.word	1064996305
	.word	1064841607
	.word	1064678735
	.word	1064506491
	.word	1064323367
	.word	1064127408
	.word	1063916016
	.word	1063685609
	.word	1063430999
	.word	1063144191
	.word	1062811650
	.word	1062406831
	.word	1061860760
	.word	1060439283
	.word	1058822469
	.word	1058072478
	.word	1057454133
	.word	1056829841
	.word	1055782772
	.word	1054771589
	.word	1053775456
	.word	1052777657
	.word	1051762437
	.word	1050712489
	.word	1049605867
	.word	1048245065
	.word	1045567478
	.word	1042369025
	.word	1035512590
	.align	2
	.type	cplscale4, @object
	.size	cplscale4, 60
cplscale4:
	.word	1065210387
	.word	1064904409
	.word	1064565033
	.word	1064181051
	.word	1063733430
	.word	1063184697
	.word	1062437008
	.word	1060439283
	.word	1058028377
	.word	1056687205
	.word	1054571728
	.word	1052509300
	.word	1050351212
	.word	1047217240
	.word	1040536888
	.align	2
	.type	cplscale3, @object
	.size	cplscale3, 28
cplscale3:
	.word	1065039143
	.word	1064296211
	.word	1063271742
	.word	1060439283
	.word	1056375058
	.word	1051909157
	.word	1044723111
	.align	2
	.type	cplscale2, @object
	.size	cplscale2, 12
cplscale2:
	.word	1064565033
	.word	1060439283
	.word	1050351212
	.align	2
	.type	cplband, @object
	.size	cplband, 204
cplband:
	.word	0
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	11
	.word	12
	.word	12
	.word	13
	.word	13
	.word	14
	.word	14
	.word	14
	.word	15
	.word	15
	.word	15
	.word	15
	.word	16
	.word	16
	.word	16
	.word	16
	.word	16
	.word	17
	.word	17
	.word	17
	.word	17
	.word	17
	.word	17
	.word	18
	.word	18
	.word	18
	.word	18
	.word	18
	.word	18
	.word	18
	.word	19
	.word	19
	.word	19
	.word	19
	.word	19
	.word	19
	.word	19
	.word	19
	.word	19
	.section	.data.rel.ro.local
	.align	2
	.type	ccpl_huffbits, @object
	.size	ccpl_huffbits, 20
ccpl_huffbits:
	.word	ccpl_huffbits2
	.word	ccpl_huffbits3
	.word	ccpl_huffbits4
	.word	ccpl_huffbits5
	.word	ccpl_huffbits6
	.align	2
	.type	ccpl_huffcodes, @object
	.size	ccpl_huffcodes, 20
ccpl_huffcodes:
	.word	ccpl_huffcodes2
	.word	ccpl_huffcodes3
	.word	ccpl_huffcodes4
	.word	ccpl_huffcodes5
	.word	ccpl_huffcodes6
	.rdata
	.align	2
	.type	ccpl_huffbits6, @object
	.size	ccpl_huffbits6, 63
ccpl_huffbits6:
	.byte	16
	.byte	15
	.byte	14
	.byte	13
	.byte	12
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	5
	.byte	5
	.byte	3
	.byte	1
	.byte	4
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	14
	.byte	16
	.align	2
	.type	ccpl_huffbits5, @object
	.size	ccpl_huffbits5, 31
ccpl_huffbits5:
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	9
	.byte	9
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	5
	.byte	5
	.byte	3
	.byte	1
	.byte	3
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.align	2
	.type	ccpl_huffbits4, @object
	.size	ccpl_huffbits4, 15
ccpl_huffbits4:
	.byte	8
	.byte	8
	.byte	7
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	1
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	8
	.align	2
	.type	ccpl_huffbits3, @object
	.size	ccpl_huffbits3, 7
ccpl_huffbits3:
	.byte	6
	.byte	5
	.byte	2
	.byte	1
	.byte	3
	.byte	4
	.byte	6
	.align	2
	.type	ccpl_huffbits2, @object
	.size	ccpl_huffbits2, 3
ccpl_huffbits2:
	.byte	2
	.byte	1
	.byte	2
	.align	2
	.type	ccpl_huffcodes6, @object
	.size	ccpl_huffcodes6, 126
ccpl_huffcodes6:
	.half	4
	.half	5
	.half	5
	.half	6
	.half	6
	.half	7
	.half	7
	.half	7
	.half	7
	.half	8
	.half	8
	.half	8
	.half	8
	.half	9
	.half	9
	.half	9
	.half	9
	.half	10
	.half	10
	.half	10
	.half	10
	.half	10
	.half	11
	.half	11
	.half	11
	.half	11
	.half	12
	.half	13
	.half	14
	.half	14
	.half	16
	.half	0
	.half	10
	.half	24
	.half	25
	.half	54
	.half	55
	.half	116
	.half	117
	.half	118
	.half	119
	.half	244
	.half	245
	.half	246
	.half	247
	.half	501
	.half	502
	.half	503
	.half	504
	.half	1014
	.half	1015
	.half	1016
	.half	1017
	.half	1018
	.half	2042
	.half	2043
	.half	2044
	.half	2045
	.half	4093
	.half	8189
	.half	16381
	.half	16382
	.half	-1
	.align	2
	.type	ccpl_huffcodes5, @object
	.size	ccpl_huffcodes5, 62
ccpl_huffcodes5:
	.half	1016
	.half	1017
	.half	1018
	.half	1019
	.half	504
	.half	505
	.half	248
	.half	249
	.half	120
	.half	121
	.half	56
	.half	57
	.half	24
	.half	25
	.half	4
	.half	0
	.half	5
	.half	26
	.half	27
	.half	58
	.half	59
	.half	122
	.half	123
	.half	250
	.half	251
	.half	506
	.half	507
	.half	1020
	.half	1021
	.half	1022
	.half	1023
	.align	2
	.type	ccpl_huffcodes4, @object
	.size	ccpl_huffcodes4, 30
ccpl_huffcodes4:
	.half	252
	.half	253
	.half	124
	.half	60
	.half	28
	.half	12
	.half	4
	.half	0
	.half	5
	.half	13
	.half	29
	.half	61
	.half	125
	.half	254
	.half	255
	.align	2
	.type	ccpl_huffcodes3, @object
	.size	ccpl_huffcodes3, 14
ccpl_huffcodes3:
	.half	62
	.half	30
	.half	2
	.half	0
	.half	6
	.half	14
	.half	63
	.align	2
	.type	ccpl_huffcodes2, @object
	.size	ccpl_huffcodes2, 6
ccpl_huffcodes2:
	.half	2
	.half	0
	.half	3
	.section	.data.rel.ro.local
	.align	2
	.type	cvh_huffbits, @object
	.size	cvh_huffbits, 28
cvh_huffbits:
	.word	cvh_huffbits0
	.word	cvh_huffbits1
	.word	cvh_huffbits2
	.word	cvh_huffbits3
	.word	cvh_huffbits4
	.word	cvh_huffbits5
	.word	cvh_huffbits6
	.align	2
	.type	cvh_huffcodes, @object
	.size	cvh_huffcodes, 28
cvh_huffcodes:
	.word	cvh_huffcodes0
	.word	cvh_huffcodes1
	.word	cvh_huffcodes2
	.word	cvh_huffcodes3
	.word	cvh_huffcodes4
	.word	cvh_huffcodes5
	.word	cvh_huffcodes6
	.rdata
	.align	2
	.type	cvh_huffcodes6, @object
	.size	cvh_huffcodes6, 64
cvh_huffcodes6:
	.half	0
	.half	8
	.half	9
	.half	52
	.half	10
	.half	53
	.half	54
	.half	246
	.half	11
	.half	55
	.half	56
	.half	247
	.half	57
	.half	506
	.half	248
	.half	1020
	.half	12
	.half	58
	.half	122
	.half	249
	.half	59
	.half	507
	.half	250
	.half	2046
	.half	60
	.half	508
	.half	251
	.half	1021
	.half	252
	.half	1022
	.half	509
	.half	2047
	.align	2
	.type	cvh_huffbits6, @object
	.size	cvh_huffbits6, 32
cvh_huffbits6:
	.byte	1
	.byte	4
	.byte	4
	.byte	6
	.byte	4
	.byte	6
	.byte	6
	.byte	8
	.byte	4
	.byte	6
	.byte	6
	.byte	8
	.byte	6
	.byte	9
	.byte	8
	.byte	10
	.byte	4
	.byte	6
	.byte	7
	.byte	8
	.byte	6
	.byte	9
	.byte	8
	.byte	11
	.byte	6
	.byte	9
	.byte	8
	.byte	10
	.byte	8
	.byte	10
	.byte	9
	.byte	11
	.align	2
	.type	cvh_huffcodes5, @object
	.size	cvh_huffcodes5, 460
cvh_huffcodes5:
	.half	0
	.half	4
	.half	240
	.half	5
	.half	18
	.half	496
	.half	497
	.half	1000
	.half	16334
	.half	6
	.half	48
	.half	2014
	.half	19
	.half	49
	.half	4050
	.half	1001
	.half	2015
	.half	32688
	.half	498
	.half	2016
	.half	32689
	.half	1002
	.half	8146
	.half	32690
	.half	16335
	.half	32691
	.half	49
	.half	7
	.half	50
	.half	4051
	.half	51
	.half	112
	.half	4052
	.half	4053
	.half	4054
	.half	32692
	.half	20
	.half	113
	.half	8147
	.half	52
	.half	114
	.half	8148
	.half	4055
	.half	8149
	.half	32693
	.half	1003
	.half	4056
	.half	32694
	.half	2017
	.half	8150
	.half	32695
	.half	32696
	.half	32697
	.half	114
	.half	241
	.half	8151
	.half	32698
	.half	2018
	.half	4057
	.half	32699
	.half	32700
	.half	32701
	.half	112
	.half	1004
	.half	8152
	.half	32702
	.half	4058
	.half	32703
	.half	32704
	.half	32705
	.half	32706
	.half	114
	.half	32707
	.half	32708
	.half	113
	.half	32709
	.half	32710
	.half	114
	.half	52
	.half	114
	.half	114
	.half	8
	.half	21
	.half	2019
	.half	22
	.half	115
	.half	4059
	.half	2020
	.half	4060
	.half	32711
	.half	53
	.half	116
	.half	8153
	.half	117
	.half	242
	.half	16336
	.half	4061
	.half	16337
	.half	32712
	.half	2021
	.half	8154
	.half	32713
	.half	4062
	.half	8155
	.half	32714
	.half	32715
	.half	32716
	.half	242
	.half	23
	.half	54
	.half	8156
	.half	118
	.half	243
	.half	32717
	.half	4063
	.half	16338
	.half	32718
	.half	55
	.half	244
	.half	16339
	.half	119
	.half	245
	.half	32719
	.half	16340
	.half	32720
	.half	32721
	.half	4064
	.half	4065
	.half	32722
	.half	4066
	.half	8157
	.half	32723
	.half	32724
	.half	32725
	.half	245
	.half	499
	.half	8158
	.half	32726
	.half	4067
	.half	8159
	.half	32727
	.half	32728
	.half	32729
	.half	243
	.half	2022
	.half	8160
	.half	32730
	.half	8161
	.half	8162
	.half	32731
	.half	32732
	.half	32733
	.half	245
	.half	16341
	.half	32734
	.half	244
	.half	32735
	.half	32736
	.half	245
	.half	119
	.half	245
	.half	245
	.half	246
	.half	1005
	.half	32737
	.half	2023
	.half	4068
	.half	32738
	.half	32739
	.half	32740
	.half	115
	.half	1006
	.half	4069
	.half	32741
	.half	4070
	.half	8163
	.half	32742
	.half	32743
	.half	32744
	.half	242
	.half	16342
	.half	32745
	.half	116
	.half	32746
	.half	32747
	.half	242
	.half	117
	.half	242
	.half	242
	.half	247
	.half	4071
	.half	32748
	.half	4072
	.half	8164
	.half	32749
	.half	32750
	.half	32751
	.half	243
	.half	2024
	.half	8165
	.half	32752
	.half	8166
	.half	32753
	.half	32754
	.half	32755
	.half	32756
	.half	245
	.half	32757
	.half	32758
	.half	244
	.half	32759
	.half	32760
	.half	245
	.half	119
	.half	245
	.half	245
	.half	16343
	.half	32761
	.half	54
	.half	32762
	.half	32763
	.half	243
	.half	118
	.half	243
	.half	243
	.half	32764
	.half	32765
	.half	0
	.half	32766
	.half	32767
	.align	2
	.type	cvh_huffbits5, @object
	.size	cvh_huffbits5, 230
cvh_huffbits5:
	.byte	2
	.byte	4
	.byte	8
	.byte	4
	.byte	5
	.byte	9
	.byte	9
	.byte	10
	.byte	14
	.byte	4
	.byte	6
	.byte	11
	.byte	5
	.byte	6
	.byte	12
	.byte	10
	.byte	11
	.byte	15
	.byte	9
	.byte	11
	.byte	15
	.byte	10
	.byte	13
	.byte	15
	.byte	14
	.byte	15
	.byte	0
	.byte	4
	.byte	6
	.byte	12
	.byte	6
	.byte	7
	.byte	12
	.byte	12
	.byte	12
	.byte	15
	.byte	5
	.byte	7
	.byte	13
	.byte	6
	.byte	7
	.byte	13
	.byte	12
	.byte	13
	.byte	15
	.byte	10
	.byte	12
	.byte	15
	.byte	11
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	8
	.byte	13
	.byte	15
	.byte	11
	.byte	12
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	10
	.byte	13
	.byte	15
	.byte	12
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	5
	.byte	11
	.byte	5
	.byte	7
	.byte	12
	.byte	11
	.byte	12
	.byte	15
	.byte	6
	.byte	7
	.byte	13
	.byte	7
	.byte	8
	.byte	14
	.byte	12
	.byte	14
	.byte	15
	.byte	11
	.byte	13
	.byte	15
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	5
	.byte	6
	.byte	13
	.byte	7
	.byte	8
	.byte	15
	.byte	12
	.byte	14
	.byte	15
	.byte	6
	.byte	8
	.byte	14
	.byte	7
	.byte	8
	.byte	15
	.byte	14
	.byte	15
	.byte	15
	.byte	12
	.byte	12
	.byte	15
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	9
	.byte	13
	.byte	15
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	11
	.byte	13
	.byte	15
	.byte	13
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	14
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	10
	.byte	15
	.byte	11
	.byte	12
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	10
	.byte	12
	.byte	15
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	14
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	12
	.byte	15
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	11
	.byte	13
	.byte	15
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	14
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.align	2
	.type	cvh_huffcodes4, @object
	.size	cvh_huffcodes4, 492
cvh_huffcodes4:
	.half	0
	.half	4
	.half	108
	.half	998
	.half	5
	.half	18
	.half	109
	.half	999
	.half	110
	.half	232
	.half	1000
	.half	16324
	.half	2016
	.half	2017
	.half	32676
	.half	32677
	.half	6
	.half	19
	.half	482
	.half	4058
	.half	20
	.half	21
	.half	233
	.half	4059
	.half	234
	.half	111
	.half	1001
	.half	32678
	.half	2018
	.half	2019
	.half	32679
	.half	32680
	.half	112
	.half	483
	.half	4060
	.half	32681
	.half	235
	.half	236
	.half	4061
	.half	32682
	.half	1002
	.half	1003
	.half	8150
	.half	32683
	.half	16325
	.half	16326
	.half	32684
	.half	8150
	.half	2020
	.half	8151
	.half	32685
	.half	32686
	.half	2021
	.half	8152
	.half	32687
	.half	32688
	.half	16327
	.half	32689
	.half	32690
	.half	8150
	.half	32691
	.half	32692
	.half	8150
	.half	8150
	.half	7
	.half	22
	.half	484
	.half	8153
	.half	23
	.half	50
	.half	485
	.half	8154
	.half	486
	.half	487
	.half	2022
	.half	32693
	.half	16328
	.half	8155
	.half	32694
	.half	32695
	.half	8
	.half	51
	.half	488
	.half	4062
	.half	24
	.half	52
	.half	489
	.half	8156
	.half	490
	.half	237
	.half	2023
	.half	32696
	.half	8157
	.half	4063
	.half	32697
	.half	32698
	.half	113
	.half	491
	.half	4064
	.half	32699
	.half	114
	.half	238
	.half	2024
	.half	32700
	.half	1004
	.half	1005
	.half	16329
	.half	32701
	.half	16330
	.half	32702
	.half	32703
	.half	16329
	.half	1006
	.half	4065
	.half	32704
	.half	32705
	.half	2025
	.half	8158
	.half	32706
	.half	32707
	.half	32708
	.half	32709
	.half	32710
	.half	16329
	.half	32711
	.half	32712
	.half	16329
	.half	16329
	.half	53
	.half	492
	.half	8159
	.half	16331
	.half	239
	.half	493
	.half	4066
	.half	32713
	.half	4067
	.half	4068
	.half	32714
	.half	32715
	.half	32716
	.half	32717
	.half	32718
	.half	32714
	.half	115
	.half	494
	.half	8160
	.half	32719
	.half	240
	.half	495
	.half	4069
	.half	32720
	.half	2026
	.half	4070
	.half	32721
	.half	32722
	.half	32723
	.half	32724
	.half	32725
	.half	32721
	.half	496
	.half	2027
	.half	32726
	.half	32727
	.half	497
	.half	2028
	.half	32728
	.half	32729
	.half	16332
	.half	16333
	.half	32730
	.half	32730
	.half	32731
	.half	32732
	.half	32730
	.half	32730
	.half	16334
	.half	32733
	.half	32734
	.half	32726
	.half	16335
	.half	32735
	.half	32736
	.half	32728
	.half	32737
	.half	32738
	.half	32730
	.half	32730
	.half	16332
	.half	16333
	.half	32730
	.half	32730
	.half	498
	.half	4071
	.half	32739
	.half	32740
	.half	4072
	.half	8161
	.half	32741
	.half	32742
	.half	32743
	.half	32744
	.half	32745
	.half	32714
	.half	32746
	.half	32747
	.half	32714
	.half	32714
	.half	1007
	.half	4073
	.half	32748
	.half	32749
	.half	4074
	.half	16336
	.half	32750
	.half	32751
	.half	32752
	.half	32753
	.half	32754
	.half	32721
	.half	32755
	.half	32756
	.half	32721
	.half	32721
	.half	16337
	.half	32757
	.half	32758
	.half	32726
	.half	32759
	.half	32760
	.half	32761
	.half	32728
	.half	32762
	.half	32763
	.half	32730
	.half	32730
	.half	16332
	.half	16333
	.half	32730
	.half	32730
	.half	32764
	.half	32765
	.half	32726
	.half	32726
	.half	32766
	.half	32767
	.align	2
	.type	cvh_huffbits4, @object
	.size	cvh_huffbits4, 246
cvh_huffbits4:
	.byte	2
	.byte	4
	.byte	7
	.byte	10
	.byte	4
	.byte	5
	.byte	7
	.byte	10
	.byte	7
	.byte	8
	.byte	10
	.byte	14
	.byte	11
	.byte	11
	.byte	15
	.byte	15
	.byte	4
	.byte	5
	.byte	9
	.byte	12
	.byte	5
	.byte	5
	.byte	8
	.byte	12
	.byte	8
	.byte	7
	.byte	10
	.byte	15
	.byte	11
	.byte	11
	.byte	15
	.byte	15
	.byte	7
	.byte	9
	.byte	12
	.byte	15
	.byte	8
	.byte	8
	.byte	12
	.byte	15
	.byte	10
	.byte	10
	.byte	13
	.byte	15
	.byte	14
	.byte	14
	.byte	15
	.byte	0
	.byte	11
	.byte	13
	.byte	15
	.byte	15
	.byte	11
	.byte	13
	.byte	15
	.byte	15
	.byte	14
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	4
	.byte	5
	.byte	9
	.byte	13
	.byte	5
	.byte	6
	.byte	9
	.byte	13
	.byte	9
	.byte	9
	.byte	11
	.byte	15
	.byte	14
	.byte	13
	.byte	15
	.byte	15
	.byte	4
	.byte	6
	.byte	9
	.byte	12
	.byte	5
	.byte	6
	.byte	9
	.byte	13
	.byte	9
	.byte	8
	.byte	11
	.byte	15
	.byte	13
	.byte	12
	.byte	15
	.byte	15
	.byte	7
	.byte	9
	.byte	12
	.byte	15
	.byte	7
	.byte	8
	.byte	11
	.byte	15
	.byte	10
	.byte	10
	.byte	14
	.byte	15
	.byte	14
	.byte	15
	.byte	15
	.byte	0
	.byte	10
	.byte	12
	.byte	15
	.byte	15
	.byte	11
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	6
	.byte	9
	.byte	13
	.byte	14
	.byte	8
	.byte	9
	.byte	12
	.byte	15
	.byte	12
	.byte	12
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	7
	.byte	9
	.byte	13
	.byte	15
	.byte	8
	.byte	9
	.byte	12
	.byte	15
	.byte	11
	.byte	12
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	9
	.byte	11
	.byte	15
	.byte	15
	.byte	9
	.byte	11
	.byte	15
	.byte	15
	.byte	14
	.byte	14
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	14
	.byte	15
	.byte	15
	.byte	0
	.byte	14
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	9
	.byte	12
	.byte	15
	.byte	15
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	10
	.byte	12
	.byte	15
	.byte	15
	.byte	12
	.byte	14
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	14
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	15
	.byte	15
	.align	2
	.type	cvh_huffcodes3, @object
	.size	cvh_huffcodes3, 1214
cvh_huffcodes3:
	.half	0
	.half	4
	.half	34
	.half	198
	.half	944
	.half	12
	.half	13
	.half	35
	.half	199
	.half	945
	.half	92
	.half	200
	.half	201
	.half	946
	.half	4004
	.half	450
	.half	451
	.half	947
	.half	4005
	.half	32626
	.half	948
	.half	1970
	.half	8090
	.half	-220
	.half	-219
	.half	14
	.half	36
	.half	202
	.half	949
	.half	1971
	.half	15
	.half	37
	.half	203
	.half	950
	.half	4006
	.half	93
	.half	94
	.half	204
	.half	951
	.half	8091
	.half	452
	.half	453
	.half	952
	.half	4007
	.half	32627
	.half	4008
	.half	1972
	.half	8092
	.half	-218
	.half	-217
	.half	95
	.half	454
	.half	953
	.half	4009
	.half	32628
	.half	96
	.half	205
	.half	954
	.half	4010
	.half	8093
	.half	455
	.half	456
	.half	1973
	.half	8094
	.half	-216
	.half	1974
	.half	1975
	.half	4011
	.half	16290
	.half	-215
	.half	4012
	.half	4013
	.half	16291
	.half	-214
	.half	16290
	.half	457
	.half	1976
	.half	4014
	.half	-213
	.half	-212
	.half	458
	.half	955
	.half	8095
	.half	32629
	.half	-211
	.half	956
	.half	1977
	.half	4015
	.half	-210
	.half	-209
	.half	8096
	.half	8097
	.half	-208
	.half	-207
	.half	-206
	.half	-205
	.half	-204
	.half	32630
	.half	-203
	.half	-207
	.half	1978
	.half	8098
	.half	-202
	.half	-201
	.half	32631
	.half	1979
	.half	8099
	.half	32632
	.half	-200
	.half	-199
	.half	8100
	.half	8101
	.half	-198
	.half	-197
	.half	-210
	.half	16292
	.half	-196
	.half	-195
	.half	-194
	.half	-207
	.half	-193
	.half	-192
	.half	-208
	.half	-207
	.half	-207
	.half	5
	.half	38
	.half	206
	.half	957
	.half	8102
	.half	39
	.half	40
	.half	207
	.half	958
	.half	8103
	.half	459
	.half	208
	.half	959
	.half	4016
	.half	-191
	.half	960
	.half	961
	.half	1980
	.half	32633
	.half	-190
	.half	8104
	.half	4017
	.half	16293
	.half	-189
	.half	-188
	.half	16
	.half	41
	.half	209
	.half	1981
	.half	8105
	.half	42
	.half	43
	.half	210
	.half	962
	.half	8106
	.half	211
	.half	212
	.half	460
	.half	1982
	.half	16294
	.half	963
	.half	964
	.half	4018
	.half	4019
	.half	-187
	.half	8107
	.half	4020
	.half	8108
	.half	32634
	.half	-186
	.half	97
	.half	213
	.half	461
	.half	4021
	.half	-185
	.half	98
	.half	214
	.half	965
	.half	4022
	.half	16295
	.half	462
	.half	463
	.half	966
	.half	8109
	.half	-184
	.half	1983
	.half	967
	.half	4023
	.half	32635
	.half	-183
	.half	8110
	.half	8111
	.half	-182
	.half	-181
	.half	32635
	.half	464
	.half	1984
	.half	8112
	.half	-180
	.half	-179
	.half	465
	.half	968
	.half	4024
	.half	32636
	.half	-178
	.half	969
	.half	1985
	.half	8113
	.half	-177
	.half	-176
	.half	8114
	.half	4025
	.half	-175
	.half	-174
	.half	-173
	.half	-172
	.half	-171
	.half	-170
	.half	-169
	.half	-174
	.half	1986
	.half	8115
	.half	-168
	.half	-167
	.half	-166
	.half	1987
	.half	8116
	.half	-165
	.half	-164
	.half	-163
	.half	4026
	.half	8117
	.half	32637
	.half	-162
	.half	-177
	.half	-161
	.half	-160
	.half	-159
	.half	-158
	.half	-174
	.half	-157
	.half	-156
	.half	-175
	.half	-174
	.half	-174
	.half	44
	.half	215
	.half	1988
	.half	8118
	.half	-155
	.half	216
	.half	217
	.half	970
	.half	4027
	.half	-154
	.half	1989
	.half	971
	.half	1990
	.half	8119
	.half	-153
	.half	4028
	.half	8120
	.half	8121
	.half	32638
	.half	-152
	.half	-151
	.half	-150
	.half	16296
	.half	-149
	.half	32638
	.half	45
	.half	218
	.half	972
	.half	8122
	.half	-148
	.half	219
	.half	220
	.half	973
	.half	4029
	.half	-147
	.half	974
	.half	975
	.half	1991
	.half	8123
	.half	-146
	.half	8124
	.half	4030
	.half	8125
	.half	-145
	.half	-144
	.half	16297
	.half	16298
	.half	16299
	.half	-143
	.half	-145
	.half	221
	.half	466
	.half	1992
	.half	8126
	.half	-142
	.half	222
	.half	467
	.half	1993
	.half	-141
	.half	16300
	.half	976
	.half	977
	.half	4031
	.half	32639
	.half	-140
	.half	4032
	.half	4033
	.half	8127
	.half	-139
	.half	-138
	.half	32640
	.half	-137
	.half	-136
	.half	-135
	.half	-139
	.half	978
	.half	4034
	.half	32641
	.half	-134
	.half	-133
	.half	979
	.half	4035
	.half	4036
	.half	16301
	.half	-132
	.half	4037
	.half	4038
	.half	8128
	.half	-131
	.half	-130
	.half	16302
	.half	32642
	.half	-129
	.half	-128
	.half	-128
	.half	-127
	.half	-126
	.half	-125
	.half	-128
	.half	-128
	.half	4039
	.half	32643
	.half	32644
	.half	-124
	.half	-134
	.half	8129
	.half	8130
	.half	-123
	.half	-122
	.half	16301
	.half	16303
	.half	-121
	.half	-120
	.half	-119
	.half	-131
	.half	-118
	.half	-117
	.half	-116
	.half	-128
	.half	-128
	.half	16302
	.half	32642
	.half	-129
	.half	-128
	.half	-128
	.half	223
	.half	980
	.half	8131
	.half	32645
	.half	-115
	.half	981
	.half	1994
	.half	8132
	.half	-114
	.half	-113
	.half	8133
	.half	8134
	.half	16304
	.half	-112
	.half	-111
	.half	-110
	.half	-109
	.half	-108
	.half	-107
	.half	-106
	.half	-105
	.half	-104
	.half	-103
	.half	-102
	.half	-107
	.half	224
	.half	982
	.half	1995
	.half	32646
	.half	-101
	.half	468
	.half	983
	.half	4040
	.half	-100
	.half	-99
	.half	4041
	.half	4042
	.half	32647
	.half	-98
	.half	-97
	.half	-96
	.half	16305
	.half	-95
	.half	-94
	.half	-93
	.half	-92
	.half	-91
	.half	-90
	.half	-89
	.half	-94
	.half	469
	.half	1996
	.half	16306
	.half	-88
	.half	-87
	.half	984
	.half	1997
	.half	8135
	.half	-86
	.half	-85
	.half	16307
	.half	8136
	.half	16308
	.half	-84
	.half	-83
	.half	-82
	.half	32648
	.half	32649
	.half	-81
	.half	-81
	.half	-80
	.half	-79
	.half	-78
	.half	-81
	.half	-81
	.half	1998
	.half	8137
	.half	-77
	.half	-76
	.half	-75
	.half	1999
	.half	8138
	.half	32650
	.half	-74
	.half	-73
	.half	8139
	.half	-72
	.half	-71
	.half	-70
	.half	-70
	.half	-69
	.half	-68
	.half	-67
	.half	-66
	.half	-66
	.half	-65
	.half	-64
	.half	-67
	.half	-66
	.half	-66
	.half	32651
	.half	-63
	.half	-62
	.half	-61
	.half	-76
	.half	16309
	.half	-60
	.half	-59
	.half	-58
	.half	-74
	.half	-57
	.half	-56
	.half	-55
	.half	-70
	.half	-70
	.half	-54
	.half	-53
	.half	-67
	.half	-66
	.half	-66
	.half	-69
	.half	-68
	.half	-67
	.half	-66
	.half	-66
	.half	470
	.half	8140
	.half	-52
	.half	-51
	.half	-50
	.half	2000
	.half	8141
	.half	-49
	.half	-48
	.half	-47
	.half	16310
	.half	32652
	.half	-46
	.half	-45
	.half	-112
	.half	32653
	.half	-44
	.half	-43
	.half	-42
	.half	-107
	.half	-41
	.half	-40
	.half	-108
	.half	-107
	.half	-107
	.half	471
	.half	8142
	.half	32654
	.half	32655
	.half	-39
	.half	4043
	.half	8143
	.half	16311
	.half	-38
	.half	-37
	.half	-36
	.half	32656
	.half	-35
	.half	-34
	.half	-98
	.half	-33
	.half	-32
	.half	-31
	.half	-30
	.half	-94
	.half	-29
	.half	-28
	.half	-95
	.half	-94
	.half	-94
	.half	2001
	.half	8144
	.half	32657
	.half	-27
	.half	-88
	.half	4044
	.half	16312
	.half	-26
	.half	-25
	.half	-86
	.half	-24
	.half	-23
	.half	-22
	.half	-21
	.half	-84
	.half	-20
	.half	-19
	.half	-18
	.half	-81
	.half	-81
	.half	-82
	.half	32648
	.half	32649
	.half	-81
	.half	-81
	.half	-17
	.half	-16
	.half	-15
	.half	-14
	.half	-76
	.half	-13
	.half	-12
	.half	-11
	.half	-10
	.half	-74
	.half	-9
	.half	-8
	.half	-7
	.half	-70
	.half	-70
	.half	-6
	.half	-5
	.half	-67
	.half	-66
	.half	-66
	.half	-69
	.half	-68
	.half	-67
	.half	-66
	.half	-66
	.half	-4
	.half	-3
	.half	-77
	.half	-76
	.half	-76
	.half	-2
	.half	-1
	.align	2
	.type	cvh_huffbits3, @object
	.size	cvh_huffbits3, 607
cvh_huffbits3:
	.byte	2
	.byte	4
	.byte	6
	.byte	8
	.byte	10
	.byte	5
	.byte	5
	.byte	6
	.byte	8
	.byte	10
	.byte	7
	.byte	8
	.byte	8
	.byte	10
	.byte	12
	.byte	9
	.byte	9
	.byte	10
	.byte	12
	.byte	15
	.byte	10
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	5
	.byte	6
	.byte	8
	.byte	10
	.byte	11
	.byte	5
	.byte	6
	.byte	8
	.byte	10
	.byte	12
	.byte	7
	.byte	7
	.byte	8
	.byte	10
	.byte	13
	.byte	9
	.byte	9
	.byte	10
	.byte	12
	.byte	15
	.byte	12
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	7
	.byte	9
	.byte	10
	.byte	12
	.byte	15
	.byte	7
	.byte	8
	.byte	10
	.byte	12
	.byte	13
	.byte	9
	.byte	9
	.byte	11
	.byte	13
	.byte	16
	.byte	11
	.byte	11
	.byte	12
	.byte	14
	.byte	16
	.byte	12
	.byte	12
	.byte	14
	.byte	16
	.byte	0
	.byte	9
	.byte	11
	.byte	12
	.byte	16
	.byte	16
	.byte	9
	.byte	10
	.byte	13
	.byte	15
	.byte	16
	.byte	10
	.byte	11
	.byte	12
	.byte	16
	.byte	16
	.byte	13
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	15
	.byte	16
	.byte	0
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	15
	.byte	11
	.byte	13
	.byte	15
	.byte	16
	.byte	16
	.byte	13
	.byte	13
	.byte	16
	.byte	16
	.byte	0
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	6
	.byte	8
	.byte	10
	.byte	13
	.byte	6
	.byte	6
	.byte	8
	.byte	10
	.byte	13
	.byte	9
	.byte	8
	.byte	10
	.byte	12
	.byte	16
	.byte	10
	.byte	10
	.byte	11
	.byte	15
	.byte	16
	.byte	13
	.byte	12
	.byte	14
	.byte	16
	.byte	16
	.byte	5
	.byte	6
	.byte	8
	.byte	11
	.byte	13
	.byte	6
	.byte	6
	.byte	8
	.byte	10
	.byte	13
	.byte	8
	.byte	8
	.byte	9
	.byte	11
	.byte	14
	.byte	10
	.byte	10
	.byte	12
	.byte	12
	.byte	16
	.byte	13
	.byte	12
	.byte	13
	.byte	15
	.byte	16
	.byte	7
	.byte	8
	.byte	9
	.byte	12
	.byte	16
	.byte	7
	.byte	8
	.byte	10
	.byte	12
	.byte	14
	.byte	9
	.byte	9
	.byte	10
	.byte	13
	.byte	16
	.byte	11
	.byte	10
	.byte	12
	.byte	15
	.byte	16
	.byte	13
	.byte	13
	.byte	16
	.byte	16
	.byte	0
	.byte	9
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	9
	.byte	10
	.byte	12
	.byte	15
	.byte	16
	.byte	10
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	13
	.byte	12
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	12
	.byte	13
	.byte	15
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	6
	.byte	8
	.byte	11
	.byte	13
	.byte	16
	.byte	8
	.byte	8
	.byte	10
	.byte	12
	.byte	16
	.byte	11
	.byte	10
	.byte	11
	.byte	13
	.byte	16
	.byte	12
	.byte	13
	.byte	13
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	14
	.byte	16
	.byte	0
	.byte	6
	.byte	8
	.byte	10
	.byte	13
	.byte	16
	.byte	8
	.byte	8
	.byte	10
	.byte	12
	.byte	16
	.byte	10
	.byte	10
	.byte	11
	.byte	13
	.byte	16
	.byte	13
	.byte	12
	.byte	13
	.byte	16
	.byte	16
	.byte	14
	.byte	14
	.byte	14
	.byte	16
	.byte	0
	.byte	8
	.byte	9
	.byte	11
	.byte	13
	.byte	16
	.byte	8
	.byte	9
	.byte	11
	.byte	16
	.byte	14
	.byte	10
	.byte	10
	.byte	12
	.byte	15
	.byte	16
	.byte	12
	.byte	12
	.byte	13
	.byte	16
	.byte	16
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	10
	.byte	12
	.byte	15
	.byte	16
	.byte	16
	.byte	10
	.byte	12
	.byte	12
	.byte	14
	.byte	16
	.byte	12
	.byte	12
	.byte	13
	.byte	16
	.byte	16
	.byte	14
	.byte	15
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	12
	.byte	15
	.byte	15
	.byte	16
	.byte	0
	.byte	13
	.byte	13
	.byte	16
	.byte	16
	.byte	0
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	8
	.byte	10
	.byte	13
	.byte	15
	.byte	16
	.byte	10
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	13
	.byte	13
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	8
	.byte	10
	.byte	11
	.byte	15
	.byte	16
	.byte	9
	.byte	10
	.byte	12
	.byte	16
	.byte	16
	.byte	12
	.byte	12
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	9
	.byte	11
	.byte	14
	.byte	16
	.byte	16
	.byte	10
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	14
	.byte	13
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	15
	.byte	15
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	11
	.byte	13
	.byte	15
	.byte	16
	.byte	16
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	9
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	14
	.byte	15
	.byte	16
	.byte	16
	.byte	0
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	9
	.byte	13
	.byte	15
	.byte	15
	.byte	16
	.byte	12
	.byte	13
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	15
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	11
	.byte	13
	.byte	15
	.byte	16
	.byte	0
	.byte	12
	.byte	14
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	16
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	16
	.byte	16
	.align	2
	.type	cvh_huffcodes2, @object
	.size	cvh_huffcodes2, 96
cvh_huffcodes2:
	.half	0
	.half	10
	.half	24
	.half	116
	.half	242
	.half	500
	.half	1014
	.half	4
	.half	11
	.half	25
	.half	117
	.half	243
	.half	501
	.half	1015
	.half	26
	.half	27
	.half	56
	.half	118
	.half	244
	.half	1016
	.half	1017
	.half	119
	.half	57
	.half	120
	.half	245
	.half	502
	.half	1018
	.half	4092
	.half	246
	.half	247
	.half	248
	.half	503
	.half	1019
	.half	4093
	.half	16382
	.half	249
	.half	504
	.half	505
	.half	1020
	.half	2044
	.half	32766
	.half	-2
	.half	506
	.half	1021
	.half	2045
	.half	4094
	.half	8190
	.half	-1
	.align	2
	.type	cvh_huffbits2, @object
	.size	cvh_huffbits2, 48
cvh_huffbits2:
	.byte	1
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	3
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	10
	.byte	10
	.byte	7
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	12
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	10
	.byte	12
	.byte	14
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	15
	.byte	16
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	16
	.align	2
	.type	cvh_huffcodes1, @object
	.size	cvh_huffcodes1, 194
cvh_huffcodes1:
	.half	0
	.half	8
	.half	20
	.half	48
	.half	106
	.half	226
	.half	227
	.half	484
	.half	1004
	.half	1005
	.half	9
	.half	21
	.half	49
	.half	107
	.half	108
	.half	228
	.half	229
	.half	485
	.half	486
	.half	2032
	.half	22
	.half	23
	.half	50
	.half	109
	.half	230
	.half	231
	.half	487
	.half	488
	.half	1006
	.half	2033
	.half	51
	.half	52
	.half	110
	.half	232
	.half	233
	.half	489
	.half	490
	.half	1007
	.half	2034
	.half	4086
	.half	111
	.half	112
	.half	234
	.half	235
	.half	491
	.half	492
	.half	1008
	.half	2035
	.half	2036
	.half	8186
	.half	236
	.half	237
	.half	238
	.half	493
	.half	494
	.half	1009
	.half	1010
	.half	2037
	.half	4087
	.half	16378
	.half	239
	.half	240
	.half	241
	.half	495
	.half	1011
	.half	2038
	.half	2039
	.half	4088
	.half	8187
	.half	32766
	.half	496
	.half	497
	.half	498
	.half	1012
	.half	2040
	.half	4089
	.half	4090
	.half	16379
	.half	16380
	.half	0
	.half	499
	.half	500
	.half	501
	.half	1013
	.half	2041
	.half	4091
	.half	16381
	.half	-2
	.half	0
	.half	0
	.half	1014
	.half	1015
	.half	2042
	.half	4092
	.half	8188
	.half	16382
	.half	-1
	.align	2
	.type	cvh_huffbits1, @object
	.size	cvh_huffbits1, 97
cvh_huffbits1:
	.byte	1
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	10
	.byte	10
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	11
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	6
	.byte	6
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	11
	.byte	13
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	11
	.byte	12
	.byte	14
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	15
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	12
	.byte	14
	.byte	14
	.byte	0
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	12
	.byte	14
	.byte	16
	.byte	0
	.byte	0
	.byte	10
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	16
	.align	2
	.type	cvh_huffcodes0, @object
	.size	cvh_huffcodes0, 382
cvh_huffcodes0:
	.half	0
	.half	8
	.half	44
	.half	45
	.half	98
	.half	99
	.half	212
	.half	213
	.half	214
	.half	454
	.half	455
	.half	970
	.half	2006
	.half	2007
	.half	9
	.half	20
	.half	46
	.half	100
	.half	101
	.half	215
	.half	216
	.half	456
	.half	457
	.half	458
	.half	459
	.half	971
	.half	2008
	.half	2009
	.half	21
	.half	47
	.half	102
	.half	217
	.half	218
	.half	460
	.half	461
	.half	462
	.half	463
	.half	972
	.half	973
	.half	974
	.half	2010
	.half	4068
	.half	48
	.half	103
	.half	219
	.half	464
	.half	465
	.half	466
	.half	467
	.half	975
	.half	976
	.half	977
	.half	978
	.half	2011
	.half	4069
	.half	8170
	.half	104
	.half	105
	.half	220
	.half	468
	.half	469
	.half	470
	.half	979
	.half	980
	.half	981
	.half	982
	.half	2012
	.half	2013
	.half	4070
	.half	8171
	.half	221
	.half	222
	.half	471
	.half	472
	.half	473
	.half	983
	.half	984
	.half	985
	.half	986
	.half	2014
	.half	2015
	.half	4071
	.half	8172
	.half	16370
	.half	223
	.half	224
	.half	474
	.half	475
	.half	987
	.half	988
	.half	2016
	.half	2017
	.half	2018
	.half	4072
	.half	4073
	.half	8173
	.half	8174
	.half	32756
	.half	225
	.half	226
	.half	476
	.half	477
	.half	989
	.half	990
	.half	2019
	.half	2020
	.half	2021
	.half	4074
	.half	4075
	.half	8175
	.half	16371
	.half	32757
	.half	478
	.half	479
	.half	480
	.half	991
	.half	992
	.half	993
	.half	2022
	.half	2023
	.half	4076
	.half	8176
	.half	4077
	.half	16372
	.half	32758
	.half	-8
	.half	481
	.half	482
	.half	994
	.half	995
	.half	996
	.half	997
	.half	2024
	.half	4078
	.half	4079
	.half	16373
	.half	16374
	.half	-7
	.half	-6
	.half	-6
	.half	483
	.half	484
	.half	998
	.half	999
	.half	2025
	.half	2026
	.half	4080
	.half	8177
	.half	8178
	.half	16375
	.half	16376
	.half	32759
	.half	32759
	.half	-6
	.half	1000
	.half	1001
	.half	1002
	.half	2027
	.half	2028
	.half	4081
	.half	4082
	.half	8179
	.half	32760
	.half	32761
	.half	-5
	.half	16376
	.half	32759
	.half	32759
	.half	2029
	.half	2030
	.half	2031
	.half	4083
	.half	8180
	.half	8181
	.half	8182
	.half	32762
	.half	-4
	.half	-3
	.half	-5
	.half	-5
	.half	16376
	.half	32759
	.half	2032
	.half	2033
	.half	4084
	.half	8183
	.half	8184
	.half	16377
	.half	32763
	.half	-2
	.half	-1
	.align	2
	.type	cvh_huffbits0, @object
	.size	cvh_huffbits0, 191
cvh_huffbits0:
	.byte	1
	.byte	4
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	11
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	11
	.byte	11
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	12
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	7
	.byte	7
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	15
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	12
	.byte	14
	.byte	15
	.byte	16
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	12
	.byte	12
	.byte	14
	.byte	14
	.byte	16
	.byte	16
	.byte	0
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	15
	.byte	0
	.byte	0
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	15
	.byte	16
	.byte	16
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	11
	.byte	11
	.byte	12
	.byte	13
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	16
	.align	2
	.type	envelope_quant_index_huffcodes, @object
	.size	envelope_quant_index_huffcodes, 624
envelope_quant_index_huffcodes:
	.half	6
	.half	62
	.half	28
	.half	29
	.half	7
	.half	8
	.half	9
	.half	10
	.half	11
	.half	12
	.half	0
	.half	1
	.half	2
	.half	13
	.half	30
	.half	126
	.half	254
	.half	510
	.half	2044
	.half	2045
	.half	4092
	.half	4093
	.half	4094
	.half	4095
	.half	1022
	.half	254
	.half	62
	.half	28
	.half	29
	.half	12
	.half	0
	.half	1
	.half	2
	.half	3
	.half	4
	.half	5
	.half	13
	.half	30
	.half	126
	.half	510
	.half	2046
	.half	4094
	.half	8190
	.half	32764
	.half	32765
	.half	32766
	.half	-2
	.half	-1
	.half	4094
	.half	1022
	.half	254
	.half	62
	.half	28
	.half	6
	.half	7
	.half	8
	.half	9
	.half	10
	.half	11
	.half	0
	.half	1
	.half	2
	.half	12
	.half	13
	.half	29
	.half	30
	.half	126
	.half	510
	.half	2046
	.half	8190
	.half	16382
	.half	16383
	.half	8188
	.half	1022
	.half	508
	.half	509
	.half	124
	.half	125
	.half	28
	.half	29
	.half	10
	.half	0
	.half	1
	.half	2
	.half	3
	.half	4
	.half	11
	.half	12
	.half	13
	.half	30
	.half	126
	.half	510
	.half	2046
	.half	8189
	.half	8190
	.half	8191
	.half	4094
	.half	8190
	.half	1022
	.half	254
	.half	60
	.half	61
	.half	26
	.half	27
	.half	10
	.half	11
	.half	0
	.half	1
	.half	2
	.half	3
	.half	4
	.half	12
	.half	28
	.half	29
	.half	62
	.half	126
	.half	510
	.half	2046
	.half	16382
	.half	16383
	.half	4094
	.half	2046
	.half	510
	.half	252
	.half	253
	.half	124
	.half	28
	.half	10
	.half	11
	.half	0
	.half	1
	.half	2
	.half	3
	.half	4
	.half	12
	.half	13
	.half	29
	.half	30
	.half	125
	.half	254
	.half	1022
	.half	8190
	.half	16382
	.half	16383
	.half	32764
	.half	-4
	.half	32765
	.half	4094
	.half	1022
	.half	254
	.half	62
	.half	28
	.half	12
	.half	2
	.half	3
	.half	4
	.half	0
	.half	5
	.half	13
	.half	29
	.half	30
	.half	126
	.half	510
	.half	2046
	.half	8190
	.half	-3
	.half	-2
	.half	-1
	.half	16380
	.half	16381
	.half	2046
	.half	1022
	.half	508
	.half	124
	.half	125
	.half	28
	.half	29
	.half	12
	.half	2
	.half	3
	.half	0
	.half	4
	.half	5
	.half	13
	.half	30
	.half	126
	.half	509
	.half	510
	.half	4094
	.half	16382
	.half	32766
	.half	32767
	.half	508
	.half	509
	.half	510
	.half	252
	.half	124
	.half	60
	.half	28
	.half	12
	.half	0
	.half	1
	.half	2
	.half	3
	.half	4
	.half	5
	.half	13
	.half	29
	.half	61
	.half	125
	.half	253
	.half	1022
	.half	2046
	.half	4094
	.half	8190
	.half	8191
	.half	16380
	.half	4094
	.half	1022
	.half	252
	.half	60
	.half	61
	.half	28
	.half	12
	.half	0
	.half	1
	.half	2
	.half	3
	.half	4
	.half	5
	.half	13
	.half	29
	.half	62
	.half	253
	.half	254
	.half	510
	.half	2046
	.half	16381
	.half	16382
	.half	16383
	.half	8190
	.half	1022
	.half	508
	.half	252
	.half	60
	.half	61
	.half	28
	.half	10
	.half	11
	.half	12
	.half	2
	.half	3
	.half	0
	.half	4
	.half	13
	.half	29
	.half	62
	.half	253
	.half	509
	.half	510
	.half	2046
	.half	4094
	.half	16382
	.half	16383
	.half	-4
	.half	8190
	.half	4094
	.half	2046
	.half	510
	.half	62
	.half	28
	.half	29
	.half	10
	.half	11
	.half	12
	.half	2
	.half	0
	.half	3
	.half	4
	.half	13
	.half	30
	.half	126
	.half	254
	.half	1022
	.half	16382
	.half	-3
	.half	-2
	.half	-1
	.half	8188
	.half	16378
	.half	16379
	.half	16380
	.half	1022
	.half	254
	.half	124
	.half	125
	.half	28
	.half	12
	.half	2
	.half	3
	.half	0
	.half	4
	.half	5
	.half	13
	.half	29
	.half	30
	.half	126
	.half	510
	.half	2046
	.half	16381
	.half	16382
	.half	16383
	.align	2
	.type	envelope_quant_index_huffbits, @object
	.size	envelope_quant_index_huffbits, 312
envelope_quant_index_huffbits:
	.byte	4
	.byte	6
	.byte	5
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	9
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	10
	.byte	8
	.byte	6
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	7
	.byte	9
	.byte	11
	.byte	12
	.byte	13
	.byte	15
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	12
	.byte	10
	.byte	8
	.byte	6
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	7
	.byte	9
	.byte	11
	.byte	13
	.byte	14
	.byte	14
	.byte	13
	.byte	10
	.byte	9
	.byte	9
	.byte	7
	.byte	7
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	4
	.byte	5
	.byte	7
	.byte	9
	.byte	11
	.byte	13
	.byte	13
	.byte	13
	.byte	12
	.byte	13
	.byte	10
	.byte	8
	.byte	6
	.byte	6
	.byte	5
	.byte	5
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	9
	.byte	11
	.byte	14
	.byte	14
	.byte	12
	.byte	11
	.byte	9
	.byte	8
	.byte	8
	.byte	7
	.byte	5
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	10
	.byte	13
	.byte	14
	.byte	14
	.byte	15
	.byte	16
	.byte	15
	.byte	12
	.byte	10
	.byte	8
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	7
	.byte	9
	.byte	11
	.byte	13
	.byte	16
	.byte	16
	.byte	16
	.byte	14
	.byte	14
	.byte	11
	.byte	10
	.byte	9
	.byte	7
	.byte	7
	.byte	5
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	7
	.byte	9
	.byte	9
	.byte	12
	.byte	14
	.byte	15
	.byte	15
	.byte	9
	.byte	9
	.byte	9
	.byte	8
	.byte	7
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	13
	.byte	14
	.byte	12
	.byte	10
	.byte	8
	.byte	6
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	8
	.byte	8
	.byte	9
	.byte	11
	.byte	14
	.byte	14
	.byte	14
	.byte	13
	.byte	10
	.byte	9
	.byte	8
	.byte	6
	.byte	6
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	8
	.byte	9
	.byte	9
	.byte	11
	.byte	12
	.byte	14
	.byte	14
	.byte	16
	.byte	13
	.byte	12
	.byte	11
	.byte	9
	.byte	6
	.byte	5
	.byte	5
	.byte	4
	.byte	4
	.byte	4
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	7
	.byte	8
	.byte	10
	.byte	14
	.byte	16
	.byte	16
	.byte	16
	.byte	13
	.byte	14
	.byte	14
	.byte	14
	.byte	10
	.byte	8
	.byte	7
	.byte	7
	.byte	5
	.byte	4
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	7
	.byte	9
	.byte	11
	.byte	14
	.byte	14
	.byte	14
	.align	2
	.type	vhvlcsize_tab, @object
	.size	vhvlcsize_tab, 28
vhvlcsize_tab:
	.word	8
	.word	7
	.word	7
	.word	10
	.word	9
	.word	9
	.word	6
	.align	2
	.type	vhsize_tab, @object
	.size	vhsize_tab, 28
vhsize_tab:
	.word	191
	.word	97
	.word	48
	.word	607
	.word	246
	.word	230
	.word	32
	.align	2
	.type	vpr_tab, @object
	.size	vpr_tab, 28
vpr_tab:
	.word	10
	.word	10
	.word	10
	.word	5
	.word	5
	.word	4
	.word	4
	.align	2
	.type	vd_tab, @object
	.size	vd_tab, 28
vd_tab:
	.word	2
	.word	2
	.word	2
	.word	4
	.word	4
	.word	5
	.word	5
	.align	2
	.type	kmax_tab, @object
	.size	kmax_tab, 28
kmax_tab:
	.word	13
	.word	9
	.word	6
	.word	4
	.word	3
	.word	2
	.word	1
	.align	2
	.type	invradix_tab, @object
	.size	invradix_tab, 28
invradix_tab:
	.word	74899
	.word	104858
	.word	149797
	.word	209716
	.word	262144
	.word	349526
	.word	524288
	.align	2
	.type	s32_quant_centroid_tab_L14, @object
	.size	s32_quant_centroid_tab_L14, 392
s32_quant_centroid_tab_L14:
	.word	0
	.word	6422
	.word	12468
	.word	18350
	.word	24199
	.word	30015
	.word	35766
	.word	41631
	.word	47398
	.word	53166
	.word	58949
	.word	64585
	.word	70254
	.word	77398
	.word	0
	.word	8912
	.word	17367
	.word	25608
	.word	33882
	.word	42123
	.word	50331
	.word	58359
	.word	66682
	.word	75694
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	12222
	.word	23986
	.word	35717
	.word	47218
	.word	58720
	.word	70713
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	16482
	.word	32768
	.word	49037
	.word	65290
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	21643
	.word	44285
	.word	65257
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	27148
	.word	57196
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	32178
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.align	2
	.type	quant_centroid_tab, @object
	.size	quant_centroid_tab, 392
quant_centroid_tab:
	.word	0
	.word	1053340729
	.word	1061343461
	.word	1066359849
	.word	1069354582
	.word	1072332538
	.word	1074509382
	.word	1076010942
	.word	1077487337
	.word	1078963732
	.word	1080444322
	.word	1081887162
	.word	1082734412
	.word	1083648770
	.word	0
	.word	1057702806
	.word	1065856532
	.word	1070076002
	.word	1074027037
	.word	1076136772
	.word	1078238118
	.word	1080293327
	.word	1082277233
	.word	1083430666
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1061091803
	.word	1069245530
	.word	1074496799
	.word	1077441200
	.word	1080385602
	.word	1082793132
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1065403548
	.word	1073741824
	.word	1077906768
	.word	1082067517
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1068045959
	.word	1076690420
	.word	1082059129
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1070864531
	.word	1079995531
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1073439834
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.align	2
	.type	s32_dither_tab_L20, @object
	.size	s32_dither_tab_L20, 32
s32_dither_tab_L20:
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	185364
	.word	262411
	.word	741455
	.align	2
	.type	dither_tab, @object
	.size	dither_tab, 32
dither_tab:
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1043662088
	.word	1048576000
	.word	1060439287
	.align	2
	.type	expbits_tab, @object
	.size	expbits_tab, 32
expbits_tab:
	.word	52
	.word	47
	.word	43
	.word	37
	.word	29
	.word	22
	.word	16
	.word	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align	3
$LC17:
	.word	0
	.word	1073741824
	.align	3
$LC18:
	.word	0
	.word	1071644672
	.section	.rodata.cst4,"aM",@progbits,4
	.align	2
$LC19:
	.word	1233125376
	.align	2
$LC20:
	.word	1182793728
	.section	.rodata.cst8
	.align	3
$LC21:
	.word	0
	.word	1072693248
	.section	.rodata.cst4
	.align	2
$LC22:
	.word	1317011456
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
