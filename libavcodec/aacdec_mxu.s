.file	1 "aacdec.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	apply_independent_coupling
.type	apply_independent_coupling, @function
apply_independent_coupling:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$7,5
lwc1	$f3,9980($4)
sll	$7,$7,9
li	$3,65536			# 0x10000
subu	$7,$7,$2
lw	$2,20($4)
li	$4,2048			# 0x800
addu	$7,$6,$7
addiu	$5,$5,13440
addu	$7,$3,$7
xori	$3,$2,0x1
li	$2,1024			# 0x400
lwc1	$f2,10232($7)
addiu	$6,$6,13584
movz	$2,$4,$3
sll	$2,$2,2
addu	$2,$5,$2
$L3:
lwc1	$f0,0($6)
addiu	$5,$5,4
addiu	$6,$6,4
lwc1	$f1,-4($5)
sub.s	$f0,$f0,$f3
mul.s	$f0,$f0,$f2
add.s	$f0,$f1,$f0
bne	$5,$2,$L3
swc1	$f0,-4($5)

j	$31
nop

.set	macro
.set	reorder
.end	apply_independent_coupling
.size	apply_independent_coupling, .-apply_independent_coupling
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"Dependent coupling is not supported together with LTP\012"
.ascii	"\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	apply_dependent_coupling
.type	apply_dependent_coupling, @function
apply_dependent_coupling:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,4($4)
addiu	$sp,$sp,-16
lui	$28,%hi(__gnu_local_gp)
li	$2,4			# 0x4
sw	$18,8($sp)
sw	$16,0($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$19,12($sp)
addiu	$5,$5,5248
sw	$17,4($sp)
addiu	$16,$6,5392
beq	$3,$2,$L8
lw	$18,172($6)

lw	$19,160($6)
blez	$19,$L7
sll	$17,$7,3

lbu	$4,144($6)
sll	$7,$7,7
addu	$19,$6,$19
subu	$17,$7,$17
move	$7,$0
addiu	$17,$17,18942
move	$12,$6
$L10:
beq	$4,$0,$L30
addiu	$14,$7,817

lbu	$2,164($12)
addu	$24,$17,$7
sll	$14,$14,2
sll	$24,$24,2
addu	$14,$6,$14
addu	$24,$6,$24
move	$13,$0
move	$25,$7
move	$15,$18
$L15:
lw	$3,0($14)
beq	$3,$0,$L11
nop

beq	$2,$0,$L11
lwc1	$f2,0($24)

lhu	$3,0($15)
move	$9,$0
lhu	$11,2($15)
sll	$7,$3,2
subu	$8,$11,$3
addu	$10,$5,$7
sll	$8,$8,2
addu	$7,$16,$7
slt	$11,$3,$11
$L14:
beq	$11,$0,$L12
move	$3,$0

move	$2,$10
$L13:
addu	$4,$7,$3
lwc1	$f1,0($2)
addiu	$3,$3,4
lwc1	$f0,0($4)
addiu	$2,$2,4
mul.s	$f0,$f2,$f0
add.s	$f0,$f1,$f0
bne	$3,$8,$L13
swc1	$f0,-4($2)

lbu	$2,164($12)
$L12:
addiu	$9,$9,1
addiu	$10,$10,512
slt	$3,$9,$2
bne	$3,$0,$L14
addiu	$7,$7,512

lbu	$4,144($6)
$L11:
addiu	$13,$13,1
addiu	$14,$14,4
slt	$3,$13,$4
addu	$7,$13,$25
addiu	$24,$24,4
bne	$3,$0,$L15
addiu	$15,$15,2

sll	$2,$2,9
addiu	$12,$12,1
addu	$5,$5,$2
bne	$12,$19,$L10
addu	$16,$16,$2

$L7:
lw	$19,12($sp)
$L31:
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

$L30:
lbu	$2,164($12)
addiu	$12,$12,1
sll	$2,$2,9
addu	$5,$5,$2
bne	$12,$19,$L10
addu	$16,$16,$2

.option	pic0
j	$L31
.option	pic2
lw	$19,12($sp)

$L8:
lui	$6,%hi($LC0)
lw	$19,12($sp)
lw	$18,8($sp)
li	$5,16			# 0x10
lw	$17,4($sp)
addiu	$6,$6,%lo($LC0)
lw	$16,0($sp)
lw	$4,0($4)
lw	$25,%call16(av_log)($28)
.reloc	1f,R_MIPS_JALR,av_log
1:	jr	$25
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	apply_dependent_coupling
.size	apply_dependent_coupling, .-apply_dependent_coupling
.section	.text.unlikely,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	aac_decode_close
.type	aac_decode_close, @function
aac_decode_close:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-56
lui	$28,%hi(__gnu_local_gp)
sw	$22,48($sp)
li	$22,65536			# 0x10000
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,28($sp)
sw	$21,44($sp)
addiu	$22,$22,17920
sw	$20,40($sp)
li	$21,64			# 0x40
sw	$18,32($sp)
li	$20,256			# 0x100
sw	$31,52($sp)
move	$18,$0
sw	$19,36($sp)
sw	$16,24($sp)
.cprestore	16
lw	$17,136($4)
move	$16,$0
$L42:
addiu	$19,$18,784
$L35:
addu	$2,$16,$18
addu	$2,$17,$2
lw	$4,784($2)
beq	$4,$0,$L34
lw	$25,%call16(ff_aac_sbr_ctx_close)($28)

.reloc	1f,R_MIPS_JALR,ff_aac_sbr_ctx_close
1:	jalr	$25
addu	$4,$4,$22

lw	$28,16($sp)
$L34:
addu	$4,$19,$16
lw	$25,%call16(av_freep)($28)
addiu	$16,$16,64
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$17,$4

bne	$16,$20,$L35
lw	$28,16($sp)

addiu	$18,$18,4
bne	$18,$21,$L42
move	$16,$0

lw	$25,%call16(ff_mdct_end)($28)
.reloc	1f,R_MIPS_JALR,ff_mdct_end
1:	jalr	$25
addiu	$4,$17,5472

lw	$28,16($sp)
lw	$25,%call16(ff_mdct_end)($28)
.reloc	1f,R_MIPS_JALR,ff_mdct_end
1:	jalr	$25
addiu	$4,$17,5536

move	$2,$0
lw	$31,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	aac_decode_close
.size	aac_decode_close, .-aac_decode_close
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"invalid default channel configuration (%d)\012\000"
.section	.text.unlikely
.align	2
.set	nomips16
.set	nomicromips
.ent	set_default_channel_config.isra.13
.type	set_default_channel_config.isra.13, @function
set_default_channel_config.isra.13:
.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$2,$6,-1
sltu	$2,$2,7
bne	$2,$0,$L44
li	$2,2			# 0x2

lui	$28,%hi(__gnu_local_gp)
lw	$4,0($4)
move	$7,$6
addiu	$28,$28,%lo(__gnu_local_gp)
lui	$6,%hi($LC1)
addiu	$sp,$sp,-32
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
sw	$31,28($sp)
.cprestore	16
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC1)

li	$2,-1			# 0xffffffffffffffff
lw	$31,28($sp)
j	$31
addiu	$sp,$sp,32

$L44:
bne	$6,$2,$L46
li	$2,1			# 0x1

$L49:
li	$3,1			# 0x1
li	$4,4			# 0x4
bne	$6,$4,$L57
sw	$3,64($5)

li	$2,3			# 0x3
.option	pic0
j	$L53
.option	pic2
sw	$2,4($5)

$L46:
bne	$6,$2,$L49
sw	$2,0($5)

j	$31
move	$2,$0

$L57:
slt	$2,$6,5
bne	$2,$0,$L60
move	$2,$0

li	$7,2			# 0x2
xori	$2,$6,0x7
movn	$7,$3,$2
sll	$2,$7,2
li	$7,3			# 0x3
addu	$2,$5,$2
sw	$7,64($2)
li	$2,5			# 0x5
beq	$6,$2,$L60
move	$2,$0

li	$2,7			# 0x7
bne	$6,$2,$L53
sw	$4,192($5)

sw	$3,68($5)
j	$31
move	$2,$0

$L53:
move	$2,$0
$L60:
j	$31
nop

.set	macro
.set	reorder
.end	set_default_channel_config.isra.13
.size	set_default_channel_config.isra.13, .-set_default_channel_config.isra.13
.align	2
.set	nomips16
.set	nomicromips
.ent	che_configure
.type	che_configure, @function
che_configure:
.frame	$sp,48,$31		# vars= 8, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$6,6
lui	$28,%hi(__gnu_local_gp)
addu	$5,$5,$2
sll	$2,$7,2
addiu	$28,$28,%lo(__gnu_local_gp)
addu	$5,$5,$2
addiu	$sp,$sp,-48
lw	$2,0($5)
sw	$17,40($sp)
move	$17,$4
sw	$16,36($sp)
sll	$16,$6,4
sw	$31,44($sp)
beq	$2,$0,$L62
.cprestore	16

addu	$7,$7,$16
sll	$16,$7,2
addu	$16,$4,$16
lw	$2,784($16)
beq	$2,$0,$L63
lw	$25,%call16(av_mallocz)($28)

li	$2,65536			# 0x10000
$L77:
lw	$4,784($16)
lw	$25,%call16(ff_aac_sbr_ctx_init)($28)
addiu	$2,$2,17920
sw	$6,24($sp)
.reloc	1f,R_MIPS_JALR,ff_aac_sbr_ctx_init
1:	jalr	$25
addu	$4,$4,$2

li	$2,2			# 0x2
lw	$6,24($sp)
bne	$6,$2,$L64
li	$5,1			# 0x1

.option	pic0
j	$L66
.option	pic2
move	$3,$0

$L63:
li	$4,393216			# 0x60000
sw	$6,24($sp)
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
ori	$4,$4,0x8430

li	$3,-12			# 0xfffffffffffffff4
lw	$28,16($sp)
sw	$2,784($16)
beq	$2,$0,$L66
lw	$6,24($sp)

.option	pic0
j	$L77
.option	pic2
li	$2,65536			# 0x10000

$L64:
lw	$3,64($sp)
lw	$7,64($sp)
lw	$2,0($3)
addiu	$3,$2,1
sll	$4,$2,2
sw	$3,0($7)
addu	$4,$17,$4
lw	$3,784($16)
addiu	$7,$3,13584
beq	$6,$5,$L68
sw	$7,9724($4)

beq	$6,$0,$L76
nop

$L65:
.option	pic0
j	$L66
.option	pic2
move	$3,$0

$L76:
lw	$6,44($17)
bne	$6,$5,$L65
nop

$L68:
li	$5,51344			# 0xc890
lw	$6,64($sp)
addiu	$2,$2,2
addu	$5,$3,$5
move	$3,$0
sw	$2,0($6)
.option	pic0
j	$L66
.option	pic2
sw	$5,9728($4)

$L62:
addu	$2,$16,$7
addiu	$2,$2,196
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,0($2)
beq	$2,$0,$L78
addu	$7,$7,$16

subu	$7,$7,$16
li	$4,65536			# 0x10000
lw	$25,%call16(ff_aac_sbr_ctx_close)($28)
sw	$7,24($sp)
addiu	$4,$4,17920
.reloc	1f,R_MIPS_JALR,ff_aac_sbr_ctx_close
1:	jalr	$25
addu	$4,$2,$4

lw	$28,16($sp)
lw	$7,24($sp)
addu	$7,$7,$16
$L78:
lw	$25,%call16(av_freep)($28)
addiu	$7,$7,196
sll	$4,$7,2
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addu	$4,$17,$4

move	$3,$0
$L66:
lw	$31,44($sp)
move	$2,$3
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	che_configure
.size	che_configure, .-che_configure
.align	2
.set	nomips16
.set	nomicromips
.ent	output_configure
.type	output_configure, @function
output_configure:
.frame	$sp,80,$31		# vars= 8, regs= 9/0, args= 24, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-80
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$19,56($sp)
move	$19,$5
sw	$18,52($sp)
sw	$17,48($sp)
move	$17,$4
sw	$16,44($sp)
move	$16,$7
sw	$31,76($sp)
sw	$23,72($sp)
sw	$22,68($sp)
sw	$21,64($sp)
sw	$20,60($sp)
.cprestore	24
sw	$0,32($sp)
beq	$6,$5,$L80
lw	$18,0($4)

addiu	$8,$6,256
move	$2,$5
$L81:
lwl	$7,3($6)
lwl	$5,7($6)
lwl	$4,11($6)
lwl	$3,15($6)
lwr	$7,0($6)
lwr	$5,4($6)
lwr	$4,8($6)
lwr	$3,12($6)
addiu	$6,$6,16
swl	$7,3($2)
swr	$7,0($2)
swl	$5,7($2)
swr	$5,4($2)
swl	$4,11($2)
swr	$4,8($2)
swl	$3,15($2)
swr	$3,12($2)
bne	$6,$8,$L81
addiu	$2,$2,16

$L80:
beq	$16,$0,$L90
move	$20,$0

li	$4,10			# 0xa
lui	$2,%hi(tags_per_config)
mul	$5,$16,$4
addiu	$2,$2,%lo(tags_per_config)
lui	$3,%hi(aac_channel_layout_map)
addu	$2,$16,$2
addiu	$3,$3,%lo(aac_channel_layout_map)
lb	$21,0($2)
addiu	$22,$sp,32
addu	$23,$5,$3
$L83:
slt	$2,$20,$21
beq	$2,$0,$L97
lw	$25,%call16(memset)($28)

lbu	$6,-10($23)
move	$4,$17
lbu	$7,-9($23)
move	$5,$19
sw	$22,16($sp)
addiu	$20,$20,1
.option	pic0
jal	che_configure
.option	pic2
addiu	$23,$23,2

beq	$2,$0,$L83
lw	$28,24($sp)

.option	pic0
j	$L98
.option	pic2
lw	$31,76($sp)

$L97:
addiu	$4,$17,1040
move	$5,$0
li	$6,256			# 0x100
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$16,$16,-1

lui	$2,%hi(aac_channel_layout)
sll	$16,$16,3
sw	$0,1360($17)
addiu	$2,$2,%lo(aac_channel_layout)
addu	$2,$16,$2
lw	$3,4($2)
lw	$2,0($2)
sw	$3,852($18)
.option	pic0
j	$L86
.option	pic2
sw	$2,848($18)

$L90:
addiu	$21,$sp,32
li	$22,4			# 0x4
li	$23,16			# 0x10
$L82:
move	$16,$0
move	$6,$16
$L99:
sw	$21,16($sp)
move	$4,$17
move	$5,$19
.option	pic0
jal	che_configure
.option	pic2
move	$7,$20

bne	$2,$0,$L84
addiu	$16,$16,1

bne	$16,$22,$L99
move	$6,$16

addiu	$20,$20,1
bne	$20,$23,$L82
addiu	$2,$17,1040

addiu	$3,$17,784
move	$8,$2
$L89:
lwl	$7,3($3)
lwl	$6,7($3)
lwl	$5,11($3)
lwl	$4,15($3)
lwr	$7,0($3)
lwr	$6,4($3)
lwr	$5,8($3)
lwr	$4,12($3)
addiu	$3,$3,16
swl	$7,3($2)
swr	$7,0($2)
swl	$6,7($2)
swr	$6,4($2)
swl	$5,11($2)
swr	$5,8($2)
swl	$4,15($2)
swr	$4,12($2)
bne	$3,$8,$L89
addiu	$2,$2,16

li	$2,64			# 0x40
move	$3,$0
sw	$2,1360($17)
move	$2,$0
sw	$3,852($18)
sw	$2,848($18)
$L86:
lw	$3,32($sp)
move	$2,$0
sw	$3,68($18)
lw	$3,96($sp)
sw	$3,10512($17)
$L84:
lw	$31,76($sp)
$L98:
lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	output_configure
.size	output_configure, .-output_configure
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	apply_channel_coupling.constprop.25
.type	apply_channel_coupling.constprop.25, @function
apply_channel_coupling.constprop.25:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
li	$14,65536			# 0x10000
li	$25,51344			# 0xc890
sw	$18,12($sp)
sw	$17,8($sp)
addiu	$9,$4,912
sw	$16,4($sp)
addu	$25,$5,$25
sw	$fp,36($sp)
addiu	$15,$4,976
sw	$23,32($sp)
li	$24,3			# 0x3
sw	$22,28($sp)
addiu	$16,$14,10200
sw	$21,24($sp)
li	$13,2			# 0x2
sw	$20,20($sp)
li	$12,1			# 0x1
sw	$19,16($sp)
li	$18,2048			# 0x800
li	$17,1024			# 0x400
.option	pic0
j	$L102
.option	pic2
addiu	$5,$5,13584

$L104:
addiu	$9,$9,4
beq	$9,$15,$L126
lw	$fp,36($sp)

$L102:
lw	$11,0($9)
beq	$11,$0,$L104
addu	$2,$11,$14

lw	$3,10128($2)
bne	$3,$24,$L104
nop

lw	$10,10132($2)
bltz	$10,$L104
addiu	$10,$10,18935

addu	$3,$11,$16
sll	$10,$10,2
move	$8,$0
addu	$10,$11,$10
.option	pic0
j	$L114
.option	pic2
addiu	$19,$11,13584

$L105:
lw	$2,0($3)
move	$20,$12
xori	$2,$2,0x3
movz	$20,$13,$2
addu	$8,$8,$20
$L110:
addiu	$3,$3,4
$L127:
beq	$3,$10,$L104
nop

$L114:
lw	$2,-64($3)
bne	$2,$6,$L105
nop

lw	$2,-32($3)
bne	$2,$7,$L105
nop

lw	$23,0($3)
bne	$23,$12,$L106
sll	$20,$8,9

lw	$21,20($4)
move	$2,$8
$L107:
sll	$20,$2,5
lwc1	$f3,9980($4)
sll	$2,$2,9
xori	$21,$21,0x1
subu	$20,$2,$20
move	$2,$17
movz	$2,$18,$21
addu	$20,$11,$20
addiu	$8,$8,1
addu	$20,$14,$20
move	$21,$2
lwc1	$f2,10232($20)
sll	$21,$21,2
move	$2,$25
addu	$21,$25,$21
move	$20,$19
$L112:
lwc1	$f0,0($20)
addiu	$2,$2,4
addiu	$20,$20,4
lwc1	$f1,-4($2)
sub.s	$f0,$f0,$f3
mul.s	$f0,$f0,$f2
add.s	$f0,$f1,$f0
bne	$2,$21,$L112
swc1	$f0,-4($2)

.option	pic0
j	$L127
.option	pic2
addiu	$3,$3,4

$L106:
lw	$21,20($4)
sll	$fp,$8,5
lwc1	$f3,9980($4)
move	$2,$5
subu	$fp,$20,$fp
xori	$22,$21,0x1
addu	$fp,$11,$fp
move	$20,$18
addu	$fp,$14,$fp
movn	$20,$17,$22
lwc1	$f2,10232($fp)
move	$22,$20
sll	$22,$22,2
move	$20,$19
addu	$22,$5,$22
$L109:
lwc1	$f0,0($20)
addiu	$2,$2,4
addiu	$20,$20,4
lwc1	$f1,-4($2)
sub.s	$f0,$f0,$f3
mul.s	$f0,$f0,$f2
add.s	$f0,$f1,$f0
bne	$2,$22,$L109
swc1	$f0,-4($2)

beq	$23,$0,$L116
nop

beq	$23,$13,$L110
addiu	$8,$8,1

$L116:
.option	pic0
j	$L107
.option	pic2
move	$2,$8

$L126:
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	apply_channel_coupling.constprop.25
.size	apply_channel_coupling.constprop.25, .-apply_channel_coupling.constprop.25
.section	.rodata.str1.4
.align	2
$LC2:
.ascii	"Reserved bit set.\012\000"
.align	2
$LC3:
.ascii	"Invalid Predictor Reset Group.\012\000"
.align	2
$LC4:
.ascii	"Prediction is not allowed in AAC-LC.\012\000"
.align	2
$LC5:
.ascii	"Predictor bit set but LTP is\000"
.align	2
$LC6:
.ascii	"Number of scalefactor bands in group (%d) exceeds limit "
.ascii	"(%d).\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_ics_info.isra.21
.type	decode_ics_info.isra.21, @function
decode_ics_info.isra.21:
.frame	$sp,40,$31		# vars= 0, regs= 2/0, args= 24, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
lw	$7,8($6)
addiu	$sp,$sp,-40
lui	$28,%hi(__gnu_local_gp)
lw	$2,0($6)
sw	$16,32($sp)
move	$16,$5
srl	$5,$7,3
sw	$31,36($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
addu	$5,$2,$5
andi	$8,$7,0x7
.cprestore	24
addiu	$9,$7,1
lbu	$3,0($5)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L165
sw	$9,8($6)
.set	macro
.set	reorder

lw	$3,4($16)
addiu	$7,$7,3
srl	$5,$9,3
li	$8,16711680			# 0xff0000
addu	$2,$2,$5
sw	$3,8($16)
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
sw	$7,8($6)
srl	$3,$5,8
sll	$2,$5,8
lbu	$7,12($16)
li	$5,-16777216			# 0xffffffffff000000
and	$3,$3,$8
ori	$5,$5,0xff00
and	$2,$2,$5
sb	$7,13($16)
or	$2,$3,$2
lw	$7,8($6)
sll	$3,$2,16
lw	$10,0($6)
srl	$2,$2,16
andi	$9,$9,0x7
or	$2,$3,$2
sll	$2,$2,$9
srl	$3,$7,3
srl	$2,$2,30
addu	$10,$10,$3
andi	$11,$7,0x7
sw	$2,4($16)
li	$9,1			# 0x1
lbu	$3,0($10)
addiu	$7,$7,1
li	$10,2			# 0x2
sll	$3,$3,$11
sw	$7,8($6)
sw	$9,16($16)
andi	$3,$3,0x00ff
sb	$9,20($16)
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$2,$10,$L166
sb	$3,12($16)
.set	macro
.set	reorder

lw	$11,8($6)
lw	$3,0($6)
srl	$2,$11,3
andi	$12,$11,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
and	$3,$3,$8
and	$7,$7,$5
or	$2,$3,$7
lw	$7,%got(ff_aac_num_swb_1024)($28)
sll	$3,$2,16
srl	$2,$2,16
addiu	$11,$11,6
or	$2,$3,$2
lw	$3,%got(ff_tns_max_bands_1024)($28)
sll	$2,$2,$12
sw	$11,8($6)
srl	$2,$2,26
sw	$9,40($16)
sb	$2,0($16)
lw	$11,8($4)
lw	$2,%got(ff_swb_offset_1024)($28)
sll	$12,$11,2
addu	$7,$7,$11
addu	$3,$3,$11
addu	$2,$2,$12
lbu	$7,0($7)
lbu	$3,0($3)
lw	$2,0($2)
sw	$7,36($16)
sw	$3,44($16)
sw	$2,28($16)
lw	$3,8($6)
lw	$11,0($6)
srl	$7,$3,3
andi	$12,$3,0x7
addu	$7,$11,$7
addiu	$3,$3,1
lbu	$2,0($7)
sw	$3,8($6)
sw	$0,56($16)
sll	$2,$2,$12
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L147
sw	$2,48($16)
.set	macro
.set	reorder

lw	$2,4($4)
beq	$2,$9,$L167
.set	noreorder
.set	nomacro
beq	$2,$10,$L168
lw	$4,0($4)
.set	macro
.set	reorder

lui	$5,%hi($LC5)
lw	$25,%call16(av_log_missing_feature)($28)
li	$6,1			# 0x1
addiu	$5,$5,%lo($LC5)
$L164:
jalr	$25
move	$5,$0
lw	$28,24($sp)
li	$6,104			# 0x68
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,36($sp)
lw	$16,32($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L147:
lbu	$3,0($16)
$L163:
lw	$7,36($16)
$L146:
slt	$5,$7,$3
.set	noreorder
.set	nomacro
bne	$5,$0,$L169
move	$2,$0
.set	macro
.set	reorder

$L130:
lw	$31,36($sp)
lw	$16,32($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L166:
lw	$7,8($6)
lw	$2,0($6)
srl	$3,$7,3
andi	$10,$7,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$8,$2,$8
and	$3,$3,$5
or	$5,$8,$3
sll	$2,$5,16
srl	$5,$5,16
addiu	$7,$7,4
or	$5,$2,$5
sll	$5,$5,$10
sw	$7,8($6)
srl	$5,$5,28
sb	$5,0($16)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L132
sw	$3,8($6)
.set	macro
.set	reorder

lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
sb	$9,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L134
sw	$3,8($6)
.set	macro
.set	reorder

$L170:
lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
li	$3,1			# 0x1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L136
sw	$3,8($6)
.set	macro
.set	reorder

$L171:
lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
li	$3,1			# 0x1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L138
sw	$3,8($6)
.set	macro
.set	reorder

$L172:
lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
li	$3,1			# 0x1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L140
sw	$3,8($6)
.set	macro
.set	reorder

$L173:
lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
li	$3,1			# 0x1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L142
sw	$3,8($6)
.set	macro
.set	reorder

$L174:
lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
li	$3,1			# 0x1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L144
sw	$3,8($6)
.set	macro
.set	reorder

$L175:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
$L145:
lw	$7,8($4)
li	$2,8			# 0x8
lw	$5,%got(ff_swb_offset_128)($28)
lbu	$3,0($16)
sll	$6,$7,2
sw	$2,40($16)
lw	$2,%got(ff_tns_max_bands_128)($28)
addu	$6,$5,$6
lw	$5,%got(ff_aac_num_swb_128)($28)
sw	$0,48($16)
addu	$2,$2,$7
addu	$5,$5,$7
lw	$6,0($6)
lbu	$2,0($2)
lbu	$7,0($5)
sw	$6,28($16)
sw	$2,44($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L146
.option	pic2
sw	$7,36($16)
.set	macro
.set	reorder

$L132:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L170
sw	$3,8($6)
.set	macro
.set	reorder

$L134:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L171
sw	$3,8($6)
.set	macro
.set	reorder

$L136:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L172
sw	$3,8($6)
.set	macro
.set	reorder

$L138:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L173
sw	$3,8($6)
.set	macro
.set	reorder

$L140:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L174
sw	$3,8($6)
.set	macro
.set	reorder

$L142:
lw	$2,16($16)
addiu	$2,$2,-1
addu	$2,$16,$2
lbu	$3,20($2)
addiu	$3,$3,1
sb	$3,20($2)
lw	$3,8($6)
lw	$5,0($6)
srl	$2,$3,3
andi	$7,$3,0x7
addu	$5,$5,$2
addiu	$3,$3,1
lbu	$2,0($5)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L175
sw	$3,8($6)
.set	macro
.set	reorder

$L144:
lw	$2,16($16)
addiu	$3,$2,1
addu	$2,$16,$2
sw	$3,16($16)
li	$3,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L145
.option	pic2
sb	$3,20($2)
.set	macro
.set	reorder

$L168:
lui	$6,%hi($LC4)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
j	$L164
.option	pic2
addiu	$6,$6,%lo($LC4)
.set	macro
.set	reorder

$L167:
lw	$3,8($6)
srl	$7,$3,3
andi	$10,$3,0x7
addu	$7,$11,$7
addiu	$9,$3,1
lbu	$2,0($7)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L149
sw	$9,8($6)
.set	macro
.set	reorder

lw	$10,%got(ff_aac_pred_sfb_max)($28)
$L177:
lw	$2,8($4)
lbu	$3,0($16)
addu	$2,$10,$2
lbu	$2,0($2)
sltu	$5,$3,$2
movn	$2,$3,$5
.set	noreorder
.set	nomacro
beq	$2,$0,$L163
move	$5,$0
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L178
.option	pic2
lw	$3,8($6)
.set	macro
.set	reorder

$L176:
lw	$11,0($6)
lw	$3,8($6)
$L178:
addu	$8,$16,$5
addiu	$5,$5,1
srl	$7,$3,3
andi	$9,$3,0x7
addu	$7,$11,$7
addiu	$3,$3,1
lbu	$2,0($7)
sw	$3,8($6)
sll	$2,$2,$9
andi	$2,$2,0x00ff
srl	$2,$2,7
sb	$2,60($8)
lw	$2,8($4)
lbu	$3,0($16)
addu	$2,$10,$2
lbu	$2,0($2)
sltu	$7,$3,$2
movn	$2,$3,$7
slt	$2,$5,$2
bne	$2,$0,$L176
.option	pic0
.set	noreorder
.set	nomacro
j	$L146
.option	pic2
lw	$7,36($16)
.set	macro
.set	reorder

$L165:
lui	$6,%hi($LC2)
lw	$4,0($4)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
j	$L164
.option	pic2
addiu	$6,$6,%lo($LC2)
.set	macro
.set	reorder

$L149:
srl	$2,$9,3
andi	$9,$9,0x7
addu	$2,$11,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$7,8
sll	$7,$7,8
and	$8,$2,$8
and	$5,$7,$5
or	$5,$8,$5
sll	$2,$5,16
srl	$5,$5,16
addiu	$3,$3,6
or	$2,$2,$5
sll	$9,$2,$9
sw	$3,8($6)
srl	$2,$9,27
.set	noreorder
.set	nomacro
beq	$2,$0,$L153
sw	$2,56($16)
.set	macro
.set	reorder

li	$3,31			# 0x1f
.set	noreorder
.set	nomacro
bne	$2,$3,$L177
lw	$10,%got(ff_aac_pred_sfb_max)($28)
.set	macro
.set	reorder

$L153:
lui	$6,%hi($LC3)
lw	$4,0($4)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
j	$L164
.option	pic2
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

$L169:
lui	$6,%hi($LC6)
lw	$25,%call16(av_log)($28)
lw	$4,0($4)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC6)
sw	$7,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$7,$3
.set	macro
.set	reorder

move	$5,$0
lw	$28,24($sp)
li	$6,104			# 0x68
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L130
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	decode_ics_info.isra.21
.size	decode_ics_info.isra.21, .-decode_ics_info.isra.21
.section	.rodata.str1.4
.align	2
$LC7:
.ascii	"Sample rate index in program config element does not mat"
.ascii	"ch the sample rate index configured by the container.\012"
.ascii	"\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_pce.isra.20
.type	decode_pce.isra.20, @function
decode_pce.isra.20:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
lw	$10,8($7)
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-64
lw	$2,0($7)
addiu	$28,$28,%lo(__gnu_local_gp)
addiu	$3,$10,2
sw	$16,24($sp)
move	$16,$7
sw	$17,28($sp)
srl	$7,$3,3
sw	$31,60($sp)
sw	$fp,56($sp)
andi	$9,$3,0x7
sw	$23,52($sp)
addiu	$10,$10,6
sw	$22,48($sp)
move	$17,$6
sw	$21,44($sp)
sw	$20,40($sp)
sw	$19,36($sp)
sw	$18,32($sp)
.cprestore	16
sw	$3,8($16)
addu	$3,$2,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
li	$3,16711680			# 0xff0000
sw	$10,8($16)
srl	$7,$8,8
lw	$11,0($5)
addiu	$3,$3,255
sll	$8,$8,8
and	$7,$7,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$8,$8,$3
or	$3,$7,$8
sll	$5,$3,16
srl	$3,$3,16
or	$3,$5,$3
sll	$3,$3,$9
srl	$3,$3,28
.set	noreorder
.set	nomacro
beq	$3,$11,$L180
sw	$4,64($sp)
.set	macro
.set	reorder

lui	$6,%hi($LC7)
lw	$25,%call16(av_log)($28)
lw	$4,0($4)
li	$5,24			# 0x18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

lw	$28,16($sp)
lw	$2,0($16)
lw	$10,8($16)
$L180:
addiu	$11,$10,21
addiu	$25,$10,4
addiu	$24,$10,8
addiu	$15,$10,12
addiu	$14,$10,14
addiu	$13,$10,17
srl	$5,$11,3
srl	$4,$24,3
srl	$9,$14,3
srl	$6,$25,3
srl	$19,$15,3
srl	$18,$13,3
addu	$5,$2,$5
srl	$3,$10,3
addu	$6,$2,$6
addu	$3,$2,$3
addu	$19,$2,$19
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
addu	$18,$2,$18
sw	$25,8($16)
addu	$3,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($6)  
lwr $7, 0($6)  

# 0 "" 2
#NO_APP
li	$12,16711680			# 0xff0000
sw	$24,8($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
sw	$15,8($16)
addu	$3,$2,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($19)  
lwr $9, 0($19)  

# 0 "" 2
#NO_APP
sw	$14,8($16)
addiu	$12,$12,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($3)  
lwr $19, 0($3)  

# 0 "" 2
#NO_APP
sw	$13,8($16)
move	$3,$19
srl	$20,$19,8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($18)  
lwr $19, 0($18)  

# 0 "" 2
#NO_APP
sw	$11,8($16)
move	$18,$19
lbu	$22,0($5)
li	$5,-16777216			# 0xffffffffff000000
srl	$fp,$8,8
srl	$23,$7,8
srl	$4,$6,8
srl	$21,$9,8
sll	$8,$8,8
sll	$7,$7,8
sll	$6,$6,8
sll	$9,$9,8
sll	$3,$3,8
srl	$19,$19,8
sll	$18,$18,8
ori	$5,$5,0xff00
and	$fp,$fp,$12
and	$23,$23,$12
and	$6,$6,$5
and	$21,$21,$12
and	$20,$20,$12
and	$8,$8,$5
and	$7,$7,$5
and	$4,$4,$12
and	$9,$9,$5
and	$3,$3,$5
and	$12,$19,$12
and	$5,$18,$5
or	$4,$4,$6
or	$8,$fp,$8
or	$7,$23,$7
or	$9,$21,$9
or	$3,$20,$3
or	$5,$12,$5
andi	$6,$11,0x7
sll	$fp,$8,16
sll	$12,$5,16
sll	$23,$7,16
sll	$11,$4,16
sll	$21,$9,16
sll	$20,$3,16
srl	$8,$8,16
srl	$7,$7,16
srl	$4,$4,16
srl	$9,$9,16
srl	$3,$3,16
srl	$5,$5,16
sll	$6,$22,$6
or	$8,$fp,$8
or	$4,$11,$4
or	$5,$12,$5
andi	$fp,$10,0x7
or	$7,$23,$7
andi	$25,$25,0x7
andi	$24,$24,0x7
or	$9,$21,$9
andi	$15,$15,0x7
or	$3,$20,$3
andi	$14,$14,0x7
andi	$13,$13,0x7
andi	$6,$6,0x00ff
addiu	$12,$10,22
sll	$24,$4,$24
sll	$8,$8,$fp
sll	$25,$7,$25
sw	$12,8($16)
sll	$15,$9,$15
sll	$14,$3,$14
sll	$13,$5,$13
srl	$6,$6,7
srl	$4,$8,28
srl	$25,$25,28
srl	$24,$24,28
srl	$15,$15,30
srl	$14,$14,29
.set	noreorder
.set	nomacro
beq	$6,$0,$L181
srl	$13,$13,28
.set	macro
.set	reorder

addiu	$12,$10,26
sw	$12,8($16)
$L181:
srl	$5,$12,3
andi	$3,$12,0x7
addu	$5,$2,$5
addiu	$7,$12,1
lbu	$5,0($5)
sll	$3,$5,$3
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L182
sw	$7,8($16)
.set	macro
.set	reorder

addiu	$7,$12,5
sw	$7,8($16)
$L182:
srl	$6,$7,3
andi	$5,$7,0x7
addu	$6,$2,$6
addiu	$3,$7,1
lbu	$6,0($6)
sll	$5,$6,$5
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
beq	$5,$0,$L183
sw	$3,8($16)
.set	macro
.set	reorder

addiu	$3,$7,4
sw	$3,8($16)
$L183:
addiu	$11,$4,-1
addiu	$6,$17,64
.set	noreorder
.set	nomacro
beq	$4,$0,$L184
move	$10,$11
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$0,$L260
sll	$9,$4,2
.set	macro
.set	reorder

li	$11,16711680			# 0xff0000
addu	$9,$9,$4
li	$10,-16777216			# 0xffffffffff000000
addu	$9,$9,$3
addiu	$8,$3,1
addiu	$11,$11,255
li	$12,1			# 0x1
ori	$10,$10,0xff00
$L189:
srl	$5,$3,3
srl	$18,$8,3
addu	$5,$2,$5
addu	$18,$2,$18
andi	$4,$3,0x7
lbu	$5,0($5)
andi	$7,$8,0x7
sw	$8,8($16)
addiu	$3,$3,5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($18)  
lwr $19, 0($18)  

# 0 "" 2
#NO_APP
srl	$18,$19,8
sll	$19,$19,8
sw	$3,8($16)
and	$18,$18,$11
and	$19,$19,$10
or	$19,$18,$19
sll	$4,$5,$4
sll	$18,$19,16
srl	$5,$19,16
andi	$4,$4,0x00ff
or	$5,$18,$5
srl	$4,$4,7
sll	$7,$5,$7
move	$5,$17
movn	$5,$6,$4
srl	$4,$7,28
addiu	$8,$8,5
sll	$4,$4,2
addu	$4,$5,$4
.set	noreorder
.set	nomacro
bne	$9,$3,$L189
sw	$12,0($4)
.set	macro
.set	reorder

$L184:
addiu	$10,$25,-1
.set	noreorder
.set	nomacro
beq	$25,$0,$L191
move	$9,$10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$0,$L261
li	$11,16711680			# 0xff0000
.set	macro
.set	reorder

sll	$9,$25,2
addu	$9,$9,$25
li	$10,-16777216			# 0xffffffffff000000
addu	$9,$9,$3
addiu	$8,$3,1
addiu	$11,$11,255
li	$12,2			# 0x2
ori	$10,$10,0xff00
$L196:
srl	$5,$3,3
srl	$18,$8,3
addu	$5,$2,$5
addu	$18,$2,$18
andi	$4,$3,0x7
lbu	$5,0($5)
andi	$7,$8,0x7
sw	$8,8($16)
addiu	$3,$3,5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($18)  
lwr $19, 0($18)  

# 0 "" 2
#NO_APP
srl	$18,$19,8
sll	$19,$19,8
sw	$3,8($16)
and	$18,$18,$11
and	$19,$19,$10
or	$19,$18,$19
sll	$4,$5,$4
sll	$18,$19,16
srl	$5,$19,16
andi	$4,$4,0x00ff
or	$5,$18,$5
srl	$4,$4,7
sll	$7,$5,$7
move	$5,$17
movn	$5,$6,$4
srl	$4,$7,28
addiu	$8,$8,5
sll	$4,$4,2
addu	$4,$5,$4
.set	noreorder
.set	nomacro
bne	$9,$3,$L196
sw	$12,0($4)
.set	macro
.set	reorder

$L191:
addiu	$9,$24,-1
.set	noreorder
.set	nomacro
beq	$24,$0,$L198
move	$8,$9
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$0,$L262
sll	$4,$24,2
.set	macro
.set	reorder

li	$11,16711680			# 0xff0000
addu	$24,$4,$24
li	$10,-16777216			# 0xffffffffff000000
addu	$24,$3,$24
addiu	$9,$3,1
addiu	$11,$11,255
li	$12,3			# 0x3
ori	$10,$10,0xff00
$L203:
srl	$7,$3,3
srl	$8,$9,3
addu	$7,$2,$7
addu	$8,$2,$8
andi	$4,$3,0x7
lbu	$5,0($7)
andi	$7,$9,0x7
sw	$9,8($16)
addiu	$3,$3,5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 3($8)  
lwr $18, 0($8)  

# 0 "" 2
#NO_APP
srl	$8,$18,8
sll	$18,$18,8
sw	$3,8($16)
and	$8,$8,$11
and	$18,$18,$10
or	$18,$8,$18
sll	$4,$5,$4
sll	$8,$18,16
srl	$5,$18,16
andi	$4,$4,0x00ff
or	$5,$8,$5
srl	$4,$4,7
sll	$7,$5,$7
move	$5,$17
movn	$5,$6,$4
srl	$4,$7,28
addiu	$9,$9,5
sll	$4,$4,2
addu	$4,$5,$4
.set	noreorder
.set	nomacro
bne	$24,$3,$L203
sw	$12,0($4)
.set	macro
.set	reorder

$L198:
addiu	$12,$15,-1
.set	noreorder
.set	nomacro
beq	$15,$0,$L205
move	$8,$12
.set	macro
.set	reorder

li	$10,16711680			# 0xff0000
li	$9,-16777216			# 0xffffffffff000000
addiu	$10,$10,255
li	$15,4			# 0x4
li	$11,-1			# 0xffffffffffffffff
move	$7,$3
ori	$9,$9,0xff00
$L206:
srl	$5,$7,3
andi	$18,$7,0x7
addu	$5,$2,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$6,8
sll	$6,$6,8
and	$5,$5,$10
and	$6,$6,$9
or	$6,$5,$6
sll	$5,$6,16
srl	$6,$6,16
addiu	$7,$7,4
or	$4,$5,$6
sll	$4,$4,$18
addiu	$8,$8,-1
sw	$7,8($16)
srl	$4,$4,28
sll	$4,$4,2
addu	$4,$17,$4
.set	noreorder
.set	nomacro
bne	$8,$11,$L206
sw	$15,192($4)
.set	macro
.set	reorder

sll	$12,$12,2
addu	$3,$3,$12
addiu	$3,$3,4
$L205:
sll	$14,$14,2
addiu	$9,$13,-1
addu	$3,$14,$3
addiu	$17,$17,128
move	$8,$9
.set	noreorder
.set	nomacro
beq	$13,$0,$L207
sw	$3,8($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$17,$0,$L263
sll	$4,$13,2
.set	macro
.set	reorder

li	$10,16711680			# 0xff0000
addu	$13,$4,$13
li	$8,-16777216			# 0xffffffffff000000
addiu	$13,$13,1
addiu	$7,$3,1
addu	$13,$13,$3
addiu	$10,$10,255
li	$11,5			# 0x5
ori	$8,$8,0xff00
$L211:
srl	$5,$7,3
sw	$7,8($16)
andi	$14,$7,0x7
addu	$5,$2,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$6,8
sll	$6,$6,8
and	$5,$5,$10
and	$6,$6,$8
or	$6,$5,$6
sll	$5,$6,16
srl	$6,$6,16
addiu	$12,$7,4
or	$4,$5,$6
sll	$4,$4,$14
addiu	$7,$7,5
sw	$12,8($16)
srl	$4,$4,28
sll	$4,$4,2
addu	$4,$17,$4
.set	noreorder
.set	nomacro
bne	$7,$13,$L211
sw	$11,0($4)
.set	macro
.set	reorder

sll	$4,$9,2
addu	$9,$4,$9
addu	$3,$3,$9
addiu	$3,$3,5
$L207:
subu	$4,$0,$3
andi	$4,$4,0x7
beq	$4,$0,$L213
addu	$3,$4,$3
sw	$3,8($16)
$L213:
srl	$4,$3,3
lw	$7,12($16)
addu	$2,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$5,$4,8
addiu	$2,$2,255
sll	$6,$4,8
and	$4,$5,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$5,$3,0x7
ori	$2,$2,0xff00
and	$2,$6,$2
or	$2,$4,$2
sll	$4,$2,16
srl	$2,$2,16
addiu	$3,$3,8
or	$2,$4,$2
sll	$2,$2,$5
subu	$4,$7,$3
srl	$2,$2,24
sll	$2,$2,3
slt	$4,$4,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L264
sw	$3,8($16)
.set	macro
.set	reorder

addu	$3,$2,$3
move	$2,$0
sw	$3,8($16)
$L215:
lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L260:
li	$12,16711680			# 0xff0000
li	$9,-16777216			# 0xffffffffff000000
addiu	$12,$12,255
li	$19,1			# 0x1
li	$18,-1			# 0xffffffffffffffff
move	$8,$3
ori	$9,$9,0xff00
$L186:
srl	$4,$8,3
andi	$20,$8,0x7
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($4)  
lwr $7, 0($4)  

# 0 "" 2
#NO_APP
srl	$5,$7,8
sll	$7,$7,8
and	$5,$5,$12
and	$7,$7,$9
or	$7,$5,$7
sll	$5,$7,16
srl	$7,$7,16
addiu	$8,$8,4
or	$4,$5,$7
sll	$4,$4,$20
addiu	$10,$10,-1
sw	$8,8($16)
srl	$4,$4,28
sll	$4,$4,2
addu	$4,$17,$4
.set	noreorder
.set	nomacro
bne	$10,$18,$L186
sw	$19,0($4)
.set	macro
.set	reorder

sll	$11,$11,2
addu	$3,$3,$11
.option	pic0
.set	noreorder
.set	nomacro
j	$L184
.option	pic2
addiu	$3,$3,4
.set	macro
.set	reorder

$L261:
li	$12,16711680			# 0xff0000
li	$11,-16777216			# 0xffffffffff000000
addiu	$12,$12,255
li	$19,2			# 0x2
li	$18,-1			# 0xffffffffffffffff
move	$8,$3
ori	$11,$11,0xff00
$L193:
srl	$4,$8,3
andi	$20,$8,0x7
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($4)  
lwr $7, 0($4)  

# 0 "" 2
#NO_APP
srl	$5,$7,8
sll	$7,$7,8
and	$5,$5,$12
and	$7,$7,$11
or	$7,$5,$7
sll	$5,$7,16
srl	$7,$7,16
addiu	$8,$8,4
or	$4,$5,$7
sll	$4,$4,$20
addiu	$9,$9,-1
sw	$8,8($16)
srl	$4,$4,28
sll	$4,$4,2
addu	$4,$17,$4
.set	noreorder
.set	nomacro
bne	$9,$18,$L193
sw	$19,0($4)
.set	macro
.set	reorder

sll	$10,$10,2
addu	$3,$3,$10
.option	pic0
.set	noreorder
.set	nomacro
j	$L191
.option	pic2
addiu	$3,$3,4
.set	macro
.set	reorder

$L262:
li	$11,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
addiu	$11,$11,255
li	$18,3			# 0x3
li	$12,-1			# 0xffffffffffffffff
move	$7,$3
ori	$10,$10,0xff00
$L200:
srl	$4,$7,3
andi	$19,$7,0x7
addu	$4,$2,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$5,$6,8
sll	$6,$6,8
and	$5,$5,$11
and	$6,$6,$10
or	$6,$5,$6
sll	$5,$6,16
srl	$6,$6,16
addiu	$7,$7,4
or	$4,$5,$6
sll	$4,$4,$19
addiu	$8,$8,-1
sw	$7,8($16)
srl	$4,$4,28
sll	$4,$4,2
addu	$4,$17,$4
.set	noreorder
.set	nomacro
bne	$8,$12,$L200
sw	$18,0($4)
.set	macro
.set	reorder

sll	$9,$9,2
addu	$3,$3,$9
.option	pic0
.set	noreorder
.set	nomacro
j	$L198
.option	pic2
addiu	$3,$3,4
.set	macro
.set	reorder

$L263:
li	$11,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
addiu	$11,$11,255
li	$13,5			# 0x5
li	$12,-1			# 0xffffffffffffffff
move	$7,$3
ori	$10,$10,0xff00
$L209:
srl	$5,$7,3
andi	$14,$7,0x7
addu	$5,$2,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$6,8
sll	$6,$6,8
and	$5,$5,$11
and	$6,$6,$10
or	$6,$5,$6
sll	$5,$6,16
srl	$6,$6,16
addiu	$7,$7,4
or	$4,$5,$6
sll	$4,$4,$14
addiu	$8,$8,-1
sw	$7,8($16)
srl	$4,$4,28
sll	$4,$4,2
.set	noreorder
.set	nomacro
bne	$8,$12,$L209
sw	$13,0($4)
.set	macro
.set	reorder

sll	$9,$9,2
addu	$3,$3,$9
.option	pic0
.set	noreorder
.set	nomacro
j	$L207
.option	pic2
addiu	$3,$3,4
.set	macro
.set	reorder

$L264:
lw	$19,64($sp)
lui	$6,%hi(overread_err)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
addiu	$6,$6,%lo(overread_err)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
lw	$4,0($19)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L215
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	decode_pce.isra.20
.size	decode_pce.isra.20, .-decode_pce.isra.20
.section	.rodata.str1.4
.align	2
$LC8:
.ascii	"960/120 MDCT window is\000"
.section	.text.unlikely
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_ga_specific_config
.type	decode_ga_specific_config, @function
decode_ga_specific_config:
.frame	$sp,320,$31		# vars= 256, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
lui	$28,%hi(__gnu_local_gp)
sw	$16,292($sp)
move	$16,$5
sw	$17,296($sp)
move	$17,$6
lw	$5,8($5)
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$6,0($16)
sw	$18,300($sp)
move	$18,$4
srl	$7,$5,3
sw	$31,316($sp)
sw	$21,312($sp)
andi	$4,$5,0x7
addu	$7,$6,$7
sw	$20,308($sp)
sw	$19,304($sp)
addiu	$3,$5,1
.cprestore	24
lbu	$2,0($7)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L266
sw	$3,8($16)

lui	$5,%hi($LC8)
lw	$25,%call16(av_log_missing_feature)($28)
lw	$4,0($18)
li	$6,1			# 0x1
.reloc	1f,R_MIPS_JALR,av_log_missing_feature
1:	jalr	$25
addiu	$5,$5,%lo($LC8)

.option	pic0
j	$L267
.option	pic2
li	$2,-1			# 0xffffffffffffffff

$L266:
srl	$4,$3,3
andi	$2,$3,0x7
addu	$3,$6,$4
addiu	$4,$5,2
lbu	$3,0($3)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L268
sw	$4,8($16)

addiu	$5,$5,16
sw	$5,8($16)
$L268:
lw	$19,8($16)
srl	$3,$19,3
addiu	$2,$19,1
addu	$6,$6,$3
li	$3,6			# 0x6
lbu	$20,0($6)
sw	$2,8($16)
lw	$2,4($18)
beq	$2,$3,$L269
li	$3,20			# 0x14

bne	$2,$3,$L290
lw	$25,%call16(memset)($28)

$L269:
addiu	$2,$19,4
sw	$2,8($16)
lw	$25,%call16(memset)($28)
$L290:
addiu	$21,$sp,32
move	$5,$0
li	$6,256			# 0x100
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$21

bne	$17,$0,$L271
addiu	$5,$18,8

lw	$2,8($16)
move	$4,$18
move	$6,$21
addiu	$2,$2,4
move	$7,$16
.option	pic0
jal	decode_pce.isra.20
.option	pic2
sw	$2,8($16)

bne	$2,$0,$L288
lw	$31,316($sp)

li	$2,3			# 0x3
$L289:
addiu	$5,$18,528
move	$4,$18
sw	$2,16($sp)
move	$6,$21
.option	pic0
jal	output_configure
.option	pic2
move	$7,$17

beq	$2,$0,$L286
andi	$19,$19,0x7

.option	pic0
j	$L288
.option	pic2
lw	$31,316($sp)

$L271:
move	$4,$18
move	$5,$21
.option	pic0
jal	set_default_channel_config.isra.13
.option	pic2
move	$6,$17

bne	$2,$0,$L288
lw	$31,316($sp)

.option	pic0
j	$L289
.option	pic2
li	$2,3			# 0x3

$L286:
sll	$2,$20,$19
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L279
move	$2,$0

lw	$2,4($18)
addiu	$3,$2,-17
sltu	$2,$3,7
beq	$2,$0,$L274
nop

li	$2,1			# 0x1
sll	$2,$2,$3
andi	$3,$2,0x4d
bne	$3,$0,$L275
nop

andi	$2,$2,0x20
beq	$2,$0,$L274
nop

lw	$2,8($16)
addiu	$2,$2,16
.option	pic0
j	$L274
.option	pic2
sw	$2,8($16)

$L275:
lw	$2,8($16)
addiu	$2,$2,3
sw	$2,8($16)
$L274:
lw	$3,8($16)
move	$2,$0
addiu	$3,$3,1
sw	$3,8($16)
$L279:
$L267:
lw	$31,316($sp)
$L288:
lw	$21,312($sp)
lw	$20,308($sp)
lw	$19,304($sp)
lw	$18,300($sp)
lw	$17,296($sp)
lw	$16,292($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	decode_ga_specific_config
.size	decode_ga_specific_config, .-decode_ga_specific_config
.section	.rodata.str1.4
.align	2
$LC9:
.ascii	"SBR+\000"
.align	2
$LC10:
.ascii	"\000"
.align	2
$LC14:
.ascii	"invalid sampling rate index %d\012\000"
.align	2
$LC15:
.ascii	"Audio object type %s%d is not supported.\012\000"
.section	.text.unlikely
.align	2
.set	nomips16
.set	nomicromips
.ent	aac_decode_init
.type	aac_decode_init, @function
aac_decode_init:
.frame	$sp,136,$31		# vars= 24, regs= 10/2, args= 56, gp= 8
.mask	0xc0ff0000,-12
.fmask	0x00300000,-8
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-136
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,92($sp)
move	$17,$4
sw	$31,124($sp)
sw	$fp,120($sp)
sw	$23,116($sp)
sw	$22,112($sp)
sw	$21,108($sp)
sw	$20,104($sp)
sw	$19,100($sp)
sw	$18,96($sp)
sw	$16,88($sp)
.cprestore	56
lw	$2,%got(aac_bugs)($28)
sdc1	$f20,128($sp)
sw	$0,0($2)
li	$2,7			# 0x7
#APP
# 618 "aacdec.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
lw	$2,64($4)
lw	$16,136($4)
lw	$6,28($4)
sw	$4,0($16)
.set	noreorder
.set	nomacro
bgtz	$6,$L292
sw	$2,12($16)
.set	macro
.set	reorder

lw	$22,%got(ff_aac_spectral_bits)($28)
$L309:
lui	$19,%hi(vlc_spectral)
lw	$fp,%got(ff_aac_spectral_codes)($28)
li	$18,1			# 0x1
lw	$23,%got(ff_aac_spectral_sizes)($28)
addiu	$19,$19,%lo(vlc_spectral)
li	$20,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
lw	$2,0($22)
li	$21,4			# 0x4
lw	$3,0($fp)
li	$5,8			# 0x8
lhu	$6,0($23)
move	$4,$19
sw	$18,72($17)
sw	$2,80($sp)
lui	$2,%hi(table.6927)
sw	$3,24($sp)
addiu	$2,$2,%lo(table.6927)
sw	$18,16($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,4($19)
li	$2,304			# 0x130
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,12($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+16)
lw	$2,4($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+16)
lw	$3,4($fp)
lhu	$6,2($23)
sw	$2,80($sp)
lui	$2,%hi(table.6928)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6928)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,20($19)
li	$2,270			# 0x10e
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,28($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+32)
lw	$2,8($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+32)
lw	$3,8($fp)
lhu	$6,4($23)
sw	$2,80($sp)
lui	$2,%hi(table.6929)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6929)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,36($19)
li	$2,550			# 0x226
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,44($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+48)
lw	$2,12($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+48)
lw	$3,12($fp)
lhu	$6,6($23)
sw	$2,80($sp)
lui	$2,%hi(table.6930)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6930)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,52($19)
li	$2,300			# 0x12c
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,60($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+64)
lw	$2,16($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+64)
lw	$3,16($fp)
lhu	$6,8($23)
sw	$2,80($sp)
lui	$2,%hi(table.6931)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6931)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,68($19)
li	$2,328			# 0x148
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,76($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+80)
lw	$2,20($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+80)
lw	$3,20($fp)
lhu	$6,10($23)
sw	$2,80($sp)
lui	$2,%hi(table.6932)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6932)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,84($19)
li	$2,294			# 0x126
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,92($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+96)
lw	$2,24($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+96)
lw	$3,24($fp)
lhu	$6,12($23)
sw	$2,80($sp)
lui	$2,%hi(table.6933)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6933)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,100($19)
li	$2,306			# 0x132
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,108($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+112)
lw	$2,28($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+112)
lw	$3,28($fp)
lhu	$6,14($23)
sw	$2,80($sp)
lui	$2,%hi(table.6934)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6934)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,116($19)
li	$2,268			# 0x10c
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,124($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+128)
lw	$2,32($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+128)
lw	$3,32($fp)
lhu	$6,16($23)
sw	$2,80($sp)
lui	$2,%hi(table.6935)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6935)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,132($19)
li	$2,510			# 0x1fe
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,140($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$4,%hi(vlc_spectral+144)
lw	$2,36($22)
li	$5,8			# 0x8
lw	$28,56($sp)
addiu	$4,$4,%lo(vlc_spectral+144)
lw	$3,36($fp)
lhu	$6,18($23)
sw	$2,80($sp)
lui	$2,%hi(table.6936)
sw	$18,16($sp)
addiu	$2,$2,%lo(table.6936)
sw	$3,24($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$2,148($19)
li	$2,366			# 0x16e
sw	$20,32($sp)
sw	$0,36($sp)
sw	$2,156($19)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
lw	$25,%call16(init_vlc_sparse)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
lw	$7,80($sp)
.set	macro
.set	reorder

lui	$2,%hi(table.6937)
lw	$28,56($sp)
lui	$4,%hi(vlc_spectral+160)
addiu	$2,$2,%lo(table.6937)
lw	$3,40($fp)
lw	$22,40($22)
li	$5,8			# 0x8
lhu	$6,20($23)
addiu	$4,$4,%lo(vlc_spectral+160)
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,164($19)
li	$2,462			# 0x1ce
move	$7,$22
sw	$3,24($sp)
sw	$22,80($sp)
sw	$2,172($19)
sw	$18,16($sp)
sw	$18,20($sp)
sw	$20,28($sp)
sw	$20,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$21,48($sp)
.set	macro
.set	reorder

lw	$28,56($sp)
lw	$25,%call16(ff_aac_sbr_init)($28)
.reloc	1f,R_MIPS_JALR,ff_aac_sbr_init
1:	jalr	$25
addiu	$4,$16,5600
lw	$28,56($sp)
lw	$25,%call16(dsputil_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

li	$3,523108352			# 0x1f2e0000
lw	$28,56($sp)
lui	$2,%hi(vlc_scalefactors)
addiu	$3,$3,15692
addiu	$2,$2,%lo(vlc_scalefactors)
li	$5,7			# 0x7
sw	$3,9720($16)
li	$6,121			# 0x79
lw	$3,%got(ff_aac_scalefactor_code)($28)
move	$4,$2
lw	$7,%got(ff_aac_scalefactor_bits)($28)
lw	$25,%call16(init_vlc_sparse)($28)
sw	$18,16($sp)
sw	$3,24($sp)
lui	$3,%hi(table.6938)
sw	$18,20($sp)
addiu	$3,$3,%lo(table.6938)
sw	$21,28($sp)
sw	$21,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$21,48($sp)
sw	$3,4($2)
li	$3,352			# 0x160
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)
.set	macro
.set	reorder

lui	$2,%hi($LC11)
lw	$28,56($sp)
addiu	$4,$16,5472
ldc1	$f20,%lo($LC11)($2)
li	$5,11			# 0xb
li	$6,1			# 0x1
lw	$25,%call16(aac_mdct_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_mdct_init
1:	jalr	$25
sdc1	$f20,16($sp)
.set	macro
.set	reorder

addiu	$4,$16,5536
lw	$28,56($sp)
li	$5,8			# 0x8
li	$6,1			# 0x1
lw	$25,%call16(aac_mdct_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_mdct_init
1:	jalr	$25
sdc1	$f20,16($sp)
.set	macro
.set	reorder

lui	$2,%hi($LC12)
lw	$28,56($sp)
li	$6,1024			# 0x400
lw	$25,%call16(aac_kbd_window_init)($28)
lw	$4,%got(ff_aac_kbd_long_1024)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_kbd_window_init
1:	jalr	$25
lw	$5,%lo($LC12)($2)
.set	macro
.set	reorder

lui	$2,%hi($LC13)
lw	$28,56($sp)
li	$6,128			# 0x80
lw	$25,%call16(aac_kbd_window_init)($28)
lw	$4,%got(ff_aac_kbd_short_128)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_kbd_window_init
1:	jalr	$25
lw	$5,%lo($LC13)($2)
.set	macro
.set	reorder

lw	$28,56($sp)
lw	$25,%call16(ff_init_ff_sine_windows)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_ff_sine_windows
1:	jalr	$25
li	$4,10			# 0xa
.set	macro
.set	reorder

lw	$28,56($sp)
lw	$25,%call16(ff_init_ff_sine_windows)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_ff_sine_windows
1:	jalr	$25
li	$4,7			# 0x7
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L293
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L292:
sll	$3,$6,3
sra	$2,$3,3
.set	noreorder
.set	nomacro
bgez	$2,$L301
lw	$5,24($4)
.set	macro
.set	reorder

move	$2,$0
move	$3,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L294
.option	pic2
move	$7,$0
.set	macro
.set	reorder

$L301:
move	$7,$5
$L294:
addu	$2,$7,$2
lw	$25,%call16(ff_mpeg4audio_get_config)($28)
addiu	$4,$16,4
sw	$7,64($sp)
sw	$3,76($sp)
sw	$2,68($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4audio_get_config
1:	jalr	$25
sw	$0,72($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L306
lw	$28,56($sp)
.set	macro
.set	reorder

lw	$7,8($16)
slt	$3,$7,13
.set	noreorder
.set	nomacro
bne	$3,$0,$L296
lw	$3,72($sp)
.set	macro
.set	reorder

lui	$6,%hi($LC14)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L293
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L296:
lw	$6,4($16)
lw	$5,20($16)
addu	$2,$3,$2
addiu	$3,$6,-1
sltu	$3,$3,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L297
sw	$2,72($sp)
.set	macro
.set	reorder

lw	$6,16($16)
addiu	$5,$sp,64
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_ga_specific_config
.option	pic2
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L306
lw	$28,56($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L309
.option	pic2
lw	$22,%got(ff_aac_spectral_bits)($28)
.set	macro
.set	reorder

$L297:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$5,$2,$L303
lw	$4,0($16)
.set	macro
.set	reorder

lui	$7,%hi($LC10)
.option	pic0
.set	noreorder
.set	nomacro
j	$L299
.option	pic2
addiu	$7,$7,%lo($LC10)
.set	macro
.set	reorder

$L303:
lui	$7,%hi($LC9)
addiu	$7,$7,%lo($LC9)
$L299:
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
sw	$6,16($sp)
lui	$6,%hi($LC15)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC15)
.set	macro
.set	reorder

$L306:
li	$2,-1			# 0xffffffffffffffff
$L293:
lw	$31,124($sp)
lw	$fp,120($sp)
lw	$23,116($sp)
lw	$22,112($sp)
lw	$21,108($sp)
lw	$20,104($sp)
lw	$19,100($sp)
lw	$18,96($sp)
lw	$17,92($sp)
lw	$16,88($sp)
ldc1	$f20,128($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

.end	aac_decode_init
.size	aac_decode_init, .-aac_decode_init
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	imdct_and_windowing
.type	imdct_and_windowing, @function
imdct_and_windowing:
.frame	$sp,80,$31		# vars= 16, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-80
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$20,56($sp)
addiu	$3,$5,5248
sw	$19,52($sp)
move	$20,$5
sw	$18,48($sp)
move	$19,$4
sw	$17,44($sp)
addiu	$18,$5,9344
sw	$31,76($sp)
sw	$fp,72($sp)
sw	$23,68($sp)
sw	$22,64($sp)
sw	$21,60($sp)
sw	$16,40($sp)
.cprestore	16
sw	$3,24($sp)
lbu	$2,12($5)
.set	noreorder
.set	nomacro
bne	$2,$0,$L345
addiu	$17,$5,13440
.set	macro
.set	reorder

lui	$8,%hi(aac_sine_short_128)
addiu	$fp,$8,%lo(aac_sine_short_128)
$L311:
lbu	$2,13($20)
.set	noreorder
.set	nomacro
bne	$2,$0,$L346
lw	$9,%got(ff_aac_kbd_long_1024)($28)
.set	macro
.set	reorder

lui	$2,%hi(aac_sine_long_1024)
lw	$3,4($20)
lui	$7,%hi(aac_sine_short_128)
addiu	$2,$2,%lo(aac_sine_long_1024)
addiu	$22,$7,%lo(aac_sine_short_128)
addiu	$16,$19,1376
sw	$2,32($sp)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L372
addiu	$23,$19,10000
.set	macro
.set	reorder

$L313:
addiu	$8,$19,5472
lw	$25,%call16(aac_imdct_half_c)($28)
lw	$6,24($sp)
move	$5,$16
move	$4,$8
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
sw	$8,36($sp)
.set	macro
.set	reorder

lw	$8,36($sp)
$L315:
lw	$2,8($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L316
lw	$11,4($20)
.set	macro
.set	reorder

li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L316
addiu	$12,$20,11136
.set	macro
.set	reorder

$L378:
move	$3,$18
move	$2,$17
$L319:
lwl	$10,3($3)
lwl	$6,7($3)
lwl	$5,11($3)
lwl	$4,15($3)
lwr	$10,0($3)
lwr	$6,4($3)
lwr	$5,8($3)
lwr	$4,12($3)
addiu	$3,$3,16
swl	$10,3($2)
swr	$10,0($2)
swl	$6,7($2)
swr	$6,4($2)
swl	$5,11($2)
swr	$5,8($2)
swl	$4,15($2)
swr	$4,12($2)
.set	noreorder
.set	nomacro
bne	$3,$12,$L319
addiu	$2,$2,16
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$11,$2,$L373
addiu	$13,$20,15488
.set	macro
.set	reorder

addiu	$21,$20,11392
addiu	$5,$22,508
addiu	$4,$19,1628
move	$2,$0
li	$12,2			# 0x2
li	$11,31			# 0x1f
li	$15,252			# 0xfc
li	$14,256			# 0x100
$L331:
addu	$3,$21,$2
lw	$6,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001100000100001100110	#S32MUL XR1,XR2,$3,$6
# 0 "" 2
#NO_APP
addu	$10,$22,$2
lw	$10,0($10)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010100001000011100110	#S32MUL XR3,XR4,$3,$10
# 0 "" 2
#NO_APP
lw	$3,0($4)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010101000100001000100	#S32MSUB XR1,XR2,$3,$10
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001101001000011000000	#S32MADD XR3,XR4,$3,$6
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$13,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$15,$2
addu	$3,$13,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L331
addiu	$4,$4,-4
.set	macro
.set	reorder

move	$2,$0
li	$7,1792			# 0x700
$L332:
addu	$6,$16,$2
addu	$3,$17,$2
addiu	$2,$2,4
lw	$6,256($6)
.set	noreorder
.set	nomacro
bne	$2,$7,$L332
sw	$6,2304($3)
.set	macro
.set	reorder

lw	$2,4($20)
$L376:
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$3,$L374
li	$3,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L375
addiu	$2,$19,3424
.set	macro
.set	reorder

addiu	$19,$19,3424
$L344:
lwl	$5,3($19)
lwl	$4,7($19)
lwl	$3,11($19)
lwl	$2,15($19)
lwr	$5,0($19)
lwr	$4,4($19)
lwr	$3,8($19)
lwr	$2,12($19)
addiu	$19,$19,16
swl	$5,3($18)
swr	$5,0($18)
swl	$4,7($18)
swr	$4,4($18)
swl	$3,11($18)
swr	$3,8($18)
swl	$2,15($18)
swr	$2,12($18)
.set	noreorder
.set	nomacro
bne	$19,$8,$L344
addiu	$18,$18,16
.set	macro
.set	reorder

lw	$31,76($sp)
$L377:
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L316:
sltu	$2,$11,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L378
addiu	$12,$20,11136
.set	macro
.set	reorder

lw	$2,32($sp)
addiu	$12,$20,15488
addiu	$15,$20,11392
addiu	$4,$19,3420
addiu	$5,$2,4092
li	$11,2			# 0x2
move	$2,$0
li	$10,31			# 0x1f
li	$14,2044			# 0x7fc
li	$13,2048			# 0x800
$L322:
addu	$3,$15,$2
lw	$6,0($5)
lw	$3,-2048($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001100000100001100110	#S32MUL XR1,XR2,$3,$6
# 0 "" 2
#NO_APP
lw	$9,32($sp)
addu	$7,$9,$2
lw	$7,0($7)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011001110001000011100110	#S32MUL XR3,XR4,$3,$7
# 0 "" 2
#NO_APP
lw	$3,0($4)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011001111000100001000100	#S32MSUB XR1,XR2,$3,$7
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001101001000011000000	#S32MADD XR3,XR4,$3,$6
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001011010101100100001100110	#S32EXTRV XR1,XR2,$11,$10
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001011010101101000011100110	#S32EXTRV XR3,XR4,$11,$10
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-2048
addu	$3,$12,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$14,$2
addu	$3,$12,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$13,$L322
addiu	$4,$4,-4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L376
.option	pic2
lw	$2,4($20)
.set	macro
.set	reorder

$L346:
li	$2,2			# 0x2
lw	$3,4($20)
addiu	$16,$19,1376
lw	$22,%got(ff_aac_kbd_short_128)($28)
addiu	$23,$19,10000
.set	noreorder
.set	nomacro
bne	$3,$2,$L313
sw	$9,32($sp)
.set	macro
.set	reorder

$L372:
addiu	$2,$19,5536
move	$21,$0
sw	$2,28($sp)
$L314:
lw	$3,24($sp)
addu	$5,$16,$21
lw	$25,%call16(aac_imdct_half_c)($28)
lw	$4,28($sp)
addu	$6,$3,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
addiu	$21,$21,512
.set	macro
.set	reorder

li	$9,4096			# 0x1000
.set	noreorder
.set	nomacro
bne	$21,$9,$L314
lw	$28,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L315
.option	pic2
addiu	$8,$19,5472
.set	macro
.set	reorder

$L345:
.option	pic0
.set	noreorder
.set	nomacro
j	$L311
.option	pic2
lw	$fp,%got(ff_aac_kbd_short_128)($28)
.set	macro
.set	reorder

$L374:
move	$2,$0
li	$4,256			# 0x100
$L335:
addu	$6,$23,$2
addu	$3,$18,$2
addiu	$2,$2,4
lw	$6,256($6)
.set	noreorder
.set	nomacro
bne	$2,$4,$L335
sw	$6,0($3)
.set	macro
.set	reorder

addiu	$4,$fp,508
addiu	$15,$19,3936
addiu	$12,$20,9856
addiu	$6,$19,4188
move	$2,$0
li	$11,2			# 0x2
li	$10,31			# 0x1f
li	$14,252			# 0xfc
li	$13,256			# 0x100
move	$5,$4
$L336:
addu	$3,$15,$2
lw	$7,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001110000100001100110	#S32MUL XR1,XR2,$3,$7
# 0 "" 2
#NO_APP
addu	$9,$fp,$2
lw	$9,0($9)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010010001000011100110	#S32MUL XR3,XR4,$3,$9
# 0 "" 2
#NO_APP
lw	$3,0($6)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010011000100001000100	#S32MSUB XR1,XR2,$3,$9
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001111001000011000000	#S32MADD XR3,XR4,$3,$7
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001011010101100100001100110	#S32EXTRV XR1,XR2,$11,$10
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001011010101101000011100110	#S32EXTRV XR3,XR4,$11,$10
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$12,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$14,$2
addu	$3,$12,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$13,$L336
addiu	$6,$6,-4
.set	macro
.set	reorder

addiu	$15,$19,4448
addiu	$12,$20,10368
addiu	$6,$19,4700
move	$2,$0
li	$11,2			# 0x2
li	$10,31			# 0x1f
li	$14,252			# 0xfc
li	$13,256			# 0x100
move	$5,$4
$L337:
addu	$3,$15,$2
lw	$7,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001110000100001100110	#S32MUL XR1,XR2,$3,$7
# 0 "" 2
#NO_APP
addu	$9,$fp,$2
lw	$9,0($9)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010010001000011100110	#S32MUL XR3,XR4,$3,$9
# 0 "" 2
#NO_APP
lw	$3,0($6)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010011000100001000100	#S32MSUB XR1,XR2,$3,$9
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001111001000011000000	#S32MADD XR3,XR4,$3,$7
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001011010101100100001100110	#S32EXTRV XR1,XR2,$11,$10
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001011010101101000011100110	#S32EXTRV XR3,XR4,$11,$10
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$12,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$14,$2
addu	$3,$12,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$13,$L337
addiu	$6,$6,-4
.set	macro
.set	reorder

addiu	$14,$19,4960
addiu	$11,$20,10880
addiu	$5,$19,5212
move	$2,$0
li	$10,2			# 0x2
li	$9,31			# 0x1f
li	$13,252			# 0xfc
li	$12,256			# 0x100
$L338:
addu	$3,$14,$2
lw	$6,0($4)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001100000100001100110	#S32MUL XR1,XR2,$3,$6
# 0 "" 2
#NO_APP
addu	$7,$fp,$2
lw	$7,0($7)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011001110001000011100110	#S32MUL XR3,XR4,$3,$7
# 0 "" 2
#NO_APP
lw	$3,0($5)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011001111000100001000100	#S32MSUB XR1,XR2,$3,$7
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001101001000011000000	#S32MADD XR3,XR4,$3,$6
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001010010011100100001100110	#S32EXTRV XR1,XR2,$10,$9
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001010010011101000011100110	#S32EXTRV XR3,XR4,$10,$9
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$11,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$13,$2
addu	$3,$11,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$4,$4,-4
.set	noreorder
.set	nomacro
bne	$2,$12,$L338
addiu	$5,$5,-4
.set	macro
.set	reorder

addiu	$19,$19,5216
addiu	$20,$20,11136
$L339:
lwl	$5,3($19)
lwl	$4,7($19)
lwl	$3,11($19)
lwl	$2,15($19)
lwr	$5,0($19)
lwr	$4,4($19)
lwr	$3,8($19)
lwr	$2,12($19)
addiu	$19,$19,16
swl	$5,3($20)
swr	$5,0($20)
swl	$4,7($20)
swr	$4,4($20)
swl	$3,11($20)
swr	$3,8($20)
swl	$2,15($20)
swr	$2,12($20)
.set	noreorder
.set	nomacro
bne	$19,$8,$L339
addiu	$20,$20,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L377
.option	pic2
lw	$31,76($sp)
.set	macro
.set	reorder

$L373:
addiu	$16,$20,11392
addiu	$5,$22,508
addiu	$4,$19,1628
move	$2,$0
li	$12,2			# 0x2
li	$11,31			# 0x1f
li	$15,252			# 0xfc
li	$14,256			# 0x100
$L325:
addu	$3,$16,$2
lw	$6,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001100000100001100110	#S32MUL XR1,XR2,$3,$6
# 0 "" 2
#NO_APP
addu	$10,$22,$2
lw	$10,0($10)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010100001000011100110	#S32MUL XR3,XR4,$3,$10
# 0 "" 2
#NO_APP
lw	$3,0($4)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010101000100001000100	#S32MSUB XR1,XR2,$3,$10
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001101001000011000000	#S32MADD XR3,XR4,$3,$6
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$13,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$15,$2
addu	$3,$13,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L325
addiu	$4,$4,-4
.set	macro
.set	reorder

addiu	$4,$fp,508
addiu	$16,$19,1888
addiu	$13,$20,16000
addiu	$6,$19,2140
move	$2,$0
li	$12,2			# 0x2
li	$11,31			# 0x1f
li	$15,252			# 0xfc
li	$14,256			# 0x100
move	$5,$4
$L326:
addu	$3,$16,$2
lw	$7,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001110000100001100110	#S32MUL XR1,XR2,$3,$7
# 0 "" 2
#NO_APP
addu	$10,$fp,$2
lw	$10,0($10)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010100001000011100110	#S32MUL XR3,XR4,$3,$10
# 0 "" 2
#NO_APP
lw	$3,0($6)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010101000100001000100	#S32MSUB XR1,XR2,$3,$10
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001111001000011000000	#S32MADD XR3,XR4,$3,$7
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$13,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$15,$2
addu	$3,$13,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L326
addiu	$6,$6,-4
.set	macro
.set	reorder

addiu	$16,$19,2400
addiu	$13,$20,16512
addiu	$6,$19,2652
move	$2,$0
li	$12,2			# 0x2
li	$11,31			# 0x1f
li	$15,252			# 0xfc
li	$14,256			# 0x100
move	$5,$4
$L327:
addu	$3,$16,$2
lw	$7,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001110000100001100110	#S32MUL XR1,XR2,$3,$7
# 0 "" 2
#NO_APP
addu	$10,$fp,$2
lw	$10,0($10)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010100001000011100110	#S32MUL XR3,XR4,$3,$10
# 0 "" 2
#NO_APP
lw	$3,0($6)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010101000100001000100	#S32MSUB XR1,XR2,$3,$10
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001111001000011000000	#S32MADD XR3,XR4,$3,$7
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$13,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$15,$2
addu	$3,$13,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L327
addiu	$6,$6,-4
.set	macro
.set	reorder

addiu	$16,$19,2912
addiu	$13,$20,17024
addiu	$6,$19,3164
move	$2,$0
li	$12,2			# 0x2
li	$11,31			# 0x1f
li	$15,252			# 0xfc
li	$14,256			# 0x100
move	$5,$4
$L328:
addu	$3,$16,$2
lw	$7,0($5)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001110000100001100110	#S32MUL XR1,XR2,$3,$7
# 0 "" 2
#NO_APP
addu	$10,$fp,$2
lw	$10,0($10)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011010100001000011100110	#S32MUL XR3,XR4,$3,$10
# 0 "" 2
#NO_APP
lw	$3,0($6)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011010101000100001000100	#S32MSUB XR1,XR2,$3,$10
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001111001000011000000	#S32MADD XR3,XR4,$3,$7
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$13,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$15,$2
addu	$3,$13,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L328
addiu	$6,$6,-4
.set	macro
.set	reorder

addiu	$15,$19,3424
addiu	$12,$19,10256
addiu	$5,$19,3676
move	$2,$0
li	$11,2			# 0x2
li	$10,31			# 0x1f
li	$14,252			# 0xfc
li	$13,256			# 0x100
$L329:
addu	$3,$15,$2
lw	$6,0($4)
lw	$3,-256($3)
#APP
# 1936 "aacdec.c" 1
.word	0b01110000011001100000100001100110	#S32MUL XR1,XR2,$3,$6
# 0 "" 2
#NO_APP
addu	$7,$fp,$2
lw	$7,0($7)
#APP
# 1937 "aacdec.c" 1
.word	0b01110000011001110001000011100110	#S32MUL XR3,XR4,$3,$7
# 0 "" 2
#NO_APP
lw	$3,0($5)
#APP
# 1938 "aacdec.c" 1
.word	0b01110000011001111000100001000100	#S32MSUB XR1,XR2,$3,$7
# 0 "" 2
# 1939 "aacdec.c" 1
.word	0b01110000011001101001000011000000	#S32MADD XR3,XR4,$3,$6
# 0 "" 2
# 1940 "aacdec.c" 1
.word	0b01110001011010101100100001100110	#S32EXTRV XR1,XR2,$11,$10
# 0 "" 2
# 1941 "aacdec.c" 1
.word	0b01110001011010101101000011100110	#S32EXTRV XR3,XR4,$11,$10
# 0 "" 2
# 1942 "aacdec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
#NO_APP
addiu	$3,$2,-256
addu	$3,$12,$3
#APP
# 1943 "aacdec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
subu	$3,$14,$2
addu	$3,$12,$3
#APP
# 1944 "aacdec.c" 1
.word	0b01110000011000000000000110010001	#S32STD XR6,$3,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$4,$4,-4
.set	noreorder
.set	nomacro
bne	$2,$13,$L329
addiu	$5,$5,-4
.set	macro
.set	reorder

addiu	$2,$20,17280
addiu	$10,$23,256
move	$3,$23
$L330:
lwl	$7,3($3)
lwl	$6,7($3)
lwl	$5,11($3)
lwl	$4,15($3)
lwr	$7,0($3)
lwr	$6,4($3)
lwr	$5,8($3)
lwr	$4,12($3)
addiu	$3,$3,16
swl	$7,3($2)
swr	$7,0($2)
swl	$6,7($2)
swr	$6,4($2)
swl	$5,11($2)
swr	$5,8($2)
swl	$4,15($2)
swr	$4,12($2)
.set	noreorder
.set	nomacro
bne	$3,$10,$L330
addiu	$2,$2,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L376
.option	pic2
lw	$2,4($20)
.set	macro
.set	reorder

$L375:
addiu	$19,$19,5216
$L342:
lwl	$6,3($2)
lwl	$5,7($2)
lwl	$4,11($2)
lwl	$3,15($2)
lwr	$6,0($2)
lwr	$5,4($2)
lwr	$4,8($2)
lwr	$3,12($2)
addiu	$2,$2,16
swl	$6,3($18)
swr	$6,0($18)
swl	$5,7($18)
swr	$5,4($18)
swl	$4,11($18)
swr	$4,8($18)
swl	$3,15($18)
swr	$3,12($18)
.set	noreorder
.set	nomacro
bne	$2,$19,$L342
addiu	$18,$18,16
.set	macro
.set	reorder

addiu	$20,$20,11136
$L343:
lwl	$6,3($2)
lwl	$5,7($2)
lwl	$4,11($2)
lwl	$3,15($2)
lwr	$6,0($2)
lwr	$5,4($2)
lwr	$4,8($2)
lwr	$3,12($2)
addiu	$2,$2,16
swl	$6,3($20)
swr	$6,0($20)
swl	$5,7($20)
swr	$5,4($20)
swl	$4,11($20)
swr	$4,8($20)
swl	$3,15($20)
swr	$3,12($20)
.set	noreorder
.set	nomacro
bne	$2,$8,$L343
addiu	$20,$20,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L377
.option	pic2
lw	$31,76($sp)
.set	macro
.set	reorder

.end	imdct_and_windowing
.size	imdct_and_windowing, .-imdct_and_windowing
.section	.rodata.str1.4
.align	2
$LC16:
.ascii	"invalid band type\012\000"
.align	2
$LC17:
.ascii	"Number of bands (%d) exceeds limit (%d).\012\000"
.align	2
$LC18:
.ascii	"%s (%d) out of range.\012\000"
.align	2
$LC19:
.ascii	"Intensity stereo position\000"
.align	2
$LC20:
.ascii	"Noise gain\000"
.align	2
$LC21:
.ascii	"Global gain\000"
.align	2
$LC22:
.ascii	"Pulse tool not allowed in eight short sequence.\012\000"
.align	2
$LC23:
.ascii	"TNS filter order %d is greater than maximum %d.\012\000"
.align	2
$LC24:
.ascii	"SSR\000"
.align	2
$LC25:
.ascii	"error in spectral data, ESC overflow\012\000"
.align	2
$LC26:
.ascii	"Pulse data corrupt or invalid.\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_ics.constprop.27
.type	decode_ics.constprop.27, @function
decode_ics.constprop.27:
.frame	$sp,216,$31		# vars= 128, regs= 10/4, args= 24, gp= 8
.mask	0xc0ff0000,-20
.fmask	0x00f00000,-8
addiu	$sp,$sp,-216
lw	$8,8($6)
li	$9,16711680			# 0xff0000
sw	$16,160($sp)
lui	$28,%hi(__gnu_local_gp)
lw	$16,0($6)
srl	$2,$8,3
addiu	$9,$9,255
sw	$23,188($sp)
andi	$10,$8,0x7
sw	$22,184($sp)
addu	$2,$16,$2
sw	$19,172($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$31,196($sp)
sll	$3,$3,8
sw	$fp,192($sp)
and	$2,$2,$9
sw	$21,180($sp)
li	$9,-16777216			# 0xffffffffff000000
sw	$20,176($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$18,168($sp)
ori	$9,$9,0xff00
sw	$17,164($sp)
and	$3,$3,$9
sdc1	$f22,208($sp)
or	$2,$2,$3
.cprestore	24
sll	$3,$2,16
sdc1	$f20,200($sp)
srl	$2,$2,16
sw	$5,220($sp)
addiu	$8,$8,8
or	$2,$3,$2
addiu	$3,$5,5248
sll	$2,$2,$10
sw	$8,8($6)
move	$19,$6
move	$22,$4
sw	$3,108($sp)
.set	noreorder
.set	nomacro
bne	$7,$0,$L380
srl	$23,$2,24
.set	macro
.set	reorder

.option	pic0
jal	decode_ics_info.isra.21
.option	pic2
.set	noreorder
.set	nomacro
bltz	$2,$L624
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$16,0($19)
$L380:
lw	$8,220($sp)
li	$2,2			# 0x2
lw	$9,220($sp)
lw	$8,4($8)
addiu	$21,$9,3124
addiu	$17,$9,3636
.set	noreorder
.set	nomacro
beq	$8,$2,$L632
sw	$8,72($sp)
.set	macro
.set	reorder

lw	$2,16($9)
.set	noreorder
.set	nomacro
blez	$2,$L633
li	$8,31			# 0x1f
.set	macro
.set	reorder

li	$12,5			# 0x5
li	$11,27			# 0x1b
$L383:
lw	$9,220($sp)
li	$10,16711680			# 0xff0000
lw	$13,220($sp)
move	$18,$0
move	$14,$0
addiu	$10,$10,255
lbu	$3,0($9)
li	$9,-16777216			# 0xffffffffff000000
li	$15,12			# 0xc
ori	$9,$9,0xff00
move	$7,$0
$L394:
slt	$2,$7,$3
beq	$2,$0,$L634
$L395:
lw	$4,8($19)
andi	$5,$7,0x00ff
srl	$2,$4,3
andi	$24,$4,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($2)  
lwr $20, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$20,8
sll	$2,$20,8
and	$3,$3,$10
and	$2,$2,$9
or	$2,$3,$2
sll	$6,$2,16
srl	$2,$2,16
addiu	$4,$4,4
or	$2,$6,$2
sll	$6,$2,$24
srl	$6,$6,28
.set	noreorder
.set	nomacro
bne	$6,$15,$L386
sw	$4,8($19)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L635
.option	pic2
lui	$6,%hi($LC16)
.set	macro
.set	reorder

$L388:
addu	$5,$5,$8
andi	$5,$5,0x00ff
$L386:
srl	$2,$4,3
andi	$20,$4,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$2,$2,8
srl	$3,$3,8
and	$2,$2,$9
and	$3,$3,$10
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addu	$4,$4,$12
or	$2,$3,$2
sll	$2,$2,$20
srl	$2,$2,$11
.set	noreorder
.set	nomacro
beq	$2,$8,$L388
sw	$4,8($19)
.set	macro
.set	reorder

lw	$3,12($19)
addu	$2,$5,$2
subu	$4,$3,$4
.set	noreorder
.set	nomacro
bltz	$4,$L636
andi	$2,$2,0x00ff
.set	macro
.set	reorder

lbu	$3,0($13)
sltu	$4,$3,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L390
slt	$4,$7,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L394
subu	$20,$2,$7
.set	macro
.set	reorder

sll	$24,$14,2
sll	$20,$20,2
move	$4,$0
$L393:
addu	$3,$4,$24
addiu	$4,$4,4
addu	$5,$21,$3
addu	$3,$17,$3
sw	$6,0($5)
.set	noreorder
.set	nomacro
bne	$4,$20,$L393
sw	$2,0($3)
.set	macro
.set	reorder

lbu	$3,0($13)
subu	$14,$14,$7
move	$7,$2
addu	$14,$14,$2
slt	$2,$7,$3
bne	$2,$0,$L395
$L634:
lw	$4,16($13)
addiu	$18,$18,1
slt	$2,$18,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L394
move	$7,$0
.set	macro
.set	reorder

lw	$7,72($sp)
li	$14,12			# 0xc
lw	$8,220($sp)
move	$24,$23
lw	$3,9988($22)
addiu	$18,$23,-90
xori	$2,$7,0x2
movn	$14,$0,$2
addiu	$8,$8,4116
sw	$8,92($sp)
.set	noreorder
.set	nomacro
blez	$4,$L398
addu	$14,$3,$14
.set	macro
.set	reorder

lui	$3,%hi(vlc_scalefactors+4)
lw	$11,220($sp)
li	$23,16711680			# 0xff0000
sw	$0,80($sp)
lui	$8,%hi(aac_pow2sf_tab)
sw	$22,76($sp)
move	$25,$0
lw	$fp,%lo(vlc_scalefactors+4)($3)
li	$7,100			# 0x64
lbu	$2,0($11)
li	$9,1			# 0x1
addiu	$23,$23,255
addiu	$8,$8,%lo(aac_pow2sf_tab)
move	$10,$21
move	$11,$17
move	$20,$24
move	$17,$25
move	$21,$16
$L399:
move	$22,$0
slt	$6,$0,$2
$L423:
beq	$6,$0,$L637
$L422:
sll	$5,$17,2
addu	$3,$10,$5
addu	$4,$11,$5
lw	$3,0($3)
.set	noreorder
.set	nomacro
bne	$3,$0,$L400
lw	$16,0($4)
.set	macro
.set	reorder

slt	$3,$22,$16
.set	noreorder
.set	nomacro
beq	$3,$0,$L423
lw	$25,%call16(memset)($28)
.set	macro
.set	reorder

lw	$12,92($sp)
subu	$6,$16,$22
subu	$17,$17,$22
sll	$6,$6,2
sw	$7,148($sp)
addu	$4,$12,$5
sw	$8,132($sp)
move	$5,$0
sw	$9,152($sp)
sw	$10,136($sp)
move	$22,$16
sw	$11,140($sp)
addu	$17,$16,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$14,144($sp)
.set	macro
.set	reorder

lw	$3,220($sp)
lw	$28,24($sp)
lw	$7,148($sp)
lw	$8,132($sp)
lbu	$2,0($3)
lw	$9,152($sp)
lw	$10,136($sp)
slt	$6,$16,$2
lw	$11,140($sp)
.set	noreorder
.set	nomacro
bne	$6,$0,$L422
lw	$14,144($sp)
.set	macro
.set	reorder

$L637:
lw	$12,220($sp)
lw	$3,16($12)
lw	$12,80($sp)
addiu	$12,$12,1
slt	$3,$12,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L399
sw	$12,80($sp)
.set	macro
.set	reorder

lw	$22,76($sp)
move	$16,$21
move	$21,$10
$L398:
lw	$4,8($19)
srl	$5,$4,3
andi	$6,$4,0x7
addu	$5,$16,$5
addiu	$2,$4,1
lbu	$3,0($5)
sw	$2,8($19)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L620
sw	$3,124($sp)
.set	macro
.set	reorder

lw	$3,72($sp)
li	$8,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$8,$L547
addiu	$12,$4,3
.set	macro
.set	reorder

lw	$9,220($sp)
srl	$5,$2,3
srl	$3,$12,3
li	$6,16711680			# 0xff0000
lw	$11,36($9)
addu	$5,$16,$5
lw	$9,28($9)
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($5)  
lwr $10, 0($5)  

# 0 "" 2
#NO_APP
sw	$12,8($19)
li	$5,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$7
addiu	$6,$6,255
sll	$14,$3,8
srl	$7,$7,8
sll	$13,$10,8
ori	$5,$5,0xff00
srl	$3,$10,8
and	$7,$7,$6
and	$10,$14,$5
or	$10,$7,$10
and	$3,$3,$6
and	$7,$13,$5
or	$7,$3,$7
sll	$13,$7,16
sll	$3,$10,16
srl	$7,$7,16
srl	$10,$10,16
andi	$12,$12,0x7
or	$3,$3,$10
or	$7,$13,$7
andi	$10,$2,0x7
sll	$2,$3,$12
sll	$3,$7,$10
srl	$2,$2,26
srl	$3,$3,30
addiu	$10,$4,9
addiu	$3,$3,1
slt	$11,$2,$11
sw	$10,8($19)
.set	noreorder
.set	nomacro
beq	$11,$0,$L425
sw	$3,120($sp)
.set	macro
.set	reorder

srl	$3,$10,3
sll	$7,$2,1
addu	$2,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$6
and	$3,$3,$5
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addu	$7,$9,$7
or	$2,$3,$2
andi	$10,$10,0x7
sll	$2,$2,$10
lhu	$3,0($7)
addiu	$7,$4,14
srl	$2,$2,27
addu	$3,$2,$3
sw	$7,8($19)
sltu	$2,$3,1024
.set	noreorder
.set	nomacro
beq	$2,$0,$L425
sw	$3,40($sp)
.set	macro
.set	reorder

srl	$2,$7,3
andi	$10,$7,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$9,8
sll	$9,$9,8
and	$2,$2,$6
and	$9,$9,$5
or	$2,$2,$9
sll	$9,$2,16
srl	$7,$2,16
addiu	$2,$4,18
or	$7,$9,$7
lw	$9,120($sp)
sll	$7,$7,$10
sw	$2,8($19)
srl	$7,$7,28
sw	$7,56($sp)
li	$7,1			# 0x1
beq	$9,$7,$L550
srl	$7,$2,3
andi	$2,$2,0x7
addu	$7,$16,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($7)  
lwr $9, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$9,8
sll	$9,$9,8
and	$7,$7,$6
and	$9,$9,$5
or	$7,$7,$9
sll	$9,$7,16
srl	$7,$7,16
addiu	$10,$4,23
or	$7,$9,$7
sll	$2,$7,$2
sw	$10,8($19)
srl	$2,$2,27
addu	$3,$2,$3
slt	$2,$3,1024
.set	noreorder
.set	nomacro
beq	$2,$0,$L425
sw	$3,44($sp)
.set	macro
.set	reorder

srl	$2,$10,3
lw	$11,120($sp)
andi	$7,$10,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$9,8
sll	$9,$9,8
and	$2,$2,$6
and	$9,$9,$5
or	$2,$2,$9
sll	$9,$2,16
srl	$2,$2,16
addiu	$10,$4,27
or	$2,$9,$2
sll	$7,$2,$7
sw	$10,8($19)
move	$2,$10
srl	$7,$7,28
.set	noreorder
.set	nomacro
beq	$11,$8,$L427
sw	$7,60($sp)
.set	macro
.set	reorder

srl	$7,$10,3
andi	$10,$10,0x7
addu	$7,$16,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($7)  
lwr $12, 0($7)  

# 0 "" 2
#NO_APP
srl	$2,$12,8
sll	$7,$12,8
and	$2,$2,$6
and	$7,$7,$5
or	$2,$2,$7
sll	$7,$2,16
srl	$2,$2,16
addiu	$8,$4,32
or	$2,$7,$2
sll	$2,$2,$10
sw	$8,8($19)
srl	$2,$2,27
addu	$3,$2,$3
slt	$2,$3,1024
.set	noreorder
.set	nomacro
beq	$2,$0,$L425
sw	$3,48($sp)
.set	macro
.set	reorder

srl	$2,$8,3
andi	$8,$8,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$7,8
sll	$7,$7,8
and	$2,$2,$6
and	$7,$7,$5
or	$2,$2,$7
sll	$9,$2,16
srl	$2,$2,16
addiu	$7,$4,36
or	$2,$9,$2
sll	$8,$2,$8
sw	$7,8($19)
srl	$8,$8,28
sw	$8,64($sp)
li	$8,4			# 0x4
.set	noreorder
.set	nomacro
bne	$11,$8,$L427
move	$2,$7
.set	macro
.set	reorder

srl	$2,$7,3
andi	$9,$7,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$8,8
sll	$8,$8,8
and	$6,$2,$6
and	$5,$8,$5
or	$5,$6,$5
sll	$2,$5,16
srl	$5,$5,16
addiu	$4,$4,41
or	$2,$2,$5
sll	$2,$2,$9
sw	$4,8($19)
srl	$2,$2,27
addu	$3,$2,$3
slt	$2,$3,1024
.set	noreorder
.set	nomacro
beq	$2,$0,$L425
sw	$3,52($sp)
.set	macro
.set	reorder

srl	$2,$4,3
li	$5,16711680			# 0xff0000
addu	$2,$16,$2
addiu	$5,$5,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$3,$3,$5
or	$2,$2,$3
sll	$5,$2,16
srl	$2,$2,16
andi	$3,$4,0x7
or	$2,$5,$2
sll	$3,$2,$3
addiu	$2,$7,9
srl	$3,$3,28
sw	$2,8($19)
sw	$3,68($sp)
$L427:
li	$3,1			# 0x1
sw	$3,124($sp)
$L426:
srl	$3,$2,3
lw	$8,220($sp)
andi	$4,$2,0x7
addu	$3,$16,$3
addiu	$2,$2,1
lbu	$3,0($3)
sw	$2,8($19)
sll	$2,$3,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L428
sw	$2,104($8)
.set	macro
.set	reorder

$L431:
lw	$5,8($19)
$L429:
srl	$3,$5,3
andi	$4,$5,0x7
addu	$16,$16,$3
addiu	$5,$5,1
lbu	$2,0($16)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L638
sw	$5,8($19)
.set	macro
.set	reorder

lw	$8,220($sp)
li	$16,1024			# 0x400
move	$17,$0
lw	$2,40($8)
lw	$9,28($8)
teq	$2,$0,7
div	$0,$16,$2
sw	$9,104($sp)
.set	noreorder
.set	nomacro
blez	$2,$L445
mflo	$16
.set	macro
.set	reorder

lw	$18,108($sp)
move	$20,$9
move	$23,$8
$L572:
lbu	$2,0($23)
sll	$4,$17,7
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$17,$17,1
sll	$2,$2,1
addu	$2,$20,$2
lhu	$6,0($2)
addu	$4,$6,$4
subu	$6,$16,$6
sll	$4,$4,2
sll	$6,$6,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$18,$4
.set	macro
.set	reorder

lw	$2,40($23)
slt	$2,$17,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L572
lw	$28,24($sp)
.set	macro
.set	reorder

$L445:
lw	$3,220($sp)
lw	$2,16($3)
blez	$2,$L444
lw	$9,108($sp)
move	$23,$22
lbu	$5,0($3)
sw	$0,84($sp)
sw	$0,96($sp)
sw	$9,80($sp)
sw	$21,116($sp)
sw	$19,88($sp)
$L534:
lw	$11,220($sp)
lw	$12,96($sp)
addu	$3,$11,$12
lbu	$3,20($3)
.set	noreorder
.set	nomacro
beq	$5,$0,$L448
sw	$3,100($sp)
.set	macro
.set	reorder

move	$18,$3
lw	$3,84($sp)
lw	$7,116($sp)
lw	$8,92($sp)
sll	$2,$3,2
lw	$22,104($sp)
sw	$0,72($sp)
addu	$7,$7,$2
addu	$fp,$8,$2
sw	$7,76($sp)
$L533:
lw	$9,76($sp)
lhu	$2,0($22)
lhu	$6,2($22)
lw	$11,80($sp)
lw	$3,0($9)
sll	$16,$2,2
subu	$6,$6,$2
addiu	$3,$3,-1
sltu	$4,$3,13
.set	noreorder
.set	nomacro
bne	$4,$0,$L449
addu	$16,$11,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$18,$0,$L680
lw	$9,72($sp)
.set	macro
.set	reorder

sll	$17,$6,2
move	$19,$0
$L451:
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$19,$19,1
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$6,$17
.set	macro
.set	reorder

addiu	$16,$16,512
.set	noreorder
.set	nomacro
bne	$19,$18,$L451
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$12,220($sp)
lbu	$5,0($12)
$L450:
lw	$9,72($sp)
$L680:
addiu	$22,$22,2
lw	$11,76($sp)
addiu	$fp,$fp,4
lw	$12,84($sp)
addiu	$9,$9,1
addiu	$11,$11,4
slt	$2,$9,$5
sw	$9,72($sp)
addu	$3,$9,$12
.set	noreorder
.set	nomacro
bne	$2,$0,$L533
sw	$11,76($sp)
.set	macro
.set	reorder

lw	$8,220($sp)
sw	$3,84($sp)
lw	$2,16($8)
$L448:
lw	$11,100($sp)
lw	$9,96($sp)
lw	$12,80($sp)
sll	$3,$11,9
addiu	$9,$9,1
addu	$12,$12,$3
slt	$4,$9,$2
sw	$9,96($sp)
.set	noreorder
.set	nomacro
bne	$4,$0,$L534
sw	$12,80($sp)
.set	macro
.set	reorder

lw	$21,116($sp)
$L444:
lw	$7,124($sp)
beq	$7,$0,$L447
lw	$3,120($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L447
mtc1	$0,$f22
.set	macro
.set	reorder

lw	$7,104($sp)
addiu	$18,$sp,32
sll	$20,$3,2
move	$17,$0
addu	$20,$18,$20
lhu	$16,2($7)
li	$22,13			# 0xd
$L542:
lw	$5,8($18)
lw	$8,108($sp)
sll	$19,$5,2
slt	$2,$5,$16
addu	$19,$8,$19
.set	noreorder
.set	nomacro
bne	$2,$0,$L535
lw	$6,0($19)
.set	macro
.set	reorder

lw	$9,104($sp)
sll	$2,$17,1
addu	$2,$9,$2
$L536:
lhu	$3,4($2)
addiu	$17,$17,1
slt	$4,$5,$3
.set	noreorder
.set	nomacro
beq	$4,$0,$L536
addiu	$2,$2,2
.set	macro
.set	reorder

move	$16,$3
$L535:
sll	$2,$17,2
addu	$3,$21,$2
lw	$3,0($3)
beq	$3,$22,$L537
lw	$11,92($sp)
addu	$2,$11,$2
lw	$2,0($2)
beq	$2,$0,$L537
mtc1	$6,$f1
lw	$3,24($18)
subu	$3,$0,$3
cvt.s.w	$f0,$f1
mtc1	$3,$f1
c.eq.s	$fcc0,$f0,$f22
.set	noreorder
.set	nomacro
bc1t	$fcc0,$L639
cvt.s.w	$f20,$f1
.set	macro
.set	reorder

mtc1	$2,$f1
cvt.s.w	$f21,$f1
div.s	$f0,$f0,$f21
abs.s	$f1,$f0
c.lt.s	$fcc1,$f22,$f0
sqrt.s	$f1,$f1
sqrt.s	$f1,$f1
.set	noreorder
.set	nomacro
bc1f	$fcc1,$L540
div.s	$f0,$f0,$f1
.set	macro
.set	reorder

neg.s	$f20,$f20
$L540:
add.s	$f20,$f20,$f0
$L539:
lw	$25,%call16(cbrtf)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,cbrtf
1:	jalr	$25
abs.s	$f12,$f20
.set	macro
.set	reorder

mul.s	$f1,$f21,$f20
lw	$28,24($sp)
mul.s	$f0,$f1,$f0
trunc.w.s $f0,$f0
swc1	$f0,0($19)
$L537:
addiu	$18,$18,4
bne	$18,$20,$L542
$L447:
.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L425:
lui	$6,%hi($LC26)
lw	$25,%call16(av_log)($28)
lw	$4,0($22)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC26)
.set	macro
.set	reorder

$L624:
li	$2,-1			# 0xffffffffffffffff
$L608:
lw	$31,196($sp)
lw	$fp,192($sp)
lw	$23,188($sp)
lw	$22,184($sp)
lw	$21,180($sp)
lw	$20,176($sp)
lw	$19,172($sp)
lw	$18,168($sp)
lw	$17,164($sp)
lw	$16,160($sp)
ldc1	$f22,208($sp)
ldc1	$f20,200($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L632:
lw	$2,16($9)
.set	noreorder
.set	nomacro
blez	$2,$L640
li	$12,3			# 0x3
.set	macro
.set	reorder

li	$11,29			# 0x1d
.option	pic0
.set	noreorder
.set	nomacro
j	$L383
.option	pic2
li	$8,7			# 0x7
.set	macro
.set	reorder

$L635:
lw	$25,%call16(av_log)($28)
lw	$4,0($22)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC16)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L636:
lui	$6,%hi(overread_err)
lw	$25,%call16(av_log)($28)
lw	$4,0($22)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo(overread_err)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L390:
lui	$6,%hi($LC17)
lw	$4,0($22)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC17)
sw	$3,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$7,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L400:
addiu	$4,$3,-14
sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L641
li	$4,13			# 0xd
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$4,$L408
slt	$3,$22,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L423
lw	$3,92($sp)
.set	macro
.set	reorder

li	$12,-16777216			# 0xffffffffff000000
move	$6,$22
ori	$12,$12,0xff00
.option	pic0
.set	noreorder
.set	nomacro
j	$L421
.option	pic2
addu	$5,$3,$5
.set	macro
.set	reorder

$L418:
addiu	$2,$2,-60
addu	$4,$3,$4
addu	$20,$20,$2
sltu	$2,$20,256
.set	noreorder
.set	nomacro
beq	$2,$0,$L642
sw	$4,8($19)
.set	macro
.set	reorder

$L420:
addu	$2,$14,$20
addiu	$6,$6,1
sll	$2,$2,2
addu	$3,$17,$6
addu	$2,$2,$8
addiu	$5,$5,4
subu	$3,$3,$22
lw	$2,0($2)
subu	$2,$0,$2
.set	noreorder
.set	nomacro
beq	$6,$16,$L619
sw	$2,-4($5)
.set	macro
.set	reorder

$L421:
lw	$4,8($19)
srl	$2,$4,3
andi	$15,$4,0x7
addu	$2,$21,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$13,$2,8
srl	$3,$3,8
and	$13,$13,$12
and	$3,$3,$23
or	$13,$3,$13
sll	$3,$13,16
srl	$13,$13,16
or	$2,$3,$13
sll	$2,$2,$15
srl	$2,$2,25
sll	$2,$2,2
addu	$2,$fp,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L418
lh	$2,0($2)
.set	macro
.set	reorder

addiu	$4,$4,7
srl	$13,$4,3
andi	$25,$4,0x7
addu	$13,$21,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($13)  
lwr $15, 0($13)  

# 0 "" 2
#NO_APP
srl	$13,$15,8
sll	$24,$15,8
and	$13,$13,$23
and	$24,$24,$12
or	$24,$13,$24
sll	$15,$24,16
srl	$13,$24,16
or	$13,$15,$13
sll	$13,$13,$25
srl	$13,$13,$3
addu	$2,$13,$2
sll	$2,$2,2
addu	$2,$fp,$2
lh	$13,2($2)
.set	noreorder
.set	nomacro
bltz	$13,$L419
lh	$2,0($2)
.set	macro
.set	reorder

addiu	$2,$2,-60
move	$3,$13
addu	$4,$3,$4
addu	$20,$20,$2
sltu	$2,$20,256
.set	noreorder
.set	nomacro
bne	$2,$0,$L420
sw	$4,8($19)
.set	macro
.set	reorder

$L642:
lw	$22,76($sp)
lui	$6,%hi($LC18)
lui	$7,%hi($LC21)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC18)
lw	$4,0($22)
addiu	$7,$7,%lo($LC21)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$20,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L419:
subu	$4,$4,$3
srl	$3,$4,3
andi	$25,$4,0x7
addu	$3,$21,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($3)  
lwr $15, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$15,8
sll	$24,$15,8
and	$3,$3,$23
and	$24,$24,$12
or	$24,$3,$24
sll	$15,$24,16
srl	$3,$24,16
or	$3,$15,$3
sll	$3,$3,$25
srl	$3,$3,$13
addu	$2,$3,$2
sll	$3,$2,2
addu	$3,$fp,$3
lh	$2,0($3)
.option	pic0
.set	noreorder
.set	nomacro
j	$L418
.option	pic2
lh	$3,2($3)
.set	macro
.set	reorder

$L619:
lw	$12,220($sp)
move	$22,$6
move	$17,$3
lbu	$2,0($12)
.option	pic0
.set	noreorder
.set	nomacro
j	$L423
.option	pic2
slt	$6,$6,$2
.set	macro
.set	reorder

$L641:
slt	$3,$22,$16
beq	$3,$0,$L423
lw	$12,92($sp)
li	$13,300			# 0x12c
move	$6,$22
addu	$5,$12,$5
li	$12,-16777216			# 0xffffffffff000000
.option	pic0
.set	noreorder
.set	nomacro
j	$L407
.option	pic2
ori	$12,$12,0xff00
.set	macro
.set	reorder

$L404:
addiu	$2,$2,-60
addu	$3,$4,$3
addu	$7,$7,$2
sltu	$2,$7,256
.set	noreorder
.set	nomacro
beq	$2,$0,$L643
sw	$3,8($19)
.set	macro
.set	reorder

$L406:
subu	$2,$13,$7
addiu	$6,$6,1
sll	$2,$2,2
addu	$3,$17,$6
addu	$2,$2,$8
addiu	$5,$5,4
subu	$3,$3,$22
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$6,$16,$L619
sw	$2,-4($5)
.set	macro
.set	reorder

$L407:
lw	$3,8($19)
srl	$2,$3,3
andi	$24,$3,0x7
addu	$2,$21,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$15,$4,8
and	$2,$2,$23
and	$15,$15,$12
or	$15,$2,$15
sll	$4,$15,16
srl	$15,$15,16
or	$2,$4,$15
sll	$2,$2,$24
srl	$2,$2,25
sll	$2,$2,2
addu	$2,$fp,$2
lh	$4,2($2)
.set	noreorder
.set	nomacro
bgez	$4,$L404
lh	$2,0($2)
.set	macro
.set	reorder

addiu	$3,$3,7
srl	$15,$3,3
andi	$25,$3,0x7
addu	$15,$21,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $24, 3($15)  
lwr $24, 0($15)  

# 0 "" 2
#NO_APP
srl	$15,$24,8
sll	$24,$24,8
and	$15,$15,$23
and	$24,$24,$12
or	$15,$15,$24
sll	$24,$15,16
srl	$15,$15,16
or	$15,$24,$15
sll	$15,$15,$25
srl	$15,$15,$4
addu	$2,$15,$2
sll	$2,$2,2
addu	$2,$fp,$2
lh	$15,2($2)
.set	noreorder
.set	nomacro
bltz	$15,$L405
lh	$2,0($2)
.set	macro
.set	reorder

addiu	$2,$2,-60
move	$4,$15
addu	$3,$4,$3
addu	$7,$7,$2
sltu	$2,$7,256
.set	noreorder
.set	nomacro
bne	$2,$0,$L406
sw	$3,8($19)
.set	macro
.set	reorder

$L643:
lw	$22,76($sp)
lui	$6,%hi($LC18)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC18)
lw	$4,0($22)
sw	$7,16($sp)
lui	$7,%hi($LC19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC19)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L405:
subu	$3,$3,$4
srl	$4,$3,3
andi	$25,$3,0x7
addu	$4,$21,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $24, 3($4)  
lwr $24, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$24,8
sll	$24,$24,8
and	$4,$4,$23
and	$24,$24,$12
or	$4,$4,$24
sll	$24,$4,16
srl	$4,$4,16
or	$4,$24,$4
sll	$4,$4,$25
srl	$4,$4,$15
addu	$2,$4,$2
sll	$4,$2,2
addu	$4,$fp,$4
lh	$2,0($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L404
.option	pic2
lh	$4,2($4)
.set	macro
.set	reorder

$L408:
beq	$3,$0,$L423
lw	$12,92($sp)
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
addu	$3,$12,$5
.option	pic0
.set	noreorder
.set	nomacro
j	$L416
.option	pic2
move	$5,$22
.set	macro
.set	reorder

$L646:
lw	$4,8($19)
addiu	$18,$18,-256
srl	$2,$4,3
andi	$12,$4,0x7
addu	$2,$21,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 3($2)  
lwr $13, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$13,8
sll	$13,$13,8
and	$2,$2,$23
and	$13,$13,$6
or	$2,$2,$13
sll	$13,$2,16
srl	$2,$2,16
addiu	$4,$4,9
or	$2,$13,$2
sll	$2,$2,$12
srl	$2,$2,23
addu	$18,$18,$2
sltu	$2,$18,256
.set	noreorder
.set	nomacro
beq	$2,$0,$L644
sw	$4,8($19)
.set	macro
.set	reorder

$L415:
addu	$2,$14,$18
addiu	$5,$5,1
addiu	$2,$2,100
addu	$4,$17,$5
sll	$2,$2,2
addiu	$3,$3,4
addu	$2,$2,$8
subu	$4,$4,$22
lw	$2,0($2)
subu	$2,$0,$2
.set	noreorder
.set	nomacro
beq	$5,$16,$L645
sw	$2,-4($3)
.set	macro
.set	reorder

$L416:
addiu	$9,$9,-1
beq	$9,$0,$L646
lw	$12,8($19)
srl	$2,$12,3
andi	$15,$12,0x7
addu	$2,$21,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$4
sll	$13,$2,8
srl	$4,$4,8
and	$13,$13,$6
and	$4,$4,$23
or	$13,$4,$13
sll	$4,$13,16
srl	$13,$13,16
or	$2,$4,$13
sll	$2,$2,$15
srl	$2,$2,25
sll	$2,$2,2
addu	$2,$fp,$2
lh	$4,2($2)
.set	noreorder
.set	nomacro
bltz	$4,$L647
lh	$2,0($2)
.set	macro
.set	reorder

$L413:
addiu	$2,$2,-60
addu	$4,$4,$12
addu	$18,$18,$2
sltu	$2,$18,256
.set	noreorder
.set	nomacro
bne	$2,$0,$L415
sw	$4,8($19)
.set	macro
.set	reorder

$L644:
lw	$22,76($sp)
lui	$6,%hi($LC18)
lui	$7,%hi($LC20)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC18)
lw	$4,0($22)
addiu	$7,$7,%lo($LC20)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$18,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L647:
addiu	$12,$12,7
srl	$13,$12,3
andi	$25,$12,0x7
addu	$13,$21,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($13)  
lwr $15, 0($13)  

# 0 "" 2
#NO_APP
srl	$13,$15,8
sll	$24,$15,8
and	$13,$13,$23
and	$24,$24,$6
or	$24,$13,$24
sll	$15,$24,16
srl	$13,$24,16
or	$13,$15,$13
sll	$13,$13,$25
srl	$13,$13,$4
addu	$2,$13,$2
sll	$2,$2,2
addu	$2,$fp,$2
lh	$13,2($2)
.set	noreorder
.set	nomacro
bltz	$13,$L414
lh	$2,0($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L413
.option	pic2
move	$4,$13
.set	macro
.set	reorder

$L645:
lw	$12,220($sp)
move	$22,$5
move	$17,$4
lbu	$2,0($12)
.option	pic0
.set	noreorder
.set	nomacro
j	$L423
.option	pic2
slt	$6,$5,$2
.set	macro
.set	reorder

$L414:
subu	$12,$12,$4
srl	$4,$12,3
andi	$25,$12,0x7
addu	$4,$21,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($4)  
lwr $15, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$15,8
sll	$24,$15,8
and	$4,$4,$23
and	$24,$24,$6
or	$24,$4,$24
sll	$15,$24,16
srl	$4,$24,16
or	$4,$15,$4
sll	$4,$4,$25
srl	$4,$4,$13
addu	$2,$4,$2
sll	$4,$2,2
addu	$4,$fp,$4
lh	$2,0($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L413
.option	pic2
lh	$4,2($4)
.set	macro
.set	reorder

$L428:
lw	$9,72($sp)
xori	$2,$9,0x2
sltu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$0,$L430
li	$fp,7			# 0x7
.set	macro
.set	reorder

lw	$3,4($22)
li	$4,12			# 0xc
li	$5,20			# 0x14
move	$fp,$4
xori	$3,$3,0x1
movz	$fp,$5,$3
$L430:
lw	$11,220($sp)
lw	$3,40($11)
.set	noreorder
.set	nomacro
blez	$3,$L431
li	$3,2			# 0x2
.set	macro
.set	reorder

lw	$5,8($19)
li	$20,3			# 0x3
sw	$22,88($sp)
subu	$3,$3,$2
sw	$21,96($sp)
subu	$20,$20,$2
subu	$24,$0,$2
addiu	$12,$2,30
sw	$3,84($sp)
li	$2,32			# 0x20
addiu	$3,$11,524
sll	$20,$20,1
sw	$12,80($sp)
sll	$24,$24,1
subu	$25,$2,$20
sw	$3,76($sp)
addiu	$2,$11,396
li	$23,27			# 0x1b
addiu	$18,$11,108
li	$11,16711680			# 0xff0000
sw	$2,72($sp)
move	$3,$0
li	$10,-16777216			# 0xffffffffff000000
subu	$23,$23,$24
addiu	$11,$11,255
addiu	$24,$24,5
ori	$10,$10,0xff00
move	$22,$3
$L440:
srl	$2,$5,3
lw	$7,84($sp)
andi	$6,$5,0x7
lw	$8,80($sp)
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$11
and	$4,$4,$10
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
addu	$5,$5,$7
or	$2,$3,$4
sll	$2,$2,$6
sw	$5,8($19)
srl	$2,$2,$8
.set	noreorder
.set	nomacro
bne	$2,$0,$L432
sw	$2,0($18)
.set	macro
.set	reorder

lw	$5,8($19)
$L433:
lw	$3,220($sp)
addiu	$22,$22,1
lw	$4,72($sp)
addiu	$18,$18,4
lw	$6,76($sp)
lw	$2,40($3)
addiu	$4,$4,16
addiu	$6,$6,320
slt	$2,$22,$2
sw	$4,72($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L440
sw	$6,76($sp)
.set	macro
.set	reorder

lw	$21,96($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L429
.option	pic2
lw	$22,88($sp)
.set	macro
.set	reorder

$L620:
.option	pic0
.set	noreorder
.set	nomacro
j	$L426
.option	pic2
sw	$0,120($sp)
.set	macro
.set	reorder

$L449:
li	$2,12			# 0xc
.set	noreorder
.set	nomacro
beq	$3,$2,$L648
lw	$7,88($sp)
.set	macro
.set	reorder

lui	$8,%hi(aac_codebook_vector_vals)
lw	$9,%got(ff_aac_codebook_vector_idx)($28)
lui	$11,%hi(vlc_spectral)
sll	$4,$3,2
addiu	$8,$8,%lo(aac_codebook_vector_vals)
lw	$2,8($7)
addiu	$11,$11,%lo(vlc_spectral)
sll	$7,$3,4
addu	$5,$4,$8
addu	$4,$9,$4
addu	$7,$7,$11
srl	$3,$3,1
lw	$5,0($5)
lw	$12,0($4)
sltu	$4,$3,5
.set	noreorder
.set	nomacro
beq	$4,$0,$L460
lw	$8,4($7)
.set	macro
.set	reorder

lui	$4,%hi($L462)
sll	$3,$3,2
addiu	$4,$4,%lo($L462)
addu	$3,$4,$3
lw	$3,0($3)
j	$3
.rdata
.align	2
.align	2
$L462:
.word	$L461
.word	$L463
.word	$L464
.word	$L465
.word	$L465
.text
$L464:
.set	noreorder
.set	nomacro
beq	$18,$0,$L467
lw	$3,88($sp)
.set	macro
.set	reorder

li	$13,16711680			# 0xff0000
li	$9,-16777216			# 0xffffffffff000000
move	$19,$0
addiu	$13,$13,255
lw	$17,0($3)
ori	$9,$9,0xff00
move	$20,$5
$L495:
move	$15,$6
.option	pic0
.set	noreorder
.set	nomacro
j	$L494
.option	pic2
move	$14,$16
.set	macro
.set	reorder

$L493:
sll	$3,$3,1
lw	$4,0($fp)
addiu	$14,$14,8
addu	$3,$12,$3
addiu	$15,$15,-2
addu	$2,$2,$7
lhu	$3,0($3)
andi	$5,$3,0xf
sll	$5,$5,2
srl	$3,$3,2
addu	$5,$20,$5
andi	$3,$3,0x3c
addu	$3,$20,$3
lw	$10,0($5)
mult	$10,$4
mflo	$10
mfhi	$11
srl	$10,$10,14
sll	$5,$11,18
or	$10,$5,$10
sw	$10,-8($14)
lw	$3,0($3)
mult	$4,$3
mflo	$4
mfhi	$5
srl	$4,$4,14
sll	$3,$5,18
or	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$15,$0,$L649
sw	$4,-4($14)
.set	macro
.set	reorder

$L494:
srl	$4,$2,3
andi	$3,$2,0x7
addu	$4,$17,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
move	$4,$5
sll	$4,$4,8
srl	$5,$5,8
and	$4,$4,$9
and	$5,$5,$13
or	$4,$5,$4
sll	$5,$4,16
srl	$4,$4,16
or	$4,$5,$4
sll	$3,$4,$3
srl	$3,$3,24
sll	$3,$3,2
addu	$3,$8,$3
lh	$7,2($3)
.set	noreorder
.set	nomacro
bgez	$7,$L493
lh	$3,0($3)
.set	macro
.set	reorder

addiu	$2,$2,8
srl	$5,$2,3
andi	$4,$2,0x7
addu	$5,$17,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($5)  
lwr $10, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$10,8
sll	$11,$10,8
and	$10,$5,$13
and	$5,$11,$9
or	$5,$10,$5
sll	$10,$5,16
srl	$5,$5,16
or	$5,$10,$5
sll	$4,$5,$4
srl	$4,$4,$7
addu	$3,$4,$3
sll	$4,$3,2
addu	$4,$8,$4
lh	$3,0($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L493
.option	pic2
lh	$7,2($4)
.set	macro
.set	reorder

$L465:
.set	noreorder
.set	nomacro
beq	$18,$0,$L467
lw	$3,88($sp)
.set	macro
.set	reorder

addiu	$19,$6,-2
li	$9,16711680			# 0xff0000
srl	$19,$19,1
li	$4,-16777216			# 0xffffffffff000000
sll	$19,$19,3
lw	$10,0($3)
move	$14,$0
addiu	$19,$19,12
addiu	$9,$9,255
li	$13,32			# 0x20
ori	$4,$4,0xff00
$L505:
addiu	$3,$16,4
.option	pic0
.set	noreorder
.set	nomacro
j	$L504
.option	pic2
addu	$11,$16,$19
.set	macro
.set	reorder

$L654:
andi	$7,$20,0xf
lw	$6,0($fp)
sll	$7,$7,2
addu	$7,$5,$7
lw	$7,0($7)
mult	$7,$6
$L623:
mflo	$6
mfhi	$7
srl	$6,$6,14
sll	$7,$7,18
or	$6,$7,$6
subu	$6,$0,$6
sw	$6,-4($3)
$L499:
sll	$15,$15,31
.set	noreorder
.set	nomacro
bne	$15,$0,$L650
srl	$7,$20,2
.set	macro
.set	reorder

lw	$6,0($fp)
andi	$7,$7,0x3c
addu	$7,$5,$7
lw	$7,0($7)
.set	noreorder
.set	nomacro
blez	$6,$L651
mult	$7,$6
.set	macro
.set	reorder

$L503:
mflo	$6
mfhi	$7
addiu	$3,$3,8
srl	$6,$6,14
sll	$7,$7,18
or	$6,$7,$6
subu	$6,$0,$6
.set	noreorder
.set	nomacro
beq	$3,$11,$L652
sw	$6,-8($3)
.set	macro
.set	reorder

$L504:
srl	$6,$2,3
andi	$17,$2,0x7
addu	$6,$10,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($6)  
lwr $15, 0($6)  

# 0 "" 2
#NO_APP
srl	$7,$15,8
sll	$6,$15,8
and	$7,$7,$9
and	$6,$6,$4
or	$6,$7,$6
sll	$7,$6,16
srl	$6,$6,16
or	$6,$7,$6
sll	$6,$6,$17
srl	$17,$6,24
sll	$17,$17,2
addu	$17,$8,$17
lh	$20,2($17)
.set	noreorder
.set	nomacro
bltz	$20,$L653
lh	$7,0($17)
.set	macro
.set	reorder

$L497:
sll	$7,$7,1
sll	$6,$6,$20
addu	$7,$12,$7
addu	$2,$20,$2
lhu	$20,0($7)
srl	$7,$20,8
srl	$17,$20,12
andi	$7,$7,0xf
subu	$15,$13,$7
srl	$6,$6,$15
sll	$15,$6,$17
srl	$6,$15,1
sll	$6,$6,31
.set	noreorder
.set	nomacro
bne	$6,$0,$L654
addu	$2,$2,$7
.set	macro
.set	reorder

lw	$17,0($fp)
.set	noreorder
.set	nomacro
blez	$17,$L655
andi	$7,$20,0xf
.set	macro
.set	reorder

sll	$7,$7,2
addu	$7,$5,$7
lw	$6,0($7)
.option	pic0
.set	noreorder
.set	nomacro
j	$L623
.option	pic2
mult	$6,$17
.set	macro
.set	reorder

$L463:
.set	noreorder
.set	nomacro
beq	$18,$0,$L467
lw	$3,88($sp)
.set	macro
.set	reorder

addiu	$25,$6,-4
li	$14,16711680			# 0xff0000
srl	$25,$25,2
li	$15,-16777216			# 0xffffffffff000000
sll	$25,$25,4
lw	$13,0($3)
move	$21,$0
addiu	$25,$25,28
addiu	$14,$14,255
li	$17,32			# 0x20
ori	$15,$15,0xff00
move	$7,$5
$L491:
addiu	$6,$16,12
addiu	$10,$16,8
addiu	$9,$16,4
addu	$11,$16,$25
move	$20,$16
.option	pic0
.set	noreorder
.set	nomacro
j	$L490
.option	pic2
move	$19,$18
.set	macro
.set	reorder

$L477:
sll	$5,$5,1
sll	$24,$3,$4
addu	$5,$12,$5
addu	$2,$4,$2
lhu	$3,0($5)
srl	$5,$3,8
srl	$18,$3,12
andi	$5,$5,0xf
subu	$4,$17,$5
srl	$16,$24,$4
sll	$16,$16,$4
.set	noreorder
.set	nomacro
bltz	$16,$L656
addu	$2,$2,$5
.set	macro
.set	reorder

$L478:
lw	$24,0($fp)
.set	noreorder
.set	nomacro
blez	$24,$L657
andi	$5,$3,0x3
.set	macro
.set	reorder

sll	$5,$5,2
addu	$5,$7,$5
lw	$4,0($5)
mult	$4,$24
$L621:
mflo	$4
mfhi	$5
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
subu	$4,$0,$4
sw	$4,-12($6)
$L479:
andi	$4,$18,0x1
sll	$16,$16,$4
.set	noreorder
.set	nomacro
bltz	$16,$L658
srl	$18,$18,1
.set	macro
.set	reorder

andi	$4,$3,0xc
lw	$5,0($fp)
addu	$4,$7,$4
lw	$4,0($4)
mult	$4,$5
.set	noreorder
.set	nomacro
blez	$5,$L659
mflo	$4
.set	macro
.set	reorder

$L483:
mfhi	$5
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
subu	$4,$0,$4
sw	$4,0($9)
$L482:
andi	$4,$18,0x1
sll	$16,$16,$4
.set	noreorder
.set	nomacro
bltz	$16,$L660
srl	$18,$18,1
.set	macro
.set	reorder

lw	$24,0($fp)
.set	noreorder
.set	nomacro
blez	$24,$L661
srl	$5,$3,2
.set	macro
.set	reorder

andi	$5,$5,0xc
addu	$5,$7,$5
lw	$4,0($5)
mult	$4,$24
$L622:
mflo	$4
mfhi	$5
andi	$18,$18,0x1
sll	$16,$16,$18
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
subu	$4,$0,$4
.set	noreorder
.set	nomacro
bltz	$16,$L662
sw	$4,0($10)
.set	macro
.set	reorder

$L487:
srl	$3,$3,4
lw	$5,0($fp)
andi	$3,$3,0xc
addu	$3,$7,$3
lw	$4,0($3)
mult	$4,$5
.set	noreorder
.set	nomacro
blez	$5,$L663
mflo	$4
.set	macro
.set	reorder

$L489:
mfhi	$5
srl	$4,$4,14
addiu	$6,$6,16
addiu	$10,$10,16
addiu	$9,$9,16
sll	$3,$5,18
or	$4,$3,$4
subu	$4,$0,$4
.set	noreorder
.set	nomacro
beq	$6,$11,$L664
sw	$4,-16($6)
.set	macro
.set	reorder

$L490:
srl	$3,$2,3
andi	$16,$2,0x7
addu	$3,$13,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$3,$5,8
and	$4,$4,$14
and	$3,$3,$15
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$3,$3,$16
srl	$5,$3,24
sll	$5,$5,2
addu	$5,$8,$5
lh	$4,2($5)
.set	noreorder
.set	nomacro
bgez	$4,$L477
lh	$5,0($5)
.set	macro
.set	reorder

addiu	$2,$2,8
srl	$3,$2,3
andi	$16,$2,0x7
addu	$3,$13,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 3($3)  
lwr $18, 0($3)  

# 0 "" 2
#NO_APP
srl	$24,$18,8
sll	$18,$18,8
and	$24,$24,$14
and	$18,$18,$15
or	$18,$24,$18
sll	$3,$18,16
srl	$18,$18,16
or	$3,$3,$18
sll	$3,$3,$16
srl	$4,$3,$4
addu	$5,$4,$5
sll	$4,$5,2
addu	$4,$8,$4
lh	$5,0($4)
lh	$4,2($4)
sll	$5,$5,1
sll	$24,$3,$4
addu	$5,$12,$5
addu	$2,$4,$2
lhu	$3,0($5)
srl	$5,$3,8
srl	$18,$3,12
andi	$5,$5,0xf
subu	$4,$17,$5
srl	$16,$24,$4
sll	$16,$16,$4
.set	noreorder
.set	nomacro
bgez	$16,$L478
addu	$2,$2,$5
.set	macro
.set	reorder

$L656:
andi	$5,$3,0x3
lw	$4,0($fp)
sll	$5,$5,2
addu	$5,$7,$5
lw	$5,0($5)
.option	pic0
.set	noreorder
.set	nomacro
j	$L621
.option	pic2
mult	$5,$4
.set	macro
.set	reorder

$L461:
.set	noreorder
.set	nomacro
beq	$18,$0,$L467
li	$9,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$7,88($sp)
move	$17,$0
addiu	$9,$9,255
move	$15,$5
lw	$13,0($7)
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
$L475:
move	$19,$6
.option	pic0
.set	noreorder
.set	nomacro
j	$L474
.option	pic2
move	$14,$16
.set	macro
.set	reorder

$L473:
sll	$3,$3,1
lw	$4,0($fp)
addu	$2,$2,$10
addu	$3,$12,$3
addiu	$14,$14,16
addiu	$19,$19,-4
lhu	$3,0($3)
andi	$10,$3,0x3
sll	$10,$10,2
andi	$20,$3,0xc
addu	$10,$15,$10
addu	$20,$15,$20
srl	$5,$3,2
lw	$10,0($10)
srl	$3,$3,4
andi	$5,$5,0xc
addu	$5,$15,$5
mult	$10,$4
andi	$3,$3,0xc
addu	$3,$15,$3
mflo	$10
mfhi	$11
srl	$10,$10,14
sll	$11,$11,18
or	$10,$11,$10
sw	$10,-16($14)
lw	$10,0($20)
mult	$4,$10
mflo	$10
mfhi	$11
srl	$10,$10,14
sll	$11,$11,18
or	$10,$11,$10
sw	$10,-12($14)
lw	$10,0($5)
mult	$4,$10
mflo	$10
mfhi	$11
srl	$10,$10,14
sll	$5,$11,18
or	$10,$5,$10
sw	$10,-8($14)
lw	$3,0($3)
mult	$4,$3
mflo	$4
mfhi	$5
srl	$4,$4,14
sll	$3,$5,18
or	$4,$3,$4
.set	noreorder
.set	nomacro
beq	$19,$0,$L665
sw	$4,-4($14)
.set	macro
.set	reorder

$L474:
srl	$4,$2,3
andi	$3,$2,0x7
addu	$4,$13,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($4)  
lwr $11, 0($4)  

# 0 "" 2
#NO_APP
srl	$5,$11,8
sll	$4,$11,8
and	$5,$5,$9
and	$4,$4,$7
or	$4,$5,$4
sll	$5,$4,16
srl	$4,$4,16
or	$4,$5,$4
sll	$3,$4,$3
srl	$3,$3,24
sll	$3,$3,2
addu	$3,$8,$3
lh	$10,2($3)
.set	noreorder
.set	nomacro
bgez	$10,$L473
lh	$3,0($3)
.set	macro
.set	reorder

addiu	$2,$2,8
srl	$5,$2,3
andi	$4,$2,0x7
addu	$5,$13,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($5)  
lwr $11, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$11,8
sll	$20,$11,8
and	$11,$5,$9
and	$5,$20,$7
or	$5,$11,$5
sll	$11,$5,16
srl	$5,$5,16
or	$5,$11,$5
sll	$4,$5,$4
srl	$4,$4,$10
addu	$3,$4,$3
sll	$4,$3,2
addu	$4,$8,$4
lh	$3,0($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L473
.option	pic2
lh	$10,2($4)
.set	macro
.set	reorder

$L650:
lw	$6,0($fp)
andi	$7,$7,0x3c
addu	$7,$5,$7
lw	$7,0($7)
.option	pic0
.set	noreorder
.set	nomacro
j	$L503
.option	pic2
mult	$7,$6
.set	macro
.set	reorder

$L660:
srl	$5,$3,2
lw	$4,0($fp)
andi	$5,$5,0xc
addu	$5,$7,$5
lw	$5,0($5)
.option	pic0
.set	noreorder
.set	nomacro
j	$L622
.option	pic2
mult	$5,$4
.set	macro
.set	reorder

$L658:
andi	$5,$3,0xc
lw	$4,0($fp)
addu	$5,$7,$5
lw	$5,0($5)
mult	$5,$4
.option	pic0
.set	noreorder
.set	nomacro
j	$L483
.option	pic2
mflo	$4
.set	macro
.set	reorder

$L653:
addiu	$2,$2,8
srl	$6,$2,3
andi	$15,$2,0x7
addu	$6,$10,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($6)  
lwr $21, 0($6)  

# 0 "" 2
#NO_APP
srl	$24,$21,8
sll	$21,$21,8
and	$24,$24,$9
and	$21,$21,$4
or	$21,$24,$21
sll	$6,$21,16
srl	$21,$21,16
or	$6,$6,$21
sll	$6,$6,$15
srl	$15,$6,$20
addu	$7,$15,$7
sll	$15,$7,2
addu	$15,$8,$15
lh	$7,0($15)
.option	pic0
.set	noreorder
.set	nomacro
j	$L497
.option	pic2
lh	$20,2($15)
.set	macro
.set	reorder

$L659:
mfhi	$5
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
.option	pic0
.set	noreorder
.set	nomacro
j	$L482
.option	pic2
sw	$4,0($9)
.set	macro
.set	reorder

$L657:
andi	$4,$3,0x3
sll	$4,$4,2
addu	$4,$7,$4
lw	$4,0($4)
mult	$4,$24
mflo	$4
mfhi	$5
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
.option	pic0
.set	noreorder
.set	nomacro
j	$L479
.option	pic2
sw	$4,-12($6)
.set	macro
.set	reorder

$L661:
srl	$4,$3,2
andi	$18,$18,0x1
andi	$4,$4,0xc
addu	$4,$7,$4
sll	$16,$16,$18
lw	$4,0($4)
mult	$4,$24
mflo	$4
mfhi	$5
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
.set	noreorder
.set	nomacro
bgez	$16,$L487
sw	$4,0($10)
.set	macro
.set	reorder

$L662:
srl	$3,$3,4
lw	$4,0($fp)
andi	$3,$3,0xc
addu	$3,$7,$3
lw	$3,0($3)
mult	$3,$4
.option	pic0
.set	noreorder
.set	nomacro
j	$L489
.option	pic2
mflo	$4
.set	macro
.set	reorder

$L663:
mfhi	$5
srl	$4,$4,14
addiu	$6,$6,16
addiu	$10,$10,16
addiu	$9,$9,16
sll	$3,$5,18
or	$4,$3,$4
.set	noreorder
.set	nomacro
bne	$6,$11,$L490
sw	$4,-16($6)
.set	macro
.set	reorder

$L664:
addiu	$21,$21,1
move	$18,$19
.set	noreorder
.set	nomacro
bne	$21,$19,$L491
addiu	$16,$20,512
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L678
.option	pic2
lw	$3,88($sp)
.set	macro
.set	reorder

$L655:
andi	$6,$20,0xf
sll	$6,$6,2
addu	$6,$5,$6
lw	$6,0($6)
mult	$6,$17
mflo	$6
mfhi	$7
srl	$6,$6,14
sll	$7,$7,18
or	$6,$7,$6
.option	pic0
.set	noreorder
.set	nomacro
j	$L499
.option	pic2
sw	$6,-4($3)
.set	macro
.set	reorder

$L651:
mflo	$6
mfhi	$7
addiu	$3,$3,8
srl	$6,$6,14
sll	$7,$7,18
or	$6,$7,$6
.set	noreorder
.set	nomacro
bne	$3,$11,$L504
sw	$6,-8($3)
.set	macro
.set	reorder

$L652:
addiu	$14,$14,1
.set	noreorder
.set	nomacro
bne	$14,$18,$L505
addiu	$16,$16,512
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L678
.option	pic2
lw	$3,88($sp)
.set	macro
.set	reorder

$L665:
addiu	$17,$17,1
.set	noreorder
.set	nomacro
bne	$17,$18,$L475
addiu	$16,$16,512
.set	macro
.set	reorder

$L467:
lw	$3,88($sp)
$L678:
lw	$8,220($sp)
sw	$2,8($3)
.option	pic0
.set	noreorder
.set	nomacro
j	$L450
.option	pic2
lbu	$5,0($8)
.set	macro
.set	reorder

$L649:
addiu	$19,$19,1
.set	noreorder
.set	nomacro
bne	$19,$18,$L495
addiu	$16,$16,512
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L678
.option	pic2
lw	$3,88($sp)
.set	macro
.set	reorder

$L648:
.set	noreorder
.set	nomacro
beq	$18,$0,$L450
sll	$9,$6,2
.set	macro
.set	reorder

li	$7,1638400			# 0x190000
li	$5,1013841920			# 0x3c6e0000
move	$8,$0
addiu	$7,$7,26125
ori	$5,$5,0xf35f
$L453:
.set	noreorder
.set	nomacro
blez	$6,$L459
addu	$10,$16,$9
.set	macro
.set	reorder

move	$4,$16
move	$3,$16
$L454:
lw	$2,9720($23)
addiu	$3,$3,4
mul	$11,$2,$7
addu	$2,$11,$5
sw	$2,9720($23)
.set	noreorder
.set	nomacro
bne	$3,$10,$L454
sw	$2,-4($3)
.set	macro
.set	reorder

mtlo	$0
$L455:
lw	$2,0($4)
addiu	$4,$4,4
.set	noreorder
.set	nomacro
bne	$4,$10,$L455
madd	$2,$2
.set	macro
.set	reorder

mflo	$12
lw	$11,0($fp)
move	$4,$0
move	$2,$16
mtc1	$12,$f1
cvt.s.w	$f0,$f1
sqrt.s	$f0,$f0
trunc.w.s $f0,$f0
mfc1	$3,$f0
teq	$3,$0,7
div	$0,$11,$3
mflo	$11
$L458:
lw	$3,0($2)
addiu	$4,$4,1
addiu	$2,$2,4
slt	$10,$4,$6
mul	$3,$3,$11
.set	noreorder
.set	nomacro
bne	$10,$0,$L458
sw	$3,-4($2)
.set	macro
.set	reorder

$L459:
addiu	$8,$8,1
.set	noreorder
.set	nomacro
bne	$8,$18,$L453
addiu	$16,$16,512
.set	macro
.set	reorder

lw	$3,220($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L450
.option	pic2
lbu	$5,0($3)
.set	macro
.set	reorder

$L460:
.set	noreorder
.set	nomacro
beq	$18,$0,$L467
lw	$3,88($sp)
.set	macro
.set	reorder

sll	$4,$6,2
li	$15,16711680			# 0xff0000
sw	$22,128($sp)
li	$14,-16777216			# 0xffffffffff000000
move	$20,$0
sw	$4,112($sp)
lw	$17,0($3)
addiu	$15,$15,255
li	$25,32			# 0x20
li	$24,1			# 0x1
ori	$14,$14,0xff00
move	$19,$5
$L530:
addiu	$11,$16,8
sw	$6,156($sp)
addiu	$5,$16,4
.option	pic0
.set	noreorder
.set	nomacro
j	$L528
.option	pic2
move	$13,$6
.set	macro
.set	reorder

$L668:
addiu	$13,$13,-2
sw	$0,-8($11)
addiu	$5,$5,8
sw	$0,-8($5)
.set	noreorder
.set	nomacro
beq	$13,$0,$L666
addiu	$11,$11,8
.set	macro
.set	reorder

$L528:
srl	$3,$2,3
andi	$9,$2,0x7
addu	$3,$17,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$7,8
sll	$3,$7,8
and	$4,$4,$15
and	$3,$3,$14
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$3,$3,$9
srl	$7,$3,24
sll	$7,$7,2
addu	$7,$8,$7
lh	$4,2($7)
.set	noreorder
.set	nomacro
bltz	$4,$L667
lh	$7,0($7)
.set	macro
.set	reorder

$L507:
.set	noreorder
.set	nomacro
beq	$7,$0,$L668
addu	$2,$2,$4
.set	macro
.set	reorder

sll	$7,$7,1
sll	$3,$3,$4
addu	$7,$12,$7
lhu	$7,0($7)
srl	$10,$7,12
srl	$21,$7,8
subu	$4,$25,$10
andi	$9,$21,0x1
srl	$3,$3,$4
addu	$2,$2,$10
.set	noreorder
.set	nomacro
bne	$9,$0,$L510
sll	$4,$3,$4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$4,$L669
andi	$9,$7,0xf
.set	macro
.set	reorder

sll	$9,$9,2
addu	$9,$19,$9
lw	$3,0($9)
$L514:
sw	$3,-8($11)
sll	$3,$4,1
lw	$9,0($9)
movn	$4,$3,$9
andi	$3,$21,0x2
.set	noreorder
.set	nomacro
beq	$3,$0,$L520
srl	$7,$7,4
.set	macro
.set	reorder

$L672:
srl	$7,$2,3
andi	$3,$2,0x7
addu	$7,$17,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($7)  
lwr $9, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$9,8
sll	$10,$9,8
and	$9,$7,$15
and	$7,$10,$14
or	$7,$9,$7
sll	$9,$7,16
srl	$7,$7,16
or	$7,$9,$7
sll	$3,$7,$3
nor	$7,$0,$3
ori	$7,$7,0x1
clz	$7,$7
slt	$9,$7,9
.set	noreorder
.set	nomacro
beq	$9,$0,$L516
addiu	$10,$7,1
.set	macro
.set	reorder

li	$6,28			# 0x1c
addiu	$9,$7,4
sll	$3,$3,$10
subu	$10,$6,$7
addiu	$2,$2,1
srl	$3,$3,$10
sll	$10,$24,$9
addu	$7,$2,$7
addu	$3,$3,$10
addu	$2,$7,$9
.set	noreorder
.set	nomacro
bltz	$4,$L670
sll	$3,$3,2
.set	macro
.set	reorder

lui	$9,%hi(aac_cbrt_tab)
addiu	$9,$9,%lo(aac_cbrt_tab)
addu	$3,$3,$9
lw	$3,0($3)
$L527:
sw	$3,0($5)
$L674:
addiu	$13,$13,-2
addiu	$11,$11,8
.set	noreorder
.set	nomacro
bne	$13,$0,$L528
addiu	$5,$5,8
.set	macro
.set	reorder

$L666:
lw	$6,156($sp)
.set	noreorder
.set	nomacro
blez	$6,$L532
lw	$7,0($fp)
.set	macro
.set	reorder

lw	$11,112($sp)
move	$3,$16
addu	$9,$16,$11
$L531:
lw	$4,0($3)
addiu	$3,$3,4
mult	$4,$7
mflo	$4
mfhi	$5
srl	$4,$4,14
sll	$5,$5,18
or	$4,$5,$4
.set	noreorder
.set	nomacro
bne	$3,$9,$L531
sw	$4,-4($3)
.set	macro
.set	reorder

$L532:
addiu	$20,$20,1
.set	noreorder
.set	nomacro
bne	$20,$18,$L530
addiu	$16,$16,512
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L467
.option	pic2
lw	$22,128($sp)
.set	macro
.set	reorder

$L667:
addiu	$2,$2,8
srl	$3,$2,3
andi	$21,$2,0x7
addu	$3,$17,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$9,$6,8
sll	$3,$6,8
and	$10,$9,$15
and	$9,$3,$14
or	$9,$10,$9
sll	$3,$9,16
srl	$9,$9,16
or	$3,$3,$9
sll	$3,$3,$21
srl	$4,$3,$4
addu	$4,$4,$7
sll	$4,$4,2
addu	$4,$8,$4
lh	$7,0($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L507
.option	pic2
lh	$4,2($4)
.set	macro
.set	reorder

$L510:
srl	$3,$2,3
andi	$10,$2,0x7
addu	$3,$17,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($3)  
lwr $9, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$9,8
sll	$22,$9,8
and	$9,$3,$15
and	$3,$22,$14
or	$3,$9,$3
sll	$9,$3,16
srl	$3,$3,16
or	$3,$9,$3
sll	$3,$3,$10
nor	$9,$0,$3
ori	$9,$9,0x1
clz	$9,$9
slt	$10,$9,9
.set	noreorder
.set	nomacro
beq	$10,$0,$L516
addiu	$10,$9,1
.set	macro
.set	reorder

li	$6,28			# 0x1c
addiu	$22,$9,4
sll	$10,$3,$10
addiu	$2,$2,1
subu	$3,$6,$9
addu	$2,$2,$9
srl	$10,$10,$3
sll	$9,$24,$22
addu	$2,$2,$22
.set	noreorder
.set	nomacro
bltz	$4,$L671
addu	$9,$10,$9
.set	macro
.set	reorder

lui	$3,%hi(aac_cbrt_tab)
sll	$9,$9,2
addiu	$3,$3,%lo(aac_cbrt_tab)
sll	$4,$4,1
addu	$9,$9,$3
lw	$3,0($9)
sw	$3,-8($11)
$L675:
andi	$3,$21,0x2
.set	noreorder
.set	nomacro
bne	$3,$0,$L672
srl	$7,$7,4
.set	macro
.set	reorder

$L520:
.set	noreorder
.set	nomacro
bltz	$4,$L673
andi	$3,$7,0xf
.set	macro
.set	reorder

andi	$7,$7,0xf
sll	$3,$7,2
addu	$3,$19,$3
lw	$3,0($3)
.option	pic0
.set	noreorder
.set	nomacro
j	$L674
.option	pic2
sw	$3,0($5)
.set	macro
.set	reorder

$L669:
sll	$9,$9,2
addu	$9,$19,$9
lw	$3,0($9)
blez	$3,$L514
.option	pic0
.set	noreorder
.set	nomacro
j	$L514
.option	pic2
subu	$3,$0,$3
.set	macro
.set	reorder

$L673:
sll	$3,$3,2
addu	$3,$19,$3
lw	$3,0($3)
blez	$3,$L527
subu	$3,$0,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L674
.option	pic2
sw	$3,0($5)
.set	macro
.set	reorder

$L671:
lui	$6,%hi(aac_cbrt_tab)
sll	$3,$9,2
addiu	$6,$6,%lo(aac_cbrt_tab)
sll	$4,$4,1
addu	$3,$3,$6
lw	$3,0($3)
subu	$3,$0,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L675
.option	pic2
sw	$3,-8($11)
.set	macro
.set	reorder

$L670:
lui	$7,%hi(aac_cbrt_tab)
addiu	$7,$7,%lo(aac_cbrt_tab)
addu	$3,$3,$7
lw	$3,0($3)
subu	$3,$0,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L674
.option	pic2
sw	$3,0($5)
.set	macro
.set	reorder

$L516:
lui	$6,%hi($LC25)
lw	$25,%call16(av_log)($28)
lw	$4,0($23)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC25)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,196($sp)
lw	$fp,192($sp)
lw	$23,188($sp)
lw	$22,184($sp)
lw	$21,180($sp)
lw	$20,176($sp)
lw	$19,172($sp)
lw	$18,168($sp)
lw	$17,164($sp)
lw	$16,160($sp)
ldc1	$f22,208($sp)
ldc1	$f20,200($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L432:
lw	$2,8($19)
srl	$3,$2,3
addiu	$5,$2,1
addu	$3,$16,$3
andi	$15,$2,0x7
lbu	$2,0($3)
sw	$5,8($19)
lw	$3,0($18)
sll	$15,$2,$15
andi	$15,$15,0x00ff
.set	noreorder
.set	nomacro
blez	$3,$L433
srl	$15,$15,7
.set	macro
.set	reorder

move	$17,$0
lw	$14,76($sp)
addiu	$21,$15,3
.option	pic0
.set	noreorder
.set	nomacro
j	$L439
.option	pic2
lw	$9,72($sp)
.set	macro
.set	reorder

$L677:
lw	$5,8($19)
$L436:
lw	$2,0($18)
$L679:
addiu	$17,$17,1
addiu	$9,$9,4
slt	$2,$17,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L433
addiu	$14,$14,80
.set	macro
.set	reorder

$L439:
srl	$2,$5,3
andi	$4,$5,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$6,$3,8
and	$3,$2,$11
and	$2,$6,$10
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addu	$5,$5,$20
or	$2,$3,$2
sll	$2,$2,$4
sw	$5,8($19)
srl	$2,$2,$25
sw	$2,-256($9)
lw	$4,8($19)
srl	$2,$4,3
andi	$7,$4,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$5,$3,8
and	$3,$2,$11
and	$2,$5,$10
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addu	$4,$24,$4
or	$2,$3,$2
sll	$7,$2,$7
sw	$4,8($19)
srl	$7,$7,$23
slt	$2,$fp,$7
.set	noreorder
.set	nomacro
bne	$2,$0,$L676
sw	$7,0($9)
.set	macro
.set	reorder

beq	$7,$0,$L677
lw	$3,8($19)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$16,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($19)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,-128($9)
lw	$5,8($19)
srl	$2,$5,3
andi	$6,$5,0x7
addu	$2,$16,$2
addiu	$5,$5,1
lbu	$2,0($2)
sw	$5,8($19)
lw	$3,0($9)
sll	$6,$2,$6
andi	$6,$6,0x00ff
srl	$6,$6,7
sll	$2,$6,1
subu	$6,$21,$6
.set	noreorder
.set	nomacro
blez	$3,$L436
addu	$2,$15,$2
.set	macro
.set	reorder

lui	$3,%hi(aac_tns_tmp2_map)
sll	$2,$2,2
addiu	$3,$3,%lo(aac_tns_tmp2_map)
li	$12,32			# 0x20
addu	$2,$2,$3
subu	$8,$12,$6
move	$4,$0
lw	$7,0($2)
move	$3,$14
$L438:
srl	$2,$5,3
andi	$13,$5,0x7
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$12
sll	$2,$2,8
srl	$12,$12,8
and	$2,$2,$10
and	$12,$12,$11
or	$2,$12,$2
sll	$12,$2,16
srl	$2,$2,16
addu	$5,$5,$6
or	$2,$12,$2
sll	$2,$2,$13
sw	$5,8($19)
addiu	$3,$3,4
srl	$2,$2,$8
addiu	$4,$4,1
sll	$2,$2,2
addu	$2,$7,$2
lw	$2,0($2)
sw	$2,-4($3)
lw	$2,0($9)
slt	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L438
lw	$5,8($19)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L679
.option	pic2
lw	$2,0($18)
.set	macro
.set	reorder

$L676:
move	$3,$22
lw	$22,88($sp)
lui	$6,%hi($LC23)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC23)
lw	$4,0($22)
sw	$fp,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,132($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$3,132($sp)
lw	$9,220($sp)
lw	$31,196($sp)
sll	$3,$3,2
lw	$fp,192($sp)
lw	$23,188($sp)
addu	$3,$3,$17
lw	$22,184($sp)
lw	$21,180($sp)
addiu	$3,$3,72
lw	$20,176($sp)
lw	$19,172($sp)
sll	$3,$3,2
lw	$18,168($sp)
lw	$17,164($sp)
addu	$3,$9,$3
lw	$16,160($sp)
ldc1	$f22,208($sp)
sw	$0,108($3)
ldc1	$f20,200($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L639:
mtc1	$2,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L539
.option	pic2
cvt.s.w	$f21,$f0
.set	macro
.set	reorder

$L550:
.option	pic0
.set	noreorder
.set	nomacro
j	$L426
.option	pic2
sw	$7,124($sp)
.set	macro
.set	reorder

$L638:
lui	$5,%hi($LC24)
lw	$25,%call16(av_log_missing_feature)($28)
lw	$4,0($22)
li	$6,1			# 0x1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log_missing_feature
1:	jalr	$25
addiu	$5,$5,%lo($LC24)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L640:
lw	$2,8($19)
lw	$3,220($sp)
srl	$4,$2,3
addiu	$3,$3,4116
addu	$4,$16,$4
andi	$5,$2,0x7
sw	$3,92($sp)
addiu	$2,$2,1
lbu	$3,0($4)
sw	$2,8($19)
sll	$3,$3,$5
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L620
sw	$3,124($sp)
.set	macro
.set	reorder

$L547:
lui	$6,%hi($LC22)
lw	$25,%call16(av_log)($28)
lw	$4,0($22)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC22)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L608
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L633:
lw	$9,220($sp)
addiu	$9,$9,4116
.option	pic0
.set	noreorder
.set	nomacro
j	$L398
.option	pic2
sw	$9,92($sp)
.set	macro
.set	reorder

.end	decode_ics.constprop.27
.size	decode_ics.constprop.27, .-decode_ics.constprop.27
.section	.rodata.str1.4
.align	2
$LC28:
.ascii	"More than one AAC RDB per ADTS frame is\000"
.align	2
$LC29:
.ascii	"Error decoding AAC frame header.\012\000"
.align	2
$LC30:
.ascii	"Duplicate channel tag found, attempting to remap.\012\000"
.align	2
$LC31:
.ascii	"channel element %d.%d is not allocated\012\000"
.align	2
$LC32:
.ascii	"ms_present = 3 is reserved.\012\000"
.align	2
$LC33:
.ascii	"Not evaluating a further program_config_element as this "
.ascii	"construct is dubious at best.\012\000"
.align	2
$LC34:
.ascii	"SBR was found before the first channel element.\012\000"
.align	2
$LC35:
.ascii	"SBR signaled to be not-present but was found in the bits"
.ascii	"tream.\012\000"
.align	2
$LC36:
.ascii	"Output buffer too small (%d) or trying to output too man"
.ascii	"y samples (%d) for this frame.\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	aac_decode_frame
.type	aac_decode_frame, @function
aac_decode_frame:
.frame	$sp,480,$31		# vars= 384, regs= 10/6, args= 24, gp= 8
.mask	0xc0ff0000,-28
.fmask	0x03f00000,-8
lw	$3,20($7)
addiu	$sp,$sp,-480
lui	$28,%hi(__gnu_local_gp)
lw	$7,16($7)
sw	$5,484($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$fp,448($sp)
sw	$3,372($sp)
sll	$3,$3,3
sw	$31,452($sp)
sra	$5,$3,3
sw	$23,444($sp)
sw	$22,440($sp)
sw	$21,436($sp)
sw	$20,432($sp)
sw	$19,428($sp)
sw	$18,424($sp)
sw	$17,420($sp)
sw	$16,416($sp)
sdc1	$f24,472($sp)
.cprestore	24
sdc1	$f22,464($sp)
sw	$4,480($sp)
sdc1	$f20,456($sp)
sw	$6,488($sp)
sw	$7,360($sp)
.set	noreorder
.set	nomacro
bltz	$5,$L889
lw	$fp,136($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$3,$L1106
li	$4,3			# 0x3
.set	macro
.set	reorder

addu	$5,$7,$5
addiu	$4,$7,3
move	$2,$7
$L682:
sw	$3,320($sp)
sw	$0,316($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 0($4)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
sw	$5,312($sp)
li	$5,16711680			# 0xff0000
srl	$3,$6,8
addiu	$5,$5,255
sll	$4,$6,8
and	$3,$3,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$4,$4,$5
or	$3,$3,$4
sll	$3,$3,16
li	$4,4095			# 0xfff
srl	$3,$3,20
.set	noreorder
.set	nomacro
beq	$3,$4,$L1049
sw	$2,308($sp)
.set	macro
.set	reorder

move	$16,$0
move	$17,$2
$L683:
lw	$25,%call16(memset)($28)
addiu	$4,$fp,1296
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,64			# 0x40
.set	macro
.set	reorder

lui	$2,%hi($LC27)
li	$3,7			# 0x7
lw	$28,24($sp)
lwc1	$f23,%lo($LC27)($2)
sw	$0,368($sp)
sw	$3,344($sp)
sw	$0,328($sp)
$L698:
li	$2,16711680			# 0xff0000
srl	$3,$16,3
li	$7,-16777216			# 0xffffffffff000000
addu	$3,$17,$3
addiu	$8,$2,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$3,$4,8
ori	$7,$7,0xff00
and	$2,$2,$8
and	$3,$3,$7
or	$2,$2,$3
sll	$5,$2,16
srl	$2,$2,16
andi	$6,$16,0x7
or	$5,$5,$2
sll	$6,$5,$6
addiu	$5,$16,3
srl	$6,$6,29
li	$2,7			# 0x7
sw	$5,316($sp)
.set	noreorder
.set	nomacro
beq	$6,$2,$L1050
sw	$6,336($sp)
.set	macro
.set	reorder

lw	$2,308($sp)
srl	$3,$5,3
andi	$5,$5,0x7
addiu	$4,$16,7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$4,316($sp)
sll	$3,$3,8
and	$2,$2,$8
and	$3,$3,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
sltu	$4,$6,4
or	$2,$3,$2
sll	$5,$2,$5
srl	$18,$5,28
.set	noreorder
.set	nomacro
bne	$4,$0,$L1051
move	$23,$18
.set	macro
.set	reorder

lw	$16,328($sp)
$L699:
lw	$3,336($sp)
sltu	$2,$3,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L779
lui	$4,%hi($L879)
.set	macro
.set	reorder

sll	$2,$3,2
addiu	$4,$4,%lo($L879)
addu	$2,$4,$2
lw	$2,0($2)
j	$2
.rdata
.align	2
.align	2
$L879:
.word	$L779
.word	$L725
.word	$L757
.word	$L779
.word	$L780
.word	$L784
.word	$L787
.text
$L889:
li	$4,3			# 0x3
$L1106:
move	$5,$0
move	$3,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L682
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L780:
lw	$5,316($sp)
li	$2,16711680			# 0xff0000
lw	$7,308($sp)
li	$8,-16777216			# 0xffffffffff000000
addiu	$9,$2,255
srl	$4,$5,3
addiu	$2,$5,1
addu	$4,$7,$4
srl	$3,$2,3
ori	$8,$8,0xff00
lbu	$6,0($4)
addu	$3,$7,$3
sw	$2,316($sp)
andi	$10,$2,0x7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$9
and	$4,$4,$8
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
andi	$11,$5,0x7
or	$2,$3,$4
sll	$2,$2,$10
sll	$6,$6,$11
addiu	$4,$5,9
srl	$2,$2,24
li	$3,255			# 0xff
andi	$6,$6,0x00ff
sw	$4,316($sp)
.set	noreorder
.set	nomacro
bne	$2,$3,$L781
srl	$6,$6,7
.set	macro
.set	reorder

srl	$2,$4,3
andi	$4,$4,0x7
addu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($7)  
lwr $3, 0($7)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$9
and	$3,$3,$8
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$5,$5,17
or	$2,$3,$2
sll	$2,$2,$4
sw	$5,316($sp)
move	$4,$5
srl	$2,$2,24
addiu	$2,$2,255
$L781:
.set	noreorder
.set	nomacro
beq	$6,$0,$L1107
lw	$3,320($sp)
.set	macro
.set	reorder

subu	$3,$0,$4
andi	$3,$3,0x7
.set	noreorder
.set	nomacro
beq	$3,$0,$L782
addu	$3,$3,$4
.set	macro
.set	reorder

move	$4,$3
sw	$3,316($sp)
$L782:
lw	$3,320($sp)
$L1107:
sll	$2,$2,3
subu	$5,$3,$4
slt	$5,$5,$2
.set	noreorder
.set	nomacro
bne	$5,$0,$L1052
addu	$4,$2,$4
.set	macro
.set	reorder

subu	$3,$3,$4
sw	$4,316($sp)
$L755:
slt	$3,$3,3
.set	noreorder
.set	nomacro
bne	$3,$0,$L789
sw	$16,328($sp)
.set	macro
.set	reorder

$L1068:
lw	$3,336($sp)
move	$16,$4
lw	$17,308($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L698
.option	pic2
sw	$3,344($sp)
.set	macro
.set	reorder

$L757:
lw	$18,316($sp)
li	$10,16711680			# 0xff0000
lw	$6,308($sp)
li	$9,-16777216			# 0xffffffffff000000
addiu	$10,$10,255
srl	$2,$18,3
addiu	$5,$18,1
addu	$2,$6,$2
srl	$4,$5,3
ori	$9,$9,0xff00
lbu	$3,0($2)
addu	$4,$6,$4
sw	$5,316($sp)
andi	$7,$18,0x7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($4)  
lwr $2, 0($4)  

# 0 "" 2
#NO_APP
move	$4,$2
sll	$4,$4,8
srl	$2,$2,8
and	$4,$4,$9
and	$2,$2,$10
or	$2,$2,$4
sll	$8,$2,16
srl	$2,$2,16
sll	$3,$3,$7
or	$2,$8,$2
andi	$8,$5,0x7
sll	$8,$2,$8
li	$4,65536			# 0x10000
srl	$2,$3,6
addiu	$18,$18,4
srl	$8,$8,29
addu	$3,$16,$4
andi	$2,$2,0x2
sw	$18,316($sp)
addiu	$4,$4,10200
sw	$8,10132($3)
addiu	$5,$16,144
sw	$2,10128($3)
addu	$7,$16,$4
move	$11,$0
move	$14,$0
li	$13,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L760
.option	pic2
li	$12,2			# 0x2
.set	macro
.set	reorder

$L758:
addiu	$11,$11,1
sw	$12,0($7)
slt	$2,$8,$11
.set	noreorder
.set	nomacro
bne	$2,$0,$L1053
addiu	$7,$7,4
.set	macro
.set	reorder

$L899:
move	$14,$15
$L760:
srl	$2,$18,3
addiu	$3,$18,1
addu	$2,$6,$2
srl	$4,$3,3
andi	$15,$3,0x7
lbu	$2,0($2)
addu	$4,$6,$4
sw	$3,316($sp)
andi	$17,$18,0x7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
move	$4,$3
sll	$4,$4,8
srl	$3,$3,8
and	$4,$4,$9
and	$3,$3,$10
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
sll	$2,$2,$17
or	$3,$4,$3
sll	$3,$3,$15
andi	$2,$2,0x00ff
move	$17,$18
srl	$2,$2,7
addiu	$18,$18,5
srl	$3,$3,28
sw	$2,-64($7)
addiu	$15,$14,1
sw	$18,316($sp)
.set	noreorder
.set	nomacro
bne	$2,$13,$L758
sw	$3,-32($7)
.set	macro
.set	reorder

srl	$3,$18,3
andi	$19,$18,0x7
addu	$3,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$10
and	$4,$4,$9
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
addiu	$18,$17,7
or	$2,$3,$4
sll	$2,$2,$19
addiu	$11,$11,1
sw	$18,316($sp)
srl	$2,$2,30
addiu	$14,$14,2
xori	$3,$2,0x3
sw	$2,0($7)
slt	$2,$8,$11
movz	$15,$14,$3
.set	noreorder
.set	nomacro
beq	$2,$0,$L899
addiu	$7,$7,4
.set	macro
.set	reorder

$L1053:
srl	$2,$18,3
sw	$15,380($sp)
andi	$4,$18,0x7
addu	$2,$6,$2
li	$3,65536			# 0x10000
addiu	$7,$18,1
lbu	$2,0($2)
addu	$3,$16,$3
sw	$7,316($sp)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L900
lw	$8,10128($3)
.set	macro
.set	reorder

srl	$9,$8,1
sltu	$9,$0,$9
$L761:
addiu	$11,$18,2
srl	$2,$7,3
srl	$3,$11,3
li	$14,16711680			# 0xff0000
li	$13,-16777216			# 0xffffffffff000000
addiu	$20,$14,255
addu	$2,$6,$2
addu	$6,$6,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($2)  
lwr $10, 0($2)  

# 0 "" 2
#NO_APP
sw	$11,316($sp)
ori	$12,$13,0xff00
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($6)  
lwr $3, 0($6)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$20
and	$3,$3,$12
or	$3,$2,$3
srl	$4,$3,16
sll	$2,$3,16
srl	$3,$10,8
sll	$10,$10,8
or	$2,$2,$4
and	$3,$3,$20
and	$4,$10,$12
or	$3,$3,$4
andi	$11,$11,0x7
sll	$17,$3,16
sll	$2,$2,$11
srl	$3,$3,16
srl	$2,$2,30
or	$3,$17,$3
lui	$4,%hi(cce_scale)
andi	$17,$7,0x7
sll	$17,$3,$17
li	$21,65536			# 0x10000
addu	$8,$8,$9
addiu	$4,$4,%lo(cce_scale)
sll	$2,$2,2
addiu	$18,$18,4
addu	$22,$16,$21
srl	$17,$17,31
addu	$2,$2,$4
sw	$8,10128($22)
addiu	$6,$sp,308
sw	$12,396($sp)
move	$7,$0
sw	$18,316($sp)
move	$4,$fp
lwc1	$f21,0($2)
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_ics.constprop.27
.option	pic2
sw	$17,328($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1015
lw	$12,396($sp)
.set	macro
.set	reorder

lui	$5,%hi($LC27)
lw	$22,10128($22)
mov.s	$f0,$f23
addiu	$21,$21,10232
lw	$23,316($sp)
lwc1	$f22,%lo($LC27)($5)
lui	$2,%hi(vlc_scalefactors+4)
addu	$21,$16,$21
lw	$19,308($sp)
sw	$22,364($sp)
move	$18,$0
sw	$fp,392($sp)
li	$4,3			# 0x3
sw	$21,352($sp)
li	$21,1			# 0x1
sw	$0,332($sp)
move	$22,$12
sw	$16,344($sp)
move	$fp,$18
lw	$17,%lo(vlc_scalefactors+4)($2)
move	$2,$23
lw	$3,364($sp)
move	$23,$21
mov.s	$f25,$f22
move	$21,$19
mov.s	$f24,$f22
move	$19,$17
.set	noreorder
.set	nomacro
beq	$3,$4,$L767
move	$17,$2
.set	macro
.set	reorder

$L1057:
lw	$5,344($sp)
lw	$5,160($5)
.set	noreorder
.set	nomacro
blez	$5,$L768
sw	$5,376($sp)
.set	macro
.set	reorder

lw	$2,344($sp)
move	$5,$17
lw	$6,332($sp)
move	$18,$21
sw	$0,348($sp)
move	$16,$fp
sw	$0,340($sp)
move	$17,$23
lbu	$2,144($2)
sll	$3,$6,3
sw	$2,356($sp)
sll	$2,$6,7
subu	$2,$2,$3
lw	$3,356($sp)
addiu	$2,$2,18942
addiu	$3,$3,817
sw	$2,384($sp)
sw	$3,388($sp)
$L769:
lw	$2,356($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L777
lw	$3,340($sp)
.set	macro
.set	reorder

move	$7,$18
lw	$4,384($sp)
move	$18,$17
lw	$6,388($sp)
move	$17,$5
addiu	$21,$3,817
lw	$2,344($sp)
addu	$23,$4,$3
addu	$fp,$6,$3
sll	$21,$21,2
sll	$23,$23,2
sll	$fp,$fp,2
addu	$21,$2,$21
addu	$23,$2,$23
addu	$fp,$2,$fp
move	$2,$16
move	$16,$19
move	$19,$2
$L776:
lw	$2,0($21)
beq	$2,$0,$L770
.set	noreorder
.set	nomacro
bne	$18,$0,$L771
srl	$3,$17,3
.set	macro
.set	reorder

andi	$8,$17,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$20
and	$4,$4,$22
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
move	$5,$17
or	$2,$3,$4
sll	$2,$2,$8
srl	$2,$2,25
sll	$2,$2,2
addu	$2,$16,$2
lh	$17,2($2)
.set	noreorder
.set	nomacro
bltz	$17,$L1054
lh	$2,0($2)
.set	macro
.set	reorder

$L773:
addu	$17,$17,$5
addiu	$2,$2,-60
.set	noreorder
.set	nomacro
beq	$2,$0,$L771
sw	$17,316($sp)
.set	macro
.set	reorder

lw	$3,328($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L903
addu	$19,$19,$2
.set	macro
.set	reorder

andi	$3,$19,0x1
subu	$3,$0,$3
sra	$2,$19,1
sll	$3,$3,1
addiu	$3,$3,1
mtc1	$3,$f0
cvt.s.w	$f20,$f0
$L775:
subu	$2,$0,$2
mov.s	$f12,$f21
lw	$25,%call16(powf)($28)
sw	$7,396($sp)
mtc1	$2,$f0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,powf
1:	jalr	$25
cvt.s.w	$f14,$f0
.set	macro
.set	reorder

mul.s	$f0,$f0,$f20
lw	$28,24($sp)
lw	$7,396($sp)
$L771:
swc1	$f0,0($23)
$L770:
addiu	$21,$21,4
.set	noreorder
.set	nomacro
bne	$21,$fp,$L776
addiu	$23,$23,4
.set	macro
.set	reorder

move	$2,$19
lw	$3,340($sp)
move	$19,$16
move	$16,$2
lw	$2,356($sp)
move	$5,$17
move	$17,$18
addu	$3,$2,$3
move	$18,$7
sw	$3,340($sp)
$L777:
lw	$4,348($sp)
lw	$6,376($sp)
addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$4,$6,$L769
sw	$4,348($sp)
.set	macro
.set	reorder

move	$21,$18
move	$17,$5
$L768:
lw	$3,332($sp)
lw	$4,380($sp)
addiu	$3,$3,1
.set	noreorder
.set	nomacro
beq	$3,$4,$L1055
sw	$3,332($sp)
.set	macro
.set	reorder

$L778:
.set	noreorder
.set	nomacro
beq	$3,$0,$L901
lw	$2,364($sp)
.set	macro
.set	reorder

li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L764
srl	$3,$17,3
.set	macro
.set	reorder

andi	$4,$17,0x7
addu	$3,$21,$3
addiu	$17,$17,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L902
sw	$17,316($sp)
.set	macro
.set	reorder

$L764:
srl	$2,$17,3
andi	$5,$17,0x7
addu	$2,$21,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$20
and	$3,$3,$22
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
move	$6,$17
or	$2,$2,$3
sll	$5,$2,$5
srl	$5,$5,25
sll	$5,$5,2
addu	$5,$19,$5
lh	$4,2($5)
.set	noreorder
.set	nomacro
bltz	$4,$L1056
lh	$9,0($5)
.set	macro
.set	reorder

$L766:
addiu	$fp,$9,-60
mov.s	$f12,$f21
lw	$25,%call16(powf)($28)
addu	$17,$4,$6
subu	$2,$0,$fp
li	$23,1			# 0x1
mtc1	$2,$f0
sw	$17,316($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,powf
1:	jalr	$25
cvt.s.w	$f14,$f0
.set	macro
.set	reorder

lw	$28,24($sp)
$L763:
lw	$2,352($sp)
li	$4,3			# 0x3
lw	$3,364($sp)
addiu	$2,$2,480
.set	noreorder
.set	nomacro
bne	$3,$4,$L1057
sw	$2,352($sp)
.set	macro
.set	reorder

$L767:
lw	$4,352($sp)
lw	$3,332($sp)
swc1	$f0,0($4)
addiu	$3,$3,1
lw	$4,380($sp)
.set	noreorder
.set	nomacro
bne	$3,$4,$L778
sw	$3,332($sp)
.set	macro
.set	reorder

$L1055:
lw	$2,320($sp)
move	$4,$17
lw	$fp,392($sp)
lw	$16,344($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L755
.option	pic2
subu	$3,$2,$17
.set	macro
.set	reorder

$L710:
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$3,$L719
li	$3,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$3,$L707
lw	$4,336($sp)
.set	macro
.set	reorder

$L1111:
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
bne	$4,$3,$L1108
li	$3,2			# 0x2
.set	macro
.set	reorder

addiu	$3,$17,276
lw	$16,848($fp)
addiu	$2,$2,1
sll	$3,$3,2
addu	$3,$fp,$3
sw	$2,1360($fp)
sw	$16,0($3)
$L715:
.set	noreorder
.set	nomacro
beq	$16,$0,$L704
li	$3,1024			# 0x400
.set	macro
.set	reorder

sw	$3,368($sp)
$L725:
lw	$2,316($sp)
addiu	$20,$16,144
lw	$3,308($sp)
srl	$4,$2,3
andi	$17,$2,0x7
addu	$4,$3,$4
addiu	$3,$2,1
lbu	$2,0($4)
sll	$17,$2,$17
andi	$17,$17,0x00ff
srl	$17,$17,7
.set	noreorder
.set	nomacro
bne	$17,$0,$L726
sw	$3,316($sp)
.set	macro
.set	reorder

move	$19,$0
addiu	$18,$sp,308
$L727:
move	$4,$fp
$L1097:
move	$5,$20
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_ics.constprop.27
.option	pic2
move	$7,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L1058
li	$5,37904			# 0x9410
.set	macro
.set	reorder

$L1015:
lw	$31,452($sp)
$L1100:
lw	$fp,448($sp)
$L1120:
lw	$23,444($sp)
lw	$22,440($sp)
lw	$21,436($sp)
lw	$20,432($sp)
lw	$19,428($sp)
lw	$18,424($sp)
lw	$17,420($sp)
lw	$16,416($sp)
ldc1	$f24,472($sp)
ldc1	$f22,464($sp)
ldc1	$f20,456($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,480
.set	macro
.set	reorder

$L1054:
addiu	$5,$5,7
srl	$4,$5,3
andi	$9,$5,0x7
addu	$4,$7,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($4)  
lwr $8, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$8,8
sll	$8,$8,8
and	$4,$4,$20
and	$8,$8,$22
or	$8,$4,$8
sll	$4,$8,16
srl	$8,$8,16
or	$3,$4,$8
sll	$3,$3,$9
srl	$3,$3,$17
addu	$2,$3,$2
sll	$2,$2,2
addu	$2,$16,$2
lh	$8,2($2)
.set	noreorder
.set	nomacro
bltz	$8,$L774
lh	$2,0($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L773
.option	pic2
move	$17,$8
.set	macro
.set	reorder

$L903:
mov.s	$f20,$f22
.option	pic0
.set	noreorder
.set	nomacro
j	$L775
.option	pic2
move	$2,$19
.set	macro
.set	reorder

$L774:
subu	$5,$5,$17
srl	$4,$5,3
andi	$3,$5,0x7
addu	$4,$7,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($4)  
lwr $9, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$9,8
sll	$9,$9,8
and	$4,$4,$20
and	$9,$9,$22
or	$9,$4,$9
sll	$4,$9,16
srl	$9,$9,16
or	$4,$4,$9
sll	$3,$4,$3
srl	$3,$3,$8
addu	$2,$3,$2
sll	$3,$2,2
addu	$3,$16,$3
lh	$2,0($3)
.option	pic0
.set	noreorder
.set	nomacro
j	$L773
.option	pic2
lh	$17,2($3)
.set	macro
.set	reorder

$L902:
mov.s	$f0,$f24
move	$23,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L763
.option	pic2
move	$fp,$0
.set	macro
.set	reorder

$L1056:
addiu	$6,$17,7
subu	$8,$0,$4
srl	$2,$6,3
andi	$3,$6,0x7
addu	$2,$21,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$7,8
sll	$7,$7,8
and	$2,$2,$20
and	$7,$7,$22
or	$2,$2,$7
sll	$7,$2,16
srl	$2,$2,16
or	$5,$7,$2
sll	$5,$5,$3
srl	$3,$5,$4
addu	$3,$3,$9
sll	$3,$3,2
addu	$3,$19,$3
lh	$4,2($3)
.set	noreorder
.set	nomacro
bgez	$4,$L766
lh	$9,0($3)
.set	macro
.set	reorder

addu	$6,$6,$8
srl	$5,$6,3
andi	$2,$6,0x7
addu	$5,$21,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
srl	$5,$7,8
sll	$7,$7,8
and	$5,$5,$20
and	$7,$7,$22
or	$7,$5,$7
sll	$5,$7,16
srl	$7,$7,16
or	$3,$5,$7
sll	$3,$3,$2
srl	$2,$3,$4
addu	$2,$2,$9
sll	$2,$2,2
addu	$2,$19,$2
lh	$9,0($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L766
.option	pic2
lh	$4,2($2)
.set	macro
.set	reorder

$L900:
.option	pic0
.set	noreorder
.set	nomacro
j	$L761
.option	pic2
li	$9,1			# 0x1
.set	macro
.set	reorder

$L901:
mov.s	$f0,$f25
move	$fp,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L763
.option	pic2
li	$23,1			# 0x1
.set	macro
.set	reorder

$L1058:
move	$4,$fp
addu	$5,$16,$5
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_ics.constprop.27
.option	pic2
move	$7,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L1015
lw	$28,24($sp)
.set	macro
.set	reorder

beq	$17,$0,$L733
.set	noreorder
.set	nomacro
bne	$19,$0,$L1059
li	$17,43152			# 0xa890
.set	macro
.set	reorder

$L733:
li	$17,43152			# 0xa890
addiu	$18,$16,5392
addu	$17,$16,$17
$L735:
li	$2,65536			# 0x10000
$L1096:
addu	$2,$16,$2
lw	$5,-27604($2)
lw	$3,-27616($2)
.set	noreorder
.set	nomacro
blez	$3,$L744
sw	$5,332($sp)
.set	macro
.set	reorder

li	$12,37924			# 0x9424
lbu	$6,-27632($2)
li	$21,2147418112			# 0x7fff0000
sw	$2,328($sp)
addu	$12,$16,$12
move	$25,$0
move	$22,$0
ori	$21,$21,0xfff2
$L745:
lbu	$2,0($12)
move	$3,$0
slt	$7,$0,$6
$L756:
.set	noreorder
.set	nomacro
beq	$7,$0,$L1109
lw	$5,328($sp)
.set	macro
.set	reorder

$L754:
sll	$4,$22,2
li	$5,65536			# 0x10000
addu	$4,$16,$4
addu	$4,$4,$5
lw	$5,-24508($4)
addiu	$5,$5,-14
sltu	$5,$5,2
bne	$5,$0,$L1061
lw	$4,-23996($4)
subu	$5,$4,$3
slt	$7,$4,$6
move	$3,$4
.set	noreorder
.set	nomacro
bne	$7,$0,$L754
addu	$22,$22,$5
.set	macro
.set	reorder

lw	$5,328($sp)
$L1109:
addiu	$25,$25,1
sll	$2,$2,9
addiu	$12,$12,1
addu	$18,$18,$2
lw	$3,-27616($5)
slt	$3,$25,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L745
addu	$17,$17,$2
.set	macro
.set	reorder

$L744:
lw	$4,316($sp)
$L1042:
lw	$3,320($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L755
.option	pic2
subu	$3,$3,$4
.set	macro
.set	reorder

$L1061:
lw	$24,-23996($4)
slt	$4,$3,$24
.set	noreorder
.set	nomacro
beq	$4,$0,$L756
addiu	$4,$22,10257
.set	macro
.set	reorder

lw	$6,332($sp)
move	$23,$3
addu	$20,$4,$24
sll	$3,$4,2
subu	$20,$20,$23
addiu	$15,$22,8
sll	$14,$23,1
sll	$20,$20,2
addu	$13,$16,$3
addu	$15,$16,$15
addu	$14,$6,$14
addu	$20,$16,$20
$L753:
lw	$3,0($13)
addu	$3,$3,$21
sll	$3,$3,1
.set	noreorder
.set	nomacro
beq	$19,$0,$L748
addiu	$3,$3,-1
.set	macro
.set	reorder

lbu	$4,0($15)
subu	$4,$0,$4
sll	$4,$4,1
addiu	$4,$4,1
mul	$3,$3,$4
$L748:
lw	$8,992($13)
.set	noreorder
.set	nomacro
beq	$2,$0,$L749
mul	$8,$3,$8
.set	macro
.set	reorder

lhu	$3,0($14)
move	$10,$0
lhu	$11,2($14)
sll	$6,$3,2
subu	$9,$11,$3
addu	$7,$18,$6
sll	$9,$9,2
addu	$6,$17,$6
slt	$11,$3,$11
$L752:
beq	$11,$0,$L750
move	$4,$0
$L751:
addu	$2,$7,$4
addu	$5,$6,$4
addiu	$4,$4,4
lw	$2,0($2)
mult	$2,$8
mflo	$2
mfhi	$3
srl	$2,$2,14
sll	$3,$3,18
or	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$4,$9,$L751
sw	$2,0($5)
.set	macro
.set	reorder

lbu	$2,0($12)
$L750:
addiu	$10,$10,1
addiu	$7,$7,512
slt	$3,$10,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L752
addiu	$6,$6,512
.set	macro
.set	reorder

$L749:
addiu	$13,$13,4
addiu	$15,$15,1
.set	noreorder
.set	nomacro
bne	$13,$20,$L753
addiu	$14,$14,2
.set	macro
.set	reorder

lw	$4,328($sp)
subu	$23,$24,$23
move	$3,$24
addu	$22,$23,$22
lbu	$6,-27632($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L756
.option	pic2
slt	$7,$24,$6
.set	macro
.set	reorder

$L726:
addiu	$18,$sp,308
move	$4,$fp
move	$5,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_ics_info.isra.21
.option	pic2
move	$6,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L894
lw	$28,24($sp)
.set	macro
.set	reorder

li	$4,65536			# 0x10000
addiu	$5,$16,240
addu	$4,$16,$4
move	$2,$20
addiu	$3,$4,-27632
lbu	$4,-27620($4)
$L729:
lw	$9,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$8,-12($2)
lw	$7,-8($2)
lw	$6,-4($2)
sw	$9,-16($3)
sw	$8,-12($3)
sw	$7,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$5,$L729
sw	$6,-4($3)
.set	macro
.set	reorder

lw	$6,0($2)
lw	$5,4($2)
li	$2,65536			# 0x10000
addu	$2,$16,$2
sw	$6,0($3)
sw	$5,4($3)
li	$5,16711680			# 0xff0000
sb	$4,-27619($2)
lw	$6,316($sp)
addiu	$5,$5,255
lw	$7,308($sp)
srl	$2,$6,3
andi	$8,$6,0x7
addu	$2,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$4,$4,$5
or	$2,$2,$4
sll	$4,$2,16
srl	$3,$2,16
addiu	$2,$6,2
or	$3,$4,$3
sll	$3,$3,$8
li	$4,3			# 0x3
srl	$3,$3,30
.set	noreorder
.set	nomacro
beq	$3,$4,$L1062
sw	$2,316($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L1063
move	$19,$0
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1097
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

$L1059:
lw	$2,160($16)
addiu	$18,$16,5392
lw	$25,172($16)
.set	noreorder
.set	nomacro
blez	$2,$L735
addu	$17,$16,$17
.set	macro
.set	reorder

lbu	$5,144($16)
addiu	$11,$16,164
move	$20,$0
move	$12,$0
move	$22,$17
move	$21,$18
$L736:
.set	noreorder
.set	nomacro
beq	$5,$0,$L1064
addiu	$7,$12,817
.set	macro
.set	reorder

lbu	$3,0($11)
move	$13,$0
sll	$7,$7,2
move	$14,$25
addu	$7,$16,$7
move	$4,$12
$L741:
addu	$4,$16,$4
lbu	$2,8($4)
beq	$2,$0,$L737
lw	$2,0($7)
sltu	$2,$2,13
.set	noreorder
.set	nomacro
beq	$2,$0,$L737
li	$4,37760			# 0x9380
.set	macro
.set	reorder

addu	$2,$7,$4
lw	$2,0($2)
sltu	$2,$2,13
beq	$2,$0,$L737
beq	$3,$0,$L737
lhu	$2,0($14)
move	$23,$0
lhu	$10,2($14)
sll	$8,$2,2
subu	$10,$10,$2
addu	$15,$21,$8
sll	$24,$10,2
addu	$8,$22,$8
$L740:
blez	$10,$L738
addu	$9,$15,$24
move	$5,$8
move	$4,$15
$L739:
lw	$2,0($4)
addiu	$5,$5,4
lw	$6,-4($5)
addiu	$4,$4,4
addu	$3,$2,$6
subu	$2,$2,$6
sw	$3,-4($4)
.set	noreorder
.set	nomacro
bne	$4,$9,$L739
sw	$2,-4($5)
.set	macro
.set	reorder

lbu	$3,0($11)
$L738:
addiu	$23,$23,1
addiu	$15,$15,512
slt	$2,$23,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L740
addiu	$8,$8,512
.set	macro
.set	reorder

lbu	$5,144($16)
$L737:
addiu	$13,$13,1
addiu	$7,$7,4
slt	$2,$13,$5
addu	$4,$12,$13
.set	noreorder
.set	nomacro
bne	$2,$0,$L741
addiu	$14,$14,2
.set	macro
.set	reorder

lw	$2,160($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L743
.option	pic2
move	$12,$4
.set	macro
.set	reorder

$L1064:
lbu	$3,0($11)
$L743:
addiu	$20,$20,1
sll	$3,$3,9
slt	$4,$20,$2
addu	$21,$21,$3
addu	$22,$22,$3
.set	noreorder
.set	nomacro
bne	$4,$0,$L736
addiu	$11,$11,1
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1096
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L894:
.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1063:
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$4,$L1065
addiu	$4,$16,8
.set	macro
.set	reorder

lbu	$6,144($16)
lw	$2,160($16)
li	$5,1			# 0x1
lw	$25,%call16(memset)($28)
li	$19,2			# 0x2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
mul	$6,$6,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1097
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

$L1062:
lui	$6,%hi($LC32)
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC32)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1065:
lbu	$3,144($16)
lw	$4,160($16)
mul	$3,$3,$4
.set	noreorder
.set	nomacro
blez	$3,$L896
addu	$5,$2,$3
.set	macro
.set	reorder

$L732:
srl	$4,$2,3
andi	$8,$2,0x7
addu	$4,$7,$4
addiu	$2,$2,1
lbu	$3,0($4)
subu	$4,$2,$6
sw	$2,316($sp)
addu	$4,$16,$4
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$5,$2,$L732
sb	$3,5($4)
.set	macro
.set	reorder

$L896:
.option	pic0
.set	noreorder
.set	nomacro
j	$L727
.option	pic2
li	$19,1			# 0x1
.set	macro
.set	reorder

$L713:
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$4,$L1066
lw	$5,336($sp)
.set	macro
.set	reorder

$L712:
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
beq	$2,$3,$L1110
lw	$4,336($sp)
.set	macro
.set	reorder

$L711:
li	$3,2			# 0x2
$L1118:
.set	noreorder
.set	nomacro
beq	$2,$3,$L1067
lw	$4,336($sp)
.set	macro
.set	reorder

$L709:
xori	$3,$6,0x2
sltu	$3,$0,$3
.set	noreorder
.set	nomacro
beq	$2,$3,$L1111
lw	$4,336($sp)
.set	macro
.set	reorder

li	$3,2			# 0x2
$L1108:
.set	noreorder
.set	nomacro
beq	$6,$3,$L1105
lui	$6,%hi($LC31)
.set	macro
.set	reorder

$L707:
.set	noreorder
.set	nomacro
bne	$2,$0,$L704
lw	$3,336($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L704
li	$3,1			# 0x1
.set	macro
.set	reorder

addiu	$2,$17,260
lw	$16,784($fp)
sll	$2,$2,2
addu	$2,$fp,$2
sw	$3,1360($fp)
sw	$16,0($2)
$L722:
.set	noreorder
.set	nomacro
beq	$16,$0,$L704
li	$3,1024			# 0x400
.set	macro
.set	reorder

sw	$3,368($sp)
$L779:
addiu	$5,$16,144
addiu	$6,$sp,308
move	$7,$0
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_ics.constprop.27
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

lw	$28,24($sp)
$L724:
.set	noreorder
.set	nomacro
bne	$2,$0,$L1100
lw	$31,452($sp)
.set	macro
.set	reorder

$L812:
lw	$4,316($sp)
lw	$3,320($sp)
subu	$3,$3,$4
slt	$3,$3,3
.set	noreorder
.set	nomacro
beq	$3,$0,$L1068
sw	$16,328($sp)
.set	macro
.set	reorder

$L789:
lui	$6,%hi(overread_err)
$L1099:
lw	$25,%call16(av_log)($28)
lw	$4,480($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo(overread_err)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L784:
lw	$25,%call16(memset)($28)
addiu	$17,$sp,32
move	$5,$0
li	$6,256			# 0x100
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

addiu	$5,$fp,8
addiu	$7,$sp,308
move	$4,$fp
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_pce.isra.20
.option	pic2
move	$6,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L1015
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$2,10512($fp)
sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L786
lui	$6,%hi($LC33)
.set	macro
.set	reorder

lw	$25,%call16(av_log)($28)
lw	$4,480($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC33)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L812
.option	pic2
lw	$28,24($sp)
.set	macro
.set	reorder

$L787:
li	$2,15			# 0xf
.set	noreorder
.set	nomacro
beq	$18,$2,$L788
lw	$4,316($sp)
.set	macro
.set	reorder

sll	$2,$18,3
lw	$3,320($sp)
subu	$3,$3,$4
slt	$2,$3,$2
bne	$2,$0,$L789
.set	noreorder
.set	nomacro
beq	$18,$0,$L755
move	$6,$4
.set	macro
.set	reorder

$L791:
li	$2,16711680			# 0xff0000
lw	$3,328($sp)
sw	$16,332($sp)
addiu	$19,$2,255
li	$2,-16777216			# 0xffffffffff000000
ori	$18,$2,0xff00
li	$2,65536			# 0x10000
addiu	$2,$2,17920
addu	$2,$3,$2
sw	$2,340($sp)
$L811:
lw	$7,308($sp)
srl	$2,$6,3
andi	$4,$6,0x7
addiu	$8,$6,4
addu	$2,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$19
and	$3,$3,$18
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
or	$2,$2,$3
sll	$2,$2,$4
li	$3,13			# 0xd
srl	$2,$2,28
.set	noreorder
.set	nomacro
beq	$2,$3,$L906
sw	$8,316($sp)
.set	macro
.set	reorder

li	$3,14			# 0xe
.set	noreorder
.set	nomacro
beq	$2,$3,$L794
li	$3,11			# 0xb
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L1069
sll	$2,$23,3
.set	macro
.set	reorder

lw	$16,332($sp)
$L1119:
addiu	$2,$2,-4
addu	$8,$2,$8
move	$4,$8
.option	pic0
.set	noreorder
.set	nomacro
j	$L1042
.option	pic2
sw	$8,316($sp)
.set	macro
.set	reorder

$L906:
lw	$3,328($sp)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1070
move	$16,$0
.set	macro
.set	reorder

$L796:
lw	$2,20($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1071
lui	$3,%hi($LC35)
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
beq	$2,$3,$L1072
li	$2,4			# 0x4
.set	macro
.set	reorder

$L799:
lw	$4,44($fp)
li	$2,-1			# 0xffffffffffffffff
beq	$4,$2,$L1073
$L800:
li	$2,-1			# 0xffffffffffffffff
sw	$2,20($fp)
$L801:
lw	$3,344($sp)
addiu	$6,$sp,308
lw	$25,%call16(ff_decode_sbr_extension)($28)
move	$4,$fp
lw	$5,340($sp)
move	$7,$16
sw	$23,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_decode_sbr_extension
1:	jalr	$25
sw	$3,20($sp)
.set	macro
.set	reorder

subu	$23,$23,$2
lw	$28,24($sp)
.set	noreorder
.set	nomacro
blez	$23,$L1074
lw	$4,316($sp)
.set	macro
.set	reorder

$L909:
.option	pic0
.set	noreorder
.set	nomacro
j	$L811
.option	pic2
move	$6,$4
.set	macro
.set	reorder

$L794:
lw	$3,328($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L796
li	$16,1			# 0x1
.set	macro
.set	reorder

$L1070:
lui	$2,%hi($LC34)
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
li	$5,16			# 0x10
addiu	$6,$2,%lo($LC34)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
lw	$16,332($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L1042
.option	pic2
lw	$4,316($sp)
.set	macro
.set	reorder

$L1073:
lw	$2,10512($fp)
sltu	$3,$2,4
.set	noreorder
.set	nomacro
beq	$3,$0,$L800
li	$5,1			# 0x1
.set	macro
.set	reorder

lw	$3,0($fp)
lw	$3,68($3)
.set	noreorder
.set	nomacro
bne	$3,$5,$L800
addiu	$5,$fp,528
.set	macro
.set	reorder

lw	$7,16($fp)
sw	$4,20($fp)
move	$4,$fp
sw	$3,44($fp)
move	$6,$5
.option	pic0
.set	noreorder
.set	nomacro
jal	output_configure
.option	pic2
sw	$2,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L801
.option	pic2
lw	$28,24($sp)
.set	macro
.set	reorder

$L1072:
lw	$3,10512($fp)
.set	noreorder
.set	nomacro
bne	$3,$2,$L799
sll	$2,$23,3
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1119
.option	pic2
lw	$16,332($sp)
.set	macro
.set	reorder

$L1071:
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
li	$5,16			# 0x10
addiu	$6,$3,%lo($LC35)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
lw	$16,332($sp)
.set	macro
.set	reorder

sll	$2,$23,3
lw	$3,316($sp)
addiu	$2,$2,-4
lw	$28,24($sp)
addu	$2,$2,$3
move	$4,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L1042
.option	pic2
sw	$2,316($sp)
.set	macro
.set	reorder

$L788:
lw	$6,316($sp)
li	$4,16711680			# 0xff0000
lw	$2,308($sp)
addiu	$4,$4,255
srl	$3,$6,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$4
li	$4,-16777216			# 0xffffffffff000000
ori	$4,$4,0xff00
and	$3,$3,$4
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$6,0x7
or	$2,$3,$2
lw	$3,320($sp)
sll	$2,$2,$4
addiu	$6,$6,8
srl	$2,$2,24
subu	$3,$3,$6
addiu	$23,$2,14
sll	$2,$23,3
slt	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L791
sw	$6,316($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1099
.option	pic2
lui	$6,%hi(overread_err)
.set	macro
.set	reorder

$L786:
li	$2,1			# 0x1
addiu	$5,$fp,528
move	$7,$0
sw	$2,16($sp)
move	$4,$fp
.option	pic0
.set	noreorder
.set	nomacro
jal	output_configure
.option	pic2
move	$6,$17
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L724
.option	pic2
lw	$28,24($sp)
.set	macro
.set	reorder

$L1050:
lw	$5,480($sp)
lw	$4,12($fp)
lw	$2,%got(aac_bugs)($28)
lw	$3,68($5)
sw	$4,0($2)
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
beq	$3,$2,$L814
addiu	$2,$fp,976
.set	macro
.set	reorder

addiu	$3,$fp,912
li	$20,65536			# 0x10000
li	$21,3			# 0x3
sw	$2,332($sp)
li	$22,2			# 0x2
sw	$2,328($sp)
sw	$3,344($sp)
$L815:
lw	$2,328($sp)
slt	$3,$21,2
move	$19,$0
sw	$3,376($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L843
.option	pic2
sw	$2,336($sp)
.set	macro
.set	reorder

$L818:
.set	noreorder
.set	nomacro
beq	$21,$22,$L838
addiu	$5,$16,144
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
jal	imdct_and_windowing
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

lw	$28,24($sp)
$L839:
li	$6,3			# 0x3
.set	noreorder
.set	nomacro
beq	$21,$6,$L1112
lw	$2,336($sp)
.set	macro
.set	reorder

$L882:
move	$4,$fp
$L1104:
move	$5,$16
move	$6,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_channel_coupling.constprop.25
.option	pic2
move	$7,$19
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$2,336($sp)
$L1112:
addiu	$19,$19,1
addiu	$2,$2,4
sw	$2,336($sp)
li	$2,16			# 0x10
.set	noreorder
.set	nomacro
beq	$19,$2,$L1075
lw	$3,328($sp)
.set	macro
.set	reorder

$L843:
lw	$2,336($sp)
lw	$16,0($2)
.set	noreorder
.set	nomacro
beq	$16,$0,$L1112
lw	$3,376($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L818
li	$2,37904			# 0x9410
.set	macro
.set	reorder

lw	$18,344($sp)
addiu	$23,$16,144
sw	$16,380($sp)
addu	$2,$16,$2
sw	$19,340($sp)
move	$9,$18
sw	$21,348($sp)
sw	$18,364($sp)
move	$18,$fp
sw	$2,356($sp)
move	$fp,$9
.option	pic0
.set	noreorder
.set	nomacro
j	$L820
.option	pic2
sw	$23,352($sp)
.set	macro
.set	reorder

$L822:
lw	$2,332($sp)
$L1113:
addiu	$fp,$fp,4
.set	noreorder
.set	nomacro
beq	$2,$fp,$L1076
lw	$16,380($sp)
.set	macro
.set	reorder

$L820:
lw	$23,0($fp)
.set	noreorder
.set	nomacro
beq	$23,$0,$L822
addu	$16,$23,$20
.set	macro
.set	reorder

lw	$2,10128($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1113
lw	$2,332($sp)
.set	macro
.set	reorder

lw	$6,10132($16)
.set	noreorder
.set	nomacro
bltz	$6,$L1113
li	$3,65536			# 0x10000
.set	macro
.set	reorder

lw	$4,348($sp)
move	$17,$0
ori	$3,$3,0x27d8
addu	$21,$23,$3
move	$19,$0
lw	$2,-64($21)
.set	noreorder
.set	nomacro
beq	$4,$2,$L1077
move	$3,$6
.set	macro
.set	reorder

$L823:
lw	$2,0($21)
li	$4,1			# 0x1
xori	$2,$2,0x3
movz	$4,$22,$2
addu	$17,$17,$4
$L826:
addiu	$19,$19,1
slt	$2,$3,$19
.set	noreorder
.set	nomacro
bne	$2,$0,$L822
addiu	$21,$21,4
.set	macro
.set	reorder

lw	$2,-64($21)
lw	$4,348($sp)
bne	$4,$2,$L823
$L1077:
lw	$2,-32($21)
lw	$5,340($sp)
.set	noreorder
.set	nomacro
bne	$5,$2,$L823
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$3,0($21)
.set	noreorder
.set	nomacro
beq	$3,$2,$L825
lw	$5,352($sp)
.set	macro
.set	reorder

move	$4,$18
move	$6,$23
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$2,0($21)
beq	$2,$0,$L825
.set	noreorder
.set	nomacro
beq	$2,$22,$L1041
addiu	$17,$17,1
.set	macro
.set	reorder

$L825:
addiu	$2,$17,1
lw	$5,356($sp)
move	$7,$17
move	$4,$18
move	$6,$23
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
sw	$2,400($sp)
.set	macro
.set	reorder

lw	$2,400($sp)
move	$17,$2
$L1041:
.option	pic0
.set	noreorder
.set	nomacro
j	$L826
.option	pic2
lw	$3,10132($16)
.set	macro
.set	reorder

$L814:
addiu	$2,$fp,912
addiu	$3,$fp,976
li	$20,65536			# 0x10000
addiu	$21,$fp,784
sw	$2,336($sp)
move	$16,$0
sw	$3,328($sp)
$L883:
lw	$18,0($21)
.set	noreorder
.set	nomacro
beq	$18,$0,$L845
li	$23,37904			# 0x9410
.set	macro
.set	reorder

lw	$19,336($sp)
addiu	$22,$18,144
addu	$23,$18,$23
sw	$23,332($sp)
move	$23,$19
$L847:
lw	$8,0($23)
.set	noreorder
.set	nomacro
beq	$8,$0,$L1114
lw	$2,328($sp)
.set	macro
.set	reorder

addu	$17,$8,$20
lw	$2,10128($17)
beq	$2,$0,$L1078
$L849:
lw	$2,328($sp)
$L1114:
addiu	$23,$23,4
.set	noreorder
.set	nomacro
bne	$23,$2,$L847
li	$9,1			# 0x1
.set	macro
.set	reorder

li	$23,37904			# 0x9410
addu	$23,$18,$23
sw	$23,332($sp)
$L848:
lw	$8,0($19)
.set	noreorder
.set	nomacro
beq	$8,$0,$L1115
lw	$2,328($sp)
.set	macro
.set	reorder

addu	$17,$8,$20
lw	$2,10128($17)
beq	$2,$9,$L1079
$L858:
lw	$2,328($sp)
$L1115:
addiu	$19,$19,4
.set	noreorder
.set	nomacro
bne	$2,$19,$L848
move	$4,$fp
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
jal	imdct_and_windowing
.option	pic2
move	$5,$22
.set	macro
.set	reorder

move	$6,$0
move	$4,$fp
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_channel_coupling.constprop.25
.option	pic2
move	$7,$16
.set	macro
.set	reorder

lw	$28,24($sp)
$L845:
addiu	$16,$16,1
li	$2,16			# 0x10
.set	noreorder
.set	nomacro
bne	$16,$2,$L883
addiu	$21,$21,4
.set	macro
.set	reorder

lw	$3,20($fp)
$L1103:
li	$2,1			# 0x1
beq	$3,$2,$L1080
move	$2,$0
$L865:
lw	$8,10512($fp)
lw	$5,368($sp)
sltu	$4,$8,4
.set	noreorder
.set	nomacro
beq	$4,$0,$L866
sll	$3,$5,$2
.set	macro
.set	reorder

lw	$4,12($fp)
lw	$6,480($sp)
sll	$2,$4,$2
sw	$3,76($6)
sw	$2,64($6)
$L866:
lw	$4,480($sp)
lw	$5,488($sp)
lw	$2,68($4)
lw	$7,0($5)
mul	$2,$3,$2
sll	$2,$2,1
slt	$4,$7,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L1081
lui	$6,%hi($LC36)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L1082
sw	$2,0($5)
.set	macro
.set	reorder

$L868:
.set	noreorder
.set	nomacro
beq	$8,$0,$L1116
lw	$2,316($sp)
.set	macro
.set	reorder

li	$2,4			# 0x4
sw	$2,10512($fp)
lw	$2,316($sp)
$L1116:
lw	$6,372($sp)
addiu	$2,$2,7
sra	$2,$2,3
slt	$3,$2,$6
.set	noreorder
.set	nomacro
beq	$3,$0,$L915
lw	$4,360($sp)
.set	macro
.set	reorder

addu	$3,$4,$2
lbu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1015
addiu	$3,$2,1
.set	macro
.set	reorder

move	$5,$4
addu	$3,$5,$3
addu	$4,$4,$6
$L876:
.set	noreorder
.set	nomacro
beq	$3,$4,$L1083
addiu	$3,$3,1
.set	macro
.set	reorder

lbu	$5,-1($3)
.set	noreorder
.set	nomacro
beq	$5,$0,$L876
lw	$31,452($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1120
.option	pic2
lw	$fp,448($sp)
.set	macro
.set	reorder

$L1078:
lw	$6,10132($17)
.set	noreorder
.set	nomacro
bltz	$6,$L849
li	$4,65536			# 0x10000
.set	macro
.set	reorder

move	$10,$0
ori	$4,$4,0x27d8
.option	pic0
.set	noreorder
.set	nomacro
j	$L855
.option	pic2
addu	$3,$8,$4
.set	macro
.set	reorder

$L850:
lw	$4,0($3)
li	$7,2			# 0x2
xori	$5,$4,0x3
li	$4,1			# 0x1
movz	$4,$7,$5
addu	$2,$2,$4
$L853:
addiu	$10,$10,1
slt	$4,$6,$10
.set	noreorder
.set	nomacro
bne	$4,$0,$L849
addiu	$3,$3,4
.set	macro
.set	reorder

$L855:
lw	$4,-64($3)
bne	$4,$0,$L850
lw	$4,-32($3)
.set	noreorder
.set	nomacro
bne	$16,$4,$L850
li	$4,1			# 0x1
.set	macro
.set	reorder

lw	$5,0($3)
.set	noreorder
.set	nomacro
beq	$5,$4,$L852
move	$4,$fp
.set	macro
.set	reorder

sw	$2,400($sp)
move	$6,$8
sw	$3,404($sp)
move	$7,$2
sw	$8,408($sp)
move	$5,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
sw	$10,396($sp)
.set	macro
.set	reorder

lw	$3,404($sp)
lw	$2,400($sp)
lw	$8,408($sp)
lw	$4,0($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L852
lw	$10,396($sp)
.set	macro
.set	reorder

li	$5,2			# 0x2
.set	noreorder
.set	nomacro
bne	$4,$5,$L852
addiu	$2,$2,1
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L853
.option	pic2
lw	$6,10132($17)
.set	macro
.set	reorder

$L1049:
lw	$25,%call16(ff_aac_parse_header)($28)
addiu	$4,$sp,308
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_aac_parse_header
1:	jalr	$25
addiu	$5,$sp,288
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$2,$L684
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$2,10512($fp)
li	$18,4			# 0x4
.set	noreorder
.set	nomacro
beq	$2,$18,$L1102
lw	$2,288($sp)
.set	macro
.set	reorder

lbu	$17,303($sp)
.set	noreorder
.set	nomacro
bne	$17,$0,$L1084
lw	$25,%call16(memset)($28)
.set	macro
.set	reorder

sw	$0,10512($fp)
$L689:
li	$2,-1			# 0xffffffffffffffff
sw	$2,20($fp)
sw	$2,44($fp)
lw	$2,288($sp)
$L1102:
lw	$4,0($fp)
$L1122:
sw	$2,12($fp)
lbu	$5,302($sp)
lw	$3,64($4)
sw	$5,8($fp)
lbu	$5,301($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L692
sw	$5,4($fp)
.set	macro
.set	reorder

sw	$2,64($4)
$L692:
lbu	$3,304($sp)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L693
lw	$25,%call16(av_log_missing_feature)($28)
.set	macro
.set	reorder

lbu	$2,300($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L695
lw	$2,316($sp)
.set	macro
.set	reorder

addiu	$2,$2,16
sw	$2,316($sp)
$L695:
lw	$7,8($fp)
slt	$2,$7,13
.set	noreorder
.set	nomacro
beq	$2,$0,$L697
lw	$17,308($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L683
.option	pic2
lw	$16,316($sp)
.set	macro
.set	reorder

$L693:
lui	$5,%hi($LC28)
move	$6,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log_missing_feature
1:	jalr	$25
addiu	$5,$5,%lo($LC28)
.set	macro
.set	reorder

lw	$28,24($sp)
$L878:
lui	$6,%hi($LC29)
lw	$25,%call16(av_log)($28)
$L1121:
lw	$4,480($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC29)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L697:
lui	$6,%hi($LC14)
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L684:
.set	noreorder
.set	nomacro
beq	$2,$0,$L695
lui	$6,%hi($LC29)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1121
.option	pic2
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

$L1084:
addiu	$16,$sp,32
move	$5,$0
li	$6,256			# 0x100
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

move	$4,$fp
sw	$17,16($fp)
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	set_default_channel_config.isra.13
.option	pic2
move	$6,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L878
lw	$28,24($sp)
.set	macro
.set	reorder

li	$2,2			# 0x2
lbu	$7,303($sp)
addiu	$5,$fp,528
move	$4,$fp
sw	$2,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	output_configure
.option	pic2
move	$6,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L878
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$2,10512($fp)
.set	noreorder
.set	nomacro
bne	$2,$18,$L689
lw	$2,288($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1122
.option	pic2
lw	$4,0($fp)
.set	macro
.set	reorder

$L1075:
addiu	$21,$21,-1
li	$2,-1			# 0xffffffffffffffff
addiu	$3,$3,-64
.set	noreorder
.set	nomacro
bne	$21,$2,$L815
sw	$3,328($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1103
.option	pic2
lw	$3,20($fp)
.set	macro
.set	reorder

$L1079:
lw	$5,10132($17)
.set	noreorder
.set	nomacro
bltz	$5,$L858
li	$4,65536			# 0x10000
.set	macro
.set	reorder

move	$2,$0
ori	$4,$4,0x27d8
addu	$3,$8,$4
.option	pic0
.set	noreorder
.set	nomacro
j	$L864
.option	pic2
move	$23,$0
.set	macro
.set	reorder

$L859:
lw	$4,0($3)
li	$6,2			# 0x2
xori	$4,$4,0x3
movn	$6,$9,$4
addu	$2,$2,$6
$L862:
addiu	$23,$23,1
slt	$4,$5,$23
.set	noreorder
.set	nomacro
bne	$4,$0,$L858
addiu	$3,$3,4
.set	macro
.set	reorder

$L864:
lw	$4,-64($3)
bne	$4,$0,$L859
lw	$4,-32($3)
bne	$16,$4,$L859
lw	$4,0($3)
.set	noreorder
.set	nomacro
beq	$4,$9,$L861
move	$4,$fp
.set	macro
.set	reorder

sw	$2,400($sp)
move	$6,$8
sw	$3,404($sp)
move	$7,$2
sw	$8,408($sp)
move	$5,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
sw	$9,396($sp)
.set	macro
.set	reorder

lw	$3,404($sp)
lw	$2,400($sp)
lw	$8,408($sp)
lw	$4,0($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L861
lw	$9,396($sp)
.set	macro
.set	reorder

li	$5,2			# 0x2
.set	noreorder
.set	nomacro
bne	$4,$5,$L861
addiu	$2,$2,1
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L862
.option	pic2
lw	$5,10132($17)
.set	macro
.set	reorder

$L1082:
lw	$5,480($sp)
li	$4,2			# 0x2
lw	$2,68($5)
.set	noreorder
.set	nomacro
beq	$2,$4,$L1085
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$4,$L871
move	$4,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$2,$L868
sll	$11,$2,1
.set	macro
.set	reorder

lw	$12,9732($fp)
lw	$5,484($sp)
move	$4,$0
$L872:
move	$6,$0
move	$9,$12
move	$7,$5
$L874:
lw	$10,0($9)
addiu	$6,$6,1
addiu	$9,$9,4
slt	$13,$6,$3
sra	$10,$10,1
sh	$10,0($7)
.set	noreorder
.set	nomacro
bne	$13,$0,$L874
addu	$7,$7,$11
.set	macro
.set	reorder

addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$4,$2,$L872
addiu	$5,$5,2
.set	macro
.set	reorder

.option	pic0
j	$L868
.option	pic2
$L1081:
lw	$25,%call16(av_log)($28)
lw	$4,480($sp)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC36)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L871:
lw	$5,9724($fp)
lw	$2,484($sp)
$L873:
lw	$6,0($5)
addiu	$4,$4,1
addiu	$2,$2,2
slt	$7,$4,$3
sra	$6,$6,1
addiu	$5,$5,4
.set	noreorder
.set	nomacro
bne	$7,$0,$L873
sh	$6,-2($2)
.set	macro
.set	reorder

.option	pic0
j	$L868
.option	pic2
$L1085:
lw	$6,9724($fp)
move	$5,$0
lw	$4,9728($fp)
lw	$2,484($sp)
$L870:
lw	$9,0($6)
addiu	$5,$5,1
lw	$7,0($4)
addiu	$2,$2,4
slt	$10,$5,$3
sra	$9,$9,1
sra	$7,$7,1
addiu	$6,$6,4
sh	$9,-4($2)
addiu	$4,$4,4
.set	noreorder
.set	nomacro
bne	$10,$0,$L870
sh	$7,-2($2)
.set	macro
.set	reorder

.option	pic0
j	$L868
.option	pic2
$L1083:
.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
lw	$2,372($sp)
.set	macro
.set	reorder

$L915:
.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
move	$2,$6
.set	macro
.set	reorder

$L1080:
lw	$2,32($fp)
lw	$3,12($fp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L865
.option	pic2
slt	$2,$3,$2
.set	macro
.set	reorder

$L852:
addiu	$11,$2,1
lw	$5,332($sp)
move	$6,$8
sw	$3,404($sp)
move	$7,$2
sw	$8,408($sp)
move	$4,$fp
sw	$10,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
sw	$11,400($sp)
.set	macro
.set	reorder

lw	$11,400($sp)
lw	$6,10132($17)
lw	$3,404($sp)
move	$2,$11
lw	$8,408($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L853
.option	pic2
lw	$10,396($sp)
.set	macro
.set	reorder

$L861:
addiu	$11,$2,1
lw	$5,332($sp)
move	$7,$2
sw	$3,404($sp)
move	$6,$8
sw	$8,408($sp)
move	$4,$fp
sw	$9,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
sw	$11,400($sp)
.set	macro
.set	reorder

lw	$11,400($sp)
lw	$5,10132($17)
lw	$3,404($sp)
move	$2,$11
lw	$8,408($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L862
.option	pic2
lw	$9,396($sp)
.set	macro
.set	reorder

$L1052:
lui	$6,%hi(overread_err)
lw	$25,%call16(av_log)($28)
lw	$4,0($fp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo(overread_err)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1069:
srl	$4,$8,3
andi	$8,$8,0x7
addu	$4,$7,$4
addiu	$3,$6,5
lbu	$2,0($4)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1086
sw	$3,316($sp)
.set	macro
.set	reorder

li	$8,2			# 0x2
li	$5,1			# 0x1
$L803:
srl	$2,$3,3
andi	$6,$3,0x7
addu	$2,$7,$2
addiu	$4,$3,1
lbu	$2,0($2)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1087
sw	$4,316($sp)
.set	macro
.set	reorder

$L804:
srl	$3,$4,3
andi	$6,$4,0x7
addu	$3,$7,$3
addiu	$2,$4,1
lbu	$3,0($3)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1088
sw	$2,316($sp)
.set	macro
.set	reorder

li	$10,1			# 0x1
$L807:
srl	$3,$2,3
andi	$4,$2,0x7
addu	$3,$7,$3
addiu	$6,$2,1
lbu	$3,0($3)
sll	$3,$3,$4
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L809
sw	$6,316($sp)
.set	macro
.set	reorder

srl	$3,$6,3
andi	$5,$6,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$19
and	$4,$4,$18
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
addiu	$6,$2,9
or	$3,$4,$3
sll	$2,$3,$5
sw	$6,316($sp)
move	$5,$8
srl	$2,$2,25
sw	$2,524($fp)
$L809:
sll	$8,$10,3
addiu	$11,$fp,56
addu	$13,$8,$6
addiu	$9,$6,1
move	$12,$6
$L810:
srl	$14,$6,3
andi	$15,$6,0x7
addu	$14,$7,$14
srl	$3,$9,3
addiu	$11,$11,4
lbu	$2,0($14)
addu	$3,$7,$3
sw	$9,316($sp)
andi	$4,$9,0x7
addiu	$6,$6,8
sll	$2,$2,$15
addiu	$9,$9,8
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,-4($11)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sw	$6,316($sp)
srl	$2,$2,8
sll	$3,$3,8
and	$2,$2,$19
and	$3,$3,$18
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$4
srl	$2,$2,25
.set	noreorder
.set	nomacro
bne	$13,$6,$L810
sw	$2,64($11)
.set	macro
.set	reorder

addu	$5,$5,$10
subu	$23,$23,$5
.set	noreorder
.set	nomacro
bgtz	$23,$L909
addu	$4,$8,$12
.set	macro
.set	reorder

$L1074:
.option	pic0
.set	noreorder
.set	nomacro
j	$L1042
.option	pic2
lw	$16,332($sp)
.set	macro
.set	reorder

$L1088:
srl	$3,$2,3
andi	$2,$2,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$19
and	$5,$5,$18
or	$3,$3,$5
sll	$9,$3,16
srl	$3,$3,16
addiu	$6,$4,5
or	$3,$9,$3
sll	$5,$3,$2
srl	$2,$6,3
sw	$6,316($sp)
srl	$5,$5,28
addu	$2,$7,$2
andi	$6,$6,0x7
sw	$5,448($fp)
addiu	$4,$4,9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
sw	$4,316($sp)
and	$2,$2,$19
and	$3,$3,$18
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$10,$5,1
or	$2,$3,$2
sll	$2,$2,$6
addiu	$12,$fp,456
srl	$2,$2,28
move	$11,$0
move	$9,$4
sw	$2,452($fp)
$L808:
srl	$3,$9,3
andi	$6,$9,0x7
addu	$3,$7,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sll	$3,$3,8
srl	$2,$2,8
and	$3,$3,$18
and	$2,$2,$19
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$9,$9,8
or	$2,$3,$2
sll	$2,$2,$6
addiu	$11,$11,1
sw	$9,316($sp)
srl	$2,$2,24
addiu	$12,$12,4
slt	$3,$11,$10
.set	noreorder
.set	nomacro
bne	$3,$0,$L808
sw	$2,-4($12)
.set	macro
.set	reorder

sll	$2,$5,3
addiu	$8,$8,1
addu	$4,$2,$4
addu	$5,$8,$5
addiu	$2,$4,8
.option	pic0
.set	noreorder
.set	nomacro
j	$L807
.option	pic2
addiu	$8,$5,1
.set	macro
.set	reorder

$L1051:
sll	$16,$6,4
addu	$2,$fp,$16
addu	$2,$2,$18
lbu	$2,1296($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L700
lui	$19,%hi($LC30)
.set	macro
.set	reorder

move	$2,$0
addiu	$19,$19,%lo($LC30)
li	$20,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
j	$L703
.option	pic2
move	$17,$18
.set	macro
.set	reorder

$L892:
$L701:
addiu	$17,$17,1
addu	$3,$17,$16
addu	$3,$fp,$3
lbu	$3,1296($3)
beq	$3,$0,$L702
$L1089:
.set	noreorder
.set	nomacro
beq	$17,$20,$L1105
lui	$6,%hi($LC31)
.set	macro
.set	reorder

$L703:
lw	$3,10512($fp)
sltu	$3,$3,4
beq	$3,$0,$L701
.set	noreorder
.set	nomacro
bne	$2,$0,$L892
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$25,%call16(av_log)($28)
li	$5,24			# 0x18
lw	$4,0($fp)
addiu	$17,$17,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$6,$19
.set	macro
.set	reorder

addu	$3,$17,$16
lw	$28,24($sp)
addu	$3,$fp,$3
lbu	$3,1296($3)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1089
li	$2,1			# 0x1
.set	macro
.set	reorder

$L702:
li	$2,16			# 0x10
bne	$17,$2,$L887
$L704:
lui	$6,%hi($LC31)
$L1105:
lw	$4,0($fp)
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
lw	$7,336($sp)
addiu	$6,$6,%lo($LC31)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$18,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1015
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L700:
move	$17,$18
$L887:
addu	$7,$17,$16
addu	$2,$fp,$16
sll	$7,$7,2
li	$3,1			# 0x1
addu	$2,$2,$17
addu	$7,$fp,$7
sb	$3,1296($2)
lw	$16,1040($7)
.set	noreorder
.set	nomacro
beq	$16,$0,$L1090
lui	$3,%hi(tags_per_config)
.set	macro
.set	reorder

$L893:
li	$3,1024			# 0x400
.option	pic0
.set	noreorder
.set	nomacro
j	$L699
.option	pic2
sw	$3,368($sp)
.set	macro
.set	reorder

$L1090:
lw	$6,16($fp)
lw	$2,1360($fp)
addiu	$3,$3,%lo(tags_per_config)
addu	$3,$6,$3
lb	$3,0($3)
slt	$4,$2,$3
.set	noreorder
.set	nomacro
beq	$4,$0,$L704
sltu	$4,$6,8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L704
lui	$4,%hi($L708)
.set	macro
.set	reorder

sll	$5,$6,2
addiu	$4,$4,%lo($L708)
addu	$4,$4,$5
lw	$4,0($4)
j	$4
.rdata
.align	2
.align	2
$L708:
.word	$L704
.word	$L707
.word	$L709
.word	$L709
.word	$L710
.word	$L711
.word	$L712
.word	$L713
.text
$L1086:
srl	$4,$3,3
andi	$5,$3,0x7
addu	$3,$7,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$3,$4,8
and	$2,$2,$19
and	$3,$3,$18
or	$2,$2,$3
sll	$4,$2,16
srl	$2,$2,16
addiu	$3,$6,13
or	$2,$4,$2
sll	$2,$2,$5
sw	$3,316($sp)
li	$8,3			# 0x3
srl	$2,$2,28
li	$5,2			# 0x2
.option	pic0
.set	noreorder
.set	nomacro
j	$L803
.option	pic2
sw	$2,52($fp)
.set	macro
.set	reorder

$L838:
addu	$2,$16,$20
li	$5,3			# 0x3
lw	$2,10128($2)
.set	noreorder
.set	nomacro
bne	$2,$5,$L882
addiu	$5,$16,144
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
jal	imdct_and_windowing
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1104
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

$L1076:
li	$2,37904			# 0x9410
move	$fp,$18
lw	$18,364($sp)
li	$9,1			# 0x1
addu	$2,$16,$2
sw	$16,364($sp)
move	$19,$18
.option	pic0
.set	noreorder
.set	nomacro
j	$L821
.option	pic2
sw	$2,356($sp)
.set	macro
.set	reorder

$L831:
lw	$2,332($sp)
$L1117:
addiu	$19,$19,4
.set	noreorder
.set	nomacro
beq	$2,$19,$L1091
lw	$23,352($sp)
.set	macro
.set	reorder

$L821:
lw	$23,0($19)
.set	noreorder
.set	nomacro
beq	$23,$0,$L831
addu	$18,$23,$20
.set	macro
.set	reorder

lw	$3,10128($18)
.set	noreorder
.set	nomacro
bne	$3,$9,$L1117
lw	$2,332($sp)
.set	macro
.set	reorder

lw	$5,10132($18)
.set	noreorder
.set	nomacro
bltz	$5,$L1117
li	$3,65536			# 0x10000
.set	macro
.set	reorder

move	$17,$0
ori	$3,$3,0x27d8
addu	$16,$23,$3
lw	$3,348($sp)
lw	$2,-64($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L1092
move	$21,$0
.set	macro
.set	reorder

$L832:
lw	$2,0($16)
move	$4,$9
xori	$2,$2,0x3
movz	$4,$22,$2
addu	$17,$17,$4
$L835:
addiu	$21,$21,1
slt	$2,$5,$21
.set	noreorder
.set	nomacro
bne	$2,$0,$L831
addiu	$16,$16,4
.set	macro
.set	reorder

lw	$2,-64($16)
lw	$3,348($sp)
bne	$3,$2,$L832
$L1092:
lw	$2,-32($16)
lw	$3,340($sp)
bne	$3,$2,$L832
lw	$2,0($16)
.set	noreorder
.set	nomacro
beq	$2,$9,$L834
move	$4,$fp
.set	macro
.set	reorder

lw	$5,352($sp)
move	$6,$23
sw	$9,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$2,0($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L834
lw	$9,396($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$22,$L834
addiu	$17,$17,1
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L835
.option	pic2
lw	$5,10132($18)
.set	macro
.set	reorder

$L1091:
move	$4,$fp
lw	$21,348($sp)
lw	$16,364($sp)
move	$5,$23
.option	pic0
.set	noreorder
.set	nomacro
jal	imdct_and_windowing
.option	pic2
lw	$19,340($sp)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$21,$2,$L839
lw	$28,24($sp)
.set	macro
.set	reorder

li	$5,37904			# 0x9410
move	$4,$fp
.option	pic0
.set	noreorder
.set	nomacro
jal	imdct_and_windowing
.option	pic2
addu	$5,$16,$5
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1104
.option	pic2
move	$4,$fp
.set	macro
.set	reorder

$L834:
addiu	$2,$17,1
lw	$5,356($sp)
move	$7,$17
sw	$9,396($sp)
move	$4,$fp
move	$6,$23
.option	pic0
.set	noreorder
.set	nomacro
jal	apply_dependent_coupling
.option	pic2
sw	$2,400($sp)
.set	macro
.set	reorder

lw	$2,400($sp)
lw	$5,10132($18)
lw	$9,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L835
.option	pic2
move	$17,$2
.set	macro
.set	reorder

$L1087:
addiu	$17,$3,7
addiu	$16,$3,6
addiu	$15,$3,5
addiu	$14,$3,4
addiu	$2,$3,3
addiu	$21,$3,2
addiu	$12,$fp,192
addiu	$3,$3,8
.option	pic0
.set	noreorder
.set	nomacro
j	$L806
.option	pic2
move	$13,$0
.set	macro
.set	reorder

$L1093:
srl	$6,$3,3
andi	$8,$3,0x7
addu	$6,$7,$6
addiu	$4,$25,8
addiu	$12,$12,28
lbu	$6,0($6)
addiu	$21,$21,8
sw	$4,316($sp)
addiu	$2,$2,8
addiu	$3,$3,8
sll	$6,$6,$8
addiu	$14,$14,8
andi	$6,$6,0x00ff
srl	$6,$6,7
addiu	$15,$15,8
addiu	$16,$16,8
.set	noreorder
.set	nomacro
beq	$6,$0,$L805
addiu	$17,$17,8
.set	macro
.set	reorder

$L806:
srl	$6,$4,3
andi	$8,$4,0x7
addu	$6,$7,$6
srl	$10,$21,3
andi	$9,$21,0x7
lbu	$6,0($6)
addu	$10,$7,$10
sw	$21,316($sp)
srl	$11,$2,3
andi	$25,$2,0x7
sll	$8,$6,$8
addu	$6,$7,$11
andi	$8,$8,0x00ff
srl	$8,$8,7
srl	$11,$14,3
andi	$24,$14,0x7
sw	$8,0($12)
addu	$11,$7,$11
lbu	$8,0($10)
srl	$10,$15,3
sw	$2,316($sp)
andi	$22,$15,0x7
addu	$10,$7,$10
sll	$8,$8,$9
srl	$9,$16,3
andi	$8,$8,0x00ff
srl	$8,$8,7
addu	$9,$7,$9
andi	$20,$16,0x7
sw	$8,4($12)
srl	$8,$17,3
lbu	$6,0($6)
addiu	$13,$13,7
sw	$14,316($sp)
addu	$8,$7,$8
sll	$6,$6,$25
move	$25,$4
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,8($12)
lbu	$6,0($11)
sw	$15,316($sp)
sll	$6,$6,$24
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,12($12)
lbu	$6,0($10)
sw	$16,316($sp)
sll	$6,$6,$22
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,16($12)
lbu	$6,0($9)
sw	$17,316($sp)
sll	$6,$6,$20
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,20($12)
lbu	$6,0($8)
andi	$8,$17,0x7
sw	$3,316($sp)
sll	$6,$6,$8
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,24($12)
li	$6,63			# 0x3f
.set	noreorder
.set	nomacro
bne	$13,$6,$L1093
move	$4,$3
.set	macro
.set	reorder

$L805:
li	$2,-1840709632			# 0xffffffff92490000
sra	$3,$13,31
addiu	$2,$2,9363
mult	$13,$2
mfhi	$2
addu	$2,$2,$13
sra	$2,$2,2
subu	$2,$2,$3
addu	$5,$5,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L804
.option	pic2
addiu	$8,$5,1
.set	macro
.set	reorder

$L1066:
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$5,$4,$L1094
li	$4,4			# 0x4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$4,$L1108
li	$3,2			# 0x2
.set	macro
.set	reorder

lw	$4,336($sp)
$L1110:
li	$3,3			# 0x3
beq	$4,$3,$L716
.set	noreorder
.set	nomacro
bne	$4,$0,$L1118
li	$3,2			# 0x2
.set	macro
.set	reorder

$L716:
lw	$16,976($fp)
addiu	$2,$2,1
sw	$2,1360($fp)
.set	noreorder
.set	nomacro
bne	$16,$0,$L893
sw	$16,1040($7)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1105
.option	pic2
lui	$6,%hi($LC31)
.set	macro
.set	reorder

$L1067:
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$4,$3,$L1095
li	$3,4			# 0x4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$6,$3,$L1108
li	$3,2			# 0x2
.set	macro
.set	reorder

$L719:
lw	$3,336($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1105
lui	$6,%hi($LC31)
.set	macro
.set	reorder

addiu	$2,$17,260
lw	$16,788($fp)
li	$3,3			# 0x3
sll	$2,$2,2
addu	$2,$fp,$2
sw	$3,1360($fp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L722
.option	pic2
sw	$16,0($2)
.set	macro
.set	reorder

$L1094:
addiu	$2,$17,276
lw	$16,856($fp)
li	$3,4			# 0x4
sll	$2,$2,2
addu	$2,$fp,$2
sw	$3,1360($fp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L715
.option	pic2
sw	$16,0($2)
.set	macro
.set	reorder

$L1095:
addiu	$2,$17,276
lw	$16,852($fp)
li	$3,3			# 0x3
sll	$2,$2,2
addu	$2,$fp,$2
sw	$3,1360($fp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L715
.option	pic2
sw	$16,0($2)
.set	macro
.set	reorder

.end	aac_decode_frame
.size	aac_decode_frame, .-aac_decode_frame
.local	table.6938
.comm	table.6938,1408,4
.local	table.6937
.comm	table.6937,1848,4
.local	table.6936
.comm	table.6936,1464,4
.local	table.6935
.comm	table.6935,2040,4
.local	table.6934
.comm	table.6934,1072,4
.local	table.6933
.comm	table.6933,1224,4
.local	table.6932
.comm	table.6932,1176,4
.local	table.6931
.comm	table.6931,1312,4
.local	table.6930
.comm	table.6930,1200,4
.local	table.6929
.comm	table.6929,2200,4
.local	table.6928
.comm	table.6928,1080,4
.local	table.6927
.comm	table.6927,1216,4
.globl	aac_decoder
.section	.rodata.str1.4
.align	2
$LC37:
.ascii	"aac\000"
.align	2
$LC38:
.ascii	"Advanced Audio Coding\000"
.section	.data.rel.local,"aw",@progbits
.align	2
.type	aac_decoder, @object
.size	aac_decoder, 72
aac_decoder:
.word	$LC37
.word	1
.word	86018
.word	10528
.word	aac_decode_init
.word	0
.word	aac_decode_close
.word	aac_decode_frame
.space	20
.word	$LC38
.space	4
.word	__compound_literal.0
.word	aac_channel_layout
.space	4
.rdata
.align	2
.type	__compound_literal.0, @object
.size	__compound_literal.0, 8
__compound_literal.0:
.word	1
.word	-1
.align	2
.type	cce_scale, @object
.size	cce_scale, 16
cce_scale:
.word	1066112450
.word	1066940400
.word	1068827891
.word	1073741824
.section	.data.rel.ro.local,"aw",@progbits
.align	2
.type	aac_tns_tmp2_map, @object
.size	aac_tns_tmp2_map, 16
aac_tns_tmp2_map:
.word	aac_tns_tmp2_map_0_3
.word	aac_tns_tmp2_map_0_4
.word	aac_tns_tmp2_map_1_3
.word	aac_tns_tmp2_map_1_4
.rdata
.align	2
.type	aac_tns_tmp2_map_0_4, @object
.size	aac_tns_tmp2_map_0_4, 64
aac_tns_tmp2_map_0_4:
.word	0
.word	-55810872
.word	-109182535
.word	-157782399
.word	-199486416
.word	-232471920
.word	-255297296
.word	-266964945
.word	267290353
.word	258188096
.word	240293569
.word	214216111
.word	180843791
.word	141313057
.word	96970072
.word	49324884
.align	2
.type	aac_tns_tmp2_map_1_4, @object
.size	aac_tns_tmp2_map_1_4, 32
aac_tns_tmp2_map_1_4:
.word	0
.word	-55810872
.word	-109182535
.word	-157782399
.word	180843791
.word	141313057
.word	96970072
.word	49324884
.align	2
.type	aac_tns_tmp2_map_0_3, @object
.size	aac_tns_tmp2_map_0_3, 32
aac_tns_tmp2_map_0_3:
.word	0
.word	-116469777
.word	-209871295
.word	-261705215
.word	264357312
.word	232471920
.word	172546977
.word	91810335
.align	2
.type	aac_tns_tmp2_map_1_3, @object
.size	aac_tns_tmp2_map_1_3, 16
aac_tns_tmp2_map_1_3:
.word	0
.word	-116469777
.word	172546977
.word	91810335
.globl	aac_codebook_vector_vals
.section	.data.rel.ro.local
.align	2
.type	aac_codebook_vector_vals, @object
.size	aac_codebook_vector_vals, 44
aac_codebook_vector_vals:
.word	aac_codebook_vector0_vals
.word	aac_codebook_vector0_vals
.word	aac_codebook_vector10_vals
.word	aac_codebook_vector10_vals
.word	aac_codebook_vector4_vals
.word	aac_codebook_vector4_vals
.word	aac_codebook_vector10_vals
.word	aac_codebook_vector10_vals
.word	aac_codebook_vector10_vals
.word	aac_codebook_vector10_vals
.word	aac_codebook_vector10_vals
.rdata
.align	2
.type	aac_codebook_vector10_vals, @object
.size	aac_codebook_vector10_vals, 64
aac_codebook_vector10_vals:
.word	0
.word	16384
.word	41285
.word	70889
.word	104032
.word	140081
.word	178630
.word	219390
.word	262144
.word	306721
.word	352983
.word	400815
.word	450120
.word	500815
.word	552829
.word	606096
.align	2
.type	aac_codebook_vector4_vals, @object
.size	aac_codebook_vector4_vals, 36
aac_codebook_vector4_vals:
.word	-104032
.word	-70889
.word	-41285
.word	-16384
.word	0
.word	16384
.word	41285
.word	70889
.word	104032
.align	2
.type	aac_codebook_vector0_vals, @object
.size	aac_codebook_vector0_vals, 12
aac_codebook_vector0_vals:
.word	-16384
.word	0
.word	16384
.align	2
.type	overread_err, @object
.size	overread_err, 49
overread_err:
.ascii	"Input buffer exhausted before END element found\012\000"
.local	vlc_spectral
.comm	vlc_spectral,176,4
.local	vlc_scalefactors
.comm	vlc_scalefactors,16,4

.comm	aac_bugs,4,4
.align	3
.type	aac_channel_layout, @object
.size	aac_channel_layout, 64
aac_channel_layout:
.word	4
.word	0
.word	3
.word	0
.word	7
.word	0
.word	263
.word	0
.word	55
.word	0
.word	63
.word	0
.word	255
.word	0
.word	0
.word	0
.align	2
.type	aac_channel_layout_map, @object
.size	aac_channel_layout_map, 70
aac_channel_layout_map:
.byte	0
.byte	0
.space	8
.byte	1
.byte	0
.space	8
.byte	1
.byte	0
.byte	0
.byte	0
.space	6
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.space	4
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.space	4
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	1
.space	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	2
.byte	1
.byte	1
.align	2
.type	tags_per_config, @object
.size	tags_per_config, 16
tags_per_config:
.byte	0
.byte	1
.byte	1
.byte	2
.byte	3
.byte	3
.byte	4
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	aac_sine_short_128, @object
.size	aac_sine_short_128, 512
aac_sine_short_128:
.word	6588356
.word	19764076
.word	32936818
.word	46104600
.word	59265444
.word	72417352
.word	85558368
.word	98686488
.word	111799752
.word	124896184
.word	137973792
.word	151030640
.word	164064736
.word	177074112
.word	190056832
.word	203010928
.word	215934448
.word	228825456
.word	241682016
.word	254502144
.word	267283984
.word	280025568
.word	292724960
.word	305380288
.word	317989568
.word	330551040
.word	343062688
.word	355522688
.word	367929152
.word	380280192
.word	392573952
.word	404808608
.word	416982304
.word	429093216
.word	441139520
.word	453119360
.word	465030944
.word	476872512
.word	488642272
.word	500338464
.word	511959296
.word	523503008
.word	534967872
.word	546352256
.word	557654272
.word	568872256
.word	580004736
.word	591049728
.word	602005824
.word	612871168
.word	623644288
.word	634323392
.word	644907008
.word	655393536
.word	665781376
.word	676068928
.word	686254656
.word	696337024
.word	706314560
.word	716185728
.word	725949056
.word	735603008
.word	745146176
.word	754577152
.word	763894464
.word	773096832
.word	782182656
.word	791150784
.word	799999680
.word	808728128
.word	817334848
.word	825818432
.word	834177664
.word	842411200
.word	850517952
.word	858496640
.word	866345920
.word	874064832
.word	881652096
.word	889106624
.word	896427200
.word	903612736
.word	910662272
.word	917574656
.word	924348800
.word	930983808
.word	937478592
.word	943832192
.word	950043648
.word	956112064
.word	962036480
.word	967815936
.word	973449728
.word	978936896
.word	984276608
.word	989468160
.word	994510656
.word	999403456
.word	1004145664
.word	1008736640
.word	1013175744
.word	1017462272
.word	1021595584
.word	1025575040
.word	1029400000
.word	1033070016
.word	1036584384
.word	1039942720
.word	1043144320
.word	1046188928
.word	1049075968
.word	1051804992
.word	1054375680
.word	1056787520
.word	1059040256
.word	1061133504
.word	1063066880
.word	1064840256
.word	1066453184
.word	1067905600
.word	1069197120
.word	1070327680
.word	1071296960
.word	1072104960
.word	1072751552
.word	1073236544
.word	1073559936
.word	1073721600
.align	2
.type	aac_sine_long_1024, @object
.size	aac_sine_long_1024, 4096
aac_sine_long_1024:
.word	823550
.word	2470647
.word	4117738
.word	5764820
.word	7411888
.word	9058939
.word	10705969
.word	12352972
.word	13999947
.word	15646890
.word	17293794
.word	18940660
.word	20587480
.word	22234250
.word	23880970
.word	25527634
.word	27174236
.word	28820776
.word	30467248
.word	32113646
.word	33759972
.word	35406216
.word	37052380
.word	38698452
.word	40344432
.word	41990320
.word	43636112
.word	45281800
.word	46927380
.word	48572852
.word	50218204
.word	51863440
.word	53508556
.word	55153544
.word	56798408
.word	58443132
.word	60087716
.word	61732164
.word	63376468
.word	65020620
.word	66664620
.word	68308464
.word	69952144
.word	71595664
.word	73239008
.word	74882184
.word	76525192
.word	78168008
.word	79810648
.word	81453088
.word	83095352
.word	84737408
.word	86379272
.word	88020936
.word	89662384
.word	91303624
.word	92944648
.word	94585448
.word	96226040
.word	97866392
.word	99506528
.word	101146416
.word	102786080
.word	104425488
.word	106064656
.word	107703576
.word	109342240
.word	110980648
.word	112618792
.word	114256672
.word	115894288
.word	117531624
.word	119168696
.word	120805472
.word	122441976
.word	124078184
.word	125714096
.word	127349728
.word	128985048
.word	130620072
.word	132254776
.word	133889176
.word	135523264
.word	137157040
.word	138790480
.word	140423600
.word	142056384
.word	143688832
.word	145320944
.word	146952720
.word	148584160
.word	150215216
.word	151845952
.word	153476320
.word	155106320
.word	156735968
.word	158365232
.word	159994144
.word	161622656
.word	163250800
.word	164878560
.word	166505920
.word	168132912
.word	169759488
.word	171385664
.word	173011456
.word	174636832
.word	176261792
.word	177886336
.word	179510464
.word	181134176
.word	182757440
.word	184380304
.word	186002720
.word	187624704
.word	189246224
.word	190867328
.word	192487968
.word	194108144
.word	195727888
.word	197347168
.word	198965984
.word	200584304
.word	202202176
.word	203819568
.word	205436480
.word	207052912
.word	208668848
.word	210284288
.word	211899248
.word	213513712
.word	215127680
.word	216741120
.word	218354064
.word	219966480
.word	221578384
.word	223189776
.word	224800640
.word	226410976
.word	228020768
.word	229630032
.word	231238752
.word	232846928
.word	234454560
.word	236061632
.word	237668160
.word	239274112
.word	240879520
.word	242484368
.word	244088608
.word	245692304
.word	247295424
.word	248897936
.word	250499888
.word	252101248
.word	253702016
.word	255302160
.word	256901728
.word	258500688
.word	260099024
.word	261696768
.word	263293904
.word	264890400
.word	266486272
.word	268081536
.word	269676160
.word	271270144
.word	272863488
.word	274456192
.word	276048256
.word	277639648
.word	279230432
.word	280820512
.word	282409984
.word	283998752
.word	285586816
.word	287174272
.word	288761024
.word	290347104
.word	291932512
.word	293517216
.word	295101248
.word	296684544
.word	298267168
.word	299849120
.word	301430336
.word	303010848
.word	304590656
.word	306169696
.word	307748064
.word	309325696
.word	310902624
.word	312478784
.word	314054240
.word	315628960
.word	317202880
.word	318776096
.word	320348576
.word	321920288
.word	323491232
.word	325061440
.word	326630848
.word	328199488
.word	329767360
.word	331334496
.word	332900832
.word	334466368
.word	336031136
.word	337595104
.word	339158240
.word	340720640
.word	342282208
.word	343842976
.word	345402944
.word	346962080
.word	348520416
.word	350077920
.word	351634592
.word	353190464
.word	354745504
.word	356299680
.word	357853056
.word	359405568
.word	360957184
.word	362508032
.word	364057984
.word	365607104
.word	367155360
.word	368702752
.word	370249248
.word	371794880
.word	373339648
.word	374883552
.word	376426560
.word	377968672
.word	379509920
.word	381050240
.word	382589696
.word	384128224
.word	385665888
.word	387202624
.word	388738432
.word	390273344
.word	391807296
.word	393340384
.word	394872512
.word	396403744
.word	397934016
.word	399463360
.word	400991776
.word	402519200
.word	404045728
.word	405571296
.word	407095904
.word	408619552
.word	410142240
.word	411663968
.word	413184672
.word	414704480
.word	416223296
.word	417741120
.word	419257952
.word	420773824
.word	422288672
.word	423802528
.word	425315424
.word	426827296
.word	428338176
.word	429848032
.word	431356896
.word	432864736
.word	434371520
.word	435877312
.word	437382080
.word	438885824
.word	440388544
.word	441890208
.word	443390816
.word	444890400
.word	446388960
.word	447886432
.word	449382880
.word	450878272
.word	452372576
.word	453865824
.word	455358016
.word	456849120
.word	458339168
.word	459828128
.word	461316032
.word	462802816
.word	464288480
.word	465773120
.word	467256640
.word	468739040
.word	470220352
.word	471700576
.word	473179680
.word	474657632
.word	476134496
.word	477610240
.word	479084864
.word	480558368
.word	482030752
.word	483501984
.word	484972032
.word	486440992
.word	487908800
.word	489375456
.word	490840992
.word	492305344
.word	493768544
.word	495230528
.word	496691392
.word	498151104
.word	499609632
.word	501066976
.word	502523168
.word	503978144
.word	505431936
.word	506884544
.word	508335968
.word	509786208
.word	511235232
.word	512683040
.word	514129632
.word	515575040
.word	517019232
.word	518462208
.word	519903968
.word	521344512
.word	522783808
.word	524221888
.word	525658752
.word	527094336
.word	528528704
.word	529961792
.word	531393664
.word	532824288
.word	534253664
.word	535681760
.word	537108608
.word	538534208
.word	539958528
.word	541381568
.word	542803392
.word	544223872
.word	545643072
.word	547061056
.word	548477632
.word	549892992
.word	551307072
.word	552719808
.word	554131264
.word	555541440
.word	556950336
.word	558357888
.word	559764096
.word	561169024
.word	562572608
.word	563974848
.word	565375808
.word	566775424
.word	568173632
.word	569570560
.word	570966208
.word	572360448
.word	573753344
.word	575144896
.word	576535104
.word	577923968
.word	579311488
.word	580697600
.word	582082368
.word	583465728
.word	584847808
.word	586228416
.word	587607616
.word	588985536
.word	590361984
.word	591737088
.word	593110848
.word	594483136
.word	595854080
.word	597223552
.word	598591680
.word	599958400
.word	601323712
.word	602687552
.word	604050048
.word	605411072
.word	606770624
.word	608128768
.word	609485568
.word	610840896
.word	612194752
.word	613547200
.word	614898176
.word	616247744
.word	617595840
.word	618942464
.word	620287680
.word	621631424
.word	622973696
.word	624314432
.word	625653760
.word	626991616
.word	628328000
.word	629662912
.word	630996352
.word	632328256
.word	633658752
.word	634987712
.word	636315136
.word	637641152
.word	638965632
.word	640288576
.word	641610048
.word	642929920
.word	644248384
.word	645565312
.word	646880704
.word	648194560
.word	649506944
.word	650817728
.word	652127040
.word	653434816
.word	654741056
.word	656045696
.word	657348864
.word	658650432
.word	659950464
.word	661248896
.word	662545856
.word	663841216
.word	665135040
.word	666427264
.word	667717952
.word	669007040
.word	670294592
.word	671580544
.word	672864896
.word	674147712
.word	675428928
.word	676708544
.word	677986496
.word	679262912
.word	680537792
.word	681811008
.word	683082624
.word	684352640
.word	685621056
.word	686887872
.word	688153024
.word	689416576
.word	690678528
.word	691938816
.word	693197504
.word	694454528
.word	695709952
.word	696963712
.word	698215808
.word	699466304
.word	700715200
.word	701962368
.word	703207936
.word	704451840
.word	705694080
.word	706934656
.word	708173568
.word	709410816
.word	710646400
.word	711880320
.word	713112448
.word	714343040
.word	715571904
.word	716799104
.word	718024576
.word	719248384
.word	720470528
.word	721690944
.word	722909632
.word	724126656
.word	725342016
.word	726555648
.word	727767552
.word	728977664
.word	730186176
.word	731392896
.word	732597952
.word	733801280
.word	735002816
.word	736202688
.word	737400832
.word	738597184
.word	739791872
.word	740984768
.word	742175936
.word	743365376
.word	744553024
.word	745738880
.word	746923072
.word	748105472
.word	749286144
.word	750465024
.word	751642112
.word	752817472
.word	753991040
.word	755162816
.word	756332864
.word	757501120
.word	758667584
.word	759832256
.word	760995136
.word	762156224
.word	763315520
.word	764473024
.word	765628736
.word	766782656
.word	767934720
.word	769085056
.word	770233536
.word	771380224
.word	772525056
.word	773668096
.word	774809344
.word	775948736
.word	777086336
.word	778222016
.word	779355904
.word	780488000
.word	781618240
.word	782746624
.word	783873216
.word	784997952
.word	786120768
.word	787241792
.word	788360960
.word	789478272
.word	790593728
.word	791707328
.word	792819008
.word	793928896
.word	795036928
.word	796143040
.word	797247296
.word	798349632
.word	799450176
.word	800548800
.word	801645504
.word	802740352
.word	803833344
.word	804924416
.word	806013568
.word	807100864
.word	808186176
.word	809269632
.word	810351232
.word	811430912
.word	812508672
.word	813584512
.word	814658432
.word	815730432
.word	816800512
.word	817868672
.word	818934912
.word	819999232
.word	821061632
.word	822122112
.word	823180608
.word	824237184
.word	825291840
.word	826344512
.word	827395264
.word	828444096
.word	829490944
.word	830535872
.word	831578816
.word	832619840
.word	833658880
.word	834695936
.word	835731072
.word	836764160
.word	837795328
.word	838824576
.word	839851776
.word	840877056
.word	841900352
.word	842921600
.word	843940928
.word	844958272
.word	845973632
.word	846986944
.word	847998336
.word	849007680
.word	850015040
.word	851020352
.word	852023744
.word	853025088
.word	854024448
.word	855021760
.word	856017088
.word	857010432
.word	858001728
.word	858990976
.word	859978240
.word	860963456
.word	861946688
.word	862927872
.word	863907008
.word	864884096
.word	865859200
.word	866832192
.word	867803200
.word	868772160
.word	869739072
.word	870703936
.word	871666816
.word	872627584
.word	873586304
.word	874542912
.word	875497536
.word	876450112
.word	877400512
.word	878348928
.word	879295296
.word	880239552
.word	881181760
.word	882121920
.word	883059968
.word	883995968
.word	884929856
.word	885861696
.word	886791424
.word	887719040
.word	888644608
.word	889568064
.word	890489408
.word	891408704
.word	892325888
.word	893240960
.word	894153920
.word	895064832
.word	895973568
.word	896880256
.word	897784832
.word	898687232
.word	899587584
.word	900485760
.word	901381888
.word	902275840
.word	903167616
.word	904057344
.word	904944960
.word	905830400
.word	906713728
.word	907594944
.word	908473984
.word	909350912
.word	910225728
.word	911098368
.word	911968832
.word	912837184
.word	913703424
.word	914567424
.word	915429376
.word	916289088
.word	917146688
.word	918002112
.word	918855424
.word	919706496
.word	920555392
.word	921402176
.word	922246784
.word	923089216
.word	923929472
.word	924767616
.word	925603520
.word	926437248
.word	927268800
.word	928098176
.word	928925376
.word	929750400
.word	930573248
.word	931393856
.word	932212288
.word	933028544
.word	933842624
.word	934654464
.word	935464128
.word	936271552
.word	937076800
.word	937879872
.word	938680704
.word	939479296
.word	940275776
.word	941069952
.word	941861952
.word	942651712
.word	943439232
.word	944224576
.word	945007680
.word	945788544
.word	946567232
.word	947343680
.word	948117888
.word	948889856
.word	949659584
.word	950427136
.word	951192384
.word	951955456
.word	952716224
.word	953474752
.word	954231104
.word	954985152
.word	955736960
.word	956486528
.word	957233856
.word	957978944
.word	958721728
.word	959462272
.word	960200576
.word	960936640
.word	961670400
.word	962401920
.word	963131200
.word	963858112
.word	964582848
.word	965305280
.word	966025472
.word	966743360
.word	967459008
.word	968172352
.word	968883392
.word	969592192
.word	970298688
.word	971002944
.word	971704896
.word	972404544
.word	973101888
.word	973796992
.word	974489792
.word	975180288
.word	975868480
.word	976554368
.word	977237952
.word	977919296
.word	978598272
.word	979274944
.word	979949376
.word	980621440
.word	981291264
.word	981958720
.word	982623872
.word	983286656
.word	983947200
.word	984605440
.word	985261376
.word	985914944
.word	986566208
.word	987215168
.word	987861760
.word	988506048
.word	989148032
.word	989787712
.word	990425024
.word	991059968
.word	991692672
.word	992322944
.word	992950976
.word	993576576
.word	994199936
.word	994820864
.word	995439488
.word	996055808
.word	996669760
.word	997281344
.word	997890560
.word	998497472
.word	999102016
.word	999704256
.word	1000304064
.word	1000901568
.word	1001496704
.word	1002089472
.word	1002679872
.word	1003267968
.word	1003853696
.word	1004436992
.word	1005017984
.word	1005596608
.word	1006172864
.word	1006746752
.word	1007318272
.word	1007887424
.word	1008454144
.word	1009018560
.word	1009580608
.word	1010140224
.word	1010697536
.word	1011252416
.word	1011804928
.word	1012355072
.word	1012902784
.word	1013448192
.word	1013991168
.word	1014531712
.word	1015069952
.word	1015605696
.word	1016139136
.word	1016670208
.word	1017198848
.word	1017725120
.word	1018248960
.word	1018770432
.word	1019289472
.word	1019806144
.word	1020320384
.word	1020832256
.word	1021341760
.word	1021848832
.word	1022353472
.word	1022855680
.word	1023355520
.word	1023852992
.word	1024348032
.word	1024840640
.word	1025330816
.word	1025818624
.word	1026304000
.word	1026786944
.word	1027267520
.word	1027745664
.word	1028221376
.word	1028694656
.word	1029165504
.word	1029633920
.word	1030099968
.word	1030563520
.word	1031024704
.word	1031483456
.word	1031939776
.word	1032393728
.word	1032845184
.word	1033294208
.word	1033740800
.word	1034184960
.word	1034626688
.word	1035066048
.word	1035502912
.word	1035937344
.word	1036369280
.word	1036798848
.word	1037225984
.word	1037650624
.word	1038072896
.word	1038492672
.word	1038910016
.word	1039324928
.word	1039737408
.word	1040147392
.word	1040554944
.word	1040960064
.word	1041362688
.word	1041762944
.word	1042160704
.word	1042555968
.word	1042948864
.word	1043339264
.word	1043727168
.word	1044112704
.word	1044495744
.word	1044876288
.word	1045254400
.word	1045630080
.word	1046003264
.word	1046374016
.word	1046742272
.word	1047108096
.word	1047471424
.word	1047832320
.word	1048190720
.word	1048546688
.word	1048900160
.word	1049251200
.word	1049599744
.word	1049945792
.word	1050289408
.word	1050630528
.word	1050969216
.word	1051305408
.word	1051639104
.word	1051970304
.word	1052299072
.word	1052625408
.word	1052949184
.word	1053270528
.word	1053589376
.word	1053905728
.word	1054219648
.word	1054531072
.word	1054840000
.word	1055146432
.word	1055450432
.word	1055751936
.word	1056050944
.word	1056347456
.word	1056641472
.word	1056932992
.word	1057222080
.word	1057508608
.word	1057792704
.word	1058074304
.word	1058353408
.word	1058630016
.word	1058904128
.word	1059175744
.word	1059444928
.word	1059711552
.word	1059975680
.word	1060237312
.word	1060496512
.word	1060753152
.word	1061007360
.word	1061259008
.word	1061508160
.word	1061754880
.word	1061999040
.word	1062240704
.word	1062479872
.word	1062716608
.word	1062950784
.word	1063182464
.word	1063411584
.word	1063638272
.word	1063862464
.word	1064084096
.word	1064303296
.word	1064519936
.word	1064734080
.word	1064945728
.word	1065154880
.word	1065361536
.word	1065565696
.word	1065767296
.word	1065966400
.word	1066163008
.word	1066357120
.word	1066548672
.word	1066737792
.word	1066924352
.word	1067108416
.word	1067289920
.word	1067468992
.word	1067645504
.word	1067819520
.word	1067991040
.word	1068160000
.word	1068326464
.word	1068490432
.word	1068651840
.word	1068810816
.word	1068967232
.word	1069121088
.word	1069272512
.word	1069421376
.word	1069567744
.word	1069711552
.word	1069852864
.word	1069991680
.word	1070127936
.word	1070261696
.word	1070392960
.word	1070521664
.word	1070647872
.word	1070771584
.word	1070892736
.word	1071011392
.word	1071127488
.word	1071241152
.word	1071352192
.word	1071460800
.word	1071566848
.word	1071670336
.word	1071771328
.word	1071869824
.word	1071965760
.word	1072059200
.word	1072150144
.word	1072238528
.word	1072324416
.word	1072407744
.word	1072488576
.word	1072566848
.word	1072642624
.word	1072715840
.word	1072786560
.word	1072854784
.word	1072920448
.word	1072983616
.word	1073044224
.word	1073102336
.word	1073157888
.word	1073210944
.word	1073261504
.word	1073309504
.word	1073354944
.word	1073397888
.word	1073438336
.word	1073476224
.word	1073511616
.word	1073544448
.word	1073574784
.word	1073602560
.word	1073627840
.word	1073650560
.word	1073670784
.word	1073688448
.word	1073703616
.word	1073716224
.word	1073726336
.word	1073733952
.word	1073739008
.word	1073741504
.align	2
.type	aac_pow2sf_tab, @object
.size	aac_pow2sf_tab, 1712
aac_pow2sf_tab:
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	2
.word	2
.word	2
.word	3
.word	3
.word	4
.word	5
.word	6
.word	7
.word	8
.word	10
.word	11
.word	13
.word	16
.word	19
.word	23
.word	27
.word	32
.word	38
.word	45
.word	54
.word	64
.word	76
.word	91
.word	108
.word	128
.word	152
.word	181
.word	215
.word	256
.word	304
.word	362
.word	431
.word	512
.word	609
.word	724
.word	861
.word	1024
.word	1218
.word	1448
.word	1722
.word	2048
.word	2435
.word	2896
.word	3444
.word	4096
.word	4871
.word	5793
.word	6889
.word	8192
.word	9742
.word	11585
.word	13777
.word	16384
.word	19484
.word	23170
.word	27554
.word	32768
.word	38968
.word	46341
.word	55109
.word	65536
.word	77936
.word	92682
.word	110218
.word	131072
.word	155872
.word	185364
.word	220436
.word	262144
.word	311744
.word	370728
.word	440872
.word	524288
.word	623487
.word	741455
.word	881744
.word	1048576
.word	1246974
.word	1482910
.word	1763488
.word	2097152
.word	2493948
.word	2965821
.word	3526975
.word	4194304
.word	4987896
.word	5931642
.word	7053951
.word	8388608
.word	9975792
.word	11863283
.word	14107901
.word	16777216
.word	19951584
.word	23726566
.word	28215802
.word	33554432
.word	39903168
.word	47453132
.word	56431604
.word	67108864
.word	79806336
.word	94906264
.word	112863208
.word	134217728
.word	159612672
.word	189812528
.word	225726416
.word	268435456
.word	319225344
.word	379625056
.word	451452832
.word	536870912
.word	638450688
.word	759250112
.word	902905664
.word	1073741824
.word	1276901376
.word	1518500224
.word	1805811328
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.align	2
.type	aac_cbrt_tab, @object
.size	aac_cbrt_tab, 32768
aac_cbrt_tab:
.word	0
.word	16384
.word	41285
.word	70889
.word	104032
.word	140081
.word	178630
.word	219390
.word	262144
.word	306721
.word	352983
.word	400815
.word	450120
.word	500815
.word	552829
.word	606096
.word	660562
.word	716174
.word	772888
.word	830663
.word	889460
.word	949247
.word	1009990
.word	1071660
.word	1134231
.word	1197678
.word	1261976
.word	1327104
.word	1393041
.word	1459768
.word	1527267
.word	1595520
.word	1664511
.word	1734224
.word	1804645
.word	1875760
.word	1947556
.word	2020020
.word	2093139
.word	2166903
.word	2241300
.word	2316319
.word	2391951
.word	2468186
.word	2545014
.word	2622426
.word	2700414
.word	2778969
.word	2858084
.word	2937749
.word	3017959
.word	3098705
.word	3179980
.word	3261778
.word	3344093
.word	3426917
.word	3510244
.word	3594069
.word	3678386
.word	3763188
.word	3848472
.word	3934230
.word	4020458
.word	4107151
.word	4194304
.word	4281912
.word	4369971
.word	4458475
.word	4547421
.word	4636804
.word	4726620
.word	4816864
.word	4907534
.word	4998624
.word	5090131
.word	5182051
.word	5274380
.word	5367115
.word	5460253
.word	5553789
.word	5647722
.word	5742046
.word	5836759
.word	5931858
.word	6027340
.word	6123201
.word	6219439
.word	6316051
.word	6413034
.word	6510384
.word	6608100
.word	6706179
.word	6804618
.word	6903413
.word	7002564
.word	7102067
.word	7201920
.word	7302119
.word	7402664
.word	7503552
.word	7604779
.word	7706345
.word	7808247
.word	7910482
.word	8013048
.word	8115944
.word	8219166
.word	8322714
.word	8426585
.word	8530777
.word	8635289
.word	8740117
.word	8845261
.word	8950718
.word	9056486
.word	9162565
.word	9268951
.word	9375644
.word	9482640
.word	9589940
.word	9697540
.word	9805440
.word	9913638
.word	10022131
.word	10130919
.word	10240000
.word	10349372
.word	10459034
.word	10568984
.word	10679220
.word	10789742
.word	10900548
.word	11011636
.word	11123005
.word	11234653
.word	11346580
.word	11458783
.word	11571261
.word	11684014
.word	11797039
.word	11910335
.word	12023902
.word	12137737
.word	12251840
.word	12366209
.word	12480844
.word	12595742
.word	12710902
.word	12826325
.word	12942007
.word	13057948
.word	13174148
.word	13290604
.word	13407316
.word	13524282
.word	13641502
.word	13758975
.word	13876698
.word	13994672
.word	14112895
.word	14231366
.word	14350084
.word	14469048
.word	14588257
.word	14707710
.word	14827407
.word	14947345
.word	15067524
.word	15187944
.word	15308602
.word	15429499
.word	15550634
.word	15672004
.word	15793610
.word	15915451
.word	16037525
.word	16159832
.word	16282370
.word	16405140
.word	16528140
.word	16651369
.word	16774827
.word	16898512
.word	17022424
.word	17146562
.word	17270924
.word	17395512
.word	17520322
.word	17645356
.word	17770610
.word	17896086
.word	18021784
.word	18147700
.word	18273834
.word	18400188
.word	18526758
.word	18653544
.word	18780548
.word	18907764
.word	19035196
.word	19162842
.word	19290702
.word	19418772
.word	19547054
.word	19675548
.word	19804250
.word	19933164
.word	20062286
.word	20191614
.word	20321152
.word	20450896
.word	20580846
.word	20711000
.word	20841362
.word	20971926
.word	21102694
.word	21233664
.word	21364838
.word	21496212
.word	21627788
.word	21759564
.word	21891540
.word	22023714
.word	22156088
.word	22288660
.word	22421430
.word	22554396
.word	22687558
.word	22820916
.word	22954468
.word	23088216
.word	23222158
.word	23356294
.word	23490622
.word	23625142
.word	23759854
.word	23894756
.word	24029850
.word	24165134
.word	24300608
.word	24436270
.word	24572122
.word	24708162
.word	24844388
.word	24980802
.word	25117402
.word	25254188
.word	25391160
.word	25528316
.word	25665658
.word	25803184
.word	25940892
.word	26078784
.word	26216858
.word	26355114
.word	26493552
.word	26632170
.word	26770970
.word	26909950
.word	27049108
.word	27188448
.word	27327964
.word	27467660
.word	27607534
.word	27747584
.word	27887812
.word	28028216
.word	28168796
.word	28309552
.word	28450484
.word	28591588
.word	28732870
.word	28874324
.word	29015950
.word	29157750
.word	29299724
.word	29441870
.word	29584186
.word	29726674
.word	29869334
.word	30012164
.word	30155164
.word	30298334
.word	30441674
.word	30585182
.word	30728858
.word	30872702
.word	31016716
.word	31160894
.word	31305242
.word	31449756
.word	31594434
.word	31739280
.word	31884292
.word	32029468
.word	32174808
.word	32320312
.word	32465982
.word	32611814
.word	32757810
.word	32903968
.word	33050290
.word	33196772
.word	33343418
.word	33490224
.word	33637192
.word	33784320
.word	33931608
.word	34079056
.word	34226664
.word	34374432
.word	34522360
.word	34670444
.word	34818688
.word	34967088
.word	35115648
.word	35264364
.word	35413236
.word	35562268
.word	35711452
.word	35860796
.word	36010292
.word	36159944
.word	36309752
.word	36459716
.word	36609832
.word	36760104
.word	36910528
.word	37061108
.word	37211840
.word	37362724
.word	37513760
.word	37664948
.word	37816288
.word	37967784
.word	38119424
.word	38271220
.word	38423164
.word	38575260
.word	38727508
.word	38879904
.word	39032448
.word	39185140
.word	39337984
.word	39490976
.word	39644116
.word	39797404
.word	39950840
.word	40104424
.word	40258152
.word	40412028
.word	40566052
.word	40720224
.word	40874540
.word	41029004
.word	41183608
.word	41338364
.word	41493260
.word	41648304
.word	41803488
.word	41958820
.word	42114296
.word	42269916
.word	42425676
.word	42581580
.word	42737628
.word	42893820
.word	43050152
.word	43206628
.word	43363244
.word	43520000
.word	43676900
.word	43833940
.word	43991124
.word	44148444
.word	44305908
.word	44463508
.word	44621252
.word	44779132
.word	44937152
.word	45095312
.word	45253612
.word	45412048
.word	45570624
.word	45729336
.word	45888188
.word	46047176
.word	46206304
.word	46365568
.word	46524968
.word	46684504
.word	46844176
.word	47003988
.word	47163932
.word	47324012
.word	47484232
.word	47644580
.word	47805068
.word	47965692
.word	48126448
.word	48287336
.word	48448364
.word	48609520
.word	48770812
.word	48932240
.word	49093800
.word	49255492
.word	49417316
.word	49579272
.word	49741364
.word	49903584
.word	50065940
.word	50228424
.word	50391044
.word	50553792
.word	50716672
.word	50879680
.word	51042824
.word	51206092
.word	51369496
.word	51533028
.word	51696688
.word	51860480
.word	52024400
.word	52188452
.word	52352632
.word	52516940
.word	52681376
.word	52845940
.word	53010632
.word	53175456
.word	53340404
.word	53505480
.word	53670684
.word	53836016
.word	54001476
.word	54167060
.word	54332772
.word	54498608
.word	54664572
.word	54830664
.word	54996880
.word	55163224
.word	55329692
.word	55496284
.word	55663000
.word	55829844
.word	55996812
.word	56163904
.word	56331120
.word	56498460
.word	56665928
.word	56833516
.word	57001228
.word	57169064
.word	57337024
.word	57505104
.word	57673308
.word	57841636
.word	58010088
.word	58178660
.word	58347356
.word	58516172
.word	58685112
.word	58854172
.word	59023352
.word	59192656
.word	59362080
.word	59531624
.word	59701292
.word	59871080
.word	60040984
.word	60211012
.word	60381160
.word	60551428
.word	60721816
.word	60892324
.word	61062948
.word	61233696
.word	61404560
.word	61575544
.word	61746648
.word	61917868
.word	62089208
.word	62260664
.word	62432240
.word	62603936
.word	62775748
.word	62947676
.word	63119724
.word	63291888
.word	63464168
.word	63636568
.word	63809084
.word	63981716
.word	64154464
.word	64327328
.word	64500308
.word	64673404
.word	64846620
.word	65019948
.word	65193392
.word	65366952
.word	65540628
.word	65714416
.word	65888320
.word	66062340
.word	66236476
.word	66410724
.word	66585088
.word	66759568
.word	66934160
.word	67108864
.word	67283680
.word	67458616
.word	67633664
.word	67808824
.word	67984096
.word	68159480
.word	68334984
.word	68510592
.word	68686320
.word	68862152
.word	69038104
.word	69214168
.word	69390336
.word	69566624
.word	69743024
.word	69919528
.word	70096152
.word	70272880
.word	70449728
.word	70626680
.word	70803744
.word	70980920
.word	71158208
.word	71335600
.word	71513112
.word	71690728
.word	71868456
.word	72046288
.word	72224240
.word	72402296
.word	72580456
.word	72758736
.word	72937120
.word	73115616
.word	73294216
.word	73472928
.word	73651752
.word	73830680
.word	74009712
.word	74188864
.word	74368120
.word	74547480
.word	74726952
.word	74906528
.word	75086216
.word	75266008
.word	75445904
.word	75625912
.word	75806032
.word	75986256
.word	76166584
.word	76347016
.word	76527560
.word	76708208
.word	76888968
.word	77069824
.word	77250792
.word	77431872
.word	77613048
.word	77794336
.word	77975728
.word	78157224
.word	78338824
.word	78520536
.word	78702352
.word	78884264
.word	79066288
.word	79248416
.word	79430648
.word	79612984
.word	79795432
.word	79977976
.word	80160624
.word	80343376
.word	80526240
.word	80709200
.word	80892264
.word	81075432
.word	81258712
.word	81442088
.word	81625568
.word	81809144
.word	81992832
.word	82176624
.word	82360512
.word	82544512
.word	82728608
.word	82912808
.word	83097104
.word	83281512
.word	83466016
.word	83650624
.word	83835336
.word	84020144
.word	84205064
.word	84390072
.word	84575192
.word	84760408
.word	84945728
.word	85131152
.word	85316672
.word	85502296
.word	85688016
.word	85873840
.word	86059768
.word	86245792
.word	86431912
.word	86618144
.word	86804464
.word	86990896
.word	87177416
.word	87364040
.word	87550768
.word	87737592
.word	87924520
.word	88111544
.word	88298664
.word	88485888
.word	88673208
.word	88860624
.word	89048144
.word	89235768
.word	89423480
.word	89611296
.word	89799208
.word	89987224
.word	90175328
.word	90363544
.word	90551848
.word	90740248
.word	90928752
.word	91117352
.word	91306048
.word	91494848
.word	91683736
.word	91872728
.word	92061816
.word	92251000
.word	92440280
.word	92629656
.word	92819136
.word	93008704
.word	93198376
.word	93388136
.word	93578000
.word	93767960
.word	93958016
.word	94148160
.word	94338408
.word	94528752
.word	94719192
.word	94909728
.word	95100352
.word	95291080
.word	95481896
.word	95672816
.word	95863824
.word	96054936
.word	96246136
.word	96437432
.word	96628824
.word	96820312
.word	97011888
.word	97203568
.word	97395336
.word	97587200
.word	97779160
.word	97971216
.word	98163360
.word	98355600
.word	98547936
.word	98740368
.word	98932888
.word	99125512
.word	99318216
.word	99511024
.word	99703920
.word	99896912
.word	100089992
.word	100283176
.word	100476440
.word	100669808
.word	100863264
.word	101056808
.word	101250456
.word	101444184
.word	101638016
.word	101831936
.word	102025944
.word	102220048
.word	102414248
.word	102608536
.word	102802912
.word	102997384
.word	103191952
.word	103386608
.word	103581352
.word	103776192
.word	103971128
.word	104166144
.word	104361256
.word	104556464
.word	104751760
.word	104947144
.word	105142624
.word	105338192
.word	105533856
.word	105729600
.word	105925440
.word	106121376
.word	106317400
.word	106513512
.word	106709712
.word	106906008
.word	107102384
.word	107298864
.word	107495424
.word	107692080
.word	107888816
.word	108085656
.word	108282576
.word	108479584
.word	108676688
.word	108873880
.word	109071160
.word	109268528
.word	109465984
.word	109663536
.word	109861168
.word	110058896
.word	110256712
.word	110454608
.word	110652600
.word	110850680
.word	111048856
.word	111247112
.word	111445456
.word	111643888
.word	111842408
.word	112041024
.word	112239720
.word	112438504
.word	112637384
.word	112836344
.word	113035392
.word	113234528
.word	113433752
.word	113633064
.word	113832464
.word	114031952
.word	114231528
.word	114431192
.word	114630936
.word	114830776
.word	115030696
.word	115230712
.word	115430808
.word	115630992
.word	115831256
.word	116031616
.word	116232056
.word	116432592
.word	116633208
.word	116833904
.word	117034696
.word	117235568
.word	117436528
.word	117637576
.word	117838712
.word	118039928
.word	118241232
.word	118442624
.word	118644104
.word	118845664
.word	119047312
.word	119249040
.word	119450856
.word	119652760
.word	119854752
.word	120056824
.word	120258984
.word	120461224
.word	120663552
.word	120865968
.word	121068464
.word	121271048
.word	121473712
.word	121676464
.word	121879304
.word	122082224
.word	122285232
.word	122488320
.word	122691488
.word	122894752
.word	123098088
.word	123301520
.word	123505024
.word	123708624
.word	123912296
.word	124116056
.word	124319904
.word	124523832
.word	124727848
.word	124931944
.word	125136120
.word	125340384
.word	125544728
.word	125749152
.word	125953664
.word	126158264
.word	126362936
.word	126567704
.word	126772544
.word	126977472
.word	127182480
.word	127387568
.word	127592744
.word	127798000
.word	128003344
.word	128208760
.word	128414264
.word	128619856
.word	128825520
.word	129031272
.word	129237104
.word	129443016
.word	129649016
.word	129855096
.word	130061256
.word	130267496
.word	130473816
.word	130680224
.word	130886712
.word	131093280
.word	131299928
.word	131506656
.word	131713472
.word	131920360
.word	132127336
.word	132334392
.word	132541528
.word	132748744
.word	132956048
.word	133163424
.word	133370888
.word	133578424
.word	133786048
.word	133993752
.word	134201536
.word	134409392
.word	134617344
.word	134825360
.word	135033472
.word	135241648
.word	135449920
.word	135658256
.word	135866688
.word	136075184
.word	136283776
.word	136492432
.word	136701184
.word	136910000
.word	137118912
.word	137327888
.word	137536960
.word	137746096
.word	137955312
.word	138164624
.word	138374000
.word	138583456
.word	138792992
.word	139002608
.word	139212304
.word	139422080
.word	139631936
.word	139841872
.word	140051888
.word	140261968
.word	140472144
.word	140682400
.word	140892720
.word	141103120
.word	141313616
.word	141524176
.word	141734816
.word	141945536
.word	142156320
.word	142367200
.word	142578160
.word	142789184
.word	143000304
.word	143211488
.word	143422752
.word	143634096
.word	143845520
.word	144057008
.word	144268592
.word	144480240
.word	144691968
.word	144903776
.word	145115664
.word	145327632
.word	145539680
.word	145751792
.word	145963984
.word	146176256
.word	146388608
.word	146601040
.word	146813536
.word	147026128
.word	147238784
.word	147451520
.word	147664320
.word	147877216
.word	148090176
.word	148303216
.word	148516336
.word	148729536
.word	148942800
.word	149156144
.word	149369568
.word	149583072
.word	149796640
.word	150010304
.word	150224032
.word	150437824
.word	150651712
.word	150865664
.word	151079696
.word	151293808
.word	151507984
.word	151722240
.word	151936576
.word	152150992
.word	152365472
.word	152580032
.word	152794672
.word	153009392
.word	153224176
.word	153439040
.word	153653984
.word	153868992
.word	154084080
.word	154299248
.word	154514480
.word	154729792
.word	154945184
.word	155160640
.word	155376192
.word	155591792
.word	155807488
.word	156023248
.word	156239088
.word	156454992
.word	156670992
.word	156887040
.word	157103184
.word	157319392
.word	157535680
.word	157752032
.word	157968464
.word	158184976
.word	158401552
.word	158618208
.word	158834928
.word	159051728
.word	159268608
.word	159485568
.word	159702592
.word	159919680
.word	160136848
.word	160354096
.word	160571424
.word	160788816
.word	161006272
.word	161223824
.word	161441424
.word	161659120
.word	161876880
.word	162094704
.word	162312608
.word	162530592
.word	162748640
.word	162966768
.word	163184960
.word	163403232
.word	163621584
.word	163840000
.word	164058496
.word	164277056
.word	164495680
.word	164714400
.word	164933168
.word	165152032
.word	165370960
.word	165589952
.word	165809024
.word	166028160
.word	166247376
.word	166466672
.word	166686032
.word	166905456
.word	167124960
.word	167344544
.word	167564192
.word	167783904
.word	168003696
.word	168223568
.word	168443504
.word	168663504
.word	168883584
.word	169103744
.word	169323968
.word	169544256
.word	169764624
.word	169985056
.word	170205568
.word	170426160
.word	170646800
.word	170867520
.word	171088320
.word	171309184
.word	171530128
.word	171751136
.word	171972208
.word	172193360
.word	172414592
.word	172635872
.word	172857248
.word	173078672
.word	173300176
.word	173521760
.word	173743408
.word	173965120
.word	174186912
.word	174408768
.word	174630704
.word	174852704
.word	175074768
.word	175296912
.word	175519120
.word	175741408
.word	175963760
.word	176186176
.word	176408672
.word	176631232
.word	176853856
.word	177076560
.word	177299344
.word	177522176
.word	177745088
.word	177968080
.word	178191136
.word	178414256
.word	178637440
.word	178860704
.word	179084032
.word	179307440
.word	179530912
.word	179754448
.word	179978064
.word	180201744
.word	180425488
.word	180649312
.word	180873200
.word	181097152
.word	181321184
.word	181545280
.word	181769440
.word	181993664
.word	182217968
.word	182442352
.word	182666784
.word	182891296
.word	183115872
.word	183340528
.word	183565232
.word	183790032
.word	184014880
.word	184239808
.word	184464800
.word	184689856
.word	184914976
.word	185140176
.word	185365440
.word	185590784
.word	185816176
.word	186041648
.word	186267184
.word	186492800
.word	186718480
.word	186944224
.word	187170032
.word	187395904
.word	187621856
.word	187847872
.word	188073952
.word	188300112
.word	188526336
.word	188752624
.word	188978976
.word	189205392
.word	189431888
.word	189658448
.word	189885072
.word	190111776
.word	190338528
.word	190565360
.word	190792256
.word	191019232
.word	191246256
.word	191473360
.word	191700528
.word	191927760
.word	192155056
.word	192382432
.word	192609872
.word	192837376
.word	193064944
.word	193292576
.word	193520288
.word	193748048
.word	193975888
.word	194203792
.word	194431760
.word	194659808
.word	194887920
.word	195116080
.word	195344320
.word	195572624
.word	195801008
.word	196029440
.word	196257952
.word	196486528
.word	196715152
.word	196943872
.word	197172640
.word	197401472
.word	197630384
.word	197859344
.word	198088384
.word	198317488
.word	198546656
.word	198775888
.word	199005200
.word	199234560
.word	199464000
.word	199693504
.word	199923056
.word	200152688
.word	200382400
.word	200612160
.word	200841984
.word	201071888
.word	201301840
.word	201531872
.word	201761952
.word	201992112
.word	202222336
.word	202452624
.word	202682976
.word	202913408
.word	203143888
.word	203374432
.word	203605056
.word	203835744
.word	204066480
.word	204297296
.word	204528176
.word	204759120
.word	204990128
.word	205221200
.word	205452336
.word	205683536
.word	205914800
.word	206146128
.word	206377536
.word	206608992
.word	206840512
.word	207072112
.word	207303776
.word	207535488
.word	207767280
.word	207999120
.word	208231040
.word	208463024
.word	208695072
.word	208927168
.word	209159344
.word	209391584
.word	209623888
.word	209856256
.word	210088688
.word	210321184
.word	210553744
.word	210786368
.word	211019056
.word	211251808
.word	211484624
.word	211717504
.word	211950448
.word	212183456
.word	212416528
.word	212649664
.word	212882864
.word	213116128
.word	213349456
.word	213582848
.word	213816304
.word	214049824
.word	214283408
.word	214517056
.word	214750768
.word	214984544
.word	215218384
.word	215452272
.word	215686240
.word	215920272
.word	216154368
.word	216388512
.word	216622736
.word	216857024
.word	217091360
.word	217325776
.word	217560240
.word	217794784
.word	218029376
.word	218264032
.word	218498768
.word	218733552
.word	218968400
.word	219203312
.word	219438288
.word	219673328
.word	219908432
.word	220143600
.word	220378816
.word	220614112
.word	220849472
.word	221084880
.word	221320352
.word	221555904
.word	221791504
.word	222027168
.word	222262896
.word	222498688
.word	222734544
.word	222970464
.word	223206432
.word	223442480
.word	223678592
.word	223914752
.word	224150976
.word	224387264
.word	224623616
.word	224860032
.word	225096512
.word	225333056
.word	225569648
.word	225806320
.word	226043040
.word	226279824
.word	226516672
.word	226753584
.word	226990560
.word	227227600
.word	227464688
.word	227701856
.word	227939072
.word	228176352
.word	228413696
.word	228651104
.word	228888576
.word	229126096
.word	229363696
.word	229601344
.word	229839056
.word	230076832
.word	230314672
.word	230552560
.word	230790528
.word	231028544
.word	231266624
.word	231504768
.word	231742976
.word	231981232
.word	232219568
.word	232457952
.word	232696400
.word	232934912
.word	233173488
.word	233412112
.word	233650816
.word	233889568
.word	234128384
.word	234367248
.word	234606192
.word	234845184
.word	235084240
.word	235323360
.word	235562544
.word	235801792
.word	236041088
.word	236280448
.word	236519872
.word	236759360
.word	236998896
.word	237238512
.word	237478176
.word	237717904
.word	237957680
.word	238197536
.word	238437440
.word	238677408
.word	238917424
.word	239157520
.word	239397664
.word	239637872
.word	239878144
.word	240118480
.word	240358864
.word	240599312
.word	240839824
.word	241080384
.word	241321024
.word	241561712
.word	241802464
.word	242043264
.word	242284128
.word	242525056
.word	242766048
.word	243007104
.word	243248208
.word	243489376
.word	243730608
.word	243971888
.word	244213232
.word	244454640
.word	244696112
.word	244937632
.word	245179232
.word	245420864
.word	245662576
.word	245904336
.word	246146160
.word	246388048
.word	246629984
.word	246871984
.word	247114048
.word	247356176
.word	247598352
.word	247840592
.word	248082880
.word	248325248
.word	248567664
.word	248810144
.word	249052672
.word	249295264
.word	249537920
.word	249780624
.word	250023392
.word	250266224
.word	250509120
.word	250752064
.word	250995072
.word	251238128
.word	251481264
.word	251724448
.word	251967680
.word	252210976
.word	252454336
.word	252697760
.word	252941232
.word	253184768
.word	253428368
.word	253672016
.word	253915728
.word	254159488
.word	254403328
.word	254647216
.word	254891152
.word	255135152
.word	255379216
.word	255623328
.word	255867520
.word	256111744
.word	256356048
.word	256600400
.word	256844800
.word	257089280
.word	257333808
.word	257578384
.word	257823024
.word	258067728
.word	258312480
.word	258557312
.word	258802176
.word	259047120
.word	259292096
.word	259537152
.word	259782256
.word	260027424
.word	260272640
.word	260517920
.word	260763264
.word	261008656
.word	261254112
.word	261499616
.word	261745184
.word	261990816
.word	262236496
.word	262482240
.word	262728032
.word	262973888
.word	263219808
.word	263465776
.word	263711808
.word	263957888
.word	264204032
.word	264450240
.word	264696496
.word	264942816
.word	265189184
.word	265435616
.word	265682096
.word	265928640
.word	266175248
.word	266421904
.word	266668624
.word	266915392
.word	267162224
.word	267409104
.word	267656048
.word	267903056
.word	268150112
.word	268397232
.word	268644384
.word	268891616
.word	269138912
.word	269386240
.word	269633664
.word	269881120
.word	270128608
.word	270376192
.word	270623808
.word	270871488
.word	271119232
.word	271367040
.word	271614880
.word	271862784
.word	272110752
.word	272358784
.word	272606848
.word	272854976
.word	273103168
.word	273351424
.word	273599744
.word	273848096
.word	274096512
.word	274344992
.word	274593504
.word	274842080
.word	275090752
.word	275339424
.word	275588192
.word	275836992
.word	276085856
.word	276334784
.word	276583776
.word	276832800
.word	277081888
.word	277331040
.word	277580224
.word	277829504
.word	278078816
.word	278328192
.word	278577600
.word	278827104
.word	279076640
.word	279326208
.word	279575872
.word	279825568
.word	280075328
.word	280325152
.word	280575008
.word	280824960
.word	281074944
.word	281324960
.word	281575072
.word	281825216
.word	282075424
.word	282325696
.word	282576000
.word	282826368
.word	283076800
.word	283327296
.word	283577824
.word	283828416
.word	284079072
.word	284329760
.word	284580512
.word	284831328
.word	285082208
.word	285333120
.word	285584128
.word	285835136
.word	286086240
.word	286337376
.word	286588576
.word	286839840
.word	287091168
.word	287342528
.word	287593952
.word	287845408
.word	288096960
.word	288348544
.word	288600160
.word	288851872
.word	289103616
.word	289355424
.word	289607296
.word	289859200
.word	290111168
.word	290363200
.word	290615264
.word	290867392
.word	291119584
.word	291371840
.word	291624128
.word	291876480
.word	292128896
.word	292381344
.word	292633856
.word	292886432
.word	293139072
.word	293391744
.word	293644480
.word	293897248
.word	294150112
.word	294403008
.word	294655936
.word	294908960
.word	295162016
.word	295415136
.word	295668288
.word	295921504
.word	296174784
.word	296428128
.word	296681504
.word	296934944
.word	297188448
.word	297441984
.word	297695584
.word	297949248
.word	298202944
.word	298456704
.word	298710528
.word	298964384
.word	299218336
.word	299472288
.word	299726336
.word	299980416
.word	300234560
.word	300488768
.word	300743008
.word	300997312
.word	301251648
.word	301506080
.word	301760544
.word	302015040
.word	302269600
.word	302524224
.word	302778912
.word	303033632
.word	303288416
.word	303543264
.word	303798144
.word	304053088
.word	304308096
.word	304563136
.word	304818240
.word	305073408
.word	305328640
.word	305583904
.word	305839200
.word	306094592
.word	306350016
.word	306605472
.word	306861024
.word	307116608
.word	307372224
.word	307627936
.word	307883680
.word	308139456
.word	308395328
.word	308651232
.word	308907168
.word	309163200
.word	309419264
.word	309675360
.word	309931520
.word	310187744
.word	310444032
.word	310700352
.word	310956736
.word	311213152
.word	311469664
.word	311726176
.word	311982784
.word	312239424
.word	312496128
.word	312752864
.word	313009664
.word	313266528
.word	313523424
.word	313780384
.word	314037408
.word	314294464
.word	314551584
.word	314808768
.word	315065984
.word	315323264
.word	315580576
.word	315837984
.word	316095392
.word	316352896
.word	316610432
.word	316868000
.word	317125664
.word	317383360
.word	317641088
.word	317898912
.word	318156736
.word	318414656
.word	318672608
.word	318930624
.word	319188672
.word	319446784
.word	319704960
.word	319963168
.word	320221440
.word	320479776
.word	320738144
.word	320996576
.word	321255040
.word	321513568
.word	321772160
.word	322030784
.word	322289472
.word	322548192
.word	322807008
.word	323065824
.word	323324736
.word	323583680
.word	323842656
.word	324101728
.word	324360832
.word	324619968
.word	324879168
.word	325138432
.word	325397728
.word	325657088
.word	325916512
.word	326175968
.word	326435488
.word	326695040
.word	326954656
.word	327214336
.word	327474048
.word	327733824
.word	327993632
.word	328253504
.word	328513440
.word	328773408
.word	329033440
.word	329293536
.word	329553664
.word	329813856
.word	330074080
.word	330334368
.word	330594688
.word	330855072
.word	331115520
.word	331376000
.word	331636544
.word	331897152
.word	332157792
.word	332418496
.word	332679232
.word	332940032
.word	333200864
.word	333461792
.word	333722720
.word	333983744
.word	334244768
.word	334505888
.word	334767040
.word	335028256
.word	335289504
.word	335550816
.word	335812160
.word	336073568
.word	336335040
.word	336596544
.word	336858112
.word	337119712
.word	337381376
.word	337643104
.word	337904864
.word	338166656
.word	338428544
.word	338690464
.word	338952416
.word	339214432
.word	339476512
.word	339738624
.word	340000800
.word	340263008
.word	340525280
.word	340787616
.word	341049984
.word	341312384
.word	341574880
.word	341837408
.word	342099968
.word	342362592
.word	342625248
.word	342888000
.word	343150752
.word	343413600
.word	343676448
.word	343939392
.word	344202368
.word	344465376
.word	344728480
.word	344991584
.word	345254784
.word	345517984
.word	345781280
.word	346044608
.word	346307968
.word	346571392
.word	346834880
.word	347098400
.word	347361984
.word	347625600
.word	347889280
.word	348153024
.word	348416800
.word	348680608
.word	348944512
.word	349208416
.word	349472416
.word	349736416
.word	350000512
.word	350264640
.word	350528800
.word	350793024
.word	351057312
.word	351321632
.word	351586016
.word	351850432
.word	352114912
.word	352379424
.word	352644000
.word	352908640
.word	353173312
.word	353438016
.word	353702816
.word	353967616
.word	354232480
.word	354497408
.word	354762400
.word	355027392
.word	355292480
.word	355557600
.word	355822752
.word	356087968
.word	356353248
.word	356618560
.word	356883936
.word	357149344
.word	357414816
.word	357680320
.word	357945888
.word	358211488
.word	358477152
.word	358742880
.word	359008640
.word	359274432
.word	359540288
.word	359806208
.word	360072160
.word	360338176
.word	360604224
.word	360870336
.word	361136480
.word	361402688
.word	361668928
.word	361935232
.word	362201568
.word	362467968
.word	362734432
.word	363000928
.word	363267456
.word	363534048
.word	363800704
.word	364067392
.word	364334144
.word	364600928
.word	364867776
.word	365134656
.word	365401600
.word	365668576
.word	365935616
.word	366202688
.word	366469824
.word	366736992
.word	367004224
.word	367271488
.word	367538816
.word	367806208
.word	368073632
.word	368341088
.word	368608608
.word	368876192
.word	369143808
.word	369411456
.word	369679168
.word	369946944
.word	370214752
.word	370482624
.word	370750528
.word	371018464
.word	371286464
.word	371554528
.word	371822624
.word	372090784
.word	372358976
.word	372627232
.word	372895520
.word	373163872
.word	373432256
.word	373700704
.word	373969184
.word	374237728
.word	374506304
.word	374774944
.word	375043616
.word	375312352
.word	375581120
.word	375849952
.word	376118816
.word	376387744
.word	376656704
.word	376925728
.word	377194784
.word	377463904
.word	377733056
.word	378002272
.word	378271520
.word	378540832
.word	378810176
.word	379079584
.word	379349024
.word	379618528
.word	379888064
.word	380157664
.word	380427296
.word	380696992
.word	380966720
.word	381236512
.word	381506336
.word	381776192
.word	382046144
.word	382316096
.word	382586112
.word	382856192
.word	383126304
.word	383396480
.word	383666688
.word	383936960
.word	384207264
.word	384477600
.word	384748000
.word	385018464
.word	385288960
.word	385559488
.word	385830080
.word	386100736
.word	386371424
.word	386642144
.word	386912928
.word	387183776
.word	387454624
.word	387725568
.word	387996544
.word	388267552
.word	388538624
.word	388809728
.word	389080896
.word	389352096
.word	389623360
.word	389894656
.word	390166016
.word	390437408
.word	390708832
.word	390980320
.word	391251872
.word	391523456
.word	391795072
.word	392066752
.word	392338496
.word	392610272
.word	392882080
.word	393153952
.word	393425856
.word	393697824
.word	393969824
.word	394241888
.word	394513984
.word	394786144
.word	395058336
.word	395330592
.word	395602880
.word	395875200
.word	396147584
.word	396420032
.word	396692512
.word	396965024
.word	397237600
.word	397510208
.word	397782880
.word	398055584
.word	398328352
.word	398601152
.word	398873984
.word	399146880
.word	399419840
.word	399692832
.word	399965856
.word	400238944
.word	400512096
.word	400785248
.word	401058496
.word	401331744
.word	401605056
.word	401878432
.word	402151840
.word	402425312
.word	402698816
.word	402972352
.word	403245952
.word	403519584
.word	403793280
.word	404067008
.word	404340800
.word	404614624
.word	404888512
.word	405162432
.word	405436384
.word	405710400
.word	405984448
.word	406258560
.word	406532704
.word	406806912
.word	407081152
.word	407355456
.word	407629792
.word	407904160
.word	408178592
.word	408453056
.word	408727584
.word	409002144
.word	409276768
.word	409551424
.word	409826144
.word	410100896
.word	410375680
.word	410650528
.word	410925408
.word	411200352
.word	411475328
.word	411750368
.word	412025440
.word	412300544
.word	412575712
.word	412850944
.word	413126176
.word	413401504
.word	413676832
.word	413952224
.word	414227680
.word	414503168
.word	414778688
.word	415054272
.word	415329888
.word	415605568
.word	415881280
.word	416157024
.word	416432832
.word	416708704
.word	416984576
.word	417260544
.word	417536512
.word	417812544
.word	418088640
.word	418364768
.word	418640928
.word	418917152
.word	419193408
.word	419469728
.word	419746080
.word	420022464
.word	420298912
.word	420575392
.word	420851936
.word	421128512
.word	421405152
.word	421681824
.word	421958528
.word	422235296
.word	422512096
.word	422788960
.word	423065856
.word	423342816
.word	423619776
.word	423896832
.word	424173888
.word	424451040
.word	424728192
.word	425005408
.word	425282688
.word	425559968
.word	425837344
.word	426114720
.word	426392160
.word	426669664
.word	426947168
.word	427224768
.word	427502368
.word	427780064
.word	428057760
.word	428335520
.word	428613312
.word	428891168
.word	429169056
.word	429447008
.word	429724960
.word	430003008
.word	430281088
.word	430559200
.word	430837344
.word	431115552
.word	431393824
.word	431672096
.word	431950432
.word	432228832
.word	432507264
.word	432785728
.word	433064256
.word	433342816
.word	433621440
.word	433900096
.word	434178784
.word	434457536
.word	434736320
.word	435015168
.word	435294016
.word	435572960
.word	435851936
.word	436130944
.word	436409984
.word	436689088
.word	436968224
.word	437247424
.word	437526656
.word	437805952
.word	438085280
.word	438364640
.word	438644064
.word	438923520
.word	439203008
.word	439482560
.word	439762144
.word	440041792
.word	440321472
.word	440601184
.word	440880960
.word	441160768
.word	441440640
.word	441720544
.word	442000480
.word	442280480
.word	442560512
.word	442840576
.word	443120704
.word	443400864
.word	443681088
.word	443961344
.word	444241632
.word	444521984
.word	444802368
.word	445082816
.word	445363296
.word	445643808
.word	445924384
.word	446204992
.word	446485632
.word	446766336
.word	447047072
.word	447327872
.word	447608704
.word	447889568
.word	448170496
.word	448451456
.word	448732448
.word	449013504
.word	449294592
.word	449575744
.word	449856928
.word	450138144
.word	450419424
.word	450700736
.word	450982112
.word	451263488
.word	451544960
.word	451826432
.word	452107968
.word	452389536
.word	452671168
.word	452952832
.word	453234528
.word	453516288
.word	453798080
.word	454079936
.word	454361824
.word	454643744
.word	454925728
.word	455207744
.word	455489792
.word	455771904
.word	456054048
.word	456336224
.word	456618464
.word	456900736
.word	457183072
.word	457465408
.word	457747840
.word	458030272
.word	458312768
.word	458595328
.word	458877888
.word	459160512
.word	459443200
.word	459725920
.word	460008672
.word	460291456
.word	460574304
.word	460857184
.word	461140128
.word	461423104
.word	461706112
.word	461989184
.word	462272288
.word	462555424
.word	462838624
.word	463121856
.word	463405120
.word	463688448
.word	463971808
.word	464255200
.word	464538656
.word	464822144
.word	465105696
.word	465389280
.word	465672896
.word	465956544
.word	466240256
.word	466524000
.word	466807808
.word	467091648
.word	467375520
.word	467659456
.word	467943424
.word	468227424
.word	468511488
.word	468795584
.word	469079712
.word	469363904
.word	469648128
.word	469932416
.word	470216704
.word	470501056
.word	470785472
.word	471069920
.word	471354400
.word	471638912
.word	471923488
.word	472208096
.word	472492768
.word	472777472
.word	473062208
.word	473346976
.word	473631808
.word	473916672
.word	474201600
.word	474486560
.word	474771552
.word	475056576
.word	475341664
.word	475626784
.word	475911968
.word	476197184
.word	476482432
.word	476767744
.word	477053088
.word	477338464
.word	477623872
.word	477909344
.word	478194848
.word	478480416
.word	478766016
.word	479051648
.word	479337344
.word	479623040
.word	479908832
.word	480194624
.word	480480480
.word	480766368
.word	481052320
.word	481338304
.word	481624320
.word	481910368
.word	482196480
.word	482482624
.word	482768832
.word	483055040
.word	483341344
.word	483627648
.word	483914016
.word	484200416
.word	484486848
.word	484773344
.word	485059872
.word	485346464
.word	485633056
.word	485919712
.word	486206432
.word	486493152
.word	486779936
.word	487066784
.word	487353632
.word	487640544
.word	487927488
.word	488214496
.word	488501536
.word	488788608
.word	489075744
.word	489362912
.word	489650112
.word	489937344
.word	490224640
.word	490511968
.word	490799360
.word	491086752
.word	491374240
.word	491661728
.word	491949280
.word	492236864
.word	492524480
.word	492812160
.word	493099872
.word	493387616
.word	493675392
.word	493963232
.word	494251136
.word	494539040
.word	494827008
.word	495115008
.word	495403040
.word	495691136
.word	495979264
.word	496267456
.word	496555648
.word	496843904
.word	497132192
.word	497420544
.word	497708928
.word	497997344
.word	498285824
.word	498574304
.word	498862880
.word	499151456
.word	499440096
.word	499728768
.word	500017472
.word	500306240
.word	500595040
.word	500883872
.word	501172736
.word	501461664
.word	501750624
.word	502039648
.word	502328704
.word	502617792
.word	502906912
.word	503196096
.word	503485312
.word	503774560
.word	504063840
.word	504353184
.word	504642560
.word	504932000
.word	505221440
.word	505510944
.word	505800512
.word	506090080
.word	506379712
.word	506669376
.word	506959104
.word	507248864
.word	507538656
.word	507828480
.word	508118368
.word	508408288
.word	508698240
.word	508988256
.word	509278272
.word	509568352
.word	509858496
.word	510148672
.word	510438880
.word	510729120
.word	511019392
.word	511309728
.word	511600096
.word	511890528
.word	512180992
.word	512471488
.word	512762016
.word	513052576
.word	513343200
.word	513633856
.word	513924576
.word	514215328
.word	514506112
.word	514796928
.word	515087776
.word	515378688
.word	515669632
.word	515960640
.word	516251680
.word	516542752
.word	516833856
.word	517124992
.word	517416192
.word	517707424
.word	517998720
.word	518290016
.word	518581376
.word	518872768
.word	519164224
.word	519455712
.word	519747232
.word	520038784
.word	520330400
.word	520622048
.word	520913728
.word	521205440
.word	521497216
.word	521789024
.word	522080864
.word	522372768
.word	522664704
.word	522956672
.word	523248672
.word	523540736
.word	523832832
.word	524124960
.word	524417120
.word	524709344
.word	525001600
.word	525293888
.word	525586240
.word	525878624
.word	526171040
.word	526463488
.word	526756000
.word	527048544
.word	527341120
.word	527633728
.word	527926400
.word	528219104
.word	528511840
.word	528804640
.word	529097440
.word	529390304
.word	529683232
.word	529976160
.word	530269152
.word	530562176
.word	530855264
.word	531148352
.word	531441504
.word	531734688
.word	532027936
.word	532321184
.word	532614496
.word	532907872
.word	533201248
.word	533494688
.word	533788160
.word	534081664
.word	534375200
.word	534668800
.word	534962432
.word	535256128
.word	535549824
.word	535843584
.word	536137376
.word	536431200
.word	536725088
.word	537019008
.word	537312960
.word	537606976
.word	537900992
.word	538195072
.word	538489152
.word	538783360
.word	539077504
.word	539371776
.word	539666048
.word	539960384
.word	540254720
.word	540549120
.word	540843520
.word	541138048
.word	541432512
.word	541727104
.word	542021696
.word	542316352
.word	542611008
.word	542905728
.word	543200512
.word	543495296
.word	543790144
.word	544084992
.word	544379904
.word	544674880
.word	544969856
.word	545264896
.word	545560000
.word	545855104
.word	546150272
.word	546445440
.word	546740672
.word	547035968
.word	547331264
.word	547626624
.word	547922048
.word	548217472
.word	548512960
.word	548808448
.word	549104000
.word	549399616
.word	549695232
.word	549990912
.word	550286656
.word	550582400
.word	550878144
.word	551174016
.word	551469888
.word	551765760
.word	552061760
.word	552357760
.word	552653760
.word	552949824
.word	553245952
.word	553542080
.word	553838272
.word	554134528
.word	554430784
.word	554727104
.word	555023424
.word	555319808
.word	555616256
.word	555912704
.word	556209216
.word	556505792
.word	556802368
.word	557099008
.word	557395648
.word	557692352
.word	557989120
.word	558285888
.word	558582720
.word	558879552
.word	559176448
.word	559473408
.word	559770368
.word	560067392
.word	560364480
.word	560661568
.word	560958720
.word	561255872
.word	561553088
.word	561850368
.word	562147648
.word	562444992
.word	562742336
.word	563039744
.word	563337216
.word	563634688
.word	563932224
.word	564229824
.word	564527424
.word	564825088
.word	565122752
.word	565420480
.word	565718272
.word	566016064
.word	566313920
.word	566611776
.word	566909696
.word	567207680
.word	567505664
.word	567803712
.word	568101760
.word	568399936
.word	568698048
.word	568996288
.word	569294528
.word	569592768
.word	569891072
.word	570189440
.word	570487808
.word	570786240
.word	571084736
.word	571383232
.word	571681792
.word	571980352
.word	572278976
.word	572577664
.word	572876352
.word	573175104
.word	573473920
.word	573772736
.word	574071552
.word	574370496
.word	574669440
.word	574968384
.word	575267392
.word	575566464
.word	575865536
.word	576164672
.word	576463872
.word	576763072
.word	577062336
.word	577361600
.word	577660928
.word	577960256
.word	578259712
.word	578559104
.word	578858624
.word	579158144
.word	579457664
.word	579757312
.word	580056896
.word	580356608
.word	580656320
.word	580956032
.word	581255872
.word	581555648
.word	581855552
.word	582155456
.word	582455424
.word	582755392
.word	583055424
.word	583355456
.word	583655552
.word	583955712
.word	584255872
.word	584556096
.word	584856320
.word	585156608
.word	585456960
.word	585757312
.word	586057728
.word	586358208
.word	586658688
.word	586959168
.word	587259776
.word	587560384
.word	587860992
.word	588161664
.word	588462400
.word	588763136
.word	589063936
.word	589364736
.word	589665664
.word	589966528
.word	590267520
.word	590568448
.word	590869504
.word	591170560
.word	591471680
.word	591772800
.word	592073984
.word	592375168
.word	592676416
.word	592977728
.word	593279040
.word	593580416
.word	593881856
.word	594183296
.word	594484736
.word	594786304
.word	595087808
.word	595389440
.word	595691072
.word	595992704
.word	596294464
.word	596596224
.word	596897984
.word	597199808
.word	597501696
.word	597803584
.word	598105536
.word	598407488
.word	598709504
.word	599011584
.word	599313664
.word	599615808
.word	599917952
.word	600220160
.word	600522432
.word	600824704
.word	601127040
.word	601429376
.word	601731776
.word	602034176
.word	602336704
.word	602639168
.word	602941760
.word	603244288
.word	603546944
.word	603849600
.word	604152320
.word	604455040
.word	604757824
.word	605060608
.word	605363456
.word	605666368
.word	605969280
.word	606272256
.word	606575296
.word	606878336
.word	607181376
.word	607484544
.word	607787648
.word	608090880
.word	608394112
.word	608697344
.word	609000640
.word	609304000
.word	609607360
.word	609910784
.word	610214272
.word	610517760
.word	610821312
.word	611124864
.word	611428480
.word	611732096
.word	612035776
.word	612339520
.word	612643264
.word	612947072
.word	613250880
.word	613554752
.word	613858688
.word	614162624
.word	614466624
.word	614770624
.word	615074688
.word	615378816
.word	615682944
.word	615987136
.word	616291328
.word	616595584
.word	616899840
.word	617204160
.word	617508544
.word	617812928
.word	618117376
.word	618421824
.word	618726336
.word	619030912
.word	619335488
.word	619640128
.word	619944768
.word	620249472
.word	620554176
.word	620858944
.word	621163776
.word	621468608
.word	621773504
.word	622078464
.word	622383360
.word	622688384
.word	622993408
.word	623298496
.word	623603584
.word	623908736
.word	624213952
.word	624519168
.word	624824384
.word	625129728
.word	625435008
.word	625740416
.word	626045824
.word	626351232
.word	626656768
.word	626962240
.word	627267840
.word	627573376
.word	627879040
.word	628184704
.word	628490432
.word	628796160
.word	629101952
.word	629407744
.word	629713600
.word	630019456
.word	630325440
.word	630631360
.word	630937408
.word	631243392
.word	631549504
.word	631855616
.word	632161728
.word	632467968
.word	632774144
.word	633080448
.word	633386752
.word	633693056
.word	633999424
.word	634305856
.word	634612288
.word	634918784
.word	635225280
.word	635531840
.word	635838464
.word	636145088
.word	636451776
.word	636758464
.word	637065216
.word	637371968
.word	637678784
.word	637985664
.word	638292544
.word	638599488
.word	638906432
.word	639213440
.word	639520448
.word	639827520
.word	640134656
.word	640441792
.word	640748992
.word	641056192
.word	641363456
.word	641670784
.word	641978112
.word	642285440
.word	642592832
.word	642900288
.word	643207808
.word	643515328
.word	643822848
.word	644130432
.word	644438080
.word	644745728
.word	645053440
.word	645361152
.word	645668928
.word	645976768
.word	646284608
.word	646592448
.word	646900416
.word	647208384
.word	647516352
.word	647824384
.word	648132416
.word	648440512
.word	648748672
.word	649056832
.word	649365056
.word	649673344
.word	649981632
.word	650289920
.word	650598272
.word	650906688
.word	651215104
.word	651523584
.word	651832064
.word	652140608
.word	652449216
.word	652757824
.word	653066496
.word	653375168
.word	653683904
.word	653992640
.word	654301440
.word	654610240
.word	654919168
.word	655228032
.word	655536960
.word	655845952
.word	656155008
.word	656464064
.word	656773120
.word	657082240
.word	657391424
.word	657700608
.word	658009856
.word	658319104
.word	658628416
.word	658937728
.word	659247104
.word	659556544
.word	659865984
.word	660175488
.word	660484992
.word	660794560
.word	661104192
.word	661413824
.word	661723456
.word	662033152
.word	662342912
.word	662652672
.word	662962496
.word	663272384
.word	663582272
.word	663892160
.word	664202112
.word	664512128
.word	664822144
.word	665132224
.word	665442304
.word	665752448
.word	666062656
.word	666372864
.word	666683072
.word	666993344
.word	667303680
.word	667614080
.word	667924416
.word	668234880
.word	668545344
.word	668855808
.word	669166400
.word	669476928
.word	669787520
.word	670098176
.word	670408896
.word	670719616
.word	671030336
.word	671341120
.word	671651968
.word	671962816
.word	672273728
.word	672584640
.word	672895616
.word	673206592
.word	673517632
.word	673828736
.word	674139840
.word	674451008
.word	674762176
.word	675073408
.word	675384640
.word	675695936
.word	676007296
.word	676318656
.word	676630016
.word	676941440
.word	677252928
.word	677564416
.word	677875968
.word	678187584
.word	678499200
.word	678810816
.word	679122496
.word	679434240
.word	679745984
.word	680057792
.word	680369600
.word	680681472
.word	680993344
.word	681305280
.word	681617280
.word	681929280
.word	682241344
.word	682553408
.word	682865536
.word	683177664
.word	683489856
.word	683802048
.word	684114304
.word	684426624
.word	684738944
.word	685051328
.word	685363712
.word	685676160
.word	685988608
.word	686301120
.word	686613632
.word	686926208
.word	687238848
.word	687551488
.word	687864192
.word	688176896
.word	688489664
.word	688802432
.word	689115264
.word	689428096
.word	689740992
.word	690053952
.word	690366912
.word	690679936
.word	690992960
.word	691306048
.word	691619136
.word	691932288
.word	692245440
.word	692558656
.word	692871936
.word	693185216
.word	693498560
.word	693811904
.word	694125312
.word	694438720
.word	694752192
.word	695065664
.word	695379200
.word	695692800
.word	696006400
.word	696320000
.word	696633728
.word	696947392
.word	697261184
.word	697574912
.word	697888768
.word	698202624
.word	698516480
.word	698830400
.word	699144384
.word	699458368
.word	699772416
.word	700086464
.word	700400576
.word	700714688
.word	701028864
.word	701343040
.word	701657280
.word	701971584
.word	702285888
.word	702600256
.word	702914624
.word	703229056
.word	703543488
.word	703857984
.word	704172480
.word	704487040
.word	704801664
.word	705116288
.word	705430912
.word	705745600
.word	706060352
.word	706375104
.word	706689920
.word	707004736
.word	707319616
.word	707634560
.word	707949504
.word	708264448
.word	708579456
.word	708894528
.word	709209600
.word	709524736
.word	709839872
.word	710155072
.word	710470272
.word	710785536
.word	711100800
.word	711416128
.word	711731520
.word	712046912
.word	712362304
.word	712677824
.word	712993280
.word	713308864
.word	713624384
.word	713940032
.word	714255680
.word	714571328
.word	714887040
.word	715202816
.word	715518592
.word	715834368
.word	716150208
.word	716466112
.word	716782016
.word	717097984
.word	717413952
.word	717729984
.word	718046080
.word	718362176
.word	718678272
.word	718994432
.word	719310656
.word	719626880
.word	719943168
.word	720259456
.word	720575808
.word	720892160
.word	721208576
.word	721524992
.word	721841472
.word	722158016
.word	722474560
.word	722791104
.word	723107712
.word	723424384
.word	723741056
.word	724057792
.word	724374528
.word	724691328
.word	725008128
.word	725324992
.word	725641920
.word	725958784
.word	726275776
.word	726592768
.word	726909824
.word	727226880
.word	727543936
.word	727861120
.word	728178240
.word	728495488
.word	728812736
.word	729129984
.word	729447296
.word	729764608
.word	730081984
.word	730399424
.word	730716864
.word	731034304
.word	731351872
.word	731669376
.word	731986944
.word	732304576
.word	732622272
.word	732939904
.word	733257664
.word	733575424
.word	733893184
.word	734211008
.word	734528896
.word	734846784
.word	735164672
.word	735482624
.word	735800640
.word	736118656
.word	736436736
.word	736754816
.word	737072960
.word	737391104
.word	737709312
.word	738027584
.word	738345856
.word	738664128
.word	738982464
.word	739300864
.word	739619264
.word	739937728
.word	740256192
.word	740574720
.word	740893248
.word	741211840
.word	741530432
.word	741849088
.word	742167744
.word	742486464
.word	742805184
.word	743123968
.word	743442816
.word	743761664
.word	744080576
.word	744399488
.word	744718400
.word	745037440
.word	745356416
.word	745675520
.word	745994560
.word	746313728
.word	746632896
.word	746952064
.word	747271296
.word	747590528
.word	747909824
.word	748229184
.word	748548544
.word	748867968
.word	749187392
.word	749506816
.word	749826368
.word	750145856
.word	750465408
.word	750785024
.word	751104704
.word	751424320
.word	751744064
.word	752063808
.word	752383552
.word	752703360
.word	753023232
.word	753343104
.word	753662976
.word	753982912
.word	754302912
.word	754622912
.word	754942976
.word	755263040
.word	755583168
.word	755903296
.word	756223488
.word	756543680
.word	756863936
.word	757184192
.word	757504512
.word	757824896
.word	758145280
.word	758465664
.word	758786112
.word	759106624
.word	759427136
.word	759747712
.word	760068288
.word	760388864
.word	760709568
.word	761030208
.word	761350976
.word	761671680
.word	761992512
.word	762313280
.word	762634176
.word	762955072
.word	763275968
.word	763596928
.word	763917952
.word	764238976
.word	764560000
.word	764881088
.word	765202240
.word	765523392
.word	765844608
.word	766165824
.word	766487040
.word	766808384
.word	767129664
.word	767451072
.word	767772416
.word	768093888
.word	768415360
.word	768736832
.word	769058368
.word	769379904
.word	769701504
.word	770023168
.word	770344832
.word	770666496
.word	770988224
.word	771310016
.word	771631808
.word	771953664
.word	772275520
.word	772597376
.word	772919360
.word	773241280
.word	773563328
.word	773885312
.word	774207424
.word	774529472
.word	774851648
.word	775173824
.word	775496000
.word	775818240
.word	776140480
.word	776462784
.word	776785152
.word	777107520
.word	777429888
.word	777752320
.word	778074816
.word	778397312
.word	778719872
.word	779042432
.word	779364992
.word	779687616
.word	780010304
.word	780332992
.word	780655744
.word	780978496
.word	781301312
.word	781624128
.word	781947008
.word	782269952
.word	782592832
.word	782915840
.word	783238848
.word	783561856
.word	783884928
.word	784208064
.word	784531200
.word	784854336
.word	785177536
.word	785500800
.word	785824064
.word	786147328
.word	786470656
.word	786794048
.word	787117440
.word	787440896
.word	787764352
.word	788087872
.word	788411392
.word	788734976
.word	789058560
.word	789382208
.word	789705856
.word	790029568
.word	790353280
.word	790677056
.word	791000832
.word	791324672
.word	791648576
.word	791972416
.word	792296384
.word	792620352
.word	792944320
.word	793268352
.word	793592448
.word	793916544
.word	794240640
.word	794564800
.word	794889024
.word	795213248
.word	795537536
.word	795861824
.word	796186112
.word	796510528
.word	796834880
.word	797159296
.word	797483776
.word	797808256
.word	798132800
.word	798457344
.word	798781952
.word	799106560
.word	799431232
.word	799755904
.word	800080640
.word	800405440
.word	800730176
.word	801055040
.word	801379904
.word	801704768
.word	802029696
.word	802354624
.word	802679616
.word	803004672
.word	803329728
.word	803654784
.word	803979904
.word	804305088
.word	804630272
.word	804955456
.word	805280704
.word	805606016
.word	805931328
.word	806256704
.word	806582080
.word	806907456
.word	807232896
.word	807558400
.word	807883904
.word	808209472
.word	808535040
.word	808860672
.word	809186304
.word	809512000
.word	809837696
.word	810163456
.word	810489216
.word	810815040
.word	811140864
.word	811466752
.word	811792640
.word	812118592
.word	812444544
.word	812770560
.word	813096576
.word	813422656
.word	813748736
.word	814074880
.word	814401088
.word	814727296
.word	815053504
.word	815379776
.word	815706048
.word	816032384
.word	816358784
.word	816685184
.word	817011584
.word	817338048
.word	817664512
.word	817991040
.word	818317632
.word	818644224
.word	818970816
.word	819297472
.word	819624192
.word	819950912
.word	820277696
.word	820604480
.word	820931264
.word	821258112
.word	821585024
.word	821911936
.word	822238912
.word	822565888
.word	822892864
.word	823219904
.word	823547008
.word	823874112
.word	824201280
.word	824528448
.word	824855680
.word	825182912
.word	825510144
.word	825837504
.word	826164800
.word	826492160
.word	826819584
.word	827147008
.word	827474496
.word	827801984
.word	828129536
.word	828457088
.word	828784704
.word	829112320
.word	829440000
.word	829767680
.word	830095424
.word	830423168
.word	830750976
.word	831078784
.word	831406656
.word	831734528
.word	832062464
.word	832390400
.word	832718400
.word	833046464
.word	833374464
.word	833702592
.word	834030720
.word	834358848
.word	834687040
.word	835015232
.word	835343488
.word	835671744
.word	836000064
.word	836328384
.word	836656768
.word	836985216
.word	837313600
.word	837642112
.word	837970624
.word	838299136
.word	838627712
.word	838956288
.word	839284928
.word	839613568
.word	839942272
.word	840271040
.word	840599808
.word	840928576
.word	841257408
.word	841586240
.word	841915136
.word	842244096
.word	842572992
.word	842902016
.word	843231040
.word	843560064
.word	843889152
.word	844218240
.word	844547392
.word	844876608
.word	845205824
.word	845535040
.word	845864320
.word	846193600
.word	846522944
.word	846852352
.word	847181760
.word	847511168
.word	847840640
.word	848170112
.word	848499648
.word	848829248
.word	849158848
.word	849488448
.word	849818112
.word	850147776
.word	850477504
.word	850807296
.word	851137088
.word	851466880
.word	851796736
.word	852126592
.word	852456512
.word	852786496
.word	853116480
.word	853446464
.word	853776512
.word	854106560
.word	854436672
.word	854766848
.word	855096960
.word	855427200
.word	855757440
.word	856087680
.word	856417984
.word	856748288
.word	857078656
.word	857409088
.word	857739520
.word	858069952
.word	858400448
.word	858730944
.word	859061504
.word	859392064
.word	859722688
.word	860053376
.word	860384000
.word	860714752
.word	861045504
.word	861376256
.word	861707072
.word	862037888
.word	862368768
.word	862699648
.word	863030592
.word	863361536
.word	863692544
.word	864023616
.word	864354624
.word	864685760
.word	865016832
.word	865348032
.word	865679168
.word	866010432
.word	866341696
.word	866672960
.word	867004288
.word	867335616
.word	867667008
.word	867998400
.word	868329856
.word	868661312
.word	868992832
.word	869324352
.word	869655872
.word	869987520
.word	870319104
.word	870650816
.word	870982464
.word	871314176
.word	871645952
.word	871977728
.word	872309568
.word	872641408
.word	872973312
.word	873305216
.word	873637184
.word	873969152
.word	874301120
.word	874633152
.word	874965248
.word	875297344
.word	875629504
.word	875961664
.word	876293824
.word	876626048
.word	876958336
.word	877290624
.word	877622976
.word	877955328
.word	878287680
.word	878620096
.word	878952576
.word	879285056
.word	879617536
.word	879950080
.word	880282688
.word	880615296
.word	880947904
.word	881280576
.word	881613248
.word	881945984
.word	882278784
.word	882611584
.word	882944384
.word	883277248
.word	883610112
.word	883943040
.word	884276032
.word	884608960
.word	884942016
.word	885275072
.word	885608128
.word	885941248
.word	886274368
.word	886607552
.word	886940736
.word	887273984
.word	887607232
.word	887940544
.word	888273856
.word	888607232
.word	888940608
.word	889274048
.word	889607488
.word	889940992
.word	890274496
.word	890608000
.word	890941568
.word	891275200
.word	891608832
.word	891942528
.word	892276224
.word	892609920
.word	892943744
.word	893277504
.word	893611328
.word	893945216
.word	894279104
.word	894612992
.word	894946944
.word	895280960
.word	895614976
.word	895948992
.word	896283072
.word	896617152
.word	896951296
.word	897285504
.word	897619712
.word	897953920
.word	898288192
.word	898622464
.word	898956800
.word	899291136
.word	899625536
.word	899959936
.word	900294400
.word	900628864
.word	900963392
.word	901297920
.word	901632512
.word	901967104
.word	902301760
.word	902636416
.word	902971136
.word	903305856
.word	903640576
.word	903975360
.word	904310208
.word	904645056
.word	904979968
.word	905314880
.word	905649792
.word	905984768
.word	906319808
.word	906654848
.word	906989888
.word	907324992
.word	907660096
.word	907995264
.word	908330496
.word	908665728
.word	909000960
.word	909336256
.word	909671552
.word	910006912
.word	910342272
.word	910677696
.word	911013120
.word	911348608
.word	911684096
.word	912019648
.word	912355200
.word	912690816
.word	913026432
.word	913362112
.word	913697792
.word	914033472
.word	914369216
.word	914705024
.word	915040832
.word	915376640
.word	915712512
.word	916048448
.word	916384384
.word	916720320
.word	917056320
.word	917392384
.word	917728384
.word	918064512
.word	918400640
.word	918736768
.word	919072960
.word	919409152
.word	919745408
.word	920081664
.word	920417984
.word	920754304
.word	921090688
.word	921427072
.word	921763520
.word	922099968
.word	922436416
.word	922772928
.word	923109504
.word	923446080
.word	923782656
.word	924119296
.word	924456000
.word	924792704
.word	925129408
.word	925466176
.word	925803008
.word	926139840
.word	926476672
.word	926813568
.word	927150464
.word	927487424
.word	927824384
.word	928161408
.word	928498432
.word	928835520
.word	929172608
.word	929509760
.word	929846912
.word	930184064
.word	930521280
.word	930858560
.word	931195840
.word	931533184
.word	931870528
.word	932207872
.word	932545280
.word	932882688
.word	933220160
.word	933557696
.word	933895232
.word	934232768
.word	934570368
.word	934907968
.word	935245632
.word	935583296
.word	935921024
.word	936258752
.word	936596544
.word	936934336
.word	937272128
.word	937610048
.word	937947904
.word	938285824
.word	938623808
.word	938961792
.word	939299776
.word	939637824
.word	939975872
.word	940313984
.word	940652160
.word	940990336
.word	941328512
.word	941666752
.word	942004992
.word	942343296
.word	942681600
.word	943019968
.word	943358336
.word	943696768
.word	944035200
.word	944373632
.word	944712128
.word	945050688
.word	945389248
.word	945727808
.word	946066432
.word	946405120
.word	946743808
.word	947082496
.word	947421248
.word	947760000
.word	948098816
.word	948437632
.word	948776512
.word	949115392
.word	949454336
.word	949793280
.word	950132288
.word	950471296
.word	950810304
.word	951149440
.word	951488512
.word	951827648
.word	952166848
.word	952505984
.word	952845248
.word	953184512
.word	953523776
.word	953863104
.word	954202432
.word	954541824
.word	954881216
.word	955220672
.word	955560128
.word	955899648
.word	956239168
.word	956578752
.word	956918336
.word	957257920
.word	957597568
.word	957937280
.word	958276992
.word	958616704
.word	958956480
.word	959296256
.word	959636096
.word	959975936
.word	960315840
.word	960655744
.word	960995712
.word	961335680
.word	961675712
.word	962015744
.word	962355840
.word	962695936
.word	963036032
.word	963376192
.word	963716416
.word	964056640
.word	964396864
.word	964737152
.word	965077440
.word	965417792
.word	965758208
.word	966098560
.word	966439040
.word	966779456
.word	967119936
.word	967460480
.word	967801024
.word	968141632
.word	968482240
.word	968822848
.word	969163520
.word	969504256
.word	969844928
.word	970185728
.word	970526528
.word	970867328
.word	971208192
.word	971549056
.word	971889984
.word	972230912
.word	972571904
.word	972912896
.word	973253888
.word	973594944
.word	973936064
.word	974277184
.word	974618304
.word	974959488
.word	975300672
.word	975641920
.word	975983232
.word	976324480
.word	976665856
.word	977007168
.word	977348608
.word	977689984
.word	978031424
.word	978372928
.word	978714432
.word	979055936
.word	979397504
.word	979739136
.word	980080768
.word	980422400
.word	980764096
.word	981105792
.word	981447552
.word	981789312
.word	982131136
.word	982472960
.word	982814848
.word	983156736
.word	983498624
.word	983840576
.word	984182592
.word	984524608
.word	984866624
.word	985208704
.word	985550784
.word	985892928
.word	986235072
.word	986577280
.word	986919488
.word	987261760
.word	987604032
.word	987946368
.word	988288704
.word	988631040
.word	988973440
.word	989315904
.word	989658304
.word	990000832
.word	990343360
.word	990685888
.word	991028480
.word	991371072
.word	991713664
.word	992056384
.word	992399040
.word	992741760
.word	993084544
.word	993427328
.word	993770112
.word	994112960
.word	994455808
.word	994798720
.word	995141696
.word	995484608
.word	995827648
.word	996170624
.word	996513664
.word	996856768
.word	997199872
.word	997543040
.word	997886208
.word	998229376
.word	998572608
.word	998915840
.word	999259136
.word	999602432
.word	999945792
.word	1000289152
.word	1000632576
.word	1000976000
.word	1001319488
.word	1001662976
.word	1002006464
.word	1002350016
.word	1002693632
.word	1003037248
.word	1003380864
.word	1003724544
.word	1004068224
.word	1004411968
.word	1004755712
.word	1005099520
.word	1005443328
.word	1005787136
.word	1006131008
.word	1006474944
.word	1006818880
.word	1007162816
.word	1007506816
.word	1007850816
.word	1008194880
.word	1008538944
.word	1008883072
.word	1009227200
.word	1009571392
.word	1009915584
.word	1010259776
.word	1010604032
.word	1010948352
.word	1011292672
.word	1011636992
.word	1011981376
.word	1012325760
.word	1012670208
.word	1013014656
.word	1013359168
.word	1013703680
.word	1014048192
.word	1014392768
.word	1014737408
.word	1015082048
.word	1015426688
.word	1015771392
.word	1016116096
.word	1016460864
.word	1016805632
.word	1017150464
.word	1017495296
.word	1017840192
.word	1018185088
.word	1018529984
.word	1018874944
.word	1019219968
.word	1019564992
.word	1019910016
.word	1020255104
.word	1020600192
.word	1020945344
.word	1021290496
.word	1021635648
.word	1021980864
.word	1022326144
.word	1022671424
.word	1023016704
.word	1023362048
.word	1023707456
.word	1024052800
.word	1024398272
.word	1024743680
.word	1025089152
.word	1025434688
.word	1025780224
.word	1026125824
.word	1026471424
.word	1026817024
.word	1027162688
.word	1027508352
.word	1027854080
.word	1028199808
.word	1028545600
.word	1028891392
.word	1029237248
.word	1029583104
.word	1029928960
.word	1030274880
.word	1030620864
.word	1030966848
.word	1031312832
.word	1031658880
.word	1032004928
.word	1032351040
.word	1032697152
.word	1033043328
.word	1033389504
.word	1033735680
.word	1034081920
.word	1034428160
.word	1034774464
.word	1035120832
.word	1035467136
.word	1035813568
.word	1036159936
.word	1036506368
.word	1036852864
.word	1037199360
.word	1037545920
.word	1037892480
.word	1038239040
.word	1038585664
.word	1038932288
.word	1039278976
.word	1039625664
.word	1039972416
.word	1040319168
.word	1040665920
.word	1041012736
.word	1041359616
.word	1041706496
.word	1042053376
.word	1042400320
.word	1042747264
.word	1043094272
.word	1043441280
.word	1043788352
.word	1044135424
.word	1044482496
.word	1044829632
.word	1045176832
.word	1045523968
.word	1045871232
.word	1046218496
.word	1046565760
.word	1046913024
.word	1047260416
.word	1047607744
.word	1047955136
.word	1048302592
.word	1048650048
.word	1048997504
.word	1049345024
.word	1049692544
.word	1050040128
.word	1050387712
.word	1050735296
.word	1051083008
.word	1051430656
.word	1051778368
.word	1052126080
.word	1052473856
.word	1052821696
.word	1053169472
.word	1053517376
.word	1053865216
.word	1054213120
.word	1054561088
.word	1054909056
.word	1055257024
.word	1055605056
.word	1055953152
.word	1056301184
.word	1056649344
.word	1056997440
.word	1057345600
.word	1057693824
.word	1058042048
.word	1058390336
.word	1058738624
.word	1059086912
.word	1059435264
.word	1059783616
.word	1060132032
.word	1060480448
.word	1060828864
.word	1061177408
.word	1061525888
.word	1061874432
.word	1062222976
.word	1062571584
.word	1062920256
.word	1063268864
.word	1063617600
.word	1063966272
.word	1064315008
.word	1064663808
.word	1065012608
.word	1065361408
.word	1065710272
.word	1066059136
.word	1066408064
.word	1066756992
.word	1067105984
.word	1067454976
.word	1067804032
.word	1068153088
.word	1068502144
.word	1068851264
.word	1069200384
.word	1069549568
.word	1069898752
.word	1070248000
.word	1070597248
.word	1070946560
.word	1071295872
.word	1071645184
.word	1071994560
.word	1072343936
.word	1072693376
.word	1073042816
.word	1073392320
.word	1073741824
.word	1074091392
.word	1074440960
.word	1074790528
.word	1075140096
.word	1075489792
.word	1075839488
.word	1076189184
.word	1076538880
.word	1076888704
.word	1077238528
.word	1077588352
.word	1077938176
.word	1078288000
.word	1078637952
.word	1078987904
.word	1079337856
.word	1079687808
.word	1080037888
.word	1080387968
.word	1080738048
.word	1081088128
.word	1081438208
.word	1081788416
.word	1082138624
.word	1082488832
.word	1082839040
.word	1083189376
.word	1083539712
.word	1083890048
.word	1084240384
.word	1084590720
.word	1084941184
.word	1085291648
.word	1085642112
.word	1085992576
.word	1086343168
.word	1086693632
.word	1087044224
.word	1087394944
.word	1087745536
.word	1088096256
.word	1088446976
.word	1088797696
.word	1089148416
.word	1089499136
.word	1089849984
.word	1090200832
.word	1090551680
.word	1090902656
.word	1091253504
.word	1091604480
.word	1091955456
.word	1092306560
.word	1092657536
.word	1093008640
.word	1093359744
.word	1093710848
.word	1094061952
.word	1094413184
.word	1094764416
.word	1095115648
.word	1095466880
.word	1095818240
.word	1096169472
.word	1096520832
.word	1096872192
.word	1097223680
.word	1097575040
.word	1097926528
.word	1098278016
.word	1098629504
.word	1098981120
.word	1099332608
.word	1099684224
.word	1100035840
.word	1100387584
.word	1100739200
.word	1101090944
.word	1101442688
.word	1101794432
.word	1102146304
.word	1102498048
.word	1102849920
.word	1103201792
.word	1103553792
.word	1103905664
.word	1104257664
.word	1104609664
.word	1104961664
.word	1105313792
.word	1105665792
.word	1106017920
.word	1106370048
.word	1106722176
.word	1107074432
.word	1107426688
.word	1107778944
.word	1108131200
.word	1108483456
.word	1108835840
.word	1109188224
.word	1109540608
.word	1109892992
.word	1110245376
.word	1110597888
.word	1110950400
.word	1111302912
.word	1111655424
.word	1112008064
.word	1112360704
.word	1112713344
.word	1113065984
.word	1113418752
.word	1113771392
.word	1114124160
.word	1114476928
.word	1114829696
.word	1115182592
.word	1115535488
.word	1115888384
.word	1116241280
.word	1116594176
.word	1116947200
.word	1117300224
.word	1117653248
.word	1118006272
.word	1118359424
.word	1118712448
.word	1119065600
.word	1119418752
.word	1119772032
.word	1120125184
.word	1120478464
.word	1120831744
.word	1121185024
.word	1121538432
.word	1121891712
.word	1122245120
.word	1122598528
.word	1122952064
.word	1123305472
.word	1123659008
.word	1124012544
.word	1124366080
.word	1124719744
.word	1125073280
.word	1125426944
.word	1125780608
.word	1126134272
.word	1126488064
.word	1126841856
.word	1127195648
.word	1127549440
.word	1127903232
.word	1128257152
.word	1128610944
.word	1128964864
.word	1129318912
.word	1129672832
.word	1130026880
.word	1130380928
.word	1130734976
.word	1131089024
.word	1131443200
.word	1131797248
.word	1132151424
.word	1132505728
.word	1132859904
.word	1133214208
.word	1133568384
.word	1133922688
.word	1134277120
.word	1134631424
.word	1134985856
.word	1135340288
.word	1135694720
.word	1136049152
.word	1136403712
.word	1136758144
.word	1137112704
.word	1137467392
.word	1137821952
.word	1138176640
.word	1138531328
.word	1138886016
.word	1139240704
.word	1139595392
.word	1139950208
.word	1140305024
.word	1140659840
.word	1141014784
.word	1141369600
.word	1141724544
.word	1142079488
.word	1142434432
.word	1142789504
.word	1143144448
.word	1143499520
.word	1143854592
.word	1144209792
.word	1144564864
.word	1144920064
.word	1145275264
.word	1145630464
.word	1145985664
.word	1146340992
.word	1146696320
.word	1147051648
.word	1147406976
.word	1147762304
.word	1148117760
.word	1148473216
.word	1148828672
.word	1149184128
.word	1149539712
.word	1149895296
.word	1150250752
.word	1150606464
.word	1150962048
.word	1151317760
.word	1151673344
.word	1152029056
.word	1152384896
.word	1152740608
.word	1153096448
.word	1153452288
.word	1153808128
.word	1154163968
.word	1154519936
.word	1154875776
.word	1155231744
.word	1155587840
.word	1155943808
.word	1156299904
.word	1156655872
.word	1157011968
.word	1157368192
.word	1157724288
.word	1158080512
.word	1158436736
.word	1158792960
.word	1159149184
.word	1159505536
.word	1159861760
.word	1160218112
.word	1160574464
.word	1160930944
.word	1161287296
.word	1161643776
.word	1162000256
.word	1162356736
.word	1162713344
.word	1163069952
.word	1163426432
.word	1163783168
.word	1164139776
.word	1164496384
.word	1164853120
.word	1165209856
.word	1165566592
.word	1165923456
.word	1166280192
.word	1166637056
.word	1166993920
.word	1167350784
.word	1167707776
.word	1168064640
.word	1168421632
.word	1168778624
.word	1169135616
.word	1169492736
.word	1169849856
.word	1170206976
.word	1170564096
.word	1170921216
.word	1171278464
.word	1171635584
.word	1171992832
.word	1172350208
.word	1172707456
.word	1173064832
.word	1173422208
.word	1173779584
.word	1174136960
.word	1174494336
.word	1174851840
.word	1175209344
.word	1175566848
.word	1175924352
.word	1176281984
.word	1176639616
.word	1176997248
.word	1177354880
.word	1177712512
.word	1178070272
.word	1178428032
.word	1178785792
.word	1179143552
.word	1179501312
.word	1179859200
.word	1180217088
.word	1180574976
.word	1180932864
.word	1181290880
.word	1181648896
.word	1182006784
.word	1182364928
.word	1182722944
.word	1183081088
.word	1183439104
.word	1183797248
.word	1184155392
.word	1184513664
.word	1184871936
.word	1185230080
.word	1185588352
.word	1185946752
.word	1186305024
.word	1186663424
.word	1187021824
.word	1187380224
.word	1187738624
.word	1188097152
.word	1188455552
.word	1188814080
.word	1189172736
.word	1189531264
.word	1189889920
.word	1190248448
.word	1190607104
.word	1190965888
.word	1191324544
.word	1191683328
.word	1192041984
.word	1192400896
.word	1192759680
.word	1193118464
.word	1193477376
.word	1193836288
.word	1194195200
.word	1194554112
.word	1194913152
.word	1195272192
.word	1195631232
.word	1195990272
.word	1196349312
.word	1196708480
.word	1197067648
.word	1197426816
.word	1197785984
.word	1198145152
.word	1198504448
.word	1198863744
.word	1199223040
.word	1199582336
.word	1199941760
.word	1200301056
.word	1200660480
.word	1201019904
.word	1201379456
.word	1201738880
.word	1202098432
.word	1202457984
.word	1202817536
.word	1203177088
.word	1203536768
.word	1203896448
.word	1204256128
.word	1204615808
.word	1204975488
.word	1205335296
.word	1205695104
.word	1206054912
.word	1206414720
.word	1206774656
.word	1207134464
.word	1207494400
.word	1207854336
.word	1208214400
.word	1208574336
.word	1208934400
.word	1209294464
.word	1209654528
.word	1210014592
.word	1210374784
.word	1210734976
.word	1211095168
.word	1211455360
.word	1211815552
.word	1212175872
.word	1212536192
.word	1212896512
.word	1213256832
.word	1213617152
.word	1213977600
.word	1214338048
.word	1214698496
.word	1215058944
.word	1215419520
.word	1215780096
.word	1216140544
.word	1216501248
.word	1216861824
.word	1217222400
.word	1217583104
.word	1217943808
.word	1218304512
.word	1218665344
.word	1219026048
.word	1219386880
.word	1219747712
.word	1220108544
.word	1220469504
.word	1220830336
.word	1221191296
.word	1221552256
.word	1221913216
.word	1222274304
.word	1222635392
.word	1222996352
.word	1223357568
.word	1223718656
.word	1224079744
.word	1224440960
.word	1224802176
.word	1225163392
.word	1225524608
.word	1225885952
.word	1226247296
.word	1226608640
.word	1226969984
.word	1227331328
.word	1227692800
.word	1228054272
.word	1228415744
.word	1228777216
.word	1229138688
.word	1229500288
.word	1229861888
.word	1230223488
.word	1230585088
.word	1230946688
.word	1231308416
.word	1231670144
.word	1232031872
.word	1232393600
.word	1232755456
.word	1233117184
.word	1233479040
.word	1233840896
.word	1234202880
.word	1234564736
.word	1234926720
.word	1235288704
.word	1235650688
.word	1236012672
.word	1236374784
.word	1236736896
.word	1237099008
.word	1237461120
.word	1237823232
.word	1238185472
.word	1238547712
.word	1238909952
.word	1239272192
.word	1239634432
.word	1239996800
.word	1240359168
.word	1240721536
.word	1241083904
.word	1241446400
.word	1241808768
.word	1242171264
.word	1242533760
.word	1242896256
.word	1243258880
.word	1243621504
.word	1243984128
.word	1244346752
.word	1244709376
.word	1245072000
.word	1245434752
.word	1245797504
.word	1246160256
.word	1246523136
.word	1246885888
.word	1247248768
.word	1247611648
.word	1247974528
.word	1248337408
.word	1248700416
.word	1249063424
.word	1249426432
.word	1249789440
.word	1250152448
.word	1250515584
.word	1250878720
.word	1251241856
.word	1251604992
.word	1251968256
.word	1252331392
.word	1252694656
.word	1253057920
.word	1253421184
.word	1253784576
.word	1254147968
.word	1254511232
.word	1254874624
.word	1255238144
.word	1255601536
.word	1255965056
.word	1256328576
.word	1256692096
.word	1257055616
.word	1257419264
.word	1257782912
.word	1258146432
.word	1258510208
.word	1258873856
.word	1259237632
.word	1259601280
.word	1259965056
.word	1260328832
.word	1260692736
.word	1261056512
.word	1261420416
.word	1261784320
.word	1262148224
.word	1262512256
.word	1262876160
.word	1263240192
.word	1263604224
.word	1263968256
.word	1264332416
.word	1264696448
.word	1265060608
.word	1265424768
.word	1265788928
.word	1266153216
.word	1266517504
.word	1266881664
.word	1267245952
.word	1267610368
.word	1267974656
.word	1268339072
.word	1268703488
.word	1269067904
.word	1269432320
.word	1269796864
.word	1270161280
.word	1270525824
.word	1270890368
.word	1271255040
.word	1271619584
.word	1271984256
.word	1272348928
.word	1272713600
.word	1273078272
.word	1273443072
.word	1273807744
.word	1274172544
.word	1274537344
.word	1274902272
.word	1275267072
.word	1275632000
.word	1275996928
.word	1276361856
.word	1276726912
.word	1277091840
.word	1277456896
.word	1277821952
.word	1278187008
.word	1278552064
.word	1278917248
.word	1279282432
.word	1279647616
.word	1280012800
.word	1280377984
.word	1280743296
.word	1281108608
.word	1281473920
.word	1281839232
.word	1282204544
.word	1282569984
.word	1282935424
.word	1283300864
.word	1283666304
.word	1284031872
.word	1284397312
.word	1284762880
.word	1285128448
.word	1285494016
.word	1285859712
.word	1286225280
.word	1286590976
.word	1286956672
.word	1287322496
.word	1287688192
.word	1288054016
.word	1288419840
.word	1288785664
.word	1289151488
.word	1289517440
.word	1289883264
.word	1290249216
.word	1290615168
.word	1290981120
.word	1291347200
.word	1291713280
.word	1292079360
.word	1292445440
.word	1292811520
.word	1293177600
.word	1293543808
.word	1293910016
.word	1294276224
.word	1294642432
.word	1295008768
.word	1295375104
.word	1295741440
.word	1296107776
.word	1296474112
.word	1296840576
.word	1297206912
.word	1297573376
.word	1297939840
.word	1298306432
.word	1298672896
.word	1299039488
.word	1299406080
.word	1299772672
.word	1300139392
.word	1300505984
.word	1300872704
.word	1301239424
.word	1301606144
.word	1301972864
.word	1302339712
.word	1302706560
.word	1303073408
.word	1303440256
.word	1303807104
.word	1304174080
.word	1304540928
.word	1304907904
.word	1305275008
.word	1305641984
.word	1306009088
.word	1306376064
.word	1306743168
.word	1307110272
.word	1307477504
.word	1307844608
.word	1308211840
.word	1308579072
.word	1308946304
.word	1309313664
.word	1309680896
.word	1310048256
.word	1310415616
.word	1310782976
.word	1311150464
.word	1311517824
.word	1311885312
.word	1312252800
.word	1312620288
.word	1312987904
.word	1313355392
.word	1313723008
.word	1314090624
.word	1314458240
.word	1314825984
.word	1315193600
.word	1315561344
.word	1315929088
.word	1316296832
.word	1316664704
.word	1317032448
.word	1317400320
.word	1317768192
.word	1318136064
.word	1318504064
.word	1318871936
.word	1319239936
.word	1319607936
.word	1319975936
.word	1320344064
.word	1320712192
.word	1321080192
.word	1321448320
.word	1321816576
.word	1322184704
.word	1322552960
.word	1322921088
.word	1323289344
.word	1323657728
.word	1324025984
.word	1324394368
.word	1324762624
.word	1325131136
.word	1325499520
.word	1325867904
.word	1326236416
.word	1326604928
.word	1326973440
.word	1327341952
.word	1327710464
.word	1328079104
.word	1328447744
.word	1328816384
.word	1329185024
.word	1329553664
.word	1329922432
.word	1330291200
.word	1330659968
.word	1331028736
.word	1331397504
.word	1331766400
.word	1332135296
.word	1332504192
.word	1332873088
.word	1333241984
.word	1333611008
.word	1333980032
.word	1334349056
.word	1334718080
.word	1335087104
.word	1335456256
.word	1335825408
.word	1336194560
.word	1336563712
.word	1336932992
.word	1337302144
.word	1337671424
.word	1338040704
.word	1338409984
.word	1338779392
.word	1339148672
.word	1339518080
.word	1339887488
.word	1340256896
.word	1340626432
.word	1340995840
.word	1341365376
.word	1341734912
.word	1342104448
.word	1342473984
.word	1342843648
.word	1343213312
.word	1343582976
.word	1343952640
.word	1344322304
.word	1344692096
.word	1345061888
.word	1345431680
.word	1345801472
.word	1346171264
.word	1346541184
.word	1346911104
.word	1347281024
.word	1347650944
.word	1348020864
.word	1348390912
.word	1348760832
.word	1349130880
.word	1349501056
.word	1349871104
.word	1350241152
.word	1350611328
.word	1350981504
.word	1351351680
.word	1351721984
.word	1352092160
.word	1352462464
.word	1352832768
.word	1353203072
.word	1353573376
.word	1353943808
.word	1354314240
.word	1354684672
.word	1355055104
.word	1355425536
.word	1355796096
.word	1356166528
.word	1356537088
.word	1356907648
.word	1357278336
.word	1357648896
.word	1358019584
.word	1358390272
.word	1358760960
.word	1359131648
.word	1359502464
.word	1359873152
.word	1360243968
.word	1360614784
.word	1360985728
.word	1361356544
.word	1361727488
.word	1362098432
.word	1362469376
.word	1362840320
.word	1363211392
.word	1363582336
.word	1363953408
.word	1364324480
.word	1364695552
.word	1365066752
.word	1365437952
.word	1365809024
.word	1366180352
.word	1366551552
.word	1366922752
.word	1367294080
.word	1367665408
.word	1368036736
.word	1368408064
.word	1368779392
.word	1369150848
.word	1369522304
.word	1369893760
.word	1370265216
.word	1370636800
.word	1371008256
.word	1371379840
.word	1371751424
.word	1372123008
.word	1372494720
.word	1372866304
.word	1373238016
.word	1373609728
.word	1373981440
.word	1374353152
.word	1374724992
.word	1375096832
.word	1375468672
.word	1375840512
.word	1376212352
.word	1376584320
.word	1376956288
.word	1377328256
.word	1377700224
.word	1378072192
.word	1378444288
.word	1378816256
.word	1379188352
.word	1379560448
.word	1379932672
.word	1380304768
.word	1380676992
.word	1381049216
.word	1381421440
.word	1381793664
.word	1382166016
.word	1382538368
.word	1382910592
.word	1383283072
.word	1383655424
.word	1384027776
.word	1384400256
.word	1384772736
.word	1385145216
.word	1385517696
.word	1385890304
.word	1386262784
.word	1386635392
.word	1387008000
.word	1387380608
.word	1387753344
.word	1388125952
.word	1388498688
.word	1388871424
.word	1389244288
.word	1389617024
.word	1389989888
.word	1390362624
.word	1390735488
.word	1391108480
.word	1391481344
.word	1391854336
.word	1392227200
.word	1392600192
.word	1392973184
.word	1393346304
.word	1393719296
.word	1394092416
.word	1394465536
.word	1394838656
.word	1395211904
.word	1395585024
.word	1395958272
.word	1396331520
.word	1396704768
.word	1397078016
.word	1397451392
.word	1397824640
.word	1398198016
.word	1398571392
.word	1398944896
.word	1399318272
.word	1399691776
.word	1400065280
.word	1400438784
.word	1400812288
.word	1401185792
.word	1401559424
.word	1401933056
.word	1402306688
.word	1402680320
.word	1403054080
.word	1403427712
.word	1403801472
.word	1404175232
.word	1404548992
.word	1404922880
.word	1405296640
.word	1405670528
.word	1406044416
.word	1406418304
.word	1406792320
.word	1407166208
.word	1407540224
.word	1407914240
.word	1408288256
.word	1408662272
.word	1409036416
.word	1409410560
.word	1409784704
.word	1410158848
.word	1410532992
.word	1410907264
.word	1411281408
.word	1411655680
.word	1412029952
.word	1412404352
.word	1412778624
.word	1413153024
.word	1413527424
.word	1413901824
.word	1414276224
.word	1414650624
.word	1415025152
.word	1415399680
.word	1415774208
.word	1416148736
.word	1416523264
.word	1416897920
.word	1417272576
.word	1417647232
.word	1418021888
.word	1418396544
.word	1418771328
.word	1419146112
.word	1419520896
.word	1419895680
.word	1420270464
.word	1420645376
.word	1421020160
.word	1421395072
.word	1421769984
.word	1422145024
.word	1422519936
.word	1422894976
.word	1423270016
.word	1423645056
.word	1424020096
.word	1424395264
.word	1424770304
.word	1425145472
.word	1425520640
.word	1425895808
.word	1426271104
.word	1426646400
.word	1427021568
.word	1427396864
.word	1427772288
.word	1428147584
.word	1428523008
.word	1428898304
.word	1429273728
.word	1429649152
.word	1430024704
.word	1430400128
.word	1430775680
.word	1431151232
.word	1431526784
.word	1431902464
.word	1432278016
.word	1432653696
.word	1433029376
.word	1433405056
.word	1433780736
.word	1434156544
.word	1434532224
.word	1434908032
.word	1435283840
.word	1435659648
.word	1436035584
.word	1436411392
.word	1436787328
.word	1437163264
.word	1437539200
.word	1437915264
.word	1438291200
.word	1438667264
.word	1439043328
.word	1439419392
.word	1439795584
.word	1440171648
.word	1440547840
.word	1440924032
.word	1441300224
.word	1441676416
.word	1442052736
.word	1442429056
.word	1442805248
.word	1443181696
.word	1443558016
.word	1443934336
.word	1444310784
.word	1444687232
.word	1445063680
.word	1445440128
.word	1445816704
.word	1446193152
.word	1446569728
.word	1446946304
.word	1447322880
.word	1447699584
.word	1448076160
.word	1448452864
.word	1448829568
.word	1449206272
.word	1449582976
.word	1449959808
.word	1450336640
.word	1450713344
.word	1451090304
.word	1451467136
.word	1451843968
.word	1452220928
.word	1452597888
.word	1452974848
.word	1453351808
.word	1453728896
.word	1454105856
.word	1454482944
.word	1454860032
.word	1455237120
.word	1455614336
.word	1455991424
.word	1456368640
.word	1456745856
.word	1457123072
.word	1457500416
.word	1457877632
.word	1458254976
.word	1458632320
.word	1459009664
.word	1459387008
.word	1459764480
.word	1460141824
.word	1460519296
.word	1460896768
.word	1461274368
.word	1461651840
.word	1462029440
.word	1462407040
.word	1462784640
.word	1463162240
.word	1463539840
.word	1463917568
.word	1464295168
.word	1464672896
.word	1465050752
.word	1465428480
.word	1465806336
.word	1466184064
.word	1466561920
.word	1466939776
.word	1467317760
.word	1467695616
.word	1468073600
.word	1468451584
.word	1468829568
.word	1469207552
.word	1469585536
.word	1469963648
.word	1470341760
.word	1470719872
.word	1471097984
.word	1471476096
.word	1471854336
.word	1472232576
.word	1472610816
.word	1472989056
.word	1473367296
.word	1473745664
.word	1474124032
.word	1474502272
.word	1474880768
.word	1475259136
.word	1475637504
.word	1476016000
.word	1476394496
.word	1476772992
.word	1477151488
.word	1477530112
.word	1477908608
.word	1478287232
.word	1478665856
.word	1479044480
.word	1479423104
.word	1479801856
.word	1480180608
.word	1480559360
.word	1480938112
.word	1481316864
.word	1481695744
.word	1482074496
.word	1482453376
.word	1482832256
.word	1483211264
.word	1483590144
.word	1483969152
.word	1484348160
.word	1484727168
.word	1485106176
.word	1485485184
.word	1485864320
.word	1486243456
.word	1486622592
.word	1487001728
.word	1487380864
.word	1487760128
.word	1488139264
.word	1488518528
.word	1488897792
.word	1489277184
.word	1489656448
.word	1490035840
.word	1490415232
.word	1490794624
.word	1491174016
.word	1491553408
.word	1491932928
.word	1492312448
.word	1492691968
.word	1493071488
.word	1493451008
.word	1493830656
.word	1494210176
.word	1494589824
.word	1494969600
.word	1495349248
.word	1495728896
.word	1496108672
.word	1496488448
.word	1496868224
.word	1497248000
.word	1497627904
.word	1498007680
.word	1498387584
.word	1498767488
.word	1499147392
.word	1499527424
.word	1499907328
.word	1500287360
.word	1500667392
.word	1501047424
.word	1501427456
.word	1501807616
.word	1502187648
.word	1502567808
.word	1502947968
.word	1503328256
.word	1503708416
.word	1504088704
.word	1504468992
.word	1504849280
.word	1505229568
.word	1505609856
.word	1505990272
.word	1506370560
.word	1506750976
.word	1507131520
.word	1507511936
.word	1507892352
.word	1508272896
.word	1508653440
.word	1509033984
.word	1509414528
.word	1509795200
.word	1510175744
.word	1510556416
.word	1510937088
.word	1511317760
.word	1511698560
.word	1512079232
.word	1512460032
.word	1512840832
.word	1513221632
.word	1513602432
.word	1513983360
.word	1514364288
.word	1514745088
.word	1515126144
.word	1515507072
.word	1515888000
.word	1516269056
.word	1516650112
.word	1517031168
.word	1517412224
.word	1517793280
.word	1518174464
.word	1518555648
.word	1518936704
.word	1519318016
.word	1519699200
.word	1520080384
.word	1520461696
.word	1520843008
.word	1521224320
.word	1521605632
.word	1521987072
.word	1522368384
.word	1522749824
.word	1523131264
.word	1523512704
.word	1523894272
.word	1524275712
.word	1524657280
.word	1525038848
.word	1525420416
.word	1525801984
.word	1526183680
.word	1526565248
.word	1526946944
.word	1527328640
.word	1527710336
.word	1528092160
.word	1528473856
.word	1528855680
.word	1529237504
.word	1529619328
.word	1530001280
.word	1530383104
.word	1530765056
.word	1531147008
.word	1531528960
.word	1531910912
.word	1532292992
.word	1532674944
.word	1533057024
.word	1533439104
.word	1533821184
.word	1534203392
.word	1534585472
.word	1534967680
.word	1535349888
.word	1535732096
.word	1536114304
.word	1536496640
.word	1536878976
.word	1537261312
.word	1537643648
.word	1538025984
.word	1538408320
.word	1538790784
.word	1539173248
.word	1539555712
.word	1539938176
.word	1540320640
.word	1540703232
.word	1541085824
.word	1541468288
.word	1541851008
.word	1542233600
.word	1542616192
.word	1542998912
.word	1543381632
.word	1543764352
.word	1544147072
.word	1544529792
.word	1544912640
.word	1545295488
.word	1545678336
.word	1546061184
.word	1546444032
.word	1546827008
.word	1547209856
.word	1547592832
.word	1547975808
.word	1548358912
.word	1548741888
.word	1549124992
.word	1549508096
.word	1549891200
.word	1550274304
.word	1550657408
.word	1551040640
.word	1551423744
.word	1551806976
.word	1552190208
.word	1552573568
.word	1552956800
.word	1553340160
.word	1553723520
.word	1554106880
.word	1554490240
.word	1554873600
.word	1555257088
.word	1555640576
.word	1556024064
.word	1556407552
.word	1556791040
.word	1557174528
.word	1557558144
.word	1557941760
.word	1558325376
.word	1558708992
.word	1559092736
.word	1559476352
.word	1559860096
.word	1560243840
.word	1560627584
.word	1561011456
.word	1561395200
.word	1561779072
.word	1562162944
.word	1562546816
.word	1562930688
.word	1563314688
.word	1563698560
.word	1564082560
.word	1564466560
.word	1564850560
.word	1565234688
.word	1565618688
.word	1566002816
.word	1566386944
.word	1566771072
.word	1567155200
.word	1567539456
.word	1567923584
.word	1568307840
.word	1568692096
.word	1569076352
.word	1569460736
.word	1569844992
.word	1570229376
.word	1570613760
.word	1570998144
.word	1571382656
.word	1571767040
.word	1572151552
.word	1572536064
.word	1572920576
.word	1573305088
.word	1573689600
.word	1574074240
.word	1574458880
.word	1574843520
.word	1575228160
.word	1575612800
.word	1575997568
.word	1576382208
.word	1576766976
.word	1577151744
.word	1577536640
.word	1577921408
.word	1578306304
.word	1578691072
.word	1579075968
.word	1579460992
.word	1579845888
.word	1580230912
.word	1580615808
.word	1581000832
.word	1581385856
.word	1581770880
.word	1582156032
.word	1582541184
.word	1582926208
.word	1583311360
.word	1583696640
.word	1584081792
.word	1584466944
.word	1584852224
.word	1585237504
.word	1585622784
.word	1586008192
.word	1586393472
.word	1586778880
.word	1587164160
.word	1587549568
.word	1587935104
.word	1588320512
.word	1588706048
.word	1589091456
.word	1589476992
.word	1589862528
.word	1590248192
.word	1590633728
.word	1591019392
.word	1591405056
.word	1591790720
.word	1592176384
.word	1592562048
.word	1592947840
.word	1593333632
.word	1593719296
.word	1594105216
.word	1594491008
.word	1594876800
.word	1595262720
.word	1595648640
.word	1596034560
.word	1596420480
.word	1596806528
.word	1597192448
.word	1597578496
.word	1597964544
.word	1598350592
.word	1598736640
.word	1599122816
.word	1599508864
.word	1599895040
.word	1600281216
.word	1600667392
.word	1601053696
.word	1601439872
.word	1601826176
.word	1602212480
.word	1602598784
.word	1602985216
.word	1603371520
.word	1603757952
.word	1604144384
.word	1604530816
.word	1604917248
.word	1605303680
.word	1605690240
.word	1606076800
.word	1606463232
.word	1606849920
.word	1607236480
.word	1607623040
.word	1608009728
.word	1608396416
.word	1608783104
.word	1609169792
.word	1609556608
.word	1609943296
.word	1610330112
.word	1610716928
.word	1611103744
.word	1611490560
.word	1611877504
.word	1612264320
.word	1612651264
.word	1613038208
.word	1613425152
.word	1613812224
.word	1614199168
.word	1614586240
.word	1614973312
.word	1615360384
.word	1615747456
.word	1616134656
.word	1616521856
.word	1616908928
.word	1617296128
.word	1617683456
.word	1618070656
.word	1618457984
.word	1618845184
.word	1619232512
.word	1619619840
.word	1620007296
.word	1620394624
.word	1620782080
.word	1621169536
.word	1621556992
.word	1621944448
.word	1622331904
.word	1622719488
.word	1623106944
.word	1623494528
.word	1623882112
.word	1624269824
.word	1624657408
.word	1625045120
.word	1625432832
.word	1625820544
.word	1626208256
.word	1626595968
.word	1626983808
.word	1627371520
.word	1627759360
.word	1628147200
.word	1628535168
.word	1628923008
.word	1629310976
.word	1629698944
.word	1630086912
.word	1630474880
.word	1630862848
.word	1631250944
.word	1631638912
.word	1632027008
.word	1632415104
.word	1632803200
.word	1633191424
.word	1633579648
.word	1633967744
.word	1634355968
.word	1634744192
.word	1635132544
.word	1635520768
.word	1635909120
.word	1636297472
.word	1636685824
.word	1637074176
.word	1637462528
.word	1637851008
.word	1638239488
.word	1638627968
.word	1639016448
.word	1639404928
.word	1639793536
.word	1640182016
.word	1640570624
.word	1640959232
.word	1641347840
.word	1641736576
.word	1642125184
.word	1642513920
.word	1642902656
.word	1643291392
.word	1643680128
.word	1644068992
.word	1644457856
.word	1644846592
.word	1645235456
.word	1645624448
.word	1646013312
.word	1646402176
.word	1646791168
.word	1647180160
.word	1647569152
.word	1647958144
.word	1648347264
.word	1648736256
.word	1649125376
.word	1649514496
.word	1649903616
.word	1650292864
.word	1650681984
.word	1651071232
.word	1651460480
.word	1651849728
.word	1652238976
.word	1652628224
.word	1653017600
.word	1653406976
.word	1653796352
.word	1654185728
.word	1654575104
.word	1654964608
.word	1655353984
.word	1655743488
.word	1656132992
.word	1656522496
.word	1656912128
.word	1657301632
.word	1657691264
.word	1658080896
.word	1658470528
.word	1658860160
.word	1659249920
.word	1659639552
.word	1660029312
.word	1660419072
.word	1660808832
.word	1661198720
.word	1661588480
.word	1661978368
.word	1662368256
.word	1662758144
.word	1663148032
.word	1663538048
.word	1663927936
.word	1664317952
.word	1664707968
.word	1665097984
.word	1665488000
.word	1665878144
.word	1666268160
.word	1666658304
.word	1667048448
.word	1667438720
.word	1667828864
.word	1668219008
.word	1668609280
.word	1668999552
.word	1669389824
.word	1669780096
.word	1670170496
.word	1670560896
.word	1670951168
.word	1671341568
.word	1671731968
.word	1672122496
.word	1672512896
.word	1672903424
.word	1673293952
.word	1673684480
.word	1674075008
.word	1674465664
.word	1674856192
.word	1675246848
.word	1675637504
.word	1676028160
.word	1676418816
.word	1676809600
.word	1677200256
.word	1677591040
.word	1677981824
.word	1678372608
.word	1678763520
.word	1679154304
.word	1679545216
.word	1679936128
.word	1680327040
.word	1680717952
.word	1681108992
.word	1681499904
.word	1681890944
.word	1682281984
.word	1682673024
.word	1683064192
.word	1683455232
.word	1683846400
.word	1684237568
.word	1684628736
.word	1685019904
.word	1685411072
.word	1685802368
.word	1686193536
.word	1686584832
.word	1686976128
.word	1687367552
.word	1687758848
.word	1688150272
.word	1688541696
.word	1688933120
.word	1689324544
.word	1689715968
.word	1690107520
.word	1690498944
.word	1690890496
.word	1691282048
.word	1691673600
.word	1692065280
.word	1692456832
.word	1692848512
.word	1693240192
.word	1693631872
.word	1694023552
.word	1694415360
.word	1694807040
.word	1695198848
.word	1695590656
.word	1695982464
.word	1696374400
.word	1696766208
.word	1697158144
.word	1697550080
.word	1697942016
.word	1698333952
.word	1698726016
.word	1699117952
.word	1699510016
.word	1699902080
.word	1700294144
.word	1700686208
.word	1701078400
.word	1701470464
.word	1701862656
.word	1702254848
.word	1702647040
.word	1703039360
.word	1703431552
.word	1703823872
.word	1704216192
.word	1704608512
.word	1705000832
.word	1705393152
.word	1705785600
.word	1706178048
.word	1706570496
.word	1706962944
.word	1707355392
.word	1707747840
.word	1708140416
.word	1708532992
.word	1708925568
.word	1709318144
.word	1709710720
.word	1710103424
.word	1710496128
.word	1710888704
.word	1711281536
.word	1711674240
.word	1712066944
.word	1712459776
.word	1712852480
.word	1713245312
.word	1713638144
.word	1714031104
.word	1714423936
.word	1714816896
.word	1715209856
.word	1715602816
.word	1715995776
.word	1716388736
.word	1716781824
.word	1717174784
.word	1717567872
.word	1717960960
.word	1718354048
.word	1718747264
.word	1719140352
.word	1719533568
.word	1719926784
.word	1720320000
.word	1720713216
.word	1721106560
.word	1721499776
.word	1721893120
.word	1722286464
.word	1722679808
.word	1723073280
.word	1723466624
.word	1723860096
.word	1724253568
.word	1724647040
.word	1725040512
.word	1725433984
.word	1725827584
.word	1726221056
.word	1726614656
.word	1727008256
.word	1727401984
.word	1727795584
.word	1728189312
.word	1728582912
.word	1728976640
.word	1729370496
.word	1729764224
.word	1730157952
.word	1730551808
.word	1730945664
.word	1731339520
.word	1731733376
.word	1732127232
.word	1732521216
.word	1732915072
.word	1733309056
.word	1733703040
.word	1734097152
.word	1734491136
.word	1734885248
.word	1735279232
.word	1735673344
.word	1736067456
.word	1736461696
.word	1736855808
.word	1737250048
.word	1737644160
.word	1738038400
.word	1738432768
.word	1738827008
.word	1739221248
.word	1739615616
.word	1740009984
.word	1740404352
.word	1740798720
.word	1741193088
.word	1741587584
.word	1741982080
.word	1742376576
.word	1742771072
.word	1743165568
.word	1743560064
.word	1743954688
.word	1744349312
.word	1744743936
.word	1745138560
.word	1745533184
.word	1745927808
.word	1746322560
.word	1746717312
.word	1747112064
.word	1747506816
.word	1747901568
.word	1748296448
.word	1748691328
.word	1749086080
.word	1749480960
.word	1749875968
.word	1750270848
.word	1750665856
.word	1751060736
.word	1751455744
.word	1751850752
.word	1752245760
.word	1752640896
.word	1753035904
.word	1753431040
.word	1753826176
.word	1754221312
.word	1754616576
.word	1755011712
.word	1755406976
.word	1755802112
.word	1756197376
.word	1756592768
.word	1756988032
.word	1757383296
.word	1757778688
.word	1758174080
.word	1758569472
.word	1758964864
.word	1759360384
.word	1759755776
.word	1760151296
.word	1760546816
.word	1760942336
.word	1761337856
.word	1761733376
.word	1762129024
.word	1762524672
.word	1762920320
.word	1763315968
.word	1763711616
.word	1764107392
.word	1764503040
.word	1764898816
.word	1765294592
.word	1765690368
.word	1766086144
.word	1766482048
.word	1766877952
.word	1767273728
.word	1767669632
.word	1768065664
.word	1768461568
.word	1768857600
.word	1769253504
.word	1769649536
.word	1770045568
.word	1770441600
.word	1770837760
.word	1771233792
.word	1771629952
.word	1772026112
.word	1772422272
.word	1772818432
.word	1773214720
.word	1773610880
.word	1774007168
.word	1774403456
.word	1774799744
.word	1775196160
.word	1775592448
.word	1775988864
.word	1776385280
.word	1776781696
.word	1777178112
.word	1777574528
.word	1777971072
.word	1778367488
.word	1778764032
.word	1779160576
.word	1779557120
.word	1779953792
.word	1780350336
.word	1780747008
.word	1781143680
.word	1781540352
.word	1781937024
.word	1782333824
.word	1782730496
.word	1783127296
.word	1783524096
.word	1783920896
.word	1784317696
.word	1784714624
.word	1785111424
.word	1785508352
.word	1785905280
.word	1786302208
.word	1786699136
.word	1787096192
.word	1787493248
.word	1787890176
.word	1788287232
.word	1788684416
.word	1789081472
.word	1789478528
.word	1789875712
.word	1790272896
.word	1790670080
.word	1791067264
.word	1791464576
.word	1791861760
.word	1792259072
.word	1792656384
.word	1793053696
.word	1793451008
.word	1793848320
.word	1794245760
.word	1794643200
.word	1795040640
.word	1795438080
.word	1795835520
.word	1796232960
.word	1796630528
.word	1797028096
.word	1797425664
.word	1797823232
.word	1798220800
.word	1798618496
.word	1799016064
.word	1799413760
.word	1799811456
.word	1800209152
.word	1800606976
.word	1801004672
.word	1801402496
.word	1801800320
.word	1802198144
.word	1802595968
.word	1802993792
.word	1803391744
.word	1803789568
.word	1804187520
.word	1804585472
.word	1804983424
.word	1805381504
.word	1805779456
.word	1806177536
.word	1806575616
.word	1806973696
.word	1807371776
.word	1807769984
.word	1808168064
.word	1808566272
.word	1808964480
.word	1809362688
.word	1809760896
.word	1810159232
.word	1810557440
.word	1810955776
.word	1811354112
.word	1811752448
.word	1812150784
.word	1812549248
.word	1812947712
.word	1813346048
.word	1813744512
.word	1814143104
.word	1814541568
.word	1814940032
.word	1815338624
.word	1815737216
.word	1816135808
.word	1816534400
.word	1816932992
.word	1817331712
.word	1817730432
.word	1818129024
.word	1818527744
.word	1818926592
.word	1819325312
.word	1819724160
.word	1820122880
.word	1820521728
.word	1820920576
.word	1821319424
.word	1821718400
.word	1822117248
.word	1822516224
.word	1822915200
.word	1823314176
.word	1823713152
.word	1824112256
.word	1824511232
.word	1824910336
.word	1825309440
.word	1825708544
.word	1826107648
.word	1826506880
.word	1826905984
.word	1827305216
.word	1827704448
.word	1828103680
.word	1828503040
.word	1828902272
.word	1829301632
.word	1829700864
.word	1830100224
.word	1830499712
.word	1830899072
.word	1831298432
.word	1831697920
.word	1832097408
.word	1832496896
.word	1832896384
.word	1833295872
.word	1833695488
.word	1834094976
.word	1834494592
.word	1834894208
.word	1835293952
.word	1835693568
.word	1836093184
.word	1836492928
.word	1836892672
.word	1837292416
.word	1837692160
.word	1838092032
.word	1838491776
.word	1838891648
.word	1839291520
.word	1839691392
.word	1840091264
.word	1840491136
.word	1840891136
.word	1841291136
.word	1841691136
.word	1842091136
.word	1842491136
.word	1842891136
.word	1843291264
.word	1843691392
.word	1844091392
.word	1844491648
.word	1844891776
.word	1845291904
.word	1845692160
.word	1846092416
.word	1846492672
.word	1846892928
.word	1847293184
.word	1847693440
.word	1848093824
.word	1848494208
.word	1848894592
.word	1849294976
.word	1849695360
.word	1850095872
.word	1850496256
.word	1850896768
.word	1851297280
.word	1851697792
.word	1852098304
.word	1852498944
.word	1852899584
.word	1853300096
.word	1853700736
.word	1854101504
.word	1854502144
.word	1854902784
.word	1855303552
.word	1855704320
.word	1856105088
.word	1856505856
.word	1856906624
.word	1857307520
.word	1857708288
.word	1858109184
.word	1858510080
.word	1858911104
.word	1859312000
.word	1859712896
.word	1860113920
.word	1860514944
.word	1860915968
.word	1861316992
.word	1861718016
.word	1862119168
.word	1862520320
.word	1862921472
.word	1863322624
.word	1863723776
.word	1864124928
.word	1864526208
.word	1864927360
.word	1865328640
.word	1865729920
.word	1866131328
.word	1866532608
.word	1866934016
.word	1867335296
.word	1867736704
.word	1868138112
.word	1868539520
.word	1868941056
.word	1869342464
.word	1869744000
.word	1870145536
.word	1870547072
.word	1870948608
.word	1871350272
.word	1871751808
.word	1872153472
.word	1872555136
.word	1872956800
.word	1873358464
.word	1873760256
.word	1874161920
.word	1874563712
.word	1874965504
.word	1875367296
.word	1875769088
.word	1876171008
.word	1876572800
.word	1876974720
.word	1877376640
.word	1877778560
.word	1878180480
.word	1878582528
.word	1878984448
.word	1879386496
.word	1879788544
.word	1880190592
.word	1880592768
.word	1880994816
.word	1881396992
.word	1881799040
.word	1882201216
.word	1882603392
.word	1883005696
.word	1883407872
.word	1883810176
.word	1884212480
.word	1884614784
.word	1885017088
.word	1885419392
.word	1885821696
.word	1886224128
.word	1886626560
.word	1887028992
.word	1887431424
.word	1887833856
.word	1888236416
.word	1888638848
.word	1889041408
.word	1889443968
.word	1889846528
.word	1890249088
.word	1890651776
.word	1891054464
.word	1891457024
.word	1891859712
.word	1892262400
.word	1892665216
.word	1893067904
.word	1893470720
.word	1893873536
.word	1894276352
.word	1894679168
.word	1895081984
.word	1895484928
.word	1895887744
.word	1896290688
.word	1896693632
.word	1897096576
.word	1897499520
.word	1897902592
.word	1898305664
.word	1898708608
.word	1899111680
.word	1899514880
.word	1899917952
.word	1900321024
.word	1900724224
.word	1901127424
.word	1901530624
.word	1901933824
.word	1902337024
.word	1902740352
.word	1903143552
.word	1903546880
.word	1903950208
.word	1904353536
.word	1904756992
.word	1905160320
.word	1905563776
.word	1905967232
.word	1906370688
.word	1906774144
.word	1907177600
.word	1907581184
.word	1907984640
.word	1908388224
.word	1908791808
.word	1909195392
.word	1909598976
.word	1910002688
.word	1910406400
.word	1910809984
.word	1911213696
.word	1911617536
.word	1912021248
.word	1912424960
.word	1912828800
.word	1913232640
.word	1913636480
.word	1914040320
.word	1914444160
.word	1914848128
.word	1915251968
.word	1915655936
.word	1916059904
.word	1916463872
.word	1916867968
.word	1917271936
.word	1917676032
.word	1918080128
.word	1918484224
.word	1918888320
.word	1919292416
.word	1919696512
.word	1920100736
.word	1920504960
.word	1920909184
.word	1921313408
.word	1921717632
.word	1922121984
.word	1922526208
.word	1922930560
.word	1923334912
.word	1923739264
.word	1924143744
.word	1924548096
.word	1924952576
.word	1925357056
.word	1925761536
.word	1926166016
.word	1926570496
.word	1926975104
.word	1927379584
.word	1927784192
.word	1928188800
.word	1928593408
.word	1928998016
.word	1929402752
.word	1929807360
.word	1930212096
.word	1930616832
.word	1931021568
.word	1931426432
.word	1931831168
.word	1932236032
.word	1932640768
.word	1933045632
.word	1933450624
.word	1933855488
.word	1934260352
.word	1934665344
.word	1935070336
.word	1935475328
.word	1935880320
.word	1936285312
.word	1936690304
.word	1937095424
.word	1937500544
.word	1937905664
.word	1938310784
.word	1938715904
.word	1939121152
.word	1939526272
.word	1939931520
.word	1940336768
.word	1940742016
.word	1941147264
.word	1941552640
.word	1941957888
.word	1942363264
.word	1942768640
.word	1943174016
.word	1943579392
.word	1943984896
.word	1944390272
.word	1944795776
.word	1945201280
.word	1945606784
.word	1946012288
.word	1946417920
.word	1946823424
.word	1947229056
.word	1947634688
.word	1948040320
.word	1948445952
.word	1948851712
.word	1949257344
.word	1949663104
.word	1950068864
.word	1950474624
.word	1950880384
.word	1951286272
.word	1951692032
.word	1952097920
.word	1952503808
.word	1952909696
.word	1953315584
.word	1953721472
.word	1954127488
.word	1954533504
.word	1954939392
.word	1955345408
.word	1955751552
.word	1956157568
.word	1956563712
.word	1956969728
.word	1957375872
.word	1957782016
.word	1958188160
.word	1958594432
.word	1959000576
.word	1959406848
.word	1959813120
.word	1960219392
.word	1960625664
.word	1961031936
.word	1961438336
.word	1961844608
.word	1962251008
.word	1962657408
.word	1963063808
.word	1963470336
.word	1963876736
.word	1964283264
.word	1964689792
.word	1965096320
.word	1965502848
.word	1965909376
.word	1966316032
.word	1966722560
.word	1967129216
.word	1967535872
.word	1967942528
.word	1968349312
.word	1968755968
.word	1969162752
.word	1969569408
.word	1969976192
.word	1970382976
.word	1970789888
.word	1971196672
.word	1971603584
.word	1972010496
.word	1972417408
.word	1972824320
.word	1973231232
.word	1973638144
.word	1974045184
.word	1974452224
.word	1974859264
.word	1975266304
.word	1975673344
.word	1976080384
.word	1976487552
.word	1976894720
.word	1977301888
.word	1977709056
.word	1978116224
.word	1978523392
.word	1978930688
.word	1979337984
.word	1979745152
.word	1980152576
.word	1980559872
.word	1980967168
.word	1981374592
.word	1981781888
.word	1982189312
.word	1982596736
.word	1983004160
.word	1983411712
.word	1983819136
.word	1984226688
.word	1984634240
.word	1985041792
.word	1985449344
.word	1985856896
.word	1986264576
.word	1986672256
.word	1987079808
.word	1987487488
.word	1987895296
.word	1988302976
.word	1988710656
.word	1989118464
.word	1989526272
.word	1989934080
.word	1990341888
.word	1990749696
.word	1991157632
.word	1991565440
.word	1991973376
.word	1992381312
.word	1992789248
.word	1993197184
.word	1993605248
.word	1994013312
.word	1994421248
.word	1994829312
.word	1995237376
.word	1995645568
.word	1996053632
.word	1996461824
.word	1996869888
.word	1997278080
.word	1997686272
.word	1998094592
.word	1998502784
.word	1998911104
.word	1999319296
.word	1999727616
.word	2000135936
.word	2000544256
.word	2000952704
.word	2001361024
.word	2001769472
.word	2002177920
.word	2002586368
.word	2002994816
.word	2003403264
.word	2003811840
.word	2004220416
.word	2004628864
.word	2005037440
.word	2005446144
.word	2005854720
.word	2006263296
.word	2006672000
.word	2007080704
.word	2007489408
.word	2007898112
.word	2008306816
.word	2008715648
.word	2009124352
.word	2009533184
.word	2009942016
.word	2010350848
.word	2010759808
.word	2011168640
.word	2011577600
.word	2011986432
.word	2012395392
.word	2012804352
.word	2013213440
.word	2013622400
.word	2014031488
.word	2014440448
.word	2014849536
.word	2015258624
.word	2015667840
.word	2016076928
.word	2016486144
.word	2016895232
.word	2017304448
.word	2017713664
.word	2018122880
.word	2018532224
.word	2018941440
.word	2019350784
.word	2019760128
.word	2020169472
.word	2020578816
.word	2020988160
.word	2021397632
.word	2021806976
.word	2022216448
.word	2022625920
.word	2023035392
.word	2023444992
.word	2023854464
.word	2024264064
.word	2024673664
.word	2025083264
.word	2025492864
.word	2025902464
.word	2026312064
.word	2026721792
.word	2027131520
.word	2027541248
.word	2027950976
.word	2028360704
.word	2028770432
.word	2029180288
.word	2029590144
.word	2030000000
.word	2030409856
.word	2030819712
.word	2031229568
.word	2031639552
.word	2032049408
.word	2032459392
.word	2032869376
.word	2033279488
.word	2033689472
.word	2034099456
.word	2034509568
.word	2034919680
.word	2035329792
.word	2035739904
.word	2036150016
.word	2036560256
.word	2036970368
.word	2037380608
.word	2037790848
.word	2038201088
.word	2038611456
.word	2039021696
.word	2039432064
.word	2039842304
.word	2040252672
.word	2040663040
.word	2041073536
.word	2041483904
.word	2041894400
.word	2042304768
.word	2042715264
.word	2043125760
.word	2043536384
.word	2043946880
.word	2044357376
.word	2044768000
.word	2045178624
.word	2045589248
.word	2045999872
.word	2046410624
.word	2046821248
.word	2047232000
.word	2047642752
.word	2048053504
.word	2048464256
.word	2048875008
.word	2049285888
.word	2049696640
.word	2050107520
.word	2050518400
.word	2050929280
.word	2051340160
.word	2051751168
.word	2052162048
.word	2052573056
.word	2052984064
.word	2053395072
.word	2053806080
.word	2054217216
.word	2054628224
.word	2055039360
.word	2055450496
.word	2055861632
.word	2056272768
.word	2056683904
.word	2057095168
.word	2057506432
.word	2057917696
.word	2058328960
.word	2058740224
.word	2059151488
.word	2059562880
.word	2059974144
.word	2060385536
.word	2060796928
.word	2061208320
.word	2061619712
.word	2062031232
.word	2062442624
.word	2062854144
.word	2063265664
.word	2063677184
.word	2064088704
.word	2064500352
.word	2064911872
.word	2065323520
.word	2065735168
.word	2066146816
.word	2066558464
.word	2066970240
.word	2067381888
.word	2067793664
.word	2068205440
.word	2068617216
.word	2069028992
.word	2069440768
.word	2069852672
.word	2070264448
.word	2070676352
.word	2071088256
.word	2071500160
.word	2071912192
.word	2072324096
.word	2072736128
.word	2073148160
.word	2073560064
.word	2073972224
.word	2074384256
.word	2074796288
.word	2075208448
.word	2075620608
.word	2076032640
.word	2076444800
.word	2076857088
.word	2077269248
.word	2077681536
.word	2078093696
.word	2078505984
.word	2078918272
.word	2079330560
.word	2079742976
.word	2080155264
.word	2080567680
.word	2080980096
.word	2081392512
.word	2081804928
.word	2082217344
.word	2082629760
.word	2083042304
.word	2083454848
.word	2083867392
.word	2084279936
.word	2084692480
.word	2085105152
.word	2085517696
.word	2085930368
.word	2086343040
.word	2086755712
.word	2087168384
.word	2087581056
.word	2087993856
.word	2088406528
.word	2088819328
.word	2089232128
.word	2089644928
.word	2090057856
.word	2090470656
.word	2090883584
.word	2091296512
.word	2091709440
.word	2092122368
.word	2092535296
.word	2092948224
.word	2093361280
.word	2093774336
.word	2094187392
.word	2094600448
.word	2095013504
.word	2095426560
.word	2095839744
.word	2096252928
.word	2096666112
.word	2097079296
.word	2097492480
.word	2097905664
.word	2098318976
.word	2098732160
.word	2099145472
.word	2099558784
.word	2099972096
.word	2100385408
.word	2100798848
.word	2101212288
.word	2101625600
.word	2102039040
.word	2102452480
.word	2102866048
.word	2103279488
.word	2103693056
.word	2104106496
.word	2104520064
.word	2104933632
.word	2105347200
.word	2105760896
.word	2106174464
.word	2106588160
.word	2107001856
.word	2107415552
.word	2107829248
.word	2108242944
.word	2108656768
.word	2109070464
.word	2109484288
.word	2109898112
.word	2110311936
.word	2110725760
.word	2111139712
.word	2111553536
.word	2111967488
.word	2112381440
.word	2112795392
.word	2113209344
.word	2113623424
.word	2114037376
.word	2114451456
.word	2114865536
.word	2115279616
.word	2115693696
.word	2116107776
.word	2116521984
.word	2116936064
.word	2117350272
.word	2117764480
.word	2118178688
.word	2118593024
.word	2119007232
.word	2119421568
.word	2119835776
.word	2120250112
.word	2120664448
.word	2121078912
.word	2121493248
.word	2121907584
.word	2122322048
.word	2122736512
.word	2123150976
.word	2123565440
.word	2123979904
.word	2124394496
.word	2124809088
.word	2125223552
.word	2125638144
.word	2126052736
.word	2126467456
.word	2126882048
.word	2127296768
.word	2127711488
.word	2128126080
.word	2128540928
.word	2128955648
.word	2129370368
.word	2129785216
.word	2130199936
.word	2130614784
.word	2131029632
.word	2131444480
.word	2131859456
.word	2132274304
.word	2132689280
.word	2133104256
.word	2133519232
.word	2133934208
.word	2134349184
.word	2134764160
.word	2135179264
.word	2135594368
.word	2136009472
.word	2136424576
.word	2136839680
.word	2137254784
.word	2137670016
.word	2138085248
.word	2138500352
.word	2138915584
.word	2139330944
.word	2139746176
.word	2140161408
.word	2140576768
.word	2140992128
.word	2141407488
.word	2141822848
.word	2142238208
.word	2142653696
.word	2143069056
.word	2143484544
.word	2143900032
.word	2144315520
.word	2144731008
.word	2145146496
.word	2145562112
.word	2145977728
.word	2146393216
.word	2146808832
.word	2147224576
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.word	2147483647
.section	.rodata.cst8,"aM",@progbits,8
.align	3
$LC11:
.word	0
.word	1072693248
.section	.rodata.cst4,"aM",@progbits,4
.align	2
$LC12:
.word	1082130432
.align	2
$LC13:
.word	1086324736
.align	2
$LC27:
.word	1065353216
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
