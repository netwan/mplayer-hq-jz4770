.file	1 "dsputil.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_sum_c
.type	pix_sum_c, @function
pix_sum_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$14,16			# 0x10
move	$2,$0
$L2:
lbu	$6,0($4)
addiu	$14,$14,-1
lbu	$3,1($4)
lbu	$9,2($4)
addu	$2,$6,$2
lbu	$8,3($4)
lbu	$7,4($4)
addu	$2,$3,$2
lbu	$6,5($4)
lbu	$3,6($4)
addu	$2,$9,$2
lbu	$13,7($4)
lbu	$12,8($4)
addu	$2,$8,$2
lbu	$11,9($4)
lbu	$10,10($4)
addu	$2,$7,$2
lbu	$9,11($4)
lbu	$8,12($4)
addu	$2,$6,$2
lbu	$7,13($4)
lbu	$6,14($4)
addu	$2,$3,$2
lbu	$3,15($4)
addu	$4,$4,$5
addu	$2,$13,$2
addu	$2,$12,$2
addu	$2,$11,$2
addu	$2,$10,$2
addu	$2,$9,$2
addu	$2,$8,$2
addu	$2,$7,$2
addu	$2,$6,$2
bne	$14,$0,$L2
addu	$2,$3,$2

j	$31
nop

.set	macro
.set	reorder
.end	pix_sum_c
.size	pix_sum_c, .-pix_sum_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_norm1_c
.type	pix_norm1_c, @function
pix_norm1_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$8,%hi(ff_squareTbl+1024)
li	$11,16			# 0x10
move	$2,$0
addiu	$8,$8,%lo(ff_squareTbl+1024)
$L6:
lw	$13,0($4)
addiu	$11,$11,-1
lw	$12,4($4)
lw	$10,8($4)
srl	$14,$13,24
lw	$9,12($4)
andi	$3,$13,0xff
sll	$3,$3,2
sll	$14,$14,2
addu	$3,$8,$3
addu	$14,$8,$14
andi	$15,$12,0xff
sll	$15,$15,2
lw	$7,0($3)
srl	$24,$12,24
lw	$6,0($14)
addu	$15,$8,$15
sll	$24,$24,2
andi	$14,$10,0xff
addu	$7,$7,$6
lw	$6,0($15)
addu	$24,$8,$24
sll	$14,$14,2
srl	$15,$10,24
addu	$2,$7,$2
lw	$25,0($24)
addu	$7,$8,$14
sll	$15,$15,2
andi	$24,$9,0xff
addu	$6,$2,$6
lw	$3,0($7)
addu	$15,$8,$15
srl	$14,$9,24
sll	$24,$24,2
addu	$6,$6,$25
lw	$2,0($15)
addu	$24,$8,$24
sll	$14,$14,2
srl	$15,$13,6
addu	$3,$6,$3
lw	$7,0($24)
addu	$14,$8,$14
andi	$15,$15,0x3fc
srl	$13,$13,14
addu	$3,$3,$2
lw	$6,0($14)
addu	$15,$8,$15
srl	$14,$12,6
andi	$13,$13,0x3fc
addu	$2,$3,$7
lw	$7,0($15)
addu	$13,$8,$13
andi	$14,$14,0x3fc
srl	$12,$12,14
addu	$2,$2,$6
lw	$6,0($13)
addu	$14,$8,$14
srl	$13,$10,6
andi	$12,$12,0x3fc
addu	$7,$2,$7
lw	$3,0($14)
addu	$12,$8,$12
andi	$13,$13,0x3fc
srl	$10,$10,14
addu	$7,$7,$6
lw	$2,0($12)
addu	$13,$8,$13
srl	$12,$9,6
andi	$10,$10,0x3fc
addu	$6,$7,$3
lw	$3,0($13)
addu	$10,$8,$10
andi	$7,$12,0x3fc
srl	$9,$9,14
addu	$6,$6,$2
lw	$2,0($10)
addu	$7,$8,$7
andi	$9,$9,0x3fc
addu	$3,$6,$3
addu	$9,$8,$9
lw	$7,0($7)
addu	$3,$3,$2
addu	$4,$4,$5
lw	$6,0($9)
addu	$2,$3,$7
bne	$11,$0,$L6
addu	$2,$2,$6

j	$31
nop

.set	macro
.set	reorder
.end	pix_norm1_c
.size	pix_norm1_c, .-pix_norm1_c
.align	2
.set	nomips16
.set	nomicromips
.ent	bswap_buf
.type	bswap_buf, @function
bswap_buf:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
slt	$2,$6,8
addiu	$4,$4,-4
.set	noreorder
.set	nomacro
bne	$2,$0,$L14
addiu	$5,$5,-4
.set	macro
.set	reorder

addiu	$2,$6,-8
move	$3,$0
srl	$2,$2,3
addiu	$2,$2,1
sll	$7,$2,3
$L10:
#APP
# 222 "dsputil.c" 1
.word	0b01110000101100000000010001010100	#S32LDIR XR1,$5,4
# 0 "" 2
# 223 "dsputil.c" 1
.word	0b01110000101100000000010010010100	#S32LDIR XR2,$5,4
# 0 "" 2
# 224 "dsputil.c" 1
.word	0b01110000101100000000010011010100	#S32LDIR XR3,$5,4
# 0 "" 2
# 225 "dsputil.c" 1
.word	0b01110000101100000000010100010100	#S32LDIR XR4,$5,4
# 0 "" 2
# 226 "dsputil.c" 1
.word	0b01110000101100000000010101010100	#S32LDIR XR5,$5,4
# 0 "" 2
# 227 "dsputil.c" 1
.word	0b01110000101100000000010110010100	#S32LDIR XR6,$5,4
# 0 "" 2
# 228 "dsputil.c" 1
.word	0b01110000101100000000010111010100	#S32LDIR XR7,$5,4
# 0 "" 2
# 229 "dsputil.c" 1
.word	0b01110000101100000000011000010100	#S32LDIR XR8,$5,4
# 0 "" 2
# 230 "dsputil.c" 1
.word	0b01110000100000000000010001010101	#S32SDI XR1,$4,4
# 0 "" 2
# 231 "dsputil.c" 1
.word	0b01110000100000000000010010010101	#S32SDI XR2,$4,4
# 0 "" 2
# 232 "dsputil.c" 1
.word	0b01110000100000000000010011010101	#S32SDI XR3,$4,4
# 0 "" 2
# 233 "dsputil.c" 1
.word	0b01110000100000000000010100010101	#S32SDI XR4,$4,4
# 0 "" 2
# 234 "dsputil.c" 1
.word	0b01110000100000000000010101010101	#S32SDI XR5,$4,4
# 0 "" 2
# 235 "dsputil.c" 1
.word	0b01110000100000000000010110010101	#S32SDI XR6,$4,4
# 0 "" 2
# 236 "dsputil.c" 1
.word	0b01110000100000000000010111010101	#S32SDI XR7,$4,4
# 0 "" 2
# 237 "dsputil.c" 1
.word	0b01110000100000000000011000010101	#S32SDI XR8,$4,4
# 0 "" 2
#NO_APP
addiu	$3,$3,8
bne	$3,$7,$L10
sll	$2,$2,3
$L9:
slt	$3,$2,$6
beq	$3,$0,$L18
$L15:
#APP
# 240 "dsputil.c" 1
.word	0b01110000101100000000011000010100	#S32LDIR XR8,$5,4
# 0 "" 2
# 241 "dsputil.c" 1
.word	0b01110000100000000000011000010101	#S32SDI XR8,$4,4
# 0 "" 2
#NO_APP
addiu	$2,$2,1
bne	$2,$6,$L15
$L18:
j	$31
$L14:
.option	pic0
.set	noreorder
.set	nomacro
j	$L9
.option	pic2
move	$2,$0
.set	macro
.set	reorder

.end	bswap_buf
.size	bswap_buf, .-bswap_buf
.align	2
.set	nomips16
.set	nomicromips
.ent	sse4_c
.type	sse4_c, @function
sse4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,16($sp)
blez	$11,$L22
lui	$12,%hi(ff_squareTbl+1024)

move	$13,$0
move	$2,$0
addiu	$12,$12,%lo(ff_squareTbl+1024)
$L21:
lbu	$4,0($5)
addiu	$13,$13,1
lbu	$3,1($5)
lbu	$10,0($6)
lbu	$9,1($6)
lbu	$8,2($5)
subu	$10,$4,$10
lbu	$14,2($6)
subu	$9,$3,$9
lbu	$4,3($6)
sll	$10,$10,2
lbu	$3,3($5)
sll	$9,$9,2
addu	$10,$12,$10
addu	$9,$12,$9
subu	$8,$8,$14
subu	$4,$3,$4
lw	$3,0($10)
sll	$8,$8,2
lw	$9,0($9)
sll	$4,$4,2
addu	$8,$12,$8
addu	$3,$3,$9
addu	$4,$12,$4
lw	$8,0($8)
addu	$2,$3,$2
addu	$5,$5,$7
lw	$3,0($4)
addu	$6,$6,$7
addu	$2,$2,$8
bne	$13,$11,$L21
addu	$2,$2,$3

j	$31
nop

$L22:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	sse4_c
.size	sse4_c, .-sse4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	sse8_c
.type	sse8_c, @function
sse8_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-16
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
lw	$13,32($sp)
blez	$13,$L27
nop

lui	$9,%hi(ff_squareTbl+1024)
move	$14,$0
move	$2,$0
addiu	$9,$9,%lo(ff_squareTbl+1024)
$L26:
lbu	$8,0($6)
addiu	$14,$14,1
lbu	$3,1($5)
lbu	$4,0($5)
lbu	$11,1($6)
lbu	$10,2($5)
subu	$4,$4,$8
lbu	$25,2($6)
subu	$11,$3,$11
lbu	$8,3($5)
sll	$3,$4,2
lbu	$24,3($6)
sll	$11,$11,2
lbu	$16,4($6)
addu	$3,$9,$3
lbu	$15,4($5)
addu	$11,$9,$11
lbu	$17,5($6)
subu	$25,$10,$25
lbu	$12,5($5)
subu	$24,$8,$24
lw	$8,0($3)
sll	$25,$25,2
lw	$4,0($11)
subu	$15,$15,$16
lbu	$11,6($5)
sll	$24,$24,2
lbu	$18,6($6)
addu	$25,$9,$25
lbu	$10,7($5)
addu	$8,$8,$4
lbu	$16,7($6)
subu	$12,$12,$17
addu	$24,$9,$24
lw	$4,0($25)
sll	$15,$15,2
addu	$2,$8,$2
addu	$15,$9,$15
lw	$17,0($24)
sll	$12,$12,2
subu	$11,$11,$18
addu	$4,$2,$4
lw	$3,0($15)
subu	$8,$10,$16
addu	$12,$9,$12
sll	$11,$11,2
addu	$4,$4,$17
sll	$8,$8,2
lw	$2,0($12)
addu	$10,$9,$11
addu	$3,$4,$3
addu	$4,$9,$8
lw	$8,0($10)
addu	$3,$3,$2
addu	$5,$5,$7
lw	$4,0($4)
addu	$6,$6,$7
addu	$2,$3,$8
bne	$14,$13,$L26
addu	$2,$2,$4

$L25:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

$L27:
.option	pic0
j	$L25
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	sse8_c
.size	sse8_c, .-sse8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	sse16_c
.type	sse16_c, @function
sse16_c:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-24
sw	$21,20($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
lw	$12,40($sp)
blez	$12,$L33
nop

lui	$11,%hi(ff_squareTbl+1024)
move	$13,$0
move	$2,$0
addiu	$11,$11,%lo(ff_squareTbl+1024)
$L32:
lbu	$9,0($6)
addiu	$13,$13,1
lbu	$8,1($6)
lbu	$4,0($5)
lbu	$3,1($5)
lbu	$15,2($6)
subu	$4,$4,$9
lbu	$9,2($5)
subu	$3,$3,$8
lbu	$10,3($5)
sll	$4,$4,2
lbu	$19,3($6)
sll	$3,$3,2
lbu	$8,4($5)
addu	$4,$11,$4
lbu	$25,4($6)
addu	$3,$11,$3
lbu	$14,5($5)
subu	$9,$9,$15
lbu	$18,5($6)
lw	$4,0($4)
subu	$19,$10,$19
sll	$9,$9,2
lw	$10,0($3)
subu	$25,$8,$25
lbu	$16,6($6)
sll	$19,$19,2
lbu	$24,7($6)
addu	$9,$11,$9
lbu	$8,8($5)
lbu	$3,10($5)
addu	$10,$4,$10
lbu	$17,6($5)
subu	$18,$14,$18
lbu	$4,9($5)
addu	$19,$11,$19
lbu	$21,7($5)
sll	$25,$25,2
lbu	$20,8($6)
addu	$2,$10,$2
lbu	$15,9($6)
subu	$17,$17,$16
lbu	$14,10($6)
subu	$16,$21,$24
lw	$9,0($9)
subu	$24,$8,$20
subu	$15,$4,$15
lw	$10,0($19)
subu	$14,$3,$14
addu	$25,$11,$25
sll	$18,$18,2
sll	$4,$16,2
addu	$2,$2,$9
lw	$25,0($25)
sll	$17,$17,2
sll	$3,$24,2
addu	$18,$11,$18
sll	$15,$15,2
sll	$14,$14,2
addu	$16,$11,$17
lw	$18,0($18)
addu	$24,$11,$4
addu	$2,$2,$10
addu	$15,$11,$15
lw	$9,0($16)
addu	$14,$11,$14
lw	$8,0($24)
addu	$3,$11,$3
addu	$10,$2,$25
lw	$4,0($15)
lw	$25,0($14)
lw	$3,0($3)
addu	$10,$10,$18
lbu	$2,11($5)
lbu	$24,11($6)
addu	$9,$10,$9
lbu	$16,12($6)
lbu	$15,12($5)
addu	$9,$9,$8
subu	$24,$2,$24
lbu	$14,13($5)
lbu	$17,13($6)
addu	$8,$9,$3
subu	$15,$15,$16
lbu	$2,14($6)
sll	$24,$24,2
lbu	$10,14($5)
sll	$15,$15,2
lbu	$9,15($5)
addu	$24,$11,$24
lbu	$16,15($6)
subu	$14,$14,$17
addu	$8,$8,$4
subu	$10,$10,$2
lw	$3,0($24)
addu	$15,$11,$15
sll	$14,$14,2
addu	$4,$8,$25
addu	$14,$11,$14
lw	$2,0($15)
subu	$8,$9,$16
sll	$10,$10,2
addu	$4,$4,$3
lw	$14,0($14)
addu	$9,$11,$10
sll	$8,$8,2
addu	$3,$4,$2
addu	$4,$11,$8
lw	$2,0($9)
addu	$3,$3,$14
addu	$5,$5,$7
lw	$4,0($4)
addu	$2,$3,$2
addu	$6,$6,$7
bne	$13,$12,$L32
addu	$2,$2,$4

$L31:
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,24

$L33:
.option	pic0
j	$L31
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	sse16_c
.size	sse16_c, .-sse16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	get_pixels_c
.type	get_pixels_c, @function
get_pixels_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,0($5)
addu	$10,$5,$6
addu	$9,$10,$6
sh	$2,0($4)
addu	$8,$9,$6
lbu	$2,1($5)
addu	$7,$8,$6
addu	$3,$7,$6
sh	$2,2($4)
lbu	$11,2($5)
addu	$2,$3,$6
addu	$6,$2,$6
sh	$11,4($4)
lbu	$11,3($5)
sh	$11,6($4)
lbu	$11,4($5)
sh	$11,8($4)
lbu	$11,5($5)
sh	$11,10($4)
lbu	$11,6($5)
sh	$11,12($4)
lbu	$5,7($5)
sh	$5,14($4)
lbu	$5,0($10)
sh	$5,16($4)
lbu	$5,1($10)
sh	$5,18($4)
lbu	$5,2($10)
sh	$5,20($4)
lbu	$5,3($10)
sh	$5,22($4)
lbu	$5,4($10)
sh	$5,24($4)
lbu	$5,5($10)
sh	$5,26($4)
lbu	$5,6($10)
sh	$5,28($4)
lbu	$5,7($10)
sh	$5,30($4)
lbu	$5,0($9)
sh	$5,32($4)
lbu	$5,1($9)
sh	$5,34($4)
lbu	$5,2($9)
sh	$5,36($4)
lbu	$5,3($9)
sh	$5,38($4)
lbu	$5,4($9)
sh	$5,40($4)
lbu	$5,5($9)
sh	$5,42($4)
lbu	$5,6($9)
sh	$5,44($4)
lbu	$5,7($9)
sh	$5,46($4)
lbu	$5,0($8)
sh	$5,48($4)
lbu	$5,1($8)
sh	$5,50($4)
lbu	$5,2($8)
sh	$5,52($4)
lbu	$5,3($8)
sh	$5,54($4)
lbu	$5,4($8)
sh	$5,56($4)
lbu	$5,5($8)
sh	$5,58($4)
lbu	$5,6($8)
sh	$5,60($4)
lbu	$5,7($8)
sh	$5,62($4)
lbu	$5,0($7)
sh	$5,64($4)
lbu	$5,1($7)
sh	$5,66($4)
lbu	$5,2($7)
sh	$5,68($4)
lbu	$5,3($7)
sh	$5,70($4)
lbu	$5,4($7)
sh	$5,72($4)
lbu	$5,5($7)
sh	$5,74($4)
lbu	$5,6($7)
sh	$5,76($4)
lbu	$5,7($7)
sh	$5,78($4)
lbu	$5,0($3)
sh	$5,80($4)
lbu	$5,1($3)
sh	$5,82($4)
lbu	$5,2($3)
sh	$5,84($4)
lbu	$5,3($3)
sh	$5,86($4)
lbu	$5,4($3)
sh	$5,88($4)
lbu	$5,5($3)
sh	$5,90($4)
lbu	$5,6($3)
sh	$5,92($4)
lbu	$3,7($3)
sh	$3,94($4)
lbu	$3,0($2)
sh	$3,96($4)
lbu	$3,1($2)
sh	$3,98($4)
lbu	$3,2($2)
sh	$3,100($4)
lbu	$3,3($2)
sh	$3,102($4)
lbu	$3,4($2)
sh	$3,104($4)
lbu	$3,5($2)
sh	$3,106($4)
lbu	$3,6($2)
sh	$3,108($4)
lbu	$2,7($2)
sh	$2,110($4)
lbu	$2,0($6)
sh	$2,112($4)
lbu	$2,1($6)
sh	$2,114($4)
lbu	$2,2($6)
sh	$2,116($4)
lbu	$2,3($6)
sh	$2,118($4)
lbu	$2,4($6)
sh	$2,120($4)
lbu	$2,5($6)
sh	$2,122($4)
lbu	$2,6($6)
sh	$2,124($4)
lbu	$2,7($6)
j	$31
sh	$2,126($4)

.set	macro
.set	reorder
.end	get_pixels_c
.size	get_pixels_c, .-get_pixels_c
.align	2
.set	nomips16
.set	nomicromips
.ent	diff_pixels_c
.type	diff_pixels_c, @function
diff_pixels_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$8,$4,128
$L38:
lbu	$3,0($6)
addiu	$4,$4,16
lbu	$2,0($5)
subu	$2,$2,$3
sh	$2,-16($4)
lbu	$3,1($6)
lbu	$2,1($5)
subu	$2,$2,$3
sh	$2,-14($4)
lbu	$3,2($6)
lbu	$2,2($5)
subu	$2,$2,$3
sh	$2,-12($4)
lbu	$3,3($6)
lbu	$2,3($5)
subu	$2,$2,$3
sh	$2,-10($4)
lbu	$3,4($6)
lbu	$2,4($5)
subu	$2,$2,$3
sh	$2,-8($4)
lbu	$3,5($6)
lbu	$2,5($5)
subu	$2,$2,$3
sh	$2,-6($4)
lbu	$3,6($6)
lbu	$2,6($5)
subu	$2,$2,$3
sh	$2,-4($4)
lbu	$2,7($5)
addu	$5,$5,$7
lbu	$3,7($6)
addu	$6,$6,$7
subu	$2,$2,$3
bne	$4,$8,$L38
sh	$2,-2($4)

j	$31
nop

.set	macro
.set	reorder
.end	diff_pixels_c
.size	diff_pixels_c, .-diff_pixels_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels_clamped_c
.type	put_pixels_clamped_c, @function
put_pixels_clamped_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(ff_cropTbl+1024)
addiu	$7,$4,128
addiu	$2,$2,%lo(ff_cropTbl+1024)
$L41:
lh	$3,0($4)
addiu	$4,$4,16
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($5)
lh	$3,-14($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,1($5)
lh	$3,-12($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,2($5)
lh	$3,-10($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,3($5)
lh	$3,-8($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,4($5)
lh	$3,-6($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,5($5)
lh	$3,-4($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,6($5)
lh	$3,-2($4)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,7($5)
bne	$4,$7,$L41
addu	$5,$5,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_pixels_clamped_c
.size	put_pixels_clamped_c, .-put_pixels_clamped_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_signed_pixels_clamped_c
.type	put_signed_pixels_clamped_c, @function
put_signed_pixels_clamped_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$8,$4,128
.option	pic0
j	$L68
.option	pic2
li	$3,-1			# 0xffffffffffffffff

$L71:
beq	$7,$0,$L45
addiu	$2,$2,-128

sb	$2,0($5)
$L46:
lh	$2,2($4)
slt	$7,$2,-128
bne	$7,$0,$L47
slt	$7,$2,128

$L79:
beq	$7,$0,$L48
addiu	$2,$2,-128

sb	$2,1($5)
$L49:
lh	$2,4($4)
slt	$7,$2,-128
bne	$7,$0,$L50
slt	$7,$2,128

$L80:
beq	$7,$0,$L51
addiu	$2,$2,-128

sb	$2,2($5)
$L52:
lh	$2,6($4)
slt	$7,$2,-128
bne	$7,$0,$L53
slt	$7,$2,128

$L81:
beq	$7,$0,$L54
addiu	$2,$2,-128

sb	$2,3($5)
$L55:
lh	$2,8($4)
slt	$7,$2,-128
bne	$7,$0,$L56
slt	$7,$2,128

$L82:
beq	$7,$0,$L57
addiu	$2,$2,-128

sb	$2,4($5)
$L58:
lh	$2,10($4)
slt	$7,$2,-128
bne	$7,$0,$L59
slt	$7,$2,128

$L83:
beq	$7,$0,$L60
addiu	$2,$2,-128

sb	$2,5($5)
$L61:
lh	$2,12($4)
slt	$7,$2,-128
bne	$7,$0,$L62
slt	$7,$2,128

$L84:
beq	$7,$0,$L63
addiu	$2,$2,-128

sb	$2,6($5)
$L64:
lh	$2,14($4)
slt	$7,$2,-128
bne	$7,$0,$L65
slt	$7,$2,128

$L85:
beq	$7,$0,$L66
addiu	$2,$2,-128

sb	$2,7($5)
$L67:
addiu	$4,$4,16
beq	$4,$8,$L86
addu	$5,$5,$6

$L68:
lh	$2,0($4)
slt	$7,$2,-128
beq	$7,$0,$L71
slt	$7,$2,128

sb	$0,0($5)
lh	$2,2($4)
slt	$7,$2,-128
beq	$7,$0,$L79
slt	$7,$2,128

$L47:
sb	$0,1($5)
lh	$2,4($4)
slt	$7,$2,-128
beq	$7,$0,$L80
slt	$7,$2,128

$L50:
sb	$0,2($5)
lh	$2,6($4)
slt	$7,$2,-128
beq	$7,$0,$L81
slt	$7,$2,128

$L53:
sb	$0,3($5)
lh	$2,8($4)
slt	$7,$2,-128
beq	$7,$0,$L82
slt	$7,$2,128

$L56:
sb	$0,4($5)
lh	$2,10($4)
slt	$7,$2,-128
beq	$7,$0,$L83
slt	$7,$2,128

$L59:
sb	$0,5($5)
lh	$2,12($4)
slt	$7,$2,-128
beq	$7,$0,$L84
slt	$7,$2,128

$L62:
sb	$0,6($5)
lh	$2,14($4)
slt	$7,$2,-128
beq	$7,$0,$L85
slt	$7,$2,128

$L65:
addiu	$4,$4,16
sb	$0,7($5)
bne	$4,$8,$L68
addu	$5,$5,$6

$L86:
j	$31
nop

$L45:
.option	pic0
j	$L46
.option	pic2
sb	$3,0($5)

$L57:
.option	pic0
j	$L58
.option	pic2
sb	$3,4($5)

$L54:
.option	pic0
j	$L55
.option	pic2
sb	$3,3($5)

$L51:
.option	pic0
j	$L52
.option	pic2
sb	$3,2($5)

$L48:
.option	pic0
j	$L49
.option	pic2
sb	$3,1($5)

$L66:
.option	pic0
j	$L67
.option	pic2
sb	$3,7($5)

$L63:
.option	pic0
j	$L64
.option	pic2
sb	$3,6($5)

$L60:
.option	pic0
j	$L61
.option	pic2
sb	$3,5($5)

.set	macro
.set	reorder
.end	put_signed_pixels_clamped_c
.size	put_signed_pixels_clamped_c, .-put_signed_pixels_clamped_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels_nonclamped_c
.type	put_pixels_nonclamped_c, @function
put_pixels_nonclamped_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lhu	$2,0($4)
addu	$10,$5,$6
addu	$9,$10,$6
sb	$2,0($5)
addu	$8,$9,$6
lhu	$2,2($4)
addu	$7,$8,$6
addu	$3,$7,$6
sb	$2,1($5)
lhu	$11,4($4)
addu	$2,$3,$6
addu	$6,$2,$6
sb	$11,2($5)
lhu	$11,6($4)
sb	$11,3($5)
lhu	$11,8($4)
sb	$11,4($5)
lhu	$11,10($4)
sb	$11,5($5)
lhu	$11,12($4)
sb	$11,6($5)
lhu	$11,14($4)
sb	$11,7($5)
lhu	$5,16($4)
sb	$5,0($10)
lhu	$5,18($4)
sb	$5,1($10)
lhu	$5,20($4)
sb	$5,2($10)
lhu	$5,22($4)
sb	$5,3($10)
lhu	$5,24($4)
sb	$5,4($10)
lhu	$5,26($4)
sb	$5,5($10)
lhu	$5,28($4)
sb	$5,6($10)
lhu	$5,30($4)
sb	$5,7($10)
lhu	$5,32($4)
sb	$5,0($9)
lhu	$5,34($4)
sb	$5,1($9)
lhu	$5,36($4)
sb	$5,2($9)
lhu	$5,38($4)
sb	$5,3($9)
lhu	$5,40($4)
sb	$5,4($9)
lhu	$5,42($4)
sb	$5,5($9)
lhu	$5,44($4)
sb	$5,6($9)
lhu	$5,46($4)
sb	$5,7($9)
lhu	$5,48($4)
sb	$5,0($8)
lhu	$5,50($4)
sb	$5,1($8)
lhu	$5,52($4)
sb	$5,2($8)
lhu	$5,54($4)
sb	$5,3($8)
lhu	$5,56($4)
sb	$5,4($8)
lhu	$5,58($4)
sb	$5,5($8)
lhu	$5,60($4)
sb	$5,6($8)
lhu	$5,62($4)
sb	$5,7($8)
lhu	$5,64($4)
sb	$5,0($7)
lhu	$5,66($4)
sb	$5,1($7)
lhu	$5,68($4)
sb	$5,2($7)
lhu	$5,70($4)
sb	$5,3($7)
lhu	$5,72($4)
sb	$5,4($7)
lhu	$5,74($4)
sb	$5,5($7)
lhu	$5,76($4)
sb	$5,6($7)
lhu	$5,78($4)
sb	$5,7($7)
lhu	$5,80($4)
sb	$5,0($3)
lhu	$5,82($4)
sb	$5,1($3)
lhu	$5,84($4)
sb	$5,2($3)
lhu	$5,86($4)
sb	$5,3($3)
lhu	$5,88($4)
sb	$5,4($3)
lhu	$5,90($4)
sb	$5,5($3)
lhu	$5,92($4)
sb	$5,6($3)
lhu	$5,94($4)
sb	$5,7($3)
lhu	$3,96($4)
sb	$3,0($2)
lhu	$3,98($4)
sb	$3,1($2)
lhu	$3,100($4)
sb	$3,2($2)
lhu	$3,102($4)
sb	$3,3($2)
lhu	$3,104($4)
sb	$3,4($2)
lhu	$3,106($4)
sb	$3,5($2)
lhu	$3,108($4)
sb	$3,6($2)
lhu	$3,110($4)
sb	$3,7($2)
lhu	$2,112($4)
sb	$2,0($6)
lhu	$2,114($4)
sb	$2,1($6)
lhu	$2,116($4)
sb	$2,2($6)
lhu	$2,118($4)
sb	$2,3($6)
lhu	$2,120($4)
sb	$2,4($6)
lhu	$2,122($4)
sb	$2,5($6)
lhu	$2,124($4)
sb	$2,6($6)
lhu	$2,126($4)
j	$31
sb	$2,7($6)

.set	macro
.set	reorder
.end	put_pixels_nonclamped_c
.size	put_pixels_nonclamped_c, .-put_pixels_nonclamped_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_pixels_clamped_c
.type	add_pixels_clamped_c, @function
add_pixels_clamped_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(ff_cropTbl+1024)
addiu	$13,$4,128
addiu	$2,$2,%lo(ff_cropTbl+1024)
$L89:
lh	$14,0($4)
addiu	$4,$4,16
lbu	$3,0($5)
lbu	$12,1($5)
lbu	$11,2($5)
addu	$14,$3,$14
lbu	$10,3($5)
lbu	$9,4($5)
addu	$14,$2,$14
lbu	$8,5($5)
lbu	$7,6($5)
lbu	$3,7($5)
lbu	$14,0($14)
sb	$14,0($5)
lh	$14,-14($4)
addu	$12,$12,$14
addu	$12,$2,$12
lbu	$12,0($12)
sb	$12,1($5)
lh	$12,-12($4)
addu	$11,$11,$12
addu	$11,$2,$11
lbu	$11,0($11)
sb	$11,2($5)
lh	$11,-10($4)
addu	$10,$10,$11
addu	$10,$2,$10
lbu	$10,0($10)
sb	$10,3($5)
lh	$10,-8($4)
addu	$9,$9,$10
addu	$9,$2,$9
lbu	$9,0($9)
sb	$9,4($5)
lh	$9,-6($4)
addu	$8,$8,$9
addu	$8,$2,$8
lbu	$8,0($8)
sb	$8,5($5)
lh	$8,-4($4)
addu	$7,$7,$8
addu	$7,$2,$7
lbu	$7,0($7)
sb	$7,6($5)
lh	$7,-2($4)
addu	$3,$3,$7
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,7($5)
bne	$4,$13,$L89
addu	$5,$5,$6

j	$31
nop

.set	macro
.set	reorder
.end	add_pixels_clamped_c
.size	add_pixels_clamped_c, .-add_pixels_clamped_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_pixels8_c
.type	add_pixels8_c, @function
add_pixels8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$13,$5,128
$L92:
lbu	$2,0($5)
addiu	$5,$5,16
lbu	$3,0($4)
lbu	$10,1($4)
lbu	$9,2($4)
addu	$2,$2,$3
lbu	$8,3($4)
lbu	$7,4($4)
lbu	$3,5($4)
sb	$2,0($4)
lbu	$11,-14($5)
lbu	$2,6($4)
lbu	$12,7($4)
addu	$11,$11,$10
sb	$11,1($4)
lbu	$10,-12($5)
addu	$10,$10,$9
sb	$10,2($4)
lbu	$9,-10($5)
addu	$9,$9,$8
sb	$9,3($4)
lbu	$8,-8($5)
addu	$8,$8,$7
sb	$8,4($4)
lbu	$7,-6($5)
addu	$7,$7,$3
sb	$7,5($4)
lbu	$3,-4($5)
addu	$3,$3,$2
sb	$3,6($4)
lbu	$2,-2($5)
addu	$2,$2,$12
sb	$2,7($4)
bne	$5,$13,$L92
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	add_pixels8_c
.size	add_pixels8_c, .-add_pixels8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_pixels4_c
.type	add_pixels4_c, @function
add_pixels4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$7,0($5)
addu	$3,$4,$6
lbu	$9,0($4)
lbu	$8,1($4)
addu	$2,$3,$6
lbu	$11,2($4)
addu	$7,$7,$9
lbu	$10,3($4)
addu	$6,$2,$6
sb	$7,0($4)
lbu	$9,2($5)
addu	$9,$9,$8
sb	$9,1($4)
lbu	$8,4($5)
addu	$8,$8,$11
sb	$8,2($4)
lbu	$7,6($5)
addu	$7,$7,$10
sb	$7,3($4)
lbu	$4,8($5)
lbu	$9,0($3)
lbu	$7,1($3)
lbu	$11,2($3)
addu	$9,$4,$9
lbu	$10,3($3)
sb	$9,0($3)
lbu	$8,10($5)
addu	$8,$8,$7
sb	$8,1($3)
lbu	$7,12($5)
addu	$7,$7,$11
sb	$7,2($3)
lbu	$4,14($5)
addu	$4,$4,$10
sb	$4,3($3)
lbu	$7,16($5)
lbu	$4,0($2)
lbu	$3,1($2)
lbu	$8,2($2)
addu	$7,$7,$4
sb	$7,0($2)
lbu	$4,18($5)
addu	$4,$4,$3
sb	$4,1($2)
lbu	$3,20($5)
addu	$3,$3,$8
sb	$3,2($2)
lbu	$3,22($5)
lbu	$4,3($2)
addu	$3,$3,$4
sb	$3,3($2)
lbu	$2,24($5)
lbu	$7,0($6)
lbu	$3,1($6)
lbu	$9,2($6)
addu	$7,$2,$7
lbu	$8,3($6)
sb	$7,0($6)
lbu	$4,26($5)
addu	$4,$4,$3
sb	$4,1($6)
lbu	$3,28($5)
addu	$3,$3,$9
sb	$3,2($6)
lbu	$2,30($5)
addu	$2,$2,$8
j	$31
sb	$2,3($6)

.set	macro
.set	reorder
.end	add_pixels4_c
.size	add_pixels4_c, .-add_pixels4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	sum_abs_dctelem_c
.type	sum_abs_dctelem_c, @function
sum_abs_dctelem_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$7,$4,128
move	$2,$0
$L97:
lh	$3,0($4)
addiu	$4,$4,2
subu	$5,$0,$3
slt	$6,$3,0
movn	$3,$5,$6
bne	$4,$7,$L97
addu	$2,$2,$3

j	$31
nop

.set	macro
.set	reorder
.end	sum_abs_dctelem_c
.size	sum_abs_dctelem_c, .-sum_abs_dctelem_c
.align	2
.set	nomips16
.set	nomicromips
.ent	scale_block_c
.type	scale_block_c, @function
scale_block_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addu	$2,$5,$6
addiu	$7,$4,64
sll	$6,$6,1
$L100:
lbu	$8,0($4)
addiu	$4,$4,8
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,0($2)
sh	$3,0($5)
lbu	$8,-7($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,2($2)
sh	$3,2($5)
lbu	$8,-6($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,4($2)
sh	$3,4($5)
lbu	$8,-5($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,6($2)
sh	$3,6($5)
lbu	$8,-4($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,8($2)
sh	$3,8($5)
lbu	$8,-3($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,10($2)
sh	$3,10($5)
lbu	$8,-2($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,12($2)
sh	$3,12($5)
lbu	$8,-1($4)
sll	$3,$8,8
addu	$3,$3,$8
andi	$3,$3,0xffff
sh	$3,14($2)
addu	$2,$2,$6
sh	$3,14($5)
bne	$4,$7,$L100
addu	$5,$5,$6

j	$31
nop

.set	macro
.set	reorder
.end	scale_block_c
.size	scale_block_c, .-scale_block_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels2_c
.type	avg_pixels2_c, @function
avg_pixels2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L108
move	$9,$0

li	$10,-16908288			# 0xfffffffffefe0000
ori	$10,$10,0xfefe
$L106:
lbu	$3,1($5)
addiu	$9,$9,1
lbu	$8,0($5)
addu	$5,$5,$6
lhu	$2,0($4)
sll	$3,$3,8
or	$3,$3,$8
xor	$8,$2,$3
and	$8,$8,$10
srl	$8,$8,1
or	$2,$2,$3
subu	$2,$2,$8
sh	$2,0($4)
bne	$9,$7,$L106
addu	$4,$4,$6

$L108:
j	$31
nop

.set	macro
.set	reorder
.end	avg_pixels2_c
.size	avg_pixels2_c, .-avg_pixels2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels4_c
.type	avg_pixels4_c, @function
avg_pixels4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L115
move	$8,$0
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
ori	$10,$10,0xfefe
$L113:
lw	$2,0($4)
addiu	$8,$8,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($5)  
lwr $9, 0($5)  

# 0 "" 2
#NO_APP
addu	$5,$5,$6
xor	$3,$2,$9
and	$3,$3,$10
srl	$3,$3,1
or	$2,$2,$9
subu	$2,$2,$3
sw	$2,0($4)
.set	noreorder
.set	nomacro
bne	$8,$7,$L113
addu	$4,$4,$6
.set	macro
.set	reorder

$L115:
j	$31
.end	avg_pixels4_c
.size	avg_pixels4_c, .-avg_pixels4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels8_c
.type	avg_pixels8_c, @function
avg_pixels8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L122
move	$9,$0
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
ori	$11,$11,0xfefe
$L120:
lw	$3,0($4)
addiu	$9,$9,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($5)  
lwr $10, 0($5)  

# 0 "" 2
#NO_APP
lw	$2,4($4)
xor	$8,$3,$10
and	$8,$8,$11
srl	$8,$8,1
or	$3,$3,$10
subu	$3,$3,$8
sw	$3,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 7($5)  
lwr $8, 4($5)  

# 0 "" 2
#NO_APP
xor	$3,$2,$8
and	$3,$3,$11
srl	$3,$3,1
or	$2,$2,$8
subu	$2,$2,$3
addu	$5,$5,$6
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$9,$7,$L120
addu	$4,$4,$6
.set	macro
.set	reorder

$L122:
j	$31
.end	avg_pixels8_c
.size	avg_pixels8_c, .-avg_pixels8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels8_c
.type	avg_no_rnd_pixels8_c, @function
avg_no_rnd_pixels8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
.option	pic0
j	avg_pixels8_c
nop

.option	pic2
.set	macro
.set	reorder
.end	avg_no_rnd_pixels8_c
.size	avg_no_rnd_pixels8_c, .-avg_no_rnd_pixels8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels8_l2
.type	avg_no_rnd_pixels8_l2, @function
avg_no_rnd_pixels8_l2:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$10,24($sp)
lw	$12,16($sp)
.set	noreorder
.set	nomacro
blez	$10,$L129
lw	$13,20($sp)
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
addiu	$5,$5,3
addiu	$6,$6,3
move	$11,$0
ori	$9,$9,0xfefe
$L126:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($5)  
lwr $3, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 0($6)  
lwr $2, -3($6)  

# 0 "" 2
#NO_APP
xor	$8,$2,$3
lw	$15,0($4)
and	$8,$8,$9
lw	$14,4($4)
srl	$8,$8,1
and	$2,$2,$3
addu	$3,$8,$2
addiu	$11,$11,1
xor	$8,$3,$15
and	$8,$8,$9
srl	$8,$8,1
or	$3,$3,$15
subu	$3,$3,$8
sw	$3,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($5)  
lwr $8, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($6)  
lwr $2, 1($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$8
and	$3,$3,$9
srl	$3,$3,1
and	$2,$2,$8
addu	$2,$3,$2
addu	$5,$5,$12
xor	$3,$2,$14
and	$3,$3,$9
srl	$3,$3,1
or	$2,$2,$14
subu	$2,$2,$3
addu	$6,$6,$13
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$11,$10,$L126
addu	$4,$4,$7
.set	macro
.set	reorder

$L129:
j	$31
.end	avg_no_rnd_pixels8_l2
.size	avg_no_rnd_pixels8_l2, .-avg_no_rnd_pixels8_l2
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels8_l2
.type	avg_pixels8_l2, @function
avg_pixels8_l2:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$10,24($sp)
lw	$12,16($sp)
.set	noreorder
.set	nomacro
blez	$10,$L135
lw	$13,20($sp)
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
addiu	$5,$5,3
addiu	$6,$6,3
move	$11,$0
ori	$9,$9,0xfefe
$L132:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($5)  
lwr $3, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($6)  
lwr $8, -3($6)  

# 0 "" 2
#NO_APP
xor	$2,$8,$3
lw	$15,0($4)
and	$2,$2,$9
lw	$14,4($4)
srl	$2,$2,1
or	$8,$8,$3
subu	$3,$8,$2
addiu	$11,$11,1
xor	$8,$3,$15
and	$8,$8,$9
srl	$8,$8,1
or	$3,$3,$15
subu	$3,$3,$8
sw	$3,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($5)  
lwr $2, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($6)  
lwr $3, 1($6)  

# 0 "" 2
#NO_APP
xor	$8,$3,$2
and	$8,$8,$9
srl	$8,$8,1
or	$3,$3,$2
subu	$2,$3,$8
addu	$5,$5,$12
xor	$3,$2,$14
and	$3,$3,$9
srl	$3,$3,1
or	$2,$2,$14
subu	$2,$2,$3
addu	$6,$6,$13
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$11,$10,$L132
addu	$4,$4,$7
.set	macro
.set	reorder

$L135:
j	$31
.end	avg_pixels8_l2
.size	avg_pixels8_l2, .-avg_pixels8_l2
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels8_x2_c
.type	avg_no_rnd_pixels8_x2_c, @function
avg_no_rnd_pixels8_x2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addiu	$6,$5,1
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_no_rnd_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_no_rnd_pixels8_x2_c
.size	avg_no_rnd_pixels8_x2_c, .-avg_no_rnd_pixels8_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels8_x2_c
.type	avg_pixels8_x2_c, @function
avg_pixels8_x2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addiu	$6,$5,1
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_pixels8_x2_c
.size	avg_pixels8_x2_c, .-avg_pixels8_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels8_y2_c
.type	avg_no_rnd_pixels8_y2_c, @function
avg_no_rnd_pixels8_y2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addu	$6,$5,$6
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_no_rnd_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_no_rnd_pixels8_y2_c
.size	avg_no_rnd_pixels8_y2_c, .-avg_no_rnd_pixels8_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels8_y2_c
.type	avg_pixels8_y2_c, @function
avg_pixels8_y2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addu	$6,$5,$6
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_pixels8_y2_c
.size	avg_pixels8_y2_c, .-avg_pixels8_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels8_l4
.type	avg_pixels8_l4, @function
avg_pixels8_l4:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-40
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
sw	$fp,36($sp)
sw	$23,32($sp)
sw	$22,28($sp)
sw	$21,24($sp)
sw	$20,20($sp)
lw	$24,80($sp)
lw	$25,60($sp)
lw	$16,64($sp)
lw	$17,68($sp)
lw	$18,72($sp)
.set	noreorder
.set	nomacro
blez	$24,$L144
lw	$19,76($sp)
.set	macro
.set	reorder

lw	$11,56($sp)
li	$10,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$14,252641280			# 0xf0f0000
li	$9,-50593792			# 0xfffffffffcfc0000
li	$13,-16908288			# 0xfffffffffefe0000
addiu	$5,$5,3
addiu	$6,$6,3
addiu	$7,$7,3
addiu	$11,$11,3
move	$12,$0
addiu	$10,$10,771
addiu	$15,$15,514
addiu	$14,$14,3855
ori	$9,$9,0xfcfc
ori	$13,$13,0xfefe
$L146:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 0($5)  
lwr $fp, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 0($6)  
lwr $2, -3($6)  

# 0 "" 2
#NO_APP
and	$3,$fp,$10
lw	$22,0($4)
and	$21,$2,$10
lw	$20,4($4)
addu	$21,$3,$21
and	$fp,$fp,$9
addu	$21,$21,$15
and	$2,$2,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($7)  
lwr $8, -3($7)  

# 0 "" 2
#NO_APP
srl	$3,$fp,2
srl	$2,$2,2
and	$23,$8,$10
addu	$23,$21,$23
and	$8,$8,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 0($11)  
lwr $fp, -3($11)  

# 0 "" 2
#NO_APP
addu	$21,$2,$3
srl	$8,$8,2
and	$2,$fp,$10
addu	$23,$23,$2
and	$fp,$fp,$9
addu	$2,$21,$8
srl	$3,$fp,2
srl	$21,$23,2
addu	$8,$2,$3
and	$2,$21,$14
addu	$3,$8,$2
addiu	$12,$12,1
xor	$2,$3,$22
and	$2,$2,$13
srl	$2,$2,1
or	$3,$3,$22
subu	$3,$3,$2
sw	$3,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 4($5)  
lwr $21, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 4($6)  
lwr $22, 1($6)  

# 0 "" 2
#NO_APP
and	$2,$21,$10
and	$3,$22,$10
addu	$2,$2,$3
and	$22,$22,$9
and	$21,$21,$9
addu	$2,$2,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $23, 4($7)  
lwr $23, 1($7)  

# 0 "" 2
#NO_APP
srl	$21,$21,2
and	$3,$23,$10
srl	$22,$22,2
and	$23,$23,$9
addu	$2,$2,$3
addu	$22,$22,$21
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($11)  
lwr $3, 1($11)  

# 0 "" 2
#NO_APP
srl	$8,$23,2
and	$23,$3,$10
and	$21,$3,$9
addu	$2,$2,$23
addu	$8,$22,$8
srl	$3,$21,2
srl	$2,$2,2
addu	$3,$8,$3
and	$2,$2,$14
addu	$2,$3,$2
addu	$5,$5,$16
xor	$3,$2,$20
and	$3,$3,$13
srl	$3,$3,1
or	$2,$2,$20
subu	$2,$2,$3
addu	$6,$6,$17
addu	$7,$7,$18
sw	$2,4($4)
addu	$11,$11,$19
.set	noreorder
.set	nomacro
bne	$12,$24,$L146
addu	$4,$4,$25
.set	macro
.set	reorder

$L144:
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

.end	avg_pixels8_l4
.size	avg_pixels8_l4, .-avg_pixels8_l4
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels4_y2_c
.type	avg_pixels4_y2_c, @function
avg_pixels4_y2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L155
li	$13,-16908288			# 0xfffffffffefe0000
.set	macro
.set	reorder

move	$3,$0
move	$9,$0
addu	$14,$5,$6
ori	$13,$13,0xfefe
$L151:
addu	$11,$6,$3
addu	$10,$4,$3
addu	$8,$14,$3
addu	$2,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
lw	$12,0($10)
addu	$3,$5,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($8)  
lwr $15, 0($3)  

# 0 "" 2
#NO_APP
xor	$8,$15,$2
and	$8,$8,$13
srl	$8,$8,1
or	$3,$15,$2
subu	$2,$3,$8
addiu	$9,$9,1
xor	$3,$2,$12
and	$3,$3,$13
srl	$3,$3,1
or	$2,$2,$12
subu	$2,$2,$3
move	$3,$11
.set	noreorder
.set	nomacro
bne	$9,$7,$L151
sw	$2,0($10)
.set	macro
.set	reorder

$L155:
j	$31
.end	avg_pixels4_y2_c
.size	avg_pixels4_y2_c, .-avg_pixels4_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels2_y2_c
.type	avg_pixels2_y2_c, @function
avg_pixels2_y2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L161
li	$14,-16908288			# 0xfffffffffefe0000

move	$11,$0
move	$12,$0
ori	$14,$14,0xfefe
$L157:
addu	$3,$6,$11
addu	$2,$5,$11
addu	$9,$5,$3
addu	$13,$4,$11
lbu	$10,1($2)
move	$11,$3
lbu	$8,1($9)
addiu	$12,$12,1
lbu	$3,0($2)
lbu	$15,0($9)
sll	$10,$10,8
sll	$8,$8,8
lhu	$9,0($13)
or	$10,$10,$3
or	$3,$8,$15
xor	$2,$3,$10
and	$2,$2,$14
srl	$2,$2,1
or	$3,$3,$10
subu	$2,$3,$2
xor	$3,$9,$2
and	$3,$3,$14
srl	$3,$3,1
or	$2,$9,$2
subu	$2,$2,$3
bne	$12,$7,$L157
sh	$2,0($13)

$L161:
j	$31
nop

.set	macro
.set	reorder
.end	avg_pixels2_y2_c
.size	avg_pixels2_y2_c, .-avg_pixels2_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels2_xy2_c
.type	avg_pixels2_xy2_c, @function
avg_pixels2_xy2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,1($5)
lbu	$8,0($5)
lbu	$3,2($5)
addu	$5,$5,$6
addiu	$2,$2,2
addu	$8,$8,$2
blez	$7,$L167
addu	$2,$3,$2

sll	$12,$6,1
addu	$11,$5,$6
move	$13,$0
addu	$6,$4,$6
$L164:
lbu	$9,1($5)
addiu	$13,$13,2
lbu	$3,2($5)
lbu	$10,0($5)
slt	$14,$13,$7
addu	$5,$5,$12
addu	$10,$10,$9
addu	$9,$3,$9
addu	$8,$10,$8
addu	$2,$9,$2
sra	$8,$8,2
sra	$2,$2,2
sb	$8,0($4)
sb	$2,1($4)
addu	$4,$4,$12
lbu	$2,1($11)
lbu	$8,0($11)
lbu	$3,2($11)
addu	$11,$11,$12
addiu	$2,$2,2
addu	$8,$8,$2
addu	$2,$3,$2
addu	$10,$10,$8
addu	$9,$9,$2
sra	$10,$10,2
sra	$9,$9,2
sb	$10,0($6)
sb	$9,1($6)
bne	$14,$0,$L164
addu	$6,$6,$12

$L167:
j	$31
nop

.set	macro
.set	reorder
.end	avg_pixels2_xy2_c
.size	avg_pixels2_xy2_c, .-avg_pixels2_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels4_xy2_c
.type	avg_pixels4_xy2_c, @function
avg_pixels4_xy2_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
li	$11,50528256			# 0x3030000
li	$10,-50593792			# 0xfffffffffcfc0000
addiu	$11,$11,771
ori	$10,$10,0xfcfc
li	$15,33685504			# 0x2020000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $24, 4($5)  
lwr $24, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$11
and	$8,$24,$11
and	$3,$3,$10
and	$24,$24,$10
addu	$2,$2,$8
addiu	$15,$15,514
srl	$24,$24,2
srl	$3,$3,2
addu	$2,$2,$15
addu	$24,$24,$3
addu	$5,$5,$6
.set	noreorder
.set	nomacro
blez	$7,$L176
move	$12,$0
.set	macro
.set	reorder

li	$14,252641280			# 0xf0f0000
addiu	$sp,$sp,-16
li	$13,-16908288			# 0xfffffffffefe0000
addiu	$14,$14,3855
sw	$18,12($sp)
sw	$17,8($sp)
ori	$13,$13,0xfefe
sw	$16,4($sp)
$L172:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $16, 3($5)  
lwr $16, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($5)  
lwr $8, 1($5)  

# 0 "" 2
#NO_APP
and	$3,$16,$11
lw	$17,0($4)
and	$9,$8,$11
addu	$9,$9,$3
and	$8,$8,$10
addu	$3,$2,$9
and	$16,$16,$10
srl	$16,$16,2
srl	$8,$8,2
srl	$3,$3,2
addu	$8,$8,$16
and	$3,$3,$14
addu	$3,$3,$8
addu	$25,$4,$6
addu	$2,$3,$24
addu	$18,$5,$6
xor	$3,$2,$17
and	$3,$3,$13
srl	$3,$3,1
or	$2,$2,$17
subu	$2,$2,$3
addiu	$12,$12,2
addu	$5,$18,$6
sw	$2,0($4)
slt	$16,$12,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($18)  
lwr $4, 0($18)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $24, 4($18)  
lwr $24, 1($18)  

# 0 "" 2
#NO_APP
and	$2,$4,$11
lw	$17,0($25)
and	$3,$24,$11
addu	$2,$2,$3
and	$24,$24,$10
addu	$2,$2,$15
and	$4,$4,$10
srl	$4,$4,2
srl	$24,$24,2
addu	$9,$2,$9
addu	$24,$24,$4
srl	$9,$9,2
addu	$8,$24,$8
and	$9,$9,$14
addu	$3,$8,$9
addu	$4,$25,$6
xor	$8,$3,$17
and	$8,$8,$13
srl	$8,$8,1
or	$3,$3,$17
subu	$3,$3,$8
.set	noreorder
.set	nomacro
bne	$16,$0,$L172
sw	$3,0($25)
.set	macro
.set	reorder

lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,16
$L176:
j	$31
.end	avg_pixels4_xy2_c
.size	avg_pixels4_xy2_c, .-avg_pixels4_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels8_xy2_c
.type	avg_pixels8_xy2_c, @function
avg_pixels8_xy2_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
addiu	$2,$7,-1
addiu	$sp,$sp,-40
srl	$2,$2,1
sw	$18,12($sp)
nor	$18,$0,$7
sw	$17,8($sp)
mul	$18,$18,$6
sll	$17,$6,1
sw	$19,16($sp)
addiu	$2,$2,1
sw	$16,4($sp)
mul	$3,$6,$7
sw	$21,24($sp)
mul	$17,$2,$17
sw	$20,20($sp)
li	$19,4			# 0x4
sw	$fp,36($sp)
li	$13,50528256			# 0x3030000
sw	$23,32($sp)
li	$16,33685504			# 0x2020000
sw	$22,28($sp)
li	$25,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
li	$24,-16908288			# 0xfffffffffefe0000
addiu	$18,$18,4
subu	$19,$19,$3
li	$20,2			# 0x2
addiu	$13,$13,771
addiu	$16,$16,514
addiu	$25,$25,3855
li	$21,1			# 0x1
ori	$12,$12,0xfcfc
ori	$24,$24,0xfefe
$L180:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $23, 4($5)  
lwr $23, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$13
and	$8,$23,$13
and	$3,$3,$12
and	$23,$23,$12
addu	$2,$2,$8
srl	$23,$23,2
srl	$3,$3,2
addu	$2,$2,$16
addu	$23,$23,$3
.set	noreorder
.set	nomacro
blez	$7,$L178
addu	$5,$5,$6
.set	macro
.set	reorder

move	$15,$0
move	$10,$5
move	$14,$4
$L179:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($10)  
lwr $3, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($10)  
lwr $8, 1($10)  

# 0 "" 2
#NO_APP
and	$9,$3,$13
lw	$22,0($14)
and	$11,$8,$13
addu	$9,$11,$9
and	$8,$8,$12
addu	$2,$2,$9
and	$3,$3,$12
srl	$3,$3,2
srl	$8,$8,2
srl	$2,$2,2
addu	$8,$8,$3
and	$2,$2,$25
addu	$3,$2,$8
addu	$11,$10,$6
addu	$2,$3,$23
addu	$3,$14,$6
xor	$10,$2,$22
and	$10,$10,$24
srl	$10,$10,1
or	$2,$2,$22
subu	$2,$2,$10
addu	$10,$11,$6
addiu	$15,$15,2
sw	$2,0($14)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($11)  
lwr $14, 0($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $23, 4($11)  
lwr $23, 1($11)  

# 0 "" 2
#NO_APP
and	$2,$14,$13
lw	$11,0($3)
and	$fp,$23,$13
addu	$2,$2,$fp
and	$23,$23,$12
addu	$2,$2,$16
and	$14,$14,$12
srl	$14,$14,2
srl	$fp,$23,2
addu	$9,$2,$9
addu	$23,$fp,$14
srl	$9,$9,2
addu	$8,$23,$8
and	$9,$9,$25
addu	$8,$8,$9
slt	$22,$15,$7
xor	$9,$8,$11
and	$9,$9,$24
srl	$9,$9,1
or	$8,$8,$11
subu	$8,$8,$9
addu	$14,$3,$6
.set	noreorder
.set	nomacro
bne	$22,$0,$L179
sw	$8,0($3)
.set	macro
.set	reorder

addu	$5,$5,$17
addu	$4,$4,$17
$L178:
addu	$5,$5,$18
.set	noreorder
.set	nomacro
bne	$20,$21,$L181
addu	$4,$4,$19
.set	macro
.set	reorder

lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L181:
.option	pic0
.set	noreorder
.set	nomacro
j	$L180
.option	pic2
li	$20,1			# 0x1
.set	macro
.set	reorder

.end	avg_pixels8_xy2_c
.size	avg_pixels8_xy2_c, .-avg_pixels8_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels8_xy2_c
.type	avg_no_rnd_pixels8_xy2_c, @function
avg_no_rnd_pixels8_xy2_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
addiu	$2,$7,-1
addiu	$sp,$sp,-40
srl	$2,$2,1
sw	$18,12($sp)
nor	$18,$0,$7
sw	$17,8($sp)
mul	$18,$18,$6
sll	$17,$6,1
sw	$19,16($sp)
addiu	$2,$2,1
sw	$16,4($sp)
mul	$3,$6,$7
sw	$21,24($sp)
mul	$17,$2,$17
sw	$20,20($sp)
li	$19,4			# 0x4
sw	$fp,36($sp)
li	$13,50528256			# 0x3030000
sw	$23,32($sp)
li	$16,16842752			# 0x1010000
sw	$22,28($sp)
li	$25,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
li	$24,-16908288			# 0xfffffffffefe0000
addiu	$18,$18,4
subu	$19,$19,$3
li	$20,2			# 0x2
addiu	$13,$13,771
addiu	$16,$16,257
addiu	$25,$25,3855
li	$21,1			# 0x1
ori	$12,$12,0xfcfc
ori	$24,$24,0xfefe
$L187:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $23, 4($5)  
lwr $23, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$13
and	$8,$23,$13
and	$3,$3,$12
and	$23,$23,$12
addu	$2,$2,$8
srl	$23,$23,2
srl	$3,$3,2
addu	$2,$2,$16
addu	$23,$23,$3
.set	noreorder
.set	nomacro
blez	$7,$L185
addu	$5,$5,$6
.set	macro
.set	reorder

move	$15,$0
move	$10,$5
move	$14,$4
$L186:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($10)  
lwr $3, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($10)  
lwr $8, 1($10)  

# 0 "" 2
#NO_APP
and	$9,$3,$13
lw	$22,0($14)
and	$11,$8,$13
addu	$9,$11,$9
and	$8,$8,$12
addu	$2,$2,$9
and	$3,$3,$12
srl	$3,$3,2
srl	$8,$8,2
srl	$2,$2,2
addu	$8,$8,$3
and	$2,$2,$25
addu	$3,$2,$8
addu	$11,$10,$6
addu	$2,$3,$23
addu	$3,$14,$6
xor	$10,$2,$22
and	$10,$10,$24
srl	$10,$10,1
or	$2,$2,$22
subu	$2,$2,$10
addu	$10,$11,$6
addiu	$15,$15,2
sw	$2,0($14)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($11)  
lwr $14, 0($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $23, 4($11)  
lwr $23, 1($11)  

# 0 "" 2
#NO_APP
and	$2,$14,$13
lw	$11,0($3)
and	$fp,$23,$13
addu	$2,$2,$fp
and	$23,$23,$12
addu	$2,$2,$16
and	$14,$14,$12
srl	$14,$14,2
srl	$fp,$23,2
addu	$9,$2,$9
addu	$23,$fp,$14
srl	$9,$9,2
addu	$8,$23,$8
and	$9,$9,$25
addu	$8,$8,$9
slt	$22,$15,$7
xor	$9,$8,$11
and	$9,$9,$24
srl	$9,$9,1
or	$8,$8,$11
subu	$8,$8,$9
addu	$14,$3,$6
.set	noreorder
.set	nomacro
bne	$22,$0,$L186
sw	$8,0($3)
.set	macro
.set	reorder

addu	$5,$5,$17
addu	$4,$4,$17
$L185:
addu	$5,$5,$18
.set	noreorder
.set	nomacro
bne	$20,$21,$L188
addu	$4,$4,$19
.set	macro
.set	reorder

lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L188:
.option	pic0
.set	noreorder
.set	nomacro
j	$L187
.option	pic2
li	$20,1			# 0x1
.set	macro
.set	reorder

.end	avg_no_rnd_pixels8_xy2_c
.size	avg_no_rnd_pixels8_xy2_c, .-avg_no_rnd_pixels8_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels16_c
.type	avg_pixels16_c, @function
avg_pixels16_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,40($sp)
move	$19,$4
sw	$18,36($sp)
move	$18,$5
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_pixels8_c
.option	pic2
move	$16,$7

addiu	$4,$19,8
lw	$31,44($sp)
addiu	$5,$18,8
lw	$19,40($sp)
move	$6,$17
lw	$18,36($sp)
move	$7,$16
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	avg_pixels8_c
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_pixels16_c
.size	avg_pixels16_c, .-avg_pixels16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels16_x2_c
.type	avg_pixels16_x2_c, @function
avg_pixels16_x2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addiu	$6,$5,1
sw	$19,56($sp)
move	$19,$4
sw	$18,52($sp)
move	$18,$7
sw	$17,48($sp)
move	$17,$5
sw	$16,20($sp)
sw	$7,24($sp)
sw	$31,60($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

addiu	$4,$19,8
addiu	$5,$17,8
sw	$16,16($sp)
addiu	$6,$17,9
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_pixels16_x2_c
.size	avg_pixels16_x2_c, .-avg_pixels16_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels16_y2_c
.type	avg_pixels16_y2_c, @function
avg_pixels16_y2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$18,52($sp)
move	$18,$7
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addu	$6,$5,$6
sw	$17,48($sp)
move	$7,$16
move	$17,$5
sw	$16,20($sp)
sw	$18,24($sp)
sw	$31,60($sp)
sw	$19,56($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$19,$4

addiu	$5,$17,8
addiu	$4,$19,8
sw	$16,16($sp)
addu	$6,$5,$16
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_pixels16_y2_c
.size	avg_pixels16_y2_c, .-avg_pixels16_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels16_xy2_c
.type	avg_pixels16_xy2_c, @function
avg_pixels16_xy2_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,40($sp)
move	$19,$4
sw	$18,36($sp)
move	$18,$5
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_pixels8_xy2_c
.option	pic2
move	$16,$7

addiu	$4,$19,8
lw	$31,44($sp)
addiu	$5,$18,8
lw	$19,40($sp)
move	$6,$17
lw	$18,36($sp)
move	$7,$16
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	avg_pixels8_xy2_c
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_pixels16_xy2_c
.size	avg_pixels16_xy2_c, .-avg_pixels16_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels16_c
.type	avg_no_rnd_pixels16_c, @function
avg_no_rnd_pixels16_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,40($sp)
move	$19,$4
sw	$18,36($sp)
move	$18,$5
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_pixels8_c
.option	pic2
move	$16,$7

addiu	$4,$19,8
lw	$31,44($sp)
addiu	$5,$18,8
lw	$19,40($sp)
move	$6,$17
lw	$18,36($sp)
move	$7,$16
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	avg_pixels8_c
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_no_rnd_pixels16_c
.size	avg_no_rnd_pixels16_c, .-avg_no_rnd_pixels16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels16_x2_c
.type	avg_no_rnd_pixels16_x2_c, @function
avg_no_rnd_pixels16_x2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addiu	$6,$5,1
sw	$19,56($sp)
move	$19,$4
sw	$18,52($sp)
move	$18,$7
sw	$17,48($sp)
move	$17,$5
sw	$16,20($sp)
sw	$7,24($sp)
sw	$31,60($sp)
.option	pic0
jal	avg_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

addiu	$4,$19,8
addiu	$5,$17,8
sw	$16,16($sp)
addiu	$6,$17,9
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	avg_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_no_rnd_pixels16_x2_c
.size	avg_no_rnd_pixels16_x2_c, .-avg_no_rnd_pixels16_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels16_y2_c
.type	avg_no_rnd_pixels16_y2_c, @function
avg_no_rnd_pixels16_y2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$18,52($sp)
move	$18,$7
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addu	$6,$5,$6
sw	$17,48($sp)
move	$7,$16
move	$17,$5
sw	$16,20($sp)
sw	$18,24($sp)
sw	$31,60($sp)
sw	$19,56($sp)
.option	pic0
jal	avg_no_rnd_pixels8_l2
.option	pic2
move	$19,$4

addiu	$5,$17,8
addiu	$4,$19,8
sw	$16,16($sp)
addu	$6,$5,$16
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	avg_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_no_rnd_pixels16_y2_c
.size	avg_no_rnd_pixels16_y2_c, .-avg_no_rnd_pixels16_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_pixels16_xy2_c
.type	avg_no_rnd_pixels16_xy2_c, @function
avg_no_rnd_pixels16_xy2_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,40($sp)
move	$19,$4
sw	$18,36($sp)
move	$18,$5
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,44($sp)
.option	pic0
jal	avg_no_rnd_pixels8_xy2_c
.option	pic2
move	$16,$7

addiu	$4,$19,8
lw	$31,44($sp)
addiu	$5,$18,8
lw	$19,40($sp)
move	$6,$17
lw	$18,36($sp)
move	$7,$16
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	avg_no_rnd_pixels8_xy2_c
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	avg_no_rnd_pixels16_xy2_c
.size	avg_no_rnd_pixels16_xy2_c, .-avg_no_rnd_pixels16_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels2_c
.type	put_pixels2_c, @function
put_pixels2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L213
move	$3,$0

$L211:
lbu	$2,1($5)
addiu	$3,$3,1
lbu	$8,0($5)
addu	$5,$5,$6
sll	$2,$2,8
or	$2,$2,$8
sh	$2,0($4)
bne	$3,$7,$L211
addu	$4,$4,$6

$L213:
j	$31
nop

.set	macro
.set	reorder
.end	put_pixels2_c
.size	put_pixels2_c, .-put_pixels2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels4_c
.type	put_pixels4_c, @function
put_pixels4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L220
move	$2,$0
.set	macro
.set	reorder

$L218:
addiu	$2,$2,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
#NO_APP
addu	$5,$5,$6
sw	$3,0($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L218
addu	$4,$4,$6
.set	macro
.set	reorder

$L220:
j	$31
.end	put_pixels4_c
.size	put_pixels4_c, .-put_pixels4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels8_c
.type	put_pixels8_c, @function
put_pixels8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L227
move	$2,$0
.set	macro
.set	reorder

$L225:
addiu	$2,$2,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
#NO_APP
sw	$3,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
#NO_APP
addu	$5,$5,$6
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L225
addu	$4,$4,$6
.set	macro
.set	reorder

$L227:
j	$31
.end	put_pixels8_c
.size	put_pixels8_c, .-put_pixels8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels8_l2
.type	put_no_rnd_pixels8_l2, @function
put_no_rnd_pixels8_l2:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$10,24($sp)
lw	$12,16($sp)
.set	noreorder
.set	nomacro
blez	$10,$L233
lw	$13,20($sp)
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
addiu	$5,$5,3
addiu	$6,$6,3
move	$8,$0
ori	$11,$11,0xfefe
$L230:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 0($5)  
lwr $9, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 0($6)  
lwr $2, -3($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$9
and	$3,$3,$11
srl	$3,$3,1
and	$2,$2,$9
addu	$2,$3,$2
addiu	$8,$8,1
sw	$2,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 4($5)  
lwr $9, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($6)  
lwr $2, 1($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$9
and	$3,$3,$11
srl	$3,$3,1
and	$2,$2,$9
addu	$2,$3,$2
addu	$5,$5,$12
addu	$6,$6,$13
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$10,$L230
addu	$4,$4,$7
.set	macro
.set	reorder

$L233:
j	$31
.end	put_no_rnd_pixels8_l2
.size	put_no_rnd_pixels8_l2, .-put_no_rnd_pixels8_l2
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels8_l2
.type	put_pixels8_l2, @function
put_pixels8_l2:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$10,24($sp)
lw	$12,16($sp)
.set	noreorder
.set	nomacro
blez	$10,$L239
lw	$13,20($sp)
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
addiu	$5,$5,3
addiu	$6,$6,3
move	$8,$0
ori	$11,$11,0xfefe
$L236:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 0($5)  
lwr $9, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 0($6)  
lwr $2, -3($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$9
and	$3,$3,$11
srl	$3,$3,1
or	$2,$2,$9
subu	$2,$2,$3
addiu	$8,$8,1
sw	$2,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 4($5)  
lwr $9, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($6)  
lwr $2, 1($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$9
and	$3,$3,$11
srl	$3,$3,1
or	$2,$2,$9
subu	$2,$2,$3
addu	$5,$5,$12
addu	$6,$6,$13
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$10,$L236
addu	$4,$4,$7
.set	macro
.set	reorder

$L239:
j	$31
.end	put_pixels8_l2
.size	put_pixels8_l2, .-put_pixels8_l2
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels8_x2_c
.type	put_no_rnd_pixels8_x2_c, @function
put_no_rnd_pixels8_x2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addiu	$6,$5,1
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_no_rnd_pixels8_x2_c
.size	put_no_rnd_pixels8_x2_c, .-put_no_rnd_pixels8_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels8_x2_c
.type	put_pixels8_x2_c, @function
put_pixels8_x2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addiu	$6,$5,1
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	put_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_pixels8_x2_c
.size	put_pixels8_x2_c, .-put_pixels8_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels8_y2_c
.type	put_no_rnd_pixels8_y2_c, @function
put_no_rnd_pixels8_y2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addu	$6,$5,$6
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_no_rnd_pixels8_y2_c
.size	put_no_rnd_pixels8_y2_c, .-put_no_rnd_pixels8_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels8_y2_c
.type	put_pixels8_y2_c, @function
put_pixels8_y2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
move	$2,$6
addu	$6,$5,$6
sw	$7,24($sp)
sw	$2,16($sp)
sw	$2,20($sp)
sw	$31,44($sp)
.option	pic0
jal	put_pixels8_l2
.option	pic2
move	$7,$2

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_pixels8_y2_c
.size	put_pixels8_y2_c, .-put_pixels8_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels8_l4
.type	put_pixels8_l4, @function
put_pixels8_l4:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-24
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
sw	$21,20($sp)
sw	$20,16($sp)
lw	$24,64($sp)
lw	$25,44($sp)
lw	$16,48($sp)
lw	$17,52($sp)
lw	$18,56($sp)
.set	noreorder
.set	nomacro
blez	$24,$L248
lw	$19,60($sp)
.set	macro
.set	reorder

lw	$12,40($sp)
li	$11,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$14,252641280			# 0xf0f0000
li	$10,-50593792			# 0xfffffffffcfc0000
addiu	$5,$5,3
addiu	$6,$6,3
addiu	$7,$7,3
addiu	$12,$12,3
move	$13,$0
addiu	$11,$11,771
addiu	$15,$15,514
addiu	$14,$14,3855
ori	$10,$10,0xfcfc
$L250:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($5)  
lwr $8, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 0($6)  
lwr $2, -3($6)  

# 0 "" 2
#NO_APP
and	$20,$8,$11
and	$3,$2,$11
addu	$20,$20,$3
and	$2,$2,$10
and	$8,$8,$10
addu	$20,$20,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 0($7)  
lwr $21, -3($7)  

# 0 "" 2
#NO_APP
srl	$2,$2,2
and	$9,$21,$11
srl	$8,$8,2
addu	$20,$20,$9
and	$3,$21,$10
addu	$9,$2,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 0($12)  
lwr $21, -3($12)  

# 0 "" 2
#NO_APP
srl	$3,$3,2
and	$2,$21,$11
addu	$2,$20,$2
and	$21,$21,$10
addu	$8,$9,$3
srl	$3,$21,2
srl	$2,$2,2
addu	$3,$8,$3
and	$2,$2,$14
addu	$2,$3,$2
addiu	$13,$13,1
sw	$2,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($5)  
lwr $8, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($6)  
lwr $2, 1($6)  

# 0 "" 2
#NO_APP
and	$20,$8,$11
and	$3,$2,$11
addu	$20,$20,$3
and	$2,$2,$10
and	$8,$8,$10
addu	$20,$20,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 4($7)  
lwr $21, 1($7)  

# 0 "" 2
#NO_APP
srl	$2,$2,2
and	$9,$21,$11
srl	$8,$8,2
and	$3,$21,$10
addu	$20,$20,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 4($12)  
lwr $21, 1($12)  

# 0 "" 2
#NO_APP
addu	$9,$2,$8
srl	$3,$3,2
and	$2,$21,$11
addu	$2,$20,$2
and	$21,$21,$10
addu	$8,$9,$3
srl	$3,$21,2
srl	$2,$2,2
addu	$3,$8,$3
and	$2,$2,$14
addu	$2,$3,$2
addu	$5,$5,$16
addu	$6,$6,$17
sw	$2,4($4)
addu	$7,$7,$18
addu	$12,$12,$19
.set	noreorder
.set	nomacro
bne	$13,$24,$L250
addu	$4,$4,$25
.set	macro
.set	reorder

$L248:
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,24
.set	macro
.set	reorder

.end	put_pixels8_l4
.size	put_pixels8_l4, .-put_pixels8_l4
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels4_y2_c
.type	put_pixels4_y2_c, @function
put_pixels4_y2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L259
li	$12,-16908288			# 0xfffffffffefe0000
.set	macro
.set	reorder

move	$2,$0
move	$9,$0
addu	$13,$5,$6
ori	$12,$12,0xfefe
$L255:
addu	$10,$6,$2
addu	$3,$5,$2
addu	$8,$13,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($3)  
lwr $11, 0($3)  

# 0 "" 2
#NO_APP
addu	$3,$5,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($8)  
lwr $14, 0($3)  

# 0 "" 2
#NO_APP
xor	$8,$14,$11
and	$8,$8,$12
srl	$8,$8,1
or	$3,$14,$11
subu	$3,$3,$8
addu	$2,$4,$2
addiu	$9,$9,1
sw	$3,0($2)
.set	noreorder
.set	nomacro
bne	$9,$7,$L255
move	$2,$10
.set	macro
.set	reorder

$L259:
j	$31
.end	put_pixels4_y2_c
.size	put_pixels4_y2_c, .-put_pixels4_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels2_x2_c
.type	put_pixels2_x2_c, @function
put_pixels2_x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L265
li	$13,-16908288			# 0xfffffffffefe0000

move	$9,$0
move	$10,$0
ori	$13,$13,0xfefe
$L262:
addu	$8,$5,$9
addu	$12,$4,$9
addiu	$10,$10,1
lbu	$11,1($8)
addu	$9,$9,$6
lbu	$3,2($8)
lbu	$2,0($8)
sll	$8,$11,8
sll	$3,$3,8
or	$8,$8,$2
or	$2,$3,$11
xor	$3,$2,$8
and	$3,$3,$13
srl	$3,$3,1
or	$2,$2,$8
subu	$2,$2,$3
bne	$10,$7,$L262
sh	$2,0($12)

$L265:
j	$31
nop

.set	macro
.set	reorder
.end	put_pixels2_x2_c
.size	put_pixels2_x2_c, .-put_pixels2_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels2_y2_c
.type	put_pixels2_y2_c, @function
put_pixels2_y2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L271
li	$14,-16908288			# 0xfffffffffefe0000

move	$9,$0
move	$10,$0
ori	$14,$14,0xfefe
$L267:
addu	$12,$6,$9
addu	$2,$5,$9
addu	$11,$5,$12
addu	$13,$4,$9
lbu	$8,1($2)
addiu	$10,$10,1
lbu	$3,1($11)
move	$9,$12
lbu	$2,0($2)
lbu	$11,0($11)
sll	$8,$8,8
sll	$3,$3,8
or	$8,$8,$2
or	$2,$3,$11
xor	$3,$2,$8
and	$3,$3,$14
srl	$3,$3,1
or	$2,$2,$8
subu	$2,$2,$3
bne	$10,$7,$L267
sh	$2,0($13)

$L271:
j	$31
nop

.set	macro
.set	reorder
.end	put_pixels2_y2_c
.size	put_pixels2_y2_c, .-put_pixels2_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels8_l4
.type	put_no_rnd_pixels8_l4, @function
put_no_rnd_pixels8_l4:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-24
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
sw	$21,20($sp)
sw	$20,16($sp)
lw	$24,64($sp)
lw	$25,44($sp)
lw	$16,48($sp)
lw	$17,52($sp)
lw	$18,56($sp)
.set	noreorder
.set	nomacro
blez	$24,$L272
lw	$19,60($sp)
.set	macro
.set	reorder

lw	$12,40($sp)
li	$11,50528256			# 0x3030000
li	$15,16842752			# 0x1010000
li	$14,252641280			# 0xf0f0000
li	$10,-50593792			# 0xfffffffffcfc0000
addiu	$5,$5,3
addiu	$6,$6,3
addiu	$7,$7,3
addiu	$12,$12,3
move	$13,$0
addiu	$11,$11,771
addiu	$15,$15,257
addiu	$14,$14,3855
ori	$10,$10,0xfcfc
$L274:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($5)  
lwr $8, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 0($6)  
lwr $2, -3($6)  

# 0 "" 2
#NO_APP
and	$20,$8,$11
and	$3,$2,$11
addu	$20,$20,$3
and	$2,$2,$10
and	$8,$8,$10
addu	$20,$20,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 0($7)  
lwr $21, -3($7)  

# 0 "" 2
#NO_APP
srl	$2,$2,2
and	$9,$21,$11
srl	$8,$8,2
addu	$20,$20,$9
and	$3,$21,$10
addu	$9,$2,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 0($12)  
lwr $21, -3($12)  

# 0 "" 2
#NO_APP
srl	$3,$3,2
and	$2,$21,$11
addu	$2,$20,$2
and	$21,$21,$10
addu	$8,$9,$3
srl	$3,$21,2
srl	$2,$2,2
addu	$3,$8,$3
and	$2,$2,$14
addu	$2,$3,$2
addiu	$13,$13,1
sw	$2,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($5)  
lwr $8, 1($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($6)  
lwr $2, 1($6)  

# 0 "" 2
#NO_APP
and	$20,$8,$11
and	$3,$2,$11
addu	$20,$20,$3
and	$2,$2,$10
and	$8,$8,$10
addu	$20,$20,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 4($7)  
lwr $21, 1($7)  

# 0 "" 2
#NO_APP
srl	$2,$2,2
and	$9,$21,$11
srl	$8,$8,2
and	$3,$21,$10
addu	$20,$20,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 4($12)  
lwr $21, 1($12)  

# 0 "" 2
#NO_APP
addu	$9,$2,$8
srl	$3,$3,2
and	$2,$21,$11
addu	$2,$20,$2
and	$21,$21,$10
addu	$8,$9,$3
srl	$3,$21,2
srl	$2,$2,2
addu	$3,$8,$3
and	$2,$2,$14
addu	$2,$3,$2
addu	$5,$5,$16
addu	$6,$6,$17
sw	$2,4($4)
addu	$7,$7,$18
addu	$12,$12,$19
.set	noreorder
.set	nomacro
bne	$13,$24,$L274
addu	$4,$4,$25
.set	macro
.set	reorder

$L272:
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,24
.set	macro
.set	reorder

.end	put_no_rnd_pixels8_l4
.size	put_no_rnd_pixels8_l4, .-put_no_rnd_pixels8_l4
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels2_xy2_c
.type	put_pixels2_xy2_c, @function
put_pixels2_xy2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,1($5)
lbu	$8,0($5)
lbu	$3,2($5)
addu	$5,$5,$6
addiu	$2,$2,2
addu	$8,$8,$2
blez	$7,$L283
addu	$2,$3,$2

sll	$12,$6,1
addu	$11,$5,$6
move	$13,$0
addu	$6,$4,$6
$L280:
lbu	$9,1($5)
addiu	$13,$13,2
lbu	$3,2($5)
lbu	$10,0($5)
slt	$14,$13,$7
addu	$5,$5,$12
addu	$10,$10,$9
addu	$9,$3,$9
addu	$8,$10,$8
addu	$2,$9,$2
sra	$8,$8,2
sra	$2,$2,2
sb	$8,0($4)
sb	$2,1($4)
addu	$4,$4,$12
lbu	$2,1($11)
lbu	$8,0($11)
lbu	$3,2($11)
addu	$11,$11,$12
addiu	$2,$2,2
addu	$8,$8,$2
addu	$2,$3,$2
addu	$10,$10,$8
addu	$9,$9,$2
sra	$10,$10,2
sra	$9,$9,2
sb	$10,0($6)
sb	$9,1($6)
bne	$14,$0,$L280
addu	$6,$6,$12

$L283:
j	$31
nop

.set	macro
.set	reorder
.end	put_pixels2_xy2_c
.size	put_pixels2_xy2_c, .-put_pixels2_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels4_xy2_c
.type	put_pixels4_xy2_c, @function
put_pixels4_xy2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$11,50528256			# 0x3030000
li	$10,-50593792			# 0xfffffffffcfc0000
addiu	$11,$11,771
ori	$10,$10,0xfcfc
li	$14,33685504			# 0x2020000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($5)  
lwr $8, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$11
and	$9,$8,$11
and	$3,$3,$10
and	$8,$8,$10
addu	$2,$2,$9
addiu	$14,$14,514
srl	$8,$8,2
srl	$3,$3,2
move	$25,$4
addu	$2,$2,$14
addu	$8,$8,$3
addu	$5,$5,$6
.set	noreorder
.set	nomacro
blez	$7,$L290
move	$12,$0
.set	macro
.set	reorder

li	$13,252641280			# 0xf0f0000
addiu	$13,$13,3855
$L288:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $15, 3($5)  
lwr $15, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($5)  
lwr $3, 1($5)  

# 0 "" 2
#NO_APP
and	$4,$15,$11
and	$9,$3,$11
addu	$4,$9,$4
and	$3,$3,$10
addu	$2,$2,$4
and	$15,$15,$10
srl	$15,$15,2
srl	$3,$3,2
srl	$2,$2,2
addu	$3,$3,$15
and	$2,$2,$13
addu	$2,$2,$3
addu	$5,$5,$6
addu	$2,$2,$8
addu	$15,$25,$6
addiu	$12,$12,2
sw	$2,0($25)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 4($5)  
lwr $9, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$8,$11
and	$25,$9,$11
addu	$2,$2,$25
and	$9,$9,$10
addu	$2,$2,$14
and	$8,$8,$10
srl	$9,$9,2
srl	$8,$8,2
addu	$4,$2,$4
addu	$8,$9,$8
srl	$4,$4,2
addu	$3,$8,$3
and	$4,$4,$13
addu	$3,$3,$4
slt	$24,$12,$7
addu	$5,$5,$6
addu	$25,$15,$6
.set	noreorder
.set	nomacro
bne	$24,$0,$L288
sw	$3,0($15)
.set	macro
.set	reorder

$L290:
j	$31
.end	put_pixels4_xy2_c
.size	put_pixels4_xy2_c, .-put_pixels4_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels8_xy2_c
.type	put_pixels8_xy2_c, @function
put_pixels8_xy2_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
addiu	$2,$7,-1
nor	$24,$0,$7
srl	$2,$2,1
mul	$24,$24,$6
sll	$15,$6,1
addiu	$2,$2,1
mul	$3,$6,$7
mul	$15,$2,$15
addiu	$sp,$sp,-32
li	$25,4			# 0x4
li	$11,50528256			# 0x3030000
sw	$17,8($sp)
li	$14,33685504			# 0x2020000
sw	$16,4($sp)
li	$13,252641280			# 0xf0f0000
sw	$22,28($sp)
li	$10,-50593792			# 0xfffffffffcfc0000
sw	$21,24($sp)
addiu	$24,$24,4
sw	$20,20($sp)
subu	$25,$25,$3
sw	$19,16($sp)
li	$16,2			# 0x2
sw	$18,12($sp)
addiu	$11,$11,771
addiu	$14,$14,514
addiu	$13,$13,3855
li	$17,1			# 0x1
ori	$10,$10,0xfcfc
$L294:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($5)  
lwr $18, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$11
and	$8,$18,$11
and	$3,$3,$10
and	$18,$18,$10
addu	$2,$2,$8
srl	$18,$18,2
srl	$3,$3,2
addu	$2,$2,$14
addu	$18,$18,$3
.set	noreorder
.set	nomacro
blez	$7,$L292
addu	$5,$5,$6
.set	macro
.set	reorder

move	$12,$0
move	$9,$5
move	$19,$4
$L293:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($9)  
lwr $20, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($9)  
lwr $3, 1($9)  

# 0 "" 2
#NO_APP
and	$8,$20,$11
and	$21,$3,$11
addu	$8,$21,$8
and	$3,$3,$10
addu	$2,$2,$8
and	$20,$20,$10
srl	$20,$20,2
srl	$3,$3,2
srl	$2,$2,2
addu	$3,$3,$20
and	$2,$2,$13
addu	$2,$2,$3
addu	$9,$9,$6
addu	$2,$2,$18
addu	$20,$19,$6
addiu	$12,$12,2
sw	$2,0($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($9)  
lwr $19, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($9)  
lwr $18, 1($9)  

# 0 "" 2
#NO_APP
and	$2,$19,$11
and	$22,$18,$11
addu	$2,$2,$22
and	$18,$18,$10
addu	$2,$2,$14
and	$19,$19,$10
srl	$19,$19,2
srl	$18,$18,2
addu	$8,$2,$8
addu	$18,$18,$19
srl	$8,$8,2
addu	$3,$18,$3
and	$8,$8,$13
addu	$8,$3,$8
slt	$21,$12,$7
addu	$9,$9,$6
addu	$19,$20,$6
.set	noreorder
.set	nomacro
bne	$21,$0,$L293
sw	$8,0($20)
.set	macro
.set	reorder

addu	$5,$5,$15
addu	$4,$4,$15
$L292:
addu	$5,$5,$24
.set	noreorder
.set	nomacro
bne	$16,$17,$L295
addu	$4,$4,$25
.set	macro
.set	reorder

lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,32
.set	macro
.set	reorder

$L295:
.option	pic0
.set	noreorder
.set	nomacro
j	$L294
.option	pic2
li	$16,1			# 0x1
.set	macro
.set	reorder

.end	put_pixels8_xy2_c
.size	put_pixels8_xy2_c, .-put_pixels8_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels8_xy2_c
.type	put_no_rnd_pixels8_xy2_c, @function
put_no_rnd_pixels8_xy2_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
addiu	$2,$7,-1
nor	$24,$0,$7
srl	$2,$2,1
mul	$24,$24,$6
sll	$15,$6,1
addiu	$2,$2,1
mul	$3,$6,$7
mul	$15,$2,$15
addiu	$sp,$sp,-32
li	$25,4			# 0x4
li	$11,50528256			# 0x3030000
sw	$17,8($sp)
li	$14,16842752			# 0x1010000
sw	$16,4($sp)
li	$13,252641280			# 0xf0f0000
sw	$22,28($sp)
li	$10,-50593792			# 0xfffffffffcfc0000
sw	$21,24($sp)
addiu	$24,$24,4
sw	$20,20($sp)
subu	$25,$25,$3
sw	$19,16($sp)
li	$16,2			# 0x2
sw	$18,12($sp)
addiu	$11,$11,771
addiu	$14,$14,257
addiu	$13,$13,3855
li	$17,1			# 0x1
ori	$10,$10,0xfcfc
$L301:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($5)  
lwr $18, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$11
and	$8,$18,$11
and	$3,$3,$10
and	$18,$18,$10
addu	$2,$2,$8
srl	$18,$18,2
srl	$3,$3,2
addu	$2,$2,$14
addu	$18,$18,$3
.set	noreorder
.set	nomacro
blez	$7,$L299
addu	$5,$5,$6
.set	macro
.set	reorder

move	$12,$0
move	$9,$5
move	$19,$4
$L300:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($9)  
lwr $20, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($9)  
lwr $3, 1($9)  

# 0 "" 2
#NO_APP
and	$8,$20,$11
and	$21,$3,$11
addu	$8,$21,$8
and	$3,$3,$10
addu	$2,$2,$8
and	$20,$20,$10
srl	$20,$20,2
srl	$3,$3,2
srl	$2,$2,2
addu	$3,$3,$20
and	$2,$2,$13
addu	$2,$2,$3
addu	$9,$9,$6
addu	$2,$2,$18
addu	$20,$19,$6
addiu	$12,$12,2
sw	$2,0($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($9)  
lwr $19, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($9)  
lwr $18, 1($9)  

# 0 "" 2
#NO_APP
and	$2,$19,$11
and	$22,$18,$11
addu	$2,$2,$22
and	$18,$18,$10
addu	$2,$2,$14
and	$19,$19,$10
srl	$19,$19,2
srl	$18,$18,2
addu	$8,$2,$8
addu	$18,$18,$19
srl	$8,$8,2
addu	$3,$18,$3
and	$8,$8,$13
addu	$8,$3,$8
slt	$21,$12,$7
addu	$9,$9,$6
addu	$19,$20,$6
.set	noreorder
.set	nomacro
bne	$21,$0,$L300
sw	$8,0($20)
.set	macro
.set	reorder

addu	$5,$5,$15
addu	$4,$4,$15
$L299:
addu	$5,$5,$24
.set	noreorder
.set	nomacro
bne	$16,$17,$L302
addu	$4,$4,$25
.set	macro
.set	reorder

lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,32
.set	macro
.set	reorder

$L302:
.option	pic0
.set	noreorder
.set	nomacro
j	$L301
.option	pic2
li	$16,1			# 0x1
.set	macro
.set	reorder

.end	put_no_rnd_pixels8_xy2_c
.size	put_no_rnd_pixels8_xy2_c, .-put_no_rnd_pixels8_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels16_c
.type	put_pixels16_c, @function
put_pixels16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L316
move	$8,$0
.set	macro
.set	reorder

move	$3,$4
move	$2,$5
$L307:
addiu	$8,$8,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
sw	$9,0($3)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 7($2)  
lwr $9, 4($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$6
sw	$9,4($3)
.set	noreorder
.set	nomacro
bne	$8,$7,$L307
addu	$3,$3,$6
.set	macro
.set	reorder

addiu	$4,$4,8
addiu	$5,$5,8
move	$2,$0
$L309:
addiu	$2,$2,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
#NO_APP
sw	$3,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
#NO_APP
addu	$5,$5,$6
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L309
addu	$4,$4,$6
.set	macro
.set	reorder

$L316:
j	$31
.end	put_pixels16_c
.size	put_pixels16_c, .-put_pixels16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels16_x2_c
.type	put_pixels16_x2_c, @function
put_pixels16_x2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addiu	$6,$5,1
sw	$19,56($sp)
move	$19,$4
sw	$18,52($sp)
move	$18,$7
sw	$17,48($sp)
move	$17,$5
sw	$16,20($sp)
sw	$7,24($sp)
sw	$31,60($sp)
.option	pic0
jal	put_pixels8_l2
.option	pic2
move	$7,$16

addiu	$4,$19,8
addiu	$5,$17,8
sw	$16,16($sp)
addiu	$6,$17,9
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	put_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_pixels16_x2_c
.size	put_pixels16_x2_c, .-put_pixels16_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels16_y2_c
.type	put_pixels16_y2_c, @function
put_pixels16_y2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$18,52($sp)
move	$18,$7
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addu	$6,$5,$6
sw	$17,48($sp)
move	$7,$16
move	$17,$5
sw	$16,20($sp)
sw	$18,24($sp)
sw	$31,60($sp)
sw	$19,56($sp)
.option	pic0
jal	put_pixels8_l2
.option	pic2
move	$19,$4

addiu	$5,$17,8
addiu	$4,$19,8
sw	$16,16($sp)
addu	$6,$5,$16
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	put_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_pixels16_y2_c
.size	put_pixels16_y2_c, .-put_pixels16_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels16_xy2_c
.type	put_pixels16_xy2_c, @function
put_pixels16_xy2_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,40($sp)
move	$19,$4
sw	$18,36($sp)
move	$18,$5
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,44($sp)
.option	pic0
jal	put_pixels8_xy2_c
.option	pic2
move	$16,$7

addiu	$4,$19,8
lw	$31,44($sp)
addiu	$5,$18,8
lw	$19,40($sp)
move	$6,$17
lw	$18,36($sp)
move	$7,$16
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	put_pixels8_xy2_c
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_pixels16_xy2_c
.size	put_pixels16_xy2_c, .-put_pixels16_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels16_x2_c
.type	put_no_rnd_pixels16_x2_c, @function
put_no_rnd_pixels16_x2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addiu	$6,$5,1
sw	$19,56($sp)
move	$19,$4
sw	$18,52($sp)
move	$18,$7
sw	$17,48($sp)
move	$17,$5
sw	$16,20($sp)
sw	$7,24($sp)
sw	$31,60($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

addiu	$4,$19,8
addiu	$5,$17,8
sw	$16,16($sp)
addiu	$6,$17,9
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_no_rnd_pixels16_x2_c
.size	put_no_rnd_pixels16_x2_c, .-put_no_rnd_pixels16_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels16_y2_c
.type	put_no_rnd_pixels16_y2_c, @function
put_no_rnd_pixels16_y2_c:
.frame	$sp,64,$31		# vars= 0, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$18,52($sp)
move	$18,$7
sw	$16,44($sp)
move	$16,$6
sw	$6,16($sp)
addu	$6,$5,$6
sw	$17,48($sp)
move	$7,$16
move	$17,$5
sw	$16,20($sp)
sw	$18,24($sp)
sw	$31,60($sp)
sw	$19,56($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$19,$4

addiu	$5,$17,8
addiu	$4,$19,8
sw	$16,16($sp)
addu	$6,$5,$16
sw	$16,20($sp)
sw	$18,24($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_no_rnd_pixels16_y2_c
.size	put_no_rnd_pixels16_y2_c, .-put_no_rnd_pixels16_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels16_xy2_c
.type	put_no_rnd_pixels16_xy2_c, @function
put_no_rnd_pixels16_xy2_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,40($sp)
move	$19,$4
sw	$18,36($sp)
move	$18,$5
sw	$17,32($sp)
move	$17,$6
sw	$16,28($sp)
sw	$31,44($sp)
.option	pic0
jal	put_no_rnd_pixels8_xy2_c
.option	pic2
move	$16,$7

addiu	$4,$19,8
lw	$31,44($sp)
addiu	$5,$18,8
lw	$19,40($sp)
move	$6,$17
lw	$18,36($sp)
move	$7,$16
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	put_no_rnd_pixels8_xy2_c
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_no_rnd_pixels16_xy2_c
.size	put_no_rnd_pixels16_xy2_c, .-put_no_rnd_pixels16_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels8_l2_c
.type	put_no_rnd_pixels8_l2_c, @function
put_no_rnd_pixels8_l2_c:
.frame	$sp,48,$31		# vars= 0, regs= 1/0, args= 32, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$7,16($sp)
sw	$7,20($sp)
lw	$2,64($sp)
sw	$31,44($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
sw	$2,24($sp)

lw	$31,44($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_no_rnd_pixels8_l2_c
.size	put_no_rnd_pixels8_l2_c, .-put_no_rnd_pixels8_l2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	gmc1_c
.type	gmc1_c, @function
gmc1_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
li	$3,16			# 0x10
lw	$2,20($sp)
lw	$11,24($sp)
subu	$9,$3,$8
subu	$3,$3,$2
mul	$10,$9,$3
mul	$3,$8,$3
mul	$9,$9,$2
blez	$7,$L336
mul	$8,$8,$2

addu	$2,$5,$6
move	$14,$0
$L333:
lbu	$12,1($5)
addiu	$14,$14,1
lbu	$24,0($5)
lbu	$15,0($2)
lbu	$13,1($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,0($4)
lbu	$12,2($5)
lbu	$24,1($5)
lbu	$15,1($2)
lbu	$13,2($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,1($4)
lbu	$12,3($5)
lbu	$24,2($5)
lbu	$15,2($2)
lbu	$13,3($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,2($4)
lbu	$12,4($5)
lbu	$24,3($5)
lbu	$15,3($2)
lbu	$13,4($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,3($4)
lbu	$12,5($5)
lbu	$24,4($5)
lbu	$15,4($2)
lbu	$13,5($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,4($4)
lbu	$12,6($5)
lbu	$24,5($5)
lbu	$15,5($2)
lbu	$13,6($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,5($4)
lbu	$12,7($5)
lbu	$24,6($5)
lbu	$15,6($2)
mult	$12,$3
lbu	$13,7($2)
madd	$24,$10
madd	$15,$9
madd	$13,$8
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,6($4)
lbu	$12,8($5)
lbu	$24,7($5)
addu	$5,$5,$6
lbu	$15,7($2)
lbu	$13,8($2)
mult	$12,$3
madd	$24,$10
madd	$15,$9
madd	$13,$8
addu	$2,$2,$6
mflo	$12
addu	$12,$12,$11
sra	$12,$12,8
sb	$12,7($4)
bne	$14,$7,$L333
addu	$4,$4,$6

$L336:
j	$31
nop

.set	macro
.set	reorder
.end	gmc1_c
.size	gmc1_c, .-gmc1_c
.align	2
.globl	ff_gmc_c
.set	nomips16
.set	nomicromips
.ent	ff_gmc_c
.type	ff_gmc_c, @function
ff_gmc_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$16,4($sp)
li	$16,1			# 0x1
sw	$23,32($sp)
sw	$21,24($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$fp,36($sp)
sw	$22,28($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$7,52($sp)
lw	$15,80($sp)
lw	$24,88($sp)
lw	$25,92($sp)
lw	$23,60($sp)
sll	$16,$16,$15
lw	$19,64($sp)
addiu	$24,$24,-1
lw	$20,72($sp)
addiu	$25,$25,-1
blez	$7,$L337
lw	$21,84($sp)

addiu	$17,$16,-1
sll	$22,$15,1
addiu	$18,$4,8
move	$fp,$0
$L339:
addiu	$12,$18,-8
lw	$13,56($sp)
.option	pic0
j	$L348
.option	pic2
move	$14,$23

$L357:
sltu	$8,$2,$25
beq	$8,$0,$L341
and	$4,$4,$17

mul	$8,$2,$6
subu	$11,$16,$4
subu	$9,$16,$7
addu	$3,$8,$3
addu	$8,$3,$6
addu	$3,$5,$3
addu	$8,$5,$8
lbu	$10,1($3)
lbu	$2,1($8)
lbu	$8,0($8)
mul	$10,$10,$4
lbu	$3,0($3)
mul	$4,$2,$4
mul	$2,$11,$8
addu	$8,$2,$4
mul	$2,$3,$11
mult	$8,$7
addu	$3,$2,$10
madd	$3,$9
mflo	$2
addu	$2,$2,$21
sra	$2,$2,$22
sb	$2,0($12)
$L342:
addiu	$12,$12,1
addu	$13,$13,$19
beq	$12,$18,$L356
addu	$14,$14,$20

$L348:
sra	$4,$13,16
sra	$2,$14,16
sra	$3,$4,$15
and	$7,$2,$17
sltu	$8,$3,$24
bne	$8,$0,$L357
sra	$2,$2,$15

sltu	$4,$2,$25
beq	$4,$0,$L344
nop

bltz	$3,$L350
slt	$4,$24,$3

movn	$3,$24,$4
$L345:
mul	$4,$2,$6
subu	$8,$16,$7
addu	$2,$4,$3
addu	$2,$5,$2
addu	$3,$2,$6
lbu	$4,0($2)
lbu	$3,0($3)
mult	$3,$7
madd	$4,$8
$L355:
mflo	$2
addiu	$12,$12,1
addu	$13,$13,$19
addu	$14,$14,$20
mul	$3,$2,$16
addu	$2,$3,$21
sra	$2,$2,$22
bne	$12,$18,$L348
sb	$2,-1($12)

$L356:
lw	$2,56($sp)
addiu	$fp,$fp,1
lw	$3,68($sp)
addu	$18,$18,$6
lw	$4,76($sp)
lw	$8,52($sp)
addu	$2,$2,$3
addu	$23,$23,$4
bne	$fp,$8,$L339
sw	$2,56($sp)

$L337:
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

$L341:
bltz	$2,$L349
slt	$7,$25,$2

movn	$2,$25,$7
mul	$2,$6,$2
$L343:
addu	$3,$3,$2
subu	$2,$16,$4
addu	$3,$5,$3
lbu	$7,1($3)
lbu	$3,0($3)
mult	$7,$4
.option	pic0
j	$L355
.option	pic2
madd	$3,$2

$L344:
bltz	$3,$L351
slt	$4,$24,$3

bltz	$2,$L352
movn	$3,$24,$4

$L358:
slt	$4,$25,$2
movn	$2,$25,$4
mul	$2,$6,$2
$L347:
addu	$3,$5,$3
addu	$2,$3,$2
lbu	$2,0($2)
.option	pic0
j	$L342
.option	pic2
sb	$2,0($12)

$L351:
bgez	$2,$L358
move	$3,$0

$L352:
.option	pic0
j	$L347
.option	pic2
move	$2,$0

$L350:
.option	pic0
j	$L345
.option	pic2
move	$3,$0

$L349:
.option	pic0
j	$L343
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	ff_gmc_c
.size	ff_gmc_c, .-ff_gmc_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc10_c
.type	put_tpel_pixels_mc10_c, @function
put_tpel_pixels_mc10_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$13,16($sp)
blez	$13,$L365
move	$12,$0

li	$11,683			# 0x2ab
$L361:
blez	$7,$L363
nop

addu	$10,$5,$7
move	$3,$5
move	$8,$4
$L362:
lbu	$2,0($3)
addiu	$3,$3,1
addiu	$8,$8,1
sll	$2,$2,1
lbu	$9,0($3)
addu	$2,$2,$9
addiu	$2,$2,1
mul	$2,$2,$11
sra	$2,$2,11
bne	$10,$3,$L362
sb	$2,-1($8)

$L363:
addiu	$12,$12,1
addu	$5,$5,$6
bne	$12,$13,$L361
addu	$4,$4,$6

$L365:
j	$31
nop

.set	macro
.set	reorder
.end	put_tpel_pixels_mc10_c
.size	put_tpel_pixels_mc10_c, .-put_tpel_pixels_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc20_c
.type	put_tpel_pixels_mc20_c, @function
put_tpel_pixels_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$13,16($sp)
blez	$13,$L372
move	$12,$0

li	$11,683			# 0x2ab
$L368:
blez	$7,$L370
nop

addu	$10,$5,$7
move	$8,$5
move	$9,$4
$L369:
lbu	$2,1($8)
addiu	$8,$8,1
addiu	$9,$9,1
sll	$2,$2,1
lbu	$3,-1($8)
addu	$2,$3,$2
addiu	$2,$2,1
mul	$2,$2,$11
sra	$2,$2,11
bne	$10,$8,$L369
sb	$2,-1($9)

$L370:
addiu	$12,$12,1
addu	$5,$5,$6
bne	$12,$13,$L368
addu	$4,$4,$6

$L372:
j	$31
nop

.set	macro
.set	reorder
.end	put_tpel_pixels_mc20_c
.size	put_tpel_pixels_mc20_c, .-put_tpel_pixels_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc01_c
.type	put_tpel_pixels_mc01_c, @function
put_tpel_pixels_mc01_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$12,16($sp)
blez	$12,$L381
move	$11,$0

li	$10,683			# 0x2ab
$L375:
blez	$7,$L377
move	$3,$0

$L379:
addu	$2,$5,$3
addu	$8,$3,$6
addu	$9,$4,$3
addu	$8,$5,$8
lbu	$2,0($2)
addiu	$3,$3,1
lbu	$8,0($8)
sll	$2,$2,1
addu	$2,$2,$8
addiu	$2,$2,1
mul	$2,$2,$10
sra	$2,$2,11
bne	$3,$7,$L379
sb	$2,0($9)

$L377:
addiu	$11,$11,1
addu	$5,$5,$6
bne	$11,$12,$L375
addu	$4,$4,$6

$L381:
j	$31
nop

.set	macro
.set	reorder
.end	put_tpel_pixels_mc01_c
.size	put_tpel_pixels_mc01_c, .-put_tpel_pixels_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc11_c
.type	put_tpel_pixels_mc11_c, @function
put_tpel_pixels_mc11_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$24,24($sp)
blez	$24,$L392
lw	$16,4($sp)

move	$25,$0
li	$15,2731			# 0xaab
$L384:
blez	$7,$L391
move	$9,$5

addu	$5,$5,$6
addu	$14,$9,$7
move	$13,$4
move	$10,$5
$L385:
lbu	$16,1($9)
addiu	$9,$9,1
lbu	$2,0($10)
addiu	$10,$10,1
addiu	$13,$13,1
sll	$3,$16,1
lbu	$8,-1($9)
sll	$12,$2,1
addu	$3,$3,$16
lbu	$11,0($10)
addu	$2,$12,$2
sll	$8,$8,2
addu	$3,$8,$3
sll	$8,$11,1
addu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,6
mul	$2,$2,$15
sra	$2,$2,15
bne	$14,$9,$L385
sb	$2,-1($13)

addiu	$25,$25,1
bne	$25,$24,$L384
addu	$4,$4,$6

lw	$16,4($sp)
$L392:
j	$31
addiu	$sp,$sp,8

$L391:
addiu	$25,$25,1
addu	$5,$5,$6
bne	$25,$24,$L384
addu	$4,$4,$6

.option	pic0
j	$L392
.option	pic2
lw	$16,4($sp)

.set	macro
.set	reorder
.end	put_tpel_pixels_mc11_c
.size	put_tpel_pixels_mc11_c, .-put_tpel_pixels_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc12_c
.type	put_tpel_pixels_mc12_c, @function
put_tpel_pixels_mc12_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$15,16($sp)
blez	$15,$L402
move	$24,$0

li	$14,2731			# 0xaab
$L395:
blez	$7,$L401
move	$9,$5

addu	$5,$5,$6
addu	$13,$9,$7
move	$12,$4
move	$10,$5
$L396:
lbu	$25,0($9)
addiu	$9,$9,1
addiu	$10,$10,1
lbu	$2,0($10)
lbu	$11,-1($10)
addiu	$12,$12,1
sll	$8,$25,1
lbu	$3,0($9)
addu	$8,$8,$25
sll	$11,$11,2
sll	$3,$3,1
addu	$3,$8,$3
sll	$8,$2,1
addu	$3,$3,$11
addu	$2,$8,$2
addu	$2,$3,$2
addiu	$2,$2,6
mul	$2,$2,$14
sra	$2,$2,15
bne	$13,$9,$L396
sb	$2,-1($12)

addiu	$24,$24,1
bne	$24,$15,$L395
addu	$4,$4,$6

$L402:
j	$31
nop

$L401:
addiu	$24,$24,1
addu	$5,$5,$6
bne	$24,$15,$L395
addu	$4,$4,$6

.option	pic0
j	$L402
nop

.option	pic2
.set	macro
.set	reorder
.end	put_tpel_pixels_mc12_c
.size	put_tpel_pixels_mc12_c, .-put_tpel_pixels_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc02_c
.type	put_tpel_pixels_mc02_c, @function
put_tpel_pixels_mc02_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$12,16($sp)
blez	$12,$L411
move	$11,$0

li	$10,683			# 0x2ab
$L405:
blez	$7,$L407
move	$8,$0

$L409:
addu	$2,$8,$6
addu	$3,$5,$8
addu	$2,$5,$2
addu	$9,$4,$8
lbu	$3,0($3)
addiu	$8,$8,1
lbu	$2,0($2)
sll	$2,$2,1
addu	$2,$3,$2
addiu	$2,$2,1
mul	$2,$2,$10
sra	$2,$2,11
bne	$8,$7,$L409
sb	$2,0($9)

$L407:
addiu	$11,$11,1
addu	$5,$5,$6
bne	$11,$12,$L405
addu	$4,$4,$6

$L411:
j	$31
nop

.set	macro
.set	reorder
.end	put_tpel_pixels_mc02_c
.size	put_tpel_pixels_mc02_c, .-put_tpel_pixels_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc21_c
.type	put_tpel_pixels_mc21_c, @function
put_tpel_pixels_mc21_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$15,16($sp)
blez	$15,$L421
move	$24,$0

li	$14,2731			# 0xaab
$L414:
blez	$7,$L420
move	$9,$5

addu	$5,$5,$6
addu	$13,$9,$7
move	$12,$4
move	$10,$5
$L415:
lbu	$25,0($9)
addiu	$9,$9,1
addiu	$10,$10,1
lbu	$2,0($10)
lbu	$11,-1($10)
addiu	$12,$12,1
sll	$8,$25,1
lbu	$3,0($9)
addu	$8,$8,$25
sll	$11,$11,1
sll	$3,$3,2
addu	$3,$8,$3
sll	$8,$2,1
addu	$3,$3,$11
addu	$2,$8,$2
addu	$2,$3,$2
addiu	$2,$2,6
mul	$2,$2,$14
sra	$2,$2,15
bne	$13,$9,$L415
sb	$2,-1($12)

addiu	$24,$24,1
bne	$24,$15,$L414
addu	$4,$4,$6

$L421:
j	$31
nop

$L420:
addiu	$24,$24,1
addu	$5,$5,$6
bne	$24,$15,$L414
addu	$4,$4,$6

.option	pic0
j	$L421
nop

.option	pic2
.set	macro
.set	reorder
.end	put_tpel_pixels_mc21_c
.size	put_tpel_pixels_mc21_c, .-put_tpel_pixels_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc22_c
.type	put_tpel_pixels_mc22_c, @function
put_tpel_pixels_mc22_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$24,24($sp)
blez	$24,$L432
lw	$16,4($sp)

move	$25,$0
li	$15,2731			# 0xaab
$L424:
blez	$7,$L431
move	$9,$5

addu	$5,$5,$6
addu	$14,$9,$7
move	$13,$4
move	$10,$5
$L425:
lbu	$16,1($9)
addiu	$9,$9,1
lbu	$2,0($10)
addiu	$10,$10,1
addiu	$13,$13,1
sll	$3,$16,1
lbu	$8,-1($9)
sll	$12,$2,1
addu	$3,$3,$16
lbu	$11,0($10)
addu	$2,$12,$2
sll	$8,$8,1
addu	$3,$8,$3
sll	$8,$11,2
addu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,6
mul	$2,$2,$15
sra	$2,$2,15
bne	$14,$9,$L425
sb	$2,-1($13)

addiu	$25,$25,1
bne	$25,$24,$L424
addu	$4,$4,$6

lw	$16,4($sp)
$L432:
j	$31
addiu	$sp,$sp,8

$L431:
addiu	$25,$25,1
addu	$5,$5,$6
bne	$25,$24,$L424
addu	$4,$4,$6

.option	pic0
j	$L432
.option	pic2
lw	$16,4($sp)

.set	macro
.set	reorder
.end	put_tpel_pixels_mc22_c
.size	put_tpel_pixels_mc22_c, .-put_tpel_pixels_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc10_c
.type	avg_tpel_pixels_mc10_c, @function
avg_tpel_pixels_mc10_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
blez	$14,$L439
move	$13,$0

li	$12,683			# 0x2ab
$L435:
blez	$7,$L437
nop

addu	$11,$4,$7
move	$9,$4
move	$10,$5
$L436:
lbu	$3,0($10)
addiu	$9,$9,1
lbu	$2,1($10)
addiu	$10,$10,1
lbu	$8,-1($9)
sll	$3,$3,1
addu	$2,$3,$2
addiu	$2,$2,1
mul	$2,$2,$12
sra	$2,$2,11
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$11,$9,$L436
sb	$2,-1($9)

$L437:
addiu	$13,$13,1
addu	$5,$5,$6
bne	$13,$14,$L435
addu	$4,$4,$6

$L439:
j	$31
nop

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc10_c
.size	avg_tpel_pixels_mc10_c, .-avg_tpel_pixels_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc20_c
.type	avg_tpel_pixels_mc20_c, @function
avg_tpel_pixels_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
blez	$14,$L446
move	$13,$0

li	$12,683			# 0x2ab
$L442:
blez	$7,$L444
nop

addu	$11,$4,$7
move	$9,$4
move	$10,$5
$L443:
lbu	$2,1($10)
addiu	$9,$9,1
lbu	$3,0($10)
addiu	$10,$10,1
lbu	$8,-1($9)
sll	$2,$2,1
addu	$2,$3,$2
addiu	$2,$2,1
mul	$2,$2,$12
sra	$2,$2,11
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$11,$9,$L443
sb	$2,-1($9)

$L444:
addiu	$13,$13,1
addu	$5,$5,$6
bne	$13,$14,$L442
addu	$4,$4,$6

$L446:
j	$31
nop

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc20_c
.size	avg_tpel_pixels_mc20_c, .-avg_tpel_pixels_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc01_c
.type	avg_tpel_pixels_mc01_c, @function
avg_tpel_pixels_mc01_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
blez	$14,$L453
move	$13,$0

li	$12,683			# 0x2ab
$L449:
blez	$7,$L451
nop

addu	$11,$5,$7
move	$10,$4
move	$9,$5
$L450:
addu	$2,$9,$6
lbu	$3,0($9)
lbu	$8,0($10)
addiu	$9,$9,1
addiu	$10,$10,1
lbu	$2,0($2)
sll	$3,$3,1
addu	$2,$3,$2
addiu	$2,$2,1
mul	$2,$2,$12
sra	$2,$2,11
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$11,$9,$L450
sb	$2,-1($10)

$L451:
addiu	$13,$13,1
addu	$5,$5,$6
bne	$13,$14,$L449
addu	$4,$4,$6

$L453:
j	$31
nop

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc01_c
.size	avg_tpel_pixels_mc01_c, .-avg_tpel_pixels_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc11_c
.type	avg_tpel_pixels_mc11_c, @function
avg_tpel_pixels_mc11_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$17,4($sp)
sw	$16,0($sp)
lw	$24,24($sp)
blez	$24,$L464
lw	$17,4($sp)

move	$25,$0
li	$15,2731			# 0xaab
$L456:
blez	$7,$L463
move	$11,$5

addu	$5,$5,$6
addu	$14,$4,$7
move	$10,$4
move	$12,$5
$L457:
lbu	$3,1($11)
addiu	$11,$11,1
lbu	$2,0($12)
addiu	$12,$12,1
addiu	$10,$10,1
lbu	$9,-1($10)
sll	$17,$3,1
lbu	$8,-1($11)
sll	$16,$2,1
addu	$3,$17,$3
lbu	$13,0($12)
addu	$16,$16,$2
sll	$8,$8,2
sll	$2,$13,1
addu	$3,$8,$3
addu	$3,$3,$16
addu	$2,$3,$2
addiu	$2,$2,6
mul	$2,$2,$15
sra	$2,$2,15
addu	$2,$9,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$14,$10,$L457
sb	$2,-1($10)

addiu	$25,$25,1
bne	$25,$24,$L456
addu	$4,$4,$6

lw	$17,4($sp)
$L464:
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L463:
addiu	$25,$25,1
addu	$5,$5,$6
bne	$25,$24,$L456
addu	$4,$4,$6

.option	pic0
j	$L464
.option	pic2
lw	$17,4($sp)

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc11_c
.size	avg_tpel_pixels_mc11_c, .-avg_tpel_pixels_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc12_c
.type	avg_tpel_pixels_mc12_c, @function
avg_tpel_pixels_mc12_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$15,24($sp)
blez	$15,$L475
lw	$16,4($sp)

move	$24,$0
li	$14,2731			# 0xaab
$L467:
blez	$7,$L474
move	$11,$5

addu	$5,$5,$6
addu	$13,$4,$7
move	$10,$4
move	$12,$5
$L468:
lbu	$16,0($11)
addiu	$12,$12,1
lbu	$3,1($11)
addiu	$10,$10,1
lbu	$2,0($12)
addiu	$11,$11,1
sll	$8,$16,1
lbu	$25,-1($12)
sll	$3,$3,1
lbu	$9,-1($10)
addu	$8,$8,$16
sll	$25,$25,2
addu	$3,$8,$3
sll	$8,$2,1
addu	$3,$3,$25
addu	$2,$8,$2
addu	$2,$3,$2
addiu	$2,$2,6
mul	$2,$2,$14
sra	$2,$2,15
addu	$2,$9,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$13,$10,$L468
sb	$2,-1($10)

addiu	$24,$24,1
bne	$24,$15,$L467
addu	$4,$4,$6

lw	$16,4($sp)
$L475:
j	$31
addiu	$sp,$sp,8

$L474:
addiu	$24,$24,1
addu	$5,$5,$6
bne	$24,$15,$L467
addu	$4,$4,$6

.option	pic0
j	$L475
.option	pic2
lw	$16,4($sp)

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc12_c
.size	avg_tpel_pixels_mc12_c, .-avg_tpel_pixels_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc02_c
.type	avg_tpel_pixels_mc02_c, @function
avg_tpel_pixels_mc02_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
blez	$14,$L482
move	$13,$0

li	$12,683			# 0x2ab
$L478:
blez	$7,$L480
nop

addu	$11,$5,$7
move	$10,$4
move	$9,$5
$L479:
addu	$2,$9,$6
lbu	$3,0($9)
lbu	$8,0($10)
addiu	$9,$9,1
addiu	$10,$10,1
lbu	$2,0($2)
sll	$2,$2,1
addu	$2,$3,$2
addiu	$2,$2,1
mul	$2,$2,$12
sra	$2,$2,11
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$11,$9,$L479
sb	$2,-1($10)

$L480:
addiu	$13,$13,1
addu	$5,$5,$6
bne	$13,$14,$L478
addu	$4,$4,$6

$L482:
j	$31
nop

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc02_c
.size	avg_tpel_pixels_mc02_c, .-avg_tpel_pixels_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc21_c
.type	avg_tpel_pixels_mc21_c, @function
avg_tpel_pixels_mc21_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$15,24($sp)
blez	$15,$L493
lw	$16,4($sp)

move	$24,$0
li	$14,2731			# 0xaab
$L485:
blez	$7,$L492
move	$11,$5

addu	$5,$5,$6
addu	$13,$4,$7
move	$10,$4
move	$12,$5
$L486:
lbu	$16,0($11)
addiu	$12,$12,1
lbu	$3,1($11)
addiu	$10,$10,1
lbu	$2,0($12)
addiu	$11,$11,1
sll	$8,$16,1
lbu	$25,-1($12)
sll	$3,$3,2
lbu	$9,-1($10)
addu	$8,$8,$16
sll	$25,$25,1
addu	$3,$8,$3
sll	$8,$2,1
addu	$3,$3,$25
addu	$2,$8,$2
addu	$2,$3,$2
addiu	$2,$2,6
mul	$2,$2,$14
sra	$2,$2,15
addu	$2,$9,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$13,$10,$L486
sb	$2,-1($10)

addiu	$24,$24,1
bne	$24,$15,$L485
addu	$4,$4,$6

lw	$16,4($sp)
$L493:
j	$31
addiu	$sp,$sp,8

$L492:
addiu	$24,$24,1
addu	$5,$5,$6
bne	$24,$15,$L485
addu	$4,$4,$6

.option	pic0
j	$L493
.option	pic2
lw	$16,4($sp)

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc21_c
.size	avg_tpel_pixels_mc21_c, .-avg_tpel_pixels_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc22_c
.type	avg_tpel_pixels_mc22_c, @function
avg_tpel_pixels_mc22_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$17,4($sp)
sw	$16,0($sp)
lw	$24,24($sp)
blez	$24,$L504
lw	$17,4($sp)

move	$25,$0
li	$15,2731			# 0xaab
$L496:
blez	$7,$L503
move	$11,$5

addu	$5,$5,$6
addu	$14,$4,$7
move	$10,$4
move	$12,$5
$L497:
lbu	$3,1($11)
addiu	$11,$11,1
lbu	$2,0($12)
addiu	$12,$12,1
addiu	$10,$10,1
lbu	$9,-1($10)
sll	$17,$3,1
lbu	$8,-1($11)
sll	$16,$2,1
addu	$3,$17,$3
lbu	$13,0($12)
addu	$16,$16,$2
sll	$8,$8,1
sll	$2,$13,2
addu	$3,$8,$3
addu	$3,$3,$16
addu	$2,$3,$2
addiu	$2,$2,6
mul	$2,$2,$15
sra	$2,$2,15
addu	$2,$9,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$14,$10,$L497
sb	$2,-1($10)

addiu	$25,$25,1
bne	$25,$24,$L496
addu	$4,$4,$6

lw	$17,4($sp)
$L504:
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L503:
addiu	$25,$25,1
addu	$5,$5,$6
bne	$25,$24,$L496
addu	$4,$4,$6

.option	pic0
j	$L504
.option	pic2
lw	$17,4($sp)

.set	macro
.set	reorder
.end	avg_tpel_pixels_mc22_c
.size	avg_tpel_pixels_mc22_c, .-avg_tpel_pixels_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_chroma_mc2_c
.type	put_h264_chroma_mc2_c, @function
put_h264_chroma_mc2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,16($sp)
li	$2,8			# 0x8
lw	$3,20($sp)
subu	$8,$2,$11
mul	$14,$11,$3
subu	$2,$2,$3
mul	$9,$8,$2
mul	$11,$11,$2
beq	$14,$0,$L506
mul	$8,$8,$3

blez	$7,$L516
addu	$10,$5,$6

move	$15,$0
addiu	$5,$5,1
$L508:
lbu	$2,0($5)
addiu	$15,$15,1
lbu	$13,-1($5)
lbu	$12,0($10)
lbu	$3,1($10)
mult	$2,$11
madd	$13,$9
madd	$12,$8
madd	$3,$14
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
sb	$2,0($4)
lbu	$2,1($5)
lbu	$13,0($5)
addu	$5,$5,$6
lbu	$12,1($10)
lbu	$3,2($10)
mult	$2,$11
madd	$13,$9
madd	$12,$8
madd	$3,$14
addu	$10,$10,$6
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
sb	$2,1($4)
bne	$15,$7,$L508
addu	$4,$4,$6

$L516:
j	$31
nop

$L506:
li	$2,1			# 0x1
addu	$11,$11,$8
blez	$7,$L516
movn	$2,$6,$8

addu	$8,$5,$2
move	$10,$0
$L511:
lbu	$2,0($8)
addiu	$10,$10,1
lbu	$3,0($5)
mult	$2,$11
madd	$3,$9
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
sb	$2,0($4)
lbu	$2,1($8)
addu	$8,$8,$6
lbu	$3,1($5)
addu	$5,$5,$6
mult	$2,$11
madd	$3,$9
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
sb	$2,1($4)
bne	$10,$7,$L511
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_h264_chroma_mc2_c
.size	put_h264_chroma_mc2_c, .-put_h264_chroma_mc2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_chroma_mc4_c
.type	put_h264_chroma_mc4_c, @function
put_h264_chroma_mc4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
li	$3,8			# 0x8
lw	$2,20($sp)
subu	$10,$3,$8
mul	$12,$8,$2
subu	$3,$3,$2
mul	$2,$10,$2
mul	$9,$10,$3
beq	$12,$0,$L518
mul	$8,$8,$3

blez	$7,$L528
addu	$3,$5,$6

move	$13,$0
addiu	$5,$5,1
$L520:
lbu	$10,0($5)
addiu	$13,$13,1
lbu	$15,-1($5)
lbu	$14,0($3)
lbu	$11,1($3)
mult	$10,$8
madd	$15,$9
madd	$14,$2
madd	$11,$12
mflo	$10
addiu	$10,$10,32
sra	$10,$10,6
sb	$10,0($4)
lbu	$10,1($5)
lbu	$15,0($5)
lbu	$14,1($3)
lbu	$11,2($3)
mult	$10,$8
madd	$15,$9
madd	$14,$2
madd	$11,$12
mflo	$10
addiu	$10,$10,32
sra	$10,$10,6
sb	$10,1($4)
lbu	$10,2($5)
lbu	$15,1($5)
lbu	$14,2($3)
lbu	$11,3($3)
mult	$10,$8
madd	$15,$9
madd	$14,$2
madd	$11,$12
mflo	$10
addiu	$10,$10,32
sra	$10,$10,6
sb	$10,2($4)
lbu	$10,3($5)
lbu	$15,2($5)
addu	$5,$5,$6
lbu	$14,3($3)
lbu	$11,4($3)
mult	$10,$8
madd	$15,$9
madd	$14,$2
madd	$11,$12
addu	$3,$3,$6
mflo	$10
addiu	$10,$10,32
sra	$10,$10,6
sb	$10,3($4)
bne	$13,$7,$L520
addu	$4,$4,$6

$L528:
j	$31
nop

$L518:
li	$3,1			# 0x1
addu	$8,$8,$2
blez	$7,$L528
movn	$3,$6,$2

addu	$2,$5,$3
move	$11,$0
$L523:
lbu	$3,0($2)
addiu	$11,$11,1
lbu	$10,0($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,0($4)
lbu	$3,1($2)
lbu	$10,1($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,1($4)
lbu	$3,2($2)
lbu	$10,2($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,2($4)
lbu	$3,3($2)
addu	$2,$2,$6
lbu	$10,3($5)
addu	$5,$5,$6
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,3($4)
bne	$11,$7,$L523
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_h264_chroma_mc4_c
.size	put_h264_chroma_mc4_c, .-put_h264_chroma_mc4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_chroma_mc8_c
.type	put_h264_chroma_mc8_c, @function
put_h264_chroma_mc8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
li	$3,8			# 0x8
lw	$2,20($sp)
subu	$11,$3,$8
mul	$10,$8,$2
subu	$3,$3,$2
mul	$2,$11,$2
mul	$9,$11,$3
beq	$10,$0,$L530
mul	$8,$8,$3

blez	$7,$L540
addu	$3,$5,$6

move	$13,$0
$L532:
lbu	$11,1($5)
addiu	$13,$13,1
lbu	$15,0($5)
lbu	$14,0($3)
lbu	$12,1($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,0($4)
lbu	$11,2($5)
lbu	$15,1($5)
lbu	$14,1($3)
lbu	$12,2($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,1($4)
lbu	$11,3($5)
lbu	$15,2($5)
lbu	$14,2($3)
lbu	$12,3($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,2($4)
lbu	$11,4($5)
lbu	$15,3($5)
lbu	$14,3($3)
lbu	$12,4($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,3($4)
lbu	$11,5($5)
lbu	$15,4($5)
lbu	$14,4($3)
lbu	$12,5($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,4($4)
lbu	$11,6($5)
lbu	$15,5($5)
lbu	$14,5($3)
lbu	$12,6($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,5($4)
lbu	$11,7($5)
lbu	$15,6($5)
lbu	$14,6($3)
mult	$11,$8
lbu	$12,7($3)
madd	$15,$9
madd	$14,$2
madd	$12,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,6($4)
lbu	$11,8($5)
lbu	$15,7($5)
addu	$5,$5,$6
lbu	$14,7($3)
lbu	$12,8($3)
mult	$11,$8
madd	$15,$9
madd	$14,$2
madd	$12,$10
addu	$3,$3,$6
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
sb	$11,7($4)
bne	$13,$7,$L532
addu	$4,$4,$6

$L540:
j	$31
nop

$L530:
li	$3,1			# 0x1
addu	$8,$8,$2
blez	$7,$L540
movn	$3,$6,$2

addu	$2,$5,$3
move	$11,$0
$L535:
lbu	$3,0($2)
addiu	$11,$11,1
lbu	$10,0($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,0($4)
lbu	$3,1($2)
lbu	$10,1($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,1($4)
lbu	$3,2($2)
lbu	$10,2($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,2($4)
lbu	$3,3($2)
lbu	$10,3($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,3($4)
lbu	$3,4($2)
lbu	$10,4($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,4($4)
lbu	$3,5($2)
lbu	$10,5($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,5($4)
lbu	$3,6($2)
lbu	$10,6($5)
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,6($4)
lbu	$3,7($2)
addu	$2,$2,$6
lbu	$10,7($5)
addu	$5,$5,$6
mult	$3,$8
madd	$10,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
sb	$3,7($4)
bne	$11,$7,$L535
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_h264_chroma_mc8_c
.size	put_h264_chroma_mc8_c, .-put_h264_chroma_mc8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_chroma_mc2_c
.type	avg_h264_chroma_mc2_c, @function
avg_h264_chroma_mc2_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
li	$2,8			# 0x8
sw	$16,4($sp)
lw	$14,24($sp)
lw	$3,28($sp)
subu	$11,$2,$14
mul	$12,$14,$3
subu	$2,$2,$3
mul	$10,$11,$2
mul	$14,$14,$2
beq	$12,$0,$L542
mul	$11,$11,$3

blez	$7,$L552
lw	$16,4($sp)

addu	$13,$5,$6
move	$15,$0
addiu	$5,$5,1
$L544:
lbu	$2,0($5)
addiu	$15,$15,1
lbu	$8,-1($5)
lbu	$3,0($13)
lbu	$9,1($13)
mult	$2,$14
madd	$8,$10
lbu	$8,0($4)
madd	$3,$11
lbu	$3,1($4)
madd	$9,$12
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,0($4)
lbu	$2,1($5)
lbu	$24,0($5)
addu	$5,$5,$6
lbu	$9,1($13)
lbu	$8,2($13)
mult	$2,$14
madd	$24,$10
madd	$9,$11
madd	$8,$12
addu	$13,$13,$6
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,1($4)
bne	$15,$7,$L544
addu	$4,$4,$6

$L541:
lw	$16,4($sp)
$L552:
j	$31
addiu	$sp,$sp,8

$L542:
li	$2,1			# 0x1
addu	$14,$14,$11
blez	$7,$L541
movn	$2,$6,$11

addu	$11,$5,$2
move	$12,$0
$L547:
lbu	$2,0($11)
addiu	$12,$12,1
lbu	$9,0($5)
lbu	$8,0($4)
mult	$2,$14
lbu	$3,1($4)
madd	$9,$10
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
addu	$2,$8,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,0($4)
lbu	$2,1($11)
addu	$11,$11,$6
lbu	$8,1($5)
addu	$5,$5,$6
mult	$2,$14
madd	$8,$10
mflo	$2
addiu	$2,$2,32
sra	$2,$2,6
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,1($4)
bne	$12,$7,$L547
addu	$4,$4,$6

lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	avg_h264_chroma_mc2_c
.size	avg_h264_chroma_mc2_c, .-avg_h264_chroma_mc2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_chroma_mc4_c
.type	avg_h264_chroma_mc4_c, @function
avg_h264_chroma_mc4_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
li	$8,8			# 0x8
sw	$16,4($sp)
lw	$3,24($sp)
lw	$2,28($sp)
subu	$9,$8,$3
mul	$13,$3,$2
subu	$14,$8,$2
mul	$2,$9,$2
mul	$8,$9,$14
beq	$13,$0,$L554
mul	$14,$3,$14

blez	$7,$L564
lw	$16,4($sp)

addu	$3,$5,$6
move	$15,$0
addiu	$5,$5,1
$L556:
lbu	$9,0($5)
addiu	$15,$15,1
lbu	$12,-1($5)
lbu	$11,0($3)
lbu	$10,1($3)
mult	$9,$14
madd	$12,$8
lbu	$24,0($4)
madd	$11,$2
lbu	$12,1($4)
madd	$10,$13
lbu	$11,2($4)
lbu	$10,3($4)
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$24,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,0($4)
lbu	$9,1($5)
lbu	$16,0($5)
lbu	$25,1($3)
lbu	$24,2($3)
mult	$9,$14
madd	$16,$8
madd	$25,$2
madd	$24,$13
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$12,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,1($4)
lbu	$9,2($5)
lbu	$25,1($5)
lbu	$24,2($3)
lbu	$12,3($3)
mult	$9,$14
madd	$25,$8
madd	$24,$2
madd	$12,$13
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$11,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,2($4)
lbu	$9,3($5)
lbu	$24,2($5)
addu	$5,$5,$6
lbu	$12,3($3)
lbu	$11,4($3)
mult	$9,$14
madd	$24,$8
madd	$12,$2
madd	$11,$13
addu	$3,$3,$6
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$10,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,3($4)
bne	$15,$7,$L556
addu	$4,$4,$6

$L553:
lw	$16,4($sp)
$L564:
j	$31
addiu	$sp,$sp,8

$L554:
li	$9,1			# 0x1
addu	$3,$14,$2
blez	$7,$L553
movn	$9,$6,$2

addu	$2,$5,$9
move	$13,$0
$L559:
lbu	$9,0($2)
addiu	$13,$13,1
lbu	$10,0($5)
lbu	$14,0($4)
mult	$9,$3
lbu	$12,1($4)
madd	$10,$8
lbu	$11,2($4)
lbu	$10,3($4)
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$14,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,0($4)
lbu	$9,1($2)
lbu	$14,1($5)
mult	$9,$3
madd	$14,$8
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$12,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,1($4)
lbu	$9,2($2)
lbu	$12,2($5)
mult	$9,$3
madd	$12,$8
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$11,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,2($4)
lbu	$9,3($2)
addu	$2,$2,$6
lbu	$11,3($5)
addu	$5,$5,$6
mult	$9,$3
madd	$11,$8
mflo	$9
addiu	$9,$9,32
sra	$9,$9,6
addu	$9,$10,$9
addiu	$9,$9,1
sra	$9,$9,1
sb	$9,3($4)
bne	$13,$7,$L559
addu	$4,$4,$6

lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	avg_h264_chroma_mc4_c
.size	avg_h264_chroma_mc4_c, .-avg_h264_chroma_mc4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_chroma_mc8_c
.type	avg_h264_chroma_mc8_c, @function
avg_h264_chroma_mc8_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-16
li	$3,8			# 0x8
sw	$18,12($sp)
lw	$8,32($sp)
lw	$2,36($sp)
sw	$17,8($sp)
subu	$11,$3,$8
sw	$16,4($sp)
mul	$10,$8,$2
subu	$3,$3,$2
mul	$2,$11,$2
mul	$9,$11,$3
beq	$10,$0,$L566
mul	$8,$8,$3

blez	$7,$L576
lw	$18,12($sp)

addu	$3,$5,$6
move	$14,$0
$L568:
lbu	$11,1($5)
addiu	$14,$14,1
lbu	$15,0($5)
lbu	$13,0($3)
lbu	$12,1($3)
mult	$11,$8
madd	$15,$9
lbu	$25,0($4)
madd	$13,$2
lbu	$24,1($4)
madd	$12,$10
lbu	$16,2($4)
lbu	$13,3($4)
lbu	$12,4($4)
lbu	$15,5($4)
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$25,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,0($4)
lbu	$11,2($5)
lbu	$18,1($5)
lbu	$17,1($3)
lbu	$25,2($3)
mult	$11,$8
madd	$18,$9
madd	$17,$2
madd	$25,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$24,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,1($4)
lbu	$11,3($5)
lbu	$17,2($5)
lbu	$25,2($3)
lbu	$24,3($3)
mult	$11,$8
madd	$17,$9
madd	$25,$2
madd	$24,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$16,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,2($4)
lbu	$11,4($5)
lbu	$25,3($5)
lbu	$24,3($3)
lbu	$16,4($3)
mult	$11,$8
madd	$25,$9
madd	$24,$2
madd	$16,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$13,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,3($4)
lbu	$11,5($5)
lbu	$24,4($5)
lbu	$16,4($3)
lbu	$13,5($3)
mult	$11,$8
madd	$24,$9
madd	$16,$2
madd	$13,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$12,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,4($4)
lbu	$11,6($5)
lbu	$13,5($5)
lbu	$12,5($3)
mult	$11,$8
lbu	$16,6($3)
madd	$13,$9
lbu	$13,6($4)
madd	$12,$2
lbu	$12,7($4)
madd	$16,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$15,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,5($4)
lbu	$11,7($5)
lbu	$24,6($5)
lbu	$16,6($3)
lbu	$15,7($3)
mult	$11,$8
madd	$24,$9
madd	$16,$2
madd	$15,$10
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$13,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,6($4)
lbu	$11,8($5)
lbu	$16,7($5)
addu	$5,$5,$6
lbu	$15,7($3)
lbu	$13,8($3)
mult	$11,$8
madd	$16,$9
madd	$15,$2
madd	$13,$10
addu	$3,$3,$6
mflo	$11
addiu	$11,$11,32
sra	$11,$11,6
addu	$11,$12,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,7($4)
bne	$14,$7,$L568
addu	$4,$4,$6

$L565:
lw	$18,12($sp)
$L576:
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

$L566:
li	$3,1			# 0x1
addu	$8,$8,$2
blez	$7,$L565
movn	$3,$6,$2

addu	$2,$5,$3
move	$13,$0
$L571:
lbu	$3,0($2)
addiu	$13,$13,1
lbu	$10,0($5)
lbu	$16,0($4)
mult	$3,$8
lbu	$25,1($4)
madd	$10,$9
lbu	$24,2($4)
lbu	$15,3($4)
lbu	$14,4($4)
lbu	$12,5($4)
mflo	$3
lbu	$11,6($4)
lbu	$10,7($4)
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$16,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,0($4)
lbu	$3,1($2)
lbu	$16,1($5)
mult	$3,$8
madd	$16,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$25,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,1($4)
lbu	$3,2($2)
lbu	$25,2($5)
mult	$3,$8
madd	$25,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$24,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,2($4)
lbu	$3,3($2)
lbu	$24,3($5)
mult	$3,$8
madd	$24,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$15,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,3($4)
lbu	$3,4($2)
lbu	$15,4($5)
mult	$3,$8
madd	$15,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$14,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,4($4)
lbu	$3,5($2)
lbu	$14,5($5)
mult	$3,$8
madd	$14,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$12,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,5($4)
lbu	$3,6($2)
lbu	$12,6($5)
mult	$3,$8
madd	$12,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$11,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,6($4)
lbu	$3,7($2)
addu	$2,$2,$6
lbu	$11,7($5)
addu	$5,$5,$6
mult	$3,$8
madd	$11,$9
mflo	$3
addiu	$3,$3,32
sra	$3,$3,6
addu	$3,$10,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,7($4)
bne	$13,$7,$L571
addu	$4,$4,$6

lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	avg_h264_chroma_mc8_c
.size	avg_h264_chroma_mc8_c, .-avg_h264_chroma_mc8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_vc1_chroma_mc8_c
.type	put_no_rnd_vc1_chroma_mc8_c, @function
put_no_rnd_vc1_chroma_mc8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
li	$3,8			# 0x8
lw	$2,20($sp)
subu	$9,$3,$8
subu	$3,$3,$2
mul	$10,$9,$3
mul	$3,$8,$3
mul	$9,$9,$2
blez	$7,$L582
mul	$8,$8,$2

addu	$2,$5,$6
move	$13,$0
$L579:
lbu	$11,1($5)
addiu	$13,$13,1
lbu	$15,0($5)
lbu	$14,0($2)
lbu	$12,1($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,0($4)
lbu	$11,2($5)
lbu	$15,1($5)
lbu	$14,1($2)
lbu	$12,2($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,1($4)
lbu	$11,3($5)
lbu	$15,2($5)
lbu	$14,2($2)
lbu	$12,3($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,2($4)
lbu	$11,4($5)
lbu	$15,3($5)
lbu	$14,3($2)
lbu	$12,4($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,3($4)
lbu	$11,5($5)
lbu	$15,4($5)
lbu	$14,4($2)
lbu	$12,5($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,4($4)
lbu	$11,6($5)
lbu	$15,5($5)
lbu	$14,5($2)
lbu	$12,6($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,5($4)
lbu	$11,7($5)
lbu	$15,6($5)
lbu	$14,6($2)
mult	$11,$3
lbu	$12,7($2)
madd	$15,$10
madd	$14,$9
madd	$12,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,6($4)
lbu	$11,8($5)
lbu	$15,7($5)
addu	$5,$5,$6
lbu	$14,7($2)
lbu	$12,8($2)
mult	$11,$3
madd	$15,$10
madd	$14,$9
madd	$12,$8
addu	$2,$2,$6
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
sb	$11,7($4)
bne	$13,$7,$L579
addu	$4,$4,$6

$L582:
j	$31
nop

.set	macro
.set	reorder
.end	put_no_rnd_vc1_chroma_mc8_c
.size	put_no_rnd_vc1_chroma_mc8_c, .-put_no_rnd_vc1_chroma_mc8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_no_rnd_vc1_chroma_mc8_c
.type	avg_no_rnd_vc1_chroma_mc8_c, @function
avg_no_rnd_vc1_chroma_mc8_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-16
li	$3,8			# 0x8
sw	$18,12($sp)
lw	$8,32($sp)
lw	$2,36($sp)
sw	$17,8($sp)
subu	$9,$3,$8
sw	$16,4($sp)
subu	$3,$3,$2
mul	$10,$9,$3
mul	$3,$8,$3
mul	$9,$9,$2
blez	$7,$L583
mul	$8,$8,$2

addu	$2,$5,$6
move	$15,$0
$L585:
lbu	$11,1($5)
addiu	$15,$15,1
lbu	$14,0($5)
lbu	$13,0($2)
lbu	$12,1($2)
mult	$11,$3
madd	$14,$10
lbu	$16,0($4)
madd	$13,$9
lbu	$25,1($4)
madd	$12,$8
lbu	$24,2($4)
lbu	$13,3($4)
lbu	$12,4($4)
lbu	$14,5($4)
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$16,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,0($4)
lbu	$11,2($5)
lbu	$18,1($5)
lbu	$17,1($2)
lbu	$16,2($2)
mult	$11,$3
madd	$18,$10
madd	$17,$9
madd	$16,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$25,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,1($4)
lbu	$11,3($5)
lbu	$17,2($5)
lbu	$16,2($2)
lbu	$25,3($2)
mult	$11,$3
madd	$17,$10
madd	$16,$9
madd	$25,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$24,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,2($4)
lbu	$11,4($5)
lbu	$25,3($5)
lbu	$16,3($2)
lbu	$24,4($2)
mult	$11,$3
madd	$25,$10
madd	$16,$9
madd	$24,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$13,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,3($4)
lbu	$11,5($5)
lbu	$24,4($5)
lbu	$16,4($2)
lbu	$13,5($2)
mult	$11,$3
madd	$24,$10
madd	$16,$9
madd	$13,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$12,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,4($4)
lbu	$11,6($5)
lbu	$13,5($5)
lbu	$12,5($2)
mult	$11,$3
lbu	$24,6($2)
madd	$13,$10
lbu	$13,6($4)
madd	$12,$9
lbu	$12,7($4)
madd	$24,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$14,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,5($4)
lbu	$11,7($5)
lbu	$24,6($5)
lbu	$16,6($2)
lbu	$14,7($2)
mult	$11,$3
madd	$24,$10
madd	$16,$9
madd	$14,$8
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$13,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,6($4)
lbu	$11,8($5)
lbu	$16,7($5)
addu	$5,$5,$6
lbu	$14,7($2)
lbu	$13,8($2)
mult	$11,$3
madd	$16,$10
madd	$14,$9
madd	$13,$8
addu	$2,$2,$6
mflo	$11
addiu	$11,$11,28
sra	$11,$11,6
addu	$11,$12,$11
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,7($4)
bne	$15,$7,$L585
addu	$4,$4,$6

$L583:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	avg_no_rnd_vc1_chroma_mc8_c
.size	avg_no_rnd_vc1_chroma_mc8_c, .-avg_no_rnd_vc1_chroma_mc8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mpeg4_qpel8_h_lowpass
.type	put_mpeg4_qpel8_h_lowpass, @function
put_mpeg4_qpel8_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$10,16($sp)
blez	$10,$L594
lui	$11,%hi(ff_cropTbl+1024)

move	$12,$0
addiu	$11,$11,%lo(ff_cropTbl+1024)
$L591:
lbu	$9,1($5)
addiu	$12,$12,1
lbu	$13,0($5)
lbu	$15,2($5)
lbu	$14,3($5)
addu	$3,$13,$9
lbu	$25,4($5)
addu	$13,$13,$15
addu	$14,$9,$14
sll	$24,$13,1
sll	$9,$3,2
sll	$13,$13,3
sll	$3,$3,4
subu	$13,$24,$13
addu	$8,$9,$3
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
subu	$2,$2,$15
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$8,0($5)
lbu	$15,1($5)
lbu	$3,2($5)
lbu	$13,3($5)
lbu	$14,4($5)
addu	$3,$15,$3
lbu	$25,5($5)
addu	$13,$8,$13
addu	$14,$8,$14
sll	$9,$3,2
sll	$24,$13,1
sll	$3,$3,4
sll	$13,$13,3
addu	$8,$9,$3
subu	$13,$24,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
subu	$2,$2,$15
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$3,3($5)
lbu	$9,1($5)
lbu	$8,2($5)
lbu	$13,4($5)
lbu	$14,0($5)
lbu	$15,5($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$24,6($5)
sll	$9,$8,2
addu	$15,$14,$15
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$15,1
addu	$3,$8,$13
addu	$2,$9,$15
addu	$14,$14,$24
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,2($4)
lbu	$3,4($5)
lbu	$9,2($5)
lbu	$8,3($5)
lbu	$13,5($5)
lbu	$2,6($5)
lbu	$14,1($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$15,0($5)
sll	$9,$8,2
lbu	$24,7($5)
addu	$14,$14,$2
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$8,$15,$24
addu	$2,$3,$2
subu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,3($4)
lbu	$3,5($5)
lbu	$9,3($5)
lbu	$8,4($5)
lbu	$13,6($5)
lbu	$2,7($5)
lbu	$14,2($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$15,1($5)
sll	$9,$8,2
lbu	$24,8($5)
addu	$14,$14,$2
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$8,$15,$24
addu	$2,$3,$2
subu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,4($4)
lbu	$3,6($5)
lbu	$9,4($5)
lbu	$8,5($5)
lbu	$13,7($5)
lbu	$14,8($5)
lbu	$15,3($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$24,2($5)
sll	$9,$8,2
addu	$15,$15,$14
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$15,1
addu	$3,$8,$13
addu	$2,$9,$15
addu	$14,$14,$24
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,5($4)
lbu	$8,8($5)
lbu	$15,7($5)
lbu	$3,6($5)
lbu	$13,5($5)
lbu	$14,4($5)
addu	$3,$3,$15
lbu	$25,3($5)
addu	$13,$13,$8
addu	$14,$8,$14
sll	$9,$3,2
sll	$24,$13,1
sll	$3,$3,4
sll	$13,$13,3
addu	$8,$9,$3
subu	$13,$24,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
subu	$2,$2,$15
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,6($4)
lbu	$9,7($5)
lbu	$13,8($5)
lbu	$15,6($5)
lbu	$14,5($5)
addu	$3,$9,$13
lbu	$25,4($5)
addu	$13,$13,$15
addu	$14,$9,$14
sll	$24,$13,1
sll	$9,$3,2
sll	$13,$13,3
sll	$3,$3,4
subu	$13,$24,$13
addu	$8,$9,$3
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
addu	$5,$5,$7
subu	$2,$2,$15
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,7($4)
bne	$12,$10,$L591
addu	$4,$4,$6

$L594:
j	$31
nop

.set	macro
.set	reorder
.end	put_mpeg4_qpel8_h_lowpass
.size	put_mpeg4_qpel8_h_lowpass, .-put_mpeg4_qpel8_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mpeg4_qpel8_v_lowpass
.type	put_mpeg4_qpel8_v_lowpass, @function
put_mpeg4_qpel8_v_lowpass:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
addiu	$2,$4,8
sw	$21,48($sp)
lui	$21,%hi(ff_cropTbl+1024)
sw	$22,52($sp)
move	$22,$4
addiu	$21,$21,%lo(ff_cropTbl+1024)
sw	$18,36($sp)
sw	$fp,60($sp)
move	$18,$5
sw	$23,56($sp)
sw	$20,44($sp)
sw	$19,40($sp)
sw	$17,32($sp)
sw	$16,28($sp)
sw	$2,16($sp)
$L596:
addu	$2,$18,$7
lbu	$3,0($18)
addu	$23,$22,$6
addu	$4,$2,$7
addu	$19,$23,$6
lbu	$2,0($2)
addu	$5,$4,$7
lbu	$11,0($4)
addu	$4,$19,$6
addu	$13,$3,$2
lbu	$10,0($5)
addu	$5,$5,$7
addu	$9,$3,$11
sw	$4,8($sp)
sll	$4,$13,2
addu	$17,$2,$10
lbu	$8,0($5)
sll	$14,$9,1
sll	$13,$13,4
sll	$9,$9,3
sll	$15,$17,1
subu	$9,$14,$9
addu	$13,$4,$13
addu	$16,$3,$10
addu	$4,$13,$9
addu	$12,$15,$17
sll	$13,$16,3
sll	$fp,$16,1
addu	$12,$4,$12
addu	$16,$11,$8
addu	$14,$2,$11
subu	$12,$12,$16
addu	$5,$5,$7
addiu	$12,$12,16
addu	$15,$3,$8
sra	$12,$12,5
lbu	$9,0($5)
sll	$24,$14,2
sll	$14,$14,4
addu	$12,$21,$12
addu	$20,$11,$10
addu	$4,$24,$14
subu	$fp,$fp,$13
lbu	$12,0($12)
addu	$14,$2,$8
sll	$24,$15,1
addu	$fp,$4,$fp
sll	$17,$14,1
sw	$12,12($sp)
addu	$5,$5,$7
addu	$24,$24,$15
sll	$4,$20,2
sll	$15,$20,4
lbu	$13,0($5)
sll	$14,$14,3
addu	$16,$3,$9
addu	$20,$4,$15
subu	$14,$17,$14
addu	$15,$10,$8
addu	$17,$11,$9
addu	$24,$fp,$24
addu	$fp,$2,$9
sll	$4,$16,1
addu	$25,$20,$14
subu	$24,$24,$fp
sll	$20,$15,2
sll	$fp,$17,1
sll	$15,$15,4
sll	$12,$17,3
addu	$4,$4,$16
addu	$20,$20,$15
addu	$16,$2,$13
subu	$17,$fp,$12
addu	$5,$5,$7
addiu	$24,$24,16
addu	$4,$25,$4
addu	$25,$3,$13
lbu	$14,0($5)
addu	$17,$20,$17
lw	$20,12($sp)
sll	$fp,$16,1
sra	$24,$24,5
subu	$4,$4,$25
addu	$5,$5,$7
addu	$24,$21,$24
addu	$16,$fp,$16
addiu	$4,$4,16
lbu	$5,0($5)
addu	$15,$8,$9
sb	$20,0($22)
addu	$12,$10,$13
lbu	$fp,0($24)
addu	$16,$17,$16
addu	$3,$3,$14
sra	$4,$4,5
addu	$20,$11,$14
sb	$fp,0($23)
sll	$25,$15,2
sll	$24,$12,1
subu	$3,$16,$3
sll	$15,$15,4
sll	$12,$12,3
addu	$4,$21,$4
addu	$15,$25,$15
subu	$12,$24,$12
sll	$25,$20,1
lbu	$4,0($4)
addiu	$3,$3,16
addu	$23,$9,$13
addu	$fp,$8,$14
addu	$12,$15,$12
sb	$4,0($19)
addu	$20,$25,$20
sra	$3,$3,5
addu	$16,$10,$5
sll	$17,$23,2
sll	$24,$fp,1
sll	$23,$23,4
sll	$fp,$fp,3
addu	$12,$12,$20
addu	$2,$2,$5
addu	$3,$21,$3
addu	$17,$17,$23
subu	$fp,$24,$fp
sll	$23,$16,1
lbu	$20,0($3)
addu	$4,$9,$5
addu	$19,$13,$14
subu	$2,$12,$2
addu	$17,$17,$fp
addu	$23,$23,$16
sll	$3,$19,4
addu	$16,$8,$5
sll	$12,$19,2
lw	$19,8($sp)
sll	$15,$4,1
addiu	$2,$2,16
sll	$4,$4,3
sb	$20,0($19)
addu	$23,$17,$23
lw	$20,8($sp)
addu	$11,$11,$5
addu	$12,$12,$3
subu	$15,$15,$4
sra	$2,$2,5
sll	$4,$16,1
addu	$3,$14,$5
addu	$19,$13,$5
subu	$11,$23,$11
addu	$2,$21,$2
addu	$15,$12,$15
addu	$12,$4,$16
sll	$5,$19,1
lbu	$17,0($2)
sll	$4,$3,2
addu	$16,$9,$14
addiu	$11,$11,16
sll	$3,$3,4
sll	$19,$19,3
addu	$12,$15,$12
addu	$10,$10,$14
addu	$3,$4,$3
sra	$11,$11,5
subu	$5,$5,$19
sll	$9,$16,1
subu	$10,$12,$10
addu	$4,$20,$6
addu	$5,$3,$5
addu	$11,$21,$11
sb	$17,0($4)
addu	$9,$9,$16
addiu	$10,$10,16
addu	$9,$5,$9
lbu	$11,0($11)
addu	$2,$8,$13
sra	$10,$10,5
subu	$2,$9,$2
addu	$3,$4,$6
addu	$4,$21,$10
addiu	$2,$2,16
sb	$11,0($3)
addu	$3,$3,$6
lbu	$5,0($4)
sra	$2,$2,5
addu	$4,$3,$6
addu	$2,$21,$2
addiu	$22,$22,1
sb	$5,0($3)
lbu	$2,0($2)
sb	$2,0($4)
lw	$2,16($sp)
bne	$22,$2,$L596
addiu	$18,$18,1

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_mpeg4_qpel8_v_lowpass
.size	put_mpeg4_qpel8_v_lowpass, .-put_mpeg4_qpel8_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mpeg4_qpel16_h_lowpass
.type	put_mpeg4_qpel16_h_lowpass, @function
put_mpeg4_qpel16_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,16($sp)
blez	$11,$L604
lui	$2,%hi(ff_cropTbl+1024)

move	$12,$0
addiu	$2,$2,%lo(ff_cropTbl+1024)
$L601:
lbu	$10,1($5)
addiu	$12,$12,1
lbu	$13,0($5)
lbu	$15,2($5)
lbu	$14,3($5)
addu	$8,$13,$10
lbu	$25,4($5)
addu	$13,$13,$15
addu	$14,$10,$14
sll	$24,$13,1
sll	$10,$8,2
sll	$13,$13,3
sll	$8,$8,4
subu	$13,$24,$13
addu	$9,$10,$8
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
subu	$3,$3,$15
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($4)
lbu	$9,0($5)
lbu	$15,1($5)
lbu	$8,2($5)
lbu	$13,3($5)
lbu	$14,4($5)
addu	$8,$15,$8
lbu	$25,5($5)
addu	$13,$9,$13
addu	$14,$9,$14
sll	$10,$8,2
sll	$24,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$9,$10,$8
subu	$13,$24,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
subu	$3,$3,$15
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,1($4)
lbu	$8,3($5)
lbu	$10,1($5)
lbu	$9,2($5)
lbu	$13,4($5)
lbu	$14,0($5)
lbu	$15,5($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$24,6($5)
sll	$10,$9,2
addu	$15,$14,$15
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$15,1
addu	$8,$9,$13
addu	$3,$10,$15
addu	$14,$14,$24
addu	$3,$8,$3
subu	$3,$3,$14
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,2($4)
lbu	$8,4($5)
lbu	$10,2($5)
lbu	$9,3($5)
lbu	$13,5($5)
lbu	$3,6($5)
lbu	$14,1($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,0($5)
sll	$10,$9,2
lbu	$24,7($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,3($4)
lbu	$8,5($5)
lbu	$10,3($5)
lbu	$9,4($5)
lbu	$13,6($5)
lbu	$3,7($5)
lbu	$14,2($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,1($5)
sll	$10,$9,2
lbu	$24,8($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,4($4)
lbu	$8,6($5)
lbu	$10,4($5)
lbu	$9,5($5)
lbu	$13,7($5)
lbu	$3,8($5)
lbu	$14,3($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,2($5)
sll	$10,$9,2
lbu	$24,9($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,5($4)
lbu	$8,7($5)
lbu	$10,5($5)
lbu	$9,6($5)
lbu	$13,8($5)
lbu	$3,9($5)
lbu	$14,4($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,3($5)
sll	$10,$9,2
lbu	$24,10($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,6($4)
lbu	$8,8($5)
lbu	$10,6($5)
lbu	$9,7($5)
lbu	$13,9($5)
lbu	$3,10($5)
lbu	$14,5($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,4($5)
sll	$10,$9,2
lbu	$24,11($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,7($4)
lbu	$8,9($5)
lbu	$10,7($5)
lbu	$9,8($5)
lbu	$13,10($5)
lbu	$3,11($5)
lbu	$14,6($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,5($5)
sll	$10,$9,2
lbu	$24,12($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,8($4)
lbu	$8,10($5)
lbu	$10,8($5)
lbu	$9,9($5)
lbu	$13,11($5)
lbu	$3,12($5)
lbu	$14,7($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$24,13($5)
sll	$10,$9,2
lbu	$15,6($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,9($4)
lbu	$8,11($5)
lbu	$10,9($5)
lbu	$9,10($5)
lbu	$13,12($5)
lbu	$3,13($5)
lbu	$14,8($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,7($5)
sll	$10,$9,2
lbu	$24,14($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,10($4)
lbu	$8,12($5)
lbu	$10,10($5)
lbu	$9,11($5)
lbu	$13,13($5)
lbu	$3,14($5)
lbu	$14,9($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,8($5)
sll	$10,$9,2
lbu	$24,15($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11($4)
lbu	$8,13($5)
lbu	$10,11($5)
lbu	$9,12($5)
lbu	$13,14($5)
lbu	$3,15($5)
lbu	$14,10($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,9($5)
sll	$10,$9,2
lbu	$24,16($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,12($4)
lbu	$8,14($5)
lbu	$10,12($5)
lbu	$9,13($5)
lbu	$13,15($5)
lbu	$14,16($5)
lbu	$15,11($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$24,10($5)
sll	$10,$9,2
addu	$15,$15,$14
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$15,1
addu	$8,$9,$13
addu	$3,$10,$15
addu	$14,$14,$24
addu	$3,$8,$3
subu	$3,$3,$14
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,13($4)
lbu	$15,15($5)
lbu	$9,16($5)
lbu	$8,14($5)
lbu	$13,13($5)
lbu	$14,12($5)
addu	$8,$8,$15
lbu	$25,11($5)
addu	$13,$13,$9
addu	$14,$9,$14
sll	$10,$8,2
sll	$24,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$9,$10,$8
subu	$13,$24,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
subu	$3,$3,$15
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,14($4)
lbu	$10,15($5)
lbu	$13,16($5)
lbu	$15,14($5)
lbu	$14,13($5)
addu	$8,$10,$13
lbu	$25,12($5)
addu	$13,$13,$15
addu	$14,$10,$14
sll	$24,$13,1
sll	$10,$8,2
sll	$13,$13,3
sll	$8,$8,4
subu	$13,$24,$13
addu	$9,$10,$8
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
addu	$5,$5,$7
subu	$3,$3,$15
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,15($4)
bne	$12,$11,$L601
addu	$4,$4,$6

$L604:
j	$31
nop

.set	macro
.set	reorder
.end	put_mpeg4_qpel16_h_lowpass
.size	put_mpeg4_qpel16_h_lowpass, .-put_mpeg4_qpel16_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mpeg4_qpel16_v_lowpass
.type	put_mpeg4_qpel16_v_lowpass, @function
put_mpeg4_qpel16_v_lowpass:
.frame	$sp,80,$31		# vars= 32, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$2,$4,16
addiu	$sp,$sp,-80
move	$11,$6
sw	$23,72($sp)
move	$23,$7
sw	$fp,76($sp)
sw	$22,68($sp)
sw	$21,64($sp)
sw	$20,60($sp)
sw	$19,56($sp)
sw	$18,52($sp)
sw	$17,48($sp)
sw	$16,44($sp)
sw	$4,80($sp)
sw	$2,36($sp)
sw	$5,84($sp)
$L606:
lw	$3,84($sp)
lui	$25,%hi(ff_cropTbl+1024)
lw	$4,80($sp)
addiu	$25,$25,%lo(ff_cropTbl+1024)
addu	$2,$3,$23
addu	$4,$4,$11
lbu	$9,0($3)
addu	$3,$2,$23
addu	$5,$4,$11
lbu	$8,0($2)
addu	$2,$3,$23
sw	$4,24($sp)
addu	$6,$5,$11
lbu	$7,0($3)
addu	$3,$2,$23
sw	$5,28($sp)
addu	$12,$9,$8
sw	$6,32($sp)
addu	$10,$9,$7
lbu	$6,0($2)
addu	$2,$3,$23
lbu	$5,0($3)
sll	$14,$10,1
addu	$3,$2,$23
lbu	$4,0($2)
addu	$13,$8,$6
addu	$2,$3,$23
sll	$15,$12,2
lbu	$3,0($3)
sll	$12,$12,4
lbu	$16,0($2)
addu	$2,$2,$23
sll	$10,$10,3
addu	$15,$15,$12
lbu	$18,0($2)
addu	$2,$2,$23
sw	$16,8($sp)
subu	$10,$14,$10
sll	$16,$13,1
lbu	$19,0($2)
addu	$2,$2,$23
addu	$12,$9,$6
sw	$18,12($sp)
addu	$14,$15,$10
addu	$18,$8,$7
lbu	$21,0($2)
addu	$16,$16,$13
sw	$19,16($sp)
addu	$13,$9,$5
sll	$15,$12,1
sll	$10,$18,2
sw	$21,20($sp)
sll	$17,$18,4
addu	$16,$14,$16
addu	$2,$2,$23
addu	$14,$7,$5
sll	$12,$12,3
addu	$18,$10,$17
subu	$12,$15,$12
addu	$17,$8,$5
addu	$15,$7,$6
sll	$10,$13,1
subu	$16,$16,$14
lbu	$14,0($2)
addu	$2,$2,$23
addu	$21,$9,$4
addu	$10,$10,$13
sll	$19,$15,2
sll	$20,$15,4
sll	$22,$17,1
sll	$13,$17,3
addiu	$16,$16,16
addu	$24,$18,$12
lbu	$12,0($2)
addu	$15,$19,$20
subu	$17,$22,$13
addu	$2,$2,$23
sll	$19,$21,1
sra	$16,$16,5
addu	$18,$15,$17
lbu	$13,0($2)
addu	$16,$25,$16
addu	$10,$24,$10
addu	$19,$19,$21
addu	$24,$8,$4
lbu	$fp,0($16)
addu	$22,$7,$4
addu	$2,$2,$23
addu	$19,$18,$19
addu	$15,$9,$3
subu	$10,$10,$24
lbu	$17,0($2)
addu	$25,$8,$3
sll	$21,$22,1
sll	$16,$22,3
addu	$2,$2,$23
subu	$15,$19,$15
addiu	$10,$10,16
lui	$19,%hi(ff_cropTbl+1024)
lbu	$18,0($2)
addu	$20,$6,$5
subu	$22,$21,$16
sll	$21,$25,1
addiu	$19,$19,%lo(ff_cropTbl+1024)
sra	$10,$10,5
addu	$21,$21,$25
lw	$25,80($sp)
sll	$24,$20,2
addu	$2,$2,$23
sll	$20,$20,4
addu	$10,$19,$10
lw	$19,8($sp)
addu	$24,$24,$20
lbu	$2,0($2)
sb	$fp,0($25)
addu	$20,$5,$4
addu	$16,$6,$3
lbu	$10,0($10)
addu	$22,$24,$22
lw	$25,24($sp)
addu	$fp,$7,$19
sll	$24,$16,1
sll	$19,$20,2
sb	$10,0($25)
addu	$22,$22,$21
lw	$10,8($sp)
addiu	$15,$15,16
sll	$20,$20,4
sll	$16,$16,3
lui	$21,%hi(ff_cropTbl+1024)
subu	$16,$24,$16
addu	$20,$19,$20
addiu	$21,$21,%lo(ff_cropTbl+1024)
sra	$15,$15,5
addu	$20,$20,$16
lw	$16,12($sp)
addu	$15,$21,$15
addu	$9,$9,$10
sll	$19,$fp,1
subu	$22,$22,$9
addu	$9,$6,$16
lbu	$16,0($15)
addu	$fp,$19,$fp
lw	$19,28($sp)
addu	$25,$4,$3
addu	$10,$5,$10
sb	$16,0($19)
addu	$20,$20,$fp
lw	$16,12($sp)
sll	$21,$25,2
sll	$15,$25,4
addiu	$22,$22,16
addu	$8,$8,$16
sll	$25,$10,1
subu	$20,$20,$8
sll	$10,$10,3
lui	$8,%hi(ff_cropTbl+1024)
addu	$15,$21,$15
lw	$21,8($sp)
subu	$10,$25,$10
lw	$25,12($sp)
addiu	$8,$8,%lo(ff_cropTbl+1024)
sra	$22,$22,5
sll	$16,$9,1
addu	$22,$8,$22
addu	$10,$15,$10
lw	$15,16($sp)
addu	$19,$3,$21
lw	$21,16($sp)
addu	$9,$16,$9
lw	$16,32($sp)
addu	$24,$4,$25
addu	$8,$5,$15
lbu	$15,0($22)
addu	$7,$7,$21
addu	$10,$10,$9
lw	$9,8($sp)
sll	$fp,$19,2
sll	$22,$19,4
sb	$15,0($16)
sll	$19,$24,1
sll	$24,$24,3
addu	$21,$3,$21
subu	$10,$10,$7
lw	$7,20($sp)
addu	$22,$fp,$22
addu	$25,$9,$25
subu	$24,$19,$24
sll	$16,$8,1
sll	$9,$21,1
addiu	$20,$20,16
lui	$19,%hi(ff_cropTbl+1024)
sll	$21,$21,3
addu	$15,$4,$7
addu	$8,$16,$8
lw	$16,16($sp)
sll	$7,$25,2
addu	$24,$22,$24
sll	$25,$25,4
subu	$21,$9,$21
lw	$9,12($sp)
addiu	$19,$19,%lo(ff_cropTbl+1024)
sra	$20,$20,5
addu	$24,$24,$8
lw	$8,20($sp)
addu	$25,$7,$25
lw	$7,8($sp)
addu	$20,$19,$20
addiu	$10,$10,16
addu	$19,$9,$16
lui	$9,%hi(ff_cropTbl+1024)
lbu	$20,0($20)
addu	$16,$7,$8
addu	$6,$6,$8
lw	$8,32($sp)
addiu	$9,$9,%lo(ff_cropTbl+1024)
sra	$10,$10,5
sll	$fp,$15,1
addu	$22,$8,$11
addu	$10,$9,$10
addu	$15,$fp,$15
sb	$20,0($22)
addu	$25,$25,$21
lw	$21,12($sp)
lbu	$8,0($10)
addu	$5,$5,$14
sll	$10,$16,1
subu	$24,$24,$6
addu	$25,$25,$15
lw	$15,20($sp)
sll	$16,$16,3
addu	$20,$3,$14
addu	$fp,$21,$14
subu	$16,$10,$16
lw	$10,16($sp)
subu	$25,$25,$5
addu	$22,$22,$11
addiu	$24,$24,16
lui	$5,%hi(ff_cropTbl+1024)
sb	$8,0($22)
sll	$7,$19,2
sll	$6,$20,1
sll	$19,$19,4
sll	$8,$fp,1
addiu	$5,$5,%lo(ff_cropTbl+1024)
sra	$24,$24,5
sll	$fp,$fp,3
addu	$9,$10,$15
addu	$20,$6,$20
lw	$6,8($sp)
addu	$19,$7,$19
subu	$fp,$8,$fp
lw	$8,20($sp)
addu	$24,$5,$24
sll	$15,$9,2
addu	$19,$19,$16
addiu	$25,$25,16
lbu	$24,0($24)
sll	$9,$9,4
lui	$16,%hi(ff_cropTbl+1024)
addu	$21,$6,$12
addu	$7,$8,$14
addu	$19,$19,$20
addu	$4,$4,$12
addu	$9,$15,$9
lw	$15,16($sp)
addiu	$16,$16,%lo(ff_cropTbl+1024)
sra	$25,$25,5
sll	$10,$21,1
subu	$19,$19,$4
lw	$4,12($sp)
addu	$22,$22,$11
addu	$25,$16,$25
sll	$16,$7,2
sb	$24,0($22)
sll	$7,$7,4
addu	$5,$15,$12
lbu	$25,0($25)
addu	$21,$10,$21
addu	$9,$9,$fp
addu	$7,$16,$7
addiu	$19,$19,16
lui	$16,%hi(ff_cropTbl+1024)
sll	$24,$5,1
addu	$3,$3,$13
addu	$9,$9,$21
lw	$21,16($sp)
sll	$5,$5,3
addiu	$16,$16,%lo(ff_cropTbl+1024)
sra	$19,$19,5
subu	$5,$24,$5
addu	$20,$14,$12
move	$10,$8
subu	$9,$9,$3
lw	$3,8($sp)
addu	$8,$8,$13
addu	$22,$22,$11
addu	$6,$4,$13
addu	$19,$16,$19
sb	$25,0($22)
sll	$fp,$8,1
sll	$25,$20,2
addu	$7,$7,$5
lbu	$19,0($19)
addiu	$9,$9,16
sll	$20,$20,4
sll	$8,$8,3
lui	$5,%hi(ff_cropTbl+1024)
addu	$4,$21,$17
sll	$15,$6,1
subu	$8,$fp,$8
addu	$20,$25,$20
addiu	$5,$5,%lo(ff_cropTbl+1024)
sra	$9,$9,5
addu	$6,$15,$6
addu	$21,$3,$17
sll	$3,$4,1
addu	$9,$5,$9
addu	$22,$22,$11
addu	$20,$20,$8
lw	$8,12($sp)
addu	$4,$3,$4
sb	$19,0($22)
addu	$7,$7,$6
lbu	$25,0($9)
addu	$24,$12,$13
addu	$16,$14,$17
addu	$20,$20,$4
addu	$9,$8,$18
subu	$7,$7,$21
addu	$19,$10,$18
sll	$5,$24,2
sll	$6,$16,1
sll	$24,$24,4
sll	$16,$16,3
subu	$10,$20,$9
addiu	$7,$7,16
lui	$9,%hi(ff_cropTbl+1024)
subu	$16,$6,$16
addu	$24,$5,$24
sll	$8,$19,1
addiu	$9,$9,%lo(ff_cropTbl+1024)
sra	$7,$7,5
addu	$19,$8,$19
addu	$24,$24,$16
lw	$16,16($sp)
addu	$22,$22,$11
addu	$7,$9,$7
addu	$4,$13,$17
sb	$25,0($22)
addu	$fp,$12,$18
addu	$24,$24,$19
lbu	$7,0($7)
addiu	$10,$10,16
lui	$19,%hi(ff_cropTbl+1024)
addu	$15,$16,$2
addu	$3,$14,$2
sll	$20,$4,2
sll	$21,$fp,1
sll	$9,$fp,3
addiu	$19,$19,%lo(ff_cropTbl+1024)
sll	$4,$4,4
sra	$10,$10,5
subu	$15,$24,$15
addu	$4,$20,$4
subu	$fp,$21,$9
lw	$21,20($sp)
addu	$5,$22,$11
sll	$8,$3,1
addu	$10,$19,$10
sb	$7,0($5)
addu	$9,$13,$2
addu	$6,$17,$18
addiu	$15,$15,16
lbu	$10,0($10)
lui	$25,%hi(ff_cropTbl+1024)
addu	$fp,$4,$fp
addu	$8,$8,$3
addu	$16,$12,$2
addiu	$25,$25,%lo(ff_cropTbl+1024)
sll	$4,$6,2
sll	$3,$9,3
sll	$6,$6,4
sra	$15,$15,5
sll	$20,$9,1
addu	$19,$21,$2
addu	$8,$fp,$8
addu	$4,$4,$6
subu	$9,$20,$3
addu	$6,$5,$11
sll	$7,$16,1
addu	$5,$25,$15
sb	$10,0($6)
addu	$3,$18,$2
subu	$8,$8,$19
addu	$2,$17,$2
addu	$9,$4,$9
addu	$7,$7,$16
lbu	$16,0($5)
addu	$15,$13,$18
sll	$5,$3,2
sll	$13,$2,1
sll	$10,$2,3
sll	$3,$3,4
addiu	$8,$8,16
addu	$7,$9,$7
lw	$9,84($sp)
addu	$14,$14,$18
addu	$4,$5,$3
sra	$8,$8,5
subu	$3,$13,$10
lw	$10,36($sp)
sll	$13,$15,1
subu	$14,$7,$14
addu	$5,$6,$11
addu	$3,$4,$3
addu	$8,$25,$8
sb	$16,0($5)
addu	$13,$13,$15
addiu	$4,$14,16
addu	$13,$3,$13
lbu	$6,0($8)
addu	$2,$12,$17
lw	$8,80($sp)
sra	$4,$4,5
subu	$2,$13,$2
addu	$3,$5,$11
addu	$4,$25,$4
addiu	$2,$2,16
sb	$6,0($3)
addu	$3,$3,$11
lbu	$5,0($4)
sra	$2,$2,5
addiu	$8,$8,1
addu	$2,$25,$2
addiu	$9,$9,1
sb	$5,0($3)
addu	$4,$3,$11
sw	$8,80($sp)
sw	$9,84($sp)
lbu	$2,0($2)
bne	$8,$10,$L606
sb	$2,0($4)

lw	$fp,76($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	put_mpeg4_qpel16_v_lowpass
.size	put_mpeg4_qpel16_v_lowpass, .-put_mpeg4_qpel16_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc20_c
.type	put_qpel8_mc20_c, @function
put_qpel8_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,8			# 0x8
addiu	$sp,$sp,-40
sw	$2,16($sp)
sw	$31,36($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$7,$6

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_qpel8_mc20_c
.size	put_qpel8_mc20_c, .-put_qpel8_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc22_c
.type	put_qpel8_mc22_c, @function
put_qpel8_mc22_c:
.frame	$sp,120,$31		# vars= 72, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-120
li	$2,9			# 0x9
sw	$16,104($sp)
addiu	$16,$sp,32
sw	$17,108($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$18,112($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,116($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16

li	$7,8			# 0x8
move	$4,$18
move	$5,$16
.option	pic0
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$17

lw	$31,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
j	$31
addiu	$sp,$sp,120

.set	macro
.set	reorder
.end	put_qpel8_mc22_c
.size	put_qpel8_mc22_c, .-put_qpel8_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc20_c
.type	put_qpel16_mc20_c, @function
put_qpel16_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,16			# 0x10
addiu	$sp,$sp,-40
sw	$2,16($sp)
sw	$31,36($sp)
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$7,$6

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_qpel16_mc20_c
.size	put_qpel16_mc20_c, .-put_qpel16_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc22_c
.type	put_qpel16_mc22_c, @function
put_qpel16_mc22_c:
.frame	$sp,320,$31		# vars= 272, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
li	$2,17			# 0x11
sw	$16,304($sp)
addiu	$16,$sp,32
sw	$17,308($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$18,312($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,316($sp)
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17

lw	$31,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	put_qpel16_mc22_c
.size	put_qpel16_mc22_c, .-put_qpel16_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_mpeg4_qpel8_h_lowpass
.type	put_no_rnd_mpeg4_qpel8_h_lowpass, @function
put_no_rnd_mpeg4_qpel8_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$10,16($sp)
blez	$10,$L622
lui	$11,%hi(ff_cropTbl+1024)

move	$12,$0
addiu	$11,$11,%lo(ff_cropTbl+1024)
$L619:
lbu	$9,1($5)
addiu	$12,$12,1
lbu	$13,0($5)
lbu	$15,2($5)
lbu	$14,3($5)
addu	$3,$13,$9
lbu	$25,4($5)
addu	$13,$13,$15
addu	$14,$9,$14
sll	$24,$13,1
sll	$9,$3,2
sll	$13,$13,3
sll	$3,$3,4
subu	$13,$24,$13
addu	$8,$9,$3
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
subu	$2,$2,$15
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$8,0($5)
lbu	$15,1($5)
lbu	$3,2($5)
lbu	$13,3($5)
lbu	$14,4($5)
addu	$3,$15,$3
lbu	$25,5($5)
addu	$13,$8,$13
addu	$14,$8,$14
sll	$9,$3,2
sll	$24,$13,1
sll	$3,$3,4
sll	$13,$13,3
addu	$8,$9,$3
subu	$13,$24,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
subu	$2,$2,$15
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$3,3($5)
lbu	$9,1($5)
lbu	$8,2($5)
lbu	$13,4($5)
lbu	$14,0($5)
lbu	$15,5($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$24,6($5)
sll	$9,$8,2
addu	$15,$14,$15
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$15,1
addu	$3,$8,$13
addu	$2,$9,$15
addu	$14,$14,$24
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,2($4)
lbu	$3,4($5)
lbu	$9,2($5)
lbu	$8,3($5)
lbu	$13,5($5)
lbu	$2,6($5)
lbu	$14,1($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$15,0($5)
sll	$9,$8,2
lbu	$24,7($5)
addu	$14,$14,$2
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$8,$15,$24
addu	$2,$3,$2
subu	$2,$2,$8
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,3($4)
lbu	$3,5($5)
lbu	$9,3($5)
lbu	$8,4($5)
lbu	$13,6($5)
lbu	$2,7($5)
lbu	$14,2($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$15,1($5)
sll	$9,$8,2
lbu	$24,8($5)
addu	$14,$14,$2
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$8,$15,$24
addu	$2,$3,$2
subu	$2,$2,$8
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,4($4)
lbu	$3,6($5)
lbu	$9,4($5)
lbu	$8,5($5)
lbu	$13,7($5)
lbu	$14,8($5)
lbu	$15,3($5)
addu	$8,$8,$3
addu	$13,$9,$13
lbu	$24,2($5)
sll	$9,$8,2
addu	$15,$15,$14
sll	$3,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$3,$13
sll	$9,$15,1
addu	$3,$8,$13
addu	$2,$9,$15
addu	$14,$14,$24
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,5($4)
lbu	$8,8($5)
lbu	$15,7($5)
lbu	$3,6($5)
lbu	$13,5($5)
lbu	$14,4($5)
addu	$3,$3,$15
lbu	$25,3($5)
addu	$13,$13,$8
addu	$14,$8,$14
sll	$9,$3,2
sll	$24,$13,1
sll	$3,$3,4
sll	$13,$13,3
addu	$8,$9,$3
subu	$13,$24,$13
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
subu	$2,$2,$15
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,6($4)
lbu	$9,7($5)
lbu	$13,8($5)
lbu	$15,6($5)
lbu	$14,5($5)
addu	$3,$9,$13
lbu	$25,4($5)
addu	$13,$13,$15
addu	$14,$9,$14
sll	$24,$13,1
sll	$9,$3,2
sll	$13,$13,3
sll	$3,$3,4
subu	$13,$24,$13
addu	$8,$9,$3
sll	$9,$14,1
addu	$3,$8,$13
addu	$2,$9,$14
addu	$15,$15,$25
addu	$2,$3,$2
addu	$5,$5,$7
subu	$2,$2,$15
addiu	$2,$2,15
sra	$2,$2,5
addu	$2,$11,$2
lbu	$2,0($2)
sb	$2,7($4)
bne	$12,$10,$L619
addu	$4,$4,$6

$L622:
j	$31
nop

.set	macro
.set	reorder
.end	put_no_rnd_mpeg4_qpel8_h_lowpass
.size	put_no_rnd_mpeg4_qpel8_h_lowpass, .-put_no_rnd_mpeg4_qpel8_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_mpeg4_qpel8_v_lowpass
.type	put_no_rnd_mpeg4_qpel8_v_lowpass, @function
put_no_rnd_mpeg4_qpel8_v_lowpass:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
addiu	$2,$4,8
sw	$21,48($sp)
lui	$21,%hi(ff_cropTbl+1024)
sw	$22,52($sp)
move	$22,$4
addiu	$21,$21,%lo(ff_cropTbl+1024)
sw	$18,36($sp)
sw	$fp,60($sp)
move	$18,$5
sw	$23,56($sp)
sw	$20,44($sp)
sw	$19,40($sp)
sw	$17,32($sp)
sw	$16,28($sp)
sw	$2,16($sp)
$L624:
addu	$2,$18,$7
lbu	$3,0($18)
addu	$23,$22,$6
addu	$4,$2,$7
addu	$19,$23,$6
lbu	$2,0($2)
addu	$5,$4,$7
lbu	$11,0($4)
addu	$4,$19,$6
addu	$13,$3,$2
lbu	$10,0($5)
addu	$5,$5,$7
addu	$9,$3,$11
sw	$4,8($sp)
sll	$4,$13,2
addu	$17,$2,$10
lbu	$8,0($5)
sll	$14,$9,1
sll	$13,$13,4
sll	$9,$9,3
sll	$15,$17,1
subu	$9,$14,$9
addu	$13,$4,$13
addu	$16,$3,$10
addu	$4,$13,$9
addu	$12,$15,$17
sll	$13,$16,3
sll	$fp,$16,1
addu	$12,$4,$12
addu	$16,$11,$8
addu	$14,$2,$11
subu	$12,$12,$16
addu	$5,$5,$7
addiu	$12,$12,15
addu	$15,$3,$8
sra	$12,$12,5
lbu	$9,0($5)
sll	$24,$14,2
sll	$14,$14,4
addu	$12,$21,$12
addu	$20,$11,$10
addu	$4,$24,$14
subu	$fp,$fp,$13
lbu	$12,0($12)
addu	$14,$2,$8
sll	$24,$15,1
addu	$fp,$4,$fp
sll	$17,$14,1
sw	$12,12($sp)
addu	$5,$5,$7
addu	$24,$24,$15
sll	$4,$20,2
sll	$15,$20,4
lbu	$13,0($5)
sll	$14,$14,3
addu	$16,$3,$9
addu	$20,$4,$15
subu	$14,$17,$14
addu	$15,$10,$8
addu	$17,$11,$9
addu	$24,$fp,$24
addu	$fp,$2,$9
sll	$4,$16,1
addu	$25,$20,$14
subu	$24,$24,$fp
sll	$20,$15,2
sll	$fp,$17,1
sll	$15,$15,4
sll	$12,$17,3
addu	$4,$4,$16
addu	$20,$20,$15
addu	$16,$2,$13
subu	$17,$fp,$12
addu	$5,$5,$7
addiu	$24,$24,15
addu	$4,$25,$4
addu	$25,$3,$13
lbu	$14,0($5)
addu	$17,$20,$17
lw	$20,12($sp)
sll	$fp,$16,1
sra	$24,$24,5
subu	$4,$4,$25
addu	$5,$5,$7
addu	$24,$21,$24
addu	$16,$fp,$16
addiu	$4,$4,15
lbu	$5,0($5)
addu	$15,$8,$9
sb	$20,0($22)
addu	$12,$10,$13
lbu	$fp,0($24)
addu	$16,$17,$16
addu	$3,$3,$14
sra	$4,$4,5
addu	$20,$11,$14
sb	$fp,0($23)
sll	$25,$15,2
sll	$24,$12,1
subu	$3,$16,$3
sll	$15,$15,4
sll	$12,$12,3
addu	$4,$21,$4
addu	$15,$25,$15
subu	$12,$24,$12
sll	$25,$20,1
lbu	$4,0($4)
addiu	$3,$3,15
addu	$23,$9,$13
addu	$fp,$8,$14
addu	$12,$15,$12
sb	$4,0($19)
addu	$20,$25,$20
sra	$3,$3,5
addu	$16,$10,$5
sll	$17,$23,2
sll	$24,$fp,1
sll	$23,$23,4
sll	$fp,$fp,3
addu	$12,$12,$20
addu	$2,$2,$5
addu	$3,$21,$3
addu	$17,$17,$23
subu	$fp,$24,$fp
sll	$23,$16,1
lbu	$20,0($3)
addu	$4,$9,$5
addu	$19,$13,$14
subu	$2,$12,$2
addu	$17,$17,$fp
addu	$23,$23,$16
sll	$3,$19,4
addu	$16,$8,$5
sll	$12,$19,2
lw	$19,8($sp)
sll	$15,$4,1
addiu	$2,$2,15
sll	$4,$4,3
sb	$20,0($19)
addu	$23,$17,$23
lw	$20,8($sp)
addu	$11,$11,$5
addu	$12,$12,$3
subu	$15,$15,$4
sra	$2,$2,5
sll	$4,$16,1
addu	$3,$14,$5
addu	$19,$13,$5
subu	$11,$23,$11
addu	$2,$21,$2
addu	$15,$12,$15
addu	$12,$4,$16
sll	$5,$19,1
lbu	$17,0($2)
sll	$4,$3,2
addu	$16,$9,$14
addiu	$11,$11,15
sll	$3,$3,4
sll	$19,$19,3
addu	$12,$15,$12
addu	$10,$10,$14
addu	$3,$4,$3
sra	$11,$11,5
subu	$5,$5,$19
sll	$9,$16,1
subu	$10,$12,$10
addu	$4,$20,$6
addu	$5,$3,$5
addu	$11,$21,$11
sb	$17,0($4)
addu	$9,$9,$16
addiu	$10,$10,15
addu	$9,$5,$9
lbu	$11,0($11)
addu	$2,$8,$13
sra	$10,$10,5
subu	$2,$9,$2
addu	$3,$4,$6
addu	$4,$21,$10
addiu	$2,$2,15
sb	$11,0($3)
addu	$3,$3,$6
lbu	$5,0($4)
sra	$2,$2,5
addu	$4,$3,$6
addu	$2,$21,$2
addiu	$22,$22,1
sb	$5,0($3)
lbu	$2,0($2)
sb	$2,0($4)
lw	$2,16($sp)
bne	$22,$2,$L624
addiu	$18,$18,1

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_no_rnd_mpeg4_qpel8_v_lowpass
.size	put_no_rnd_mpeg4_qpel8_v_lowpass, .-put_no_rnd_mpeg4_qpel8_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_mpeg4_qpel16_h_lowpass
.type	put_no_rnd_mpeg4_qpel16_h_lowpass, @function
put_no_rnd_mpeg4_qpel16_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,16($sp)
blez	$11,$L632
lui	$2,%hi(ff_cropTbl+1024)

move	$12,$0
addiu	$2,$2,%lo(ff_cropTbl+1024)
$L629:
lbu	$10,1($5)
addiu	$12,$12,1
lbu	$13,0($5)
lbu	$15,2($5)
lbu	$14,3($5)
addu	$8,$13,$10
lbu	$25,4($5)
addu	$13,$13,$15
addu	$14,$10,$14
sll	$24,$13,1
sll	$10,$8,2
sll	$13,$13,3
sll	$8,$8,4
subu	$13,$24,$13
addu	$9,$10,$8
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
subu	$3,$3,$15
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($4)
lbu	$9,0($5)
lbu	$15,1($5)
lbu	$8,2($5)
lbu	$13,3($5)
lbu	$14,4($5)
addu	$8,$15,$8
lbu	$25,5($5)
addu	$13,$9,$13
addu	$14,$9,$14
sll	$10,$8,2
sll	$24,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$9,$10,$8
subu	$13,$24,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
subu	$3,$3,$15
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,1($4)
lbu	$8,3($5)
lbu	$10,1($5)
lbu	$9,2($5)
lbu	$13,4($5)
lbu	$14,0($5)
lbu	$15,5($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$24,6($5)
sll	$10,$9,2
addu	$15,$14,$15
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$15,1
addu	$8,$9,$13
addu	$3,$10,$15
addu	$14,$14,$24
addu	$3,$8,$3
subu	$3,$3,$14
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,2($4)
lbu	$8,4($5)
lbu	$10,2($5)
lbu	$9,3($5)
lbu	$13,5($5)
lbu	$3,6($5)
lbu	$14,1($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,0($5)
sll	$10,$9,2
lbu	$24,7($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,3($4)
lbu	$8,5($5)
lbu	$10,3($5)
lbu	$9,4($5)
lbu	$13,6($5)
lbu	$3,7($5)
lbu	$14,2($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,1($5)
sll	$10,$9,2
lbu	$24,8($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,4($4)
lbu	$8,6($5)
lbu	$10,4($5)
lbu	$9,5($5)
lbu	$13,7($5)
lbu	$3,8($5)
lbu	$14,3($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,2($5)
sll	$10,$9,2
lbu	$24,9($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,5($4)
lbu	$8,7($5)
lbu	$10,5($5)
lbu	$9,6($5)
lbu	$13,8($5)
lbu	$3,9($5)
lbu	$14,4($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,3($5)
sll	$10,$9,2
lbu	$24,10($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,6($4)
lbu	$8,8($5)
lbu	$10,6($5)
lbu	$9,7($5)
lbu	$13,9($5)
lbu	$3,10($5)
lbu	$14,5($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,4($5)
sll	$10,$9,2
lbu	$24,11($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,7($4)
lbu	$8,9($5)
lbu	$10,7($5)
lbu	$9,8($5)
lbu	$13,10($5)
lbu	$3,11($5)
lbu	$14,6($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,5($5)
sll	$10,$9,2
lbu	$24,12($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,8($4)
lbu	$8,10($5)
lbu	$10,8($5)
lbu	$9,9($5)
lbu	$13,11($5)
lbu	$3,12($5)
lbu	$14,7($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$24,13($5)
sll	$10,$9,2
lbu	$15,6($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,9($4)
lbu	$8,11($5)
lbu	$10,9($5)
lbu	$9,10($5)
lbu	$13,12($5)
lbu	$3,13($5)
lbu	$14,8($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,7($5)
sll	$10,$9,2
lbu	$24,14($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,10($4)
lbu	$8,12($5)
lbu	$10,10($5)
lbu	$9,11($5)
lbu	$13,13($5)
lbu	$3,14($5)
lbu	$14,9($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,8($5)
sll	$10,$9,2
lbu	$24,15($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,11($4)
lbu	$8,13($5)
lbu	$10,11($5)
lbu	$9,12($5)
lbu	$13,14($5)
lbu	$3,15($5)
lbu	$14,10($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$15,9($5)
sll	$10,$9,2
lbu	$24,16($5)
addu	$14,$14,$3
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$9,$15,$24
addu	$3,$8,$3
subu	$3,$3,$9
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,12($4)
lbu	$8,14($5)
lbu	$10,12($5)
lbu	$9,13($5)
lbu	$13,15($5)
lbu	$14,16($5)
lbu	$15,11($5)
addu	$9,$9,$8
addu	$13,$10,$13
lbu	$24,10($5)
sll	$10,$9,2
addu	$15,$15,$14
sll	$8,$13,1
sll	$9,$9,4
sll	$13,$13,3
addu	$9,$10,$9
subu	$13,$8,$13
sll	$10,$15,1
addu	$8,$9,$13
addu	$3,$10,$15
addu	$14,$14,$24
addu	$3,$8,$3
subu	$3,$3,$14
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,13($4)
lbu	$15,15($5)
lbu	$9,16($5)
lbu	$8,14($5)
lbu	$13,13($5)
lbu	$14,12($5)
addu	$8,$8,$15
lbu	$25,11($5)
addu	$13,$13,$9
addu	$14,$9,$14
sll	$10,$8,2
sll	$24,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$9,$10,$8
subu	$13,$24,$13
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
subu	$3,$3,$15
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,14($4)
lbu	$10,15($5)
lbu	$13,16($5)
lbu	$15,14($5)
lbu	$14,13($5)
addu	$8,$10,$13
lbu	$25,12($5)
addu	$13,$13,$15
addu	$14,$10,$14
sll	$24,$13,1
sll	$10,$8,2
sll	$13,$13,3
sll	$8,$8,4
subu	$13,$24,$13
addu	$9,$10,$8
sll	$10,$14,1
addu	$8,$9,$13
addu	$3,$10,$14
addu	$15,$15,$25
addu	$3,$8,$3
addu	$5,$5,$7
subu	$3,$3,$15
addiu	$3,$3,15
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,15($4)
bne	$12,$11,$L629
addu	$4,$4,$6

$L632:
j	$31
nop

.set	macro
.set	reorder
.end	put_no_rnd_mpeg4_qpel16_h_lowpass
.size	put_no_rnd_mpeg4_qpel16_h_lowpass, .-put_no_rnd_mpeg4_qpel16_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_mpeg4_qpel16_v_lowpass
.type	put_no_rnd_mpeg4_qpel16_v_lowpass, @function
put_no_rnd_mpeg4_qpel16_v_lowpass:
.frame	$sp,80,$31		# vars= 32, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$2,$4,16
addiu	$sp,$sp,-80
move	$11,$6
sw	$23,72($sp)
move	$23,$7
sw	$fp,76($sp)
sw	$22,68($sp)
sw	$21,64($sp)
sw	$20,60($sp)
sw	$19,56($sp)
sw	$18,52($sp)
sw	$17,48($sp)
sw	$16,44($sp)
sw	$4,80($sp)
sw	$2,36($sp)
sw	$5,84($sp)
$L634:
lw	$3,84($sp)
lui	$25,%hi(ff_cropTbl+1024)
lw	$4,80($sp)
addiu	$25,$25,%lo(ff_cropTbl+1024)
addu	$2,$3,$23
addu	$4,$4,$11
lbu	$9,0($3)
addu	$3,$2,$23
addu	$5,$4,$11
lbu	$8,0($2)
addu	$2,$3,$23
sw	$4,24($sp)
addu	$6,$5,$11
lbu	$7,0($3)
addu	$3,$2,$23
sw	$5,28($sp)
addu	$12,$9,$8
sw	$6,32($sp)
addu	$10,$9,$7
lbu	$6,0($2)
addu	$2,$3,$23
lbu	$5,0($3)
sll	$14,$10,1
addu	$3,$2,$23
lbu	$4,0($2)
addu	$13,$8,$6
addu	$2,$3,$23
sll	$15,$12,2
lbu	$3,0($3)
sll	$12,$12,4
lbu	$16,0($2)
addu	$2,$2,$23
sll	$10,$10,3
addu	$15,$15,$12
lbu	$18,0($2)
addu	$2,$2,$23
sw	$16,8($sp)
subu	$10,$14,$10
sll	$16,$13,1
lbu	$19,0($2)
addu	$2,$2,$23
addu	$12,$9,$6
sw	$18,12($sp)
addu	$14,$15,$10
addu	$18,$8,$7
lbu	$21,0($2)
addu	$16,$16,$13
sw	$19,16($sp)
addu	$13,$9,$5
sll	$15,$12,1
sll	$10,$18,2
sw	$21,20($sp)
sll	$17,$18,4
addu	$16,$14,$16
addu	$2,$2,$23
addu	$14,$7,$5
sll	$12,$12,3
addu	$18,$10,$17
subu	$12,$15,$12
addu	$17,$8,$5
addu	$15,$7,$6
sll	$10,$13,1
subu	$16,$16,$14
lbu	$14,0($2)
addu	$2,$2,$23
addu	$21,$9,$4
addu	$10,$10,$13
sll	$19,$15,2
sll	$20,$15,4
sll	$22,$17,1
sll	$13,$17,3
addiu	$16,$16,15
addu	$24,$18,$12
lbu	$12,0($2)
addu	$15,$19,$20
subu	$17,$22,$13
addu	$2,$2,$23
sll	$19,$21,1
sra	$16,$16,5
addu	$18,$15,$17
lbu	$13,0($2)
addu	$16,$25,$16
addu	$10,$24,$10
addu	$19,$19,$21
addu	$24,$8,$4
lbu	$fp,0($16)
addu	$22,$7,$4
addu	$2,$2,$23
addu	$19,$18,$19
addu	$15,$9,$3
subu	$10,$10,$24
lbu	$17,0($2)
addu	$25,$8,$3
sll	$21,$22,1
sll	$16,$22,3
addu	$2,$2,$23
subu	$15,$19,$15
addiu	$10,$10,15
lui	$19,%hi(ff_cropTbl+1024)
lbu	$18,0($2)
addu	$20,$6,$5
subu	$22,$21,$16
sll	$21,$25,1
addiu	$19,$19,%lo(ff_cropTbl+1024)
sra	$10,$10,5
addu	$21,$21,$25
lw	$25,80($sp)
sll	$24,$20,2
addu	$2,$2,$23
sll	$20,$20,4
addu	$10,$19,$10
lw	$19,8($sp)
addu	$24,$24,$20
lbu	$2,0($2)
sb	$fp,0($25)
addu	$20,$5,$4
addu	$16,$6,$3
lbu	$10,0($10)
addu	$22,$24,$22
lw	$25,24($sp)
addu	$fp,$7,$19
sll	$24,$16,1
sll	$19,$20,2
sb	$10,0($25)
addu	$22,$22,$21
lw	$10,8($sp)
addiu	$15,$15,15
sll	$20,$20,4
sll	$16,$16,3
lui	$21,%hi(ff_cropTbl+1024)
subu	$16,$24,$16
addu	$20,$19,$20
addiu	$21,$21,%lo(ff_cropTbl+1024)
sra	$15,$15,5
addu	$20,$20,$16
lw	$16,12($sp)
addu	$15,$21,$15
addu	$9,$9,$10
sll	$19,$fp,1
subu	$22,$22,$9
addu	$9,$6,$16
lbu	$16,0($15)
addu	$fp,$19,$fp
lw	$19,28($sp)
addu	$25,$4,$3
addu	$10,$5,$10
sb	$16,0($19)
addu	$20,$20,$fp
lw	$16,12($sp)
sll	$21,$25,2
sll	$15,$25,4
addiu	$22,$22,15
addu	$8,$8,$16
sll	$25,$10,1
subu	$20,$20,$8
sll	$10,$10,3
lui	$8,%hi(ff_cropTbl+1024)
addu	$15,$21,$15
lw	$21,8($sp)
subu	$10,$25,$10
lw	$25,12($sp)
addiu	$8,$8,%lo(ff_cropTbl+1024)
sra	$22,$22,5
sll	$16,$9,1
addu	$22,$8,$22
addu	$10,$15,$10
lw	$15,16($sp)
addu	$19,$3,$21
lw	$21,16($sp)
addu	$9,$16,$9
lw	$16,32($sp)
addu	$24,$4,$25
addu	$8,$5,$15
lbu	$15,0($22)
addu	$7,$7,$21
addu	$10,$10,$9
lw	$9,8($sp)
sll	$fp,$19,2
sll	$22,$19,4
sb	$15,0($16)
sll	$19,$24,1
sll	$24,$24,3
addu	$21,$3,$21
subu	$10,$10,$7
lw	$7,20($sp)
addu	$22,$fp,$22
addu	$25,$9,$25
subu	$24,$19,$24
sll	$16,$8,1
sll	$9,$21,1
addiu	$20,$20,15
lui	$19,%hi(ff_cropTbl+1024)
sll	$21,$21,3
addu	$15,$4,$7
addu	$8,$16,$8
lw	$16,16($sp)
sll	$7,$25,2
addu	$24,$22,$24
sll	$25,$25,4
subu	$21,$9,$21
lw	$9,12($sp)
addiu	$19,$19,%lo(ff_cropTbl+1024)
sra	$20,$20,5
addu	$24,$24,$8
lw	$8,20($sp)
addu	$25,$7,$25
lw	$7,8($sp)
addu	$20,$19,$20
addiu	$10,$10,15
addu	$19,$9,$16
lui	$9,%hi(ff_cropTbl+1024)
lbu	$20,0($20)
addu	$16,$7,$8
addu	$6,$6,$8
lw	$8,32($sp)
addiu	$9,$9,%lo(ff_cropTbl+1024)
sra	$10,$10,5
sll	$fp,$15,1
addu	$22,$8,$11
addu	$10,$9,$10
addu	$15,$fp,$15
sb	$20,0($22)
addu	$25,$25,$21
lw	$21,12($sp)
lbu	$8,0($10)
addu	$5,$5,$14
sll	$10,$16,1
subu	$24,$24,$6
addu	$25,$25,$15
lw	$15,20($sp)
sll	$16,$16,3
addu	$20,$3,$14
addu	$fp,$21,$14
subu	$16,$10,$16
lw	$10,16($sp)
subu	$25,$25,$5
addu	$22,$22,$11
addiu	$24,$24,15
lui	$5,%hi(ff_cropTbl+1024)
sb	$8,0($22)
sll	$7,$19,2
sll	$6,$20,1
sll	$19,$19,4
sll	$8,$fp,1
addiu	$5,$5,%lo(ff_cropTbl+1024)
sra	$24,$24,5
sll	$fp,$fp,3
addu	$9,$10,$15
addu	$20,$6,$20
lw	$6,8($sp)
addu	$19,$7,$19
subu	$fp,$8,$fp
lw	$8,20($sp)
addu	$24,$5,$24
sll	$15,$9,2
addu	$19,$19,$16
addiu	$25,$25,15
lbu	$24,0($24)
sll	$9,$9,4
lui	$16,%hi(ff_cropTbl+1024)
addu	$21,$6,$12
addu	$7,$8,$14
addu	$19,$19,$20
addu	$4,$4,$12
addu	$9,$15,$9
lw	$15,16($sp)
addiu	$16,$16,%lo(ff_cropTbl+1024)
sra	$25,$25,5
sll	$10,$21,1
subu	$19,$19,$4
lw	$4,12($sp)
addu	$22,$22,$11
addu	$25,$16,$25
sll	$16,$7,2
sb	$24,0($22)
sll	$7,$7,4
addu	$5,$15,$12
lbu	$25,0($25)
addu	$21,$10,$21
addu	$9,$9,$fp
addu	$7,$16,$7
addiu	$19,$19,15
lui	$16,%hi(ff_cropTbl+1024)
sll	$24,$5,1
addu	$3,$3,$13
addu	$9,$9,$21
lw	$21,16($sp)
sll	$5,$5,3
addiu	$16,$16,%lo(ff_cropTbl+1024)
sra	$19,$19,5
subu	$5,$24,$5
addu	$20,$14,$12
move	$10,$8
subu	$9,$9,$3
lw	$3,8($sp)
addu	$8,$8,$13
addu	$22,$22,$11
addu	$6,$4,$13
addu	$19,$16,$19
sb	$25,0($22)
sll	$fp,$8,1
sll	$25,$20,2
addu	$7,$7,$5
lbu	$19,0($19)
addiu	$9,$9,15
sll	$20,$20,4
sll	$8,$8,3
lui	$5,%hi(ff_cropTbl+1024)
addu	$4,$21,$17
sll	$15,$6,1
subu	$8,$fp,$8
addu	$20,$25,$20
addiu	$5,$5,%lo(ff_cropTbl+1024)
sra	$9,$9,5
addu	$6,$15,$6
addu	$21,$3,$17
sll	$3,$4,1
addu	$9,$5,$9
addu	$22,$22,$11
addu	$20,$20,$8
lw	$8,12($sp)
addu	$4,$3,$4
sb	$19,0($22)
addu	$7,$7,$6
lbu	$25,0($9)
addu	$24,$12,$13
addu	$16,$14,$17
addu	$20,$20,$4
addu	$9,$8,$18
subu	$7,$7,$21
addu	$19,$10,$18
sll	$5,$24,2
sll	$6,$16,1
sll	$24,$24,4
sll	$16,$16,3
subu	$10,$20,$9
addiu	$7,$7,15
lui	$9,%hi(ff_cropTbl+1024)
subu	$16,$6,$16
addu	$24,$5,$24
sll	$8,$19,1
addiu	$9,$9,%lo(ff_cropTbl+1024)
sra	$7,$7,5
addu	$19,$8,$19
addu	$24,$24,$16
lw	$16,16($sp)
addu	$22,$22,$11
addu	$7,$9,$7
addu	$4,$13,$17
sb	$25,0($22)
addu	$fp,$12,$18
addu	$24,$24,$19
lbu	$7,0($7)
addiu	$10,$10,15
lui	$19,%hi(ff_cropTbl+1024)
addu	$15,$16,$2
addu	$3,$14,$2
sll	$20,$4,2
sll	$21,$fp,1
sll	$9,$fp,3
addiu	$19,$19,%lo(ff_cropTbl+1024)
sll	$4,$4,4
sra	$10,$10,5
subu	$15,$24,$15
addu	$4,$20,$4
subu	$fp,$21,$9
lw	$21,20($sp)
addu	$5,$22,$11
sll	$8,$3,1
addu	$10,$19,$10
sb	$7,0($5)
addu	$9,$13,$2
addu	$6,$17,$18
addiu	$15,$15,15
lbu	$10,0($10)
lui	$25,%hi(ff_cropTbl+1024)
addu	$fp,$4,$fp
addu	$8,$8,$3
addu	$16,$12,$2
addiu	$25,$25,%lo(ff_cropTbl+1024)
sll	$4,$6,2
sll	$3,$9,3
sll	$6,$6,4
sra	$15,$15,5
sll	$20,$9,1
addu	$19,$21,$2
addu	$8,$fp,$8
addu	$4,$4,$6
subu	$9,$20,$3
addu	$6,$5,$11
sll	$7,$16,1
addu	$5,$25,$15
sb	$10,0($6)
addu	$3,$18,$2
subu	$8,$8,$19
addu	$2,$17,$2
addu	$9,$4,$9
addu	$7,$7,$16
lbu	$16,0($5)
addu	$15,$13,$18
sll	$5,$3,2
sll	$13,$2,1
sll	$10,$2,3
sll	$3,$3,4
addiu	$8,$8,15
addu	$7,$9,$7
lw	$9,84($sp)
addu	$14,$14,$18
addu	$4,$5,$3
sra	$8,$8,5
subu	$3,$13,$10
lw	$10,36($sp)
sll	$13,$15,1
subu	$14,$7,$14
addu	$5,$6,$11
addu	$3,$4,$3
addu	$8,$25,$8
sb	$16,0($5)
addu	$13,$13,$15
addiu	$4,$14,15
addu	$13,$3,$13
lbu	$6,0($8)
addu	$2,$12,$17
lw	$8,80($sp)
sra	$4,$4,5
subu	$2,$13,$2
addu	$3,$5,$11
addu	$4,$25,$4
addiu	$2,$2,15
sb	$6,0($3)
addu	$3,$3,$11
lbu	$5,0($4)
sra	$2,$2,5
addiu	$8,$8,1
addu	$2,$25,$2
addiu	$9,$9,1
sb	$5,0($3)
addu	$4,$3,$11
sw	$8,80($sp)
sw	$9,84($sp)
lbu	$2,0($2)
bne	$8,$10,$L634
sb	$2,0($4)

lw	$fp,76($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,80

.set	macro
.set	reorder
.end	put_no_rnd_mpeg4_qpel16_v_lowpass
.size	put_no_rnd_mpeg4_qpel16_v_lowpass, .-put_no_rnd_mpeg4_qpel16_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc20_c
.type	put_no_rnd_qpel8_mc20_c, @function
put_no_rnd_qpel8_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,8			# 0x8
addiu	$sp,$sp,-40
sw	$2,16($sp)
sw	$31,36($sp)
.option	pic0
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$7,$6

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_no_rnd_qpel8_mc20_c
.size	put_no_rnd_qpel8_mc20_c, .-put_no_rnd_qpel8_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc22_c
.type	put_no_rnd_qpel8_mc22_c, @function
put_no_rnd_qpel8_mc22_c:
.frame	$sp,120,$31		# vars= 72, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-120
li	$2,9			# 0x9
sw	$16,104($sp)
addiu	$16,$sp,32
sw	$17,108($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$18,112($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,116($sp)
.option	pic0
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16

li	$7,8			# 0x8
move	$4,$18
move	$5,$16
.option	pic0
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$17

lw	$31,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
j	$31
addiu	$sp,$sp,120

.set	macro
.set	reorder
.end	put_no_rnd_qpel8_mc22_c
.size	put_no_rnd_qpel8_mc22_c, .-put_no_rnd_qpel8_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc20_c
.type	put_no_rnd_qpel16_mc20_c, @function
put_no_rnd_qpel16_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,16			# 0x10
addiu	$sp,$sp,-40
sw	$2,16($sp)
sw	$31,36($sp)
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$7,$6

lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_no_rnd_qpel16_mc20_c
.size	put_no_rnd_qpel16_mc20_c, .-put_no_rnd_qpel16_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc22_c
.type	put_no_rnd_qpel16_mc22_c, @function
put_no_rnd_qpel16_mc22_c:
.frame	$sp,320,$31		# vars= 272, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
li	$2,17			# 0x11
sw	$16,304($sp)
addiu	$16,$sp,32
sw	$17,308($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$18,312($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,316($sp)
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17

lw	$31,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	put_no_rnd_qpel16_mc22_c
.size	put_no_rnd_qpel16_mc22_c, .-put_no_rnd_qpel16_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_mpeg4_qpel8_v_lowpass
.type	avg_mpeg4_qpel8_v_lowpass, @function
avg_mpeg4_qpel8_v_lowpass:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
addiu	$2,$5,8
sw	$21,48($sp)
addu	$21,$4,$6
sw	$20,44($sp)
addu	$20,$21,$6
sw	$22,52($sp)
sw	$19,40($sp)
addu	$22,$20,$6
sw	$18,36($sp)
sw	$17,32($sp)
lui	$17,%hi(ff_cropTbl+1024)
addu	$19,$22,$6
sw	$fp,60($sp)
addiu	$17,$17,%lo(ff_cropTbl+1024)
sw	$23,56($sp)
addu	$18,$19,$6
sw	$16,28($sp)
sw	$2,16($sp)
addu	$3,$18,$6
addu	$6,$3,$6
sw	$3,8($sp)
sw	$6,12($sp)
$L646:
addu	$3,$5,$7
lbu	$13,0($5)
lbu	$16,0($4)
addiu	$21,$21,1
addu	$2,$3,$7
lbu	$12,0($3)
addiu	$4,$4,1
addu	$6,$2,$7
lbu	$11,0($2)
addiu	$5,$5,1
addu	$2,$13,$12
lbu	$10,0($6)
addu	$6,$6,$7
addu	$15,$13,$11
sll	$14,$2,2
addu	$23,$12,$10
lbu	$8,0($6)
sll	$24,$15,1
sll	$2,$2,4
sll	$15,$15,3
addu	$9,$14,$2
subu	$15,$24,$15
sll	$14,$23,1
addu	$2,$9,$15
addu	$23,$14,$23
addu	$9,$11,$8
addu	$3,$2,$23
addu	$14,$12,$11
subu	$3,$3,$9
addu	$9,$13,$10
addiu	$3,$3,16
addu	$23,$13,$8
sll	$2,$14,2
sll	$24,$9,1
addu	$6,$6,$7
sll	$14,$14,4
sll	$9,$9,3
sra	$3,$3,5
lbu	$15,0($6)
subu	$9,$24,$9
addu	$14,$2,$14
sll	$25,$23,1
addu	$3,$17,$3
addu	$14,$14,$9
addu	$23,$25,$23
addu	$2,$12,$15
lbu	$25,0($3)
addu	$23,$14,$23
addu	$6,$6,$7
subu	$2,$23,$2
addu	$16,$16,$25
lbu	$14,0($6)
addiu	$2,$2,16
addu	$6,$6,$7
addu	$3,$11,$10
addu	$23,$12,$8
addiu	$16,$16,1
lbu	$9,0($6)
sra	$2,$2,5
addu	$25,$13,$15
sll	$24,$3,2
sll	$fp,$23,1
sra	$16,$16,1
sll	$3,$3,4
sll	$23,$23,3
addu	$6,$6,$7
addu	$2,$17,$2
addu	$3,$24,$3
subu	$23,$fp,$23
lbu	$6,0($6)
sll	$24,$25,1
sb	$16,-1($4)
lbu	$fp,0($2)
addu	$23,$3,$23
lbu	$16,-1($21)
addu	$24,$24,$25
addu	$2,$13,$14
addu	$3,$23,$24
addu	$16,$16,$fp
subu	$3,$3,$2
addu	$23,$10,$8
addu	$2,$11,$15
addiu	$16,$16,1
addiu	$3,$3,16
addu	$24,$12,$14
sll	$fp,$23,2
sll	$25,$2,1
sra	$16,$16,1
sll	$23,$23,4
sll	$2,$2,3
sra	$3,$3,5
sb	$16,-1($21)
subu	$2,$25,$2
addu	$23,$fp,$23
sll	$25,$24,1
addu	$16,$17,$3
lbu	$3,0($20)
addu	$24,$25,$24
addu	$23,$23,$2
lbu	$fp,0($16)
addu	$13,$13,$9
addu	$23,$23,$24
addu	$16,$8,$15
subu	$13,$23,$13
addu	$2,$10,$14
addu	$3,$3,$fp
addiu	$13,$13,16
addu	$23,$11,$9
sll	$fp,$16,2
sll	$25,$2,1
addiu	$3,$3,1
sll	$16,$16,4
sll	$2,$2,3
sra	$13,$13,5
subu	$2,$25,$2
sra	$3,$3,1
addu	$16,$fp,$16
sll	$24,$23,1
addu	$13,$17,$13
sb	$3,0($20)
addu	$23,$24,$23
lbu	$3,0($22)
addu	$16,$16,$2
lbu	$25,0($13)
addu	$12,$12,$6
addu	$16,$16,$23
addu	$2,$8,$9
subu	$12,$16,$12
addu	$13,$15,$14
addu	$3,$3,$25
addiu	$12,$12,16
addu	$16,$10,$6
sll	$25,$13,2
sll	$24,$2,1
addiu	$3,$3,1
sll	$13,$13,4
sll	$2,$2,3
sra	$12,$12,5
subu	$2,$24,$2
sra	$3,$3,1
addu	$13,$25,$13
sll	$23,$16,1
addu	$12,$17,$12
sb	$3,0($22)
addu	$16,$23,$16
lbu	$fp,0($19)
addu	$13,$13,$2
lbu	$24,0($12)
addu	$11,$11,$6
addu	$13,$13,$16
addu	$3,$15,$6
subu	$11,$13,$11
addu	$12,$14,$9
addu	$fp,$fp,$24
addiu	$11,$11,16
addu	$24,$8,$6
sll	$25,$12,2
sll	$13,$3,1
sll	$12,$12,4
addiu	$fp,$fp,1
sll	$16,$3,3
sra	$11,$11,5
addu	$25,$25,$12
subu	$3,$13,$16
sra	$fp,$fp,1
sll	$23,$24,1
addu	$11,$17,$11
addu	$3,$25,$3
sb	$fp,0($19)
addu	$2,$23,$24
lbu	$12,0($18)
lbu	$11,0($11)
addu	$10,$10,$9
addu	$2,$3,$2
addu	$13,$14,$6
subu	$10,$2,$10
addu	$12,$12,$11
addiu	$10,$10,16
addu	$3,$9,$6
addiu	$12,$12,1
sll	$6,$13,1
sll	$13,$13,3
sra	$10,$10,5
sll	$11,$3,2
subu	$13,$6,$13
lw	$6,8($sp)
sll	$3,$3,4
sra	$12,$12,1
addu	$10,$17,$10
addu	$3,$11,$3
addu	$15,$15,$9
sb	$12,0($18)
lbu	$11,0($6)
addu	$6,$3,$13
sll	$9,$15,1
lbu	$3,0($10)
addu	$8,$8,$14
addu	$9,$9,$15
addu	$3,$11,$3
addu	$9,$6,$9
lw	$6,8($sp)
addiu	$3,$3,1
subu	$2,$9,$8
sra	$3,$3,1
addiu	$2,$2,16
addiu	$20,$20,1
sb	$3,0($6)
sra	$2,$2,5
lw	$6,12($sp)
addiu	$22,$22,1
addu	$2,$17,$2
addiu	$19,$19,1
addiu	$18,$18,1
lbu	$3,0($6)
lw	$6,8($sp)
lbu	$2,0($2)
addiu	$6,$6,1
addu	$2,$3,$2
sw	$6,8($sp)
addiu	$2,$2,1
lw	$6,12($sp)
sra	$2,$2,1
addiu	$6,$6,1
sb	$2,-1($6)
lw	$2,16($sp)
bne	$5,$2,$L646
sw	$6,12($sp)

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_mpeg4_qpel8_v_lowpass
.size	avg_mpeg4_qpel8_v_lowpass, .-avg_mpeg4_qpel8_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_mpeg4_qpel16_v_lowpass
.type	avg_mpeg4_qpel16_v_lowpass, @function
avg_mpeg4_qpel16_v_lowpass:
.frame	$sp,112,$31		# vars= 64, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addu	$2,$4,$6
addiu	$sp,$sp,-112
addu	$3,$2,$6
sw	$4,112($sp)
addiu	$8,$5,16
addu	$4,$3,$6
sw	$16,76($sp)
sw	$17,80($sp)
addu	$13,$4,$6
sw	$18,84($sp)
sw	$19,88($sp)
addu	$14,$13,$6
sw	$20,92($sp)
sw	$22,100($sp)
addu	$15,$14,$6
sw	$23,104($sp)
sw	$fp,108($sp)
addu	$16,$15,$6
sw	$21,96($sp)
sw	$2,52($sp)
addu	$17,$16,$6
sw	$3,8($sp)
sw	$5,116($sp)
addu	$18,$17,$6
sw	$4,12($sp)
sw	$8,68($sp)
addu	$19,$18,$6
sw	$13,16($sp)
sw	$14,20($sp)
addu	$20,$19,$6
sw	$15,24($sp)
sw	$16,28($sp)
addu	$22,$20,$6
sw	$17,32($sp)
sw	$18,36($sp)
addu	$23,$22,$6
sw	$19,40($sp)
sw	$20,44($sp)
addu	$24,$23,$6
sw	$22,48($sp)
sw	$23,60($sp)
addu	$6,$24,$6
sw	$24,56($sp)
sw	$6,64($sp)
$L650:
lw	$fp,116($sp)
lw	$2,112($sp)
lw	$4,52($sp)
addu	$3,$fp,$7
lbu	$19,0($fp)
addiu	$fp,$fp,1
lbu	$22,0($2)
addu	$2,$3,$7
lbu	$18,0($3)
addiu	$4,$4,1
lw	$3,112($sp)
lbu	$17,0($2)
addu	$8,$19,$18
sw	$4,52($sp)
addiu	$3,$3,1
sw	$fp,116($sp)
addu	$5,$19,$17
sw	$3,112($sp)
addu	$3,$2,$7
sll	$9,$5,1
sll	$2,$8,2
lbu	$16,0($3)
addu	$3,$3,$7
sll	$8,$8,4
sll	$4,$5,3
lbu	$15,0($3)
addu	$3,$3,$7
addu	$6,$18,$16
addu	$2,$2,$8
lbu	$14,0($3)
subu	$5,$9,$4
addu	$3,$3,$7
sll	$4,$6,1
addu	$5,$2,$5
addu	$2,$4,$6
lbu	$13,0($3)
addu	$4,$3,$7
addu	$2,$5,$2
addu	$5,$4,$7
addu	$3,$17,$15
lbu	$12,0($4)
addu	$8,$18,$17
lbu	$11,0($5)
subu	$2,$2,$3
addu	$5,$5,$7
addu	$3,$19,$16
addiu	$2,$2,16
sll	$20,$3,1
lbu	$10,0($5)
sll	$9,$3,3
addu	$5,$5,$7
subu	$3,$20,$9
lui	$20,%hi(ff_cropTbl+1024)
addu	$6,$19,$15
lbu	$9,0($5)
sll	$4,$8,2
sll	$8,$8,4
addu	$5,$5,$7
addiu	$20,$20,%lo(ff_cropTbl+1024)
sra	$2,$2,5
addu	$8,$4,$8
sll	$4,$6,1
addu	$2,$20,$2
addu	$3,$8,$3
lbu	$8,0($5)
addu	$4,$4,$6
addu	$5,$5,$7
lbu	$6,0($2)
addu	$4,$3,$4
addu	$2,$5,$7
addu	$3,$18,$14
lbu	$5,0($5)
addu	$23,$17,$16
subu	$4,$4,$3
lbu	$21,0($2)
addu	$3,$2,$7
addu	$22,$22,$6
sll	$fp,$23,2
lbu	$20,0($3)
addu	$3,$3,$7
addiu	$22,$22,1
sll	$23,$23,4
lbu	$6,0($3)
sra	$22,$22,1
addu	$23,$fp,$23
lw	$fp,112($sp)
addu	$3,$3,$7
addiu	$4,$4,16
addu	$2,$18,$15
lbu	$3,0($3)
sra	$4,$4,5
sb	$22,-1($fp)
lui	$22,%hi(ff_cropTbl+1024)
lw	$fp,52($sp)
sll	$25,$2,1
addiu	$22,$22,%lo(ff_cropTbl+1024)
sll	$2,$2,3
addu	$4,$22,$4
addu	$24,$19,$14
lbu	$22,-1($fp)
subu	$2,$25,$2
lbu	$fp,0($4)
sll	$25,$24,1
addu	$23,$23,$2
addu	$2,$17,$14
addu	$22,$22,$fp
addu	$24,$25,$24
sll	$fp,$2,1
addiu	$22,$22,1
sll	$2,$2,3
addu	$4,$19,$13
addu	$24,$23,$24
subu	$2,$fp,$2
lw	$fp,52($sp)
sra	$22,$22,1
subu	$4,$24,$4
addu	$23,$16,$15
sb	$22,-1($fp)
addiu	$4,$4,16
lui	$fp,%hi(ff_cropTbl+1024)
sra	$4,$4,5
addiu	$fp,$fp,%lo(ff_cropTbl+1024)
addu	$25,$18,$13
addu	$22,$fp,$4
lw	$fp,8($sp)
sll	$24,$23,2
sll	$23,$23,4
addu	$19,$19,$12
addu	$23,$24,$23
lbu	$4,0($fp)
sll	$24,$25,1
lbu	$fp,0($22)
addu	$23,$23,$2
addu	$25,$24,$25
addu	$22,$15,$14
addu	$2,$16,$13
addu	$23,$23,$25
addu	$4,$4,$fp
sll	$24,$2,1
sll	$fp,$22,2
subu	$19,$23,$19
addiu	$4,$4,1
sll	$22,$22,4
sll	$2,$2,3
sra	$4,$4,1
subu	$2,$24,$2
lw	$24,8($sp)
addu	$22,$fp,$22
addiu	$19,$19,16
lui	$fp,%hi(ff_cropTbl+1024)
sb	$4,0($24)
sra	$19,$19,5
lw	$24,12($sp)
addiu	$fp,$fp,%lo(ff_cropTbl+1024)
addu	$23,$17,$12
addu	$19,$fp,$19
lw	$fp,12($sp)
lbu	$4,0($24)
sll	$25,$23,1
addu	$22,$22,$2
lbu	$24,0($19)
addu	$23,$25,$23
addu	$18,$18,$11
addu	$22,$22,$23
addu	$4,$4,$24
subu	$18,$22,$18
addiu	$4,$4,1
addu	$2,$15,$12
sra	$4,$4,1
addiu	$18,$18,16
sll	$24,$2,1
sb	$4,0($fp)
lui	$4,%hi(ff_cropTbl+1024)
sll	$2,$2,3
addiu	$4,$4,%lo(ff_cropTbl+1024)
sra	$18,$18,5
subu	$2,$24,$2
lw	$24,16($sp)
addu	$18,$4,$18
addu	$19,$14,$13
addu	$22,$16,$11
lbu	$fp,0($18)
sll	$25,$19,2
lbu	$4,0($24)
sll	$19,$19,4
sll	$23,$22,1
addu	$19,$25,$19
addu	$4,$4,$fp
lw	$fp,16($sp)
addu	$22,$23,$22
addu	$19,$19,$2
addiu	$4,$4,1
addu	$19,$19,$22
addu	$17,$17,$10
sra	$4,$4,1
subu	$17,$19,$17
addu	$18,$13,$12
addu	$2,$14,$11
sb	$4,0($fp)
addiu	$17,$17,16
lui	$4,%hi(ff_cropTbl+1024)
addu	$19,$15,$10
sll	$24,$18,2
sll	$23,$2,1
sll	$18,$18,4
sll	$2,$2,3
addiu	$4,$4,%lo(ff_cropTbl+1024)
sra	$17,$17,5
subu	$2,$23,$2
lw	$23,20($sp)
addu	$18,$24,$18
lw	$24,20($sp)
sll	$22,$19,1
addu	$17,$4,$17
addu	$19,$22,$19
lbu	$4,0($23)
addu	$18,$18,$2
lbu	$25,0($17)
addu	$16,$16,$9
addu	$18,$18,$19
addu	$17,$12,$11
subu	$16,$18,$16
addu	$2,$13,$10
addu	$4,$4,$25
addiu	$16,$16,16
lui	$fp,%hi(ff_cropTbl+1024)
addu	$18,$14,$9
sll	$23,$17,2
sll	$22,$2,1
addiu	$4,$4,1
sll	$17,$17,4
sll	$2,$2,3
addiu	$fp,$fp,%lo(ff_cropTbl+1024)
sra	$16,$16,5
subu	$2,$22,$2
lw	$22,24($sp)
sra	$4,$4,1
addu	$17,$23,$17
lw	$23,24($sp)
sll	$19,$18,1
addu	$16,$fp,$16
sb	$4,0($24)
addu	$18,$19,$18
lbu	$4,0($22)
addu	$17,$17,$2
lbu	$24,0($16)
addu	$15,$15,$8
addu	$17,$17,$18
addu	$16,$11,$10
addu	$2,$12,$9
subu	$15,$17,$15
addu	$4,$4,$24
lw	$24,28($sp)
addu	$17,$13,$8
sll	$22,$16,2
sll	$19,$2,1
addiu	$15,$15,16
sll	$16,$16,4
sll	$2,$2,3
addiu	$4,$4,1
subu	$2,$19,$2
addu	$16,$22,$16
sll	$18,$17,1
sra	$15,$15,5
sra	$4,$4,1
addu	$15,$fp,$15
addu	$17,$18,$17
addu	$16,$16,$2
sb	$4,0($23)
addu	$14,$14,$5
lbu	$23,0($15)
addu	$16,$16,$17
lbu	$4,0($24)
addu	$15,$10,$9
subu	$14,$16,$14
addu	$4,$4,$23
addiu	$14,$14,16
addiu	$4,$4,1
sra	$14,$14,5
sra	$4,$4,1
addu	$14,$fp,$14
lw	$fp,32($sp)
addu	$2,$11,$8
sb	$4,0($24)
addu	$16,$12,$5
lbu	$22,0($14)
sll	$19,$15,2
lbu	$4,0($fp)
sll	$18,$2,1
sll	$15,$15,4
sll	$2,$2,3
addu	$15,$19,$15
subu	$2,$18,$2
sll	$17,$16,1
addu	$4,$4,$22
addu	$16,$17,$16
addu	$15,$15,$2
addiu	$4,$4,1
addu	$15,$15,$16
addu	$13,$13,$21
sra	$4,$4,1
subu	$13,$15,$13
addu	$14,$9,$8
addu	$2,$10,$5
sb	$4,0($fp)
addiu	$13,$13,16
lui	$4,%hi(ff_cropTbl+1024)
addu	$15,$11,$21
sll	$18,$14,2
sll	$17,$2,1
sll	$14,$14,4
sll	$2,$2,3
addiu	$4,$4,%lo(ff_cropTbl+1024)
sra	$13,$13,5
subu	$2,$17,$2
lw	$17,36($sp)
addu	$14,$18,$14
lw	$22,40($sp)
sll	$16,$15,1
lw	$18,36($sp)
addu	$13,$4,$13
lw	$23,44($sp)
addu	$15,$16,$15
lbu	$4,0($17)
addu	$14,$14,$2
lw	$24,48($sp)
lbu	$19,0($13)
addu	$12,$12,$20
addu	$14,$14,$15
lw	$fp,60($sp)
addu	$2,$9,$21
subu	$12,$14,$12
addu	$13,$8,$5
addu	$4,$4,$19
addiu	$12,$12,16
lui	$19,%hi(ff_cropTbl+1024)
addu	$14,$10,$20
sll	$17,$13,2
sll	$16,$2,1
addiu	$4,$4,1
sll	$13,$13,4
sll	$2,$2,3
addiu	$19,$19,%lo(ff_cropTbl+1024)
sra	$12,$12,5
subu	$2,$16,$2
sra	$4,$4,1
addu	$13,$17,$13
sll	$15,$14,1
addu	$12,$19,$12
sb	$4,0($18)
addu	$14,$15,$14
lbu	$4,0($22)
addu	$13,$13,$2
lbu	$18,0($12)
addu	$11,$11,$6
addu	$13,$13,$14
addu	$2,$8,$20
subu	$11,$13,$11
addu	$12,$5,$21
addu	$4,$4,$18
addiu	$11,$11,16
addu	$13,$9,$6
sll	$16,$12,2
sll	$15,$2,1
addiu	$4,$4,1
sll	$12,$12,4
sll	$2,$2,3
sra	$11,$11,5
subu	$2,$15,$2
sra	$4,$4,1
addu	$12,$16,$12
sll	$14,$13,1
addu	$11,$19,$11
sb	$4,0($22)
addu	$13,$14,$13
lbu	$4,0($23)
addu	$12,$12,$2
lbu	$17,0($11)
addu	$10,$10,$3
addu	$12,$12,$13
addu	$11,$21,$20
subu	$10,$12,$10
addu	$2,$5,$6
addu	$4,$4,$17
lw	$17,64($sp)
addiu	$10,$10,16
addu	$12,$8,$3
sll	$15,$11,2
sll	$14,$2,1
addiu	$4,$4,1
sll	$11,$11,4
sll	$2,$2,3
sra	$10,$10,5
subu	$2,$14,$2
sra	$4,$4,1
addu	$11,$15,$11
sll	$13,$12,1
addu	$10,$19,$10
sb	$4,0($23)
addu	$12,$13,$12
lbu	$4,0($24)
addu	$11,$11,$2
lbu	$16,0($10)
addu	$9,$9,$3
addu	$11,$11,$12
addu	$2,$20,$6
subu	$9,$11,$9
addu	$10,$21,$3
addu	$4,$4,$16
lw	$16,16($sp)
addiu	$9,$9,16
addu	$11,$5,$3
sll	$14,$2,2
sll	$13,$10,1
addiu	$4,$4,1
sll	$2,$2,4
sll	$10,$10,3
sra	$9,$9,5
sra	$4,$4,1
addu	$2,$14,$2
subu	$10,$13,$10
sll	$12,$11,1
sb	$4,0($24)
addu	$9,$19,$9
lbu	$4,0($fp)
addu	$2,$2,$10
addu	$11,$12,$11
lbu	$9,0($9)
addu	$8,$8,$6
addu	$2,$2,$11
addu	$15,$6,$3
subu	$2,$2,$8
lw	$8,56($sp)
addu	$3,$20,$3
addu	$4,$4,$9
addiu	$2,$2,16
sll	$13,$15,2
sll	$14,$3,1
addu	$6,$21,$6
addiu	$4,$4,1
sll	$15,$15,4
sll	$3,$3,3
sra	$2,$2,5
subu	$3,$14,$3
lw	$14,12($sp)
sra	$4,$4,1
addu	$15,$13,$15
lw	$13,8($sp)
sll	$21,$6,1
addu	$2,$19,$2
sb	$4,0($fp)
addu	$15,$15,$3
lbu	$4,0($8)
addu	$6,$21,$6
lbu	$8,0($2)
addu	$2,$5,$20
addu	$15,$15,$6
addiu	$13,$13,1
subu	$2,$15,$2
lw	$15,56($sp)
addu	$4,$4,$8
addiu	$2,$2,16
sw	$13,8($sp)
addiu	$4,$4,1
sra	$2,$2,5
sra	$4,$4,1
addu	$2,$19,$2
addiu	$14,$14,1
addiu	$16,$16,1
sb	$4,0($15)
addiu	$17,$17,1
sw	$14,12($sp)
addiu	$15,$15,1
sw	$16,16($sp)
lbu	$3,-1($17)
lbu	$2,0($2)
lw	$18,20($sp)
lw	$19,24($sp)
addu	$2,$3,$2
lw	$20,28($sp)
lw	$22,32($sp)
addiu	$18,$18,1
addiu	$2,$2,1
lw	$23,36($sp)
lw	$24,40($sp)
addiu	$19,$19,1
lw	$fp,44($sp)
sra	$2,$2,1
lw	$3,48($sp)
addiu	$20,$20,1
lw	$4,60($sp)
addiu	$22,$22,1
lw	$8,116($sp)
addiu	$23,$23,1
lw	$13,68($sp)
addiu	$24,$24,1
sb	$2,-1($17)
addiu	$fp,$fp,1
addiu	$3,$3,1
sw	$18,20($sp)
addiu	$4,$4,1
sw	$19,24($sp)
sw	$20,28($sp)
sw	$22,32($sp)
sw	$23,36($sp)
sw	$24,40($sp)
sw	$fp,44($sp)
sw	$3,48($sp)
sw	$4,60($sp)
sw	$15,56($sp)
bne	$8,$13,$L650
sw	$17,64($sp)

lw	$fp,108($sp)
lw	$23,104($sp)
lw	$22,100($sp)
lw	$21,96($sp)
lw	$20,92($sp)
lw	$19,88($sp)
lw	$18,84($sp)
lw	$17,80($sp)
lw	$16,76($sp)
j	$31
addiu	$sp,$sp,112

.set	macro
.set	reorder
.end	avg_mpeg4_qpel16_v_lowpass
.size	avg_mpeg4_qpel16_v_lowpass, .-avg_mpeg4_qpel16_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc10_c
.type	avg_qpel8_mc10_c, @function
avg_qpel8_mc10_c:
.frame	$sp,128,$31		# vars= 64, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-128
sw	$18,112($sp)
li	$18,8			# 0x8
sw	$16,104($sp)
addiu	$16,$sp,40
sw	$17,108($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$20,120($sp)
sw	$19,116($sp)
move	$20,$4
sw	$18,16($sp)
move	$19,$5
move	$4,$16
sw	$31,124($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$7,$17

move	$4,$20
sw	$17,16($sp)
move	$5,$19
sw	$18,20($sp)
move	$6,$16
sw	$18,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,124($sp)
lw	$20,120($sp)
lw	$19,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
j	$31
addiu	$sp,$sp,128

.set	macro
.set	reorder
.end	avg_qpel8_mc10_c
.size	avg_qpel8_mc10_c, .-avg_qpel8_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc30_c
.type	avg_qpel8_mc30_c, @function
avg_qpel8_mc30_c:
.frame	$sp,128,$31		# vars= 64, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-128
sw	$18,112($sp)
li	$18,8			# 0x8
sw	$16,104($sp)
addiu	$16,$sp,40
sw	$17,108($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$20,120($sp)
sw	$19,116($sp)
move	$20,$5
sw	$18,16($sp)
move	$19,$4
move	$7,$17
sw	$31,124($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16

addiu	$5,$20,1
sw	$17,16($sp)
move	$4,$19
sw	$18,20($sp)
move	$6,$16
sw	$18,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,124($sp)
lw	$20,120($sp)
lw	$19,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
j	$31
addiu	$sp,$sp,128

.set	macro
.set	reorder
.end	avg_qpel8_mc30_c
.size	avg_qpel8_mc30_c, .-avg_qpel8_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc21_c
.type	avg_qpel8_mc21_c, @function
avg_qpel8_mc21_c:
.frame	$sp,200,$31		# vars= 136, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-200
li	$2,9			# 0x9
sw	$16,180($sp)
addiu	$16,$sp,40
sw	$18,188($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$19,192($sp)
sw	$17,184($sp)
move	$19,$4
sw	$2,16($sp)
addiu	$17,$sp,112
move	$7,$18
sw	$31,196($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16

li	$2,8			# 0x8
move	$4,$19
move	$5,$16
sw	$2,16($sp)
move	$6,$17
sw	$2,20($sp)
move	$7,$18
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)

lw	$31,196($sp)
lw	$19,192($sp)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
j	$31
addiu	$sp,$sp,200

.set	macro
.set	reorder
.end	avg_qpel8_mc21_c
.size	avg_qpel8_mc21_c, .-avg_qpel8_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc23_c
.type	avg_qpel8_mc23_c, @function
avg_qpel8_mc23_c:
.frame	$sp,200,$31		# vars= 136, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-200
li	$2,9			# 0x9
sw	$17,184($sp)
addiu	$17,$sp,40
sw	$18,188($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$19,192($sp)
sw	$16,180($sp)
move	$19,$4
sw	$2,16($sp)
addiu	$16,$sp,112
move	$7,$18
sw	$31,196($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$17

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$16
.option	pic0
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$17

li	$2,8			# 0x8
addiu	$5,$sp,48
move	$4,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)

lw	$31,196($sp)
lw	$19,192($sp)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
j	$31
addiu	$sp,$sp,200

.set	macro
.set	reorder
.end	avg_qpel8_mc23_c
.size	avg_qpel8_mc23_c, .-avg_qpel8_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc22_c
.type	avg_qpel8_mc22_c, @function
avg_qpel8_mc22_c:
.frame	$sp,120,$31		# vars= 72, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-120
li	$2,9			# 0x9
sw	$16,104($sp)
addiu	$16,$sp,32
sw	$17,108($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$18,112($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,116($sp)
.option	pic0
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16

li	$7,8			# 0x8
move	$4,$18
move	$5,$16
.option	pic0
jal	avg_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$17

lw	$31,116($sp)
lw	$18,112($sp)
lw	$17,108($sp)
lw	$16,104($sp)
j	$31
addiu	$sp,$sp,120

.set	macro
.set	reorder
.end	avg_qpel8_mc22_c
.size	avg_qpel8_mc22_c, .-avg_qpel8_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc22_c
.type	avg_qpel16_mc22_c, @function
avg_qpel16_mc22_c:
.frame	$sp,320,$31		# vars= 272, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
li	$2,17			# 0x11
sw	$16,304($sp)
addiu	$16,$sp,32
sw	$17,308($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$18,312($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,316($sp)
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
jal	avg_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17

lw	$31,316($sp)
lw	$18,312($sp)
lw	$17,308($sp)
lw	$16,304($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	avg_qpel16_mc22_c
.size	avg_qpel16_mc22_c, .-avg_qpel16_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_h_lowpass
.type	put_h264_qpel2_h_lowpass, @function
put_h264_qpel2_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,1($5)
lui	$9,%hi(ff_cropTbl+1024)
lbu	$8,-1($5)
addu	$7,$5,$7
lbu	$3,0($5)
addiu	$9,$9,%lo(ff_cropTbl+1024)
lbu	$10,2($5)
addu	$6,$4,$6
lbu	$11,-2($5)
addu	$3,$3,$2
lbu	$12,3($5)
addu	$10,$8,$10
sll	$8,$3,2
sll	$2,$10,2
sll	$3,$3,4
addu	$2,$2,$10
addu	$3,$8,$3
addu	$8,$11,$12
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$10,2($5)
lbu	$2,3($5)
lbu	$3,1($5)
lbu	$8,0($5)
lbu	$11,4($5)
addu	$3,$3,$10
lbu	$10,-1($5)
addu	$8,$8,$2
sll	$5,$3,2
sll	$2,$8,2
sll	$3,$3,4
addu	$2,$2,$8
addu	$3,$5,$3
addu	$5,$10,$11
subu	$2,$3,$2
addu	$2,$2,$5
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$2,1($7)
lbu	$4,-1($7)
lbu	$3,0($7)
lbu	$5,2($7)
lbu	$8,-2($7)
addu	$3,$3,$2
lbu	$10,3($7)
addu	$5,$4,$5
sll	$4,$3,2
sll	$2,$5,2
sll	$3,$3,4
addu	$2,$2,$5
addu	$3,$4,$3
addu	$4,$8,$10
subu	$2,$3,$2
addu	$2,$2,$4
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,0($6)
lbu	$2,2($7)
lbu	$4,0($7)
lbu	$3,1($7)
lbu	$5,3($7)
lbu	$8,-1($7)
addu	$3,$3,$2
lbu	$7,4($7)
addu	$5,$4,$5
sll	$4,$3,2
sll	$2,$5,2
sll	$3,$3,4
addu	$2,$2,$5
addu	$3,$4,$3
addu	$4,$8,$7
subu	$2,$3,$2
addu	$2,$2,$4
addiu	$2,$2,16
sra	$2,$2,5
addu	$9,$9,$2
lbu	$2,0($9)
j	$31
sb	$2,1($6)

.set	macro
.set	reorder
.end	put_h264_qpel2_h_lowpass
.size	put_h264_qpel2_h_lowpass, .-put_h264_qpel2_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_h_lowpass
.type	put_h264_qpel4_h_lowpass, @function
put_h264_qpel4_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$9,%hi(ff_cropTbl+1024)
li	$10,4			# 0x4
addiu	$9,$9,%lo(ff_cropTbl+1024)
$L667:
lbu	$2,1($5)
addiu	$10,$10,-1
lbu	$8,-1($5)
lbu	$3,0($5)
lbu	$11,2($5)
lbu	$12,-2($5)
addu	$3,$3,$2
lbu	$13,3($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$2,2($5)
lbu	$8,0($5)
lbu	$3,1($5)
lbu	$11,3($5)
lbu	$12,-1($5)
addu	$3,$3,$2
lbu	$13,4($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$2,3($5)
lbu	$8,1($5)
lbu	$3,2($5)
lbu	$11,4($5)
lbu	$12,0($5)
addu	$3,$3,$2
lbu	$13,5($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,2($4)
lbu	$2,4($5)
lbu	$8,2($5)
lbu	$3,3($5)
lbu	$11,5($5)
lbu	$12,1($5)
addu	$3,$3,$2
lbu	$13,6($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$5,$5,$7
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,3($4)
bne	$10,$0,$L667
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_h264_qpel4_h_lowpass
.size	put_h264_qpel4_h_lowpass, .-put_h264_qpel4_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_h_lowpass
.type	put_h264_qpel8_h_lowpass, @function
put_h264_qpel8_h_lowpass:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$9,%hi(ff_cropTbl+1024)
li	$10,8			# 0x8
addiu	$9,$9,%lo(ff_cropTbl+1024)
$L670:
lbu	$2,1($5)
addiu	$10,$10,-1
lbu	$8,-1($5)
lbu	$3,0($5)
lbu	$11,2($5)
lbu	$12,-2($5)
addu	$3,$3,$2
lbu	$13,3($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$2,2($5)
lbu	$8,0($5)
lbu	$3,1($5)
lbu	$11,3($5)
lbu	$12,-1($5)
addu	$3,$3,$2
lbu	$13,4($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$2,3($5)
lbu	$8,1($5)
lbu	$3,2($5)
lbu	$11,4($5)
lbu	$12,0($5)
addu	$3,$3,$2
lbu	$13,5($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,2($4)
lbu	$2,4($5)
lbu	$8,2($5)
lbu	$3,3($5)
lbu	$11,5($5)
lbu	$12,1($5)
addu	$3,$3,$2
lbu	$13,6($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,3($4)
lbu	$3,4($5)
lbu	$2,5($5)
lbu	$8,3($5)
lbu	$11,6($5)
addu	$3,$3,$2
lbu	$12,2($5)
lbu	$13,7($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,4($4)
lbu	$2,6($5)
lbu	$8,4($5)
lbu	$3,5($5)
lbu	$11,7($5)
lbu	$12,3($5)
addu	$3,$3,$2
lbu	$13,8($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,5($4)
lbu	$2,7($5)
lbu	$8,5($5)
lbu	$3,6($5)
lbu	$11,8($5)
lbu	$12,4($5)
addu	$3,$3,$2
lbu	$13,9($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,6($4)
lbu	$2,8($5)
lbu	$8,6($5)
lbu	$3,7($5)
lbu	$11,9($5)
lbu	$12,5($5)
addu	$3,$3,$2
lbu	$13,10($5)
addu	$11,$8,$11
sll	$8,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$8,$3
addu	$8,$12,$13
subu	$2,$3,$2
addu	$5,$5,$7
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
sb	$2,7($4)
bne	$10,$0,$L670
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_h264_qpel8_h_lowpass
.size	put_h264_qpel8_h_lowpass, .-put_h264_qpel8_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_v_lowpass
.type	put_h264_qpel8_v_lowpass, @function
put_h264_qpel8_v_lowpass:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
subu	$2,$0,$7
addiu	$sp,$sp,-56
sll	$2,$2,1
lui	$24,%hi(ff_cropTbl+1024)
sw	$fp,52($sp)
sw	$23,48($sp)
sw	$2,12($sp)
addiu	$2,$4,8
addiu	$24,$24,%lo(ff_cropTbl+1024)
sw	$22,44($sp)
sw	$21,40($sp)
sw	$20,36($sp)
sw	$19,32($sp)
sw	$18,28($sp)
sw	$17,24($sp)
sw	$16,20($sp)
sw	$2,8($sp)
$L673:
lw	$2,12($sp)
addu	$21,$4,$6
lbu	$9,0($5)
addiu	$4,$4,1
addu	$8,$5,$2
addu	$2,$5,$7
addu	$10,$8,$7
addu	$3,$2,$7
lbu	$22,0($8)
lbu	$8,0($2)
lbu	$16,0($10)
addu	$2,$3,$7
lbu	$25,0($3)
addu	$3,$21,$6
addu	$12,$9,$8
lbu	$15,0($2)
addu	$2,$2,$7
addu	$13,$16,$25
sll	$10,$12,2
sll	$11,$13,2
lbu	$14,0($2)
sll	$12,$12,4
addu	$17,$9,$15
addu	$12,$10,$12
addu	$11,$11,$13
addu	$2,$2,$7
subu	$11,$12,$11
addu	$22,$22,$15
sll	$12,$17,2
addu	$13,$8,$25
addu	$22,$11,$22
addu	$11,$12,$17
lbu	$12,0($2)
addu	$2,$2,$7
sll	$10,$13,2
sll	$13,$13,4
addu	$20,$25,$15
addu	$10,$10,$13
lbu	$13,0($2)
addu	$2,$2,$7
addu	$17,$8,$14
addiu	$22,$22,16
subu	$23,$10,$11
lbu	$11,0($2)
addu	$10,$16,$14
sll	$19,$20,2
sll	$16,$20,4
sll	$18,$17,2
addu	$2,$2,$7
sra	$22,$22,5
addu	$10,$23,$10
addu	$20,$19,$16
lbu	$19,0($2)
addu	$22,$24,$22
addu	$17,$18,$17
addu	$2,$2,$7
addiu	$10,$10,16
lbu	$23,0($22)
subu	$18,$20,$17
addu	$9,$9,$12
lbu	$20,0($2)
addu	$16,$15,$14
addu	$22,$25,$12
sra	$10,$10,5
addu	$9,$18,$9
addu	$2,$2,$7
sll	$fp,$16,2
addu	$10,$24,$10
sll	$16,$16,4
lbu	$2,0($2)
sll	$17,$22,2
sb	$23,-1($4)
addiu	$9,$9,16
lbu	$18,0($10)
addu	$17,$17,$22
addu	$16,$fp,$16
sra	$9,$9,5
addu	$8,$8,$13
sb	$18,0($21)
subu	$16,$16,$17
addu	$10,$15,$13
addu	$23,$14,$12
addu	$9,$24,$9
addu	$16,$16,$8
sll	$21,$23,2
sll	$17,$10,2
lbu	$18,0($9)
sll	$23,$23,4
addiu	$16,$16,16
addu	$23,$21,$23
addu	$10,$17,$10
sb	$18,0($3)
addu	$9,$14,$11
addu	$8,$12,$13
sra	$16,$16,5
subu	$10,$23,$10
addu	$25,$25,$11
sll	$18,$8,2
sll	$17,$9,2
addu	$16,$24,$16
sll	$8,$8,4
addu	$25,$10,$25
addu	$9,$17,$9
addu	$8,$18,$8
lbu	$18,0($16)
addu	$10,$13,$11
addu	$17,$12,$19
addiu	$16,$25,16
subu	$8,$8,$9
addu	$15,$15,$19
sll	$21,$10,2
sra	$16,$16,5
sll	$10,$10,4
sll	$9,$17,2
addu	$15,$8,$15
addu	$3,$3,$6
addu	$10,$21,$10
addu	$16,$24,$16
sb	$18,0($3)
addu	$9,$9,$17
addu	$11,$11,$19
addu	$13,$13,$20
lbu	$16,0($16)
addiu	$15,$15,16
subu	$9,$10,$9
addu	$14,$14,$20
sll	$8,$11,2
sra	$15,$15,5
sll	$11,$11,4
sll	$10,$13,2
addu	$14,$9,$14
addu	$3,$3,$6
addu	$11,$8,$11
addu	$15,$24,$15
sb	$16,0($3)
addu	$13,$10,$13
addiu	$8,$14,16
subu	$13,$11,$13
lbu	$9,0($15)
addu	$2,$12,$2
sra	$8,$8,5
addu	$2,$13,$2
addu	$3,$3,$6
addu	$8,$24,$8
addiu	$2,$2,16
sb	$9,0($3)
addu	$3,$3,$6
lbu	$9,0($8)
sra	$2,$2,5
addu	$8,$3,$6
addu	$2,$24,$2
sb	$9,0($3)
lbu	$2,0($2)
sb	$2,0($8)
lw	$2,8($sp)
bne	$4,$2,$L673
addiu	$5,$5,1

lw	$fp,52($sp)
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_h264_qpel8_v_lowpass
.size	put_h264_qpel8_v_lowpass, .-put_h264_qpel8_v_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_hv_lowpass
.type	put_h264_qpel8_hv_lowpass, @function
put_h264_qpel8_hv_lowpass:
.frame	$sp,96,$31		# vars= 48, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-96
li	$14,13			# 0xd
sw	$18,68($sp)
move	$15,$5
lw	$13,116($sp)
lw	$12,112($sp)
sw	$fp,92($sp)
sll	$2,$13,1
sw	$23,88($sp)
sll	$18,$12,1
sw	$22,84($sp)
subu	$6,$6,$2
sw	$21,80($sp)
sw	$20,76($sp)
sw	$19,72($sp)
sw	$17,64($sp)
sw	$16,60($sp)
$L677:
lbu	$3,0($6)
addiu	$14,$14,-1
lbu	$10,1($6)
lbu	$2,2($6)
lbu	$9,-1($6)
addu	$10,$3,$10
lbu	$8,3($6)
lbu	$3,-2($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,0($15)
lbu	$3,1($6)
lbu	$10,2($6)
lbu	$2,3($6)
lbu	$9,0($6)
addu	$10,$3,$10
lbu	$8,4($6)
lbu	$3,-1($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,2($15)
lbu	$3,2($6)
lbu	$10,3($6)
lbu	$2,4($6)
lbu	$9,1($6)
addu	$10,$3,$10
lbu	$8,5($6)
lbu	$3,0($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,4($15)
lbu	$3,3($6)
lbu	$10,4($6)
lbu	$2,5($6)
lbu	$9,2($6)
addu	$10,$3,$10
lbu	$8,6($6)
lbu	$3,1($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,6($15)
lbu	$3,5($6)
lbu	$10,4($6)
lbu	$2,2($6)
lbu	$8,7($6)
lbu	$9,3($6)
addu	$10,$10,$3
lbu	$11,6($6)
sll	$3,$10,2
sll	$10,$10,4
addu	$9,$9,$11
addu	$8,$8,$2
addu	$10,$3,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,8($15)
lbu	$3,5($6)
lbu	$10,6($6)
lbu	$2,7($6)
lbu	$9,4($6)
addu	$10,$3,$10
lbu	$8,8($6)
lbu	$3,3($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,10($15)
lbu	$3,6($6)
lbu	$10,7($6)
lbu	$2,8($6)
lbu	$9,5($6)
addu	$10,$3,$10
lbu	$8,9($6)
lbu	$3,4($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,12($15)
lbu	$3,7($6)
lbu	$10,8($6)
lbu	$2,9($6)
lbu	$9,6($6)
addu	$10,$3,$10
lbu	$8,10($6)
lbu	$3,5($6)
addu	$6,$6,$13
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,14($15)
bne	$14,$0,$L677
addu	$15,$15,$18

sll	$23,$12,4
sw	$18,48($sp)
sll	$fp,$12,2
sll	$3,$18,2
subu	$6,$23,$fp
sll	$2,$18,4
sll	$9,$12,3
subu	$2,$2,$3
sw	$6,12($sp)
subu	$6,$6,$12
addu	$3,$2,$18
sw	$9,8($sp)
sll	$2,$6,1
subu	$16,$0,$fp
subu	$2,$3,$2
subu	$12,$0,$12
subu	$21,$9,$18
sw	$16,20($sp)
addu	$5,$5,$2
sll	$12,$12,1
addu	$2,$18,$9
sw	$21,24($sp)
subu	$3,$23,$18
addu	$6,$18,$23
sw	$12,44($sp)
addu	$9,$fp,$23
sw	$2,28($sp)
addiu	$16,$4,8
sw	$3,32($sp)
lui	$17,%hi(ff_cropTbl+1024)
sw	$6,36($sp)
sw	$9,40($sp)
addiu	$17,$17,%lo(ff_cropTbl+1024)
sw	$16,16($sp)
$L678:
lw	$18,44($sp)
addu	$3,$5,$fp
lw	$21,48($sp)
addu	$25,$5,$23
lw	$9,24($sp)
addu	$2,$5,$18
lw	$16,20($sp)
addu	$6,$5,$21
lw	$18,8($sp)
lh	$3,0($3)
lh	$8,0($5)
addu	$12,$5,$16
lh	$14,0($2)
addu	$2,$5,$9
lh	$6,0($6)
addu	$11,$5,$18
lh	$15,0($12)
addu	$20,$14,$3
lh	$2,0($2)
addu	$9,$8,$6
lh	$12,0($11)
sll	$13,$20,2
lw	$21,28($sp)
sll	$11,$9,2
lw	$18,32($sp)
sll	$9,$9,4
lh	$25,0($25)
addu	$16,$8,$2
addu	$20,$13,$20
addu	$9,$11,$9
addu	$13,$6,$3
subu	$11,$9,$20
addu	$19,$15,$2
sll	$9,$13,2
sll	$15,$16,2
sll	$13,$13,4
addu	$19,$11,$19
addu	$9,$9,$13
addu	$11,$15,$16
addu	$16,$3,$2
addu	$13,$6,$12
subu	$20,$9,$11
addu	$10,$5,$21
lw	$21,36($sp)
sll	$15,$16,2
sll	$11,$16,4
lw	$16,12($sp)
addiu	$19,$19,512
lh	$10,0($10)
addu	$9,$14,$12
sll	$14,$13,2
sra	$19,$19,10
addu	$14,$14,$13
addu	$9,$20,$9
addu	$20,$5,$16
addu	$16,$15,$11
addu	$19,$17,$19
subu	$15,$16,$14
lh	$11,0($20)
addu	$16,$5,$18
lw	$18,40($sp)
addiu	$9,$9,512
lbu	$20,0($19)
addu	$8,$8,$10
addu	$13,$2,$12
lh	$16,0($16)
addu	$19,$3,$10
sra	$9,$9,10
addu	$24,$5,$21
addu	$8,$15,$8
addu	$21,$5,$18
sll	$22,$13,2
lh	$24,0($24)
sll	$14,$19,2
addu	$9,$17,$9
lh	$21,0($21)
sll	$13,$13,4
sb	$20,0($4)
addiu	$8,$8,512
addu	$14,$14,$19
lbu	$19,0($9)
addu	$13,$22,$13
addu	$9,$2,$11
addu	$20,$12,$10
sra	$8,$8,10
addu	$6,$6,$11
subu	$13,$13,$14
sll	$22,$20,2
addu	$14,$4,$7
sll	$15,$9,2
addu	$8,$17,$8
sb	$19,0($14)
sll	$20,$20,4
addu	$13,$13,$6
addu	$9,$15,$9
lbu	$15,0($8)
addu	$20,$22,$20
addu	$8,$12,$16
addu	$19,$10,$11
addiu	$13,$13,512
addu	$3,$3,$16
subu	$20,$20,$9
sll	$6,$19,2
sll	$22,$8,2
sra	$13,$13,10
sll	$19,$19,4
addu	$20,$20,$3
addu	$14,$14,$7
addu	$13,$17,$13
addu	$19,$6,$19
sb	$15,0($14)
addu	$8,$22,$8
addu	$15,$11,$16
lbu	$6,0($13)
addu	$9,$10,$25
addiu	$20,$20,512
addu	$2,$2,$25
subu	$19,$19,$8
sll	$22,$15,2
sra	$20,$20,10
sll	$15,$15,4
sll	$13,$9,2
addu	$19,$19,$2
addu	$14,$14,$7
addu	$20,$17,$20
addu	$15,$22,$15
sb	$6,0($14)
addu	$13,$13,$9
addu	$16,$16,$25
lbu	$3,0($20)
addu	$11,$11,$24
addiu	$19,$19,512
subu	$15,$15,$13
addu	$12,$12,$24
sll	$6,$16,2
sra	$19,$19,10
sll	$16,$16,4
sll	$20,$11,2
addu	$15,$15,$12
addu	$14,$14,$7
addu	$16,$6,$16
addu	$19,$17,$19
sb	$3,0($14)
addu	$11,$20,$11
addiu	$15,$15,512
addu	$2,$10,$21
lbu	$3,0($19)
subu	$16,$16,$11
lw	$21,16($sp)
sra	$15,$15,10
addu	$2,$16,$2
addu	$14,$14,$7
addu	$15,$17,$15
addiu	$2,$2,512
sb	$3,0($14)
addu	$14,$14,$7
lbu	$6,0($15)
sra	$2,$2,10
addu	$3,$14,$7
addu	$2,$17,$2
addiu	$4,$4,1
sb	$6,0($14)
addiu	$5,$5,2
lbu	$2,0($2)
bne	$4,$21,$L678
sb	$2,0($3)

lw	$fp,92($sp)
lw	$23,88($sp)
lw	$22,84($sp)
lw	$21,80($sp)
lw	$20,76($sp)
lw	$19,72($sp)
lw	$18,68($sp)
lw	$17,64($sp)
lw	$16,60($sp)
j	$31
addiu	$sp,$sp,96

.set	macro
.set	reorder
.end	put_h264_qpel8_hv_lowpass
.size	put_h264_qpel8_hv_lowpass, .-put_h264_qpel8_hv_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_h_lowpass
.type	avg_h264_qpel8_h_lowpass, @function
avg_h264_qpel8_h_lowpass:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$9,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-8
li	$10,8			# 0x8
addiu	$9,$9,%lo(ff_cropTbl+1024)
sw	$16,4($sp)
$L683:
lbu	$2,1($5)
addiu	$10,$10,-1
lbu	$8,-1($5)
lbu	$3,0($5)
lbu	$15,2($5)
lbu	$24,-2($5)
addu	$3,$3,$2
lbu	$16,3($5)
addu	$15,$8,$15
lbu	$13,0($4)
sll	$8,$3,2
lbu	$12,1($4)
sll	$2,$15,2
lbu	$11,2($4)
sll	$3,$3,4
lbu	$14,3($4)
addu	$2,$2,$15
addu	$3,$8,$3
addu	$8,$24,$16
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$13,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,0($4)
lbu	$2,2($5)
lbu	$8,0($5)
lbu	$3,1($5)
lbu	$13,3($5)
lbu	$15,-1($5)
addu	$3,$3,$2
lbu	$16,4($5)
addu	$13,$8,$13
sll	$8,$3,2
sll	$2,$13,2
sll	$3,$3,4
addu	$2,$2,$13
addu	$3,$8,$3
addu	$8,$15,$16
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$12,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,1($4)
lbu	$2,3($5)
lbu	$8,1($5)
lbu	$3,2($5)
lbu	$12,4($5)
lbu	$13,0($5)
addu	$3,$3,$2
lbu	$15,5($5)
addu	$12,$8,$12
sll	$8,$3,2
sll	$2,$12,2
sll	$3,$3,4
addu	$2,$2,$12
addu	$3,$8,$3
addu	$8,$13,$15
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$11,$11,$2
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,2($4)
lbu	$2,4($5)
lbu	$8,2($5)
lbu	$3,3($5)
lbu	$24,5($5)
lbu	$25,1($5)
addu	$3,$3,$2
lbu	$16,6($5)
addu	$24,$8,$24
lbu	$15,4($4)
sll	$8,$3,2
lbu	$13,5($4)
sll	$2,$24,2
lbu	$12,6($4)
sll	$3,$3,4
lbu	$11,7($4)
addu	$2,$2,$24
addu	$3,$8,$3
addu	$8,$25,$16
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$14,$14,$2
addiu	$14,$14,1
sra	$14,$14,1
sb	$14,3($4)
lbu	$2,5($5)
lbu	$8,3($5)
lbu	$3,4($5)
lbu	$14,6($5)
lbu	$24,2($5)
addu	$3,$3,$2
lbu	$25,7($5)
addu	$14,$8,$14
sll	$8,$3,2
sll	$2,$14,2
sll	$3,$3,4
addu	$2,$2,$14
addu	$3,$8,$3
addu	$8,$24,$25
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$15,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,4($4)
lbu	$2,6($5)
lbu	$8,4($5)
lbu	$3,5($5)
lbu	$14,7($5)
lbu	$15,3($5)
addu	$3,$3,$2
lbu	$24,8($5)
addu	$14,$8,$14
sll	$8,$3,2
sll	$2,$14,2
sll	$3,$3,4
addu	$2,$2,$14
addu	$3,$8,$3
addu	$8,$15,$24
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$13,$13,$2
addiu	$13,$13,1
sra	$13,$13,1
sb	$13,5($4)
lbu	$2,7($5)
lbu	$8,5($5)
lbu	$3,6($5)
lbu	$13,8($5)
lbu	$14,4($5)
addu	$3,$3,$2
lbu	$15,9($5)
addu	$13,$8,$13
sll	$8,$3,2
sll	$2,$13,2
sll	$3,$3,4
addu	$2,$2,$13
addu	$3,$8,$3
addu	$8,$14,$15
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$12,$12,$2
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,6($4)
lbu	$2,8($5)
lbu	$3,7($5)
lbu	$8,6($5)
lbu	$12,9($5)
addu	$3,$3,$2
lbu	$13,5($5)
lbu	$14,10($5)
addu	$5,$5,$7
addu	$12,$8,$12
sll	$8,$3,2
sll	$2,$12,2
sll	$3,$3,4
addu	$2,$2,$12
addu	$3,$8,$3
addu	$8,$13,$14
subu	$2,$3,$2
addu	$2,$2,$8
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$11,$11,$2
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,7($4)
bne	$10,$0,$L683
addu	$4,$4,$6

lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	avg_h264_qpel8_h_lowpass
.size	avg_h264_qpel8_h_lowpass, .-avg_h264_qpel8_h_lowpass
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc00_c
.type	put_h264_qpel2_mc00_c, @function
put_h264_qpel2_mc00_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$2,1($5)
addu	$3,$5,$6
lbu	$5,0($5)
addu	$6,$4,$6
sll	$2,$2,8
or	$2,$2,$5
sh	$2,0($4)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
j	$31
sh	$2,0($6)

.set	macro
.set	reorder
.end	put_h264_qpel2_mc00_c
.size	put_h264_qpel2_mc00_c, .-put_h264_qpel2_mc00_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc20_c
.type	put_h264_qpel2_mc20_c, @function
put_h264_qpel2_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
.option	pic0
j	put_h264_qpel2_h_lowpass
.option	pic2
move	$7,$6

.set	macro
.set	reorder
.end	put_h264_qpel2_mc20_c
.size	put_h264_qpel2_mc20_c, .-put_h264_qpel2_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc00_c
.type	put_h264_qpel4_mc00_c, @function
put_h264_qpel4_mc00_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
addu	$8,$4,$6
addu	$3,$5,$6
addu	$7,$8,$6
addu	$2,$3,$6
addu	$9,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($5)  
lwr $10, 0($5)  

# 0 "" 2
#NO_APP
addu	$6,$2,$6
sw	$10,0($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
sw	$4,0($8)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($2)  
lwr $10, 0($2)  

# 0 "" 2
#NO_APP
sw	$10,0($7)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($6)  
lwr $2, 0($6)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
j	$31
sw	$2,0($9)
.set	macro
.set	reorder

.end	put_h264_qpel4_mc00_c
.size	put_h264_qpel4_mc00_c, .-put_h264_qpel4_mc00_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc20_c
.type	put_h264_qpel4_mc20_c, @function
put_h264_qpel4_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
.option	pic0
j	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$6

.set	macro
.set	reorder
.end	put_h264_qpel4_mc20_c
.size	put_h264_qpel4_mc20_c, .-put_h264_qpel4_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc20_c
.type	put_h264_qpel8_mc20_c, @function
put_h264_qpel8_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
.option	pic0
j	put_h264_qpel8_h_lowpass
.option	pic2
move	$7,$6

.set	macro
.set	reorder
.end	put_h264_qpel8_mc20_c
.size	put_h264_qpel8_mc20_c, .-put_h264_qpel8_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc22_c
.type	put_h264_qpel8_mc22_c, @function
put_h264_qpel8_mc22_c:
.frame	$sp,248,$31		# vars= 208, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-248
li	$2,8			# 0x8
move	$7,$6
sw	$31,244($sp)
move	$6,$5
addiu	$5,$sp,32
sw	$2,16($sp)
.option	pic0
jal	put_h264_qpel8_hv_lowpass
.option	pic2
sw	$7,20($sp)

lw	$31,244($sp)
j	$31
addiu	$sp,$sp,248

.set	macro
.set	reorder
.end	put_h264_qpel8_mc22_c
.size	put_h264_qpel8_mc22_c, .-put_h264_qpel8_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc20_c
.type	put_h264_qpel16_mc20_c, @function
put_h264_qpel16_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
move	$7,$6
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$17,$5
sw	$31,36($sp)
sw	$16,24($sp)
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$16,$6

addiu	$4,$18,8
addiu	$5,$17,8
move	$6,$16
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

sll	$2,$16,3
move	$6,$16
addu	$17,$17,$2
addu	$18,$18,$2
move	$5,$17
move	$4,$18
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

addiu	$4,$18,8
lw	$31,36($sp)
addiu	$5,$17,8
lw	$18,32($sp)
move	$6,$16
lw	$17,28($sp)
move	$7,$16
lw	$16,24($sp)
.option	pic0
j	put_h264_qpel8_h_lowpass
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_h264_qpel16_mc20_c
.size	put_h264_qpel16_mc20_c, .-put_h264_qpel16_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc00_c
.type	avg_h264_qpel4_mc00_c, @function
avg_h264_qpel4_mc00_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$8,0($4)
li	$3,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
#NO_APP
ori	$3,$3,0xfefe
xor	$7,$2,$8
and	$7,$7,$3
srl	$7,$7,1
or	$2,$2,$8
subu	$2,$2,$7
addu	$9,$4,$6
addu	$5,$5,$6
sw	$2,0($4)
addu	$7,$9,$6
lw	$10,0($9)
addu	$8,$5,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
#NO_APP
addu	$5,$7,$6
xor	$4,$2,$10
and	$4,$4,$3
srl	$4,$4,1
or	$2,$2,$10
subu	$2,$2,$4
addu	$6,$8,$6
sw	$2,0($9)
lw	$9,0($7)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($8)  
lwr $2, 0($8)  

# 0 "" 2
#NO_APP
xor	$4,$2,$9
and	$4,$4,$3
srl	$4,$4,1
or	$2,$2,$9
subu	$2,$2,$4
sw	$2,0($7)
lw	$7,0($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($6)  
lwr $2, 0($6)  

# 0 "" 2
#NO_APP
xor	$4,$2,$7
and	$3,$4,$3
srl	$3,$3,1
or	$2,$2,$7
subu	$2,$2,$3
.set	noreorder
.set	nomacro
j	$31
sw	$2,0($5)
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc00_c
.size	avg_h264_qpel4_mc00_c, .-avg_h264_qpel4_mc00_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc20_c
.type	avg_h264_qpel4_mc20_c, @function
avg_h264_qpel4_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$8,%hi(ff_cropTbl+1024)
li	$9,4			# 0x4
addiu	$8,$8,%lo(ff_cropTbl+1024)
$L697:
lbu	$2,1($5)
addiu	$9,$9,-1
lbu	$7,-1($5)
lbu	$3,0($5)
lbu	$14,2($5)
lbu	$15,-2($5)
addu	$3,$3,$2
lbu	$24,3($5)
addu	$14,$7,$14
lbu	$13,0($4)
sll	$7,$3,2
lbu	$12,1($4)
sll	$2,$14,2
lbu	$11,2($4)
sll	$3,$3,4
lbu	$10,3($4)
addu	$2,$2,$14
addu	$3,$7,$3
addu	$7,$15,$24
subu	$2,$3,$2
addu	$2,$2,$7
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$8,$2
lbu	$2,0($2)
addu	$2,$13,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,0($4)
lbu	$2,2($5)
lbu	$7,0($5)
lbu	$3,1($5)
lbu	$13,3($5)
lbu	$14,-1($5)
addu	$3,$3,$2
lbu	$15,4($5)
addu	$13,$7,$13
sll	$7,$3,2
sll	$2,$13,2
sll	$3,$3,4
addu	$2,$2,$13
addu	$3,$7,$3
addu	$7,$14,$15
subu	$2,$3,$2
addu	$2,$2,$7
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$8,$2
lbu	$2,0($2)
addu	$2,$12,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,1($4)
lbu	$2,3($5)
lbu	$7,1($5)
lbu	$3,2($5)
lbu	$12,4($5)
lbu	$13,0($5)
addu	$3,$3,$2
lbu	$14,5($5)
addu	$12,$7,$12
sll	$7,$3,2
sll	$2,$12,2
sll	$3,$3,4
addu	$2,$2,$12
addu	$3,$7,$3
addu	$7,$13,$14
subu	$2,$3,$2
addu	$2,$2,$7
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$8,$2
lbu	$2,0($2)
addu	$11,$11,$2
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,2($4)
lbu	$2,4($5)
lbu	$7,2($5)
lbu	$3,3($5)
lbu	$11,5($5)
lbu	$12,1($5)
addu	$3,$3,$2
lbu	$13,6($5)
addu	$11,$7,$11
sll	$7,$3,2
sll	$2,$11,2
sll	$3,$3,4
addu	$2,$2,$11
addu	$3,$7,$3
addu	$7,$12,$13
subu	$2,$3,$2
addu	$5,$5,$6
addu	$2,$2,$7
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$8,$2
lbu	$2,0($2)
addu	$10,$10,$2
addiu	$10,$10,1
sra	$10,$10,1
sb	$10,3($4)
bne	$9,$0,$L697
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	avg_h264_qpel4_mc20_c
.size	avg_h264_qpel4_mc20_c, .-avg_h264_qpel4_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc10_c
.type	avg_h264_qpel8_mc10_c, @function
avg_h264_qpel8_mc10_c:
.frame	$sp,128,$31		# vars= 64, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-128
sw	$16,108($sp)
addiu	$16,$sp,40
sw	$17,112($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$19,120($sp)
move	$7,$17
sw	$31,124($sp)
move	$19,$4
sw	$18,116($sp)
move	$4,$16
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$18,$5

li	$2,8			# 0x8
sw	$17,16($sp)
move	$4,$19
move	$5,$18
sw	$2,20($sp)
move	$6,$16
sw	$2,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,124($sp)
lw	$19,120($sp)
lw	$18,116($sp)
lw	$17,112($sp)
lw	$16,108($sp)
j	$31
addiu	$sp,$sp,128

.set	macro
.set	reorder
.end	avg_h264_qpel8_mc10_c
.size	avg_h264_qpel8_mc10_c, .-avg_h264_qpel8_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc20_c
.type	avg_h264_qpel8_mc20_c, @function
avg_h264_qpel8_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
.option	pic0
j	avg_h264_qpel8_h_lowpass
.option	pic2
move	$7,$6

.set	macro
.set	reorder
.end	avg_h264_qpel8_mc20_c
.size	avg_h264_qpel8_mc20_c, .-avg_h264_qpel8_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc30_c
.type	avg_h264_qpel8_mc30_c, @function
avg_h264_qpel8_mc30_c:
.frame	$sp,128,$31		# vars= 64, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-128
sw	$16,108($sp)
addiu	$16,$sp,40
sw	$17,112($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$18,116($sp)
move	$7,$17
sw	$31,124($sp)
move	$18,$4
sw	$19,120($sp)
move	$4,$16
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$19,$5

li	$2,8			# 0x8
sw	$17,16($sp)
addiu	$5,$19,1
move	$4,$18
sw	$2,20($sp)
move	$6,$16
sw	$2,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,124($sp)
lw	$19,120($sp)
lw	$18,116($sp)
lw	$17,112($sp)
lw	$16,108($sp)
j	$31
addiu	$sp,$sp,128

.set	macro
.set	reorder
.end	avg_h264_qpel8_mc30_c
.size	avg_h264_qpel8_mc30_c, .-avg_h264_qpel8_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc20_c
.type	avg_h264_qpel16_mc20_c, @function
avg_h264_qpel16_mc20_c:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
move	$7,$6
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$17,$5
sw	$31,36($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_h264_qpel8_h_lowpass
.option	pic2
move	$16,$6

addiu	$4,$18,8
addiu	$5,$17,8
move	$6,$16
.option	pic0
jal	avg_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

sll	$2,$16,3
move	$6,$16
addu	$17,$17,$2
addu	$18,$18,$2
move	$5,$17
move	$4,$18
.option	pic0
jal	avg_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

addiu	$4,$18,8
lw	$31,36($sp)
addiu	$5,$17,8
lw	$18,32($sp)
move	$6,$16
lw	$17,28($sp)
move	$7,$16
lw	$16,24($sp)
.option	pic0
j	avg_h264_qpel8_h_lowpass
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc20_c
.size	avg_h264_qpel16_mc20_c, .-avg_h264_qpel16_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	h263_v_loop_filter_c
.type	h263_v_loop_filter_c, @function
h263_v_loop_filter_c:
.frame	$sp,24,$31		# vars= 16, regs= 0/0, args= 0, gp= 8
.mask	0x00000000,0
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
sll	$2,$5,1
addiu	$28,$28,%lo(__gnu_local_gp)
addu	$2,$2,$5
li	$10,4			# 0x4
lw	$3,%got(ff_h263_loop_filter_strength)($28)
addiu	$sp,$sp,-24
subu	$10,$10,$5
subu	$4,$4,$2
.cprestore	0
addu	$6,$3,$6
subu	$7,$0,$5
li	$12,2			# 0x2
lbu	$8,0($6)
li	$14,-1			# 0xffffffffffffffff
li	$13,1			# 0x1
andi	$11,$8,0xffff
subu	$6,$0,$11
subu	$9,$0,$8
sll	$6,$6,1
sll	$3,$9,1
sll	$15,$8,1
sll	$11,$11,1
andi	$6,$6,0xffff
$L727:
#APP
# 2963 "dsputil.c" 1
.word	0b01110000100001010000000001010110	#S32LDIV XR1,$4,$5,0
# 0 "" 2
# 2964 "dsputil.c" 1
.word	0b01110000100001010000000011010110	#S32LDIV XR3,$4,$5,0
# 0 "" 2
# 2965 "dsputil.c" 1
.word	0b01110000100001010000000101010110	#S32LDIV XR5,$4,$5,0
# 0 "" 2
# 2966 "dsputil.c" 1
.word	0b01110000100001010000000111010110	#S32LDIV XR7,$4,$5,0
# 0 "" 2
# 2968 "dsputil.c" 1
.word	0b01110011001001011100011010011100	#Q8ADDE XR10,XR1,XR7,XR9,SS
# 0 "" 2
# 2969 "dsputil.c" 1
.word	0b01110011001011001101011100011100	#Q8ADDE XR12,XR5,XR3,XR11,SS
# 0 "" 2
# 2970 "dsputil.c" 1
.word	0b01110000101011101111001100110100	#Q16SLL XR12,XR12,XR11,XR11,2
# 0 "" 2
# 2972 "dsputil.c" 1
.word	0b01110000001011100110110000001110	#Q16ADD XR0,XR11,XR9,XR11,AA,WW
# 0 "" 2
# 2973 "dsputil.c" 1
.word	0b01110000001100101011000000001110	#Q16ADD XR0,XR12,XR10,XR12,AA,WW
# 0 "" 2
# 2976 "dsputil.c" 1
.word	0b01110000111011101111001100110111	#Q16SAR XR12,XR12,XR11,XR11,3
# 0 "" 2
# 2978 "dsputil.c" 1
.word	0b01110000000110000000001011101110	#S32M2I XR11, $24
# 0 "" 2
#NO_APP
move	$2,$24
sw	$24,16($sp)
#APP
# 2979 "dsputil.c" 1
.word	0b01110000000110010000001100101110	#S32M2I XR12, $25
# 0 "" 2
#NO_APP
sll	$2,$2,16
sra	$2,$2,16
slt	$24,$2,$3
.set	noreorder
.set	nomacro
bne	$24,$0,$L707
sw	$25,20($sp)
.set	macro
.set	reorder

slt	$24,$2,$9
.set	noreorder
.set	nomacro
bne	$24,$0,$L708
slt	$24,$2,$8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$24,$0,$L734
slt	$24,$2,$15
.set	macro
.set	reorder

$L730:
sh	$2,8($sp)
$L711:
lh	$2,18($sp)
slt	$24,$2,$3
.set	noreorder
.set	nomacro
bne	$24,$0,$L712
slt	$24,$2,$9
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$24,$0,$L713
slt	$24,$2,$8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$24,$0,$L735
slt	$24,$2,$15
.set	macro
.set	reorder

$L731:
sh	$2,10($sp)
$L716:
lh	$2,20($sp)
slt	$24,$2,$3
bne	$24,$0,$L717
$L738:
slt	$24,$2,$9
.set	noreorder
.set	nomacro
bne	$24,$0,$L718
slt	$24,$2,$8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$24,$0,$L736
slt	$24,$2,$15
.set	macro
.set	reorder

$L732:
sh	$2,12($sp)
$L721:
lh	$2,22($sp)
slt	$24,$2,$3
.set	noreorder
.set	nomacro
bne	$24,$0,$L722
slt	$24,$2,$9
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$24,$0,$L723
slt	$24,$2,$8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$24,$0,$L737
slt	$24,$2,$15
.set	macro
.set	reorder

$L733:
sh	$2,14($sp)
$L726:
lw	$2,8($sp)
#APP
# 2990 "dsputil.c" 1
.word	0b01110000000000100000001110101111	#S32I2M XR14,$2
# 0 "" 2
#NO_APP
lw	$2,12($sp)
#APP
# 2991 "dsputil.c" 1
.word	0b01110000000000100000001111101111	#S32I2M XR15,$2
# 0 "" 2
# 2993 "dsputil.c" 1
.word	0b01110000000110111011111110000111	#Q16SAT XR14,XR15,XR14
# 0 "" 2
# 2995 "dsputil.c" 1
.word	0b01110000000011111000110100011100	#Q8ADDE XR4,XR3,XR14,XR3,AA
# 0 "" 2
# 2996 "dsputil.c" 1
.word	0b01110011000101111001010110011100	#Q8ADDE XR6,XR5,XR14,XR5,SS
# 0 "" 2
# 2998 "dsputil.c" 1
.word	0b01110000000110001101000011000111	#Q16SAT XR3,XR4,XR3
# 0 "" 2
# 2999 "dsputil.c" 1
.word	0b01110000000110010101100101000111	#Q16SAT XR5,XR6,XR5
# 0 "" 2
# 3002 "dsputil.c" 1
.word	0b01110000000010111011101110000111	#D16CPS XR14,XR14,XR14
# 0 "" 2
# 3003 "dsputil.c" 1
.word	0b01110000000010111111111111000111	#D16CPS XR15,XR15,XR15
# 0 "" 2
# 3005 "dsputil.c" 1
.word	0b01110000011110111011111111110111	#Q16SAR XR15,XR15,XR14,XR14,1
# 0 "" 2
# 3007 "dsputil.c" 1
.word	0b01110000101001100110101010110111	#Q16SAR XR10,XR10,XR9,XR9,2
# 0 "" 2
# 3009 "dsputil.c" 1
.word	0b01110000000011100000001100101111	#S32I2M XR12,$14
# 0 "" 2
# 3011 "dsputil.c" 1
.word	0b01110000000010110011101101000111	#D16CPS XR13,XR14,XR12
# 0 "" 2
# 3012 "dsputil.c" 1
.word	0b01110000000010100111011011000011	#D16MAX XR11,XR13,XR9
# 0 "" 2
# 3013 "dsputil.c" 1
.word	0b01110000000011101111101011000011	#D16MIN XR11,XR14,XR11
# 0 "" 2
# 3015 "dsputil.c" 1
.word	0b01110000000010110011111101000111	#D16CPS XR13,XR15,XR12
# 0 "" 2
# 3016 "dsputil.c" 1
.word	0b01110000000010101011010110000011	#D16MAX XR6,XR13,XR10
# 0 "" 2
# 3017 "dsputil.c" 1
.word	0b01110000000011011011110110000011	#D16MIN XR6,XR15,XR6
# 0 "" 2
# 3019 "dsputil.c" 1
.word	0b01110000000110101101101011000111	#Q16SAT XR11,XR6,XR11
# 0 "" 2
# 3021 "dsputil.c" 1
.word	0b01110011000001101100010010011100	#Q8ADDE XR2,XR1,XR11,XR1,SS
# 0 "" 2
# 3022 "dsputil.c" 1
.word	0b01110000000111101101111000011100	#Q8ADDE XR8,XR7,XR11,XR7,AA
# 0 "" 2
# 3024 "dsputil.c" 1
.word	0b01110000000110000100100001000111	#Q16SAT XR1,XR2,XR1
# 0 "" 2
# 3025 "dsputil.c" 1
.word	0b01110000000110011110000111000111	#Q16SAT XR7,XR8,XR7
# 0 "" 2
# 3028 "dsputil.c" 1
.word	0b01110000100000000000000111010001	#S32STD XR7,$4,0
# 0 "" 2
# 3029 "dsputil.c" 1
.word	0b01110000100001110000000101010111	#S32SDIV XR5,$4,$7,0
# 0 "" 2
# 3030 "dsputil.c" 1
.word	0b01110000100001110000000011010111	#S32SDIV XR3,$4,$7,0
# 0 "" 2
# 3031 "dsputil.c" 1
.word	0b01110000100001110000000001010111	#S32SDIV XR1,$4,$7,0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$12,$13,$L728
addu	$4,$4,$10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,24
.set	macro
.set	reorder

$L737:
.set	noreorder
.set	nomacro
bne	$24,$0,$L725
subu	$2,$11,$2
.set	macro
.set	reorder

$L722:
.option	pic0
.set	noreorder
.set	nomacro
j	$L726
.option	pic2
sh	$0,14($sp)
.set	macro
.set	reorder

$L735:
.set	noreorder
.set	nomacro
bne	$24,$0,$L715
subu	$2,$11,$2
.set	macro
.set	reorder

$L712:
lh	$2,20($sp)
slt	$24,$2,$3
.set	noreorder
.set	nomacro
beq	$24,$0,$L738
sh	$0,10($sp)
.set	macro
.set	reorder

$L717:
.option	pic0
.set	noreorder
.set	nomacro
j	$L721
.option	pic2
sh	$0,12($sp)
.set	macro
.set	reorder

$L734:
.set	noreorder
.set	nomacro
bne	$24,$0,$L710
subu	$2,$11,$2
.set	macro
.set	reorder

$L707:
.option	pic0
.set	noreorder
.set	nomacro
j	$L711
.option	pic2
sh	$0,8($sp)
.set	macro
.set	reorder

$L728:
.option	pic0
.set	noreorder
.set	nomacro
j	$L727
.option	pic2
li	$12,1			# 0x1
.set	macro
.set	reorder

$L718:
.option	pic0
.set	noreorder
.set	nomacro
j	$L732
.option	pic2
subu	$2,$6,$2
.set	macro
.set	reorder

$L723:
.option	pic0
.set	noreorder
.set	nomacro
j	$L733
.option	pic2
subu	$2,$6,$2
.set	macro
.set	reorder

$L713:
.option	pic0
.set	noreorder
.set	nomacro
j	$L731
.option	pic2
subu	$2,$6,$2
.set	macro
.set	reorder

$L708:
.option	pic0
.set	noreorder
.set	nomacro
j	$L730
.option	pic2
subu	$2,$6,$2
.set	macro
.set	reorder

$L736:
.set	noreorder
.set	nomacro
beq	$24,$0,$L717
subu	$2,$11,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L721
.option	pic2
sh	$2,12($sp)
.set	macro
.set	reorder

$L710:
.option	pic0
.set	noreorder
.set	nomacro
j	$L711
.option	pic2
sh	$2,8($sp)
.set	macro
.set	reorder

$L715:
.option	pic0
.set	noreorder
.set	nomacro
j	$L716
.option	pic2
sh	$2,10($sp)
.set	macro
.set	reorder

$L725:
.option	pic0
.set	noreorder
.set	nomacro
j	$L726
.option	pic2
sh	$2,14($sp)
.set	macro
.set	reorder

.end	h263_v_loop_filter_c
.size	h263_v_loop_filter_c, .-h263_v_loop_filter_c
.align	2
.set	nomips16
.set	nomicromips
.ent	h263_h_loop_filter_c
.type	h263_h_loop_filter_c, @function
h263_h_loop_filter_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$4,$4,-2
addiu	$28,$28,%lo(__gnu_local_gp)
li	$11,8			# 0x8
lw	$2,%got(ff_h263_loop_filter_strength)($28)
addu	$6,$2,$6
lbu	$14,0($6)
subu	$13,$0,$14
sll	$15,$14,1
.option	pic0
j	$L751
.option	pic2
sll	$12,$13,1

$L762:
slt	$3,$2,$13
beq	$3,$0,$L742
slt	$3,$2,$14

subu	$2,$12,$2
$L743:
addu	$6,$6,$2
andi	$3,$6,0x100
beq	$3,$0,$L760
subu	$8,$8,$2

subu	$3,$0,$2
slt	$10,$2,0
movn	$2,$3,$10
sra	$6,$6,31
nor	$6,$0,$6
sra	$2,$2,1
subu	$3,$0,$2
$L746:
andi	$10,$8,0x100
beq	$10,$0,$L763
addiu	$10,$7,3

sra	$8,$8,31
nor	$8,$0,$8
$L748:
addiu	$10,$7,3
$L763:
sb	$6,1($4)
slt	$6,$7,0
movn	$7,$10,$6
sra	$7,$7,2
slt	$6,$7,$3
bne	$6,$0,$L750
sb	$8,2($4)

slt	$6,$7,$2
move	$3,$7
movz	$3,$2,$6
$L750:
andi	$3,$3,0x00ff
subu	$9,$9,$3
addu	$3,$3,$24
addiu	$11,$11,-1
sb	$9,0($4)
sb	$3,3($4)
beq	$11,$0,$L765
addu	$4,$4,$5

$L751:
lbu	$6,1($4)
lbu	$8,2($4)
lbu	$9,0($4)
lbu	$24,3($4)
subu	$2,$8,$6
subu	$7,$9,$24
sll	$2,$2,2
addu	$2,$7,$2
addiu	$3,$2,7
slt	$10,$2,0
movn	$2,$3,$10
sra	$2,$2,3
slt	$3,$2,$12
beq	$3,$0,$L762
move	$3,$0

$L764:
.option	pic0
j	$L748
.option	pic2
move	$2,$0

$L742:
bne	$3,$0,$L743
slt	$3,$2,$15

beq	$3,$0,$L764
move	$3,$0

.option	pic0
j	$L743
.option	pic2
subu	$2,$15,$2

$L765:
j	$31
nop

$L760:
subu	$10,$0,$2
slt	$3,$2,0
movn	$2,$10,$3
sra	$2,$2,1
.option	pic0
j	$L746
.option	pic2
subu	$3,$0,$2

.set	macro
.set	reorder
.end	h263_h_loop_filter_c
.size	h263_h_loop_filter_c, .-h263_h_loop_filter_c
.align	2
.set	nomips16
.set	nomicromips
.ent	h261_loop_filter_c
.type	h261_loop_filter_c, @function
h261_loop_filter_c:
.frame	$sp,288,$31		# vars= 256, regs= 6/0, args= 0, gp= 8
.mask	0x003f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$5,3
addiu	$sp,$sp,-288
subu	$2,$2,$5
sw	$21,284($sp)
sll	$3,$5,1
addu	$2,$4,$2
sw	$20,280($sp)
sw	$19,276($sp)
addu	$3,$4,$3
sw	$18,272($sp)
move	$6,$4
sw	$17,268($sp)
sw	$16,264($sp)
lbu	$8,1($2)
lbu	$16,2($2)
lbu	$17,3($4)
lbu	$21,0($2)
sll	$8,$8,2
lbu	$18,3($2)
sll	$16,$16,2
lbu	$15,4($2)
sll	$17,$17,2
lbu	$13,5($2)
sll	$21,$21,2
lbu	$11,6($2)
sll	$18,$18,2
lbu	$9,7($2)
sll	$15,$15,2
lbu	$24,0($4)
sll	$13,$13,2
lbu	$20,1($4)
sll	$11,$11,2
lbu	$19,2($4)
sll	$9,$9,2
lbu	$7,4($4)
sll	$24,$24,2
lbu	$14,5($4)
sll	$20,$20,2
lbu	$12,6($4)
sll	$19,$19,2
lbu	$10,7($4)
sll	$7,$7,2
sll	$14,$14,2
sw	$8,236($sp)
sll	$12,$12,2
sw	$16,240($sp)
sll	$10,$10,2
sw	$17,20($sp)
addiu	$2,$sp,8
sw	$7,24($sp)
addu	$8,$4,$5
sw	$24,8($sp)
li	$16,1			# 0x1
sw	$21,232($sp)
li	$17,7			# 0x7
sw	$20,12($sp)
sw	$19,16($sp)
move	$7,$2
sw	$18,244($sp)
sw	$15,248($sp)
sw	$14,28($sp)
sw	$13,252($sp)
sw	$12,32($sp)
sw	$11,256($sp)
sw	$10,36($sp)
sw	$9,260($sp)
$L767:
lbu	$25,0($8)
addiu	$7,$7,32
lbu	$15,2($8)
addiu	$16,$16,1
lbu	$19,0($4)
lbu	$18,2($4)
sll	$25,$25,1
lbu	$24,1($8)
sll	$15,$15,1
lbu	$13,4($8)
addu	$25,$19,$25
lbu	$12,5($8)
addu	$15,$18,$15
lbu	$11,6($8)
sll	$24,$24,1
lbu	$9,1($4)
sll	$13,$13,1
lbu	$20,4($4)
sll	$12,$12,1
lbu	$19,5($4)
sll	$11,$11,1
lbu	$18,6($4)
addu	$24,$9,$24
lbu	$14,3($8)
addu	$13,$20,$13
lbu	$10,7($8)
addu	$12,$19,$12
lbu	$21,3($4)
addu	$11,$18,$11
lbu	$9,7($4)
sll	$14,$14,1
lbu	$20,0($3)
sll	$10,$10,1
lbu	$19,1($3)
addu	$14,$21,$14
lbu	$18,2($3)
addu	$10,$9,$10
lbu	$21,3($3)
addu	$25,$25,$20
addu	$9,$24,$19
lbu	$20,4($3)
lbu	$19,5($3)
addu	$15,$15,$18
lbu	$24,7($3)
addu	$14,$14,$21
lbu	$18,6($3)
addu	$13,$13,$20
addu	$12,$12,$19
sw	$9,4($7)
addu	$9,$10,$24
sw	$25,0($7)
addu	$11,$11,$18
sw	$15,8($7)
sw	$14,12($7)
addu	$4,$4,$5
sw	$13,16($7)
addu	$8,$8,$5
sw	$12,20($7)
addu	$3,$3,$5
sw	$11,24($7)
bne	$16,$17,$L767
sw	$9,28($7)

addiu	$10,$sp,264
$L768:
lw	$15,8($2)
addiu	$2,$2,32
sll	$17,$15,1
lw	$3,-28($2)
lw	$14,-20($2)
lw	$16,-16($2)
lw	$25,-12($2)
sll	$11,$3,1
lw	$24,-8($2)
addu	$3,$3,$17
lw	$13,-32($2)
sll	$9,$14,1
sll	$8,$16,1
lw	$12,-4($2)
sll	$7,$25,1
sll	$4,$24,1
addu	$3,$3,$14
addu	$11,$13,$11
addu	$9,$15,$9
addu	$8,$14,$8
addu	$7,$16,$7
addu	$4,$25,$4
addiu	$3,$3,8
addu	$4,$12,$4
addu	$11,$11,$15
addu	$9,$9,$16
addu	$8,$8,$25
addu	$7,$7,$24
sra	$3,$3,4
addiu	$13,$13,2
addiu	$12,$12,2
addiu	$11,$11,8
sb	$3,2($6)
addiu	$9,$9,8
addiu	$8,$8,8
addiu	$7,$7,8
addiu	$4,$4,8
sra	$13,$13,2
sra	$12,$12,2
sra	$11,$11,4
sra	$9,$9,4
sb	$13,0($6)
sra	$8,$8,4
sb	$12,7($6)
sra	$7,$7,4
sb	$11,1($6)
sra	$3,$4,4
sb	$9,3($6)
sb	$8,4($6)
sb	$7,5($6)
sb	$3,6($6)
bne	$2,$10,$L768
addu	$6,$6,$5

lw	$21,284($sp)
lw	$20,280($sp)
lw	$19,276($sp)
lw	$18,272($sp)
lw	$17,268($sp)
lw	$16,264($sp)
j	$31
addiu	$sp,$sp,288

.set	macro
.set	reorder
.end	h261_loop_filter_c
.size	h261_loop_filter_c, .-h261_loop_filter_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs16_c
.type	pix_abs16_c, @function
pix_abs16_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$15,24($sp)
blez	$15,$L791
nop

move	$24,$0
move	$2,$0
$L790:
lbu	$11,0($5)
addiu	$24,$24,1
lbu	$3,0($6)
lbu	$4,1($6)
lbu	$14,1($5)
subu	$3,$11,$3
lbu	$13,2($6)
lbu	$11,2($5)
subu	$14,$14,$4
lbu	$8,3($6)
subu	$4,$0,$3
lbu	$10,3($5)
slt	$12,$3,0
lbu	$9,4($5)
subu	$11,$11,$13
movn	$3,$4,$12
subu	$4,$0,$14
lbu	$13,4($6)
slt	$12,$14,0
subu	$10,$10,$8
movn	$14,$4,$12
subu	$4,$0,$11
lbu	$12,5($6)
slt	$16,$11,0
lbu	$8,5($5)
subu	$9,$9,$13
addu	$2,$3,$2
movn	$11,$4,$16
subu	$3,$0,$10
lbu	$4,6($6)
slt	$16,$10,0
lbu	$13,6($5)
subu	$8,$8,$12
addu	$14,$14,$2
movn	$10,$3,$16
subu	$2,$0,$9
lbu	$3,7($6)
slt	$16,$9,0
lbu	$12,7($5)
subu	$13,$13,$4
addu	$11,$11,$14
movn	$9,$2,$16
subu	$2,$0,$8
lbu	$25,8($6)
slt	$14,$8,0
lbu	$4,8($5)
subu	$12,$12,$3
addu	$10,$10,$11
movn	$8,$2,$14
subu	$2,$0,$13
lbu	$16,9($6)
slt	$14,$13,0
lbu	$3,9($5)
subu	$4,$4,$25
addu	$9,$9,$10
movn	$13,$2,$14
subu	$2,$0,$12
lbu	$25,10($6)
slt	$14,$12,0
lbu	$11,10($5)
subu	$3,$3,$16
addu	$8,$8,$9
movn	$12,$2,$14
subu	$2,$0,$4
lbu	$10,11($5)
slt	$14,$4,0
lbu	$16,11($6)
subu	$11,$11,$25
movn	$4,$2,$14
addu	$13,$13,$8
subu	$14,$0,$3
lbu	$2,12($6)
slt	$25,$3,0
lbu	$9,12($5)
subu	$10,$10,$16
movn	$3,$14,$25
addu	$12,$12,$13
subu	$13,$0,$11
lbu	$25,13($6)
slt	$14,$11,0
lbu	$8,13($5)
subu	$9,$9,$2
movn	$11,$13,$14
addu	$12,$4,$12
subu	$13,$0,$10
lbu	$2,14($5)
slt	$14,$10,0
lbu	$4,14($6)
subu	$8,$8,$25
movn	$10,$13,$14
addu	$12,$3,$12
subu	$14,$0,$9
lbu	$3,15($5)
slt	$25,$9,0
lbu	$13,15($6)
subu	$4,$2,$4
movn	$9,$14,$25
addu	$2,$11,$12
subu	$11,$0,$8
slt	$12,$8,0
subu	$3,$3,$13
movn	$8,$11,$12
addu	$2,$10,$2
subu	$10,$0,$4
slt	$11,$4,0
addu	$2,$9,$2
movn	$4,$10,$11
subu	$9,$0,$3
slt	$10,$3,0
addu	$2,$8,$2
movn	$3,$9,$10
addu	$5,$5,$7
addu	$2,$4,$2
addu	$6,$6,$7
bne	$24,$15,$L790
addu	$2,$3,$2

$L773:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

$L791:
.option	pic0
j	$L773
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	pix_abs16_c
.size	pix_abs16_c, .-pix_abs16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs16_x2_c
.type	pix_abs16_x2_c, @function
pix_abs16_x2_c:
.frame	$sp,88,$31		# vars= 40, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-88
sw	$fp,84($sp)
sw	$23,80($sp)
sw	$22,76($sp)
sw	$21,72($sp)
sw	$20,68($sp)
sw	$19,64($sp)
sw	$18,60($sp)
sw	$17,56($sp)
sw	$16,52($sp)
lw	$2,104($sp)
blez	$2,$L813
move	$16,$0

move	$15,$0
sw	$7,12($sp)
move	$17,$5
move	$24,$6
move	$5,$15
$L812:
lbu	$20,1($24)
addiu	$16,$16,1
lbu	$6,2($24)
lbu	$3,0($24)
lbu	$18,3($24)
lbu	$19,1($17)
addu	$3,$3,$20
lbu	$25,2($17)
addu	$20,$20,$6
lbu	$15,0($17)
addu	$6,$6,$18
lbu	$14,4($24)
addiu	$20,$20,1
lbu	$22,3($17)
addiu	$6,$6,1
lbu	$11,5($24)
sra	$20,$20,1
lbu	$10,6($24)
sra	$6,$6,1
lbu	$9,7($24)
subu	$20,$19,$20
lbu	$19,7($17)
addiu	$3,$3,1
lbu	$8,8($24)
addu	$18,$18,$14
lbu	$23,4($17)
sra	$3,$3,1
lbu	$7,9($24)
sw	$19,16($sp)
subu	$19,$25,$6
lbu	$25,12($24)
subu	$15,$15,$3
addiu	$18,$18,1
lbu	$13,5($17)
subu	$21,$0,$15
lbu	$4,10($24)
slt	$2,$15,0
lbu	$12,6($17)
sw	$25,8($sp)
subu	$6,$0,$20
lbu	$25,9($17)
addu	$14,$14,$11
movn	$15,$21,$2
slt	$21,$20,0
sra	$2,$18,1
movn	$20,$6,$21
addu	$11,$11,$10
addiu	$14,$14,1
lbu	$18,8($17)
addu	$15,$15,$5
lbu	$6,10($17)
addiu	$11,$11,1
lbu	$3,11($24)
sra	$14,$14,1
lbu	$fp,13($24)
sra	$11,$11,1
sw	$25,24($sp)
subu	$21,$0,$19
sw	$18,20($sp)
subu	$18,$22,$2
sw	$6,28($sp)
addu	$10,$10,$9
lbu	$25,11($17)
subu	$14,$23,$14
lbu	$2,14($24)
subu	$11,$13,$11
lbu	$6,15($24)
slt	$22,$19,0
movn	$19,$21,$22
addu	$9,$9,$8
addiu	$10,$10,1
sw	$25,32($sp)
subu	$25,$0,$18
lbu	$5,12($17)
addu	$15,$20,$15
subu	$20,$0,$14
lbu	$23,16($24)
sra	$10,$10,1
lbu	$22,13($17)
addiu	$9,$9,1
lbu	$21,14($17)
sw	$5,36($sp)
slt	$5,$18,0
addu	$8,$8,$7
sw	$5,40($sp)
subu	$10,$12,$10
lw	$13,40($sp)
addu	$15,$19,$15
sra	$9,$9,1
lbu	$5,15($17)
addu	$7,$7,$4
movn	$18,$25,$13
slt	$25,$14,0
subu	$13,$0,$11
movn	$14,$20,$25
slt	$19,$11,0
addiu	$8,$8,1
lw	$25,16($sp)
addu	$15,$18,$15
movn	$11,$13,$19
subu	$12,$0,$10
addu	$4,$4,$3
subu	$9,$25,$9
lw	$19,8($sp)
lw	$18,20($sp)
slt	$13,$10,0
sra	$8,$8,1
movn	$10,$12,$13
addiu	$7,$7,1
subu	$12,$0,$9
lw	$25,24($sp)
subu	$8,$18,$8
lw	$18,36($sp)
addu	$15,$14,$15
lw	$14,32($sp)
addu	$3,$3,$19
slt	$13,$9,0
sra	$7,$7,1
movn	$9,$12,$13
addiu	$4,$4,1
lw	$13,28($sp)
subu	$7,$25,$7
addu	$15,$11,$15
addu	$25,$19,$fp
lw	$19,12($sp)
subu	$11,$0,$8
sra	$4,$4,1
addiu	$3,$3,1
slt	$12,$8,0
subu	$4,$13,$4
movn	$8,$11,$12
addu	$15,$10,$15
addu	$20,$fp,$2
subu	$10,$0,$7
sra	$3,$3,1
addiu	$25,$25,1
slt	$11,$7,0
subu	$3,$14,$3
movn	$7,$10,$11
addu	$9,$9,$15
subu	$10,$0,$4
addu	$2,$2,$6
sra	$25,$25,1
addiu	$15,$20,1
slt	$11,$4,0
subu	$25,$18,$25
movn	$4,$10,$11
addu	$8,$8,$9
sra	$15,$15,1
subu	$9,$0,$3
addiu	$2,$2,1
addu	$6,$6,$23
slt	$10,$3,0
subu	$22,$22,$15
movn	$3,$9,$10
addu	$7,$7,$8
sra	$2,$2,1
subu	$8,$0,$25
addiu	$6,$6,1
slt	$9,$25,0
subu	$21,$21,$2
movn	$25,$8,$9
addu	$4,$4,$7
subu	$2,$0,$22
sra	$6,$6,1
slt	$7,$22,0
subu	$5,$5,$6
movn	$22,$2,$7
addu	$3,$3,$4
subu	$2,$0,$21
slt	$4,$21,0
addu	$25,$25,$3
movn	$21,$2,$4
subu	$2,$0,$5
slt	$3,$5,0
addu	$22,$22,$25
movn	$5,$2,$3
addu	$17,$17,$19
lw	$25,104($sp)
addu	$21,$21,$22
addu	$24,$24,$19
bne	$16,$25,$L812
addu	$5,$5,$21

move	$15,$5
$L795:
lw	$fp,84($sp)
move	$2,$15
lw	$23,80($sp)
lw	$22,76($sp)
lw	$21,72($sp)
lw	$20,68($sp)
lw	$19,64($sp)
lw	$18,60($sp)
lw	$17,56($sp)
lw	$16,52($sp)
j	$31
addiu	$sp,$sp,88

$L813:
.option	pic0
j	$L795
.option	pic2
move	$15,$0

.set	macro
.set	reorder
.end	pix_abs16_x2_c
.size	pix_abs16_x2_c, .-pix_abs16_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs16_y2_c
.type	pix_abs16_y2_c, @function
pix_abs16_y2_c:
.frame	$sp,80,$31		# vars= 32, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-80
move	$9,$6
sw	$fp,76($sp)
sw	$23,72($sp)
sw	$22,68($sp)
sw	$21,64($sp)
sw	$20,60($sp)
sw	$19,56($sp)
sw	$18,52($sp)
sw	$17,48($sp)
sw	$16,44($sp)
lw	$2,96($sp)
blez	$2,$L835
addu	$25,$6,$7

move	$12,$0
sw	$0,8($sp)
move	$15,$5
move	$16,$7
$L834:
lw	$3,8($sp)
lbu	$14,0($25)
lbu	$4,0($9)
addiu	$3,$3,1
lbu	$2,1($25)
lbu	$5,2($9)
lbu	$8,2($25)
addu	$4,$4,$14
lbu	$7,3($9)
lbu	$19,4($9)
addiu	$4,$4,1
lbu	$11,3($25)
addu	$8,$5,$8
lbu	$6,4($25)
sra	$4,$4,1
sw	$3,8($sp)
addiu	$8,$8,1
lbu	$3,1($9)
addu	$11,$7,$11
lbu	$14,0($15)
sra	$8,$8,1
lbu	$13,2($15)
addu	$6,$19,$6
addu	$3,$3,$2
lbu	$2,1($15)
subu	$14,$14,$4
lbu	$10,5($9)
addiu	$3,$3,1
lbu	$18,3($15)
subu	$13,$13,$8
lbu	$7,6($9)
sra	$3,$3,1
lbu	$22,7($9)
addiu	$11,$11,1
lbu	$5,5($25)
subu	$2,$2,$3
lbu	$4,6($25)
addiu	$6,$6,1
lbu	$fp,4($15)
lbu	$3,7($25)
subu	$17,$0,$14
slt	$19,$14,0
lbu	$21,5($15)
subu	$8,$0,$2
movn	$14,$17,$19
subu	$23,$0,$13
sra	$11,$11,1
lbu	$20,8($9)
sra	$6,$6,1
lbu	$19,6($15)
slt	$17,$2,0
slt	$24,$13,0
movn	$2,$8,$17
subu	$11,$18,$11
subu	$6,$fp,$6
lbu	$18,9($9)
lbu	$8,9($25)
addu	$5,$10,$5
lbu	$17,7($15)
addu	$4,$7,$4
lbu	$10,8($25)
addu	$3,$22,$3
lbu	$7,10($9)
addu	$12,$14,$12
movn	$13,$23,$24
subu	$14,$0,$11
addiu	$5,$5,1
lbu	$24,10($25)
addu	$10,$20,$10
lbu	$23,8($15)
sra	$5,$5,1
lbu	$fp,9($15)
addiu	$4,$4,1
lbu	$22,10($15)
addu	$12,$2,$12
addiu	$3,$3,1
lbu	$20,13($9)
subu	$2,$0,$6
subu	$5,$21,$5
sw	$fp,12($sp)
sw	$22,16($sp)
slt	$22,$11,0
movn	$11,$14,$22
sra	$4,$4,1
addiu	$10,$10,1
lbu	$14,11($25)
addu	$13,$13,$12
lbu	$fp,11($15)
sra	$3,$3,1
subu	$19,$19,$4
lbu	$22,12($9)
subu	$21,$0,$5
lbu	$4,14($9)
sw	$14,24($sp)
sra	$10,$10,1
lbu	$14,12($25)
subu	$17,$17,$3
addu	$11,$11,$13
sw	$fp,20($sp)
addu	$8,$18,$8
sw	$22,28($sp)
slt	$18,$5,0
lbu	$fp,11($9)
sw	$14,32($sp)
slt	$14,$6,0
movn	$6,$2,$14
subu	$10,$23,$10
addu	$7,$7,$24
movn	$5,$21,$18
subu	$18,$0,$19
slt	$24,$19,0
lw	$23,24($sp)
addu	$6,$6,$11
subu	$11,$0,$17
movn	$19,$18,$24
addiu	$8,$8,1
slt	$24,$17,0
lw	$18,12($sp)
addu	$fp,$fp,$23
movn	$17,$11,$24
sra	$8,$8,1
lw	$23,28($sp)
addu	$5,$5,$6
lw	$11,32($sp)
subu	$6,$0,$10
subu	$8,$18,$8
lbu	$2,13($25)
lbu	$14,14($25)
addiu	$7,$7,1
addu	$18,$23,$11
lw	$23,16($sp)
sw	$20,36($sp)
slt	$11,$10,0
movn	$10,$6,$11
sra	$7,$7,1
addiu	$fp,$fp,1
lw	$11,36($sp)
addu	$5,$19,$5
lw	$19,20($sp)
subu	$7,$23,$7
subu	$6,$0,$8
lbu	$12,15($9)
addu	$2,$11,$2
lbu	$20,12($15)
sra	$fp,$fp,1
lbu	$13,15($25)
addiu	$18,$18,1
lbu	$3,13($15)
slt	$11,$8,0
lbu	$21,14($15)
subu	$fp,$19,$fp
movn	$8,$6,$11
addu	$5,$17,$5
addu	$14,$4,$14
lbu	$22,15($15)
subu	$6,$0,$7
sra	$18,$18,1
addiu	$2,$2,1
slt	$4,$7,0
subu	$20,$20,$18
movn	$7,$6,$4
addu	$10,$10,$5
subu	$4,$0,$fp
sra	$2,$2,1
addu	$13,$12,$13
addiu	$14,$14,1
slt	$5,$fp,0
subu	$3,$3,$2
movn	$fp,$4,$5
subu	$2,$0,$20
addu	$8,$8,$10
sra	$14,$14,1
addiu	$13,$13,1
slt	$4,$20,0
subu	$21,$21,$14
movn	$20,$2,$4
sra	$12,$13,1
subu	$2,$0,$3
addu	$7,$7,$8
slt	$4,$3,0
subu	$22,$22,$12
movn	$3,$2,$4
addu	$fp,$fp,$7
subu	$2,$0,$21
slt	$4,$21,0
movn	$21,$2,$4
addu	$20,$20,$fp
subu	$2,$0,$22
slt	$4,$22,0
addu	$12,$3,$20
movn	$22,$2,$4
addu	$15,$15,$16
addu	$21,$21,$12
lw	$20,8($sp)
addu	$9,$9,$16
addu	$12,$22,$21
lw	$22,96($sp)
bne	$20,$22,$L834
addu	$25,$25,$16

$L817:
lw	$fp,76($sp)
move	$2,$12
lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
j	$31
addiu	$sp,$sp,80

$L835:
.option	pic0
j	$L817
.option	pic2
move	$12,$0

.set	macro
.set	reorder
.end	pix_abs16_y2_c
.size	pix_abs16_y2_c, .-pix_abs16_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs16_xy2_c
.type	pix_abs16_xy2_c, @function
pix_abs16_xy2_c:
.frame	$sp,120,$31		# vars= 72, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-120
move	$15,$6
sw	$fp,116($sp)
sw	$23,112($sp)
sw	$22,108($sp)
sw	$21,104($sp)
sw	$20,100($sp)
sw	$19,96($sp)
sw	$18,92($sp)
sw	$17,88($sp)
sw	$16,84($sp)
sw	$7,132($sp)
lw	$2,136($sp)
blez	$2,$L857
addu	$12,$6,$7

move	$14,$5
sw	$0,12($sp)
sw	$0,8($sp)
$L856:
lbu	$7,1($15)
lbu	$16,2($15)
lbu	$6,0($15)
lbu	$2,1($12)
lbu	$8,3($15)
addu	$17,$7,$16
lbu	$5,0($12)
addu	$6,$6,$7
lbu	$4,2($12)
addu	$17,$2,$17
lbu	$11,4($15)
addu	$16,$16,$8
lbu	$10,5($15)
addu	$5,$6,$5
lbu	$6,3($12)
addu	$17,$17,$4
addu	$16,$4,$16
lbu	$13,4($12)
addu	$4,$8,$11
lw	$3,12($sp)
addu	$11,$11,$10
lbu	$24,5($12)
addu	$4,$6,$4
lbu	$18,6($12)
addu	$11,$13,$11
lbu	$23,7($12)
addu	$4,$4,$13
lbu	$13,4($14)
addiu	$3,$3,1
lbu	$22,8($12)
addu	$5,$5,$2
lbu	$2,0($14)
lbu	$21,9($12)
addu	$16,$16,$6
lbu	$9,6($15)
addiu	$5,$5,2
lbu	$8,7($15)
addiu	$16,$16,2
lbu	$19,1($14)
sra	$5,$5,2
lbu	$7,8($15)
addiu	$17,$17,2
lbu	$25,2($14)
subu	$2,$2,$5
lbu	$6,9($15)
addu	$11,$11,$24
lbu	$fp,3($14)
sra	$17,$17,2
lbu	$5,10($15)
addu	$10,$10,$9
sw	$3,12($sp)
addiu	$11,$11,2
sw	$13,64($sp)
subu	$17,$19,$17
lbu	$3,11($15)
subu	$19,$0,$2
lbu	$13,5($14)
addu	$10,$24,$10
sra	$11,$11,2
slt	$24,$2,0
sw	$3,16($sp)
sra	$3,$16,2
sw	$13,36($sp)
addiu	$4,$4,2
lbu	$20,10($12)
addu	$9,$9,$8
lbu	$13,7($14)
sra	$4,$4,2
lbu	$16,6($14)
addu	$10,$10,$18
movn	$2,$19,$24
subu	$24,$0,$17
subu	$4,$fp,$4
sw	$13,44($sp)
addu	$9,$18,$9
lbu	$13,9($14)
addu	$8,$8,$7
sw	$16,40($sp)
addu	$7,$7,$6
lbu	$16,8($14)
addiu	$10,$10,2
lbu	$19,11($12)
addu	$9,$9,$23
sw	$13,52($sp)
addu	$8,$23,$8
lbu	$13,14($15)
addu	$7,$22,$7
sw	$16,48($sp)
subu	$16,$25,$3
lbu	$25,13($15)
addu	$6,$6,$5
lbu	$3,12($15)
slt	$23,$16,0
sw	$13,28($sp)
sra	$10,$10,2
lbu	$13,13($12)
addu	$8,$8,$22
sw	$25,20($sp)
slt	$25,$17,0
movn	$17,$24,$25
subu	$24,$0,$16
addiu	$9,$9,2
sw	$13,24($sp)
slt	$22,$4,0
lbu	$13,10($14)
addu	$7,$7,$21
lbu	$25,15($15)
addu	$6,$21,$6
lbu	$18,12($12)
sra	$9,$9,2
lbu	$fp,14($12)
addiu	$8,$8,2
sw	$13,56($sp)
addiu	$7,$7,2
lbu	$13,11($14)
sra	$8,$8,2
sw	$25,32($sp)
addu	$6,$6,$20
lbu	$25,16($15)
sra	$7,$7,2
movn	$16,$24,$23
subu	$24,$0,$4
addiu	$6,$6,2
sw	$13,60($sp)
lw	$13,64($sp)
sra	$6,$6,2
sw	$25,72($sp)
lbu	$25,15($12)
subu	$11,$13,$11
lw	$13,8($sp)
movn	$4,$24,$22
addu	$2,$2,$13
lbu	$13,12($14)
addu	$2,$17,$2
sw	$13,8($sp)
addu	$2,$16,$2
lbu	$13,16($12)
addu	$2,$4,$2
sw	$13,76($sp)
lbu	$13,13($14)
lw	$17,16($sp)
lw	$21,40($sp)
lw	$16,16($sp)
sw	$13,64($sp)
addu	$5,$5,$17
lbu	$13,14($14)
slt	$17,$11,0
subu	$9,$21,$9
lw	$21,20($sp)
addu	$24,$16,$3
lbu	$23,15($14)
subu	$4,$0,$9
sw	$13,68($sp)
addu	$5,$20,$5
lw	$13,36($sp)
addu	$3,$3,$21
addu	$5,$5,$19
addu	$3,$18,$3
subu	$10,$13,$10
subu	$13,$0,$11
slt	$16,$10,0
movn	$11,$13,$17
subu	$13,$0,$10
addu	$19,$19,$24
lw	$17,44($sp)
addiu	$5,$5,2
movn	$10,$13,$16
slt	$13,$9,0
addu	$2,$11,$2
subu	$8,$17,$8
movn	$9,$4,$13
addu	$19,$19,$18
subu	$4,$0,$8
lw	$17,28($sp)
lw	$16,48($sp)
slt	$11,$8,0
movn	$8,$4,$11
addu	$10,$10,$2
addu	$22,$21,$17
lw	$4,24($sp)
subu	$7,$16,$7
lw	$11,32($sp)
addu	$9,$9,$10
lw	$21,52($sp)
subu	$2,$0,$7
addu	$22,$4,$22
lw	$13,56($sp)
addu	$17,$17,$11
addu	$3,$3,$4
addu	$22,$22,$fp
slt	$4,$7,0
addu	$fp,$fp,$17
lw	$17,72($sp)
subu	$6,$21,$6
movn	$7,$2,$4
sra	$5,$5,2
addiu	$19,$19,2
lw	$21,60($sp)
subu	$5,$13,$5
lw	$13,12($sp)
addu	$16,$11,$17
lw	$11,132($sp)
addu	$8,$8,$9
subu	$2,$0,$6
slt	$10,$6,0
sra	$19,$19,2
movn	$6,$2,$10
addiu	$3,$3,2
addu	$2,$25,$16
lw	$10,68($sp)
addu	$7,$7,$8
lw	$8,76($sp)
subu	$9,$0,$5
lw	$16,136($sp)
addu	$fp,$fp,$25
lw	$25,8($sp)
subu	$19,$21,$19
slt	$4,$5,0
sra	$3,$3,2
movn	$5,$9,$4
addiu	$22,$22,2
lw	$9,64($sp)
subu	$3,$25,$3
subu	$4,$0,$19
addu	$2,$2,$8
sra	$22,$22,2
addiu	$fp,$fp,2
slt	$8,$19,0
subu	$22,$9,$22
movn	$19,$4,$8
addu	$6,$6,$7
sra	$fp,$fp,2
subu	$7,$0,$3
addiu	$2,$2,2
slt	$8,$3,0
subu	$4,$10,$fp
movn	$3,$7,$8
addu	$5,$5,$6
sra	$2,$2,2
subu	$6,$0,$22
slt	$7,$22,0
subu	$2,$23,$2
movn	$22,$6,$7
addu	$19,$19,$5
subu	$5,$0,$4
slt	$6,$4,0
movn	$4,$5,$6
addu	$3,$3,$19
subu	$5,$0,$2
slt	$6,$2,0
addu	$23,$22,$3
movn	$2,$5,$6
addu	$14,$14,$11
addu	$4,$4,$23
addu	$15,$15,$11
addu	$2,$2,$4
addu	$12,$12,$11
bne	$13,$16,$L856
sw	$2,8($sp)

$L839:
lw	$2,8($sp)
lw	$fp,116($sp)
lw	$23,112($sp)
lw	$22,108($sp)
lw	$21,104($sp)
lw	$20,100($sp)
lw	$19,96($sp)
lw	$18,92($sp)
lw	$17,88($sp)
lw	$16,84($sp)
j	$31
addiu	$sp,$sp,120

$L857:
.option	pic0
j	$L839
.option	pic2
sw	$0,8($sp)

.set	macro
.set	reorder
.end	pix_abs16_xy2_c
.size	pix_abs16_xy2_c, .-pix_abs16_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs8_c
.type	pix_abs8_c, @function
pix_abs8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
blez	$14,$L871
move	$13,$0

move	$2,$0
$L870:
lbu	$12,0($5)
addiu	$13,$13,1
lbu	$3,0($6)
lbu	$8,1($6)
lbu	$4,1($5)
subu	$3,$12,$3
lbu	$24,2($6)
lbu	$12,2($5)
subu	$4,$4,$8
lbu	$9,3($6)
subu	$8,$0,$3
lbu	$11,3($5)
slt	$15,$3,0
lbu	$25,4($6)
subu	$12,$12,$24
movn	$3,$8,$15
subu	$8,$0,$4
lbu	$10,4($5)
slt	$15,$4,0
subu	$11,$11,$9
movn	$4,$8,$15
subu	$8,$0,$12
lbu	$24,5($6)
slt	$15,$12,0
lbu	$9,5($5)
subu	$10,$10,$25
movn	$12,$8,$15
addu	$2,$3,$2
subu	$3,$0,$11
lbu	$25,6($5)
slt	$15,$11,0
lbu	$8,6($6)
subu	$9,$9,$24
movn	$11,$3,$15
addu	$2,$4,$2
subu	$15,$0,$10
lbu	$4,7($5)
slt	$24,$10,0
lbu	$3,7($6)
subu	$8,$25,$8
movn	$10,$15,$24
addu	$2,$12,$2
subu	$12,$0,$9
slt	$15,$9,0
subu	$3,$4,$3
movn	$9,$12,$15
addu	$2,$11,$2
subu	$4,$0,$8
slt	$11,$8,0
addu	$2,$10,$2
movn	$8,$4,$11
subu	$4,$0,$3
slt	$10,$3,0
addu	$2,$9,$2
movn	$3,$4,$10
addu	$5,$5,$7
addu	$2,$8,$2
addu	$6,$6,$7
bne	$13,$14,$L870
addu	$2,$3,$2

j	$31
nop

$L871:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	pix_abs8_c
.size	pix_abs8_c, .-pix_abs8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs8_x2_c
.type	pix_abs8_x2_c, @function
pix_abs8_x2_c:
.frame	$sp,32,$31		# vars= 0, regs= 7/0, args= 0, gp= 0
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-32
sw	$22,28($sp)
sw	$21,24($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
lw	$11,48($sp)
blez	$11,$L884
nop

move	$12,$0
move	$2,$0
$L883:
lbu	$10,1($6)
addiu	$12,$12,1
lbu	$13,0($6)
lbu	$8,2($6)
lbu	$16,3($6)
addu	$13,$13,$10
lbu	$20,4($6)
addu	$10,$10,$8
lbu	$24,0($5)
addiu	$13,$13,1
lbu	$9,5($6)
addu	$8,$8,$16
lbu	$18,1($5)
sra	$13,$13,1
lbu	$4,6($6)
addiu	$10,$10,1
lbu	$15,2($5)
subu	$24,$24,$13
lbu	$17,7($6)
addu	$14,$16,$20
lbu	$3,3($5)
sra	$10,$10,1
lbu	$21,4($5)
addiu	$8,$8,1
lbu	$19,8($6)
subu	$10,$18,$10
lbu	$18,5($5)
addu	$13,$20,$9
lbu	$16,6($5)
sra	$8,$8,1
lbu	$25,7($5)
subu	$22,$0,$24
addiu	$14,$14,1
slt	$20,$24,0
subu	$15,$15,$8
movn	$24,$22,$20
subu	$8,$0,$10
addu	$9,$9,$4
sra	$14,$14,1
addiu	$13,$13,1
slt	$20,$10,0
subu	$14,$3,$14
movn	$10,$8,$20
subu	$3,$0,$15
addu	$8,$4,$17
sra	$13,$13,1
addiu	$9,$9,1
slt	$4,$15,0
subu	$13,$21,$13
movn	$15,$3,$4
sra	$9,$9,1
addu	$3,$24,$2
addu	$4,$17,$19
subu	$2,$0,$14
addiu	$8,$8,1
slt	$17,$14,0
subu	$9,$18,$9
movn	$14,$2,$17
addu	$10,$10,$3
subu	$2,$0,$13
sra	$8,$8,1
addiu	$4,$4,1
slt	$17,$13,0
subu	$3,$16,$8
movn	$13,$2,$17
addu	$8,$15,$10
subu	$2,$0,$9
sra	$4,$4,1
slt	$10,$9,0
subu	$4,$25,$4
movn	$9,$2,$10
addu	$14,$14,$8
subu	$2,$0,$3
slt	$8,$3,0
movn	$3,$2,$8
addu	$13,$13,$14
subu	$2,$0,$4
slt	$8,$4,0
addu	$9,$9,$13
movn	$4,$2,$8
addu	$5,$5,$7
addu	$3,$3,$9
addu	$6,$6,$7
bne	$12,$11,$L883
addu	$2,$4,$3

$L874:
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,32

$L884:
.option	pic0
j	$L874
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	pix_abs8_x2_c
.size	pix_abs8_x2_c, .-pix_abs8_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs8_y2_c
.type	pix_abs8_y2_c, @function
pix_abs8_y2_c:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$fp,36($sp)
sw	$23,32($sp)
sw	$22,28($sp)
sw	$21,24($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
lw	$14,56($sp)
blez	$14,$L898
addu	$8,$6,$7

move	$15,$0
move	$17,$0
$L897:
lbu	$11,0($6)
addiu	$15,$15,1
lbu	$13,0($8)
lbu	$2,1($8)
lbu	$12,1($6)
addu	$13,$11,$13
lbu	$9,2($8)
lbu	$11,2($6)
addu	$12,$12,$2
lbu	$16,3($6)
addiu	$13,$13,1
lbu	$10,0($5)
lbu	$18,3($8)
addu	$11,$11,$9
sra	$13,$13,1
lbu	$20,4($8)
addiu	$12,$12,1
lbu	$19,1($5)
lbu	$22,4($6)
subu	$10,$10,$13
sra	$12,$12,1
lbu	$9,2($5)
addu	$18,$16,$18
lbu	$25,5($8)
addiu	$11,$11,1
lbu	$13,5($6)
subu	$19,$19,$12
lbu	$24,6($8)
subu	$3,$0,$10
lbu	$fp,3($5)
addiu	$18,$18,1
lbu	$12,6($6)
addu	$22,$22,$20
lbu	$23,4($5)
sra	$11,$11,1
lbu	$4,7($6)
slt	$2,$10,0
lbu	$21,7($8)
subu	$11,$9,$11
movn	$10,$3,$2
sra	$9,$18,1
subu	$2,$0,$19
lbu	$3,5($5)
addiu	$22,$22,1
lbu	$20,6($5)
addu	$13,$13,$25
lbu	$18,7($5)
slt	$25,$19,0
subu	$16,$fp,$9
movn	$19,$2,$25
subu	$9,$0,$11
sra	$2,$22,1
addu	$12,$12,$24
addiu	$13,$13,1
slt	$22,$11,0
subu	$24,$23,$2
movn	$11,$9,$22
addu	$10,$10,$17
subu	$2,$0,$16
sra	$13,$13,1
addiu	$12,$12,1
addu	$4,$4,$21
slt	$9,$16,0
subu	$13,$3,$13
movn	$16,$2,$9
addu	$10,$19,$10
subu	$2,$0,$24
sra	$12,$12,1
addiu	$4,$4,1
slt	$9,$24,0
subu	$3,$20,$12
movn	$24,$2,$9
sra	$4,$4,1
addu	$9,$11,$10
subu	$2,$0,$13
slt	$10,$13,0
subu	$4,$18,$4
movn	$13,$2,$10
addu	$9,$16,$9
subu	$10,$0,$3
slt	$11,$3,0
addu	$2,$24,$9
movn	$3,$10,$11
subu	$9,$0,$4
slt	$10,$4,0
addu	$2,$13,$2
movn	$4,$9,$10
addu	$5,$5,$7
addu	$3,$3,$2
addu	$6,$6,$7
addu	$17,$4,$3
bne	$15,$14,$L897
addu	$8,$8,$7

$L888:
move	$2,$17
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

$L898:
.option	pic0
j	$L888
.option	pic2
move	$17,$0

.set	macro
.set	reorder
.end	pix_abs8_y2_c
.size	pix_abs8_y2_c, .-pix_abs8_y2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	pix_abs8_xy2_c
.type	pix_abs8_xy2_c, @function
pix_abs8_xy2_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-56
sw	$fp,52($sp)
sw	$23,48($sp)
sw	$22,44($sp)
sw	$21,40($sp)
sw	$20,36($sp)
sw	$19,32($sp)
sw	$18,28($sp)
sw	$17,24($sp)
sw	$16,20($sp)
lw	$2,72($sp)
blez	$2,$L912
addu	$11,$6,$7

move	$24,$0
move	$2,$0
$L911:
lbu	$4,1($6)
addiu	$24,$24,1
lbu	$12,2($6)
lbu	$15,1($11)
lbu	$3,0($6)
addu	$14,$4,$12
lbu	$9,2($11)
lbu	$10,0($11)
lbu	$13,3($6)
addu	$3,$3,$4
addu	$14,$15,$14
lbu	$8,4($6)
addu	$10,$3,$10
lbu	$20,3($11)
addu	$14,$14,$9
lbu	$4,5($6)
addu	$12,$12,$13
lbu	$23,1($5)
addu	$10,$10,$15
lbu	$18,4($11)
addiu	$14,$14,2
lbu	$3,6($6)
addu	$12,$9,$12
lbu	$19,0($5)
addu	$9,$13,$8
lbu	$fp,5($11)
addiu	$10,$10,2
lbu	$16,7($6)
sra	$14,$14,2
lbu	$17,6($11)
addu	$9,$20,$9
lbu	$13,2($5)
addu	$8,$8,$4
lbu	$22,3($5)
subu	$14,$23,$14
lbu	$23,7($5)
sra	$15,$10,2
lbu	$10,8($6)
addu	$12,$12,$20
lbu	$25,7($11)
addu	$9,$9,$18
lbu	$21,4($5)
addu	$8,$18,$8
lbu	$20,8($11)
subu	$15,$19,$15
sw	$23,8($sp)
addu	$4,$4,$3
lbu	$19,5($5)
addiu	$12,$12,2
lbu	$18,6($5)
addu	$8,$8,$fp
addiu	$9,$9,2
subu	$23,$0,$15
addu	$4,$fp,$4
sra	$12,$12,2
addu	$3,$3,$16
slt	$fp,$15,0
sra	$9,$9,2
movn	$15,$23,$fp
addiu	$8,$8,2
subu	$12,$13,$12
subu	$23,$0,$14
addu	$10,$16,$10
addu	$4,$4,$17
addu	$3,$17,$3
subu	$9,$22,$9
slt	$13,$14,0
sra	$8,$8,2
movn	$14,$23,$13
addiu	$4,$4,2
addu	$3,$3,$25
lw	$23,72($sp)
addu	$25,$25,$10
subu	$13,$0,$12
subu	$21,$21,$8
slt	$10,$12,0
subu	$8,$0,$9
movn	$12,$13,$10
addu	$15,$15,$2
addiu	$3,$3,2
sra	$2,$4,2
addu	$20,$25,$20
slt	$4,$9,0
movn	$9,$8,$4
subu	$4,$0,$21
subu	$2,$19,$2
addu	$14,$14,$15
sra	$3,$3,2
slt	$8,$21,0
addiu	$15,$20,2
movn	$21,$4,$8
subu	$18,$18,$3
lw	$8,8($sp)
subu	$3,$0,$2
addu	$12,$12,$14
sra	$15,$15,2
slt	$4,$2,0
subu	$15,$8,$15
movn	$2,$3,$4
addu	$12,$9,$12
subu	$3,$0,$18
slt	$4,$18,0
movn	$18,$3,$4
addu	$21,$21,$12
subu	$3,$0,$15
slt	$4,$15,0
addu	$2,$2,$21
movn	$15,$3,$4
addu	$5,$5,$7
addu	$18,$18,$2
addu	$6,$6,$7
addu	$2,$15,$18
bne	$24,$23,$L911
addu	$11,$11,$7

$L902:
lw	$fp,52($sp)
lw	$23,48($sp)
lw	$22,44($sp)
lw	$21,40($sp)
lw	$20,36($sp)
lw	$19,32($sp)
lw	$18,28($sp)
lw	$17,24($sp)
lw	$16,20($sp)
j	$31
addiu	$sp,$sp,56

$L912:
.option	pic0
j	$L902
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	pix_abs8_xy2_c
.size	pix_abs8_xy2_c, .-pix_abs8_xy2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	nsse16_c
.type	nsse16_c, @function
nsse16_c:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-24
sw	$16,4($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
lw	$16,40($sp)
blez	$16,$L930
mtlo	$0

addu	$25,$5,$7
addu	$24,$6,$7
move	$18,$0
move	$15,$0
li	$17,15			# 0xf
$L923:
lbu	$11,0($5)
addiu	$18,$18,1
lbu	$12,0($6)
lbu	$2,1($5)
lbu	$20,1($6)
lbu	$8,2($5)
subu	$3,$11,$12
lbu	$10,3($5)
lbu	$19,2($6)
subu	$20,$2,$20
lbu	$14,3($6)
madd	$3,$3
lbu	$9,4($5)
madd	$20,$20
lbu	$13,4($6)
subu	$19,$8,$19
subu	$14,$10,$14
lbu	$8,5($5)
lbu	$10,5($6)
madd	$19,$19
subu	$13,$9,$13
lbu	$2,6($5)
lbu	$9,6($6)
madd	$14,$14
subu	$10,$8,$10
lbu	$3,7($5)
lbu	$14,7($6)
madd	$13,$13
lbu	$8,8($5)
subu	$9,$2,$9
lbu	$13,8($6)
madd	$10,$10
lbu	$2,9($5)
subu	$14,$3,$14
lbu	$10,9($6)
madd	$9,$9
subu	$13,$8,$13
lbu	$3,10($5)
lbu	$9,10($6)
madd	$14,$14
subu	$10,$2,$10
lbu	$8,11($5)
madd	$13,$13
lbu	$14,11($6)
subu	$9,$3,$9
lbu	$2,12($5)
madd	$10,$10
lbu	$3,13($5)
lbu	$13,12($6)
subu	$14,$8,$14
lbu	$10,13($6)
madd	$9,$9
lbu	$8,14($6)
madd	$14,$14
lbu	$9,14($5)
subu	$13,$2,$13
subu	$10,$3,$10
lbu	$2,15($5)
lbu	$3,15($6)
madd	$13,$13
subu	$8,$9,$8
madd	$10,$10
subu	$3,$2,$3
madd	$8,$8
beq	$18,$16,$L920
madd	$3,$3

lbu	$14,0($24)
move	$9,$0
lbu	$13,0($25)
$L918:
addu	$8,$5,$9
addiu	$9,$9,1
move	$20,$11
addu	$3,$6,$9
lbu	$11,1($8)
addu	$2,$25,$9
addu	$10,$24,$9
move	$19,$12
lbu	$12,0($3)
move	$8,$13
lbu	$13,0($2)
move	$3,$14
lbu	$14,0($10)
subu	$2,$20,$8
subu	$10,$19,$3
subu	$8,$8,$20
subu	$3,$3,$19
subu	$2,$2,$11
subu	$10,$10,$12
addu	$8,$11,$8
addu	$3,$12,$3
addu	$2,$2,$13
addu	$10,$10,$14
subu	$8,$8,$13
subu	$3,$3,$14
slt	$19,$2,0
slt	$20,$10,0
movz	$8,$2,$19
movz	$3,$10,$20
subu	$3,$8,$3
bne	$9,$17,$L918
addu	$15,$15,$3

addu	$5,$5,$7
addu	$6,$6,$7
addu	$25,$25,$7
.option	pic0
j	$L923
.option	pic2
addu	$24,$24,$7

$L930:
move	$15,$0
$L917:
beq	$4,$0,$L921
lw	$20,20($sp)

lw	$2,0($4)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$2,636($2)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,24
madd	$15,$2
j	$31
mflo	$2

$L921:
mflo	$3
sll	$2,$15,3
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,24
j	$31
addu	$2,$2,$3

$L920:
subu	$3,$0,$15
slt	$2,$15,0
.option	pic0
j	$L917
.option	pic2
movn	$15,$3,$2

.set	macro
.set	reorder
.end	nsse16_c
.size	nsse16_c, .-nsse16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	nsse8_c
.type	nsse8_c, @function
nsse8_c:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-24
sw	$16,4($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
lw	$16,40($sp)
blez	$16,$L946
mtlo	$0

addu	$25,$5,$7
addu	$24,$6,$7
move	$18,$0
move	$15,$0
li	$17,7			# 0x7
$L939:
lbu	$11,0($5)
addiu	$18,$18,1
lbu	$12,0($6)
lbu	$2,1($5)
lbu	$14,1($6)
lbu	$13,2($5)
subu	$3,$11,$12
lbu	$10,2($6)
lbu	$9,3($5)
subu	$14,$2,$14
lbu	$20,3($6)
madd	$3,$3
subu	$10,$13,$10
lbu	$8,4($5)
lbu	$19,4($6)
madd	$14,$14
lbu	$13,5($6)
subu	$20,$9,$20
lbu	$14,5($5)
madd	$10,$10
lbu	$9,6($6)
subu	$19,$8,$19
lbu	$10,6($5)
madd	$20,$20
lbu	$8,7($5)
subu	$13,$14,$13
lbu	$3,7($6)
madd	$19,$19
subu	$9,$10,$9
madd	$13,$13
subu	$3,$8,$3
madd	$9,$9
beq	$18,$16,$L936
madd	$3,$3

lbu	$14,0($24)
move	$9,$0
lbu	$13,0($25)
$L934:
addu	$8,$5,$9
addiu	$9,$9,1
move	$20,$11
addu	$3,$6,$9
lbu	$11,1($8)
addu	$2,$25,$9
addu	$10,$24,$9
move	$19,$12
lbu	$12,0($3)
move	$8,$13
lbu	$13,0($2)
move	$3,$14
lbu	$14,0($10)
subu	$2,$20,$8
subu	$10,$19,$3
subu	$8,$8,$20
subu	$3,$3,$19
subu	$2,$2,$11
subu	$10,$10,$12
addu	$8,$11,$8
addu	$3,$12,$3
addu	$2,$2,$13
addu	$10,$10,$14
subu	$8,$8,$13
subu	$3,$3,$14
slt	$19,$2,0
slt	$20,$10,0
movz	$8,$2,$19
movz	$3,$10,$20
subu	$3,$8,$3
bne	$9,$17,$L934
addu	$15,$15,$3

addu	$5,$5,$7
addu	$6,$6,$7
addu	$25,$25,$7
.option	pic0
j	$L939
.option	pic2
addu	$24,$24,$7

$L946:
move	$15,$0
$L933:
beq	$4,$0,$L937
lw	$20,20($sp)

lw	$2,0($4)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$2,636($2)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,24
madd	$15,$2
j	$31
mflo	$2

$L937:
mflo	$3
sll	$2,$15,3
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,24
j	$31
addu	$2,$2,$3

$L936:
subu	$3,$0,$15
slt	$2,$15,0
.option	pic0
j	$L933
.option	pic2
movn	$15,$3,$2

.set	macro
.set	reorder
.end	nsse8_c
.size	nsse8_c, .-nsse8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	try_8x8basis_c
.type	try_8x8basis_c, @function
try_8x8basis_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
move	$9,$0
move	$2,$0
li	$11,128			# 0x80
$L948:
addu	$3,$6,$9
addu	$10,$4,$9
addu	$8,$5,$9
lh	$3,0($3)
addiu	$9,$9,2
lh	$10,0($10)
lh	$8,0($8)
mul	$3,$3,$7
addiu	$3,$3,512
sra	$3,$3,10
addu	$3,$10,$3
sra	$3,$3,6
mul	$3,$8,$3
mul	$3,$3,$3
sra	$3,$3,4
bne	$9,$11,$L948
addu	$2,$2,$3

j	$31
srl	$2,$2,2

.set	macro
.set	reorder
.end	try_8x8basis_c
.size	try_8x8basis_c, .-try_8x8basis_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_8x8basis_c
.type	add_8x8basis_c, @function
add_8x8basis_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$7,$4,128
$L951:
lh	$2,0($5)
addiu	$4,$4,2
lhu	$3,-2($4)
addiu	$5,$5,2
mul	$2,$2,$6
addiu	$2,$2,512
sra	$2,$2,10
addu	$2,$2,$3
bne	$4,$7,$L951
sh	$2,-2($4)

j	$31
nop

.set	macro
.set	reorder
.end	add_8x8basis_c
.size	add_8x8basis_c, .-add_8x8basis_c
.align	2
.set	nomips16
.set	nomicromips
.ent	zero_cmp
.type	zero_cmp, @function
zero_cmp:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	zero_cmp
.size	zero_cmp, .-zero_cmp
.align	2
.set	nomips16
.set	nomicromips
.ent	add_bytes_c
.type	add_bytes_c, @function
add_bytes_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$10,2139029504			# 0x7f7f0000
li	$13,-2139095040			# 0xffffffff80800000
addiu	$14,$6,-4
move	$3,$0
addiu	$10,$10,32639
move	$7,$4
ori	$13,$13,0x8080
$L955:
addu	$8,$5,$3
lw	$2,0($7)
addiu	$3,$3,4
addiu	$7,$7,4
lw	$9,0($8)
and	$8,$2,$10
sltu	$11,$14,$3
and	$12,$9,$10
xor	$2,$2,$9
addu	$8,$8,$12
and	$2,$2,$13
xor	$2,$2,$8
beq	$11,$0,$L955
sw	$2,-4($7)

slt	$2,$3,$6
beq	$2,$0,$L978
move	$10,$3

addiu	$7,$3,4
addu	$2,$4,$3
addu	$9,$5,$7
addu	$8,$5,$3
addu	$7,$4,$7
subu	$12,$6,$3
sltu	$9,$2,$9
sltu	$7,$8,$7
xori	$9,$9,0x1
xori	$7,$7,0x1
sltu	$11,$12,7
or	$7,$9,$7
xori	$9,$11,0x1
and	$7,$9,$7
beq	$7,$0,$L957
or	$7,$8,$2

andi	$7,$7,0x3
bne	$7,$0,$L957
addiu	$9,$12,-4

addiu	$7,$6,-1
srl	$9,$9,2
subu	$7,$7,$3
addiu	$sp,$sp,-8
addiu	$9,$9,1
sltu	$7,$7,3
sw	$17,4($sp)
sw	$16,0($sp)
bne	$7,$0,$L958
sll	$25,$9,2

li	$14,2139029504			# 0x7f7f0000
li	$24,-2139095040			# 0xffffffff80800000
move	$13,$0
addiu	$14,$14,32639
move	$11,$2
ori	$24,$24,0x8080
$L959:
lw	$15,0($8)
addiu	$13,$13,1
lw	$7,0($2)
addiu	$8,$8,4
sltu	$16,$13,$9
and	$10,$15,$14
and	$17,$7,$14
addu	$10,$10,$17
xor	$7,$7,$15
and	$7,$7,$24
xor	$7,$10,$7
addiu	$2,$2,4
sw	$7,0($11)
bne	$16,$0,$L959
addiu	$11,$11,4

beq	$12,$25,$L954
addu	$10,$3,$25

$L958:
addu	$7,$4,$10
addu	$3,$5,$10
addiu	$2,$10,1
lbu	$9,0($7)
lbu	$3,0($3)
slt	$8,$2,$6
addu	$3,$3,$9
beq	$8,$0,$L954
sb	$3,0($7)

addu	$3,$4,$2
addu	$2,$5,$2
addiu	$10,$10,2
lbu	$7,0($3)
lbu	$2,0($2)
slt	$6,$10,$6
addu	$2,$2,$7
beq	$6,$0,$L954
sb	$2,0($3)

addu	$4,$4,$10
addu	$10,$5,$10
lbu	$3,0($4)
lbu	$2,0($10)
addu	$2,$2,$3
sb	$2,0($4)
$L954:
lw	$17,4($sp)
lw	$16,0($sp)
addiu	$sp,$sp,8
$L978:
j	$31
nop

$L957:
addu	$6,$4,$6
move	$3,$8
$L964:
addiu	$2,$2,1
lbu	$5,0($3)
lbu	$4,-1($2)
addiu	$3,$3,1
addu	$5,$5,$4
bne	$2,$6,$L964
sb	$5,-1($2)

j	$31
nop

.set	macro
.set	reorder
.end	add_bytes_c
.size	add_bytes_c, .-add_bytes_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_bytes_l2_c
.type	add_bytes_l2_c, @function
add_bytes_l2_c:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$10,2139029504			# 0x7f7f0000
li	$14,-2139095040			# 0xffffffff80800000
addiu	$15,$7,-4
move	$3,$0
addiu	$10,$10,32639
ori	$14,$14,0x8080
$L981:
addu	$8,$6,$3
addu	$9,$5,$3
addu	$12,$4,$3
lw	$2,0($8)
addiu	$3,$3,4
lw	$9,0($9)
sltu	$11,$15,$3
and	$8,$2,$10
and	$13,$9,$10
xor	$2,$2,$9
addu	$8,$8,$13
and	$2,$2,$14
xor	$2,$2,$8
beq	$11,$0,$L981
sw	$2,0($12)

slt	$8,$3,$7
beq	$8,$0,$L1004
move	$2,$3

addiu	$8,$3,4
addu	$12,$4,$3
addu	$9,$4,$8
addu	$10,$5,$8
addu	$14,$5,$3
addu	$13,$6,$3
addu	$8,$6,$8
sltu	$10,$12,$10
sltu	$25,$14,$9
sltu	$8,$12,$8
sltu	$24,$13,$9
subu	$15,$7,$3
xori	$11,$10,0x1
xori	$9,$8,0x1
xori	$10,$25,0x1
xori	$24,$24,0x1
or	$8,$9,$24
or	$10,$11,$10
sltu	$9,$15,7
and	$8,$10,$8
xori	$9,$9,0x1
and	$8,$9,$8
beq	$8,$0,$L990
or	$8,$13,$14

or	$8,$12,$8
andi	$8,$8,0x3
bne	$8,$0,$L990
addiu	$8,$15,-4

addiu	$9,$7,-1
srl	$8,$8,2
subu	$9,$9,$3
addiu	$sp,$sp,-16
addiu	$8,$8,1
sltu	$9,$9,3
sw	$18,12($sp)
sw	$17,8($sp)
sll	$25,$8,2
bne	$9,$0,$L984
sw	$16,4($sp)

li	$24,2139029504			# 0x7f7f0000
li	$16,-2139095040			# 0xffffffff80800000
move	$11,$0
addiu	$24,$24,32639
move	$9,$13
ori	$16,$16,0x8080
$L985:
lw	$13,0($9)
addiu	$11,$11,1
lw	$2,0($14)
addiu	$9,$9,4
sltu	$17,$11,$8
and	$10,$13,$24
and	$18,$2,$24
addu	$10,$10,$18
xor	$2,$2,$13
and	$2,$2,$16
xor	$2,$10,$2
addiu	$14,$14,4
sw	$2,0($12)
bne	$17,$0,$L985
addiu	$12,$12,4

beq	$15,$25,$L980
addu	$2,$3,$25

$L984:
addu	$9,$5,$2
addu	$8,$6,$2
addiu	$3,$2,1
lbu	$11,0($9)
addu	$10,$4,$2
lbu	$8,0($8)
slt	$9,$3,$7
addu	$8,$8,$11
beq	$9,$0,$L980
sb	$8,0($10)

addu	$8,$6,$3
addu	$9,$5,$3
addiu	$2,$2,2
lbu	$8,0($8)
addu	$3,$4,$3
lbu	$9,0($9)
slt	$7,$2,$7
addu	$8,$8,$9
beq	$7,$0,$L980
sb	$8,0($3)

addu	$6,$6,$2
addu	$5,$5,$2
addu	$2,$4,$2
lbu	$3,0($6)
lbu	$4,0($5)
addu	$3,$3,$4
sb	$3,0($2)
$L980:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
addiu	$sp,$sp,16
$L1004:
j	$31
nop

$L990:
addu	$3,$6,$2
addu	$9,$5,$2
addu	$8,$4,$2
lbu	$3,0($3)
addiu	$2,$2,1
lbu	$9,0($9)
addu	$3,$3,$9
bne	$2,$7,$L990
sb	$3,0($8)

j	$31
nop

.set	macro
.set	reorder
.end	add_bytes_l2_c
.size	add_bytes_l2_c, .-add_bytes_l2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	diff_bytes_c
.type	diff_bytes_c, @function
diff_bytes_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
andi	$3,$6,0x3
bne	$3,$0,$L1007
slt	$2,$7,8

li	$13,2139029504			# 0x7f7f0000
li	$10,-2139095040			# 0xffffffff80800000
addiu	$14,$7,-4
addiu	$13,$13,32639
ori	$10,$10,0x8080
$L1008:
addu	$2,$5,$3
addu	$8,$6,$3
addu	$12,$4,$3
lw	$2,0($2)
addiu	$3,$3,4
lw	$8,0($8)
sltu	$11,$14,$3
or	$9,$2,$10
xor	$2,$8,$2
and	$8,$8,$13
nor	$2,$0,$2
subu	$8,$9,$8
and	$2,$2,$10
xor	$2,$2,$8
beq	$11,$0,$L1008
sw	$2,0($12)

$L1009:
slt	$2,$3,$7
beq	$2,$0,$L1038
subu	$14,$7,$3

addiu	$2,$3,4
addu	$9,$4,$3
addu	$8,$4,$2
addu	$12,$5,$2
addu	$11,$5,$3
addu	$10,$6,$3
addu	$2,$6,$2
sltu	$12,$9,$12
sltu	$24,$11,$8
sltu	$2,$9,$2
sltu	$15,$10,$8
xori	$13,$12,0x1
xori	$8,$2,0x1
xori	$12,$24,0x1
xori	$15,$15,0x1
or	$2,$8,$15
or	$12,$13,$12
sltu	$8,$14,7
and	$2,$12,$2
xori	$8,$8,0x1
and	$2,$8,$2
beq	$2,$0,$L1019
or	$2,$10,$11

or	$2,$9,$2
andi	$2,$2,0x3
bne	$2,$0,$L1019
addiu	$8,$14,-4

addiu	$2,$7,-1
srl	$8,$8,2
subu	$2,$2,$3
addiu	$sp,$sp,-8
addiu	$8,$8,1
sltu	$2,$2,3
sw	$17,4($sp)
sw	$16,0($sp)
bne	$2,$0,$L1013
sll	$24,$8,2

li	$25,2139029504			# 0x7f7f0000
li	$16,-2139095040			# 0xffffffff80800000
move	$15,$0
addiu	$25,$25,32639
ori	$16,$16,0x8080
$L1014:
lw	$2,0($11)
addiu	$15,$15,1
lw	$12,0($10)
addiu	$11,$11,4
sltu	$17,$15,$8
or	$13,$2,$16
xor	$2,$12,$2
and	$12,$12,$25
nor	$2,$0,$2
subu	$12,$13,$12
and	$2,$2,$16
xor	$2,$2,$12
addiu	$10,$10,4
sw	$2,0($9)
bne	$17,$0,$L1014
addiu	$9,$9,4

beq	$14,$24,$L1006
addu	$3,$3,$24

addu	$11,$5,$3
addu	$10,$6,$3
addu	$9,$4,$3
$L1013:
lbu	$8,0($11)
addiu	$2,$3,1
lbu	$11,0($10)
slt	$10,$2,$7
subu	$8,$8,$11
beq	$10,$0,$L1006
sb	$8,0($9)

addu	$8,$5,$2
addu	$9,$6,$2
addiu	$3,$3,2
lbu	$8,0($8)
addu	$2,$4,$2
lbu	$9,0($9)
slt	$7,$3,$7
subu	$8,$8,$9
beq	$7,$0,$L1006
sb	$8,0($2)

addu	$5,$5,$3
addu	$6,$6,$3
addu	$3,$4,$3
lbu	$2,0($5)
lbu	$4,0($6)
subu	$2,$2,$4
sb	$2,0($3)
$L1006:
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

$L1019:
lbu	$2,0($11)
addiu	$3,$3,1
lbu	$8,0($10)
addu	$11,$5,$3
addu	$10,$6,$3
subu	$2,$2,$8
sb	$2,0($9)
bne	$3,$7,$L1019
addu	$9,$4,$3

$L1038:
j	$31
nop

$L1007:
bne	$2,$0,$L1021
addiu	$2,$7,-8

addiu	$8,$5,1
srl	$2,$2,3
move	$10,$4
sll	$12,$2,3
move	$9,$6
addiu	$12,$12,9
addu	$12,$5,$12
$L1010:
lbu	$3,-1($8)
addiu	$10,$10,8
lbu	$11,0($9)
addiu	$8,$8,8
addiu	$9,$9,8
subu	$3,$3,$11
sb	$3,-8($10)
lbu	$11,-7($9)
lbu	$3,-8($8)
subu	$3,$3,$11
sb	$3,-7($10)
lbu	$11,-6($9)
lbu	$3,-7($8)
subu	$3,$3,$11
sb	$3,-6($10)
lbu	$11,-5($9)
lbu	$3,-6($8)
subu	$3,$3,$11
sb	$3,-5($10)
lbu	$11,-4($9)
lbu	$3,-5($8)
subu	$3,$3,$11
sb	$3,-4($10)
lbu	$3,-4($8)
lbu	$11,-3($9)
subu	$11,$3,$11
sb	$11,-3($10)
lbu	$3,-2($9)
lbu	$11,-3($8)
subu	$11,$11,$3
sb	$11,-2($10)
lbu	$11,-2($8)
lbu	$3,-1($9)
subu	$11,$11,$3
bne	$8,$12,$L1010
sb	$11,-1($10)

addiu	$3,$2,1
.option	pic0
j	$L1009
.option	pic2
sll	$3,$3,3

$L1021:
.option	pic0
j	$L1009
.option	pic2
move	$3,$0

.set	macro
.set	reorder
.end	diff_bytes_c
.size	diff_bytes_c, .-diff_bytes_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_hfyu_median_prediction_c
.type	add_hfyu_median_prediction_c, @function
add_hfyu_median_prediction_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$12,20($sp)
move	$8,$0
lw	$13,16($sp)
lw	$9,0($12)
lbu	$2,0($13)
andi	$10,$9,0x00ff
blez	$7,$L1041
andi	$9,$9,0xff

$L1047:
addu	$3,$5,$8
lbu	$9,0($3)
addu	$3,$2,$9
slt	$11,$9,$2
subu	$3,$3,$10
beq	$11,$0,$L1042
andi	$3,$3,0xff

slt	$10,$9,$3
bne	$10,$0,$L1049
slt	$11,$2,$3

$L1046:
move	$2,$9
$L1043:
addu	$10,$6,$8
addu	$3,$4,$8
addiu	$8,$8,1
lbu	$11,0($10)
move	$10,$9
addu	$2,$2,$11
andi	$2,$2,0x00ff
bne	$8,$7,$L1047
sb	$2,0($3)

$L1041:
sw	$2,0($13)
j	$31
sw	$9,0($12)

$L1042:
slt	$10,$3,$9
beq	$10,$0,$L1046
slt	$11,$3,$2

$L1049:
.option	pic0
j	$L1043
.option	pic2
movz	$2,$3,$11

.set	macro
.set	reorder
.end	add_hfyu_median_prediction_c
.size	add_hfyu_median_prediction_c, .-add_hfyu_median_prediction_c
.align	2
.set	nomips16
.set	nomicromips
.ent	sub_hfyu_median_prediction_c
.type	sub_hfyu_median_prediction_c, @function
sub_hfyu_median_prediction_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$12,20($sp)
move	$8,$0
lw	$13,16($sp)
lw	$3,0($12)
lbu	$10,0($13)
andi	$11,$3,0x00ff
blez	$7,$L1052
andi	$9,$3,0xff

$L1058:
addu	$2,$5,$8
lbu	$9,0($2)
addu	$2,$10,$9
slt	$3,$9,$10
subu	$2,$2,$11
beq	$3,$0,$L1053
andi	$2,$2,0xff

slt	$11,$9,$2
bne	$11,$0,$L1060
slt	$3,$10,$2

$L1057:
move	$3,$9
$L1054:
addu	$10,$6,$8
addu	$2,$4,$8
addiu	$8,$8,1
lbu	$10,0($10)
move	$11,$9
subu	$3,$10,$3
bne	$8,$7,$L1058
sb	$3,0($2)

$L1052:
sw	$10,0($13)
j	$31
sw	$9,0($12)

$L1053:
slt	$11,$2,$9
beq	$11,$0,$L1057
slt	$3,$2,$10

$L1060:
movz	$10,$2,$3
.option	pic0
j	$L1054
.option	pic2
move	$3,$10

.set	macro
.set	reorder
.end	sub_hfyu_median_prediction_c
.size	sub_hfyu_median_prediction_c, .-sub_hfyu_median_prediction_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_hfyu_left_prediction_c
.type	add_hfyu_left_prediction_c, @function
add_hfyu_left_prediction_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
slt	$3,$6,2
bne	$3,$0,$L1066
move	$2,$7

addiu	$3,$6,-2
addiu	$8,$4,1
srl	$3,$3,1
move	$9,$5
sll	$10,$3,1
addiu	$10,$10,3
addu	$10,$4,$10
$L1063:
lbu	$7,0($9)
addiu	$8,$8,2
addiu	$9,$9,2
addu	$2,$7,$2
sb	$2,-3($8)
lbu	$7,-1($9)
addu	$2,$7,$2
bne	$8,$10,$L1063
sb	$2,-2($8)

addiu	$3,$3,1
sll	$3,$3,1
$L1062:
slt	$7,$3,$6
beq	$7,$0,$L1069
nop

$L1065:
addu	$8,$5,$3
addu	$7,$4,$3
addiu	$3,$3,1
lbu	$8,0($8)
addu	$2,$2,$8
bne	$3,$6,$L1065
sb	$2,0($7)

$L1069:
j	$31
nop

$L1066:
.option	pic0
j	$L1062
.option	pic2
move	$3,$0

.set	macro
.set	reorder
.end	add_hfyu_left_prediction_c
.size	add_hfyu_left_prediction_c, .-add_hfyu_left_prediction_c
.align	2
.set	nomips16
.set	nomicromips
.ent	add_hfyu_left_prediction_bgr32_c
.type	add_hfyu_left_prediction_bgr32_c, @function
add_hfyu_left_prediction_bgr32_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$24,16($sp)
lw	$15,20($sp)
lw	$14,24($sp)
lw	$9,0($7)
lw	$8,0($24)
lw	$3,0($15)
blez	$6,$L1071
lw	$2,0($14)

sll	$6,$6,2
addu	$6,$5,$6
$L1072:
lbu	$13,0($5)
addiu	$5,$5,4
addiu	$4,$4,4
addu	$3,$3,$13
lbu	$12,-3($5)
lbu	$11,-2($5)
lbu	$10,-1($5)
addu	$8,$8,$12
sb	$3,-4($4)
addu	$9,$9,$11
addu	$2,$2,$10
sb	$8,-3($4)
sb	$9,-2($4)
bne	$5,$6,$L1072
sb	$2,-1($4)

$L1071:
sw	$9,0($7)
sw	$8,0($24)
sw	$3,0($15)
j	$31
sw	$2,0($14)

.set	macro
.set	reorder
.end	add_hfyu_left_prediction_bgr32_c
.size	add_hfyu_left_prediction_bgr32_c, .-add_hfyu_left_prediction_bgr32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	hadamard8_diff8x8_c
.type	hadamard8_diff8x8_c, @function
hadamard8_diff8x8_c:
.frame	$sp,296,$31		# vars= 256, regs= 7/0, args= 0, gp= 8
.mask	0x007f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-296
move	$15,$0
addiu	$3,$sp,8
sw	$22,292($sp)
li	$24,8			# 0x8
sw	$21,288($sp)
sw	$20,284($sp)
move	$13,$3
sw	$19,280($sp)
sw	$18,276($sp)
sw	$17,272($sp)
sw	$16,268($sp)
$L1075:
lbu	$2,1($6)
addiu	$13,$13,32
lbu	$16,1($5)
addiu	$15,$15,1
lbu	$10,3($6)
lbu	$12,3($5)
lbu	$8,5($6)
subu	$18,$2,$16
lbu	$4,7($5)
subu	$22,$16,$2
lbu	$20,5($5)
subu	$14,$10,$12
lbu	$19,7($6)
subu	$21,$12,$10
lbu	$16,0($6)
subu	$10,$8,$20
lbu	$2,2($6)
subu	$17,$19,$4
lbu	$9,4($6)
lbu	$25,0($5)
subu	$20,$20,$8
lbu	$12,2($5)
subu	$19,$4,$19
lbu	$8,4($5)
lbu	$4,6($5)
subu	$25,$16,$25
lbu	$11,6($6)
subu	$12,$2,$12
subu	$8,$9,$8
addu	$16,$25,$22
subu	$11,$11,$4
addu	$14,$12,$14
addu	$10,$8,$10
addu	$12,$12,$21
addu	$8,$8,$20
addu	$9,$11,$17
addu	$4,$11,$19
addu	$18,$25,$18
subu	$2,$16,$12
addu	$25,$18,$14
subu	$11,$18,$14
addu	$14,$16,$12
addu	$12,$10,$9
subu	$9,$10,$9
addu	$10,$8,$4
subu	$4,$8,$4
addu	$8,$14,$10
addu	$16,$25,$12
subu	$14,$14,$10
subu	$25,$25,$12
sw	$8,-28($13)
addu	$10,$11,$9
sw	$16,-32($13)
addu	$8,$2,$4
sw	$14,-12($13)
subu	$9,$11,$9
sw	$25,-16($13)
subu	$2,$2,$4
sw	$10,-24($13)
addu	$6,$6,$7
sw	$8,-20($13)
sw	$9,-8($13)
addu	$5,$5,$7
bne	$15,$24,$L1075
sw	$2,-4($13)

addiu	$14,$sp,40
move	$2,$0
$L1084:
lw	$10,0($3)
addiu	$3,$3,4
lw	$4,28($3)
lw	$12,60($3)
lw	$9,92($3)
lw	$5,124($3)
subu	$11,$10,$4
lw	$8,156($3)
addu	$13,$10,$4
lw	$6,188($3)
addu	$4,$12,$9
lw	$7,220($3)
subu	$10,$12,$9
subu	$9,$5,$8
addu	$12,$5,$8
addu	$17,$6,$7
subu	$7,$6,$7
addu	$5,$12,$17
addu	$15,$9,$7
addu	$6,$13,$4
addu	$16,$11,$10
sw	$5,124($3)
subu	$13,$13,$4
sw	$15,156($3)
subu	$12,$12,$17
sw	$6,-4($3)
addu	$4,$6,$5
sw	$16,28($3)
subu	$6,$6,$5
sw	$13,60($3)
addu	$5,$16,$15
sw	$12,188($3)
subu	$15,$16,$15
subu	$8,$9,$7
subu	$16,$0,$4
subu	$10,$11,$10
addu	$24,$13,$12
sw	$8,220($3)
subu	$9,$13,$12
subu	$7,$0,$6
sw	$10,92($3)
subu	$12,$0,$5
subu	$13,$0,$15
slt	$17,$4,0
movn	$4,$16,$17
slt	$11,$6,0
slt	$17,$5,0
slt	$16,$15,0
movn	$6,$7,$11
addu	$11,$10,$8
subu	$7,$10,$8
movn	$5,$12,$17
subu	$8,$0,$9
subu	$12,$0,$24
movn	$15,$13,$16
slt	$10,$9,0
slt	$13,$24,0
movn	$9,$8,$10
subu	$8,$0,$7
movn	$24,$12,$13
subu	$12,$0,$11
slt	$13,$11,0
slt	$10,$7,0
movn	$11,$12,$13
addu	$6,$4,$6
movn	$7,$8,$10
addu	$5,$5,$15
addu	$8,$24,$9
addu	$5,$6,$5
addu	$7,$11,$7
addu	$4,$5,$8
addu	$4,$4,$7
bne	$3,$14,$L1084
addu	$2,$2,$4

lw	$22,292($sp)
lw	$21,288($sp)
lw	$20,284($sp)
lw	$19,280($sp)
lw	$18,276($sp)
lw	$17,272($sp)
lw	$16,268($sp)
j	$31
addiu	$sp,$sp,296

.set	macro
.set	reorder
.end	hadamard8_diff8x8_c
.size	hadamard8_diff8x8_c, .-hadamard8_diff8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	hadamard8_intra8x8_c
.type	hadamard8_intra8x8_c, @function
hadamard8_intra8x8_c:
.frame	$sp,272,$31		# vars= 256, regs= 1/0, args= 0, gp= 8
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-272
addiu	$3,$sp,8
sw	$16,268($sp)
addiu	$24,$sp,264
move	$10,$3
$L1089:
lbu	$2,1($5)
addiu	$10,$10,32
lbu	$11,2($5)
lbu	$15,5($5)
lbu	$14,7($5)
lbu	$4,0($5)
lbu	$13,3($5)
lbu	$6,4($5)
lbu	$12,6($5)
addu	$9,$4,$2
addu	$8,$11,$13
subu	$4,$4,$2
subu	$13,$11,$13
addu	$25,$12,$14
addu	$11,$6,$15
subu	$12,$12,$14
subu	$6,$6,$15
addu	$14,$4,$13
addu	$15,$9,$8
subu	$2,$4,$13
subu	$8,$9,$8
addu	$13,$11,$25
subu	$9,$11,$25
addu	$11,$6,$12
subu	$4,$6,$12
addu	$6,$14,$11
addu	$12,$15,$13
subu	$14,$14,$11
subu	$15,$15,$13
sw	$6,-28($10)
addu	$11,$8,$9
sw	$12,-32($10)
addu	$6,$2,$4
sw	$14,-12($10)
subu	$8,$8,$9
sw	$15,-16($10)
subu	$2,$2,$4
sw	$11,-24($10)
addu	$5,$5,$7
sw	$6,-20($10)
sw	$8,-8($10)
bne	$10,$24,$L1089
sw	$2,-4($10)

addiu	$2,$sp,40
move	$24,$0
$L1098:
lw	$10,0($3)
addiu	$3,$3,4
lw	$4,28($3)
lw	$12,60($3)
lw	$9,92($3)
lw	$5,124($3)
subu	$11,$10,$4
lw	$8,156($3)
addu	$13,$10,$4
lw	$6,188($3)
addu	$4,$12,$9
lw	$7,220($3)
subu	$10,$12,$9
subu	$9,$5,$8
addu	$12,$5,$8
addu	$25,$6,$7
subu	$7,$6,$7
addu	$5,$12,$25
addu	$14,$9,$7
addu	$6,$13,$4
addu	$15,$11,$10
sw	$5,124($3)
subu	$13,$13,$4
sw	$14,156($3)
subu	$12,$12,$25
sw	$6,-4($3)
addu	$4,$6,$5
sw	$15,28($3)
subu	$6,$6,$5
sw	$13,60($3)
addu	$5,$15,$14
sw	$12,188($3)
subu	$14,$15,$14
subu	$8,$9,$7
subu	$16,$0,$4
subu	$10,$11,$10
addu	$15,$13,$12
sw	$8,220($3)
subu	$9,$13,$12
subu	$7,$0,$6
sw	$10,92($3)
subu	$12,$0,$5
subu	$13,$0,$14
slt	$25,$4,0
movn	$4,$16,$25
slt	$11,$6,0
slt	$16,$5,0
slt	$25,$14,0
movn	$6,$7,$11
addu	$11,$10,$8
subu	$7,$10,$8
movn	$5,$12,$16
subu	$8,$0,$9
subu	$12,$0,$15
movn	$14,$13,$25
slt	$10,$9,0
slt	$13,$15,0
movn	$9,$8,$10
subu	$8,$0,$7
movn	$15,$12,$13
subu	$12,$0,$11
slt	$13,$11,0
slt	$10,$7,0
movn	$11,$12,$13
addu	$6,$4,$6
movn	$7,$8,$10
addu	$5,$5,$14
addu	$8,$15,$9
addu	$5,$6,$5
addu	$7,$11,$7
addu	$4,$5,$8
addu	$4,$4,$7
bne	$3,$2,$L1098
addu	$24,$24,$4

lw	$3,136($sp)
lw	$2,8($sp)
lw	$16,268($sp)
addiu	$sp,$sp,272
addu	$2,$2,$3
subu	$3,$0,$2
slt	$4,$2,0
movn	$2,$3,$4
j	$31
subu	$2,$24,$2

.set	macro
.set	reorder
.end	hadamard8_intra8x8_c
.size	hadamard8_intra8x8_c, .-hadamard8_intra8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	dct_sad8x8_c
.type	dct_sad8x8_c, @function
dct_sad8x8_c:
.frame	$sp,184,$31		# vars= 144, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-184
lw	$25,2964($4)
addiu	$2,$sp,39
sw	$16,172($sp)
li	$16,-16			# 0xfffffffffffffff0
sw	$17,176($sp)
move	$17,$4
sw	$31,180($sp)
and	$16,$2,$16
jalr	$25
move	$4,$16

lw	$25,5644($17)
jalr	$25
move	$4,$16

lw	$25,2992($17)
jalr	$25
move	$4,$16

lw	$31,180($sp)
lw	$17,176($sp)
lw	$16,172($sp)
j	$31
addiu	$sp,$sp,184

.set	macro
.set	reorder
.end	dct_sad8x8_c
.size	dct_sad8x8_c, .-dct_sad8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	dct264_sad8x8_c
.type	dct264_sad8x8_c, @function
dct264_sad8x8_c:
.frame	$sp,184,$31		# vars= 128, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-184
lw	$25,2964($4)
sw	$16,152($sp)
addiu	$16,$sp,24
sw	$31,180($sp)
move	$4,$16
sw	$22,176($sp)
sw	$21,172($sp)
sw	$20,168($sp)
sw	$19,164($sp)
sw	$18,160($sp)
jalr	$25
sw	$17,156($sp)

addiu	$14,$sp,152
move	$4,$16
move	$2,$16
$L1106:
lh	$8,0($2)
addiu	$2,$2,16
lh	$3,-2($2)
lh	$5,-4($2)
lh	$19,-12($2)
subu	$7,$8,$3
lh	$18,-6($2)
addu	$8,$8,$3
lh	$17,-8($2)
lh	$9,-14($2)
sra	$11,$7,1
lh	$3,-10($2)
subu	$13,$19,$18
addu	$11,$11,$7
subu	$6,$9,$5
subu	$12,$3,$17
sra	$16,$6,1
sra	$10,$13,1
sra	$15,$12,1
addu	$9,$9,$5
addu	$16,$16,$6
addu	$5,$19,$18
addu	$3,$3,$17
addu	$19,$6,$13
subu	$18,$7,$12
addu	$10,$10,$13
addu	$7,$7,$12
subu	$6,$6,$13
addu	$12,$15,$12
subu	$17,$8,$3
subu	$15,$9,$5
addu	$11,$19,$11
subu	$7,$7,$16
addu	$6,$6,$12
subu	$10,$18,$10
addu	$3,$8,$3
addu	$5,$9,$5
sra	$16,$6,2
sra	$12,$7,2
sra	$18,$10,2
sra	$8,$11,2
sra	$13,$15,1
sra	$9,$17,1
andi	$5,$5,0xffff
andi	$3,$3,0xffff
addu	$19,$5,$3
addu	$11,$16,$11
subu	$3,$3,$5
addu	$13,$13,$17
sh	$19,-16($2)
addu	$10,$12,$10
sh	$11,-14($2)
subu	$7,$7,$18
sh	$3,-8($2)
subu	$5,$9,$15
sh	$13,-12($2)
subu	$6,$8,$6
sh	$10,-10($2)
sh	$7,-6($2)
sh	$5,-4($2)
bne	$2,$14,$L1106
sh	$6,-2($2)

addiu	$14,$sp,40
move	$3,$0
$L1115:
lh	$16,48($4)
addiu	$4,$4,2
lh	$5,62($4)
lh	$6,110($4)
lh	$2,94($4)
lh	$11,30($4)
subu	$22,$16,$5
lh	$17,-2($4)
addu	$16,$16,$5
lh	$12,14($4)
sra	$18,$22,1
lh	$10,78($4)
subu	$13,$17,$6
subu	$24,$12,$2
subu	$21,$11,$10
addu	$12,$12,$2
addu	$17,$17,$6
addu	$10,$11,$10
subu	$6,$24,$21
sra	$7,$13,1
addu	$5,$18,$22
addu	$15,$12,$10
addu	$18,$6,$5
addu	$2,$17,$16
addu	$7,$7,$13
addu	$19,$24,$21
sra	$11,$24,1
addu	$20,$2,$15
subu	$10,$12,$10
addu	$19,$19,$7
addu	$6,$13,$22
sra	$8,$18,2
sra	$7,$21,1
addu	$24,$11,$24
addu	$25,$8,$19
subu	$11,$6,$24
subu	$8,$13,$22
subu	$5,$17,$16
subu	$6,$0,$20
addu	$7,$7,$21
sra	$9,$10,1
slt	$12,$20,0
addu	$9,$9,$5
movn	$20,$6,$12
subu	$7,$8,$7
sra	$13,$11,2
subu	$8,$0,$25
slt	$12,$25,0
addu	$13,$13,$7
movn	$25,$8,$12
subu	$6,$0,$9
slt	$8,$9,0
subu	$15,$2,$15
movn	$9,$6,$8
subu	$2,$0,$13
addu	$8,$20,$3
sra	$7,$7,2
slt	$3,$13,0
subu	$6,$11,$7
movn	$13,$2,$3
addu	$8,$25,$8
subu	$2,$0,$15
sra	$5,$5,1
slt	$7,$15,0
subu	$3,$5,$10
movn	$15,$2,$7
addu	$9,$9,$8
subu	$2,$0,$6
sra	$7,$19,2
slt	$8,$6,0
subu	$5,$7,$18
movn	$6,$2,$8
subu	$7,$0,$3
addu	$9,$13,$9
slt	$8,$3,0
movn	$3,$7,$8
addu	$2,$15,$9
subu	$7,$0,$5
slt	$8,$5,0
addu	$6,$6,$2
movn	$5,$7,$8
addu	$3,$3,$6
bne	$4,$14,$L1115
addu	$3,$5,$3

lw	$31,180($sp)
move	$2,$3
lw	$22,176($sp)
lw	$21,172($sp)
lw	$20,168($sp)
lw	$19,164($sp)
lw	$18,160($sp)
lw	$17,156($sp)
lw	$16,152($sp)
j	$31
addiu	$sp,$sp,184

.set	macro
.set	reorder
.end	dct264_sad8x8_c
.size	dct264_sad8x8_c, .-dct264_sad8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	dct_max8x8_c
.type	dct_max8x8_c, @function
dct_max8x8_c:
.frame	$sp,184,$31		# vars= 144, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-184
lw	$25,2964($4)
addiu	$2,$sp,39
sw	$17,176($sp)
move	$17,$4
sw	$16,172($sp)
li	$4,-16			# 0xfffffffffffffff0
sw	$31,180($sp)
and	$16,$2,$4
jalr	$25
move	$4,$16

lw	$25,5644($17)
jalr	$25
move	$4,$16

addiu	$7,$16,128
move	$2,$0
move	$4,$16
$L1121:
lh	$3,0($4)
addiu	$4,$4,2
subu	$5,$0,$3
slt	$6,$3,0
movn	$3,$5,$6
slt	$5,$2,$3
bne	$4,$7,$L1121
movn	$2,$3,$5

lw	$31,180($sp)
lw	$17,176($sp)
lw	$16,172($sp)
j	$31
addiu	$sp,$sp,184

.set	macro
.set	reorder
.end	dct_max8x8_c
.size	dct_max8x8_c, .-dct_max8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	bit8x8_c
.type	bit8x8_c, @function
bit8x8_c:
.frame	$sp,200,$31		# vars= 152, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-200
lw	$25,2964($4)
li	$2,-16			# 0xfffffffffffffff0
sw	$17,188($sp)
addiu	$17,$sp,47
sw	$31,196($sp)
and	$17,$17,$2
sw	$18,192($sp)
sw	$16,184($sp)
move	$16,$4
lw	$18,8604($4)
jalr	$25
move	$4,$17

addiu	$2,$sp,176
lw	$7,2872($16)
move	$6,$0
lw	$25,10576($16)
move	$4,$16
sw	$2,16($sp)
jalr	$25
move	$5,$17

lw	$3,8004($16)
beq	$3,$0,$L1125
sw	$2,8680($16)

lh	$4,0($17)
li	$6,1			# 0x1
lw	$3,8624($16)
lw	$11,8608($16)
lw	$10,8612($16)
addu	$3,$3,$4
lbu	$5,256($3)
$L1126:
slt	$3,$2,$6
bne	$3,$0,$L1127
slt	$3,$6,$2

sw	$6,176($sp)
move	$7,$0
beq	$3,$0,$L1128
addiu	$16,$16,8732

.option	pic0
j	$L1132
.option	pic2
li	$9,-128			# 0xffffffffffffff80

$L1137:
sll	$7,$7,7
and	$8,$4,$9
bne	$8,$0,$L1130
addu	$3,$7,$4

addu	$3,$11,$3
move	$7,$0
lbu	$3,0($3)
addu	$5,$5,$3
$L1131:
addiu	$6,$6,1
beq	$6,$2,$L1128
sw	$6,176($sp)

$L1132:
addu	$3,$16,$6
lbu	$3,0($3)
sll	$3,$3,1
addu	$3,$17,$3
lh	$3,0($3)
bne	$3,$0,$L1137
addiu	$4,$3,64

addiu	$6,$6,1
addiu	$7,$7,1
bne	$6,$2,$L1132
sw	$6,176($sp)

$L1128:
addu	$16,$16,$2
lbu	$2,0($16)
sll	$3,$2,1
sw	$2,176($sp)
addu	$17,$17,$3
li	$3,-128			# 0xffffffffffffff80
lh	$2,0($17)
addiu	$2,$2,64
and	$3,$2,$3
beq	$3,$0,$L1138
lw	$31,196($sp)

addu	$5,$5,$18
$L1127:
lw	$31,196($sp)
move	$2,$5
lw	$18,192($sp)
lw	$17,188($sp)
lw	$16,184($sp)
j	$31
addiu	$sp,$sp,200

$L1130:
addu	$5,$5,$18
.option	pic0
j	$L1131
.option	pic2
move	$7,$0

$L1125:
lw	$11,8616($16)
move	$6,$0
lw	$10,8620($16)
.option	pic0
j	$L1126
.option	pic2
move	$5,$0

$L1138:
sll	$7,$7,7
lw	$18,192($sp)
addu	$7,$7,$2
lw	$17,188($sp)
lw	$16,184($sp)
addu	$7,$10,$7
lbu	$2,0($7)
addiu	$sp,$sp,200
addu	$5,$5,$2
j	$31
move	$2,$5

.set	macro
.set	reorder
.end	bit8x8_c
.size	bit8x8_c, .-bit8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vsad_intra8_c
.type	vsad_intra8_c, @function
vsad_intra8_c:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-24
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
lw	$25,40($sp)
slt	$2,$25,2
bne	$2,$0,$L1140
move	$2,$0

addu	$8,$5,$7
li	$15,1			# 0x1
$L1149:
lbu	$6,0($5)
addiu	$15,$15,1
lbu	$3,0($8)
lbu	$4,1($8)
lbu	$14,1($5)
subu	$3,$6,$3
lbu	$9,4($5)
lbu	$6,4($8)
subu	$14,$14,$4
lbu	$10,2($8)
lbu	$4,5($8)
subu	$24,$0,$3
lbu	$13,2($5)
subu	$6,$9,$6
lbu	$12,5($5)
subu	$16,$0,$14
lbu	$9,3($8)
slt	$19,$14,0
lbu	$11,3($5)
subu	$13,$13,$10
subu	$12,$12,$4
lbu	$10,6($5)
lbu	$4,6($8)
subu	$17,$0,$13
subu	$11,$11,$9
lbu	$18,7($5)
lbu	$9,7($8)
slt	$20,$3,0
subu	$10,$10,$4
movn	$14,$16,$19
subu	$4,$0,$6
movn	$3,$24,$20
subu	$16,$0,$12
slt	$19,$13,0
slt	$24,$6,0
movn	$13,$17,$19
slt	$17,$12,0
movn	$6,$4,$24
subu	$9,$18,$9
subu	$4,$0,$11
movn	$12,$16,$17
subu	$24,$0,$10
slt	$17,$11,0
slt	$16,$10,0
movn	$11,$4,$17
addu	$4,$3,$14
movn	$10,$24,$16
subu	$14,$0,$9
slt	$24,$9,0
addu	$3,$4,$13
movn	$9,$14,$24
addu	$4,$6,$12
addu	$3,$3,$11
addu	$4,$4,$10
addu	$2,$3,$2
addu	$6,$4,$9
addu	$5,$5,$7
addu	$2,$6,$2
bne	$15,$25,$L1149
addu	$8,$8,$7

$L1140:
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,24

.set	macro
.set	reorder
.end	vsad_intra8_c
.size	vsad_intra8_c, .-vsad_intra8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vsad_intra16_c
.type	vsad_intra16_c, @function
vsad_intra16_c:
.frame	$sp,88,$31		# vars= 40, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-88
sw	$fp,84($sp)
sw	$23,80($sp)
sw	$22,76($sp)
sw	$21,72($sp)
sw	$20,68($sp)
sw	$19,64($sp)
sw	$18,60($sp)
sw	$17,56($sp)
sw	$16,52($sp)
sw	$7,100($sp)
lw	$3,104($sp)
slt	$2,$3,2
bne	$2,$0,$L1172
li	$4,1			# 0x1

sw	$0,16($sp)
addu	$25,$5,$7
move	$16,$5
sw	$4,12($sp)
$L1171:
lbu	$2,1($25)
lbu	$6,1($16)
lbu	$4,0($16)
lbu	$3,0($25)
lbu	$5,2($25)
subu	$6,$6,$2
lbu	$15,2($16)
lbu	$2,4($25)
subu	$19,$4,$3
lbu	$14,4($16)
subu	$7,$0,$6
lw	$8,12($sp)
subu	$15,$15,$5
lbu	$3,5($16)
subu	$21,$0,$19
lbu	$5,5($25)
subu	$14,$14,$2
lbu	$4,3($16)
addiu	$8,$8,1
lbu	$2,3($25)
subu	$9,$0,$14
subu	$3,$3,$5
lbu	$11,7($16)
lbu	$5,6($25)
slt	$24,$19,0
subu	$fp,$4,$2
lbu	$4,7($25)
lbu	$13,6($16)
subu	$22,$0,$3
lbu	$2,8($25)
slt	$17,$6,0
lbu	$12,9($16)
subu	$11,$11,$4
lbu	$4,13($25)
subu	$13,$13,$5
lbu	$5,9($25)
subu	$18,$0,$15
sw	$8,12($sp)
slt	$23,$15,0
sw	$9,8($sp)
slt	$9,$3,0
lbu	$8,8($16)
subu	$12,$12,$5
sw	$9,32($sp)
lbu	$9,13($16)
subu	$2,$8,$2
movn	$19,$21,$24
subu	$21,$0,$13
subu	$9,$9,$4
lbu	$8,10($25)
subu	$4,$0,$2
lbu	$20,12($16)
lbu	$10,10($16)
subu	$24,$0,$fp
lbu	$5,12($25)
sw	$22,28($sp)
slt	$22,$14,0
sw	$21,20($sp)
subu	$10,$10,$8
lbu	$21,11($16)
subu	$5,$20,$5
sw	$4,24($sp)
subu	$4,$0,$12
movn	$6,$7,$17
slt	$20,$13,0
slt	$17,$fp,0
movn	$15,$18,$23
slt	$18,$2,0
slt	$23,$12,0
sw	$4,36($sp)
addu	$6,$19,$6
sw	$18,44($sp)
subu	$19,$0,$5
sw	$23,40($sp)
subu	$23,$0,$11
lbu	$4,14($25)
lbu	$7,14($16)
lbu	$8,11($25)
lw	$18,8($sp)
subu	$7,$7,$4
lw	$4,20($sp)
subu	$8,$21,$8
lw	$21,32($sp)
movn	$14,$18,$22
lw	$22,28($sp)
movn	$13,$4,$20
slt	$20,$5,0
lw	$18,44($sp)
lw	$4,24($sp)
sw	$6,28($sp)
sw	$23,32($sp)
lbu	$6,15($16)
lbu	$23,15($25)
movn	$3,$22,$21
subu	$21,$0,$10
slt	$22,$10,0
subu	$6,$6,$23
movn	$2,$4,$18
slt	$18,$9,0
lw	$23,40($sp)
addu	$3,$14,$3
lw	$4,36($sp)
lw	$14,32($sp)
addu	$3,$3,$13
movn	$fp,$24,$17
subu	$17,$0,$9
slt	$24,$11,0
movn	$12,$4,$23
slt	$13,$6,0
lw	$23,28($sp)
movn	$10,$21,$22
slt	$22,$8,0
slt	$21,$7,0
addu	$4,$23,$15
movn	$11,$14,$24
subu	$23,$0,$7
movn	$5,$19,$20
subu	$24,$0,$8
addu	$2,$2,$12
movn	$9,$17,$18
addu	$4,$4,$fp
subu	$12,$0,$6
lw	$18,16($sp)
addu	$11,$3,$11
movn	$8,$24,$22
addu	$10,$2,$10
addu	$5,$5,$9
movn	$7,$23,$21
addu	$4,$4,$18
movn	$6,$12,$13
addu	$11,$11,$4
addu	$8,$10,$8
addu	$7,$5,$7
lw	$19,100($sp)
addu	$8,$8,$11
lw	$21,12($sp)
addu	$6,$7,$6
lw	$22,104($sp)
addu	$16,$16,$19
addu	$6,$6,$8
addu	$25,$25,$19
bne	$21,$22,$L1171
sw	$6,16($sp)

$L1154:
lw	$2,16($sp)
lw	$fp,84($sp)
lw	$23,80($sp)
lw	$22,76($sp)
lw	$21,72($sp)
lw	$20,68($sp)
lw	$19,64($sp)
lw	$18,60($sp)
lw	$17,56($sp)
lw	$16,52($sp)
j	$31
addiu	$sp,$sp,88

$L1172:
.option	pic0
j	$L1154
.option	pic2
sw	$0,16($sp)

.set	macro
.set	reorder
.end	vsad_intra16_c
.size	vsad_intra16_c, .-vsad_intra16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vsad16_c
.type	vsad16_c, @function
vsad16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
slt	$2,$14,2
bne	$2,$0,$L1179
li	$24,1			# 0x1

move	$2,$0
li	$15,16			# 0x10
$L1177:
move	$3,$0
addu	$13,$5,$7
addu	$12,$6,$7
$L1178:
addu	$8,$5,$3
addu	$9,$6,$3
addu	$4,$13,$3
lbu	$11,0($8)
addu	$8,$12,$3
lbu	$9,0($9)
addiu	$3,$3,1
lbu	$4,0($4)
lbu	$10,0($8)
subu	$8,$11,$9
subu	$9,$9,$11
subu	$8,$8,$4
addu	$4,$4,$9
addu	$8,$8,$10
subu	$4,$4,$10
slt	$9,$8,0
movz	$4,$8,$9
bne	$3,$15,$L1178
addu	$2,$2,$4

addiu	$24,$24,1
move	$5,$13
bne	$24,$14,$L1177
move	$6,$12

j	$31
nop

$L1179:
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	vsad16_c
.size	vsad16_c, .-vsad16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vsse_intra8_c
.type	vsse_intra8_c, @function
vsse_intra8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
slt	$2,$14,2
bne	$2,$0,$L1184
addu	$3,$5,$7

li	$15,1			# 0x1
move	$24,$0
$L1183:
lbu	$8,1($5)
addiu	$15,$15,1
lbu	$25,1($3)
lbu	$6,0($5)
lbu	$13,0($3)
lbu	$12,2($5)
subu	$25,$8,$25
lbu	$10,2($3)
subu	$13,$6,$13
lbu	$11,5($3)
mult	$25,$25
lbu	$8,5($5)
subu	$10,$12,$10
lbu	$6,3($5)
madd	$13,$13
lbu	$4,3($3)
madd	$10,$10
lbu	$12,4($5)
subu	$8,$8,$11
lbu	$11,4($3)
subu	$4,$6,$4
lbu	$10,6($5)
lbu	$9,6($3)
mflo	$2
lbu	$6,7($3)
mul	$13,$8,$8
lbu	$8,7($5)
subu	$11,$12,$11
mul	$12,$4,$4
subu	$9,$10,$9
subu	$6,$8,$6
addu	$5,$5,$7
addu	$3,$3,$7
mtlo	$13
addu	$4,$12,$2
addu	$2,$4,$24
madd	$11,$11
madd	$9,$9
madd	$6,$6
mflo	$6
bne	$15,$14,$L1183
addu	$24,$6,$2

$L1182:
j	$31
move	$2,$24

$L1184:
.option	pic0
j	$L1182
.option	pic2
move	$24,$0

.set	macro
.set	reorder
.end	vsse_intra8_c
.size	vsse_intra8_c, .-vsse_intra8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vsse_intra16_c
.type	vsse_intra16_c, @function
vsse_intra16_c:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-8
sw	$16,4($sp)
lw	$15,24($sp)
slt	$2,$15,2
bne	$2,$0,$L1189
nop

addu	$3,$5,$7
li	$24,1			# 0x1
move	$2,$0
$L1188:
lbu	$4,1($5)
addiu	$24,$24,1
lbu	$14,1($3)
lbu	$6,0($5)
lbu	$13,0($3)
lbu	$8,2($5)
subu	$14,$4,$14
lbu	$11,2($3)
subu	$13,$6,$13
lbu	$4,5($5)
mult	$14,$14
lbu	$10,5($3)
subu	$11,$8,$11
lbu	$14,3($5)
madd	$13,$13
lbu	$8,3($3)
madd	$11,$11
lbu	$13,4($5)
subu	$10,$4,$10
lbu	$12,4($3)
subu	$4,$14,$8
lbu	$11,6($5)
lbu	$6,6($3)
mflo	$9
lbu	$14,9($3)
mul	$25,$10,$10
lbu	$10,9($5)
mul	$8,$4,$4
lbu	$4,7($5)
subu	$12,$13,$12
lbu	$13,8($5)
subu	$6,$11,$6
lbu	$11,10($5)
subu	$10,$10,$14
lbu	$14,8($3)
mtlo	$25
addu	$9,$8,$9
lbu	$8,7($3)
subu	$14,$13,$14
lbu	$13,13($3)
addu	$2,$9,$2
subu	$8,$4,$8
madd	$12,$12
lbu	$12,10($3)
madd	$6,$6
subu	$12,$11,$12
lbu	$11,14($3)
mflo	$6
mul	$25,$10,$10
lbu	$10,13($5)
mul	$4,$8,$8
subu	$10,$10,$13
lbu	$13,12($3)
mtlo	$25
addu	$8,$4,$6
lbu	$6,11($5)
lbu	$4,11($3)
addu	$8,$8,$2
subu	$4,$6,$4
lbu	$6,15($3)
addu	$3,$3,$7
madd	$14,$14
lbu	$14,12($5)
madd	$12,$12
lbu	$12,14($5)
subu	$13,$14,$13
subu	$11,$12,$11
mflo	$25
mul	$16,$10,$10
lbu	$10,15($5)
mul	$9,$4,$4
addu	$5,$5,$7
subu	$6,$10,$6
mtlo	$16
addu	$4,$9,$25
addu	$4,$4,$8
madd	$13,$13
madd	$11,$11
madd	$6,$6
mflo	$6
bne	$24,$15,$L1188
addu	$2,$6,$4

$L1187:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

$L1189:
.option	pic0
j	$L1187
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	vsse_intra16_c
.size	vsse_intra16_c, .-vsse_intra16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vsse16_c
.type	vsse16_c, @function
vsse16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$14,16($sp)
slt	$2,$14,2
bne	$2,$0,$L1196
mtlo	$0

li	$13,1			# 0x1
li	$12,16			# 0x10
$L1194:
move	$8,$0
addu	$11,$5,$7
addu	$10,$6,$7
$L1195:
addu	$2,$5,$8
addu	$9,$6,$8
addu	$3,$11,$8
lbu	$4,0($2)
addu	$2,$10,$8
lbu	$9,0($9)
addiu	$8,$8,1
lbu	$3,0($3)
lbu	$2,0($2)
subu	$4,$4,$9
subu	$3,$4,$3
addu	$3,$3,$2
bne	$8,$12,$L1195
madd	$3,$3

addiu	$13,$13,1
move	$5,$11
bne	$13,$14,$L1194
move	$6,$10

j	$31
mflo	$2

$L1196:
j	$31
mflo	$2

.set	macro
.set	reorder
.end	vsse16_c
.size	vsse16_c, .-vsse16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	ssd_int8_vs_int16_c
.type	ssd_int8_vs_int16_c, @function
ssd_int8_vs_int16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$6,$L1201
sll	$6,$6,1

mtlo	$0
addu	$6,$5,$6
$L1200:
addiu	$5,$5,2
lb	$3,0($4)
lh	$2,-2($5)
addiu	$4,$4,1
subu	$3,$3,$2
bne	$5,$6,$L1200
madd	$3,$3

$L1199:
j	$31
mflo	$2

$L1201:
.option	pic0
j	$L1199
.option	pic2
mtlo	$0

.set	macro
.set	reorder
.end	ssd_int8_vs_int16_c
.size	ssd_int8_vs_int16_c, .-ssd_int8_vs_int16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	hadamard8_diff16_c
.type	hadamard8_diff16_c, @function
hadamard8_diff16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	hadamard8_diff8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	hadamard8_diff8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
bne	$3,$2,$L1206
lw	$31,60($sp)

sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	hadamard8_diff8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
addu	$16,$2,$16
move	$4,$21
.option	pic0
jal	hadamard8_diff8x8_c
.option	pic2
move	$7,$20

addu	$16,$2,$16
lw	$31,60($sp)
$L1206:
move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	hadamard8_diff16_c
.size	hadamard8_diff16_c, .-hadamard8_diff16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	hadamard8_intra16_c
.type	hadamard8_intra16_c, @function
hadamard8_intra16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	hadamard8_intra8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	hadamard8_intra8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
bne	$3,$2,$L1210
lw	$31,60($sp)

sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	hadamard8_intra8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
addu	$16,$2,$16
move	$4,$21
.option	pic0
jal	hadamard8_intra8x8_c
.option	pic2
move	$7,$20

addu	$16,$2,$16
lw	$31,60($sp)
$L1210:
move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	hadamard8_intra16_c
.size	hadamard8_intra16_c, .-hadamard8_intra16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	dct264_sad16_c
.type	dct264_sad16_c, @function
dct264_sad16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	dct264_sad8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	dct264_sad8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
beq	$3,$2,$L1214
lw	$31,60($sp)

move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

$L1214:
sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	dct264_sad8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	dct264_sad8x8_c
.option	pic2
addu	$16,$2,$16

lw	$31,60($sp)
addu	$16,$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
move	$2,$16
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	dct264_sad16_c
.size	dct264_sad16_c, .-dct264_sad16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	dct_max16_c
.type	dct_max16_c, @function
dct_max16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	dct_max8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	dct_max8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
beq	$3,$2,$L1218
lw	$31,60($sp)

move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

$L1218:
sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	dct_max8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	dct_max8x8_c
.option	pic2
addu	$16,$2,$16

lw	$31,60($sp)
addu	$16,$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
move	$2,$16
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	dct_max16_c
.size	dct_max16_c, .-dct_max16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	bit16_c
.type	bit16_c, @function
bit16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	bit8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	bit8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
beq	$3,$2,$L1222
lw	$31,60($sp)

move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

$L1222:
sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	bit8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	bit8x8_c
.option	pic2
addu	$16,$2,$16

lw	$31,60($sp)
addu	$16,$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
move	$2,$16
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	bit16_c
.size	bit16_c, .-bit16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_fmul_c
.type	vector_fmul_c, @function
vector_fmul_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$6,$L1228
sll	$6,$6,2

addu	$6,$4,$6
$L1225:
addiu	$5,$5,4
lwc1	$f0,0($4)
addiu	$4,$4,4
lwc1	$f1,-4($5)
mul.s	$f0,$f0,$f1
bne	$4,$6,$L1225
swc1	$f0,-4($4)

$L1228:
j	$31
nop

.set	macro
.set	reorder
.end	vector_fmul_c
.size	vector_fmul_c, .-vector_fmul_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_fmul_reverse_c
.type	vector_fmul_reverse_c, @function
vector_fmul_reverse_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$8,$7,2
blez	$7,$L1234
addiu	$8,$8,-4

addu	$6,$6,$8
move	$2,$0
addiu	$8,$8,4
$L1231:
addu	$7,$5,$2
lwc1	$f0,0($6)
addu	$3,$4,$2
lwc1	$f1,0($7)
addiu	$2,$2,4
addiu	$6,$6,-4
mul.s	$f0,$f0,$f1
bne	$2,$8,$L1231
swc1	$f0,0($3)

$L1234:
j	$31
nop

.set	macro
.set	reorder
.end	vector_fmul_reverse_c
.size	vector_fmul_reverse_c, .-vector_fmul_reverse_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_fmul_add_c
.type	vector_fmul_add_c, @function
vector_fmul_add_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$10,16($sp)
blez	$10,$L1240
sll	$10,$10,2

move	$2,$0
$L1237:
addu	$3,$6,$2
addu	$9,$5,$2
lwc1	$f1,0($3)
addu	$8,$7,$2
addu	$3,$4,$2
lwc1	$f0,0($9)
addiu	$2,$2,4
lwc1	$f2,0($8)
mul.s	$f0,$f1,$f0
add.s	$f0,$f0,$f2
bne	$2,$10,$L1237
swc1	$f0,0($3)

$L1240:
j	$31
nop

.set	macro
.set	reorder
.end	vector_fmul_add_c
.size	vector_fmul_add_c, .-vector_fmul_add_c
.align	2
.globl	ff_vector_fmul_window_c
.set	nomips16
.set	nomicromips
.ent	ff_vector_fmul_window_c
.type	ff_vector_fmul_window_c, @function
ff_vector_fmul_window_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$10,20($sp)
lwc1	$f5,16($sp)
sll	$11,$10,2
subu	$10,$0,$10
addu	$4,$4,$11
bgez	$10,$L1246
addu	$7,$7,$11

sll	$10,$10,2
move	$8,$0
addu	$12,$11,$10
move	$3,$0
$L1243:
addu	$2,$8,$11
addu	$13,$3,$10
addu	$14,$6,$2
addu	$9,$3,$12
lwc1	$f4,-4($14)
addu	$15,$7,$13
addu	$14,$7,$2
addu	$9,$5,$9
lwc1	$f3,0($15)
addu	$13,$4,$13
lwc1	$f0,0($9)
addu	$2,$4,$2
addiu	$8,$8,-4
lwc1	$f1,-4($14)
addiu	$3,$3,4
mul.s	$f2,$f3,$f0
mul.s	$f0,$f1,$f0
mul.s	$f1,$f1,$f4
mul.s	$f3,$f3,$f4
add.s	$f0,$f0,$f5
add.s	$f1,$f2,$f1
sub.s	$f0,$f0,$f3
add.s	$f1,$f1,$f5
swc1	$f0,0($13)
bne	$8,$10,$L1243
swc1	$f1,-4($2)

$L1246:
j	$31
nop

.set	macro
.set	reorder
.end	ff_vector_fmul_window_c
.size	ff_vector_fmul_window_c, .-ff_vector_fmul_window_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_fmul_scalar_c
.type	vector_fmul_scalar_c, @function
vector_fmul_scalar_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1252
sll	$7,$7,2

move	$2,$0
$L1249:
addu	$8,$5,$2
mtc1	$6,$f1
addu	$3,$4,$2
lwc1	$f0,0($8)
addiu	$2,$2,4
mul.s	$f0,$f0,$f1
bne	$2,$7,$L1249
swc1	$f0,0($3)

$L1252:
j	$31
nop

.set	macro
.set	reorder
.end	vector_fmul_scalar_c
.size	vector_fmul_scalar_c, .-vector_fmul_scalar_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_fmul_sv_scalar_2_c
.type	vector_fmul_sv_scalar_2_c, @function
vector_fmul_sv_scalar_2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,16($sp)
blez	$2,$L1258
mtc1	$7,$f1

addiu	$3,$2,-1
addiu	$2,$5,4
srl	$3,$3,1
sll	$3,$3,3
addiu	$3,$3,12
addu	$5,$5,$3
$L1255:
lw	$3,0($6)
addiu	$2,$2,8
lwc1	$f0,-12($2)
addiu	$4,$4,8
addiu	$6,$6,4
lwc1	$f2,0($3)
mul.s	$f0,$f0,$f2
mul.s	$f0,$f0,$f1
swc1	$f0,-8($4)
lwc1	$f0,-8($2)
lwc1	$f2,4($3)
mul.s	$f0,$f0,$f2
mul.s	$f0,$f0,$f1
bne	$2,$5,$L1255
swc1	$f0,-4($4)

$L1258:
j	$31
nop

.set	macro
.set	reorder
.end	vector_fmul_sv_scalar_2_c
.size	vector_fmul_sv_scalar_2_c, .-vector_fmul_sv_scalar_2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_fmul_sv_scalar_4_c
.type	vector_fmul_sv_scalar_4_c, @function
vector_fmul_sv_scalar_4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,16($sp)
blez	$8,$L1264
mtc1	$7,$f1

move	$3,$6
$L1261:
lw	$2,0($3)
addiu	$4,$4,16
lwc1	$f0,0($5)
addiu	$5,$5,16
addiu	$3,$3,4
lwc1	$f2,0($2)
subu	$7,$3,$6
slt	$7,$7,$8
mul.s	$f0,$f0,$f2
mul.s	$f0,$f0,$f1
swc1	$f0,-16($4)
lwc1	$f2,4($2)
lwc1	$f0,-12($5)
mul.s	$f0,$f0,$f2
mul.s	$f0,$f0,$f1
swc1	$f0,-12($4)
lwc1	$f2,8($2)
lwc1	$f0,-8($5)
mul.s	$f0,$f0,$f2
mul.s	$f0,$f0,$f1
swc1	$f0,-8($4)
lwc1	$f0,-4($5)
lwc1	$f2,12($2)
mul.s	$f0,$f0,$f2
mul.s	$f0,$f0,$f1
bne	$7,$0,$L1261
swc1	$f0,-4($4)

$L1264:
j	$31
nop

.set	macro
.set	reorder
.end	vector_fmul_sv_scalar_4_c
.size	vector_fmul_sv_scalar_4_c, .-vector_fmul_sv_scalar_4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	sv_fmul_scalar_2_c
.type	sv_fmul_scalar_2_c, @function
sv_fmul_scalar_2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1270
mtc1	$6,$f1

addiu	$3,$7,-1
addiu	$2,$4,4
srl	$3,$3,1
sll	$3,$3,3
addiu	$3,$3,12
addu	$4,$4,$3
$L1267:
lw	$3,0($5)
addiu	$2,$2,8
addiu	$5,$5,4
lwc1	$f0,0($3)
mul.s	$f0,$f0,$f1
swc1	$f0,-12($2)
lwc1	$f0,4($3)
mul.s	$f0,$f0,$f1
bne	$2,$4,$L1267
swc1	$f0,-8($2)

$L1270:
j	$31
nop

.set	macro
.set	reorder
.end	sv_fmul_scalar_2_c
.size	sv_fmul_scalar_2_c, .-sv_fmul_scalar_2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	sv_fmul_scalar_4_c
.type	sv_fmul_scalar_4_c, @function
sv_fmul_scalar_4_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1276
mtc1	$6,$f0

move	$3,$5
$L1273:
lw	$2,0($3)
addiu	$4,$4,16
addiu	$3,$3,4
lwc1	$f1,0($2)
subu	$6,$3,$5
slt	$6,$6,$7
mul.s	$f1,$f1,$f0
swc1	$f1,-16($4)
lwc1	$f1,4($2)
mul.s	$f1,$f1,$f0
swc1	$f1,-12($4)
lwc1	$f1,8($2)
mul.s	$f1,$f1,$f0
swc1	$f1,-8($4)
lwc1	$f1,12($2)
mul.s	$f1,$f1,$f0
bne	$6,$0,$L1273
swc1	$f1,-4($4)

$L1276:
j	$31
nop

.set	macro
.set	reorder
.end	sv_fmul_scalar_4_c
.size	sv_fmul_scalar_4_c, .-sv_fmul_scalar_4_c
.align	2
.set	nomips16
.set	nomicromips
.ent	butterflies_float_c
.type	butterflies_float_c, @function
butterflies_float_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$6,$L1282
sll	$6,$6,2

addu	$6,$4,$6
$L1279:
addiu	$5,$5,4
lwc1	$f0,0($4)
addiu	$4,$4,4
lwc1	$f1,-4($5)
add.s	$f2,$f1,$f0
sub.s	$f0,$f0,$f1
swc1	$f2,-4($4)
bne	$4,$6,$L1279
swc1	$f0,-4($5)

$L1282:
j	$31
nop

.set	macro
.set	reorder
.end	butterflies_float_c
.size	butterflies_float_c, .-butterflies_float_c
.align	2
.set	nomips16
.set	nomicromips
.ent	scalarproduct_float_c
.type	scalarproduct_float_c, @function
scalarproduct_float_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$6,$L1289
mtc1	$0,$f0

sll	$6,$6,2
move	$2,$0
$L1285:
addu	$7,$5,$2
addu	$3,$4,$2
lwc1	$f1,0($7)
addiu	$2,$2,4
lwc1	$f2,0($3)
mul.s	$f1,$f1,$f2
bne	$2,$6,$L1285
add.s	$f0,$f0,$f1

$L1289:
j	$31
nop

.set	macro
.set	reorder
.end	scalarproduct_float_c
.size	scalarproduct_float_c, .-scalarproduct_float_c
.align	2
.set	nomips16
.set	nomicromips
.ent	int32_to_float_fmul_scalar_c
.type	int32_to_float_fmul_scalar_c, @function
int32_to_float_fmul_scalar_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1295
sll	$7,$7,2

move	$3,$0
$L1292:
addu	$2,$5,$3
addu	$8,$4,$3
addiu	$3,$3,4
lw	$2,0($2)
mtc1	$2,$f1
cvt.s.w	$f0,$f1
mtc1	$6,$f1
mul.s	$f0,$f0,$f1
bne	$3,$7,$L1292
swc1	$f0,0($8)

$L1295:
j	$31
nop

.set	macro
.set	reorder
.end	int32_to_float_fmul_scalar_c
.size	int32_to_float_fmul_scalar_c, .-int32_to_float_fmul_scalar_c
.align	2
.set	nomips16
.set	nomicromips
.ent	vector_clipf_c
.type	vector_clipf_c, @function
vector_clipf_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sw	$6,8($sp)
mtc1	$0,$f0
lwc1	$f1,8($sp)
sw	$7,12($sp)
c.lt.s	$fcc0,$f1,$f0
bc1t	$fcc0,$L1331
lw	$6,16($sp)

$L1297:
blez	$6,$L1350
addiu	$3,$6,-1

addiu	$2,$5,4
srl	$3,$3,3
sll	$3,$3,5
addiu	$3,$3,36
addu	$5,$5,$3
$L1311:
lwc1	$f0,-4($2)
addiu	$4,$4,32
addiu	$2,$2,32
lwc1	$f1,12($sp)
c.le.s	$fcc2,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc3,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc2
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc3
lwc1	$f1,12($sp)
swc1	$f0,-32($4)
lwc1	$f0,-32($2)
c.le.s	$fcc4,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc5,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc4
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc5
lwc1	$f1,12($sp)
swc1	$f0,-28($4)
lwc1	$f0,-28($2)
c.le.s	$fcc6,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc7,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc6
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc7
lwc1	$f1,12($sp)
swc1	$f0,-24($4)
lwc1	$f0,-24($2)
c.le.s	$fcc0,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc1,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc0
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc1
lwc1	$f1,12($sp)
swc1	$f0,-20($4)
lwc1	$f0,-20($2)
c.le.s	$fcc2,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc3,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc2
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc3
lwc1	$f1,12($sp)
swc1	$f0,-16($4)
lwc1	$f0,-16($2)
c.le.s	$fcc4,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc5,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc4
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc5
lwc1	$f1,12($sp)
swc1	$f0,-12($4)
lwc1	$f0,-12($2)
c.le.s	$fcc6,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc7,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc6
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc7
lwc1	$f1,12($sp)
swc1	$f0,-8($4)
lwc1	$f0,-8($2)
c.le.s	$fcc0,$f0,$f1
lwc1	$f1,8($sp)
c.lt.s	$fcc1,$f0,$f1
lwc1	$f1,12($sp)
movf.s	$f0,$f1,$fcc0
lwc1	$f1,8($sp)
movt.s	$f0,$f1,$fcc1
bne	$2,$5,$L1311
swc1	$f0,-4($4)

$L1350:
j	$31
nop

$L1331:
mtc1	$7,$f1
c.lt.s	$fcc1,$f0,$f1
bc1f	$fcc1,$L1297
lw	$3,12($sp)

li	$8,-2147483648			# 0xffffffff80000000
lw	$2,8($sp)
blez	$6,$L1350
xor	$8,$3,$8

addiu	$7,$6,-1
li	$9,-2147483648			# 0xffffffff80000000
srl	$7,$7,3
addiu	$6,$4,24
sll	$7,$7,5
addiu	$7,$7,56
.option	pic0
j	$L1310
.option	pic2
addu	$4,$4,$7

$L1334:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,-24($6)
lw	$7,4($5)
sltu	$10,$2,$7
bne	$10,$0,$L1314
xor	$10,$7,$9

$L1342:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,-20($6)
lw	$7,8($5)
sltu	$10,$2,$7
bne	$10,$0,$L1316
xor	$10,$7,$9

$L1343:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,-16($6)
lw	$7,12($5)
sltu	$10,$2,$7
bne	$10,$0,$L1318
xor	$10,$7,$9

$L1344:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,-12($6)
lw	$7,16($5)
sltu	$10,$2,$7
bne	$10,$0,$L1320
xor	$10,$7,$9

$L1345:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,-8($6)
lw	$7,20($5)
sltu	$10,$2,$7
bne	$10,$0,$L1322
xor	$10,$7,$9

$L1346:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,-4($6)
lw	$7,24($5)
sltu	$10,$2,$7
bne	$10,$0,$L1324
xor	$10,$7,$9

$L1347:
sltu	$10,$8,$10
movn	$7,$3,$10
sw	$7,0($6)
lw	$7,28($5)
sltu	$10,$2,$7
bne	$10,$0,$L1326
xor	$10,$7,$9

$L1348:
sltu	$10,$8,$10
movn	$7,$3,$10
addiu	$6,$6,32
addiu	$5,$5,32
beq	$6,$4,$L1350
sw	$7,-28($6)

$L1310:
lw	$7,0($5)
sltu	$10,$2,$7
beq	$10,$0,$L1334
xor	$10,$7,$9

move	$7,$2
sw	$7,-24($6)
lw	$7,4($5)
sltu	$10,$2,$7
beq	$10,$0,$L1342
xor	$10,$7,$9

$L1314:
move	$7,$2
sw	$2,-20($6)
lw	$7,8($5)
sltu	$10,$2,$7
beq	$10,$0,$L1343
xor	$10,$7,$9

$L1316:
move	$7,$2
sw	$2,-16($6)
lw	$7,12($5)
sltu	$10,$2,$7
beq	$10,$0,$L1344
xor	$10,$7,$9

$L1318:
move	$7,$2
sw	$2,-12($6)
lw	$7,16($5)
sltu	$10,$2,$7
beq	$10,$0,$L1345
xor	$10,$7,$9

$L1320:
move	$7,$2
sw	$2,-8($6)
lw	$7,20($5)
sltu	$10,$2,$7
beq	$10,$0,$L1346
xor	$10,$7,$9

$L1322:
move	$7,$2
sw	$2,-4($6)
lw	$7,24($5)
sltu	$10,$2,$7
beq	$10,$0,$L1347
xor	$10,$7,$9

$L1324:
move	$7,$2
sw	$2,0($6)
lw	$7,28($5)
sltu	$10,$2,$7
beq	$10,$0,$L1348
xor	$10,$7,$9

$L1326:
move	$7,$2
addiu	$6,$6,32
sw	$7,-28($6)
bne	$6,$4,$L1310
addiu	$5,$5,32

j	$31
nop

.set	macro
.set	reorder
.end	vector_clipf_c
.size	vector_clipf_c, .-vector_clipf_c
.align	2
.globl	ff_float_to_int16_c
.set	nomips16
.set	nomicromips
.ent	ff_float_to_int16_c
.type	ff_float_to_int16_c, @function
ff_float_to_int16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$6,$L1356
sll	$6,$6,2

li	$8,1136656384			# 0x43c00000
addu	$6,$5,$6
li	$9,983040			# 0xf0000
ori	$8,$8,0xffff
$L1353:
lw	$7,0($5)
addiu	$4,$4,2
addiu	$5,$5,4
subu	$2,$8,$7
and	$3,$7,$9
sra	$2,$2,31
movz	$2,$7,$3
addiu	$2,$2,-32768
bne	$5,$6,$L1353
sh	$2,-2($4)

$L1356:
j	$31
nop

.set	macro
.set	reorder
.end	ff_float_to_int16_c
.size	ff_float_to_int16_c, .-ff_float_to_int16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	scalarproduct_int16_c
.type	scalarproduct_int16_c, @function
scalarproduct_int16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$8,$6,-1
beq	$6,$0,$L1363
move	$2,$0

li	$9,-1			# 0xffffffffffffffff
$L1359:
addiu	$4,$4,2
lh	$6,0($5)
lh	$3,-2($4)
addiu	$8,$8,-1
addiu	$5,$5,2
mul	$3,$3,$6
sra	$3,$3,$7
bne	$8,$9,$L1359
addu	$2,$2,$3

$L1363:
j	$31
nop

.set	macro
.set	reorder
.end	scalarproduct_int16_c
.size	scalarproduct_int16_c, .-scalarproduct_int16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	scalarproduct_and_madd_int16_c
.type	scalarproduct_and_madd_int16_c, @function
scalarproduct_and_madd_int16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$9,16($sp)
addiu	$8,$7,-1
beq	$7,$0,$L1365
move	$12,$0

li	$10,-1			# 0xffffffffffffffff
$L1366:
addiu	$6,$6,2
lh	$3,-2($6)
addiu	$5,$5,2
lh	$7,0($4)
lh	$2,-2($5)
addiu	$4,$4,2
mul	$11,$3,$9
addiu	$8,$8,-1
addu	$3,$11,$7
mul	$11,$7,$2
sh	$3,-2($4)
bne	$8,$10,$L1366
addu	$12,$11,$12

$L1365:
j	$31
move	$2,$12

.set	macro
.set	reorder
.end	scalarproduct_and_madd_int16_c
.size	scalarproduct_and_madd_int16_c, .-scalarproduct_and_madd_int16_c
.align	2
.globl	ff_wmv2_idct_c
.set	nomips16
.set	nomicromips
.ent	ff_wmv2_idct_c
.type	ff_wmv2_idct_c, @function
ff_wmv2_idct_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lui	$7,%hi(coef_tab)
addiu	$7,$7,%lo(coef_tab)
#APP
# 4461 "dsputil.c" 1
.word	0b01110000111000000000001011010000	#S32LDD XR11,$7,0
# 0 "" 2
# 4462 "dsputil.c" 1
.word	0b01110000111000000000011100010000	#S32LDD XR12,$7,4
# 0 "" 2
# 4463 "dsputil.c" 1
.word	0b01110000111000000000101101010000	#S32LDD XR13,$7,8
# 0 "" 2
# 4464 "dsputil.c" 1
.word	0b01110000111000000000111110010000	#S32LDD XR14,$7,12
# 0 "" 2
#NO_APP
li	$8,181			# 0xb5
addiu	$9,$4,128
move	$2,$4
move	$3,$4
$L1370:
#APP
# 4291 "dsputil.c" 1
.word	0b01110000011000000000101001010000	#S32LDD XR9,$3,8
# 0 "" 2
# 4292 "dsputil.c" 1
.word	0b01110000011000000000011111010000	#S32LDD XR15,$3,4
# 0 "" 2
# 4293 "dsputil.c" 1
.word	0b01110000100110110010010100001000	#D16MUL XR4,XR9,XR12,XR6,HW
# 0 "" 2
# 4294 "dsputil.c" 1
.word	0b01110000011000000000111010010000	#S32LDD XR10,$3,12
# 0 "" 2
# 4295 "dsputil.c" 1
.word	0b01110001100100110011110110001010	#D16MAC XR6,XR15,XR12,XR4,AS,HW
# 0 "" 2
# 4296 "dsputil.c" 1
.word	0b01110000010011110111110111001000	#D16MUL XR7,XR15,XR13,XR3,LW
# 0 "" 2
# 4297 "dsputil.c" 1
.word	0b01110001010111110110100011001010	#D16MAC XR3,XR10,XR13,XR7,AS,LW
# 0 "" 2
# 4298 "dsputil.c" 1
.word	0b01110000011000000000001111010000	#S32LDD XR15,$3,0
# 0 "" 2
# 4299 "dsputil.c" 1
.word	0b01110000100010101111111000001000	#D16MUL XR8,XR15,XR11,XR2,HW
# 0 "" 2
# 4300 "dsputil.c" 1
.word	0b01110001101000101110100010001010	#D16MAC XR2,XR10,XR11,XR8,AS,HW
# 0 "" 2
# 4301 "dsputil.c" 1
.word	0b01110000011010000001001111101010	#S16LDD XR15,$3,8,PTN1
# 0 "" 2
# 4302 "dsputil.c" 1
.word	0b01110000010001111111100101001000	#D16MUL XR5,XR14,XR15,XR1,LW
# 0 "" 2
# 4303 "dsputil.c" 1
.word	0b01110001000101010100010001011000	#D32ADD XR1,XR1,XR5,XR5,AS
# 0 "" 2
# 4305 "dsputil.c" 1
.word	0b01110001000011001100010001011000	#D32ADD XR1,XR1,XR3,XR3,AS
# 0 "" 2
# 4306 "dsputil.c" 1
.word	0b01110001000110011000100010011000	#D32ADD XR2,XR2,XR6,XR6,AS
# 0 "" 2
# 4307 "dsputil.c" 1
.word	0b01110001001000100001000100011000	#D32ADD XR4,XR4,XR8,XR8,AS
# 0 "" 2
# 4308 "dsputil.c" 1
.word	0b01110001000111011101010101011000	#D32ADD XR5,XR5,XR7,XR7,AS
# 0 "" 2
# 4310 "dsputil.c" 1
.word	0b01110001001000100001100110011000	#D32ADD XR6,XR6,XR8,XR8,AS
# 0 "" 2
# 4311 "dsputil.c" 1
.word	0b01110001000010001000010001011000	#D32ADD XR1,XR1,XR2,XR2,AS
# 0 "" 2
# 4312 "dsputil.c" 1
.word	0b01110001000100010000110011011000	#D32ADD XR3,XR3,XR4,XR4,AS
# 0 "" 2
# 4313 "dsputil.c" 1
.word	0b01110000000001010000000110101110	#S32M2I XR6, $5
# 0 "" 2
# 4314 "dsputil.c" 1
.word	0b01110000000001100000001000101110	#S32M2I XR8, $6
# 0 "" 2
#NO_APP
mul	$6,$6,$8
mul	$5,$5,$8
addiu	$6,$6,128
addiu	$5,$5,128
sra	$6,$6,8
#APP
# 4318 "dsputil.c" 1
.word	0b01110000000001100000001000101111	#S32I2M XR8,$6
# 0 "" 2
#NO_APP
sra	$5,$5,8
#APP
# 4319 "dsputil.c" 1
.word	0b01110000000001010000000110101111	#S32I2M XR6,$5
# 0 "" 2
# 4320 "dsputil.c" 1
.word	0b01110001001000100001010101011000	#D32ADD XR5,XR5,XR8,XR8,AS
# 0 "" 2
# 4321 "dsputil.c" 1
.word	0b01110000111000000001001111010000	#S32LDD XR15,$7,16
# 0 "" 2
# 4322 "dsputil.c" 1
.word	0b01110001000111011001110110011000	#D32ADD XR6,XR7,XR6,XR7,AS
# 0 "" 2
# 4324 "dsputil.c" 1
.word	0b01110000100010111111110001011001	#D32ASUM XR1,XR15,XR15,XR2,AA
# 0 "" 2
# 4325 "dsputil.c" 1
.word	0b01110000100100111111110011011001	#D32ASUM XR3,XR15,XR15,XR4,AA
# 0 "" 2
# 4326 "dsputil.c" 1
.word	0b01110000100110111111110101011001	#D32ASUM XR5,XR15,XR15,XR6,AA
# 0 "" 2
# 4327 "dsputil.c" 1
.word	0b01110000101000111111110111011001	#D32ASUM XR7,XR15,XR15,XR8,AA
# 0 "" 2
# 4328 "dsputil.c" 1
.word	0b01110010000000000101010001110010	#D32SARL XR1,XR5,XR1,8
# 0 "" 2
# 4329 "dsputil.c" 1
.word	0b01110010000000011000110011110010	#D32SARL XR3,XR3,XR6,8
# 0 "" 2
# 4330 "dsputil.c" 1
.word	0b01110010000000010001110100110010	#D32SARL XR4,XR7,XR4,8
# 0 "" 2
# 4331 "dsputil.c" 1
.word	0b01110010000000100000100010110010	#D32SARL XR2,XR2,XR8,8
# 0 "" 2
# 4332 "dsputil.c" 1
.word	0b01110000011000000000000001010001	#S32STD XR1,$3,0
# 0 "" 2
# 4333 "dsputil.c" 1
.word	0b01110000011000000000010011010001	#S32STD XR3,$3,4
# 0 "" 2
# 4334 "dsputil.c" 1
.word	0b01110000011000000000100100010001	#S32STD XR4,$3,8
# 0 "" 2
# 4335 "dsputil.c" 1
.word	0b01110000011000000000110010010001	#S32STD XR2,$3,12
# 0 "" 2
#NO_APP
addiu	$3,$3,16
.set	noreorder
.set	nomacro
bne	$3,$9,$L1370
li	$6,181			# 0xb5
.set	macro
.set	reorder

addiu	$4,$4,16
$L1371:
#APP
# 4341 "dsputil.c" 1
.word	0b01110000010010001010001001101010	#S16LDD XR9,$2,80,PTN1
# 0 "" 2
# 4342 "dsputil.c" 1
.word	0b01110000010010000110001111101010	#S16LDD XR15,$2,48,PTN1
# 0 "" 2
# 4343 "dsputil.c" 1
.word	0b01110000100110110010010100001000	#D16MUL XR4,XR9,XR12,XR6,HW
# 0 "" 2
# 4344 "dsputil.c" 1
.word	0b01110000010000000100001111101010	#S16LDD XR15,$2,32,PTN0
# 0 "" 2
# 4345 "dsputil.c" 1
.word	0b01110000010000001000001001101010	#S16LDD XR9,$2,64,PTN0
# 0 "" 2
# 4346 "dsputil.c" 1
.word	0b01110001100100110011110110001010	#D16MAC XR6,XR15,XR12,XR4,AS,HW
# 0 "" 2
# 4347 "dsputil.c" 1
.word	0b01110000010000001100001010101010	#S16LDD XR10,$2,96,PTN0
# 0 "" 2
# 4348 "dsputil.c" 1
.word	0b01110000010010001110001010101010	#S16LDD XR10,$2,112,PTN1
# 0 "" 2
# 4349 "dsputil.c" 1
.word	0b01110000010011110111110111001000	#D16MUL XR7,XR15,XR13,XR3,LW
# 0 "" 2
# 4350 "dsputil.c" 1
.word	0b01110001010111110110100011001010	#D16MAC XR3,XR10,XR13,XR7,AS,LW
# 0 "" 2
# 4351 "dsputil.c" 1
.word	0b01110000010010000010001111101010	#S16LDD XR15,$2,16,PTN1
# 0 "" 2
# 4352 "dsputil.c" 1
.word	0b01110000100010101111111000001000	#D16MUL XR8,XR15,XR11,XR2,HW
# 0 "" 2
# 4353 "dsputil.c" 1
.word	0b01110001101000101110100010001010	#D16MAC XR2,XR10,XR11,XR8,AS,HW
# 0 "" 2
# 4354 "dsputil.c" 1
.word	0b01110000010010000000001001101010	#S16LDD XR9,$2,0,PTN1
# 0 "" 2
# 4355 "dsputil.c" 1
.word	0b01110011000000111000001111111101	#S32SFL XR15,XR0,XR14,XR0,PTN3
# 0 "" 2
# 4356 "dsputil.c" 1
.word	0b01110000010101100111100001001000	#D16MUL XR1,XR14,XR9,XR5,LW
# 0 "" 2
# 4357 "dsputil.c" 1
.word	0b01110001000101010100010001011000	#D32ADD XR1,XR1,XR5,XR5,AS
# 0 "" 2
# 4359 "dsputil.c" 1
.word	0b01110000100100111111110011011001	#D32ASUM XR3,XR15,XR15,XR4,AA
# 0 "" 2
# 4360 "dsputil.c" 1
.word	0b01110000100110111111110010011001	#D32ASUM XR2,XR15,XR15,XR6,AA
# 0 "" 2
# 4361 "dsputil.c" 1
.word	0b01110000101000111111110111011001	#D32ASUM XR7,XR15,XR15,XR8,AA
# 0 "" 2
# 4362 "dsputil.c" 1
.word	0b01110000110010001000010001110011	#D32SAR XR1,XR1,XR2,XR2,3
# 0 "" 2
# 4363 "dsputil.c" 1
.word	0b01110000110100010000110011110011	#D32SAR XR3,XR3,XR4,XR4,3
# 0 "" 2
# 4364 "dsputil.c" 1
.word	0b01110000110110011001010101110011	#D32SAR XR5,XR5,XR6,XR6,3
# 0 "" 2
# 4365 "dsputil.c" 1
.word	0b01110000111000100001110111110011	#D32SAR XR7,XR7,XR8,XR8,3
# 0 "" 2
# 4367 "dsputil.c" 1
.word	0b01110001000011001100010001011000	#D32ADD XR1,XR1,XR3,XR3,AS
# 0 "" 2
# 4368 "dsputil.c" 1
.word	0b01110001000110011000100010011000	#D32ADD XR2,XR2,XR6,XR6,AS
# 0 "" 2
# 4369 "dsputil.c" 1
.word	0b01110001001000100001000100011000	#D32ADD XR4,XR4,XR8,XR8,AS
# 0 "" 2
# 4370 "dsputil.c" 1
.word	0b01110001000111011101010101011000	#D32ADD XR5,XR5,XR7,XR7,AS
# 0 "" 2
# 4372 "dsputil.c" 1
.word	0b01110001001000100001100110011000	#D32ADD XR6,XR6,XR8,XR8,AS
# 0 "" 2
# 4373 "dsputil.c" 1
.word	0b01110001000010001000010001011000	#D32ADD XR1,XR1,XR2,XR2,AS
# 0 "" 2
# 4374 "dsputil.c" 1
.word	0b01110001000100010000110011011000	#D32ADD XR3,XR3,XR4,XR4,AS
# 0 "" 2
# 4375 "dsputil.c" 1
.word	0b01110000000000110000000110101110	#S32M2I XR6, $3
# 0 "" 2
# 4376 "dsputil.c" 1
.word	0b01110000000001010000001000101110	#S32M2I XR8, $5
# 0 "" 2
#NO_APP
mul	$5,$5,$6
mul	$3,$3,$6
addiu	$5,$5,128
addiu	$3,$3,128
sra	$5,$5,8
#APP
# 4380 "dsputil.c" 1
.word	0b01110000000001010000001000101111	#S32I2M XR8,$5
# 0 "" 2
#NO_APP
sra	$3,$3,8
#APP
# 4381 "dsputil.c" 1
.word	0b01110000000000110000000110101111	#S32I2M XR6,$3
# 0 "" 2
# 4382 "dsputil.c" 1
.word	0b01110001001000100001010101011000	#D32ADD XR5,XR5,XR8,XR8,AS
# 0 "" 2
# 4383 "dsputil.c" 1
.word	0b01110000111000000001011111010000	#S32LDD XR15,$7,20
# 0 "" 2
# 4384 "dsputil.c" 1
.word	0b01110001000111011001110110011000	#D32ADD XR6,XR7,XR6,XR7,AS
# 0 "" 2
# 4386 "dsputil.c" 1
.word	0b01110000100010111111110001011001	#D32ASUM XR1,XR15,XR15,XR2,AA
# 0 "" 2
# 4387 "dsputil.c" 1
.word	0b01110000100100111111110011011001	#D32ASUM XR3,XR15,XR15,XR4,AA
# 0 "" 2
# 4388 "dsputil.c" 1
.word	0b01110000100110111111110101011001	#D32ASUM XR5,XR15,XR15,XR6,AA
# 0 "" 2
# 4389 "dsputil.c" 1
.word	0b01110000101000111111110111011001	#D32ASUM XR7,XR15,XR15,XR8,AA
# 0 "" 2
# 4390 "dsputil.c" 1
.word	0b01110011100000000101010001110010	#D32SARL XR1,XR5,XR1,14
# 0 "" 2
# 4391 "dsputil.c" 1
.word	0b01110011100000011000110011110010	#D32SARL XR3,XR3,XR6,14
# 0 "" 2
# 4392 "dsputil.c" 1
.word	0b01110011100000010001110100110010	#D32SARL XR4,XR7,XR4,14
# 0 "" 2
# 4393 "dsputil.c" 1
.word	0b01110011100000100000100010110010	#D32SARL XR2,XR2,XR8,14
# 0 "" 2
# 4394 "dsputil.c" 1
.word	0b01110000010000000000000001101011	#S16STD XR1,$2,0,PTN0
# 0 "" 2
# 4395 "dsputil.c" 1
.word	0b01110000010010000010000001101011	#S16STD XR1,$2,16,PTN1
# 0 "" 2
# 4396 "dsputil.c" 1
.word	0b01110000010000000100000011101011	#S16STD XR3,$2,32,PTN0
# 0 "" 2
# 4397 "dsputil.c" 1
.word	0b01110000010010000110000011101011	#S16STD XR3,$2,48,PTN1
# 0 "" 2
# 4398 "dsputil.c" 1
.word	0b01110000010000001000000100101011	#S16STD XR4,$2,64,PTN0
# 0 "" 2
# 4399 "dsputil.c" 1
.word	0b01110000010010001010000100101011	#S16STD XR4,$2,80,PTN1
# 0 "" 2
# 4400 "dsputil.c" 1
.word	0b01110000010000001100000010101011	#S16STD XR2,$2,96,PTN0
# 0 "" 2
# 4401 "dsputil.c" 1
.word	0b01110000010010001110000010101011	#S16STD XR2,$2,112,PTN1
# 0 "" 2
#NO_APP
addiu	$2,$2,2
bne	$2,$4,$L1371
j	$31
.end	ff_wmv2_idct_c
.size	ff_wmv2_idct_c, .-ff_wmv2_idct_c
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_wmv2_idct_put_c
.type	ff_wmv2_idct_put_c, @function
ff_wmv2_idct_put_c:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$4,$6
move	$17,$5
sw	$16,24($sp)
sw	$31,36($sp)
.option	pic0
jal	ff_wmv2_idct_c
.option	pic2
move	$16,$6

move	$4,$16
lw	$31,36($sp)
move	$5,$18
lw	$16,24($sp)
move	$6,$17
lw	$18,32($sp)
lw	$17,28($sp)
.option	pic0
j	put_pixels_clamped_c
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_wmv2_idct_put_c
.size	ff_wmv2_idct_put_c, .-ff_wmv2_idct_put_c
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_wmv2_idct_add_c
.type	ff_wmv2_idct_add_c, @function
ff_wmv2_idct_add_c:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$4,$6
move	$17,$5
sw	$16,24($sp)
sw	$31,36($sp)
.option	pic0
jal	ff_wmv2_idct_c
.option	pic2
move	$16,$6

move	$4,$16
lw	$31,36($sp)
move	$5,$18
lw	$16,24($sp)
move	$6,$17
lw	$18,32($sp)
lw	$17,28($sp)
.option	pic0
j	add_pixels_clamped_c
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_wmv2_idct_add_c
.size	ff_wmv2_idct_add_c, .-ff_wmv2_idct_add_c
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct1_put
.type	ff_jref_idct1_put, @function
ff_jref_idct1_put:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lh	$2,0($6)
addiu	$2,$2,4
sra	$3,$2,3
lui	$2,%hi(ff_cropTbl+1024)
addiu	$2,$2,%lo(ff_cropTbl+1024)
addu	$2,$2,$3
lbu	$2,0($2)
j	$31
sb	$2,0($4)

.set	macro
.set	reorder
.end	ff_jref_idct1_put
.size	ff_jref_idct1_put, .-ff_jref_idct1_put
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct1_add
.type	ff_jref_idct1_add, @function
ff_jref_idct1_add:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lh	$3,0($6)
lbu	$2,0($4)
addiu	$3,$3,4
sra	$3,$3,3
addu	$3,$2,$3
lui	$2,%hi(ff_cropTbl+1024)
addiu	$2,$2,%lo(ff_cropTbl+1024)
addu	$2,$2,$3
lbu	$2,0($2)
j	$31
sb	$2,0($4)

.set	macro
.set	reorder
.end	ff_jref_idct1_add
.size	ff_jref_idct1_add, .-ff_jref_idct1_add
.align	2
.set	nomips16
.set	nomicromips
.ent	just_return
.type	just_return, @function
just_return:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
j	$31
nop

.set	macro
.set	reorder
.end	just_return
.size	just_return, .-just_return
.align	2
.set	nomips16
.set	nomicromips
.ent	fill_block8_c
.type	fill_block8_c, @function
fill_block8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
andi	$5,$5,0x00ff
blez	$7,$L1387
move	$2,$0

$L1385:
addiu	$2,$2,1
sb	$5,0($4)
sb	$5,1($4)
sb	$5,2($4)
sb	$5,3($4)
sb	$5,4($4)
sb	$5,5($4)
sb	$5,6($4)
sb	$5,7($4)
bne	$2,$7,$L1385
addu	$4,$4,$6

$L1387:
j	$31
nop

.set	macro
.set	reorder
.end	fill_block8_c
.size	fill_block8_c, .-fill_block8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	fill_block16_c
.type	fill_block16_c, @function
fill_block16_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-48
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$19,40($sp)
move	$3,$4
sw	$18,36($sp)
move	$19,$6
sw	$17,32($sp)
andi	$18,$5,0x00ff
sw	$16,28($sp)
move	$17,$7
sw	$31,44($sp)
move	$16,$0
blez	$7,$L1388
.cprestore	16

$L1392:
lw	$25,%call16(memset)($28)
li	$6,16			# 0x10
move	$4,$3
move	$5,$18
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$16,$16,1

lw	$28,16($sp)
bne	$16,$17,$L1392
addu	$3,$2,$19

$L1388:
lw	$31,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	fill_block16_c
.size	fill_block16_c, .-fill_block16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	clear_blocks_c
.type	clear_blocks_c, @function
clear_blocks_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
move	$5,$0
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jr	$25
li	$6,768			# 0x300

.set	macro
.set	reorder
.end	clear_blocks_c
.size	clear_blocks_c, .-clear_blocks_c
.align	2
.set	nomips16
.set	nomicromips
.ent	clear_block_c
.type	clear_block_c, @function
clear_block_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
move	$5,$0
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jr	$25
li	$6,128			# 0x80

.set	macro
.set	reorder
.end	clear_block_c
.size	clear_block_c, .-clear_block_c
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct4_add
.type	ff_jref_idct4_add, @function
ff_jref_idct4_add:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,28($sp)
move	$17,$4
sw	$16,24($sp)
move	$16,$6
sw	$31,36($sp)
move	$4,$6
sw	$18,32($sp)
.cprestore	16
lw	$25,%call16(j_rev_dct4)($28)
.reloc	1f,R_MIPS_JALR,j_rev_dct4
1:	jalr	$25
move	$18,$5

lui	$2,%hi(ff_cropTbl+1024)
lh	$9,0($16)
addu	$6,$17,$18
lbu	$4,0($17)
addiu	$2,$2,%lo(ff_cropTbl+1024)
lbu	$8,1($17)
addu	$3,$6,$18
lbu	$7,2($17)
addu	$9,$4,$9
lbu	$4,3($17)
addu	$5,$3,$18
addu	$9,$2,$9
lbu	$9,0($9)
sb	$9,0($17)
lh	$9,2($16)
addu	$8,$8,$9
addu	$8,$2,$8
lbu	$8,0($8)
sb	$8,1($17)
lh	$8,4($16)
addu	$7,$7,$8
addu	$7,$2,$7
lbu	$7,0($7)
sb	$7,2($17)
lh	$7,6($16)
addu	$4,$4,$7
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,3($17)
lh	$9,16($16)
lbu	$4,0($6)
lbu	$8,1($6)
lbu	$7,2($6)
addu	$9,$4,$9
lbu	$4,3($6)
addu	$9,$2,$9
lbu	$9,0($9)
sb	$9,0($6)
lh	$9,18($16)
addu	$8,$8,$9
addu	$8,$2,$8
lbu	$8,0($8)
sb	$8,1($6)
lh	$8,20($16)
addu	$7,$7,$8
addu	$7,$2,$7
lbu	$7,0($7)
sb	$7,2($6)
lh	$7,22($16)
addu	$4,$4,$7
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,3($6)
lbu	$4,0($3)
lh	$8,32($16)
lbu	$7,1($3)
lbu	$6,2($3)
addu	$8,$4,$8
lbu	$4,3($3)
addu	$8,$2,$8
lbu	$8,0($8)
sb	$8,0($3)
lh	$8,34($16)
addu	$7,$7,$8
addu	$7,$2,$7
lbu	$7,0($7)
sb	$7,1($3)
lh	$7,36($16)
addu	$6,$6,$7
addu	$6,$2,$6
lbu	$6,0($6)
sb	$6,2($3)
lh	$6,38($16)
addu	$4,$4,$6
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,3($3)
lh	$7,48($16)
lbu	$6,0($5)
lbu	$4,1($5)
lbu	$3,2($5)
addu	$6,$6,$7
lbu	$7,3($5)
addu	$6,$2,$6
lbu	$6,0($6)
sb	$6,0($5)
lh	$6,50($16)
addu	$4,$4,$6
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,1($5)
lh	$4,52($16)
addu	$3,$3,$4
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,2($5)
lh	$3,54($16)
addu	$3,$7,$3
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,3($5)
lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_jref_idct4_add
.size	ff_jref_idct4_add, .-ff_jref_idct4_add
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct4_put
.type	ff_jref_idct4_put, @function
ff_jref_idct4_put:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,36($sp)
sw	$18,32($sp)
move	$18,$5
sw	$17,28($sp)
move	$17,$4
sw	$16,24($sp)
move	$4,$6
.cprestore	16
lw	$25,%call16(j_rev_dct4)($28)
.reloc	1f,R_MIPS_JALR,j_rev_dct4
1:	jalr	$25
move	$16,$6

lui	$2,%hi(ff_cropTbl+1024)
lh	$4,0($16)
addu	$6,$17,$18
addiu	$2,$2,%lo(ff_cropTbl+1024)
addu	$3,$6,$18
addu	$4,$2,$4
addu	$5,$3,$18
lbu	$4,0($4)
sb	$4,0($17)
lh	$4,2($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,1($17)
lh	$4,4($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,2($17)
lh	$4,6($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,3($17)
lh	$4,16($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,0($6)
lh	$4,18($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,1($6)
lh	$4,20($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,2($6)
lh	$4,22($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,3($6)
lh	$4,32($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,0($3)
lh	$4,34($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,1($3)
lh	$4,36($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,2($3)
lh	$4,38($16)
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,3($3)
lh	$3,48($16)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($5)
lh	$3,50($16)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,1($5)
lh	$3,52($16)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,2($5)
lh	$3,54($16)
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,3($5)
lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_jref_idct4_put
.size	ff_jref_idct4_put, .-ff_jref_idct4_put
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct2_add
.type	ff_jref_idct2_add, @function
ff_jref_idct2_add:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$17,$6
sw	$31,36($sp)
move	$4,$6
sw	$16,24($sp)
.cprestore	16
lw	$25,%call16(j_rev_dct2)($28)
.reloc	1f,R_MIPS_JALR,j_rev_dct2
1:	jalr	$25
move	$16,$5

lui	$2,%hi(ff_cropTbl+1024)
lh	$6,0($17)
addu	$5,$18,$16
lbu	$4,0($18)
addiu	$2,$2,%lo(ff_cropTbl+1024)
lbu	$3,1($18)
addu	$4,$4,$6
addu	$4,$2,$4
lbu	$4,0($4)
sb	$4,0($18)
lh	$4,2($17)
addu	$3,$3,$4
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,1($18)
lh	$6,16($17)
lbu	$3,0($5)
lbu	$4,1($5)
addu	$3,$3,$6
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($5)
lh	$3,18($17)
addu	$3,$4,$3
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,1($5)
lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_jref_idct2_add
.size	ff_jref_idct2_add, .-ff_jref_idct2_add
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct2_put
.type	ff_jref_idct2_put, @function
ff_jref_idct2_put:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,36($sp)
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$4,$6
sw	$16,24($sp)
move	$16,$6
.cprestore	16
lw	$25,%call16(j_rev_dct2)($28)
.reloc	1f,R_MIPS_JALR,j_rev_dct2
1:	jalr	$25
move	$17,$5

lui	$2,%hi(ff_cropTbl+1024)
lh	$3,0($16)
addu	$5,$18,$17
addiu	$2,$2,%lo(ff_cropTbl+1024)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($18)
lh	$3,2($16)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,1($18)
lh	$3,16($16)
addu	$3,$2,$3
lbu	$3,0($3)
sb	$3,0($5)
lh	$3,18($16)
addu	$2,$2,$3
lbu	$2,0($2)
sb	$2,1($5)
lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_jref_idct2_put
.size	ff_jref_idct2_put, .-ff_jref_idct2_put
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct_add
.type	ff_jref_idct_add, @function
ff_jref_idct_add:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,36($sp)
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$4,$6
sw	$16,24($sp)
move	$17,$5
.cprestore	16
lw	$25,%call16(j_rev_dct)($28)
.reloc	1f,R_MIPS_JALR,j_rev_dct
1:	jalr	$25
move	$16,$6

move	$4,$16
lw	$28,16($sp)
move	$5,$18
lw	$31,36($sp)
move	$6,$17
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.option	pic0
j	add_pixels_clamped_c
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_jref_idct_add
.size	ff_jref_idct_add, .-ff_jref_idct_add
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_jref_idct_put
.type	ff_jref_idct_put, @function
ff_jref_idct_put:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,36($sp)
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$4,$6
sw	$16,24($sp)
move	$17,$5
.cprestore	16
lw	$25,%call16(j_rev_dct)($28)
.reloc	1f,R_MIPS_JALR,j_rev_dct
1:	jalr	$25
move	$16,$6

move	$4,$16
lw	$28,16($sp)
move	$5,$18
lw	$31,36($sp)
move	$6,$17
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.option	pic0
j	put_pixels_clamped_c
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_jref_idct_put
.size	ff_jref_idct_put, .-ff_jref_idct_put
.align	2
.set	nomips16
.set	nomicromips
.ent	quant_psnr8x8_c
.type	quant_psnr8x8_c, @function
quant_psnr8x8_c:
.frame	$sp,328,$31		# vars= 280, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$25,2964($4)
addiu	$sp,$sp,-328
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$18,320($sp)
addiu	$2,$sp,47
sw	$17,316($sp)
move	$18,$4
sw	$16,312($sp)
sw	$31,324($sp)
.cprestore	24
sw	$0,8004($4)
li	$4,-16			# 0xfffffffffffffff0
and	$16,$2,$4
addiu	$17,$16,128
jalr	$25
move	$4,$16

move	$2,$16
move	$3,$17
$L1412:
lw	$7,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$6,-12($2)
lw	$5,-8($2)
lw	$4,-4($2)
sw	$7,-16($3)
sw	$6,-12($3)
sw	$5,-8($3)
bne	$2,$17,$L1412
sw	$4,-4($3)

addiu	$2,$sp,304
lw	$7,2872($18)
lw	$25,10576($18)
move	$6,$0
move	$4,$18
move	$5,$16
jalr	$25
sw	$2,16($sp)

move	$6,$0
lw	$7,2872($18)
move	$4,$18
lw	$25,10568($18)
move	$5,$16
jalr	$25
sw	$2,8680($18)

lw	$28,24($sp)
lw	$25,%call16(ff_simple_idct)($28)
.reloc	1f,R_MIPS_JALR,ff_simple_idct
1:	jalr	$25
move	$4,$16

mtlo	$0
move	$4,$16
$L1413:
lh	$3,0($4)
addiu	$4,$4,2
lh	$2,126($4)
subu	$3,$3,$2
bne	$4,$17,$L1413
madd	$3,$3

lw	$31,324($sp)
mflo	$2
lw	$18,320($sp)
lw	$17,316($sp)
lw	$16,312($sp)
j	$31
addiu	$sp,$sp,328

.set	macro
.set	reorder
.end	quant_psnr8x8_c
.size	quant_psnr8x8_c, .-quant_psnr8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	quant_psnr16_c
.type	quant_psnr16_c, @function
quant_psnr16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	quant_psnr8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	quant_psnr8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
beq	$3,$2,$L1420
lw	$31,60($sp)

move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

$L1420:
sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	quant_psnr8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	quant_psnr8x8_c
.option	pic2
addu	$16,$2,$16

lw	$31,60($sp)
addu	$16,$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
move	$2,$16
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	quant_psnr16_c
.size	quant_psnr16_c, .-quant_psnr16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	draw_edges_c
.type	draw_edges_c, @function
draw_edges_c:
.frame	$sp,80,$31		# vars= 16, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-80
lui	$28,%hi(__gnu_local_gp)
sw	$22,64($sp)
addiu	$22,$7,-1
sw	$16,40($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
mul	$2,$22,$5
sw	$21,60($sp)
sw	$20,56($sp)
move	$21,$4
sw	$19,52($sp)
move	$20,$7
sw	$17,44($sp)
move	$19,$6
sw	$31,76($sp)
move	$17,$5
sw	$fp,72($sp)
sw	$23,68($sp)
sw	$18,48($sp)
.cprestore	16
lw	$16,96($sp)
blez	$16,$L1422
addu	$22,$2,$4

subu	$fp,$4,$5
addu	$23,$22,$5
move	$18,$0
$L1423:
lw	$25,%call16(memcpy)($28)
move	$4,$fp
move	$5,$21
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$6,$19

addiu	$18,$18,1
lw	$28,16($sp)
move	$4,$23
move	$5,$22
move	$6,$19
subu	$fp,$fp,$17
lw	$25,%call16(memcpy)($28)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
addu	$23,$23,$17

bne	$18,$16,$L1423
lw	$28,16($sp)

blez	$20,$L1436
addiu	$2,$19,-1

subu	$2,$0,$16
$L1438:
addu	$fp,$21,$19
move	$23,$0
sw	$2,24($sp)
addiu	$2,$19,-1
move	$18,$21
sw	$2,28($sp)
$L1425:
lw	$2,24($sp)
move	$6,$16
lw	$25,%call16(memset)($28)
addiu	$23,$23,1
lbu	$5,0($18)
addu	$4,$18,$2
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$18,$18,$17

move	$4,$fp
lw	$28,16($sp)
move	$6,$16
lbu	$5,-1($fp)
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$fp,$fp,$17

slt	$2,$23,$20
bne	$2,$0,$L1425
lw	$28,16($sp)

blez	$16,$L1437
lw	$31,76($sp)

$L1426:
lw	$2,28($sp)
addu	$3,$16,$17
sll	$18,$16,1
subu	$20,$19,$17
addu	$2,$21,$2
subu	$18,$3,$18
subu	$fp,$21,$3
sw	$2,32($sp)
addu	$20,$21,$20
lw	$2,28($sp)
addu	$18,$22,$18
move	$23,$0
addu	$19,$16,$19
addu	$2,$22,$2
sw	$2,28($sp)
subu	$2,$0,$17
sw	$2,24($sp)
$L1427:
lw	$25,%call16(memset)($28)
move	$4,$fp
lbu	$5,0($21)
move	$6,$16
addiu	$23,$23,1
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
subu	$fp,$fp,$17

move	$4,$20
lw	$28,16($sp)
move	$6,$16
lw	$2,32($sp)
lw	$25,%call16(memset)($28)
lbu	$5,0($2)
lw	$2,24($sp)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$20,$20,$2

lw	$28,16($sp)
move	$4,$18
lbu	$5,0($22)
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$6,$16

addu	$4,$19,$18
lw	$28,16($sp)
move	$6,$16
lw	$2,28($sp)
addu	$18,$18,$17
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
lbu	$5,0($2)

slt	$2,$23,$16
bne	$2,$0,$L1427
lw	$28,16($sp)

lw	$31,76($sp)
$L1437:
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,80

$L1422:
bgtz	$7,$L1438
subu	$2,$0,$16

.option	pic0
j	$L1437
.option	pic2
lw	$31,76($sp)

$L1436:
.option	pic0
j	$L1426
.option	pic2
sw	$2,28($sp)

.set	macro
.set	reorder
.end	draw_edges_c
.size	draw_edges_c, .-draw_edges_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_tpel_pixels_mc00_c
.type	put_tpel_pixels_mc00_c, @function
put_tpel_pixels_mc00_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$10,4			# 0x4
lw	$9,16($sp)
move	$3,$4
move	$2,$5
.set	noreorder
.set	nomacro
beq	$7,$10,$L1441
move	$8,$6
.set	macro
.set	reorder

slt	$10,$7,5
.set	noreorder
.set	nomacro
bne	$10,$0,$L1459
li	$10,8			# 0x8
.set	macro
.set	reorder

beq	$7,$10,$L1444
li	$2,16			# 0x10
bne	$7,$2,$L1465
.option	pic0
.set	noreorder
.set	nomacro
j	put_pixels16_c
.option	pic2
move	$7,$9
.set	macro
.set	reorder

$L1444:
.set	noreorder
.set	nomacro
blez	$9,$L1466
move	$4,$0
.set	macro
.set	reorder

$L1454:
addiu	$4,$4,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
sw	$5,0($3)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($2)  
lwr $5, 4($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$8
sw	$5,4($3)
.set	noreorder
.set	nomacro
bne	$4,$9,$L1454
addu	$3,$3,$8
.set	macro
.set	reorder

$L1466:
j	$31
$L1459:
li	$4,2			# 0x2
bne	$7,$4,$L1465
.set	noreorder
.set	nomacro
blez	$9,$L1466
move	$5,$0
.set	macro
.set	reorder

$L1455:
lbu	$4,1($2)
addiu	$5,$5,1
lbu	$6,0($2)
addu	$2,$2,$8
sll	$4,$4,8
or	$4,$4,$6
sh	$4,0($3)
.set	noreorder
.set	nomacro
bne	$5,$9,$L1455
addu	$3,$3,$8
.set	macro
.set	reorder

j	$31
$L1441:
.set	noreorder
.set	nomacro
blez	$9,$L1466
move	$4,$0
.set	macro
.set	reorder

$L1453:
addiu	$4,$4,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$8
sw	$5,0($3)
.set	noreorder
.set	nomacro
bne	$4,$9,$L1453
addu	$3,$3,$8
.set	macro
.set	reorder

j	$31
$L1465:
j	$31
.end	put_tpel_pixels_mc00_c
.size	put_tpel_pixels_mc00_c, .-put_tpel_pixels_mc00_c
.align	2
.globl	ff_float_to_int16_interleave_c
.set	nomips16
.set	nomicromips
.ent	ff_float_to_int16_interleave_c
.type	ff_float_to_int16_interleave_c, @function
ff_float_to_int16_interleave_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$2,2			# 0x2
beq	$7,$2,$L1468
nop

blez	$7,$L1477
li	$12,1136656384			# 0x43c00000

sll	$14,$7,1
sll	$24,$6,2
move	$15,$0
li	$13,983040			# 0xf0000
ori	$12,$12,0xffff
$L1470:
blez	$6,$L1473
nop

lw	$8,0($5)
move	$10,$4
addu	$11,$8,$24
$L1472:
lw	$9,0($8)
addiu	$8,$8,4
subu	$2,$12,$9
and	$3,$9,$13
sra	$2,$2,31
movz	$2,$9,$3
addiu	$2,$2,-32768
sh	$2,0($10)
bne	$8,$11,$L1472
addu	$10,$10,$14

$L1473:
addiu	$15,$15,1
addiu	$4,$4,2
bne	$15,$7,$L1470
addiu	$5,$5,4

$L1477:
j	$31
nop

$L1468:
blez	$6,$L1477
li	$11,1136656384			# 0x43c00000

lw	$14,0($5)
lw	$13,4($5)
sll	$8,$6,2
move	$7,$0
li	$12,983040			# 0xf0000
ori	$11,$11,0xffff
$L1471:
addu	$3,$14,$7
addu	$2,$13,$7
addiu	$4,$4,4
lw	$10,0($3)
addiu	$7,$7,4
lw	$9,0($2)
subu	$5,$11,$10
subu	$2,$11,$9
sra	$5,$5,31
sra	$2,$2,31
and	$3,$9,$12
and	$6,$10,$12
movz	$2,$9,$3
movz	$5,$10,$6
addiu	$2,$2,-32768
addiu	$3,$5,-32768
sh	$2,-2($4)
bne	$7,$8,$L1471
sh	$3,-4($4)

j	$31
nop

.set	macro
.set	reorder
.end	ff_float_to_int16_interleave_c
.size	ff_float_to_int16_interleave_c, .-ff_float_to_int16_interleave_c
.align	2
.set	nomips16
.set	nomicromips
.ent	wmv2_mspel8_v_lowpass.constprop.3
.type	wmv2_mspel8_v_lowpass.constprop.3, @function
wmv2_mspel8_v_lowpass.constprop.3:
.frame	$sp,16,$31		# vars= 0, regs= 4/0, args= 0, gp= 0
.mask	0x000f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$9,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-16
subu	$11,$0,$6
addiu	$10,$4,8
sw	$19,12($sp)
addiu	$9,$9,%lo(ff_cropTbl+1024)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
$L1479:
addu	$2,$5,$6
lbu	$13,0($5)
addu	$8,$5,$11
addu	$3,$2,$6
lbu	$7,0($2)
addiu	$4,$4,1
addu	$2,$3,$6
lbu	$15,0($8)
lbu	$14,0($3)
addiu	$5,$5,1
addu	$18,$13,$7
lbu	$12,0($2)
addu	$2,$2,$6
sll	$3,$18,3
addu	$15,$15,$14
lbu	$8,0($2)
addu	$18,$3,$18
addu	$2,$2,$6
addu	$25,$7,$14
subu	$18,$18,$15
lbu	$16,0($2)
sll	$3,$25,3
addu	$2,$2,$6
addiu	$18,$18,8
addu	$3,$3,$25
addu	$25,$13,$12
lbu	$15,0($2)
addu	$13,$14,$12
addu	$2,$2,$6
sra	$18,$18,4
subu	$25,$3,$25
addu	$18,$9,$18
lbu	$17,0($2)
sll	$3,$13,3
addu	$2,$2,$6
addiu	$25,$25,8
lbu	$18,0($18)
addu	$24,$3,$13
addu	$13,$7,$8
lbu	$3,0($2)
addu	$19,$12,$8
sra	$25,$25,4
addu	$2,$2,$6
subu	$13,$24,$13
sll	$7,$19,3
addu	$24,$9,$25
lbu	$25,0($2)
addiu	$13,$13,8
sb	$18,-1($4)
addu	$7,$7,$19
addu	$2,$14,$16
lbu	$18,0($24)
addu	$14,$8,$16
sra	$13,$13,4
subu	$2,$7,$2
addu	$13,$9,$13
sb	$18,7($4)
sll	$7,$14,3
addiu	$2,$2,8
addu	$24,$7,$14
lbu	$7,0($13)
addu	$14,$12,$15
sra	$2,$2,4
addu	$12,$16,$15
subu	$14,$24,$14
sb	$7,15($4)
sll	$13,$12,3
addu	$2,$9,$2
addiu	$14,$14,8
addu	$13,$13,$12
addu	$12,$8,$17
lbu	$2,0($2)
addu	$7,$15,$17
sra	$14,$14,4
subu	$12,$13,$12
sll	$8,$7,3
sb	$2,23($4)
addu	$13,$9,$14
addiu	$12,$12,8
addu	$8,$8,$7
addu	$7,$16,$3
lbu	$13,0($13)
addu	$2,$17,$3
sra	$12,$12,4
subu	$7,$8,$7
sll	$3,$2,3
sb	$13,31($4)
addu	$8,$9,$12
addiu	$7,$7,8
addu	$3,$3,$2
addu	$2,$15,$25
lbu	$8,0($8)
sra	$7,$7,4
subu	$2,$3,$2
addu	$3,$9,$7
addiu	$2,$2,8
sb	$8,39($4)
lbu	$3,0($3)
sra	$2,$2,4
addu	$2,$9,$2
sb	$3,47($4)
lbu	$2,0($2)
bne	$4,$10,$L1479
sb	$2,55($4)

lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	wmv2_mspel8_v_lowpass.constprop.3
.size	wmv2_mspel8_v_lowpass.constprop.3, .-wmv2_mspel8_v_lowpass.constprop.3
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc02_c
.type	put_mspel8_mc02_c, @function
put_mspel8_mc02_c:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$9,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-24
subu	$11,$0,$6
addiu	$10,$5,8
sw	$21,20($sp)
addiu	$9,$9,%lo(ff_cropTbl+1024)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
$L1483:
addu	$3,$5,$6
lbu	$14,0($5)
addu	$7,$5,$11
addu	$2,$3,$6
lbu	$24,0($3)
addu	$19,$4,$6
addu	$3,$2,$6
lbu	$15,0($7)
lbu	$8,0($2)
addu	$7,$19,$6
addu	$17,$14,$24
lbu	$13,0($3)
addu	$3,$3,$6
sll	$2,$17,3
addu	$15,$15,$8
addu	$17,$2,$17
lbu	$12,0($3)
addu	$20,$24,$8
addu	$3,$3,$6
subu	$17,$17,$15
sll	$2,$20,3
lbu	$25,0($3)
addiu	$17,$17,8
addu	$3,$3,$6
addu	$2,$2,$20
addu	$20,$14,$13
addu	$18,$8,$13
lbu	$14,0($3)
sra	$17,$17,4
addu	$3,$3,$6
subu	$20,$2,$20
addu	$17,$9,$17
sll	$15,$18,3
lbu	$16,0($3)
addu	$2,$3,$6
addiu	$20,$20,8
lbu	$21,0($17)
addu	$15,$15,$18
addu	$18,$24,$12
lbu	$3,0($2)
sra	$20,$20,4
addu	$2,$2,$6
subu	$18,$15,$18
addu	$17,$13,$12
addu	$20,$9,$20
lbu	$24,0($2)
addiu	$18,$18,8
sb	$21,0($4)
sll	$15,$17,3
lbu	$20,0($20)
sra	$18,$18,4
addu	$2,$15,$17
addu	$15,$12,$25
addu	$17,$8,$25
addu	$18,$9,$18
sb	$20,0($19)
subu	$17,$2,$17
sll	$2,$15,3
addiu	$17,$17,8
addu	$8,$2,$15
lbu	$2,0($18)
addu	$15,$13,$14
sra	$17,$17,4
addu	$13,$25,$14
subu	$15,$8,$15
sb	$2,0($7)
addu	$8,$9,$17
sll	$2,$13,3
addiu	$15,$15,8
addu	$2,$2,$13
lbu	$17,0($8)
addu	$13,$12,$16
addu	$8,$14,$16
sra	$15,$15,4
subu	$13,$2,$13
sll	$12,$8,3
addu	$2,$7,$6
addu	$15,$9,$15
addiu	$13,$13,8
sb	$17,0($2)
addu	$12,$12,$8
addu	$8,$25,$3
lbu	$15,0($15)
addu	$3,$16,$3
sra	$13,$13,4
subu	$8,$12,$8
addu	$2,$2,$6
sll	$7,$3,3
addu	$12,$9,$13
sb	$15,0($2)
addiu	$8,$8,8
addu	$7,$7,$3
addu	$3,$14,$24
lbu	$12,0($12)
sra	$8,$8,4
subu	$3,$7,$3
addu	$2,$2,$6
addu	$7,$9,$8
addiu	$3,$3,8
sb	$12,0($2)
addu	$2,$2,$6
lbu	$8,0($7)
sra	$3,$3,4
addu	$7,$2,$6
addu	$3,$9,$3
addiu	$5,$5,1
sb	$8,0($2)
addiu	$4,$4,1
lbu	$2,0($3)
bne	$5,$10,$L1483
sb	$2,0($7)

lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,24

.set	macro
.set	reorder
.end	put_mspel8_mc02_c
.size	put_mspel8_mc02_c, .-put_mspel8_mc02_c
.align	2
.globl	ff_avg_pixels16x16_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_pixels16x16_c
.type	ff_avg_pixels16x16_c, @function
ff_avg_pixels16x16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,-4			# 0xfffffffffffffffc
andi	$3,$5,0x3
subu	$4,$4,$6
.set	noreorder
.set	nomacro
beq	$3,$0,$L1487
and	$5,$5,$2
.set	macro
.set	reorder

li	$2,4			# 0x4
subu	$5,$5,$6
subu	$2,$2,$3
li	$3,16			# 0x10
$L1488:
#APP
# 2756 "dsputil.c" 1
.word	0b01110000101001100000000001010110	#S32LDIV XR1,$5,$6,0
# 0 "" 2
# 2757 "dsputil.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2758 "dsputil.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2759 "dsputil.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2760 "dsputil.c" 1
.word	0b01110000101000000001000101010000	#S32LDD XR5,$5,16
# 0 "" 2
# 2762 "dsputil.c" 1
.word	0b01110000010001000100100001100111	#S32ALN XR1,XR2,XR1,$2
# 0 "" 2
# 2763 "dsputil.c" 1
.word	0b01110000010001001000110010100111	#S32ALN XR2,XR3,XR2,$2
# 0 "" 2
# 2764 "dsputil.c" 1
.word	0b01110000010001001101000011100111	#S32ALN XR3,XR4,XR3,$2
# 0 "" 2
# 2765 "dsputil.c" 1
.word	0b01110000010001010001010100100111	#S32ALN XR4,XR5,XR4,$2
# 0 "" 2
# 2767 "dsputil.c" 1
.word	0b01110000100001100000000110010110	#S32LDIV XR6,$4,$6,0
# 0 "" 2
# 2768 "dsputil.c" 1
.word	0b01110000100000000000010111010000	#S32LDD XR7,$4,4
# 0 "" 2
# 2769 "dsputil.c" 1
.word	0b01110000100000000000101000010000	#S32LDD XR8,$4,8
# 0 "" 2
# 2770 "dsputil.c" 1
.word	0b01110000100000000000111001010000	#S32LDD XR9,$4,12
# 0 "" 2
# 2772 "dsputil.c" 1
.word	0b01110000000101011000010001000110	#Q8AVGR XR1,XR1,XR6
# 0 "" 2
# 2773 "dsputil.c" 1
.word	0b01110000000101011100100010000110	#Q8AVGR XR2,XR2,XR7
# 0 "" 2
# 2774 "dsputil.c" 1
.word	0b01110000000101100000110011000110	#Q8AVGR XR3,XR3,XR8
# 0 "" 2
# 2775 "dsputil.c" 1
.word	0b01110000000101100101000100000110	#Q8AVGR XR4,XR4,XR9
# 0 "" 2
# 2777 "dsputil.c" 1
.word	0b01110000100000000000000001010001	#S32STD XR1,$4,0
# 0 "" 2
# 2778 "dsputil.c" 1
.word	0b01110000100000000000010010010001	#S32STD XR2,$4,4
# 0 "" 2
# 2779 "dsputil.c" 1
.word	0b01110000100000000000100011010001	#S32STD XR3,$4,8
# 0 "" 2
# 2780 "dsputil.c" 1
.word	0b01110000100000000000110100010001	#S32STD XR4,$4,12
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
bne	$3,$0,$L1488
j	$31
$L1487:
subu	$5,$5,$6
li	$2,16			# 0x10
$L1489:
#APP
# 2786 "dsputil.c" 1
.word	0b01110000101001100000000001010110	#S32LDIV XR1,$5,$6,0
# 0 "" 2
# 2787 "dsputil.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2788 "dsputil.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2789 "dsputil.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2791 "dsputil.c" 1
.word	0b01110000100001100000000110010110	#S32LDIV XR6,$4,$6,0
# 0 "" 2
# 2792 "dsputil.c" 1
.word	0b01110000100000000000010111010000	#S32LDD XR7,$4,4
# 0 "" 2
# 2793 "dsputil.c" 1
.word	0b01110000100000000000101000010000	#S32LDD XR8,$4,8
# 0 "" 2
# 2794 "dsputil.c" 1
.word	0b01110000100000000000111001010000	#S32LDD XR9,$4,12
# 0 "" 2
# 2796 "dsputil.c" 1
.word	0b01110000000101011000010001000110	#Q8AVGR XR1,XR1,XR6
# 0 "" 2
# 2797 "dsputil.c" 1
.word	0b01110000000101011100100010000110	#Q8AVGR XR2,XR2,XR7
# 0 "" 2
# 2798 "dsputil.c" 1
.word	0b01110000000101100000110011000110	#Q8AVGR XR3,XR3,XR8
# 0 "" 2
# 2799 "dsputil.c" 1
.word	0b01110000000101100101000100000110	#Q8AVGR XR4,XR4,XR9
# 0 "" 2
# 2801 "dsputil.c" 1
.word	0b01110000100000000000000001010001	#S32STD XR1,$4,0
# 0 "" 2
# 2802 "dsputil.c" 1
.word	0b01110000100000000000010010010001	#S32STD XR2,$4,4
# 0 "" 2
# 2803 "dsputil.c" 1
.word	0b01110000100000000000100011010001	#S32STD XR3,$4,8
# 0 "" 2
# 2804 "dsputil.c" 1
.word	0b01110000100000000000110100010001	#S32STD XR4,$4,12
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$0,$L1489
j	$31
.end	ff_avg_pixels16x16_c
.size	ff_avg_pixels16x16_c, .-ff_avg_pixels16x16_c
.align	2
.globl	ff_avg_pixels8x8_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_pixels8x8_c
.type	ff_avg_pixels8x8_c, @function
ff_avg_pixels8x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$3,-4			# 0xfffffffffffffffc
andi	$7,$5,0x3
and	$5,$5,$3
.set	noreorder
.set	nomacro
beq	$7,$0,$L1494
subu	$3,$5,$6
.set	macro
.set	reorder

li	$2,4			# 0x4
subu	$5,$2,$7
subu	$2,$4,$6
#APP
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2717 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2718 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2720 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2721 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2723 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2724 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2710 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2711 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2712 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2714 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2715 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
#NO_APP
j	$31
$L1494:
subu	$2,$4,$6
#APP
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2730 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2731 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2733 "dsputil.c" 1
.word	0b01110000010001100000000100010110	#S32LDIV XR4,$2,$6,0
# 0 "" 2
# 2734 "dsputil.c" 1
.word	0b01110000010000000000010101010000	#S32LDD XR5,$2,4
# 0 "" 2
# 2736 "dsputil.c" 1
.word	0b01110000000101010000010001000110	#Q8AVGR XR1,XR1,XR4
# 0 "" 2
# 2737 "dsputil.c" 1
.word	0b01110000000101010100100010000110	#Q8AVGR XR2,XR2,XR5
# 0 "" 2
# 2739 "dsputil.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 2740 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
#NO_APP
j	$31
.end	ff_avg_pixels8x8_c
.size	ff_avg_pixels8x8_c, .-ff_avg_pixels8x8_c
.align	2
.globl	ff_put_pixels16x16_c
.set	nomips16
.set	nomicromips
.ent	ff_put_pixels16x16_c
.type	ff_put_pixels16x16_c, @function
ff_put_pixels16x16_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$3,-4			# 0xfffffffffffffffc
andi	$7,$5,0x3
.set	noreorder
.set	nomacro
beq	$7,$0,$L1498
and	$5,$5,$3
.set	macro
.set	reorder

li	$2,4			# 0x4
subu	$4,$4,$6
subu	$2,$2,$7
subu	$5,$5,$6
li	$3,16			# 0x10
$L1499:
#APP
# 2667 "dsputil.c" 1
.word	0b01110000101001100000000001010110	#S32LDIV XR1,$5,$6,0
# 0 "" 2
# 2668 "dsputil.c" 1
.word	0b01110000101000000000010010010000	#S32LDD XR2,$5,4
# 0 "" 2
# 2669 "dsputil.c" 1
.word	0b01110000101000000000100011010000	#S32LDD XR3,$5,8
# 0 "" 2
# 2670 "dsputil.c" 1
.word	0b01110000101000000000110100010000	#S32LDD XR4,$5,12
# 0 "" 2
# 2671 "dsputil.c" 1
.word	0b01110000101000000001000101010000	#S32LDD XR5,$5,16
# 0 "" 2
# 2673 "dsputil.c" 1
.word	0b01110000010001000100100001100111	#S32ALN XR1,XR2,XR1,$2
# 0 "" 2
# 2674 "dsputil.c" 1
.word	0b01110000010001001000110010100111	#S32ALN XR2,XR3,XR2,$2
# 0 "" 2
# 2675 "dsputil.c" 1
.word	0b01110000010001001101000011100111	#S32ALN XR3,XR4,XR3,$2
# 0 "" 2
# 2676 "dsputil.c" 1
.word	0b01110000010001010001010100100111	#S32ALN XR4,XR5,XR4,$2
# 0 "" 2
# 2678 "dsputil.c" 1
.word	0b01110000100001100000000001010111	#S32SDIV XR1,$4,$6,0
# 0 "" 2
# 2679 "dsputil.c" 1
.word	0b01110000100000000000010010010001	#S32STD XR2,$4,4
# 0 "" 2
# 2680 "dsputil.c" 1
.word	0b01110000100000000000100011010001	#S32STD XR3,$4,8
# 0 "" 2
# 2681 "dsputil.c" 1
.word	0b01110000100000000000110100010001	#S32STD XR4,$4,12
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
bne	$3,$0,$L1499
j	$31
$L1498:
subu	$3,$5,$6
subu	$2,$4,$6
#APP
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
# 2687 "dsputil.c" 1
.word	0b01110000011001100000000001010110	#S32LDIV XR1,$3,$6,0
# 0 "" 2
# 2688 "dsputil.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 2689 "dsputil.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 2690 "dsputil.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 2692 "dsputil.c" 1
.word	0b01110000010001100000000001010111	#S32SDIV XR1,$2,$6,0
# 0 "" 2
# 2693 "dsputil.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 2694 "dsputil.c" 1
.word	0b01110000010000000000100011010001	#S32STD XR3,$2,8
# 0 "" 2
# 2695 "dsputil.c" 1
.word	0b01110000010000000000110100010001	#S32STD XR4,$2,12
# 0 "" 2
#NO_APP
j	$31
.end	ff_put_pixels16x16_c
.size	ff_put_pixels16x16_c, .-ff_put_pixels16x16_c
.align	2
.globl	ff_put_pixels8x8_c
.set	nomips16
.set	nomicromips
.ent	ff_put_pixels8x8_c
.type	ff_put_pixels8x8_c, @function
ff_put_pixels8x8_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,-4			# 0xfffffffffffffffc
andi	$7,$5,0x3
and	$5,$5,$2
.set	noreorder
.set	nomacro
beq	$7,$0,$L1503
subu	$2,$5,$6
.set	macro
.set	reorder

li	$3,4			# 0x4
subu	$5,$3,$7
subu	$3,$4,$6
#APP
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2633 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2634 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2635 "dsputil.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 2637 "dsputil.c" 1
.word	0b01110000101001000100100001100111	#S32ALN XR1,XR2,XR1,$5
# 0 "" 2
# 2638 "dsputil.c" 1
.word	0b01110000101001001000110010100111	#S32ALN XR2,XR3,XR2,$5
# 0 "" 2
# 2640 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2641 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
#NO_APP
j	$31
$L1503:
subu	$3,$4,$6
#APP
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 2647 "dsputil.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 2648 "dsputil.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 2650 "dsputil.c" 1
.word	0b01110000011001100000000001010111	#S32SDIV XR1,$3,$6,0
# 0 "" 2
# 2651 "dsputil.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
#NO_APP
j	$31
.end	ff_put_pixels8x8_c
.size	ff_put_pixels8x8_c, .-ff_put_pixels8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	wmv2_mspel8_h_lowpass.constprop.10
.type	wmv2_mspel8_h_lowpass.constprop.10, @function
wmv2_mspel8_h_lowpass.constprop.10:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1510
lui	$8,%hi(ff_cropTbl+1024)

move	$9,$0
addiu	$8,$8,%lo(ff_cropTbl+1024)
$L1507:
lbu	$3,0($5)
addiu	$4,$4,8
lbu	$10,1($5)
addiu	$9,$9,1
lbu	$11,-1($5)
lbu	$2,2($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-8($4)
lbu	$3,1($5)
lbu	$10,2($5)
lbu	$11,0($5)
lbu	$2,3($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-7($4)
lbu	$3,2($5)
lbu	$10,3($5)
lbu	$11,1($5)
lbu	$2,4($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-6($4)
lbu	$3,3($5)
lbu	$10,4($5)
lbu	$11,2($5)
lbu	$2,5($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-5($4)
lbu	$3,4($5)
lbu	$10,5($5)
lbu	$11,3($5)
lbu	$2,6($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-4($4)
lbu	$3,5($5)
lbu	$10,6($5)
lbu	$11,4($5)
lbu	$2,7($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-3($4)
lbu	$3,6($5)
lbu	$10,7($5)
lbu	$11,5($5)
lbu	$2,8($5)
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
sb	$2,-2($4)
lbu	$3,7($5)
lbu	$10,8($5)
lbu	$11,6($5)
lbu	$2,9($5)
addu	$5,$5,$6
addu	$10,$3,$10
sll	$3,$10,3
addu	$2,$11,$2
addu	$3,$3,$10
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$8,$2
lbu	$2,0($2)
bne	$9,$7,$L1507
sb	$2,-1($4)

$L1510:
j	$31
nop

.set	macro
.set	reorder
.end	wmv2_mspel8_h_lowpass.constprop.10
.size	wmv2_mspel8_h_lowpass.constprop.10, .-wmv2_mspel8_h_lowpass.constprop.10
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc22_c
.type	put_mspel8_mc22_c, @function
put_mspel8_mc22_c:
.frame	$sp,136,$31		# vars= 88, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-136
subu	$5,$5,$6
sw	$17,116($sp)
li	$7,11			# 0xb
move	$17,$4
sw	$16,112($sp)
addiu	$4,$sp,24
sw	$31,132($sp)
move	$16,$6
sw	$20,128($sp)
sw	$19,124($sp)
.option	pic0
jal	wmv2_mspel8_h_lowpass.constprop.10
.option	pic2
sw	$18,120($sp)

lui	$6,%hi(ff_cropTbl+1024)
addiu	$10,$17,8
addiu	$3,$sp,32
addiu	$6,$6,%lo(ff_cropTbl+1024)
$L1512:
lbu	$25,8($3)
addiu	$3,$3,1
lbu	$11,-1($3)
addu	$19,$17,$16
lbu	$15,15($3)
addiu	$17,$17,1
lbu	$14,-9($3)
addu	$2,$19,$16
addu	$20,$11,$25
addu	$12,$25,$15
sll	$4,$20,3
addu	$14,$14,$15
lbu	$18,23($3)
addu	$20,$4,$20
lbu	$9,31($3)
sll	$8,$12,3
lbu	$24,39($3)
subu	$20,$20,$14
lbu	$13,47($3)
addu	$8,$8,$12
lbu	$5,55($3)
addiu	$20,$20,8
lbu	$7,63($3)
addu	$12,$11,$18
lbu	$14,71($3)
addu	$4,$15,$18
sra	$20,$20,4
subu	$12,$8,$12
sll	$11,$4,3
addu	$8,$6,$20
addiu	$12,$12,8
addu	$11,$11,$4
addu	$4,$25,$9
lbu	$20,0($8)
sra	$12,$12,4
addu	$8,$18,$9
subu	$4,$11,$4
addu	$11,$6,$12
sb	$20,-1($17)
sll	$25,$8,3
addiu	$4,$4,8
lbu	$11,0($11)
addu	$25,$25,$8
sra	$4,$4,4
addu	$8,$15,$24
addu	$12,$9,$24
subu	$8,$25,$8
sb	$11,0($19)
addu	$25,$6,$4
sll	$15,$12,3
addiu	$8,$8,8
lbu	$11,0($25)
addu	$15,$15,$12
addu	$12,$18,$13
addu	$4,$24,$13
sra	$8,$8,4
subu	$12,$15,$12
sb	$11,0($2)
addu	$15,$6,$8
sll	$11,$4,3
addiu	$12,$12,8
addu	$11,$11,$4
lbu	$15,0($15)
addu	$4,$9,$5
addu	$8,$13,$5
sra	$12,$12,4
subu	$4,$11,$4
addu	$2,$2,$16
sll	$9,$8,3
addu	$11,$6,$12
sb	$15,0($2)
addiu	$4,$4,8
addu	$9,$9,$8
addu	$8,$24,$7
lbu	$11,0($11)
addu	$5,$5,$7
sra	$4,$4,4
subu	$8,$9,$8
addu	$2,$2,$16
sll	$7,$5,3
addu	$9,$6,$4
sb	$11,0($2)
addiu	$8,$8,8
addu	$7,$7,$5
addu	$5,$13,$14
lbu	$4,0($9)
sra	$8,$8,4
subu	$5,$7,$5
addu	$2,$2,$16
addu	$7,$6,$8
addiu	$5,$5,8
sb	$4,0($2)
addu	$2,$2,$16
lbu	$7,0($7)
sra	$5,$5,4
addu	$4,$2,$16
addu	$5,$6,$5
sb	$7,0($2)
lbu	$2,0($5)
bne	$17,$10,$L1512
sb	$2,0($4)

lw	$31,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
j	$31
addiu	$sp,$sp,136

.set	macro
.set	reorder
.end	put_mspel8_mc22_c
.size	put_mspel8_mc22_c, .-put_mspel8_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc20_c
.type	put_mspel8_mc20_c, @function
put_mspel8_mc20_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$7,%hi(ff_cropTbl+1024)
li	$8,8			# 0x8
addiu	$7,$7,%lo(ff_cropTbl+1024)
$L1516:
lbu	$3,0($5)
addiu	$8,$8,-1
lbu	$9,1($5)
lbu	$10,-1($5)
lbu	$2,2($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,0($4)
lbu	$3,1($5)
lbu	$9,2($5)
lbu	$10,0($5)
lbu	$2,3($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,1($4)
lbu	$3,2($5)
lbu	$9,3($5)
lbu	$10,1($5)
lbu	$2,4($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,2($4)
lbu	$3,3($5)
lbu	$9,4($5)
lbu	$10,2($5)
lbu	$2,5($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,3($4)
lbu	$3,4($5)
lbu	$9,5($5)
lbu	$10,3($5)
lbu	$2,6($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,4($4)
lbu	$3,5($5)
lbu	$9,6($5)
lbu	$10,4($5)
lbu	$2,7($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,5($4)
lbu	$3,6($5)
lbu	$9,7($5)
lbu	$10,5($5)
lbu	$2,8($5)
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,6($4)
lbu	$3,7($5)
lbu	$9,8($5)
lbu	$10,6($5)
lbu	$2,9($5)
addu	$5,$5,$6
addu	$9,$3,$9
sll	$3,$9,3
addu	$2,$10,$2
addu	$3,$3,$9
subu	$2,$3,$2
addiu	$2,$2,8
sra	$2,$2,4
addu	$2,$7,$2
lbu	$2,0($2)
sb	$2,7($4)
bne	$8,$0,$L1516
addu	$4,$4,$6

j	$31
nop

.set	macro
.set	reorder
.end	put_mspel8_mc20_c
.size	put_mspel8_mc20_c, .-put_mspel8_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_hv_lowpass.constprop.14
.type	avg_h264_qpel8_hv_lowpass.constprop.14, @function
avg_h264_qpel8_hv_lowpass.constprop.14:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
addiu	$13,$5,416
sw	$fp,60($sp)
move	$14,$5
lw	$12,80($sp)
sw	$23,56($sp)
sw	$22,52($sp)
sll	$2,$12,1
sw	$21,48($sp)
sw	$20,44($sp)
subu	$6,$6,$2
sw	$19,40($sp)
sw	$18,36($sp)
sw	$17,32($sp)
sw	$16,28($sp)
$L1519:
lbu	$3,0($6)
addiu	$14,$14,32
lbu	$10,1($6)
lbu	$2,2($6)
lbu	$9,-1($6)
addu	$10,$3,$10
lbu	$8,3($6)
lbu	$3,-2($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-32($14)
lbu	$3,1($6)
lbu	$10,2($6)
lbu	$2,3($6)
lbu	$9,0($6)
addu	$10,$3,$10
lbu	$8,4($6)
lbu	$3,-1($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-30($14)
lbu	$3,2($6)
lbu	$10,3($6)
lbu	$2,4($6)
lbu	$9,1($6)
addu	$10,$3,$10
lbu	$8,5($6)
lbu	$3,0($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-28($14)
lbu	$3,3($6)
lbu	$10,4($6)
lbu	$2,5($6)
lbu	$9,2($6)
addu	$10,$3,$10
lbu	$8,6($6)
lbu	$3,1($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-26($14)
lbu	$3,5($6)
lbu	$10,4($6)
lbu	$2,2($6)
lbu	$8,7($6)
lbu	$9,3($6)
addu	$10,$10,$3
lbu	$11,6($6)
sll	$3,$10,2
sll	$10,$10,4
addu	$9,$9,$11
addu	$8,$8,$2
addu	$10,$3,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-24($14)
lbu	$3,5($6)
lbu	$10,6($6)
lbu	$2,7($6)
lbu	$9,4($6)
addu	$10,$3,$10
lbu	$8,8($6)
lbu	$3,3($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-22($14)
lbu	$3,6($6)
lbu	$10,7($6)
lbu	$2,8($6)
lbu	$9,5($6)
addu	$10,$3,$10
lbu	$8,9($6)
lbu	$3,4($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-20($14)
lbu	$3,7($6)
lbu	$10,8($6)
lbu	$2,9($6)
lbu	$9,6($6)
addu	$10,$3,$10
lbu	$8,10($6)
lbu	$3,5($6)
addu	$6,$6,$12
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
bne	$14,$13,$L1519
sh	$2,-18($14)

addu	$fp,$4,$7
addiu	$2,$4,8
addu	$23,$fp,$7
lui	$20,%hi(ff_cropTbl+1024)
addu	$22,$23,$7
sw	$2,20($sp)
addiu	$18,$5,64
addu	$21,$22,$7
addiu	$20,$20,%lo(ff_cropTbl+1024)
addu	$17,$21,$7
addu	$16,$17,$7
addu	$15,$16,$7
$L1520:
lh	$11,32($18)
addiu	$4,$4,1
lh	$12,0($18)
addiu	$fp,$fp,1
lh	$10,64($18)
addiu	$23,$23,1
lh	$14,-32($18)
addiu	$22,$22,1
addu	$3,$12,$11
lh	$9,96($18)
lh	$19,-64($18)
addu	$2,$11,$10
addu	$7,$14,$10
lh	$8,128($18)
sll	$13,$3,2
lbu	$5,-1($4)
sll	$3,$3,4
lh	$6,192($18)
sll	$24,$7,2
addu	$25,$13,$3
lh	$3,288($18)
addu	$24,$24,$7
sw	$5,16($sp)
addu	$19,$19,$9
lh	$5,160($18)
lh	$13,224($18)
addiu	$15,$15,1
sw	$3,8($sp)
subu	$3,$25,$24
lh	$24,320($18)
sll	$25,$2,2
addu	$3,$3,$19
lh	$7,256($18)
sll	$2,$2,4
addiu	$3,$3,512
sw	$24,12($sp)
addu	$24,$12,$9
sra	$3,$3,10
sll	$19,$24,2
addu	$2,$25,$2
addu	$19,$19,$24
addu	$3,$20,$3
addu	$24,$14,$8
subu	$2,$2,$19
lbu	$3,0($3)
addu	$14,$10,$9
addu	$24,$2,$24
lw	$2,16($sp)
addu	$19,$11,$8
addu	$12,$12,$5
addu	$25,$2,$3
addiu	$3,$24,512
addiu	$25,$25,1
sra	$3,$3,10
sll	$24,$14,2
sra	$25,$25,1
sll	$2,$14,4
sll	$14,$19,2
addu	$3,$20,$3
sb	$25,-1($4)
addu	$14,$14,$19
lbu	$25,-1($fp)
addu	$2,$24,$2
lbu	$19,0($3)
addu	$3,$9,$8
subu	$2,$2,$14
sll	$24,$3,2
addu	$12,$2,$12
addu	$25,$25,$19
addiu	$12,$12,512
addu	$19,$10,$5
addiu	$25,$25,1
sra	$12,$12,10
sra	$25,$25,1
sll	$2,$3,4
sll	$14,$19,2
addu	$12,$20,$12
sb	$25,-1($fp)
addu	$14,$14,$19
lbu	$3,-1($23)
addu	$2,$24,$2
lbu	$25,0($12)
addu	$11,$11,$6
subu	$2,$2,$14
addu	$12,$8,$5
addu	$11,$2,$11
addu	$3,$3,$25
addiu	$11,$11,512
addu	$25,$9,$6
addiu	$3,$3,1
sra	$11,$11,10
sll	$14,$12,2
sra	$3,$3,1
sll	$2,$12,4
sll	$12,$25,2
addu	$11,$20,$11
sb	$3,-1($23)
addu	$2,$14,$2
lbu	$3,-1($22)
addu	$25,$12,$25
lbu	$11,0($11)
addu	$10,$10,$13
subu	$12,$2,$25
addu	$19,$8,$13
addu	$12,$12,$10
addu	$10,$3,$11
addiu	$3,$12,512
addu	$2,$5,$6
addiu	$10,$10,1
sra	$3,$3,10
sll	$12,$2,2
sra	$10,$10,1
sll	$2,$2,4
sll	$11,$19,2
addu	$3,$20,$3
sb	$10,-1($22)
addu	$2,$12,$2
lbu	$10,0($21)
addu	$11,$11,$19
lbu	$3,0($3)
addu	$9,$9,$7
subu	$12,$2,$11
addu	$14,$5,$7
addu	$9,$12,$9
addu	$10,$10,$3
addiu	$9,$9,512
addiu	$10,$10,1
sra	$9,$9,10
addu	$2,$6,$13
sra	$10,$10,1
addu	$9,$20,$9
sll	$3,$2,2
sll	$11,$14,2
sb	$10,0($21)
sll	$2,$2,4
lbu	$10,0($17)
lbu	$9,0($9)
addu	$7,$13,$7
addu	$2,$3,$2
lw	$24,12($sp)
addu	$3,$11,$14
lw	$11,8($sp)
addu	$9,$10,$9
subu	$3,$2,$3
addu	$8,$8,$11
addu	$6,$6,$11
addu	$8,$3,$8
addiu	$9,$9,1
addiu	$8,$8,512
sll	$3,$7,2
sra	$8,$8,10
sll	$2,$6,2
sll	$7,$7,4
sra	$9,$9,1
addu	$8,$20,$8
addu	$7,$3,$7
addu	$2,$2,$6
sb	$9,0($17)
lbu	$3,0($16)
addu	$5,$5,$24
subu	$6,$7,$2
lbu	$7,0($8)
addiu	$18,$18,2
addu	$2,$6,$5
addu	$3,$3,$7
addiu	$2,$2,512
addiu	$3,$3,1
sra	$2,$2,10
sra	$3,$3,1
addu	$2,$20,$2
addiu	$21,$21,1
sb	$3,0($16)
addiu	$17,$17,1
lbu	$3,-1($15)
lbu	$2,0($2)
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,-1($15)
lw	$2,20($sp)
bne	$4,$2,$L1520
addiu	$16,$16,1

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_h264_qpel8_hv_lowpass.constprop.14
.size	avg_h264_qpel8_hv_lowpass.constprop.14, .-avg_h264_qpel8_hv_lowpass.constprop.14
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc22_c
.type	avg_h264_qpel16_mc22_c, @function
avg_h264_qpel16_mc22_c:
.frame	$sp,728,$31		# vars= 672, regs= 6/0, args= 24, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-728
sw	$20,720($sp)
addiu	$20,$sp,32
sw	$17,708($sp)
move	$17,$5
sw	$16,704($sp)
move	$16,$6
sw	$19,716($sp)
move	$5,$20
sw	$18,712($sp)
addiu	$19,$sp,48
sw	$6,16($sp)
move	$18,$4
move	$6,$17
sw	$31,724($sp)
.option	pic0
jal	avg_h264_qpel8_hv_lowpass.constprop.14
.option	pic2
move	$7,$16

addiu	$4,$18,8
addiu	$6,$17,8
sw	$16,16($sp)
move	$5,$19
.option	pic0
jal	avg_h264_qpel8_hv_lowpass.constprop.14
.option	pic2
move	$7,$16

sll	$2,$16,3
move	$5,$20
sw	$16,16($sp)
addu	$17,$17,$2
addu	$18,$18,$2
move	$6,$17
move	$4,$18
.option	pic0
jal	avg_h264_qpel8_hv_lowpass.constprop.14
.option	pic2
move	$7,$16

addiu	$4,$18,8
addiu	$6,$17,8
sw	$16,16($sp)
move	$5,$19
.option	pic0
jal	avg_h264_qpel8_hv_lowpass.constprop.14
.option	pic2
move	$7,$16

lw	$31,724($sp)
lw	$20,720($sp)
lw	$19,716($sp)
lw	$18,712($sp)
lw	$17,708($sp)
lw	$16,704($sp)
j	$31
addiu	$sp,$sp,728

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc22_c
.size	avg_h264_qpel16_mc22_c, .-avg_h264_qpel16_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc22_c
.type	avg_h264_qpel8_mc22_c, @function
avg_h264_qpel8_mc22_c:
.frame	$sp,336,$31		# vars= 288, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-336
sll	$2,$6,1
addiu	$3,$sp,8
sw	$fp,332($sp)
subu	$2,$5,$2
sw	$23,328($sp)
addiu	$5,$sp,216
sw	$22,324($sp)
sw	$21,320($sp)
sw	$20,316($sp)
sw	$19,312($sp)
sw	$18,308($sp)
sw	$17,304($sp)
sw	$16,300($sp)
sw	$6,344($sp)
sw	$5,280($sp)
sw	$4,336($sp)
$L1527:
lbu	$22,-2($2)
addiu	$3,$3,16
lbu	$14,2($2)
lbu	$11,-1($2)
lbu	$9,3($2)
lbu	$10,0($2)
sh	$22,232($sp)
lbu	$22,10($2)
addu	$19,$14,$9
lbu	$8,4($2)
lbu	$4,1($2)
sll	$24,$19,2
lbu	$7,5($2)
sll	$19,$19,4
sh	$22,272($sp)
addu	$22,$11,$14
lbu	$15,6($2)
addu	$18,$9,$8
lbu	$6,7($2)
addu	$17,$8,$7
sw	$22,220($sp)
addu	$22,$10,$9
lbu	$16,8($2)
addu	$5,$7,$15
lbu	$12,9($2)
addu	$13,$15,$6
sw	$22,224($sp)
addu	$22,$4,$8
addu	$21,$10,$4
sll	$23,$18,2
sw	$22,228($sp)
addu	$22,$14,$7
sw	$12,216($sp)
addu	$12,$6,$16
sll	$fp,$21,2
sw	$22,240($sp)
addu	$22,$9,$15
andi	$9,$9,0xffff
sll	$21,$21,4
sw	$22,244($sp)
addu	$22,$8,$6
andi	$8,$8,0xffff
addu	$11,$8,$11
sw	$22,248($sp)
addu	$22,$7,$16
andi	$7,$7,0xffff
addu	$10,$7,$10
sw	$11,284($sp)
sw	$22,252($sp)
sll	$18,$18,4
lw	$22,216($sp)
addu	$20,$4,$14
addu	$18,$23,$18
addu	$21,$fp,$21
addu	$22,$15,$22
addu	$6,$6,$14
sll	$25,$20,2
sw	$22,256($sp)
sll	$22,$17,2
sll	$20,$20,4
addu	$19,$24,$19
sw	$22,288($sp)
sll	$22,$5,2
addu	$20,$25,$20
addu	$4,$15,$4
sw	$22,260($sp)
sll	$22,$13,2
sll	$17,$17,4
sll	$5,$5,4
sw	$22,264($sp)
sll	$22,$12,2
sll	$13,$13,4
sll	$12,$12,4
sw	$22,268($sp)
addu	$4,$4,$18
lhu	$22,232($sp)
addu	$22,$9,$22
addu	$9,$9,$16
sw	$22,232($sp)
lw	$22,220($sp)
lw	$11,224($sp)
lw	$14,244($sp)
sw	$10,292($sp)
sll	$fp,$22,2
lw	$10,240($sp)
sll	$25,$11,2
lw	$22,228($sp)
lw	$11,288($sp)
sll	$23,$10,2
lw	$10,248($sp)
lw	$15,260($sp)
sll	$24,$22,2
sll	$22,$14,2
lw	$14,264($sp)
sll	$16,$10,2
lw	$10,252($sp)
addu	$17,$11,$17
lw	$11,216($sp)
addu	$5,$15,$5
lw	$18,248($sp)
addu	$13,$14,$13
lw	$14,268($sp)
sll	$15,$10,2
lw	$10,256($sp)
addu	$8,$8,$11
lhu	$11,272($sp)
addu	$12,$14,$12
sll	$14,$10,2
lw	$10,220($sp)
addu	$7,$7,$11
lw	$11,232($sp)
addu	$6,$6,$17
lw	$17,244($sp)
addu	$fp,$10,$fp
lw	$10,284($sp)
addu	$21,$11,$21
addu	$22,$17,$22
addu	$11,$10,$20
lw	$20,224($sp)
addu	$5,$9,$5
addu	$16,$18,$16
addu	$25,$20,$25
lw	$20,292($sp)
addu	$8,$8,$13
addu	$7,$7,$12
addu	$10,$20,$19
lw	$19,228($sp)
lw	$20,240($sp)
subu	$6,$6,$22
subu	$5,$5,$16
lw	$22,344($sp)
addu	$24,$19,$24
lw	$19,252($sp)
addu	$23,$20,$23
lw	$20,256($sp)
subu	$21,$21,$fp
sh	$5,-6($3)
addu	$15,$19,$15
sh	$6,-8($3)
addu	$14,$20,$14
subu	$11,$11,$25
sh	$21,-16($3)
subu	$10,$10,$24
subu	$4,$4,$23
subu	$8,$8,$15
sh	$11,-14($3)
subu	$7,$7,$14
sh	$10,-12($3)
sh	$4,-10($3)
sh	$8,-4($3)
sh	$7,-2($3)
lw	$5,280($sp)
bne	$3,$5,$L1527
addu	$2,$2,$22

lw	$9,336($sp)
move	$11,$22
lui	$21,%hi(ff_cropTbl+1024)
addiu	$19,$sp,40
addu	$fp,$9,$22
addiu	$10,$9,8
addu	$25,$fp,$22
addiu	$21,$21,%lo(ff_cropTbl+1024)
addu	$24,$25,$22
sw	$10,228($sp)
move	$12,$9
addu	$23,$24,$22
addu	$22,$23,$22
addu	$16,$22,$11
addu	$15,$16,$11
$L1528:
lh	$10,16($19)
addiu	$12,$12,1
lh	$11,0($19)
addiu	$fp,$fp,1
lh	$9,32($19)
addiu	$25,$25,1
lh	$14,-16($19)
addiu	$24,$24,1
lbu	$18,-1($12)
addu	$3,$11,$10
lh	$8,48($19)
addu	$2,$10,$9
addu	$6,$14,$9
lh	$17,-32($19)
sll	$13,$3,2
lh	$7,64($19)
sll	$3,$3,4
sw	$18,224($sp)
sll	$18,$6,2
lh	$4,80($19)
addu	$20,$13,$3
lh	$3,144($19)
addu	$18,$18,$6
lh	$5,96($19)
addu	$17,$17,$8
lh	$13,112($19)
lh	$6,128($19)
addiu	$15,$15,1
sw	$3,216($sp)
subu	$3,$20,$18
lh	$18,160($19)
sll	$20,$2,2
addu	$3,$3,$17
sll	$2,$2,4
addiu	$3,$3,512
sw	$18,220($sp)
addu	$18,$11,$8
sra	$3,$3,10
sll	$17,$18,2
addu	$2,$20,$2
addu	$17,$17,$18
addu	$3,$21,$3
addu	$18,$14,$7
subu	$2,$2,$17
lbu	$3,0($3)
addu	$14,$9,$8
addu	$18,$2,$18
lw	$2,224($sp)
addu	$17,$10,$7
addu	$11,$11,$4
addu	$20,$2,$3
addiu	$3,$18,512
addiu	$20,$20,1
sra	$3,$3,10
sll	$18,$14,2
sra	$20,$20,1
sll	$2,$14,4
sll	$14,$17,2
addu	$3,$21,$3
sb	$20,-1($12)
addu	$14,$14,$17
lbu	$20,-1($fp)
addu	$2,$18,$2
lbu	$17,0($3)
addu	$3,$8,$7
subu	$2,$2,$14
sll	$18,$3,2
addu	$11,$2,$11
addu	$20,$20,$17
addiu	$11,$11,512
addu	$17,$9,$4
addiu	$20,$20,1
sra	$11,$11,10
sra	$20,$20,1
sll	$2,$3,4
sll	$14,$17,2
addu	$11,$21,$11
sb	$20,-1($fp)
addu	$14,$14,$17
lbu	$3,-1($25)
addu	$2,$18,$2
lbu	$20,0($11)
addu	$10,$10,$5
subu	$2,$2,$14
addu	$11,$7,$4
addu	$10,$2,$10
addu	$3,$3,$20
addiu	$10,$10,512
addu	$20,$8,$5
addiu	$3,$3,1
sra	$10,$10,10
sll	$14,$11,2
sra	$3,$3,1
sll	$2,$11,4
sll	$11,$20,2
addu	$10,$21,$10
sb	$3,-1($25)
addu	$2,$14,$2
lbu	$3,-1($24)
addu	$20,$11,$20
lbu	$10,0($10)
addu	$9,$9,$13
subu	$11,$2,$20
addu	$17,$7,$13
addu	$11,$11,$9
addu	$9,$3,$10
addiu	$3,$11,512
addu	$2,$4,$5
addiu	$9,$9,1
sra	$3,$3,10
sll	$11,$2,2
sra	$9,$9,1
sll	$2,$2,4
sll	$10,$17,2
addu	$3,$21,$3
sb	$9,-1($24)
addu	$2,$11,$2
lbu	$9,0($23)
addu	$10,$10,$17
lbu	$3,0($3)
addu	$8,$8,$6
subu	$11,$2,$10
addu	$14,$4,$6
addu	$8,$11,$8
addu	$9,$9,$3
addiu	$8,$8,512
addiu	$9,$9,1
sra	$8,$8,10
addu	$2,$5,$13
sra	$9,$9,1
addu	$8,$21,$8
sll	$3,$2,2
sll	$10,$14,2
sb	$9,0($23)
sll	$2,$2,4
lbu	$9,0($22)
lbu	$8,0($8)
addu	$6,$13,$6
addu	$2,$3,$2
lw	$11,220($sp)
addu	$3,$10,$14
lw	$10,216($sp)
addu	$8,$9,$8
lw	$14,228($sp)
subu	$3,$2,$3
addu	$7,$7,$10
addu	$5,$5,$10
addu	$7,$3,$7
addiu	$8,$8,1
addiu	$7,$7,512
sll	$3,$6,2
sra	$7,$7,10
sll	$2,$5,2
sll	$6,$6,4
sra	$8,$8,1
addu	$7,$21,$7
addu	$6,$3,$6
addu	$2,$2,$5
sb	$8,0($22)
lbu	$3,0($16)
addu	$4,$4,$11
subu	$5,$6,$2
lbu	$6,0($7)
addiu	$19,$19,2
addu	$2,$5,$4
addu	$3,$3,$6
addiu	$2,$2,512
addiu	$3,$3,1
sra	$2,$2,10
sra	$3,$3,1
addu	$2,$21,$2
addiu	$23,$23,1
sb	$3,0($16)
addiu	$22,$22,1
lbu	$3,-1($15)
addiu	$16,$16,1
lbu	$2,0($2)
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$12,$14,$L1528
sb	$2,-1($15)

lw	$fp,332($sp)
lw	$23,328($sp)
lw	$22,324($sp)
lw	$21,320($sp)
lw	$20,316($sp)
lw	$19,312($sp)
lw	$18,308($sp)
lw	$17,304($sp)
lw	$16,300($sp)
j	$31
addiu	$sp,$sp,336

.set	macro
.set	reorder
.end	avg_h264_qpel8_mc22_c
.size	avg_h264_qpel8_mc22_c, .-avg_h264_qpel8_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_v_lowpass.constprop.16
.type	avg_h264_qpel8_v_lowpass.constprop.16, @function
avg_h264_qpel8_v_lowpass.constprop.16:
.frame	$sp,64,$31		# vars= 16, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
addiu	$2,$5,8
sw	$23,56($sp)
addu	$23,$4,$6
sw	$22,52($sp)
addu	$22,$23,$6
sw	$fp,60($sp)
sw	$21,48($sp)
addu	$fp,$22,$6
sw	$18,36($sp)
sw	$20,44($sp)
lui	$20,%hi(ff_cropTbl+1024)
addu	$21,$fp,$6
sw	$17,32($sp)
sw	$16,28($sp)
addiu	$20,$20,%lo(ff_cropTbl+1024)
addu	$18,$21,$6
sw	$19,40($sp)
sw	$2,20($sp)
addu	$17,$18,$6
addu	$16,$17,$6
$L1533:
lbu	$11,32($5)
addiu	$5,$5,1
addiu	$4,$4,1
lbu	$6,-1($4)
addiu	$23,$23,1
addiu	$22,$22,1
lbu	$12,15($5)
addiu	$16,$16,1
lbu	$13,-1($5)
addiu	$fp,$fp,1
lbu	$15,-17($5)
addiu	$21,$21,1
lbu	$10,47($5)
addu	$2,$12,$11
addu	$3,$13,$12
lbu	$19,-33($5)
addu	$8,$15,$11
lbu	$9,63($5)
sll	$14,$3,2
sw	$6,16($sp)
sll	$3,$3,4
lbu	$6,79($5)
sll	$24,$8,2
lbu	$7,95($5)
addu	$25,$14,$3
lbu	$3,143($5)
addu	$24,$24,$8
lbu	$14,111($5)
addu	$19,$19,$10
lbu	$8,127($5)
addiu	$18,$18,1
sw	$3,8($sp)
subu	$3,$25,$24
lbu	$24,159($5)
sll	$25,$2,2
addu	$3,$3,$19
sll	$2,$2,4
addiu	$3,$3,16
sw	$24,12($sp)
addu	$24,$13,$10
sra	$3,$3,5
sll	$19,$24,2
addu	$2,$25,$2
addu	$19,$19,$24
addu	$3,$20,$3
addu	$24,$15,$9
subu	$2,$2,$19
lbu	$3,0($3)
addu	$15,$11,$10
addu	$24,$2,$24
lw	$2,16($sp)
addu	$19,$12,$9
addu	$13,$13,$6
addu	$25,$2,$3
addiu	$3,$24,16
addiu	$25,$25,1
sra	$3,$3,5
sll	$24,$15,2
sra	$25,$25,1
sll	$2,$15,4
sll	$15,$19,2
addu	$3,$20,$3
sb	$25,-1($4)
addu	$15,$15,$19
lbu	$25,-1($23)
addu	$2,$24,$2
lbu	$19,0($3)
addu	$3,$10,$9
subu	$2,$2,$15
sll	$24,$3,2
addu	$13,$2,$13
addu	$25,$25,$19
addiu	$13,$13,16
addu	$19,$11,$6
addiu	$25,$25,1
sra	$13,$13,5
sra	$25,$25,1
sll	$2,$3,4
sll	$15,$19,2
addu	$13,$20,$13
sb	$25,-1($23)
addu	$15,$15,$19
lbu	$3,-1($22)
addu	$2,$24,$2
lbu	$25,0($13)
addu	$12,$12,$7
subu	$2,$2,$15
addu	$13,$9,$6
addu	$12,$2,$12
addu	$3,$3,$25
addiu	$12,$12,16
addu	$25,$10,$7
addiu	$3,$3,1
sra	$12,$12,5
sll	$15,$13,2
sra	$3,$3,1
sll	$2,$13,4
sll	$13,$25,2
addu	$12,$20,$12
sb	$3,-1($22)
addu	$2,$15,$2
lbu	$3,-1($fp)
addu	$25,$13,$25
lbu	$12,0($12)
addu	$11,$11,$14
subu	$13,$2,$25
addu	$19,$9,$14
addu	$13,$13,$11
addu	$11,$3,$12
addiu	$3,$13,16
addu	$2,$6,$7
addiu	$11,$11,1
sra	$3,$3,5
sll	$13,$2,2
sra	$11,$11,1
sll	$2,$2,4
sll	$12,$19,2
addu	$3,$20,$3
sb	$11,-1($fp)
addu	$2,$13,$2
lbu	$11,-1($21)
addu	$12,$12,$19
lbu	$3,0($3)
addu	$10,$10,$8
subu	$13,$2,$12
addu	$15,$6,$8
addu	$10,$13,$10
addu	$11,$11,$3
addiu	$10,$10,16
addiu	$11,$11,1
sra	$10,$10,5
addu	$2,$7,$14
sra	$11,$11,1
addu	$10,$20,$10
sll	$3,$2,2
sll	$12,$15,2
sb	$11,-1($21)
sll	$2,$2,4
lbu	$11,-1($18)
lbu	$10,0($10)
addu	$8,$14,$8
addu	$2,$3,$2
lw	$24,12($sp)
addu	$3,$12,$15
lw	$12,8($sp)
addu	$10,$11,$10
subu	$3,$2,$3
addu	$9,$9,$12
addu	$7,$7,$12
addu	$9,$3,$9
addiu	$10,$10,1
addiu	$9,$9,16
sll	$3,$8,2
sra	$9,$9,5
sll	$2,$7,2
sll	$8,$8,4
sra	$10,$10,1
addu	$9,$20,$9
addu	$8,$3,$8
addu	$2,$2,$7
sb	$10,-1($18)
lbu	$3,0($17)
addu	$6,$6,$24
subu	$7,$8,$2
lbu	$8,0($9)
addiu	$17,$17,1
addu	$2,$7,$6
addu	$3,$3,$8
addiu	$2,$2,16
addiu	$3,$3,1
sra	$2,$2,5
sra	$3,$3,1
addu	$2,$20,$2
sb	$3,-1($17)
lbu	$3,-1($16)
lbu	$2,0($2)
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,-1($16)
lw	$2,20($sp)
bne	$5,$2,$L1533
lw	$19,40($sp)

lw	$fp,60($sp)
lw	$23,56($sp)
lw	$22,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	avg_h264_qpel8_v_lowpass.constprop.16
.size	avg_h264_qpel8_v_lowpass.constprop.16, .-avg_h264_qpel8_v_lowpass.constprop.16
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc22_c
.type	avg_h264_qpel4_mc22_c, @function
avg_h264_qpel4_mc22_c:
.frame	$sp,120,$31		# vars= 72, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-120
sll	$2,$6,1
sw	$19,96($sp)
addiu	$19,$sp,80
sw	$18,92($sp)
subu	$5,$5,$2
addiu	$18,$sp,8
sw	$fp,116($sp)
sw	$23,112($sp)
sw	$22,108($sp)
sw	$21,104($sp)
sw	$20,100($sp)
sw	$17,88($sp)
sw	$16,84($sp)
$L1537:
lbu	$23,3($5)
addiu	$18,$18,8
lbu	$15,1($5)
lbu	$16,2($5)
lbu	$17,0($5)
lbu	$12,4($5)
lbu	$10,-1($5)
addu	$21,$15,$16
lbu	$8,5($5)
addu	$20,$16,$23
addu	$22,$17,$15
lbu	$3,-2($5)
addu	$14,$23,$12
lbu	$fp,6($5)
addu	$13,$10,$16
addu	$25,$17,$23
addu	$24,$15,$12
addu	$16,$16,$8
sll	$2,$22,2
sll	$11,$21,2
sll	$9,$20,2
sll	$7,$14,2
sll	$22,$22,4
sll	$14,$14,4
sll	$21,$21,4
sll	$20,$20,4
addu	$3,$23,$3
addu	$12,$12,$10
addu	$17,$8,$17
addu	$7,$7,$14
addu	$2,$2,$22
sll	$23,$13,2
addu	$11,$11,$21
sll	$10,$25,2
addu	$9,$9,$20
sll	$8,$24,2
addu	$15,$15,$fp
sll	$14,$16,2
addu	$2,$3,$2
addu	$13,$13,$23
addu	$11,$12,$11
addu	$10,$25,$10
addu	$9,$17,$9
addu	$8,$24,$8
addu	$7,$15,$7
addu	$3,$16,$14
subu	$13,$2,$13
subu	$10,$11,$10
subu	$8,$9,$8
subu	$3,$7,$3
sh	$13,-8($18)
sh	$10,-6($18)
addu	$5,$5,$6
sh	$8,-4($18)
bne	$18,$19,$L1537
sh	$3,-2($18)

addu	$13,$4,$6
lui	$11,%hi(ff_cropTbl+1024)
addu	$12,$13,$6
addiu	$14,$4,4
addu	$6,$12,$6
addiu	$10,$sp,24
addiu	$11,$11,%lo(ff_cropTbl+1024)
$L1538:
lh	$15,8($10)
addiu	$4,$4,1
lh	$16,16($10)
addiu	$13,$13,1
lh	$18,0($10)
addiu	$12,$12,1
lh	$20,-8($10)
addiu	$6,$6,1
lh	$2,24($10)
addu	$5,$15,$16
addu	$7,$18,$15
lh	$9,-16($10)
addu	$25,$20,$16
lh	$3,32($10)
sll	$8,$7,2
lbu	$19,-1($4)
sll	$7,$7,4
lh	$17,40($10)
sll	$21,$25,2
lh	$24,48($10)
addu	$7,$8,$7
addu	$25,$21,$25
addu	$8,$9,$2
subu	$25,$7,$25
addu	$7,$18,$2
addu	$25,$25,$8
sll	$9,$7,2
addiu	$25,$25,512
sll	$8,$5,2
sra	$25,$25,10
sll	$5,$5,4
addu	$25,$11,$25
addu	$5,$8,$5
addu	$9,$9,$7
addu	$20,$20,$3
lbu	$7,0($25)
subu	$9,$5,$9
addu	$5,$16,$2
addu	$9,$9,$20
addu	$19,$19,$7
addiu	$9,$9,512
addu	$20,$15,$3
addiu	$19,$19,1
sra	$9,$9,10
sll	$8,$5,2
sra	$19,$19,1
sll	$5,$5,4
sll	$7,$20,2
addu	$9,$11,$9
sb	$19,-1($4)
addu	$5,$8,$5
lbu	$19,-1($13)
addu	$7,$7,$20
lbu	$8,0($9)
addu	$18,$18,$17
subu	$7,$5,$7
addu	$3,$2,$3
addu	$7,$7,$18
addu	$8,$19,$8
addiu	$7,$7,512
addu	$16,$16,$17
addiu	$8,$8,1
sra	$7,$7,10
sll	$5,$3,2
sra	$8,$8,1
sll	$3,$3,4
sll	$2,$16,2
addu	$7,$11,$7
sb	$8,-1($13)
addu	$3,$5,$3
lbu	$8,-1($12)
addu	$2,$2,$16
lbu	$5,0($7)
addu	$15,$15,$24
subu	$2,$3,$2
addiu	$10,$10,2
addu	$2,$2,$15
addu	$3,$8,$5
addiu	$2,$2,512
addiu	$3,$3,1
sra	$2,$2,10
sra	$3,$3,1
addu	$2,$11,$2
sb	$3,-1($12)
lbu	$3,-1($6)
lbu	$2,0($2)
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
bne	$4,$14,$L1538
sb	$2,-1($6)

lw	$fp,116($sp)
lw	$23,112($sp)
lw	$22,108($sp)
lw	$21,104($sp)
lw	$20,100($sp)
lw	$19,96($sp)
lw	$18,92($sp)
lw	$17,88($sp)
lw	$16,84($sp)
j	$31
addiu	$sp,$sp,120

.set	macro
.set	reorder
.end	avg_h264_qpel4_mc22_c
.size	avg_h264_qpel4_mc22_c, .-avg_h264_qpel4_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc02_c
.type	avg_h264_qpel4_mc02_c, @function
avg_h264_qpel4_mc02_c:
.frame	$sp,72,$31		# vars= 40, regs= 6/0, args= 0, gp= 8
.mask	0x003f0000,-4
.fmask	0x00000000,0
sll	$2,$6,1
addiu	$sp,$sp,-72
subu	$5,$5,$2
sw	$17,52($sp)
addu	$12,$4,$6
addu	$17,$5,$6
sw	$16,48($sp)
lui	$10,%hi(ff_cropTbl+1024)
sw	$18,56($sp)
addu	$16,$17,$6
sw	$21,68($sp)
addu	$11,$12,$6
sw	$20,64($sp)
addu	$15,$16,$6
sw	$19,60($sp)
addiu	$14,$4,4
addu	$8,$15,$6
addiu	$9,$sp,16
addu	$7,$8,$6
addiu	$10,$10,%lo(ff_cropTbl+1024)
addu	$3,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 3($5)  
lwr $18, 0($5)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
move	$5,$18
addu	$13,$11,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 3($17)  
lwr $18, 0($17)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 11($sp)  
swr $5, 8($sp)  

# 0 "" 2
#NO_APP
addu	$6,$2,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($16)  
lwr $5, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $18, 15($sp)  
swr $18, 12($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $16, 3($15)  
lwr $16, 0($15)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 19($sp)  
swr $5, 16($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 3($7)  
lwr $18, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($8)  
lwr $5, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $16, 23($sp)  
swr $16, 20($sp)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 27($sp)  
swr $5, 24($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $18, 31($sp)  
swr $18, 28($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 35($sp)  
swr $5, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($6)  
lwr $2, 0($6)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 39($sp)  
swr $3, 36($sp)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 43($sp)  
swr $2, 40($sp)  

# 0 "" 2
#NO_APP
$L1543:
lbu	$15,4($9)
addiu	$4,$4,1
lbu	$16,8($9)
addiu	$12,$12,1
lbu	$18,0($9)
addiu	$11,$11,1
lbu	$20,-4($9)
addiu	$13,$13,1
lbu	$2,12($9)
addu	$5,$15,$16
addu	$6,$18,$15
lbu	$8,-8($9)
addu	$21,$20,$16
lbu	$3,16($9)
sll	$7,$6,2
lbu	$19,-1($4)
sll	$6,$6,4
lbu	$17,20($9)
sll	$25,$21,2
lbu	$24,24($9)
addu	$6,$7,$6
addu	$25,$25,$21
addu	$7,$8,$2
subu	$25,$6,$25
addu	$6,$18,$2
addu	$25,$25,$7
sll	$8,$6,2
addiu	$25,$25,16
sll	$7,$5,2
sra	$25,$25,5
sll	$5,$5,4
addu	$25,$10,$25
addu	$5,$7,$5
addu	$8,$8,$6
addu	$20,$20,$3
lbu	$6,0($25)
subu	$8,$5,$8
addu	$25,$15,$3
addu	$8,$8,$20
addu	$19,$19,$6
addiu	$8,$8,16
addu	$5,$16,$2
addiu	$19,$19,1
sra	$8,$8,5
sll	$7,$5,2
sra	$19,$19,1
sll	$5,$5,4
sll	$6,$25,2
addu	$8,$10,$8
sb	$19,-1($4)
addu	$5,$7,$5
lbu	$19,-1($12)
addu	$6,$6,$25
lbu	$7,0($8)
addu	$18,$18,$17
subu	$6,$5,$6
addu	$3,$2,$3
addu	$6,$6,$18
addu	$7,$19,$7
addiu	$6,$6,16
addu	$16,$16,$17
addiu	$7,$7,1
sra	$6,$6,5
sll	$5,$3,2
sra	$7,$7,1
sll	$3,$3,4
sll	$2,$16,2
addu	$6,$10,$6
sb	$7,-1($12)
addu	$3,$5,$3
lbu	$7,-1($11)
addu	$2,$2,$16
lbu	$5,0($6)
addu	$15,$15,$24
subu	$2,$3,$2
addiu	$9,$9,1
addu	$2,$2,$15
addu	$3,$7,$5
addiu	$2,$2,16
addiu	$3,$3,1
sra	$2,$2,5
sra	$3,$3,1
addu	$2,$10,$2
sb	$3,-1($11)
lbu	$3,-1($13)
lbu	$2,0($2)
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
.set	noreorder
.set	nomacro
bne	$4,$14,$L1543
sb	$2,-1($13)
.set	macro
.set	reorder

lw	$21,68($sp)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc02_c
.size	avg_h264_qpel4_mc02_c, .-avg_h264_qpel4_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc22_c
.type	put_h264_qpel16_mc22_c, @function
put_h264_qpel16_mc22_c:
.frame	$sp,736,$31		# vars= 672, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-736
sw	$20,724($sp)
li	$20,16			# 0x10
sw	$21,728($sp)
addiu	$21,$sp,32
sw	$17,712($sp)
move	$17,$5
sw	$16,708($sp)
move	$16,$6
sw	$19,720($sp)
move	$5,$21
sw	$18,716($sp)
addiu	$19,$sp,48
sw	$20,16($sp)
move	$18,$4
sw	$6,20($sp)
move	$7,$16
sw	$31,732($sp)
.option	pic0
jal	put_h264_qpel8_hv_lowpass
.option	pic2
move	$6,$17

addiu	$4,$18,8
addiu	$6,$17,8
sw	$20,16($sp)
sw	$16,20($sp)
move	$5,$19
.option	pic0
jal	put_h264_qpel8_hv_lowpass
.option	pic2
move	$7,$16

sll	$2,$16,3
move	$5,$21
sw	$20,16($sp)
addu	$17,$17,$2
sw	$16,20($sp)
addu	$18,$18,$2
move	$6,$17
move	$4,$18
.option	pic0
jal	put_h264_qpel8_hv_lowpass
.option	pic2
move	$7,$16

addiu	$4,$18,8
addiu	$6,$17,8
sw	$20,16($sp)
sw	$16,20($sp)
move	$5,$19
.option	pic0
jal	put_h264_qpel8_hv_lowpass
.option	pic2
move	$7,$16

lw	$31,732($sp)
lw	$21,728($sp)
lw	$20,724($sp)
lw	$19,720($sp)
lw	$18,716($sp)
lw	$17,712($sp)
lw	$16,708($sp)
j	$31
addiu	$sp,$sp,736

.set	macro
.set	reorder
.end	put_h264_qpel16_mc22_c
.size	put_h264_qpel16_mc22_c, .-put_h264_qpel16_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_h_lowpass.constprop.22
.type	put_h264_qpel16_h_lowpass.constprop.22, @function
put_h264_qpel16_h_lowpass.constprop.22:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$16,24($sp)
move	$16,$6
li	$6,16			# 0x10
sw	$18,32($sp)
sw	$17,28($sp)
move	$18,$4
move	$17,$5
sw	$31,36($sp)
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

addiu	$4,$18,8
addiu	$5,$17,8
li	$6,16			# 0x10
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

sll	$2,$16,3
addiu	$4,$18,128
addu	$17,$17,$2
li	$6,16			# 0x10
move	$5,$17
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$7,$16

addiu	$4,$18,136
lw	$31,36($sp)
addiu	$5,$17,8
lw	$18,32($sp)
move	$7,$16
lw	$17,28($sp)
li	$6,16			# 0x10
lw	$16,24($sp)
.option	pic0
j	put_h264_qpel8_h_lowpass
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_h264_qpel16_h_lowpass.constprop.22
.size	put_h264_qpel16_h_lowpass.constprop.22, .-put_h264_qpel16_h_lowpass.constprop.22
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_hv_lowpass.constprop.25
.type	put_h264_qpel8_hv_lowpass.constprop.25, @function
put_h264_qpel8_hv_lowpass.constprop.25:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$7,1
addiu	$10,$5,416
subu	$6,$6,$2
move	$2,$5
$L1551:
lbu	$8,0($6)
addiu	$2,$2,32
lbu	$12,1($6)
lbu	$3,2($6)
lbu	$11,-1($6)
addu	$12,$8,$12
lbu	$9,3($6)
lbu	$8,-2($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-32($2)
lbu	$8,1($6)
lbu	$12,2($6)
lbu	$3,3($6)
lbu	$11,0($6)
addu	$12,$8,$12
lbu	$9,4($6)
lbu	$8,-1($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-30($2)
lbu	$8,2($6)
lbu	$12,3($6)
lbu	$3,4($6)
lbu	$11,1($6)
addu	$12,$8,$12
lbu	$9,5($6)
lbu	$8,0($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-28($2)
lbu	$8,3($6)
lbu	$12,4($6)
lbu	$3,5($6)
lbu	$11,2($6)
addu	$12,$8,$12
lbu	$9,6($6)
lbu	$8,1($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-26($2)
lbu	$8,5($6)
lbu	$12,4($6)
lbu	$3,2($6)
lbu	$9,7($6)
lbu	$11,3($6)
addu	$12,$12,$8
lbu	$13,6($6)
sll	$8,$12,2
sll	$12,$12,4
addu	$11,$11,$13
addu	$9,$9,$3
addu	$12,$8,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-24($2)
lbu	$8,5($6)
lbu	$12,6($6)
lbu	$3,7($6)
lbu	$11,4($6)
addu	$12,$8,$12
lbu	$9,8($6)
lbu	$8,3($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-22($2)
lbu	$8,6($6)
lbu	$12,7($6)
lbu	$3,8($6)
lbu	$11,5($6)
addu	$12,$8,$12
lbu	$9,9($6)
lbu	$8,4($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-20($2)
lbu	$8,7($6)
lbu	$12,8($6)
lbu	$3,9($6)
lbu	$11,6($6)
addu	$12,$8,$12
lbu	$9,10($6)
lbu	$8,5($6)
addu	$6,$6,$7
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
bne	$2,$10,$L1551
sh	$3,-18($2)

lui	$24,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-32
addiu	$5,$5,64
addiu	$25,$4,8
sw	$23,28($sp)
addiu	$24,$24,%lo(ff_cropTbl+1024)
sw	$22,24($sp)
sw	$21,20($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
$L1552:
lh	$10,64($5)
addiu	$5,$5,2
lh	$12,30($5)
addiu	$4,$4,1
addu	$6,$12,$10
lh	$14,-2($5)
lh	$19,-34($5)
lh	$23,94($5)
addu	$8,$14,$12
lh	$9,-66($5)
addu	$11,$19,$10
lh	$21,126($5)
sll	$3,$8,2
lh	$17,158($5)
sll	$7,$11,2
lh	$2,190($5)
sll	$8,$8,4
lh	$20,222($5)
addu	$7,$7,$11
lh	$22,286($5)
addu	$8,$3,$8
lh	$18,318($5)
addu	$13,$14,$23
lh	$3,254($5)
subu	$7,$8,$7
addu	$9,$9,$23
sll	$8,$6,2
sll	$11,$13,2
sll	$6,$6,4
addu	$9,$7,$9
addu	$6,$8,$6
addu	$13,$11,$13
addiu	$9,$9,512
subu	$7,$6,$13
addu	$13,$19,$21
sra	$9,$9,10
addu	$16,$12,$21
addu	$11,$10,$23
addu	$13,$7,$13
addu	$9,$24,$9
sll	$6,$11,2
sll	$8,$16,2
sll	$11,$11,4
addiu	$13,$13,512
addu	$15,$6,$11
lbu	$6,0($9)
addu	$11,$8,$16
sra	$13,$13,10
addu	$16,$10,$17
addu	$7,$23,$21
sb	$6,-1($4)
subu	$11,$15,$11
addu	$14,$14,$17
addu	$13,$24,$13
sll	$9,$7,2
sll	$6,$16,2
sll	$7,$7,4
lbu	$13,0($13)
addu	$11,$11,$14
addu	$8,$9,$7
addu	$6,$6,$16
addiu	$11,$11,512
sb	$13,15($4)
addu	$14,$23,$2
addu	$9,$21,$17
subu	$7,$8,$6
addu	$6,$12,$2
sra	$8,$11,10
sll	$13,$9,2
sll	$16,$14,2
sll	$9,$9,4
addu	$6,$7,$6
addu	$7,$24,$8
addu	$19,$13,$9
addu	$16,$16,$14
addiu	$6,$6,512
lbu	$8,0($7)
subu	$9,$19,$16
addu	$7,$21,$20
addu	$14,$17,$2
addu	$16,$10,$20
sb	$8,31($4)
sra	$19,$6,10
sll	$15,$14,2
sll	$12,$7,2
addu	$16,$9,$16
sll	$14,$14,4
addu	$19,$24,$19
addu	$14,$15,$14
addu	$12,$12,$7
addiu	$15,$16,512
lbu	$7,0($19)
subu	$13,$14,$12
addu	$12,$23,$3
sra	$14,$15,10
addu	$10,$2,$20
sb	$7,47($4)
addu	$6,$17,$3
addu	$12,$13,$12
addu	$13,$24,$14
sll	$11,$10,2
sll	$8,$6,2
sll	$10,$10,4
addu	$20,$20,$3
lbu	$3,0($13)
addu	$10,$11,$10
addu	$8,$8,$6
addu	$2,$2,$22
addiu	$11,$12,512
sb	$3,63($4)
subu	$9,$10,$8
addu	$8,$21,$22
sll	$7,$20,2
sll	$3,$2,2
sra	$10,$11,10
sll	$20,$20,4
addu	$8,$9,$8
addu	$6,$7,$20
addu	$2,$3,$2
addu	$9,$24,$10
addiu	$7,$8,512
subu	$3,$6,$2
addu	$2,$17,$18
lbu	$8,0($9)
sra	$6,$7,10
addu	$2,$3,$2
addu	$3,$24,$6
addiu	$2,$2,512
sb	$8,79($4)
lbu	$3,0($3)
sra	$2,$2,10
addu	$2,$24,$2
sb	$3,95($4)
lbu	$2,0($2)
bne	$4,$25,$L1552
sb	$2,111($4)

lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	put_h264_qpel8_hv_lowpass.constprop.25
.size	put_h264_qpel8_hv_lowpass.constprop.25, .-put_h264_qpel8_hv_lowpass.constprop.25
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_hv_lowpass.constprop.20
.type	put_h264_qpel16_hv_lowpass.constprop.20, @function
put_h264_qpel16_hv_lowpass.constprop.20:
.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-48
sw	$19,36($sp)
addiu	$19,$5,16
sw	$18,32($sp)
move	$18,$4
sw	$16,24($sp)
move	$16,$6
sw	$31,44($sp)
sw	$20,40($sp)
move	$20,$5
sw	$17,28($sp)
.option	pic0
jal	put_h264_qpel8_hv_lowpass.constprop.25
.option	pic2
move	$17,$7

addiu	$4,$18,8
addiu	$6,$16,8
move	$5,$19
.option	pic0
jal	put_h264_qpel8_hv_lowpass.constprop.25
.option	pic2
move	$7,$17

sll	$2,$17,3
addiu	$4,$18,128
addu	$16,$16,$2
move	$5,$20
move	$6,$16
.option	pic0
jal	put_h264_qpel8_hv_lowpass.constprop.25
.option	pic2
move	$7,$17

addiu	$4,$18,136
lw	$31,44($sp)
addiu	$6,$16,8
lw	$20,40($sp)
move	$5,$19
lw	$18,32($sp)
move	$7,$17
lw	$19,36($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.option	pic0
j	put_h264_qpel8_hv_lowpass.constprop.25
.option	pic2
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_h264_qpel16_hv_lowpass.constprop.20
.size	put_h264_qpel16_hv_lowpass.constprop.20, .-put_h264_qpel16_hv_lowpass.constprop.20
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_hv_lowpass.constprop.26
.type	put_h264_qpel8_hv_lowpass.constprop.26, @function
put_h264_qpel8_hv_lowpass.constprop.26:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$7,1
addiu	$10,$5,208
subu	$6,$6,$2
move	$2,$5
$L1559:
lbu	$8,0($6)
addiu	$2,$2,16
lbu	$12,1($6)
lbu	$3,2($6)
lbu	$11,-1($6)
addu	$12,$8,$12
lbu	$9,3($6)
lbu	$8,-2($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-16($2)
lbu	$8,1($6)
lbu	$12,2($6)
lbu	$3,3($6)
lbu	$11,0($6)
addu	$12,$8,$12
lbu	$9,4($6)
lbu	$8,-1($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-14($2)
lbu	$8,2($6)
lbu	$12,3($6)
lbu	$3,4($6)
lbu	$11,1($6)
addu	$12,$8,$12
lbu	$9,5($6)
lbu	$8,0($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-12($2)
lbu	$8,3($6)
lbu	$12,4($6)
lbu	$3,5($6)
lbu	$11,2($6)
addu	$12,$8,$12
lbu	$9,6($6)
lbu	$8,1($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-10($2)
lbu	$8,5($6)
lbu	$12,4($6)
lbu	$3,2($6)
lbu	$9,7($6)
lbu	$11,3($6)
addu	$12,$12,$8
lbu	$13,6($6)
sll	$8,$12,2
sll	$12,$12,4
addu	$11,$11,$13
addu	$9,$9,$3
addu	$12,$8,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-8($2)
lbu	$8,5($6)
lbu	$12,6($6)
lbu	$3,7($6)
lbu	$11,4($6)
addu	$12,$8,$12
lbu	$9,8($6)
lbu	$8,3($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-6($2)
lbu	$8,6($6)
lbu	$12,7($6)
lbu	$3,8($6)
lbu	$11,5($6)
addu	$12,$8,$12
lbu	$9,9($6)
lbu	$8,4($6)
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
sh	$3,-4($2)
lbu	$8,7($6)
lbu	$12,8($6)
lbu	$3,9($6)
lbu	$11,6($6)
addu	$12,$8,$12
lbu	$9,10($6)
lbu	$8,5($6)
addu	$6,$6,$7
addu	$11,$11,$3
sll	$3,$12,2
sll	$12,$12,4
addu	$9,$9,$8
addu	$12,$3,$12
sll	$3,$11,2
addu	$8,$9,$12
addu	$3,$11,$3
subu	$3,$8,$3
bne	$2,$10,$L1559
sh	$3,-2($2)

lui	$24,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-32
addiu	$5,$5,32
addiu	$25,$4,8
sw	$23,28($sp)
addiu	$24,$24,%lo(ff_cropTbl+1024)
sw	$22,24($sp)
sw	$21,20($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
$L1560:
lh	$10,32($5)
addiu	$5,$5,2
lh	$12,14($5)
addiu	$4,$4,1
addu	$6,$12,$10
lh	$14,-2($5)
lh	$19,-18($5)
lh	$23,46($5)
addu	$8,$14,$12
lh	$9,-34($5)
addu	$11,$19,$10
lh	$21,62($5)
sll	$3,$8,2
lh	$17,78($5)
sll	$7,$11,2
lh	$2,94($5)
sll	$8,$8,4
lh	$20,110($5)
addu	$7,$7,$11
lh	$22,142($5)
addu	$8,$3,$8
lh	$18,158($5)
addu	$13,$14,$23
lh	$3,126($5)
subu	$7,$8,$7
addu	$9,$9,$23
sll	$8,$6,2
sll	$11,$13,2
sll	$6,$6,4
addu	$9,$7,$9
addu	$6,$8,$6
addu	$13,$11,$13
addiu	$9,$9,512
subu	$7,$6,$13
addu	$13,$19,$21
sra	$9,$9,10
addu	$16,$12,$21
addu	$11,$10,$23
addu	$13,$7,$13
addu	$9,$24,$9
sll	$6,$11,2
sll	$8,$16,2
sll	$11,$11,4
addiu	$13,$13,512
addu	$15,$6,$11
lbu	$6,0($9)
addu	$11,$8,$16
sra	$13,$13,10
addu	$16,$10,$17
addu	$7,$23,$21
sb	$6,-1($4)
subu	$11,$15,$11
addu	$14,$14,$17
addu	$13,$24,$13
sll	$9,$7,2
sll	$6,$16,2
sll	$7,$7,4
lbu	$13,0($13)
addu	$11,$11,$14
addu	$8,$9,$7
addu	$6,$6,$16
addiu	$11,$11,512
sb	$13,7($4)
addu	$14,$23,$2
addu	$9,$21,$17
subu	$7,$8,$6
addu	$6,$12,$2
sra	$8,$11,10
sll	$13,$9,2
sll	$16,$14,2
sll	$9,$9,4
addu	$6,$7,$6
addu	$7,$24,$8
addu	$19,$13,$9
addu	$16,$16,$14
addiu	$6,$6,512
lbu	$8,0($7)
subu	$9,$19,$16
addu	$7,$21,$20
addu	$14,$17,$2
addu	$16,$10,$20
sb	$8,15($4)
sra	$19,$6,10
sll	$15,$14,2
sll	$12,$7,2
addu	$16,$9,$16
sll	$14,$14,4
addu	$19,$24,$19
addu	$14,$15,$14
addu	$12,$12,$7
addiu	$15,$16,512
lbu	$7,0($19)
subu	$13,$14,$12
addu	$12,$23,$3
sra	$14,$15,10
addu	$10,$2,$20
sb	$7,23($4)
addu	$6,$17,$3
addu	$12,$13,$12
addu	$13,$24,$14
sll	$11,$10,2
sll	$8,$6,2
sll	$10,$10,4
addu	$20,$20,$3
lbu	$3,0($13)
addu	$10,$11,$10
addu	$8,$8,$6
addu	$2,$2,$22
addiu	$11,$12,512
sb	$3,31($4)
subu	$9,$10,$8
addu	$8,$21,$22
sll	$7,$20,2
sll	$3,$2,2
sra	$10,$11,10
sll	$20,$20,4
addu	$8,$9,$8
addu	$6,$7,$20
addu	$2,$3,$2
addu	$9,$24,$10
addiu	$7,$8,512
subu	$3,$6,$2
addu	$2,$17,$18
lbu	$8,0($9)
sra	$6,$7,10
addu	$2,$3,$2
addu	$3,$24,$6
addiu	$2,$2,512
sb	$8,39($4)
lbu	$3,0($3)
sra	$2,$2,10
addu	$2,$24,$2
sb	$3,47($4)
lbu	$2,0($2)
bne	$4,$25,$L1560
sb	$2,55($4)

lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	put_h264_qpel8_hv_lowpass.constprop.26
.size	put_h264_qpel8_hv_lowpass.constprop.26, .-put_h264_qpel8_hv_lowpass.constprop.26
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc21_c
.type	avg_h264_qpel8_mc21_c, @function
avg_h264_qpel8_mc21_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-400
sw	$17,380($sp)
addiu	$17,$sp,312
sw	$18,384($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$19,388($sp)
sw	$16,376($sp)
move	$19,$4
addiu	$16,$sp,248
sw	$31,396($sp)
move	$7,$18
sw	$20,392($sp)
move	$4,$17
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$20,$5

addiu	$5,$sp,40
move	$4,$16
move	$6,$20
.option	pic0
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$7,$18

li	$2,8			# 0x8
move	$4,$19
move	$5,$17
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	avg_h264_qpel8_mc21_c
.size	avg_h264_qpel8_mc21_c, .-avg_h264_qpel8_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc23_c
.type	avg_h264_qpel8_mc23_c, @function
avg_h264_qpel8_mc23_c:
.frame	$sp,400,$31		# vars= 336, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-400
sw	$18,384($sp)
addiu	$18,$sp,312
sw	$19,388($sp)
move	$19,$5
sw	$16,376($sp)
addu	$5,$5,$6
move	$16,$6
sw	$20,392($sp)
li	$6,8			# 0x8
sw	$17,380($sp)
move	$20,$4
sw	$31,396($sp)
addiu	$17,$sp,248
move	$7,$16
.option	pic0
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$4,$18

addiu	$5,$sp,40
move	$4,$17
move	$6,$19
.option	pic0
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$7,$16

li	$2,8			# 0x8
move	$4,$20
move	$5,$18
sw	$2,16($sp)
move	$6,$17
sw	$2,20($sp)
move	$7,$16
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)

lw	$31,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
j	$31
addiu	$sp,$sp,400

.set	macro
.set	reorder
.end	avg_h264_qpel8_mc23_c
.size	avg_h264_qpel8_mc23_c, .-avg_h264_qpel8_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_v_lowpass.constprop.27
.type	put_h264_qpel8_v_lowpass.constprop.27, @function
put_h264_qpel8_v_lowpass.constprop.27:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$15,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-32
addiu	$24,$4,8
addiu	$15,$15,%lo(ff_cropTbl+1024)
sw	$23,28($sp)
sw	$22,24($sp)
sw	$21,20($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
$L1569:
lbu	$10,32($5)
addiu	$5,$5,1
lbu	$12,15($5)
addiu	$4,$4,1
addu	$6,$12,$10
lbu	$14,-1($5)
lbu	$25,-17($5)
lbu	$23,47($5)
addu	$8,$14,$12
lbu	$9,-33($5)
addu	$11,$25,$10
lbu	$21,63($5)
sll	$3,$8,2
lbu	$17,79($5)
sll	$7,$11,2
lbu	$2,95($5)
sll	$8,$8,4
lbu	$20,111($5)
addu	$7,$7,$11
lbu	$22,143($5)
addu	$8,$3,$8
lbu	$18,159($5)
addu	$13,$14,$23
lbu	$3,127($5)
subu	$7,$8,$7
addu	$9,$9,$23
sll	$8,$6,2
sll	$11,$13,2
sll	$6,$6,4
addu	$9,$7,$9
addu	$6,$8,$6
addu	$13,$11,$13
addiu	$9,$9,16
subu	$7,$6,$13
addu	$13,$25,$21
sra	$9,$9,5
addu	$19,$12,$21
addu	$11,$10,$23
addu	$13,$7,$13
addu	$9,$15,$9
sll	$6,$11,2
sll	$8,$19,2
sll	$11,$11,4
addiu	$13,$13,16
addu	$16,$6,$11
lbu	$6,0($9)
addu	$11,$8,$19
sra	$13,$13,5
addu	$25,$10,$17
addu	$7,$23,$21
sb	$6,-1($4)
subu	$11,$16,$11
addu	$14,$14,$17
addu	$13,$15,$13
sll	$9,$7,2
sll	$6,$25,2
sll	$7,$7,4
lbu	$13,0($13)
addu	$11,$11,$14
addu	$8,$9,$7
addu	$6,$6,$25
addiu	$11,$11,16
sb	$13,15($4)
addu	$14,$23,$2
addu	$9,$21,$17
subu	$7,$8,$6
addu	$6,$12,$2
sra	$8,$11,5
sll	$13,$9,2
sll	$16,$14,2
sll	$9,$9,4
addu	$6,$7,$6
addu	$7,$15,$8
addu	$19,$13,$9
addu	$16,$16,$14
addiu	$6,$6,16
lbu	$8,0($7)
subu	$9,$19,$16
addu	$7,$21,$20
addu	$14,$17,$2
addu	$16,$10,$20
sb	$8,31($4)
sra	$19,$6,5
sll	$25,$14,2
sll	$12,$7,2
addu	$16,$9,$16
sll	$14,$14,4
addu	$19,$15,$19
addu	$14,$25,$14
addu	$12,$12,$7
addiu	$25,$16,16
lbu	$7,0($19)
subu	$13,$14,$12
addu	$12,$23,$3
sra	$14,$25,5
addu	$10,$2,$20
sb	$7,47($4)
addu	$6,$17,$3
addu	$12,$13,$12
addu	$13,$15,$14
sll	$11,$10,2
sll	$8,$6,2
sll	$10,$10,4
addu	$20,$20,$3
lbu	$3,0($13)
addu	$10,$11,$10
addu	$8,$8,$6
addu	$2,$2,$22
addiu	$11,$12,16
sb	$3,63($4)
subu	$9,$10,$8
addu	$8,$21,$22
sll	$7,$20,2
sll	$3,$2,2
sra	$10,$11,5
sll	$20,$20,4
addu	$8,$9,$8
addu	$6,$7,$20
addu	$2,$3,$2
addu	$9,$15,$10
addiu	$7,$8,16
subu	$3,$6,$2
addu	$2,$17,$18
lbu	$8,0($9)
sra	$6,$7,5
addu	$2,$3,$2
addu	$3,$15,$6
addiu	$2,$2,16
sb	$8,79($4)
lbu	$3,0($3)
sra	$2,$2,5
addu	$2,$15,$2
sb	$3,95($4)
lbu	$2,0($2)
bne	$4,$24,$L1569
sb	$2,111($4)

lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	put_h264_qpel8_v_lowpass.constprop.27
.size	put_h264_qpel8_v_lowpass.constprop.27, .-put_h264_qpel8_v_lowpass.constprop.27
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_v_lowpass.constprop.23
.type	put_h264_qpel16_v_lowpass.constprop.23, @function
put_h264_qpel16_v_lowpass.constprop.23:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$17,32($sp)
move	$17,$4
sw	$16,28($sp)
sw	$31,36($sp)
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
move	$16,$5

addiu	$4,$17,8
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
addiu	$5,$16,8

addiu	$4,$17,128
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
addiu	$5,$16,128

addiu	$4,$17,136
lw	$31,36($sp)
addiu	$5,$16,136
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
j	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_h264_qpel16_v_lowpass.constprop.23
.size	put_h264_qpel16_v_lowpass.constprop.23, .-put_h264_qpel16_v_lowpass.constprop.23
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_hv_lowpass.constprop.28
.type	put_h264_qpel4_hv_lowpass.constprop.28, @function
put_h264_qpel4_hv_lowpass.constprop.28:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-32
addiu	$12,$5,72
sw	$23,28($sp)
move	$9,$5
lw	$11,48($sp)
sw	$22,24($sp)
sw	$21,20($sp)
sll	$2,$11,1
sw	$20,16($sp)
sw	$19,12($sp)
subu	$6,$6,$2
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
$L1575:
lbu	$3,0($6)
addiu	$9,$9,8
lbu	$13,1($6)
lbu	$2,2($6)
lbu	$10,-1($6)
addu	$13,$3,$13
lbu	$8,3($6)
lbu	$3,-2($6)
addu	$10,$10,$2
sll	$2,$13,2
sll	$13,$13,4
addu	$8,$8,$3
addu	$13,$2,$13
sll	$2,$10,2
addu	$3,$8,$13
addu	$2,$10,$2
subu	$2,$3,$2
sh	$2,-8($9)
lbu	$3,1($6)
lbu	$13,2($6)
lbu	$2,3($6)
lbu	$10,0($6)
addu	$13,$3,$13
lbu	$8,4($6)
lbu	$3,-1($6)
addu	$10,$10,$2
sll	$2,$13,2
sll	$13,$13,4
addu	$8,$8,$3
addu	$13,$2,$13
sll	$2,$10,2
addu	$3,$8,$13
addu	$2,$10,$2
subu	$2,$3,$2
sh	$2,-6($9)
lbu	$3,2($6)
lbu	$13,3($6)
lbu	$2,4($6)
lbu	$10,1($6)
addu	$13,$3,$13
lbu	$8,5($6)
lbu	$3,0($6)
addu	$10,$10,$2
sll	$2,$13,2
sll	$13,$13,4
addu	$8,$8,$3
addu	$13,$2,$13
sll	$2,$10,2
addu	$3,$8,$13
addu	$2,$10,$2
subu	$2,$3,$2
sh	$2,-4($9)
lbu	$3,3($6)
lbu	$13,4($6)
lbu	$2,5($6)
lbu	$10,2($6)
addu	$13,$3,$13
lbu	$8,6($6)
lbu	$3,1($6)
addu	$6,$6,$11
addu	$10,$10,$2
sll	$2,$13,2
sll	$13,$13,4
addu	$8,$8,$3
addu	$13,$2,$13
sll	$2,$10,2
addu	$3,$8,$13
addu	$2,$10,$2
subu	$2,$3,$2
bne	$9,$12,$L1575
sh	$2,-2($9)

lui	$15,%hi(ff_cropTbl+1024)
addiu	$14,$5,16
addiu	$15,$15,%lo(ff_cropTbl+1024)
addiu	$5,$5,24
$L1576:
lh	$16,8($14)
addiu	$14,$14,2
lh	$2,14($14)
addu	$25,$4,$7
addiu	$4,$4,1
addu	$24,$25,$7
addu	$3,$16,$2
lh	$20,-2($14)
lh	$9,-10($14)
lh	$22,22($14)
addu	$11,$20,$16
lh	$13,-18($14)
addu	$10,$9,$2
lh	$6,30($14)
sll	$12,$11,2
lh	$21,38($14)
sll	$8,$10,2
lh	$17,46($14)
sll	$11,$11,4
addu	$10,$8,$10
addu	$11,$12,$11
addu	$13,$13,$22
subu	$10,$11,$10
addu	$18,$20,$22
addu	$13,$10,$13
sll	$8,$3,2
sll	$11,$18,2
sll	$3,$3,4
addiu	$13,$13,512
addu	$3,$8,$3
addu	$18,$11,$18
addu	$8,$2,$22
addu	$23,$16,$6
sra	$13,$13,10
subu	$19,$3,$18
addu	$18,$9,$6
sll	$12,$8,2
addu	$13,$15,$13
sll	$8,$8,4
sll	$9,$23,2
addu	$18,$19,$18
lbu	$3,0($13)
addu	$11,$12,$8
addu	$9,$9,$23
addu	$12,$22,$6
addu	$2,$2,$21
sb	$3,-1($4)
addiu	$13,$18,512
subu	$10,$11,$9
addu	$9,$20,$21
sll	$8,$12,2
sra	$11,$13,10
sll	$12,$12,4
sll	$13,$2,2
addu	$9,$10,$9
addu	$6,$8,$12
addu	$2,$13,$2
addu	$10,$15,$11
addiu	$8,$9,512
subu	$3,$6,$2
addu	$2,$16,$17
lbu	$9,0($10)
sra	$6,$8,10
addu	$2,$3,$2
addu	$3,$15,$6
addiu	$2,$2,512
sb	$9,0($25)
addu	$6,$24,$7
lbu	$3,0($3)
sra	$2,$2,10
addu	$2,$15,$2
sb	$3,0($24)
lbu	$2,0($2)
bne	$14,$5,$L1576
sb	$2,0($6)

lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	put_h264_qpel4_hv_lowpass.constprop.28
.size	put_h264_qpel4_hv_lowpass.constprop.28, .-put_h264_qpel4_hv_lowpass.constprop.28
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc22_c
.type	put_h264_qpel4_mc22_c, @function
put_h264_qpel4_mc22_c:
.frame	$sp,112,$31		# vars= 72, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-112
move	$7,$6
move	$6,$5
sw	$31,108($sp)
addiu	$5,$sp,32
.option	pic0
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
sw	$7,16($sp)

lw	$31,108($sp)
j	$31
addiu	$sp,$sp,112

.set	macro
.set	reorder
.end	put_h264_qpel4_mc22_c
.size	put_h264_qpel4_mc22_c, .-put_h264_qpel4_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_v_lowpass.constprop.29
.type	put_h264_qpel4_v_lowpass.constprop.29, @function
put_h264_qpel4_v_lowpass.constprop.29:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$14,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-40
addiu	$15,$4,4
addiu	$14,$14,%lo(ff_cropTbl+1024)
sw	$fp,36($sp)
sw	$23,32($sp)
sw	$22,28($sp)
sw	$21,24($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
$L1583:
lbu	$16,4($5)
addiu	$5,$5,1
lbu	$2,7($5)
addu	$25,$4,$6
lbu	$20,-1($5)
addiu	$4,$4,1
lbu	$9,-5($5)
addu	$24,$25,$6
addu	$3,$16,$2
addu	$11,$20,$16
addu	$10,$9,$2
sll	$12,$11,2
lbu	$22,11($5)
sll	$8,$10,2
lbu	$13,-9($5)
sll	$11,$11,4
lbu	$7,15($5)
addu	$10,$8,$10
lbu	$21,19($5)
addu	$11,$12,$11
lbu	$17,23($5)
addu	$13,$13,$22
subu	$10,$11,$10
addu	$18,$20,$22
addu	$13,$10,$13
sll	$8,$3,2
sll	$11,$18,2
sll	$3,$3,4
addiu	$13,$13,16
addu	$3,$8,$3
addu	$18,$11,$18
addu	$23,$2,$22
addu	$fp,$16,$7
sra	$13,$13,5
subu	$19,$3,$18
addu	$18,$9,$7
sll	$12,$23,2
addu	$13,$14,$13
sll	$23,$23,4
sll	$9,$fp,2
addu	$18,$19,$18
lbu	$3,0($13)
addu	$11,$12,$23
addu	$9,$9,$fp
addu	$12,$22,$7
addu	$2,$2,$21
sb	$3,-1($4)
addiu	$13,$18,16
subu	$10,$11,$9
addu	$9,$20,$21
sll	$8,$12,2
sra	$11,$13,5
sll	$12,$12,4
sll	$13,$2,2
addu	$9,$10,$9
addu	$7,$8,$12
addu	$2,$13,$2
addu	$10,$14,$11
addiu	$8,$9,16
subu	$3,$7,$2
addu	$2,$16,$17
lbu	$9,0($10)
sra	$7,$8,5
addu	$2,$3,$2
addu	$3,$14,$7
addiu	$2,$2,16
sb	$9,0($25)
addu	$7,$24,$6
lbu	$3,0($3)
sra	$2,$2,5
addu	$2,$14,$2
sb	$3,0($24)
lbu	$2,0($2)
bne	$4,$15,$L1583
sb	$2,0($7)

lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	put_h264_qpel4_v_lowpass.constprop.29
.size	put_h264_qpel4_v_lowpass.constprop.29, .-put_h264_qpel4_v_lowpass.constprop.29
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc02_c
.type	put_h264_qpel4_mc02_c, @function
put_h264_qpel4_mc02_c:
.frame	$sp,72,$31		# vars= 40, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
sll	$3,$6,1
addiu	$sp,$sp,-72
subu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
#NO_APP
addu	$7,$3,$6
sw	$31,68($sp)
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 27($sp)  
swr $8, 24($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
addu	$10,$3,$6
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 31($sp)  
swr $8, 28($sp)  

# 0 "" 2
#NO_APP
addu	$9,$10,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$8,$9,$6
move	$3,$7
addu	$7,$8,$6
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 35($sp)  
swr $3, 32($sp)  

# 0 "" 2
#NO_APP
addiu	$5,$sp,32
addu	$3,$7,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 39($sp)  
swr $11, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 43($sp)  
swr $10, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 47($sp)  
swr $9, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 51($sp)  
swr $8, 48($sp)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 55($sp)  
swr $7, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 59($sp)  
swr $8, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
lw	$31,68($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

.end	put_h264_qpel4_mc02_c
.size	put_h264_qpel4_mc02_c, .-put_h264_qpel4_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_hv_lowpass.constprop.30
.type	put_h264_qpel2_hv_lowpass.constprop.30, @function
put_h264_qpel2_hv_lowpass.constprop.30:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$11,16($sp)
addiu	$12,$5,28
move	$13,$5
sll	$2,$11,1
subu	$6,$6,$2
$L1589:
lbu	$3,0($6)
addiu	$13,$13,4
lbu	$10,1($6)
lbu	$2,2($6)
lbu	$9,-1($6)
addu	$10,$3,$10
lbu	$8,3($6)
lbu	$3,-2($6)
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
sh	$2,-4($13)
lbu	$3,1($6)
lbu	$10,2($6)
lbu	$2,3($6)
lbu	$9,0($6)
addu	$10,$3,$10
lbu	$8,4($6)
lbu	$3,-1($6)
addu	$6,$6,$11
addu	$9,$9,$2
sll	$2,$10,2
sll	$10,$10,4
addu	$8,$8,$3
addu	$10,$2,$10
sll	$2,$9,2
addu	$3,$8,$10
addu	$2,$9,$2
subu	$2,$3,$2
bne	$13,$12,$L1589
sh	$2,-2($13)

lh	$15,4($5)
lui	$13,%hi(ff_cropTbl+1024)
lh	$3,8($5)
addu	$7,$4,$7
lh	$14,12($5)
addiu	$13,$13,%lo(ff_cropTbl+1024)
lh	$6,16($5)
lh	$9,20($5)
addu	$11,$3,$14
lh	$24,0($5)
addu	$8,$15,$6
lh	$2,24($5)
sll	$12,$11,2
sll	$10,$8,2
sll	$11,$11,4
addu	$10,$10,$8
addu	$11,$12,$11
addu	$12,$14,$6
addu	$14,$3,$9
subu	$10,$11,$10
addu	$9,$24,$9
sll	$8,$12,2
sll	$3,$14,2
sll	$12,$12,4
addu	$9,$10,$9
addu	$6,$8,$12
addu	$3,$3,$14
addiu	$8,$9,512
subu	$3,$6,$3
addu	$2,$15,$2
sra	$6,$8,10
addu	$2,$3,$2
addu	$3,$13,$6
addiu	$2,$2,512
lbu	$3,0($3)
sra	$2,$2,10
addu	$2,$13,$2
sb	$3,0($4)
lbu	$2,0($2)
sb	$2,0($7)
lh	$8,14($5)
lh	$10,6($5)
lh	$9,10($5)
lh	$15,18($5)
lh	$2,22($5)
addu	$11,$9,$8
lh	$12,2($5)
addu	$24,$10,$15
lh	$14,26($5)
sll	$6,$11,2
sll	$3,$24,2
sll	$5,$11,4
addu	$11,$3,$24
addu	$5,$6,$5
addu	$9,$9,$2
addu	$6,$8,$15
subu	$3,$5,$11
addu	$2,$12,$2
sll	$8,$6,2
sll	$5,$9,2
sll	$6,$6,4
addu	$2,$3,$2
addu	$6,$8,$6
addu	$5,$5,$9
addiu	$2,$2,512
subu	$5,$6,$5
addu	$3,$10,$14
sra	$2,$2,10
addu	$3,$5,$3
addu	$2,$13,$2
addiu	$3,$3,512
lbu	$5,0($2)
sra	$2,$3,10
addu	$13,$13,$2
sb	$5,1($4)
lbu	$2,0($13)
j	$31
sb	$2,1($7)

.set	macro
.set	reorder
.end	put_h264_qpel2_hv_lowpass.constprop.30
.size	put_h264_qpel2_hv_lowpass.constprop.30, .-put_h264_qpel2_hv_lowpass.constprop.30
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc22_c
.type	put_h264_qpel2_mc22_c, @function
put_h264_qpel2_mc22_c:
.frame	$sp,72,$31		# vars= 32, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-72
move	$7,$6
move	$6,$5
sw	$31,68($sp)
addiu	$5,$sp,32
.option	pic0
jal	put_h264_qpel2_hv_lowpass.constprop.30
.option	pic2
sw	$7,16($sp)

lw	$31,68($sp)
j	$31
addiu	$sp,$sp,72

.set	macro
.set	reorder
.end	put_h264_qpel2_mc22_c
.size	put_h264_qpel2_mc22_c, .-put_h264_qpel2_mc22_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_v_lowpass.constprop.31
.type	put_h264_qpel2_v_lowpass.constprop.31, @function
put_h264_qpel2_v_lowpass.constprop.31:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lbu	$7,4($5)
lui	$13,%hi(ff_cropTbl+1024)
lbu	$24,-2($5)
addu	$6,$4,$6
lbu	$15,0($5)
addiu	$13,$13,%lo(ff_cropTbl+1024)
lbu	$14,2($5)
addu	$8,$24,$7
lbu	$9,6($5)
lbu	$25,-4($5)
addu	$11,$15,$14
lbu	$2,8($5)
sll	$10,$8,2
sll	$12,$11,2
sll	$11,$11,4
addu	$10,$10,$8
addu	$11,$12,$11
addu	$14,$14,$7
addu	$15,$15,$9
subu	$10,$11,$10
addu	$9,$25,$9
sll	$8,$14,2
sll	$3,$15,2
sll	$14,$14,4
addu	$9,$10,$9
addu	$7,$8,$14
addu	$15,$3,$15
addiu	$8,$9,16
subu	$3,$7,$15
addu	$2,$24,$2
sra	$7,$8,5
addu	$2,$3,$2
addu	$3,$13,$7
addiu	$2,$2,16
lbu	$3,0($3)
sra	$2,$2,5
addu	$2,$13,$2
sb	$3,0($4)
lbu	$2,0($2)
sb	$2,0($6)
lbu	$8,3($5)
lbu	$10,-1($5)
lbu	$9,1($5)
lbu	$15,5($5)
lbu	$2,7($5)
addu	$11,$9,$8
lbu	$12,-3($5)
addu	$3,$10,$15
lbu	$14,9($5)
sll	$7,$11,2
sll	$5,$11,4
sll	$11,$3,2
addu	$5,$7,$5
addu	$11,$11,$3
addu	$9,$9,$2
addu	$7,$8,$15
subu	$3,$5,$11
addu	$2,$12,$2
sll	$8,$7,2
sll	$5,$9,2
sll	$7,$7,4
addu	$2,$3,$2
addu	$7,$8,$7
addu	$5,$5,$9
addiu	$2,$2,16
subu	$5,$7,$5
addu	$3,$10,$14
sra	$2,$2,5
addu	$3,$5,$3
addu	$2,$13,$2
addiu	$3,$3,16
lbu	$5,0($2)
sra	$2,$3,5
addu	$13,$13,$2
sb	$5,1($4)
lbu	$2,0($13)
j	$31
sb	$2,1($6)

.set	macro
.set	reorder
.end	put_h264_qpel2_v_lowpass.constprop.31
.size	put_h264_qpel2_v_lowpass.constprop.31, .-put_h264_qpel2_v_lowpass.constprop.31
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc32_c
.type	put_h264_qpel2_mc32_c, @function
put_h264_qpel2_mc32_c:
.frame	$sp,112,$31		# vars= 56, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
subu	$2,$0,$6
addiu	$sp,$sp,-112
sll	$2,$2,1
sw	$16,92($sp)
move	$16,$6
addiu	$2,$2,1
sw	$31,108($sp)
sw	$18,100($sp)
move	$18,$5
addu	$2,$5,$2
sw	$17,96($sp)
sw	$19,104($sp)
move	$17,$4
addu	$3,$2,$6
lbu	$19,0($2)
addiu	$4,$sp,80
addu	$8,$3,$16
lbu	$11,1($2)
lbu	$25,0($3)
addiu	$5,$sp,64
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$24,0($8)
sll	$11,$11,8
addu	$3,$7,$16
lbu	$9,1($8)
lbu	$15,0($7)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$8,1($7)
lbu	$14,0($3)
sll	$9,$9,8
addu	$12,$2,$16
lbu	$7,1($3)
lbu	$13,0($2)
sll	$8,$8,8
lbu	$3,1($2)
or	$11,$11,$19
lbu	$2,1($12)
sll	$7,$7,8
lbu	$12,0($12)
or	$8,$8,$15
sll	$3,$3,8
sh	$11,60($sp)
sll	$2,$2,8
sh	$8,66($sp)
or	$7,$7,$14
or	$3,$3,$13
or	$2,$2,$12
sh	$7,68($sp)
or	$10,$10,$25
sh	$3,70($sp)
or	$9,$9,$24
sh	$2,72($sp)
li	$6,2			# 0x2
sh	$10,62($sp)
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$9,64($sp)

addiu	$4,$sp,76
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,2			# 0x2
.option	pic0
jal	put_h264_qpel2_hv_lowpass.constprop.30
.option	pic2
move	$6,$18

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$7,82($sp)
addu	$16,$17,$16
lhu	$2,78($sp)
ori	$4,$5,0xfefe
lhu	$8,80($sp)
lhu	$3,76($sp)
xor	$5,$2,$7
lw	$31,108($sp)
and	$5,$5,$4
lw	$19,104($sp)
xor	$6,$3,$8
lw	$18,100($sp)
and	$6,$6,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($17)
sh	$2,0($16)
lw	$17,96($sp)
lw	$16,92($sp)
j	$31
addiu	$sp,$sp,112

.set	macro
.set	reorder
.end	put_h264_qpel2_mc32_c
.size	put_h264_qpel2_mc32_c, .-put_h264_qpel2_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc12_c
.type	put_h264_qpel2_mc12_c, @function
put_h264_qpel2_mc12_c:
.frame	$sp,104,$31		# vars= 56, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$6,1
addiu	$sp,$sp,-104
subu	$2,$5,$2
sw	$16,88($sp)
move	$16,$6
addu	$3,$2,$6
sw	$31,100($sp)
sw	$18,96($sp)
move	$18,$5
addu	$8,$3,$16
sw	$17,92($sp)
lbu	$25,0($3)
move	$17,$4
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$6,1($2)
addiu	$4,$sp,80
addu	$3,$7,$16
lbu	$11,0($2)
lbu	$24,0($8)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$15,0($7)
lbu	$14,0($3)
sll	$6,$6,8
addu	$12,$2,$16
lbu	$9,1($8)
lbu	$13,0($2)
or	$11,$6,$11
lbu	$8,1($7)
or	$10,$10,$25
lbu	$7,1($3)
sll	$9,$9,8
lbu	$3,1($2)
addiu	$5,$sp,64
lbu	$2,1($12)
sll	$8,$8,8
lbu	$6,0($12)
sll	$7,$7,8
sll	$3,$3,8
sh	$11,60($sp)
sll	$2,$2,8
sh	$10,62($sp)
or	$8,$8,$15
or	$7,$7,$14
or	$3,$3,$13
sh	$8,66($sp)
or	$2,$2,$6
sh	$7,68($sp)
or	$9,$9,$24
sh	$3,70($sp)
li	$6,2			# 0x2
sh	$2,72($sp)
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$9,64($sp)

addiu	$4,$sp,76
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,2			# 0x2
.option	pic0
jal	put_h264_qpel2_hv_lowpass.constprop.30
.option	pic2
move	$6,$18

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$7,82($sp)
addu	$16,$17,$16
lhu	$2,78($sp)
ori	$4,$5,0xfefe
lhu	$8,80($sp)
lhu	$3,76($sp)
xor	$5,$2,$7
lw	$31,100($sp)
and	$5,$5,$4
lw	$18,96($sp)
xor	$6,$3,$8
and	$6,$6,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($17)
sh	$2,0($16)
lw	$17,92($sp)
lw	$16,88($sp)
j	$31
addiu	$sp,$sp,104

.set	macro
.set	reorder
.end	put_h264_qpel2_mc12_c
.size	put_h264_qpel2_mc12_c, .-put_h264_qpel2_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc33_c
.type	put_h264_qpel2_mc33_c, @function
put_h264_qpel2_mc33_c:
.frame	$sp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$18,56($sp)
move	$18,$4
sw	$17,52($sp)
addiu	$4,$sp,44
sw	$16,48($sp)
move	$17,$5
move	$16,$6
sw	$31,60($sp)
addu	$5,$5,$6
li	$6,2			# 0x2
.option	pic0
jal	put_h264_qpel2_h_lowpass
.option	pic2
move	$7,$16

subu	$2,$0,$16
addiu	$4,$sp,40
sll	$2,$2,1
addiu	$5,$sp,28
addiu	$2,$2,1
li	$6,2			# 0x2
addu	$2,$17,$2
addu	$3,$2,$16
lbu	$25,0($2)
addu	$8,$3,$16
lbu	$11,1($2)
lbu	$24,0($3)
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$17,0($8)
sll	$11,$11,8
addu	$3,$7,$16
lbu	$9,1($8)
lbu	$15,0($7)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$8,1($7)
lbu	$14,0($3)
sll	$9,$9,8
addu	$12,$2,$16
lbu	$7,1($3)
lbu	$13,0($2)
sll	$8,$8,8
lbu	$3,1($2)
or	$9,$9,$17
lbu	$2,1($12)
sll	$7,$7,8
lbu	$12,0($12)
or	$8,$8,$15
sll	$3,$3,8
sh	$9,28($sp)
sll	$2,$2,8
sh	$8,30($sp)
or	$7,$7,$14
or	$3,$3,$13
or	$2,$2,$12
sh	$7,32($sp)
or	$11,$11,$25
sh	$3,34($sp)
or	$10,$10,$24
sh	$2,36($sp)
sh	$11,24($sp)
addu	$16,$18,$16
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$10,26($sp)

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$7,46($sp)
lhu	$2,42($sp)
ori	$4,$5,0xfefe
lhu	$8,44($sp)
lhu	$3,40($sp)
xor	$5,$2,$7
lw	$31,60($sp)
and	$5,$5,$4
lw	$17,52($sp)
xor	$6,$3,$8
and	$6,$6,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($18)
sh	$2,0($16)
lw	$18,56($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_h264_qpel2_mc33_c
.size	put_h264_qpel2_mc33_c, .-put_h264_qpel2_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc13_c
.type	put_h264_qpel2_mc13_c, @function
put_h264_qpel2_mc13_c:
.frame	$sp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$18,56($sp)
move	$18,$4
sw	$17,52($sp)
addiu	$4,$sp,44
sw	$16,48($sp)
move	$17,$5
move	$16,$6
sw	$31,60($sp)
addu	$5,$5,$6
li	$6,2			# 0x2
.option	pic0
jal	put_h264_qpel2_h_lowpass
.option	pic2
move	$7,$16

sll	$2,$16,1
addiu	$4,$sp,40
subu	$2,$17,$2
addiu	$5,$sp,28
addu	$3,$2,$16
lbu	$25,0($2)
li	$6,2			# 0x2
addu	$8,$3,$16
lbu	$11,1($2)
lbu	$24,0($3)
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$17,0($8)
sll	$11,$11,8
addu	$3,$7,$16
lbu	$9,1($8)
lbu	$15,0($7)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$8,1($7)
lbu	$14,0($3)
sll	$9,$9,8
addu	$12,$2,$16
lbu	$7,1($3)
lbu	$13,0($2)
sll	$8,$8,8
lbu	$3,1($2)
or	$9,$9,$17
lbu	$2,1($12)
sll	$7,$7,8
lbu	$12,0($12)
or	$8,$8,$15
sll	$3,$3,8
sh	$9,28($sp)
sll	$2,$2,8
sh	$8,30($sp)
or	$7,$7,$14
or	$3,$3,$13
or	$2,$2,$12
sh	$7,32($sp)
or	$11,$11,$25
sh	$3,34($sp)
or	$10,$10,$24
sh	$2,36($sp)
sh	$11,24($sp)
addu	$16,$18,$16
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$10,26($sp)

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$7,46($sp)
lhu	$2,42($sp)
ori	$4,$5,0xfefe
lhu	$8,44($sp)
lhu	$3,40($sp)
xor	$5,$2,$7
lw	$31,60($sp)
and	$5,$5,$4
lw	$17,52($sp)
xor	$6,$3,$8
and	$6,$6,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($18)
sh	$2,0($16)
lw	$18,56($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_h264_qpel2_mc13_c
.size	put_h264_qpel2_mc13_c, .-put_h264_qpel2_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc31_c
.type	put_h264_qpel2_mc31_c, @function
put_h264_qpel2_mc31_c:
.frame	$sp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,52($sp)
move	$17,$4
sw	$16,48($sp)
addiu	$4,$sp,44
move	$16,$6
sw	$31,60($sp)
li	$6,2			# 0x2
sw	$18,56($sp)
move	$7,$16
.option	pic0
jal	put_h264_qpel2_h_lowpass
.option	pic2
move	$18,$5

subu	$2,$0,$16
addiu	$4,$sp,40
sll	$2,$2,1
addiu	$5,$sp,28
addiu	$2,$2,1
li	$6,2			# 0x2
addu	$2,$18,$2
addu	$3,$2,$16
lbu	$25,0($2)
addu	$8,$3,$16
lbu	$11,1($2)
lbu	$24,0($3)
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$18,0($8)
sll	$11,$11,8
addu	$3,$7,$16
lbu	$9,1($8)
lbu	$15,0($7)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$8,1($7)
lbu	$14,0($3)
sll	$9,$9,8
addu	$12,$2,$16
lbu	$7,1($3)
lbu	$13,0($2)
sll	$8,$8,8
lbu	$3,1($2)
or	$9,$9,$18
lbu	$2,1($12)
sll	$7,$7,8
lbu	$12,0($12)
or	$8,$8,$15
sll	$3,$3,8
sh	$9,28($sp)
sll	$2,$2,8
sh	$8,30($sp)
or	$7,$7,$14
or	$3,$3,$13
or	$2,$2,$12
sh	$7,32($sp)
or	$11,$11,$25
sh	$3,34($sp)
or	$10,$10,$24
sh	$2,36($sp)
sh	$11,24($sp)
addu	$16,$17,$16
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$10,26($sp)

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$7,46($sp)
lhu	$2,42($sp)
ori	$4,$5,0xfefe
lhu	$8,44($sp)
lhu	$3,40($sp)
xor	$5,$2,$7
lw	$31,60($sp)
and	$5,$5,$4
lw	$18,56($sp)
xor	$6,$3,$8
and	$6,$6,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($17)
sh	$2,0($16)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_h264_qpel2_mc31_c
.size	put_h264_qpel2_mc31_c, .-put_h264_qpel2_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc03_c
.type	put_h264_qpel2_mc03_c, @function
put_h264_qpel2_mc03_c:
.frame	$sp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$6,1
addiu	$sp,$sp,-64
subu	$2,$5,$2
sw	$16,48($sp)
move	$16,$6
addu	$3,$2,$6
sw	$31,60($sp)
sw	$17,52($sp)
addiu	$5,$sp,28
addu	$8,$3,$6
sw	$18,56($sp)
lbu	$25,0($3)
move	$17,$4
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$18,0($2)
addiu	$4,$sp,40
addu	$3,$7,$16
lbu	$11,1($2)
lbu	$24,0($8)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$15,0($7)
lbu	$14,0($3)
sll	$11,$11,8
addu	$12,$2,$16
lbu	$9,1($8)
lbu	$13,0($2)
or	$11,$11,$18
lbu	$8,1($7)
or	$10,$10,$25
lbu	$7,1($3)
sll	$9,$9,8
lbu	$3,1($2)
li	$6,2			# 0x2
lbu	$2,1($12)
sll	$8,$8,8
lbu	$12,0($12)
sll	$7,$7,8
sll	$3,$3,8
sh	$11,24($sp)
sll	$2,$2,8
sh	$10,26($sp)
or	$8,$8,$15
or	$7,$7,$14
or	$3,$3,$13
sh	$8,30($sp)
or	$2,$2,$12
sh	$7,32($sp)
or	$9,$9,$24
sh	$3,34($sp)
sh	$2,36($sp)
addu	$16,$17,$16
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$9,28($sp)

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$8,30($sp)
lhu	$7,32($sp)
ori	$4,$5,0xfefe
lhu	$2,42($sp)
lhu	$3,40($sp)
lw	$31,60($sp)
xor	$5,$2,$7
lw	$18,56($sp)
xor	$6,$3,$8
and	$6,$6,$4
and	$5,$5,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($17)
sh	$2,0($16)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_h264_qpel2_mc03_c
.size	put_h264_qpel2_mc03_c, .-put_h264_qpel2_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc02_c
.type	put_h264_qpel2_mc02_c, @function
put_h264_qpel2_mc02_c:
.frame	$sp,48,$31		# vars= 16, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$3,$6,1
addiu	$sp,$sp,-48
subu	$3,$5,$3
sw	$31,44($sp)
addiu	$5,$sp,28
addu	$7,$3,$6
sw	$16,40($sp)
lbu	$13,0($3)
addu	$9,$7,$6
lbu	$10,1($3)
lbu	$25,0($7)
addu	$8,$9,$6
lbu	$12,1($7)
lbu	$24,0($9)
sll	$10,$10,8
addu	$7,$8,$6
lbu	$11,1($9)
lbu	$15,0($8)
or	$10,$10,$13
addu	$3,$7,$6
lbu	$9,1($8)
lbu	$16,0($7)
sll	$11,$11,8
addu	$2,$3,$6
lbu	$8,1($7)
lbu	$14,0($3)
sll	$12,$12,8
lbu	$7,1($3)
sll	$9,$9,8
lbu	$3,1($2)
sll	$8,$8,8
lbu	$13,0($2)
or	$12,$12,$25
sll	$7,$7,8
sh	$10,24($sp)
sll	$2,$3,8
sh	$12,26($sp)
or	$8,$8,$16
or	$10,$11,$24
or	$9,$9,$15
sh	$8,32($sp)
or	$3,$7,$14
sh	$10,28($sp)
or	$2,$2,$13
sh	$9,30($sp)
sh	$3,34($sp)
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$2,36($sp)

lw	$31,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,48

.set	macro
.set	reorder
.end	put_h264_qpel2_mc02_c
.size	put_h264_qpel2_mc02_c, .-put_h264_qpel2_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc01_c
.type	put_h264_qpel2_mc01_c, @function
put_h264_qpel2_mc01_c:
.frame	$sp,64,$31		# vars= 24, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$6,1
addiu	$sp,$sp,-64
subu	$2,$5,$2
sw	$16,48($sp)
move	$16,$6
addu	$3,$2,$6
sw	$31,60($sp)
sw	$17,52($sp)
addiu	$5,$sp,28
addu	$8,$3,$6
sw	$18,56($sp)
lbu	$25,0($3)
move	$17,$4
addu	$7,$8,$16
lbu	$10,1($3)
lbu	$18,0($2)
addiu	$4,$sp,40
addu	$3,$7,$16
lbu	$11,1($2)
lbu	$24,0($8)
sll	$10,$10,8
addu	$2,$3,$16
lbu	$15,0($7)
lbu	$14,0($3)
sll	$11,$11,8
addu	$12,$2,$16
lbu	$9,1($8)
lbu	$13,0($2)
or	$11,$11,$18
lbu	$8,1($7)
or	$10,$10,$25
lbu	$7,1($3)
sll	$9,$9,8
lbu	$3,1($2)
li	$6,2			# 0x2
lbu	$2,1($12)
sll	$8,$8,8
lbu	$12,0($12)
sll	$7,$7,8
sll	$3,$3,8
sh	$11,24($sp)
sll	$2,$2,8
sh	$10,26($sp)
or	$8,$8,$15
or	$7,$7,$14
or	$3,$3,$13
sh	$8,30($sp)
or	$2,$2,$12
sh	$7,32($sp)
or	$9,$9,$24
sh	$3,34($sp)
sh	$2,36($sp)
addu	$16,$17,$16
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
sh	$9,28($sp)

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$8,28($sp)
lhu	$7,30($sp)
ori	$4,$5,0xfefe
lhu	$2,42($sp)
lhu	$3,40($sp)
lw	$31,60($sp)
xor	$5,$2,$7
lw	$18,56($sp)
xor	$6,$3,$8
and	$6,$6,$4
and	$5,$5,$4
srl	$6,$6,1
srl	$5,$5,1
or	$3,$3,$8
or	$2,$2,$7
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($17)
sh	$2,0($16)
lw	$17,52($sp)
lw	$16,48($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_h264_qpel2_mc01_c
.size	put_h264_qpel2_mc01_c, .-put_h264_qpel2_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc20_c
.type	avg_qpel16_mc20_c, @function
avg_qpel16_mc20_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-8
li	$10,16			# 0x10
addiu	$2,$2,%lo(ff_cropTbl+1024)
sw	$17,4($sp)
sw	$16,0($sp)
$L1611:
lbu	$9,1($5)
addiu	$10,$10,-1
lbu	$15,0($5)
lbu	$25,2($5)
lbu	$24,3($5)
addu	$7,$15,$9
lbu	$17,4($5)
addu	$15,$15,$25
lbu	$14,0($4)
addu	$24,$9,$24
lbu	$13,1($4)
sll	$9,$7,2
lbu	$11,2($4)
sll	$16,$15,1
lbu	$12,3($4)
sll	$7,$7,4
sll	$15,$15,3
addu	$8,$9,$7
subu	$15,$16,$15
sll	$9,$24,1
addu	$7,$8,$15
addu	$3,$9,$24
addu	$25,$25,$17
addu	$3,$7,$3
subu	$3,$3,$25
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$3,$14,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,0($4)
lbu	$8,0($5)
lbu	$24,1($5)
lbu	$7,2($5)
lbu	$14,3($5)
lbu	$15,4($5)
addu	$7,$24,$7
lbu	$16,5($5)
addu	$14,$8,$14
addu	$15,$8,$15
sll	$9,$7,2
sll	$25,$14,1
sll	$7,$7,4
sll	$14,$14,3
addu	$8,$9,$7
subu	$14,$25,$14
sll	$9,$15,1
addu	$7,$8,$14
addu	$3,$9,$15
addu	$24,$24,$16
addu	$3,$7,$3
subu	$3,$3,$24
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$3,$13,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,1($4)
lbu	$7,3($5)
lbu	$9,1($5)
lbu	$8,2($5)
lbu	$13,4($5)
lbu	$14,0($5)
lbu	$15,5($5)
addu	$8,$8,$7
addu	$13,$9,$13
lbu	$16,6($5)
sll	$9,$8,2
addu	$15,$14,$15
sll	$7,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$7,$13
sll	$9,$15,1
addu	$7,$8,$13
addu	$3,$9,$15
addu	$14,$14,$16
addu	$3,$7,$3
subu	$3,$3,$14
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$11,$11,$3
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,2($4)
lbu	$7,4($5)
lbu	$9,2($5)
lbu	$8,3($5)
lbu	$15,5($5)
lbu	$11,1($5)
lbu	$24,6($5)
addu	$8,$8,$7
addu	$15,$9,$15
lbu	$25,0($5)
sll	$9,$8,2
lbu	$16,7($5)
addu	$24,$11,$24
lbu	$14,4($4)
sll	$7,$15,1
lbu	$11,5($4)
sll	$8,$8,4
lbu	$13,6($4)
sll	$15,$15,3
addu	$8,$9,$8
subu	$15,$7,$15
sll	$9,$24,1
addu	$7,$8,$15
addu	$3,$9,$24
addu	$8,$25,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$12,$12,$3
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,3($4)
lbu	$7,5($5)
lbu	$9,3($5)
lbu	$8,4($5)
lbu	$12,6($5)
lbu	$3,7($5)
lbu	$15,2($5)
addu	$8,$8,$7
addu	$12,$9,$12
lbu	$16,8($5)
sll	$9,$8,2
lbu	$24,1($5)
addu	$15,$15,$3
sll	$7,$12,1
sll	$8,$8,4
sll	$12,$12,3
addu	$8,$9,$8
subu	$12,$7,$12
sll	$9,$15,1
addu	$7,$8,$12
addu	$3,$9,$15
addu	$8,$24,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$3,$14,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,4($4)
lbu	$7,6($5)
lbu	$9,4($5)
lbu	$8,5($5)
lbu	$12,7($5)
lbu	$3,8($5)
lbu	$14,3($5)
addu	$8,$8,$7
addu	$12,$9,$12
lbu	$15,2($5)
sll	$9,$8,2
lbu	$16,9($5)
addu	$14,$14,$3
sll	$7,$12,1
sll	$8,$8,4
sll	$12,$12,3
addu	$8,$9,$8
subu	$12,$7,$12
sll	$9,$14,1
addu	$7,$8,$12
addu	$3,$9,$14
addu	$8,$15,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$11,$11,$3
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,5($4)
lbu	$7,7($5)
lbu	$9,5($5)
lbu	$8,6($5)
lbu	$15,8($5)
lbu	$11,4($5)
lbu	$24,9($5)
addu	$8,$8,$7
addu	$15,$9,$15
lbu	$25,3($5)
sll	$9,$8,2
lbu	$16,10($5)
addu	$24,$11,$24
lbu	$14,7($4)
sll	$7,$15,1
lbu	$11,8($4)
sll	$8,$8,4
lbu	$12,9($4)
sll	$15,$15,3
addu	$8,$9,$8
subu	$15,$7,$15
sll	$9,$24,1
addu	$7,$8,$15
addu	$3,$9,$24
addu	$8,$25,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$13,$13,$3
addiu	$13,$13,1
sra	$13,$13,1
sb	$13,6($4)
lbu	$7,8($5)
lbu	$9,6($5)
lbu	$8,7($5)
lbu	$13,9($5)
lbu	$3,10($5)
lbu	$15,5($5)
addu	$8,$8,$7
addu	$13,$9,$13
lbu	$16,11($5)
sll	$9,$8,2
lbu	$24,4($5)
addu	$15,$15,$3
sll	$7,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$7,$13
sll	$9,$15,1
addu	$7,$8,$13
addu	$3,$9,$15
addu	$8,$24,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$3,$14,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,7($4)
lbu	$7,9($5)
lbu	$9,7($5)
lbu	$8,8($5)
lbu	$13,10($5)
lbu	$3,11($5)
lbu	$14,6($5)
addu	$8,$8,$7
addu	$13,$9,$13
lbu	$15,5($5)
sll	$9,$8,2
lbu	$16,12($5)
addu	$14,$14,$3
sll	$7,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$7,$13
sll	$9,$14,1
addu	$7,$8,$13
addu	$3,$9,$14
addu	$8,$15,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$11,$11,$3
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,8($4)
lbu	$7,10($5)
lbu	$9,8($5)
lbu	$8,9($5)
lbu	$15,11($5)
lbu	$11,7($5)
lbu	$24,12($5)
addu	$8,$8,$7
addu	$15,$9,$15
lbu	$25,6($5)
sll	$9,$8,2
lbu	$16,13($5)
addu	$24,$11,$24
lbu	$14,10($4)
sll	$7,$15,1
lbu	$11,11($4)
sll	$8,$8,4
lbu	$13,12($4)
sll	$15,$15,3
addu	$8,$9,$8
subu	$15,$7,$15
sll	$9,$24,1
addu	$7,$8,$15
addu	$3,$9,$24
addu	$8,$25,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$12,$12,$3
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,9($4)
lbu	$7,11($5)
lbu	$9,9($5)
lbu	$8,10($5)
lbu	$12,12($5)
lbu	$3,13($5)
lbu	$15,8($5)
addu	$8,$8,$7
addu	$12,$9,$12
lbu	$16,14($5)
sll	$9,$8,2
lbu	$24,7($5)
addu	$15,$15,$3
sll	$7,$12,1
sll	$8,$8,4
sll	$12,$12,3
addu	$8,$9,$8
subu	$12,$7,$12
sll	$9,$15,1
addu	$7,$8,$12
addu	$3,$9,$15
addu	$8,$24,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$3,$14,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,10($4)
lbu	$7,12($5)
lbu	$9,10($5)
lbu	$8,11($5)
lbu	$12,13($5)
lbu	$3,14($5)
lbu	$14,9($5)
addu	$8,$8,$7
addu	$12,$9,$12
lbu	$15,8($5)
sll	$9,$8,2
lbu	$16,15($5)
addu	$14,$14,$3
sll	$7,$12,1
sll	$8,$8,4
sll	$12,$12,3
addu	$8,$9,$8
subu	$12,$7,$12
sll	$9,$14,1
addu	$7,$8,$12
addu	$3,$9,$14
addu	$8,$15,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$11,$11,$3
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,11($4)
lbu	$7,13($5)
lbu	$9,11($5)
lbu	$8,12($5)
lbu	$15,14($5)
lbu	$11,10($5)
lbu	$24,15($5)
addu	$8,$8,$7
addu	$15,$9,$15
lbu	$16,16($5)
sll	$9,$8,2
lbu	$25,9($5)
addu	$24,$11,$24
lbu	$14,13($4)
sll	$7,$15,1
lbu	$12,14($4)
sll	$8,$8,4
lbu	$11,15($4)
sll	$15,$15,3
addu	$8,$9,$8
subu	$15,$7,$15
sll	$9,$24,1
addu	$7,$8,$15
addu	$3,$9,$24
addu	$8,$25,$16
addu	$3,$7,$3
subu	$3,$3,$8
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$13,$13,$3
addiu	$13,$13,1
sra	$13,$13,1
sb	$13,12($4)
lbu	$7,14($5)
lbu	$9,12($5)
lbu	$8,13($5)
lbu	$13,15($5)
lbu	$15,16($5)
lbu	$24,11($5)
addu	$8,$8,$7
addu	$13,$9,$13
lbu	$16,10($5)
sll	$9,$8,2
addu	$24,$24,$15
sll	$7,$13,1
sll	$8,$8,4
sll	$13,$13,3
addu	$8,$9,$8
subu	$13,$7,$13
sll	$9,$24,1
addu	$7,$8,$13
addu	$3,$9,$24
addu	$15,$15,$16
addu	$3,$7,$3
subu	$3,$3,$15
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$3,$14,$3
addiu	$3,$3,1
sra	$3,$3,1
sb	$3,13($4)
lbu	$8,16($5)
lbu	$15,15($5)
lbu	$7,14($5)
lbu	$13,13($5)
lbu	$14,12($5)
addu	$7,$7,$15
lbu	$16,11($5)
addu	$13,$13,$8
addu	$14,$8,$14
sll	$9,$7,2
sll	$24,$13,1
sll	$7,$7,4
sll	$13,$13,3
addu	$8,$9,$7
subu	$13,$24,$13
sll	$9,$14,1
addu	$7,$8,$13
addu	$3,$9,$14
addu	$15,$15,$16
addu	$3,$7,$3
subu	$3,$3,$15
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$12,$12,$3
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,14($4)
lbu	$9,15($5)
lbu	$12,16($5)
lbu	$14,14($5)
lbu	$13,13($5)
addu	$7,$9,$12
lbu	$16,12($5)
addu	$12,$12,$14
addu	$13,$9,$13
sll	$15,$12,1
sll	$9,$7,2
sll	$12,$12,3
sll	$7,$7,4
subu	$12,$15,$12
addu	$8,$9,$7
sll	$9,$13,1
addu	$7,$8,$12
addu	$3,$9,$13
addu	$14,$14,$16
addu	$3,$7,$3
addu	$5,$5,$6
subu	$3,$3,$14
addiu	$3,$3,16
sra	$3,$3,5
addu	$3,$2,$3
lbu	$3,0($3)
addu	$11,$11,$3
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,15($4)
bne	$10,$0,$L1611
addu	$4,$4,$6

lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	avg_qpel16_mc20_c
.size	avg_qpel16_mc20_c, .-avg_qpel16_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc20_c
.type	avg_qpel8_mc20_c, @function
avg_qpel8_mc20_c:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$9,%hi(ff_cropTbl+1024)
addiu	$sp,$sp,-8
li	$10,8			# 0x8
addiu	$9,$9,%lo(ff_cropTbl+1024)
sw	$17,4($sp)
sw	$16,0($sp)
$L1615:
lbu	$8,1($5)
addiu	$10,$10,-1
lbu	$15,0($5)
lbu	$25,2($5)
lbu	$24,3($5)
addu	$3,$15,$8
lbu	$17,4($5)
addu	$15,$15,$25
lbu	$14,0($4)
addu	$24,$8,$24
lbu	$12,1($4)
sll	$8,$3,2
lbu	$11,2($4)
sll	$16,$15,1
lbu	$13,3($4)
sll	$3,$3,4
sll	$15,$15,3
addu	$7,$8,$3
subu	$15,$16,$15
sll	$8,$24,1
addu	$3,$7,$15
addu	$2,$8,$24
addu	$25,$25,$17
addu	$2,$3,$2
subu	$2,$2,$25
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$14,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,0($4)
lbu	$7,0($5)
lbu	$24,1($5)
lbu	$3,2($5)
lbu	$14,3($5)
lbu	$15,4($5)
addu	$3,$24,$3
lbu	$16,5($5)
addu	$14,$7,$14
addu	$15,$7,$15
sll	$8,$3,2
sll	$25,$14,1
sll	$3,$3,4
sll	$14,$14,3
addu	$7,$8,$3
subu	$14,$25,$14
sll	$8,$15,1
addu	$3,$7,$14
addu	$2,$8,$15
addu	$24,$24,$16
addu	$2,$3,$2
subu	$2,$2,$24
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$12,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,1($4)
lbu	$3,3($5)
lbu	$8,1($5)
lbu	$7,2($5)
lbu	$12,4($5)
lbu	$14,0($5)
lbu	$15,5($5)
addu	$7,$7,$3
addu	$12,$8,$12
lbu	$16,6($5)
sll	$8,$7,2
addu	$15,$14,$15
sll	$3,$12,1
sll	$7,$7,4
sll	$12,$12,3
addu	$7,$8,$7
subu	$12,$3,$12
sll	$8,$15,1
addu	$3,$7,$12
addu	$2,$8,$15
addu	$14,$14,$16
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$11,$11,$2
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,2($4)
lbu	$3,4($5)
lbu	$8,2($5)
lbu	$7,3($5)
lbu	$15,5($5)
lbu	$11,1($5)
lbu	$24,6($5)
addu	$7,$7,$3
addu	$15,$8,$15
lbu	$16,7($5)
sll	$8,$7,2
lbu	$25,0($5)
addu	$24,$11,$24
lbu	$14,4($4)
sll	$3,$15,1
lbu	$12,5($4)
sll	$7,$7,4
lbu	$11,6($4)
sll	$15,$15,3
addu	$7,$8,$7
subu	$15,$3,$15
sll	$8,$24,1
addu	$3,$7,$15
addu	$2,$8,$24
addu	$7,$25,$16
addu	$2,$3,$2
subu	$2,$2,$7
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$13,$13,$2
addiu	$13,$13,1
sra	$13,$13,1
sb	$13,3($4)
lbu	$3,5($5)
lbu	$8,3($5)
lbu	$7,4($5)
lbu	$13,6($5)
lbu	$2,7($5)
lbu	$15,2($5)
addu	$7,$7,$3
addu	$13,$8,$13
lbu	$16,8($5)
sll	$8,$7,2
lbu	$24,1($5)
addu	$15,$15,$2
sll	$3,$13,1
sll	$7,$7,4
sll	$13,$13,3
addu	$7,$8,$7
subu	$13,$3,$13
sll	$8,$15,1
addu	$3,$7,$13
addu	$2,$8,$15
addu	$7,$24,$16
addu	$2,$3,$2
subu	$2,$2,$7
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$14,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,4($4)
lbu	$3,6($5)
lbu	$8,4($5)
lbu	$7,5($5)
lbu	$13,7($5)
lbu	$14,8($5)
lbu	$15,3($5)
addu	$7,$7,$3
addu	$13,$8,$13
lbu	$16,2($5)
sll	$8,$7,2
addu	$15,$15,$14
sll	$3,$13,1
sll	$7,$7,4
sll	$13,$13,3
addu	$7,$8,$7
subu	$13,$3,$13
sll	$8,$15,1
addu	$3,$7,$13
addu	$2,$8,$15
addu	$14,$14,$16
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$12,$12,$2
addiu	$12,$12,1
sra	$12,$12,1
sb	$12,5($4)
lbu	$7,8($5)
lbu	$14,7($5)
lbu	$3,6($5)
lbu	$12,5($5)
lbu	$13,4($5)
addu	$3,$3,$14
lbu	$16,3($5)
addu	$12,$12,$7
addu	$13,$7,$13
sll	$8,$3,2
sll	$15,$12,1
sll	$3,$3,4
sll	$12,$12,3
addu	$7,$8,$3
subu	$12,$15,$12
sll	$8,$13,1
addu	$3,$7,$12
addu	$2,$8,$13
addu	$14,$14,$16
addu	$2,$3,$2
subu	$2,$2,$14
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
lbu	$12,7($4)
addu	$11,$11,$2
addiu	$11,$11,1
sra	$11,$11,1
sb	$11,6($4)
lbu	$8,7($5)
lbu	$11,8($5)
lbu	$14,6($5)
lbu	$13,5($5)
addu	$3,$8,$11
lbu	$16,4($5)
addu	$11,$11,$14
addu	$13,$8,$13
sll	$15,$11,1
sll	$8,$3,2
sll	$11,$11,3
sll	$3,$3,4
subu	$11,$15,$11
addu	$7,$8,$3
sll	$8,$13,1
addu	$3,$7,$11
addu	$2,$8,$13
addu	$14,$14,$16
addu	$2,$3,$2
addu	$5,$5,$6
subu	$2,$2,$14
addiu	$2,$2,16
sra	$2,$2,5
addu	$2,$9,$2
lbu	$2,0($2)
addu	$2,$12,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,7($4)
bne	$10,$0,$L1615
addu	$4,$4,$6

lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	avg_qpel8_mc20_c
.size	avg_qpel8_mc20_c, .-avg_qpel8_mc20_c
.align	2
.set	nomips16
.set	nomicromips
.ent	copy_block17.constprop.40
.type	copy_block17.constprop.40, @function
copy_block17.constprop.40:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
addiu	$3,$4,408
$L1619:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 3($4)  
swr $2, 0($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($4)  
swr $2, 4($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 11($5)  
lwr $2, 8($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 11($4)  
swr $2, 8($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 15($5)  
lwr $2, 12($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 15($4)  
swr $2, 12($4)  

# 0 "" 2
#NO_APP
lbu	$2,16($5)
addiu	$4,$4,24
addu	$5,$5,$6
.set	noreorder
.set	nomacro
bne	$4,$3,$L1619
sb	$2,-8($4)
.set	macro
.set	reorder

j	$31
.end	copy_block17.constprop.40
.size	copy_block17.constprop.40, .-copy_block17.constprop.40
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc02_c
.type	avg_qpel16_mc02_c, @function
avg_qpel16_mc02_c:
.frame	$sp,448,$31		# vars= 408, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-448
sw	$16,432($sp)
addiu	$16,$sp,24
sw	$18,440($sp)
move	$18,$4
move	$4,$16
sw	$31,444($sp)
sw	$17,436($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6

li	$7,24			# 0x18
move	$4,$18
move	$5,$16
.option	pic0
jal	avg_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17

lw	$31,444($sp)
lw	$18,440($sp)
lw	$17,436($sp)
lw	$16,432($sp)
j	$31
addiu	$sp,$sp,448

.set	macro
.set	reorder
.end	avg_qpel16_mc02_c
.size	avg_qpel16_mc02_c, .-avg_qpel16_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc02_c
.type	put_no_rnd_qpel16_mc02_c, @function
put_no_rnd_qpel16_mc02_c:
.frame	$sp,448,$31		# vars= 408, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-448
sw	$16,432($sp)
addiu	$16,$sp,24
sw	$18,440($sp)
move	$18,$4
move	$4,$16
sw	$31,444($sp)
sw	$17,436($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6

li	$7,24			# 0x18
move	$4,$18
move	$5,$16
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17

lw	$31,444($sp)
lw	$18,440($sp)
lw	$17,436($sp)
lw	$16,432($sp)
j	$31
addiu	$sp,$sp,448

.set	macro
.set	reorder
.end	put_no_rnd_qpel16_mc02_c
.size	put_no_rnd_qpel16_mc02_c, .-put_no_rnd_qpel16_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc02_c
.type	put_qpel16_mc02_c, @function
put_qpel16_mc02_c:
.frame	$sp,448,$31		# vars= 408, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-448
sw	$16,432($sp)
addiu	$16,$sp,24
sw	$18,440($sp)
move	$18,$4
move	$4,$16
sw	$31,444($sp)
sw	$17,436($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6

li	$7,24			# 0x18
move	$4,$18
move	$5,$16
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17

lw	$31,444($sp)
lw	$18,440($sp)
lw	$17,436($sp)
lw	$16,432($sp)
j	$31
addiu	$sp,$sp,448

.set	macro
.set	reorder
.end	put_qpel16_mc02_c
.size	put_qpel16_mc02_c, .-put_qpel16_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	copy_block16.constprop.41
.type	copy_block16.constprop.41, @function
copy_block16.constprop.41:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
addiu	$3,$4,336
$L1628:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 3($4)  
swr $2, 0($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($4)  
swr $2, 4($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 11($5)  
lwr $2, 8($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 11($4)  
swr $2, 8($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 15($5)  
lwr $2, 12($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 15($4)  
swr $2, 12($4)  

# 0 "" 2
#NO_APP
addiu	$4,$4,16
.set	noreorder
.set	nomacro
bne	$4,$3,$L1628
addu	$5,$5,$6
.set	macro
.set	reorder

j	$31
.end	copy_block16.constprop.41
.size	copy_block16.constprop.41, .-copy_block16.constprop.41
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc02_c
.type	avg_h264_qpel16_mc02_c, @function
avg_h264_qpel16_mc02_c:
.frame	$sp,376,$31		# vars= 336, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-376
sll	$2,$6,1
sw	$17,368($sp)
move	$17,$4
subu	$5,$5,$2
sw	$31,372($sp)
addiu	$4,$sp,24
sw	$16,364($sp)
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
move	$16,$6

addiu	$5,$sp,56
move	$4,$17
.option	pic0
jal	avg_h264_qpel8_v_lowpass.constprop.16
.option	pic2
move	$6,$16

addiu	$4,$17,8
addiu	$5,$sp,64
.option	pic0
jal	avg_h264_qpel8_v_lowpass.constprop.16
.option	pic2
move	$6,$16

sll	$2,$16,3
addiu	$5,$sp,184
addu	$17,$17,$2
move	$6,$16
.option	pic0
jal	avg_h264_qpel8_v_lowpass.constprop.16
.option	pic2
move	$4,$17

addiu	$4,$17,8
addiu	$5,$sp,192
.option	pic0
jal	avg_h264_qpel8_v_lowpass.constprop.16
.option	pic2
move	$6,$16

lw	$31,372($sp)
lw	$17,368($sp)
lw	$16,364($sp)
j	$31
addiu	$sp,$sp,376

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc02_c
.size	avg_h264_qpel16_mc02_c, .-avg_h264_qpel16_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc02_c
.type	put_h264_qpel16_mc02_c, @function
put_h264_qpel16_mc02_c:
.frame	$sp,376,$31		# vars= 336, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-376
sll	$2,$6,1
sw	$17,368($sp)
move	$17,$4
subu	$5,$5,$2
sw	$31,372($sp)
addiu	$4,$sp,24
sw	$16,364($sp)
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
move	$16,$6

addiu	$5,$sp,56
li	$7,16			# 0x10
move	$4,$17
.option	pic0
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$6,$16

addiu	$4,$17,8
addiu	$5,$sp,64
li	$7,16			# 0x10
.option	pic0
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$6,$16

sll	$2,$16,3
addiu	$5,$sp,184
addu	$17,$17,$2
li	$7,16			# 0x10
move	$4,$17
.option	pic0
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$6,$16

addiu	$4,$17,8
addiu	$5,$sp,192
li	$7,16			# 0x10
.option	pic0
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$6,$16

lw	$31,372($sp)
lw	$17,368($sp)
lw	$16,364($sp)
j	$31
addiu	$sp,$sp,376

.set	macro
.set	reorder
.end	put_h264_qpel16_mc02_c
.size	put_h264_qpel16_mc02_c, .-put_h264_qpel16_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	copy_block9.constprop.42
.type	copy_block9.constprop.42, @function
copy_block9.constprop.42:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
addiu	$3,$4,144
$L1635:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($5)  
lwr $2, 0($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 3($4)  
swr $2, 0($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($4)  
swr $2, 4($4)  

# 0 "" 2
#NO_APP
lbu	$2,8($5)
addiu	$4,$4,16
addu	$5,$5,$6
.set	noreorder
.set	nomacro
bne	$4,$3,$L1635
sb	$2,-8($4)
.set	macro
.set	reorder

j	$31
.end	copy_block9.constprop.42
.size	copy_block9.constprop.42, .-copy_block9.constprop.42
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels2_x2_c
.type	avg_pixels2_x2_c, @function
avg_pixels2_x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1642
li	$14,-16908288			# 0xfffffffffefe0000

move	$11,$0
move	$12,$0
ori	$14,$14,0xfefe
$L1639:
addu	$9,$5,$11
addu	$13,$4,$11
addiu	$12,$12,1
lbu	$15,1($9)
addu	$11,$11,$6
lbu	$8,2($9)
lbu	$3,0($9)
sll	$10,$15,8
lhu	$9,0($13)
sll	$8,$8,8
or	$2,$10,$3
or	$3,$8,$15
xor	$8,$3,$2
and	$8,$8,$14
srl	$8,$8,1
or	$3,$3,$2
subu	$2,$3,$8
xor	$3,$9,$2
and	$3,$3,$14
srl	$3,$3,1
or	$2,$9,$2
subu	$2,$2,$3
bne	$12,$7,$L1639
sh	$2,0($13)

$L1642:
j	$31
nop

.set	macro
.set	reorder
.end	avg_pixels2_x2_c
.size	avg_pixels2_x2_c, .-avg_pixels2_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_pixels16_l2_c
.type	put_no_rnd_pixels16_l2_c, @function
put_no_rnd_pixels16_l2_c:
.frame	$sp,64,$31		# vars= 0, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,44($sp)
sw	$20,56($sp)
move	$20,$4
sw	$19,52($sp)
move	$19,$5
sw	$18,48($sp)
move	$18,$6
sw	$16,40($sp)
move	$16,$7
sw	$7,16($sp)
sw	$7,20($sp)
lw	$17,80($sp)
sw	$31,60($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
sw	$17,24($sp)

addiu	$4,$20,8
addiu	$5,$19,8
sw	$16,16($sp)
addiu	$6,$18,8
sw	$16,20($sp)
sw	$17,24($sp)
.option	pic0
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	put_no_rnd_pixels16_l2_c
.size	put_no_rnd_pixels16_l2_c, .-put_no_rnd_pixels16_l2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc10_c
.type	put_h264_qpel4_mc10_c, @function
put_h264_qpel4_mc10_c:
.frame	$sp,56,$31		# vars= 16, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-56
sw	$18,48($sp)
move	$18,$4
sw	$16,40($sp)
addiu	$4,$sp,24
move	$16,$6
sw	$31,52($sp)
li	$6,4			# 0x4
sw	$17,44($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

li	$3,-16908288			# 0xfffffffffefe0000
lw	$31,52($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($17)  
lwr $4, 0($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 27($sp)  
lwr $2, 24($sp)  

# 0 "" 2
#NO_APP
ori	$3,$3,0xfefe
xor	$5,$2,$4
and	$5,$5,$3
srl	$5,$5,1
or	$2,$2,$4
subu	$4,$2,$5
addu	$6,$17,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 31($sp)  
lwr $2, 28($sp)  

# 0 "" 2
#NO_APP
sw	$4,0($18)
sll	$5,$16,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($6)  
lwr $4, 0($6)  

# 0 "" 2
#NO_APP
xor	$6,$2,$4
and	$6,$6,$3
srl	$6,$6,1
or	$2,$2,$4
subu	$4,$2,$6
addu	$6,$18,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 35($sp)  
lwr $2, 32($sp)  

# 0 "" 2
#NO_APP
sw	$4,0($6)
addu	$4,$17,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
move	$4,$6
xor	$6,$2,$6
and	$6,$6,$3
srl	$6,$6,1
or	$2,$2,$4
subu	$4,$2,$6
addu	$16,$5,$16
addu	$5,$18,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 39($sp)  
lwr $2, 36($sp)  

# 0 "" 2
#NO_APP
addu	$17,$17,$16
sw	$4,0($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($17)  
lwr $5, 0($17)  

# 0 "" 2
#NO_APP
xor	$4,$2,$5
lw	$17,44($sp)
and	$3,$4,$3
srl	$3,$3,1
or	$2,$2,$5
subu	$2,$2,$3
addu	$16,$18,$16
lw	$18,48($sp)
sw	$2,0($16)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

.end	put_h264_qpel4_mc10_c
.size	put_h264_qpel4_mc10_c, .-put_h264_qpel4_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc30_c
.type	put_h264_qpel4_mc30_c, @function
put_h264_qpel4_mc30_c:
.frame	$sp,56,$31		# vars= 16, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-56
sw	$18,48($sp)
move	$18,$4
sw	$17,44($sp)
addiu	$4,$sp,24
move	$17,$6
sw	$31,52($sp)
li	$6,4			# 0x4
sw	$16,40($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$3,-16908288			# 0xfffffffffefe0000
lw	$31,52($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 4($16)  
lwr $4, 1($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 27($sp)  
lwr $2, 24($sp)  

# 0 "" 2
#NO_APP
ori	$3,$3,0xfefe
xor	$5,$2,$4
and	$5,$5,$3
srl	$5,$5,1
or	$2,$2,$4
subu	$4,$2,$5
addiu	$5,$17,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 31($sp)  
lwr $2, 28($sp)  

# 0 "" 2
#NO_APP
sw	$4,0($18)
addu	$4,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
xor	$5,$2,$6
and	$5,$5,$3
srl	$5,$5,1
sll	$4,$17,1
or	$2,$2,$6
subu	$2,$2,$5
addu	$6,$18,$17
addiu	$5,$4,1
sw	$2,0($6)
addu	$17,$4,$17
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 35($sp)  
lwr $2, 32($sp)  

# 0 "" 2
#NO_APP
addu	$5,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
xor	$5,$2,$6
and	$5,$5,$3
srl	$5,$5,1
or	$2,$2,$6
subu	$2,$2,$5
addu	$4,$18,$4
addiu	$5,$17,1
sw	$2,0($4)
addu	$17,$18,$17
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 39($sp)  
lwr $2, 36($sp)  

# 0 "" 2
#NO_APP
addu	$16,$16,$5
lw	$18,48($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($16)  
lwr $5, 0($16)  

# 0 "" 2
#NO_APP
xor	$4,$2,$5
lw	$16,40($sp)
and	$3,$4,$3
srl	$3,$3,1
or	$2,$2,$5
subu	$2,$2,$3
sw	$2,0($17)
lw	$17,44($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

.end	put_h264_qpel4_mc30_c
.size	put_h264_qpel4_mc30_c, .-put_h264_qpel4_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc21_c
.type	put_h264_qpel4_mc21_c, @function
put_h264_qpel4_mc21_c:
.frame	$sp,152,$31		# vars= 104, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-152
sw	$17,140($sp)
move	$17,$4
sw	$16,136($sp)
addiu	$4,$sp,120
move	$16,$6
sw	$31,148($sp)
li	$6,4			# 0x4
sw	$18,144($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$18,$5
.set	macro
.set	reorder

addiu	$4,$sp,104
sw	$16,16($sp)
addiu	$5,$sp,32
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$18
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 123($sp)  
lwr $14, 120($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 107($sp)  
lwr $6, 104($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,148($sp)
xor	$10,$6,$14
lw	$18,144($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 135($sp)  
lwr $11, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 119($sp)  
lwr $2, 116($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 127($sp)  
lwr $13, 124($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 111($sp)  
lwr $5, 108($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 131($sp)  
lwr $12, 128($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 115($sp)  
lwr $3, 112($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
sll	$15,$16,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$8,$17,$15
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($17)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$17,$17,$16
addu	$16,$8,$16
sw	$5,0($17)
sw	$3,0($8)
sw	$2,0($16)
lw	$17,140($sp)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

.end	put_h264_qpel4_mc21_c
.size	put_h264_qpel4_mc21_c, .-put_h264_qpel4_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc23_c
.type	put_h264_qpel4_mc23_c, @function
put_h264_qpel4_mc23_c:
.frame	$sp,152,$31		# vars= 104, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-152
sw	$18,144($sp)
move	$18,$5
sw	$17,140($sp)
addu	$5,$5,$6
sw	$16,136($sp)
move	$17,$4
move	$16,$6
sw	$31,148($sp)
addiu	$4,$sp,120
li	$6,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

addiu	$4,$sp,104
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$18
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 123($sp)  
lwr $14, 120($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 107($sp)  
lwr $6, 104($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,148($sp)
xor	$10,$6,$14
lw	$18,144($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 135($sp)  
lwr $11, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 119($sp)  
lwr $2, 116($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 127($sp)  
lwr $13, 124($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 111($sp)  
lwr $5, 108($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 131($sp)  
lwr $12, 128($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 115($sp)  
lwr $3, 112($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
sll	$15,$16,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$8,$17,$15
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($17)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$17,$17,$16
addu	$16,$8,$16
sw	$5,0($17)
sw	$3,0($8)
sw	$2,0($16)
lw	$17,140($sp)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

.end	put_h264_qpel4_mc23_c
.size	put_h264_qpel4_mc23_c, .-put_h264_qpel4_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc01_c
.type	put_h264_qpel4_mc01_c, @function
put_h264_qpel4_mc01_c:
.frame	$sp,96,$31		# vars= 56, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-96
sw	$17,84($sp)
sll	$17,$6,1
sw	$16,80($sp)
move	$16,$6
subu	$3,$5,$17
sw	$18,88($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
sw	$31,92($sp)
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$2,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
move	$2,$3
addu	$8,$9,$16
move	$18,$4
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
addiu	$4,$sp,60
addiu	$5,$sp,32
addu	$2,$3,$16
li	$6,4			# 0x4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 59($sp)  
swr $7, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 35($sp)  
lwr $14, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 63($sp)  
lwr $6, 60($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,92($sp)
xor	$10,$6,$14
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 75($sp)  
lwr $2, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 39($sp)  
lwr $13, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 43($sp)  
lwr $12, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 71($sp)  
lwr $3, 68($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 47($sp)  
lwr $11, 44($sp)  

# 0 "" 2
#NO_APP
xor	$9,$5,$13
xor	$4,$2,$11
xor	$8,$3,$12
srl	$10,$10,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$17,$18,$17
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($18)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$18,$18,$16
addu	$16,$17,$16
sw	$5,0($18)
sw	$3,0($17)
sw	$2,0($16)
lw	$18,88($sp)
lw	$17,84($sp)
lw	$16,80($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

.end	put_h264_qpel4_mc01_c
.size	put_h264_qpel4_mc01_c, .-put_h264_qpel4_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc03_c
.type	put_h264_qpel4_mc03_c, @function
put_h264_qpel4_mc03_c:
.frame	$sp,96,$31		# vars= 56, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-96
sw	$17,84($sp)
sll	$17,$6,1
sw	$16,80($sp)
move	$16,$6
subu	$3,$5,$17
sw	$18,88($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
sw	$31,92($sp)
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$2,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
move	$2,$3
addu	$8,$9,$16
move	$18,$4
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
addiu	$4,$sp,60
addiu	$5,$sp,32
addu	$2,$3,$16
li	$6,4			# 0x4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 59($sp)  
swr $7, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 39($sp)  
lwr $14, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 63($sp)  
lwr $6, 60($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,92($sp)
xor	$10,$6,$14
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 75($sp)  
lwr $2, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 43($sp)  
lwr $13, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 47($sp)  
lwr $12, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 71($sp)  
lwr $3, 68($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 51($sp)  
lwr $11, 48($sp)  

# 0 "" 2
#NO_APP
xor	$9,$5,$13
xor	$4,$2,$11
xor	$8,$3,$12
srl	$10,$10,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$17,$18,$17
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($18)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$18,$18,$16
addu	$16,$17,$16
sw	$5,0($18)
sw	$3,0($17)
sw	$2,0($16)
lw	$18,88($sp)
lw	$17,84($sp)
lw	$16,80($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

.end	put_h264_qpel4_mc03_c
.size	put_h264_qpel4_mc03_c, .-put_h264_qpel4_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc10_c
.type	avg_h264_qpel16_mc10_c, @function
avg_h264_qpel16_mc10_c:
.frame	$sp,320,$31		# vars= 256, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
sw	$18,304($sp)
addiu	$18,$sp,40
sw	$17,300($sp)
li	$17,16			# 0x10
sw	$16,296($sp)
move	$16,$6
sw	$20,312($sp)
move	$20,$4
sw	$19,308($sp)
move	$4,$18
sw	$31,316($sp)
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$19,$5

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$17,20($sp)
move	$7,$16
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$17,24($sp)

addiu	$4,$20,8
addiu	$5,$19,8
sw	$16,16($sp)
addiu	$6,$sp,48
sw	$17,20($sp)
sw	$17,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,316($sp)
lw	$20,312($sp)
lw	$19,308($sp)
lw	$18,304($sp)
lw	$17,300($sp)
lw	$16,296($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc10_c
.size	avg_h264_qpel16_mc10_c, .-avg_h264_qpel16_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc30_c
.type	avg_h264_qpel16_mc30_c, @function
avg_h264_qpel16_mc30_c:
.frame	$sp,320,$31		# vars= 256, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
sw	$18,304($sp)
addiu	$18,$sp,40
sw	$17,300($sp)
li	$17,16			# 0x10
sw	$20,312($sp)
move	$20,$4
sw	$19,308($sp)
move	$4,$18
sw	$16,296($sp)
move	$19,$5
sw	$31,316($sp)
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$16,$6

addiu	$5,$19,1
move	$4,$20
sw	$16,16($sp)
move	$6,$18
sw	$17,20($sp)
move	$7,$16
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$17,24($sp)

addiu	$4,$20,8
addiu	$5,$19,9
sw	$16,16($sp)
addiu	$6,$sp,48
sw	$17,20($sp)
sw	$17,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,316($sp)
lw	$20,312($sp)
lw	$19,308($sp)
lw	$18,304($sp)
lw	$17,300($sp)
lw	$16,296($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc30_c
.size	avg_h264_qpel16_mc30_c, .-avg_h264_qpel16_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc10_c
.type	avg_qpel16_mc10_c, @function
avg_qpel16_mc10_c:
.frame	$sp,320,$31		# vars= 256, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
sw	$17,300($sp)
li	$17,16			# 0x10
sw	$18,304($sp)
addiu	$18,$sp,40
sw	$16,296($sp)
move	$16,$6
li	$6,16			# 0x10
sw	$20,312($sp)
sw	$19,308($sp)
move	$20,$4
move	$19,$5
sw	$17,16($sp)
move	$4,$18
sw	$31,316($sp)
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$7,$16

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$17,20($sp)
move	$7,$16
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$17,24($sp)

addiu	$4,$20,8
addiu	$5,$19,8
sw	$16,16($sp)
addiu	$6,$sp,48
sw	$17,20($sp)
sw	$17,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,316($sp)
lw	$20,312($sp)
lw	$19,308($sp)
lw	$18,304($sp)
lw	$17,300($sp)
lw	$16,296($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	avg_qpel16_mc10_c
.size	avg_qpel16_mc10_c, .-avg_qpel16_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc30_c
.type	avg_qpel16_mc30_c, @function
avg_qpel16_mc30_c:
.frame	$sp,320,$31		# vars= 256, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-320
sw	$17,300($sp)
li	$17,16			# 0x10
sw	$18,304($sp)
addiu	$18,$sp,40
sw	$16,296($sp)
move	$16,$6
li	$6,16			# 0x10
sw	$20,312($sp)
sw	$19,308($sp)
move	$20,$4
move	$19,$5
sw	$17,16($sp)
move	$4,$18
sw	$31,316($sp)
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$7,$16

addiu	$5,$19,1
move	$4,$20
sw	$16,16($sp)
move	$6,$18
sw	$17,20($sp)
move	$7,$16
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$17,24($sp)

addiu	$4,$20,8
addiu	$5,$19,9
sw	$16,16($sp)
addiu	$6,$sp,48
sw	$17,20($sp)
sw	$17,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$16

lw	$31,316($sp)
lw	$20,312($sp)
lw	$19,308($sp)
lw	$18,304($sp)
lw	$17,300($sp)
lw	$16,296($sp)
j	$31
addiu	$sp,$sp,320

.set	macro
.set	reorder
.end	avg_qpel16_mc30_c
.size	avg_qpel16_mc30_c, .-avg_qpel16_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc31_c
.type	put_h264_qpel4_mc31_c, @function
put_h264_qpel4_mc31_c:
.frame	$sp,112,$31		# vars= 72, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
sw	$18,104($sp)
move	$18,$4
sw	$16,96($sp)
addiu	$4,$sp,76
move	$16,$6
sw	$31,108($sp)
li	$6,4			# 0x4
sw	$17,100($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

subu	$2,$0,$16
addiu	$4,$sp,60
sll	$2,$2,1
addiu	$5,$sp,32
addiu	$2,$2,1
li	$6,4			# 0x4
addu	$2,$17,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 27($sp)  
swr $3, 24($sp)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 31($sp)  
swr $7, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$7
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 35($sp)  
swr $3, 32($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 39($sp)  
swr $9, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 79($sp)  
lwr $14, 76($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 63($sp)  
lwr $6, 60($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,108($sp)
xor	$10,$6,$14
lw	$17,100($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 91($sp)  
lwr $11, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 75($sp)  
lwr $2, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 83($sp)  
lwr $13, 80($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 87($sp)  
lwr $12, 84($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 71($sp)  
lwr $3, 68($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
sll	$15,$16,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$8,$18,$15
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($18)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$18,$18,$16
addu	$16,$8,$16
sw	$5,0($18)
sw	$3,0($8)
sw	$2,0($16)
lw	$18,104($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	put_h264_qpel4_mc31_c
.size	put_h264_qpel4_mc31_c, .-put_h264_qpel4_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc11_c
.type	put_h264_qpel4_mc11_c, @function
put_h264_qpel4_mc11_c:
.frame	$sp,120,$31		# vars= 72, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
sw	$16,100($sp)
move	$16,$6
sw	$18,108($sp)
li	$6,4			# 0x4
sw	$17,104($sp)
move	$18,$4
sll	$17,$16,1
sw	$19,112($sp)
addiu	$4,$sp,76
sw	$31,116($sp)
move	$19,$5
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

subu	$2,$19,$17
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$9,$16
addiu	$4,$sp,60
addu	$7,$8,$16
addiu	$5,$sp,32
addu	$3,$7,$16
li	$6,4			# 0x4
addu	$2,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 79($sp)  
lwr $14, 76($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 63($sp)  
lwr $6, 60($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,116($sp)
xor	$10,$6,$14
lw	$19,112($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 91($sp)  
lwr $11, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 75($sp)  
lwr $2, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 83($sp)  
lwr $13, 80($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 87($sp)  
lwr $12, 84($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 71($sp)  
lwr $3, 68($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$17,$18,$17
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($18)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$18,$18,$16
addu	$16,$17,$16
sw	$5,0($18)
sw	$3,0($17)
sw	$2,0($16)
lw	$18,108($sp)
lw	$17,104($sp)
lw	$16,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	put_h264_qpel4_mc11_c
.size	put_h264_qpel4_mc11_c, .-put_h264_qpel4_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc23_c
.type	avg_qpel16_mc23_c, @function
avg_qpel16_mc23_c:
.frame	$sp,592,$31		# vars= 528, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-592
li	$2,17			# 0x11
sw	$18,576($sp)
addiu	$18,$sp,40
sw	$19,580($sp)
move	$19,$6
li	$6,16			# 0x10
sw	$2,16($sp)
sw	$20,584($sp)
move	$7,$19
sw	$17,572($sp)
move	$20,$4
addiu	$17,$sp,312
sw	$31,588($sp)
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
sw	$16,568($sp)

li	$16,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18

addiu	$5,$sp,56
move	$4,$20
sw	$16,16($sp)
move	$6,$17
sw	$16,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,64
sw	$16,16($sp)
addiu	$6,$sp,320
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,588($sp)
lw	$20,584($sp)
lw	$19,580($sp)
lw	$18,576($sp)
lw	$17,572($sp)
lw	$16,568($sp)
j	$31
addiu	$sp,$sp,592

.set	macro
.set	reorder
.end	avg_qpel16_mc23_c
.size	avg_qpel16_mc23_c, .-avg_qpel16_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc03_c
.type	avg_qpel16_mc03_c, @function
avg_qpel16_mc03_c:
.frame	$sp,736,$31		# vars= 664, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-736
sw	$18,716($sp)
addiu	$18,$sp,40
sw	$21,728($sp)
move	$21,$4
sw	$17,712($sp)
move	$4,$18
addiu	$17,$sp,448
sw	$31,732($sp)
sw	$20,724($sp)
li	$20,24			# 0x18
sw	$19,720($sp)
move	$19,$6
sw	$16,708($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
li	$16,16			# 0x10

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18

addiu	$5,$sp,64
move	$4,$21
sw	$20,16($sp)
move	$6,$17
sw	$16,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$21,8
addiu	$5,$sp,72
sw	$20,16($sp)
addiu	$6,$sp,456
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,732($sp)
lw	$21,728($sp)
lw	$20,724($sp)
lw	$19,720($sp)
lw	$18,716($sp)
lw	$17,712($sp)
lw	$16,708($sp)
j	$31
addiu	$sp,$sp,736

.set	macro
.set	reorder
.end	avg_qpel16_mc03_c
.size	avg_qpel16_mc03_c, .-avg_qpel16_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc01_c
.type	avg_qpel16_mc01_c, @function
avg_qpel16_mc01_c:
.frame	$sp,736,$31		# vars= 664, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-736
sw	$16,708($sp)
addiu	$16,$sp,40
sw	$21,728($sp)
move	$21,$4
sw	$20,724($sp)
move	$4,$16
sw	$18,716($sp)
li	$20,24			# 0x18
sw	$17,712($sp)
addiu	$18,$sp,448
li	$17,16			# 0x10
sw	$31,732($sp)
sw	$19,720($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
move	$19,$6

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16

move	$4,$21
move	$5,$16
sw	$20,16($sp)
move	$6,$18
sw	$17,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$17,24($sp)

addiu	$4,$21,8
addiu	$5,$sp,48
sw	$20,16($sp)
addiu	$6,$sp,456
sw	$17,20($sp)
sw	$17,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,732($sp)
lw	$21,728($sp)
lw	$20,724($sp)
lw	$19,720($sp)
lw	$18,716($sp)
lw	$17,712($sp)
lw	$16,708($sp)
j	$31
addiu	$sp,$sp,736

.set	macro
.set	reorder
.end	avg_qpel16_mc01_c
.size	avg_qpel16_mc01_c, .-avg_qpel16_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc21_c
.type	avg_qpel16_mc21_c, @function
avg_qpel16_mc21_c:
.frame	$sp,592,$31		# vars= 528, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-592
li	$2,17			# 0x11
sw	$17,572($sp)
addiu	$17,$sp,40
sw	$19,580($sp)
move	$19,$6
li	$6,16			# 0x10
sw	$2,16($sp)
sw	$20,584($sp)
move	$7,$19
sw	$18,576($sp)
move	$20,$4
sw	$16,568($sp)
addiu	$18,$sp,312
li	$16,16			# 0x10
sw	$31,588($sp)
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$17

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$17

move	$4,$20
move	$5,$17
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,48
sw	$16,16($sp)
addiu	$6,$sp,320
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,588($sp)
lw	$20,584($sp)
lw	$19,580($sp)
lw	$18,576($sp)
lw	$17,572($sp)
lw	$16,568($sp)
j	$31
addiu	$sp,$sp,592

.set	macro
.set	reorder
.end	avg_qpel16_mc21_c
.size	avg_qpel16_mc21_c, .-avg_qpel16_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc33_c
.type	put_h264_qpel4_mc33_c, @function
put_h264_qpel4_mc33_c:
.frame	$sp,112,$31		# vars= 72, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
sw	$18,104($sp)
move	$18,$4
sw	$17,100($sp)
addiu	$4,$sp,76
sw	$16,96($sp)
move	$17,$5
move	$16,$6
sw	$31,108($sp)
addu	$5,$5,$6
li	$6,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

subu	$2,$0,$16
addiu	$4,$sp,60
sll	$2,$2,1
addiu	$5,$sp,32
addiu	$2,$2,1
li	$6,4			# 0x4
addu	$2,$17,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 27($sp)  
swr $3, 24($sp)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 31($sp)  
swr $7, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$7
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 35($sp)  
swr $3, 32($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 39($sp)  
swr $9, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 79($sp)  
lwr $14, 76($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 63($sp)  
lwr $6, 60($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,108($sp)
xor	$10,$6,$14
lw	$17,100($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 91($sp)  
lwr $11, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 75($sp)  
lwr $2, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 83($sp)  
lwr $13, 80($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 87($sp)  
lwr $12, 84($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 71($sp)  
lwr $3, 68($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
sll	$15,$16,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$8,$18,$15
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($18)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$18,$18,$16
addu	$16,$8,$16
sw	$5,0($18)
sw	$3,0($8)
sw	$2,0($16)
lw	$18,104($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	put_h264_qpel4_mc33_c
.size	put_h264_qpel4_mc33_c, .-put_h264_qpel4_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc13_c
.type	put_h264_qpel4_mc13_c, @function
put_h264_qpel4_mc13_c:
.frame	$sp,120,$31		# vars= 72, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
sw	$16,100($sp)
move	$16,$6
sw	$19,112($sp)
move	$19,$4
sw	$18,108($sp)
addiu	$4,$sp,76
sw	$17,104($sp)
move	$18,$5
sll	$17,$16,1
sw	$31,116($sp)
addu	$5,$5,$6
li	$6,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

subu	$2,$18,$17
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$9,$16
addiu	$4,$sp,60
addu	$7,$8,$16
addiu	$5,$sp,32
addu	$3,$7,$16
li	$6,4			# 0x4
addu	$2,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 79($sp)  
lwr $14, 76($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 63($sp)  
lwr $6, 60($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,116($sp)
xor	$10,$6,$14
lw	$18,108($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 91($sp)  
lwr $11, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 75($sp)  
lwr $2, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 83($sp)  
lwr $13, 80($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 87($sp)  
lwr $12, 84($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 71($sp)  
lwr $3, 68($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$17,$19,$17
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($19)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$19,$19,$16
addu	$16,$17,$16
sw	$5,0($19)
sw	$3,0($17)
sw	$2,0($16)
lw	$19,112($sp)
lw	$17,104($sp)
lw	$16,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	put_h264_qpel4_mc13_c
.size	put_h264_qpel4_mc13_c, .-put_h264_qpel4_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc12_c
.type	put_h264_qpel4_mc12_c, @function
put_h264_qpel4_mc12_c:
.frame	$sp,200,$31		# vars= 144, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-200
sw	$17,184($sp)
sll	$17,$6,1
sw	$16,180($sp)
move	$16,$6
subu	$3,$5,$17
sw	$19,192($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
sw	$18,188($sp)
move	$3,$7
sw	$31,196($sp)
addu	$10,$2,$6
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 107($sp)  
swr $3, 104($sp)  

# 0 "" 2
#NO_APP
addu	$9,$10,$6
move	$18,$4
addu	$8,$9,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
#NO_APP
addu	$7,$8,$16
move	$19,$5
addu	$3,$7,$16
addiu	$4,$sp,156
addiu	$5,$sp,112
addu	$2,$3,$16
li	$6,4			# 0x4
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 111($sp)  
swr $11, 108($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 115($sp)  
swr $11, 112($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 119($sp)  
swr $10, 116($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 123($sp)  
swr $9, 120($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 127($sp)  
swr $8, 124($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 131($sp)  
swr $7, 128($sp)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 135($sp)  
swr $3, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 139($sp)  
swr $3, 136($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
addiu	$4,$sp,140
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$19
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 159($sp)  
lwr $14, 156($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 143($sp)  
lwr $6, 140($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,196($sp)
xor	$10,$6,$14
lw	$19,192($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 171($sp)  
lwr $11, 168($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 155($sp)  
lwr $2, 152($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 163($sp)  
lwr $13, 160($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 147($sp)  
lwr $5, 144($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 167($sp)  
lwr $12, 164($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 151($sp)  
lwr $3, 148($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$17,$18,$17
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($18)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$18,$18,$16
addu	$16,$17,$16
sw	$5,0($18)
sw	$3,0($17)
sw	$2,0($16)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,200
.set	macro
.set	reorder

.end	put_h264_qpel4_mc12_c
.size	put_h264_qpel4_mc12_c, .-put_h264_qpel4_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel4_mc32_c
.type	put_h264_qpel4_mc32_c, @function
put_h264_qpel4_mc32_c:
.frame	$sp,192,$31		# vars= 144, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
subu	$2,$0,$6
addiu	$sp,$sp,-192
sll	$2,$2,1
sw	$16,176($sp)
move	$16,$6
addiu	$2,$2,1
sw	$18,184($sp)
sw	$17,180($sp)
move	$18,$5
addu	$2,$5,$2
sw	$31,188($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$3,$2,$6
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 107($sp)  
swr $7, 104($sp)  

# 0 "" 2
#NO_APP
addu	$10,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
move	$17,$4
addu	$8,$9,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 111($sp)  
swr $2, 108($sp)  

# 0 "" 2
#NO_APP
addu	$7,$8,$16
addiu	$4,$sp,156
addu	$3,$7,$16
addiu	$5,$sp,112
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($10)  
lwr $2, 0($10)  

# 0 "" 2
#NO_APP
li	$6,4			# 0x4
move	$10,$2
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 115($sp)  
swr $10, 112($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 119($sp)  
swr $10, 116($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 123($sp)  
swr $9, 120($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 127($sp)  
swr $8, 124($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 131($sp)  
swr $7, 128($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 135($sp)  
swr $3, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 139($sp)  
swr $3, 136($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
addiu	$4,$sp,140
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$18
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 159($sp)  
lwr $14, 156($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 143($sp)  
lwr $6, 140($sp)  

# 0 "" 2
#NO_APP
ori	$7,$7,0xfefe
lw	$31,188($sp)
xor	$10,$6,$14
lw	$18,184($sp)
and	$10,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 171($sp)  
lwr $11, 168($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 155($sp)  
lwr $2, 152($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 163($sp)  
lwr $13, 160($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 147($sp)  
lwr $5, 144($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 167($sp)  
lwr $12, 164($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 151($sp)  
lwr $3, 148($sp)  

# 0 "" 2
#NO_APP
xor	$4,$2,$11
xor	$9,$5,$13
xor	$8,$3,$12
srl	$10,$10,1
sll	$15,$16,1
and	$8,$8,$7
and	$9,$9,$7
or	$6,$6,$14
and	$7,$4,$7
srl	$9,$9,1
srl	$4,$8,1
srl	$7,$7,1
subu	$6,$6,$10
addu	$8,$17,$15
or	$5,$5,$13
or	$3,$3,$12
sw	$6,0($17)
or	$2,$2,$11
subu	$5,$5,$9
subu	$3,$3,$4
subu	$2,$2,$7
addu	$17,$17,$16
addu	$16,$8,$16
sw	$5,0($17)
sw	$3,0($8)
sw	$2,0($16)
lw	$17,180($sp)
lw	$16,176($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,192
.set	macro
.set	reorder

.end	put_h264_qpel4_mc32_c
.size	put_h264_qpel4_mc32_c, .-put_h264_qpel4_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc21_c
.type	avg_h264_qpel16_mc21_c, @function
avg_h264_qpel16_mc21_c:
.frame	$sp,1256,$31		# vars= 1184, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1256
sw	$18,1236($sp)
addiu	$18,$sp,968
sw	$20,1244($sp)
move	$20,$4
sw	$17,1232($sp)
move	$4,$18
sw	$16,1228($sp)
addiu	$17,$sp,712
li	$16,16			# 0x10
sw	$31,1252($sp)
sw	$21,1248($sp)
move	$21,$5
sw	$19,1240($sp)
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$19,$6

addiu	$5,$sp,40
move	$4,$17
move	$6,$21
.option	pic0
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$19

move	$4,$20
move	$5,$18
sw	$16,16($sp)
move	$6,$17
sw	$16,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,976
sw	$16,16($sp)
addiu	$6,$sp,720
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,1252($sp)
lw	$21,1248($sp)
lw	$20,1244($sp)
lw	$19,1240($sp)
lw	$18,1236($sp)
lw	$17,1232($sp)
lw	$16,1228($sp)
j	$31
addiu	$sp,$sp,1256

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc21_c
.size	avg_h264_qpel16_mc21_c, .-avg_h264_qpel16_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc23_c
.type	avg_h264_qpel16_mc23_c, @function
avg_h264_qpel16_mc23_c:
.frame	$sp,1256,$31		# vars= 1184, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1256
sw	$19,1240($sp)
addiu	$19,$sp,968
sw	$21,1248($sp)
move	$21,$5
addu	$5,$5,$6
sw	$20,1244($sp)
sw	$18,1236($sp)
move	$20,$4
sw	$16,1228($sp)
addiu	$18,$sp,712
li	$16,16			# 0x10
sw	$31,1252($sp)
move	$4,$19
sw	$17,1232($sp)
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$17,$6

addiu	$5,$sp,40
move	$4,$18
move	$6,$21
.option	pic0
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,976
sw	$16,16($sp)
addiu	$6,$sp,720
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,1252($sp)
lw	$21,1248($sp)
lw	$20,1244($sp)
lw	$19,1240($sp)
lw	$18,1236($sp)
lw	$17,1232($sp)
lw	$16,1228($sp)
j	$31
addiu	$sp,$sp,1256

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc23_c
.size	avg_h264_qpel16_mc23_c, .-avg_h264_qpel16_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc02_c
.type	put_h264_qpel8_mc02_c, @function
put_h264_qpel8_mc02_c:
.frame	$sp,136,$31		# vars= 104, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-136
sll	$3,$6,1
addiu	$2,$sp,24
sw	$31,132($sp)
subu	$5,$5,$3
addiu	$8,$sp,128
$L1690:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 3($2)  
swr $7, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($2)  
swr $3, 4($2)  

# 0 "" 2
#NO_APP
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$8,$L1690
addu	$5,$5,$6
.set	macro
.set	reorder

addiu	$5,$sp,40
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
li	$7,8			# 0x8
.set	macro
.set	reorder

lw	$31,132($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

.end	put_h264_qpel8_mc02_c
.size	put_h264_qpel8_mc02_c, .-put_h264_qpel8_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc02_c
.type	avg_h264_qpel8_mc02_c, @function
avg_h264_qpel8_mc02_c:
.frame	$sp,168,$31		# vars= 120, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-168
sll	$3,$6,1
addiu	$2,$sp,8
sw	$fp,164($sp)
subu	$5,$5,$3
sw	$23,160($sp)
addiu	$8,$sp,112
sw	$22,156($sp)
sw	$21,152($sp)
sw	$20,148($sp)
sw	$19,144($sp)
sw	$18,140($sp)
sw	$17,136($sp)
sw	$16,132($sp)
$L1694:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 3($2)  
swr $7, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($2)  
swr $3, 4($2)  

# 0 "" 2
#NO_APP
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$8,$L1694
addu	$5,$5,$6
.set	macro
.set	reorder

addu	$fp,$4,$6
addiu	$2,$4,8
addu	$23,$fp,$6
lui	$20,%hi(ff_cropTbl+1024)
addu	$22,$23,$6
sw	$2,124($sp)
addiu	$18,$sp,24
addu	$21,$22,$6
addiu	$20,$20,%lo(ff_cropTbl+1024)
addu	$17,$21,$6
addu	$16,$17,$6
addu	$15,$16,$6
$L1695:
lbu	$11,8($18)
addiu	$4,$4,1
lbu	$12,0($18)
addiu	$fp,$fp,1
lbu	$10,16($18)
addiu	$23,$23,1
lbu	$14,-8($18)
addiu	$22,$22,1
addu	$3,$12,$11
lbu	$9,24($18)
lbu	$19,-16($18)
addu	$2,$11,$10
addu	$7,$14,$10
lbu	$8,32($18)
sll	$13,$3,2
lbu	$5,-1($4)
sll	$3,$3,4
lbu	$6,48($18)
sll	$24,$7,2
addu	$25,$13,$3
lbu	$3,72($18)
addu	$24,$24,$7
sw	$5,120($sp)
addu	$19,$19,$9
lbu	$5,40($18)
lbu	$13,56($18)
addiu	$15,$15,1
sw	$3,112($sp)
subu	$3,$25,$24
lbu	$24,80($18)
sll	$25,$2,2
addu	$3,$3,$19
lbu	$7,64($18)
sll	$2,$2,4
addiu	$3,$3,16
sw	$24,116($sp)
addu	$24,$12,$9
sra	$3,$3,5
sll	$19,$24,2
addu	$2,$25,$2
addu	$19,$19,$24
addu	$3,$20,$3
addu	$24,$14,$8
subu	$2,$2,$19
lbu	$3,0($3)
addu	$14,$10,$9
addu	$24,$2,$24
lw	$2,120($sp)
addu	$19,$11,$8
addu	$12,$12,$5
addu	$25,$2,$3
addiu	$3,$24,16
addiu	$25,$25,1
sra	$3,$3,5
sll	$24,$14,2
sra	$25,$25,1
sll	$2,$14,4
sll	$14,$19,2
addu	$3,$20,$3
sb	$25,-1($4)
addu	$14,$14,$19
lbu	$25,-1($fp)
addu	$2,$24,$2
lbu	$19,0($3)
addu	$3,$9,$8
subu	$2,$2,$14
sll	$24,$3,2
addu	$12,$2,$12
addu	$25,$25,$19
addiu	$12,$12,16
addu	$19,$10,$5
addiu	$25,$25,1
sra	$12,$12,5
sra	$25,$25,1
sll	$2,$3,4
sll	$14,$19,2
addu	$12,$20,$12
sb	$25,-1($fp)
addu	$14,$14,$19
lbu	$3,-1($23)
addu	$2,$24,$2
lbu	$25,0($12)
addu	$11,$11,$6
subu	$2,$2,$14
addu	$12,$8,$5
addu	$11,$2,$11
addu	$3,$3,$25
addiu	$11,$11,16
addu	$25,$9,$6
addiu	$3,$3,1
sra	$11,$11,5
sll	$14,$12,2
sra	$3,$3,1
sll	$2,$12,4
sll	$12,$25,2
addu	$11,$20,$11
sb	$3,-1($23)
addu	$2,$14,$2
lbu	$3,-1($22)
addu	$25,$12,$25
lbu	$11,0($11)
addu	$10,$10,$13
subu	$12,$2,$25
addu	$19,$8,$13
addu	$12,$12,$10
addu	$10,$3,$11
addiu	$3,$12,16
addu	$2,$5,$6
addiu	$10,$10,1
sra	$3,$3,5
sll	$12,$2,2
sra	$10,$10,1
sll	$2,$2,4
sll	$11,$19,2
addu	$3,$20,$3
sb	$10,-1($22)
addu	$2,$12,$2
lbu	$10,0($21)
addu	$11,$11,$19
lbu	$3,0($3)
addu	$9,$9,$7
subu	$12,$2,$11
addu	$14,$5,$7
addu	$9,$12,$9
addu	$10,$10,$3
addiu	$9,$9,16
addiu	$10,$10,1
sra	$9,$9,5
addu	$2,$6,$13
sra	$10,$10,1
addu	$9,$20,$9
sll	$3,$2,2
sll	$11,$14,2
sb	$10,0($21)
sll	$2,$2,4
lbu	$10,0($17)
lbu	$9,0($9)
addu	$7,$13,$7
addu	$2,$3,$2
lw	$24,116($sp)
addu	$3,$11,$14
lw	$11,112($sp)
addu	$9,$10,$9
subu	$3,$2,$3
addu	$8,$8,$11
addu	$6,$6,$11
addu	$8,$3,$8
addiu	$9,$9,1
addiu	$8,$8,16
sll	$3,$7,2
sra	$8,$8,5
sll	$2,$6,2
sll	$7,$7,4
sra	$9,$9,1
addu	$8,$20,$8
addu	$7,$3,$7
addu	$2,$2,$6
sb	$9,0($17)
lbu	$3,0($16)
addu	$5,$5,$24
subu	$6,$7,$2
lbu	$7,0($8)
addiu	$18,$18,1
addu	$2,$6,$5
addu	$3,$3,$7
addiu	$2,$2,16
addiu	$3,$3,1
sra	$2,$2,5
sra	$3,$3,1
addu	$2,$20,$2
addiu	$21,$21,1
sb	$3,0($16)
addiu	$17,$17,1
lbu	$3,-1($15)
lbu	$2,0($2)
addu	$2,$3,$2
addiu	$2,$2,1
sra	$2,$2,1
sb	$2,-1($15)
lw	$2,124($sp)
.set	noreorder
.set	nomacro
bne	$4,$2,$L1695
addiu	$16,$16,1
.set	macro
.set	reorder

lw	$fp,164($sp)
lw	$23,160($sp)
lw	$22,156($sp)
lw	$21,152($sp)
lw	$20,148($sp)
lw	$19,144($sp)
lw	$18,140($sp)
lw	$17,136($sp)
lw	$16,132($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,168
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc02_c
.size	avg_h264_qpel8_mc02_c, .-avg_h264_qpel8_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc01_c
.type	avg_h264_qpel16_mc01_c, @function
avg_h264_qpel16_mc01_c:
.frame	$sp,656,$31		# vars= 592, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-656
sll	$2,$6,1
sw	$20,648($sp)
move	$20,$4
subu	$5,$5,$2
sw	$18,640($sp)
sw	$17,636($sp)
addiu	$18,$sp,72
sw	$16,632($sp)
addiu	$17,$sp,376
li	$16,16			# 0x10
sw	$31,652($sp)
addiu	$4,$sp,40
sw	$19,644($sp)
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
move	$19,$6

move	$4,$17
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$5,$18

move	$4,$20
move	$5,$18
sw	$16,16($sp)
move	$6,$17
sw	$16,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,80
sw	$16,16($sp)
addiu	$6,$sp,384
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,652($sp)
lw	$20,648($sp)
lw	$19,644($sp)
lw	$18,640($sp)
lw	$17,636($sp)
lw	$16,632($sp)
j	$31
addiu	$sp,$sp,656

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc01_c
.size	avg_h264_qpel16_mc01_c, .-avg_h264_qpel16_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc03_c
.type	put_h264_qpel8_mc03_c, @function
put_h264_qpel8_mc03_c:
.frame	$sp,224,$31		# vars= 168, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-224
sll	$2,$6,1
sw	$17,212($sp)
move	$17,$6
sw	$16,208($sp)
subu	$5,$5,$2
addiu	$16,$sp,40
sw	$18,216($sp)
addiu	$6,$sp,144
sw	$31,220($sp)
move	$18,$4
$L1702:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 3($16)  
swr $3, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($16)  
swr $2, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1702
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
addiu	$5,$sp,64
move	$4,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,224
.set	macro
.set	reorder

.end	put_h264_qpel8_mc03_c
.size	put_h264_qpel8_mc03_c, .-put_h264_qpel8_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc03_c
.type	avg_h264_qpel8_mc03_c, @function
avg_h264_qpel8_mc03_c:
.frame	$sp,224,$31		# vars= 168, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-224
sll	$2,$6,1
sw	$17,212($sp)
move	$17,$6
sw	$16,208($sp)
subu	$5,$5,$2
addiu	$16,$sp,40
sw	$18,216($sp)
addiu	$6,$sp,144
sw	$31,220($sp)
move	$18,$4
$L1706:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 3($16)  
swr $3, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($16)  
swr $2, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1706
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
addiu	$5,$sp,64
move	$4,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,224
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc03_c
.size	avg_h264_qpel8_mc03_c, .-avg_h264_qpel8_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc01_c
.type	avg_h264_qpel8_mc01_c, @function
avg_h264_qpel8_mc01_c:
.frame	$sp,232,$31		# vars= 168, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-232
sll	$2,$6,1
sw	$17,216($sp)
move	$17,$6
sw	$16,212($sp)
subu	$5,$5,$2
addiu	$16,$sp,40
sw	$19,224($sp)
addiu	$6,$sp,144
sw	$31,228($sp)
sw	$18,220($sp)
move	$19,$4
$L1710:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 3($16)  
swr $3, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($16)  
swr $2, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1710
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$18,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$19
move	$5,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,228($sp)
lw	$19,224($sp)
lw	$18,220($sp)
lw	$17,216($sp)
lw	$16,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,232
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc01_c
.size	avg_h264_qpel8_mc01_c, .-avg_h264_qpel8_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc01_c
.type	put_h264_qpel8_mc01_c, @function
put_h264_qpel8_mc01_c:
.frame	$sp,232,$31		# vars= 168, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-232
sll	$2,$6,1
sw	$17,216($sp)
move	$17,$6
sw	$16,212($sp)
subu	$5,$5,$2
addiu	$16,$sp,40
sw	$19,224($sp)
addiu	$6,$sp,144
sw	$31,228($sp)
sw	$18,220($sp)
move	$19,$4
$L1714:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($5)  
lwr $2, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 3($16)  
swr $3, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 7($16)  
swr $2, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1714
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$18,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$19
move	$5,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,228($sp)
lw	$19,224($sp)
lw	$18,220($sp)
lw	$17,216($sp)
lw	$16,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,232
.set	macro
.set	reorder

.end	put_h264_qpel8_mc01_c
.size	put_h264_qpel8_mc01_c, .-put_h264_qpel8_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc11_c
.type	put_h264_qpel8_mc11_c, @function
put_h264_qpel8_mc11_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$20,288($sp)
move	$7,$18
sw	$17,276($sp)
move	$20,$4
sw	$16,272($sp)
move	$4,$19
sw	$31,292($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

sll	$2,$18,1
addiu	$16,$sp,40
subu	$2,$17,$2
addiu	$6,$sp,144
$L1718:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1718
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	put_h264_qpel8_mc11_c
.size	put_h264_qpel8_mc11_c, .-put_h264_qpel8_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc31_c
.type	put_h264_qpel8_mc31_c, @function
put_h264_qpel8_mc31_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$20,288($sp)
move	$7,$18
sw	$17,276($sp)
move	$20,$4
sw	$16,272($sp)
move	$4,$19
sw	$31,292($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

subu	$2,$0,$18
addiu	$16,$sp,40
sll	$2,$2,1
addiu	$6,$sp,144
addiu	$2,$2,1
addu	$2,$17,$2
$L1722:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1722
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	put_h264_qpel8_mc31_c
.size	put_h264_qpel8_mc31_c, .-put_h264_qpel8_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc11_c
.type	avg_h264_qpel8_mc11_c, @function
avg_h264_qpel8_mc11_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$20,288($sp)
move	$7,$18
sw	$17,276($sp)
move	$20,$4
sw	$16,272($sp)
move	$4,$19
sw	$31,292($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

sll	$2,$18,1
addiu	$16,$sp,40
subu	$2,$17,$2
addiu	$6,$sp,144
$L1726:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1726
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc11_c
.size	avg_h264_qpel8_mc11_c, .-avg_h264_qpel8_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc31_c
.type	avg_h264_qpel8_mc31_c, @function
avg_h264_qpel8_mc31_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$20,288($sp)
move	$7,$18
sw	$17,276($sp)
move	$20,$4
sw	$16,272($sp)
move	$4,$19
sw	$31,292($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

subu	$2,$0,$18
addiu	$16,$sp,40
sll	$2,$2,1
addiu	$6,$sp,144
addiu	$2,$2,1
addu	$2,$17,$2
$L1730:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1730
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc31_c
.size	avg_h264_qpel8_mc31_c, .-avg_h264_qpel8_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc13_c
.type	avg_h264_qpel8_mc13_c, @function
avg_h264_qpel8_mc13_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
sw	$17,276($sp)
move	$17,$5
addu	$5,$5,$6
sw	$20,288($sp)
li	$6,8			# 0x8
sw	$16,272($sp)
move	$20,$4
sw	$31,292($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

sll	$2,$18,1
addiu	$16,$sp,40
subu	$2,$17,$2
addiu	$6,$sp,144
$L1734:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1734
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc13_c
.size	avg_h264_qpel8_mc13_c, .-avg_h264_qpel8_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc33_c
.type	avg_h264_qpel8_mc33_c, @function
avg_h264_qpel8_mc33_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
sw	$17,276($sp)
move	$17,$5
addu	$5,$5,$6
sw	$20,288($sp)
li	$6,8			# 0x8
sw	$16,272($sp)
move	$20,$4
sw	$31,292($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

subu	$2,$0,$18
addiu	$16,$sp,40
sll	$2,$2,1
addiu	$6,$sp,144
addiu	$2,$2,1
addu	$2,$17,$2
$L1738:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1738
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc33_c
.size	avg_h264_qpel8_mc33_c, .-avg_h264_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc13_c
.type	put_h264_qpel8_mc13_c, @function
put_h264_qpel8_mc13_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
sw	$17,276($sp)
move	$17,$5
addu	$5,$5,$6
sw	$20,288($sp)
li	$6,8			# 0x8
sw	$16,272($sp)
move	$20,$4
sw	$31,292($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

sll	$2,$18,1
addiu	$16,$sp,40
subu	$2,$17,$2
addiu	$6,$sp,144
$L1742:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1742
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	put_h264_qpel8_mc13_c
.size	put_h264_qpel8_mc13_c, .-put_h264_qpel8_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc33_c
.type	put_h264_qpel8_mc33_c, @function
put_h264_qpel8_mc33_c:
.frame	$sp,296,$31		# vars= 232, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-296
sw	$19,284($sp)
addiu	$19,$sp,208
sw	$18,280($sp)
move	$18,$6
sw	$17,276($sp)
move	$17,$5
addu	$5,$5,$6
sw	$20,288($sp)
li	$6,8			# 0x8
sw	$16,272($sp)
move	$20,$4
sw	$31,292($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

subu	$2,$0,$18
addiu	$16,$sp,40
sll	$2,$2,1
addiu	$6,$sp,144
addiu	$2,$2,1
addu	$2,$17,$2
$L1746:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$6,$L1746
addu	$2,$2,$18
.set	macro
.set	reorder

addiu	$5,$sp,56
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,292($sp)
lw	$20,288($sp)
lw	$19,284($sp)
lw	$18,280($sp)
lw	$17,276($sp)
lw	$16,272($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,296
.set	macro
.set	reorder

.end	put_h264_qpel8_mc33_c
.size	put_h264_qpel8_mc33_c, .-put_h264_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc12_c
.type	put_h264_qpel8_mc12_c, @function
put_h264_qpel8_mc12_c:
.frame	$sp,504,$31		# vars= 440, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-504
sll	$2,$6,1
sw	$16,480($sp)
addiu	$7,$sp,352
subu	$2,$5,$2
sw	$20,496($sp)
addiu	$16,$sp,248
sw	$19,492($sp)
sw	$17,484($sp)
move	$19,$5
sw	$31,500($sp)
move	$17,$6
sw	$18,488($sp)
move	$20,$4
$L1750:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$7,$L1750
addu	$2,$2,$17
.set	macro
.set	reorder

addiu	$18,$sp,416
addiu	$5,$sp,264
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$18
.set	macro
.set	reorder

addiu	$5,$sp,40
move	$4,$16
move	$6,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,500($sp)
lw	$20,496($sp)
lw	$19,492($sp)
lw	$18,488($sp)
lw	$17,484($sp)
lw	$16,480($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,504
.set	macro
.set	reorder

.end	put_h264_qpel8_mc12_c
.size	put_h264_qpel8_mc12_c, .-put_h264_qpel8_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc32_c
.type	put_h264_qpel8_mc32_c, @function
put_h264_qpel8_mc32_c:
.frame	$sp,504,$31		# vars= 440, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
subu	$2,$0,$6
addiu	$sp,$sp,-504
sll	$2,$2,1
sw	$16,480($sp)
addiu	$7,$sp,352
addiu	$2,$2,1
sw	$20,496($sp)
addiu	$16,$sp,248
sw	$19,492($sp)
addu	$2,$5,$2
sw	$17,484($sp)
sw	$31,500($sp)
move	$17,$6
sw	$18,488($sp)
move	$19,$5
move	$20,$4
$L1754:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$7,$L1754
addu	$2,$2,$17
.set	macro
.set	reorder

addiu	$18,$sp,416
addiu	$5,$sp,264
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$18
.set	macro
.set	reorder

addiu	$5,$sp,40
move	$4,$16
move	$6,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,500($sp)
lw	$20,496($sp)
lw	$19,492($sp)
lw	$18,488($sp)
lw	$17,484($sp)
lw	$16,480($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,504
.set	macro
.set	reorder

.end	put_h264_qpel8_mc32_c
.size	put_h264_qpel8_mc32_c, .-put_h264_qpel8_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc12_c
.type	avg_h264_qpel8_mc12_c, @function
avg_h264_qpel8_mc12_c:
.frame	$sp,504,$31		# vars= 440, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-504
sll	$2,$6,1
sw	$16,480($sp)
addiu	$7,$sp,352
subu	$2,$5,$2
sw	$20,496($sp)
addiu	$16,$sp,248
sw	$19,492($sp)
sw	$17,484($sp)
move	$19,$5
sw	$31,500($sp)
move	$17,$6
sw	$18,488($sp)
move	$20,$4
$L1758:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$7,$L1758
addu	$2,$2,$17
.set	macro
.set	reorder

addiu	$18,$sp,416
addiu	$5,$sp,264
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$18
.set	macro
.set	reorder

addiu	$5,$sp,40
move	$4,$16
move	$6,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,500($sp)
lw	$20,496($sp)
lw	$19,492($sp)
lw	$18,488($sp)
lw	$17,484($sp)
lw	$16,480($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,504
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc12_c
.size	avg_h264_qpel8_mc12_c, .-avg_h264_qpel8_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel8_mc32_c
.type	avg_h264_qpel8_mc32_c, @function
avg_h264_qpel8_mc32_c:
.frame	$sp,504,$31		# vars= 440, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
subu	$2,$0,$6
addiu	$sp,$sp,-504
sll	$2,$2,1
sw	$16,480($sp)
addiu	$7,$sp,352
addiu	$2,$2,1
sw	$20,496($sp)
addiu	$16,$sp,248
sw	$19,492($sp)
addu	$2,$5,$2
sw	$17,484($sp)
sw	$31,500($sp)
move	$17,$6
sw	$18,488($sp)
move	$19,$5
move	$20,$4
$L1762:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
addiu	$16,$16,8
.set	noreorder
.set	nomacro
bne	$16,$7,$L1762
addu	$2,$2,$17
.set	macro
.set	reorder

addiu	$18,$sp,416
addiu	$5,$sp,264
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_v_lowpass
.option	pic2
move	$4,$18
.set	macro
.set	reorder

addiu	$5,$sp,40
move	$4,$16
move	$6,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$18
sw	$2,16($sp)
move	$6,$16
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,500($sp)
lw	$20,496($sp)
lw	$19,492($sp)
lw	$18,488($sp)
lw	$17,484($sp)
lw	$16,480($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,504
.set	macro
.set	reorder

.end	avg_h264_qpel8_mc32_c
.size	avg_h264_qpel8_mc32_c, .-avg_h264_qpel8_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc11_c
.type	avg_h264_qpel16_mc11_c, @function
avg_h264_qpel16_mc11_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$20,904($sp)
move	$20,$4
sw	$17,892($sp)
move	$4,$19
move	$17,$6
sw	$31,908($sp)
sw	$18,896($sp)
addiu	$18,$sp,376
sw	$16,888($sp)
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$16,$5

sll	$5,$17,1
addiu	$4,$sp,40
subu	$5,$16,$5
move	$6,$17
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
li	$16,16			# 0x10

addiu	$5,$sp,72
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,640
sw	$16,16($sp)
addiu	$6,$sp,384
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
j	$31
addiu	$sp,$sp,912

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc11_c
.size	avg_h264_qpel16_mc11_c, .-avg_h264_qpel16_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc31_c
.type	avg_h264_qpel16_mc31_c, @function
avg_h264_qpel16_mc31_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$20,904($sp)
move	$20,$4
sw	$17,892($sp)
move	$4,$19
move	$17,$6
sw	$31,908($sp)
sw	$18,896($sp)
move	$18,$5
sw	$16,888($sp)
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
li	$16,16			# 0x10

subu	$2,$0,$17
addiu	$4,$sp,40
sll	$2,$2,1
move	$6,$17
addiu	$5,$2,1
addu	$5,$18,$5
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,376

addiu	$5,$sp,72
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,640
sw	$16,16($sp)
addiu	$6,$sp,384
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
j	$31
addiu	$sp,$sp,912

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc31_c
.size	avg_h264_qpel16_mc31_c, .-avg_h264_qpel16_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc33_c
.type	avg_h264_qpel16_mc33_c, @function
avg_h264_qpel16_mc33_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$18,896($sp)
move	$18,$5
addu	$5,$5,$6
sw	$20,904($sp)
sw	$17,892($sp)
move	$20,$4
move	$17,$6
sw	$31,908($sp)
move	$4,$19
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
sw	$16,888($sp)

subu	$2,$0,$17
addiu	$4,$sp,40
sll	$2,$2,1
li	$16,16			# 0x10
addiu	$5,$2,1
move	$6,$17
addu	$5,$18,$5
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,376

addiu	$5,$sp,72
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,640
sw	$16,16($sp)
addiu	$6,$sp,384
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
j	$31
addiu	$sp,$sp,912

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc33_c
.size	avg_h264_qpel16_mc33_c, .-avg_h264_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc13_c
.type	avg_h264_qpel16_mc13_c, @function
avg_h264_qpel16_mc13_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$16,888($sp)
move	$16,$5
addu	$5,$5,$6
sw	$20,904($sp)
sw	$17,892($sp)
move	$20,$4
move	$17,$6
sw	$31,908($sp)
move	$4,$19
.option	pic0
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
sw	$18,896($sp)

sll	$5,$17,1
addiu	$4,$sp,40
subu	$5,$16,$5
move	$6,$17
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
li	$16,16			# 0x10

addiu	$18,$sp,376
addiu	$5,$sp,72
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,640
sw	$16,16($sp)
addiu	$6,$sp,384
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
j	$31
addiu	$sp,$sp,912

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc13_c
.size	avg_h264_qpel16_mc13_c, .-avg_h264_qpel16_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc12_c
.type	avg_h264_qpel16_mc12_c, @function
avg_h264_qpel16_mc12_c:
.frame	$sp,1592,$31		# vars= 1520, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1592
sll	$2,$6,1
sw	$21,1584($sp)
move	$21,$5
sw	$20,1580($sp)
subu	$5,$5,$2
move	$20,$4
sw	$19,1576($sp)
addiu	$4,$sp,712
sw	$31,1588($sp)
addiu	$19,$sp,1304
sw	$18,1572($sp)
sw	$17,1568($sp)
move	$17,$6
sw	$16,1564($sp)
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,1048

addiu	$5,$sp,744
li	$16,16			# 0x10
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$19

addiu	$5,$sp,40
move	$4,$18
move	$6,$21
.option	pic0
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,1312
sw	$16,16($sp)
addiu	$6,$sp,1056
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,1588($sp)
lw	$21,1584($sp)
lw	$20,1580($sp)
lw	$19,1576($sp)
lw	$18,1572($sp)
lw	$17,1568($sp)
lw	$16,1564($sp)
j	$31
addiu	$sp,$sp,1592

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc12_c
.size	avg_h264_qpel16_mc12_c, .-avg_h264_qpel16_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc32_c
.type	avg_h264_qpel16_mc32_c, @function
avg_h264_qpel16_mc32_c:
.frame	$sp,1592,$31		# vars= 1520, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
subu	$2,$0,$6
addiu	$sp,$sp,-1592
sll	$2,$2,1
sw	$21,1584($sp)
move	$21,$5
addiu	$5,$2,1
sw	$20,1580($sp)
move	$20,$4
sw	$19,1576($sp)
addiu	$4,$sp,712
sw	$31,1588($sp)
addu	$5,$21,$5
sw	$18,1572($sp)
addiu	$19,$sp,1304
sw	$17,1568($sp)
sw	$16,1564($sp)
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
move	$17,$6

addiu	$5,$sp,744
li	$16,16			# 0x10
addiu	$18,$sp,1048
.option	pic0
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$19

addiu	$5,$sp,40
move	$4,$18
move	$6,$21
.option	pic0
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17

move	$4,$20
move	$5,$19
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$17
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,1312
sw	$16,16($sp)
addiu	$6,$sp,1056
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$17

lw	$31,1588($sp)
lw	$21,1584($sp)
lw	$20,1580($sp)
lw	$19,1576($sp)
lw	$18,1572($sp)
lw	$17,1568($sp)
lw	$16,1564($sp)
j	$31
addiu	$sp,$sp,1592

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc32_c
.size	avg_h264_qpel16_mc32_c, .-avg_h264_qpel16_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc10_c
.type	avg_h264_qpel4_mc10_c, @function
avg_h264_qpel4_mc10_c:
.frame	$sp,56,$31		# vars= 16, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-56
sw	$18,48($sp)
move	$18,$6
sw	$16,40($sp)
li	$6,4			# 0x4
move	$16,$4
sw	$31,52($sp)
addiu	$4,$sp,24
sw	$17,44($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
lw	$4,0($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($17)  
lwr $8, 0($17)  

# 0 "" 2
#NO_APP
ori	$9,$9,0xfefe
lw	$31,52($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 27($sp)  
lwr $3, 24($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$8
and	$2,$2,$9
srl	$2,$2,1
or	$3,$3,$8
subu	$8,$3,$2
addu	$6,$16,$18
xor	$2,$8,$4
and	$2,$2,$9
srl	$2,$2,1
or	$8,$8,$4
subu	$8,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 31($sp)  
lwr $3, 28($sp)  

# 0 "" 2
#NO_APP
addu	$4,$17,$18
sw	$8,0($16)
sll	$2,$18,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($4)  
lwr $8, 0($4)  

# 0 "" 2
#NO_APP
xor	$4,$3,$8
lw	$5,0($6)
and	$4,$4,$9
srl	$4,$4,1
or	$3,$3,$8
subu	$8,$3,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 35($sp)  
lwr $3, 32($sp)  

# 0 "" 2
#NO_APP
xor	$4,$8,$5
and	$4,$4,$9
srl	$4,$4,1
or	$8,$8,$5
subu	$8,$8,$4
addu	$5,$16,$2
addu	$4,$17,$2
sw	$8,0($6)
addu	$2,$2,$18
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($4)  
lwr $7, 0($4)  

# 0 "" 2
#NO_APP
xor	$4,$3,$7
lw	$6,0($5)
and	$4,$4,$9
lw	$18,48($sp)
srl	$4,$4,1
or	$3,$3,$7
subu	$7,$3,$4
addu	$16,$16,$2
xor	$4,$7,$6
and	$4,$4,$9
srl	$4,$4,1
or	$7,$7,$6
subu	$7,$7,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 39($sp)  
lwr $3, 36($sp)  

# 0 "" 2
#NO_APP
addu	$17,$17,$2
sw	$7,0($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($17)  
lwr $2, 0($17)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
lw	$5,0($16)
and	$4,$4,$9
lw	$17,44($sp)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$9,$3,$9
srl	$9,$9,1
or	$2,$2,$5
subu	$2,$2,$9
sw	$2,0($16)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc10_c
.size	avg_h264_qpel4_mc10_c, .-avg_h264_qpel4_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc30_c
.type	avg_h264_qpel4_mc30_c, @function
avg_h264_qpel4_mc30_c:
.frame	$sp,56,$31		# vars= 16, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-56
sw	$18,48($sp)
move	$18,$6
sw	$16,40($sp)
li	$6,4			# 0x4
move	$16,$4
sw	$31,52($sp)
addiu	$4,$sp,24
sw	$17,44($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$17,$5
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
lw	$5,0($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($17)  
lwr $3, 1($17)  

# 0 "" 2
#NO_APP
ori	$9,$9,0xfefe
lw	$31,52($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 27($sp)  
lwr $8, 24($sp)  

# 0 "" 2
#NO_APP
xor	$2,$8,$3
and	$2,$2,$9
srl	$2,$2,1
or	$8,$8,$3
subu	$3,$8,$2
addiu	$4,$18,1
xor	$2,$3,$5
and	$2,$2,$9
srl	$2,$2,1
or	$3,$3,$5
subu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 31($sp)  
lwr $8, 28($sp)  

# 0 "" 2
#NO_APP
addu	$5,$16,$18
sw	$3,0($16)
addu	$2,$17,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
xor	$2,$8,$3
lw	$6,0($5)
and	$2,$2,$9
srl	$2,$2,1
or	$8,$8,$3
subu	$3,$8,$2
sll	$2,$18,1
xor	$4,$3,$6
and	$4,$4,$9
srl	$4,$4,1
or	$3,$3,$6
subu	$3,$3,$4
addiu	$4,$2,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 35($sp)  
lwr $8, 32($sp)  

# 0 "" 2
#NO_APP
sw	$3,0($5)
addu	$5,$16,$2
addu	$3,$17,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
lw	$7,0($5)
xor	$4,$8,$4
and	$4,$4,$9
srl	$4,$4,1
or	$8,$8,$3
subu	$3,$8,$4
addu	$2,$2,$18
lw	$18,48($sp)
xor	$4,$3,$7
and	$4,$4,$9
srl	$4,$4,1
or	$3,$3,$7
subu	$7,$3,$4
addiu	$4,$2,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 39($sp)  
lwr $3, 36($sp)  

# 0 "" 2
#NO_APP
addu	$16,$16,$2
sw	$7,0($5)
addu	$17,$17,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($17)  
lwr $2, 0($17)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
lw	$5,0($16)
and	$4,$4,$9
lw	$17,44($sp)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$9,$3,$9
srl	$9,$9,1
or	$2,$2,$5
subu	$2,$2,$9
sw	$2,0($16)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc30_c
.size	avg_h264_qpel4_mc30_c, .-avg_h264_qpel4_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc21_c
.type	avg_h264_qpel4_mc21_c, @function
avg_h264_qpel4_mc21_c:
.frame	$sp,152,$31		# vars= 104, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-152
sw	$17,140($sp)
move	$17,$6
sw	$16,136($sp)
li	$6,4			# 0x4
move	$16,$4
sw	$31,148($sp)
addiu	$4,$sp,120
sw	$18,144($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$18,$5
.set	macro
.set	reorder

addiu	$4,$sp,104
sw	$17,16($sp)
addiu	$5,$sp,32
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$18
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 123($sp)  
lwr $8, 120($sp)  

# 0 "" 2
#NO_APP
ori	$10,$10,0xfefe
lw	$4,0($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 107($sp)  
lwr $3, 104($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$8
lw	$31,148($sp)
and	$2,$2,$10
lw	$18,144($sp)
srl	$2,$2,1
or	$3,$3,$8
subu	$8,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 111($sp)  
lwr $5, 108($sp)  

# 0 "" 2
#NO_APP
xor	$2,$8,$4
and	$2,$2,$10
srl	$2,$2,1
or	$8,$8,$4
subu	$8,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 127($sp)  
lwr $3, 124($sp)  

# 0 "" 2
#NO_APP
addu	$7,$16,$17
xor	$2,$5,$3
sw	$8,0($16)
and	$2,$2,$10
srl	$2,$2,1
lw	$9,0($7)
or	$5,$5,$3
subu	$3,$5,$2
sll	$4,$17,1
xor	$2,$3,$9
and	$2,$2,$10
srl	$2,$2,1
or	$3,$3,$9
subu	$9,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 115($sp)  
lwr $5, 112($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 131($sp)  
lwr $3, 128($sp)  

# 0 "" 2
#NO_APP
addu	$6,$16,$4
sw	$9,0($7)
xor	$2,$5,$3
and	$2,$2,$10
srl	$2,$2,1
lw	$7,0($6)
or	$8,$5,$3
subu	$3,$8,$2
addu	$17,$4,$17
xor	$2,$3,$7
and	$2,$2,$10
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$16,$16,$17
lw	$17,140($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 135($sp)  
lwr $2, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 119($sp)  
lwr $3, 116($sp)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
sw	$5,0($6)
and	$4,$4,$10
lw	$5,0($16)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$10,$3,$10
srl	$10,$10,1
or	$2,$2,$5
subu	$2,$2,$10
sw	$2,0($16)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc21_c
.size	avg_h264_qpel4_mc21_c, .-avg_h264_qpel4_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc23_c
.type	avg_h264_qpel4_mc23_c, @function
avg_h264_qpel4_mc23_c:
.frame	$sp,152,$31		# vars= 104, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-152
sw	$18,144($sp)
move	$18,$5
sw	$17,140($sp)
addu	$5,$5,$6
move	$17,$6
sw	$16,136($sp)
li	$6,4			# 0x4
sw	$31,148($sp)
move	$16,$4
addiu	$4,$sp,120
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$17
.set	macro
.set	reorder

addiu	$4,$sp,104
addiu	$5,$sp,32
sw	$17,16($sp)
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$18
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 123($sp)  
lwr $8, 120($sp)  

# 0 "" 2
#NO_APP
ori	$10,$10,0xfefe
lw	$4,0($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 107($sp)  
lwr $3, 104($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$8
lw	$31,148($sp)
and	$2,$2,$10
lw	$18,144($sp)
srl	$2,$2,1
or	$3,$3,$8
subu	$8,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 111($sp)  
lwr $5, 108($sp)  

# 0 "" 2
#NO_APP
xor	$2,$8,$4
and	$2,$2,$10
srl	$2,$2,1
or	$8,$8,$4
subu	$8,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 127($sp)  
lwr $3, 124($sp)  

# 0 "" 2
#NO_APP
addu	$7,$16,$17
xor	$2,$5,$3
sw	$8,0($16)
and	$2,$2,$10
srl	$2,$2,1
lw	$9,0($7)
or	$5,$5,$3
subu	$3,$5,$2
sll	$4,$17,1
xor	$2,$3,$9
and	$2,$2,$10
srl	$2,$2,1
or	$3,$3,$9
subu	$9,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 115($sp)  
lwr $5, 112($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 131($sp)  
lwr $3, 128($sp)  

# 0 "" 2
#NO_APP
addu	$6,$16,$4
sw	$9,0($7)
xor	$2,$5,$3
and	$2,$2,$10
srl	$2,$2,1
lw	$7,0($6)
or	$8,$5,$3
subu	$3,$8,$2
addu	$17,$4,$17
xor	$2,$3,$7
and	$2,$2,$10
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$16,$16,$17
lw	$17,140($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 135($sp)  
lwr $2, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 119($sp)  
lwr $3, 116($sp)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
sw	$5,0($6)
and	$4,$4,$10
lw	$5,0($16)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$10,$3,$10
srl	$10,$10,1
or	$2,$2,$5
subu	$2,$2,$10
sw	$2,0($16)
lw	$16,136($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc23_c
.size	avg_h264_qpel4_mc23_c, .-avg_h264_qpel4_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	rd8x8_c
.type	rd8x8_c, @function
rd8x8_c:
.frame	$sp,376,$31		# vars= 312, regs= 8/0, args= 24, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
addu	$15,$5,$7
addu	$10,$6,$7
addu	$14,$15,$7
move	$2,$7
addu	$13,$14,$7
addu	$7,$10,$7
addu	$12,$13,$2
addiu	$sp,$sp,-376
addu	$3,$7,$2
addu	$11,$12,$2
sw	$19,356($sp)
sw	$18,352($sp)
addu	$9,$3,$2
move	$18,$4
sw	$17,348($sp)
sw	$16,344($sp)
addu	$4,$11,$2
li	$19,-16			# 0xfffffffffffffff0
sw	$31,372($sp)
addiu	$17,$sp,271
sw	$22,368($sp)
addiu	$16,$sp,191
sw	$21,364($sp)
and	$17,$17,$19
sw	$20,360($sp)
and	$16,$16,$19
lw	$21,8604($18)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $24, 3($5)  
lwr $24, 0($5)  

# 0 "" 2
#NO_APP
addu	$8,$9,$2
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $24, 3($17)  
swr $24, 0($17)  

# 0 "" 2
#NO_APP
addu	$20,$4,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 7($5)  
lwr $22, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $22, 7($17)  
swr $22, 4($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($15)  
lwr $5, 0($15)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 11($17)  
swr $5, 8($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($15)  
lwr $5, 4($15)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 15($17)  
swr $5, 12($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($14)  
lwr $5, 0($14)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 19($17)  
swr $5, 16($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($14)  
lwr $5, 4($14)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 23($17)  
swr $5, 20($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($13)  
lwr $5, 0($13)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 27($17)  
swr $5, 24($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($13)  
lwr $5, 4($13)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 31($17)  
swr $5, 28($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($12)  
lwr $5, 0($12)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 35($17)  
swr $5, 32($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($12)  
lwr $5, 4($12)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 39($17)  
swr $5, 36($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($11)  
lwr $5, 0($11)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 43($17)  
swr $5, 40($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($11)  
lwr $5, 4($11)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 47($17)  
swr $5, 44($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 51($17)  
swr $5, 48($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 7($4)  
lwr $5, 4($4)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $5, 55($17)  
swr $5, 52($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($20)  
lwr $4, 0($20)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 59($17)  
swr $4, 56($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 7($20)  
lwr $4, 4($20)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 63($17)  
swr $4, 60($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($6)  
lwr $4, 0($6)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 3($16)  
swr $4, 0($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 7($6)  
lwr $4, 4($6)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 7($16)  
swr $4, 4($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($10)  
lwr $4, 0($10)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 11($16)  
swr $4, 8($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 7($10)  
lwr $4, 4($10)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 15($16)  
swr $4, 12($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($7)  
lwr $4, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 19($16)  
swr $4, 16($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 7($7)  
lwr $4, 4($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 23($16)  
swr $4, 20($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $4, 27($16)  
swr $4, 24($16)  

# 0 "" 2
#NO_APP
addiu	$4,$sp,47
lw	$25,2964($18)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($3)  
lwr $10, 4($3)  

# 0 "" 2
#NO_APP
and	$19,$4,$19
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 31($16)  
swr $10, 28($16)  

# 0 "" 2
#NO_APP
li	$7,8			# 0x8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($9)  
lwr $3, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 35($16)  
swr $3, 32($16)  

# 0 "" 2
#NO_APP
addu	$3,$8,$2
move	$4,$19
move	$5,$17
move	$6,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 7($9)  
lwr $22, 4($9)  

# 0 "" 2
#NO_APP
addu	$2,$3,$2
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $22, 39($16)  
swr $22, 36($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($16)  
swr $9, 40($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 7($8)  
lwr $9, 4($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 47($16)  
swr $9, 44($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($3)  
lwr $8, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 51($16)  
swr $8, 48($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($3)  
lwr $10, 4($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 55($16)  
swr $10, 52($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($16)  
swr $3, 56($16)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 7($2)  
lwr $22, 4($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $22, 63($16)  
swr $22, 60($16)  

# 0 "" 2
#NO_APP
jalr	$25
addiu	$2,$sp,336
lw	$7,2872($18)
move	$6,$0
lw	$25,10576($18)
move	$4,$18
sw	$2,16($sp)
.set	noreorder
.set	nomacro
jalr	$25
move	$5,$19
.set	macro
.set	reorder

lw	$9,8004($18)
.set	noreorder
.set	nomacro
beq	$9,$0,$L1786
sw	$2,8680($18)
.set	macro
.set	reorder

lh	$4,0($19)
li	$7,1			# 0x1
lw	$3,8624($18)
lw	$12,8608($18)
lw	$10,8612($18)
addu	$3,$3,$4
lbu	$20,256($3)
$L1787:
slt	$3,$2,$7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1788
slt	$3,$7,$2
.set	macro
.set	reorder

sw	$7,336($sp)
move	$4,$0
.set	noreorder
.set	nomacro
beq	$3,$0,$L1789
addiu	$6,$18,8732
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1793
.option	pic2
li	$11,-128			# 0xffffffffffffff80
.set	macro
.set	reorder

$L1801:
sll	$4,$4,7
and	$8,$5,$11
.set	noreorder
.set	nomacro
bne	$8,$0,$L1791
addu	$3,$4,$5
.set	macro
.set	reorder

addu	$3,$12,$3
move	$4,$0
lbu	$3,0($3)
addu	$20,$20,$3
$L1792:
addiu	$7,$7,1
.set	noreorder
.set	nomacro
beq	$7,$2,$L1789
sw	$7,336($sp)
.set	macro
.set	reorder

$L1793:
addu	$5,$6,$7
lbu	$3,0($5)
sll	$3,$3,1
addu	$3,$19,$3
lh	$3,0($3)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1801
addiu	$5,$3,64
.set	macro
.set	reorder

addiu	$7,$7,1
addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$7,$2,$L1793
sw	$7,336($sp)
.set	macro
.set	reorder

$L1789:
addu	$6,$6,$2
lbu	$5,0($6)
sll	$3,$5,1
sw	$5,336($sp)
li	$5,-128			# 0xffffffffffffff80
addu	$3,$19,$3
lh	$3,0($3)
addiu	$3,$3,64
and	$5,$3,$5
.set	noreorder
.set	nomacro
bne	$5,$0,$L1794
sll	$4,$4,7
.set	macro
.set	reorder

addu	$4,$4,$3
addu	$4,$10,$4
lbu	$3,0($4)
addu	$20,$20,$3
$L1788:
bltz	$2,$L1795
bne	$9,$0,$L1802
lw	$25,10568($18)
$L1800:
lw	$7,2872($18)
move	$6,$0
move	$4,$18
.set	noreorder
.set	nomacro
jalr	$25
move	$5,$19
.set	macro
.set	reorder

$L1795:
lw	$25,5660($18)
li	$5,8			# 0x8
move	$4,$16
.set	noreorder
.set	nomacro
jalr	$25
move	$6,$19
.set	macro
.set	reorder

li	$2,8			# 0x8
lw	$25,3048($18)
move	$4,$0
li	$7,8			# 0x8
move	$5,$16
sw	$2,16($sp)
.set	noreorder
.set	nomacro
jalr	$25
move	$6,$17
.set	macro
.set	reorder

li	$4,109			# 0x6d
lw	$3,2872($18)
lw	$31,372($sp)
lw	$22,368($sp)
mul	$20,$3,$20
lw	$21,364($sp)
lw	$19,356($sp)
lw	$18,352($sp)
lw	$17,348($sp)
lw	$16,344($sp)
mul	$3,$3,$20
lw	$20,360($sp)
addiu	$sp,$sp,376
mul	$3,$3,$4
addiu	$3,$3,64
sra	$3,$3,7
.set	noreorder
.set	nomacro
j	$31
addu	$2,$3,$2
.set	macro
.set	reorder

$L1786:
lw	$12,8616($18)
move	$7,$0
lw	$10,8620($18)
.option	pic0
.set	noreorder
.set	nomacro
j	$L1787
.option	pic2
move	$20,$0
.set	macro
.set	reorder

$L1802:
.option	pic0
.set	noreorder
.set	nomacro
j	$L1800
.option	pic2
lw	$25,10564($18)
.set	macro
.set	reorder

$L1794:
.option	pic0
.set	noreorder
.set	nomacro
j	$L1788
.option	pic2
addu	$20,$20,$21
.set	macro
.set	reorder

$L1791:
addu	$20,$20,$21
.option	pic0
.set	noreorder
.set	nomacro
j	$L1792
.option	pic2
move	$4,$0
.set	macro
.set	reorder

.end	rd8x8_c
.size	rd8x8_c, .-rd8x8_c
.align	2
.set	nomips16
.set	nomicromips
.ent	rd16_c
.type	rd16_c, @function
rd16_c:
.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-64
sw	$17,40($sp)
li	$17,8			# 0x8
sw	$19,48($sp)
move	$19,$5
sw	$18,44($sp)
move	$18,$6
sw	$17,16($sp)
sw	$31,60($sp)
sw	$21,56($sp)
move	$21,$4
sw	$20,52($sp)
move	$20,$7
.option	pic0
jal	rd8x8_c
.option	pic2
sw	$16,36($sp)

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	rd8x8_c
.option	pic2
move	$16,$2

lw	$3,80($sp)
addu	$16,$16,$2
li	$2,16			# 0x10
beq	$3,$2,$L1806
lw	$31,60($sp)

move	$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

$L1806:
sll	$2,$20,3
sw	$17,16($sp)
move	$4,$21
addu	$19,$19,$2
addu	$18,$18,$2
move	$5,$19
move	$6,$18
.option	pic0
jal	rd8x8_c
.option	pic2
move	$7,$20

addiu	$5,$19,8
addiu	$6,$18,8
sw	$17,16($sp)
move	$4,$21
move	$7,$20
.option	pic0
jal	rd8x8_c
.option	pic2
addu	$16,$2,$16

lw	$31,60($sp)
addu	$16,$2,$16
lw	$21,56($sp)
lw	$20,52($sp)
move	$2,$16
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	rd16_c
.size	rd16_c, .-rd16_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc01_c
.type	avg_h264_qpel4_mc01_c, @function
avg_h264_qpel4_mc01_c:
.frame	$sp,96,$31		# vars= 56, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-96
sw	$18,88($sp)
sll	$18,$6,1
sw	$16,80($sp)
move	$16,$6
subu	$3,$5,$18
sw	$17,84($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
sw	$31,92($sp)
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$2,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
move	$2,$3
addu	$8,$9,$16
move	$17,$4
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
addiu	$4,$sp,60
addiu	$5,$sp,32
addu	$2,$3,$16
li	$6,4			# 0x4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 59($sp)  
swr $7, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 35($sp)  
lwr $5, 32($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 63($sp)  
lwr $3, 60($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
lw	$31,92($sp)
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$5
subu	$5,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 39($sp)  
lwr $6, 36($sp)  

# 0 "" 2
#NO_APP
xor	$2,$5,$4
and	$2,$2,$8
srl	$2,$2,1
or	$5,$5,$4
subu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 67($sp)  
lwr $3, 64($sp)  

# 0 "" 2
#NO_APP
addu	$7,$17,$16
xor	$2,$3,$6
sw	$5,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$4,0($7)
or	$3,$3,$6
subu	$6,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 71($sp)  
lwr $5, 68($sp)  

# 0 "" 2
#NO_APP
xor	$2,$6,$4
and	$2,$2,$8
srl	$2,$2,1
or	$6,$6,$4
subu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 43($sp)  
lwr $3, 40($sp)  

# 0 "" 2
#NO_APP
addu	$4,$17,$18
xor	$2,$5,$3
sw	$6,0($7)
and	$2,$2,$8
srl	$2,$2,1
lw	$6,0($4)
or	$7,$5,$3
subu	$3,$7,$2
addu	$16,$18,$16
lw	$18,88($sp)
xor	$2,$3,$6
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$6
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,80($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 75($sp)  
lwr $3, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 47($sp)  
lwr $2, 44($sp)  

# 0 "" 2
#NO_APP
sw	$5,0($4)
xor	$4,$3,$2
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,84($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc01_c
.size	avg_h264_qpel4_mc01_c, .-avg_h264_qpel4_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc03_c
.type	avg_h264_qpel4_mc03_c, @function
avg_h264_qpel4_mc03_c:
.frame	$sp,96,$31		# vars= 56, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-96
sw	$18,88($sp)
sll	$18,$6,1
sw	$16,80($sp)
move	$16,$6
subu	$3,$5,$18
sw	$17,84($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
sw	$31,92($sp)
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$2,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
move	$2,$3
addu	$8,$9,$16
move	$17,$4
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
addiu	$4,$sp,60
addiu	$5,$sp,32
addu	$2,$3,$16
li	$6,4			# 0x4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 59($sp)  
swr $7, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 39($sp)  
lwr $5, 36($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 63($sp)  
lwr $3, 60($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
lw	$31,92($sp)
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$5
subu	$5,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 43($sp)  
lwr $6, 40($sp)  

# 0 "" 2
#NO_APP
xor	$2,$5,$4
and	$2,$2,$8
srl	$2,$2,1
or	$5,$5,$4
subu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 67($sp)  
lwr $3, 64($sp)  

# 0 "" 2
#NO_APP
addu	$7,$17,$16
xor	$2,$3,$6
sw	$5,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$4,0($7)
or	$3,$3,$6
subu	$6,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 71($sp)  
lwr $5, 68($sp)  

# 0 "" 2
#NO_APP
xor	$2,$6,$4
and	$2,$2,$8
srl	$2,$2,1
or	$6,$6,$4
subu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 47($sp)  
lwr $3, 44($sp)  

# 0 "" 2
#NO_APP
addu	$4,$17,$18
xor	$2,$5,$3
sw	$6,0($7)
and	$2,$2,$8
srl	$2,$2,1
lw	$6,0($4)
or	$7,$5,$3
subu	$3,$7,$2
addu	$16,$18,$16
lw	$18,88($sp)
xor	$2,$3,$6
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$6
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,80($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 75($sp)  
lwr $3, 72($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 51($sp)  
lwr $2, 48($sp)  

# 0 "" 2
#NO_APP
sw	$5,0($4)
xor	$4,$3,$2
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,84($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc03_c
.size	avg_h264_qpel4_mc03_c, .-avg_h264_qpel4_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc11_c
.type	avg_h264_qpel4_mc11_c, @function
avg_h264_qpel4_mc11_c:
.frame	$sp,120,$31		# vars= 72, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
sw	$18,108($sp)
sll	$18,$6,1
sw	$17,104($sp)
move	$17,$4
sw	$16,100($sp)
addiu	$4,$sp,76
move	$16,$6
sw	$19,112($sp)
li	$6,4			# 0x4
sw	$31,116($sp)
move	$19,$5
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

subu	$2,$19,$18
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$9,$16
addiu	$4,$sp,60
addu	$7,$8,$16
addiu	$5,$sp,32
addu	$3,$7,$16
li	$6,4			# 0x4
addu	$2,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 79($sp)  
lwr $5, 76($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 63($sp)  
lwr $3, 60($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
lw	$31,116($sp)
and	$2,$2,$8
lw	$19,112($sp)
srl	$2,$2,1
or	$3,$3,$5
subu	$5,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 83($sp)  
lwr $7, 80($sp)  

# 0 "" 2
#NO_APP
xor	$2,$5,$4
and	$2,$2,$8
srl	$2,$2,1
or	$5,$5,$4
subu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 67($sp)  
lwr $3, 64($sp)  

# 0 "" 2
#NO_APP
addu	$6,$17,$16
xor	$2,$3,$7
sw	$5,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$4,0($6)
or	$3,$3,$7
subu	$7,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 71($sp)  
lwr $5, 68($sp)  

# 0 "" 2
#NO_APP
xor	$2,$7,$4
and	$2,$2,$8
srl	$2,$2,1
or	$7,$7,$4
subu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 87($sp)  
lwr $3, 84($sp)  

# 0 "" 2
#NO_APP
addu	$4,$17,$18
xor	$2,$5,$3
sw	$7,0($6)
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($4)
or	$6,$5,$3
subu	$3,$6,$2
addu	$16,$18,$16
lw	$18,108($sp)
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,100($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 91($sp)  
lwr $2, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 75($sp)  
lwr $3, 72($sp)  

# 0 "" 2
#NO_APP
sw	$5,0($4)
xor	$4,$3,$2
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,104($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc11_c
.size	avg_h264_qpel4_mc11_c, .-avg_h264_qpel4_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc31_c
.type	avg_h264_qpel4_mc31_c, @function
avg_h264_qpel4_mc31_c:
.frame	$sp,112,$31		# vars= 72, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
sw	$17,100($sp)
move	$17,$4
sw	$16,96($sp)
addiu	$4,$sp,76
move	$16,$6
sw	$31,108($sp)
li	$6,4			# 0x4
sw	$18,104($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$18,$5
.set	macro
.set	reorder

subu	$2,$0,$16
addiu	$4,$sp,60
sll	$2,$2,1
addiu	$5,$sp,32
addiu	$2,$2,1
li	$6,4			# 0x4
addu	$2,$18,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 27($sp)  
swr $3, 24($sp)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 31($sp)  
swr $7, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$7
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 35($sp)  
swr $3, 32($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 39($sp)  
swr $9, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 79($sp)  
lwr $6, 76($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 63($sp)  
lwr $3, 60($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$6
lw	$31,108($sp)
and	$2,$2,$8
lw	$18,104($sp)
srl	$2,$2,1
or	$3,$3,$6
subu	$6,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
#NO_APP
xor	$2,$6,$4
and	$2,$2,$8
srl	$2,$2,1
or	$6,$6,$4
subu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 83($sp)  
lwr $3, 80($sp)  

# 0 "" 2
#NO_APP
addu	$10,$17,$16
xor	$2,$5,$3
sw	$6,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($10)
or	$5,$5,$3
subu	$3,$5,$2
sll	$4,$16,1
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$7,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 71($sp)  
lwr $5, 68($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 87($sp)  
lwr $3, 84($sp)  

# 0 "" 2
#NO_APP
addu	$9,$17,$4
sw	$7,0($10)
xor	$2,$5,$3
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($9)
or	$6,$5,$3
subu	$3,$6,$2
addu	$16,$4,$16
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,96($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 91($sp)  
lwr $2, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 75($sp)  
lwr $3, 72($sp)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
sw	$5,0($9)
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc31_c
.size	avg_h264_qpel4_mc31_c, .-avg_h264_qpel4_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc13_c
.type	avg_h264_qpel4_mc13_c, @function
avg_h264_qpel4_mc13_c:
.frame	$sp,120,$31		# vars= 72, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
sw	$19,112($sp)
sll	$19,$6,1
sw	$18,108($sp)
move	$18,$5
sw	$17,104($sp)
addu	$5,$5,$6
move	$17,$4
sw	$16,100($sp)
addiu	$4,$sp,76
sw	$31,116($sp)
move	$16,$6
li	$6,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

subu	$2,$18,$19
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 27($sp)  
swr $7, 24($sp)  

# 0 "" 2
#NO_APP
addu	$10,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 31($sp)  
swr $2, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$9,$16
addiu	$4,$sp,60
addu	$7,$8,$16
addiu	$5,$sp,32
addu	$3,$7,$16
li	$6,4			# 0x4
addu	$2,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 35($sp)  
swr $11, 32($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 39($sp)  
swr $10, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 79($sp)  
lwr $5, 76($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 63($sp)  
lwr $3, 60($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
lw	$31,116($sp)
and	$2,$2,$8
lw	$18,108($sp)
srl	$2,$2,1
or	$3,$3,$5
subu	$5,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 83($sp)  
lwr $7, 80($sp)  

# 0 "" 2
#NO_APP
xor	$2,$5,$4
and	$2,$2,$8
srl	$2,$2,1
or	$5,$5,$4
subu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 67($sp)  
lwr $3, 64($sp)  

# 0 "" 2
#NO_APP
addu	$6,$17,$16
xor	$2,$3,$7
sw	$5,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$4,0($6)
or	$3,$3,$7
subu	$7,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 71($sp)  
lwr $5, 68($sp)  

# 0 "" 2
#NO_APP
xor	$2,$7,$4
and	$2,$2,$8
srl	$2,$2,1
or	$7,$7,$4
subu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 87($sp)  
lwr $3, 84($sp)  

# 0 "" 2
#NO_APP
addu	$4,$17,$19
xor	$2,$5,$3
sw	$7,0($6)
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($4)
or	$6,$5,$3
subu	$3,$6,$2
addu	$16,$19,$16
lw	$19,112($sp)
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,100($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 91($sp)  
lwr $2, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 75($sp)  
lwr $3, 72($sp)  

# 0 "" 2
#NO_APP
sw	$5,0($4)
xor	$4,$3,$2
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,104($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc13_c
.size	avg_h264_qpel4_mc13_c, .-avg_h264_qpel4_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc33_c
.type	avg_h264_qpel4_mc33_c, @function
avg_h264_qpel4_mc33_c:
.frame	$sp,112,$31		# vars= 72, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
sw	$18,104($sp)
move	$18,$5
sw	$17,100($sp)
addu	$5,$5,$6
move	$17,$4
sw	$16,96($sp)
addiu	$4,$sp,76
sw	$31,108($sp)
move	$16,$6
li	$6,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_h_lowpass
.option	pic2
move	$7,$16
.set	macro
.set	reorder

subu	$2,$0,$16
addiu	$4,$sp,60
sll	$2,$2,1
addiu	$5,$sp,32
addiu	$2,$2,1
li	$6,4			# 0x4
addu	$2,$18,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 27($sp)  
swr $3, 24($sp)  

# 0 "" 2
#NO_APP
addu	$3,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 31($sp)  
swr $7, 28($sp)  

# 0 "" 2
#NO_APP
addu	$8,$2,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$7
addu	$7,$8,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 35($sp)  
swr $3, 32($sp)  

# 0 "" 2
#NO_APP
addu	$3,$7,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 39($sp)  
swr $9, 36($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 43($sp)  
swr $9, 40($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 47($sp)  
swr $8, 44($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 51($sp)  
swr $7, 48($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 55($sp)  
swr $3, 52($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 59($sp)  
swr $3, 56($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 79($sp)  
lwr $6, 76($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 63($sp)  
lwr $3, 60($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$6
lw	$31,108($sp)
and	$2,$2,$8
lw	$18,104($sp)
srl	$2,$2,1
or	$3,$3,$6
subu	$6,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 67($sp)  
lwr $5, 64($sp)  

# 0 "" 2
#NO_APP
xor	$2,$6,$4
and	$2,$2,$8
srl	$2,$2,1
or	$6,$6,$4
subu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 83($sp)  
lwr $3, 80($sp)  

# 0 "" 2
#NO_APP
addu	$10,$17,$16
xor	$2,$5,$3
sw	$6,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($10)
or	$5,$5,$3
subu	$3,$5,$2
sll	$4,$16,1
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$7,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 71($sp)  
lwr $5, 68($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 87($sp)  
lwr $3, 84($sp)  

# 0 "" 2
#NO_APP
addu	$9,$17,$4
sw	$7,0($10)
xor	$2,$5,$3
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($9)
or	$6,$5,$3
subu	$3,$6,$2
addu	$16,$4,$16
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,96($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 91($sp)  
lwr $2, 88($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 75($sp)  
lwr $3, 72($sp)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
sw	$5,0($9)
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc33_c
.size	avg_h264_qpel4_mc33_c, .-avg_h264_qpel4_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc12_c
.type	avg_h264_qpel4_mc12_c, @function
avg_h264_qpel4_mc12_c:
.frame	$sp,200,$31		# vars= 144, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-200
sw	$18,188($sp)
sll	$18,$6,1
sw	$16,180($sp)
move	$16,$6
subu	$3,$5,$18
sw	$19,192($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
addu	$2,$3,$6
sw	$17,184($sp)
move	$3,$7
sw	$31,196($sp)
addu	$10,$2,$6
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 107($sp)  
swr $3, 104($sp)  

# 0 "" 2
#NO_APP
addu	$9,$10,$6
move	$17,$4
addu	$8,$9,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
#NO_APP
addu	$7,$8,$16
move	$19,$5
addu	$3,$7,$16
addiu	$4,$sp,156
addiu	$5,$sp,112
addu	$2,$3,$16
li	$6,4			# 0x4
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 111($sp)  
swr $11, 108($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($10)  
lwr $11, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $11, 115($sp)  
swr $11, 112($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 119($sp)  
swr $10, 116($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 123($sp)  
swr $9, 120($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 127($sp)  
swr $8, 124($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 131($sp)  
swr $7, 128($sp)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 135($sp)  
swr $3, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 139($sp)  
swr $3, 136($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
addiu	$4,$sp,140
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$19
.set	macro
.set	reorder

li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 159($sp)  
lwr $5, 156($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 143($sp)  
lwr $3, 140($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
lw	$31,196($sp)
and	$2,$2,$8
lw	$19,192($sp)
srl	$2,$2,1
or	$3,$3,$5
subu	$5,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 163($sp)  
lwr $7, 160($sp)  

# 0 "" 2
#NO_APP
xor	$2,$5,$4
and	$2,$2,$8
srl	$2,$2,1
or	$5,$5,$4
subu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 147($sp)  
lwr $3, 144($sp)  

# 0 "" 2
#NO_APP
addu	$6,$17,$16
xor	$2,$3,$7
sw	$5,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$4,0($6)
or	$3,$3,$7
subu	$7,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 151($sp)  
lwr $5, 148($sp)  

# 0 "" 2
#NO_APP
xor	$2,$7,$4
and	$2,$2,$8
srl	$2,$2,1
or	$7,$7,$4
subu	$7,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 167($sp)  
lwr $3, 164($sp)  

# 0 "" 2
#NO_APP
addu	$4,$17,$18
xor	$2,$5,$3
sw	$7,0($6)
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($4)
or	$6,$5,$3
subu	$3,$6,$2
addu	$16,$18,$16
lw	$18,188($sp)
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,180($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 171($sp)  
lwr $2, 168($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 155($sp)  
lwr $3, 152($sp)  

# 0 "" 2
#NO_APP
sw	$5,0($4)
xor	$4,$3,$2
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,184($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,200
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc12_c
.size	avg_h264_qpel4_mc12_c, .-avg_h264_qpel4_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel4_mc32_c
.type	avg_h264_qpel4_mc32_c, @function
avg_h264_qpel4_mc32_c:
.frame	$sp,192,$31		# vars= 144, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
subu	$2,$0,$6
addiu	$sp,$sp,-192
sll	$2,$2,1
sw	$16,176($sp)
move	$16,$6
addiu	$2,$2,1
sw	$18,184($sp)
sw	$17,180($sp)
move	$18,$5
addu	$2,$5,$2
sw	$31,188($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
addu	$3,$2,$6
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 107($sp)  
swr $7, 104($sp)  

# 0 "" 2
#NO_APP
addu	$10,$3,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
addu	$9,$10,$16
move	$17,$4
addu	$8,$9,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $2, 111($sp)  
swr $2, 108($sp)  

# 0 "" 2
#NO_APP
addu	$7,$8,$16
addiu	$4,$sp,156
addu	$3,$7,$16
addiu	$5,$sp,112
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($10)  
lwr $2, 0($10)  

# 0 "" 2
#NO_APP
li	$6,4			# 0x4
move	$10,$2
addu	$2,$3,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 115($sp)  
swr $10, 112($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($9)  
lwr $10, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $10, 119($sp)  
swr $10, 116($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $9, 123($sp)  
swr $9, 120($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 127($sp)  
swr $8, 124($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 131($sp)  
swr $7, 128($sp)  

# 0 "" 2
#NO_APP
addu	$2,$2,$16
#APP
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 135($sp)  
swr $3, 132($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 139($sp)  
swr $3, 136($sp)  

# 0 "" 2
#NO_APP
.option	pic0
jal	put_h264_qpel4_v_lowpass.constprop.29
.option	pic2
addiu	$4,$sp,140
addiu	$5,$sp,32
sw	$16,16($sp)
li	$7,4			# 0x4
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel4_hv_lowpass.constprop.28
.option	pic2
move	$6,$18
.set	macro
.set	reorder

li	$8,-16908288			# 0xfffffffffefe0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 159($sp)  
lwr $6, 156($sp)  

# 0 "" 2
#NO_APP
ori	$8,$8,0xfefe
lw	$4,0($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 143($sp)  
lwr $3, 140($sp)  

# 0 "" 2
#NO_APP
xor	$2,$3,$6
lw	$31,188($sp)
and	$2,$2,$8
lw	$18,184($sp)
srl	$2,$2,1
or	$3,$3,$6
subu	$6,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 147($sp)  
lwr $5, 144($sp)  

# 0 "" 2
#NO_APP
xor	$2,$6,$4
and	$2,$2,$8
srl	$2,$2,1
or	$6,$6,$4
subu	$6,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 163($sp)  
lwr $3, 160($sp)  

# 0 "" 2
#NO_APP
addu	$10,$17,$16
xor	$2,$5,$3
sw	$6,0($17)
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($10)
or	$5,$5,$3
subu	$3,$5,$2
sll	$4,$16,1
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$7,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 151($sp)  
lwr $5, 148($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 167($sp)  
lwr $3, 164($sp)  

# 0 "" 2
#NO_APP
addu	$9,$17,$4
sw	$7,0($10)
xor	$2,$5,$3
and	$2,$2,$8
srl	$2,$2,1
lw	$7,0($9)
or	$6,$5,$3
subu	$3,$6,$2
addu	$16,$4,$16
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
or	$3,$3,$7
subu	$5,$3,$2
addu	$17,$17,$16
lw	$16,176($sp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 171($sp)  
lwr $2, 168($sp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 155($sp)  
lwr $3, 152($sp)  

# 0 "" 2
#NO_APP
xor	$4,$3,$2
sw	$5,0($9)
and	$4,$4,$8
lw	$5,0($17)
srl	$4,$4,1
or	$3,$3,$2
subu	$2,$3,$4
xor	$3,$2,$5
and	$8,$3,$8
srl	$8,$8,1
or	$2,$2,$5
subu	$2,$2,$8
sw	$2,0($17)
lw	$17,180($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,192
.set	macro
.set	reorder

.end	avg_h264_qpel4_mc32_c
.size	avg_h264_qpel4_mc32_c, .-avg_h264_qpel4_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_tpel_pixels_mc00_c
.type	avg_tpel_pixels_mc00_c, @function
avg_tpel_pixels_mc00_c:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-48
li	$2,4			# 0x4
sw	$19,40($sp)
sw	$18,36($sp)
move	$18,$6
sw	$17,32($sp)
move	$17,$4
sw	$16,28($sp)
move	$16,$5
sw	$31,44($sp)
.set	noreorder
.set	nomacro
beq	$7,$2,$L1825
lw	$19,64($sp)
.set	macro
.set	reorder

slt	$2,$7,5
.set	noreorder
.set	nomacro
bne	$2,$0,$L1841
li	$2,8			# 0x8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$7,$2,$L1840
li	$2,16			# 0x10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$7,$2,$L1842
lw	$31,44($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_c
.option	pic2
move	$7,$19
.set	macro
.set	reorder

addiu	$4,$17,8
addiu	$5,$16,8
move	$6,$18
$L1840:
lw	$31,44($sp)
move	$7,$19
lw	$18,36($sp)
lw	$19,40($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	avg_pixels8_c
.option	pic2
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L1841:
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$7,$2,$L1842
lw	$31,44($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgtz	$19,$L1834
move	$5,$0
.set	macro
.set	reorder

$L1823:
lw	$31,44($sp)
$L1842:
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L1825:
.set	noreorder
.set	nomacro
blez	$19,$L1823
move	$4,$0
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
ori	$6,$6,0xfefe
$L1835:
lw	$2,0($17)
addiu	$4,$4,1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($16)  
lwr $5, 0($16)  

# 0 "" 2
#NO_APP
addu	$16,$16,$18
xor	$3,$2,$5
and	$3,$3,$6
srl	$3,$3,1
or	$2,$2,$5
subu	$2,$2,$3
sw	$2,0($17)
.set	noreorder
.set	nomacro
bne	$4,$19,$L1835
addu	$17,$17,$18
.set	macro
.set	reorder

lw	$31,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L1834:
li	$6,-16908288			# 0xfffffffffefe0000
ori	$6,$6,0xfefe
$L1836:
lbu	$3,1($16)
addiu	$5,$5,1
lbu	$4,0($16)
addu	$16,$16,$18
lhu	$2,0($17)
sll	$3,$3,8
or	$3,$3,$4
xor	$4,$2,$3
and	$4,$4,$6
srl	$4,$4,1
or	$2,$2,$3
subu	$2,$2,$4
sh	$2,0($17)
.set	noreorder
.set	nomacro
bne	$5,$19,$L1836
addu	$17,$17,$18
.set	macro
.set	reorder

lw	$31,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

.end	avg_tpel_pixels_mc00_c
.size	avg_tpel_pixels_mc00_c, .-avg_tpel_pixels_mc00_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_pixels4_x2_c
.type	put_pixels4_x2_c, @function
put_pixels4_x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1848
li	$11,-16908288			# 0xfffffffffefe0000
.set	macro
.set	reorder

move	$8,$0
move	$9,$0
ori	$11,$11,0xfefe
$L1845:
addu	$3,$5,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($3)  
lwr $10, 0($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($3)  
lwr $2, 1($3)  

# 0 "" 2
#NO_APP
xor	$3,$2,$10
and	$3,$3,$11
srl	$3,$3,1
or	$2,$2,$10
subu	$2,$2,$3
addiu	$9,$9,1
addu	$3,$4,$8
addu	$8,$8,$6
.set	noreorder
.set	nomacro
bne	$9,$7,$L1845
sw	$2,0($3)
.set	macro
.set	reorder

$L1848:
j	$31
.end	put_pixels4_x2_c
.size	put_pixels4_x2_c, .-put_pixels4_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_pixels4_x2_c
.type	avg_pixels4_x2_c, @function
avg_pixels4_x2_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L1854
li	$13,-16908288			# 0xfffffffffefe0000
.set	macro
.set	reorder

move	$9,$0
move	$10,$0
ori	$13,$13,0xfefe
$L1851:
addu	$8,$5,$9
addu	$11,$4,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($8)  
lwr $2, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$8,$3,$2
lw	$12,0($11)
and	$8,$8,$13
srl	$8,$8,1
or	$3,$3,$2
subu	$2,$3,$8
addiu	$10,$10,1
xor	$3,$2,$12
and	$3,$3,$13
srl	$3,$3,1
or	$2,$2,$12
subu	$2,$2,$3
addu	$9,$9,$6
.set	noreorder
.set	nomacro
bne	$10,$7,$L1851
sw	$2,0($11)
.set	macro
.set	reorder

$L1854:
j	$31
.end	avg_pixels4_x2_c
.size	avg_pixels4_x2_c, .-avg_pixels4_x2_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc10_c
.type	put_no_rnd_qpel8_mc10_c, @function
put_no_rnd_qpel8_mc10_c:
.frame	$sp,120,$31		# vars= 64, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
li	$2,8			# 0x8
sw	$19,112($sp)
addiu	$19,$sp,32
sw	$18,108($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$17,104($sp)
sw	$2,16($sp)
move	$17,$4
move	$7,$18
sw	$16,100($sp)
move	$4,$19
sw	$31,116($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,96
move	$9,$0
move	$8,$19
ori	$5,$5,0xfefe
$L1856:
addu	$11,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($8)  
lwr $3, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($11)  
lwr $4, 0($11)  

# 0 "" 2
#NO_APP
xor	$2,$3,$4
and	$2,$2,$5
srl	$2,$2,1
and	$3,$3,$4
addu	$3,$2,$3
addiu	$10,$9,4
addu	$4,$17,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($8)  
lwr $2, 4($8)  

# 0 "" 2
#NO_APP
sw	$3,0($4)
addu	$3,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 7($11)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
xor	$3,$2,$4
and	$3,$3,$5
srl	$3,$3,1
and	$2,$2,$4
addu	$2,$3,$2
addu	$10,$17,$10
addiu	$8,$8,8
sw	$2,0($10)
.set	noreorder
.set	nomacro
bne	$8,$6,$L1856
addu	$9,$9,$18
.set	macro
.set	reorder

lw	$31,116($sp)
lw	$19,112($sp)
lw	$18,108($sp)
lw	$17,104($sp)
lw	$16,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc10_c
.size	put_no_rnd_qpel8_mc10_c, .-put_no_rnd_qpel8_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc30_c
.type	put_no_rnd_qpel8_mc30_c, @function
put_no_rnd_qpel8_mc30_c:
.frame	$sp,120,$31		# vars= 64, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
li	$2,8			# 0x8
sw	$19,112($sp)
addiu	$19,$sp,32
sw	$18,108($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$17,104($sp)
sw	$2,16($sp)
move	$17,$4
move	$7,$18
sw	$16,100($sp)
move	$4,$19
sw	$31,116($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,96
move	$9,$0
move	$8,$19
ori	$5,$5,0xfefe
$L1860:
addiu	$10,$9,4
addu	$11,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($8)  
lwr $3, 0($8)  

# 0 "" 2
#NO_APP
addu	$2,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 0($2)  
lwr $4, 1($11)  

# 0 "" 2
#NO_APP
xor	$2,$3,$4
and	$2,$2,$5
srl	$2,$2,1
and	$3,$3,$4
addu	$3,$2,$3
addu	$4,$17,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($8)  
lwr $2, 4($8)  

# 0 "" 2
#NO_APP
sw	$3,0($4)
addu	$10,$17,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 8($11)  
lwr $3, 5($11)  

# 0 "" 2
#NO_APP
move	$11,$3
xor	$3,$2,$3
and	$3,$3,$5
srl	$3,$3,1
and	$2,$2,$11
addu	$2,$3,$2
addiu	$8,$8,8
addu	$9,$9,$18
.set	noreorder
.set	nomacro
bne	$8,$6,$L1860
sw	$2,0($10)
.set	macro
.set	reorder

lw	$31,116($sp)
lw	$19,112($sp)
lw	$18,108($sp)
lw	$17,104($sp)
lw	$16,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc30_c
.size	put_no_rnd_qpel8_mc30_c, .-put_no_rnd_qpel8_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc01_c
.type	put_no_rnd_qpel8_mc01_c, @function
put_no_rnd_qpel8_mc01_c:
.frame	$sp,256,$31		# vars= 208, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-256
sw	$16,236($sp)
addiu	$16,$sp,24
sw	$18,244($sp)
move	$18,$4
sw	$17,240($sp)
move	$4,$16
addiu	$17,$sp,168
sw	$31,252($sp)
sw	$19,248($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$19,$6
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$12,$sp,152
move	$5,$16
move	$9,$17
move	$4,$18
ori	$6,$6,0xfefe
$L1864:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($5)  
lwr $11, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($9)  
lwr $7, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($5)  
lwr $10, 4($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($9)  
lwr $2, 4($9)  

# 0 "" 2
#NO_APP
xor	$8,$7,$11
xor	$3,$2,$10
and	$8,$8,$6
and	$3,$3,$6
srl	$8,$8,1
srl	$3,$3,1
and	$7,$7,$11
and	$2,$2,$10
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$5,$5,16
sw	$7,0($4)
addiu	$9,$9,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$12,$L1864
addu	$4,$4,$19
.set	macro
.set	reorder

lw	$31,252($sp)
lw	$19,248($sp)
lw	$18,244($sp)
lw	$17,240($sp)
lw	$16,236($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,256
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc01_c
.size	put_no_rnd_qpel8_mc01_c, .-put_no_rnd_qpel8_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc03_c
.type	put_no_rnd_qpel8_mc03_c, @function
put_no_rnd_qpel8_mc03_c:
.frame	$sp,256,$31		# vars= 208, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-256
sw	$19,248($sp)
addiu	$19,$sp,24
sw	$17,240($sp)
move	$17,$4
sw	$16,236($sp)
move	$4,$19
addiu	$16,$sp,168
sw	$31,252($sp)
sw	$18,244($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$18,$6
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,43
addiu	$6,$sp,171
move	$9,$16
move	$4,$17
ori	$12,$12,0xfefe
$L1868:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 0($8)  
lwr $11, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($9)  
lwr $5, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 4($8)  
lwr $10, 1($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($9)  
lwr $2, 4($9)  

# 0 "" 2
#NO_APP
xor	$7,$5,$11
xor	$3,$2,$10
and	$7,$7,$12
and	$3,$3,$12
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$11
and	$2,$2,$10
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,16
sw	$5,0($4)
addiu	$9,$9,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$6,$L1868
addu	$4,$4,$18
.set	macro
.set	reorder

lw	$31,252($sp)
lw	$19,248($sp)
lw	$18,244($sp)
lw	$17,240($sp)
lw	$16,236($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,256
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc03_c
.size	put_no_rnd_qpel8_mc03_c, .-put_no_rnd_qpel8_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc21_c
.type	put_no_rnd_qpel8_mc21_c, @function
put_no_rnd_qpel8_mc21_c:
.frame	$sp,184,$31		# vars= 136, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-184
li	$2,9			# 0x9
sw	$16,168($sp)
addiu	$16,$sp,32
sw	$17,172($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$18,176($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,180($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$sp,104
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$10,$0
li	$7,64			# 0x40
move	$4,$18
ori	$6,$6,0xfefe
$L1872:
addiu	$2,$10,3
addiu	$11,$10,4
addiu	$3,$10,7
addu	$5,$16,$10
addu	$2,$16,$2
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($2)  
lwr $12, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 72($2)  
lwr $8, 72($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$9,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($3)  
lwr $5, 0($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 72($3)  
lwr $2, 72($11)  

# 0 "" 2
#NO_APP
xor	$3,$2,$5
and	$9,$9,$6
and	$3,$3,$6
srl	$9,$9,1
srl	$3,$3,1
and	$8,$8,$12
and	$2,$2,$5
addu	$8,$9,$8
addu	$2,$3,$2
addiu	$10,$10,8
sw	$8,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$10,$7,$L1872
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,184
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc21_c
.size	put_no_rnd_qpel8_mc21_c, .-put_no_rnd_qpel8_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc23_c
.type	put_no_rnd_qpel8_mc23_c, @function
put_no_rnd_qpel8_mc23_c:
.frame	$sp,184,$31		# vars= 136, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-184
li	$2,9			# 0x9
sw	$17,172($sp)
addiu	$17,$sp,32
sw	$18,176($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$2,16($sp)
sw	$16,168($sp)
move	$7,$18
move	$16,$4
sw	$31,180($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$17
.set	macro
.set	reorder

addiu	$4,$sp,104
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$5,64			# 0x40
move	$10,$16
ori	$4,$4,0xfefe
$L1876:
addiu	$11,$2,8
addu	$2,$17,$2
addu	$3,$17,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 75($2)  
lwr $8, 72($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 11($2)  
lwr $13, 0($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 15($2)  
lwr $12, 12($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 79($2)  
lwr $3, 76($2)  

# 0 "" 2
#NO_APP
xor	$9,$8,$13
xor	$2,$3,$12
and	$9,$9,$4
and	$2,$2,$4
srl	$2,$2,1
srl	$9,$9,1
and	$8,$8,$13
and	$3,$3,$12
addu	$3,$2,$3
addu	$8,$9,$8
move	$2,$11
sw	$3,4($10)
sw	$8,0($10)
.set	noreorder
.set	nomacro
bne	$11,$5,$L1876
addu	$10,$10,$18
.set	macro
.set	reorder

lw	$31,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,184
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc23_c
.size	put_no_rnd_qpel8_mc23_c, .-put_no_rnd_qpel8_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc32_c
.type	put_no_rnd_qpel8_mc32_c, @function
put_no_rnd_qpel8_mc32_c:
.frame	$sp,272,$31		# vars= 216, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$19,264($sp)
addiu	$19,$sp,32
sw	$18,260($sp)
move	$18,$4
move	$4,$19
sw	$31,268($sp)
sw	$17,256($sp)
move	$17,$6
sw	$16,252($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
addiu	$16,$sp,176
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$10,$sp,36
addiu	$4,$sp,248
move	$2,$16
ori	$13,$13,0xfefe
$L1880:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($10)  
lwr $8, -3($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($10)  
lwr $3, 1($10)  

# 0 "" 2
#NO_APP
xor	$9,$8,$12
xor	$7,$3,$11
and	$9,$9,$13
and	$7,$7,$13
srl	$9,$9,1
srl	$7,$7,1
and	$8,$8,$12
and	$3,$3,$11
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,8
addiu	$10,$10,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1880
sw	$3,-4($2)
.set	macro
.set	reorder

li	$7,8			# 0x8
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc32_c
.size	put_no_rnd_qpel8_mc32_c, .-put_no_rnd_qpel8_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc12_c
.type	put_no_rnd_qpel8_mc12_c, @function
put_no_rnd_qpel8_mc12_c:
.frame	$sp,272,$31		# vars= 216, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$16,252($sp)
addiu	$16,$sp,32
sw	$19,264($sp)
move	$19,$4
move	$4,$16
sw	$31,268($sp)
sw	$18,260($sp)
move	$18,$6
sw	$17,256($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
addiu	$17,$sp,176
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,248
move	$2,$17
move	$10,$16
ori	$13,$13,0xfefe
$L1884:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($10)  
lwr $8, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($10)  
lwr $3, 4($10)  

# 0 "" 2
#NO_APP
xor	$9,$8,$12
xor	$7,$3,$11
and	$9,$9,$13
and	$7,$7,$13
srl	$9,$9,1
srl	$7,$7,1
and	$8,$8,$12
and	$3,$3,$11
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,8
addiu	$10,$10,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1884
sw	$3,-4($2)
.set	macro
.set	reorder

li	$7,8			# 0x8
move	$4,$19
move	$5,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$18
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc12_c
.size	put_no_rnd_qpel8_mc12_c, .-put_no_rnd_qpel8_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc10_c
.type	put_h264_qpel2_mc10_c, @function
put_h264_qpel2_mc10_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-56
addu	$10,$5,$6
sw	$17,24($sp)
lui	$11,%hi(ff_cropTbl+1024)
sw	$16,20($sp)
addu	$6,$4,$6
sw	$fp,52($sp)
addiu	$11,$11,%lo(ff_cropTbl+1024)
sw	$23,48($sp)
sw	$22,44($sp)
sw	$21,40($sp)
sw	$20,36($sp)
sw	$19,32($sp)
sw	$18,28($sp)
lbu	$14,0($5)
lbu	$7,1($5)
lbu	$21,-1($5)
lbu	$2,2($5)
lbu	$23,3($5)
addu	$16,$14,$7
lbu	$22,-2($5)
sll	$12,$7,8
addu	$8,$21,$2
lbu	$fp,4($5)
addu	$5,$7,$2
lbu	$18,1($10)
addu	$7,$14,$23
lbu	$20,2($10)
sll	$2,$16,2
lbu	$19,0($10)
sll	$9,$8,2
lbu	$13,-1($10)
sll	$16,$16,4
lbu	$25,3($10)
sll	$17,$5,2
lbu	$15,-2($10)
addu	$9,$9,$8
lbu	$24,4($10)
sll	$3,$7,2
addu	$16,$2,$16
sll	$5,$5,4
subu	$2,$16,$9
addu	$5,$17,$5
addu	$9,$22,$23
addu	$3,$3,$7
addu	$8,$21,$fp
subu	$3,$5,$3
addu	$9,$2,$9
addu	$8,$3,$8
addiu	$9,$9,16
addiu	$8,$8,16
sra	$9,$9,5
sra	$8,$8,5
addu	$9,$11,$9
addu	$8,$11,$8
addu	$17,$19,$18
lbu	$5,0($9)
addu	$2,$13,$20
addu	$16,$18,$20
lbu	$9,0($8)
addu	$19,$19,$25
sll	$7,$17,2
sll	$8,$16,2
sb	$5,8($sp)
sll	$17,$17,4
sb	$9,9($sp)
sll	$3,$2,2
lhu	$9,8($sp)
sll	$16,$16,4
sll	$18,$19,2
addu	$5,$7,$17
addu	$19,$18,$19
addu	$7,$8,$16
addu	$2,$3,$2
addu	$13,$13,$24
subu	$3,$5,$2
subu	$5,$7,$19
addu	$2,$15,$25
li	$7,-16908288			# 0xfffffffffefe0000
addu	$2,$3,$2
addu	$3,$5,$13
addiu	$2,$2,16
addiu	$3,$3,16
or	$12,$12,$14
sra	$2,$2,5
sra	$3,$3,5
ori	$7,$7,0xfefe
xor	$5,$9,$12
addu	$2,$11,$2
addu	$3,$11,$3
and	$5,$5,$7
srl	$5,$5,1
or	$9,$9,$12
subu	$9,$9,$5
lbu	$5,0($2)
lbu	$2,0($3)
sh	$9,0($4)
sb	$5,10($sp)
sb	$2,11($sp)
lbu	$3,1($10)
lbu	$4,0($10)
lhu	$2,10($sp)
sll	$3,$3,8
lw	$fp,52($sp)
lw	$23,48($sp)
or	$3,$3,$4
lw	$22,44($sp)
xor	$4,$2,$3
lw	$21,40($sp)
and	$4,$4,$7
lw	$20,36($sp)
srl	$4,$4,1
lw	$19,32($sp)
or	$2,$2,$3
lw	$18,28($sp)
subu	$2,$2,$4
lw	$17,24($sp)
lw	$16,20($sp)
sh	$2,0($6)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_h264_qpel2_mc10_c
.size	put_h264_qpel2_mc10_c, .-put_h264_qpel2_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc30_c
.type	put_h264_qpel2_mc30_c, @function
put_h264_qpel2_mc30_c:
.frame	$sp,56,$31		# vars= 8, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-56
addu	$10,$5,$6
sw	$fp,52($sp)
lui	$11,%hi(ff_cropTbl+1024)
sw	$16,20($sp)
addu	$6,$4,$6
sw	$23,48($sp)
addiu	$11,$11,%lo(ff_cropTbl+1024)
sw	$22,44($sp)
sw	$21,40($sp)
sw	$20,36($sp)
sw	$19,32($sp)
sw	$18,28($sp)
sw	$17,24($sp)
lbu	$13,1($5)
lbu	$12,2($5)
lbu	$21,-1($5)
lbu	$19,0($5)
lbu	$23,3($5)
addu	$3,$13,$12
addu	$fp,$21,$12
lbu	$22,-2($5)
addu	$16,$19,$13
lbu	$8,4($5)
addu	$19,$19,$23
lbu	$17,1($10)
sll	$2,$16,2
lbu	$20,2($10)
sll	$9,$fp,2
lbu	$18,0($10)
sll	$16,$16,4
lbu	$14,-1($10)
sll	$7,$3,2
lbu	$25,3($10)
addu	$9,$9,$fp
lbu	$15,-2($10)
sll	$5,$3,4
lbu	$24,4($10)
sll	$3,$19,2
addu	$16,$2,$16
addu	$5,$7,$5
subu	$2,$16,$9
addu	$3,$3,$19
addu	$9,$22,$23
subu	$3,$5,$3
addu	$8,$21,$8
addu	$9,$2,$9
addu	$8,$3,$8
addiu	$9,$9,16
addiu	$8,$8,16
sra	$9,$9,5
sra	$8,$8,5
addu	$9,$11,$9
addu	$8,$11,$8
addu	$19,$18,$17
lbu	$5,0($9)
addu	$2,$14,$20
addu	$16,$17,$20
lbu	$9,0($8)
addu	$18,$18,$25
sll	$7,$19,2
sll	$8,$16,2
sb	$5,8($sp)
sll	$19,$19,4
sb	$9,9($sp)
sll	$3,$2,2
lhu	$9,8($sp)
sll	$16,$16,4
sll	$17,$18,2
addu	$5,$7,$19
addu	$18,$17,$18
addu	$7,$8,$16
addu	$2,$3,$2
addu	$14,$14,$24
subu	$3,$5,$2
subu	$5,$7,$18
addu	$2,$15,$25
sll	$12,$12,8
addu	$2,$3,$2
addu	$3,$5,$14
addiu	$2,$2,16
addiu	$3,$3,16
li	$5,-16908288			# 0xfffffffffefe0000
sra	$2,$2,5
sra	$3,$3,5
or	$13,$12,$13
ori	$5,$5,0xfefe
xor	$12,$9,$13
addu	$2,$11,$2
addu	$3,$11,$3
and	$12,$12,$5
srl	$12,$12,1
lbu	$7,0($2)
or	$13,$9,$13
lbu	$2,0($3)
subu	$13,$13,$12
sb	$7,10($sp)
sh	$13,0($4)
sb	$2,11($sp)
lbu	$3,2($10)
lbu	$4,1($10)
lhu	$2,10($sp)
sll	$3,$3,8
lw	$fp,52($sp)
lw	$23,48($sp)
or	$3,$3,$4
lw	$22,44($sp)
xor	$4,$2,$3
lw	$21,40($sp)
and	$4,$4,$5
lw	$20,36($sp)
srl	$4,$4,1
lw	$19,32($sp)
or	$2,$2,$3
lw	$18,28($sp)
subu	$2,$2,$4
lw	$17,24($sp)
lw	$16,20($sp)
sh	$2,0($6)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	put_h264_qpel2_mc30_c
.size	put_h264_qpel2_mc30_c, .-put_h264_qpel2_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc10_c
.type	put_h264_qpel8_mc10_c, @function
put_h264_qpel8_mc10_c:
.frame	$sp,112,$31		# vars= 64, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
sw	$19,104($sp)
addiu	$19,$sp,24
sw	$18,100($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$17,96($sp)
move	$7,$18
sw	$16,92($sp)
move	$17,$4
sw	$31,108($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,88
move	$10,$0
move	$2,$19
ori	$4,$4,0xfefe
$L1892:
addu	$11,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($11)  
lwr $8, 0($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$4
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$5,$17,$10
addiu	$9,$10,4
sw	$8,0($5)
addu	$10,$10,$18
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
#NO_APP
addu	$5,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 7($11)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
xor	$8,$3,$7
and	$8,$8,$4
srl	$8,$8,1
or	$3,$3,$7
subu	$3,$3,$8
addu	$9,$17,$9
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$6,$L1892
sw	$3,0($9)
.set	macro
.set	reorder

lw	$31,108($sp)
lw	$19,104($sp)
lw	$18,100($sp)
lw	$17,96($sp)
lw	$16,92($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	put_h264_qpel8_mc10_c
.size	put_h264_qpel8_mc10_c, .-put_h264_qpel8_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc30_c
.type	put_h264_qpel8_mc30_c, @function
put_h264_qpel8_mc30_c:
.frame	$sp,112,$31		# vars= 64, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
sw	$19,104($sp)
addiu	$19,$sp,24
sw	$18,100($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$17,96($sp)
move	$7,$18
sw	$16,92($sp)
move	$17,$4
sw	$31,108($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,88
move	$10,$0
move	$2,$19
ori	$4,$4,0xfefe
$L1896:
addiu	$11,$10,4
addu	$12,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$5,$16,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($5)  
lwr $8, 1($12)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$4
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$5,$17,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
#NO_APP
sw	$8,0($5)
addu	$11,$17,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 8($12)  
lwr $9, 5($12)  

# 0 "" 2
#NO_APP
xor	$8,$3,$9
and	$8,$8,$4
srl	$8,$8,1
or	$3,$3,$9
subu	$3,$3,$8
addiu	$2,$2,8
addu	$10,$10,$18
.set	noreorder
.set	nomacro
bne	$2,$6,$L1896
sw	$3,0($11)
.set	macro
.set	reorder

lw	$31,108($sp)
lw	$19,104($sp)
lw	$18,100($sp)
lw	$17,96($sp)
lw	$16,92($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	put_h264_qpel8_mc30_c
.size	put_h264_qpel8_mc30_c, .-put_h264_qpel8_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc30_c
.type	put_qpel8_mc30_c, @function
put_qpel8_mc30_c:
.frame	$sp,120,$31		# vars= 64, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
li	$2,8			# 0x8
sw	$19,112($sp)
addiu	$19,$sp,32
sw	$18,108($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$17,104($sp)
sw	$2,16($sp)
move	$17,$4
move	$7,$18
sw	$16,100($sp)
move	$4,$19
sw	$31,116($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,96
move	$10,$0
move	$2,$19
ori	$4,$4,0xfefe
$L1900:
addiu	$11,$10,4
addu	$12,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$5,$16,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($5)  
lwr $8, 1($12)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$4
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$5,$17,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
#NO_APP
sw	$8,0($5)
addu	$11,$17,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 8($12)  
lwr $9, 5($12)  

# 0 "" 2
#NO_APP
xor	$8,$3,$9
and	$8,$8,$4
srl	$8,$8,1
or	$3,$3,$9
subu	$3,$3,$8
addiu	$2,$2,8
addu	$10,$10,$18
.set	noreorder
.set	nomacro
bne	$2,$6,$L1900
sw	$3,0($11)
.set	macro
.set	reorder

lw	$31,116($sp)
lw	$19,112($sp)
lw	$18,108($sp)
lw	$17,104($sp)
lw	$16,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	put_qpel8_mc30_c
.size	put_qpel8_mc30_c, .-put_qpel8_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc30_c
.type	put_mspel8_mc30_c, @function
put_mspel8_mc30_c:
.frame	$sp,112,$31		# vars= 64, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
li	$7,8			# 0x8
sw	$19,104($sp)
addiu	$19,$sp,24
sw	$17,96($sp)
move	$17,$4
move	$4,$19
sw	$18,100($sp)
sw	$16,92($sp)
move	$18,$6
sw	$31,108($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_h_lowpass.constprop.10
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,88
move	$9,$0
move	$2,$19
ori	$4,$4,0xfefe
$L1904:
addiu	$10,$9,4
addu	$11,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
addu	$5,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($5)  
lwr $7, 1($11)  

# 0 "" 2
#NO_APP
xor	$8,$3,$7
and	$8,$8,$4
srl	$8,$8,1
or	$3,$3,$7
subu	$7,$3,$8
addu	$5,$17,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
#NO_APP
sw	$7,0($5)
addu	$10,$17,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 8($11)  
lwr $8, 5($11)  

# 0 "" 2
#NO_APP
xor	$7,$3,$8
and	$7,$7,$4
srl	$7,$7,1
or	$3,$3,$8
subu	$3,$3,$7
addiu	$2,$2,8
addu	$9,$9,$18
.set	noreorder
.set	nomacro
bne	$2,$6,$L1904
sw	$3,0($10)
.set	macro
.set	reorder

lw	$31,108($sp)
lw	$19,104($sp)
lw	$18,100($sp)
lw	$17,96($sp)
lw	$16,92($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	put_mspel8_mc30_c
.size	put_mspel8_mc30_c, .-put_mspel8_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc10_c
.type	put_qpel8_mc10_c, @function
put_qpel8_mc10_c:
.frame	$sp,120,$31		# vars= 64, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-120
li	$2,8			# 0x8
sw	$19,112($sp)
addiu	$19,$sp,32
sw	$18,108($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$17,104($sp)
sw	$2,16($sp)
move	$17,$4
move	$7,$18
sw	$16,100($sp)
move	$4,$19
sw	$31,116($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,96
move	$10,$0
move	$2,$19
ori	$4,$4,0xfefe
$L1908:
addu	$11,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($11)  
lwr $8, 0($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$4
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$5,$17,$10
addiu	$9,$10,4
sw	$8,0($5)
addu	$10,$10,$18
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
#NO_APP
addu	$5,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 7($11)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
xor	$8,$3,$7
and	$8,$8,$4
srl	$8,$8,1
or	$3,$3,$7
subu	$3,$3,$8
addu	$9,$17,$9
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$6,$L1908
sw	$3,0($9)
.set	macro
.set	reorder

lw	$31,116($sp)
lw	$19,112($sp)
lw	$18,108($sp)
lw	$17,104($sp)
lw	$16,100($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,120
.set	macro
.set	reorder

.end	put_qpel8_mc10_c
.size	put_qpel8_mc10_c, .-put_qpel8_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc10_c
.type	put_mspel8_mc10_c, @function
put_mspel8_mc10_c:
.frame	$sp,112,$31		# vars= 64, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-112
li	$7,8			# 0x8
sw	$19,104($sp)
addiu	$19,$sp,24
sw	$17,96($sp)
move	$17,$4
move	$4,$19
sw	$18,100($sp)
sw	$16,92($sp)
move	$18,$6
sw	$31,108($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_h_lowpass.constprop.10
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,88
move	$9,$0
move	$2,$19
ori	$4,$4,0xfefe
$L1912:
addu	$10,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($10)  
lwr $7, 0($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$7
and	$8,$8,$4
srl	$8,$8,1
or	$3,$3,$7
subu	$7,$3,$8
addu	$5,$17,$9
addiu	$8,$9,4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($2)  
lwr $3, 4($2)  

# 0 "" 2
#NO_APP
sw	$7,0($5)
addu	$5,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 7($10)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
move	$10,$7
xor	$7,$3,$7
and	$7,$7,$4
srl	$7,$7,1
or	$3,$3,$10
subu	$3,$3,$7
addu	$8,$17,$8
addiu	$2,$2,8
sw	$3,0($8)
.set	noreorder
.set	nomacro
bne	$2,$6,$L1912
addu	$9,$9,$18
.set	macro
.set	reorder

lw	$31,108($sp)
lw	$19,104($sp)
lw	$18,100($sp)
lw	$17,96($sp)
lw	$16,92($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,112
.set	macro
.set	reorder

.end	put_mspel8_mc10_c
.size	put_mspel8_mc10_c, .-put_mspel8_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc03_c
.type	put_qpel8_mc03_c, @function
put_qpel8_mc03_c:
.frame	$sp,256,$31		# vars= 208, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-256
sw	$19,248($sp)
addiu	$19,$sp,24
sw	$17,240($sp)
move	$17,$4
sw	$16,236($sp)
move	$4,$19
addiu	$16,$sp,168
sw	$31,252($sp)
sw	$18,244($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$18,$6
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,43
addiu	$6,$sp,171
move	$7,$16
move	$4,$17
ori	$12,$12,0xfefe
$L1916:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 0($2)  
lwr $11, -3($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($7)  
lwr $5, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 4($2)  
lwr $10, 1($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($7)  
lwr $3, 4($7)  

# 0 "" 2
#NO_APP
xor	$9,$5,$11
xor	$8,$3,$10
and	$9,$9,$12
and	$8,$8,$12
srl	$9,$9,1
srl	$8,$8,1
or	$5,$5,$11
or	$3,$3,$10
subu	$5,$5,$9
subu	$3,$3,$8
addiu	$2,$2,16
sw	$5,0($4)
addiu	$7,$7,8
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$6,$L1916
addu	$4,$4,$18
.set	macro
.set	reorder

lw	$31,252($sp)
lw	$19,248($sp)
lw	$18,244($sp)
lw	$17,240($sp)
lw	$16,236($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,256
.set	macro
.set	reorder

.end	put_qpel8_mc03_c
.size	put_qpel8_mc03_c, .-put_qpel8_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc01_c
.type	put_qpel8_mc01_c, @function
put_qpel8_mc01_c:
.frame	$sp,256,$31		# vars= 208, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-256
sw	$16,236($sp)
addiu	$16,$sp,24
sw	$18,244($sp)
move	$18,$4
sw	$17,240($sp)
move	$4,$16
addiu	$17,$sp,168
sw	$31,252($sp)
sw	$19,248($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$19,$6
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$12,$sp,152
move	$5,$16
move	$7,$17
move	$4,$18
ori	$6,$6,0xfefe
$L1920:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($5)  
lwr $11, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($7)  
lwr $3, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($5)  
lwr $10, 4($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 7($7)  
lwr $2, 4($7)  

# 0 "" 2
#NO_APP
xor	$9,$3,$11
xor	$8,$2,$10
and	$9,$9,$6
and	$8,$8,$6
srl	$9,$9,1
srl	$8,$8,1
or	$3,$3,$11
or	$2,$2,$10
subu	$3,$3,$9
subu	$2,$2,$8
addiu	$5,$5,16
sw	$3,0($4)
addiu	$7,$7,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$12,$L1920
addu	$4,$4,$19
.set	macro
.set	reorder

lw	$31,252($sp)
lw	$19,248($sp)
lw	$18,244($sp)
lw	$17,240($sp)
lw	$16,236($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,256
.set	macro
.set	reorder

.end	put_qpel8_mc01_c
.size	put_qpel8_mc01_c, .-put_qpel8_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc21_c
.type	put_qpel8_mc21_c, @function
put_qpel8_mc21_c:
.frame	$sp,184,$31		# vars= 136, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-184
li	$2,9			# 0x9
sw	$16,168($sp)
addiu	$16,$sp,32
sw	$17,172($sp)
move	$17,$6
li	$6,8			# 0x8
sw	$2,16($sp)
sw	$18,176($sp)
move	$7,$17
move	$18,$4
sw	$31,180($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$sp,104
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$7,64			# 0x40
move	$4,$18
ori	$6,$6,0xfefe
$L1924:
addiu	$3,$2,3
addiu	$11,$2,4
addiu	$9,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($3)  
lwr $12, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 72($3)  
lwr $8, 72($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($9)  
lwr $5, 0($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 72($9)  
lwr $3, 72($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L1924
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,184
.set	macro
.set	reorder

.end	put_qpel8_mc21_c
.size	put_qpel8_mc21_c, .-put_qpel8_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc21_c
.type	put_h264_qpel8_mc21_c, @function
put_h264_qpel8_mc21_c:
.frame	$sp,384,$31		# vars= 336, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-384
sw	$18,372($sp)
move	$18,$4
sw	$17,368($sp)
addiu	$4,$sp,296
move	$17,$6
sw	$16,364($sp)
li	$6,8			# 0x8
sw	$31,380($sp)
move	$7,$17
sw	$19,376($sp)
addiu	$16,$sp,24
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
move	$19,$5
.set	macro
.set	reorder

addiu	$4,$sp,232
move	$6,$19
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$7,64			# 0x40
move	$4,$18
ori	$6,$6,0xfefe
$L1928:
addiu	$3,$2,3
addiu	$11,$2,4
addiu	$9,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 272($3)  
lwr $12, 272($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 208($3)  
lwr $8, 208($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 272($9)  
lwr $5, 272($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 208($9)  
lwr $3, 208($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L1928
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,380($sp)
lw	$19,376($sp)
lw	$18,372($sp)
lw	$17,368($sp)
lw	$16,364($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,384
.set	macro
.set	reorder

.end	put_h264_qpel8_mc21_c
.size	put_h264_qpel8_mc21_c, .-put_h264_qpel8_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc23_c
.type	put_qpel8_mc23_c, @function
put_qpel8_mc23_c:
.frame	$sp,184,$31		# vars= 136, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-184
li	$2,9			# 0x9
sw	$17,172($sp)
addiu	$17,$sp,32
sw	$18,176($sp)
move	$18,$6
li	$6,8			# 0x8
sw	$2,16($sp)
sw	$16,168($sp)
move	$7,$18
move	$16,$4
sw	$31,180($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$17
.set	macro
.set	reorder

addiu	$4,$sp,104
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$5,64			# 0x40
move	$11,$16
ori	$4,$4,0xfefe
$L1932:
addiu	$12,$2,8
addu	$2,$17,$2
addu	$3,$17,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 75($2)  
lwr $8, 72($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 11($2)  
lwr $13, 0($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 79($2)  
lwr $6, 76($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 15($2)  
lwr $3, 12($2)  

# 0 "" 2
#NO_APP
xor	$10,$8,$13
xor	$9,$6,$3
and	$10,$10,$4
and	$9,$9,$4
srl	$10,$10,1
srl	$9,$9,1
or	$2,$6,$3
or	$8,$8,$13
subu	$3,$2,$9
subu	$8,$8,$10
move	$2,$12
sw	$3,4($11)
sw	$8,0($11)
.set	noreorder
.set	nomacro
bne	$12,$5,$L1932
addu	$11,$11,$18
.set	macro
.set	reorder

lw	$31,180($sp)
lw	$18,176($sp)
lw	$17,172($sp)
lw	$16,168($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,184
.set	macro
.set	reorder

.end	put_qpel8_mc23_c
.size	put_qpel8_mc23_c, .-put_qpel8_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel8_mc23_c
.type	put_h264_qpel8_mc23_c, @function
put_h264_qpel8_mc23_c:
.frame	$sp,384,$31		# vars= 336, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-384
sw	$19,376($sp)
move	$19,$4
sw	$18,372($sp)
addiu	$4,$sp,296
sw	$17,368($sp)
move	$18,$5
move	$17,$6
sw	$16,364($sp)
addu	$5,$5,$6
sw	$31,380($sp)
li	$6,8			# 0x8
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_h_lowpass
.option	pic2
addiu	$16,$sp,24
.set	macro
.set	reorder

addiu	$4,$sp,232
move	$6,$18
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel8_hv_lowpass.constprop.26
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$7,64			# 0x40
move	$4,$19
ori	$6,$6,0xfefe
$L1936:
addiu	$3,$2,3
addiu	$11,$2,4
addiu	$9,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 272($3)  
lwr $12, 272($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 208($3)  
lwr $8, 208($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 272($9)  
lwr $5, 272($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 208($9)  
lwr $3, 208($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L1936
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,380($sp)
lw	$19,376($sp)
lw	$18,372($sp)
lw	$17,368($sp)
lw	$16,364($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,384
.set	macro
.set	reorder

.end	put_h264_qpel8_mc23_c
.size	put_h264_qpel8_mc23_c, .-put_h264_qpel8_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc30_c
.type	put_h264_qpel16_mc30_c, @function
put_h264_qpel16_mc30_c:
.frame	$sp,320,$31		# vars= 256, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-320
sw	$19,312($sp)
addiu	$19,$sp,40
sw	$17,304($sp)
move	$17,$4
move	$4,$19
sw	$31,316($sp)
sw	$18,308($sp)
move	$18,$5
sw	$16,300($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$5,$18,1
sw	$16,16($sp)
move	$4,$17
sw	$2,20($sp)
move	$6,$19
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$16
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$5,$18,9
addiu	$2,$sp,51
addiu	$4,$sp,307
move	$11,$0
ori	$12,$12,0xfefe
$L1940:
addu	$10,$5,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($2)  
lwr $3, -3($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($10)  
lwr $8, 0($10)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$12
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$9,$17,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($2)  
lwr $3, 1($2)  

# 0 "" 2
#NO_APP
sw	$8,8($9)
addiu	$2,$2,16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($10)  
lwr $6, 4($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$6
and	$8,$8,$12
srl	$8,$8,1
or	$3,$3,$6
subu	$3,$3,$8
addu	$11,$11,$16
.set	noreorder
.set	nomacro
bne	$2,$4,$L1940
sw	$3,12($9)
.set	macro
.set	reorder

lw	$31,316($sp)
lw	$19,312($sp)
lw	$18,308($sp)
lw	$17,304($sp)
lw	$16,300($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,320
.set	macro
.set	reorder

.end	put_h264_qpel16_mc30_c
.size	put_h264_qpel16_mc30_c, .-put_h264_qpel16_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc10_c
.type	put_qpel16_mc10_c, @function
put_qpel16_mc10_c:
.frame	$sp,320,$31		# vars= 256, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-320
sw	$20,312($sp)
li	$20,16			# 0x10
sw	$19,308($sp)
addiu	$19,$sp,40
sw	$18,304($sp)
move	$18,$6
li	$6,16			# 0x10
sw	$17,300($sp)
sw	$16,296($sp)
move	$17,$4
move	$16,$5
sw	$20,16($sp)
move	$4,$19
sw	$31,316($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$7,$18
.set	macro
.set	reorder

move	$5,$16
move	$6,$19
sw	$18,16($sp)
move	$4,$17
sw	$20,20($sp)
sw	$20,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$18
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,51
addiu	$6,$sp,307
move	$10,$0
ori	$5,$5,0xfefe
$L1944:
addiu	$11,$10,8
addu	$4,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($2)  
lwr $3, -3($2)  

# 0 "" 2
#NO_APP
addu	$7,$16,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 11($4)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$5
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$11,$17,$11
addiu	$9,$10,12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($2)  
lwr $3, 1($2)  

# 0 "" 2
#NO_APP
sw	$8,0($11)
addu	$7,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 15($4)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
move	$4,$8
xor	$8,$3,$8
and	$8,$8,$5
srl	$8,$8,1
or	$3,$3,$4
subu	$3,$3,$8
addu	$9,$17,$9
addiu	$2,$2,16
sw	$3,0($9)
.set	noreorder
.set	nomacro
bne	$2,$6,$L1944
addu	$10,$10,$18
.set	macro
.set	reorder

lw	$31,316($sp)
lw	$20,312($sp)
lw	$19,308($sp)
lw	$18,304($sp)
lw	$17,300($sp)
lw	$16,296($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,320
.set	macro
.set	reorder

.end	put_qpel16_mc10_c
.size	put_qpel16_mc10_c, .-put_qpel16_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc10_c
.type	put_h264_qpel16_mc10_c, @function
put_h264_qpel16_mc10_c:
.frame	$sp,320,$31		# vars= 256, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-320
sw	$19,312($sp)
addiu	$19,$sp,40
sw	$17,304($sp)
move	$17,$4
move	$4,$19
sw	$31,316($sp)
sw	$18,308($sp)
move	$18,$6
sw	$16,300($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$5,$16
sw	$18,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$4,$17
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$18
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,51
addiu	$6,$sp,307
move	$10,$0
ori	$5,$5,0xfefe
$L1948:
addiu	$11,$10,8
addu	$4,$16,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($2)  
lwr $3, -3($2)  

# 0 "" 2
#NO_APP
addu	$7,$16,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 11($4)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$5
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$11,$17,$11
addiu	$9,$10,12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($2)  
lwr $3, 1($2)  

# 0 "" 2
#NO_APP
sw	$8,0($11)
addu	$7,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 15($4)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
move	$4,$8
xor	$8,$3,$8
and	$8,$8,$5
srl	$8,$8,1
or	$3,$3,$4
subu	$3,$3,$8
addu	$9,$17,$9
addiu	$2,$2,16
sw	$3,0($9)
.set	noreorder
.set	nomacro
bne	$2,$6,$L1948
addu	$10,$10,$18
.set	macro
.set	reorder

lw	$31,316($sp)
lw	$19,312($sp)
lw	$18,308($sp)
lw	$17,304($sp)
lw	$16,300($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,320
.set	macro
.set	reorder

.end	put_h264_qpel16_mc10_c
.size	put_h264_qpel16_mc10_c, .-put_h264_qpel16_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc30_c
.type	put_qpel16_mc30_c, @function
put_qpel16_mc30_c:
.frame	$sp,320,$31		# vars= 256, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-320
sw	$20,312($sp)
li	$20,16			# 0x10
sw	$19,308($sp)
addiu	$19,$sp,40
sw	$16,296($sp)
move	$16,$6
li	$6,16			# 0x10
sw	$18,304($sp)
sw	$17,300($sp)
move	$18,$5
move	$17,$4
sw	$20,16($sp)
move	$7,$16
sw	$31,316($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

addiu	$5,$18,1
move	$4,$17
sw	$16,16($sp)
sw	$20,20($sp)
move	$6,$19
sw	$20,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$16
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$5,$18,9
addiu	$2,$sp,51
addiu	$4,$sp,307
move	$11,$0
ori	$12,$12,0xfefe
$L1952:
addu	$10,$5,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($2)  
lwr $3, -3($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($10)  
lwr $8, 0($10)  

# 0 "" 2
#NO_APP
xor	$9,$3,$8
and	$9,$9,$12
srl	$9,$9,1
or	$3,$3,$8
subu	$8,$3,$9
addu	$9,$17,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($2)  
lwr $3, 1($2)  

# 0 "" 2
#NO_APP
sw	$8,8($9)
addiu	$2,$2,16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($10)  
lwr $6, 4($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$6
and	$8,$8,$12
srl	$8,$8,1
or	$3,$3,$6
subu	$3,$3,$8
addu	$11,$11,$16
.set	noreorder
.set	nomacro
bne	$2,$4,$L1952
sw	$3,12($9)
.set	macro
.set	reorder

lw	$31,316($sp)
lw	$20,312($sp)
lw	$19,308($sp)
lw	$18,304($sp)
lw	$17,300($sp)
lw	$16,296($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,320
.set	macro
.set	reorder

.end	put_qpel16_mc30_c
.size	put_qpel16_mc30_c, .-put_qpel16_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc12_c
.type	put_mspel8_mc12_c, @function
put_mspel8_mc12_c:
.frame	$sp,264,$31		# vars= 216, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-264
li	$7,11			# 0xb
sw	$16,244($sp)
addiu	$16,$sp,24
sw	$18,252($sp)
move	$18,$5
subu	$5,$5,$6
sw	$19,256($sp)
move	$19,$4
sw	$31,260($sp)
move	$4,$16
sw	$17,248($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_h_lowpass.constprop.10
.option	pic2
move	$17,$6
.set	macro
.set	reorder

addiu	$4,$sp,176
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_v_lowpass.constprop.3
.option	pic2
move	$6,$17
.set	macro
.set	reorder

addiu	$4,$sp,112
addiu	$5,$sp,32
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_v_lowpass.constprop.3
.option	pic2
li	$6,8			# 0x8
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$11,64			# 0x40
move	$4,$19
ori	$10,$10,0xfefe
$L1956:
addiu	$3,$2,3
addiu	$9,$2,4
addiu	$7,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$7,$16,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 152($3)  
lwr $12, 152($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 88($3)  
lwr $6, 88($5)  

# 0 "" 2
#NO_APP
addu	$9,$16,$9
move	$5,$6
xor	$8,$6,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 88($7)  
lwr $3, 88($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 152($7)  
lwr $6, 152($9)  

# 0 "" 2
#NO_APP
xor	$7,$3,$6
and	$8,$8,$10
and	$7,$7,$10
srl	$8,$8,1
srl	$7,$7,1
or	$5,$5,$12
or	$3,$3,$6
subu	$5,$5,$8
subu	$3,$3,$7
addiu	$2,$2,8
sw	$5,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L1956
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,260($sp)
lw	$19,256($sp)
lw	$18,252($sp)
lw	$17,248($sp)
lw	$16,244($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

.end	put_mspel8_mc12_c
.size	put_mspel8_mc12_c, .-put_mspel8_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc12_c
.type	avg_qpel8_mc12_c, @function
avg_qpel8_mc12_c:
.frame	$sp,272,$31		# vars= 216, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$16,252($sp)
addiu	$16,$sp,32
sw	$19,264($sp)
move	$19,$4
move	$4,$16
sw	$31,268($sp)
sw	$18,260($sp)
move	$18,$6
sw	$17,256($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
addiu	$17,$sp,176
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,248
move	$2,$17
move	$8,$16
ori	$13,$13,0xfefe
$L1960:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($8)  
lwr $7, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($8)  
lwr $3, 4($8)  

# 0 "" 2
#NO_APP
xor	$10,$7,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$7,$7,$12
or	$3,$3,$11
subu	$7,$7,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$8,$8,16
sw	$7,-8($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1960
sw	$3,-4($2)
.set	macro
.set	reorder

li	$7,8			# 0x8
move	$4,$19
move	$5,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$18
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	avg_qpel8_mc12_c
.size	avg_qpel8_mc12_c, .-avg_qpel8_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc32_c
.type	avg_qpel8_mc32_c, @function
avg_qpel8_mc32_c:
.frame	$sp,272,$31		# vars= 216, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$19,264($sp)
addiu	$19,$sp,32
sw	$18,260($sp)
move	$18,$4
move	$4,$19
sw	$31,268($sp)
sw	$17,256($sp)
move	$17,$6
sw	$16,252($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
addiu	$16,$sp,176
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,36
addiu	$4,$sp,248
move	$2,$16
ori	$13,$13,0xfefe
$L1964:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($8)  
lwr $7, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$10,$7,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$7,$7,$12
or	$3,$3,$11
subu	$7,$7,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$8,$8,16
sw	$7,-8($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1964
sw	$3,-4($2)
.set	macro
.set	reorder

li	$7,8			# 0x8
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	avg_qpel8_mc32_c
.size	avg_qpel8_mc32_c, .-avg_qpel8_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc23_c
.type	put_qpel16_mc23_c, @function
put_qpel16_mc23_c:
.frame	$sp,592,$31		# vars= 528, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-592
li	$2,17			# 0x11
sw	$18,580($sp)
addiu	$18,$sp,40
sw	$17,576($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$2,16($sp)
sw	$19,584($sp)
move	$7,$17
sw	$16,572($sp)
addiu	$19,$sp,312
move	$16,$4
sw	$31,588($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$18
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$5,$sp,56
move	$4,$16
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$14,-16908288			# 0xfffffffffefe0000
addiu	$10,$16,8
move	$11,$0
li	$4,256			# 0x100
ori	$14,$14,0xfefe
$L1968:
addu	$2,$18,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 27($2)  
lwr $13, 24($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 283($2)  
lwr $3, 280($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 31($2)  
lwr $12, 28($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 287($2)  
lwr $5, 284($2)  

# 0 "" 2
#NO_APP
xor	$9,$3,$13
xor	$8,$5,$12
and	$9,$9,$14
and	$8,$8,$14
srl	$9,$9,1
srl	$8,$8,1
or	$3,$3,$13
or	$2,$5,$12
subu	$3,$3,$9
subu	$2,$2,$8
addiu	$11,$11,16
sw	$3,0($10)
sw	$2,4($10)
.set	noreorder
.set	nomacro
bne	$11,$4,$L1968
addu	$10,$10,$17
.set	macro
.set	reorder

lw	$31,588($sp)
lw	$19,584($sp)
lw	$18,580($sp)
lw	$17,576($sp)
lw	$16,572($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,592
.set	macro
.set	reorder

.end	put_qpel16_mc23_c
.size	put_qpel16_mc23_c, .-put_qpel16_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_mspel8_mc32_c
.type	put_mspel8_mc32_c, @function
put_mspel8_mc32_c:
.frame	$sp,264,$31		# vars= 216, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-264
li	$7,11			# 0xb
sw	$16,244($sp)
addiu	$16,$sp,24
sw	$18,252($sp)
move	$18,$5
subu	$5,$5,$6
sw	$19,256($sp)
move	$19,$4
sw	$31,260($sp)
move	$4,$16
sw	$17,248($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_h_lowpass.constprop.10
.option	pic2
move	$17,$6
.set	macro
.set	reorder

addiu	$4,$sp,176
addiu	$5,$18,1
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_v_lowpass.constprop.3
.option	pic2
move	$6,$17
.set	macro
.set	reorder

addiu	$4,$sp,112
addiu	$5,$sp,32
.option	pic0
.set	noreorder
.set	nomacro
jal	wmv2_mspel8_v_lowpass.constprop.3
.option	pic2
li	$6,8			# 0x8
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$11,64			# 0x40
move	$4,$19
ori	$10,$10,0xfefe
$L1972:
addiu	$3,$2,3
addiu	$9,$2,4
addiu	$7,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$7,$16,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 152($3)  
lwr $12, 152($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 88($3)  
lwr $6, 88($5)  

# 0 "" 2
#NO_APP
addu	$9,$16,$9
move	$5,$6
xor	$8,$6,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 88($7)  
lwr $3, 88($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 152($7)  
lwr $6, 152($9)  

# 0 "" 2
#NO_APP
xor	$7,$3,$6
and	$8,$8,$10
and	$7,$7,$10
srl	$8,$8,1
srl	$7,$7,1
or	$5,$5,$12
or	$3,$3,$6
subu	$5,$5,$8
subu	$3,$3,$7
addiu	$2,$2,8
sw	$5,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L1972
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,260($sp)
lw	$19,256($sp)
lw	$18,252($sp)
lw	$17,248($sp)
lw	$16,244($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

.end	put_mspel8_mc32_c
.size	put_mspel8_mc32_c, .-put_mspel8_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc21_c
.type	put_qpel16_mc21_c, @function
put_qpel16_mc21_c:
.frame	$sp,592,$31		# vars= 528, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-592
li	$2,17			# 0x11
sw	$16,572($sp)
addiu	$16,$sp,40
sw	$17,576($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$2,16($sp)
sw	$19,584($sp)
move	$7,$17
move	$19,$4
sw	$18,580($sp)
move	$4,$16
sw	$31,588($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
addiu	$18,$sp,312
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$19
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$19,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L1976:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($3)  
lwr $12, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 272($3)  
lwr $8, 272($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($9)  
lwr $5, 0($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 272($9)  
lwr $3, 272($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L1976
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,588($sp)
lw	$19,584($sp)
lw	$18,580($sp)
lw	$17,576($sp)
lw	$16,572($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,592
.set	macro
.set	reorder

.end	put_qpel16_mc21_c
.size	put_qpel16_mc21_c, .-put_qpel16_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc32_c
.type	put_qpel8_mc32_c, @function
put_qpel8_mc32_c:
.frame	$sp,272,$31		# vars= 216, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$19,264($sp)
addiu	$19,$sp,32
sw	$18,260($sp)
move	$18,$4
move	$4,$19
sw	$31,268($sp)
sw	$17,256($sp)
move	$17,$6
sw	$16,252($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
addiu	$16,$sp,176
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,36
addiu	$4,$sp,248
move	$2,$16
ori	$13,$13,0xfefe
$L1980:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($8)  
lwr $7, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$10,$7,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$7,$7,$12
or	$3,$3,$11
subu	$7,$7,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$8,$8,16
sw	$7,-8($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1980
sw	$3,-4($2)
.set	macro
.set	reorder

li	$7,8			# 0x8
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	put_qpel8_mc32_c
.size	put_qpel8_mc32_c, .-put_qpel8_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc21_c
.type	put_h264_qpel16_mc21_c, @function
put_h264_qpel16_mc21_c:
.frame	$sp,1256,$31		# vars= 1184, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1256
sw	$19,1240($sp)
addiu	$19,$sp,968
sw	$18,1236($sp)
addiu	$18,$sp,712
sw	$16,1228($sp)
addiu	$16,$sp,40
sw	$21,1248($sp)
move	$21,$5
sw	$20,1244($sp)
move	$20,$4
sw	$17,1232($sp)
move	$4,$19
sw	$31,1252($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$17,$6
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
move	$6,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L1984:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 928($3)  
lwr $12, 928($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 672($3)  
lwr $8, 672($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 928($9)  
lwr $5, 928($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 672($9)  
lwr $3, 672($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L1984
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1252($sp)
lw	$21,1248($sp)
lw	$20,1244($sp)
lw	$19,1240($sp)
lw	$18,1236($sp)
lw	$17,1232($sp)
lw	$16,1228($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1256
.set	macro
.set	reorder

.end	put_h264_qpel16_mc21_c
.size	put_h264_qpel16_mc21_c, .-put_h264_qpel16_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc12_c
.type	put_qpel8_mc12_c, @function
put_qpel8_mc12_c:
.frame	$sp,272,$31		# vars= 216, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$16,252($sp)
addiu	$16,$sp,32
sw	$19,264($sp)
move	$19,$4
move	$4,$16
sw	$31,268($sp)
sw	$18,260($sp)
move	$18,$6
sw	$17,256($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
addiu	$17,$sp,176
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,248
move	$2,$17
move	$8,$16
ori	$13,$13,0xfefe
$L1988:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($8)  
lwr $7, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($8)  
lwr $3, 4($8)  

# 0 "" 2
#NO_APP
xor	$10,$7,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$7,$7,$12
or	$3,$3,$11
subu	$7,$7,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$8,$8,16
sw	$7,-8($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L1988
sw	$3,-4($2)
.set	macro
.set	reorder

li	$7,8			# 0x8
move	$4,$19
move	$5,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$6,$18
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	put_qpel8_mc12_c
.size	put_qpel8_mc12_c, .-put_qpel8_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc01_c
.type	put_qpel16_mc01_c, @function
put_qpel16_mc01_c:
.frame	$sp,728,$31		# vars= 664, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-728
sw	$18,716($sp)
addiu	$18,$sp,40
sw	$19,720($sp)
addiu	$19,$sp,448
sw	$16,708($sp)
move	$16,$4
move	$4,$18
sw	$31,724($sp)
sw	$17,712($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$3,24			# 0x18
move	$4,$16
sw	$2,20($sp)
move	$6,$19
sw	$2,24($sp)
move	$5,$18
sw	$3,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,51
addiu	$8,$sp,459
addiu	$4,$16,8
addiu	$6,$sp,435
ori	$13,$13,0xfefe
$L1992:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($2)  
lwr $12, -3($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($8)  
lwr $5, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 4($2)  
lwr $11, 1($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$10,$5,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$5,$5,$12
or	$3,$3,$11
subu	$5,$5,$10
subu	$3,$3,$9
addiu	$2,$2,24
sw	$5,0($4)
addiu	$8,$8,16
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$6,$L1992
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,724($sp)
lw	$19,720($sp)
lw	$18,716($sp)
lw	$17,712($sp)
lw	$16,708($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,728
.set	macro
.set	reorder

.end	put_qpel16_mc01_c
.size	put_qpel16_mc01_c, .-put_qpel16_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc03_c
.type	put_qpel16_mc03_c, @function
put_qpel16_mc03_c:
.frame	$sp,728,$31		# vars= 664, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-728
sw	$19,720($sp)
addiu	$19,$sp,40
sw	$18,716($sp)
addiu	$18,$sp,448
sw	$16,708($sp)
move	$16,$4
move	$4,$19
sw	$31,724($sp)
sw	$17,712($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$3,24			# 0x18
addiu	$5,$sp,64
sw	$2,20($sp)
move	$4,$16
move	$6,$18
sw	$2,24($sp)
sw	$3,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

addiu	$8,$sp,459
li	$13,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,75
addiu	$4,$16,8
move	$6,$8
ori	$13,$13,0xfefe
$L1996:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($2)  
lwr $12, -3($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($8)  
lwr $5, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 4($2)  
lwr $11, 1($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$10,$5,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$5,$5,$12
or	$3,$3,$11
subu	$5,$5,$10
subu	$3,$3,$9
addiu	$2,$2,24
sw	$5,0($4)
addiu	$8,$8,16
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$6,$L1996
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,724($sp)
lw	$19,720($sp)
lw	$18,716($sp)
lw	$17,712($sp)
lw	$16,708($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,728
.set	macro
.set	reorder

.end	put_qpel16_mc03_c
.size	put_qpel16_mc03_c, .-put_qpel16_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc21_c
.type	put_h264_qpel2_mc21_c, @function
put_h264_qpel2_mc21_c:
.frame	$sp,128,$31		# vars= 56, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-128
addu	$15,$5,$6
sw	$31,124($sp)
move	$9,$5
sw	$fp,120($sp)
sw	$22,112($sp)
sw	$21,108($sp)
sw	$18,96($sp)
move	$18,$4
sw	$17,92($sp)
addiu	$4,$sp,60
sw	$16,88($sp)
move	$16,$6
sw	$23,116($sp)
sw	$20,104($sp)
sw	$19,100($sp)
lbu	$12,-1($5)
lbu	$3,1($5)
lbu	$10,0($5)
lbu	$2,2($5)
lui	$5,%hi(ff_cropTbl+1024)
lbu	$8,1($15)
lbu	$6,0($15)
addu	$13,$10,$3
addu	$22,$12,$2
lbu	$11,-1($15)
addu	$2,$3,$2
lbu	$3,2($15)
addu	$7,$6,$8
lbu	$20,3($9)
lbu	$19,3($15)
sll	$17,$13,2
addu	$21,$11,$3
lbu	$23,4($9)
addu	$3,$8,$3
lbu	$8,-2($15)
lbu	$15,4($15)
sll	$fp,$7,2
sll	$24,$21,2
lbu	$14,-2($9)
sll	$13,$13,4
sw	$23,72($sp)
sw	$8,76($sp)
sll	$7,$7,4
sll	$8,$22,2
sw	$15,80($sp)
addu	$6,$6,$19
sw	$16,16($sp)
addu	$10,$10,$20
addu	$8,$8,$22
addu	$7,$fp,$7
sll	$31,$2,2
addu	$13,$17,$13
sll	$2,$2,4
addu	$24,$24,$21
sll	$25,$10,2
sll	$23,$3,2
sll	$15,$6,2
sll	$3,$3,4
subu	$13,$13,$8
lw	$8,80($sp)
subu	$24,$7,$24
lw	$7,76($sp)
addu	$31,$31,$2
lw	$2,72($sp)
addu	$10,$25,$10
addu	$23,$23,$3
addu	$15,$15,$6
subu	$31,$31,$10
addu	$6,$14,$20
addu	$3,$12,$2
subu	$15,$23,$15
addu	$2,$7,$19
addu	$11,$11,$8
addu	$6,$13,$6
addu	$3,$31,$3
addu	$2,$24,$2
addu	$11,$15,$11
addiu	$6,$6,16
addiu	$3,$3,16
addiu	$2,$2,16
addiu	$11,$11,16
addiu	$5,$5,%lo(ff_cropTbl+1024)
sra	$6,$6,5
sra	$3,$3,5
sra	$2,$2,5
sra	$11,$11,5
addu	$6,$5,$6
addu	$11,$5,$11
addu	$3,$5,$3
addu	$2,$5,$2
lbu	$10,0($6)
addiu	$5,$sp,32
lbu	$8,0($3)
li	$7,2			# 0x2
lbu	$3,0($2)
move	$6,$9
lbu	$2,0($11)
addu	$16,$18,$16
sb	$10,64($sp)
sb	$8,65($sp)
sb	$3,66($sp)
.option	pic0
jal	put_h264_qpel2_hv_lowpass.constprop.30
.option	pic2
sb	$2,67($sp)

li	$4,-16908288			# 0xfffffffffefe0000
lhu	$7,66($sp)
lhu	$2,62($sp)
ori	$6,$4,0xfefe
lhu	$8,64($sp)
lhu	$3,60($sp)
xor	$4,$2,$7
lw	$31,124($sp)
and	$4,$4,$6
lw	$fp,120($sp)
xor	$5,$3,$8
lw	$23,116($sp)
and	$5,$5,$6
lw	$22,112($sp)
srl	$5,$5,1
lw	$21,108($sp)
srl	$4,$4,1
lw	$20,104($sp)
or	$3,$3,$8
lw	$19,100($sp)
or	$2,$2,$7
lw	$17,92($sp)
subu	$3,$3,$5
subu	$2,$2,$4
sh	$3,0($18)
sh	$2,0($16)
lw	$18,96($sp)
lw	$16,88($sp)
j	$31
addiu	$sp,$sp,128

.set	macro
.set	reorder
.end	put_h264_qpel2_mc21_c
.size	put_h264_qpel2_mc21_c, .-put_h264_qpel2_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc03_c
.type	put_h264_qpel16_mc03_c, @function
put_h264_qpel16_mc03_c:
.frame	$sp,656,$31		# vars= 592, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-656
sll	$2,$6,1
sw	$18,644($sp)
addiu	$18,$sp,40
subu	$5,$5,$2
sw	$19,648($sp)
sw	$16,636($sp)
addiu	$19,$sp,376
move	$16,$4
sw	$31,652($sp)
move	$4,$18
sw	$17,640($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
move	$17,$6
.set	macro
.set	reorder

addiu	$5,$sp,72
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$5,$sp,88
move	$4,$16
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$9,$16,8
move	$10,$0
li	$4,256			# 0x100
ori	$13,$13,0xfefe
$L2002:
addu	$2,$18,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 59($2)  
lwr $12, 56($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 347($2)  
lwr $3, 344($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 63($2)  
lwr $11, 60($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 351($2)  
lwr $5, 348($2)  

# 0 "" 2
#NO_APP
xor	$8,$3,$12
move	$2,$5
xor	$5,$5,$11
and	$8,$8,$13
and	$5,$5,$13
srl	$8,$8,1
srl	$5,$5,1
or	$3,$3,$12
or	$2,$2,$11
subu	$3,$3,$8
subu	$2,$2,$5
addiu	$10,$10,16
sw	$3,0($9)
sw	$2,4($9)
.set	noreorder
.set	nomacro
bne	$10,$4,$L2002
addu	$9,$9,$17
.set	macro
.set	reorder

lw	$31,652($sp)
lw	$19,648($sp)
lw	$18,644($sp)
lw	$17,640($sp)
lw	$16,636($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,656
.set	macro
.set	reorder

.end	put_h264_qpel16_mc03_c
.size	put_h264_qpel16_mc03_c, .-put_h264_qpel16_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc01_c
.type	put_h264_qpel16_mc01_c, @function
put_h264_qpel16_mc01_c:
.frame	$sp,656,$31		# vars= 592, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-656
sll	$2,$6,1
sw	$18,640($sp)
addiu	$18,$sp,40
subu	$5,$5,$2
sw	$20,648($sp)
sw	$19,644($sp)
addiu	$20,$sp,72
addiu	$19,$sp,376
sw	$16,632($sp)
move	$16,$4
sw	$31,652($sp)
move	$4,$18
sw	$17,636($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
move	$17,$6
.set	macro
.set	reorder

move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$16
move	$5,$20
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$9,$16,8
move	$10,$0
li	$4,256			# 0x100
ori	$13,$13,0xfefe
$L2006:
addu	$2,$18,$10
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 43($2)  
lwr $12, 40($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 347($2)  
lwr $3, 344($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 47($2)  
lwr $11, 44($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 351($2)  
lwr $5, 348($2)  

# 0 "" 2
#NO_APP
xor	$8,$3,$12
move	$2,$5
xor	$5,$5,$11
and	$8,$8,$13
and	$5,$5,$13
srl	$8,$8,1
srl	$5,$5,1
or	$3,$3,$12
or	$2,$2,$11
subu	$3,$3,$8
subu	$2,$2,$5
addiu	$10,$10,16
sw	$3,0($9)
sw	$2,4($9)
.set	noreorder
.set	nomacro
bne	$10,$4,$L2006
addu	$9,$9,$17
.set	macro
.set	reorder

lw	$31,652($sp)
lw	$20,648($sp)
lw	$19,644($sp)
lw	$18,640($sp)
lw	$17,636($sp)
lw	$16,632($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,656
.set	macro
.set	reorder

.end	put_h264_qpel16_mc01_c
.size	put_h264_qpel16_mc01_c, .-put_h264_qpel16_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc23_c
.type	put_h264_qpel16_mc23_c, @function
put_h264_qpel16_mc23_c:
.frame	$sp,1256,$31		# vars= 1184, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1256
sw	$19,1240($sp)
addiu	$19,$sp,968
sw	$21,1248($sp)
move	$21,$5
sw	$18,1236($sp)
addu	$5,$5,$6
sw	$16,1228($sp)
addiu	$18,$sp,712
addiu	$16,$sp,40
sw	$20,1244($sp)
sw	$17,1232($sp)
move	$20,$4
move	$17,$6
sw	$31,1252($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
move	$4,$19
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
move	$6,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L2010:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 928($3)  
lwr $12, 928($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 672($3)  
lwr $8, 672($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 928($9)  
lwr $5, 928($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 672($9)  
lwr $3, 672($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L2010
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1252($sp)
lw	$21,1248($sp)
lw	$20,1244($sp)
lw	$19,1240($sp)
lw	$18,1236($sp)
lw	$17,1232($sp)
lw	$16,1228($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1256
.set	macro
.set	reorder

.end	put_h264_qpel16_mc23_c
.size	put_h264_qpel16_mc23_c, .-put_h264_qpel16_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc23_c
.type	put_h264_qpel2_mc23_c, @function
put_h264_qpel2_mc23_c:
.frame	$sp,136,$31		# vars= 64, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-136
lui	$10,%hi(ff_cropTbl+1024)
sw	$16,96($sp)
move	$16,$6
sw	$18,104($sp)
addu	$18,$5,$16
sw	$31,132($sp)
addiu	$10,$10,%lo(ff_cropTbl+1024)
sw	$fp,128($sp)
move	$6,$5
sw	$23,124($sp)
addiu	$5,$sp,32
sw	$21,116($sp)
li	$7,2			# 0x2
sw	$20,112($sp)
move	$21,$4
sw	$19,108($sp)
addu	$19,$18,$16
sw	$17,100($sp)
addiu	$4,$sp,60
sw	$22,120($sp)
sw	$16,16($sp)
addu	$16,$21,$16
lbu	$2,2($18)
lbu	$13,0($18)
lbu	$11,1($18)
lbu	$15,-1($18)
lbu	$14,-1($19)
addu	$17,$13,$11
lbu	$9,1($19)
addu	$23,$15,$2
lbu	$24,3($18)
addu	$11,$11,$2
lbu	$2,2($19)
lbu	$12,0($19)
sll	$20,$17,2
lbu	$8,-2($18)
sll	$17,$17,4
lbu	$18,4($18)
addu	$25,$14,$2
addu	$3,$12,$9
lbu	$22,3($19)
addu	$2,$9,$2
lbu	$9,-2($19)
lbu	$19,4($19)
addu	$13,$13,$24
sw	$18,76($sp)
sll	$18,$25,2
sw	$8,72($sp)
sll	$8,$11,2
sll	$11,$11,4
sw	$9,84($sp)
sw	$18,80($sp)
addu	$12,$12,$22
sll	$9,$23,2
sw	$19,88($sp)
addu	$8,$8,$11
lw	$11,80($sp)
addu	$9,$9,$23
sll	$fp,$2,2
sll	$18,$12,2
sll	$2,$2,4
addu	$17,$20,$17
sll	$31,$3,2
sll	$19,$13,2
sll	$3,$3,4
addu	$25,$11,$25
addu	$12,$18,$12
lw	$18,72($sp)
addu	$fp,$fp,$2
lw	$2,76($sp)
subu	$17,$17,$9
lw	$11,88($sp)
lw	$9,84($sp)
addu	$13,$19,$13
addu	$3,$31,$3
addu	$24,$18,$24
addu	$15,$15,$2
subu	$3,$3,$25
subu	$8,$8,$13
addu	$2,$9,$22
subu	$fp,$fp,$12
addu	$14,$14,$11
addu	$8,$8,$15
addu	$17,$17,$24
addu	$fp,$fp,$14
addu	$2,$3,$2
addiu	$17,$17,16
addiu	$3,$8,16
addiu	$2,$2,16
addiu	$fp,$fp,16
sra	$8,$17,5
sra	$fp,$fp,5
sra	$3,$3,5
sra	$2,$2,5
addu	$8,$10,$8
addu	$3,$10,$3
addu	$2,$10,$2
addu	$10,$10,$fp
lbu	$9,0($8)
lbu	$8,0($3)
lbu	$3,0($2)
lbu	$2,0($10)
sb	$9,64($sp)
sb	$8,65($sp)
sb	$3,66($sp)
.option	pic0
jal	put_h264_qpel2_hv_lowpass.constprop.30
.option	pic2
sb	$2,67($sp)

li	$4,-16908288			# 0xfffffffffefe0000
lhu	$7,66($sp)
lhu	$2,62($sp)
ori	$6,$4,0xfefe
lhu	$8,64($sp)
lhu	$3,60($sp)
xor	$4,$2,$7
lw	$31,132($sp)
and	$4,$4,$6
lw	$fp,128($sp)
xor	$5,$3,$8
lw	$23,124($sp)
and	$5,$5,$6
lw	$22,120($sp)
srl	$5,$5,1
lw	$20,112($sp)
srl	$4,$4,1
lw	$19,108($sp)
or	$3,$3,$8
lw	$18,104($sp)
or	$2,$2,$7
lw	$17,100($sp)
subu	$3,$3,$5
subu	$2,$2,$4
sh	$3,0($21)
sh	$2,0($16)
lw	$21,116($sp)
lw	$16,96($sp)
j	$31
addiu	$sp,$sp,136

.set	macro
.set	reorder
.end	put_h264_qpel2_mc23_c
.size	put_h264_qpel2_mc23_c, .-put_h264_qpel2_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc33_c
.type	avg_qpel8_mc33_c, @function
avg_qpel8_mc33_c:
.frame	$sp,344,$31		# vars= 280, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,332($sp)
addiu	$18,$sp,40
sw	$17,328($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$19,336($sp)
addiu	$19,$sp,184
sw	$16,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$7,$sp,44
addiu	$18,$sp,256
move	$2,$19
ori	$13,$13,0xfefe
$L2016:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($7)  
lwr $8, -3($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($7)  
lwr $3, 1($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$18,$2,$L2016
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,8			# 0x8
addiu	$5,$sp,192
move	$4,$17
sw	$2,16($sp)
move	$6,$18
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$19,336($sp)
lw	$18,332($sp)
lw	$17,328($sp)
lw	$16,324($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	avg_qpel8_mc33_c
.size	avg_qpel8_mc33_c, .-avg_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc11_c
.type	avg_qpel8_mc11_c, @function
avg_qpel8_mc11_c:
.frame	$sp,344,$31		# vars= 280, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,328($sp)
addiu	$18,$sp,40
sw	$17,324($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$20,336($sp)
addiu	$20,$sp,184
sw	$19,332($sp)
addiu	$19,$sp,256
sw	$16,320($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
move	$2,$20
move	$7,$18
ori	$4,$4,0xfefe
$L2020:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($7)  
lwr $3, 4($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$4
and	$9,$9,$4
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2020
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$17
move	$5,$20
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$20,336($sp)
lw	$19,332($sp)
lw	$18,328($sp)
lw	$17,324($sp)
lw	$16,320($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	avg_qpel8_mc11_c
.size	avg_qpel8_mc11_c, .-avg_qpel8_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc31_c
.type	avg_qpel8_mc31_c, @function
avg_qpel8_mc31_c:
.frame	$sp,344,$31		# vars= 280, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,332($sp)
addiu	$18,$sp,40
sw	$17,328($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$19,336($sp)
addiu	$19,$sp,184
sw	$16,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$7,$sp,44
addiu	$18,$sp,256
move	$2,$19
ori	$13,$13,0xfefe
$L2024:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($7)  
lwr $8, -3($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($7)  
lwr $3, 1($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$18,$2,$L2024
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$17
move	$5,$19
sw	$2,16($sp)
move	$6,$18
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$19,336($sp)
lw	$18,332($sp)
lw	$17,328($sp)
lw	$16,324($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	avg_qpel8_mc31_c
.size	avg_qpel8_mc31_c, .-avg_qpel8_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc13_c
.type	avg_qpel8_mc13_c, @function
avg_qpel8_mc13_c:
.frame	$sp,344,$31		# vars= 280, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,328($sp)
addiu	$18,$sp,40
sw	$17,324($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$20,336($sp)
addiu	$20,$sp,184
sw	$19,332($sp)
addiu	$19,$sp,256
sw	$16,320($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
move	$2,$20
move	$7,$18
ori	$4,$4,0xfefe
$L2028:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($7)  
lwr $3, 4($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$4
and	$9,$9,$4
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2028
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,8			# 0x8
addiu	$5,$sp,192
move	$4,$17
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$20,336($sp)
lw	$19,332($sp)
lw	$18,328($sp)
lw	$17,324($sp)
lw	$16,320($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	avg_qpel8_mc13_c
.size	avg_qpel8_mc13_c, .-avg_qpel8_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc13_c
.type	put_qpel8_mc13_c, @function
put_qpel8_mc13_c:
.frame	$sp,344,$31		# vars= 280, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,328($sp)
addiu	$18,$sp,40
sw	$17,324($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$20,336($sp)
addiu	$20,$sp,184
sw	$19,332($sp)
addiu	$19,$sp,256
sw	$16,320($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
move	$2,$20
move	$7,$18
ori	$4,$4,0xfefe
$L2032:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($7)  
lwr $3, 4($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$4
and	$9,$9,$4
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2032
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,8			# 0x8
addiu	$5,$sp,192
move	$4,$17
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$20,336($sp)
lw	$19,332($sp)
lw	$18,328($sp)
lw	$17,324($sp)
lw	$16,320($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	put_qpel8_mc13_c
.size	put_qpel8_mc13_c, .-put_qpel8_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc32_c
.type	avg_qpel16_mc32_c, @function
avg_qpel16_mc32_c:
.frame	$sp,744,$31		# vars= 680, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-744
sw	$19,732($sp)
addiu	$19,$sp,40
sw	$20,736($sp)
li	$20,17			# 0x11
sw	$18,728($sp)
move	$18,$4
sw	$16,720($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,740($sp)
sw	$17,724($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$6,$sp,41
sw	$20,24($sp)
li	$7,16			# 0x10
sw	$2,16($sp)
li	$2,24			# 0x18
move	$4,$16
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$8,$sp,52
addiu	$4,$sp,728
ori	$12,$12,0xfefe
$L2036:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($8)  
lwr $7, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$9,$7,$11
xor	$5,$3,$10
and	$9,$9,$12
and	$5,$5,$12
srl	$9,$9,1
srl	$5,$5,1
or	$7,$7,$11
or	$3,$3,$10
subu	$7,$7,$9
subu	$3,$3,$5
addiu	$2,$2,16
addiu	$8,$8,24
sw	$7,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L2036
sw	$3,-12($2)
.set	macro
.set	reorder

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,740($sp)
lw	$20,736($sp)
lw	$19,732($sp)
lw	$18,728($sp)
lw	$17,724($sp)
lw	$16,720($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,744
.set	macro
.set	reorder

.end	avg_qpel16_mc32_c
.size	avg_qpel16_mc32_c, .-avg_qpel16_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc11_c
.type	put_h264_qpel16_mc11_c, @function
put_h264_qpel16_mc11_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$20,904($sp)
move	$20,$4
sw	$17,892($sp)
move	$4,$19
move	$17,$6
sw	$31,908($sp)
sw	$18,896($sp)
move	$18,$5
sw	$16,888($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
addiu	$16,$sp,40
.set	macro
.set	reorder

sll	$5,$17,1
move	$6,$17
subu	$5,$18,$5
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,376
.set	macro
.set	reorder

addiu	$5,$sp,72
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$7,$17
sw	$2,16($sp)
move	$5,$19
sw	$2,20($sp)
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$11,256			# 0x100
ori	$7,$7,0xfefe
$L2040:
addiu	$6,$2,8
addiu	$3,$2,11
addiu	$10,$2,12
addiu	$8,$2,15
addu	$3,$16,$3
addu	$6,$16,$6
addu	$8,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 592($3)  
lwr $12, 592($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 336($3)  
lwr $5, 336($6)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$9,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 592($8)  
lwr $6, 592($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 336($8)  
lwr $3, 336($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$6
and	$9,$9,$7
and	$8,$8,$7
srl	$9,$9,1
srl	$8,$8,1
or	$5,$5,$12
or	$3,$3,$6
subu	$5,$5,$9
subu	$3,$3,$8
addiu	$2,$2,16
sw	$5,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2040
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,912
.set	macro
.set	reorder

.end	put_h264_qpel16_mc11_c
.size	put_h264_qpel16_mc11_c, .-put_h264_qpel16_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc33_c
.type	put_h264_qpel16_mc33_c, @function
put_h264_qpel16_mc33_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$18,896($sp)
move	$18,$5
addu	$5,$5,$6
sw	$20,904($sp)
sw	$17,892($sp)
move	$20,$4
move	$17,$6
sw	$31,908($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
sw	$16,888($sp)
.set	macro
.set	reorder

subu	$2,$0,$17
addiu	$16,$sp,40
sll	$2,$2,1
move	$6,$17
addiu	$5,$2,1
move	$4,$16
addu	$5,$18,$5
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,376
.set	macro
.set	reorder

addiu	$5,$sp,72
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$7,$17
sw	$2,16($sp)
move	$5,$19
sw	$2,20($sp)
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$11,256			# 0x100
ori	$7,$7,0xfefe
$L2044:
addiu	$6,$2,8
addiu	$3,$2,11
addiu	$10,$2,12
addiu	$8,$2,15
addu	$3,$16,$3
addu	$6,$16,$6
addu	$8,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 592($3)  
lwr $12, 592($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 336($3)  
lwr $5, 336($6)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$9,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 592($8)  
lwr $6, 592($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 336($8)  
lwr $3, 336($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$6
and	$9,$9,$7
and	$8,$8,$7
srl	$9,$9,1
srl	$8,$8,1
or	$5,$5,$12
or	$3,$3,$6
subu	$5,$5,$9
subu	$3,$3,$8
addiu	$2,$2,16
sw	$5,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2044
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,912
.set	macro
.set	reorder

.end	put_h264_qpel16_mc33_c
.size	put_h264_qpel16_mc33_c, .-put_h264_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc32_c
.type	put_qpel16_mc32_c, @function
put_qpel16_mc32_c:
.frame	$sp,744,$31		# vars= 680, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-744
sw	$19,732($sp)
addiu	$19,$sp,40
sw	$20,736($sp)
li	$20,17			# 0x11
sw	$18,728($sp)
move	$18,$4
sw	$16,720($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,740($sp)
sw	$17,724($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$6,$sp,41
sw	$20,24($sp)
li	$7,16			# 0x10
sw	$2,16($sp)
li	$2,24			# 0x18
move	$4,$16
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$8,$sp,52
addiu	$4,$sp,728
ori	$12,$12,0xfefe
$L2048:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($8)  
lwr $7, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$9,$7,$11
xor	$5,$3,$10
and	$9,$9,$12
and	$5,$5,$12
srl	$9,$9,1
srl	$5,$5,1
or	$7,$7,$11
or	$3,$3,$10
subu	$7,$7,$9
subu	$3,$3,$5
addiu	$2,$2,16
addiu	$8,$8,24
sw	$7,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L2048
sw	$3,-12($2)
.set	macro
.set	reorder

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,740($sp)
lw	$20,736($sp)
lw	$19,732($sp)
lw	$18,728($sp)
lw	$17,724($sp)
lw	$16,720($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,744
.set	macro
.set	reorder

.end	put_qpel16_mc32_c
.size	put_qpel16_mc32_c, .-put_qpel16_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc12_c
.type	avg_qpel16_mc12_c, @function
avg_qpel16_mc12_c:
.frame	$sp,744,$31		# vars= 680, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-744
sw	$19,732($sp)
addiu	$19,$sp,40
sw	$20,736($sp)
li	$20,17			# 0x11
sw	$18,728($sp)
move	$18,$4
sw	$16,720($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,740($sp)
sw	$17,724($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$16
sw	$2,16($sp)
li	$2,24			# 0x18
move	$5,$16
move	$6,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$8,$sp,51
addiu	$4,$sp,728
ori	$12,$12,0xfefe
$L2052:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($8)  
lwr $7, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$9,$7,$11
xor	$5,$3,$10
and	$9,$9,$12
and	$5,$5,$12
srl	$9,$9,1
srl	$5,$5,1
or	$7,$7,$11
or	$3,$3,$10
subu	$7,$7,$9
subu	$3,$3,$5
addiu	$2,$2,16
addiu	$8,$8,24
sw	$7,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L2052
sw	$3,-12($2)
.set	macro
.set	reorder

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,740($sp)
lw	$20,736($sp)
lw	$19,732($sp)
lw	$18,728($sp)
lw	$17,724($sp)
lw	$16,720($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,744
.set	macro
.set	reorder

.end	avg_qpel16_mc12_c
.size	avg_qpel16_mc12_c, .-avg_qpel16_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc31_c
.type	put_h264_qpel16_mc31_c, @function
put_h264_qpel16_mc31_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$20,904($sp)
move	$20,$4
sw	$17,892($sp)
move	$4,$19
move	$17,$6
sw	$31,908($sp)
sw	$18,896($sp)
move	$18,$5
sw	$16,888($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
addiu	$16,$sp,40
.set	macro
.set	reorder

subu	$2,$0,$17
move	$6,$17
sll	$2,$2,1
move	$4,$16
addiu	$5,$2,1
addu	$5,$18,$5
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,376
.set	macro
.set	reorder

addiu	$5,$sp,72
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$7,$17
sw	$2,16($sp)
move	$5,$19
sw	$2,20($sp)
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$11,256			# 0x100
ori	$7,$7,0xfefe
$L2056:
addiu	$6,$2,8
addiu	$3,$2,11
addiu	$10,$2,12
addiu	$8,$2,15
addu	$3,$16,$3
addu	$6,$16,$6
addu	$8,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 592($3)  
lwr $12, 592($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 336($3)  
lwr $5, 336($6)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$9,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 592($8)  
lwr $6, 592($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 336($8)  
lwr $3, 336($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$6
and	$9,$9,$7
and	$8,$8,$7
srl	$9,$9,1
srl	$8,$8,1
or	$5,$5,$12
or	$3,$3,$6
subu	$5,$5,$9
subu	$3,$3,$8
addiu	$2,$2,16
sw	$5,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2056
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,912
.set	macro
.set	reorder

.end	put_h264_qpel16_mc31_c
.size	put_h264_qpel16_mc31_c, .-put_h264_qpel16_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc32_c
.type	put_h264_qpel16_mc32_c, @function
put_h264_qpel16_mc32_c:
.frame	$sp,1592,$31		# vars= 1520, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
subu	$2,$0,$6
addiu	$sp,$sp,-1592
sll	$2,$2,1
sw	$21,1584($sp)
move	$21,$5
addiu	$5,$2,1
sw	$20,1580($sp)
move	$20,$4
sw	$19,1576($sp)
addiu	$4,$sp,712
sw	$31,1588($sp)
addu	$5,$21,$5
sw	$18,1572($sp)
addiu	$19,$sp,1304
sw	$17,1568($sp)
sw	$16,1564($sp)
move	$17,$6
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,1048
.set	macro
.set	reorder

addiu	$5,$sp,744
addiu	$16,$sp,40
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$19
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
move	$6,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L2060:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 1264($3)  
lwr $12, 1264($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 1008($3)  
lwr $8, 1008($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 1264($9)  
lwr $5, 1264($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 1008($9)  
lwr $3, 1008($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L2060
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1588($sp)
lw	$21,1584($sp)
lw	$20,1580($sp)
lw	$19,1576($sp)
lw	$18,1572($sp)
lw	$17,1568($sp)
lw	$16,1564($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1592
.set	macro
.set	reorder

.end	put_h264_qpel16_mc32_c
.size	put_h264_qpel16_mc32_c, .-put_h264_qpel16_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc33_c
.type	put_qpel8_mc33_c, @function
put_qpel8_mc33_c:
.frame	$sp,344,$31		# vars= 280, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,332($sp)
addiu	$18,$sp,40
sw	$17,328($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$19,336($sp)
addiu	$19,$sp,184
sw	$16,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$7,$sp,44
addiu	$18,$sp,256
move	$2,$19
ori	$13,$13,0xfefe
$L2064:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($7)  
lwr $8, -3($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($7)  
lwr $3, 1($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$18,$2,$L2064
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,8			# 0x8
addiu	$5,$sp,192
move	$4,$17
sw	$2,16($sp)
move	$6,$18
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$19,336($sp)
lw	$18,332($sp)
lw	$17,328($sp)
lw	$16,324($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	put_qpel8_mc33_c
.size	put_qpel8_mc33_c, .-put_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc12_c
.type	put_qpel16_mc12_c, @function
put_qpel16_mc12_c:
.frame	$sp,744,$31		# vars= 680, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-744
sw	$19,732($sp)
addiu	$19,$sp,40
sw	$20,736($sp)
li	$20,17			# 0x11
sw	$18,728($sp)
move	$18,$4
sw	$16,720($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,740($sp)
sw	$17,724($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$16
sw	$2,16($sp)
li	$2,24			# 0x18
move	$5,$16
move	$6,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$8,$sp,51
addiu	$4,$sp,728
ori	$12,$12,0xfefe
$L2068:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($8)  
lwr $7, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
xor	$9,$7,$11
xor	$5,$3,$10
and	$9,$9,$12
and	$5,$5,$12
srl	$9,$9,1
srl	$5,$5,1
or	$7,$7,$11
or	$3,$3,$10
subu	$7,$7,$9
subu	$3,$3,$5
addiu	$2,$2,16
addiu	$8,$8,24
sw	$7,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$4,$L2068
sw	$3,-12($2)
.set	macro
.set	reorder

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,740($sp)
lw	$20,736($sp)
lw	$19,732($sp)
lw	$18,728($sp)
lw	$17,724($sp)
lw	$16,720($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,744
.set	macro
.set	reorder

.end	put_qpel16_mc12_c
.size	put_qpel16_mc12_c, .-put_qpel16_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc12_c
.type	put_h264_qpel16_mc12_c, @function
put_h264_qpel16_mc12_c:
.frame	$sp,1592,$31		# vars= 1520, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1592
sll	$2,$6,1
sw	$21,1584($sp)
move	$21,$5
sw	$20,1580($sp)
subu	$5,$5,$2
move	$20,$4
sw	$19,1576($sp)
addiu	$4,$sp,712
sw	$31,1588($sp)
addiu	$19,$sp,1304
sw	$18,1572($sp)
sw	$17,1568($sp)
addiu	$18,$sp,1048
move	$17,$6
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
sw	$16,1564($sp)
.set	macro
.set	reorder

addiu	$16,$sp,40
addiu	$5,$sp,744
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$19
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
move	$6,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_hv_lowpass.constprop.20
.option	pic2
move	$7,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L2072:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 1264($3)  
lwr $12, 1264($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 1008($3)  
lwr $8, 1008($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 1264($9)  
lwr $5, 1264($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 1008($9)  
lwr $3, 1008($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L2072
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1588($sp)
lw	$21,1584($sp)
lw	$20,1580($sp)
lw	$19,1576($sp)
lw	$18,1572($sp)
lw	$17,1568($sp)
lw	$16,1564($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1592
.set	macro
.set	reorder

.end	put_h264_qpel16_mc12_c
.size	put_h264_qpel16_mc12_c, .-put_h264_qpel16_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel16_mc13_c
.type	put_h264_qpel16_mc13_c, @function
put_h264_qpel16_mc13_c:
.frame	$sp,912,$31		# vars= 848, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-912
sw	$19,900($sp)
addiu	$19,$sp,632
sw	$18,896($sp)
move	$18,$5
addu	$5,$5,$6
sw	$20,904($sp)
sw	$17,892($sp)
move	$20,$4
move	$17,$6
sw	$31,908($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_h_lowpass.constprop.22
.option	pic2
sw	$16,888($sp)
.set	macro
.set	reorder

sll	$5,$17,1
addiu	$16,$sp,40
subu	$5,$18,$5
move	$6,$17
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block16.constprop.41
.option	pic2
addiu	$18,$sp,376
.set	macro
.set	reorder

addiu	$5,$sp,72
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_qpel16_v_lowpass.constprop.23
.option	pic2
move	$4,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$20
move	$7,$17
sw	$2,16($sp)
move	$5,$19
sw	$2,20($sp)
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$4,$20,8
move	$2,$0
li	$11,256			# 0x100
ori	$7,$7,0xfefe
$L2076:
addiu	$6,$2,8
addiu	$3,$2,11
addiu	$10,$2,12
addiu	$8,$2,15
addu	$3,$16,$3
addu	$6,$16,$6
addu	$8,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 592($3)  
lwr $12, 592($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 336($3)  
lwr $5, 336($6)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$9,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 592($8)  
lwr $6, 592($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 336($8)  
lwr $3, 336($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$6
and	$9,$9,$7
and	$8,$8,$7
srl	$9,$9,1
srl	$8,$8,1
or	$5,$5,$12
or	$3,$3,$6
subu	$5,$5,$9
subu	$3,$3,$8
addiu	$2,$2,16
sw	$5,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2076
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,908($sp)
lw	$20,904($sp)
lw	$19,900($sp)
lw	$18,896($sp)
lw	$17,892($sp)
lw	$16,888($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,912
.set	macro
.set	reorder

.end	put_h264_qpel16_mc13_c
.size	put_h264_qpel16_mc13_c, .-put_h264_qpel16_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc11_c
.type	put_qpel8_mc11_c, @function
put_qpel8_mc11_c:
.frame	$sp,344,$31		# vars= 280, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,328($sp)
addiu	$18,$sp,40
sw	$17,324($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$20,336($sp)
addiu	$20,$sp,184
sw	$19,332($sp)
addiu	$19,$sp,256
sw	$16,320($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$4,-16908288			# 0xfffffffffefe0000
move	$2,$20
move	$7,$18
ori	$4,$4,0xfefe
$L2080:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($7)  
lwr $3, 4($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$4
and	$9,$9,$4
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2080
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$17
move	$5,$20
sw	$2,16($sp)
move	$6,$19
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$20,336($sp)
lw	$19,332($sp)
lw	$18,328($sp)
lw	$17,324($sp)
lw	$16,320($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	put_qpel8_mc11_c
.size	put_qpel8_mc11_c, .-put_qpel8_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc31_c
.type	put_qpel8_mc31_c, @function
put_qpel8_mc31_c:
.frame	$sp,344,$31		# vars= 280, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-344
sw	$18,332($sp)
addiu	$18,$sp,40
sw	$17,328($sp)
move	$17,$4
move	$4,$18
sw	$31,340($sp)
sw	$19,336($sp)
addiu	$19,$sp,184
sw	$16,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$7,16			# 0x10
li	$6,8			# 0x8
sw	$2,16($sp)
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$13,-16908288			# 0xfffffffffefe0000
addiu	$7,$sp,44
addiu	$18,$sp,256
move	$2,$19
ori	$13,$13,0xfefe
$L2084:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 7($2)  
lwr $11, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($7)  
lwr $8, -3($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($7)  
lwr $3, 1($7)  

# 0 "" 2
#NO_APP
xor	$10,$8,$12
xor	$9,$3,$11
and	$10,$10,$13
and	$9,$9,$13
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$11
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,8
addiu	$7,$7,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$18,$2,$L2084
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$17
move	$5,$19
sw	$2,16($sp)
move	$6,$18
sw	$2,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,340($sp)
lw	$19,336($sp)
lw	$18,332($sp)
lw	$17,328($sp)
lw	$16,324($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

.end	put_qpel8_mc31_c
.size	put_qpel8_mc31_c, .-put_qpel8_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc02_c
.type	avg_qpel8_mc02_c, @function
avg_qpel8_mc02_c:
.frame	$sp,176,$31		# vars= 144, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-176
addiu	$11,$sp,24
sw	$31,172($sp)
addiu	$10,$sp,168
move	$2,$11
$L2088:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 7($5)  
lwr $7, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($2)  
swr $8, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 7($2)  
swr $7, 4($2)  

# 0 "" 2
#NO_APP
sb	$9,8($2)
addiu	$2,$2,16
.set	noreorder
.set	nomacro
bne	$2,$10,$L2088
addu	$5,$5,$6
.set	macro
.set	reorder

li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$11
.set	macro
.set	reorder

lw	$31,172($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,176
.set	macro
.set	reorder

.end	avg_qpel8_mc02_c
.size	avg_qpel8_mc02_c, .-avg_qpel8_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel8_mc02_c
.type	put_qpel8_mc02_c, @function
put_qpel8_mc02_c:
.frame	$sp,176,$31		# vars= 144, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-176
addiu	$11,$sp,24
sw	$31,172($sp)
addiu	$10,$sp,168
move	$2,$11
$L2092:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 7($5)  
lwr $7, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($2)  
swr $8, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 7($2)  
swr $7, 4($2)  

# 0 "" 2
#NO_APP
sb	$9,8($2)
addiu	$2,$2,16
.set	noreorder
.set	nomacro
bne	$2,$10,$L2092
addu	$5,$5,$6
.set	macro
.set	reorder

li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$11
.set	macro
.set	reorder

lw	$31,172($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,176
.set	macro
.set	reorder

.end	put_qpel8_mc02_c
.size	put_qpel8_mc02_c, .-put_qpel8_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc02_c
.type	put_no_rnd_qpel8_mc02_c, @function
put_no_rnd_qpel8_mc02_c:
.frame	$sp,176,$31		# vars= 144, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-176
addiu	$11,$sp,24
sw	$31,172($sp)
addiu	$10,$sp,168
move	$2,$11
$L2096:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 7($5)  
lwr $7, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($2)  
swr $8, 0($2)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 7($2)  
swr $7, 4($2)  

# 0 "" 2
#NO_APP
sb	$9,8($2)
addiu	$2,$2,16
.set	noreorder
.set	nomacro
bne	$2,$10,$L2096
addu	$5,$5,$6
.set	macro
.set	reorder

li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$11
.set	macro
.set	reorder

lw	$31,172($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,176
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc02_c
.size	put_no_rnd_qpel8_mc02_c, .-put_no_rnd_qpel8_mc02_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc10_c
.type	put_no_rnd_qpel16_mc10_c, @function
put_no_rnd_qpel16_mc10_c:
.frame	$sp,304,$31		# vars= 256, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-304
li	$2,16			# 0x10
sw	$18,296($sp)
move	$18,$6
sw	$17,292($sp)
li	$6,16			# 0x10
move	$17,$4
sw	$2,16($sp)
addiu	$4,$sp,32
sw	$16,288($sp)
move	$7,$18
sw	$31,300($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$16,$5
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,35
addiu	$9,$sp,291
move	$4,$0
ori	$7,$7,0xfefe
$L2100:
addu	$6,$16,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($8)  
lwr $3, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($6)  
lwr $5, 0($6)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
and	$2,$2,$7
srl	$2,$2,1
and	$3,$3,$5
addu	$3,$2,$3
addiu	$5,$4,4
addu	$10,$17,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($8)  
lwr $2, 1($8)  

# 0 "" 2
#NO_APP
sw	$3,0($10)
addu	$3,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($6)  
lwr $10, 0($3)  

# 0 "" 2
#NO_APP
xor	$3,$2,$10
and	$3,$3,$7
srl	$3,$3,1
and	$2,$2,$10
addu	$2,$3,$2
addu	$5,$17,$5
addiu	$8,$8,16
sw	$2,0($5)
.set	noreorder
.set	nomacro
bne	$8,$9,$L2100
addu	$4,$4,$18
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,43
addiu	$10,$sp,299
move	$4,$0
ori	$9,$9,0xfefe
$L2101:
addiu	$6,$4,8
addu	$7,$16,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($8)  
lwr $3, -3($8)  

# 0 "" 2
#NO_APP
addu	$2,$16,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 11($7)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
and	$2,$2,$9
srl	$2,$2,1
and	$3,$3,$5
addu	$3,$2,$3
addiu	$5,$4,12
addu	$6,$17,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($8)  
lwr $2, 1($8)  

# 0 "" 2
#NO_APP
sw	$3,0($6)
addu	$3,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 15($7)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
xor	$3,$2,$6
and	$3,$3,$9
srl	$3,$3,1
and	$2,$2,$6
addu	$2,$3,$2
addu	$5,$17,$5
addiu	$8,$8,16
sw	$2,0($5)
.set	noreorder
.set	nomacro
bne	$8,$10,$L2101
addu	$4,$4,$18
.set	macro
.set	reorder

lw	$31,300($sp)
lw	$18,296($sp)
lw	$17,292($sp)
lw	$16,288($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,304
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc10_c
.size	put_no_rnd_qpel16_mc10_c, .-put_no_rnd_qpel16_mc10_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc30_c
.type	put_no_rnd_qpel16_mc30_c, @function
put_no_rnd_qpel16_mc30_c:
.frame	$sp,304,$31		# vars= 256, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-304
li	$2,16			# 0x10
sw	$17,292($sp)
move	$17,$6
sw	$16,288($sp)
li	$6,16			# 0x10
move	$16,$4
sw	$2,16($sp)
addiu	$4,$sp,32
sw	$18,296($sp)
move	$7,$17
sw	$31,300($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$18,$5
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,35
addiu	$10,$sp,291
move	$9,$0
ori	$7,$7,0xfefe
$L2106:
addiu	$4,$9,4
addu	$6,$18,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($8)  
lwr $3, -3($8)  

# 0 "" 2
#NO_APP
addu	$2,$18,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($2)  
lwr $5, 1($6)  

# 0 "" 2
#NO_APP
xor	$2,$3,$5
and	$2,$2,$7
srl	$2,$2,1
and	$3,$3,$5
addu	$3,$2,$3
addu	$5,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($8)  
lwr $2, 1($8)  

# 0 "" 2
#NO_APP
sw	$3,0($5)
addu	$4,$16,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 8($6)  
lwr $5, 5($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$5
and	$3,$3,$7
srl	$3,$3,1
and	$2,$2,$5
addu	$2,$3,$2
addiu	$8,$8,16
addu	$9,$9,$17
.set	noreorder
.set	nomacro
bne	$8,$10,$L2106
sw	$2,0($4)
.set	macro
.set	reorder

li	$8,-16908288			# 0xfffffffffefe0000
addiu	$18,$18,9
addiu	$5,$sp,43
addiu	$9,$sp,299
move	$6,$0
ori	$8,$8,0xfefe
$L2107:
addu	$4,$18,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 0($5)  
lwr $3, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($4)  
lwr $7, 0($4)  

# 0 "" 2
#NO_APP
xor	$2,$3,$7
and	$2,$2,$8
srl	$2,$2,1
and	$3,$3,$7
addu	$3,$2,$3
addu	$7,$16,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($5)  
lwr $2, 1($5)  

# 0 "" 2
#NO_APP
sw	$3,8($7)
addiu	$5,$5,16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($4)  
lwr $3, 4($4)  

# 0 "" 2
#NO_APP
move	$4,$3
xor	$3,$2,$3
and	$3,$3,$8
srl	$3,$3,1
and	$2,$2,$4
addu	$2,$3,$2
addu	$6,$6,$17
.set	noreorder
.set	nomacro
bne	$5,$9,$L2107
sw	$2,12($7)
.set	macro
.set	reorder

lw	$31,300($sp)
lw	$18,296($sp)
lw	$17,292($sp)
lw	$16,288($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,304
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc30_c
.size	put_no_rnd_qpel16_mc30_c, .-put_no_rnd_qpel16_mc30_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc21_c
.type	put_no_rnd_qpel16_mc21_c, @function
put_no_rnd_qpel16_mc21_c:
.frame	$sp,576,$31		# vars= 528, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-576
li	$2,17			# 0x11
sw	$16,560($sp)
addiu	$16,$sp,32
sw	$17,564($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$18,568($sp)
sw	$2,16($sp)
move	$18,$4
move	$7,$17
sw	$31,572($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$sp,304
li	$7,16			# 0x10
li	$6,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
move	$9,$0
li	$10,256			# 0x100
move	$4,$18
ori	$7,$7,0xfefe
$L2112:
addiu	$2,$9,3
addiu	$6,$9,4
addiu	$3,$9,7
addu	$5,$16,$9
addu	$2,$16,$2
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($2)  
lwr $12, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 272($2)  
lwr $8, 272($5)  

# 0 "" 2
#NO_APP
addu	$6,$16,$6
move	$5,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 0($3)  
lwr $11, 0($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 272($3)  
lwr $2, 272($6)  

# 0 "" 2
#NO_APP
xor	$8,$8,$12
xor	$3,$2,$11
and	$8,$8,$7
and	$3,$3,$7
srl	$8,$8,1
srl	$3,$3,1
and	$5,$5,$12
and	$2,$2,$11
addu	$5,$8,$5
addu	$2,$3,$2
addiu	$9,$9,16
sw	$5,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$9,$10,$L2112
addu	$4,$4,$17
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$4,$18,8
move	$9,$0
li	$11,256			# 0x100
ori	$10,$10,0xfefe
$L2113:
addiu	$7,$9,8
addiu	$2,$9,11
addiu	$6,$9,12
addiu	$3,$9,15
addu	$2,$16,$2
addu	$7,$16,$7
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 0($2)  
lwr $12, 0($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 272($2)  
lwr $5, 272($7)  

# 0 "" 2
#NO_APP
addu	$6,$16,$6
xor	$8,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 0($3)  
lwr $7, 0($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 272($3)  
lwr $2, 272($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$7
and	$8,$8,$10
and	$3,$3,$10
srl	$8,$8,1
srl	$3,$3,1
and	$5,$5,$12
and	$2,$2,$7
addu	$5,$8,$5
addu	$2,$3,$2
addiu	$9,$9,16
sw	$5,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$9,$11,$L2113
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,572($sp)
lw	$18,568($sp)
lw	$17,564($sp)
lw	$16,560($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,576
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc21_c
.size	put_no_rnd_qpel16_mc21_c, .-put_no_rnd_qpel16_mc21_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc03_c
.type	put_no_rnd_qpel16_mc03_c, @function
put_no_rnd_qpel16_mc03_c:
.frame	$sp,704,$31		# vars= 664, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-704
sw	$18,696($sp)
addiu	$18,$sp,24
sw	$17,692($sp)
move	$17,$4
move	$4,$18
sw	$31,700($sp)
sw	$16,688($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$16,$6
.set	macro
.set	reorder

addiu	$4,$sp,432
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

addiu	$9,$sp,435
li	$11,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,51
move	$4,$17
move	$12,$9
ori	$11,$11,0xfefe
$L2118:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 0($8)  
lwr $10, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($9)  
lwr $5, -3($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 4($8)  
lwr $6, 1($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($9)  
lwr $2, 1($9)  

# 0 "" 2
#NO_APP
xor	$7,$5,$10
xor	$3,$2,$6
and	$7,$7,$11
and	$3,$3,$11
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$10
and	$2,$2,$6
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,24
sw	$5,0($4)
addiu	$9,$9,16
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$12,$L2118
addu	$4,$4,$16
.set	macro
.set	reorder

addiu	$9,$sp,443
li	$11,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,59
addiu	$4,$17,8
move	$12,$9
ori	$11,$11,0xfefe
$L2119:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 0($8)  
lwr $10, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($9)  
lwr $5, -3($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 4($8)  
lwr $6, 1($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($9)  
lwr $2, 1($9)  

# 0 "" 2
#NO_APP
xor	$7,$5,$10
xor	$3,$2,$6
and	$7,$7,$11
and	$3,$3,$11
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$10
and	$2,$2,$6
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,24
sw	$5,0($4)
addiu	$9,$9,16
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$12,$L2119
addu	$4,$4,$16
.set	macro
.set	reorder

lw	$31,700($sp)
lw	$18,696($sp)
lw	$17,692($sp)
lw	$16,688($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,704
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc03_c
.size	put_no_rnd_qpel16_mc03_c, .-put_no_rnd_qpel16_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc23_c
.type	put_no_rnd_qpel16_mc23_c, @function
put_no_rnd_qpel16_mc23_c:
.frame	$sp,576,$31		# vars= 528, regs= 4/0, args= 24, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-576
li	$2,17			# 0x11
sw	$16,560($sp)
addiu	$16,$sp,32
sw	$17,564($sp)
move	$17,$6
li	$6,16			# 0x10
sw	$2,16($sp)
sw	$18,568($sp)
move	$7,$17
move	$18,$4
sw	$31,572($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$sp,304
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$11,256			# 0x100
move	$4,$18
ori	$10,$10,0xfefe
$L2124:
addiu	$6,$2,16
addu	$2,$16,$2
addu	$3,$16,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 275($2)  
lwr $5, 272($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 19($2)  
lwr $8, 0($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 23($2)  
lwr $7, 20($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 279($2)  
lwr $3, 276($2)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
xor	$2,$3,$7
and	$9,$9,$10
and	$2,$2,$10
srl	$2,$2,1
srl	$9,$9,1
and	$8,$5,$8
and	$3,$3,$7
addu	$3,$2,$3
addu	$8,$9,$8
move	$2,$6
sw	$3,4($4)
sw	$8,0($4)
.set	noreorder
.set	nomacro
bne	$6,$11,$L2124
addu	$4,$4,$17
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$4,$18,8
move	$6,$0
li	$11,256			# 0x100
ori	$10,$10,0xfefe
$L2125:
addu	$2,$16,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 27($2)  
lwr $8, 24($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 283($2)  
lwr $5, 280($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 31($2)  
lwr $7, 28($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 287($2)  
lwr $3, 284($2)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
move	$2,$3
xor	$3,$3,$7
and	$9,$9,$10
and	$3,$3,$10
srl	$9,$9,1
srl	$3,$3,1
and	$8,$5,$8
and	$2,$2,$7
addu	$8,$9,$8
addu	$2,$3,$2
addiu	$6,$6,16
sw	$8,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$6,$11,$L2125
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,572($sp)
lw	$18,568($sp)
lw	$17,564($sp)
lw	$16,560($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,576
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc23_c
.size	put_no_rnd_qpel16_mc23_c, .-put_no_rnd_qpel16_mc23_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc01_c
.type	put_no_rnd_qpel16_mc01_c, @function
put_no_rnd_qpel16_mc01_c:
.frame	$sp,704,$31		# vars= 664, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-704
sw	$18,696($sp)
addiu	$18,$sp,24
sw	$17,692($sp)
move	$17,$4
move	$4,$18
sw	$31,700($sp)
sw	$16,688($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$16,$6
.set	macro
.set	reorder

addiu	$4,$sp,432
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,27
addiu	$9,$sp,435
addiu	$12,$sp,411
move	$4,$17
ori	$11,$11,0xfefe
$L2130:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 0($8)  
lwr $10, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($9)  
lwr $5, -3($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 4($8)  
lwr $6, 1($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($9)  
lwr $2, 1($9)  

# 0 "" 2
#NO_APP
xor	$7,$5,$10
xor	$3,$2,$6
and	$7,$7,$11
and	$3,$3,$11
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$10
and	$2,$2,$6
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,24
sw	$5,0($4)
addiu	$9,$9,16
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$12,$L2130
addu	$4,$4,$16
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
addiu	$8,$sp,35
addiu	$9,$sp,443
addiu	$4,$17,8
addiu	$12,$sp,419
ori	$11,$11,0xfefe
$L2131:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 0($8)  
lwr $10, -3($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($9)  
lwr $5, -3($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 4($8)  
lwr $6, 1($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 4($9)  
lwr $2, 1($9)  

# 0 "" 2
#NO_APP
xor	$7,$5,$10
xor	$3,$2,$6
and	$7,$7,$11
and	$3,$3,$11
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$10
and	$2,$2,$6
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,24
sw	$5,0($4)
addiu	$9,$9,16
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$12,$L2131
addu	$4,$4,$16
.set	macro
.set	reorder

lw	$31,700($sp)
lw	$18,696($sp)
lw	$17,692($sp)
lw	$16,688($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,704
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc01_c
.size	put_no_rnd_qpel16_mc01_c, .-put_no_rnd_qpel16_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc33_c
.type	put_qpel16_mc33_c, @function
put_qpel16_mc33_c:
.frame	$sp,1000,$31		# vars= 936, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1000
sw	$19,988($sp)
addiu	$19,$sp,40
sw	$18,984($sp)
move	$18,$4
move	$4,$19
sw	$31,996($sp)
sw	$17,980($sp)
move	$17,$6
sw	$16,976($sp)
addiu	$16,$sp,448
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,992($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
move	$5,$19
sw	$2,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$5,$sp,44
addiu	$19,$sp,720
move	$2,$16
ori	$12,$12,0xfefe
$L2136:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($5)  
lwr $8, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($5)  
lwr $3, 1($5)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$12
and	$7,$7,$12
srl	$9,$9,1
srl	$7,$7,1
or	$8,$8,$11
or	$3,$3,$10
subu	$8,$8,$9
subu	$3,$3,$7
addiu	$2,$2,16
addiu	$5,$5,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2136
sw	$3,-12($2)
.set	macro
.set	reorder

li	$2,24			# 0x18
li	$20,16			# 0x10
addiu	$4,$sp,456
sw	$2,20($sp)
li	$2,17			# 0x11
addiu	$6,$sp,49
sw	$20,16($sp)
li	$7,16			# 0x10
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$5,$4
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$5,$sp,464
move	$4,$18
sw	$20,16($sp)
move	$6,$19
sw	$20,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$20,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
addiu	$5,$sp,472
sw	$20,16($sp)
addiu	$6,$sp,728
sw	$20,20($sp)
sw	$20,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1000
.set	macro
.set	reorder

.end	put_qpel16_mc33_c
.size	put_qpel16_mc33_c, .-put_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc13_c
.type	put_no_rnd_qpel8_mc13_c, @function
put_no_rnd_qpel8_mc13_c:
.frame	$sp,336,$31		# vars= 280, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-336
sw	$17,320($sp)
addiu	$17,$sp,32
sw	$16,316($sp)
move	$16,$4
move	$4,$17
sw	$31,332($sp)
sw	$19,328($sp)
addiu	$19,$sp,176
sw	$18,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$18,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,248
move	$2,$19
move	$6,$17
ori	$12,$12,0xfefe
$L2140:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($6)  
lwr $8, 0($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($6)  
lwr $3, 4($6)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$12
and	$7,$7,$12
srl	$9,$9,1
srl	$7,$7,1
and	$8,$8,$11
and	$3,$3,$10
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,8
addiu	$6,$6,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$4,$2,$L2140
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$10,64			# 0x40
ori	$9,$9,0xfefe
$L2141:
addiu	$5,$2,8
addu	$2,$17,$2
addu	$3,$17,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 219($2)  
lwr $4, 216($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 155($2)  
lwr $7, 144($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 159($2)  
lwr $6, 156($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 223($2)  
lwr $3, 220($2)  

# 0 "" 2
#NO_APP
xor	$8,$4,$7
xor	$2,$3,$6
and	$8,$8,$9
and	$2,$2,$9
srl	$2,$2,1
srl	$8,$8,1
and	$7,$4,$7
and	$3,$3,$6
addu	$3,$2,$3
addu	$7,$8,$7
move	$2,$5
sw	$3,4($16)
sw	$7,0($16)
.set	noreorder
.set	nomacro
bne	$5,$10,$L2141
addu	$16,$16,$18
.set	macro
.set	reorder

lw	$31,332($sp)
lw	$19,328($sp)
lw	$18,324($sp)
lw	$17,320($sp)
lw	$16,316($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,336
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc13_c
.size	put_no_rnd_qpel8_mc13_c, .-put_no_rnd_qpel8_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc11_c
.type	put_no_rnd_qpel8_mc11_c, @function
put_no_rnd_qpel8_mc11_c:
.frame	$sp,336,$31		# vars= 280, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-336
sw	$17,320($sp)
addiu	$17,$sp,32
sw	$16,316($sp)
move	$16,$4
move	$4,$17
sw	$31,332($sp)
sw	$19,328($sp)
addiu	$19,$sp,176
sw	$18,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$18,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,248
move	$2,$19
move	$6,$17
ori	$12,$12,0xfefe
$L2146:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($6)  
lwr $8, 0($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($6)  
lwr $3, 4($6)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$12
and	$7,$7,$12
srl	$9,$9,1
srl	$7,$7,1
and	$8,$8,$11
and	$3,$3,$10
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,8
addiu	$6,$6,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$4,$2,$L2146
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
move	$9,$0
li	$10,64			# 0x40
ori	$11,$11,0xfefe
$L2147:
addiu	$2,$9,3
addiu	$5,$9,4
addiu	$3,$9,7
addu	$4,$17,$9
addu	$2,$17,$2
addu	$3,$17,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 144($2)  
lwr $7, 144($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 216($2)  
lwr $6, 216($4)  

# 0 "" 2
#NO_APP
addu	$5,$17,$5
xor	$8,$6,$7
move	$4,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($3)  
lwr $2, 216($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 144($3)  
lwr $6, 144($5)  

# 0 "" 2
#NO_APP
xor	$3,$2,$6
and	$8,$8,$11
and	$3,$3,$11
srl	$8,$8,1
srl	$3,$3,1
and	$7,$4,$7
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$9,$9,8
sw	$7,0($16)
sw	$2,4($16)
.set	noreorder
.set	nomacro
bne	$9,$10,$L2147
addu	$16,$16,$18
.set	macro
.set	reorder

lw	$31,332($sp)
lw	$19,328($sp)
lw	$18,324($sp)
lw	$17,320($sp)
lw	$16,316($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,336
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc11_c
.size	put_no_rnd_qpel8_mc11_c, .-put_no_rnd_qpel8_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc11_c
.type	put_qpel16_mc11_c, @function
put_qpel16_mc11_c:
.frame	$sp,1008,$31		# vars= 936, regs= 8/0, args= 32, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1008
sw	$19,988($sp)
addiu	$19,$sp,40
sw	$20,992($sp)
li	$20,17			# 0x11
sw	$18,984($sp)
move	$18,$4
sw	$16,976($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,1004($sp)
sw	$17,980($sp)
move	$17,$6
sw	$22,1000($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$21,996($sp)
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$16
sw	$2,16($sp)
li	$2,24			# 0x18
move	$5,$16
move	$6,$19
sw	$2,20($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
addiu	$20,$sp,456
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,51
addiu	$19,$sp,728
move	$2,$20
ori	$5,$5,0xfefe
$L2152:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($4)  
lwr $8, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$5
and	$7,$7,$5
srl	$9,$9,1
srl	$7,$7,1
or	$8,$8,$11
or	$3,$3,$10
subu	$8,$8,$9
subu	$3,$3,$7
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2152
sw	$3,-12($2)
.set	macro
.set	reorder

li	$21,16			# 0x10
addiu	$22,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
sw	$21,16($sp)
move	$6,$22
sw	$21,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$21,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
sw	$21,16($sp)
move	$5,$20
sw	$21,20($sp)
move	$6,$19
sw	$21,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,1004($sp)
lw	$22,1000($sp)
lw	$21,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1008
.set	macro
.set	reorder

.end	put_qpel16_mc11_c
.size	put_qpel16_mc11_c, .-put_qpel16_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc13_c
.type	put_qpel16_mc13_c, @function
put_qpel16_mc13_c:
.frame	$sp,1000,$31		# vars= 936, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1000
sw	$19,988($sp)
addiu	$19,$sp,40
sw	$18,984($sp)
move	$18,$4
move	$4,$19
sw	$31,996($sp)
sw	$17,980($sp)
move	$17,$6
sw	$16,976($sp)
addiu	$16,$sp,448
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,992($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
move	$5,$19
sw	$2,16($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$5,$sp,43
addiu	$19,$sp,720
move	$2,$16
ori	$12,$12,0xfefe
$L2156:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($5)  
lwr $8, -3($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($5)  
lwr $3, 1($5)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$12
and	$7,$7,$12
srl	$9,$9,1
srl	$7,$7,1
or	$8,$8,$11
or	$3,$3,$10
subu	$8,$8,$9
subu	$3,$3,$7
addiu	$2,$2,16
addiu	$5,$5,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2156
sw	$3,-12($2)
.set	macro
.set	reorder

li	$2,24			# 0x18
li	$20,16			# 0x10
addiu	$4,$sp,456
sw	$2,20($sp)
li	$2,17			# 0x11
addiu	$6,$sp,48
sw	$20,16($sp)
li	$7,16			# 0x10
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$5,$4
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$5,$sp,464
move	$4,$18
sw	$20,16($sp)
move	$6,$19
sw	$20,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$20,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
addiu	$5,$sp,472
sw	$20,16($sp)
addiu	$6,$sp,728
sw	$20,20($sp)
sw	$20,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1000
.set	macro
.set	reorder

.end	put_qpel16_mc13_c
.size	put_qpel16_mc13_c, .-put_qpel16_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc12_c
.type	put_no_rnd_qpel16_mc12_c, @function
put_no_rnd_qpel16_mc12_c:
.frame	$sp,736,$31		# vars= 680, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-736
sw	$19,728($sp)
addiu	$19,$sp,32
sw	$18,724($sp)
move	$18,$4
move	$4,$19
sw	$31,732($sp)
sw	$17,720($sp)
move	$17,$6
sw	$16,716($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
addiu	$16,$sp,440
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,35
addiu	$11,$sp,712
move	$2,$16
ori	$10,$10,0xfefe
$L2160:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
xor	$7,$3,$6
and	$9,$9,$10
and	$7,$7,$10
srl	$9,$9,1
srl	$7,$7,1
and	$8,$5,$8
and	$3,$3,$6
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2160
sw	$3,-12($2)
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,448
addiu	$4,$sp,43
addiu	$11,$sp,720
ori	$10,$10,0xfefe
$L2161:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
xor	$7,$3,$6
and	$9,$9,$10
and	$7,$7,$10
srl	$9,$9,1
srl	$7,$7,1
and	$8,$5,$8
and	$3,$3,$6
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2161
sw	$3,-12($2)
.set	macro
.set	reorder

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,732($sp)
lw	$19,728($sp)
lw	$18,724($sp)
lw	$17,720($sp)
lw	$16,716($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,736
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc12_c
.size	put_no_rnd_qpel16_mc12_c, .-put_no_rnd_qpel16_mc12_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc33_c
.type	put_no_rnd_qpel8_mc33_c, @function
put_no_rnd_qpel8_mc33_c:
.frame	$sp,336,$31		# vars= 280, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-336
sw	$17,320($sp)
addiu	$17,$sp,32
sw	$16,316($sp)
move	$16,$4
move	$4,$17
sw	$31,332($sp)
sw	$19,328($sp)
addiu	$19,$sp,176
sw	$18,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$18,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,36
addiu	$4,$sp,248
move	$2,$19
ori	$12,$12,0xfefe
$L2166:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($6)  
lwr $8, -3($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($6)  
lwr $3, 1($6)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$12
and	$7,$7,$12
srl	$9,$9,1
srl	$7,$7,1
and	$8,$8,$11
and	$3,$3,$10
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,8
addiu	$6,$6,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$4,$2,$L2166
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$10,64			# 0x40
ori	$9,$9,0xfefe
$L2167:
addiu	$5,$2,8
addu	$2,$17,$2
addu	$3,$17,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 219($2)  
lwr $4, 216($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 155($2)  
lwr $7, 144($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 159($2)  
lwr $6, 156($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 223($2)  
lwr $3, 220($2)  

# 0 "" 2
#NO_APP
xor	$8,$4,$7
xor	$2,$3,$6
and	$8,$8,$9
and	$2,$2,$9
srl	$2,$2,1
srl	$8,$8,1
and	$7,$4,$7
and	$3,$3,$6
addu	$3,$2,$3
addu	$7,$8,$7
move	$2,$5
sw	$3,4($16)
sw	$7,0($16)
.set	noreorder
.set	nomacro
bne	$5,$10,$L2167
addu	$16,$16,$18
.set	macro
.set	reorder

lw	$31,332($sp)
lw	$19,328($sp)
lw	$18,324($sp)
lw	$17,320($sp)
lw	$16,316($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,336
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc33_c
.size	put_no_rnd_qpel8_mc33_c, .-put_no_rnd_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc32_c
.type	put_no_rnd_qpel16_mc32_c, @function
put_no_rnd_qpel16_mc32_c:
.frame	$sp,736,$31		# vars= 680, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-736
sw	$19,728($sp)
addiu	$19,$sp,32
sw	$18,724($sp)
move	$18,$4
move	$4,$19
sw	$31,732($sp)
sw	$17,720($sp)
move	$17,$6
sw	$16,716($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
addiu	$16,$sp,440
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,36
addiu	$11,$sp,712
move	$2,$16
ori	$10,$10,0xfefe
$L2172:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
xor	$7,$3,$6
and	$9,$9,$10
and	$7,$7,$10
srl	$9,$9,1
srl	$7,$7,1
and	$8,$5,$8
and	$3,$3,$6
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2172
sw	$3,-12($2)
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,448
addiu	$4,$sp,44
addiu	$11,$sp,720
ori	$10,$10,0xfefe
$L2173:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
xor	$7,$3,$6
and	$9,$9,$10
and	$7,$7,$10
srl	$9,$9,1
srl	$7,$7,1
and	$8,$5,$8
and	$3,$3,$6
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2173
sw	$3,-12($2)
.set	macro
.set	reorder

li	$7,16			# 0x10
move	$4,$18
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$6,$17
.set	macro
.set	reorder

lw	$31,732($sp)
lw	$19,728($sp)
lw	$18,724($sp)
lw	$17,720($sp)
lw	$16,716($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,736
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc32_c
.size	put_no_rnd_qpel16_mc32_c, .-put_no_rnd_qpel16_mc32_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_qpel16_mc31_c
.type	put_qpel16_mc31_c, @function
put_qpel16_mc31_c:
.frame	$sp,1016,$31		# vars= 936, regs= 9/0, args= 32, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1016
sw	$17,984($sp)
addiu	$17,$sp,40
sw	$21,1000($sp)
li	$21,17			# 0x11
sw	$20,996($sp)
addiu	$20,$sp,448
sw	$19,992($sp)
move	$19,$4
move	$4,$17
sw	$31,1012($sp)
sw	$23,1008($sp)
li	$23,16			# 0x10
sw	$22,1004($sp)
li	$22,24			# 0x18
sw	$18,988($sp)
move	$18,$6
sw	$16,980($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
addiu	$16,$sp,456
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$21,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

addiu	$6,$sp,41
li	$7,16			# 0x10
sw	$23,16($sp)
move	$4,$20
sw	$22,20($sp)
move	$5,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$21,24($sp)
.set	macro
.set	reorder

addiu	$6,$sp,49
li	$7,16			# 0x10
sw	$23,16($sp)
move	$4,$16
sw	$22,20($sp)
move	$5,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$21,24($sp)
.set	macro
.set	reorder

addiu	$4,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$6,256			# 0x100
move	$4,$19
ori	$5,$5,0xfefe
$L2178:
addiu	$3,$2,3
addiu	$11,$2,4
addiu	$9,$2,7
addu	$7,$17,$2
addu	$3,$17,$3
addu	$9,$17,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 408($3)  
lwr $12, 408($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 680($3)  
lwr $8, 680($7)  

# 0 "" 2
#NO_APP
addu	$11,$17,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 408($9)  
lwr $7, 408($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($9)  
lwr $3, 680($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$7
and	$10,$10,$5
and	$9,$9,$5
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$7
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$6,$L2178
addu	$4,$4,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$4,$19,8
addiu	$6,$sp,728
sw	$2,16($sp)
move	$5,$16
sw	$2,20($sp)
move	$7,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,1012($sp)
lw	$23,1008($sp)
lw	$22,1004($sp)
lw	$21,1000($sp)
lw	$20,996($sp)
lw	$19,992($sp)
lw	$18,988($sp)
lw	$17,984($sp)
lw	$16,980($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1016
.set	macro
.set	reorder

.end	put_qpel16_mc31_c
.size	put_qpel16_mc31_c, .-put_qpel16_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc31_c
.type	avg_qpel16_mc31_c, @function
avg_qpel16_mc31_c:
.frame	$sp,1008,$31		# vars= 936, regs= 8/0, args= 32, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1008
sw	$19,988($sp)
addiu	$19,$sp,40
sw	$20,992($sp)
li	$20,17			# 0x11
sw	$18,984($sp)
move	$18,$4
sw	$16,976($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,1004($sp)
sw	$17,980($sp)
move	$17,$6
sw	$22,1000($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$21,996($sp)
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$6,$sp,41
sw	$20,24($sp)
li	$7,16			# 0x10
sw	$2,16($sp)
li	$2,24			# 0x18
move	$4,$16
move	$5,$16
sw	$2,20($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
addiu	$20,$sp,456
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,52
addiu	$19,$sp,728
move	$2,$20
ori	$5,$5,0xfefe
$L2182:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($4)  
lwr $8, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$5
and	$7,$7,$5
srl	$9,$9,1
srl	$7,$7,1
or	$8,$8,$11
or	$3,$3,$10
subu	$8,$8,$9
subu	$3,$3,$7
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2182
sw	$3,-12($2)
.set	macro
.set	reorder

li	$21,16			# 0x10
addiu	$22,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
sw	$21,16($sp)
move	$6,$22
sw	$21,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$21,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
sw	$21,16($sp)
move	$5,$20
sw	$21,20($sp)
move	$6,$19
sw	$21,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,1004($sp)
lw	$22,1000($sp)
lw	$21,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1008
.set	macro
.set	reorder

.end	avg_qpel16_mc31_c
.size	avg_qpel16_mc31_c, .-avg_qpel16_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel8_mc31_c
.type	put_no_rnd_qpel8_mc31_c, @function
put_no_rnd_qpel8_mc31_c:
.frame	$sp,336,$31		# vars= 280, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-336
sw	$17,320($sp)
addiu	$17,$sp,32
sw	$16,316($sp)
move	$16,$4
move	$4,$17
sw	$31,332($sp)
sw	$19,328($sp)
addiu	$19,$sp,176
sw	$18,324($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$18,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$12,-16908288			# 0xfffffffffefe0000
addiu	$6,$sp,36
addiu	$4,$sp,248
move	$2,$19
ori	$12,$12,0xfefe
$L2186:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($6)  
lwr $8, -3($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($6)  
lwr $3, 1($6)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$12
and	$7,$7,$12
srl	$9,$9,1
srl	$7,$7,1
and	$8,$8,$11
and	$3,$3,$10
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,8
addiu	$6,$6,16
sw	$8,-8($2)
.set	noreorder
.set	nomacro
bne	$4,$2,$L2186
sw	$3,-4($2)
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
move	$9,$0
li	$10,64			# 0x40
ori	$11,$11,0xfefe
$L2187:
addiu	$2,$9,3
addiu	$5,$9,4
addiu	$3,$9,7
addu	$4,$17,$9
addu	$2,$17,$2
addu	$3,$17,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 144($2)  
lwr $7, 144($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 216($2)  
lwr $6, 216($4)  

# 0 "" 2
#NO_APP
addu	$5,$17,$5
xor	$8,$6,$7
move	$4,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($3)  
lwr $2, 216($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 144($3)  
lwr $6, 144($5)  

# 0 "" 2
#NO_APP
xor	$3,$2,$6
and	$8,$8,$11
and	$3,$3,$11
srl	$8,$8,1
srl	$3,$3,1
and	$7,$4,$7
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$9,$9,8
sw	$7,0($16)
sw	$2,4($16)
.set	noreorder
.set	nomacro
bne	$9,$10,$L2187
addu	$16,$16,$18
.set	macro
.set	reorder

lw	$31,332($sp)
lw	$19,328($sp)
lw	$18,324($sp)
lw	$17,320($sp)
lw	$16,316($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,336
.set	macro
.set	reorder

.end	put_no_rnd_qpel8_mc31_c
.size	put_no_rnd_qpel8_mc31_c, .-put_no_rnd_qpel8_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc33_c
.type	avg_qpel16_mc33_c, @function
avg_qpel16_mc33_c:
.frame	$sp,1008,$31		# vars= 936, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1008
sw	$19,992($sp)
addiu	$19,$sp,40
sw	$20,996($sp)
li	$20,17			# 0x11
sw	$18,988($sp)
move	$18,$4
sw	$16,980($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,1004($sp)
sw	$17,984($sp)
move	$17,$6
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$21,1000($sp)
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$6,$sp,41
sw	$20,24($sp)
li	$7,16			# 0x10
sw	$2,16($sp)
li	$2,24			# 0x18
move	$4,$16
move	$5,$16
sw	$2,20($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
addiu	$19,$sp,728
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$4,$sp,52
ori	$11,$11,0xfefe
$L2192:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($2)  
lwr $10, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 7($2)  
lwr $9, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($4)  
lwr $8, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$7,$8,$10
xor	$5,$3,$9
and	$7,$7,$11
and	$5,$5,$11
srl	$7,$7,1
srl	$5,$5,1
or	$8,$8,$10
or	$3,$3,$9
subu	$8,$8,$7
subu	$3,$3,$5
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2192
sw	$3,-12($2)
.set	macro
.set	reorder

addiu	$21,$sp,720
li	$20,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$5,$sp,464
move	$4,$18
sw	$20,16($sp)
move	$6,$21
sw	$20,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$20,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
addiu	$5,$sp,472
sw	$20,16($sp)
sw	$20,20($sp)
move	$6,$19
sw	$20,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,1004($sp)
lw	$21,1000($sp)
lw	$20,996($sp)
lw	$19,992($sp)
lw	$18,988($sp)
lw	$17,984($sp)
lw	$16,980($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1008
.set	macro
.set	reorder

.end	avg_qpel16_mc33_c
.size	avg_qpel16_mc33_c, .-avg_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc11_c
.type	avg_qpel16_mc11_c, @function
avg_qpel16_mc11_c:
.frame	$sp,1008,$31		# vars= 936, regs= 8/0, args= 32, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1008
sw	$19,988($sp)
addiu	$19,$sp,40
sw	$20,992($sp)
li	$20,17			# 0x11
sw	$18,984($sp)
move	$18,$4
sw	$16,976($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,1004($sp)
sw	$17,980($sp)
move	$17,$6
sw	$22,1000($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$21,996($sp)
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$16
sw	$2,16($sp)
li	$2,24			# 0x18
move	$5,$16
move	$6,$19
sw	$2,20($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
addiu	$20,$sp,456
.set	macro
.set	reorder

li	$5,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,51
addiu	$19,$sp,728
move	$2,$20
ori	$5,$5,0xfefe
$L2196:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($2)  
lwr $11, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($4)  
lwr $8, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$8,$11
xor	$7,$3,$10
and	$9,$9,$5
and	$7,$7,$5
srl	$9,$9,1
srl	$7,$7,1
or	$8,$8,$11
or	$3,$3,$10
subu	$8,$8,$9
subu	$3,$3,$7
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2196
sw	$3,-12($2)
.set	macro
.set	reorder

li	$21,16			# 0x10
addiu	$22,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

move	$4,$18
move	$5,$16
sw	$21,16($sp)
move	$6,$22
sw	$21,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$21,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
sw	$21,16($sp)
move	$5,$20
sw	$21,20($sp)
move	$6,$19
sw	$21,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,1004($sp)
lw	$22,1000($sp)
lw	$21,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1008
.set	macro
.set	reorder

.end	avg_qpel16_mc11_c
.size	avg_qpel16_mc11_c, .-avg_qpel16_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel16_mc13_c
.type	avg_qpel16_mc13_c, @function
avg_qpel16_mc13_c:
.frame	$sp,1008,$31		# vars= 936, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1008
sw	$19,992($sp)
addiu	$19,$sp,40
sw	$20,996($sp)
li	$20,17			# 0x11
sw	$18,988($sp)
move	$18,$4
sw	$16,980($sp)
move	$4,$19
addiu	$16,$sp,448
sw	$31,1004($sp)
sw	$17,984($sp)
move	$17,$6
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$21,1000($sp)
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$16
sw	$2,16($sp)
li	$2,24			# 0x18
move	$6,$19
move	$5,$16
sw	$2,20($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
addiu	$19,$sp,728
.set	macro
.set	reorder

li	$11,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$4,$sp,51
ori	$11,$11,0xfefe
$L2200:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($2)  
lwr $10, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 7($2)  
lwr $9, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 0($4)  
lwr $8, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$7,$8,$10
xor	$5,$3,$9
and	$7,$7,$11
and	$5,$5,$11
srl	$7,$7,1
srl	$5,$5,1
or	$8,$8,$10
or	$3,$3,$9
subu	$8,$8,$7
subu	$3,$3,$5
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$19,$2,$L2200
sw	$3,-12($2)
.set	macro
.set	reorder

addiu	$21,$sp,720
li	$20,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$5,$sp,464
move	$4,$18
sw	$20,16($sp)
move	$6,$21
sw	$20,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$20,24($sp)
.set	macro
.set	reorder

addiu	$4,$18,8
addiu	$5,$sp,472
sw	$20,16($sp)
sw	$20,20($sp)
move	$6,$19
sw	$20,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,1004($sp)
lw	$21,1000($sp)
lw	$20,996($sp)
lw	$19,992($sp)
lw	$18,988($sp)
lw	$17,984($sp)
lw	$16,980($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1008
.set	macro
.set	reorder

.end	avg_qpel16_mc13_c
.size	avg_qpel16_mc13_c, .-avg_qpel16_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc01_c
.type	avg_qpel8_mc01_c, @function
avg_qpel8_mc01_c:
.frame	$sp,272,$31		# vars= 208, regs= 5/0, args= 32, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-272
sw	$18,260($sp)
addiu	$18,$sp,40
addiu	$7,$sp,184
sw	$19,264($sp)
sw	$17,256($sp)
move	$19,$4
sw	$16,252($sp)
move	$17,$6
sw	$31,268($sp)
move	$16,$18
$L2204:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$9,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$7,$L2204
addu	$5,$5,$17
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,16			# 0x10
move	$4,$19
sw	$2,20($sp)
move	$5,$18
sw	$3,16($sp)
move	$6,$16
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,268($sp)
lw	$19,264($sp)
lw	$18,260($sp)
lw	$17,256($sp)
lw	$16,252($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,272
.set	macro
.set	reorder

.end	avg_qpel8_mc01_c
.size	avg_qpel8_mc01_c, .-avg_qpel8_mc01_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_qpel8_mc03_c
.type	avg_qpel8_mc03_c, @function
avg_qpel8_mc03_c:
.frame	$sp,264,$31		# vars= 208, regs= 4/0, args= 32, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-264
addiu	$10,$sp,40
sw	$18,256($sp)
addiu	$7,$sp,184
sw	$17,252($sp)
sw	$16,248($sp)
move	$18,$4
sw	$31,260($sp)
move	$17,$6
move	$16,$10
$L2208:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$9,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$7,$L2208
addu	$5,$5,$17
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$5,$10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,16			# 0x10
addiu	$5,$sp,56
sw	$2,20($sp)
move	$4,$18
sw	$3,16($sp)
move	$6,$16
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
move	$7,$17
.set	macro
.set	reorder

lw	$31,260($sp)
lw	$18,256($sp)
lw	$17,252($sp)
lw	$16,248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,264
.set	macro
.set	reorder

.end	avg_qpel8_mc03_c
.size	avg_qpel8_mc03_c, .-avg_qpel8_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel8_mc33_c
.type	avg_rv40_qpel8_mc33_c, @function
avg_rv40_qpel8_mc33_c:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-32
li	$12,50528256			# 0x3030000
sw	$16,0($sp)
li	$24,252641280			# 0xf0f0000
li	$16,33685504			# 0x2020000
sw	$19,12($sp)
li	$11,-50593792			# 0xfffffffffcfc0000
sw	$18,8($sp)
li	$15,-16908288			# 0xfffffffffefe0000
sw	$17,4($sp)
sll	$25,$6,1
sw	$23,28($sp)
li	$19,4			# 0x4
sw	$22,24($sp)
li	$17,2			# 0x2
sw	$21,20($sp)
addiu	$12,$12,771
sw	$20,16($sp)
addiu	$16,$16,514
addiu	$24,$24,3855
li	$18,1			# 0x1
ori	$11,$11,0xfcfc
ori	$15,$15,0xfefe
$L2213:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 4($5)  
lwr $22, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$12
and	$7,$22,$12
and	$3,$3,$11
and	$22,$22,$11
addu	$2,$2,$7
srl	$22,$22,2
srl	$3,$3,2
addu	$9,$5,$6
addu	$2,$2,$16
addu	$22,$22,$3
addu	$10,$9,$6
li	$14,4			# 0x4
move	$13,$4
$L2212:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($9)  
lwr $20, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 4($9)  
lwr $7, 1($9)  

# 0 "" 2
#NO_APP
and	$3,$20,$12
lw	$21,0($13)
and	$8,$7,$12
addu	$8,$8,$3
and	$7,$7,$11
addu	$3,$2,$8
and	$20,$20,$11
srl	$20,$20,2
srl	$7,$7,2
srl	$3,$3,2
addu	$7,$7,$20
and	$3,$3,$24
addu	$3,$3,$7
addu	$20,$13,$6
addu	$2,$3,$22
addiu	$14,$14,-1
xor	$3,$2,$21
and	$3,$3,$15
srl	$3,$3,1
or	$2,$2,$21
subu	$2,$2,$3
addu	$9,$9,$25
sw	$2,0($13)
addu	$13,$20,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($10)  
lwr $3, 0($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 4($10)  
lwr $22, 1($10)  

# 0 "" 2
#NO_APP
and	$2,$3,$12
lw	$21,0($20)
and	$23,$22,$12
addu	$2,$2,$23
and	$22,$22,$11
addu	$2,$2,$16
and	$3,$3,$11
srl	$3,$3,2
srl	$22,$22,2
addu	$8,$2,$8
addu	$22,$22,$3
srl	$8,$8,2
addu	$3,$22,$7
and	$8,$8,$24
addu	$7,$3,$8
addu	$10,$10,$25
xor	$3,$7,$21
and	$3,$3,$15
srl	$3,$3,1
or	$7,$7,$21
subu	$7,$7,$3
.set	noreorder
.set	nomacro
bne	$14,$0,$L2212
sw	$7,0($20)
.set	macro
.set	reorder

addiu	$5,$5,4
.set	noreorder
.set	nomacro
bne	$17,$18,$L2214
addu	$4,$4,$19
.set	macro
.set	reorder

lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,32
.set	macro
.set	reorder

$L2214:
.option	pic0
.set	noreorder
.set	nomacro
j	$L2213
.option	pic2
li	$17,1			# 0x1
.set	macro
.set	reorder

.end	avg_rv40_qpel8_mc33_c
.size	avg_rv40_qpel8_mc33_c, .-avg_rv40_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel8_mc33_c
.type	put_rv40_qpel8_mc33_c, @function
put_rv40_qpel8_mc33_c:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-24
li	$11,50528256			# 0x3030000
li	$24,33685504			# 0x2020000
sw	$17,4($sp)
li	$14,252641280			# 0xf0f0000
sw	$16,0($sp)
li	$10,-50593792			# 0xfffffffffcfc0000
sw	$21,20($sp)
sll	$15,$6,1
sw	$20,16($sp)
li	$17,4			# 0x4
sw	$19,12($sp)
li	$25,2			# 0x2
sw	$18,8($sp)
addiu	$11,$11,771
addiu	$24,$24,514
addiu	$14,$14,3855
li	$16,1			# 0x1
ori	$10,$10,0xfcfc
$L2219:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($5)  
lwr $18, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$11
and	$7,$18,$11
and	$3,$3,$10
and	$18,$18,$10
addu	$2,$2,$7
srl	$18,$18,2
srl	$3,$3,2
addu	$8,$5,$6
addu	$2,$2,$24
addu	$18,$18,$3
addu	$9,$8,$6
li	$13,4			# 0x4
move	$12,$4
$L2218:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($8)  
lwr $19, 0($8)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($8)  
lwr $3, 1($8)  

# 0 "" 2
#NO_APP
and	$7,$19,$11
and	$20,$3,$11
addu	$7,$20,$7
and	$3,$3,$10
addu	$2,$2,$7
and	$19,$19,$10
srl	$19,$19,2
srl	$3,$3,2
srl	$2,$2,2
addu	$3,$3,$19
and	$2,$2,$14
addu	$2,$2,$3
addu	$19,$12,$6
addu	$2,$2,$18
addiu	$13,$13,-1
addu	$8,$8,$15
sw	$2,0($12)
addu	$12,$19,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($9)  
lwr $20, 0($9)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($9)  
lwr $18, 1($9)  

# 0 "" 2
#NO_APP
and	$2,$20,$11
and	$21,$18,$11
addu	$2,$2,$21
and	$18,$18,$10
addu	$2,$2,$24
and	$20,$20,$10
srl	$18,$18,2
srl	$20,$20,2
addu	$7,$2,$7
addu	$18,$18,$20
srl	$7,$7,2
addu	$3,$18,$3
and	$7,$7,$14
addu	$7,$3,$7
addu	$9,$9,$15
.set	noreorder
.set	nomacro
bne	$13,$0,$L2218
sw	$7,0($19)
.set	macro
.set	reorder

addiu	$5,$5,4
.set	noreorder
.set	nomacro
bne	$25,$16,$L2220
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,24
.set	macro
.set	reorder

$L2220:
.option	pic0
.set	noreorder
.set	nomacro
j	$L2219
.option	pic2
li	$25,1			# 0x1
.set	macro
.set	reorder

.end	put_rv40_qpel8_mc33_c
.size	put_rv40_qpel8_mc33_c, .-put_rv40_qpel8_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc33_c
.type	put_no_rnd_qpel16_mc33_c, @function
put_no_rnd_qpel16_mc33_c:
.frame	$sp,1008,$31		# vars= 936, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1008
sw	$18,988($sp)
addiu	$18,$sp,40
sw	$17,984($sp)
move	$17,$4
move	$4,$18
sw	$31,1004($sp)
sw	$19,992($sp)
addiu	$19,$sp,448
sw	$16,980($sp)
move	$16,$6
sw	$21,1000($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,996($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
addiu	$4,$sp,44
addiu	$18,$sp,720
move	$2,$19
ori	$10,$10,0xfefe
$L2224:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($2)  
lwr $8, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$9,$5,$8
xor	$7,$3,$6
and	$9,$9,$10
and	$7,$7,$10
srl	$9,$9,1
srl	$7,$7,1
and	$8,$5,$8
and	$3,$3,$6
addu	$8,$9,$8
addu	$3,$7,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$8,-16($2)
.set	noreorder
.set	nomacro
bne	$18,$2,$L2224
sw	$3,-12($2)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$4,$sp,52
addiu	$20,$sp,728
ori	$6,$6,0xfefe
$L2225:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 7($2)  
lwr $10, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$7,$5,$9
xor	$8,$3,$10
and	$7,$7,$6
and	$8,$8,$6
srl	$7,$7,1
srl	$8,$8,1
and	$9,$5,$9
and	$3,$3,$10
addu	$9,$7,$9
addu	$3,$8,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$9,-16($2)
.set	noreorder
.set	nomacro
bne	$20,$2,$L2225
sw	$3,-12($2)
.set	macro
.set	reorder

li	$21,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

addiu	$5,$sp,464
move	$4,$17
sw	$21,16($sp)
move	$6,$18
sw	$21,20($sp)
move	$7,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
sw	$21,24($sp)
.set	macro
.set	reorder

addiu	$4,$17,8
addiu	$5,$sp,472
sw	$21,16($sp)
sw	$21,20($sp)
move	$6,$20
sw	$21,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
move	$7,$16
.set	macro
.set	reorder

lw	$31,1004($sp)
lw	$21,1000($sp)
lw	$20,996($sp)
lw	$19,992($sp)
lw	$18,988($sp)
lw	$17,984($sp)
lw	$16,980($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1008
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc33_c
.size	put_no_rnd_qpel16_mc33_c, .-put_no_rnd_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc13_c
.type	put_no_rnd_qpel16_mc13_c, @function
put_no_rnd_qpel16_mc13_c:
.frame	$sp,1000,$31		# vars= 936, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1000
sw	$18,984($sp)
addiu	$18,$sp,40
sw	$20,992($sp)
li	$20,17			# 0x11
sw	$17,980($sp)
addiu	$17,$sp,448
sw	$16,976($sp)
move	$16,$4
move	$4,$18
sw	$31,996($sp)
sw	$19,988($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$19,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$17
sw	$2,16($sp)
li	$2,24			# 0x18
move	$5,$17
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$4,$sp,51
addiu	$11,$sp,728
ori	$7,$7,0xfefe
$L2230:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$10,$5,$9
xor	$8,$3,$6
and	$10,$10,$7
and	$8,$8,$7
srl	$10,$10,1
srl	$8,$8,1
and	$9,$5,$9
and	$3,$3,$6
addu	$9,$10,$9
addu	$3,$8,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$9,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2230
sw	$3,-12($2)
.set	macro
.set	reorder

addiu	$20,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
addiu	$5,$sp,464
move	$4,$16
move	$7,$19
sw	$2,16($sp)
move	$6,$20
sw	$2,20($sp)
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
addiu	$16,$16,8
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
move	$5,$0
li	$10,256			# 0x100
ori	$7,$7,0xfefe
$L2231:
addu	$2,$18,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 435($2)  
lwr $8, 432($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 691($2)  
lwr $4, 688($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 439($2)  
lwr $6, 436($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 695($2)  
lwr $3, 692($2)  

# 0 "" 2
#NO_APP
xor	$9,$4,$8
move	$2,$3
xor	$3,$3,$6
and	$9,$9,$7
and	$3,$3,$7
srl	$9,$9,1
srl	$3,$3,1
and	$8,$4,$8
and	$2,$2,$6
addu	$8,$9,$8
addu	$2,$3,$2
addiu	$5,$5,16
sw	$8,0($16)
sw	$2,4($16)
.set	noreorder
.set	nomacro
bne	$5,$10,$L2231
addu	$16,$16,$19
.set	macro
.set	reorder

lw	$31,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1000
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc13_c
.size	put_no_rnd_qpel16_mc13_c, .-put_no_rnd_qpel16_mc13_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc31_c
.type	put_no_rnd_qpel16_mc31_c, @function
put_no_rnd_qpel16_mc31_c:
.frame	$sp,1000,$31		# vars= 936, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1000
sw	$18,984($sp)
addiu	$18,$sp,40
sw	$20,992($sp)
li	$20,17			# 0x11
sw	$17,980($sp)
addiu	$17,$sp,448
sw	$16,976($sp)
move	$16,$4
move	$4,$18
sw	$31,996($sp)
sw	$19,988($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$19,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
addiu	$6,$sp,41
sw	$2,16($sp)
li	$2,24			# 0x18
move	$4,$17
move	$5,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$4,$sp,52
addiu	$11,$sp,728
ori	$7,$7,0xfefe
$L2236:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$10,$5,$9
xor	$8,$3,$6
and	$10,$10,$7
and	$8,$8,$7
srl	$10,$10,1
srl	$8,$8,1
and	$9,$5,$9
and	$3,$3,$6
addu	$9,$10,$9
addu	$3,$8,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$9,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2236
sw	$3,-12($2)
.set	macro
.set	reorder

addiu	$20,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$16
move	$5,$17
sw	$2,16($sp)
move	$6,$20
sw	$2,20($sp)
move	$7,$19
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
addiu	$16,$16,8
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
move	$4,$0
li	$11,256			# 0x100
ori	$10,$10,0xfefe
$L2237:
addiu	$7,$4,8
addiu	$2,$4,11
addiu	$6,$4,12
addiu	$3,$4,15
addu	$2,$18,$2
addu	$7,$18,$7
addu	$3,$18,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 408($2)  
lwr $8, 408($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 680($2)  
lwr $5, 680($7)  

# 0 "" 2
#NO_APP
addu	$6,$18,$6
xor	$9,$5,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 408($3)  
lwr $7, 408($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($3)  
lwr $2, 680($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$7
and	$9,$9,$10
and	$3,$3,$10
srl	$9,$9,1
srl	$3,$3,1
and	$8,$5,$8
and	$2,$2,$7
addu	$8,$9,$8
addu	$2,$3,$2
addiu	$4,$4,16
sw	$8,0($16)
sw	$2,4($16)
.set	noreorder
.set	nomacro
bne	$4,$11,$L2237
addu	$16,$16,$19
.set	macro
.set	reorder

lw	$31,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1000
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc31_c
.size	put_no_rnd_qpel16_mc31_c, .-put_no_rnd_qpel16_mc31_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_no_rnd_qpel16_mc11_c
.type	put_no_rnd_qpel16_mc11_c, @function
put_no_rnd_qpel16_mc11_c:
.frame	$sp,1000,$31		# vars= 936, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1000
sw	$18,984($sp)
addiu	$18,$sp,40
sw	$20,992($sp)
li	$20,17			# 0x11
sw	$17,980($sp)
addiu	$17,$sp,448
sw	$16,976($sp)
move	$16,$4
move	$4,$18
sw	$31,996($sp)
sw	$19,988($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$19,$6
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$20,16($sp)
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$7,16			# 0x10
sw	$20,24($sp)
move	$4,$17
sw	$2,16($sp)
li	$2,24			# 0x18
move	$5,$17
move	$6,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

li	$7,-16908288			# 0xfffffffffefe0000
addiu	$2,$sp,456
addiu	$4,$sp,51
addiu	$11,$sp,728
ori	$7,$7,0xfefe
$L2242:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($2)  
lwr $9, 0($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 7($2)  
lwr $6, 4($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 0($4)  
lwr $5, -3($4)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($4)  
lwr $3, 1($4)  

# 0 "" 2
#NO_APP
xor	$10,$5,$9
xor	$8,$3,$6
and	$10,$10,$7
and	$8,$8,$7
srl	$10,$10,1
srl	$8,$8,1
and	$9,$5,$9
and	$3,$3,$6
addu	$9,$10,$9
addu	$3,$8,$3
addiu	$2,$2,16
addiu	$4,$4,24
sw	$9,-16($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2242
sw	$3,-12($2)
.set	macro
.set	reorder

addiu	$20,$sp,720
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$17
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$16
move	$5,$17
sw	$2,16($sp)
move	$6,$20
sw	$2,20($sp)
move	$7,$19
sw	$2,24($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l2
.option	pic2
addiu	$16,$16,8
.set	macro
.set	reorder

li	$10,-16908288			# 0xfffffffffefe0000
move	$4,$0
li	$11,256			# 0x100
ori	$10,$10,0xfefe
$L2243:
addiu	$7,$4,8
addiu	$2,$4,11
addiu	$6,$4,12
addiu	$3,$4,15
addu	$2,$18,$2
addu	$7,$18,$7
addu	$3,$18,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 408($2)  
lwr $8, 408($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 680($2)  
lwr $5, 680($7)  

# 0 "" 2
#NO_APP
addu	$6,$18,$6
xor	$9,$5,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 408($3)  
lwr $7, 408($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($3)  
lwr $2, 680($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$7
and	$9,$9,$10
and	$3,$3,$10
srl	$9,$9,1
srl	$3,$3,1
and	$8,$5,$8
and	$2,$2,$7
addu	$8,$9,$8
addu	$2,$3,$2
addiu	$4,$4,16
sw	$8,0($16)
sw	$2,4($16)
.set	noreorder
.set	nomacro
bne	$4,$11,$L2243
addu	$16,$16,$19
.set	macro
.set	reorder

lw	$31,996($sp)
lw	$20,992($sp)
lw	$19,988($sp)
lw	$18,984($sp)
lw	$17,980($sp)
lw	$16,976($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1000
.set	macro
.set	reorder

.end	put_no_rnd_qpel16_mc11_c
.size	put_no_rnd_qpel16_mc11_c, .-put_no_rnd_qpel16_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_rv40_qpel16_mc33_c
.type	avg_rv40_qpel16_mc33_c, @function
avg_rv40_qpel16_mc33_c:
.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-56
li	$7,16			# 0x10
sw	$18,36($sp)
move	$18,$4
sw	$17,32($sp)
move	$17,$5
sw	$16,28($sp)
move	$16,$6
sw	$31,52($sp)
sw	$21,48($sp)
sw	$20,44($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_xy2_c
.option	pic2
sw	$19,40($sp)
.set	macro
.set	reorder

li	$9,50528256			# 0x3030000
li	$13,33685504			# 0x2020000
li	$12,252641280			# 0xf0f0000
li	$8,-50593792			# 0xfffffffffcfc0000
li	$11,-16908288			# 0xfffffffffefe0000
addiu	$4,$18,8
addiu	$5,$17,8
li	$24,4			# 0x4
li	$14,2			# 0x2
addiu	$9,$9,771
addiu	$13,$13,514
addiu	$12,$12,3855
li	$15,1			# 0x1
ori	$8,$8,0xfcfc
ori	$11,$11,0xfefe
$L2249:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($5)  
lwr $18, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$9
and	$6,$18,$9
and	$3,$3,$8
and	$18,$18,$8
addu	$2,$2,$6
srl	$18,$18,2
srl	$3,$3,2
addu	$2,$2,$13
addu	$18,$18,$3
addu	$17,$5,$16
li	$10,8			# 0x8
move	$6,$4
$L2248:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $19, 3($17)  
lwr $19, 0($17)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 4($17)  
lwr $7, 1($17)  

# 0 "" 2
#NO_APP
and	$3,$19,$9
lw	$20,0($6)
and	$25,$7,$9
addu	$25,$25,$3
and	$7,$7,$8
addu	$3,$2,$25
and	$19,$19,$8
srl	$19,$19,2
srl	$7,$7,2
srl	$3,$3,2
addu	$7,$7,$19
and	$3,$3,$12
addu	$3,$3,$7
addu	$21,$17,$16
addu	$2,$3,$18
addu	$17,$21,$16
xor	$3,$2,$20
and	$3,$3,$11
srl	$3,$3,1
or	$2,$2,$20
subu	$2,$2,$3
addu	$19,$6,$16
addiu	$10,$10,-1
sw	$2,0($6)
addu	$6,$19,$16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($21)  
lwr $3, 0($21)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $18, 4($21)  
lwr $18, 1($21)  

# 0 "" 2
#NO_APP
and	$2,$3,$9
lw	$20,0($19)
and	$21,$18,$9
addu	$2,$2,$21
and	$18,$18,$8
addu	$2,$2,$13
and	$3,$3,$8
srl	$3,$3,2
srl	$18,$18,2
addu	$25,$2,$25
addu	$18,$18,$3
srl	$25,$25,2
addu	$7,$18,$7
and	$25,$25,$12
addu	$3,$7,$25
xor	$7,$3,$20
and	$7,$7,$11
srl	$7,$7,1
or	$3,$3,$20
subu	$3,$3,$7
.set	noreorder
.set	nomacro
bne	$10,$0,$L2248
sw	$3,0($19)
.set	macro
.set	reorder

addiu	$5,$5,4
.set	noreorder
.set	nomacro
bne	$14,$15,$L2250
addu	$4,$4,$24
.set	macro
.set	reorder

lw	$31,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

$L2250:
.option	pic0
.set	noreorder
.set	nomacro
j	$L2249
.option	pic2
li	$14,1			# 0x1
.set	macro
.set	reorder

.end	avg_rv40_qpel16_mc33_c
.size	avg_rv40_qpel16_mc33_c, .-avg_rv40_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_rv40_qpel16_mc33_c
.type	put_rv40_qpel16_mc33_c, @function
put_rv40_qpel16_mc33_c:
.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
.mask	0x80070000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-40
li	$7,16			# 0x10
sw	$18,32($sp)
move	$18,$4
sw	$17,28($sp)
move	$17,$5
sw	$16,24($sp)
sw	$31,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_xy2_c
.option	pic2
move	$16,$6
.set	macro
.set	reorder

li	$10,50528256			# 0x3030000
li	$12,33685504			# 0x2020000
li	$11,252641280			# 0xf0f0000
li	$9,-50593792			# 0xfffffffffcfc0000
addiu	$4,$18,8
addiu	$5,$17,8
li	$15,4			# 0x4
li	$13,2			# 0x2
addiu	$10,$10,771
addiu	$12,$12,514
addiu	$11,$11,3855
li	$14,1			# 0x1
ori	$9,$9,0xfcfc
$L2255:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($5)  
lwr $3, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($5)  
lwr $8, 1($5)  

# 0 "" 2
#NO_APP
and	$2,$3,$10
and	$6,$8,$10
and	$3,$3,$9
and	$8,$8,$9
addu	$2,$2,$6
srl	$8,$8,2
srl	$3,$3,2
addu	$2,$2,$12
addu	$8,$8,$3
addu	$24,$5,$16
li	$6,8			# 0x8
move	$25,$4
$L2254:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $17, 3($24)  
lwr $17, 0($24)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 4($24)  
lwr $3, 1($24)  

# 0 "" 2
#NO_APP
and	$7,$17,$10
and	$18,$3,$10
addu	$7,$18,$7
and	$3,$3,$9
addu	$2,$2,$7
and	$17,$17,$9
srl	$17,$17,2
srl	$3,$3,2
srl	$2,$2,2
addu	$3,$3,$17
and	$2,$2,$11
addu	$2,$2,$3
addu	$18,$24,$16
addu	$2,$2,$8
addu	$17,$25,$16
addu	$24,$18,$16
sw	$2,0($25)
addiu	$6,$6,-1
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($18)  
lwr $25, 0($18)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($18)  
lwr $8, 1($18)  

# 0 "" 2
#NO_APP
and	$2,$25,$10
and	$18,$8,$10
addu	$2,$2,$18
and	$8,$8,$9
addu	$2,$2,$12
and	$25,$25,$9
srl	$25,$25,2
srl	$8,$8,2
addu	$7,$2,$7
addu	$8,$8,$25
srl	$7,$7,2
addu	$3,$8,$3
and	$7,$7,$11
addu	$7,$3,$7
addu	$25,$17,$16
.set	noreorder
.set	nomacro
bne	$6,$0,$L2254
sw	$7,0($17)
.set	macro
.set	reorder

addiu	$5,$5,4
.set	noreorder
.set	nomacro
bne	$13,$14,$L2256
addu	$4,$4,$15
.set	macro
.set	reorder

lw	$31,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L2256:
.option	pic0
.set	noreorder
.set	nomacro
j	$L2255
.option	pic2
li	$13,1			# 0x1
.set	macro
.set	reorder

.end	put_rv40_qpel16_mc33_c
.size	put_rv40_qpel16_mc33_c, .-put_rv40_qpel16_mc33_c
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_qpel2_mc11_c
.type	put_h264_qpel2_mc11_c, @function
put_h264_qpel2_mc11_c:
.frame	$sp,112,$31		# vars= 48, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-112
addu	$8,$5,$6
sw	$16,72($sp)
move	$16,$6
sw	$17,76($sp)
sll	$17,$16,1
sw	$31,108($sp)
sw	$fp,104($sp)
subu	$17,$5,$17
sw	$22,96($sp)
sw	$21,92($sp)
addu	$7,$17,$16
sw	$18,80($sp)
sw	$23,100($sp)
sw	$20,88($sp)
sw	$19,84($sp)
lbu	$2,2($5)
lbu	$10,0($5)
lbu	$6,1($5)
lbu	$12,-1($5)
lbu	$20,3($5)
lbu	$15,-2($5)
addu	$13,$10,$6
lbu	$23,4($5)
addu	$22,$12,$2
lbu	$5,1($17)
addu	$6,$6,$2
lbu	$17,0($17)
addu	$10,$10,$20
lbu	$14,1($8)
sll	$18,$13,2
lbu	$11,-1($8)
sll	$13,$13,4
lbu	$9,0($8)
addu	$12,$12,$23
lbu	$2,2($8)
addu	$13,$18,$13
lbu	$19,3($8)
addu	$3,$9,$14
sw	$5,56($sp)
addu	$21,$11,$2
sw	$17,52($sp)
lbu	$5,1($7)
addu	$2,$14,$2
lbu	$17,0($7)
sll	$31,$3,2
lbu	$14,-2($8)
sll	$25,$21,2
lbu	$8,4($8)
sll	$3,$3,4
sw	$5,64($sp)
sll	$5,$6,2
sll	$6,$6,4
addu	$9,$9,$19
sw	$8,48($sp)
addu	$25,$25,$21
sw	$17,60($sp)
sll	$17,$10,2
addu	$3,$31,$3
addu	$10,$17,$10
lw	$17,56($sp)
sll	$8,$22,2
sll	$fp,$2,2
addu	$5,$5,$6
sll	$2,$2,4
sll	$24,$9,2
subu	$3,$3,$25
addu	$8,$8,$22
addu	$fp,$fp,$2
subu	$5,$5,$10
addu	$2,$14,$19
addu	$6,$24,$9
lw	$9,48($sp)
subu	$13,$13,$8
addu	$5,$5,$12
addu	$8,$15,$20
addu	$2,$3,$2
subu	$fp,$fp,$6
addu	$8,$13,$8
addu	$6,$11,$9
addiu	$3,$5,16
addiu	$2,$2,16
lui	$9,%hi(ff_cropTbl+1024)
addu	$7,$7,$16
addiu	$9,$9,%lo(ff_cropTbl+1024)
addiu	$8,$8,16
sra	$3,$3,5
lbu	$10,1($7)
sra	$2,$2,5
lbu	$11,0($7)
addu	$6,$fp,$6
addu	$7,$7,$16
addu	$3,$9,$3
addu	$2,$9,$2
sra	$5,$8,5
lbu	$12,0($7)
addiu	$6,$6,16
lbu	$8,1($7)
addu	$5,$9,$5
lbu	$13,0($3)
addu	$7,$7,$16
lbu	$14,0($2)
sra	$6,$6,5
lw	$2,64($sp)
lbu	$15,0($5)
sll	$5,$17,8
addu	$6,$9,$6
lw	$17,60($sp)
addu	$9,$7,$16
sb	$13,45($sp)
sll	$3,$2,8
sb	$14,46($sp)
sll	$2,$8,8
lw	$8,52($sp)
addu	$13,$9,$16
lbu	$6,0($6)
or	$2,$2,$12
sb	$15,44($sp)
or	$3,$3,$17
or	$5,$5,$8
sh	$2,30($sp)
sh	$3,26($sp)
sll	$10,$10,8
lbu	$8,1($7)
move	$17,$4
lbu	$3,1($9)
or	$11,$10,$11
lbu	$2,1($13)
addiu	$4,$sp,40
lbu	$10,0($7)
sll	$7,$8,8
lbu	$9,0($9)
sll	$3,$3,8
lbu	$8,0($13)
sll	$2,$2,8
or	$7,$7,$10
sb	$6,47($sp)
or	$3,$3,$9
sh	$5,24($sp)
or	$2,$2,$8
sh	$11,28($sp)
addiu	$5,$sp,28
sh	$7,32($sp)
li	$6,2			# 0x2
sh	$3,34($sp)
sh	$2,36($sp)
.option	pic0
jal	put_h264_qpel2_v_lowpass.constprop.31
.option	pic2
addu	$16,$17,$16

li	$5,-16908288			# 0xfffffffffefe0000
lhu	$7,46($sp)
lhu	$2,42($sp)
ori	$4,$5,0xfefe
lhu	$8,44($sp)
lhu	$3,40($sp)
xor	$5,$2,$7
lw	$31,108($sp)
and	$5,$5,$4
lw	$fp,104($sp)
xor	$6,$3,$8
lw	$23,100($sp)
and	$6,$6,$4
lw	$22,96($sp)
srl	$6,$6,1
lw	$21,92($sp)
srl	$5,$5,1
lw	$20,88($sp)
or	$3,$3,$8
lw	$19,84($sp)
or	$2,$2,$7
lw	$18,80($sp)
subu	$3,$3,$6
subu	$2,$2,$5
sh	$3,0($17)
sh	$2,0($16)
lw	$17,76($sp)
lw	$16,72($sp)
j	$31
addiu	$sp,$sp,112

.set	macro
.set	reorder
.end	put_h264_qpel2_mc11_c
.size	put_h264_qpel2_mc11_c, .-put_h264_qpel2_mc11_c
.align	2
.set	nomips16
.set	nomicromips
.ent	avg_h264_qpel16_mc03_c
.type	avg_h264_qpel16_mc03_c, @function
avg_h264_qpel16_mc03_c:
.frame	$sp,656,$31		# vars= 592, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-656
sll	$2,$6,1
sw	$20,648($sp)
move	$20,$4
subu	$5,$5,$2
sw	$18,640($sp)
addiu	$4,$sp,40
sw	$31,652($sp)
addiu	$18,$sp,376
sw	$19,644($sp)
sw	$17,636($sp)
move	$19,$6
sw	$16,632($sp)
.option	pic0
jal	copy_block16.constprop.41
.option	pic2
addiu	$17,$sp,384

addiu	$5,$sp,72
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
move	$4,$18

addiu	$5,$sp,80
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
move	$4,$17

addiu	$4,$sp,504
addiu	$5,$sp,200
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
li	$16,16			# 0x10

addiu	$4,$sp,512
.option	pic0
jal	put_h264_qpel8_v_lowpass.constprop.27
.option	pic2
addiu	$5,$sp,208

addiu	$5,$sp,88
move	$4,$20
sw	$16,16($sp)
move	$6,$18
sw	$16,20($sp)
move	$7,$19
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$20,8
addiu	$5,$sp,96
sw	$16,16($sp)
sw	$16,20($sp)
move	$6,$17
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$19

lw	$31,652($sp)
lw	$20,648($sp)
lw	$19,644($sp)
lw	$18,640($sp)
lw	$17,636($sp)
lw	$16,632($sp)
j	$31
addiu	$sp,$sp,656

.set	macro
.set	reorder
.end	avg_h264_qpel16_mc03_c
.size	avg_h264_qpel16_mc03_c, .-avg_h264_qpel16_mc03_c
.align	2
.set	nomips16
.set	nomicromips
.ent	dct_sad16_c
.type	dct_sad16_c, @function
dct_sad16_c:
.frame	$sp,208,$31		# vars= 144, regs= 7/0, args= 24, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-208
lw	$25,2964($4)
li	$2,-16			# 0xfffffffffffffff0
sw	$16,180($sp)
addiu	$16,$sp,47
sw	$17,184($sp)
move	$17,$4
and	$16,$16,$2
sw	$31,204($sp)
sw	$21,200($sp)
move	$4,$16
sw	$20,196($sp)
move	$21,$7
sw	$19,192($sp)
move	$20,$5
sw	$18,188($sp)
jalr	$25
move	$19,$6

lw	$25,5644($17)
jalr	$25
move	$4,$16

lw	$25,2992($17)
jalr	$25
move	$4,$16

addiu	$5,$20,8
lw	$25,2964($17)
addiu	$6,$19,8
move	$7,$21
move	$4,$16
jalr	$25
move	$18,$2

lw	$25,5644($17)
jalr	$25
move	$4,$16

lw	$25,2992($17)
jalr	$25
move	$4,$16

lw	$3,224($sp)
addu	$18,$18,$2
li	$2,16			# 0x10
beq	$3,$2,$L2266
lw	$31,204($sp)

move	$2,$18
lw	$21,200($sp)
lw	$20,196($sp)
lw	$19,192($sp)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
j	$31
addiu	$sp,$sp,208

$L2266:
sll	$2,$21,3
move	$4,$17
addu	$20,$20,$2
addu	$19,$19,$2
li	$2,8			# 0x8
move	$5,$20
move	$6,$19
move	$7,$21
.option	pic0
jal	dct_sad8x8_c
.option	pic2
sw	$2,16($sp)

addiu	$5,$20,8
lw	$25,2964($17)
addiu	$6,$19,8
move	$7,$21
move	$4,$16
jalr	$25
addu	$18,$2,$18

lw	$25,5644($17)
jalr	$25
move	$4,$16

lw	$25,2992($17)
jalr	$25
move	$4,$16

lw	$31,204($sp)
addu	$18,$18,$2
lw	$21,200($sp)
move	$2,$18
lw	$20,196($sp)
lw	$19,192($sp)
lw	$18,188($sp)
lw	$17,184($sp)
lw	$16,180($sp)
j	$31
addiu	$sp,$sp,208

.set	macro
.set	reorder
.end	dct_sad16_c
.size	dct_sad16_c, .-dct_sad16_c
.align	2
.globl	ff_init_scantable
.set	nomips16
.set	nomicromips
.ent	ff_init_scantable
.type	ff_init_scantable, @function
ff_init_scantable:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
move	$2,$0
sw	$6,0($5)
li	$8,64			# 0x40
$L2268:
addu	$3,$6,$2
addu	$7,$5,$2
addiu	$2,$2,1
lbu	$3,0($3)
addu	$3,$4,$3
lbu	$3,0($3)
bne	$2,$8,$L2268
sb	$3,4($7)

addiu	$2,$5,4
li	$3,-1			# 0xffffffffffffffff
addiu	$5,$5,68
$L2269:
lbu	$4,0($2)
addiu	$2,$2,1
slt	$6,$3,$4
movn	$3,$4,$6
bne	$2,$5,$L2269
sb	$3,63($2)

j	$31
nop

.set	macro
.set	reorder
.end	ff_init_scantable
.size	ff_init_scantable, .-ff_init_scantable
.align	2
.globl	ff_emulated_edge_mc
.set	nomips16
.set	nomicromips
.ent	ff_emulated_edge_mc
.type	ff_emulated_edge_mc, @function
ff_emulated_edge_mc:
.frame	$sp,88,$31		# vars= 40, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-88
sw	$7,100($sp)
sw	$fp,84($sp)
sw	$23,80($sp)
sw	$22,76($sp)
sw	$21,72($sp)
sw	$20,68($sp)
sw	$19,64($sp)
sw	$18,60($sp)
sw	$17,56($sp)
sw	$16,52($sp)
lw	$9,112($sp)
lw	$8,120($sp)
lw	$3,108($sp)
slt	$7,$9,$8
bne	$7,$0,$L2273
lw	$2,116($sp)

addiu	$7,$8,-1
subu	$10,$7,$9
move	$9,$7
mul	$7,$10,$6
addu	$5,$7,$5
$L2274:
slt	$7,$3,$2
$L2408:
bne	$7,$0,$L2275
lw	$10,100($sp)

addiu	$7,$2,-1
subu	$10,$7,$3
move	$3,$7
addu	$5,$5,$10
subu	$22,$0,$9
$L2407:
lw	$15,104($sp)
subu	$8,$8,$9
slt	$9,$22,0
movn	$22,$0,$9
subu	$2,$2,$3
slt	$7,$15,$8
lw	$9,100($sp)
subu	$23,$0,$3
movz	$15,$8,$7
slt	$8,$23,0
slt	$3,$9,$2
movn	$23,$0,$8
movz	$9,$2,$3
slt	$8,$22,$15
move	$7,$15
beq	$8,$0,$L2288
move	$3,$9

subu	$21,$9,$23
sw	$22,16($sp)
addiu	$25,$9,-1
sw	$23,12($sp)
addiu	$24,$21,-4
mul	$2,$6,$22
srl	$24,$24,2
subu	$25,$25,$23
addiu	$24,$24,1
sltu	$25,$25,3
sll	$8,$24,2
sltu	$16,$21,10
addu	$17,$23,$8
sw	$8,8($sp)
addu	$8,$23,$2
movn	$17,$23,$25
addiu	$18,$23,4
addu	$9,$5,$8
addu	$8,$4,$8
move	$13,$22
addiu	$20,$17,1
addiu	$19,$17,2
slt	$11,$20,$3
slt	$10,$19,$3
slt	$fp,$23,$3
xori	$16,$16,0x1
move	$23,$11
move	$22,$10
$L2287:
beq	$fp,$0,$L2290
or	$11,$8,$9

andi	$11,$11,0x3
sltu	$11,$11,1
and	$11,$11,$16
beq	$11,$0,$L2342
addu	$10,$18,$2

addu	$11,$5,$10
addu	$10,$4,$10
sltu	$11,$8,$11
sltu	$10,$9,$10
xori	$11,$11,0x1
xori	$10,$10,0x1
or	$10,$11,$10
beq	$10,$0,$L2280
lw	$11,12($sp)

bne	$25,$0,$L2282
move	$11,$8

move	$12,$0
move	$10,$9
$L2283:
lw	$15,0($10)
addiu	$12,$12,1
addiu	$11,$11,4
sltu	$14,$12,$24
addiu	$10,$10,4
bne	$14,$0,$L2283
sw	$15,-4($11)

lw	$10,8($sp)
beq	$21,$10,$L2290
nop

$L2282:
addu	$10,$2,$17
addu	$11,$5,$10
addu	$10,$4,$10
lbu	$11,0($11)
beq	$23,$0,$L2290
sb	$11,0($10)

addu	$10,$2,$20
addu	$11,$5,$10
addu	$10,$4,$10
lbu	$11,0($11)
beq	$22,$0,$L2290
sb	$11,0($10)

addu	$10,$2,$19
addu	$11,$5,$10
addu	$10,$4,$10
lbu	$11,0($11)
sb	$11,0($10)
$L2290:
addiu	$13,$13,1
$L2404:
addu	$9,$9,$6
addu	$8,$8,$6
bne	$13,$7,$L2287
addu	$2,$2,$6

lw	$22,16($sp)
lw	$23,12($sp)
$L2288:
beq	$22,$0,$L2405
lw	$8,104($sp)

subu	$15,$3,$23
sw	$3,36($sp)
addiu	$18,$3,-1
sw	$7,40($sp)
addiu	$17,$15,-4
subu	$18,$18,$23
sw	$15,12($sp)
srl	$17,$17,2
mul	$12,$22,$6
addiu	$17,$17,1
sltu	$18,$18,3
sll	$2,$17,2
sltu	$19,$15,10
addu	$20,$2,$23
sw	$2,20($sp)
addu	$5,$4,$23
movn	$20,$23,$18
addu	$21,$23,$12
addu	$13,$12,$3
move	$2,$0
move	$11,$0
addiu	$8,$20,1
addiu	$9,$20,2
addu	$10,$8,$12
addu	$14,$20,$12
addu	$10,$4,$10
sw	$9,8($sp)
addu	$14,$4,$14
addu	$9,$12,$9
sw	$10,16($sp)
slt	$10,$8,$3
sw	$14,28($sp)
addu	$9,$4,$9
addu	$14,$4,$8
sw	$10,32($sp)
lw	$8,8($sp)
addu	$16,$4,$21
addiu	$24,$23,4
sw	$9,24($sp)
addiu	$25,$21,4
addu	$13,$4,$13
slt	$fp,$23,$3
xori	$19,$19,0x1
slt	$15,$8,$3
$L2289:
beq	$fp,$0,$L2301
or	$3,$16,$5

andi	$3,$3,0x3
sltu	$3,$3,1
and	$3,$3,$19
beq	$3,$0,$L2304
addu	$3,$24,$2

addu	$7,$2,$23
slt	$3,$21,$3
slt	$7,$7,$25
xori	$3,$3,0x1
xori	$7,$7,0x1
or	$7,$3,$7
beq	$7,$0,$L2294
move	$3,$16

bne	$18,$0,$L2296
move	$8,$0

move	$7,$16
move	$3,$5
$L2297:
lw	$10,0($7)
addiu	$8,$8,1
addiu	$3,$3,4
sltu	$9,$8,$17
addiu	$7,$7,4
bne	$9,$0,$L2297
sw	$10,-4($3)

lw	$3,12($sp)
lw	$7,20($sp)
beq	$3,$7,$L2301
nop

$L2296:
lw	$9,28($sp)
addu	$3,$4,$2
lw	$10,32($sp)
addu	$7,$3,$20
lbu	$8,0($9)
beq	$10,$0,$L2301
sb	$8,0($7)

lw	$7,16($sp)
lbu	$8,0($7)
addu	$7,$14,$2
beq	$15,$0,$L2301
sb	$8,0($7)

lw	$9,24($sp)
lw	$10,8($sp)
lbu	$7,0($9)
addu	$3,$3,$10
sb	$7,0($3)
$L2301:
addiu	$11,$11,1
$L2403:
addu	$2,$2,$6
bne	$11,$22,$L2289
addu	$5,$5,$6

lw	$3,36($sp)
lw	$7,40($sp)
lw	$8,104($sp)
$L2405:
slt	$2,$7,$8
beq	$2,$0,$L2316
addiu	$16,$3,-1

subu	$24,$3,$23
sw	$3,32($sp)
addiu	$15,$24,-4
addiu	$10,$7,-1
srl	$15,$15,2
mul	$10,$10,$6
addiu	$15,$15,1
subu	$16,$16,$23
sll	$2,$15,2
sltu	$16,$16,3
addu	$19,$23,$2
move	$13,$8
sw	$2,8($sp)
movn	$19,$23,$16
mul	$5,$6,$7
addu	$18,$23,$10
addu	$11,$10,$3
sltu	$17,$24,10
addu	$8,$19,$10
addiu	$22,$19,2
addu	$8,$4,$8
addiu	$2,$19,1
addu	$fp,$10,$22
addu	$25,$10,$2
sw	$8,24($sp)
addu	$fp,$4,$fp
addu	$8,$4,$2
slt	$9,$22,$3
slt	$2,$2,$3
sw	$fp,12($sp)
addiu	$21,$23,4
sw	$8,16($sp)
addu	$25,$4,$25
sw	$2,28($sp)
addu	$14,$4,$18
sw	$9,20($sp)
addiu	$20,$18,4
addu	$11,$4,$11
slt	$fp,$23,$3
xori	$17,$17,0x1
$L2315:
beq	$fp,$0,$L2318
addu	$2,$23,$5

addu	$8,$21,$5
slt	$9,$2,$20
slt	$8,$18,$8
xori	$8,$8,0x1
xori	$9,$9,0x1
or	$9,$8,$9
and	$3,$17,$9
beq	$3,$0,$L2321
addu	$2,$4,$2

or	$3,$2,$14
andi	$3,$3,0x3
bne	$3,$0,$L2321
nop

bne	$16,$0,$L2310
move	$8,$0

move	$3,$14
$L2311:
lw	$12,0($3)
addiu	$8,$8,1
addiu	$2,$2,4
sltu	$9,$8,$15
addiu	$3,$3,4
bne	$9,$0,$L2311
sw	$12,-4($2)

lw	$9,8($sp)
beq	$24,$9,$L2318
nop

$L2310:
lw	$2,24($sp)
lbu	$8,0($2)
addu	$2,$4,$5
addu	$3,$2,$19
sb	$8,0($3)
lw	$3,28($sp)
beq	$3,$0,$L2318
lw	$9,16($sp)

lbu	$8,0($25)
addu	$3,$9,$5
sb	$8,0($3)
lw	$3,20($sp)
beq	$3,$0,$L2318
lw	$8,12($sp)

addu	$2,$2,$22
lbu	$3,0($8)
sb	$3,0($2)
$L2318:
addiu	$7,$7,1
$L2402:
bne	$7,$13,$L2315
addu	$5,$5,$6

lw	$3,32($sp)
$L2316:
lw	$9,104($sp)
blez	$9,$L2272
addiu	$19,$23,-1

addiu	$15,$23,-4
lw	$5,100($sp)
srl	$15,$15,2
subu	$24,$5,$3
addiu	$15,$15,1
addiu	$16,$24,-4
sll	$8,$15,2
sw	$24,44($sp)
sltu	$19,$19,3
move	$22,$8
movn	$22,$0,$19
srl	$16,$16,2
addiu	$17,$5,-1
addiu	$16,$16,1
sw	$8,16($sp)
lw	$8,100($sp)
subu	$17,$17,$3
sll	$7,$16,2
addiu	$10,$22,1
slt	$20,$3,$8
sw	$7,12($sp)
addu	$7,$3,$7
sw	$10,8($sp)
sltu	$17,$17,3
lw	$8,8($sp)
addiu	$fp,$22,2
movn	$7,$3,$17
move	$12,$9
sltu	$18,$23,10
slt	$8,$8,$23
sltu	$21,$24,10
addiu	$5,$7,2
sw	$8,32($sp)
addiu	$25,$7,1
slt	$8,$fp,$23
addu	$7,$4,$7
sw	$8,20($sp)
addu	$9,$4,$3
lw	$8,100($sp)
addu	$2,$4,$23
sw	$7,36($sp)
move	$10,$0
lw	$7,100($sp)
move	$14,$0
slt	$8,$5,$8
addu	$5,$4,$5
slt	$7,$25,$7
sw	$8,24($sp)
addu	$fp,$4,$fp
addiu	$3,$3,-1
sw	$7,40($sp)
move	$11,$4
sw	$5,28($sp)
xori	$18,$18,0x1
xori	$21,$21,0x1
$L2317:
beq	$23,$0,$L2339
nop

or	$5,$11,$2
andi	$5,$5,0x3
bne	$5,$0,$L2343
move	$5,$11

beq	$18,$0,$L2322
nop

bne	$19,$0,$L2324
move	$8,$0

lbu	$13,0($2)
move	$7,$11
sll	$5,$13,8
sll	$24,$13,16
or	$5,$13,$5
sll	$13,$13,24
or	$5,$5,$24
or	$13,$5,$13
$L2325:
addiu	$8,$8,1
sw	$13,0($7)
sltu	$5,$8,$15
bne	$5,$0,$L2325
addiu	$7,$7,4

lw	$5,16($sp)
beq	$5,$23,$L2339
nop

$L2324:
lbu	$8,0($2)
addu	$5,$4,$10
addu	$7,$5,$22
sb	$8,0($7)
lw	$7,32($sp)
beq	$7,$0,$L2339
nop

lw	$8,8($sp)
lbu	$7,0($2)
addu	$5,$5,$8
sb	$7,0($5)
lw	$5,20($sp)
beq	$5,$0,$L2339
nop

lbu	$7,0($2)
addu	$5,$fp,$10
sb	$7,0($5)
$L2339:
beq	$20,$0,$L2336
nop

addu	$5,$3,$10
addu	$5,$4,$5
or	$5,$9,$5
andi	$5,$5,0x3
bne	$5,$0,$L2330
lw	$5,100($sp)

beq	$21,$0,$L2406
nop

bne	$17,$0,$L2331
move	$8,$0

lbu	$13,-1($9)
move	$7,$9
sll	$5,$13,8
sll	$24,$13,16
or	$5,$13,$5
sll	$13,$13,24
or	$5,$5,$24
or	$13,$5,$13
$L2332:
addiu	$8,$8,1
sw	$13,0($7)
sltu	$5,$8,$16
bne	$5,$0,$L2332
addiu	$7,$7,4

lw	$5,12($sp)
lw	$7,44($sp)
beq	$5,$7,$L2336
nop

$L2331:
lw	$8,36($sp)
lbu	$7,-1($9)
addu	$5,$8,$10
sb	$7,0($5)
lw	$5,40($sp)
beq	$5,$0,$L2336
nop

lbu	$7,-1($9)
addu	$5,$4,$10
addu	$5,$5,$25
sb	$7,0($5)
lw	$7,24($sp)
beq	$7,$0,$L2336
nop

lw	$8,28($sp)
lbu	$7,-1($9)
addu	$5,$8,$10
sb	$7,0($5)
$L2336:
addiu	$14,$14,1
$L2401:
addu	$9,$9,$6
addu	$10,$10,$6
addu	$11,$11,$6
bne	$14,$12,$L2317
addu	$2,$2,$6

$L2272:
lw	$fp,84($sp)
lw	$23,80($sp)
lw	$22,76($sp)
lw	$21,72($sp)
lw	$20,68($sp)
lw	$19,64($sp)
lw	$18,60($sp)
lw	$17,56($sp)
lw	$16,52($sp)
j	$31
addiu	$sp,$sp,88

$L2330:
$L2406:
addu	$8,$5,$10
move	$5,$9
addu	$8,$4,$8
$L2337:
lbu	$7,-1($9)
addiu	$5,$5,1
bne	$5,$8,$L2337
sb	$7,-1($5)

.option	pic0
j	$L2401
.option	pic2
addiu	$14,$14,1

$L2343:
$L2322:
lbu	$7,0($2)
addiu	$5,$5,1
bne	$5,$2,$L2322
sb	$7,-1($5)

.option	pic0
j	$L2339
nop

.option	pic2
$L2321:
move	$2,$14
$L2308:
lbu	$8,0($2)
subu	$3,$2,$10
addiu	$2,$2,1
addu	$3,$3,$5
bne	$2,$11,$L2308
sb	$8,0($3)

.option	pic0
j	$L2402
.option	pic2
addiu	$7,$7,1

$L2304:
move	$3,$16
$L2294:
lbu	$8,0($3)
subu	$7,$3,$12
addiu	$3,$3,1
addu	$7,$7,$2
bne	$3,$13,$L2294
sb	$8,0($7)

.option	pic0
j	$L2403
.option	pic2
addiu	$11,$11,1

$L2342:
lw	$11,12($sp)
$L2280:
addu	$10,$11,$2
addiu	$11,$11,1
addu	$12,$5,$10
addu	$10,$4,$10
lbu	$12,0($12)
bne	$11,$3,$L2280
sb	$12,0($10)

.option	pic0
j	$L2404
.option	pic2
addiu	$13,$13,1

$L2275:
subu	$7,$0,$10
slt	$7,$7,$3
bne	$7,$0,$L2407
subu	$22,$0,$9

li	$7,1			# 0x1
subu	$7,$7,$10
subu	$10,$7,$3
move	$3,$7
.option	pic0
j	$L2407
.option	pic2
addu	$5,$5,$10

$L2273:
lw	$10,104($sp)
subu	$7,$0,$10
slt	$7,$7,$9
bne	$7,$0,$L2408
slt	$7,$3,$2

li	$7,1			# 0x1
subu	$7,$7,$10
subu	$10,$7,$9
move	$9,$7
mul	$7,$10,$6
.option	pic0
j	$L2274
.option	pic2
addu	$5,$7,$5

.set	macro
.set	reorder
.end	ff_emulated_edge_mc
.size	ff_emulated_edge_mc, .-ff_emulated_edge_mc
.align	2
.globl	ff_put_qpel8_mc11_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel8_mc11_old_c
.type	ff_put_qpel8_mc11_old_c, @function
ff_put_qpel8_mc11_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
li	$6,8			# 0x8
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$6,33685504			# 0x2020000
li	$5,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
addiu	$24,$sp,160
move	$15,$0
addiu	$13,$13,771
addiu	$6,$6,514
addiu	$5,$5,3855
move	$14,$16
move	$4,$19
ori	$12,$12,0xfcfc
$L2410:
addiu	$19,$15,3
addiu	$22,$15,4
addiu	$18,$15,7
addu	$23,$16,$15
addu	$19,$16,$19
addu	$18,$16,$18
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($14)  
lwr $10, 0($14)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 7($14)  
lwr $8, 4($14)  

# 0 "" 2
#NO_APP
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 144($19)  
lwr $9, 144($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 144($18)  
lwr $3, 144($22)  

# 0 "" 2
#NO_APP
and	$20,$10,$13
and	$2,$8,$13
and	$25,$9,$13
and	$11,$3,$13
addu	$25,$20,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$6
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($19)  
lwr $21, 280($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$20,$13
and	$fp,$21,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$21,$21,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($19)  
lwr $2, 216($23)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$2
and	$20,$20,$12
addu	$10,$9,$10
and	$7,$19,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($18)  
lwr $2, 216($22)  

# 0 "" 2
#NO_APP
srl	$3,$20,2
move	$18,$2
srl	$9,$21,2
and	$2,$2,$13
addu	$25,$25,$7
and	$19,$19,$12
and	$18,$18,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$19,$19,2
srl	$3,$18,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$19
and	$7,$10,$5
and	$2,$2,$5
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$14,$14,16
sw	$7,0($4)
addiu	$15,$15,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$14,$24,$L2410
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_qpel8_mc11_old_c
.size	ff_put_qpel8_mc11_old_c, .-ff_put_qpel8_mc11_old_c
.align	2
.globl	ff_put_qpel8_mc31_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel8_mc31_old_c
.type	ff_put_qpel8_mc31_old_c, @function
ff_put_qpel8_mc31_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
addiu	$5,$sp,33
li	$6,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
li	$7,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$14,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,514
addiu	$6,$6,3855
li	$24,128			# 0x80
move	$4,$19
ori	$12,$12,0xfcfc
$L2414:
addiu	$19,$14,3
addiu	$22,$14,4
addiu	$18,$14,7
addu	$23,$16,$14
addu	$2,$16,$5
addu	$19,$16,$19
addu	$18,$16,$18
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 4($2)  
lwr $10, 1($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 144($19)  
lwr $9, 144($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 144($18)  
lwr $3, 144($22)  

# 0 "" 2
#NO_APP
and	$20,$10,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 8($2)  
lwr $8, 5($2)  

# 0 "" 2
#NO_APP
and	$25,$9,$13
and	$2,$8,$13
and	$11,$3,$13
addu	$25,$20,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($19)  
lwr $21, 280($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$20,$13
and	$fp,$21,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$21,$21,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($19)  
lwr $2, 216($23)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$2
and	$20,$20,$12
addu	$10,$9,$10
and	$7,$19,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($18)  
lwr $2, 216($22)  

# 0 "" 2
#NO_APP
srl	$3,$20,2
move	$18,$2
srl	$9,$21,2
and	$2,$2,$13
addu	$25,$25,$7
and	$19,$19,$12
and	$18,$18,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$19,$19,2
srl	$3,$18,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$19
and	$7,$10,$6
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$5,$5,16
sw	$7,0($4)
addiu	$14,$14,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2414
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_qpel8_mc31_old_c
.size	ff_put_qpel8_mc31_old_c, .-ff_put_qpel8_mc31_old_c
.align	2
.globl	ff_put_qpel8_mc13_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel8_mc13_old_c
.type	ff_put_qpel8_mc13_old_c, @function
ff_put_qpel8_mc13_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
li	$6,8			# 0x8
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$12,50528256			# 0x3030000
li	$14,33685504			# 0x2020000
li	$13,252641280			# 0xf0f0000
li	$11,-50593792			# 0xfffffffffcfc0000
move	$18,$0
move	$2,$0
addiu	$12,$12,771
addiu	$14,$14,514
addiu	$13,$13,3855
li	$15,128			# 0x80
move	$4,$19
ori	$11,$11,0xfcfc
$L2418:
addiu	$6,$2,16
addiu	$5,$18,8
addu	$19,$16,$18
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 159($19)  
lwr $7, 156($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 23($2)  
lwr $8, 20($2)  

# 0 "" 2
#NO_APP
addu	$9,$16,$6
addu	$3,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 19($2)  
lwr $10, 0($9)  

# 0 "" 2
#NO_APP
and	$24,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 155($19)  
lwr $9, 144($3)  

# 0 "" 2
#NO_APP
and	$20,$10,$12
and	$2,$7,$12
and	$25,$9,$12
addu	$25,$20,$25
addu	$24,$24,$2
addiu	$23,$18,3
addiu	$22,$18,4
addiu	$18,$18,7
and	$2,$7,$11
and	$9,$9,$11
and	$10,$10,$11
addu	$25,$25,$14
and	$8,$8,$11
addu	$24,$24,$14
addu	$18,$16,$18
addu	$23,$16,$23
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($23)  
lwr $21, 280($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$3,$20,$12
and	$fp,$21,$12
srl	$2,$2,2
srl	$10,$10,2
srl	$8,$8,2
addu	$24,$24,$3
and	$21,$21,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 216($23)  
lwr $3, 216($19)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$3
and	$20,$20,$11
addu	$10,$9,$10
and	$7,$19,$12
addu	$8,$2,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 216($18)  
lwr $3, 216($22)  

# 0 "" 2
#NO_APP
srl	$2,$20,2
move	$18,$3
srl	$9,$21,2
and	$3,$3,$12
addu	$25,$25,$7
and	$18,$18,$11
and	$19,$19,$11
addu	$3,$24,$3
addu	$7,$8,$2
addu	$9,$10,$9
srl	$2,$18,2
srl	$19,$19,2
srl	$10,$25,2
srl	$3,$3,2
addu	$2,$7,$2
addu	$8,$9,$19
and	$7,$10,$13
and	$3,$3,$13
addu	$3,$2,$3
addu	$7,$8,$7
move	$2,$6
sw	$3,4($4)
move	$18,$5
sw	$7,0($4)
.set	noreorder
.set	nomacro
bne	$6,$15,$L2418
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_qpel8_mc13_old_c
.size	ff_put_qpel8_mc13_old_c, .-ff_put_qpel8_mc13_old_c
.align	2
.globl	ff_put_qpel8_mc33_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel8_mc33_old_c
.type	ff_put_qpel8_mc33_old_c, @function
ff_put_qpel8_mc33_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
addiu	$5,$sp,33
li	$6,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
li	$7,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$14,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$18,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,514
addiu	$14,$14,3855
li	$24,128			# 0x80
move	$4,$19
ori	$12,$12,0xfcfc
$L2422:
addiu	$6,$18,8
addu	$19,$16,$18
addu	$2,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 159($19)  
lwr $3, 156($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 20($2)  
lwr $10, 17($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 24($2)  
lwr $8, 21($2)  

# 0 "" 2
#NO_APP
and	$20,$10,$13
addu	$2,$16,$6
and	$11,$8,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 155($19)  
lwr $9, 144($2)  

# 0 "" 2
#NO_APP
and	$2,$3,$13
and	$25,$9,$13
addu	$25,$20,$25
addu	$11,$11,$2
addiu	$23,$18,3
addiu	$22,$18,4
addiu	$18,$18,7
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
addu	$18,$16,$18
addu	$23,$16,$23
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($23)  
lwr $21, 280($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$20,$13
and	$fp,$21,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$21,$21,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($23)  
lwr $2, 216($19)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$2
and	$20,$20,$12
addu	$10,$9,$10
and	$7,$19,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($18)  
lwr $2, 216($22)  

# 0 "" 2
#NO_APP
srl	$3,$20,2
move	$18,$2
srl	$9,$21,2
and	$2,$2,$13
addu	$25,$25,$7
and	$18,$18,$12
and	$19,$19,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$3,$18,2
srl	$19,$19,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$19
and	$7,$10,$14
and	$2,$2,$14
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$5,$5,16
sw	$7,0($4)
move	$18,$6
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2422
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_qpel8_mc33_old_c
.size	ff_put_qpel8_mc33_old_c, .-ff_put_qpel8_mc33_old_c
.align	2
.globl	ff_put_qpel8_mc12_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel8_mc12_old_c
.type	ff_put_qpel8_mc12_old_c, @function
ff_put_qpel8_mc12_old_c:
.frame	$sp,400,$31		# vars= 344, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-400
sw	$16,380($sp)
addiu	$16,$sp,32
sw	$19,392($sp)
move	$19,$4
move	$4,$16
sw	$31,396($sp)
sw	$18,388($sp)
addiu	$18,$sp,176
sw	$17,384($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
li	$6,8			# 0x8
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$11,64			# 0x40
move	$4,$19
ori	$6,$6,0xfefe
$L2426:
addiu	$3,$2,3
addiu	$10,$2,4
addiu	$8,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$8,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 280($3)  
lwr $12, 280($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 216($3)  
lwr $7, 216($5)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$9,$7,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 280($8)  
lwr $5, 280($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 216($8)  
lwr $3, 216($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$5
and	$9,$9,$6
and	$8,$8,$6
srl	$9,$9,1
srl	$8,$8,1
or	$7,$7,$12
or	$3,$3,$5
subu	$7,$7,$9
subu	$3,$3,$8
addiu	$2,$2,8
sw	$7,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2426
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,396($sp)
lw	$19,392($sp)
lw	$18,388($sp)
lw	$17,384($sp)
lw	$16,380($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,400
.set	macro
.set	reorder

.end	ff_put_qpel8_mc12_old_c
.size	ff_put_qpel8_mc12_old_c, .-ff_put_qpel8_mc12_old_c
.align	2
.globl	ff_put_qpel8_mc32_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel8_mc32_old_c
.type	ff_put_qpel8_mc32_old_c, @function
ff_put_qpel8_mc32_old_c:
.frame	$sp,400,$31		# vars= 344, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-400
sw	$16,380($sp)
addiu	$16,$sp,32
sw	$19,392($sp)
move	$19,$4
move	$4,$16
sw	$31,396($sp)
sw	$18,388($sp)
addiu	$18,$sp,176
sw	$17,384($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
addiu	$5,$sp,33
li	$6,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
li	$7,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$2,$0
li	$11,64			# 0x40
move	$4,$19
ori	$6,$6,0xfefe
$L2430:
addiu	$3,$2,3
addiu	$10,$2,4
addiu	$8,$2,7
addu	$5,$16,$2
addu	$3,$16,$3
addu	$8,$16,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 280($3)  
lwr $12, 280($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 216($3)  
lwr $7, 216($5)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$9,$7,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 280($8)  
lwr $5, 280($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 216($8)  
lwr $3, 216($10)  

# 0 "" 2
#NO_APP
xor	$8,$3,$5
and	$9,$9,$6
and	$8,$8,$6
srl	$9,$9,1
srl	$8,$8,1
or	$7,$7,$12
or	$3,$3,$5
subu	$7,$7,$9
subu	$3,$3,$8
addiu	$2,$2,8
sw	$7,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$11,$L2430
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,396($sp)
lw	$19,392($sp)
lw	$18,388($sp)
lw	$17,384($sp)
lw	$16,380($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,400
.set	macro
.set	reorder

.end	ff_put_qpel8_mc32_old_c
.size	ff_put_qpel8_mc32_old_c, .-ff_put_qpel8_mc32_old_c
.align	2
.globl	ff_put_qpel16_mc11_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel16_mc11_old_c
.type	ff_put_qpel16_mc11_old_c, @function
ff_put_qpel16_mc11_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$18,1256($sp)
move	$18,$4
move	$4,$16
sw	$31,1284($sp)
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$17,1252($sp)
move	$17,$6
sw	$fp,1280($sp)
sw	$23,1276($sp)
sw	$22,1272($sp)
sw	$21,1268($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,1264($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,992
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,736
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
addiu	$14,$sp,59
addiu	$24,$sp,443
move	$4,$0
addiu	$13,$13,771
addiu	$15,$15,514
addiu	$6,$6,3855
move	$5,$18
ori	$12,$12,0xfcfc
$L2434:
addiu	$20,$4,3
addiu	$23,$4,4
addiu	$19,$4,7
addu	$fp,$16,$4
addu	$20,$16,$20
addu	$19,$16,$19
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 0($14)  
lwr $10, -3($14)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($14)  
lwr $8, 1($14)  

# 0 "" 2
#NO_APP
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 408($20)  
lwr $9, 408($fp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 408($19)  
lwr $3, 408($23)  

# 0 "" 2
#NO_APP
and	$21,$10,$13
and	$2,$8,$13
and	$25,$9,$13
and	$11,$3,$13
addu	$25,$21,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 936($20)  
lwr $22, 936($fp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($19)  
lwr $21, 936($23)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$21,$13
and	$31,$22,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$22,$22,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($20)  
lwr $2, 680($fp)  

# 0 "" 2
#NO_APP
addu	$25,$25,$31
move	$20,$2
and	$21,$21,$12
addu	$10,$9,$10
and	$7,$20,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($19)  
lwr $2, 680($23)  

# 0 "" 2
#NO_APP
srl	$3,$21,2
move	$19,$2
srl	$9,$22,2
and	$2,$2,$13
addu	$25,$25,$7
and	$20,$20,$12
and	$19,$19,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$20,$20,2
srl	$3,$19,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$20
and	$7,$10,$6
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$14,$14,24
sw	$7,0($5)
addiu	$4,$4,16
sw	$2,4($5)
.set	noreorder
.set	nomacro
bne	$14,$24,$L2434
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$3,$sp,744
sw	$17,20($sp)
li	$2,16			# 0x10
addiu	$4,$18,8
sw	$3,16($sp)
li	$3,24			# 0x18
addiu	$5,$sp,64
sw	$2,28($sp)
addiu	$6,$sp,472
sw	$2,32($sp)
addiu	$7,$sp,1000
sw	$3,24($sp)
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_qpel16_mc11_old_c
.size	ff_put_qpel16_mc11_old_c, .-ff_put_qpel16_mc11_old_c
.align	2
.globl	ff_put_qpel16_mc31_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel16_mc31_old_c
.type	ff_put_qpel16_mc31_old_c, @function
ff_put_qpel16_mc31_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$18,1256($sp)
move	$18,$4
move	$4,$16
sw	$31,1284($sp)
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$17,1252($sp)
move	$17,$6
sw	$fp,1280($sp)
sw	$23,1276($sp)
sw	$22,1272($sp)
sw	$21,1268($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,1264($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,992
addiu	$5,$sp,57
li	$6,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
li	$7,24			# 0x18
.set	macro
.set	reorder

addiu	$4,$sp,736
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$14,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,514
addiu	$6,$6,3855
li	$24,384			# 0x180
move	$4,$18
ori	$12,$12,0xfcfc
$L2438:
addiu	$20,$14,3
addiu	$23,$14,4
addiu	$19,$14,7
addu	$fp,$16,$14
addu	$2,$16,$5
addu	$20,$16,$20
addu	$19,$16,$19
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 4($2)  
lwr $10, 1($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 408($20)  
lwr $9, 408($fp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 408($19)  
lwr $3, 408($23)  

# 0 "" 2
#NO_APP
and	$21,$10,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 8($2)  
lwr $8, 5($2)  

# 0 "" 2
#NO_APP
and	$25,$9,$13
and	$2,$8,$13
and	$11,$3,$13
addu	$25,$21,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 936($20)  
lwr $22, 936($fp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($19)  
lwr $21, 936($23)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$21,$13
and	$31,$22,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$22,$22,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($20)  
lwr $2, 680($fp)  

# 0 "" 2
#NO_APP
addu	$25,$25,$31
move	$20,$2
and	$21,$21,$12
addu	$10,$9,$10
and	$7,$20,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($19)  
lwr $2, 680($23)  

# 0 "" 2
#NO_APP
srl	$3,$21,2
move	$19,$2
srl	$9,$22,2
and	$2,$2,$13
addu	$25,$25,$7
and	$20,$20,$12
and	$19,$19,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$20,$20,2
srl	$3,$19,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$20
and	$7,$10,$6
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$5,$5,24
sw	$7,0($4)
addiu	$14,$14,16
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2438
addu	$4,$4,$17
.set	macro
.set	reorder

addiu	$3,$sp,744
sw	$17,20($sp)
li	$2,16			# 0x10
addiu	$4,$18,8
sw	$3,16($sp)
li	$3,24			# 0x18
addiu	$5,$sp,65
sw	$2,28($sp)
addiu	$6,$sp,472
sw	$2,32($sp)
addiu	$7,$sp,1000
sw	$3,24($sp)
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_qpel16_mc31_old_c
.size	ff_put_qpel16_mc31_old_c, .-ff_put_qpel16_mc31_old_c
.align	2
.globl	ff_put_qpel16_mc13_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel16_mc13_old_c
.type	ff_put_qpel16_mc13_old_c, @function
ff_put_qpel16_mc13_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$18,1256($sp)
move	$18,$4
move	$4,$16
sw	$31,1284($sp)
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$17,1252($sp)
move	$17,$6
sw	$fp,1280($sp)
sw	$23,1276($sp)
sw	$22,1272($sp)
sw	$21,1268($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,1264($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,992
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,736
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$12,50528256			# 0x3030000
li	$14,33685504			# 0x2020000
li	$13,252641280			# 0xf0f0000
li	$11,-50593792			# 0xfffffffffcfc0000
move	$19,$0
move	$2,$0
addiu	$12,$12,771
addiu	$14,$14,514
addiu	$13,$13,3855
li	$15,384			# 0x180
move	$5,$18
ori	$11,$11,0xfcfc
$L2442:
addiu	$6,$2,24
addiu	$4,$19,16
addu	$20,$16,$19
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 431($20)  
lwr $7, 428($20)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 31($2)  
lwr $8, 28($2)  

# 0 "" 2
#NO_APP
addu	$9,$16,$6
addu	$3,$16,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 27($2)  
lwr $10, 0($9)  

# 0 "" 2
#NO_APP
and	$24,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 427($20)  
lwr $9, 408($3)  

# 0 "" 2
#NO_APP
and	$21,$10,$12
and	$2,$7,$12
and	$25,$9,$12
addu	$25,$21,$25
addu	$24,$24,$2
addiu	$fp,$19,3
addiu	$23,$19,4
addiu	$19,$19,7
and	$2,$7,$11
and	$9,$9,$11
and	$10,$10,$11
addu	$25,$25,$14
and	$8,$8,$11
addu	$24,$24,$14
addu	$19,$16,$19
addu	$fp,$16,$fp
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 936($fp)  
lwr $22, 936($20)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($19)  
lwr $21, 936($23)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$3,$21,$12
and	$31,$22,$12
srl	$2,$2,2
srl	$10,$10,2
srl	$8,$8,2
addu	$24,$24,$3
and	$22,$22,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($fp)  
lwr $3, 680($20)  

# 0 "" 2
#NO_APP
addu	$25,$25,$31
move	$20,$3
and	$21,$21,$11
addu	$10,$9,$10
and	$7,$20,$12
addu	$8,$2,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($19)  
lwr $3, 680($23)  

# 0 "" 2
#NO_APP
srl	$2,$21,2
move	$19,$3
srl	$9,$22,2
and	$3,$3,$12
addu	$25,$25,$7
and	$19,$19,$11
and	$20,$20,$11
addu	$3,$24,$3
addu	$7,$8,$2
addu	$9,$10,$9
srl	$2,$19,2
srl	$20,$20,2
srl	$10,$25,2
srl	$3,$3,2
addu	$2,$7,$2
addu	$8,$9,$20
and	$7,$10,$13
and	$3,$3,$13
addu	$3,$2,$3
addu	$7,$8,$7
move	$2,$6
sw	$3,4($5)
move	$19,$4
sw	$7,0($5)
.set	noreorder
.set	nomacro
bne	$6,$15,$L2442
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$3,$sp,744
sw	$17,20($sp)
li	$2,16			# 0x10
addiu	$4,$18,8
sw	$3,16($sp)
li	$3,24			# 0x18
addiu	$5,$sp,88
sw	$2,28($sp)
addiu	$6,$sp,488
sw	$2,32($sp)
addiu	$7,$sp,1000
sw	$3,24($sp)
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_qpel16_mc13_old_c
.size	ff_put_qpel16_mc13_old_c, .-ff_put_qpel16_mc13_old_c
.align	2
.globl	ff_put_qpel16_mc33_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel16_mc33_old_c
.type	ff_put_qpel16_mc33_old_c, @function
ff_put_qpel16_mc33_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$17,1252($sp)
move	$17,$4
move	$4,$16
sw	$31,1284($sp)
sw	$21,1268($sp)
addiu	$21,$sp,464
sw	$20,1264($sp)
addiu	$20,$sp,736
sw	$19,1260($sp)
addiu	$19,$sp,992
sw	$18,1256($sp)
move	$18,$6
sw	$fp,1280($sp)
sw	$23,1276($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$22,1272($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$21
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$5,$sp,57
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$21
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$3,24			# 0x18
sw	$20,16($sp)
addiu	$5,$sp,81
sw	$18,20($sp)
addiu	$6,$sp,480
sw	$2,28($sp)
move	$4,$17
sw	$3,24($sp)
sw	$2,32($sp)
move	$7,$19
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,33685504			# 0x2020000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$4,$0
move	$14,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,514
addiu	$6,$6,3855
li	$24,384			# 0x180
ori	$12,$12,0xfcfc
$L2446:
addu	$3,$16,$5
addu	$2,$16,$14
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 36($3)  
lwr $10, 33($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 435($2)  
lwr $9, 432($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 40($3)  
lwr $8, 37($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 439($2)  
lwr $22, 436($2)  

# 0 "" 2
#NO_APP
and	$3,$9,$13
and	$2,$22,$13
and	$19,$10,$13
and	$11,$8,$13
addu	$11,$11,$2
addu	$19,$19,$3
addiu	$7,$14,8
addiu	$3,$14,11
addiu	$fp,$14,12
addiu	$23,$14,15
addu	$25,$11,$15
and	$9,$9,$12
and	$10,$10,$12
addu	$19,$19,$15
and	$22,$22,$12
and	$8,$8,$12
addu	$3,$16,$3
addu	$7,$16,$7
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($3)  
lwr $21, 936($7)  

# 0 "" 2
#NO_APP
addu	$fp,$16,$fp
and	$11,$21,$13
srl	$9,$9,2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 936($23)  
lwr $20, 936($fp)  

# 0 "" 2
#NO_APP
srl	$10,$10,2
and	$2,$20,$13
srl	$22,$22,2
srl	$8,$8,2
addu	$19,$19,$11
and	$21,$21,$12
addu	$11,$25,$2
and	$20,$20,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 680($3)  
lwr $25, 680($7)  

# 0 "" 2
#NO_APP
addu	$10,$9,$10
and	$7,$25,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($23)  
lwr $3, 680($fp)  

# 0 "" 2
#NO_APP
addu	$8,$22,$8
and	$2,$3,$13
srl	$9,$21,2
srl	$20,$20,2
addu	$19,$19,$7
addu	$2,$11,$2
and	$25,$25,$12
and	$3,$3,$12
addu	$7,$8,$20
addu	$9,$10,$9
srl	$3,$3,2
srl	$10,$25,2
srl	$11,$19,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$10
and	$7,$11,$6
and	$2,$2,$6
addu	$2,$3,$2
addu	$7,$8,$7
addu	$3,$17,$4
addiu	$5,$5,24
sw	$7,8($3)
addiu	$14,$14,16
sw	$2,12($3)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2446
addu	$4,$4,$18
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_qpel16_mc33_old_c
.size	ff_put_qpel16_mc33_old_c, .-ff_put_qpel16_mc33_old_c
.align	2
.globl	ff_put_qpel16_mc12_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel16_mc12_old_c
.type	ff_put_qpel16_mc12_old_c, @function
ff_put_qpel16_mc12_old_c:
.frame	$sp,1264,$31		# vars= 1192, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1264
sw	$16,1236($sp)
addiu	$16,$sp,40
sw	$21,1256($sp)
move	$21,$4
move	$4,$16
sw	$31,1260($sp)
sw	$20,1252($sp)
addiu	$20,$sp,448
sw	$19,1248($sp)
addiu	$19,$sp,976
sw	$18,1244($sp)
addiu	$18,$sp,720
sw	$17,1240($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$21
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$21,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L2450:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 936($3)  
lwr $12, 936($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 680($3)  
lwr $8, 680($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 936($9)  
lwr $5, 936($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($9)  
lwr $3, 680($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L2450
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1260($sp)
lw	$21,1256($sp)
lw	$20,1252($sp)
lw	$19,1248($sp)
lw	$18,1244($sp)
lw	$17,1240($sp)
lw	$16,1236($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1264
.set	macro
.set	reorder

.end	ff_put_qpel16_mc12_old_c
.size	ff_put_qpel16_mc12_old_c, .-ff_put_qpel16_mc12_old_c
.align	2
.globl	ff_put_qpel16_mc32_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_qpel16_mc32_old_c
.type	ff_put_qpel16_mc32_old_c, @function
ff_put_qpel16_mc32_old_c:
.frame	$sp,1264,$31		# vars= 1192, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1264
sw	$16,1236($sp)
addiu	$16,$sp,40
sw	$21,1256($sp)
move	$21,$4
move	$4,$16
sw	$31,1260($sp)
sw	$20,1252($sp)
addiu	$20,$sp,448
sw	$19,1248($sp)
addiu	$19,$sp,976
sw	$18,1244($sp)
addiu	$18,$sp,720
sw	$17,1240($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$5,$sp,41
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$20
.set	macro
.set	reorder

li	$2,16			# 0x10
move	$4,$21
move	$6,$18
sw	$2,16($sp)
move	$7,$17
sw	$2,20($sp)
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
addiu	$4,$21,8
move	$2,$0
li	$7,256			# 0x100
ori	$6,$6,0xfefe
$L2454:
addiu	$5,$2,8
addiu	$3,$2,11
addiu	$11,$2,12
addiu	$9,$2,15
addu	$3,$16,$3
addu	$5,$16,$5
addu	$9,$16,$9
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 936($3)  
lwr $12, 936($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 680($3)  
lwr $8, 680($5)  

# 0 "" 2
#NO_APP
addu	$11,$16,$11
xor	$10,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 936($9)  
lwr $5, 936($11)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($9)  
lwr $3, 680($11)  

# 0 "" 2
#NO_APP
xor	$9,$3,$5
and	$10,$10,$6
and	$9,$9,$6
srl	$10,$10,1
srl	$9,$9,1
or	$8,$8,$12
or	$3,$3,$5
subu	$8,$8,$10
subu	$3,$3,$9
addiu	$2,$2,16
sw	$8,0($4)
sw	$3,4($4)
.set	noreorder
.set	nomacro
bne	$2,$7,$L2454
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1260($sp)
lw	$21,1256($sp)
lw	$20,1252($sp)
lw	$19,1248($sp)
lw	$18,1244($sp)
lw	$17,1240($sp)
lw	$16,1236($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1264
.set	macro
.set	reorder

.end	ff_put_qpel16_mc32_old_c
.size	ff_put_qpel16_mc32_old_c, .-ff_put_qpel16_mc32_old_c
.align	2
.globl	ff_put_no_rnd_qpel8_mc11_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel8_mc11_old_c
.type	ff_put_no_rnd_qpel8_mc11_old_c, @function
ff_put_no_rnd_qpel8_mc11_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
li	$6,8			# 0x8
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$6,16842752			# 0x1010000
li	$5,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
addiu	$24,$sp,160
move	$15,$0
addiu	$13,$13,771
addiu	$6,$6,257
addiu	$5,$5,3855
move	$14,$16
move	$4,$19
ori	$12,$12,0xfcfc
$L2458:
addiu	$19,$15,3
addiu	$22,$15,4
addiu	$18,$15,7
addu	$23,$16,$15
addu	$19,$16,$19
addu	$18,$16,$18
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 3($14)  
lwr $10, 0($14)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 7($14)  
lwr $8, 4($14)  

# 0 "" 2
#NO_APP
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 144($19)  
lwr $9, 144($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 144($18)  
lwr $3, 144($22)  

# 0 "" 2
#NO_APP
and	$20,$10,$13
and	$2,$8,$13
and	$25,$9,$13
and	$11,$3,$13
addu	$25,$20,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$6
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($19)  
lwr $21, 280($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$20,$13
and	$fp,$21,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$21,$21,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($19)  
lwr $2, 216($23)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$2
and	$20,$20,$12
addu	$10,$9,$10
and	$7,$19,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($18)  
lwr $2, 216($22)  

# 0 "" 2
#NO_APP
srl	$3,$20,2
move	$18,$2
srl	$9,$21,2
and	$2,$2,$13
addu	$25,$25,$7
and	$19,$19,$12
and	$18,$18,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$19,$19,2
srl	$3,$18,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$19
and	$7,$10,$5
and	$2,$2,$5
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$14,$14,16
sw	$7,0($4)
addiu	$15,$15,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$14,$24,$L2458
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel8_mc11_old_c
.size	ff_put_no_rnd_qpel8_mc11_old_c, .-ff_put_no_rnd_qpel8_mc11_old_c
.align	2
.globl	ff_put_no_rnd_qpel8_mc31_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel8_mc31_old_c
.type	ff_put_no_rnd_qpel8_mc31_old_c, @function
ff_put_no_rnd_qpel8_mc31_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
addiu	$5,$sp,33
li	$6,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
li	$7,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,16842752			# 0x1010000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$14,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,257
addiu	$6,$6,3855
li	$24,128			# 0x80
move	$4,$19
ori	$12,$12,0xfcfc
$L2462:
addiu	$19,$14,3
addiu	$22,$14,4
addiu	$18,$14,7
addu	$23,$16,$14
addu	$2,$16,$5
addu	$19,$16,$19
addu	$18,$16,$18
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 4($2)  
lwr $10, 1($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 144($19)  
lwr $9, 144($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 144($18)  
lwr $3, 144($22)  

# 0 "" 2
#NO_APP
and	$20,$10,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 8($2)  
lwr $8, 5($2)  

# 0 "" 2
#NO_APP
and	$25,$9,$13
and	$2,$8,$13
and	$11,$3,$13
addu	$25,$20,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($19)  
lwr $21, 280($23)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$20,$13
and	$fp,$21,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$21,$21,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($19)  
lwr $2, 216($23)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$2
and	$20,$20,$12
addu	$10,$9,$10
and	$7,$19,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($18)  
lwr $2, 216($22)  

# 0 "" 2
#NO_APP
srl	$3,$20,2
move	$18,$2
srl	$9,$21,2
and	$2,$2,$13
addu	$25,$25,$7
and	$19,$19,$12
and	$18,$18,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$19,$19,2
srl	$3,$18,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$19
and	$7,$10,$6
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$5,$5,16
sw	$7,0($4)
addiu	$14,$14,8
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2462
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel8_mc31_old_c
.size	ff_put_no_rnd_qpel8_mc31_old_c, .-ff_put_no_rnd_qpel8_mc31_old_c
.align	2
.globl	ff_put_no_rnd_qpel8_mc13_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel8_mc13_old_c
.type	ff_put_no_rnd_qpel8_mc13_old_c, @function
ff_put_no_rnd_qpel8_mc13_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
li	$6,8			# 0x8
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$12,50528256			# 0x3030000
li	$14,16842752			# 0x1010000
li	$13,252641280			# 0xf0f0000
li	$11,-50593792			# 0xfffffffffcfc0000
move	$18,$0
move	$2,$0
addiu	$12,$12,771
addiu	$14,$14,257
addiu	$13,$13,3855
li	$15,128			# 0x80
move	$4,$19
ori	$11,$11,0xfcfc
$L2466:
addiu	$6,$2,16
addiu	$5,$18,8
addu	$19,$16,$18
addu	$2,$16,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 159($19)  
lwr $7, 156($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 23($2)  
lwr $8, 20($2)  

# 0 "" 2
#NO_APP
addu	$9,$16,$6
addu	$3,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 19($2)  
lwr $10, 0($9)  

# 0 "" 2
#NO_APP
and	$24,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 155($19)  
lwr $9, 144($3)  

# 0 "" 2
#NO_APP
and	$20,$10,$12
and	$2,$7,$12
and	$25,$9,$12
addu	$25,$20,$25
addu	$24,$24,$2
addiu	$23,$18,3
addiu	$22,$18,4
addiu	$18,$18,7
and	$2,$7,$11
and	$9,$9,$11
and	$10,$10,$11
addu	$25,$25,$14
and	$8,$8,$11
addu	$24,$24,$14
addu	$18,$16,$18
addu	$23,$16,$23
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($23)  
lwr $21, 280($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$3,$20,$12
and	$fp,$21,$12
srl	$2,$2,2
srl	$10,$10,2
srl	$8,$8,2
addu	$24,$24,$3
and	$21,$21,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 216($23)  
lwr $3, 216($19)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$3
and	$20,$20,$11
addu	$10,$9,$10
and	$7,$19,$12
addu	$8,$2,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 216($18)  
lwr $3, 216($22)  

# 0 "" 2
#NO_APP
srl	$2,$20,2
move	$18,$3
srl	$9,$21,2
and	$3,$3,$12
addu	$25,$25,$7
and	$18,$18,$11
and	$19,$19,$11
addu	$3,$24,$3
addu	$7,$8,$2
addu	$9,$10,$9
srl	$2,$18,2
srl	$19,$19,2
srl	$10,$25,2
srl	$3,$3,2
addu	$2,$7,$2
addu	$8,$9,$19
and	$7,$10,$13
and	$3,$3,$13
addu	$3,$2,$3
addu	$7,$8,$7
move	$2,$6
sw	$3,4($4)
move	$18,$5
sw	$7,0($4)
.set	noreorder
.set	nomacro
bne	$6,$15,$L2466
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel8_mc13_old_c
.size	ff_put_no_rnd_qpel8_mc13_old_c, .-ff_put_no_rnd_qpel8_mc13_old_c
.align	2
.globl	ff_put_no_rnd_qpel8_mc33_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel8_mc33_old_c
.type	ff_put_no_rnd_qpel8_mc33_old_c, @function
ff_put_no_rnd_qpel8_mc33_old_c:
.frame	$sp,416,$31		# vars= 344, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-416
sw	$16,376($sp)
addiu	$16,$sp,32
sw	$19,388($sp)
move	$19,$4
move	$4,$16
sw	$31,412($sp)
sw	$18,384($sp)
addiu	$18,$sp,176
sw	$17,380($sp)
move	$17,$6
sw	$fp,408($sp)
sw	$23,404($sp)
sw	$22,400($sp)
sw	$21,396($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
sw	$20,392($sp)
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
addiu	$5,$sp,33
li	$6,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
li	$7,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,16842752			# 0x1010000
li	$14,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$18,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,257
addiu	$14,$14,3855
li	$24,128			# 0x80
move	$4,$19
ori	$12,$12,0xfcfc
$L2470:
addiu	$6,$18,8
addu	$19,$16,$18
addu	$2,$16,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 159($19)  
lwr $3, 156($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 20($2)  
lwr $10, 17($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 24($2)  
lwr $8, 21($2)  

# 0 "" 2
#NO_APP
and	$20,$10,$13
addu	$2,$16,$6
and	$11,$8,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 155($19)  
lwr $9, 144($2)  

# 0 "" 2
#NO_APP
and	$2,$3,$13
and	$25,$9,$13
addu	$25,$20,$25
addu	$11,$11,$2
addiu	$23,$18,3
addiu	$22,$18,4
addiu	$18,$18,7
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
addu	$18,$16,$18
addu	$23,$16,$23
addu	$22,$16,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 280($23)  
lwr $21, 280($19)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 280($18)  
lwr $20, 280($22)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$20,$13
and	$fp,$21,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$21,$21,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($23)  
lwr $2, 216($19)  

# 0 "" 2
#NO_APP
addu	$25,$25,$fp
move	$19,$2
and	$20,$20,$12
addu	$10,$9,$10
and	$7,$19,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($18)  
lwr $2, 216($22)  

# 0 "" 2
#NO_APP
srl	$3,$20,2
move	$18,$2
srl	$9,$21,2
and	$2,$2,$13
addu	$25,$25,$7
and	$18,$18,$12
and	$19,$19,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$3,$18,2
srl	$19,$19,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$19
and	$7,$10,$14
and	$2,$2,$14
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$5,$5,16
sw	$7,0($4)
move	$18,$6
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2470
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,412($sp)
lw	$fp,408($sp)
lw	$23,404($sp)
lw	$22,400($sp)
lw	$21,396($sp)
lw	$20,392($sp)
lw	$19,388($sp)
lw	$18,384($sp)
lw	$17,380($sp)
lw	$16,376($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,416
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel8_mc33_old_c
.size	ff_put_no_rnd_qpel8_mc33_old_c, .-ff_put_no_rnd_qpel8_mc33_old_c
.align	2
.globl	ff_put_no_rnd_qpel8_mc12_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel8_mc12_old_c
.type	ff_put_no_rnd_qpel8_mc12_old_c, @function
ff_put_no_rnd_qpel8_mc12_old_c:
.frame	$sp,400,$31		# vars= 344, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-400
sw	$16,380($sp)
addiu	$16,$sp,32
sw	$19,392($sp)
move	$19,$4
move	$4,$16
sw	$31,396($sp)
sw	$18,388($sp)
addiu	$18,$sp,176
sw	$17,384($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
li	$6,8			# 0x8
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$9,$0
li	$11,64			# 0x40
move	$4,$19
ori	$6,$6,0xfefe
$L2474:
addiu	$2,$9,3
addiu	$10,$9,4
addiu	$3,$9,7
addu	$5,$16,$9
addu	$2,$16,$2
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 280($2)  
lwr $12, 280($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 216($2)  
lwr $7, 216($5)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$8,$7,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 280($3)  
lwr $5, 280($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($3)  
lwr $2, 216($10)  

# 0 "" 2
#NO_APP
xor	$3,$2,$5
and	$8,$8,$6
and	$3,$3,$6
srl	$8,$8,1
srl	$3,$3,1
and	$7,$7,$12
and	$2,$2,$5
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$9,$9,8
sw	$7,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$9,$11,$L2474
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,396($sp)
lw	$19,392($sp)
lw	$18,388($sp)
lw	$17,384($sp)
lw	$16,380($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,400
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel8_mc12_old_c
.size	ff_put_no_rnd_qpel8_mc12_old_c, .-ff_put_no_rnd_qpel8_mc12_old_c
.align	2
.globl	ff_put_no_rnd_qpel8_mc32_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel8_mc32_old_c
.type	ff_put_no_rnd_qpel8_mc32_old_c, @function
ff_put_no_rnd_qpel8_mc32_old_c:
.frame	$sp,400,$31		# vars= 344, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-400
sw	$16,380($sp)
addiu	$16,$sp,32
sw	$19,392($sp)
move	$19,$4
move	$4,$16
sw	$31,396($sp)
sw	$18,388($sp)
addiu	$18,$sp,176
sw	$17,384($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block9.constprop.42
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,312
addiu	$5,$sp,33
li	$6,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
li	$7,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$sp,248
li	$6,8			# 0x8
li	$7,8			# 0x8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$6,-16908288			# 0xfffffffffefe0000
move	$9,$0
li	$11,64			# 0x40
move	$4,$19
ori	$6,$6,0xfefe
$L2478:
addiu	$2,$9,3
addiu	$10,$9,4
addiu	$3,$9,7
addu	$5,$16,$9
addu	$2,$16,$2
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 280($2)  
lwr $12, 280($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 216($2)  
lwr $7, 216($5)  

# 0 "" 2
#NO_APP
addu	$10,$16,$10
xor	$8,$7,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 280($3)  
lwr $5, 280($10)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 216($3)  
lwr $2, 216($10)  

# 0 "" 2
#NO_APP
xor	$3,$2,$5
and	$8,$8,$6
and	$3,$3,$6
srl	$8,$8,1
srl	$3,$3,1
and	$7,$7,$12
and	$2,$2,$5
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$9,$9,8
sw	$7,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$9,$11,$L2478
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,396($sp)
lw	$19,392($sp)
lw	$18,388($sp)
lw	$17,384($sp)
lw	$16,380($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,400
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel8_mc32_old_c
.size	ff_put_no_rnd_qpel8_mc32_old_c, .-ff_put_no_rnd_qpel8_mc32_old_c
.align	2
.globl	ff_put_no_rnd_qpel16_mc11_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel16_mc11_old_c
.type	ff_put_no_rnd_qpel16_mc11_old_c, @function
ff_put_no_rnd_qpel16_mc11_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$18,1256($sp)
move	$18,$4
move	$4,$16
sw	$31,1284($sp)
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$17,1252($sp)
move	$17,$6
sw	$fp,1280($sp)
sw	$23,1276($sp)
sw	$22,1272($sp)
sw	$21,1268($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,1264($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,992
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,736
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,16842752			# 0x1010000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
addiu	$14,$sp,59
addiu	$24,$sp,443
move	$4,$0
addiu	$13,$13,771
addiu	$15,$15,257
addiu	$6,$6,3855
move	$5,$18
ori	$12,$12,0xfcfc
$L2482:
addiu	$20,$4,3
addiu	$23,$4,4
addiu	$19,$4,7
addu	$fp,$16,$4
addu	$20,$16,$20
addu	$19,$16,$19
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 0($14)  
lwr $10, -3($14)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 4($14)  
lwr $8, 1($14)  

# 0 "" 2
#NO_APP
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 408($20)  
lwr $9, 408($fp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 408($19)  
lwr $3, 408($23)  

# 0 "" 2
#NO_APP
and	$21,$10,$13
and	$2,$8,$13
and	$25,$9,$13
and	$11,$3,$13
addu	$25,$21,$25
addu	$11,$2,$11
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 936($20)  
lwr $22, 936($fp)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($19)  
lwr $21, 936($23)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$21,$13
and	$31,$22,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$22,$22,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($20)  
lwr $2, 680($fp)  

# 0 "" 2
#NO_APP
addu	$25,$25,$31
move	$20,$2
and	$21,$21,$12
addu	$10,$9,$10
and	$7,$20,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($19)  
lwr $2, 680($23)  

# 0 "" 2
#NO_APP
srl	$3,$21,2
move	$19,$2
srl	$9,$22,2
and	$2,$2,$13
addu	$25,$25,$7
and	$20,$20,$12
and	$19,$19,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$20,$20,2
srl	$3,$19,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$20
and	$7,$10,$6
and	$2,$2,$6
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$14,$14,24
sw	$7,0($5)
addiu	$4,$4,16
sw	$2,4($5)
.set	noreorder
.set	nomacro
bne	$14,$24,$L2482
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$3,$sp,744
sw	$17,20($sp)
li	$2,16			# 0x10
addiu	$4,$18,8
sw	$3,16($sp)
li	$3,24			# 0x18
addiu	$5,$sp,64
sw	$2,28($sp)
addiu	$6,$sp,472
sw	$2,32($sp)
addiu	$7,$sp,1000
sw	$3,24($sp)
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel16_mc11_old_c
.size	ff_put_no_rnd_qpel16_mc11_old_c, .-ff_put_no_rnd_qpel16_mc11_old_c
.align	2
.globl	ff_put_no_rnd_qpel16_mc31_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel16_mc31_old_c
.type	ff_put_no_rnd_qpel16_mc31_old_c, @function
ff_put_no_rnd_qpel16_mc31_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$17,1252($sp)
move	$17,$4
move	$4,$16
sw	$31,1284($sp)
sw	$22,1272($sp)
addiu	$22,$sp,736
sw	$21,1268($sp)
addiu	$21,$sp,57
sw	$20,1264($sp)
addiu	$20,$sp,992
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$18,1256($sp)
move	$18,$6
sw	$fp,1280($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$23,1276($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$21
.set	macro
.set	reorder

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$2,16			# 0x10
li	$3,24			# 0x18
sw	$22,16($sp)
move	$4,$17
sw	$18,20($sp)
move	$5,$21
sw	$2,28($sp)
move	$6,$19
sw	$3,24($sp)
sw	$2,32($sp)
move	$7,$20
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,16842752			# 0x1010000
li	$6,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$4,$0
move	$14,$0
move	$5,$0
addiu	$13,$13,771
addiu	$15,$15,257
addiu	$6,$6,3855
li	$24,384			# 0x180
ori	$12,$12,0xfcfc
$L2486:
addiu	$3,$14,8
addiu	$25,$14,11
addiu	$fp,$14,12
addiu	$23,$14,15
addu	$2,$16,$5
addu	$25,$16,$25
addu	$3,$16,$3
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 408($25)  
lwr $9, 408($3)  

# 0 "" 2
#NO_APP
addu	$fp,$16,$fp
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 12($2)  
lwr $10, 9($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 16($2)  
lwr $8, 13($2)  

# 0 "" 2
#NO_APP
and	$7,$9,$13
and	$11,$8,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 408($23)  
lwr $22, 408($fp)  

# 0 "" 2
#NO_APP
and	$19,$10,$13
and	$2,$22,$13
addu	$19,$19,$7
addu	$2,$11,$2
and	$9,$9,$12
addu	$2,$2,$15
and	$10,$10,$12
addu	$19,$19,$15
and	$22,$22,$12
and	$8,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($25)  
lwr $21, 936($3)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 936($23)  
lwr $20, 936($fp)  

# 0 "" 2
#NO_APP
and	$7,$21,$13
srl	$9,$9,2
and	$11,$20,$13
srl	$10,$10,2
srl	$22,$22,2
srl	$8,$8,2
addu	$11,$2,$11
addu	$19,$19,$7
and	$21,$21,$12
and	$20,$20,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($25)  
lwr $2, 680($3)  

# 0 "" 2
#NO_APP
addu	$10,$9,$10
and	$7,$2,$13
move	$25,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 680($23)  
lwr $3, 680($fp)  

# 0 "" 2
#NO_APP
addu	$8,$22,$8
and	$2,$3,$13
srl	$9,$21,2
srl	$20,$20,2
addu	$19,$19,$7
addu	$2,$11,$2
and	$25,$25,$12
and	$3,$3,$12
addu	$7,$8,$20
addu	$9,$10,$9
srl	$3,$3,2
srl	$10,$25,2
srl	$11,$19,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$10
and	$7,$11,$6
and	$2,$2,$6
addu	$2,$3,$2
addu	$7,$8,$7
addu	$3,$17,$4
addiu	$5,$5,24
sw	$7,8($3)
addiu	$14,$14,16
sw	$2,12($3)
.set	noreorder
.set	nomacro
bne	$5,$24,$L2486
addu	$4,$4,$18
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel16_mc31_old_c
.size	ff_put_no_rnd_qpel16_mc31_old_c, .-ff_put_no_rnd_qpel16_mc31_old_c
.align	2
.globl	ff_put_no_rnd_qpel16_mc13_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel16_mc13_old_c
.type	ff_put_no_rnd_qpel16_mc13_old_c, @function
ff_put_no_rnd_qpel16_mc13_old_c:
.frame	$sp,1280,$31		# vars= 1192, regs= 8/0, args= 48, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1280
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$22,1272($sp)
move	$22,$4
move	$4,$16
sw	$31,1276($sp)
sw	$21,1268($sp)
move	$21,$6
sw	$20,1264($sp)
li	$20,24			# 0x18
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$18,1256($sp)
addiu	$18,$sp,736
sw	$17,1252($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
addiu	$17,$sp,992

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$17
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16

li	$16,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19

addiu	$5,$sp,80
addiu	$6,$sp,480
sw	$18,16($sp)
move	$4,$22
sw	$21,20($sp)
move	$7,$17
sw	$20,24($sp)
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	put_no_rnd_pixels8_l4
.option	pic2
sw	$16,40($sp)

addiu	$2,$sp,744
addiu	$4,$22,8
sw	$21,20($sp)
addiu	$5,$sp,88
sw	$20,24($sp)
addiu	$6,$sp,488
sw	$2,16($sp)
addiu	$7,$sp,1000
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	put_no_rnd_pixels8_l4
.option	pic2
sw	$16,40($sp)

lw	$31,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
j	$31
addiu	$sp,$sp,1280

.set	macro
.set	reorder
.end	ff_put_no_rnd_qpel16_mc13_old_c
.size	ff_put_no_rnd_qpel16_mc13_old_c, .-ff_put_no_rnd_qpel16_mc13_old_c
.align	2
.globl	ff_put_no_rnd_qpel16_mc33_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel16_mc33_old_c
.type	ff_put_no_rnd_qpel16_mc33_old_c, @function
ff_put_no_rnd_qpel16_mc33_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 10/0, args= 48, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1288
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$18,1256($sp)
move	$18,$4
move	$4,$16
sw	$31,1284($sp)
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$17,1252($sp)
move	$17,$6
sw	$fp,1280($sp)
sw	$23,1276($sp)
sw	$22,1272($sp)
sw	$21,1268($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
sw	$20,1264($sp)
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,992
addiu	$5,$sp,57
li	$6,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
li	$7,24			# 0x18
.set	macro
.set	reorder

addiu	$4,$sp,736
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$13,50528256			# 0x3030000
li	$15,16842752			# 0x1010000
li	$14,252641280			# 0xf0f0000
li	$12,-50593792			# 0xfffffffffcfc0000
move	$19,$0
move	$4,$0
addiu	$13,$13,771
addiu	$15,$15,257
addiu	$14,$14,3855
li	$24,384			# 0x180
move	$5,$18
ori	$12,$12,0xfcfc
$L2492:
addiu	$6,$19,16
addu	$20,$16,$19
addu	$2,$16,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 431($20)  
lwr $3, 428($20)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $10, 28($2)  
lwr $10, 25($2)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 32($2)  
lwr $8, 29($2)  

# 0 "" 2
#NO_APP
and	$21,$10,$13
addu	$2,$16,$6
and	$11,$8,$13
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 427($20)  
lwr $9, 408($2)  

# 0 "" 2
#NO_APP
and	$2,$3,$13
and	$25,$9,$13
addu	$25,$21,$25
addu	$11,$11,$2
addiu	$fp,$19,3
addiu	$23,$19,4
addiu	$19,$19,7
and	$9,$9,$12
and	$10,$10,$12
addu	$25,$25,$15
and	$3,$3,$12
and	$8,$8,$12
addu	$11,$11,$15
addu	$19,$16,$19
addu	$fp,$16,$fp
addu	$23,$16,$23
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 936($fp)  
lwr $22, 936($20)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 936($19)  
lwr $21, 936($23)  

# 0 "" 2
#NO_APP
srl	$9,$9,2
and	$2,$21,$13
and	$31,$22,$13
srl	$3,$3,2
srl	$10,$10,2
srl	$8,$8,2
addu	$11,$11,$2
and	$22,$22,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($fp)  
lwr $2, 680($20)  

# 0 "" 2
#NO_APP
addu	$25,$25,$31
move	$20,$2
and	$21,$21,$12
addu	$10,$9,$10
and	$7,$20,$13
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($19)  
lwr $2, 680($23)  

# 0 "" 2
#NO_APP
srl	$3,$21,2
move	$19,$2
srl	$9,$22,2
and	$2,$2,$13
addu	$25,$25,$7
and	$19,$19,$12
and	$20,$20,$12
addu	$2,$11,$2
addu	$7,$8,$3
addu	$9,$10,$9
srl	$3,$19,2
srl	$20,$20,2
srl	$10,$25,2
srl	$2,$2,2
addu	$3,$7,$3
addu	$8,$9,$20
and	$7,$10,$14
and	$2,$2,$14
addu	$7,$8,$7
addu	$2,$3,$2
addiu	$4,$4,24
sw	$7,0($5)
move	$19,$6
sw	$2,4($5)
.set	noreorder
.set	nomacro
bne	$4,$24,$L2492
addu	$5,$5,$17
.set	macro
.set	reorder

addiu	$3,$sp,744
sw	$17,20($sp)
li	$2,16			# 0x10
addiu	$4,$18,8
sw	$3,16($sp)
li	$3,24			# 0x18
addiu	$5,$sp,89
sw	$2,28($sp)
addiu	$6,$sp,488
sw	$2,32($sp)
addiu	$7,$sp,1000
sw	$3,24($sp)
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,1284($sp)
lw	$fp,1280($sp)
lw	$23,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1288
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel16_mc33_old_c
.size	ff_put_no_rnd_qpel16_mc33_old_c, .-ff_put_no_rnd_qpel16_mc33_old_c
.align	2
.globl	ff_put_no_rnd_qpel16_mc12_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel16_mc12_old_c
.type	ff_put_no_rnd_qpel16_mc12_old_c, @function
ff_put_no_rnd_qpel16_mc12_old_c:
.frame	$sp,1248,$31		# vars= 1192, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1248
sw	$16,1228($sp)
addiu	$16,$sp,32
sw	$18,1236($sp)
move	$18,$4
move	$4,$16
sw	$31,1244($sp)
sw	$19,1240($sp)
addiu	$19,$sp,440
sw	$17,1232($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,968
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,712
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
move	$8,$0
li	$10,256			# 0x100
move	$4,$18
ori	$9,$9,0xfefe
$L2496:
addiu	$2,$8,3
addiu	$6,$8,4
addiu	$3,$8,7
addu	$5,$16,$8
addu	$2,$16,$2
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 936($2)  
lwr $12, 936($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 680($2)  
lwr $7, 680($5)  

# 0 "" 2
#NO_APP
addu	$6,$16,$6
move	$5,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 936($3)  
lwr $11, 936($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($3)  
lwr $2, 680($6)  

# 0 "" 2
#NO_APP
xor	$7,$7,$12
xor	$3,$2,$11
and	$7,$7,$9
and	$3,$3,$9
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$12
and	$2,$2,$11
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,16
sw	$5,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$10,$L2496
addu	$4,$4,$17
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
addiu	$4,$18,8
move	$8,$0
li	$10,256			# 0x100
ori	$9,$9,0xfefe
$L2497:
addiu	$7,$8,8
addiu	$2,$8,11
addiu	$6,$8,12
addiu	$3,$8,15
addu	$2,$16,$2
addu	$7,$16,$7
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 936($2)  
lwr $12, 936($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 680($2)  
lwr $5, 680($7)  

# 0 "" 2
#NO_APP
addu	$6,$16,$6
xor	$7,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 936($3)  
lwr $11, 936($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($3)  
lwr $2, 680($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$11
and	$7,$7,$9
and	$3,$3,$9
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$12
and	$2,$2,$11
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,16
sw	$5,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$10,$L2497
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1244($sp)
lw	$19,1240($sp)
lw	$18,1236($sp)
lw	$17,1232($sp)
lw	$16,1228($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1248
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel16_mc12_old_c
.size	ff_put_no_rnd_qpel16_mc12_old_c, .-ff_put_no_rnd_qpel16_mc12_old_c
.align	2
.globl	ff_put_no_rnd_qpel16_mc32_old_c
.set	nomips16
.set	nomicromips
.ent	ff_put_no_rnd_qpel16_mc32_old_c
.type	ff_put_no_rnd_qpel16_mc32_old_c, @function
ff_put_no_rnd_qpel16_mc32_old_c:
.frame	$sp,1248,$31		# vars= 1192, regs= 5/0, args= 24, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-1248
sw	$16,1228($sp)
addiu	$16,$sp,32
sw	$18,1236($sp)
move	$18,$4
move	$4,$16
sw	$31,1244($sp)
sw	$19,1240($sp)
addiu	$19,$sp,440
sw	$17,1232($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	copy_block17.constprop.40
.option	pic2
move	$17,$6
.set	macro
.set	reorder

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

addiu	$4,$sp,968
addiu	$5,$sp,33
li	$6,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
li	$7,24			# 0x18
.set	macro
.set	reorder

addiu	$4,$sp,712
li	$6,16			# 0x10
li	$7,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
jal	put_no_rnd_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
move	$8,$0
li	$10,256			# 0x100
move	$4,$18
ori	$9,$9,0xfefe
$L2502:
addiu	$2,$8,3
addiu	$6,$8,4
addiu	$3,$8,7
addu	$5,$16,$8
addu	$2,$16,$2
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 936($2)  
lwr $12, 936($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 680($2)  
lwr $7, 680($5)  

# 0 "" 2
#NO_APP
addu	$6,$16,$6
move	$5,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 936($3)  
lwr $11, 936($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($3)  
lwr $2, 680($6)  

# 0 "" 2
#NO_APP
xor	$7,$7,$12
xor	$3,$2,$11
and	$7,$7,$9
and	$3,$3,$9
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$12
and	$2,$2,$11
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,16
sw	$5,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$10,$L2502
addu	$4,$4,$17
.set	macro
.set	reorder

li	$9,-16908288			# 0xfffffffffefe0000
addiu	$4,$18,8
move	$8,$0
li	$10,256			# 0x100
ori	$9,$9,0xfefe
$L2503:
addiu	$7,$8,8
addiu	$2,$8,11
addiu	$6,$8,12
addiu	$3,$8,15
addu	$2,$16,$2
addu	$7,$16,$7
addu	$3,$16,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 936($2)  
lwr $12, 936($7)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 680($2)  
lwr $5, 680($7)  

# 0 "" 2
#NO_APP
addu	$6,$16,$6
xor	$7,$5,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 936($3)  
lwr $11, 936($6)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 680($3)  
lwr $2, 680($6)  

# 0 "" 2
#NO_APP
xor	$3,$2,$11
and	$7,$7,$9
and	$3,$3,$9
srl	$7,$7,1
srl	$3,$3,1
and	$5,$5,$12
and	$2,$2,$11
addu	$5,$7,$5
addu	$2,$3,$2
addiu	$8,$8,16
sw	$5,0($4)
sw	$2,4($4)
.set	noreorder
.set	nomacro
bne	$8,$10,$L2503
addu	$4,$4,$17
.set	macro
.set	reorder

lw	$31,1244($sp)
lw	$19,1240($sp)
lw	$18,1236($sp)
lw	$17,1232($sp)
lw	$16,1228($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1248
.set	macro
.set	reorder

.end	ff_put_no_rnd_qpel16_mc32_old_c
.size	ff_put_no_rnd_qpel16_mc32_old_c, .-ff_put_no_rnd_qpel16_mc32_old_c
.align	2
.globl	ff_avg_qpel8_mc11_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel8_mc11_old_c
.type	ff_avg_qpel8_mc11_old_c, @function
ff_avg_qpel8_mc11_old_c:
.frame	$sp,432,$31		# vars= 344, regs= 7/0, args= 48, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-432
sw	$19,416($sp)
addiu	$19,$sp,56
addiu	$9,$sp,200
sw	$21,424($sp)
sw	$17,408($sp)
move	$21,$4
sw	$16,404($sp)
move	$17,$6
sw	$31,428($sp)
move	$16,$19
sw	$20,420($sp)
sw	$18,412($sp)
$L2508:
lbu	$8,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 3($16)  
swr $7, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$8,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$9,$L2508
addu	$5,$5,$17
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
addiu	$18,$sp,336
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
addiu	$20,$sp,272
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,16			# 0x10
sw	$20,16($sp)
sw	$17,20($sp)
move	$4,$21
sw	$2,28($sp)
move	$5,$19
sw	$3,24($sp)
move	$6,$16
sw	$2,32($sp)
move	$7,$18
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,428($sp)
lw	$21,424($sp)
lw	$20,420($sp)
lw	$19,416($sp)
lw	$18,412($sp)
lw	$17,408($sp)
lw	$16,404($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,432
.set	macro
.set	reorder

.end	ff_avg_qpel8_mc11_old_c
.size	ff_avg_qpel8_mc11_old_c, .-ff_avg_qpel8_mc11_old_c
.align	2
.globl	ff_avg_qpel8_mc31_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel8_mc31_old_c
.type	ff_avg_qpel8_mc31_old_c, @function
ff_avg_qpel8_mc31_old_c:
.frame	$sp,432,$31		# vars= 344, regs= 7/0, args= 48, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-432
addiu	$10,$sp,56
sw	$21,424($sp)
addiu	$9,$sp,200
sw	$17,408($sp)
sw	$16,404($sp)
move	$21,$4
sw	$31,428($sp)
move	$17,$6
sw	$20,420($sp)
move	$16,$10
sw	$19,416($sp)
sw	$18,412($sp)
$L2512:
lbu	$8,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 3($16)  
swr $7, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$8,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$9,$L2512
addu	$5,$5,$17
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
move	$5,$10
sw	$2,16($sp)
addiu	$18,$sp,336
addiu	$19,$sp,57
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
addiu	$20,$sp,272
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,16			# 0x10
sw	$20,16($sp)
sw	$17,20($sp)
move	$4,$21
sw	$2,28($sp)
move	$5,$19
sw	$3,24($sp)
move	$6,$16
sw	$2,32($sp)
move	$7,$18
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,428($sp)
lw	$21,424($sp)
lw	$20,420($sp)
lw	$19,416($sp)
lw	$18,412($sp)
lw	$17,408($sp)
lw	$16,404($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,432
.set	macro
.set	reorder

.end	ff_avg_qpel8_mc31_old_c
.size	ff_avg_qpel8_mc31_old_c, .-ff_avg_qpel8_mc31_old_c
.align	2
.globl	ff_avg_qpel8_mc13_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel8_mc13_old_c
.type	ff_avg_qpel8_mc13_old_c, @function
ff_avg_qpel8_mc13_old_c:
.frame	$sp,424,$31		# vars= 344, regs= 6/0, args= 48, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-424
sw	$19,412($sp)
addiu	$19,$sp,56
addiu	$9,$sp,200
sw	$20,416($sp)
sw	$17,404($sp)
move	$20,$4
sw	$16,400($sp)
move	$17,$6
sw	$31,420($sp)
move	$16,$19
sw	$18,408($sp)
$L2516:
lbu	$8,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 3($16)  
swr $7, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$8,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$9,$L2516
addu	$5,$5,$17
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
addiu	$18,$sp,336
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$19
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$18
move	$5,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
addiu	$19,$sp,272
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,16			# 0x10
sw	$19,16($sp)
addiu	$5,$sp,72
sw	$17,20($sp)
addiu	$6,$sp,208
sw	$2,28($sp)
sw	$3,24($sp)
move	$4,$20
sw	$2,32($sp)
move	$7,$18
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,420($sp)
lw	$20,416($sp)
lw	$19,412($sp)
lw	$18,408($sp)
lw	$17,404($sp)
lw	$16,400($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,424
.set	macro
.set	reorder

.end	ff_avg_qpel8_mc13_old_c
.size	ff_avg_qpel8_mc13_old_c, .-ff_avg_qpel8_mc13_old_c
.align	2
.globl	ff_avg_qpel8_mc33_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel8_mc33_old_c
.type	ff_avg_qpel8_mc33_old_c, @function
ff_avg_qpel8_mc33_old_c:
.frame	$sp,424,$31		# vars= 344, regs= 6/0, args= 48, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-424
addiu	$10,$sp,56
sw	$20,416($sp)
addiu	$9,$sp,200
sw	$17,404($sp)
sw	$16,400($sp)
move	$20,$4
sw	$31,420($sp)
move	$17,$6
sw	$19,412($sp)
move	$16,$10
sw	$18,408($sp)
$L2520:
lbu	$8,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $7, 3($16)  
swr $7, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$8,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$9,$L2520
addu	$5,$5,$17
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
move	$5,$10
sw	$2,16($sp)
addiu	$18,$sp,336
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$5,$sp,57
li	$6,8			# 0x8
li	$7,16			# 0x10
addiu	$19,$sp,272
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$4,$18
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$19
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
li	$3,16			# 0x10
sw	$19,16($sp)
addiu	$5,$sp,73
sw	$17,20($sp)
addiu	$6,$sp,208
sw	$2,28($sp)
sw	$3,24($sp)
move	$4,$20
sw	$2,32($sp)
move	$7,$18
sw	$2,36($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l4
.option	pic2
sw	$2,40($sp)
.set	macro
.set	reorder

lw	$31,420($sp)
lw	$20,416($sp)
lw	$19,412($sp)
lw	$18,408($sp)
lw	$17,404($sp)
lw	$16,400($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,424
.set	macro
.set	reorder

.end	ff_avg_qpel8_mc33_old_c
.size	ff_avg_qpel8_mc33_old_c, .-ff_avg_qpel8_mc33_old_c
.align	2
.globl	ff_avg_qpel8_mc12_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel8_mc12_old_c
.type	ff_avg_qpel8_mc12_old_c, @function
ff_avg_qpel8_mc12_old_c:
.frame	$sp,408,$31		# vars= 344, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-408
sw	$18,392($sp)
addiu	$18,$sp,40
addiu	$7,$sp,184
sw	$20,400($sp)
sw	$17,388($sp)
move	$20,$4
sw	$16,384($sp)
move	$17,$6
sw	$31,404($sp)
move	$16,$18
sw	$19,396($sp)
$L2524:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$9,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$7,$L2524
addu	$5,$5,$17
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
sw	$2,16($sp)
addiu	$19,$sp,320
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$5,$18
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,16			# 0x10
move	$4,$19
move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
addiu	$18,$sp,256
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$18
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,404($sp)
lw	$20,400($sp)
lw	$19,396($sp)
lw	$18,392($sp)
lw	$17,388($sp)
lw	$16,384($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,408
.set	macro
.set	reorder

.end	ff_avg_qpel8_mc12_old_c
.size	ff_avg_qpel8_mc12_old_c, .-ff_avg_qpel8_mc12_old_c
.align	2
.globl	ff_avg_qpel8_mc32_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel8_mc32_old_c
.type	ff_avg_qpel8_mc32_old_c, @function
ff_avg_qpel8_mc32_old_c:
.frame	$sp,408,$31		# vars= 344, regs= 6/0, args= 32, gp= 8
.mask	0x801f0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-408
addiu	$10,$sp,40
sw	$20,400($sp)
addiu	$7,$sp,184
sw	$17,388($sp)
sw	$16,384($sp)
move	$20,$4
sw	$31,404($sp)
move	$17,$6
sw	$19,396($sp)
move	$16,$10
sw	$18,392($sp)
$L2528:
lbu	$9,8($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($5)  
lwr $8, 0($5)  

# 0 "" 2
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 7($5)  
lwr $3, 4($5)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $8, 3($16)  
swr $8, 0($16)  

# 0 "" 2
# 42 "../libavutil/mips/intreadwrite.h" 1
swl $3, 7($16)  
swr $3, 4($16)  

# 0 "" 2
#NO_APP
sb	$9,8($16)
addiu	$16,$16,16
.set	noreorder
.set	nomacro
bne	$16,$7,$L2528
addu	$5,$5,$17
.set	macro
.set	reorder

li	$2,9			# 0x9
li	$6,8			# 0x8
li	$7,16			# 0x10
move	$5,$10
sw	$2,16($sp)
addiu	$19,$sp,320
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_h_lowpass
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$5,$sp,41
li	$6,8			# 0x8
li	$7,16			# 0x10
addiu	$18,$sp,256
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$4,$19
.set	macro
.set	reorder

li	$6,8			# 0x8
li	$7,8			# 0x8
move	$4,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	put_mpeg4_qpel8_v_lowpass
.option	pic2
move	$5,$16
.set	macro
.set	reorder

li	$2,8			# 0x8
move	$4,$20
move	$5,$19
sw	$2,16($sp)
move	$6,$18
sw	$2,20($sp)
move	$7,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	avg_pixels8_l2
.option	pic2
sw	$2,24($sp)
.set	macro
.set	reorder

lw	$31,404($sp)
lw	$20,400($sp)
lw	$19,396($sp)
lw	$18,392($sp)
lw	$17,388($sp)
lw	$16,384($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,408
.set	macro
.set	reorder

.end	ff_avg_qpel8_mc32_old_c
.size	ff_avg_qpel8_mc32_old_c, .-ff_avg_qpel8_mc32_old_c
.align	2
.globl	ff_avg_qpel16_mc11_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel16_mc11_old_c
.type	ff_avg_qpel16_mc11_old_c, @function
ff_avg_qpel16_mc11_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 9/0, args= 48, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1288
sw	$17,1256($sp)
addiu	$17,$sp,56
sw	$23,1280($sp)
move	$23,$4
move	$4,$17
sw	$31,1284($sp)
sw	$22,1276($sp)
move	$22,$6
sw	$21,1272($sp)
li	$21,24			# 0x18
sw	$20,1268($sp)
addiu	$20,$sp,736
sw	$19,1264($sp)
addiu	$19,$sp,992
sw	$18,1260($sp)
addiu	$18,$sp,464
sw	$16,1252($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
li	$16,16			# 0x10

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$17

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$19
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$17

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$20
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$18

move	$4,$23
move	$5,$17
sw	$20,16($sp)
move	$6,$18
sw	$22,20($sp)
move	$7,$19
sw	$21,24($sp)
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

addiu	$2,$sp,744
addiu	$4,$23,8
sw	$22,20($sp)
addiu	$5,$sp,64
sw	$21,24($sp)
addiu	$6,$sp,472
sw	$2,16($sp)
addiu	$7,$sp,1000
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

lw	$31,1284($sp)
lw	$23,1280($sp)
lw	$22,1276($sp)
lw	$21,1272($sp)
lw	$20,1268($sp)
lw	$19,1264($sp)
lw	$18,1260($sp)
lw	$17,1256($sp)
lw	$16,1252($sp)
j	$31
addiu	$sp,$sp,1288

.set	macro
.set	reorder
.end	ff_avg_qpel16_mc11_old_c
.size	ff_avg_qpel16_mc11_old_c, .-ff_avg_qpel16_mc11_old_c
.align	2
.globl	ff_avg_qpel16_mc31_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel16_mc31_old_c
.type	ff_avg_qpel16_mc31_old_c, @function
ff_avg_qpel16_mc31_old_c:
.frame	$sp,1288,$31		# vars= 1192, regs= 9/0, args= 48, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1288
sw	$16,1252($sp)
addiu	$16,$sp,56
sw	$23,1280($sp)
move	$23,$4
move	$4,$16
sw	$31,1284($sp)
sw	$22,1276($sp)
move	$22,$6
sw	$21,1272($sp)
li	$21,24			# 0x18
sw	$20,1268($sp)
addiu	$20,$sp,57
sw	$19,1264($sp)
addiu	$19,$sp,736
sw	$18,1260($sp)
addiu	$18,$sp,992
sw	$17,1256($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
addiu	$17,$sp,464

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16

li	$6,16			# 0x10
li	$7,24			# 0x18
li	$16,16			# 0x10
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$20

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$19
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$17

move	$4,$23
move	$5,$20
sw	$19,16($sp)
move	$6,$17
sw	$22,20($sp)
move	$7,$18
sw	$21,24($sp)
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

addiu	$2,$sp,744
addiu	$4,$23,8
sw	$22,20($sp)
addiu	$5,$sp,65
sw	$21,24($sp)
addiu	$6,$sp,472
sw	$2,16($sp)
addiu	$7,$sp,1000
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

lw	$31,1284($sp)
lw	$23,1280($sp)
lw	$22,1276($sp)
lw	$21,1272($sp)
lw	$20,1268($sp)
lw	$19,1264($sp)
lw	$18,1260($sp)
lw	$17,1256($sp)
lw	$16,1252($sp)
j	$31
addiu	$sp,$sp,1288

.set	macro
.set	reorder
.end	ff_avg_qpel16_mc31_old_c
.size	ff_avg_qpel16_mc31_old_c, .-ff_avg_qpel16_mc31_old_c
.align	2
.globl	ff_avg_qpel16_mc13_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel16_mc13_old_c
.type	ff_avg_qpel16_mc13_old_c, @function
ff_avg_qpel16_mc13_old_c:
.frame	$sp,1280,$31		# vars= 1192, regs= 8/0, args= 48, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1280
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$22,1272($sp)
move	$22,$4
move	$4,$16
sw	$31,1276($sp)
sw	$21,1268($sp)
move	$21,$6
sw	$20,1264($sp)
li	$20,24			# 0x18
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$18,1256($sp)
addiu	$18,$sp,736
sw	$17,1252($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
addiu	$17,$sp,992

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$16

li	$16,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19

addiu	$5,$sp,80
addiu	$6,$sp,480
sw	$18,16($sp)
move	$4,$22
sw	$21,20($sp)
move	$7,$17
sw	$20,24($sp)
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

addiu	$2,$sp,744
addiu	$4,$22,8
sw	$21,20($sp)
addiu	$5,$sp,88
sw	$20,24($sp)
addiu	$6,$sp,488
sw	$2,16($sp)
addiu	$7,$sp,1000
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

lw	$31,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
j	$31
addiu	$sp,$sp,1280

.set	macro
.set	reorder
.end	ff_avg_qpel16_mc13_old_c
.size	ff_avg_qpel16_mc13_old_c, .-ff_avg_qpel16_mc13_old_c
.align	2
.globl	ff_avg_qpel16_mc33_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel16_mc33_old_c
.type	ff_avg_qpel16_mc33_old_c, @function
ff_avg_qpel16_mc33_old_c:
.frame	$sp,1280,$31		# vars= 1192, regs= 8/0, args= 48, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1280
sw	$16,1248($sp)
addiu	$16,$sp,56
sw	$22,1272($sp)
move	$22,$4
move	$4,$16
sw	$31,1276($sp)
sw	$21,1268($sp)
move	$21,$6
sw	$20,1264($sp)
li	$20,24			# 0x18
sw	$19,1260($sp)
addiu	$19,$sp,464
sw	$18,1256($sp)
addiu	$18,$sp,736
sw	$17,1252($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
addiu	$17,$sp,992

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16

addiu	$5,$sp,57
li	$6,16			# 0x10
li	$7,24			# 0x18
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$4,$17

li	$16,16			# 0x10
li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$18
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19

addiu	$5,$sp,81
addiu	$6,$sp,480
sw	$18,16($sp)
move	$4,$22
sw	$21,20($sp)
move	$7,$17
sw	$20,24($sp)
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

addiu	$2,$sp,744
addiu	$4,$22,8
sw	$21,20($sp)
addiu	$5,$sp,89
sw	$20,24($sp)
addiu	$6,$sp,488
sw	$2,16($sp)
addiu	$7,$sp,1000
sw	$16,28($sp)
sw	$16,32($sp)
sw	$16,36($sp)
.option	pic0
jal	avg_pixels8_l4
.option	pic2
sw	$16,40($sp)

lw	$31,1276($sp)
lw	$22,1272($sp)
lw	$21,1268($sp)
lw	$20,1264($sp)
lw	$19,1260($sp)
lw	$18,1256($sp)
lw	$17,1252($sp)
lw	$16,1248($sp)
j	$31
addiu	$sp,$sp,1280

.set	macro
.set	reorder
.end	ff_avg_qpel16_mc33_old_c
.size	ff_avg_qpel16_mc33_old_c, .-ff_avg_qpel16_mc33_old_c
.align	2
.globl	ff_avg_qpel16_mc12_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel16_mc12_old_c
.type	ff_avg_qpel16_mc12_old_c, @function
ff_avg_qpel16_mc12_old_c:
.frame	$sp,1264,$31		# vars= 1192, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1264
sw	$17,1240($sp)
addiu	$17,$sp,40
sw	$21,1256($sp)
move	$21,$4
move	$4,$17
sw	$31,1260($sp)
sw	$20,1252($sp)
move	$20,$6
sw	$19,1248($sp)
addiu	$19,$sp,448
sw	$18,1244($sp)
addiu	$18,$sp,976
sw	$16,1236($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
li	$16,16			# 0x10

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$17

li	$6,16			# 0x10
li	$7,24			# 0x18
move	$4,$18
move	$5,$17
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
addiu	$17,$sp,720

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19

move	$4,$21
move	$5,$18
sw	$16,16($sp)
move	$6,$17
sw	$16,20($sp)
move	$7,$20
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$21,8
addiu	$5,$sp,984
sw	$16,16($sp)
addiu	$6,$sp,728
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$20

lw	$31,1260($sp)
lw	$21,1256($sp)
lw	$20,1252($sp)
lw	$19,1248($sp)
lw	$18,1244($sp)
lw	$17,1240($sp)
lw	$16,1236($sp)
j	$31
addiu	$sp,$sp,1264

.set	macro
.set	reorder
.end	ff_avg_qpel16_mc12_old_c
.size	ff_avg_qpel16_mc12_old_c, .-ff_avg_qpel16_mc12_old_c
.align	2
.globl	ff_avg_qpel16_mc32_old_c
.set	nomips16
.set	nomicromips
.ent	ff_avg_qpel16_mc32_old_c
.type	ff_avg_qpel16_mc32_old_c, @function
ff_avg_qpel16_mc32_old_c:
.frame	$sp,1264,$31		# vars= 1192, regs= 7/0, args= 32, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-1264
sw	$16,1236($sp)
addiu	$16,$sp,40
sw	$21,1256($sp)
move	$21,$4
move	$4,$16
sw	$31,1260($sp)
sw	$20,1252($sp)
move	$20,$6
sw	$19,1248($sp)
addiu	$19,$sp,448
sw	$18,1244($sp)
addiu	$18,$sp,976
sw	$17,1240($sp)
.option	pic0
jal	copy_block17.constprop.40
.option	pic2
addiu	$17,$sp,720

li	$2,17			# 0x11
li	$6,16			# 0x10
li	$7,24			# 0x18
sw	$2,16($sp)
move	$4,$19
.option	pic0
jal	put_mpeg4_qpel16_h_lowpass
.option	pic2
move	$5,$16

addiu	$5,$sp,41
li	$6,16			# 0x10
li	$7,24			# 0x18
li	$16,16			# 0x10
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$4,$18

li	$6,16			# 0x10
li	$7,16			# 0x10
move	$4,$17
.option	pic0
jal	put_mpeg4_qpel16_v_lowpass
.option	pic2
move	$5,$19

move	$4,$21
move	$5,$18
sw	$16,16($sp)
move	$6,$17
sw	$16,20($sp)
move	$7,$20
.option	pic0
jal	avg_pixels8_l2
.option	pic2
sw	$16,24($sp)

addiu	$4,$21,8
addiu	$5,$sp,984
sw	$16,16($sp)
addiu	$6,$sp,728
sw	$16,20($sp)
sw	$16,24($sp)
.option	pic0
jal	avg_pixels8_l2
.option	pic2
move	$7,$20

lw	$31,1260($sp)
lw	$21,1256($sp)
lw	$20,1252($sp)
lw	$19,1248($sp)
lw	$18,1244($sp)
lw	$17,1240($sp)
lw	$16,1236($sp)
j	$31
addiu	$sp,$sp,1264

.set	macro
.set	reorder
.end	ff_avg_qpel16_mc32_old_c
.size	ff_avg_qpel16_mc32_old_c, .-ff_avg_qpel16_mc32_old_c
.align	2
.globl	ff_block_permute
.set	nomips16
.set	nomicromips
.ent	ff_block_permute
.type	ff_block_permute, @function
ff_block_permute:
.frame	$sp,136,$31		# vars= 128, regs= 0/0, args= 0, gp= 8
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
blez	$7,$L2553
move	$3,$6

addiu	$sp,$sp,-136
addiu	$7,$7,1
addiu	$9,$sp,8
addu	$7,$6,$7
$L2545:
lbu	$2,0($3)
addiu	$3,$3,1
sll	$2,$2,1
addu	$8,$4,$2
addu	$2,$9,$2
lhu	$10,0($8)
sh	$0,0($8)
bne	$3,$7,$L2545
sh	$10,0($2)

$L2548:
lbu	$2,0($6)
addiu	$6,$6,1
addu	$3,$5,$2
sll	$2,$2,1
lbu	$3,0($3)
addu	$2,$9,$2
sll	$3,$3,1
lhu	$8,0($2)
addu	$2,$4,$3
bne	$7,$6,$L2548
sh	$8,0($2)

addiu	$sp,$sp,136
$L2553:
j	$31
nop

.set	macro
.set	reorder
.end	ff_block_permute
.size	ff_block_permute, .-ff_block_permute
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"internal error in cmp function selection\012\000"
.text
.align	2
.globl	ff_set_cmp
.set	nomips16
.set	nomicromips
.ent	ff_set_cmp
.type	ff_set_cmp, @function
ff_set_cmp:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-56
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$18,32($sp)
move	$18,$6
sw	$17,28($sp)
li	$6,24			# 0x18
sw	$22,48($sp)
move	$17,$5
sw	$21,44($sp)
move	$5,$0
sw	$20,40($sp)
move	$20,$4
sw	$19,36($sp)
move	$4,$17
sw	$16,24($sp)
andi	$18,$18,0xff
.cprestore	16
sll	$19,$18,2
sw	$31,52($sp)
addiu	$16,$20,60
lw	$25,%call16(memset)($28)
addiu	$20,$20,84
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sltu	$18,$18,15

lui	$2,%hi($L2557)
lui	$6,%hi($LC0)
lw	$28,16($sp)
addiu	$2,$2,%lo($L2557)
addiu	$22,$6,%lo($LC0)
addu	$19,$2,$19
lui	$2,%hi(zero_cmp)
addiu	$21,$2,%lo(zero_cmp)
$L2573:
beq	$18,$0,$L2555
lw	$25,%call16(av_log)($28)

lw	$2,0($19)
j	$2
nop

.rdata
.align	2
.align	2
$L2557:
.word	$L2556
.word	$L2558
.word	$L2559
.word	$L2560
.word	$L2561
.word	$L2562
.word	$L2563
.word	$L2564
.word	$L2565
.word	$L2566
.word	$L2567
.word	$L2568
.word	$L2569
.word	$L2570
.word	$L2571
.text
$L2570:
lw	$2,288($16)
sw	$2,0($17)
$L2572:
addiu	$16,$16,4
bne	$16,$20,$L2573
addiu	$17,$17,4

lw	$31,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L2569:
lw	$2,264($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2568:
lw	$2,240($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2567:
lw	$2,216($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2566:
lw	$2,192($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2565:
lw	$2,168($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2564:
.option	pic0
j	$L2572
.option	pic2
sw	$21,0($17)

$L2563:
lw	$2,144($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2562:
lw	$2,120($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2561:
lw	$2,96($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2560:
lw	$2,72($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2559:
lw	$2,48($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2558:
lw	$2,24($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2556:
lw	$2,0($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2571:
lw	$2,312($16)
.option	pic0
j	$L2572
.option	pic2
sw	$2,0($17)

$L2555:
move	$4,$0
li	$5,16			# 0x10
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$6,$22

.option	pic0
j	$L2572
.option	pic2
lw	$28,16($sp)

.set	macro
.set	reorder
.end	ff_set_cmp
.size	ff_set_cmp, .-ff_set_cmp
.section	.text.unlikely,"ax",@progbits
.align	2
.globl	dsputil_static_init
.set	nomips16
.set	nomicromips
.ent	dsputil_static_init
.type	dsputil_static_init, @function
dsputil_static_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(ff_cropTbl)
move	$3,$0
addiu	$2,$2,%lo(ff_cropTbl)
li	$5,256			# 0x100
addu	$4,$2,$3
$L2586:
sb	$3,1024($4)
addiu	$3,$3,1
bne	$3,$5,$L2586
addu	$4,$2,$3

move	$3,$0
li	$5,-1			# 0xffffffffffffffff
li	$4,1024			# 0x400
$L2578:
addiu	$3,$3,1
sb	$0,0($2)
sb	$5,1280($2)
bne	$3,$4,$L2578
addiu	$2,$2,1

lui	$5,%hi(ff_squareTbl)
li	$2,-256			# 0xffffffffffffff00
addiu	$5,$5,%lo(ff_squareTbl)
li	$6,256			# 0x100
$L2579:
mul	$4,$2,$2
sll	$3,$2,2
addiu	$2,$2,1
addu	$3,$5,$3
bne	$2,$6,$L2579
sw	$4,1024($3)

lui	$28,%hi(__gnu_local_gp)
lui	$4,%hi(ff_zigzag_direct)
addiu	$28,$28,%lo(__gnu_local_gp)
li	$3,1			# 0x1
addiu	$4,$4,%lo(ff_zigzag_direct)
lw	$6,%got(inv_zigzag_direct16)($28)
li	$5,65			# 0x41
addu	$2,$4,$3
$L2587:
lbu	$2,-1($2)
sll	$2,$2,1
addu	$2,$6,$2
sh	$3,0($2)
addiu	$3,$3,1
bne	$3,$5,$L2587
addu	$2,$4,$3

j	$31
nop

.set	macro
.set	reorder
.end	dsputil_static_init
.size	dsputil_static_init, .-dsputil_static_init
.text
.align	2
.globl	ff_check_alignment
.set	nomips16
.set	nomicromips
.ent	ff_check_alignment
.type	ff_check_alignment, @function
ff_check_alignment:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
j	$31
move	$2,$0

.set	macro
.set	reorder
.end	ff_check_alignment
.size	ff_check_alignment, .-ff_check_alignment
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"Internal error, IDCT permutation not set\012\000"
.section	.text.unlikely
.align	2
.globl	dsputil_init
.set	nomips16
.set	nomicromips
.ent	dsputil_init
.type	dsputil_init, @function
dsputil_init:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$2,340($5)
addiu	$sp,$sp,-56
addiu	$28,$28,%lo(__gnu_local_gp)
li	$3,1			# 0x1
sw	$19,36($sp)
sw	$16,24($sp)
move	$19,$5
sw	$31,52($sp)
move	$16,$4
sw	$22,48($sp)
sw	$21,44($sp)
sw	$20,40($sp)
sw	$18,32($sp)
sw	$17,28($sp)
bne	$2,$3,$L2590
.cprestore	16

lw	$2,%got(fdct_ifast)($28)
sw	$2,2684($4)
lw	$2,%got(fdct_ifast248)($28)
.option	pic0
j	$L2591
.option	pic2
sw	$2,2688($4)

$L2590:
li	$3,6			# 0x6
bne	$2,$3,$L2592
lw	$2,%got(ff_jpeg_fdct_islow)($28)

lw	$2,%got(ff_faandct)($28)
sw	$2,2684($4)
lw	$2,%got(ff_faandct248)($28)
.option	pic0
j	$L2591
.option	pic2
sw	$2,2688($4)

$L2592:
sw	$2,2684($4)
lw	$2,%got(ff_fdct248_islow)($28)
sw	$2,2688($4)
$L2591:
lw	$2,656($19)
li	$3,1			# 0x1
bne	$2,$3,$L2593
li	$4,2			# 0x2

lw	$2,364($19)
sltu	$2,$2,2
bne	$2,$0,$L2617
lw	$3,%got(ff_h264_lowres_idct_add_c)($28)

.option	pic0
j	$L2594
.option	pic2
lw	$2,%got(ff_h264_lowres_idct_put_c)($28)

$L2617:
lui	$3,%hi(ff_jref_idct4_add)
lui	$2,%hi(ff_jref_idct4_put)
addiu	$3,$3,%lo(ff_jref_idct4_add)
addiu	$2,$2,%lo(ff_jref_idct4_put)
$L2594:
sw	$2,2696($16)
li	$2,1			# 0x1
sw	$3,2700($16)
sw	$2,2768($16)
lw	$2,%got(j_rev_dct4)($28)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2692($16)

$L2593:
bne	$2,$4,$L2596
nop

lw	$2,%got(j_rev_dct2)($28)
sw	$3,2768($16)
sw	$2,2692($16)
lui	$2,%hi(ff_jref_idct2_put)
addiu	$2,$2,%lo(ff_jref_idct2_put)
sw	$2,2696($16)
lui	$2,%hi(ff_jref_idct2_add)
addiu	$2,$2,%lo(ff_jref_idct2_add)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2700($16)

$L2596:
li	$5,3			# 0x3
bne	$2,$5,$L2597
lw	$2,%got(j_rev_dct1)($28)

sw	$3,2768($16)
sw	$2,2692($16)
lui	$2,%hi(ff_jref_idct1_put)
addiu	$2,$2,%lo(ff_jref_idct1_put)
sw	$2,2696($16)
lui	$2,%hi(ff_jref_idct1_add)
addiu	$2,$2,%lo(ff_jref_idct1_add)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2700($16)

$L2597:
lw	$2,364($19)
bne	$2,$3,$L2598
nop

lw	$2,%got(j_rev_dct)($28)
sw	$4,2768($16)
sw	$2,2692($16)
lui	$2,%hi(ff_jref_idct_put)
addiu	$2,$2,%lo(ff_jref_idct_put)
sw	$2,2696($16)
lui	$2,%hi(ff_jref_idct_add)
addiu	$2,$2,%lo(ff_jref_idct_add)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2700($16)

$L2598:
li	$4,12			# 0xc
bne	$2,$4,$L2599
nop

lw	$2,%got(ff_vp3_idct_put_c)($28)
sw	$3,2768($16)
sw	$2,2696($16)
lw	$2,%got(ff_vp3_idct_add_c)($28)
sw	$2,2700($16)
lw	$2,%got(ff_vp3_idct_c)($28)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2692($16)

$L2599:
li	$4,19			# 0x13
bne	$2,$4,$L2600
nop

lui	$2,%hi(ff_wmv2_idct_put_c)
sw	$3,2768($16)
addiu	$2,$2,%lo(ff_wmv2_idct_put_c)
sw	$2,2696($16)
lui	$2,%hi(ff_wmv2_idct_add_c)
addiu	$2,$2,%lo(ff_wmv2_idct_add_c)
sw	$2,2700($16)
lui	$2,%hi(ff_wmv2_idct_c)
addiu	$2,$2,%lo(ff_wmv2_idct_c)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2692($16)

$L2600:
li	$4,20			# 0x14
bne	$2,$4,$L2601
nop

lw	$2,%got(ff_faanidct_put)($28)
sw	$3,2768($16)
sw	$2,2696($16)
lw	$2,%got(ff_faanidct_add)($28)
sw	$2,2700($16)
lw	$2,%got(ff_faanidct)($28)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2692($16)

$L2601:
li	$4,21			# 0x15
bne	$2,$4,$L2602
nop

lw	$2,%got(ff_ea_idct_put_c)($28)
sw	$3,2768($16)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2696($16)

$L2602:
li	$4,24			# 0x18
bne	$2,$4,$L2603
sw	$3,2768($16)

lw	$2,%got(ff_bink_idct_c)($28)
sw	$2,2692($16)
lw	$2,%got(ff_bink_idct_add_c)($28)
sw	$2,2700($16)
lw	$2,%got(ff_bink_idct_put_c)($28)
.option	pic0
j	$L2595
.option	pic2
sw	$2,2696($16)

$L2603:
lw	$2,%got(ff_simple_idct_put)($28)
sw	$2,2696($16)
lw	$2,%got(ff_simple_idct_add)($28)
sw	$2,2700($16)
lw	$2,%got(ff_simple_idct)($28)
sw	$2,2692($16)
$L2595:
lui	$4,%hi(put_pixels16_c)
lw	$25,%call16(ff_mlp_init)($28)
lui	$22,%hi(pix_abs16_c)
addiu	$4,$4,%lo(put_pixels16_c)
lui	$21,%hi(pix_abs8_c)
lui	$3,%hi(put_pixels8_c)
sw	$4,544($16)
addiu	$22,$22,%lo(pix_abs16_c)
sw	$4,672($16)
lui	$4,%hi(get_pixels_c)
addiu	$3,$3,%lo(put_pixels8_c)
addiu	$4,$4,%lo(get_pixels_c)
sw	$22,2512($16)
addiu	$21,$21,%lo(pix_abs8_c)
lui	$2,%hi(ff_put_pixels16x16_c)
sw	$4,0($16)
lui	$4,%hi(diff_pixels_c)
sw	$21,2528($16)
addiu	$2,$2,%lo(ff_put_pixels16x16_c)
addiu	$4,$4,%lo(diff_pixels_c)
lui	$6,%hi(ff_avg_pixels16x16_c)
lui	$20,%hi(ff_put_pixels8x8_c)
sw	$4,4($16)
lui	$4,%hi(put_pixels_clamped_c)
addiu	$6,$6,%lo(ff_avg_pixels16x16_c)
addiu	$4,$4,%lo(put_pixels_clamped_c)
addiu	$20,$20,%lo(ff_put_pixels8x8_c)
move	$5,$19
sw	$4,8($16)
lui	$4,%hi(put_signed_pixels_clamped_c)
addiu	$17,$16,2000
addiu	$4,$4,%lo(put_signed_pixels_clamped_c)
addiu	$18,$16,2256
sw	$4,12($16)
lui	$4,%hi(put_pixels_nonclamped_c)
addiu	$4,$4,%lo(put_pixels_nonclamped_c)
sw	$4,16($16)
lui	$4,%hi(add_pixels_clamped_c)
addiu	$4,$4,%lo(add_pixels_clamped_c)
sw	$4,20($16)
lui	$4,%hi(add_pixels8_c)
addiu	$4,$4,%lo(add_pixels8_c)
sw	$4,24($16)
lui	$4,%hi(add_pixels4_c)
addiu	$4,$4,%lo(add_pixels4_c)
sw	$4,28($16)
lui	$4,%hi(sum_abs_dctelem_c)
addiu	$4,$4,%lo(sum_abs_dctelem_c)
sw	$4,32($16)
lui	$4,%hi(gmc1_c)
addiu	$4,$4,%lo(gmc1_c)
sw	$4,36($16)
lui	$4,%hi(ff_gmc_c)
addiu	$4,$4,%lo(ff_gmc_c)
sw	$4,40($16)
lui	$4,%hi(clear_block_c)
addiu	$4,$4,%lo(clear_block_c)
sw	$4,44($16)
lui	$4,%hi(clear_blocks_c)
addiu	$4,$4,%lo(clear_blocks_c)
sw	$4,48($16)
lui	$4,%hi(pix_sum_c)
addiu	$4,$4,%lo(pix_sum_c)
sw	$4,52($16)
lui	$4,%hi(pix_norm1_c)
addiu	$4,$4,%lo(pix_norm1_c)
sw	$4,56($16)
lui	$4,%hi(fill_block16_c)
addiu	$4,$4,%lo(fill_block16_c)
sw	$4,4108($16)
lui	$4,%hi(fill_block8_c)
addiu	$4,$4,%lo(fill_block8_c)
sw	$4,4112($16)
lui	$4,%hi(scale_block_c)
addiu	$4,$4,%lo(scale_block_c)
sw	$4,4116($16)
lui	$4,%hi(pix_abs16_x2_c)
addiu	$4,$4,%lo(pix_abs16_x2_c)
sw	$4,2516($16)
lui	$4,%hi(pix_abs16_y2_c)
addiu	$4,$4,%lo(pix_abs16_y2_c)
sw	$4,2520($16)
lui	$4,%hi(pix_abs16_xy2_c)
addiu	$4,$4,%lo(pix_abs16_xy2_c)
sw	$4,2524($16)
lui	$4,%hi(pix_abs8_x2_c)
addiu	$4,$4,%lo(pix_abs8_x2_c)
sw	$4,2532($16)
lui	$4,%hi(pix_abs8_y2_c)
addiu	$4,$4,%lo(pix_abs8_y2_c)
sw	$4,2536($16)
lui	$4,%hi(pix_abs8_xy2_c)
addiu	$4,$4,%lo(pix_abs8_xy2_c)
sw	$4,2540($16)
lui	$4,%hi(put_pixels16_x2_c)
addiu	$4,$4,%lo(put_pixels16_x2_c)
sw	$4,548($16)
lui	$4,%hi(put_pixels16_y2_c)
addiu	$4,$4,%lo(put_pixels16_y2_c)
sw	$4,552($16)
lui	$4,%hi(put_pixels16_xy2_c)
addiu	$4,$4,%lo(put_pixels16_xy2_c)
sw	$4,556($16)
lui	$4,%hi(put_no_rnd_pixels16_x2_c)
addiu	$4,$4,%lo(put_no_rnd_pixels16_x2_c)
sw	$4,676($16)
lui	$4,%hi(put_no_rnd_pixels16_y2_c)
addiu	$4,$4,%lo(put_no_rnd_pixels16_y2_c)
sw	$4,680($16)
lui	$4,%hi(put_no_rnd_pixels16_xy2_c)
addiu	$4,$4,%lo(put_no_rnd_pixels16_xy2_c)
sw	$4,684($16)
lui	$4,%hi(put_pixels8_x2_c)
sw	$3,560($16)
sw	$3,688($16)
lui	$3,%hi(put_pixels8_y2_c)
addiu	$4,$4,%lo(put_pixels8_x2_c)
addiu	$3,$3,%lo(put_pixels8_y2_c)
sw	$4,564($16)
lui	$4,%hi(avg_no_rnd_pixels16_c)
sw	$3,568($16)
lui	$3,%hi(put_no_rnd_pixels8_x2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels16_c)
addiu	$3,$3,%lo(put_no_rnd_pixels8_x2_c)
sw	$4,736($16)
lui	$4,%hi(avg_no_rnd_pixels16_x2_c)
sw	$3,692($16)
lui	$3,%hi(put_no_rnd_pixels8_y2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels16_x2_c)
addiu	$3,$3,%lo(put_no_rnd_pixels8_y2_c)
sw	$4,740($16)
lui	$4,%hi(avg_no_rnd_pixels16_y2_c)
sw	$3,696($16)
lui	$3,%hi(put_pixels8_xy2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels16_y2_c)
addiu	$3,$3,%lo(put_pixels8_xy2_c)
sw	$4,744($16)
lui	$4,%hi(avg_no_rnd_pixels16_xy2_c)
sw	$3,572($16)
lui	$3,%hi(put_no_rnd_pixels8_xy2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels16_xy2_c)
addiu	$3,$3,%lo(put_no_rnd_pixels8_xy2_c)
sw	$4,748($16)
lui	$4,%hi(avg_pixels8_c)
sw	$3,700($16)
lui	$3,%hi(put_pixels4_c)
addiu	$4,$4,%lo(avg_pixels8_c)
addiu	$3,$3,%lo(put_pixels4_c)
sw	$4,624($16)
lui	$4,%hi(avg_pixels8_x2_c)
sw	$3,576($16)
lui	$3,%hi(put_pixels4_x2_c)
addiu	$4,$4,%lo(avg_pixels8_x2_c)
addiu	$3,$3,%lo(put_pixels4_x2_c)
sw	$4,628($16)
lui	$4,%hi(avg_pixels8_y2_c)
sw	$3,580($16)
lui	$3,%hi(put_pixels4_y2_c)
addiu	$4,$4,%lo(avg_pixels8_y2_c)
addiu	$3,$3,%lo(put_pixels4_y2_c)
sw	$4,632($16)
lui	$4,%hi(avg_pixels8_xy2_c)
sw	$3,584($16)
lui	$3,%hi(put_pixels4_xy2_c)
addiu	$4,$4,%lo(avg_pixels8_xy2_c)
addiu	$3,$3,%lo(put_pixels4_xy2_c)
sw	$4,636($16)
lui	$4,%hi(avg_no_rnd_pixels8_c)
sw	$3,588($16)
lui	$3,%hi(put_pixels2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels8_c)
addiu	$3,$3,%lo(put_pixels2_c)
sw	$4,752($16)
lui	$4,%hi(avg_no_rnd_pixels8_x2_c)
sw	$3,592($16)
lui	$3,%hi(put_pixels2_x2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels8_x2_c)
addiu	$3,$3,%lo(put_pixels2_x2_c)
sw	$4,756($16)
lui	$4,%hi(avg_no_rnd_pixels8_y2_c)
sw	$3,596($16)
lui	$3,%hi(put_pixels2_y2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels8_y2_c)
addiu	$3,$3,%lo(put_pixels2_y2_c)
sw	$4,760($16)
lui	$4,%hi(avg_no_rnd_pixels8_xy2_c)
sw	$3,600($16)
lui	$3,%hi(put_pixels2_xy2_c)
addiu	$4,$4,%lo(avg_no_rnd_pixels8_xy2_c)
addiu	$3,$3,%lo(put_pixels2_xy2_c)
sw	$4,764($16)
lui	$4,%hi(avg_pixels4_c)
sw	$3,604($16)
lui	$3,%hi(avg_pixels16_c)
addiu	$4,$4,%lo(avg_pixels4_c)
addiu	$3,$3,%lo(avg_pixels16_c)
sw	$4,640($16)
lui	$4,%hi(avg_pixels4_x2_c)
sw	$3,608($16)
lui	$3,%hi(avg_pixels16_x2_c)
addiu	$4,$4,%lo(avg_pixels4_x2_c)
addiu	$3,$3,%lo(avg_pixels16_x2_c)
sw	$3,612($16)
lui	$3,%hi(avg_pixels16_y2_c)
addiu	$3,$3,%lo(avg_pixels16_y2_c)
sw	$3,616($16)
lui	$3,%hi(avg_pixels16_xy2_c)
addiu	$3,$3,%lo(avg_pixels16_xy2_c)
sw	$3,620($16)
lui	$3,%hi(ff_avg_pixels8x8_c)
sw	$4,644($16)
lui	$4,%hi(avg_pixels4_y2_c)
sw	$2,896($16)
addiu	$3,$3,%lo(ff_avg_pixels8x8_c)
addiu	$4,$4,%lo(avg_pixels4_y2_c)
sw	$4,648($16)
lui	$4,%hi(avg_pixels4_xy2_c)
addiu	$4,$4,%lo(avg_pixels4_xy2_c)
sw	$4,652($16)
lui	$4,%hi(avg_pixels2_c)
addiu	$4,$4,%lo(avg_pixels2_c)
sw	$4,656($16)
lui	$4,%hi(avg_pixels2_y2_c)
addiu	$4,$4,%lo(avg_pixels2_y2_c)
sw	$4,664($16)
lui	$4,%hi(avg_pixels2_xy2_c)
addiu	$4,$4,%lo(avg_pixels2_xy2_c)
sw	$4,668($16)
lui	$4,%hi(avg_pixels2_x2_c)
addiu	$4,$4,%lo(avg_pixels2_x2_c)
sw	$4,660($16)
lui	$4,%hi(put_no_rnd_pixels16_l2_c)
addiu	$4,$4,%lo(put_no_rnd_pixels16_l2_c)
sw	$4,800($16)
lui	$4,%hi(put_no_rnd_pixels8_l2_c)
addiu	$4,$4,%lo(put_no_rnd_pixels8_l2_c)
sw	$4,804($16)
lui	$4,%hi(put_tpel_pixels_mc00_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc00_c)
sw	$4,808($16)
lui	$4,%hi(put_tpel_pixels_mc10_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc10_c)
sw	$4,812($16)
lui	$4,%hi(put_tpel_pixels_mc20_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc20_c)
sw	$4,816($16)
lui	$4,%hi(put_tpel_pixels_mc01_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc01_c)
sw	$4,824($16)
lui	$4,%hi(put_tpel_pixels_mc11_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc11_c)
sw	$4,828($16)
lui	$4,%hi(put_tpel_pixels_mc21_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc21_c)
sw	$4,832($16)
lui	$4,%hi(put_tpel_pixels_mc02_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc02_c)
sw	$4,840($16)
lui	$4,%hi(put_tpel_pixels_mc12_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc12_c)
sw	$4,844($16)
lui	$4,%hi(put_tpel_pixels_mc22_c)
addiu	$4,$4,%lo(put_tpel_pixels_mc22_c)
sw	$4,848($16)
lui	$4,%hi(avg_tpel_pixels_mc00_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc00_c)
sw	$4,852($16)
lui	$4,%hi(avg_tpel_pixels_mc10_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc10_c)
sw	$4,856($16)
lui	$4,%hi(avg_tpel_pixels_mc20_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc20_c)
sw	$4,860($16)
lui	$4,%hi(avg_tpel_pixels_mc01_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc01_c)
sw	$4,868($16)
lui	$4,%hi(avg_tpel_pixels_mc11_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc11_c)
sw	$4,872($16)
lui	$4,%hi(avg_tpel_pixels_mc21_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc21_c)
sw	$4,876($16)
lui	$4,%hi(avg_tpel_pixels_mc02_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc02_c)
sw	$4,884($16)
lui	$4,%hi(avg_tpel_pixels_mc12_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc12_c)
sw	$4,888($16)
lui	$4,%hi(avg_tpel_pixels_mc22_c)
addiu	$4,$4,%lo(avg_tpel_pixels_mc22_c)
sw	$4,892($16)
lui	$4,%hi(put_qpel16_mc10_c)
addiu	$4,$4,%lo(put_qpel16_mc10_c)
sw	$4,900($16)
lui	$4,%hi(put_qpel16_mc20_c)
addiu	$4,$4,%lo(put_qpel16_mc20_c)
sw	$4,904($16)
lui	$4,%hi(put_qpel16_mc30_c)
addiu	$4,$4,%lo(put_qpel16_mc30_c)
sw	$4,908($16)
lui	$4,%hi(put_qpel16_mc01_c)
addiu	$4,$4,%lo(put_qpel16_mc01_c)
sw	$4,912($16)
lui	$4,%hi(put_qpel16_mc11_c)
addiu	$4,$4,%lo(put_qpel16_mc11_c)
sw	$4,916($16)
lui	$4,%hi(put_qpel16_mc21_c)
addiu	$4,$4,%lo(put_qpel16_mc21_c)
sw	$4,920($16)
lui	$4,%hi(put_qpel16_mc31_c)
addiu	$4,$4,%lo(put_qpel16_mc31_c)
sw	$4,924($16)
lui	$4,%hi(put_qpel16_mc02_c)
sw	$6,1024($16)
addiu	$4,$4,%lo(put_qpel16_mc02_c)
sw	$2,1152($16)
sw	$4,928($16)
lui	$4,%hi(put_qpel16_mc12_c)
addiu	$4,$4,%lo(put_qpel16_mc12_c)
sw	$4,932($16)
lui	$4,%hi(put_qpel16_mc22_c)
addiu	$4,$4,%lo(put_qpel16_mc22_c)
sw	$4,936($16)
lui	$4,%hi(put_no_rnd_qpel16_mc20_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc20_c)
sw	$4,1160($16)
lui	$4,%hi(put_no_rnd_qpel16_mc30_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc30_c)
sw	$4,1164($16)
lui	$4,%hi(put_qpel16_mc03_c)
addiu	$4,$4,%lo(put_qpel16_mc03_c)
sw	$4,944($16)
lui	$4,%hi(put_qpel16_mc13_c)
addiu	$4,$4,%lo(put_qpel16_mc13_c)
sw	$4,948($16)
lui	$4,%hi(put_no_rnd_qpel16_mc01_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc01_c)
sw	$4,1168($16)
lui	$4,%hi(put_no_rnd_qpel16_mc11_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc11_c)
sw	$4,1172($16)
lui	$4,%hi(put_no_rnd_qpel16_mc21_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc21_c)
sw	$4,1176($16)
lui	$4,%hi(put_no_rnd_qpel16_mc12_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc12_c)
sw	$4,1188($16)
lui	$4,%hi(put_no_rnd_qpel16_mc22_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc22_c)
sw	$4,1192($16)
lui	$4,%hi(put_no_rnd_qpel16_mc32_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc32_c)
sw	$4,1196($16)
lui	$4,%hi(put_no_rnd_qpel16_mc03_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc03_c)
sw	$4,1200($16)
lui	$4,%hi(put_no_rnd_qpel16_mc13_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc13_c)
sw	$4,1204($16)
lui	$4,%hi(put_no_rnd_qpel16_mc33_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc33_c)
sw	$4,1212($16)
lui	$4,%hi(avg_qpel16_mc10_c)
addiu	$4,$4,%lo(avg_qpel16_mc10_c)
sw	$4,1028($16)
lui	$4,%hi(avg_qpel16_mc20_c)
addiu	$4,$4,%lo(avg_qpel16_mc20_c)
sw	$4,1032($16)
lui	$4,%hi(avg_qpel16_mc30_c)
addiu	$4,$4,%lo(avg_qpel16_mc30_c)
sw	$4,1036($16)
lui	$4,%hi(avg_qpel16_mc01_c)
addiu	$4,$4,%lo(avg_qpel16_mc01_c)
sw	$4,1040($16)
lui	$4,%hi(avg_qpel16_mc11_c)
addiu	$4,$4,%lo(avg_qpel16_mc11_c)
sw	$4,1044($16)
lui	$4,%hi(avg_qpel16_mc21_c)
addiu	$4,$4,%lo(avg_qpel16_mc21_c)
sw	$4,1048($16)
lui	$4,%hi(avg_qpel16_mc31_c)
addiu	$4,$4,%lo(avg_qpel16_mc31_c)
sw	$4,1052($16)
lui	$4,%hi(put_qpel16_mc32_c)
addiu	$4,$4,%lo(put_qpel16_mc32_c)
sw	$4,940($16)
lui	$4,%hi(put_qpel16_mc23_c)
addiu	$4,$4,%lo(put_qpel16_mc23_c)
sw	$4,952($16)
lui	$4,%hi(put_qpel16_mc33_c)
addiu	$4,$4,%lo(put_qpel16_mc33_c)
sw	$4,956($16)
lui	$4,%hi(put_no_rnd_qpel16_mc10_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc10_c)
sw	$4,1156($16)
lui	$4,%hi(put_no_rnd_qpel16_mc31_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc31_c)
sw	$4,1180($16)
lui	$4,%hi(put_no_rnd_qpel16_mc02_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc02_c)
sw	$4,1184($16)
lui	$4,%hi(put_no_rnd_qpel16_mc23_c)
addiu	$4,$4,%lo(put_no_rnd_qpel16_mc23_c)
sw	$4,1208($16)
lui	$4,%hi(avg_qpel16_mc02_c)
addiu	$4,$4,%lo(avg_qpel16_mc02_c)
sw	$4,1056($16)
lui	$4,%hi(avg_qpel16_mc12_c)
addiu	$4,$4,%lo(avg_qpel16_mc12_c)
sw	$4,1060($16)
lui	$4,%hi(avg_qpel16_mc22_c)
sw	$20,960($16)
addiu	$4,$4,%lo(avg_qpel16_mc22_c)
sw	$20,1216($16)
sw	$4,1064($16)
lui	$4,%hi(avg_qpel16_mc32_c)
addiu	$4,$4,%lo(avg_qpel16_mc32_c)
sw	$4,1068($16)
lui	$4,%hi(avg_qpel16_mc03_c)
addiu	$4,$4,%lo(avg_qpel16_mc03_c)
sw	$4,1072($16)
lui	$4,%hi(avg_qpel16_mc13_c)
addiu	$4,$4,%lo(avg_qpel16_mc13_c)
sw	$4,1076($16)
lui	$4,%hi(avg_qpel16_mc23_c)
addiu	$4,$4,%lo(avg_qpel16_mc23_c)
sw	$4,1080($16)
lui	$4,%hi(avg_qpel16_mc33_c)
addiu	$4,$4,%lo(avg_qpel16_mc33_c)
sw	$4,1084($16)
lui	$4,%hi(put_qpel8_mc10_c)
addiu	$4,$4,%lo(put_qpel8_mc10_c)
sw	$4,964($16)
lui	$4,%hi(put_qpel8_mc20_c)
addiu	$4,$4,%lo(put_qpel8_mc20_c)
sw	$4,968($16)
lui	$4,%hi(put_qpel8_mc30_c)
addiu	$4,$4,%lo(put_qpel8_mc30_c)
sw	$4,972($16)
lui	$4,%hi(put_qpel8_mc01_c)
addiu	$4,$4,%lo(put_qpel8_mc01_c)
sw	$4,976($16)
lui	$4,%hi(put_qpel8_mc11_c)
addiu	$4,$4,%lo(put_qpel8_mc11_c)
sw	$4,980($16)
lui	$4,%hi(put_qpel8_mc21_c)
addiu	$4,$4,%lo(put_qpel8_mc21_c)
sw	$4,984($16)
lui	$4,%hi(put_qpel8_mc31_c)
addiu	$4,$4,%lo(put_qpel8_mc31_c)
sw	$4,988($16)
lui	$4,%hi(put_qpel8_mc02_c)
addiu	$4,$4,%lo(put_qpel8_mc02_c)
sw	$4,992($16)
lui	$4,%hi(put_qpel8_mc12_c)
addiu	$4,$4,%lo(put_qpel8_mc12_c)
sw	$4,996($16)
lui	$4,%hi(put_qpel8_mc22_c)
addiu	$4,$4,%lo(put_qpel8_mc22_c)
sw	$4,1000($16)
lui	$4,%hi(put_qpel8_mc32_c)
addiu	$4,$4,%lo(put_qpel8_mc32_c)
sw	$4,1004($16)
lui	$4,%hi(put_qpel8_mc03_c)
addiu	$4,$4,%lo(put_qpel8_mc03_c)
sw	$4,1008($16)
lui	$4,%hi(put_qpel8_mc13_c)
addiu	$4,$4,%lo(put_qpel8_mc13_c)
sw	$4,1012($16)
lui	$4,%hi(put_qpel8_mc23_c)
addiu	$4,$4,%lo(put_qpel8_mc23_c)
sw	$4,1016($16)
lui	$4,%hi(put_qpel8_mc33_c)
addiu	$4,$4,%lo(put_qpel8_mc33_c)
sw	$4,1020($16)
lui	$4,%hi(put_no_rnd_qpel8_mc10_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc10_c)
sw	$4,1220($16)
lui	$4,%hi(put_no_rnd_qpel8_mc20_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc20_c)
sw	$4,1224($16)
lui	$4,%hi(put_no_rnd_qpel8_mc30_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc30_c)
sw	$4,1228($16)
lui	$4,%hi(put_no_rnd_qpel8_mc01_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc01_c)
sw	$4,1232($16)
lui	$4,%hi(put_no_rnd_qpel8_mc11_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc11_c)
sw	$4,1236($16)
lui	$4,%hi(put_no_rnd_qpel8_mc21_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc21_c)
sw	$4,1240($16)
lui	$4,%hi(put_no_rnd_qpel8_mc31_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc31_c)
sw	$4,1244($16)
lui	$4,%hi(put_no_rnd_qpel8_mc02_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc02_c)
sw	$4,1248($16)
lui	$4,%hi(put_no_rnd_qpel8_mc12_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc12_c)
sw	$4,1252($16)
lui	$4,%hi(put_no_rnd_qpel8_mc22_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc22_c)
sw	$4,1256($16)
lui	$4,%hi(put_no_rnd_qpel8_mc32_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc32_c)
sw	$4,1260($16)
lui	$4,%hi(put_no_rnd_qpel8_mc03_c)
sw	$2,1488($16)
lui	$2,%hi(put_h264_qpel16_mc10_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc03_c)
sw	$3,1088($16)
addiu	$2,$2,%lo(put_h264_qpel16_mc10_c)
sw	$4,1264($16)
lui	$4,%hi(put_no_rnd_qpel8_mc13_c)
sw	$2,1492($16)
lui	$2,%hi(put_h264_qpel16_mc20_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc13_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc20_c)
sw	$4,1268($16)
lui	$4,%hi(put_no_rnd_qpel8_mc23_c)
sw	$2,1496($16)
lui	$2,%hi(put_h264_qpel16_mc30_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc23_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc30_c)
sw	$4,1272($16)
lui	$4,%hi(put_no_rnd_qpel8_mc33_c)
sw	$2,1500($16)
lui	$2,%hi(put_h264_qpel16_mc01_c)
addiu	$4,$4,%lo(put_no_rnd_qpel8_mc33_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc01_c)
sw	$4,1276($16)
lui	$4,%hi(avg_qpel8_mc10_c)
sw	$2,1504($16)
lui	$2,%hi(put_h264_qpel16_mc11_c)
addiu	$4,$4,%lo(avg_qpel8_mc10_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc11_c)
sw	$4,1092($16)
lui	$4,%hi(avg_qpel8_mc20_c)
sw	$2,1508($16)
lui	$2,%hi(put_h264_qpel16_mc21_c)
addiu	$4,$4,%lo(avg_qpel8_mc20_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc21_c)
sw	$4,1096($16)
lui	$4,%hi(avg_qpel8_mc30_c)
sw	$2,1512($16)
lui	$2,%hi(put_h264_qpel16_mc31_c)
addiu	$4,$4,%lo(avg_qpel8_mc30_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc31_c)
sw	$4,1100($16)
lui	$4,%hi(avg_qpel8_mc01_c)
sw	$2,1516($16)
lui	$2,%hi(put_h264_qpel16_mc02_c)
addiu	$4,$4,%lo(avg_qpel8_mc01_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc02_c)
sw	$4,1104($16)
lui	$4,%hi(avg_qpel8_mc11_c)
sw	$2,1520($16)
lui	$2,%hi(put_h264_qpel16_mc12_c)
addiu	$4,$4,%lo(avg_qpel8_mc11_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc12_c)
sw	$4,1108($16)
lui	$4,%hi(avg_qpel8_mc21_c)
sw	$2,1524($16)
lui	$2,%hi(put_h264_qpel16_mc22_c)
addiu	$4,$4,%lo(avg_qpel8_mc21_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc22_c)
sw	$4,1112($16)
lui	$4,%hi(avg_qpel8_mc31_c)
sw	$2,1528($16)
lui	$2,%hi(put_h264_qpel16_mc32_c)
addiu	$4,$4,%lo(avg_qpel8_mc31_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc32_c)
sw	$4,1116($16)
lui	$4,%hi(avg_qpel8_mc02_c)
sw	$2,1532($16)
lui	$2,%hi(put_h264_qpel16_mc03_c)
addiu	$4,$4,%lo(avg_qpel8_mc02_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc03_c)
sw	$4,1120($16)
lui	$4,%hi(avg_qpel8_mc12_c)
sw	$2,1536($16)
lui	$2,%hi(put_h264_qpel16_mc13_c)
addiu	$4,$4,%lo(avg_qpel8_mc12_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc13_c)
sw	$4,1124($16)
lui	$4,%hi(avg_qpel8_mc22_c)
addiu	$4,$4,%lo(avg_qpel8_mc22_c)
sw	$4,1128($16)
lui	$4,%hi(avg_qpel8_mc32_c)
addiu	$4,$4,%lo(avg_qpel8_mc32_c)
sw	$4,1132($16)
lui	$4,%hi(avg_qpel8_mc03_c)
addiu	$4,$4,%lo(avg_qpel8_mc03_c)
sw	$4,1136($16)
lui	$4,%hi(avg_qpel8_mc13_c)
addiu	$4,$4,%lo(avg_qpel8_mc13_c)
sw	$4,1140($16)
lui	$4,%hi(avg_qpel8_mc23_c)
addiu	$4,$4,%lo(avg_qpel8_mc23_c)
sw	$4,1144($16)
lui	$4,%hi(avg_qpel8_mc33_c)
addiu	$4,$4,%lo(avg_qpel8_mc33_c)
sw	$4,1148($16)
move	$4,$16
sw	$2,1540($16)
lui	$2,%hi(put_h264_qpel16_mc23_c)
sw	$20,1552($16)
addiu	$2,$2,%lo(put_h264_qpel16_mc23_c)
sw	$2,1544($16)
lui	$2,%hi(put_h264_qpel16_mc33_c)
addiu	$2,$2,%lo(put_h264_qpel16_mc33_c)
sw	$2,1548($16)
lui	$2,%hi(put_h264_qpel8_mc10_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc10_c)
sw	$2,1556($16)
lui	$2,%hi(put_h264_qpel8_mc20_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc20_c)
sw	$2,1560($16)
lui	$2,%hi(put_h264_qpel8_mc30_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc30_c)
sw	$2,1564($16)
lui	$2,%hi(put_h264_qpel8_mc01_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc01_c)
sw	$2,1568($16)
lui	$2,%hi(put_h264_qpel8_mc11_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc11_c)
sw	$2,1572($16)
lui	$2,%hi(put_h264_qpel8_mc21_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc21_c)
sw	$2,1576($16)
lui	$2,%hi(put_h264_qpel8_mc31_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc31_c)
sw	$2,1580($16)
lui	$2,%hi(put_h264_qpel8_mc02_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc02_c)
sw	$2,1584($16)
lui	$2,%hi(put_h264_qpel8_mc12_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc12_c)
sw	$2,1588($16)
lui	$2,%hi(put_h264_qpel8_mc22_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc22_c)
sw	$2,1592($16)
lui	$2,%hi(put_h264_qpel8_mc32_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc32_c)
sw	$2,1596($16)
lui	$2,%hi(put_h264_qpel8_mc03_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc03_c)
sw	$2,1600($16)
lui	$2,%hi(put_h264_qpel8_mc13_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc13_c)
sw	$2,1604($16)
lui	$2,%hi(put_h264_qpel8_mc23_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc23_c)
sw	$2,1608($16)
lui	$2,%hi(put_h264_qpel8_mc33_c)
addiu	$2,$2,%lo(put_h264_qpel8_mc33_c)
sw	$2,1612($16)
lui	$2,%hi(put_h264_qpel4_mc00_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc00_c)
sw	$2,1616($16)
lui	$2,%hi(put_h264_qpel4_mc10_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc10_c)
sw	$2,1620($16)
lui	$2,%hi(put_h264_qpel4_mc20_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc20_c)
sw	$2,1624($16)
lui	$2,%hi(put_h264_qpel4_mc30_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc30_c)
sw	$2,1628($16)
lui	$2,%hi(put_h264_qpel4_mc01_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc01_c)
sw	$2,1632($16)
lui	$2,%hi(put_h264_qpel4_mc11_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc11_c)
sw	$2,1636($16)
lui	$2,%hi(put_h264_qpel4_mc21_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc21_c)
sw	$2,1640($16)
lui	$2,%hi(put_h264_qpel4_mc31_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc31_c)
sw	$2,1644($16)
lui	$2,%hi(put_h264_qpel4_mc02_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc02_c)
sw	$2,1648($16)
lui	$2,%hi(put_h264_qpel4_mc12_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc12_c)
sw	$2,1652($16)
lui	$2,%hi(put_h264_qpel4_mc22_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc22_c)
sw	$2,1656($16)
lui	$2,%hi(put_h264_qpel4_mc32_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc32_c)
sw	$2,1660($16)
lui	$2,%hi(put_h264_qpel4_mc03_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc03_c)
sw	$2,1664($16)
lui	$2,%hi(put_h264_qpel4_mc13_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc13_c)
sw	$2,1668($16)
lui	$2,%hi(put_h264_qpel4_mc23_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc23_c)
sw	$2,1672($16)
lui	$2,%hi(put_h264_qpel4_mc33_c)
addiu	$2,$2,%lo(put_h264_qpel4_mc33_c)
sw	$2,1676($16)
lui	$2,%hi(put_h264_qpel2_mc00_c)
sw	$3,1808($16)
addiu	$2,$2,%lo(put_h264_qpel2_mc00_c)
sw	$6,1744($16)
sw	$2,1680($16)
lui	$2,%hi(put_h264_qpel2_mc10_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc10_c)
sw	$2,1684($16)
lui	$2,%hi(put_h264_qpel2_mc20_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc20_c)
sw	$2,1688($16)
lui	$2,%hi(put_h264_qpel2_mc30_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc30_c)
sw	$2,1692($16)
lui	$2,%hi(put_h264_qpel2_mc01_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc01_c)
sw	$2,1696($16)
lui	$2,%hi(put_h264_qpel2_mc11_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc11_c)
sw	$2,1700($16)
lui	$2,%hi(put_h264_qpel2_mc21_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc21_c)
sw	$2,1704($16)
lui	$2,%hi(put_h264_qpel2_mc31_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc31_c)
sw	$2,1708($16)
lui	$2,%hi(put_h264_qpel2_mc02_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc02_c)
sw	$2,1712($16)
lui	$2,%hi(put_h264_qpel2_mc12_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc12_c)
sw	$2,1716($16)
lui	$2,%hi(put_h264_qpel2_mc22_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc22_c)
sw	$2,1720($16)
lui	$2,%hi(put_h264_qpel2_mc32_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc32_c)
sw	$2,1724($16)
lui	$2,%hi(put_h264_qpel2_mc03_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc03_c)
sw	$2,1728($16)
lui	$2,%hi(put_h264_qpel2_mc13_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc13_c)
sw	$2,1732($16)
lui	$2,%hi(put_h264_qpel2_mc23_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc23_c)
sw	$2,1736($16)
lui	$2,%hi(put_h264_qpel2_mc33_c)
addiu	$2,$2,%lo(put_h264_qpel2_mc33_c)
sw	$2,1740($16)
lui	$2,%hi(avg_h264_qpel16_mc10_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc10_c)
sw	$2,1748($16)
lui	$2,%hi(avg_h264_qpel16_mc20_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc20_c)
sw	$2,1752($16)
lui	$2,%hi(avg_h264_qpel16_mc30_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc30_c)
sw	$2,1756($16)
lui	$2,%hi(avg_h264_qpel16_mc01_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc01_c)
sw	$2,1760($16)
lui	$2,%hi(avg_h264_qpel16_mc11_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc11_c)
sw	$2,1764($16)
lui	$2,%hi(avg_h264_qpel16_mc21_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc21_c)
sw	$2,1768($16)
lui	$2,%hi(avg_h264_qpel16_mc31_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc31_c)
sw	$2,1772($16)
lui	$2,%hi(avg_h264_qpel16_mc02_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc02_c)
sw	$2,1776($16)
lui	$2,%hi(avg_h264_qpel16_mc12_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc12_c)
sw	$2,1780($16)
lui	$2,%hi(avg_h264_qpel16_mc22_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc22_c)
sw	$2,1784($16)
lui	$2,%hi(avg_h264_qpel16_mc32_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc32_c)
sw	$2,1788($16)
lui	$2,%hi(avg_h264_qpel16_mc03_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc03_c)
sw	$2,1792($16)
lui	$2,%hi(avg_h264_qpel16_mc13_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc13_c)
sw	$2,1796($16)
lui	$2,%hi(avg_h264_qpel16_mc23_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc23_c)
sw	$2,1800($16)
lui	$2,%hi(avg_h264_qpel16_mc33_c)
addiu	$2,$2,%lo(avg_h264_qpel16_mc33_c)
sw	$2,1804($16)
lui	$2,%hi(avg_h264_qpel8_mc10_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc10_c)
sw	$2,1812($16)
lui	$2,%hi(avg_h264_qpel8_mc20_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc20_c)
sw	$2,1816($16)
lui	$2,%hi(avg_h264_qpel8_mc30_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc30_c)
sw	$2,1820($16)
lui	$2,%hi(avg_h264_qpel8_mc01_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc01_c)
sw	$2,1824($16)
lui	$2,%hi(avg_h264_qpel8_mc11_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc11_c)
sw	$2,1828($16)
lui	$2,%hi(avg_h264_qpel8_mc21_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc21_c)
sw	$2,1832($16)
lui	$2,%hi(avg_h264_qpel8_mc31_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc31_c)
sw	$2,1836($16)
lui	$2,%hi(avg_h264_qpel8_mc02_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc02_c)
sw	$2,1840($16)
lui	$2,%hi(avg_h264_qpel8_mc12_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc12_c)
sw	$2,1844($16)
lui	$2,%hi(avg_h264_qpel8_mc22_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc22_c)
sw	$2,1848($16)
lui	$2,%hi(avg_h264_qpel8_mc32_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc32_c)
sw	$2,1852($16)
lui	$2,%hi(avg_h264_qpel8_mc03_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc03_c)
sw	$2,1856($16)
lui	$2,%hi(avg_h264_qpel8_mc13_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc13_c)
sw	$2,1860($16)
lui	$2,%hi(avg_h264_qpel8_mc23_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc23_c)
sw	$2,1864($16)
lui	$2,%hi(avg_h264_qpel8_mc33_c)
addiu	$2,$2,%lo(avg_h264_qpel8_mc33_c)
sw	$2,1868($16)
lui	$2,%hi(avg_h264_qpel4_mc00_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc00_c)
sw	$2,1872($16)
lui	$2,%hi(avg_h264_qpel4_mc10_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc10_c)
sw	$2,1876($16)
lui	$2,%hi(avg_h264_qpel4_mc20_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc20_c)
sw	$2,1880($16)
lui	$2,%hi(avg_h264_qpel4_mc30_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc30_c)
sw	$2,1884($16)
lui	$2,%hi(avg_h264_qpel4_mc01_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc01_c)
sw	$2,1888($16)
lui	$2,%hi(avg_h264_qpel4_mc11_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc11_c)
sw	$2,1892($16)
lui	$2,%hi(avg_h264_qpel4_mc21_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc21_c)
sw	$2,1896($16)
lui	$2,%hi(avg_h264_qpel4_mc31_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc31_c)
sw	$2,1900($16)
lui	$2,%hi(avg_h264_qpel4_mc02_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc02_c)
sw	$2,1904($16)
lui	$2,%hi(avg_h264_qpel4_mc12_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc12_c)
sw	$2,1908($16)
lui	$2,%hi(avg_h264_qpel4_mc22_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc22_c)
sw	$2,1912($16)
lui	$2,%hi(avg_h264_qpel4_mc32_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc32_c)
sw	$2,1916($16)
lui	$2,%hi(avg_h264_qpel4_mc03_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc03_c)
sw	$2,1920($16)
lui	$2,%hi(avg_h264_qpel4_mc13_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc13_c)
sw	$2,1924($16)
lui	$2,%hi(avg_h264_qpel4_mc23_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc23_c)
sw	$2,1928($16)
lui	$2,%hi(avg_h264_qpel4_mc33_c)
addiu	$2,$2,%lo(avg_h264_qpel4_mc33_c)
sw	$2,1932($16)
lui	$2,%hi(put_h264_chroma_mc8_c)
addiu	$2,$2,%lo(put_h264_chroma_mc8_c)
sw	$2,1440($16)
lui	$2,%hi(put_h264_chroma_mc4_c)
addiu	$2,$2,%lo(put_h264_chroma_mc4_c)
sw	$2,1444($16)
lui	$2,%hi(put_h264_chroma_mc2_c)
addiu	$2,$2,%lo(put_h264_chroma_mc2_c)
sw	$2,1448($16)
lui	$2,%hi(avg_h264_chroma_mc8_c)
addiu	$2,$2,%lo(avg_h264_chroma_mc8_c)
sw	$2,1452($16)
lui	$2,%hi(avg_h264_chroma_mc4_c)
addiu	$2,$2,%lo(avg_h264_chroma_mc4_c)
sw	$2,1456($16)
lui	$2,%hi(avg_h264_chroma_mc2_c)
addiu	$2,$2,%lo(avg_h264_chroma_mc2_c)
sw	$2,1460($16)
lui	$2,%hi(put_no_rnd_vc1_chroma_mc8_c)
addiu	$2,$2,%lo(put_no_rnd_vc1_chroma_mc8_c)
sw	$2,1464($16)
lui	$2,%hi(avg_no_rnd_vc1_chroma_mc8_c)
addiu	$2,$2,%lo(avg_no_rnd_vc1_chroma_mc8_c)
sw	$2,1476($16)
lui	$2,%hi(draw_edges_c)
addiu	$2,$2,%lo(draw_edges_c)
.reloc	1f,R_MIPS_JALR,ff_mlp_init
1:	jalr	$25
sw	$2,2780($16)

move	$4,$16
lw	$28,16($sp)
lw	$25,%call16(ff_vc1dsp_init)($28)
.reloc	1f,R_MIPS_JALR,ff_vc1dsp_init
1:	jalr	$25
move	$5,$19

move	$4,$16
lw	$28,16($sp)
lw	$25,%call16(ff_intrax8dsp_init)($28)
.reloc	1f,R_MIPS_JALR,ff_intrax8dsp_init
1:	jalr	$25
move	$5,$19

move	$4,$16
lw	$28,16($sp)
lw	$25,%call16(ff_rv30dsp_init)($28)
.reloc	1f,R_MIPS_JALR,ff_rv30dsp_init
1:	jalr	$25
move	$5,$19

move	$5,$19
lw	$28,16($sp)
lw	$25,%call16(ff_rv40dsp_init)($28)
.reloc	1f,R_MIPS_JALR,ff_rv40dsp_init
1:	jalr	$25
move	$4,$16

lui	$2,%hi(put_rv40_qpel16_mc33_c)
lw	$28,16($sp)
move	$4,$16
addiu	$2,$2,%lo(put_rv40_qpel16_mc33_c)
sw	$20,1408($16)
sw	$22,60($16)
sw	$21,64($16)
sw	$2,3632($16)
lui	$2,%hi(avg_rv40_qpel16_mc33_c)
lw	$25,%call16(ff_dsputil_init_dwt)($28)
addiu	$2,$2,%lo(avg_rv40_qpel16_mc33_c)
sw	$2,3888($16)
lui	$2,%hi(put_rv40_qpel8_mc33_c)
addiu	$2,$2,%lo(put_rv40_qpel8_mc33_c)
sw	$2,3696($16)
lui	$2,%hi(avg_rv40_qpel8_mc33_c)
addiu	$2,$2,%lo(avg_rv40_qpel8_mc33_c)
sw	$2,3952($16)
lui	$2,%hi(put_mspel8_mc10_c)
addiu	$2,$2,%lo(put_mspel8_mc10_c)
sw	$2,1412($16)
lui	$2,%hi(put_mspel8_mc20_c)
addiu	$2,$2,%lo(put_mspel8_mc20_c)
sw	$2,1416($16)
lui	$2,%hi(put_mspel8_mc30_c)
addiu	$2,$2,%lo(put_mspel8_mc30_c)
sw	$2,1420($16)
lui	$2,%hi(put_mspel8_mc02_c)
addiu	$2,$2,%lo(put_mspel8_mc02_c)
sw	$2,1424($16)
lui	$2,%hi(put_mspel8_mc12_c)
addiu	$2,$2,%lo(put_mspel8_mc12_c)
sw	$2,1428($16)
lui	$2,%hi(put_mspel8_mc22_c)
addiu	$2,$2,%lo(put_mspel8_mc22_c)
sw	$2,1432($16)
lui	$2,%hi(put_mspel8_mc32_c)
addiu	$2,$2,%lo(put_mspel8_mc32_c)
sw	$2,1436($16)
lui	$2,%hi(hadamard8_diff16_c)
addiu	$2,$2,%lo(hadamard8_diff16_c)
sw	$2,108($16)
lui	$2,%hi(hadamard8_diff8x8_c)
addiu	$2,$2,%lo(hadamard8_diff8x8_c)
sw	$2,112($16)
lui	$2,%hi(hadamard8_intra16_c)
addiu	$2,$2,%lo(hadamard8_intra16_c)
sw	$2,124($16)
lui	$2,%hi(hadamard8_intra8x8_c)
addiu	$2,$2,%lo(hadamard8_intra8x8_c)
sw	$2,128($16)
lui	$2,%hi(dct_sad16_c)
addiu	$2,$2,%lo(dct_sad16_c)
sw	$2,132($16)
lui	$2,%hi(dct_sad8x8_c)
addiu	$2,$2,%lo(dct_sad8x8_c)
sw	$2,136($16)
lui	$2,%hi(dct_max16_c)
addiu	$2,$2,%lo(dct_max16_c)
sw	$2,348($16)
lui	$2,%hi(dct_max8x8_c)
addiu	$2,$2,%lo(dct_max8x8_c)
sw	$2,352($16)
lui	$2,%hi(dct264_sad16_c)
addiu	$2,$2,%lo(dct264_sad16_c)
sw	$2,372($16)
lui	$2,%hi(dct264_sad8x8_c)
addiu	$2,$2,%lo(dct264_sad8x8_c)
sw	$2,376($16)
lui	$2,%hi(sse16_c)
addiu	$2,$2,%lo(sse16_c)
sw	$2,84($16)
lui	$2,%hi(sse8_c)
addiu	$2,$2,%lo(sse8_c)
sw	$2,88($16)
lui	$2,%hi(sse4_c)
addiu	$2,$2,%lo(sse4_c)
sw	$2,92($16)
lui	$2,%hi(quant_psnr16_c)
addiu	$2,$2,%lo(quant_psnr16_c)
sw	$2,156($16)
lui	$2,%hi(quant_psnr8x8_c)
addiu	$2,$2,%lo(quant_psnr8x8_c)
sw	$2,160($16)
lui	$2,%hi(rd16_c)
addiu	$2,$2,%lo(rd16_c)
sw	$2,204($16)
lui	$2,%hi(rd8x8_c)
addiu	$2,$2,%lo(rd8x8_c)
sw	$2,208($16)
lui	$2,%hi(bit16_c)
addiu	$2,$2,%lo(bit16_c)
sw	$2,180($16)
lui	$2,%hi(bit8x8_c)
addiu	$2,$2,%lo(bit8x8_c)
sw	$2,184($16)
lui	$2,%hi(vsad16_c)
addiu	$2,$2,%lo(vsad16_c)
sw	$2,228($16)
lui	$2,%hi(vsad_intra16_c)
addiu	$2,$2,%lo(vsad_intra16_c)
sw	$2,244($16)
lui	$2,%hi(vsad_intra8_c)
addiu	$2,$2,%lo(vsad_intra8_c)
sw	$2,248($16)
lui	$2,%hi(vsse16_c)
addiu	$2,$2,%lo(vsse16_c)
sw	$2,252($16)
lui	$2,%hi(vsse_intra16_c)
addiu	$2,$2,%lo(vsse_intra16_c)
sw	$2,268($16)
lui	$2,%hi(vsse_intra8_c)
addiu	$2,$2,%lo(vsse_intra8_c)
sw	$2,272($16)
lui	$2,%hi(nsse16_c)
addiu	$2,$2,%lo(nsse16_c)
sw	$2,276($16)
lui	$2,%hi(nsse8_c)
addiu	$2,$2,%lo(nsse8_c)
.reloc	1f,R_MIPS_JALR,ff_dsputil_init_dwt
1:	jalr	$25
sw	$2,280($16)

move	$5,$0
lw	$28,16($sp)
li	$6,256			# 0x100
move	$4,$17
lw	$2,%got(ff_add_png_paeth_prediction)($28)
lw	$25,%call16(memset)($28)
sw	$2,2572($16)
lw	$2,%got(ff_vp3_h_loop_filter_c)($28)
sw	$2,2608($16)
lw	$2,%got(ff_vp3_v_loop_filter_c)($28)
sw	$2,2604($16)
lw	$2,%got(ff_vp3_idct_dc_add_c)($28)
sw	$2,2600($16)
lw	$2,%got(vorbis_inverse_coupling)($28)
sw	$2,2612($16)
lw	$2,%got(ff_ac3_downmix_c)($28)
sw	$2,2616($16)
lw	$2,%got(ff_lpc_compute_autocorr)($28)
sw	$2,2620($16)
lui	$2,%hi(ssd_int8_vs_int16_c)
addiu	$2,$2,%lo(ssd_int8_vs_int16_c)
sw	$2,540($16)
lui	$2,%hi(add_bytes_c)
addiu	$2,$2,%lo(add_bytes_c)
sw	$2,2544($16)
lui	$2,%hi(add_bytes_l2_c)
addiu	$2,$2,%lo(add_bytes_l2_c)
sw	$2,2548($16)
lui	$2,%hi(diff_bytes_c)
addiu	$2,$2,%lo(diff_bytes_c)
sw	$2,2552($16)
lui	$2,%hi(add_hfyu_median_prediction_c)
addiu	$2,$2,%lo(add_hfyu_median_prediction_c)
sw	$2,2560($16)
lui	$2,%hi(sub_hfyu_median_prediction_c)
addiu	$2,$2,%lo(sub_hfyu_median_prediction_c)
sw	$2,2556($16)
lui	$2,%hi(add_hfyu_left_prediction_c)
addiu	$2,$2,%lo(add_hfyu_left_prediction_c)
sw	$2,2564($16)
lui	$2,%hi(add_hfyu_left_prediction_bgr32_c)
addiu	$2,$2,%lo(add_hfyu_left_prediction_bgr32_c)
sw	$2,2568($16)
lui	$2,%hi(bswap_buf)
addiu	$2,$2,%lo(bswap_buf)
sw	$2,2576($16)
lui	$2,%hi(h263_h_loop_filter_c)
addiu	$2,$2,%lo(h263_h_loop_filter_c)
sw	$2,2584($16)
lui	$2,%hi(h263_v_loop_filter_c)
addiu	$2,$2,%lo(h263_v_loop_filter_c)
sw	$2,2580($16)
lui	$2,%hi(h261_loop_filter_c)
addiu	$2,$2,%lo(h261_loop_filter_c)
sw	$2,2588($16)
lui	$2,%hi(try_8x8basis_c)
addiu	$2,$2,%lo(try_8x8basis_c)
sw	$2,2772($16)
lui	$2,%hi(add_8x8basis_c)
addiu	$2,$2,%lo(add_8x8basis_c)
sw	$2,2776($16)
lui	$2,%hi(vector_fmul_c)
addiu	$2,$2,%lo(vector_fmul_c)
sw	$2,2624($16)
lui	$2,%hi(vector_fmul_reverse_c)
addiu	$2,$2,%lo(vector_fmul_reverse_c)
sw	$2,2628($16)
lui	$2,%hi(vector_fmul_add_c)
addiu	$2,$2,%lo(vector_fmul_add_c)
sw	$2,2632($16)
lui	$2,%hi(ff_vector_fmul_window_c)
addiu	$2,$2,%lo(ff_vector_fmul_window_c)
sw	$2,2636($16)
lui	$2,%hi(int32_to_float_fmul_scalar_c)
addiu	$2,$2,%lo(int32_to_float_fmul_scalar_c)
sw	$2,2640($16)
lui	$2,%hi(vector_clipf_c)
addiu	$2,$2,%lo(vector_clipf_c)
sw	$2,2644($16)
lui	$2,%hi(ff_float_to_int16_c)
addiu	$2,$2,%lo(ff_float_to_int16_c)
sw	$2,2676($16)
lui	$2,%hi(ff_float_to_int16_interleave_c)
addiu	$2,$2,%lo(ff_float_to_int16_interleave_c)
sw	$2,2680($16)
lui	$2,%hi(scalarproduct_int16_c)
addiu	$2,$2,%lo(scalarproduct_int16_c)
sw	$2,3052($16)
lui	$2,%hi(scalarproduct_and_madd_int16_c)
addiu	$2,$2,%lo(scalarproduct_and_madd_int16_c)
sw	$2,3056($16)
lui	$2,%hi(scalarproduct_float_c)
addiu	$2,$2,%lo(scalarproduct_float_c)
sw	$2,2668($16)
lui	$2,%hi(butterflies_float_c)
addiu	$2,$2,%lo(butterflies_float_c)
sw	$2,2672($16)
lui	$2,%hi(vector_fmul_scalar_c)
addiu	$2,$2,%lo(vector_fmul_scalar_c)
sw	$2,2648($16)
lui	$2,%hi(vector_fmul_sv_scalar_2_c)
addiu	$2,$2,%lo(vector_fmul_sv_scalar_2_c)
sw	$2,2652($16)
lui	$2,%hi(vector_fmul_sv_scalar_4_c)
addiu	$2,$2,%lo(vector_fmul_sv_scalar_4_c)
sw	$2,2656($16)
lui	$2,%hi(sv_fmul_scalar_2_c)
addiu	$2,$2,%lo(sv_fmul_scalar_2_c)
sw	$2,2660($16)
lui	$2,%hi(sv_fmul_scalar_4_c)
addiu	$2,$2,%lo(sv_fmul_scalar_4_c)
sw	$2,2664($16)
lw	$2,%got(av_image_copy_plane)($28)
sw	$2,2788($16)
lw	$2,%got(ff_shrink22)($28)
sw	$2,2792($16)
lw	$2,%got(ff_shrink44)($28)
sw	$2,2796($16)
lw	$2,%got(ff_shrink88)($28)
sw	$2,2800($16)
lui	$2,%hi(just_return)
addiu	$2,$2,%lo(just_return)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$2,2784($16)

move	$5,$0
lw	$28,16($sp)
li	$6,256			# 0x100
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$18

move	$2,$17
lw	$28,16($sp)
$L2606:
lw	$3,0($2)
bne	$3,$0,$L2604
nop

lw	$3,-512($2)
sw	$3,0($2)
$L2604:
lw	$3,256($2)
bne	$3,$0,$L2605
nop

lw	$3,-256($2)
sw	$3,256($2)
$L2605:
addiu	$2,$2,4
bne	$18,$2,$L2606
nop

lw	$2,2768($16)
lw	$7,1488($16)
lw	$6,1552($16)
lw	$5,1744($16)
addiu	$2,$2,-1
lw	$4,1808($16)
sltu	$3,$2,6
sw	$7,3572($16)
sw	$6,3636($16)
sw	$5,3828($16)
beq	$3,$0,$L2607
sw	$4,3892($16)

lui	$3,%hi($L2609)
sll	$2,$2,2
addiu	$3,$3,%lo($L2609)
addu	$2,$3,$2
lw	$2,0($2)
j	$2
nop

.rdata
.align	2
.align	2
$L2609:
.word	$L2618
.word	$L2619
.word	$L2620
.word	$L2621
.word	$L2622
.word	$L2623
.section	.text.unlikely
$L2618:
move	$2,$0
li	$4,64			# 0x40
addu	$3,$16,$2
$L2633:
sb	$2,2704($3)
addiu	$2,$2,1
bne	$2,$4,$L2633
addu	$3,$16,$2

.option	pic0
j	$L2632
.option	pic2
lw	$31,52($sp)

$L2619:
move	$4,$0
li	$6,64			# 0x40
$L2610:
andi	$2,$4,0x6
andi	$5,$4,0x1
sra	$3,$2,1
sll	$2,$5,2
andi	$5,$4,0x38
or	$2,$3,$2
or	$2,$2,$5
addu	$3,$16,$4
addiu	$4,$4,1
bne	$4,$6,$L2610
sb	$2,2704($3)

.option	pic0
j	$L2632
.option	pic2
lw	$31,52($sp)

$L2620:
lui	$5,%hi(simple_mmx_permutation)
move	$2,$0
addiu	$5,$5,%lo(simple_mmx_permutation)
li	$6,64			# 0x40
$L2611:
addu	$4,$5,$2
addu	$3,$16,$2
addiu	$2,$2,1
lbu	$4,0($4)
bne	$2,$6,$L2611
sb	$4,2704($3)

.option	pic0
j	$L2632
.option	pic2
lw	$31,52($sp)

$L2621:
move	$2,$0
li	$6,64			# 0x40
$L2612:
andi	$3,$2,0x7
sra	$5,$2,3
sll	$3,$3,3
addu	$4,$16,$2
or	$3,$3,$5
addiu	$2,$2,1
bne	$2,$6,$L2612
sb	$3,2704($4)

.option	pic0
j	$L2632
.option	pic2
lw	$31,52($sp)

$L2622:
move	$4,$0
li	$6,64			# 0x40
$L2613:
andi	$2,$4,0x3
sra	$5,$4,3
sll	$3,$2,3
andi	$2,$5,0x3
or	$2,$3,$2
andi	$5,$4,0x24
addu	$3,$16,$4
or	$2,$2,$5
addiu	$4,$4,1
bne	$4,$6,$L2613
sb	$2,2704($3)

.option	pic0
j	$L2632
.option	pic2
lw	$31,52($sp)

$L2623:
lui	$6,%hi(idct_sse2_row_perm)
move	$2,$0
addiu	$6,$6,%lo(idct_sse2_row_perm)
li	$7,64			# 0x40
$L2614:
andi	$4,$2,0x7
addu	$4,$4,$6
andi	$3,$2,0x38
addu	$5,$16,$2
lbu	$4,0($4)
addiu	$2,$2,1
or	$3,$3,$4
bne	$2,$7,$L2614
sb	$3,2704($5)

.option	pic0
j	$L2589
.option	pic2
lw	$31,52($sp)

$L2607:
lui	$6,%hi($LC1)
lw	$31,52($sp)
lw	$22,48($sp)
move	$4,$19
lw	$21,44($sp)
li	$5,16			# 0x10
lw	$20,40($sp)
addiu	$6,$6,%lo($LC1)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
lw	$25,%call16(av_log)($28)
.reloc	1f,R_MIPS_JALR,av_log
1:	jr	$25
addiu	$sp,$sp,56

$L2589:
$L2632:
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

.set	macro
.set	reorder
.end	dsputil_init
.size	dsputil_init, .-dsputil_init
.globl	coef_tab
.data
.align	2
.type	coef_tab, @object
.size	coef_tab, 24
coef_tab:
.half	2841
.half	565
.half	1609
.half	2408
.half	2676
.half	1108
.half	2048
.half	4
.half	128
.half	0
.half	8192
.half	0
.rdata
.align	2
.type	idct_sse2_row_perm, @object
.size	idct_sse2_row_perm, 8
idct_sse2_row_perm:
.byte	0
.byte	4
.byte	1
.byte	5
.byte	2
.byte	6
.byte	3
.byte	7
.align	2
.type	simple_mmx_permutation, @object
.size	simple_mmx_permutation, 64
simple_mmx_permutation:
.byte	0
.byte	8
.byte	4
.byte	9
.byte	1
.byte	12
.byte	5
.byte	13
.byte	16
.byte	24
.byte	20
.byte	25
.byte	17
.byte	28
.byte	21
.byte	29
.byte	32
.byte	40
.byte	36
.byte	41
.byte	33
.byte	44
.byte	37
.byte	45
.byte	18
.byte	26
.byte	22
.byte	27
.byte	19
.byte	30
.byte	23
.byte	31
.byte	2
.byte	10
.byte	6
.byte	11
.byte	3
.byte	14
.byte	7
.byte	15
.byte	48
.byte	56
.byte	52
.byte	57
.byte	49
.byte	60
.byte	53
.byte	61
.byte	34
.byte	42
.byte	38
.byte	43
.byte	35
.byte	46
.byte	39
.byte	47
.byte	50
.byte	58
.byte	54
.byte	59
.byte	51
.byte	62
.byte	55
.byte	63
.globl	ff_alternate_vertical_scan
.align	2
.type	ff_alternate_vertical_scan, @object
.size	ff_alternate_vertical_scan, 64
ff_alternate_vertical_scan:
.byte	0
.byte	8
.byte	16
.byte	24
.byte	1
.byte	9
.byte	2
.byte	10
.byte	17
.byte	25
.byte	32
.byte	40
.byte	48
.byte	56
.byte	57
.byte	49
.byte	41
.byte	33
.byte	26
.byte	18
.byte	3
.byte	11
.byte	4
.byte	12
.byte	19
.byte	27
.byte	34
.byte	42
.byte	50
.byte	58
.byte	35
.byte	43
.byte	51
.byte	59
.byte	20
.byte	28
.byte	5
.byte	13
.byte	6
.byte	14
.byte	21
.byte	29
.byte	36
.byte	44
.byte	52
.byte	60
.byte	37
.byte	45
.byte	53
.byte	61
.byte	22
.byte	30
.byte	7
.byte	15
.byte	23
.byte	31
.byte	38
.byte	46
.byte	54
.byte	62
.byte	39
.byte	47
.byte	55
.byte	63
.globl	ff_alternate_horizontal_scan
.align	2
.type	ff_alternate_horizontal_scan, @object
.size	ff_alternate_horizontal_scan, 64
ff_alternate_horizontal_scan:
.byte	0
.byte	1
.byte	2
.byte	3
.byte	8
.byte	9
.byte	16
.byte	17
.byte	10
.byte	11
.byte	4
.byte	5
.byte	6
.byte	7
.byte	15
.byte	14
.byte	13
.byte	12
.byte	19
.byte	18
.byte	24
.byte	25
.byte	32
.byte	33
.byte	26
.byte	27
.byte	20
.byte	21
.byte	22
.byte	23
.byte	28
.byte	29
.byte	30
.byte	31
.byte	34
.byte	35
.byte	40
.byte	41
.byte	48
.byte	49
.byte	42
.byte	43
.byte	36
.byte	37
.byte	38
.byte	39
.byte	44
.byte	45
.byte	46
.byte	47
.byte	50
.byte	51
.byte	56
.byte	57
.byte	58
.byte	59
.byte	52
.byte	53
.byte	54
.byte	55
.byte	60
.byte	61
.byte	62
.byte	63

.comm	inv_zigzag_direct16,128,16
.globl	ff_zigzag248_direct
.align	2
.type	ff_zigzag248_direct, @object
.size	ff_zigzag248_direct, 64
ff_zigzag248_direct:
.byte	0
.byte	8
.byte	1
.byte	9
.byte	16
.byte	24
.byte	2
.byte	10
.byte	17
.byte	25
.byte	32
.byte	40
.byte	48
.byte	56
.byte	33
.byte	41
.byte	18
.byte	26
.byte	3
.byte	11
.byte	4
.byte	12
.byte	19
.byte	27
.byte	34
.byte	42
.byte	49
.byte	57
.byte	50
.byte	58
.byte	35
.byte	43
.byte	20
.byte	28
.byte	5
.byte	13
.byte	6
.byte	14
.byte	21
.byte	29
.byte	36
.byte	44
.byte	51
.byte	59
.byte	52
.byte	60
.byte	37
.byte	45
.byte	22
.byte	30
.byte	7
.byte	15
.byte	23
.byte	31
.byte	38
.byte	46
.byte	53
.byte	61
.byte	54
.byte	62
.byte	39
.byte	47
.byte	55
.byte	63
.globl	ff_zigzag_direct
.align	2
.type	ff_zigzag_direct, @object
.size	ff_zigzag_direct, 64
ff_zigzag_direct:
.byte	0
.byte	1
.byte	8
.byte	16
.byte	9
.byte	2
.byte	3
.byte	10
.byte	17
.byte	24
.byte	32
.byte	25
.byte	18
.byte	11
.byte	4
.byte	5
.byte	12
.byte	19
.byte	26
.byte	33
.byte	40
.byte	48
.byte	41
.byte	34
.byte	27
.byte	20
.byte	13
.byte	6
.byte	7
.byte	14
.byte	21
.byte	28
.byte	35
.byte	42
.byte	49
.byte	56
.byte	57
.byte	50
.byte	43
.byte	36
.byte	29
.byte	22
.byte	15
.byte	23
.byte	30
.byte	37
.byte	44
.byte	51
.byte	58
.byte	59
.byte	52
.byte	45
.byte	38
.byte	31
.byte	39
.byte	46
.byte	53
.byte	60
.byte	61
.byte	54
.byte	47
.byte	55
.byte	62
.byte	63
.globl	ff_squareTbl
.section	.bss,"aw",@nobits
.align	2
.type	ff_squareTbl, @object
.size	ff_squareTbl, 2048
ff_squareTbl:
.space	2048
.globl	ff_cropTbl
.align	2
.type	ff_cropTbl, @object
.size	ff_cropTbl, 2304
ff_cropTbl:
.space	2304

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
