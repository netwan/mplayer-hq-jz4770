	.file	1 "aac_fft.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.abicalls
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft4
	.type	aac_fft4, @function
aac_fft4:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$4,0
 # 0 "" 2
#NO_APP
	move	$2,$4
#APP
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	j	$31
	.end	aac_fft4
	.size	aac_fft4, .-aac_fft4
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft8
	.type	aac_fft8, @function
aac_fft8:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	move	$2,$4
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	move	$2,$4
#APP
 # 268 "aac_fft.c" 1
	S32LDI xr1,$2,32
 # 0 "" 2
 # 269 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 270 "aac_fft.c" 1
	S32LDI xr3,$2,4
 # 0 "" 2
 # 271 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 273 "aac_fft.c" 1
	D32ADD xr5,xr1,xr3,xr6,AS
 # 0 "" 2
 # 274 "aac_fft.c" 1
	D32ADD xr7,xr2,xr4,xr8,AS
 # 0 "" 2
 # 276 "aac_fft.c" 1
	S32SDI xr6,$2,-4
 # 0 "" 2
 # 277 "aac_fft.c" 1
	S32SDI xr8,$2,4
 # 0 "" 2
 # 278 "aac_fft.c" 1
	S32M2I xr5, $3
 # 0 "" 2
 # 279 "aac_fft.c" 1
	S32M2I xr7, $3
 # 0 "" 2
 # 280 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 281 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 282 "aac_fft.c" 1
	S32LDI xr3,$2,4
 # 0 "" 2
 # 283 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 285 "aac_fft.c" 1
	D32ADD xr6,xr1,xr3,xr9,AS
 # 0 "" 2
 # 286 "aac_fft.c" 1
	D32ADD xr8,xr2,xr4,xr10,AS
 # 0 "" 2
 # 287 "aac_fft.c" 1
	S32SDI xr9,$2,-4
 # 0 "" 2
 # 288 "aac_fft.c" 1
	S32SDI xr10,$2,4
 # 0 "" 2
 # 290 "aac_fft.c" 1
	D32ADD xr11,xr6,xr5,xr12,SA
 # 0 "" 2
 # 291 "aac_fft.c" 1
	D32ADD xr13,xr7,xr8,xr14,SA
 # 0 "" 2
 # 292 "aac_fft.c" 1
	S32M2I xr11, $3
 # 0 "" 2
 # 293 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
 # 294 "aac_fft.c" 1
	S32M2I xr13, $3
 # 0 "" 2
 # 295 "aac_fft.c" 1
	S32M2I xr14, $3
 # 0 "" 2
 # 296 "aac_fft.c" 1
	S32LDI xr1,$2,-60
 # 0 "" 2
 # 297 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 298 "aac_fft.c" 1
	S32LDI xr3,$2,12
 # 0 "" 2
 # 299 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 301 "aac_fft.c" 1
	D32ADD xr5,xr1,xr12,xr6,SA
 # 0 "" 2
 # 302 "aac_fft.c" 1
	D32ADD xr7,xr2,xr14,xr8,SA
 # 0 "" 2
 # 304 "aac_fft.c" 1
	D32ADD xr9,xr3,xr13,xr10,SA
 # 0 "" 2
 # 305 "aac_fft.c" 1
	D32ADD xr1,xr4,xr11,xr2,SA
 # 0 "" 2
 # 307 "aac_fft.c" 1
	S32SDI xr2,$2,0
 # 0 "" 2
 # 308 "aac_fft.c" 1
	S32SDI xr10,$2,-4
 # 0 "" 2
 # 309 "aac_fft.c" 1
	S32SDI xr8,$2,-12
 # 0 "" 2
 # 310 "aac_fft.c" 1
	S32SDI xr6,$2,-4
 # 0 "" 2
 # 311 "aac_fft.c" 1
	S32SDI xr5,$2,32
 # 0 "" 2
 # 312 "aac_fft.c" 1
	S32SDI xr7,$2,4
 # 0 "" 2
 # 313 "aac_fft.c" 1
	S32SDI xr9,$2,12
 # 0 "" 2
 # 314 "aac_fft.c" 1
	S32SDI xr1,$2,4
 # 0 "" 2
#NO_APP
	li	$2,1518469120			# 0x5a820000
	lw	$6,40($4)
	addiu	$2,$2,31130
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$2
 # 0 "" 2
#NO_APP
	lw	$7,44($4)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr3,xr4,$7,$2
 # 0 "" 2
#NO_APP
	lw	$3,56($4)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$5,60($4)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MADD xr1,xr2,$7,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 317 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr11, $5
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$8,28($4)
	subu	$11,$5,$6
	lw	$9,8($4)
	subu	$10,$3,$2
	lw	$7,12($4)
	addu	$5,$6,$5
	addu	$2,$3,$2
	lw	$6,24($4)
	subu	$3,$8,$11
	subu	$12,$9,$5
	addu	$8,$8,$11
	addu	$5,$9,$5
	sw	$3,60($4)
	subu	$9,$6,$10
	sw	$12,40($4)
	subu	$3,$7,$2
	sw	$8,28($4)
	addu	$6,$6,$10
	sw	$5,8($4)
	addu	$2,$7,$2
	sw	$9,56($4)
	sw	$3,44($4)
	sw	$6,24($4)
	.set	noreorder
	.set	nomacro
	j	$31
	sw	$2,12($4)
	.set	macro
	.set	reorder

	.end	aac_fft8
	.size	aac_fft8, .-aac_fft8
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft16
	.type	aac_fft16, @function
aac_fft16:
	.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-32
	sw	$16,24($sp)
	sw	$31,28($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	move	$16,$4
	.set	macro
	.set	reorder

	addiu	$2,$16,64
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	addiu	$2,$16,96
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	lw	$6,96($16)
	li	$2,1518469120			# 0x5a820000
	lw	$5,64($16)
	lw	$4,68($16)
	addiu	$2,$2,31130
	lw	$3,100($16)
	subu	$12,$6,$5
	lw	$9,0($16)
	addu	$5,$6,$5
	lw	$6,36($16)
	subu	$11,$4,$3
	lw	$7,4($16)
	lw	$8,32($16)
	addu	$3,$4,$3
	subu	$4,$6,$12
	lw	$10,80($16)
	subu	$13,$9,$5
	addu	$6,$6,$12
	addu	$5,$9,$5
	sw	$4,100($16)
	subu	$9,$8,$11
	sw	$13,64($16)
	subu	$4,$7,$3
	sw	$6,36($16)
	addu	$8,$8,$11
	sw	$5,0($16)
	addu	$3,$7,$3
	sw	$9,96($16)
	sw	$4,68($16)
	sw	$8,32($16)
	sw	$3,4($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$2
 # 0 "" 2
#NO_APP
	lw	$5,84($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,112($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,116($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 328 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$9,52($16)
	subu	$13,$6,$7
	subu	$12,$5,$4
	lw	$8,20($16)
	addu	$4,$5,$4
	lw	$10,16($16)
	subu	$5,$9,$13
	lw	$11,72($16)
	addu	$6,$7,$6
	lw	$7,48($16)
	addu	$9,$9,$13
	sw	$5,116($16)
	subu	$5,$8,$4
	addu	$4,$8,$4
	subu	$14,$10,$6
	sw	$9,52($16)
	addu	$6,$10,$6
	sw	$5,84($16)
	subu	$10,$7,$12
	sw	$4,20($16)
	addu	$7,$7,$12
	sw	$14,80($16)
	li	$4,1983971328			# 0x76410000
	sw	$6,16($16)
	sw	$10,112($16)
	sw	$7,48($16)
	ori	$4,$4,0xaf00
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$4
 # 0 "" 2
#NO_APP
	lw	$8,76($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$4
 # 0 "" 2
#NO_APP
	lw	$6,104($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr7,xr8,$6,$4
 # 0 "" 2
#NO_APP
	lw	$7,108($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr9,xr10,$7,$4
 # 0 "" 2
#NO_APP
	li	$5,821755904			# 0x30fb0000
	ori	$5,$5,0xc540
#APP
 # 329 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr7,xr8,$7,$5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MADD xr9,xr10,$6,$5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr5, $9
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr6, $7
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr11, $8
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr12, $6
 # 0 "" 2
#NO_APP
	lw	$11,44($16)
	subu	$15,$8,$9
	lw	$12,8($16)
	subu	$14,$7,$6
	lw	$10,12($16)
	addu	$8,$9,$8
	addu	$6,$7,$6
	lw	$9,40($16)
	subu	$7,$11,$15
	lw	$13,88($16)
	subu	$24,$12,$8
	addu	$11,$11,$15
	addu	$8,$12,$8
	sw	$7,108($16)
	subu	$12,$9,$14
	sw	$24,72($16)
	subu	$7,$10,$6
	sw	$11,44($16)
	addu	$9,$9,$14
	sw	$8,8($16)
	addu	$6,$10,$6
	sw	$12,104($16)
	sw	$7,76($16)
	sw	$9,40($16)
	sw	$6,12($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr1,xr2,$13,$5
 # 0 "" 2
#NO_APP
	lw	$8,92($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$5
 # 0 "" 2
#NO_APP
	lw	$6,120($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr7,xr8,$6,$5
 # 0 "" 2
#NO_APP
	lw	$7,124($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr9,xr10,$7,$5
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr3,xr4,$13,$4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr7,xr8,$7,$4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr9,xr10,$6,$4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,60($16)
	subu	$10,$4,$5
	lw	$8,24($16)
	subu	$9,$3,$2
	lw	$6,28($16)
	addu	$4,$5,$4
	addu	$2,$3,$2
	lw	$5,56($16)
	subu	$3,$7,$10
	lw	$31,28($sp)
	subu	$11,$8,$4
	addu	$7,$7,$10
	sw	$3,124($16)
	addu	$4,$8,$4
	subu	$3,$6,$2
	sw	$11,88($16)
	subu	$8,$5,$9
	sw	$7,60($16)
	addu	$5,$5,$9
	sw	$4,24($16)
	addu	$2,$6,$2
	sw	$3,92($16)
	sw	$8,120($16)
	sw	$5,56($16)
	sw	$2,28($16)
	lw	$16,24($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,32
	.set	macro
	.set	reorder

	.end	aac_fft16
	.size	aac_fft16, .-aac_fft16
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft32
	.type	aac_fft32, @function
aac_fft32:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-40
	sw	$16,24($sp)
	move	$16,$4
	sw	$31,36($sp)
	sw	$18,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	sw	$17,28($sp)
	.set	macro
	.set	reorder

	addiu	$2,$16,64
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	addiu	$2,$16,96
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	lw	$6,64($16)
	li	$3,1518469120			# 0x5a820000
	lw	$5,96($16)
	lw	$4,68($16)
	addiu	$3,$3,31130
	lw	$2,100($16)
	subu	$12,$5,$6
	lw	$9,0($16)
	addu	$5,$6,$5
	lw	$6,36($16)
	subu	$11,$4,$2
	lw	$7,4($16)
	lw	$8,32($16)
	addu	$2,$4,$2
	subu	$4,$6,$12
	lw	$10,80($16)
	subu	$13,$9,$5
	addu	$6,$6,$12
	addu	$5,$9,$5
	sw	$4,100($16)
	subu	$9,$8,$11
	sw	$13,64($16)
	subu	$4,$7,$2
	sw	$6,36($16)
	addu	$8,$8,$11
	sw	$5,0($16)
	addu	$2,$7,$2
	sw	$9,96($16)
	sw	$4,68($16)
	sw	$8,32($16)
	sw	$2,4($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$3
 # 0 "" 2
#NO_APP
	lw	$5,84($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$3
 # 0 "" 2
#NO_APP
	lw	$2,112($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr7,xr8,$2,$3
 # 0 "" 2
#NO_APP
	lw	$4,116($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr9,xr10,$2,$3
 # 0 "" 2
#NO_APP
	li	$17,2			# 0x2
	li	$18,31			# 0x1f
#APP
 # 328 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr11, $5
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$8,52($16)
	subu	$12,$5,$6
	subu	$11,$4,$2
	lw	$7,20($16)
	addu	$2,$4,$2
	lw	$9,16($16)
	subu	$4,$8,$12
	lw	$10,72($16)
	addu	$5,$6,$5
	lw	$6,48($16)
	addu	$8,$8,$12
	sw	$4,116($16)
	subu	$4,$7,$2
	addu	$2,$7,$2
	subu	$13,$9,$5
	sw	$8,52($16)
	addu	$5,$9,$5
	sw	$4,84($16)
	subu	$9,$6,$11
	sw	$2,20($16)
	addu	$6,$6,$11
	sw	$13,80($16)
	li	$2,1983971328			# 0x76410000
	sw	$5,16($16)
	sw	$9,112($16)
	sw	$6,48($16)
	ori	$2,$2,0xaf00
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$2
 # 0 "" 2
#NO_APP
	lw	$7,76($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr3,xr4,$7,$2
 # 0 "" 2
#NO_APP
	lw	$5,104($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr7,xr8,$5,$2
 # 0 "" 2
#NO_APP
	lw	$6,108($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr9,xr10,$6,$2
 # 0 "" 2
#NO_APP
	li	$4,821755904			# 0x30fb0000
	ori	$4,$4,0xc540
#APP
 # 329 "aac_fft.c" 1
	S32MADD xr1,xr2,$7,$4
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$4
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr7,xr8,$6,$4
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MADD xr9,xr10,$5,$4
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr5, $8
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr6, $6
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr12, $5
 # 0 "" 2
#NO_APP
	lw	$10,44($16)
	subu	$14,$7,$8
	lw	$11,8($16)
	subu	$13,$6,$5
	lw	$9,12($16)
	addu	$7,$8,$7
	addu	$5,$6,$5
	lw	$8,40($16)
	subu	$6,$10,$14
	lw	$12,88($16)
	subu	$15,$11,$7
	addu	$10,$10,$14
	addu	$7,$11,$7
	sw	$6,108($16)
	subu	$11,$8,$13
	sw	$15,72($16)
	subu	$6,$9,$5
	sw	$10,44($16)
	addu	$8,$8,$13
	sw	$7,8($16)
	addu	$5,$9,$5
	sw	$11,104($16)
	sw	$6,76($16)
	sw	$8,40($16)
	sw	$5,12($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$4
 # 0 "" 2
#NO_APP
	lw	$7,92($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr3,xr4,$7,$4
 # 0 "" 2
#NO_APP
	lw	$5,120($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr7,xr8,$5,$4
 # 0 "" 2
#NO_APP
	lw	$6,124($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr9,xr10,$6,$4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr1,xr2,$7,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr7,xr8,$6,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr9,xr10,$5,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr11, $5
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$8,60($16)
	subu	$11,$5,$6
	lw	$9,24($16)
	subu	$10,$4,$2
	lw	$7,28($16)
	addu	$2,$4,$2
	addu	$5,$6,$5
	lw	$6,56($16)
	subu	$4,$8,$11
	subu	$12,$9,$5
	addu	$8,$8,$11
	addu	$5,$9,$5
	sw	$4,124($16)
	subu	$9,$6,$10
	sw	$12,88($16)
	subu	$4,$7,$2
	sw	$8,60($16)
	addu	$7,$7,$2
	sw	$5,24($16)
	addu	$6,$6,$10
	sw	$9,120($16)
	addiu	$2,$16,128
	sw	$4,92($16)
	sw	$7,28($16)
	sw	$6,56($16)
	move	$4,$2
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$4,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$4,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$4,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$4,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$4,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$4,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$4,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$4,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$4,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$4,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$4,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$4,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$4,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$4,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$4,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$4,-4
 # 0 "" 2
 # 268 "aac_fft.c" 1
	S32LDI xr1,$2,32
 # 0 "" 2
 # 269 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 270 "aac_fft.c" 1
	S32LDI xr3,$2,4
 # 0 "" 2
 # 271 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 273 "aac_fft.c" 1
	D32ADD xr5,xr1,xr3,xr6,AS
 # 0 "" 2
 # 274 "aac_fft.c" 1
	D32ADD xr7,xr2,xr4,xr8,AS
 # 0 "" 2
 # 276 "aac_fft.c" 1
	S32SDI xr6,$2,-4
 # 0 "" 2
 # 277 "aac_fft.c" 1
	S32SDI xr8,$2,4
 # 0 "" 2
 # 278 "aac_fft.c" 1
	S32M2I xr5, $4
 # 0 "" 2
 # 279 "aac_fft.c" 1
	S32M2I xr7, $4
 # 0 "" 2
 # 280 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 281 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 282 "aac_fft.c" 1
	S32LDI xr3,$2,4
 # 0 "" 2
 # 283 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 285 "aac_fft.c" 1
	D32ADD xr6,xr1,xr3,xr9,AS
 # 0 "" 2
 # 286 "aac_fft.c" 1
	D32ADD xr8,xr2,xr4,xr10,AS
 # 0 "" 2
 # 287 "aac_fft.c" 1
	S32SDI xr9,$2,-4
 # 0 "" 2
 # 288 "aac_fft.c" 1
	S32SDI xr10,$2,4
 # 0 "" 2
 # 290 "aac_fft.c" 1
	D32ADD xr11,xr6,xr5,xr12,SA
 # 0 "" 2
 # 291 "aac_fft.c" 1
	D32ADD xr13,xr7,xr8,xr14,SA
 # 0 "" 2
 # 292 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 293 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
 # 294 "aac_fft.c" 1
	S32M2I xr13, $4
 # 0 "" 2
 # 295 "aac_fft.c" 1
	S32M2I xr14, $4
 # 0 "" 2
 # 296 "aac_fft.c" 1
	S32LDI xr1,$2,-60
 # 0 "" 2
 # 297 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 298 "aac_fft.c" 1
	S32LDI xr3,$2,12
 # 0 "" 2
 # 299 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 301 "aac_fft.c" 1
	D32ADD xr5,xr1,xr12,xr6,SA
 # 0 "" 2
 # 302 "aac_fft.c" 1
	D32ADD xr7,xr2,xr14,xr8,SA
 # 0 "" 2
 # 304 "aac_fft.c" 1
	D32ADD xr9,xr3,xr13,xr10,SA
 # 0 "" 2
 # 305 "aac_fft.c" 1
	D32ADD xr1,xr4,xr11,xr2,SA
 # 0 "" 2
 # 307 "aac_fft.c" 1
	S32SDI xr2,$2,0
 # 0 "" 2
 # 308 "aac_fft.c" 1
	S32SDI xr10,$2,-4
 # 0 "" 2
 # 309 "aac_fft.c" 1
	S32SDI xr8,$2,-12
 # 0 "" 2
 # 310 "aac_fft.c" 1
	S32SDI xr6,$2,-4
 # 0 "" 2
 # 311 "aac_fft.c" 1
	S32SDI xr5,$2,32
 # 0 "" 2
 # 312 "aac_fft.c" 1
	S32SDI xr7,$2,4
 # 0 "" 2
 # 313 "aac_fft.c" 1
	S32SDI xr9,$2,12
 # 0 "" 2
 # 314 "aac_fft.c" 1
	S32SDI xr1,$2,4
 # 0 "" 2
#NO_APP
	lw	$5,168($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$3
 # 0 "" 2
#NO_APP
	lw	$6,172($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$2,184($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr7,xr8,$2,$3
 # 0 "" 2
#NO_APP
	lw	$4,188($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MADD xr9,xr10,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,156($16)
	subu	$10,$4,$5
	lw	$8,136($16)
	subu	$9,$3,$2
	lw	$6,140($16)
	addu	$4,$5,$4
	addu	$2,$3,$2
	lw	$5,152($16)
	subu	$3,$7,$10
	subu	$11,$8,$4
	addu	$4,$8,$4
	addu	$7,$7,$10
	sw	$3,188($16)
	subu	$8,$5,$9
	sw	$11,168($16)
	subu	$3,$6,$2
	sw	$4,136($16)
	addu	$5,$5,$9
	sw	$7,156($16)
	addu	$2,$6,$2
	sw	$8,184($16)
	addiu	$4,$16,192
	sw	$3,172($16)
	sw	$5,152($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	sw	$2,140($16)
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,128($16)
	lw	$4,192($16)
	addiu	$6,$6,24320
	lw	$3,132($16)
	lw	$11,68($16)
	lw	$2,196($16)
	subu	$7,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$10,64($16)
	lw	$9,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,136($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,196($16)
	subu	$11,$10,$5
	sw	$13,128($16)
	subu	$3,$9,$2
	sw	$7,68($16)
	addu	$5,$5,$10
	sw	$4,0($16)
	addu	$2,$2,$9
	sw	$11,192($16)
	sw	$3,132($16)
	sw	$5,64($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,140($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,200($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,204($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,76($16)
	subu	$6,$7,$5
	lw	$9,72($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,136($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,204($16)
	addu	$5,$5,$9
	sw	$6,76($16)
	subu	$3,$8,$2
	sw	$7,200($16)
	addu	$2,$2,$8
	sw	$4,8($16)
	lui	$10,%hi(aac_cos_32+32)
	sw	$5,72($16)
	lui	$9,%hi(aac_cos_32)
	sw	$3,140($16)
	addiu	$11,$16,48
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_32+32)
	addiu	$9,$9,%lo(aac_cos_32)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
$L6:
	addiu	$16,$16,16
	lw	$2,8($9)
	lw	$5,128($16)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$8,132($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$2
 # 0 "" 2
#NO_APP
	lw	$3,192($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,196($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,68($16)
	subu	$8,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$15,64($16)
	subu	$5,$3,$2
	lw	$14,4($16)
	addu	$2,$3,$2
	subu	$3,$17,$8
	lw	$13,136($16)
	subu	$18,$12,$4
	addu	$8,$8,$17
	addu	$4,$12,$4
	sw	$3,196($16)
	subu	$12,$15,$5
	sw	$18,128($16)
	subu	$3,$14,$2
	sw	$8,68($16)
	addu	$2,$2,$14
	sw	$4,0($16)
	addu	$5,$5,$15
	sw	$12,192($16)
	sw	$3,132($16)
	sw	$2,4($16)
	sw	$5,64($16)
	lw	$2,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$13,$2
 # 0 "" 2
#NO_APP
	lw	$5,140($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,200($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,204($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$13,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,8($16)
	subu	$8,$12,$5
	lw	$15,76($16)
	addu	$4,$5,$12
	lw	$14,72($16)
	subu	$5,$3,$2
	lw	$13,12($16)
	addu	$2,$3,$2
	subu	$12,$17,$4
	subu	$3,$15,$8
	addu	$4,$4,$17
	addu	$8,$8,$15
	sw	$12,136($16)
	sw	$3,204($16)
	subu	$12,$14,$5
	subu	$3,$13,$2
	sw	$4,8($16)
	addu	$5,$5,$14
	sw	$8,76($16)
	addu	$2,$2,$13
	sw	$12,200($16)
	sw	$3,140($16)
	sw	$5,72($16)
	.set	noreorder
	.set	nomacro
	bne	$16,$11,$L6
	sw	$2,12($16)
	.set	macro
	.set	reorder

	lw	$31,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,40
	.set	macro
	.set	reorder

	.end	aac_fft32
	.size	aac_fft32, .-aac_fft32
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft64
	.type	aac_fft64, @function
aac_fft64:
	.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
	.mask	0x807f0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-56
	sw	$16,24($sp)
	move	$16,$4
	sw	$31,52($sp)
	sw	$22,48($sp)
	sw	$21,44($sp)
	sw	$20,40($sp)
	sw	$19,36($sp)
	sw	$18,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	sw	$17,28($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,128
	.set	macro
	.set	reorder

	addiu	$2,$16,192
	move	$3,$2
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$3,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$3,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$3,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$3,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$3,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$3,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$3,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$3,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$3,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$3,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$3,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$3,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$3,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$3,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$3,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$3,-4
 # 0 "" 2
 # 268 "aac_fft.c" 1
	S32LDI xr1,$2,32
 # 0 "" 2
 # 269 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 270 "aac_fft.c" 1
	S32LDI xr3,$2,4
 # 0 "" 2
 # 271 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 273 "aac_fft.c" 1
	D32ADD xr5,xr1,xr3,xr6,AS
 # 0 "" 2
 # 274 "aac_fft.c" 1
	D32ADD xr7,xr2,xr4,xr8,AS
 # 0 "" 2
 # 276 "aac_fft.c" 1
	S32SDI xr6,$2,-4
 # 0 "" 2
 # 277 "aac_fft.c" 1
	S32SDI xr8,$2,4
 # 0 "" 2
 # 278 "aac_fft.c" 1
	S32M2I xr5, $3
 # 0 "" 2
 # 279 "aac_fft.c" 1
	S32M2I xr7, $3
 # 0 "" 2
 # 280 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 281 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 282 "aac_fft.c" 1
	S32LDI xr3,$2,4
 # 0 "" 2
 # 283 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 285 "aac_fft.c" 1
	D32ADD xr6,xr1,xr3,xr9,AS
 # 0 "" 2
 # 286 "aac_fft.c" 1
	D32ADD xr8,xr2,xr4,xr10,AS
 # 0 "" 2
 # 287 "aac_fft.c" 1
	S32SDI xr9,$2,-4
 # 0 "" 2
 # 288 "aac_fft.c" 1
	S32SDI xr10,$2,4
 # 0 "" 2
 # 290 "aac_fft.c" 1
	D32ADD xr11,xr6,xr5,xr12,SA
 # 0 "" 2
 # 291 "aac_fft.c" 1
	D32ADD xr13,xr7,xr8,xr14,SA
 # 0 "" 2
 # 292 "aac_fft.c" 1
	S32M2I xr11, $3
 # 0 "" 2
 # 293 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
 # 294 "aac_fft.c" 1
	S32M2I xr13, $3
 # 0 "" 2
 # 295 "aac_fft.c" 1
	S32M2I xr14, $3
 # 0 "" 2
 # 296 "aac_fft.c" 1
	S32LDI xr1,$2,-60
 # 0 "" 2
 # 297 "aac_fft.c" 1
	S32LDI xr2,$2,4
 # 0 "" 2
 # 298 "aac_fft.c" 1
	S32LDI xr3,$2,12
 # 0 "" 2
 # 299 "aac_fft.c" 1
	S32LDI xr4,$2,4
 # 0 "" 2
 # 301 "aac_fft.c" 1
	D32ADD xr5,xr1,xr12,xr6,SA
 # 0 "" 2
 # 302 "aac_fft.c" 1
	D32ADD xr7,xr2,xr14,xr8,SA
 # 0 "" 2
 # 304 "aac_fft.c" 1
	D32ADD xr9,xr3,xr13,xr10,SA
 # 0 "" 2
 # 305 "aac_fft.c" 1
	D32ADD xr1,xr4,xr11,xr2,SA
 # 0 "" 2
 # 307 "aac_fft.c" 1
	S32SDI xr2,$2,0
 # 0 "" 2
 # 308 "aac_fft.c" 1
	S32SDI xr10,$2,-4
 # 0 "" 2
 # 309 "aac_fft.c" 1
	S32SDI xr8,$2,-12
 # 0 "" 2
 # 310 "aac_fft.c" 1
	S32SDI xr6,$2,-4
 # 0 "" 2
 # 311 "aac_fft.c" 1
	S32SDI xr5,$2,32
 # 0 "" 2
 # 312 "aac_fft.c" 1
	S32SDI xr7,$2,4
 # 0 "" 2
 # 313 "aac_fft.c" 1
	S32SDI xr9,$2,12
 # 0 "" 2
 # 314 "aac_fft.c" 1
	S32SDI xr1,$2,4
 # 0 "" 2
#NO_APP
	li	$2,1518469120			# 0x5a820000
	lw	$5,232($16)
	addiu	$2,$2,31130
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$6,236($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$3,248($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,252($16)
#APP
 # 317 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 317 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr5, $9
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr6, $8
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 317 "aac_fft.c" 1
	S32M2I xr12, $7
 # 0 "" 2
#NO_APP
	lw	$15,128($16)
	subu	$18,$6,$9
	lw	$14,192($16)
	subu	$12,$8,$7
	lw	$11,220($16)
	addu	$6,$9,$6
	lw	$20,0($16)
	addu	$7,$8,$7
	lw	$13,132($16)
	addu	$5,$15,$14
	lw	$4,196($16)
	subu	$9,$14,$15
	lw	$10,216($16)
	subu	$21,$11,$18
	lw	$15,4($16)
	addu	$11,$11,$18
	subu	$18,$20,$5
	lw	$19,68($16)
	addu	$5,$5,$20
	lw	$17,64($16)
	subu	$8,$13,$4
	lw	$14,204($16)
	addu	$4,$13,$4
	lw	$13,200($16)
	subu	$24,$10,$12
	sw	$5,0($16)
	addu	$10,$10,$12
	sw	$21,252($16)
	subu	$5,$15,$4
	sw	$11,220($16)
	subu	$22,$13,$6
	sw	$24,248($16)
	subu	$21,$14,$7
	sw	$10,216($16)
	subu	$11,$19,$9
	sw	$5,132($16)
	subu	$10,$17,$8
	sw	$22,232($16)
	addu	$9,$9,$19
	sw	$21,236($16)
	addu	$8,$8,$17
	sw	$18,128($16)
	addu	$4,$4,$15
	sw	$11,196($16)
	li	$5,2106195968			# 0x7d8a0000
	sw	$9,68($16)
	sw	$10,192($16)
	addu	$6,$13,$6
	sw	$8,64($16)
	addu	$7,$14,$7
	sw	$4,4($16)
	addiu	$5,$5,24320
	lw	$12,136($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$5
 # 0 "" 2
#NO_APP
	lw	$8,140($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$6,$5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$7,$5
 # 0 "" 2
#NO_APP
	li	$4,418906112			# 0x18f80000
	ori	$4,$4,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$7,$4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$6,$4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,76($16)
	subu	$6,$7,$5
	lw	$9,72($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,136($16)
	subu	$7,$9,$5
	addu	$4,$4,$11
	addu	$6,$6,$10
	sw	$3,204($16)
	addu	$5,$5,$9
	sw	$7,200($16)
	subu	$3,$8,$2
	sw	$4,8($16)
	addu	$2,$2,$8
	sw	$6,76($16)
	lui	$10,%hi(aac_cos_32+32)
	sw	$5,72($16)
	lui	$9,%hi(aac_cos_32)
	sw	$3,140($16)
	addiu	$11,$16,48
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_32+32)
	addiu	$9,$9,%lo(aac_cos_32)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$2,$16
$L11:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$6,128($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$3
 # 0 "" 2
#NO_APP
	lw	$12,132($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,192($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,196($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$17,2			# 0x2
	li	$18,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$25,68($2)
	subu	$12,$5,$6
	lw	$13,0($2)
	addu	$5,$6,$5
	lw	$24,64($2)
	subu	$6,$4,$3
	lw	$15,4($2)
	addu	$3,$4,$3
	subu	$4,$25,$12
	lw	$14,136($2)
	subu	$19,$13,$5
	addu	$12,$12,$25
	addu	$5,$13,$5
	sw	$4,196($2)
	subu	$13,$24,$6
	sw	$19,128($2)
	subu	$4,$15,$3
	sw	$12,68($2)
	addu	$3,$3,$15
	sw	$5,0($2)
	addu	$6,$6,$24
	sw	$13,192($2)
	sw	$4,132($2)
	sw	$3,4($2)
	sw	$6,64($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$14,$3
 # 0 "" 2
#NO_APP
	lw	$6,140($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,200($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,204($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$14,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $13
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$25,8($2)
	subu	$12,$13,$6
	lw	$24,76($2)
	addu	$5,$6,$13
	lw	$15,72($2)
	subu	$6,$4,$3
	lw	$14,12($2)
	addu	$3,$4,$3
	subu	$13,$25,$5
	subu	$4,$24,$12
	addu	$5,$5,$25
	addu	$12,$12,$24
	sw	$13,136($2)
	sw	$4,204($2)
	subu	$13,$15,$6
	subu	$4,$14,$3
	sw	$5,8($2)
	addu	$6,$6,$15
	sw	$12,76($2)
	addu	$3,$3,$14
	sw	$13,200($2)
	sw	$4,140($2)
	sw	$6,72($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$11,$L11
	sw	$3,12($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,256
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,384
	.set	macro
	.set	reorder

	addiu	$2,$16,448
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	addiu	$2,$16,480
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	lw	$6,448($16)
	li	$2,1518469120			# 0x5a820000
	lw	$5,452($16)
	lw	$3,480($16)
	addiu	$2,$2,31130
	lw	$4,484($16)
	lw	$7,416($16)
	lw	$8,420($16)
	subu	$9,$3,$6
	lw	$11,384($16)
	addu	$3,$6,$3
	lw	$10,388($16)
	subu	$6,$5,$4
	addu	$4,$5,$4
	lw	$5,464($16)
	subu	$12,$8,$9
	subu	$13,$11,$3
	addu	$8,$8,$9
	subu	$9,$7,$6
	sw	$12,484($16)
	addu	$7,$7,$6
	sw	$13,448($16)
	subu	$6,$10,$4
	sw	$8,420($16)
	addu	$11,$11,$3
	sw	$9,480($16)
	sw	$7,416($16)
	addu	$10,$10,$4
	sw	$6,452($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$6,468($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$3,496($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,500($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$3,436($16)
	subu	$12,$6,$7
	lw	$8,400($16)
	subu	$9,$5,$4
	lw	$2,432($16)
	addu	$6,$7,$6
	lw	$13,404($16)
	addu	$4,$5,$4
	subu	$5,$3,$12
	lw	$7,456($16)
	subu	$14,$8,$6
	addu	$6,$8,$6
	subu	$8,$2,$9
	sw	$5,500($16)
	addu	$2,$2,$9
	sw	$14,464($16)
	subu	$5,$13,$4
	sw	$6,400($16)
	addu	$3,$3,$12
	sw	$8,496($16)
	addu	$4,$13,$4
	sw	$2,432($16)
	li	$2,1983971328			# 0x76410000
	sw	$5,468($16)
	sw	$3,436($16)
	sw	$4,404($16)
	ori	$2,$2,0xaf00
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr1,xr2,$7,$2
 # 0 "" 2
#NO_APP
	lw	$6,460($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$4,488($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$2
 # 0 "" 2
#NO_APP
	lw	$5,492($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$2
 # 0 "" 2
#NO_APP
	li	$3,821755904			# 0x30fb0000
	ori	$3,$3,0xc540
#APP
 # 329 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr3,xr4,$7,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr5, $8
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$7,428($16)
	subu	$12,$6,$8
	lw	$13,424($16)
	addu	$6,$8,$6
	lw	$15,392($16)
	subu	$8,$5,$4
	lw	$9,396($16)
	addu	$4,$5,$4
	subu	$5,$7,$12
	lw	$14,472($16)
	subu	$19,$15,$6
	addu	$7,$7,$12
	sw	$5,492($16)
	subu	$12,$13,$8
	addu	$5,$13,$8
	sw	$19,456($16)
	subu	$8,$9,$4
	sw	$7,428($16)
	addu	$6,$15,$6
	sw	$12,488($16)
	sw	$5,424($16)
	addu	$9,$9,$4
	sw	$8,460($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr1,xr2,$14,$3
 # 0 "" 2
#NO_APP
	lw	$7,476($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr3,xr4,$7,$3
 # 0 "" 2
#NO_APP
	lw	$4,504($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,508($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr1,xr2,$7,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr3,xr4,$14,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr6, $8
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr12, $5
 # 0 "" 2
#NO_APP
	lw	$15,408($16)
	subu	$21,$4,$7
	addu	$4,$7,$4
	lw	$12,412($16)
	subu	$20,$8,$5
	lw	$3,256($16)
	subu	$22,$15,$4
	lw	$25,132($16)
	addu	$4,$15,$4
	lw	$2,260($16)
	addu	$8,$8,$5
	lw	$19,4($16)
	subu	$5,$11,$3
	lw	$13,440($16)
	sw	$4,408($16)
	subu	$4,$12,$8
	addu	$3,$11,$3
	lw	$14,444($16)
	subu	$11,$2,$10
	lw	$7,0($16)
	lw	$24,128($16)
	addu	$2,$10,$2
	sw	$4,476($16)
	subu	$4,$25,$5
	addu	$8,$12,$8
	sw	$22,472($16)
	subu	$15,$14,$21
	lw	$10,264($16)
	sw	$4,388($16)
	subu	$4,$19,$2
	addu	$2,$2,$19
	sw	$8,412($16)
	subu	$12,$7,$3
	sw	$15,508($16)
	addu	$14,$14,$21
	sw	$4,260($16)
	subu	$21,$13,$20
	sw	$2,4($16)
	addu	$7,$3,$7
	sw	$12,256($16)
	addu	$13,$13,$20
	sw	$14,444($16)
	addu	$5,$5,$25
	sw	$21,504($16)
	subu	$8,$24,$11
	sw	$7,0($16)
	addu	$3,$11,$24
	sw	$13,440($16)
	li	$2,2137128960			# 0x7f620000
	sw	$5,132($16)
	sw	$8,384($16)
	sw	$3,128($16)
	addiu	$2,$2,13952
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$2
 # 0 "" 2
#NO_APP
	lw	$3,268($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$6,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$9,$2
 # 0 "" 2
#NO_APP
	li	$2,210436096			# 0xc8b0000
	ori	$2,$2,0xd360
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$6,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,140($16)
	subu	$6,$7,$5
	lw	$9,136($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,264($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,396($16)
	addu	$5,$5,$9
	sw	$6,140($16)
	subu	$3,$8,$2
	sw	$7,392($16)
	addu	$2,$2,$8
	sw	$4,8($16)
	lui	$10,%hi(aac_cos_64+64)
	sw	$5,136($16)
	lui	$9,%hi(aac_cos_64)
	sw	$3,268($16)
	addiu	$11,$16,112
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_64+64)
	addiu	$9,$9,%lo(aac_cos_64)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
$L12:
	addiu	$16,$16,16
	lw	$2,8($9)
	lw	$5,256($16)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$8,260($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$2
 # 0 "" 2
#NO_APP
	lw	$3,384($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,388($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,132($16)
	subu	$8,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$15,128($16)
	subu	$5,$3,$2
	lw	$14,4($16)
	addu	$2,$3,$2
	subu	$3,$17,$8
	lw	$13,264($16)
	subu	$18,$12,$4
	addu	$8,$8,$17
	addu	$4,$12,$4
	sw	$3,388($16)
	subu	$12,$15,$5
	sw	$18,256($16)
	subu	$3,$14,$2
	sw	$8,132($16)
	addu	$2,$2,$14
	sw	$4,0($16)
	addu	$5,$5,$15
	sw	$12,384($16)
	sw	$3,260($16)
	sw	$2,4($16)
	sw	$5,128($16)
	lw	$2,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$13,$2
 # 0 "" 2
#NO_APP
	lw	$5,268($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,392($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,396($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$13,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,8($16)
	subu	$8,$12,$5
	lw	$15,140($16)
	addu	$4,$5,$12
	lw	$14,136($16)
	subu	$5,$3,$2
	lw	$13,12($16)
	addu	$2,$3,$2
	subu	$12,$17,$4
	subu	$3,$15,$8
	addu	$4,$4,$17
	addu	$8,$8,$15
	sw	$12,264($16)
	sw	$3,396($16)
	subu	$12,$14,$5
	subu	$3,$13,$2
	sw	$4,8($16)
	addu	$5,$5,$14
	sw	$8,140($16)
	addu	$2,$2,$13
	sw	$12,392($16)
	sw	$3,268($16)
	sw	$5,136($16)
	.set	noreorder
	.set	nomacro
	bne	$16,$11,$L12
	sw	$2,12($16)
	.set	macro
	.set	reorder

	lw	$31,52($sp)
	lw	$22,48($sp)
	lw	$21,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,56
	.set	macro
	.set	reorder

	.end	aac_fft64
	.size	aac_fft64, .-aac_fft64
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft128
	.type	aac_fft128, @function
aac_fft128:
	.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
	.mask	0x807f0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-56
	sw	$16,24($sp)
	move	$16,$4
	sw	$31,52($sp)
	sw	$22,48($sp)
	sw	$21,44($sp)
	sw	$20,40($sp)
	sw	$19,36($sp)
	sw	$18,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	sw	$17,28($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,128
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,192
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,128($16)
	lw	$4,192($16)
	addiu	$6,$6,24320
	lw	$3,132($16)
	lw	$11,68($16)
	lw	$2,196($16)
	subu	$7,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$10,64($16)
	lw	$9,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,136($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,196($16)
	subu	$11,$10,$5
	sw	$13,128($16)
	subu	$3,$9,$2
	sw	$7,68($16)
	addu	$5,$5,$10
	sw	$4,0($16)
	addu	$2,$2,$9
	sw	$11,192($16)
	sw	$3,132($16)
	sw	$5,64($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,140($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,200($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,204($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,76($16)
	subu	$6,$7,$5
	lw	$9,72($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,136($16)
	lui	$19,%hi(aac_cos_32+32)
	subu	$7,$9,$5
	lui	$18,%hi(aac_cos_32)
	sw	$3,204($16)
	addu	$4,$4,$11
	subu	$3,$8,$2
	sw	$7,200($16)
	addu	$2,$2,$8
	addu	$6,$6,$10
	sw	$4,8($16)
	addu	$5,$5,$9
	sw	$3,140($16)
	addiu	$19,$19,%lo(aac_cos_32+32)
	sw	$2,12($16)
	addiu	$18,$18,%lo(aac_cos_32)
	sw	$6,76($16)
	addiu	$11,$16,48
	sw	$5,72($16)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$2,$16
	move	$10,$19
	move	$9,$18
$L17:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$6,128($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$3
 # 0 "" 2
#NO_APP
	lw	$12,132($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,192($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,196($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$17,2			# 0x2
	li	$20,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $12
 # 0 "" 2
#NO_APP
	lw	$6,0($2)
	subu	$15,$4,$5
	addu	$4,$5,$4
	lw	$24,68($2)
	lw	$25,64($2)
	subu	$14,$3,$12
	lw	$21,4($2)
	subu	$13,$6,$4
	addu	$6,$6,$4
	addu	$3,$3,$12
	lw	$12,136($2)
	sw	$13,128($2)
	subu	$22,$24,$15
	sw	$6,0($2)
	addu	$5,$15,$24
	subu	$6,$21,$3
	subu	$13,$25,$14
	sw	$22,196($2)
	addu	$3,$3,$21
	sw	$5,68($2)
	addu	$4,$14,$25
	sw	$6,132($2)
	sw	$13,192($2)
	sw	$3,4($2)
	sw	$4,64($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$3
 # 0 "" 2
#NO_APP
	lw	$6,140($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,200($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,204($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$21,8($2)
	addu	$5,$6,$12
	subu	$13,$12,$6
	lw	$24,76($2)
	lw	$15,72($2)
	subu	$6,$4,$3
	subu	$12,$21,$5
	lw	$14,12($2)
	addu	$5,$5,$21
	addu	$3,$4,$3
	subu	$21,$24,$13
	sw	$12,136($2)
	addu	$4,$13,$24
	sw	$5,8($2)
	subu	$12,$15,$6
	subu	$5,$14,$3
	sw	$21,204($2)
	addu	$6,$6,$15
	sw	$4,76($2)
	addu	$3,$3,$14
	sw	$12,200($2)
	sw	$5,140($2)
	sw	$6,72($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$11,$L17
	sw	$3,12($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,256
	.set	macro
	.set	reorder

	addiu	$2,$16,320
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	addiu	$2,$16,352
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	lw	$6,320($16)
	li	$2,1518469120			# 0x5a820000
	lw	$5,352($16)
	lw	$4,324($16)
	addiu	$2,$2,31130
	lw	$3,356($16)
	subu	$12,$5,$6
	lw	$9,256($16)
	addu	$5,$6,$5
	lw	$6,292($16)
	subu	$11,$4,$3
	lw	$7,260($16)
	lw	$8,288($16)
	addu	$3,$4,$3
	subu	$4,$6,$12
	lw	$10,336($16)
	subu	$13,$9,$5
	addu	$6,$6,$12
	addu	$5,$9,$5
	sw	$4,356($16)
	subu	$9,$8,$11
	sw	$13,320($16)
	subu	$4,$7,$3
	sw	$6,292($16)
	addu	$8,$8,$11
	sw	$5,256($16)
	addu	$3,$7,$3
	sw	$9,352($16)
	sw	$4,324($16)
	sw	$8,288($16)
	sw	$3,260($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$2
 # 0 "" 2
#NO_APP
	lw	$5,340($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,368($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,372($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$20
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$20
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$20
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$20
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,308($16)
	subu	$11,$4,$5
	subu	$10,$3,$2
	lw	$6,276($16)
	addu	$2,$3,$2
	lw	$8,272($16)
	subu	$3,$7,$11
	lw	$9,328($16)
	addu	$4,$5,$4
	lw	$5,304($16)
	addu	$7,$7,$11
	sw	$3,372($16)
	subu	$3,$6,$2
	addu	$2,$6,$2
	subu	$12,$8,$4
	sw	$7,308($16)
	addu	$4,$8,$4
	sw	$3,340($16)
	subu	$8,$5,$10
	sw	$2,276($16)
	addu	$5,$5,$10
	sw	$12,336($16)
	li	$2,1983971328			# 0x76410000
	sw	$4,272($16)
	sw	$8,368($16)
	sw	$5,304($16)
	ori	$2,$2,0xaf00
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr1,xr2,$9,$2
 # 0 "" 2
#NO_APP
	lw	$6,332($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$4,360($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$2
 # 0 "" 2
#NO_APP
	lw	$5,364($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$2
 # 0 "" 2
#NO_APP
	li	$3,821755904			# 0x30fb0000
	ori	$3,$3,0xc540
#APP
 # 329 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr3,xr4,$9,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$20
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$20
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$20
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$20
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$9,300($16)
	subu	$13,$6,$7
	lw	$10,264($16)
	subu	$12,$5,$4
	lw	$8,268($16)
	addu	$6,$7,$6
	addu	$4,$5,$4
	lw	$7,296($16)
	subu	$5,$9,$13
	lw	$11,344($16)
	subu	$14,$10,$6
	addu	$9,$9,$13
	addu	$6,$10,$6
	sw	$5,364($16)
	subu	$10,$7,$12
	sw	$14,328($16)
	subu	$5,$8,$4
	sw	$9,300($16)
	addu	$7,$7,$12
	sw	$6,264($16)
	addu	$4,$8,$4
	sw	$10,360($16)
	sw	$5,332($16)
	sw	$7,296($16)
	sw	$4,268($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$3
 # 0 "" 2
#NO_APP
	lw	$6,348($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,376($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,380($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$20
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$20
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$20
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$20
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,316($16)
	subu	$10,$4,$5
	lw	$8,280($16)
	subu	$9,$3,$2
	lw	$6,284($16)
	addu	$4,$5,$4
	addu	$2,$3,$2
	lw	$5,312($16)
	subu	$3,$7,$10
	subu	$11,$8,$4
	addu	$4,$8,$4
	addu	$7,$7,$10
	sw	$3,380($16)
	subu	$8,$5,$9
	sw	$11,344($16)
	subu	$3,$6,$2
	sw	$4,280($16)
	addu	$5,$5,$9
	sw	$7,316($16)
	addu	$2,$6,$2
	sw	$8,376($16)
	addiu	$4,$16,384
	sw	$3,348($16)
	sw	$5,312($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	sw	$2,284($16)
	.set	macro
	.set	reorder

	li	$6,2137128960			# 0x7f620000
	lw	$5,256($16)
	lw	$4,384($16)
	addiu	$6,$6,13952
	lw	$3,260($16)
	lw	$11,132($16)
	lw	$2,388($16)
	subu	$7,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$10,128($16)
	lw	$9,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,264($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,388($16)
	subu	$11,$10,$5
	sw	$13,256($16)
	subu	$3,$9,$2
	sw	$7,132($16)
	addu	$5,$5,$10
	sw	$4,0($16)
	addu	$2,$2,$9
	sw	$11,384($16)
	sw	$3,260($16)
	sw	$5,128($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,268($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,392($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,396($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,210436096			# 0xc8b0000
	ori	$2,$2,0xd360
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$20
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$20
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$20
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$20
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,140($16)
	subu	$6,$7,$5
	lw	$9,136($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,264($16)
	subu	$7,$9,$5
	addu	$4,$4,$11
	addu	$6,$6,$10
	sw	$3,396($16)
	addu	$5,$5,$9
	sw	$7,392($16)
	subu	$3,$8,$2
	sw	$4,8($16)
	addu	$2,$2,$8
	sw	$6,140($16)
	lui	$10,%hi(aac_cos_64+64)
	sw	$5,136($16)
	lui	$9,%hi(aac_cos_64)
	sw	$3,268($16)
	addiu	$11,$16,112
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_64+64)
	addiu	$9,$9,%lo(aac_cos_64)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$2,$16
$L18:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$6,256($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$3
 # 0 "" 2
#NO_APP
	lw	$12,260($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,384($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,388($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$20,2			# 0x2
	li	$21,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $12
 # 0 "" 2
#NO_APP
	lw	$6,0($2)
	subu	$15,$4,$5
	addu	$4,$5,$4
	lw	$24,132($2)
	lw	$25,128($2)
	subu	$14,$3,$12
	lw	$17,4($2)
	subu	$13,$6,$4
	addu	$6,$6,$4
	addu	$3,$3,$12
	lw	$12,264($2)
	sw	$13,256($2)
	subu	$22,$24,$15
	sw	$6,0($2)
	addu	$5,$15,$24
	subu	$6,$17,$3
	subu	$13,$25,$14
	sw	$22,388($2)
	addu	$3,$3,$17
	sw	$5,132($2)
	addu	$4,$14,$25
	sw	$6,260($2)
	sw	$13,384($2)
	sw	$3,4($2)
	sw	$4,128($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$3
 # 0 "" 2
#NO_APP
	lw	$6,268($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,392($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,396($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$17,8($2)
	addu	$5,$6,$12
	subu	$13,$12,$6
	lw	$24,140($2)
	lw	$15,136($2)
	subu	$6,$4,$3
	subu	$12,$17,$5
	lw	$14,12($2)
	addu	$5,$5,$17
	addu	$3,$4,$3
	subu	$17,$24,$13
	sw	$12,264($2)
	addu	$4,$13,$24
	sw	$5,8($2)
	subu	$12,$15,$6
	subu	$5,$14,$3
	sw	$17,396($2)
	addu	$6,$6,$15
	sw	$4,140($2)
	addu	$3,$3,$14
	sw	$12,392($2)
	sw	$5,268($2)
	sw	$6,136($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$11,$L18
	sw	$3,12($2)
	.set	macro
	.set	reorder

	addiu	$17,$16,512
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,640
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,704
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,640($16)
	lw	$4,704($16)
	addiu	$6,$6,24320
	lw	$3,644($16)
	lw	$11,580($16)
	lw	$2,708($16)
	subu	$7,$4,$5
	lw	$12,512($16)
	addu	$4,$5,$4
	lw	$10,576($16)
	lw	$9,516($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,648($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,708($16)
	subu	$11,$10,$5
	sw	$13,640($16)
	subu	$3,$9,$2
	sw	$7,580($16)
	addu	$5,$5,$10
	sw	$4,512($16)
	addu	$2,$2,$9
	sw	$11,704($16)
	sw	$3,644($16)
	sw	$5,576($16)
	sw	$2,516($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,652($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,712($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,716($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,520($16)
	addu	$4,$5,$7
	lw	$10,588($16)
	subu	$6,$7,$5
	lw	$9,584($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,524($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,648($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,716($16)
	addu	$5,$5,$9
	sw	$6,588($16)
	subu	$3,$8,$2
	sw	$7,712($16)
	addu	$2,$2,$8
	sw	$4,520($16)
	addiu	$10,$16,560
	sw	$5,584($16)
	li	$7,2			# 0x2
	sw	$3,652($16)
	li	$6,31			# 0x1f
	sw	$2,524($16)
	move	$9,$19
	move	$8,$18
$L19:
	addiu	$17,$17,16
	lw	$2,8($8)
	lw	$5,128($17)
	addiu	$8,$8,8
	addiu	$9,$9,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$11,132($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$11,$2
 # 0 "" 2
#NO_APP
	lw	$3,192($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,196($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($9)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$11,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$20,2			# 0x2
	li	$21,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $11
 # 0 "" 2
#NO_APP
	lw	$5,0($17)
	subu	$14,$3,$4
	addu	$3,$4,$3
	lw	$24,68($17)
	lw	$25,64($17)
	subu	$13,$2,$11
	lw	$15,4($17)
	subu	$12,$5,$3
	addu	$5,$5,$3
	addu	$2,$2,$11
	lw	$11,136($17)
	sw	$12,128($17)
	subu	$22,$24,$14
	sw	$5,0($17)
	addu	$4,$14,$24
	subu	$5,$15,$2
	subu	$12,$25,$13
	sw	$22,196($17)
	addu	$2,$2,$15
	sw	$4,68($17)
	addu	$3,$13,$25
	sw	$5,132($17)
	sw	$12,192($17)
	sw	$2,4($17)
	sw	$3,64($17)
	lw	$2,4($8)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$2
 # 0 "" 2
#NO_APP
	lw	$5,140($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,200($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,204($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $11
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$15,8($17)
	addu	$4,$5,$11
	subu	$12,$11,$5
	lw	$24,76($17)
	lw	$14,72($17)
	subu	$5,$3,$2
	subu	$11,$15,$4
	lw	$13,12($17)
	addu	$4,$4,$15
	addu	$2,$3,$2
	subu	$15,$24,$12
	sw	$11,136($17)
	addu	$3,$12,$24
	sw	$4,8($17)
	subu	$11,$14,$5
	subu	$4,$13,$2
	sw	$15,204($17)
	addu	$5,$5,$14
	sw	$3,76($17)
	addu	$2,$2,$13
	sw	$11,200($17)
	sw	$4,140($17)
	sw	$5,72($17)
	.set	noreorder
	.set	nomacro
	bne	$17,$10,$L19
	sw	$2,12($17)
	.set	macro
	.set	reorder

	addiu	$17,$16,768
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,896
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,960
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,896($16)
	lw	$4,960($16)
	addiu	$6,$6,24320
	lw	$3,900($16)
	lw	$11,836($16)
	lw	$2,964($16)
	subu	$7,$4,$5
	lw	$12,768($16)
	addu	$4,$5,$4
	lw	$10,832($16)
	lw	$9,772($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,904($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,964($16)
	subu	$11,$10,$5
	sw	$13,896($16)
	subu	$3,$9,$2
	sw	$7,836($16)
	addu	$5,$5,$10
	sw	$4,768($16)
	addu	$2,$2,$9
	sw	$11,960($16)
	sw	$3,900($16)
	sw	$5,832($16)
	sw	$2,772($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,908($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,968($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,972($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$20,$21
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,776($16)
	addu	$4,$5,$7
	lw	$10,844($16)
	subu	$6,$7,$5
	lw	$9,840($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,780($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,904($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,972($16)
	addu	$5,$5,$9
	sw	$6,844($16)
	subu	$3,$8,$2
	sw	$7,968($16)
	addu	$2,$2,$8
	sw	$4,776($16)
	addiu	$8,$16,816
	sw	$5,840($16)
	li	$7,2			# 0x2
	sw	$3,908($16)
	li	$6,31			# 0x1f
	sw	$2,780($16)
$L20:
	addiu	$17,$17,16
	lw	$2,8($18)
	lw	$5,128($17)
	addiu	$18,$18,8
	addiu	$19,$19,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$9,132($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$2
 # 0 "" 2
#NO_APP
	lw	$3,192($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,196($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($19)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$9,2			# 0x2
	li	$10,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $11
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$13,0($17)
	addu	$4,$5,$11
	lw	$20,68($17)
	subu	$12,$11,$5
	lw	$15,64($17)
	subu	$5,$3,$2
	lw	$14,4($17)
	subu	$21,$13,$4
	addu	$4,$13,$4
	lw	$11,136($17)
	addu	$2,$3,$2
	subu	$13,$20,$12
	sw	$21,128($17)
	addu	$3,$12,$20
	sw	$4,0($17)
	subu	$12,$15,$5
	subu	$4,$14,$2
	sw	$13,196($17)
	addu	$2,$2,$14
	sw	$3,68($17)
	addu	$5,$5,$15
	sw	$12,192($17)
	sw	$4,132($17)
	sw	$2,4($17)
	sw	$5,64($17)
	lw	$2,4($18)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$2
 # 0 "" 2
#NO_APP
	lw	$5,140($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,200($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,204($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($19)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $11
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$20,8($17)
	addu	$4,$5,$11
	subu	$12,$11,$5
	lw	$15,76($17)
	lw	$14,72($17)
	subu	$5,$3,$2
	subu	$11,$20,$4
	lw	$13,12($17)
	addu	$4,$4,$20
	addu	$2,$3,$2
	subu	$20,$15,$12
	sw	$11,136($17)
	addu	$3,$12,$15
	sw	$4,8($17)
	subu	$11,$14,$5
	subu	$4,$13,$2
	sw	$20,204($17)
	addu	$5,$5,$14
	sw	$3,76($17)
	addu	$2,$2,$13
	sw	$11,200($17)
	sw	$4,140($17)
	sw	$5,72($17)
	.set	noreorder
	.set	nomacro
	bne	$17,$8,$L20
	sw	$2,12($17)
	.set	macro
	.set	reorder

	lw	$5,512($16)
	li	$6,2144862208			# 0x7fd80000
	lw	$4,768($16)
	lw	$3,516($16)
	ori	$6,$6,0x8780
	lw	$13,260($16)
	lw	$2,772($16)
	subu	$7,$4,$5
	lw	$14,0($16)
	addu	$4,$5,$4
	lw	$12,256($16)
	lw	$11,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,520($16)
	subu	$3,$13,$7
	subu	$15,$14,$4
	addu	$7,$7,$13
	addu	$4,$4,$14
	sw	$3,772($16)
	subu	$13,$12,$5
	sw	$15,512($16)
	subu	$3,$11,$2
	sw	$7,260($16)
	addu	$5,$5,$12
	sw	$4,0($16)
	addu	$2,$2,$11
	sw	$13,768($16)
	sw	$3,516($16)
	sw	$5,256($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,524($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,776($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,780($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,105316352			# 0x6470000
	ori	$2,$2,0xd980
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,268($16)
	subu	$6,$7,$5
	lw	$9,264($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,520($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,780($16)
	addu	$5,$5,$9
	sw	$6,268($16)
	subu	$3,$8,$2
	sw	$7,776($16)
	addu	$2,$2,$8
	sw	$4,8($16)
	lui	$10,%hi(aac_cos_128+128)
	sw	$5,264($16)
	lui	$8,%hi(aac_cos_128)
	sw	$3,524($16)
	lui	$11,%hi(aac_cos_128+120)
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_128+128)
	addiu	$8,$8,%lo(aac_cos_128)
	addiu	$11,$11,%lo(aac_cos_128+120)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
$L21:
	addiu	$16,$16,16
	lw	$2,8($8)
	lw	$5,512($16)
	addiu	$8,$8,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$9,516($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$2
 # 0 "" 2
#NO_APP
	lw	$3,768($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,772($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,260($16)
	subu	$9,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$15,256($16)
	subu	$5,$3,$2
	lw	$14,4($16)
	addu	$2,$3,$2
	subu	$3,$17,$9
	lw	$13,520($16)
	subu	$18,$12,$4
	addu	$9,$9,$17
	addu	$4,$12,$4
	sw	$3,772($16)
	subu	$12,$15,$5
	sw	$18,512($16)
	subu	$3,$14,$2
	sw	$9,260($16)
	addu	$2,$2,$14
	sw	$4,0($16)
	addu	$5,$5,$15
	sw	$12,768($16)
	sw	$3,516($16)
	sw	$2,4($16)
	sw	$5,256($16)
	lw	$2,4($8)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$13,$2
 # 0 "" 2
#NO_APP
	lw	$5,524($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,776($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,780($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$13,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,8($16)
	subu	$9,$12,$5
	lw	$15,268($16)
	addu	$4,$5,$12
	lw	$14,264($16)
	subu	$5,$3,$2
	lw	$13,12($16)
	addu	$2,$3,$2
	subu	$12,$17,$4
	subu	$3,$15,$9
	addu	$4,$4,$17
	addu	$9,$9,$15
	sw	$12,520($16)
	sw	$3,780($16)
	subu	$12,$14,$5
	subu	$3,$13,$2
	sw	$4,8($16)
	addu	$5,$5,$14
	sw	$9,268($16)
	addu	$2,$2,$13
	sw	$12,776($16)
	sw	$3,524($16)
	sw	$5,264($16)
	.set	noreorder
	.set	nomacro
	bne	$8,$11,$L21
	sw	$2,12($16)
	.set	macro
	.set	reorder

	lw	$31,52($sp)
	lw	$22,48($sp)
	lw	$21,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,56
	.set	macro
	.set	reorder

	.end	aac_fft128
	.size	aac_fft128, .-aac_fft128
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft256
	.type	aac_fft256, @function
aac_fft256:
	.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
	.mask	0x80ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-64
	sw	$16,28($sp)
	move	$16,$4
	sw	$17,32($sp)
	addiu	$17,$16,1024
	sw	$31,60($sp)
	sw	$23,56($sp)
	sw	$22,52($sp)
	sw	$21,48($sp)
	sw	$20,44($sp)
	sw	$19,40($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft128
	.option	pic2
	sw	$18,36($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1152
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1216
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,1152($16)
	lw	$4,1216($16)
	addiu	$6,$6,24320
	lw	$3,1156($16)
	lw	$11,1092($16)
	lw	$2,1220($16)
	subu	$7,$4,$5
	lw	$12,1024($16)
	addu	$4,$5,$4
	lw	$10,1088($16)
	lw	$9,1028($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1160($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1220($16)
	subu	$11,$10,$5
	sw	$13,1152($16)
	subu	$3,$9,$2
	sw	$7,1092($16)
	addu	$5,$5,$10
	sw	$4,1024($16)
	addu	$2,$2,$9
	sw	$11,1216($16)
	sw	$3,1156($16)
	sw	$5,1088($16)
	sw	$2,1028($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1164($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1224($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1228($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1032($16)
	addu	$4,$5,$7
	lw	$10,1100($16)
	subu	$6,$7,$5
	lw	$9,1096($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1036($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1160($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	lui	$21,%hi(aac_cos_32+32)
	sw	$3,1228($16)
	lui	$20,%hi(aac_cos_32)
	sw	$6,1100($16)
	addu	$4,$4,$11
	sw	$7,1224($16)
	addu	$5,$5,$9
	subu	$3,$8,$2
	addu	$2,$2,$8
	sw	$4,1032($16)
	addiu	$21,$21,%lo(aac_cos_32+32)
	sw	$5,1096($16)
	addiu	$20,$20,%lo(aac_cos_32)
	sw	$3,1164($16)
	addiu	$11,$16,1072
	sw	$2,1036($16)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
	move	$2,$17
	move	$10,$21
	move	$9,$20
$L29:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$8,128($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$3
 # 0 "" 2
#NO_APP
	lw	$12,132($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,192($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,196($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$18,2			# 0x2
	li	$19,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $12
 # 0 "" 2
#NO_APP
	lw	$8,0($2)
	subu	$15,$4,$5
	addu	$4,$5,$4
	lw	$22,68($2)
	lw	$23,64($2)
	subu	$14,$3,$12
	lw	$24,4($2)
	subu	$13,$8,$4
	addu	$8,$8,$4
	addu	$3,$3,$12
	lw	$12,136($2)
	sw	$13,128($2)
	subu	$25,$22,$15
	sw	$8,0($2)
	addu	$5,$15,$22
	subu	$8,$24,$3
	subu	$13,$23,$14
	sw	$25,196($2)
	addu	$3,$3,$24
	sw	$5,68($2)
	addu	$4,$14,$23
	sw	$8,132($2)
	sw	$13,192($2)
	sw	$3,4($2)
	sw	$4,64($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$3
 # 0 "" 2
#NO_APP
	lw	$8,140($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$3
 # 0 "" 2
#NO_APP
	lw	$4,200($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,204($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $8
 # 0 "" 2
#NO_APP
	lw	$22,8($2)
	subu	$14,$12,$5
	lw	$23,76($2)
	addu	$4,$5,$12
	subu	$13,$3,$8
	lw	$24,72($2)
	lw	$15,12($2)
	addu	$3,$3,$8
	subu	$12,$22,$4
	subu	$8,$23,$14
	addu	$4,$4,$22
	addu	$5,$14,$23
	sw	$12,136($2)
	sw	$8,204($2)
	subu	$12,$24,$13
	subu	$8,$15,$3
	sw	$4,8($2)
	addu	$3,$3,$15
	sw	$5,76($2)
	addu	$4,$13,$24
	sw	$12,200($2)
	sw	$8,140($2)
	sw	$3,12($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$11,$L29
	sw	$4,72($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,1280
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,1408
	.set	macro
	.set	reorder

	li	$6,2137128960			# 0x7f620000
	lw	$5,1280($16)
	lw	$4,1408($16)
	addiu	$6,$6,13952
	lw	$3,1284($16)
	lw	$11,1156($16)
	lw	$2,1412($16)
	subu	$7,$4,$5
	lw	$12,1024($16)
	addu	$4,$5,$4
	lw	$10,1152($16)
	lw	$9,1028($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1288($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1412($16)
	subu	$11,$10,$5
	sw	$13,1280($16)
	subu	$3,$9,$2
	sw	$7,1156($16)
	addu	$5,$5,$10
	sw	$4,1024($16)
	addu	$2,$2,$9
	sw	$11,1408($16)
	sw	$3,1284($16)
	sw	$5,1152($16)
	sw	$2,1028($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1292($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1416($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1420($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,210436096			# 0xc8b0000
	ori	$2,$2,0xd360
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1032($16)
	addu	$4,$5,$7
	lw	$10,1164($16)
	subu	$6,$7,$5
	lw	$9,1160($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1036($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1288($16)
	lui	$19,%hi(aac_cos_64+64)
	subu	$7,$9,$5
	lui	$18,%hi(aac_cos_64)
	sw	$3,1420($16)
	addu	$4,$4,$11
	subu	$3,$8,$2
	sw	$7,1416($16)
	addu	$6,$6,$10
	addu	$5,$5,$9
	sw	$4,1032($16)
	addu	$2,$2,$8
	sw	$3,1292($16)
	addiu	$19,$19,%lo(aac_cos_64+64)
	sw	$6,1164($16)
	addiu	$18,$18,%lo(aac_cos_64)
	sw	$5,1160($16)
	addiu	$11,$16,1136
	sw	$2,1036($16)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$10,$19
	move	$9,$18
$L30:
	addiu	$17,$17,16
	lw	$2,8($9)
	lw	$5,256($17)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$6,260($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$3,384($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,388($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$22,2			# 0x2
	li	$23,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $6
 # 0 "" 2
#NO_APP
	lw	$13,0($17)
	addu	$5,$12,$3
	subu	$4,$3,$12
	lw	$14,132($17)
	lw	$3,128($17)
	subu	$24,$2,$6
	lw	$15,4($17)
	subu	$12,$13,$5
	addu	$5,$13,$5
	addu	$2,$2,$6
	lw	$6,264($17)
	subu	$13,$14,$4
	sw	$12,256($17)
	sw	$5,0($17)
	subu	$12,$3,$24
	subu	$5,$15,$2
	addu	$4,$4,$14
	sw	$13,388($17)
	addu	$2,$2,$15
	sw	$12,384($17)
	addu	$3,$24,$3
	sw	$5,260($17)
	sw	$4,132($17)
	sw	$2,4($17)
	sw	$3,128($17)
	lw	$2,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$2
 # 0 "" 2
#NO_APP
	lw	$5,268($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,392($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,396($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $5
 # 0 "" 2
#NO_APP
	lw	$14,8($17)
	subu	$13,$6,$4
	lw	$15,140($17)
	addu	$3,$4,$6
	subu	$12,$2,$5
	lw	$24,136($17)
	lw	$25,12($17)
	addu	$2,$2,$5
	subu	$6,$14,$3
	subu	$5,$15,$13
	addu	$3,$3,$14
	addu	$4,$13,$15
	sw	$6,264($17)
	sw	$5,396($17)
	subu	$6,$24,$12
	subu	$5,$25,$2
	sw	$3,8($17)
	addu	$2,$2,$25
	sw	$4,140($17)
	addu	$3,$12,$24
	sw	$6,392($17)
	sw	$5,268($17)
	sw	$2,12($17)
	.set	noreorder
	.set	nomacro
	bne	$17,$11,$L30
	sw	$3,136($17)
	.set	macro
	.set	reorder

	addiu	$17,$16,1536
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1664
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1728
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,1664($16)
	lw	$4,1728($16)
	addiu	$6,$6,24320
	lw	$3,1668($16)
	lw	$11,1604($16)
	lw	$2,1732($16)
	subu	$7,$4,$5
	lw	$12,1536($16)
	addu	$4,$5,$4
	lw	$10,1600($16)
	lw	$9,1540($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1672($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1732($16)
	subu	$11,$10,$5
	sw	$13,1664($16)
	subu	$3,$9,$2
	sw	$7,1604($16)
	addu	$5,$5,$10
	sw	$4,1536($16)
	addu	$2,$2,$9
	sw	$11,1728($16)
	sw	$3,1668($16)
	sw	$5,1600($16)
	sw	$2,1540($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1676($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1736($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1740($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1544($16)
	addu	$4,$5,$7
	lw	$10,1612($16)
	subu	$6,$7,$5
	lw	$9,1608($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1548($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1672($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,1740($16)
	addu	$5,$5,$9
	sw	$6,1612($16)
	subu	$3,$8,$2
	sw	$7,1736($16)
	addu	$2,$2,$8
	sw	$4,1544($16)
	addiu	$9,$16,1584
	sw	$5,1608($16)
	li	$7,2			# 0x2
	sw	$3,1676($16)
	li	$6,31			# 0x1f
	sw	$2,1548($16)
	move	$2,$17
$L31:
	addiu	$2,$2,16
	lw	$3,8($20)
	lw	$8,128($2)
	addiu	$20,$20,8
	addiu	$21,$21,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$3
 # 0 "" 2
#NO_APP
	lw	$10,132($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$10,$3
 # 0 "" 2
#NO_APP
	lw	$4,192($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,196($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($21)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$10,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$22,2			# 0x2
	li	$23,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $10
 # 0 "" 2
#NO_APP
	lw	$8,0($2)
	subu	$13,$4,$5
	addu	$4,$5,$4
	lw	$14,68($2)
	lw	$15,64($2)
	subu	$12,$3,$10
	lw	$24,4($2)
	subu	$11,$8,$4
	addu	$8,$8,$4
	addu	$3,$3,$10
	lw	$10,136($2)
	sw	$11,128($2)
	subu	$25,$14,$13
	sw	$8,0($2)
	addu	$5,$13,$14
	subu	$8,$24,$3
	subu	$11,$15,$12
	sw	$25,196($2)
	addu	$3,$3,$24
	sw	$5,68($2)
	addu	$4,$12,$15
	sw	$8,132($2)
	sw	$11,192($2)
	sw	$3,4($2)
	sw	$4,64($2)
	lw	$3,4($20)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$3
 # 0 "" 2
#NO_APP
	lw	$8,140($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$3
 # 0 "" 2
#NO_APP
	lw	$4,200($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,204($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($21)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $8
 # 0 "" 2
#NO_APP
	lw	$14,8($2)
	subu	$12,$10,$5
	lw	$15,76($2)
	addu	$4,$5,$10
	subu	$11,$3,$8
	lw	$24,72($2)
	lw	$13,12($2)
	addu	$3,$3,$8
	subu	$10,$14,$4
	subu	$8,$15,$12
	addu	$4,$4,$14
	addu	$5,$12,$15
	sw	$10,136($2)
	sw	$8,204($2)
	subu	$10,$24,$11
	subu	$8,$13,$3
	sw	$4,8($2)
	addu	$3,$3,$13
	sw	$5,76($2)
	addu	$4,$11,$24
	sw	$10,200($2)
	sw	$8,140($2)
	sw	$3,12($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$9,$L31
	sw	$4,72($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1792
	.set	macro
	.set	reorder

	addiu	$2,$16,1856
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	addiu	$2,$16,1888
#APP
 # 213 "aac_fft.c" 1
	S32LDI xr1,$2,0
 # 0 "" 2
 # 214 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 215 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 216 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 218 "aac_fft.c" 1
	D32ADD xr5,xr1,xr2,xr6,SA
 # 0 "" 2
 # 219 "aac_fft.c" 1
	D32ADD xr7,xr4,xr3,xr8,SA
 # 0 "" 2
 # 220 "aac_fft.c" 1
	D32ADD xr9,xr6,xr8,xr10,SA
 # 0 "" 2
 # 222 "aac_fft.c" 1
	S32SDI xr9,$2,-8
 # 0 "" 2
 # 223 "aac_fft.c" 1
	S32SDI xr10,$2,-16
 # 0 "" 2
 # 225 "aac_fft.c" 1
	S32LDI xr1,$2,4
 # 0 "" 2
 # 226 "aac_fft.c" 1
	S32LDI xr2,$2,8
 # 0 "" 2
 # 227 "aac_fft.c" 1
	S32LDI xr3,$2,8
 # 0 "" 2
 # 228 "aac_fft.c" 1
	S32LDI xr4,$2,8
 # 0 "" 2
 # 230 "aac_fft.c" 1
	D32ADD xr6,xr1,xr2,xr9,SA
 # 0 "" 2
 # 231 "aac_fft.c" 1
	D32ADD xr8,xr3,xr4,xr10,SA
 # 0 "" 2
 # 233 "aac_fft.c" 1
	D32ADD xr1,xr6,xr7,xr2,SA
 # 0 "" 2
 # 234 "aac_fft.c" 1
	D32ADD xr11,xr5,xr8,xr12,SA
 # 0 "" 2
 # 235 "aac_fft.c" 1
	D32ADD xr13,xr9,xr10,xr14,SA
 # 0 "" 2
 # 237 "aac_fft.c" 1
	S32SDI xr1,$2,0
 # 0 "" 2
 # 238 "aac_fft.c" 1
	S32SDI xr11,$2,-4
 # 0 "" 2
 # 239 "aac_fft.c" 1
	S32SDI xr13,$2,-4
 # 0 "" 2
 # 240 "aac_fft.c" 1
	S32SDI xr2,$2,-8
 # 0 "" 2
 # 241 "aac_fft.c" 1
	S32SDI xr12,$2,-4
 # 0 "" 2
 # 242 "aac_fft.c" 1
	S32SDI xr14,$2,-4
 # 0 "" 2
#NO_APP
	lw	$6,1856($16)
	li	$2,1518469120			# 0x5a820000
	lw	$5,1888($16)
	lw	$4,1860($16)
	addiu	$2,$2,31130
	lw	$3,1892($16)
	subu	$12,$5,$6
	lw	$9,1792($16)
	addu	$5,$6,$5
	lw	$6,1828($16)
	subu	$11,$4,$3
	lw	$7,1796($16)
	lw	$8,1824($16)
	addu	$3,$4,$3
	subu	$4,$6,$12
	lw	$10,1872($16)
	subu	$13,$9,$5
	addu	$6,$6,$12
	addu	$5,$9,$5
	sw	$4,1892($16)
	subu	$9,$8,$11
	sw	$13,1856($16)
	subu	$4,$7,$3
	sw	$6,1828($16)
	addu	$8,$8,$11
	sw	$5,1792($16)
	addu	$3,$7,$3
	sw	$9,1888($16)
	sw	$4,1860($16)
	sw	$8,1824($16)
	sw	$3,1796($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$2
 # 0 "" 2
#NO_APP
	lw	$5,1876($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,1904($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,1908($16)
#APP
 # 328 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 328 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,1844($16)
	subu	$11,$4,$5
	subu	$10,$3,$2
	lw	$6,1812($16)
	addu	$2,$3,$2
	lw	$8,1808($16)
	subu	$3,$7,$11
	lw	$9,1864($16)
	addu	$4,$5,$4
	lw	$5,1840($16)
	addu	$7,$7,$11
	sw	$3,1908($16)
	subu	$3,$6,$2
	addu	$2,$6,$2
	subu	$12,$8,$4
	sw	$7,1844($16)
	addu	$4,$8,$4
	sw	$3,1876($16)
	subu	$8,$5,$10
	sw	$2,1812($16)
	addu	$5,$5,$10
	sw	$12,1872($16)
	li	$2,1983971328			# 0x76410000
	sw	$4,1808($16)
	sw	$8,1904($16)
	sw	$5,1840($16)
	ori	$2,$2,0xaf00
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr1,xr2,$9,$2
 # 0 "" 2
#NO_APP
	lw	$6,1868($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$4,1896($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$2
 # 0 "" 2
#NO_APP
	lw	$5,1900($16)
#APP
 # 329 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$2
 # 0 "" 2
#NO_APP
	li	$3,821755904			# 0x30fb0000
	ori	$3,$3,0xc540
#APP
 # 329 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr3,xr4,$9,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 329 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$9,1836($16)
	subu	$13,$6,$7
	lw	$10,1800($16)
	subu	$12,$5,$4
	lw	$8,1804($16)
	addu	$6,$7,$6
	addu	$4,$5,$4
	lw	$7,1832($16)
	subu	$5,$9,$13
	lw	$11,1880($16)
	subu	$14,$10,$6
	addu	$9,$9,$13
	addu	$6,$10,$6
	sw	$5,1900($16)
	subu	$10,$7,$12
	sw	$14,1864($16)
	subu	$5,$8,$4
	sw	$9,1836($16)
	addu	$7,$7,$12
	sw	$6,1800($16)
	addu	$4,$8,$4
	sw	$10,1896($16)
	sw	$5,1868($16)
	sw	$7,1832($16)
	sw	$4,1804($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$3
 # 0 "" 2
#NO_APP
	lw	$6,1884($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,1912($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,1916($16)
#APP
 # 330 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$2
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 330 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,1852($16)
	subu	$10,$4,$5
	lw	$8,1816($16)
	subu	$9,$3,$2
	lw	$6,1820($16)
	addu	$4,$5,$4
	addu	$2,$3,$2
	lw	$5,1848($16)
	subu	$3,$7,$10
	subu	$11,$8,$4
	addu	$4,$8,$4
	addu	$7,$7,$10
	sw	$3,1916($16)
	subu	$8,$5,$9
	sw	$11,1880($16)
	subu	$3,$6,$2
	sw	$4,1816($16)
	addu	$5,$5,$9
	sw	$7,1852($16)
	addu	$2,$6,$2
	sw	$8,1912($16)
	addiu	$4,$16,1920
	sw	$3,1884($16)
	sw	$5,1848($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	sw	$2,1820($16)
	.set	macro
	.set	reorder

	li	$6,2137128960			# 0x7f620000
	lw	$5,1792($16)
	lw	$4,1920($16)
	addiu	$6,$6,13952
	lw	$3,1796($16)
	lw	$11,1668($16)
	lw	$2,1924($16)
	subu	$7,$4,$5
	lw	$12,1536($16)
	addu	$4,$5,$4
	lw	$10,1664($16)
	lw	$9,1540($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1800($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1924($16)
	subu	$11,$10,$5
	sw	$13,1792($16)
	subu	$3,$9,$2
	sw	$7,1668($16)
	addu	$5,$5,$10
	sw	$4,1536($16)
	addu	$2,$2,$9
	sw	$11,1920($16)
	sw	$3,1796($16)
	sw	$5,1664($16)
	sw	$2,1540($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1804($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1928($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1932($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,210436096			# 0xc8b0000
	ori	$2,$2,0xd360
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1544($16)
	addu	$4,$5,$7
	lw	$10,1676($16)
	subu	$6,$7,$5
	lw	$9,1672($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1548($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1800($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,1932($16)
	addu	$5,$5,$9
	sw	$6,1676($16)
	subu	$3,$8,$2
	sw	$7,1928($16)
	addu	$2,$2,$8
	sw	$4,1544($16)
	addiu	$8,$16,1648
	sw	$5,1672($16)
	li	$7,2			# 0x2
	sw	$3,1804($16)
	li	$6,31			# 0x1f
	sw	$2,1548($16)
$L32:
	addiu	$17,$17,16
	lw	$2,8($18)
	lw	$5,256($17)
	addiu	$18,$18,8
	addiu	$19,$19,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$9,260($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$2
 # 0 "" 2
#NO_APP
	lw	$3,384($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,388($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($19)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$9,2			# 0x2
	li	$10,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $11
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$13,0($17)
	addu	$4,$5,$11
	lw	$20,132($17)
	subu	$12,$11,$5
	lw	$15,128($17)
	subu	$5,$3,$2
	lw	$14,4($17)
	subu	$21,$13,$4
	addu	$4,$13,$4
	lw	$11,264($17)
	addu	$2,$3,$2
	subu	$13,$20,$12
	sw	$21,256($17)
	addu	$3,$12,$20
	sw	$4,0($17)
	subu	$12,$15,$5
	subu	$4,$14,$2
	sw	$13,388($17)
	addu	$2,$2,$14
	sw	$3,132($17)
	addu	$5,$5,$15
	sw	$12,384($17)
	sw	$4,260($17)
	sw	$2,4($17)
	sw	$5,128($17)
	lw	$2,4($18)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$2
 # 0 "" 2
#NO_APP
	lw	$5,268($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,392($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,396($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($19)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $11
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$20,8($17)
	addu	$4,$5,$11
	subu	$12,$11,$5
	lw	$15,140($17)
	lw	$14,136($17)
	subu	$5,$3,$2
	subu	$11,$20,$4
	lw	$13,12($17)
	addu	$4,$4,$20
	addu	$2,$3,$2
	subu	$20,$15,$12
	sw	$11,264($17)
	addu	$3,$12,$15
	sw	$4,8($17)
	subu	$11,$14,$5
	subu	$4,$13,$2
	sw	$20,396($17)
	addu	$5,$5,$14
	sw	$3,140($17)
	addu	$2,$2,$13
	sw	$11,392($17)
	sw	$4,268($17)
	sw	$5,136($17)
	.set	noreorder
	.set	nomacro
	bne	$17,$8,$L32
	sw	$2,12($17)
	.set	macro
	.set	reorder

	lw	$5,1024($16)
	li	$6,2146828288			# 0x7ff60000
	lw	$4,1536($16)
	lw	$3,1028($16)
	addiu	$6,$6,8576
	lw	$13,516($16)
	lw	$2,1540($16)
	subu	$7,$4,$5
	lw	$14,0($16)
	addu	$4,$5,$4
	lw	$12,512($16)
	lw	$11,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1032($16)
	subu	$3,$13,$7
	subu	$15,$14,$4
	addu	$7,$7,$13
	addu	$4,$4,$14
	sw	$3,1540($16)
	subu	$13,$12,$5
	sw	$15,1024($16)
	subu	$3,$11,$2
	sw	$7,516($16)
	addu	$5,$5,$12
	sw	$4,0($16)
	addu	$2,$2,$11
	sw	$13,1536($16)
	sw	$3,1028($16)
	sw	$5,512($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1036($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1544($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1548($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,52690944			# 0x3240000
	addiu	$2,$2,10944
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,524($16)
	subu	$6,$7,$5
	lw	$9,520($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1032($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,1548($16)
	addu	$5,$5,$9
	sw	$6,524($16)
	subu	$3,$8,$2
	sw	$7,1544($16)
	addu	$2,$2,$8
	sw	$4,8($16)
	lui	$10,%hi(aac_cos_256+256)
	sw	$5,520($16)
	lui	$8,%hi(aac_cos_256)
	sw	$3,1036($16)
	lui	$11,%hi(aac_cos_256+248)
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_256+256)
	addiu	$8,$8,%lo(aac_cos_256)
	addiu	$11,$11,%lo(aac_cos_256+248)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
$L33:
	addiu	$16,$16,16
	lw	$2,8($8)
	lw	$5,1024($16)
	addiu	$8,$8,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$9,1028($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$2
 # 0 "" 2
#NO_APP
	lw	$3,1536($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,1540($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,516($16)
	subu	$9,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$15,512($16)
	subu	$5,$3,$2
	lw	$14,4($16)
	addu	$2,$3,$2
	subu	$3,$17,$9
	lw	$13,1032($16)
	subu	$18,$12,$4
	addu	$9,$9,$17
	addu	$4,$12,$4
	sw	$3,1540($16)
	subu	$12,$15,$5
	sw	$18,1024($16)
	subu	$3,$14,$2
	sw	$9,516($16)
	addu	$2,$2,$14
	sw	$4,0($16)
	addu	$5,$5,$15
	sw	$12,1536($16)
	sw	$3,1028($16)
	sw	$2,4($16)
	sw	$5,512($16)
	lw	$2,4($8)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$13,$2
 # 0 "" 2
#NO_APP
	lw	$5,1036($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,1544($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,1548($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$13,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,8($16)
	subu	$9,$12,$5
	lw	$15,524($16)
	addu	$4,$5,$12
	lw	$14,520($16)
	subu	$5,$3,$2
	lw	$13,12($16)
	addu	$2,$3,$2
	subu	$12,$17,$4
	subu	$3,$15,$9
	addu	$4,$4,$17
	addu	$9,$9,$15
	sw	$12,1032($16)
	sw	$3,1548($16)
	subu	$12,$14,$5
	subu	$3,$13,$2
	sw	$4,8($16)
	addu	$5,$5,$14
	sw	$9,524($16)
	addu	$2,$2,$13
	sw	$12,1544($16)
	sw	$3,1036($16)
	sw	$5,520($16)
	.set	noreorder
	.set	nomacro
	bne	$8,$11,$L33
	sw	$2,12($16)
	.set	macro
	.set	reorder

	lw	$31,60($sp)
	lw	$23,56($sp)
	lw	$22,52($sp)
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

	.end	aac_fft256
	.size	aac_fft256, .-aac_fft256
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft1024
	.type	aac_fft1024, @function
aac_fft1024:
	.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
	.mask	0x800f0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-48
	sw	$16,28($sp)
	move	$16,$4
	sw	$31,44($sp)
	sw	$19,40($sp)
	sw	$18,36($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft256
	.option	pic2
	sw	$17,32($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft128
	.option	pic2
	addiu	$4,$16,2048
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft128
	.option	pic2
	addiu	$4,$16,3072
	.set	macro
	.set	reorder

	li	$6,2147287040			# 0x7ffd0000
	lw	$5,2048($16)
	lw	$4,3072($16)
	ori	$6,$6,0x8880
	lw	$3,2052($16)
	lw	$11,1028($16)
	lw	$2,3076($16)
	subu	$7,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$10,1024($16)
	lw	$9,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,2056($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,3076($16)
	subu	$11,$10,$5
	sw	$13,2048($16)
	subu	$3,$9,$2
	sw	$7,1028($16)
	addu	$5,$5,$10
	sw	$4,0($16)
	addu	$2,$2,$9
	sw	$11,3072($16)
	sw	$3,2052($16)
	sw	$5,1024($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,2060($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,3080($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,3084($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,26345472			# 0x1920000
	addiu	$2,$2,7456
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,1036($16)
	subu	$6,$7,$5
	lw	$9,1032($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,2056($16)
	subu	$7,$9,$5
	addu	$4,$4,$11
	addu	$6,$6,$10
	sw	$3,3084($16)
	addu	$5,$5,$9
	sw	$7,3080($16)
	subu	$3,$8,$2
	sw	$4,8($16)
	addu	$2,$2,$8
	sw	$6,1036($16)
	lui	$10,%hi(aac_cos_512+512)
	sw	$5,1032($16)
	lui	$9,%hi(aac_cos_512)
	sw	$3,2060($16)
	lui	$11,%hi(aac_cos_512+504)
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_512+512)
	addiu	$9,$9,%lo(aac_cos_512)
	addiu	$11,$11,%lo(aac_cos_512+504)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$2,$16
$L41:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$6,2048($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$3
 # 0 "" 2
#NO_APP
	lw	$12,2052($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,3072($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,3076($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$17,2			# 0x2
	li	$18,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$25,1028($2)
	subu	$12,$5,$6
	lw	$13,0($2)
	addu	$5,$6,$5
	lw	$24,1024($2)
	subu	$6,$4,$3
	lw	$15,4($2)
	addu	$3,$4,$3
	subu	$4,$25,$12
	lw	$14,2056($2)
	subu	$19,$13,$5
	addu	$12,$12,$25
	addu	$5,$13,$5
	sw	$4,3076($2)
	subu	$13,$24,$6
	sw	$19,2048($2)
	subu	$4,$15,$3
	sw	$12,1028($2)
	addu	$3,$3,$15
	sw	$5,0($2)
	addu	$6,$6,$24
	sw	$13,3072($2)
	sw	$4,2052($2)
	sw	$3,4($2)
	sw	$6,1024($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$14,$3
 # 0 "" 2
#NO_APP
	lw	$6,2060($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,3080($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,3084($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$14,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $13
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$25,8($2)
	subu	$12,$13,$6
	lw	$24,1036($2)
	addu	$5,$6,$13
	lw	$15,1032($2)
	subu	$6,$4,$3
	lw	$14,12($2)
	addu	$3,$4,$3
	subu	$13,$25,$5
	subu	$4,$24,$12
	addu	$5,$5,$25
	addu	$12,$12,$24
	sw	$13,2056($2)
	sw	$4,3084($2)
	subu	$13,$15,$6
	subu	$4,$14,$3
	sw	$5,8($2)
	addu	$6,$6,$15
	sw	$12,1036($2)
	addu	$3,$3,$14
	sw	$13,3080($2)
	sw	$4,2060($2)
	sw	$6,1032($2)
	.set	noreorder
	.set	nomacro
	bne	$9,$11,$L41
	sw	$3,12($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft256
	.option	pic2
	addiu	$4,$16,4096
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft256
	.option	pic2
	addiu	$4,$16,6144
	.set	macro
	.set	reorder

	li	$2,2147418112			# 0x7fff0000
	lw	$6,4096($16)
	lw	$5,6144($16)
	addiu	$2,$2,25088
	lw	$4,4100($16)
	lw	$3,6148($16)
	subu	$12,$5,$6
	lw	$9,0($16)
	addu	$5,$6,$5
	lw	$6,2052($16)
	subu	$11,$4,$3
	lw	$7,4($16)
	lw	$8,2048($16)
	addu	$3,$4,$3
	subu	$4,$6,$12
	lw	$10,4104($16)
	subu	$13,$9,$5
	addu	$6,$6,$12
	addu	$5,$9,$5
	sw	$4,6148($16)
	subu	$9,$8,$11
	sw	$13,4096($16)
	subu	$4,$7,$3
	sw	$6,2052($16)
	addu	$8,$8,$11
	sw	$5,0($16)
	addu	$3,$7,$3
	sw	$9,6144($16)
	sw	$4,4100($16)
	sw	$8,2048($16)
	sw	$3,4($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$2
 # 0 "" 2
#NO_APP
	lw	$5,4108($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,6152($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,6156($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	li	$2,13172736			# 0xc90000
	addiu	$2,$2,3976
#APP
 # 187 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$7,2060($16)
	subu	$10,$4,$5
	lw	$8,8($16)
	addu	$4,$5,$4
	lw	$5,2056($16)
	subu	$9,$3,$2
	lw	$6,12($16)
	addu	$2,$3,$2
	subu	$3,$7,$10
	subu	$11,$8,$4
	addu	$4,$8,$4
	subu	$8,$5,$9
	sw	$3,6156($16)
	subu	$3,$6,$2
	sw	$11,4104($16)
	addu	$2,$6,$2
	sw	$4,8($16)
	addu	$7,$7,$10
	sw	$8,6152($16)
	addu	$5,$5,$9
	sw	$3,4108($16)
	lui	$12,%hi(aac_cos_1024+1024)
	sw	$2,12($16)
	lui	$8,%hi(aac_cos_1024)
	sw	$7,2060($16)
	lui	$13,%hi(aac_cos_1024+1016)
	sw	$5,2056($16)
	addiu	$12,$12,%lo(aac_cos_1024+1024)
	addiu	$8,$8,%lo(aac_cos_1024)
	addiu	$13,$13,%lo(aac_cos_1024+1016)
	li	$3,2			# 0x2
	li	$2,31			# 0x1f
$L42:
	addiu	$16,$16,16
	lw	$4,8($8)
	lw	$7,4096($16)
	addiu	$8,$8,8
	addiu	$12,$12,-8
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr1,xr2,$7,$4
 # 0 "" 2
#NO_APP
	lw	$9,4100($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$4
 # 0 "" 2
#NO_APP
	lw	$5,6144($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr7,xr8,$5,$4
 # 0 "" 2
#NO_APP
	lw	$6,6148($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr9,xr10,$6,$4
 # 0 "" 2
#NO_APP
	lw	$4,0($12)
#APP
 # 187 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MSUB xr3,xr4,$7,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MSUB xr7,xr8,$6,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MADD xr9,xr10,$5,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$10,2052($16)
	subu	$17,$6,$7
	lw	$11,0($16)
	subu	$15,$5,$4
	lw	$9,4($16)
	addu	$6,$7,$6
	addu	$4,$5,$4
	lw	$7,2048($16)
	subu	$5,$10,$17
	lw	$14,4104($16)
	subu	$18,$11,$6
	addu	$10,$10,$17
	addu	$6,$11,$6
	sw	$5,6148($16)
	subu	$11,$7,$15
	sw	$18,4096($16)
	subu	$5,$9,$4
	sw	$10,2052($16)
	addu	$4,$9,$4
	sw	$6,0($16)
	addu	$7,$7,$15
	sw	$11,6144($16)
	sw	$5,4100($16)
	sw	$4,4($16)
	sw	$7,2048($16)
	lw	$4,4($8)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr1,xr2,$14,$4
 # 0 "" 2
#NO_APP
	lw	$7,4108($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr3,xr4,$7,$4
 # 0 "" 2
#NO_APP
	lw	$5,6152($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr7,xr8,$5,$4
 # 0 "" 2
#NO_APP
	lw	$6,6156($16)
#APP
 # 187 "aac_fft.c" 1
	S32MUL xr9,xr10,$6,$4
 # 0 "" 2
#NO_APP
	lw	$4,-4($12)
#APP
 # 187 "aac_fft.c" 1
	S32MADD xr1,xr2,$7,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MSUB xr3,xr4,$14,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MSUB xr7,xr8,$6,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32MADD xr9,xr10,$5,$4
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$3,$2
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr5, $7
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr6, $5
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 187 "aac_fft.c" 1
	S32M2I xr12, $4
 # 0 "" 2
#NO_APP
	lw	$10,2060($16)
	subu	$15,$6,$7
	lw	$11,8($16)
	subu	$14,$5,$4
	lw	$9,12($16)
	addu	$6,$7,$6
	addu	$4,$5,$4
	lw	$7,2056($16)
	subu	$5,$10,$15
	subu	$17,$11,$6
	addu	$10,$10,$15
	addu	$6,$11,$6
	sw	$5,6156($16)
	subu	$11,$7,$14
	sw	$17,4104($16)
	subu	$5,$9,$4
	sw	$10,2060($16)
	addu	$7,$7,$14
	sw	$6,8($16)
	addu	$4,$9,$4
	sw	$11,6152($16)
	sw	$5,4108($16)
	sw	$7,2056($16)
	.set	noreorder
	.set	nomacro
	bne	$8,$13,$L42
	sw	$4,12($16)
	.set	macro
	.set	reorder

	lw	$31,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,48
	.set	macro
	.set	reorder

	.end	aac_fft1024
	.size	aac_fft1024, .-aac_fft1024
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft512
	.type	aac_fft512, @function
aac_fft512:
	.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
	.mask	0x80ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-64
	sw	$16,28($sp)
	move	$16,$4
	sw	$17,32($sp)
	addiu	$17,$16,1024
	sw	$31,60($sp)
	sw	$23,56($sp)
	sw	$22,52($sp)
	sw	$21,48($sp)
	sw	$20,44($sp)
	sw	$19,40($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft128
	.option	pic2
	sw	$18,36($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1152
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1216
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,1152($16)
	lw	$4,1216($16)
	addiu	$6,$6,24320
	lw	$3,1156($16)
	lw	$11,1092($16)
	lw	$2,1220($16)
	subu	$7,$4,$5
	lw	$12,1024($16)
	addu	$4,$5,$4
	lw	$10,1088($16)
	lw	$9,1028($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1160($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1220($16)
	subu	$11,$10,$5
	sw	$13,1152($16)
	subu	$3,$9,$2
	sw	$7,1092($16)
	addu	$5,$5,$10
	sw	$4,1024($16)
	addu	$2,$2,$9
	sw	$11,1216($16)
	sw	$3,1156($16)
	sw	$5,1088($16)
	sw	$2,1028($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1164($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1224($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1228($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$2,2			# 0x2
	li	$3,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$2,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1032($16)
	addu	$4,$5,$7
	lw	$10,1100($16)
	subu	$6,$7,$5
	lw	$9,1096($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1036($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1160($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	lui	$21,%hi(aac_cos_32+32)
	sw	$3,1228($16)
	lui	$20,%hi(aac_cos_32)
	sw	$6,1100($16)
	addu	$4,$4,$11
	sw	$7,1224($16)
	addu	$5,$5,$9
	subu	$3,$8,$2
	addu	$2,$2,$8
	sw	$4,1032($16)
	addiu	$21,$21,%lo(aac_cos_32+32)
	sw	$5,1096($16)
	addiu	$20,$20,%lo(aac_cos_32)
	sw	$3,1164($16)
	addiu	$11,$16,1072
	sw	$2,1036($16)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
	move	$2,$17
	move	$10,$21
	move	$9,$20
$L47:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$8,128($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$3
 # 0 "" 2
#NO_APP
	lw	$12,132($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,192($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,196($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$18,2			# 0x2
	li	$19,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $12
 # 0 "" 2
#NO_APP
	lw	$8,0($2)
	subu	$15,$4,$5
	addu	$4,$5,$4
	lw	$22,68($2)
	lw	$23,64($2)
	subu	$14,$3,$12
	lw	$24,4($2)
	subu	$13,$8,$4
	addu	$8,$8,$4
	addu	$3,$3,$12
	lw	$12,136($2)
	sw	$13,128($2)
	subu	$25,$22,$15
	sw	$8,0($2)
	addu	$5,$15,$22
	subu	$8,$24,$3
	subu	$13,$23,$14
	sw	$25,196($2)
	addu	$3,$3,$24
	sw	$5,68($2)
	addu	$4,$14,$23
	sw	$8,132($2)
	sw	$13,192($2)
	sw	$3,4($2)
	sw	$4,64($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$3
 # 0 "" 2
#NO_APP
	lw	$8,140($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$3
 # 0 "" 2
#NO_APP
	lw	$4,200($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,204($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $8
 # 0 "" 2
#NO_APP
	lw	$22,8($2)
	subu	$14,$12,$5
	lw	$23,76($2)
	addu	$4,$5,$12
	subu	$13,$3,$8
	lw	$24,72($2)
	lw	$15,12($2)
	addu	$3,$3,$8
	subu	$12,$22,$4
	subu	$8,$23,$14
	addu	$4,$4,$22
	addu	$5,$14,$23
	sw	$12,136($2)
	sw	$8,204($2)
	subu	$12,$24,$13
	subu	$8,$15,$3
	sw	$4,8($2)
	addu	$3,$3,$15
	sw	$5,76($2)
	addu	$4,$13,$24
	sw	$12,200($2)
	sw	$8,140($2)
	sw	$3,12($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$11,$L47
	sw	$4,72($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,1280
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,1408
	.set	macro
	.set	reorder

	li	$6,2137128960			# 0x7f620000
	lw	$5,1280($16)
	lw	$4,1408($16)
	addiu	$6,$6,13952
	lw	$3,1284($16)
	lw	$11,1156($16)
	lw	$2,1412($16)
	subu	$7,$4,$5
	lw	$12,1024($16)
	addu	$4,$5,$4
	lw	$10,1152($16)
	lw	$9,1028($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1288($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1412($16)
	subu	$11,$10,$5
	sw	$13,1280($16)
	subu	$3,$9,$2
	sw	$7,1156($16)
	addu	$5,$5,$10
	sw	$4,1024($16)
	addu	$2,$2,$9
	sw	$11,1408($16)
	sw	$3,1284($16)
	sw	$5,1152($16)
	sw	$2,1028($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1292($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1416($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1420($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,210436096			# 0xc8b0000
	ori	$2,$2,0xd360
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$18,$19
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1032($16)
	addu	$4,$5,$7
	lw	$10,1164($16)
	subu	$6,$7,$5
	lw	$9,1160($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1036($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1288($16)
	lui	$19,%hi(aac_cos_64+64)
	subu	$7,$9,$5
	lui	$18,%hi(aac_cos_64)
	sw	$3,1420($16)
	addu	$4,$4,$11
	subu	$3,$8,$2
	sw	$7,1416($16)
	addu	$6,$6,$10
	addu	$5,$5,$9
	sw	$4,1032($16)
	addu	$2,$2,$8
	sw	$3,1292($16)
	addiu	$19,$19,%lo(aac_cos_64+64)
	sw	$6,1164($16)
	addiu	$18,$18,%lo(aac_cos_64)
	sw	$5,1160($16)
	addiu	$11,$16,1136
	sw	$2,1036($16)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$10,$19
	move	$9,$18
$L48:
	addiu	$17,$17,16
	lw	$2,8($9)
	lw	$5,256($17)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$6,260($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$2
 # 0 "" 2
#NO_APP
	lw	$3,384($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,388($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$22,2			# 0x2
	li	$23,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $6
 # 0 "" 2
#NO_APP
	lw	$13,0($17)
	addu	$5,$12,$3
	subu	$4,$3,$12
	lw	$14,132($17)
	lw	$3,128($17)
	subu	$24,$2,$6
	lw	$15,4($17)
	subu	$12,$13,$5
	addu	$5,$13,$5
	addu	$2,$2,$6
	lw	$6,264($17)
	subu	$13,$14,$4
	sw	$12,256($17)
	sw	$5,0($17)
	subu	$12,$3,$24
	subu	$5,$15,$2
	addu	$4,$4,$14
	sw	$13,388($17)
	addu	$2,$2,$15
	sw	$12,384($17)
	addu	$3,$24,$3
	sw	$5,260($17)
	sw	$4,132($17)
	sw	$2,4($17)
	sw	$3,128($17)
	lw	$2,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$2
 # 0 "" 2
#NO_APP
	lw	$5,268($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,392($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,396($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $5
 # 0 "" 2
#NO_APP
	lw	$14,8($17)
	subu	$13,$6,$4
	lw	$15,140($17)
	addu	$3,$4,$6
	subu	$12,$2,$5
	lw	$24,136($17)
	lw	$25,12($17)
	addu	$2,$2,$5
	subu	$6,$14,$3
	subu	$5,$15,$13
	addu	$3,$3,$14
	addu	$4,$13,$15
	sw	$6,264($17)
	sw	$5,396($17)
	subu	$6,$24,$12
	subu	$5,$25,$2
	sw	$3,8($17)
	addu	$2,$2,$25
	sw	$4,140($17)
	addu	$3,$12,$24
	sw	$6,392($17)
	sw	$5,268($17)
	sw	$2,12($17)
	.set	noreorder
	.set	nomacro
	bne	$17,$11,$L48
	sw	$3,136($17)
	.set	macro
	.set	reorder

	addiu	$17,$16,1536
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1664
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft8
	.option	pic2
	addiu	$4,$16,1728
	.set	macro
	.set	reorder

	li	$6,2106195968			# 0x7d8a0000
	lw	$5,1664($16)
	lw	$4,1728($16)
	addiu	$6,$6,24320
	lw	$3,1668($16)
	lw	$11,1604($16)
	lw	$2,1732($16)
	subu	$7,$4,$5
	lw	$12,1536($16)
	addu	$4,$5,$4
	lw	$10,1600($16)
	lw	$9,1540($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1672($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1732($16)
	subu	$11,$10,$5
	sw	$13,1664($16)
	subu	$3,$9,$2
	sw	$7,1604($16)
	addu	$5,$5,$10
	sw	$4,1536($16)
	addu	$2,$2,$9
	sw	$11,1728($16)
	sw	$3,1668($16)
	sw	$5,1600($16)
	sw	$2,1540($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1676($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1736($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1740($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,418906112			# 0x18f80000
	ori	$2,$2,0xb840
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1544($16)
	addu	$4,$5,$7
	lw	$10,1612($16)
	subu	$6,$7,$5
	lw	$9,1608($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1548($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1672($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,1740($16)
	addu	$5,$5,$9
	sw	$6,1612($16)
	subu	$3,$8,$2
	sw	$7,1736($16)
	addu	$2,$2,$8
	sw	$4,1544($16)
	addiu	$9,$16,1584
	sw	$5,1608($16)
	li	$7,2			# 0x2
	sw	$3,1676($16)
	li	$6,31			# 0x1f
	sw	$2,1548($16)
	move	$2,$17
$L49:
	addiu	$2,$2,16
	lw	$3,8($20)
	lw	$8,128($2)
	addiu	$20,$20,8
	addiu	$21,$21,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$3
 # 0 "" 2
#NO_APP
	lw	$10,132($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$10,$3
 # 0 "" 2
#NO_APP
	lw	$4,192($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,196($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($21)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$10,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$22,2			# 0x2
	li	$23,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $10
 # 0 "" 2
#NO_APP
	lw	$8,0($2)
	subu	$13,$4,$5
	addu	$4,$5,$4
	lw	$14,68($2)
	lw	$15,64($2)
	subu	$12,$3,$10
	lw	$24,4($2)
	subu	$11,$8,$4
	addu	$8,$8,$4
	addu	$3,$3,$10
	lw	$10,136($2)
	sw	$11,128($2)
	subu	$25,$14,$13
	sw	$8,0($2)
	addu	$5,$13,$14
	subu	$8,$24,$3
	subu	$11,$15,$12
	sw	$25,196($2)
	addu	$3,$3,$24
	sw	$5,68($2)
	addu	$4,$12,$15
	sw	$8,132($2)
	sw	$11,192($2)
	sw	$3,4($2)
	sw	$4,64($2)
	lw	$3,4($20)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$10,$3
 # 0 "" 2
#NO_APP
	lw	$8,140($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$8,$3
 # 0 "" 2
#NO_APP
	lw	$4,200($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,204($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($21)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$8,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$10,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $8
 # 0 "" 2
#NO_APP
	lw	$14,8($2)
	subu	$12,$10,$5
	lw	$15,76($2)
	addu	$4,$5,$10
	subu	$11,$3,$8
	lw	$24,72($2)
	lw	$13,12($2)
	addu	$3,$3,$8
	subu	$10,$14,$4
	subu	$8,$15,$12
	addu	$4,$4,$14
	addu	$5,$12,$15
	sw	$10,136($2)
	sw	$8,204($2)
	subu	$10,$24,$11
	subu	$8,$13,$3
	sw	$4,8($2)
	addu	$3,$3,$13
	sw	$5,76($2)
	addu	$4,$11,$24
	sw	$10,200($2)
	sw	$8,140($2)
	sw	$3,12($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$9,$L49
	sw	$4,72($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,1792
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft16
	.option	pic2
	addiu	$4,$16,1920
	.set	macro
	.set	reorder

	li	$6,2137128960			# 0x7f620000
	lw	$5,1792($16)
	lw	$4,1920($16)
	addiu	$6,$6,13952
	lw	$3,1796($16)
	lw	$11,1668($16)
	lw	$2,1924($16)
	subu	$7,$4,$5
	lw	$12,1536($16)
	addu	$4,$5,$4
	lw	$10,1664($16)
	lw	$9,1540($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1800($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,1924($16)
	subu	$11,$10,$5
	sw	$13,1792($16)
	subu	$3,$9,$2
	sw	$7,1668($16)
	addu	$5,$5,$10
	sw	$4,1536($16)
	addu	$2,$2,$9
	sw	$11,1920($16)
	sw	$3,1796($16)
	sw	$5,1664($16)
	sw	$2,1540($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1804($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1928($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1932($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,210436096			# 0xc8b0000
	ori	$2,$2,0xd360
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$22,$23
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,1544($16)
	addu	$4,$5,$7
	lw	$10,1676($16)
	subu	$6,$7,$5
	lw	$9,1672($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,1548($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1800($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,1932($16)
	addu	$5,$5,$9
	sw	$6,1676($16)
	subu	$3,$8,$2
	sw	$7,1928($16)
	addu	$2,$2,$8
	sw	$4,1544($16)
	addiu	$8,$16,1648
	sw	$5,1672($16)
	li	$7,2			# 0x2
	sw	$3,1804($16)
	li	$6,31			# 0x1f
	sw	$2,1548($16)
$L50:
	addiu	$17,$17,16
	lw	$2,8($18)
	lw	$5,256($17)
	addiu	$18,$18,8
	addiu	$19,$19,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$9,260($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$2
 # 0 "" 2
#NO_APP
	lw	$3,384($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,388($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($19)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
#NO_APP
	li	$9,2			# 0x2
	li	$10,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $11
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$13,0($17)
	addu	$4,$5,$11
	lw	$20,132($17)
	subu	$12,$11,$5
	lw	$15,128($17)
	subu	$5,$3,$2
	lw	$14,4($17)
	subu	$21,$13,$4
	addu	$4,$13,$4
	lw	$11,264($17)
	addu	$2,$3,$2
	subu	$13,$20,$12
	sw	$21,256($17)
	addu	$3,$12,$20
	sw	$4,0($17)
	subu	$12,$15,$5
	subu	$4,$14,$2
	sw	$13,388($17)
	addu	$2,$2,$14
	sw	$3,132($17)
	addu	$5,$5,$15
	sw	$12,384($17)
	sw	$4,260($17)
	sw	$2,4($17)
	sw	$5,128($17)
	lw	$2,4($18)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$11,$2
 # 0 "" 2
#NO_APP
	lw	$5,268($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,392($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,396($17)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($19)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$11,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$20,8($17)
	subu	$11,$12,$5
	lw	$15,140($17)
	addu	$4,$5,$12
	lw	$14,136($17)
	subu	$5,$3,$2
	lw	$13,12($17)
	addu	$2,$3,$2
	subu	$12,$20,$4
	subu	$3,$15,$11
	addu	$4,$4,$20
	addu	$11,$11,$15
	sw	$12,264($17)
	sw	$3,396($17)
	subu	$12,$14,$5
	subu	$3,$13,$2
	sw	$4,8($17)
	addu	$5,$5,$14
	sw	$11,140($17)
	addu	$2,$2,$13
	sw	$12,392($17)
	sw	$3,268($17)
	sw	$5,136($17)
	.set	noreorder
	.set	nomacro
	bne	$17,$8,$L50
	sw	$2,12($17)
	.set	macro
	.set	reorder

	lw	$5,1024($16)
	li	$6,2146828288			# 0x7ff60000
	lw	$4,1536($16)
	lw	$3,1028($16)
	addiu	$6,$6,8576
	lw	$13,516($16)
	lw	$2,1540($16)
	subu	$7,$4,$5
	lw	$14,0($16)
	addu	$4,$5,$4
	lw	$12,512($16)
	lw	$11,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,1032($16)
	subu	$3,$13,$7
	subu	$15,$14,$4
	addu	$7,$7,$13
	addu	$4,$4,$14
	sw	$3,1540($16)
	subu	$13,$12,$5
	sw	$15,1024($16)
	subu	$3,$11,$2
	sw	$7,516($16)
	addu	$5,$5,$12
	sw	$4,0($16)
	addu	$2,$2,$11
	sw	$13,1536($16)
	sw	$3,1028($16)
	sw	$5,512($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,1036($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,1544($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,1548($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,52690944			# 0x3240000
	addiu	$2,$2,10944
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$9,$10
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,524($16)
	subu	$6,$7,$5
	lw	$9,520($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,1032($16)
	subu	$7,$9,$5
	addu	$4,$4,$11
	addu	$6,$6,$10
	sw	$3,1548($16)
	addu	$5,$5,$9
	sw	$7,1544($16)
	subu	$3,$8,$2
	sw	$4,8($16)
	addu	$2,$2,$8
	sw	$6,524($16)
	lui	$10,%hi(aac_cos_256+256)
	sw	$5,520($16)
	lui	$9,%hi(aac_cos_256)
	sw	$3,1036($16)
	lui	$11,%hi(aac_cos_256+248)
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_256+256)
	addiu	$9,$9,%lo(aac_cos_256)
	addiu	$11,$11,%lo(aac_cos_256+248)
	li	$8,2			# 0x2
	li	$7,31			# 0x1f
	move	$2,$16
$L51:
	addiu	$2,$2,16
	lw	$3,8($9)
	lw	$6,1024($2)
	addiu	$9,$9,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$6,$3
 # 0 "" 2
#NO_APP
	lw	$12,1028($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$12,$3
 # 0 "" 2
#NO_APP
	lw	$4,1536($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,1540($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
#NO_APP
	li	$17,2			# 0x2
	li	$18,31			# 0x1f
#APP
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$14,0($2)
	addu	$5,$6,$12
	lw	$20,516($2)
	subu	$13,$12,$6
	lw	$19,512($2)
	subu	$6,$4,$3
	lw	$15,4($2)
	subu	$21,$14,$5
	addu	$5,$14,$5
	lw	$12,1032($2)
	addu	$3,$4,$3
	subu	$14,$20,$13
	sw	$21,1024($2)
	addu	$4,$13,$20
	sw	$5,0($2)
	subu	$13,$19,$6
	subu	$5,$15,$3
	sw	$14,1540($2)
	addu	$3,$3,$15
	sw	$4,516($2)
	addu	$6,$6,$19
	sw	$13,1536($2)
	sw	$5,1028($2)
	sw	$3,4($2)
	sw	$6,512($2)
	lw	$3,4($9)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$12,$3
 # 0 "" 2
#NO_APP
	lw	$6,1036($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$6,$3
 # 0 "" 2
#NO_APP
	lw	$4,1544($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$4,$3
 # 0 "" 2
#NO_APP
	lw	$5,1548($2)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$5,$3
 # 0 "" 2
#NO_APP
	lw	$3,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$6,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$12,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$5,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$4,$3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$8,$7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $13
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $3
 # 0 "" 2
#NO_APP
	lw	$20,8($2)
	subu	$12,$13,$6
	lw	$19,524($2)
	addu	$5,$6,$13
	lw	$15,520($2)
	subu	$6,$4,$3
	lw	$14,12($2)
	addu	$3,$4,$3
	subu	$13,$20,$5
	subu	$4,$19,$12
	addu	$5,$5,$20
	addu	$12,$12,$19
	sw	$13,1032($2)
	sw	$4,1548($2)
	subu	$13,$15,$6
	subu	$4,$14,$3
	sw	$5,8($2)
	addu	$6,$6,$15
	sw	$12,524($2)
	addu	$3,$3,$14
	sw	$13,1544($2)
	sw	$4,1036($2)
	sw	$6,520($2)
	.set	noreorder
	.set	nomacro
	bne	$9,$11,$L51
	sw	$3,12($2)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft128
	.option	pic2
	addiu	$4,$16,2048
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	aac_fft128
	.option	pic2
	addiu	$4,$16,3072
	.set	macro
	.set	reorder

	li	$6,2147287040			# 0x7ffd0000
	lw	$5,2048($16)
	lw	$4,3072($16)
	ori	$6,$6,0x8880
	lw	$3,2052($16)
	lw	$11,1028($16)
	lw	$2,3076($16)
	subu	$7,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$10,1024($16)
	lw	$9,4($16)
	subu	$5,$3,$2
	addu	$2,$3,$2
	lw	$8,2056($16)
	subu	$3,$11,$7
	subu	$13,$12,$4
	addu	$7,$7,$11
	addu	$4,$4,$12
	sw	$3,3076($16)
	subu	$11,$10,$5
	sw	$13,2048($16)
	subu	$3,$9,$2
	sw	$7,1028($16)
	addu	$5,$5,$10
	sw	$4,0($16)
	addu	$2,$2,$9
	sw	$11,3072($16)
	sw	$3,2052($16)
	sw	$5,1024($16)
	sw	$2,4($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
#NO_APP
	lw	$5,2060($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$6
 # 0 "" 2
#NO_APP
	lw	$3,3080($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$6
 # 0 "" 2
#NO_APP
	lw	$4,3084($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$6
 # 0 "" 2
#NO_APP
	li	$2,26345472			# 0x1920000
	addiu	$2,$2,7456
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$8,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$17,$18
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $7
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$11,8($16)
	addu	$4,$5,$7
	lw	$10,1036($16)
	subu	$6,$7,$5
	lw	$9,1032($16)
	subu	$5,$3,$2
	subu	$7,$11,$4
	lw	$8,12($16)
	addu	$2,$3,$2
	subu	$3,$10,$6
	sw	$7,2056($16)
	addu	$6,$6,$10
	subu	$7,$9,$5
	addu	$4,$4,$11
	sw	$3,3084($16)
	addu	$5,$5,$9
	sw	$6,1036($16)
	subu	$3,$8,$2
	sw	$7,3080($16)
	addu	$2,$2,$8
	sw	$4,8($16)
	lui	$10,%hi(aac_cos_512+512)
	sw	$5,1032($16)
	lui	$8,%hi(aac_cos_512)
	sw	$3,2060($16)
	lui	$11,%hi(aac_cos_512+504)
	sw	$2,12($16)
	addiu	$10,$10,%lo(aac_cos_512+512)
	addiu	$8,$8,%lo(aac_cos_512)
	addiu	$11,$11,%lo(aac_cos_512+504)
	li	$7,2			# 0x2
	li	$6,31			# 0x1f
$L52:
	addiu	$16,$16,16
	lw	$2,8($8)
	lw	$5,2048($16)
	addiu	$8,$8,8
	addiu	$10,$10,-8
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$5,$2
 # 0 "" 2
#NO_APP
	lw	$9,2052($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$9,$2
 # 0 "" 2
#NO_APP
	lw	$3,3072($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,3076($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,0($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$9,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $4
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,1028($16)
	subu	$9,$4,$5
	lw	$12,0($16)
	addu	$4,$5,$4
	lw	$15,1024($16)
	subu	$5,$3,$2
	lw	$14,4($16)
	addu	$2,$3,$2
	subu	$3,$17,$9
	lw	$13,2056($16)
	subu	$18,$12,$4
	addu	$9,$9,$17
	addu	$4,$12,$4
	sw	$3,3076($16)
	subu	$12,$15,$5
	sw	$18,2048($16)
	subu	$3,$14,$2
	sw	$9,1028($16)
	addu	$2,$2,$14
	sw	$4,0($16)
	addu	$5,$5,$15
	sw	$12,3072($16)
	sw	$3,2052($16)
	sw	$2,4($16)
	sw	$5,1024($16)
	lw	$2,4($8)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr1,xr2,$13,$2
 # 0 "" 2
#NO_APP
	lw	$5,2060($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr3,xr4,$5,$2
 # 0 "" 2
#NO_APP
	lw	$3,3080($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr7,xr8,$3,$2
 # 0 "" 2
#NO_APP
	lw	$4,3084($16)
#APP
 # 184 "aac_fft.c" 1
	S32MUL xr9,xr10,$4,$2
 # 0 "" 2
#NO_APP
	lw	$2,-4($10)
#APP
 # 184 "aac_fft.c" 1
	S32MADD xr1,xr2,$5,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr3,xr4,$13,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MSUB xr7,xr8,$4,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32MADD xr9,xr10,$3,$2
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr1,xr2,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr3,xr4,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr7,xr8,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32EXTRV xr9,xr10,$7,$6
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr5,xr5,xr6,xr6,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	D32SAR xr11,xr11,xr12,xr12,1
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr5, $5
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr6, $3
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr11, $12
 # 0 "" 2
 # 184 "aac_fft.c" 1
	S32M2I xr12, $2
 # 0 "" 2
#NO_APP
	lw	$17,8($16)
	subu	$9,$12,$5
	lw	$15,1036($16)
	addu	$4,$5,$12
	lw	$14,1032($16)
	subu	$5,$3,$2
	lw	$13,12($16)
	addu	$2,$3,$2
	subu	$12,$17,$4
	subu	$3,$15,$9
	addu	$4,$4,$17
	addu	$9,$9,$15
	sw	$12,2056($16)
	sw	$3,3084($16)
	subu	$12,$14,$5
	subu	$3,$13,$2
	sw	$4,8($16)
	addu	$5,$5,$14
	sw	$9,1036($16)
	addu	$2,$2,$13
	sw	$12,3080($16)
	sw	$3,2060($16)
	sw	$5,1032($16)
	.set	noreorder
	.set	nomacro
	bne	$8,$11,$L52
	sw	$2,12($16)
	.set	macro
	.set	reorder

	lw	$31,60($sp)
	lw	$23,56($sp)
	lw	$22,52($sp)
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

	.end	aac_fft512
	.size	aac_fft512, .-aac_fft512
	.section	.text.unlikely,"ax",@progbits
	.align	2
	.globl	aac_fft_init
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft_init
	.type	aac_fft_init, @function
aac_fft_init:
	.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$2,$5,-2
	addiu	$28,$28,%lo(__gnu_local_gp)
	addiu	$sp,$sp,-48
	sltu	$2,$2,15
	sw	$19,36($sp)
	move	$19,$4
	sw	$18,32($sp)
	move	$18,$5
	sw	$31,44($sp)
	sw	$20,40($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	beq	$2,$0,$L61
	.cprestore	16

	li	$16,1			# 0x1
	lw	$25,%call16(av_malloc)($28)
	sw	$5,0($19)
	move	$20,$6
	sll	$16,$16,$5
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	sll	$4,$16,1

	lw	$28,16($sp)
	beq	$2,$0,$L61
	sw	$2,8($19)

	lw	$25,%call16(av_malloc)($28)
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	sll	$4,$16,3

	lw	$28,16($sp)
	beq	$2,$0,$L61
	sw	$2,12($19)

	lw	$2,%got(ff_fft_permute_c)($28)
	li	$17,4			# 0x4
	sw	$20,4($19)
	sw	$2,40($19)
	lw	$2,%got(ff_fft_calc_c)($28)
	sw	$2,44($19)
	lw	$2,%got(ff_imdct_calc_c)($28)
	sw	$2,48($19)
	lw	$2,%got(ff_imdct_half_c)($28)
	sw	$2,52($19)
	lw	$2,%got(ff_mdct_calc_c)($28)
	sw	$2,56($19)
$L62:
	slt	$2,$18,$17
	lw	$25,%call16(ff_init_ff_cos_tabs)($28)
	bne	$2,$0,$L79
	move	$4,$17

	.reloc	1f,R_MIPS_JALR,ff_init_ff_cos_tabs
1:	jalr	$25
	addiu	$17,$17,1

	.option	pic0
	j	$L62
	.option	pic2
	lw	$28,16($sp)

$L79:
	lw	$10,8($19)
	move	$5,$0
	lw	$8,4($19)
	addiu	$9,$16,-1
	move	$7,$0
$L81:
	li	$3,1			# 0x1
	move	$2,$16
	sra	$2,$2,1
$L80:
	and	$4,$5,$2
	bne	$4,$0,$L67
	sll	$6,$3,2

	sll	$3,$3,1
$L68:
	slt	$4,$2,3
	beq	$4,$0,$L80
	sra	$2,$2,1

	andi	$2,$5,0x1
	mul	$4,$2,$3
	addu	$2,$4,$7
	subu	$2,$0,$2
	and	$2,$2,$9
	sll	$2,$2,1
	addu	$2,$10,$2
	sh	$5,0($2)
	addiu	$5,$5,1
	bne	$5,$16,$L81
	move	$7,$0

	.option	pic0
	j	$L66
	.option	pic2
	move	$2,$0

$L67:
	sra	$2,$2,1
	and	$4,$5,$2
	sltu	$4,$4,1
	bne	$4,$8,$L69
	nop

	addu	$7,$7,$3
	.option	pic0
	j	$L68
	.option	pic2
	move	$3,$6

$L69:
	subu	$7,$7,$3
	.option	pic0
	j	$L68
	.option	pic2
	move	$3,$6

$L61:
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addiu	$4,$19,8

	lw	$28,16($sp)
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addiu	$4,$19,12

	li	$2,-1			# 0xffffffffffffffff
$L66:
	lw	$31,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,48

	.set	macro
	.set	reorder
	.end	aac_fft_init
	.size	aac_fft_init, .-aac_fft_init
	.text
	.align	2
	.globl	aac_fft_calc_c
	.set	nomips16
	.set	nomicromips
	.ent	aac_fft_calc_c
	.type	aac_fft_calc_c, @function
aac_fft_calc_c:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lw	$2,0($4)
	addiu	$2,$2,-2
	sll	$3,$2,2
	lui	$2,%hi(aac_fft_dispatch)
	addiu	$2,$2,%lo(aac_fft_dispatch)
	addu	$2,$3,$2
	lw	$25,0($2)
	jr	$25
	move	$4,$5

	.set	macro
	.set	reorder
	.end	aac_fft_calc_c
	.size	aac_fft_calc_c, .-aac_fft_calc_c
	.section	.data.rel.ro.local,"aw",@progbits
	.align	2
	.type	aac_fft_dispatch, @object
	.size	aac_fft_dispatch, 36
aac_fft_dispatch:
	.word	aac_fft4
	.word	aac_fft8
	.word	aac_fft16
	.word	aac_fft32
	.word	aac_fft64
	.word	aac_fft128
	.word	aac_fft256
	.word	aac_fft512
	.word	aac_fft1024
	.rdata
	.align	2
	.type	aac_cos_1024, @object
	.size	aac_cos_1024, 2048
aac_cos_1024:
	.word	2147483647
	.word	2147443200
	.word	2147321984
	.word	2147119872
	.word	2146836864
	.word	2146473088
	.word	2146028416
	.word	2145503104
	.word	2144896896
	.word	2144209920
	.word	2143442304
	.word	2142593920
	.word	2141664896
	.word	2140655232
	.word	2139565056
	.word	2138394240
	.word	2137142912
	.word	2135811200
	.word	2134398976
	.word	2132906368
	.word	2131333632
	.word	2129680512
	.word	2127947264
	.word	2126133760
	.word	2124240384
	.word	2122267008
	.word	2120213632
	.word	2118080512
	.word	2115867648
	.word	2113575040
	.word	2111202944
	.word	2108751360
	.word	2106220288
	.word	2103610112
	.word	2100920576
	.word	2098151936
	.word	2095304320
	.word	2092377856
	.word	2089372672
	.word	2086288768
	.word	2083126272
	.word	2079885312
	.word	2076566144
	.word	2073168768
	.word	2069693312
	.word	2066140032
	.word	2062508800
	.word	2058800000
	.word	2055013760
	.word	2051150080
	.word	2047209088
	.word	2043191168
	.word	2039096192
	.word	2034924544
	.word	2030676224
	.word	2026351488
	.word	2021950464
	.word	2017473280
	.word	2012920192
	.word	2008291328
	.word	2003586816
	.word	1998806784
	.word	1993951616
	.word	1989021312
	.word	1984016128
	.word	1978936320
	.word	1973782016
	.word	1968553344
	.word	1963250560
	.word	1957873792
	.word	1952423424
	.word	1946899456
	.word	1941302272
	.word	1935631872
	.word	1929888768
	.word	1924072832
	.word	1918184576
	.word	1912224128
	.word	1906191616
	.word	1900087296
	.word	1893911552
	.word	1887664384
	.word	1881346176
	.word	1874957184
	.word	1868497536
	.word	1861967616
	.word	1855367552
	.word	1848697728
	.word	1841958144
	.word	1835149312
	.word	1828271360
	.word	1821324544
	.word	1814309248
	.word	1807225600
	.word	1800073856
	.word	1792854400
	.word	1785567360
	.word	1778213248
	.word	1770792064
	.word	1763304192
	.word	1755750016
	.word	1748129664
	.word	1740443520
	.word	1732691968
	.word	1724875008
	.word	1716993152
	.word	1709046784
	.word	1701035904
	.word	1692961024
	.word	1684822400
	.word	1676620416
	.word	1668355328
	.word	1660027264
	.word	1651636864
	.word	1643184128
	.word	1634669696
	.word	1626093568
	.word	1617456384
	.word	1608758144
	.word	1599999360
	.word	1591180416
	.word	1582301568
	.word	1573363072
	.word	1564365312
	.word	1555308800
	.word	1546193664
	.word	1537020288
	.word	1527789056
	.word	1518500224
	.word	1509154304
	.word	1499751552
	.word	1490292352
	.word	1480777088
	.word	1471206016
	.word	1461579520
	.word	1451897984
	.word	1442161920
	.word	1432371456
	.word	1422527104
	.word	1412629120
	.word	1402678016
	.word	1392674048
	.word	1382617728
	.word	1372509312
	.word	1362349184
	.word	1352137856
	.word	1341875584
	.word	1331562752
	.word	1321199744
	.word	1310787072
	.word	1300325120
	.word	1289814016
	.word	1279254528
	.word	1268646784
	.word	1257991296
	.word	1247288448
	.word	1236538624
	.word	1225742336
	.word	1214899840
	.word	1204011520
	.word	1193078016
	.word	1182099456
	.word	1171076480
	.word	1160009344
	.word	1148898688
	.word	1137744640
	.word	1126547712
	.word	1115308544
	.word	1104027264
	.word	1092704384
	.word	1081340416
	.word	1069935744
	.word	1058490816
	.word	1047006016
	.word	1035481792
	.word	1023918528
	.word	1012316800
	.word	1000676928
	.word	988999360
	.word	977284544
	.word	965532992
	.word	953745024
	.word	941921216
	.word	930061888
	.word	918167552
	.word	906238656
	.word	894275648
	.word	882278976
	.word	870249088
	.word	858186432
	.word	846091456
	.word	833964608
	.word	821806400
	.word	809617280
	.word	797397632
	.word	785147904
	.word	772868736
	.word	760560384
	.word	748223424
	.word	735858304
	.word	723465472
	.word	711045376
	.word	698598528
	.word	686125376
	.word	673626432
	.word	661102080
	.word	648552832
	.word	635979200
	.word	623381568
	.word	610760512
	.word	598116480
	.word	585449920
	.word	572761280
	.word	560051072
	.word	547319808
	.word	534567968
	.word	521795968
	.word	509004320
	.word	496193504
	.word	483364032
	.word	470516320
	.word	457650912
	.word	444768288
	.word	431868928
	.word	418953280
	.word	406021856
	.word	393075168
	.word	380113664
	.word	367137856
	.word	354148224
	.word	341145280
	.word	328129472
	.word	315101280
	.word	302061280
	.word	289009856
	.word	275947584
	.word	262874928
	.word	249792352
	.word	236700384
	.word	223599504
	.word	210490208
	.word	197372976
	.word	184248320
	.word	171116736
	.word	157978704
	.word	144834720
	.word	131685280
	.word	118530888
	.word	105372032
	.word	92209208
	.word	79042912
	.word	65873640
	.word	52701888
	.word	39528152
	.word	26352928
	.word	13176712
	.word	0
	.word	13176712
	.word	26352928
	.word	39528152
	.word	52701888
	.word	65873640
	.word	79042912
	.word	92209208
	.word	105372032
	.word	118530888
	.word	131685280
	.word	144834720
	.word	157978704
	.word	171116736
	.word	184248320
	.word	197372976
	.word	210490208
	.word	223599504
	.word	236700384
	.word	249792352
	.word	262874928
	.word	275947584
	.word	289009856
	.word	302061280
	.word	315101280
	.word	328129472
	.word	341145280
	.word	354148224
	.word	367137856
	.word	380113664
	.word	393075168
	.word	406021856
	.word	418953280
	.word	431868928
	.word	444768288
	.word	457650912
	.word	470516320
	.word	483364032
	.word	496193504
	.word	509004320
	.word	521795968
	.word	534567968
	.word	547319808
	.word	560051072
	.word	572761280
	.word	585449920
	.word	598116480
	.word	610760512
	.word	623381568
	.word	635979200
	.word	648552832
	.word	661102080
	.word	673626432
	.word	686125376
	.word	698598528
	.word	711045376
	.word	723465472
	.word	735858304
	.word	748223424
	.word	760560384
	.word	772868736
	.word	785147904
	.word	797397632
	.word	809617280
	.word	821806400
	.word	833964608
	.word	846091456
	.word	858186432
	.word	870249088
	.word	882278976
	.word	894275648
	.word	906238656
	.word	918167552
	.word	930061888
	.word	941921216
	.word	953745024
	.word	965532992
	.word	977284544
	.word	988999360
	.word	1000676928
	.word	1012316800
	.word	1023918528
	.word	1035481792
	.word	1047006016
	.word	1058490816
	.word	1069935744
	.word	1081340416
	.word	1092704384
	.word	1104027264
	.word	1115308544
	.word	1126547712
	.word	1137744640
	.word	1148898688
	.word	1160009344
	.word	1171076480
	.word	1182099456
	.word	1193078016
	.word	1204011520
	.word	1214899840
	.word	1225742336
	.word	1236538624
	.word	1247288448
	.word	1257991296
	.word	1268646784
	.word	1279254528
	.word	1289814016
	.word	1300325120
	.word	1310787072
	.word	1321199744
	.word	1331562752
	.word	1341875584
	.word	1352137856
	.word	1362349184
	.word	1372509312
	.word	1382617728
	.word	1392674048
	.word	1402678016
	.word	1412629120
	.word	1422527104
	.word	1432371456
	.word	1442161920
	.word	1451897984
	.word	1461579520
	.word	1471206016
	.word	1480777088
	.word	1490292352
	.word	1499751552
	.word	1509154304
	.word	1518500224
	.word	1527789056
	.word	1537020288
	.word	1546193664
	.word	1555308800
	.word	1564365312
	.word	1573363072
	.word	1582301568
	.word	1591180416
	.word	1599999360
	.word	1608758144
	.word	1617456384
	.word	1626093568
	.word	1634669696
	.word	1643184128
	.word	1651636864
	.word	1660027264
	.word	1668355328
	.word	1676620416
	.word	1684822400
	.word	1692961024
	.word	1701035904
	.word	1709046784
	.word	1716993152
	.word	1724875008
	.word	1732691968
	.word	1740443520
	.word	1748129664
	.word	1755750016
	.word	1763304192
	.word	1770792064
	.word	1778213248
	.word	1785567360
	.word	1792854400
	.word	1800073856
	.word	1807225600
	.word	1814309248
	.word	1821324544
	.word	1828271360
	.word	1835149312
	.word	1841958144
	.word	1848697728
	.word	1855367552
	.word	1861967616
	.word	1868497536
	.word	1874957184
	.word	1881346176
	.word	1887664384
	.word	1893911552
	.word	1900087296
	.word	1906191616
	.word	1912224128
	.word	1918184576
	.word	1924072832
	.word	1929888768
	.word	1935631872
	.word	1941302272
	.word	1946899456
	.word	1952423424
	.word	1957873792
	.word	1963250560
	.word	1968553344
	.word	1973782016
	.word	1978936320
	.word	1984016128
	.word	1989021312
	.word	1993951616
	.word	1998806784
	.word	2003586816
	.word	2008291328
	.word	2012920192
	.word	2017473280
	.word	2021950464
	.word	2026351488
	.word	2030676224
	.word	2034924544
	.word	2039096192
	.word	2043191168
	.word	2047209088
	.word	2051150080
	.word	2055013760
	.word	2058800000
	.word	2062508800
	.word	2066140032
	.word	2069693312
	.word	2073168768
	.word	2076566144
	.word	2079885312
	.word	2083126272
	.word	2086288768
	.word	2089372672
	.word	2092377856
	.word	2095304320
	.word	2098151936
	.word	2100920576
	.word	2103610112
	.word	2106220288
	.word	2108751360
	.word	2111202944
	.word	2113575040
	.word	2115867648
	.word	2118080512
	.word	2120213632
	.word	2122267008
	.word	2124240384
	.word	2126133760
	.word	2127947264
	.word	2129680512
	.word	2131333632
	.word	2132906368
	.word	2134398976
	.word	2135811200
	.word	2137142912
	.word	2138394240
	.word	2139565056
	.word	2140655232
	.word	2141664896
	.word	2142593920
	.word	2143442304
	.word	2144209920
	.word	2144896896
	.word	2145503104
	.word	2146028416
	.word	2146473088
	.word	2146836864
	.word	2147119872
	.word	2147321984
	.word	2147443200
	.align	2
	.type	aac_cos_512, @object
	.size	aac_cos_512, 1024
aac_cos_512:
	.word	2147483647
	.word	2147321984
	.word	2146836864
	.word	2146028416
	.word	2144896896
	.word	2143442304
	.word	2141664896
	.word	2139565056
	.word	2137142912
	.word	2134398976
	.word	2131333632
	.word	2127947264
	.word	2124240384
	.word	2120213632
	.word	2115867648
	.word	2111202944
	.word	2106220288
	.word	2100920576
	.word	2095304320
	.word	2089372672
	.word	2083126272
	.word	2076566144
	.word	2069693312
	.word	2062508800
	.word	2055013760
	.word	2047209088
	.word	2039096192
	.word	2030676224
	.word	2021950464
	.word	2012920192
	.word	2003586816
	.word	1993951616
	.word	1984016128
	.word	1973782016
	.word	1963250560
	.word	1952423424
	.word	1941302272
	.word	1929888768
	.word	1918184576
	.word	1906191616
	.word	1893911552
	.word	1881346176
	.word	1868497536
	.word	1855367552
	.word	1841958144
	.word	1828271360
	.word	1814309248
	.word	1800073856
	.word	1785567360
	.word	1770792064
	.word	1755750016
	.word	1740443520
	.word	1724875008
	.word	1709046784
	.word	1692961024
	.word	1676620416
	.word	1660027264
	.word	1643184128
	.word	1626093568
	.word	1608758144
	.word	1591180416
	.word	1573363072
	.word	1555308800
	.word	1537020288
	.word	1518500224
	.word	1499751552
	.word	1480777088
	.word	1461579520
	.word	1442161920
	.word	1422527104
	.word	1402678016
	.word	1382617728
	.word	1362349184
	.word	1341875584
	.word	1321199744
	.word	1300325120
	.word	1279254528
	.word	1257991296
	.word	1236538624
	.word	1214899840
	.word	1193078016
	.word	1171076480
	.word	1148898688
	.word	1126547712
	.word	1104027264
	.word	1081340416
	.word	1058490816
	.word	1035481792
	.word	1012316800
	.word	988999360
	.word	965532992
	.word	941921216
	.word	918167552
	.word	894275648
	.word	870249088
	.word	846091456
	.word	821806400
	.word	797397632
	.word	772868736
	.word	748223424
	.word	723465472
	.word	698598528
	.word	673626432
	.word	648552832
	.word	623381568
	.word	598116480
	.word	572761280
	.word	547319808
	.word	521795968
	.word	496193504
	.word	470516320
	.word	444768288
	.word	418953280
	.word	393075168
	.word	367137856
	.word	341145280
	.word	315101280
	.word	289009856
	.word	262874928
	.word	236700384
	.word	210490208
	.word	184248320
	.word	157978704
	.word	131685280
	.word	105372032
	.word	79042912
	.word	52701888
	.word	26352928
	.word	0
	.word	26352928
	.word	52701888
	.word	79042912
	.word	105372032
	.word	131685280
	.word	157978704
	.word	184248320
	.word	210490208
	.word	236700384
	.word	262874928
	.word	289009856
	.word	315101280
	.word	341145280
	.word	367137856
	.word	393075168
	.word	418953280
	.word	444768288
	.word	470516320
	.word	496193504
	.word	521795968
	.word	547319808
	.word	572761280
	.word	598116480
	.word	623381568
	.word	648552832
	.word	673626432
	.word	698598528
	.word	723465472
	.word	748223424
	.word	772868736
	.word	797397632
	.word	821806400
	.word	846091456
	.word	870249088
	.word	894275648
	.word	918167552
	.word	941921216
	.word	965532992
	.word	988999360
	.word	1012316800
	.word	1035481792
	.word	1058490816
	.word	1081340416
	.word	1104027264
	.word	1126547712
	.word	1148898688
	.word	1171076480
	.word	1193078016
	.word	1214899840
	.word	1236538624
	.word	1257991296
	.word	1279254528
	.word	1300325120
	.word	1321199744
	.word	1341875584
	.word	1362349184
	.word	1382617728
	.word	1402678016
	.word	1422527104
	.word	1442161920
	.word	1461579520
	.word	1480777088
	.word	1499751552
	.word	1518500224
	.word	1537020288
	.word	1555308800
	.word	1573363072
	.word	1591180416
	.word	1608758144
	.word	1626093568
	.word	1643184128
	.word	1660027264
	.word	1676620416
	.word	1692961024
	.word	1709046784
	.word	1724875008
	.word	1740443520
	.word	1755750016
	.word	1770792064
	.word	1785567360
	.word	1800073856
	.word	1814309248
	.word	1828271360
	.word	1841958144
	.word	1855367552
	.word	1868497536
	.word	1881346176
	.word	1893911552
	.word	1906191616
	.word	1918184576
	.word	1929888768
	.word	1941302272
	.word	1952423424
	.word	1963250560
	.word	1973782016
	.word	1984016128
	.word	1993951616
	.word	2003586816
	.word	2012920192
	.word	2021950464
	.word	2030676224
	.word	2039096192
	.word	2047209088
	.word	2055013760
	.word	2062508800
	.word	2069693312
	.word	2076566144
	.word	2083126272
	.word	2089372672
	.word	2095304320
	.word	2100920576
	.word	2106220288
	.word	2111202944
	.word	2115867648
	.word	2120213632
	.word	2124240384
	.word	2127947264
	.word	2131333632
	.word	2134398976
	.word	2137142912
	.word	2139565056
	.word	2141664896
	.word	2143442304
	.word	2144896896
	.word	2146028416
	.word	2146836864
	.word	2147321984
	.align	2
	.type	aac_cos_256, @object
	.size	aac_cos_256, 512
aac_cos_256:
	.word	2147483647
	.word	2146836864
	.word	2144896896
	.word	2141664896
	.word	2137142912
	.word	2131333632
	.word	2124240384
	.word	2115867648
	.word	2106220288
	.word	2095304320
	.word	2083126272
	.word	2069693312
	.word	2055013760
	.word	2039096192
	.word	2021950464
	.word	2003586816
	.word	1984016128
	.word	1963250560
	.word	1941302272
	.word	1918184576
	.word	1893911552
	.word	1868497536
	.word	1841958144
	.word	1814309248
	.word	1785567360
	.word	1755750016
	.word	1724875008
	.word	1692961024
	.word	1660027264
	.word	1626093568
	.word	1591180416
	.word	1555308800
	.word	1518500224
	.word	1480777088
	.word	1442161920
	.word	1402678016
	.word	1362349184
	.word	1321199744
	.word	1279254528
	.word	1236538624
	.word	1193078016
	.word	1148898688
	.word	1104027264
	.word	1058490816
	.word	1012316800
	.word	965532992
	.word	918167552
	.word	870249088
	.word	821806400
	.word	772868736
	.word	723465472
	.word	673626432
	.word	623381568
	.word	572761280
	.word	521795968
	.word	470516320
	.word	418953280
	.word	367137856
	.word	315101280
	.word	262874928
	.word	210490208
	.word	157978704
	.word	105372032
	.word	52701888
	.word	0
	.word	52701888
	.word	105372032
	.word	157978704
	.word	210490208
	.word	262874928
	.word	315101280
	.word	367137856
	.word	418953280
	.word	470516320
	.word	521795968
	.word	572761280
	.word	623381568
	.word	673626432
	.word	723465472
	.word	772868736
	.word	821806400
	.word	870249088
	.word	918167552
	.word	965532992
	.word	1012316800
	.word	1058490816
	.word	1104027264
	.word	1148898688
	.word	1193078016
	.word	1236538624
	.word	1279254528
	.word	1321199744
	.word	1362349184
	.word	1402678016
	.word	1442161920
	.word	1480777088
	.word	1518500224
	.word	1555308800
	.word	1591180416
	.word	1626093568
	.word	1660027264
	.word	1692961024
	.word	1724875008
	.word	1755750016
	.word	1785567360
	.word	1814309248
	.word	1841958144
	.word	1868497536
	.word	1893911552
	.word	1918184576
	.word	1941302272
	.word	1963250560
	.word	1984016128
	.word	2003586816
	.word	2021950464
	.word	2039096192
	.word	2055013760
	.word	2069693312
	.word	2083126272
	.word	2095304320
	.word	2106220288
	.word	2115867648
	.word	2124240384
	.word	2131333632
	.word	2137142912
	.word	2141664896
	.word	2144896896
	.word	2146836864
	.align	2
	.type	aac_cos_128, @object
	.size	aac_cos_128, 256
aac_cos_128:
	.word	2147483647
	.word	2144896896
	.word	2137142912
	.word	2124240384
	.word	2106220288
	.word	2083126272
	.word	2055013760
	.word	2021950464
	.word	1984016128
	.word	1941302272
	.word	1893911552
	.word	1841958144
	.word	1785567360
	.word	1724875008
	.word	1660027264
	.word	1591180416
	.word	1518500224
	.word	1442161920
	.word	1362349184
	.word	1279254528
	.word	1193078016
	.word	1104027264
	.word	1012316800
	.word	918167552
	.word	821806400
	.word	723465472
	.word	623381568
	.word	521795968
	.word	418953280
	.word	315101280
	.word	210490208
	.word	105372032
	.word	0
	.word	105372032
	.word	210490208
	.word	315101280
	.word	418953280
	.word	521795968
	.word	623381568
	.word	723465472
	.word	821806400
	.word	918167552
	.word	1012316800
	.word	1104027264
	.word	1193078016
	.word	1279254528
	.word	1362349184
	.word	1442161920
	.word	1518500224
	.word	1591180416
	.word	1660027264
	.word	1724875008
	.word	1785567360
	.word	1841958144
	.word	1893911552
	.word	1941302272
	.word	1984016128
	.word	2021950464
	.word	2055013760
	.word	2083126272
	.word	2106220288
	.word	2124240384
	.word	2137142912
	.word	2144896896
	.align	2
	.type	aac_cos_64, @object
	.size	aac_cos_64, 128
aac_cos_64:
	.word	2147483647
	.word	2137142912
	.word	2106220288
	.word	2055013760
	.word	1984016128
	.word	1893911552
	.word	1785567360
	.word	1660027264
	.word	1518500224
	.word	1362349184
	.word	1193078016
	.word	1012316800
	.word	821806400
	.word	623381568
	.word	418953280
	.word	210490208
	.word	0
	.word	210490208
	.word	418953280
	.word	623381568
	.word	821806400
	.word	1012316800
	.word	1193078016
	.word	1362349184
	.word	1518500224
	.word	1660027264
	.word	1785567360
	.word	1893911552
	.word	1984016128
	.word	2055013760
	.word	2106220288
	.word	2137142912
	.align	2
	.type	aac_cos_32, @object
	.size	aac_cos_32, 64
aac_cos_32:
	.word	2147483647
	.word	2106220288
	.word	1984016128
	.word	1785567360
	.word	1518500224
	.word	1193078016
	.word	821806400
	.word	418953280
	.word	0
	.word	418953280
	.word	821806400
	.word	1193078016
	.word	1518500224
	.word	1785567360
	.word	1984016128
	.word	2106220288
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
