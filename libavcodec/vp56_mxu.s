.file	1 "vp56.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vp56_get_vectors_predictors
.type	vp56_get_vectors_predictors, @function
vp56_get_vectors_predictors:
.frame	$sp,16,$31		# vars= 8, regs= 0/0, args= 0, gp= 8
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-16
lui	$9,%hi(vp56_candidate_predictor_pos)
lui	$14,%hi(vp56_reference_frame)
sw	$0,8($sp)
addiu	$9,$9,%lo(vp56_candidate_predictor_pos)
sw	$0,12($sp)
move	$10,$0
move	$2,$0
addiu	$14,$14,%lo(vp56_reference_frame)
addiu	$24,$sp,8
li	$15,1			# 0x1
li	$13,12			# 0xc
andi	$6,$6,0xffff
andi	$5,$5,0xffff
$L4:
lb	$3,0($9)
addiu	$9,$9,2
addu	$3,$6,$3
sll	$3,$3,16
sra	$3,$3,16
bltz	$3,$L2
lb	$8,-1($9)

lw	$11,5248($4)
slt	$12,$3,$11
beq	$12,$0,$L2
addu	$8,$5,$8

sll	$8,$8,16
sra	$8,$8,16
bltz	$8,$L2
mul	$12,$11,$8

lw	$11,5252($4)
slt	$8,$8,$11
addu	$3,$12,$3
beq	$8,$0,$L2
sll	$3,$3,3

lw	$11,5392($4)
addu	$3,$11,$3
lbu	$8,0($3)
sll	$8,$8,2
addu	$8,$8,$14
lw	$8,0($8)
beq	$8,$7,$L12
lw	$8,8($sp)

$L2:
addiu	$10,$10,1
bne	$10,$13,$L4
nop

addiu	$2,$2,1
$L3:
lw	$3,8($sp)
sw	$3,6200($4)
lw	$3,12($sp)
addiu	$sp,$sp,16
j	$31
sw	$3,6204($4)

$L12:
lw	$3,4($3)
beq	$3,$8,$L2
nop

beq	$3,$0,$L2
sll	$8,$2,2

addu	$8,$24,$8
beq	$2,$15,$L5
sw	$3,0($8)

li	$2,1			# 0x1
.option	pic0
j	$L2
.option	pic2
sw	$10,6208($4)

$L5:
.option	pic0
j	$L3
.option	pic2
move	$2,$0

.set	macro
.set	reorder
.end	vp56_get_vectors_predictors
.size	vp56_get_vectors_predictors, .-vp56_get_vectors_predictors
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_vp3_idct_add_mxu
.type	ff_vp3_idct_add_mxu, @function
ff_vp3_idct_add_mxu:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lui	$2,%hi(whirl_idct)
addiu	$2,$2,%lo(whirl_idct)
#APP
# 766 "vp56.c" 1
.word	0b01110000010000000000000101010000	#S32LDD XR5,$2,0
# 0 "" 2
# 767 "vp56.c" 1
.word	0b01110000010000000000010110010000	#S32LDD XR6,$2,4
# 0 "" 2
# 768 "vp56.c" 1
.word	0b01110000010000000000100111010000	#S32LDD XR7,$2,8
# 0 "" 2
# 769 "vp56.c" 1
.word	0b01110000010000000000111000010000	#S32LDD XR8,$2,12
# 0 "" 2
# 770 "vp56.c" 1
.word	0b01110000010000000001001001010000	#S32LDD XR9,$2,16
# 0 "" 2
# 771 "vp56.c" 1
.word	0b01110000010000000001011010010000	#S32LDD XR10,$2,20
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$7,$0,$L18
addiu	$3,$6,-16
.set	macro
.set	reorder

move	$8,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L14
.option	pic2
move	$2,$3
.set	macro
.set	reorder

$L28:
#APP
# 787 "vp56.c" 1
.word	0b01110000000010010000001101101110	#S32M2I XR13, $9
# 0 "" 2
#NO_APP
beq	$9,$0,$L17
#APP
# 788 "vp56.c" 1
.word	0b01110000101101110101010000001000	#D16MUL XR0,XR5,XR13,XR13,HW
# 0 "" 2
# 789 "vp56.c" 1
.word	0b01110011111101110100000000110011	#D32SAR XR0,XR0,XR13,XR13,15
# 0 "" 2
# 790 "vp56.c" 1
.word	0b01110011001101110111010000111101	#S32SFL XR0,XR13,XR13,XR13,PTN3
# 0 "" 2
# 791 "vp56.c" 1
.word	0b01110000010000000000001101010001	#S32STD XR13,$2,0
# 0 "" 2
# 792 "vp56.c" 1
.word	0b01110000010000000000011101010001	#S32STD XR13,$2,4
# 0 "" 2
# 793 "vp56.c" 1
.word	0b01110000010000000000101101010001	#S32STD XR13,$2,8
# 0 "" 2
# 794 "vp56.c" 1
.word	0b01110000010000000000111101010001	#S32STD XR13,$2,12
# 0 "" 2
#NO_APP
$L16:
addiu	$8,$8,1
beq	$8,$7,$L18
$L14:
#APP
# 775 "vp56.c" 1
.word	0b01110000010000000001000001010100	#S32LDI XR1,$2,16
# 0 "" 2
# 776 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 777 "vp56.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 778 "vp56.c" 1
.word	0b01110000010000000000110100010000	#S32LDD XR4,$2,12
# 0 "" 2
# 779 "vp56.c" 1
.word	0b01110000000110001100101100100111	#S32OR XR12,XR2,XR3
# 0 "" 2
# 780 "vp56.c" 1
.word	0b01110000000110010011001011100111	#S32OR XR11,XR12,XR4
# 0 "" 2
# 781 "vp56.c" 1
.word	0b01110000000110000110111100100111	#S32OR XR12,XR11,XR1
# 0 "" 2
# 782 "vp56.c" 1
.word	0b01110000000010010000001100101110	#S32M2I XR12, $9
# 0 "" 2
#NO_APP
beq	$9,$0,$L16
#APP
# 785 "vp56.c" 1
.word	0b01110011001101000100001100111101	#S32SFL XR12,XR0,XR1,XR13,PTN3
# 0 "" 2
# 786 "vp56.c" 1
.word	0b01110000000110110010111011100111	#S32OR XR11,XR11,XR12
# 0 "" 2
# 787 "vp56.c" 1
.word	0b01110000000010010000001011101110	#S32M2I XR11, $9
# 0 "" 2
#NO_APP
beq	$9,$0,$L28
$L17:
#APP
# 798 "vp56.c" 1
.word	0b01110011000010001000010001111101	#S32SFL XR1,XR1,XR2,XR2,PTN3
# 0 "" 2
# 799 "vp56.c" 1
.word	0b01110011000100010000110011111101	#S32SFL XR3,XR3,XR4,XR4,PTN3
# 0 "" 2
# 801 "vp56.c" 1
.word	0b01110000001100010100101011001000	#D16MUL XR11,XR2,XR5,XR12,WW
# 0 "" 2
# 802 "vp56.c" 1
.word	0b01110000001100011001001011001010	#D16MAC XR11,XR4,XR6,XR12,AA,WW
# 0 "" 2
# 804 "vp56.c" 1
.word	0b01110000001110011000101101001000	#D16MUL XR13,XR2,XR6,XR14,WW
# 0 "" 2
# 805 "vp56.c" 1
.word	0b01110011001110010101001101001010	#D16MAC XR13,XR4,XR5,XR14,SS,WW
# 0 "" 2
# 807 "vp56.c" 1
.word	0b01110000100100011100010010001000	#D16MUL XR2,XR1,XR7,XR4,HW
# 0 "" 2
# 808 "vp56.c" 1
.word	0b01110001010100100100010010001010	#D16MAC XR2,XR1,XR9,XR4,AS,LW
# 0 "" 2
# 809 "vp56.c" 1
.word	0b01110001100100101000110010001010	#D16MAC XR2,XR3,XR10,XR4,AS,HW
# 0 "" 2
# 811 "vp56.c" 1
.word	0b01110001010100100000110010001010	#D16MAC XR2,XR3,XR8,XR4,AS,LW
# 0 "" 2
# 813 "vp56.c" 1
.word	0b01110011111101110110111011110011	#D32SAR XR11,XR11,XR13,XR13,15
# 0 "" 2
# 814 "vp56.c" 1
.word	0b01110011001011110110110000111101	#S32SFL XR0,XR11,XR13,XR11,PTN3
# 0 "" 2
# 815 "vp56.c" 1
.word	0b01110011111110111011001100110011	#D32SAR XR12,XR12,XR14,XR14,15
# 0 "" 2
# 816 "vp56.c" 1
.word	0b01110011001100111011000000111101	#S32SFL XR0,XR12,XR14,XR12,PTN3
# 0 "" 2
# 817 "vp56.c" 1
.word	0b01110011110100010000100010110011	#D32SAR XR2,XR2,XR4,XR4,15
# 0 "" 2
# 818 "vp56.c" 1
.word	0b01110011000010010000100000111101	#S32SFL XR0,XR2,XR4,XR2,PTN3
# 0 "" 2
# 820 "vp56.c" 1
.word	0b01110000101111100000010100001000	#D16MUL XR4,XR1,XR8,XR15,HW
# 0 "" 2
# 821 "vp56.c" 1
.word	0b01110011011111101000010100001010	#D16MAC XR4,XR1,XR10,XR15,SS,LW
# 0 "" 2
# 822 "vp56.c" 1
.word	0b01110000101111100100110100001010	#D16MAC XR4,XR3,XR9,XR15,AA,HW
# 0 "" 2
# 823 "vp56.c" 1
.word	0b01110010011111011100110100001010	#D16MAC XR4,XR3,XR7,XR15,SA,LW
# 0 "" 2
# 825 "vp56.c" 1
.word	0b01110001001100110010111011001110	#Q16ADD XR11,XR11,XR12,XR12,AS,WW
# 0 "" 2
# 829 "vp56.c" 1
.word	0b01110011110100010011111111110011	#D32SAR XR15,XR15,XR4,XR4,15
# 0 "" 2
# 830 "vp56.c" 1
.word	0b01110011001111010011110000111101	#S32SFL XR0,XR15,XR4,XR15,PTN3
# 0 "" 2
# 831 "vp56.c" 1
.word	0b01110001000010001010111011001110	#Q16ADD XR11,XR11,XR2,XR2,AS,WW
# 0 "" 2
# 837 "vp56.c" 1
.word	0b01110001111111111111001100001110	#Q16ADD XR12,XR12,XR15,XR15,AS,XW
# 0 "" 2
# 843 "vp56.c" 1
.word	0b01110011001100110010111011111101	#S32SFL XR11,XR11,XR12,XR12,PTN3
# 0 "" 2
# 848 "vp56.c" 1
.word	0b01110011001011101111001100111101	#S32SFL XR12,XR12,XR11,XR11,PTN3
# 0 "" 2
# 854 "vp56.c" 1
.word	0b01110000010000000000001100010001	#S32STD XR12,$2,0
# 0 "" 2
# 855 "vp56.c" 1
.word	0b01110000010000000000011011010001	#S32STD XR11,$2,4
# 0 "" 2
# 856 "vp56.c" 1
.word	0b01110000010000000000101111010001	#S32STD XR15,$2,8
# 0 "" 2
# 857 "vp56.c" 1
.word	0b01110000010000000000110010010001	#S32STD XR2,$2,12
# 0 "" 2
#NO_APP
addiu	$8,$8,1
bne	$8,$7,$L14
$L18:
li	$7,1518469120			# 0x5a820000
li	$8,524288			# 0x80000
addiu	$9,$7,30274
addiu	$6,$6,-4
li	$2,4			# 0x4
addiu	$7,$7,12540
addiu	$8,$8,8
$L15:
#APP
# 863 "vp56.c" 1
.word	0b01110000000010010000000101101111	#S32I2M XR5,$9
# 0 "" 2
# 864 "vp56.c" 1
.word	0b01110000000001110000000110101111	#S32I2M XR6,$7
# 0 "" 2
# 865 "vp56.c" 1
.word	0b01110000110000000000010001010100	#S32LDI XR1,$6,4
# 0 "" 2
# 866 "vp56.c" 1
.word	0b01110000110000000010000011010000	#S32LDD XR3,$6,32
# 0 "" 2
# 867 "vp56.c" 1
.word	0b01110000110000000100001011010000	#S32LDD XR11,$6,64
# 0 "" 2
# 868 "vp56.c" 1
.word	0b01110000110000000110001101010000	#S32LDD XR13,$6,96
# 0 "" 2
# 870 "vp56.c" 1
.word	0b01110000100010000101011111001000	#D16MUL XR15,XR5,XR1,XR2,HW
# 0 "" 2
# 871 "vp56.c" 1
.word	0b01110000100010101101011111001010	#D16MAC XR15,XR5,XR11,XR2,AA,HW
# 0 "" 2
# 872 "vp56.c" 1
.word	0b01110000011001001101011010001000	#D16MUL XR10,XR5,XR3,XR9,LW
# 0 "" 2
# 873 "vp56.c" 1
.word	0b01110000011001110101101010001010	#D16MAC XR10,XR6,XR13,XR9,AA,LW
# 0 "" 2
# 874 "vp56.c" 1
.word	0b01110011110010001011111111110011	#D32SAR XR15,XR15,XR2,XR2,15
# 0 "" 2
# 875 "vp56.c" 1
.word	0b01110011001111001011110000111101	#S32SFL XR0,XR15,XR2,XR15,PTN3
# 0 "" 2
# 876 "vp56.c" 1
.word	0b01110011111001100110101010110011	#D32SAR XR10,XR10,XR9,XR9,15
# 0 "" 2
# 877 "vp56.c" 1
.word	0b01110011001010100110100000111101	#S32SFL XR0,XR10,XR9,XR10,PTN3
# 0 "" 2
# 879 "vp56.c" 1
.word	0b01110000110000000001000010010000	#S32LDD XR2,$6,16
# 0 "" 2
# 880 "vp56.c" 1
.word	0b01110000110000000011000100010000	#S32LDD XR4,$6,48
# 0 "" 2
# 881 "vp56.c" 1
.word	0b01110001001001101011111111001110	#Q16ADD XR15,XR15,XR10,XR9,AS,WW
# 0 "" 2
# 885 "vp56.c" 1
.word	0b01110000100001000101011010001000	#D16MUL XR10,XR5,XR1,XR1,HW
# 0 "" 2
# 886 "vp56.c" 1
.word	0b01110011100001101101011010001010	#D16MAC XR10,XR5,XR11,XR1,SS,HW
# 0 "" 2
# 887 "vp56.c" 1
.word	0b01110000011100001101101011001000	#D16MUL XR11,XR6,XR3,XR12,LW
# 0 "" 2
# 888 "vp56.c" 1
.word	0b01110011011100110101011011001010	#D16MAC XR11,XR5,XR13,XR12,SS,LW
# 0 "" 2
# 889 "vp56.c" 1
.word	0b01110011110001000110101010110011	#D32SAR XR10,XR10,XR1,XR1,15
# 0 "" 2
# 890 "vp56.c" 1
.word	0b01110011001010000110100000111101	#S32SFL XR0,XR10,XR1,XR10,PTN3
# 0 "" 2
# 891 "vp56.c" 1
.word	0b01110011111100110010111011110011	#D32SAR XR11,XR11,XR12,XR12,15
# 0 "" 2
# 892 "vp56.c" 1
.word	0b01110011001011110010110000111101	#S32SFL XR0,XR11,XR12,XR11,PTN3
# 0 "" 2
# 895 "vp56.c" 1
.word	0b01110000110000000101001100010000	#S32LDD XR12,$6,80
# 0 "" 2
# 896 "vp56.c" 1
.word	0b01110000110000000111001110010000	#S32LDD XR14,$6,112
# 0 "" 2
# 897 "vp56.c" 1
.word	0b01110001000001101110101010001110	#Q16ADD XR10,XR10,XR11,XR1,AS,WW
# 0 "" 2
# 902 "vp56.c" 1
.word	0b01110000101101001001111011001000	#D16MUL XR11,XR7,XR2,XR13,HW
# 0 "" 2
# 903 "vp56.c" 1
.word	0b01110000011101010001111011001010	#D16MAC XR11,XR7,XR4,XR13,AA,LW
# 0 "" 2
# 904 "vp56.c" 1
.word	0b01110000011101110010001011001010	#D16MAC XR11,XR8,XR12,XR13,AA,LW
# 0 "" 2
# 905 "vp56.c" 1
.word	0b01110000101101111010001011001010	#D16MAC XR11,XR8,XR14,XR13,AA,HW
# 0 "" 2
# 907 "vp56.c" 1
.word	0b01110000010101001001110011001000	#D16MUL XR3,XR7,XR2,XR5,LW
# 0 "" 2
# 908 "vp56.c" 1
.word	0b01110011100101010010000011001010	#D16MAC XR3,XR8,XR4,XR5,SS,HW
# 0 "" 2
# 909 "vp56.c" 1
.word	0b01110011100101110001110011001010	#D16MAC XR3,XR7,XR12,XR5,SS,HW
# 0 "" 2
# 911 "vp56.c" 1
.word	0b01110011010101111010000011001010	#D16MAC XR3,XR8,XR14,XR5,SS,LW
# 0 "" 2
# 913 "vp56.c" 1
.word	0b01110011111101110110111011110011	#D32SAR XR11,XR11,XR13,XR13,15
# 0 "" 2
# 914 "vp56.c" 1
.word	0b01110011001011110110110000111101	#S32SFL XR0,XR11,XR13,XR11,PTN3
# 0 "" 2
# 915 "vp56.c" 1
.word	0b01110011110101010100110011110011	#D32SAR XR3,XR3,XR5,XR5,15
# 0 "" 2
# 916 "vp56.c" 1
.word	0b01110011000011010100110000111101	#S32SFL XR0,XR3,XR5,XR3,PTN3
# 0 "" 2
# 918 "vp56.c" 1
.word	0b01110000011101001010000101001000	#D16MUL XR5,XR8,XR2,XR13,LW
# 0 "" 2
# 919 "vp56.c" 1
.word	0b01110011101101010001110101001010	#D16MAC XR5,XR7,XR4,XR13,SS,HW
# 0 "" 2
# 920 "vp56.c" 1
.word	0b01110000101101110010000101001010	#D16MAC XR5,XR8,XR12,XR13,AA,HW
# 0 "" 2
# 922 "vp56.c" 1
.word	0b01110000011101111001110101001010	#D16MAC XR5,XR7,XR14,XR13,AA,LW
# 0 "" 2
# 924 "vp56.c" 1
.word	0b01110000100110001010000010001000	#D16MUL XR2,XR8,XR2,XR6,HW
# 0 "" 2
# 925 "vp56.c" 1
.word	0b01110011010110010010000010001010	#D16MAC XR2,XR8,XR4,XR6,SS,LW
# 0 "" 2
# 926 "vp56.c" 1
.word	0b01110000010110110001110010001010	#D16MAC XR2,XR7,XR12,XR6,AA,LW
# 0 "" 2
# 927 "vp56.c" 1
.word	0b01110011100110111001110010001010	#D16MAC XR2,XR7,XR14,XR6,SS,HW
# 0 "" 2
# 929 "vp56.c" 1
.word	0b01110011111101110101010101110011	#D32SAR XR5,XR5,XR13,XR13,15
# 0 "" 2
# 930 "vp56.c" 1
.word	0b01110011000101110101010000111101	#S32SFL XR0,XR5,XR13,XR5,PTN3
# 0 "" 2
# 931 "vp56.c" 1
.word	0b01110011110110011000100010110011	#D32SAR XR2,XR2,XR6,XR6,15
# 0 "" 2
# 932 "vp56.c" 1
.word	0b01110011000010011000100000111101	#S32SFL XR0,XR2,XR6,XR2,PTN3
# 0 "" 2
# 935 "vp56.c" 1
.word	0b01110000000010000000000100101111	#S32I2M XR4,$8
# 0 "" 2
# 936 "vp56.c" 1
.word	0b01110001001011101111111111001110	#Q16ADD XR15,XR15,XR11,XR11,AS,WW
# 0 "" 2
# 945 "vp56.c" 1
.word	0b01110001000011001110101010001110	#Q16ADD XR10,XR10,XR3,XR3,AS,WW
# 0 "" 2
# 953 "vp56.c" 1
.word	0b01110001000101010100010001001110	#Q16ADD XR1,XR1,XR5,XR5,AS,WW
# 0 "" 2
# 961 "vp56.c" 1
.word	0b01110001000010001010011001001110	#Q16ADD XR9,XR9,XR2,XR2,AS,WW
# 0 "" 2
# 970 "vp56.c" 1
.word	0b01110000011010010001001111011011	#Q16ACCM XR15,XR4,XR4,XR10,AA
# 0 "" 2
# 971 "vp56.c" 1
.word	0b01110000010001010001001011011011	#Q16ACCM XR11,XR4,XR4,XR1,AA
# 0 "" 2
# 972 "vp56.c" 1
.word	0b01110000010010010001001001011011	#Q16ACCM XR9,XR4,XR4,XR2,AA
# 0 "" 2
# 973 "vp56.c" 1
.word	0b01110000010011010001000101011011	#Q16ACCM XR5,XR4,XR4,XR3,AA
# 0 "" 2
# 974 "vp56.c" 1
.word	0b01110001001010101011111111110111	#Q16SAR XR15,XR15,XR10,XR10,4
# 0 "" 2
# 975 "vp56.c" 1
.word	0b01110001000001000110111011110111	#Q16SAR XR11,XR11,XR1,XR1,4
# 0 "" 2
# 976 "vp56.c" 1
.word	0b01110001000010001010011001110111	#Q16SAR XR9,XR9,XR2,XR2,4
# 0 "" 2
# 977 "vp56.c" 1
.word	0b01110001000011001101010101110111	#Q16SAR XR5,XR5,XR3,XR3,4
# 0 "" 2
# 979 "vp56.c" 1
.word	0b01110000110000000000001111010001	#S32STD XR15,$6,0
# 0 "" 2
# 980 "vp56.c" 1
.word	0b01110000110000000001001010010001	#S32STD XR10,$6,16
# 0 "" 2
# 981 "vp56.c" 1
.word	0b01110000110000000010000001010001	#S32STD XR1,$6,32
# 0 "" 2
# 982 "vp56.c" 1
.word	0b01110000110000000011001001010001	#S32STD XR9,$6,48
# 0 "" 2
# 983 "vp56.c" 1
.word	0b01110000110000000100000010010001	#S32STD XR2,$6,64
# 0 "" 2
# 984 "vp56.c" 1
.word	0b01110000110000000101000101010001	#S32STD XR5,$6,80
# 0 "" 2
# 985 "vp56.c" 1
.word	0b01110000110000000110000011010001	#S32STD XR3,$6,96
# 0 "" 2
# 986 "vp56.c" 1
.word	0b01110000110000000111001011010001	#S32STD XR11,$6,112
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$0,$L15
subu	$2,$4,$5
#APP
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 992 "vp56.c" 1
.word	0b01110000010001010000000001010110	#S32LDIV XR1,$2,$5,0
# 0 "" 2
# 993 "vp56.c" 1
.word	0b01110000011000000001000011010100	#S32LDI XR3,$3,16
# 0 "" 2
# 994 "vp56.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 995 "vp56.c" 1
.word	0b01110000000011000000010100011101	#Q8ACCE XR4,XR1,XR0,XR3,AA
# 0 "" 2
# 996 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 997 "vp56.c" 1
.word	0b01110000011000000000100101010000	#S32LDD XR5,$3,8
# 0 "" 2
# 998 "vp56.c" 1
.word	0b01110000011000000000110110010000	#S32LDD XR6,$3,12
# 0 "" 2
# 999 "vp56.c" 1
.word	0b01110000000101000000100110011101	#Q8ACCE XR6,XR2,XR0,XR5,AA
# 0 "" 2
# 1000 "vp56.c" 1
.word	0b01110000000110001101000001000111	#Q16SAT XR1,XR4,XR3
# 0 "" 2
# 1001 "vp56.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 1002 "vp56.c" 1
.word	0b01110000000110010101100010000111	#Q16SAT XR2,XR6,XR5
# 0 "" 2
# 1003 "vp56.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
#NO_APP
j	$31
.end	ff_vp3_idct_add_mxu
.size	ff_vp3_idct_add_mxu, .-ff_vp3_idct_add_mxu
.align	2
.globl	ff_vp56_init_dequant
.set	nomips16
.set	nomicromips
.ent	ff_vp56_init_dequant
.type	ff_vp56_init_dequant, @function
ff_vp56_init_dequant:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$6,%hi(vp56_dc_dequant)
lui	$3,%hi(vp56_ac_dequant)
addiu	$6,$6,%lo(vp56_dc_dequant)
addiu	$3,$3,%lo(vp56_ac_dequant)
addu	$6,$5,$6
addu	$3,$5,$3
lui	$28,%hi(__gnu_local_gp)
lbu	$7,0($6)
move	$2,$4
lbu	$3,0($3)
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$4,5288($4)
sll	$7,$7,2
lw	$6,5248($2)
sll	$3,$3,2
lw	$25,%call16(memset)($28)
sw	$5,5280($2)
sh	$7,5284($2)
.reloc	1f,R_MIPS_JALR,memset
1:	jr	$25
sh	$3,5286($2)

.set	macro
.set	reorder
.end	ff_vp56_init_dequant
.size	ff_vp56_init_dequant, .-ff_vp56_init_dequant
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"get_buffer() failed\012\000"
.align	2
$LC1:
.ascii	"picture too big\012\000"
.text
.align	2
.globl	ff_vp56_decode_frame
.set	nomips16
.set	nomicromips
.ent	ff_vp56_decode_frame
.type	ff_vp56_decode_frame, @function
ff_vp56_decode_frame:
.frame	$sp,248,$31		# vars= 160, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-248
lw	$3,16($7)
lui	$28,%hi(__gnu_local_gp)
lw	$8,20($7)
sw	$20,224($sp)
lw	$20,136($4)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,244($sp)
sw	$fp,240($sp)
sw	$23,236($sp)
sw	$22,232($sp)
sw	$21,228($sp)
sw	$19,220($sp)
sw	$18,216($sp)
sw	$17,212($sp)
sw	$16,208($sp)
.cprestore	40
sw	$3,168($sp)
sw	$8,172($sp)
lw	$9,5136($20)
lw	$2,6496($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L32
sw	$9,144($sp)
.set	macro
.set	reorder

slt	$2,$8,3
.set	noreorder
.set	nomacro
bne	$2,$0,$L222
lw	$10,168($sp)
.set	macro
.set	reorder

addiu	$8,$8,-3
lbu	$2,0($3)
lbu	$3,1($3)
sw	$8,172($sp)
addiu	$8,$10,3
lbu	$9,2($10)
sll	$2,$2,16
sll	$3,$3,8
lw	$11,172($sp)
or	$2,$2,$3
or	$9,$2,$9
slt	$2,$11,$9
.set	noreorder
.set	nomacro
bne	$2,$0,$L222
sw	$9,180($sp)
.set	macro
.set	reorder

sw	$8,168($sp)
$L32:
sw	$4,164($sp)
li	$4,321650688			# 0x132c0000
li	$3,-201326592			# 0xfffffffff4000000
sw	$5,192($sp)
addiu	$4,$4,4096
sw	$6,196($sp)
sw	$7,188($sp)
li	$2,321585152			# 0x132b0000
sw	$4,8192($3)
li	$4,50331648			# 0x3000000
addiu	$5,$2,4096
addiu	$4,$4,768
addiu	$2,$2,8192
sw	$5,8196($3)
sw	$4,8200($3)
li	$4,-2097152000			# 0xffffffff83000000
addiu	$4,$4,768
sw	$4,8204($3)
li	$3,-1289682944			# 0xffffffffb3210000
sw	$2,0($3)
lw	$2,6496($20)
.set	noreorder
.set	nomacro
bltz	$2,$L34
lw	$12,%got(vp56_b2p)($28)
.set	macro
.set	reorder

addiu	$13,$20,6564
li	$14,6			# 0x6
sw	$0,176($sp)
addiu	$18,$sp,92
sw	$13,184($sp)
move	$19,$20
sw	$12,112($sp)
sw	$14,104($sp)
sw	$18,200($sp)
$L200:
lw	$3,184($sp)
move	$4,$19
lw	$25,6556($19)
lw	$5,168($sp)
lw	$6,172($sp)
lw	$7,200($sp)
sw	$0,92($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$3,6560($19)
.set	macro
.set	reorder

lw	$28,40($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L222
move	$16,$2
.set	macro
.set	reorder

lw	$8,176($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L324
lw	$9,164($sp)
.set	macro
.set	reorder

$L206:
lw	$3,144($sp)
$L365:
lw	$2,48($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L43
li	$2,2			# 0x2
.set	macro
.set	reorder

li	$2,1			# 0x1
lw	$25,6544($19)
move	$4,$19
.set	noreorder
.set	nomacro
jalr	$25
sw	$2,52($3)
.set	macro
.set	reorder

move	$4,$0
lw	$2,5252($19)
lw	$3,5248($19)
mul	$2,$2,$3
.set	noreorder
.set	nomacro
blez	$2,$L45
li	$5,1			# 0x1
.set	macro
.set	reorder

$L275:
lw	$2,5392($19)
sll	$3,$4,3
addiu	$4,$4,1
addu	$2,$2,$3
sb	$5,0($2)
lw	$2,5252($19)
lw	$3,5248($19)
mul	$2,$2,$3
slt	$2,$4,$2
bne	$2,$0,$L275
$L45:
lw	$25,6552($19)
move	$4,$19
.set	noreorder
.set	nomacro
jalr	$25
addiu	$21,$19,5368
.set	macro
.set	reorder

li	$6,18			# 0x12
lw	$28,40($sp)
move	$5,$0
lw	$25,%call16(memset)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$21
.set	macro
.set	reorder

li	$2,128			# 0x80
lw	$18,5248($19)
lw	$28,40($sp)
sh	$2,5374($19)
sll	$6,$18,2
sh	$2,5380($19)
slt	$2,$6,-5
.set	noreorder
.set	nomacro
bne	$2,$0,$L79
move	$4,$0
.set	macro
.set	reorder

move	$3,$0
li	$5,-1			# 0xffffffffffffffff
$L80:
lw	$2,5292($19)
addiu	$3,$3,1
addu	$2,$2,$4
sb	$0,0($2)
lw	$18,5248($19)
sw	$5,4($2)
sh	$0,8($2)
sll	$6,$18,2
addiu	$2,$6,5
slt	$2,$2,$3
.set	noreorder
.set	nomacro
beq	$2,$0,$L80
addiu	$4,$4,12
.set	macro
.set	reorder

$L79:
addiu	$17,$18,1
lw	$3,5292($19)
sll	$2,$18,5
lw	$8,144($sp)
sll	$7,$17,3
lw	$9,144($sp)
sll	$4,$17,5
lw	$5,6500($19)
addu	$2,$6,$2
lw	$16,5252($19)
subu	$4,$4,$7
lw	$8,16($8)
addu	$2,$3,$2
lw	$9,20($9)
addu	$3,$3,$4
slt	$4,$5,0
sw	$0,4($3)
li	$3,7			# 0x7
sw	$8,148($sp)
movz	$3,$0,$4
sw	$9,156($sp)
sw	$0,52($2)
.set	noreorder
.set	nomacro
blez	$16,$L194
sw	$3,152($sp)
.set	macro
.set	reorder

sll	$10,$8,3
sw	$0,120($sp)
move	$20,$19
move	$19,$21
.set	noreorder
.set	nomacro
bltz	$5,$L325
sw	$10,160($sp)
.set	macro
.set	reorder

$L231:
lw	$16,120($sp)
$L84:
lw	$3,0($20)
li	$2,-1			# 0xffffffffffffffff
sb	$0,5296($20)
sb	$0,5308($20)
sb	$0,5320($20)
sb	$0,5332($20)
lw	$3,132($3)
sw	$2,5300($20)
sw	$2,5312($20)
sw	$2,5324($20)
sw	$2,5336($20)
li	$2,93			# 0x5d
sh	$0,5304($20)
sh	$0,5316($20)
sh	$0,5328($20)
sh	$0,5340($20)
lw	$3,8($3)
.set	noreorder
.set	nomacro
beq	$3,$2,$L326
lw	$25,%call16(memset)($28)
.set	macro
.set	reorder

$L85:
lw	$12,152($sp)
sll	$2,$16,4
lw	$8,148($sp)
sll	$5,$18,1
lw	$4,6504($20)
sll	$7,$17,1
addu	$2,$2,$12
lw	$3,6508($20)
lw	$11,160($sp)
addu	$5,$5,$18
mul	$6,$2,$8
addiu	$4,$4,1314
li	$8,2			# 0x2
addiu	$3,$3,1314
sll	$4,$4,2
sll	$3,$3,2
sw	$8,5348($20)
sll	$16,$16,3
sw	$8,5356($20)
li	$9,1			# 0x1
lw	$8,156($sp)
addiu	$7,$7,1
addiu	$5,$5,5
addu	$10,$6,$11
sw	$9,5344($20)
addu	$4,$20,$4
sw	$9,5352($20)
addu	$3,$20,$3
sw	$7,5360($20)
subu	$2,$2,$16
sw	$5,5364($20)
sw	$6,0($4)
mul	$2,$2,$8
sw	$10,0($3)
lw	$4,5256($20)
lw	$3,5264($20)
addiu	$4,$4,8
addiu	$3,$3,8
sw	$2,5272($20)
sw	$4,5260($20)
sw	$3,5268($20)
.set	noreorder
.set	nomacro
blez	$18,$L86
sw	$2,5276($20)
.set	macro
.set	reorder

lw	$9,120($sp)
addiu	$10,$20,5344
lw	$11,120($sp)
lw	$17,%got(blk_coeff)($28)
sll	$9,$9,4
sw	$0,96($sp)
andi	$11,$11,0xffff
sw	$10,132($sp)
sw	$9,128($sp)
sw	$11,140($sp)
$L165:
lw	$2,5136($20)
li	$12,-201326592			# 0xfffffffff4000000
ori	$12,$12,0x1000
lw	$8,48($2)
.set	noreorder
.set	nomacro
beq	$8,$0,$L327
sw	$12,0($17)
.set	macro
.set	reorder

lw	$12,120($sp)
li	$21,1			# 0x1
$L87:
lui	$11,%hi(vp56_reference_frame)
sll	$2,$21,2
addiu	$11,$11,%lo(vp56_reference_frame)
addu	$2,$11,$2
.set	noreorder
.set	nomacro
beq	$12,$0,$L150
lw	$16,0($2)
.set	macro
.set	reorder

lw	$13,96($sp)
beq	$13,$0,$L150
li	$3,-1289682944			# 0xffffffffb3210000
$L290:
lw	$2,4($3)
andi	$2,$2,0x4
beq	$2,$0,$L290
$L150:
lw	$25,6540($20)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$20
.set	macro
.set	reorder

li	$15,93			# 0x5d
lw	$28,40($sp)
lbu	$5,4140($20)
lw	$10,0($17)
lw	$11,132($sp)
sll	$5,$5,1
lw	$14,5292($20)
lw	$13,%got(vp56_b6to4)($28)
lw	$12,%got(vp56_b2p)($28)
addu	$5,$10,$5
$L163:
lbu	$7,0($13)
lw	$2,0($11)
sll	$4,$7,2
sll	$7,$7,4
sll	$3,$2,2
subu	$6,$7,$4
sll	$2,$2,4
addu	$6,$20,$6
subu	$3,$2,$3
lw	$2,5300($6)
.set	noreorder
.set	nomacro
beq	$16,$2,$L328
addu	$3,$14,$3
.set	macro
.set	reorder

li	$9,1			# 0x1
move	$8,$0
move	$6,$0
$L152:
lw	$2,4($3)
beq	$16,$2,$L329
lw	$2,0($20)
lw	$2,132($2)
lw	$2,8($2)
beq	$2,$15,$L330
$L308:
bne	$6,$0,$L219
lbu	$9,0($12)
sll	$2,$9,1
addu	$6,$2,$9
addu	$6,$6,$16
addiu	$6,$6,2684
sll	$6,$6,1
addu	$6,$20,$6
lh	$8,0($6)
$L161:
lhu	$6,0($5)
addu	$2,$2,$9
addiu	$5,$5,128
addu	$2,$2,$16
addu	$6,$8,$6
addiu	$2,$2,2684
sll	$6,$6,16
sll	$2,$2,1
sra	$6,$6,16
addu	$2,$20,$2
subu	$4,$7,$4
sh	$6,-128($5)
addiu	$11,$11,4
sh	$6,0($2)
addu	$4,$20,$4
lh	$2,-128($5)
addiu	$13,$13,1
sw	$16,4($3)
addiu	$12,$12,1
sh	$2,8($3)
lh	$2,-128($5)
sw	$16,5300($4)
sh	$2,5304($4)
lhu	$2,-128($5)
lhu	$3,5284($20)
mul	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$19,$11,$L163
sh	$2,-128($5)
.set	macro
.set	reorder

addiu	$2,$16,1284
lw	$3,5136($20)
sll	$2,$2,2
addu	$2,$20,$2
sw	$3,100($sp)
sltu	$3,$21,10
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L168
sw	$2,108($sp)
.set	macro
.set	reorder

li	$2,1			# 0x1
sll	$21,$2,$21
andi	$2,$21,0x3dc
.set	noreorder
.set	nomacro
beq	$2,$0,$L166
andi	$2,$21,0x21
.set	macro
.set	reorder

lw	$14,104($sp)
.set	noreorder
.set	nomacro
blez	$14,$L168
lw	$25,96($sp)
.set	macro
.set	reorder

addiu	$21,$20,6178
lw	$14,%got(idct_row)($28)
move	$fp,$0
lw	$3,100($sp)
sll	$25,$25,4
sw	$19,136($sp)
move	$19,$20
sw	$14,116($sp)
move	$5,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L193
.option	pic2
sw	$25,124($sp)
.set	macro
.set	reorder

$L332:
slt	$3,$3,32
bne	$3,$0,$L179
lw	$2,48($5)
movz	$31,$0,$2
$L179:
lh	$2,-2($21)
slt	$15,$fp,4
lh	$11,0($21)
teq	$6,$0,7
div	$0,$2,$6
mflo	$5
teq	$6,$0,7
div	$0,$11,$6
.set	noreorder
.set	nomacro
bne	$15,$0,$L180
mflo	$8
.set	macro
.set	reorder

srl	$10,$25,31
srl	$3,$24,31
addu	$10,$10,$25
addu	$3,$3,$24
sra	$25,$10,1
sra	$24,$3,1
$L180:
addiu	$10,$5,-2
addiu	$6,$8,-2
addu	$10,$10,$25
addu	$3,$6,$24
.set	noreorder
.set	nomacro
bltz	$10,$L320
lw	$24,5216($22)
.set	macro
.set	reorder

addiu	$25,$10,12
slt	$25,$25,$24
beq	$25,$0,$L320
.set	noreorder
.set	nomacro
bltz	$3,$L182
lw	$25,5232($22)
.set	macro
.set	reorder

addiu	$7,$3,12
slt	$7,$7,$25
beq	$7,$0,$L182
.set	noreorder
.set	nomacro
bne	$31,$0,$L331
mul	$3,$18,$8
.set	macro
.set	reorder

addu	$4,$3,$4
addu	$7,$5,$4
$L186:
and	$3,$2,$23
beq	$3,$0,$L188
$L335:
li	$6,-1			# 0xffffffffffffffff
li	$4,1			# 0x1
slt	$2,$0,$2
movz	$4,$6,$2
and	$3,$11,$23
.set	noreorder
.set	nomacro
beq	$3,$0,$L211
move	$2,$4
.set	macro
.set	reorder

slt	$11,$11,1
subu	$3,$0,$11
xor	$3,$3,$18
addu	$11,$3,$11
addu	$2,$2,$11
beq	$2,$0,$L190
$L211:
lw	$25,6536($19)
.set	noreorder
.set	nomacro
beq	$25,$0,$L191
addiu	$3,$fp,1544
.set	macro
.set	reorder

sw	$18,20($sp)
addu	$2,$2,$7
sll	$3,$3,2
move	$4,$19
addu	$3,$19,$3
sw	$2,16($sp)
move	$5,$16
move	$6,$13
lw	$2,0($3)
sw	$23,28($sp)
sw	$2,24($sp)
lw	$2,6220($19)
sw	$15,36($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$2,32($sp)
.set	macro
.set	reorder

lw	$8,100($sp)
$L349:
sll	$3,$fp,7
lw	$9,116($sp)
addiu	$21,$21,4
lw	$2,-926($21)
addu	$20,$8,$20
lw	$6,0($17)
addu	$7,$9,$fp
lw	$5,6512($22)
addiu	$fp,$fp,1
lw	$4,0($20)
addu	$6,$6,$3
lbu	$7,0($7)
.option	pic0
.set	noreorder
.set	nomacro
jal	ff_vp3_idct_add_mxu
.option	pic2
addu	$4,$4,$2
.set	macro
.set	reorder

lw	$10,104($sp)
.set	noreorder
.set	nomacro
beq	$10,$fp,$L312
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$5,5136($19)
$L193:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$fp,$2,$L238
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$fp,$3,$L239
li	$4,2			# 0x2
.set	macro
.set	reorder

addiu	$2,$fp,-2
sltu	$2,$2,2
movz	$4,$fp,$2
li	$24,8			# 0x8
move	$10,$0
movz	$24,$0,$2
move	$2,$4
$L178:
lw	$8,112($sp)
lw	$6,6528($19)
lw	$7,0($19)
addu	$3,$8,$fp
lw	$9,124($sp)
addu	$2,$6,$2
lw	$11,108($sp)
lw	$4,-922($21)
lbu	$20,0($3)
addu	$25,$10,$9
lbu	$6,0($2)
lw	$3,700($7)
sll	$20,$20,2
lw	$10,128($sp)
lw	$31,6216($19)
addiu	$23,$6,-1
addu	$2,$5,$20
addu	$7,$11,$20
addu	$22,$19,$20
lw	$16,0($2)
slt	$2,$3,48
addu	$24,$24,$10
lw	$13,0($7)
lw	$18,6512($22)
.set	noreorder
.set	nomacro
bne	$2,$0,$L332
addu	$16,$16,$4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L179
.option	pic2
move	$31,$0
.set	macro
.set	reorder

$L330:
lw	$2,-8($3)
beq	$16,$2,$L333
$L157:
lw	$2,16($3)
bne	$16,$2,$L308
$L218:
lh	$2,20($3)
addiu	$9,$6,1
addu	$8,$8,$2
$L154:
li	$2,2			# 0x2
$L351:
.set	noreorder
.set	nomacro
bne	$9,$2,$L219
srl	$6,$8,31
.set	macro
.set	reorder

lbu	$9,0($12)
addu	$6,$6,$8
sll	$2,$9,1
.option	pic0
.set	noreorder
.set	nomacro
j	$L161
.option	pic2
sra	$8,$6,1
.set	macro
.set	reorder

$L333:
lh	$2,-4($3)
.set	noreorder
.set	nomacro
bne	$6,$0,$L334
addu	$8,$8,$2
.set	macro
.set	reorder

lw	$2,16($3)
.set	noreorder
.set	nomacro
beq	$16,$2,$L218
li	$6,1			# 0x1
.set	macro
.set	reorder

$L219:
lbu	$9,0($12)
.option	pic0
.set	noreorder
.set	nomacro
j	$L161
.option	pic2
sll	$2,$9,1
.set	macro
.set	reorder

$L329:
lw	$2,0($20)
lh	$6,8($3)
lw	$2,132($2)
lw	$2,8($2)
.set	noreorder
.set	nomacro
bne	$2,$15,$L154
addu	$8,$8,$6
.set	macro
.set	reorder

li	$2,2			# 0x2
beq	$9,$2,$L351
lw	$2,-8($3)
.set	noreorder
.set	nomacro
bne	$16,$2,$L157
li	$6,1			# 0x1
.set	macro
.set	reorder

lh	$2,-4($3)
li	$9,2			# 0x2
.option	pic0
.set	noreorder
.set	nomacro
j	$L154
.option	pic2
addu	$8,$8,$2
.set	macro
.set	reorder

$L328:
lh	$8,5304($6)
li	$9,2			# 0x2
.option	pic0
.set	noreorder
.set	nomacro
j	$L152
.option	pic2
li	$6,1			# 0x1
.set	macro
.set	reorder

$L320:
lw	$25,5232($22)
$L182:
mul	$8,$18,$6
lw	$2,%call16(ff_emulated_edge_mc)($28)
addiu	$11,$4,-2
lw	$4,5164($19)
li	$7,12			# 0xc
sw	$3,24($sp)
addu	$5,$11,$5
sw	$25,32($sp)
sw	$15,204($sp)
move	$25,$2
sw	$7,16($sp)
move	$6,$18
sw	$10,20($sp)
addu	$5,$8,$5
sw	$24,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_emulated_edge_mc
1:	jalr	$25
addu	$5,$13,$5
.set	macro
.set	reorder

addiu	$7,$18,1
lh	$2,-2($21)
lw	$13,5164($19)
sll	$7,$7,1
lh	$11,0($21)
and	$3,$2,$23
.set	noreorder
.set	nomacro
bne	$3,$0,$L335
lw	$15,204($sp)
.set	macro
.set	reorder

$L188:
and	$2,$11,$23
.set	noreorder
.set	nomacro
beq	$2,$0,$L190
slt	$11,$11,1
.set	macro
.set	reorder

subu	$3,$0,$11
move	$2,$0
xor	$3,$3,$18
addu	$11,$3,$11
addu	$2,$2,$11
bne	$2,$0,$L211
$L190:
subu	$7,$7,$18
li	$5,-4			# 0xfffffffffffffffc
addu	$13,$13,$7
li	$4,4			# 0x4
and	$2,$13,$5
andi	$13,$13,0x3
subu	$3,$16,$18
subu	$13,$4,$13
#APP
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110001101001000100100011100111	#S32ALN XR3,XR2,XR1,$13
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110001101001001001000101100111	#S32ALN XR5,XR4,XR2,$13
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011100100000000011010111	#S32SDIV XR3,$3,$18,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
#NO_APP
.option	pic0
.set	noreorder
.set	nomacro
j	$L349
.option	pic2
lw	$8,100($sp)
.set	macro
.set	reorder

$L312:
move	$20,$19
lw	$19,136($sp)
$L168:
lw	$8,96($sp)
$L356:
li	$3,1			# 0x1
li	$2,-1289682944			# 0xffffffffb3210000
addiu	$8,$8,1
sw	$8,96($sp)
sw	$3,4($2)
lw	$13,5344($20)
lw	$12,5256($20)
lw	$11,5348($20)
addiu	$13,$13,2
lw	$10,5260($20)
lw	$9,5352($20)
addiu	$12,$12,16
lw	$8,5264($20)
addiu	$11,$11,2
lw	$7,5356($20)
addiu	$10,$10,16
lw	$6,5268($20)
addiu	$9,$9,2
lw	$5,5360($20)
addiu	$8,$8,16
lw	$4,5272($20)
addiu	$7,$7,2
lw	$3,5364($20)
addiu	$6,$6,16
lw	$2,5276($20)
addiu	$5,$5,1
lw	$18,5248($20)
addiu	$4,$4,8
sw	$13,5344($20)
addiu	$3,$3,1
sw	$12,5256($20)
addiu	$2,$2,8
sw	$11,5348($20)
sw	$10,5260($20)
sw	$9,5352($20)
sw	$8,5264($20)
sw	$7,5356($20)
sw	$6,5268($20)
sw	$5,5360($20)
sw	$4,5272($20)
sw	$3,5364($20)
sw	$2,5276($20)
lw	$13,96($sp)
slt	$12,$13,$18
bne	$12,$0,$L165
$L86:
lw	$11,120($sp)
lw	$16,5252($20)
addiu	$11,$11,1
slt	$2,$11,$16
.set	noreorder
.set	nomacro
beq	$2,$0,$L313
sw	$11,120($sp)
.set	macro
.set	reorder

lw	$5,6500($20)
.set	noreorder
.set	nomacro
bgez	$5,$L231
addiu	$17,$18,1
.set	macro
.set	reorder

$L325:
lw	$11,120($sp)
subu	$16,$16,$11
.option	pic0
.set	noreorder
.set	nomacro
j	$L84
.option	pic2
addiu	$16,$16,-1
.set	macro
.set	reorder

$L166:
.set	noreorder
.set	nomacro
beq	$2,$0,$L169
lw	$25,104($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$25,$L168
lw	$11,%got(idct_row)($28)
.set	macro
.set	reorder

move	$fp,$0
lw	$18,108($sp)
addiu	$21,$20,5256
li	$23,-4			# 0xfffffffffffffffc
lw	$16,100($sp)
li	$22,4			# 0x4
sw	$19,100($sp)
sw	$11,116($sp)
move	$19,$fp
move	$fp,$11
$L177:
lw	$12,112($sp)
lw	$4,0($21)
addu	$2,$12,$19
lbu	$7,0($2)
sll	$7,$7,2
addu	$10,$20,$7
addu	$2,$18,$7
addu	$7,$16,$7
lw	$6,6512($10)
lw	$5,0($2)
lw	$2,0($7)
subu	$3,$4,$6
addu	$5,$5,$3
addu	$3,$2,$3
and	$2,$5,$23
andi	$5,$5,0x3
subu	$5,$22,$5
#APP
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 340 "vp56.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 341 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 342 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 343 "vp56.c" 1
.word	0b01110000101001000100100011100111	#S32ALN XR3,XR2,XR1,$5
# 0 "" 2
# 344 "vp56.c" 1
.word	0b01110000101001001001000101100111	#S32ALN XR5,XR4,XR2,$5
# 0 "" 2
# 346 "vp56.c" 1
.word	0b01110000011001100000000011010111	#S32SDIV XR3,$3,$6,0
# 0 "" 2
# 347 "vp56.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
#NO_APP
addu	$11,$fp,$19
lw	$3,0($7)
lw	$6,0($17)
sll	$2,$19,7
lw	$5,6512($10)
addiu	$19,$19,1
lbu	$7,0($11)
addu	$4,$3,$4
addu	$6,$6,$2
.option	pic0
.set	noreorder
.set	nomacro
jal	ff_vp3_idct_add_mxu
.option	pic2
addiu	$21,$21,4
.set	macro
.set	reorder

lw	$13,104($sp)
.set	noreorder
.set	nomacro
bne	$13,$19,$L177
lw	$28,40($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L168
.option	pic2
lw	$19,100($sp)
.set	macro
.set	reorder

$L327:
lui	$13,%hi(vp56_candidate_predictor_pos)
lw	$14,96($sp)
move	$10,$0
sw	$0,88($sp)
addiu	$4,$13,%lo(vp56_candidate_predictor_pos)
sw	$0,48($sp)
sw	$0,52($sp)
move	$5,$0
lw	$13,140($sp)
li	$9,1			# 0x1
addiu	$11,$sp,48
li	$7,12			# 0xc
andi	$6,$14,0xffff
$L90:
lb	$2,0($4)
addu	$2,$6,$2
sll	$2,$2,16
sra	$2,$2,16
.set	noreorder
.set	nomacro
bltz	$2,$L88
lb	$3,1($4)
.set	macro
.set	reorder

slt	$12,$2,$18
.set	noreorder
.set	nomacro
beq	$12,$0,$L88
addu	$3,$13,$3
.set	macro
.set	reorder

sll	$3,$3,16
sra	$3,$3,16
bltz	$3,$L88
lw	$12,5252($20)
slt	$12,$3,$12
.set	noreorder
.set	nomacro
beq	$12,$0,$L88
mul	$12,$18,$3
.set	macro
.set	reorder

lw	$3,5392($20)
lui	$14,%hi(vp56_reference_frame)
addiu	$14,$14,%lo(vp56_reference_frame)
addu	$2,$12,$2
sll	$2,$2,3
addu	$2,$3,$2
lbu	$12,0($2)
sll	$12,$12,2
addu	$12,$14,$12
lw	$3,0($12)
.set	noreorder
.set	nomacro
beq	$3,$9,$L336
lw	$3,48($sp)
.set	macro
.set	reorder

$L88:
addiu	$5,$5,1
.set	noreorder
.set	nomacro
bne	$5,$7,$L90
addiu	$4,$4,2
.set	macro
.set	reorder

addiu	$8,$10,1
$L89:
lw	$2,5388($20)
sll	$3,$8,2
sll	$8,$8,4
lw	$10,5168($20)
lw	$9,%got(ff_vp56_norm_shift)($28)
sll	$6,$2,3
lw	$7,6560($20)
sll	$4,$2,1
lw	$5,5172($20)
addu	$8,$3,$8
lw	$11,5184($20)
addu	$4,$4,$6
lw	$6,48($sp)
sll	$3,$8,2
addu	$8,$8,$3
sw	$6,6200($20)
addu	$6,$9,$10
addu	$3,$4,$8
lw	$4,52($sp)
lbu	$6,0($6)
addiu	$3,$3,1512
sw	$4,6204($20)
addu	$7,$7,$3
sll	$4,$10,$6
addu	$5,$6,$5
lbu	$10,0($7)
sll	$8,$11,$6
.set	noreorder
.set	nomacro
bltz	$5,$L91
sw	$4,5168($20)
.set	macro
.set	reorder

lw	$11,5176($20)
lw	$3,5180($20)
sltu	$3,$11,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L352
addiu	$3,$4,-1
.set	macro
.set	reorder

addiu	$3,$11,2
sw	$3,5176($20)
lbu	$6,1($11)
lbu	$3,0($11)
sll	$6,$6,8
or	$6,$6,$3
sll	$3,$6,8
srl	$6,$6,8
or	$6,$3,$6
andi	$6,$6,0xffff
sll	$6,$6,$5
addiu	$5,$5,-16
or	$8,$8,$6
$L91:
addiu	$3,$4,-1
$L352:
sw	$5,5172($20)
mul	$3,$3,$10
sra	$3,$3,8
addiu	$3,$3,1
sll	$5,$3,16
sltu	$6,$8,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L92
lui	$10,%hi(vp56_pmbt_tree)
.set	macro
.set	reorder

subu	$3,$4,$3
subu	$8,$8,$5
sw	$3,5168($20)
sw	$8,5184($20)
$L93:
lw	$8,120($sp)
$L366:
lw	$10,96($sp)
lw	$4,5392($20)
mul	$11,$8,$18
sw	$2,5388($20)
addu	$3,$11,$10
sll	$3,$3,3
addu	$3,$4,$3
sb	$2,0($3)
lw	$21,5388($20)
sltu	$2,$21,10
.set	noreorder
.set	nomacro
beq	$2,$0,$L97
addiu	$2,$sp,88
.set	macro
.set	reorder

lui	$12,%hi($L99)
sll	$2,$21,2
addiu	$12,$12,%lo($L99)
addu	$2,$12,$2
lw	$2,0($2)
j	$2
.rdata
.align	2
.align	2
$L99:
.word	$L233
.word	$L233
.word	$L98
.word	$L100
.word	$L101
.word	$L233
.word	$L102
.word	$L103
.word	$L104
.word	$L105
.text
$L105:
lw	$5,120($sp)
li	$7,2			# 0x2
lw	$6,96($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	vp56_get_vectors_predictors
.option	pic2
move	$4,$20
.set	macro
.set	reorder

addiu	$2,$20,6204
lw	$21,5388($20)
$L97:
lw	$3,5248($20)
lw	$8,120($sp)
lw	$9,96($sp)
lw	$4,5392($20)
mul	$10,$8,$3
lw	$5,0($2)
move	$12,$8
addu	$3,$10,$9
sll	$3,$3,3
addu	$3,$4,$3
sw	$5,4($3)
lw	$3,0($2)
sw	$3,6176($20)
lw	$3,0($2)
sw	$3,6180($20)
lw	$3,0($2)
sw	$3,6184($20)
lw	$3,0($2)
sw	$3,6188($20)
lw	$3,0($2)
sw	$3,6192($20)
lw	$2,0($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L87
.option	pic2
sw	$2,6196($20)
.set	macro
.set	reorder

$L104:
lw	$5,120($sp)
li	$7,2			# 0x2
lw	$6,96($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	vp56_get_vectors_predictors
.option	pic2
move	$4,$20
.set	macro
.set	reorder

addiu	$2,$20,6200
.option	pic0
.set	noreorder
.set	nomacro
j	$L97
.option	pic2
lw	$21,5388($20)
.set	macro
.set	reorder

$L103:
lw	$8,5172($20)
addiu	$6,$sp,48
lw	$2,5168($20)
addiu	$7,$sp,64
lw	$3,5184($20)
move	$5,$8
addu	$4,$9,$2
$L355:
lbu	$4,0($4)
sll	$2,$2,$4
addu	$5,$4,$5
sll	$3,$3,$4
.set	noreorder
.set	nomacro
bltz	$5,$L106
sw	$2,5168($20)
.set	macro
.set	reorder

lw	$8,5176($20)
lw	$4,5180($20)
sltu	$4,$8,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L353
addiu	$4,$2,1
.set	macro
.set	reorder

addiu	$4,$8,2
sw	$4,5176($20)
lbu	$4,1($8)
lbu	$8,0($8)
sll	$4,$4,8
or	$4,$4,$8
sll	$8,$4,8
srl	$4,$4,8
or	$4,$8,$4
andi	$4,$4,0xffff
sll	$4,$4,$5
addiu	$5,$5,-16
or	$3,$3,$4
$L106:
addiu	$4,$2,1
$L353:
sra	$4,$4,1
sll	$10,$4,16
sltu	$8,$3,$10
xori	$8,$8,0x1
.set	noreorder
.set	nomacro
beq	$8,$0,$L234
sw	$5,5172($20)
.set	macro
.set	reorder

subu	$2,$2,$4
subu	$3,$3,$10
$L107:
addu	$4,$9,$2
sw	$3,5184($20)
sll	$11,$8,1
lbu	$8,0($4)
sll	$4,$2,$8
addu	$5,$5,$8
sll	$3,$3,$8
.set	noreorder
.set	nomacro
bltz	$5,$L108
sw	$4,5168($20)
.set	macro
.set	reorder

lw	$8,5176($20)
lw	$2,5180($20)
sltu	$2,$8,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L354
addiu	$2,$4,1
.set	macro
.set	reorder

addiu	$2,$8,2
sw	$2,5176($20)
lbu	$2,1($8)
lbu	$8,0($8)
sll	$2,$2,8
or	$2,$2,$8
sll	$8,$2,8
srl	$2,$2,8
or	$2,$8,$2
andi	$2,$2,0xffff
sll	$2,$2,$5
addiu	$5,$5,-16
or	$3,$3,$2
$L108:
addiu	$2,$4,1
$L354:
sra	$2,$2,1
sll	$12,$2,16
sltu	$10,$3,$12
xori	$8,$10,0x1
.set	noreorder
.set	nomacro
beq	$8,$0,$L109
sw	$5,5172($20)
.set	macro
.set	reorder

subu	$2,$4,$2
subu	$3,$3,$12
$L109:
or	$8,$8,$11
sw	$2,5168($20)
.set	noreorder
.set	nomacro
bne	$8,$0,$L110
sw	$3,5184($20)
.set	macro
.set	reorder

sw	$0,0($6)
$L111:
addiu	$6,$6,4
.set	noreorder
.set	nomacro
bne	$6,$7,$L355
addu	$4,$9,$2
.set	macro
.set	reorder

lw	$2,48($sp)
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$3,$L114
slt	$3,$2,3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$0,$L337
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L117
li	$3,4			# 0x4
.set	macro
.set	reorder

bne	$2,$3,$L315
lw	$16,6204($20)
andi	$18,$16,0xffff
sw	$16,6176($20)
srl	$16,$16,16
$L119:
lw	$2,52($sp)
li	$3,2			# 0x2
beq	$2,$3,$L121
slt	$3,$2,3
.set	noreorder
.set	nomacro
bne	$3,$0,$L338
li	$3,3			# 0x3
.set	macro
.set	reorder

beq	$2,$3,$L124
li	$3,4			# 0x4
bne	$2,$3,$L316
lw	$2,6204($20)
andi	$3,$2,0xffff
sw	$2,6180($20)
srl	$2,$2,16
$L126:
lw	$4,56($sp)
addu	$16,$2,$16
addu	$18,$3,$18
li	$2,2			# 0x2
andi	$18,$18,0xffff
.set	noreorder
.set	nomacro
beq	$4,$2,$L128
andi	$16,$16,0xffff
.set	macro
.set	reorder

slt	$2,$4,3
.set	noreorder
.set	nomacro
bne	$2,$0,$L339
li	$2,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$2,$L131
li	$2,4			# 0x4
.set	macro
.set	reorder

bne	$4,$2,$L317
lw	$3,6204($20)
andi	$4,$3,0xffff
sw	$3,6184($20)
srl	$3,$3,16
$L133:
lw	$2,60($sp)
addu	$16,$16,$3
addu	$18,$18,$4
li	$3,2			# 0x2
andi	$18,$18,0xffff
.set	noreorder
.set	nomacro
beq	$2,$3,$L135
andi	$16,$16,0xffff
.set	macro
.set	reorder

slt	$3,$2,3
.set	noreorder
.set	nomacro
bne	$3,$0,$L340
li	$3,3			# 0x3
.set	macro
.set	reorder

beq	$2,$3,$L138
li	$3,4			# 0x4
bne	$2,$3,$L318
lw	$2,6204($20)
andi	$3,$2,0xffff
sw	$2,6188($20)
srl	$2,$2,16
$L140:
lw	$4,5248($20)
addu	$3,$18,$3
lw	$8,120($sp)
addu	$2,$16,$2
lw	$6,0($20)
sll	$3,$3,16
lw	$9,96($sp)
sll	$2,$2,16
mul	$10,$8,$4
lw	$5,5392($20)
lw	$7,6188($20)
sra	$3,$3,16
lw	$6,132($6)
lw	$6,8($6)
addu	$4,$10,$9
sll	$4,$4,3
addu	$4,$5,$4
sw	$7,4($4)
li	$4,93			# 0x5d
.set	noreorder
.set	nomacro
beq	$6,$4,$L341
sra	$2,$2,16
.set	macro
.set	reorder

addiu	$7,$3,3
lw	$21,5388($20)
addiu	$6,$2,3
lw	$12,120($sp)
slt	$5,$3,0
slt	$4,$2,0
movn	$3,$7,$5
movn	$2,$6,$4
sra	$3,$3,2
sra	$2,$2,2
sh	$3,6196($20)
sh	$2,6198($20)
lw	$2,6196($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L87
.option	pic2
sw	$2,6192($20)
.set	macro
.set	reorder

$L102:
lw	$5,120($sp)
li	$7,2			# 0x2
lw	$6,96($sp)
addiu	$16,$sp,88
.option	pic0
.set	noreorder
.set	nomacro
jal	vp56_get_vectors_predictors
.option	pic2
move	$4,$20
.set	macro
.set	reorder

$L319:
lw	$25,6532($20)
move	$4,$20
.set	noreorder
.set	nomacro
jalr	$25
move	$5,$16
.set	macro
.set	reorder

move	$2,$16
.option	pic0
.set	noreorder
.set	nomacro
j	$L97
.option	pic2
lw	$21,5388($20)
.set	macro
.set	reorder

$L101:
addiu	$2,$20,6204
.option	pic0
.set	noreorder
.set	nomacro
j	$L97
.option	pic2
li	$21,4			# 0x4
.set	macro
.set	reorder

$L100:
addiu	$2,$20,6200
.option	pic0
.set	noreorder
.set	nomacro
j	$L97
.option	pic2
li	$21,3			# 0x3
.set	macro
.set	reorder

$L98:
.option	pic0
.set	noreorder
.set	nomacro
j	$L319
.option	pic2
addiu	$16,$sp,88
.set	macro
.set	reorder

$L233:
.option	pic0
.set	noreorder
.set	nomacro
j	$L97
.option	pic2
addiu	$2,$sp,88
.set	macro
.set	reorder

$L169:
andi	$21,$21,0x2
.set	noreorder
.set	nomacro
beq	$21,$0,$L356
lw	$8,96($sp)
.set	macro
.set	reorder

lw	$3,104($sp)
.set	noreorder
.set	nomacro
blez	$3,$L168
lw	$8,%got(idct_row)($28)
.set	macro
.set	reorder

li	$5,1518469120			# 0x5a820000
li	$6,134742016			# 0x8080000
lw	$11,100($sp)
lw	$12,112($sp)
addiu	$7,$5,30274
move	$4,$0
sw	$8,116($sp)
addiu	$5,$5,12540
lw	$14,116($sp)
addiu	$8,$20,5256
addiu	$6,$6,2056
move	$13,$3
$L176:
addu	$3,$12,$4
lw	$22,0($8)
sll	$2,$4,7
lbu	$9,0($3)
addu	$3,$10,$2
lui	$10,%hi(whirl_idct)
addiu	$2,$9,1628
sll	$9,$9,2
sll	$2,$2,2
addu	$9,$11,$9
addu	$2,$20,$2
addiu	$10,$10,%lo(whirl_idct)
lw	$15,0($9)
lw	$9,0($2)
#APP
# 524 "vp56.c" 1
.word	0b01110001010000000000000101010000	#S32LDD XR5,$10,0
# 0 "" 2
# 525 "vp56.c" 1
.word	0b01110001010000000000010110010000	#S32LDD XR6,$10,4
# 0 "" 2
# 526 "vp56.c" 1
.word	0b01110001010000000000100111010000	#S32LDD XR7,$10,8
# 0 "" 2
# 527 "vp56.c" 1
.word	0b01110001010000000000111000010000	#S32LDD XR8,$10,12
# 0 "" 2
# 528 "vp56.c" 1
.word	0b01110001010000000001001001010000	#S32LDD XR9,$10,16
# 0 "" 2
# 529 "vp56.c" 1
.word	0b01110001010000000001011010010000	#S32LDD XR10,$10,20
# 0 "" 2
#NO_APP
addu	$2,$14,$4
lbu	$21,0($2)
.set	noreorder
.set	nomacro
beq	$21,$0,$L175
addiu	$10,$3,-16
.set	macro
.set	reorder

move	$16,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L171
.option	pic2
move	$2,$10
.set	macro
.set	reorder

$L342:
#APP
# 545 "vp56.c" 1
.word	0b01110000000100100000001101101110	#S32M2I XR13, $18
# 0 "" 2
#NO_APP
beq	$18,$0,$L174
#APP
# 546 "vp56.c" 1
.word	0b01110000101101110101010000001000	#D16MUL XR0,XR5,XR13,XR13,HW
# 0 "" 2
# 547 "vp56.c" 1
.word	0b01110011111101110100000000110011	#D32SAR XR0,XR0,XR13,XR13,15
# 0 "" 2
# 548 "vp56.c" 1
.word	0b01110011001101110111010000111101	#S32SFL XR0,XR13,XR13,XR13,PTN3
# 0 "" 2
# 549 "vp56.c" 1
.word	0b01110000010000000000001101010001	#S32STD XR13,$2,0
# 0 "" 2
# 550 "vp56.c" 1
.word	0b01110000010000000000011101010001	#S32STD XR13,$2,4
# 0 "" 2
# 551 "vp56.c" 1
.word	0b01110000010000000000101101010001	#S32STD XR13,$2,8
# 0 "" 2
# 552 "vp56.c" 1
.word	0b01110000010000000000111101010001	#S32STD XR13,$2,12
# 0 "" 2
#NO_APP
$L173:
addiu	$16,$16,1
beq	$16,$21,$L175
$L171:
#APP
# 533 "vp56.c" 1
.word	0b01110000010000000001000001010100	#S32LDI XR1,$2,16
# 0 "" 2
# 534 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 535 "vp56.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 536 "vp56.c" 1
.word	0b01110000010000000000110100010000	#S32LDD XR4,$2,12
# 0 "" 2
# 537 "vp56.c" 1
.word	0b01110000000110001100101100100111	#S32OR XR12,XR2,XR3
# 0 "" 2
# 538 "vp56.c" 1
.word	0b01110000000110010011001011100111	#S32OR XR11,XR12,XR4
# 0 "" 2
# 539 "vp56.c" 1
.word	0b01110000000110000110111100100111	#S32OR XR12,XR11,XR1
# 0 "" 2
# 540 "vp56.c" 1
.word	0b01110000000100100000001100101110	#S32M2I XR12, $18
# 0 "" 2
#NO_APP
beq	$18,$0,$L173
#APP
# 543 "vp56.c" 1
.word	0b01110011001101000100001100111101	#S32SFL XR12,XR0,XR1,XR13,PTN3
# 0 "" 2
# 544 "vp56.c" 1
.word	0b01110000000110110010111011100111	#S32OR XR11,XR11,XR12
# 0 "" 2
# 545 "vp56.c" 1
.word	0b01110000000100100000001011101110	#S32M2I XR11, $18
# 0 "" 2
#NO_APP
beq	$18,$0,$L342
$L174:
#APP
# 556 "vp56.c" 1
.word	0b01110011000010001000010001111101	#S32SFL XR1,XR1,XR2,XR2,PTN3
# 0 "" 2
# 557 "vp56.c" 1
.word	0b01110011000100010000110011111101	#S32SFL XR3,XR3,XR4,XR4,PTN3
# 0 "" 2
# 559 "vp56.c" 1
.word	0b01110000001100010100101011001000	#D16MUL XR11,XR2,XR5,XR12,WW
# 0 "" 2
# 560 "vp56.c" 1
.word	0b01110000001100011001001011001010	#D16MAC XR11,XR4,XR6,XR12,AA,WW
# 0 "" 2
# 562 "vp56.c" 1
.word	0b01110000001110011000101101001000	#D16MUL XR13,XR2,XR6,XR14,WW
# 0 "" 2
# 563 "vp56.c" 1
.word	0b01110011001110010101001101001010	#D16MAC XR13,XR4,XR5,XR14,SS,WW
# 0 "" 2
# 565 "vp56.c" 1
.word	0b01110000100100011100010010001000	#D16MUL XR2,XR1,XR7,XR4,HW
# 0 "" 2
# 566 "vp56.c" 1
.word	0b01110001010100100100010010001010	#D16MAC XR2,XR1,XR9,XR4,AS,LW
# 0 "" 2
# 567 "vp56.c" 1
.word	0b01110001100100101000110010001010	#D16MAC XR2,XR3,XR10,XR4,AS,HW
# 0 "" 2
# 569 "vp56.c" 1
.word	0b01110001010100100000110010001010	#D16MAC XR2,XR3,XR8,XR4,AS,LW
# 0 "" 2
# 572 "vp56.c" 1
.word	0b01110011111101110110111011110011	#D32SAR XR11,XR11,XR13,XR13,15
# 0 "" 2
# 573 "vp56.c" 1
.word	0b01110011001011110110110000111101	#S32SFL XR0,XR11,XR13,XR11,PTN3
# 0 "" 2
# 574 "vp56.c" 1
.word	0b01110011110100010000100010110011	#D32SAR XR2,XR2,XR4,XR4,15
# 0 "" 2
# 575 "vp56.c" 1
.word	0b01110011000010010000100000111101	#S32SFL XR0,XR2,XR4,XR2,PTN3
# 0 "" 2
# 576 "vp56.c" 1
.word	0b01110011111110111011001100110011	#D32SAR XR12,XR12,XR14,XR14,15
# 0 "" 2
# 577 "vp56.c" 1
.word	0b01110011001100111011000000111101	#S32SFL XR0,XR12,XR14,XR12,PTN3
# 0 "" 2
# 580 "vp56.c" 1
.word	0b01110000101111100000010100001000	#D16MUL XR4,XR1,XR8,XR15,HW
# 0 "" 2
# 581 "vp56.c" 1
.word	0b01110011011111101000010100001010	#D16MAC XR4,XR1,XR10,XR15,SS,LW
# 0 "" 2
# 582 "vp56.c" 1
.word	0b01110000101111100100110100001010	#D16MAC XR4,XR3,XR9,XR15,AA,HW
# 0 "" 2
# 583 "vp56.c" 1
.word	0b01110010011111011100110100001010	#D16MAC XR4,XR3,XR7,XR15,SA,LW
# 0 "" 2
# 586 "vp56.c" 1
.word	0b01110001001100110010111011001110	#Q16ADD XR11,XR11,XR12,XR12,AS,WW
# 0 "" 2
# 591 "vp56.c" 1
.word	0b01110011110100010011111111110011	#D32SAR XR15,XR15,XR4,XR4,15
# 0 "" 2
# 592 "vp56.c" 1
.word	0b01110011001111010011110000111101	#S32SFL XR0,XR15,XR4,XR15,PTN3
# 0 "" 2
# 593 "vp56.c" 1
.word	0b01110001000010001010111011001110	#Q16ADD XR11,XR11,XR2,XR2,AS,WW
# 0 "" 2
# 598 "vp56.c" 1
.word	0b01110001111111111111001100001110	#Q16ADD XR12,XR12,XR15,XR15,AS,XW
# 0 "" 2
# 604 "vp56.c" 1
.word	0b01110011001100110010111011111101	#S32SFL XR11,XR11,XR12,XR12,PTN3
# 0 "" 2
# 609 "vp56.c" 1
.word	0b01110011001011101111001100111101	#S32SFL XR12,XR12,XR11,XR11,PTN3
# 0 "" 2
# 614 "vp56.c" 1
.word	0b01110000010000000000001100010001	#S32STD XR12,$2,0
# 0 "" 2
# 615 "vp56.c" 1
.word	0b01110000010000000000011011010001	#S32STD XR11,$2,4
# 0 "" 2
# 616 "vp56.c" 1
.word	0b01110000010000000000101111010001	#S32STD XR15,$2,8
# 0 "" 2
# 617 "vp56.c" 1
.word	0b01110000010000000000110010010001	#S32STD XR2,$2,12
# 0 "" 2
#NO_APP
addiu	$16,$16,1
bne	$16,$21,$L171
$L175:
addiu	$2,$3,-4
li	$3,4			# 0x4
$L172:
#APP
# 623 "vp56.c" 1
.word	0b01110000000001110000000101101111	#S32I2M XR5,$7
# 0 "" 2
# 624 "vp56.c" 1
.word	0b01110000000001010000000110101111	#S32I2M XR6,$5
# 0 "" 2
# 625 "vp56.c" 1
.word	0b01110000010000000000010001010100	#S32LDI XR1,$2,4
# 0 "" 2
# 626 "vp56.c" 1
.word	0b01110000010000000010000011010000	#S32LDD XR3,$2,32
# 0 "" 2
# 627 "vp56.c" 1
.word	0b01110000010000000100001011010000	#S32LDD XR11,$2,64
# 0 "" 2
# 628 "vp56.c" 1
.word	0b01110000010000000110001101010000	#S32LDD XR13,$2,96
# 0 "" 2
# 630 "vp56.c" 1
.word	0b01110000100010000101011111001000	#D16MUL XR15,XR5,XR1,XR2,HW
# 0 "" 2
# 631 "vp56.c" 1
.word	0b01110000100010101101011111001010	#D16MAC XR15,XR5,XR11,XR2,AA,HW
# 0 "" 2
# 632 "vp56.c" 1
.word	0b01110000011001001101011010001000	#D16MUL XR10,XR5,XR3,XR9,LW
# 0 "" 2
# 633 "vp56.c" 1
.word	0b01110000011001110101101010001010	#D16MAC XR10,XR6,XR13,XR9,AA,LW
# 0 "" 2
# 634 "vp56.c" 1
.word	0b01110011110010001011111111110011	#D32SAR XR15,XR15,XR2,XR2,15
# 0 "" 2
# 635 "vp56.c" 1
.word	0b01110011001111001011110000111101	#S32SFL XR0,XR15,XR2,XR15,PTN3
# 0 "" 2
# 636 "vp56.c" 1
.word	0b01110011111001100110101010110011	#D32SAR XR10,XR10,XR9,XR9,15
# 0 "" 2
# 637 "vp56.c" 1
.word	0b01110011001010100110100000111101	#S32SFL XR0,XR10,XR9,XR10,PTN3
# 0 "" 2
# 639 "vp56.c" 1
.word	0b01110000010000000001000010010000	#S32LDD XR2,$2,16
# 0 "" 2
# 640 "vp56.c" 1
.word	0b01110000010000000011000100010000	#S32LDD XR4,$2,48
# 0 "" 2
# 641 "vp56.c" 1
.word	0b01110001001001101011111111001110	#Q16ADD XR15,XR15,XR10,XR9,AS,WW
# 0 "" 2
# 646 "vp56.c" 1
.word	0b01110000100001000101011010001000	#D16MUL XR10,XR5,XR1,XR1,HW
# 0 "" 2
# 647 "vp56.c" 1
.word	0b01110011100001101101011010001010	#D16MAC XR10,XR5,XR11,XR1,SS,HW
# 0 "" 2
# 648 "vp56.c" 1
.word	0b01110000011100001101101011001000	#D16MUL XR11,XR6,XR3,XR12,LW
# 0 "" 2
# 649 "vp56.c" 1
.word	0b01110011011100110101011011001010	#D16MAC XR11,XR5,XR13,XR12,SS,LW
# 0 "" 2
# 650 "vp56.c" 1
.word	0b01110011110001000110101010110011	#D32SAR XR10,XR10,XR1,XR1,15
# 0 "" 2
# 651 "vp56.c" 1
.word	0b01110011001010000110100000111101	#S32SFL XR0,XR10,XR1,XR10,PTN3
# 0 "" 2
# 652 "vp56.c" 1
.word	0b01110011111100110010111011110011	#D32SAR XR11,XR11,XR12,XR12,15
# 0 "" 2
# 653 "vp56.c" 1
.word	0b01110011001011110010110000111101	#S32SFL XR0,XR11,XR12,XR11,PTN3
# 0 "" 2
# 655 "vp56.c" 1
.word	0b01110000010000000101001100010000	#S32LDD XR12,$2,80
# 0 "" 2
# 656 "vp56.c" 1
.word	0b01110000010000000111001110010000	#S32LDD XR14,$2,112
# 0 "" 2
# 657 "vp56.c" 1
.word	0b01110001000001101110101010001110	#Q16ADD XR10,XR10,XR11,XR1,AS,WW
# 0 "" 2
# 662 "vp56.c" 1
.word	0b01110000101101001001111011001000	#D16MUL XR11,XR7,XR2,XR13,HW
# 0 "" 2
# 663 "vp56.c" 1
.word	0b01110000011101010001111011001010	#D16MAC XR11,XR7,XR4,XR13,AA,LW
# 0 "" 2
# 664 "vp56.c" 1
.word	0b01110000011101110010001011001010	#D16MAC XR11,XR8,XR12,XR13,AA,LW
# 0 "" 2
# 665 "vp56.c" 1
.word	0b01110000101101111010001011001010	#D16MAC XR11,XR8,XR14,XR13,AA,HW
# 0 "" 2
# 666 "vp56.c" 1
.word	0b01110000010101001001110011001000	#D16MUL XR3,XR7,XR2,XR5,LW
# 0 "" 2
# 667 "vp56.c" 1
.word	0b01110011100101010010000011001010	#D16MAC XR3,XR8,XR4,XR5,SS,HW
# 0 "" 2
# 668 "vp56.c" 1
.word	0b01110011100101110001110011001010	#D16MAC XR3,XR7,XR12,XR5,SS,HW
# 0 "" 2
# 670 "vp56.c" 1
.word	0b01110011010101111010000011001010	#D16MAC XR3,XR8,XR14,XR5,SS,LW
# 0 "" 2
# 672 "vp56.c" 1
.word	0b01110011111101110110111011110011	#D32SAR XR11,XR11,XR13,XR13,15
# 0 "" 2
# 673 "vp56.c" 1
.word	0b01110011001011110110110000111101	#S32SFL XR0,XR11,XR13,XR11,PTN3
# 0 "" 2
# 674 "vp56.c" 1
.word	0b01110011110101010100110011110011	#D32SAR XR3,XR3,XR5,XR5,15
# 0 "" 2
# 675 "vp56.c" 1
.word	0b01110011000011010100110000111101	#S32SFL XR0,XR3,XR5,XR3,PTN3
# 0 "" 2
# 677 "vp56.c" 1
.word	0b01110000011101001010000101001000	#D16MUL XR5,XR8,XR2,XR13,LW
# 0 "" 2
# 678 "vp56.c" 1
.word	0b01110011101101010001110101001010	#D16MAC XR5,XR7,XR4,XR13,SS,HW
# 0 "" 2
# 679 "vp56.c" 1
.word	0b01110000101101110010000101001010	#D16MAC XR5,XR8,XR12,XR13,AA,HW
# 0 "" 2
# 681 "vp56.c" 1
.word	0b01110000011101111001110101001010	#D16MAC XR5,XR7,XR14,XR13,AA,LW
# 0 "" 2
# 683 "vp56.c" 1
.word	0b01110000100110001010000010001000	#D16MUL XR2,XR8,XR2,XR6,HW
# 0 "" 2
# 684 "vp56.c" 1
.word	0b01110011010110010010000010001010	#D16MAC XR2,XR8,XR4,XR6,SS,LW
# 0 "" 2
# 685 "vp56.c" 1
.word	0b01110000010110110001110010001010	#D16MAC XR2,XR7,XR12,XR6,AA,LW
# 0 "" 2
# 687 "vp56.c" 1
.word	0b01110011100110111001110010001010	#D16MAC XR2,XR7,XR14,XR6,SS,HW
# 0 "" 2
# 688 "vp56.c" 1
.word	0b01110011111101110101010101110011	#D32SAR XR5,XR5,XR13,XR13,15
# 0 "" 2
# 689 "vp56.c" 1
.word	0b01110011000101110101010000111101	#S32SFL XR0,XR5,XR13,XR5,PTN3
# 0 "" 2
# 691 "vp56.c" 1
.word	0b01110011110110011000100010110011	#D32SAR XR2,XR2,XR6,XR6,15
# 0 "" 2
# 692 "vp56.c" 1
.word	0b01110011000010011000100000111101	#S32SFL XR0,XR2,XR6,XR2,PTN3
# 0 "" 2
# 694 "vp56.c" 1
.word	0b01110000000001100000000100101111	#S32I2M XR4,$6
# 0 "" 2
# 695 "vp56.c" 1
.word	0b01110001001011101111111111001110	#Q16ADD XR15,XR15,XR11,XR11,AS,WW
# 0 "" 2
# 703 "vp56.c" 1
.word	0b01110001000011001110101010001110	#Q16ADD XR10,XR10,XR3,XR3,AS,WW
# 0 "" 2
# 711 "vp56.c" 1
.word	0b01110001000101010100010001001110	#Q16ADD XR1,XR1,XR5,XR5,AS,WW
# 0 "" 2
# 719 "vp56.c" 1
.word	0b01110001000010001010011001001110	#Q16ADD XR9,XR9,XR2,XR2,AS,WW
# 0 "" 2
# 727 "vp56.c" 1
.word	0b01110000011010010001001111011011	#Q16ACCM XR15,XR4,XR4,XR10,AA
# 0 "" 2
# 728 "vp56.c" 1
.word	0b01110000010001010001001011011011	#Q16ACCM XR11,XR4,XR4,XR1,AA
# 0 "" 2
# 729 "vp56.c" 1
.word	0b01110000010010010001001001011011	#Q16ACCM XR9,XR4,XR4,XR2,AA
# 0 "" 2
# 730 "vp56.c" 1
.word	0b01110000010011010001000101011011	#Q16ACCM XR5,XR4,XR4,XR3,AA
# 0 "" 2
# 731 "vp56.c" 1
.word	0b01110001001010101011111111110111	#Q16SAR XR15,XR15,XR10,XR10,4
# 0 "" 2
# 732 "vp56.c" 1
.word	0b01110001000001000110111011110111	#Q16SAR XR11,XR11,XR1,XR1,4
# 0 "" 2
# 733 "vp56.c" 1
.word	0b01110001000010001010011001110111	#Q16SAR XR9,XR9,XR2,XR2,4
# 0 "" 2
# 734 "vp56.c" 1
.word	0b01110001000011001101010101110111	#Q16SAR XR5,XR5,XR3,XR3,4
# 0 "" 2
# 736 "vp56.c" 1
.word	0b01110000010000000000001111010001	#S32STD XR15,$2,0
# 0 "" 2
# 737 "vp56.c" 1
.word	0b01110000010000000001001010010001	#S32STD XR10,$2,16
# 0 "" 2
# 738 "vp56.c" 1
.word	0b01110000010000000010000001010001	#S32STD XR1,$2,32
# 0 "" 2
# 739 "vp56.c" 1
.word	0b01110000010000000011001001010001	#S32STD XR9,$2,48
# 0 "" 2
# 740 "vp56.c" 1
.word	0b01110000010000000100000010010001	#S32STD XR2,$2,64
# 0 "" 2
# 741 "vp56.c" 1
.word	0b01110000010000000101000101010001	#S32STD XR5,$2,80
# 0 "" 2
# 742 "vp56.c" 1
.word	0b01110000010000000110000011010001	#S32STD XR3,$2,96
# 0 "" 2
# 743 "vp56.c" 1
.word	0b01110000010000000111001011010001	#S32STD XR11,$2,112
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
bne	$3,$0,$L172
subu	$2,$22,$9
addu	$2,$15,$2
#APP
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 749 "vp56.c" 1
.word	0b01110001010000000001000001010100	#S32LDI XR1,$10,16
# 0 "" 2
# 750 "vp56.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 751 "vp56.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 752 "vp56.c" 1
.word	0b01110001010000000000110100010000	#S32LDD XR4,$10,12
# 0 "" 2
# 753 "vp56.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 754 "vp56.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 755 "vp56.c" 1
.word	0b01110000010010010000000101010111	#S32SDIV XR5,$2,$9,0
# 0 "" 2
# 756 "vp56.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
#NO_APP
addiu	$4,$4,1
.set	noreorder
.set	nomacro
beq	$13,$4,$L168
addiu	$8,$8,4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L176
.option	pic2
lw	$10,0($17)
.set	macro
.set	reorder

$L92:
sw	$3,5168($20)
sw	$8,5184($20)
addiu	$6,$10,%lo(vp56_pmbt_tree)
$L215:
lb	$2,0($6)
.set	noreorder
.set	nomacro
blez	$2,$L93
subu	$2,$0,$2
.set	macro
.set	reorder

$L96:
lw	$3,5168($20)
lb	$8,1($6)
lw	$4,5172($20)
addu	$5,$9,$3
lw	$2,5184($20)
addu	$8,$7,$8
lbu	$5,0($5)
lbu	$11,0($8)
sll	$3,$3,$5
addu	$4,$5,$4
sll	$5,$2,$5
.set	noreorder
.set	nomacro
bltz	$4,$L94
sw	$3,5168($20)
.set	macro
.set	reorder

lw	$10,5176($20)
lw	$8,5180($20)
sltu	$8,$10,$8
.set	noreorder
.set	nomacro
beq	$8,$0,$L357
addiu	$2,$3,-1
.set	macro
.set	reorder

addiu	$2,$10,2
sw	$2,5176($20)
lbu	$8,1($10)
lbu	$2,0($10)
sll	$8,$8,8
or	$8,$8,$2
sll	$10,$8,8
srl	$8,$8,8
or	$8,$10,$8
andi	$8,$8,0xffff
sll	$8,$8,$4
addiu	$4,$4,-16
or	$5,$5,$8
$L94:
addiu	$2,$3,-1
$L357:
sw	$4,5172($20)
mul	$2,$2,$11
sra	$2,$2,8
addiu	$2,$2,1
sll	$4,$2,16
sltu	$8,$5,$4
bne	$8,$0,$L95
subu	$2,$3,$2
subu	$5,$5,$4
sw	$2,5168($20)
sw	$5,5184($20)
lb	$2,0($6)
sll	$2,$2,1
addu	$6,$6,$2
lb	$2,0($6)
.set	noreorder
.set	nomacro
bgtz	$2,$L96
subu	$2,$0,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L366
.option	pic2
lw	$8,120($sp)
.set	macro
.set	reorder

$L95:
sw	$2,5168($20)
addiu	$6,$6,2
.option	pic0
.set	noreorder
.set	nomacro
j	$L215
.option	pic2
sw	$5,5184($20)
.set	macro
.set	reorder

$L191:
li	$3,8			# 0x8
addu	$6,$2,$7
addu	$5,$13,$7
sw	$3,16($sp)
addu	$6,$13,$6
lw	$25,808($19)
move	$4,$16
.set	noreorder
.set	nomacro
jalr	$25
move	$7,$18
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L349
.option	pic2
lw	$8,100($sp)
.set	macro
.set	reorder

$L336:
lw	$2,4($2)
beq	$2,$3,$L88
.set	noreorder
.set	nomacro
beq	$2,$0,$L88
sll	$3,$10,2
.set	macro
.set	reorder

addu	$3,$11,$3
.set	noreorder
.set	nomacro
bne	$10,$0,$L89
sw	$2,0($3)
.set	macro
.set	reorder

li	$10,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L88
.option	pic2
sw	$5,6208($20)
.set	macro
.set	reorder

$L331:
addiu	$2,$4,-2
lw	$4,5164($19)
li	$3,-4			# 0xfffffffffffffffc
addu	$5,$2,$5
mul	$2,$18,$6
subu	$4,$4,$18
addu	$6,$2,$5
addu	$13,$13,$6
subu	$2,$13,$18
andi	$13,$13,0x3
and	$2,$2,$3
li	$3,4			# 0x4
subu	$3,$3,$13
#APP
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
# 358 "vp56.c" 1
.word	0b01110000010100100000000001010110	#S32LDIV XR1,$2,$18,0
# 0 "" 2
# 359 "vp56.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 360 "vp56.c" 1
.word	0b01110000010000000000100100010000	#S32LDD XR4,$2,8
# 0 "" 2
# 361 "vp56.c" 1
.word	0b01110000010000000000110110010000	#S32LDD XR6,$2,12
# 0 "" 2
# 363 "vp56.c" 1
.word	0b01110000011001000100100011100111	#S32ALN XR3,XR2,XR1,$3
# 0 "" 2
# 364 "vp56.c" 1
.word	0b01110000011001001001000101100111	#S32ALN XR5,XR4,XR2,$3
# 0 "" 2
# 365 "vp56.c" 1
.word	0b01110000011001010001100111100111	#S32ALN XR7,XR6,XR4,$3
# 0 "" 2
# 367 "vp56.c" 1
.word	0b01110000100100100000000011010111	#S32SDIV XR3,$4,$18,0
# 0 "" 2
# 368 "vp56.c" 1
.word	0b01110000100000000000010101010001	#S32STD XR5,$4,4
# 0 "" 2
# 369 "vp56.c" 1
.word	0b01110000100000000000100111010001	#S32STD XR7,$4,8
# 0 "" 2
#NO_APP
addiu	$5,$18,1
lw	$13,5164($19)
lh	$11,0($21)
sll	$7,$5,1
.option	pic0
.set	noreorder
.set	nomacro
j	$L186
.option	pic2
lh	$2,-2($21)
.set	macro
.set	reorder

$L110:
addiu	$8,$8,1
.option	pic0
.set	noreorder
.set	nomacro
j	$L111
.option	pic2
sw	$8,0($6)
.set	macro
.set	reorder

$L234:
.option	pic0
.set	noreorder
.set	nomacro
j	$L107
.option	pic2
move	$2,$4
.set	macro
.set	reorder

$L135:
lw	$25,6532($20)
addiu	$5,$20,6188
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$20
.set	macro
.set	reorder

$L318:
lhu	$3,6188($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L140
.option	pic2
lhu	$2,6190($20)
.set	macro
.set	reorder

$L128:
lw	$25,6532($20)
addiu	$5,$20,6184
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$20
.set	macro
.set	reorder

$L317:
lhu	$4,6184($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L133
.option	pic2
lhu	$3,6186($20)
.set	macro
.set	reorder

$L121:
lw	$25,6532($20)
addiu	$5,$20,6180
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$20
.set	macro
.set	reorder

$L316:
lhu	$3,6180($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L126
.option	pic2
lhu	$2,6182($20)
.set	macro
.set	reorder

$L114:
lw	$25,6532($20)
addiu	$5,$20,6176
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$20
.set	macro
.set	reorder

$L315:
lhu	$18,6176($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L119
.option	pic2
lhu	$16,6178($20)
.set	macro
.set	reorder

$L326:
addiu	$4,$20,6236
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,256			# 0x100
.set	macro
.set	reorder

li	$3,24			# 0x18
lw	$28,40($sp)
sb	$3,6492($20)
sb	$3,6493($20)
sb	$3,6494($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L85
.option	pic2
sb	$3,6495($20)
.set	macro
.set	reorder

$L337:
bne	$2,$0,$L315
move	$16,$0
sh	$0,6176($20)
move	$18,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L119
.option	pic2
sh	$0,6178($20)
.set	macro
.set	reorder

$L338:
bne	$2,$0,$L316
move	$2,$0
sh	$0,6180($20)
move	$3,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L126
.option	pic2
sh	$0,6182($20)
.set	macro
.set	reorder

$L339:
bne	$4,$0,$L317
move	$3,$0
sh	$0,6184($20)
move	$4,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L133
.option	pic2
sh	$0,6186($20)
.set	macro
.set	reorder

$L340:
bne	$2,$0,$L318
move	$2,$0
sh	$0,6188($20)
move	$3,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L140
.option	pic2
sh	$0,6190($20)
.set	macro
.set	reorder

$L341:
blez	$3,$L142
addiu	$3,$3,2
sra	$3,$3,2
sh	$3,6196($20)
.set	noreorder
.set	nomacro
blez	$2,$L144
sh	$3,6192($20)
.set	macro
.set	reorder

$L347:
addiu	$2,$2,2
sra	$2,$2,2
sh	$2,6198($20)
sh	$2,6194($20)
$L348:
lw	$21,5388($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L87
.option	pic2
lw	$12,120($sp)
.set	macro
.set	reorder

$L313:
move	$19,$20
$L194:
li	$3,-1289682944			# 0xffffffffb3210000
$L83:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L83
lw	$12,144($sp)
.set	macro
.set	reorder

lw	$2,48($12)
.set	noreorder
.set	nomacro
bne	$2,$0,$L196
lw	$2,92($sp)
.set	macro
.set	reorder

beq	$2,$0,$L197
$L196:
lw	$5,5144($19)
lw	$2,0($5)
.set	noreorder
.set	nomacro
beq	$2,$0,$L358
lw	$14,144($sp)
.set	macro
.set	reorder

lw	$2,5148($19)
.set	noreorder
.set	nomacro
beq	$5,$2,$L358
lw	$13,164($sp)
.set	macro
.set	reorder

lw	$25,260($13)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$13
.set	macro
.set	reorder

lw	$14,144($sp)
$L358:
sw	$14,5144($19)
$L197:
lw	$2,6496($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L359
lw	$10,176($sp)
.set	macro
.set	reorder

lw	$25,168($sp)
lw	$3,180($sp)
lw	$8,172($sp)
lw	$9,180($sp)
lw	$4,5148($19)
addu	$25,$25,$3
lw	$3,5144($19)
subu	$8,$8,$9
sw	$25,168($sp)
sw	$4,5144($19)
sw	$8,172($sp)
sw	$3,5148($19)
lw	$10,176($sp)
$L359:
lw	$11,184($sp)
lw	$12,104($sp)
lw	$13,112($sp)
addiu	$10,$10,1
addiu	$11,$11,1872
addiu	$12,$12,-2
addiu	$13,$13,6
sw	$10,176($sp)
slt	$2,$2,$10
sw	$11,184($sp)
sw	$12,104($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L200
sw	$13,112($sp)
.set	macro
.set	reorder

move	$20,$19
$L34:
lw	$5,5140($20)
lw	$3,5144($20)
beq	$5,$3,$L201
lw	$2,5148($20)
beq	$5,$2,$L201
lw	$2,0($5)
.set	noreorder
.set	nomacro
beq	$2,$0,$L204
lw	$14,164($sp)
.set	macro
.set	reorder

lw	$25,260($14)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$14
.set	macro
.set	reorder

lw	$5,5140($20)
$L204:
lw	$6,5136($20)
lw	$4,5288($20)
lw	$3,144($sp)
lw	$2,192($sp)
sw	$5,5136($20)
sw	$6,5140($20)
addiu	$7,$3,208
sw	$4,84($3)
li	$4,3			# 0x3
sw	$0,88($3)
sw	$4,160($3)
$L205:
lw	$8,0($3)
addiu	$3,$3,16
addiu	$2,$2,16
lw	$6,-12($3)
lw	$5,-8($3)
lw	$4,-4($3)
sw	$8,-16($2)
sw	$6,-12($2)
sw	$5,-8($2)
.set	noreorder
.set	nomacro
bne	$3,$7,$L205
sw	$4,-4($2)
.set	macro
.set	reorder

lw	$4,0($3)
lw	$3,4($3)
sw	$4,0($2)
sw	$3,4($2)
li	$2,216			# 0xd8
lw	$3,196($sp)
lw	$8,188($sp)
lw	$31,244($sp)
lw	$fp,240($sp)
lw	$23,236($sp)
lw	$22,232($sp)
lw	$21,228($sp)
lw	$20,224($sp)
lw	$19,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
sw	$2,0($3)
lw	$2,20($8)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,248
.set	macro
.set	reorder

$L138:
lw	$2,6200($20)
andi	$3,$2,0xffff
sw	$2,6188($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L140
.option	pic2
srl	$2,$2,16
.set	macro
.set	reorder

$L131:
lw	$3,6200($20)
andi	$4,$3,0xffff
sw	$3,6184($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L133
.option	pic2
srl	$3,$3,16
.set	macro
.set	reorder

$L124:
lw	$2,6200($20)
andi	$3,$2,0xffff
sw	$2,6180($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L126
.option	pic2
srl	$2,$2,16
.set	macro
.set	reorder

$L117:
lw	$16,6200($20)
andi	$18,$16,0xffff
sw	$16,6176($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L119
.option	pic2
srl	$16,$16,16
.set	macro
.set	reorder

$L43:
lw	$18,6560($19)
lw	$9,%got(ff_vp56_norm_shift)($28)
move	$17,$0
li	$20,174			# 0xae
addiu	$12,$18,20
sw	$2,52($3)
li	$16,205			# 0xcd
li	$15,1			# 0x1
li	$22,3			# 0x3
$L75:
lw	$5,5168($19)
lw	$3,5172($19)
lw	$4,5184($19)
addu	$2,$9,$5
lbu	$2,0($2)
sll	$5,$5,$2
addu	$3,$2,$3
sll	$4,$4,$2
.set	noreorder
.set	nomacro
bltz	$3,$L47
sw	$5,5168($19)
.set	macro
.set	reorder

lw	$6,5176($19)
lw	$2,5180($19)
sltu	$2,$6,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L360
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$6,2
sw	$2,5176($19)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$2,$2,$6
sll	$6,$2,8
srl	$2,$2,8
or	$2,$6,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$4,$4,$2
$L47:
addiu	$2,$5,-1
$L360:
mul	$2,$2,$20
sra	$2,$2,8
addiu	$2,$2,1
sll	$6,$2,16
sltu	$7,$4,$6
.set	noreorder
.set	nomacro
bne	$7,$0,$L48
sw	$3,5172($19)
.set	macro
.set	reorder

subu	$2,$5,$2
subu	$4,$4,$6
addu	$5,$9,$2
sw	$4,5184($19)
lbu	$10,0($5)
sll	$5,$2,$10
addu	$11,$3,$10
sll	$10,$4,$10
.set	noreorder
.set	nomacro
bltz	$11,$L49
sw	$5,5168($19)
.set	macro
.set	reorder

lw	$2,5176($19)
lw	$3,5180($19)
sltu	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L49
addiu	$3,$2,2
.set	macro
.set	reorder

sw	$3,5176($19)
lbu	$4,1($2)
lbu	$2,0($2)
sll	$4,$4,8
or	$4,$4,$2
sll	$6,$4,8
srl	$4,$4,8
or	$4,$6,$4
andi	$4,$4,0xffff
sll	$4,$4,$11
addiu	$11,$11,-16
or	$10,$10,$4
$L49:
addiu	$2,$5,1
sra	$2,$2,1
sll	$3,$2,16
sltu	$8,$10,$3
xori	$8,$8,0x1
.set	noreorder
.set	nomacro
beq	$8,$0,$L50
sw	$11,5172($19)
.set	macro
.set	reorder

subu	$2,$5,$2
subu	$10,$10,$3
$L50:
addu	$3,$9,$2
sw	$10,5184($19)
sll	$8,$8,1
lbu	$3,0($3)
sll	$7,$2,$3
addu	$2,$11,$3
sll	$3,$10,$3
.set	noreorder
.set	nomacro
bltz	$2,$L51
sw	$7,5168($19)
.set	macro
.set	reorder

lw	$4,5176($19)
lw	$5,5180($19)
sltu	$5,$4,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L51
addiu	$5,$4,2
.set	macro
.set	reorder

sw	$5,5176($19)
lbu	$5,1($4)
lbu	$4,0($4)
sll	$5,$5,8
or	$5,$5,$4
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$2
addiu	$2,$2,-16
or	$3,$3,$5
$L51:
addiu	$4,$7,1
sra	$4,$4,1
sll	$6,$4,16
sltu	$5,$3,$6
xori	$5,$5,0x1
.set	noreorder
.set	nomacro
beq	$5,$0,$L52
sw	$2,5172($19)
.set	macro
.set	reorder

subu	$4,$7,$4
subu	$3,$3,$6
$L52:
addu	$6,$9,$4
sw	$3,5184($19)
or	$5,$5,$8
sll	$8,$5,1
lbu	$5,0($6)
sll	$4,$4,$5
addu	$2,$2,$5
sll	$3,$3,$5
.set	noreorder
.set	nomacro
bltz	$2,$L53
sw	$4,5168($19)
.set	macro
.set	reorder

lw	$5,5176($19)
lw	$6,5180($19)
sltu	$6,$5,$6
.set	noreorder
.set	nomacro
beq	$6,$0,$L53
addiu	$6,$5,2
.set	macro
.set	reorder

sw	$6,5176($19)
lbu	$6,1($5)
lbu	$5,0($5)
sll	$6,$6,8
or	$6,$6,$5
sll	$7,$6,8
srl	$6,$6,8
or	$6,$7,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$3,$3,$6
$L53:
addiu	$5,$4,1
sra	$5,$5,1
sll	$7,$5,16
sltu	$6,$3,$7
xori	$6,$6,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L54
sw	$2,5172($19)
.set	macro
.set	reorder

subu	$5,$4,$5
subu	$3,$3,$7
$L54:
addu	$7,$9,$5
sw	$3,5184($19)
or	$4,$6,$8
sll	$4,$4,1
lbu	$6,0($7)
sll	$5,$5,$6
addu	$8,$2,$6
sll	$3,$3,$6
.set	noreorder
.set	nomacro
bltz	$8,$L55
sw	$5,5168($19)
.set	macro
.set	reorder

lw	$2,5176($19)
lw	$6,5180($19)
sltu	$6,$2,$6
.set	noreorder
.set	nomacro
beq	$6,$0,$L55
addiu	$6,$2,2
.set	macro
.set	reorder

sw	$6,5176($19)
lbu	$6,1($2)
lbu	$2,0($2)
sll	$6,$6,8
or	$6,$6,$2
sll	$7,$6,8
srl	$6,$6,8
or	$6,$7,$6
andi	$6,$6,0xffff
sll	$6,$6,$8
addiu	$8,$8,-16
or	$3,$3,$6
$L55:
addiu	$2,$5,1
sra	$6,$2,1
sll	$7,$6,16
sltu	$2,$3,$7
xori	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L56
sw	$8,5172($19)
.set	macro
.set	reorder

subu	$6,$5,$6
subu	$3,$3,$7
$L56:
or	$4,$2,$4
sw	$3,5184($19)
sll	$5,$17,4
sw	$6,5168($19)
sll	$3,$4,2
sll	$2,$17,2
sll	$4,$4,6
addu	$2,$2,$5
subu	$3,$4,$3
addu	$2,$2,$3
lui	$3,%hi(vp56_pre_def_mb_type_stats)
addiu	$3,$3,%lo(vp56_pre_def_mb_type_stats)
addu	$2,$2,$3
lw	$6,0($2)
lw	$5,4($2)
lw	$4,8($2)
lw	$3,12($2)
lw	$2,16($2)
swl	$6,1795($12)
swr	$6,1792($12)
swl	$5,1799($12)
swr	$5,1796($12)
swl	$4,1803($12)
swr	$4,1800($12)
swl	$3,1807($12)
swr	$3,1804($12)
swl	$2,1811($12)
swr	$2,1808($12)
lw	$2,5168($19)
lw	$5,5172($19)
lw	$4,5184($19)
$L212:
addu	$3,$9,$2
lbu	$3,0($3)
sll	$2,$2,$3
addu	$5,$3,$5
sll	$4,$4,$3
.set	noreorder
.set	nomacro
bltz	$5,$L57
sw	$2,5168($19)
.set	macro
.set	reorder

lw	$6,5176($19)
lw	$3,5180($19)
sltu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L57
addiu	$3,$6,2
.set	macro
.set	reorder

sw	$3,5176($19)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$5
addiu	$5,$5,-16
or	$4,$4,$3
$L57:
addiu	$6,$2,-1
sll	$3,$6,1
sll	$6,$6,8
subu	$6,$6,$3
sra	$6,$6,8
addiu	$6,$6,1
sll	$3,$6,16
sltu	$7,$4,$3
.set	noreorder
.set	nomacro
bne	$7,$0,$L58
sw	$5,5172($19)
.set	macro
.set	reorder

subu	$3,$4,$3
subu	$2,$2,$6
addiu	$10,$12,-20
sw	$3,5184($19)
$L59:
move	$11,$0
move	$8,$10
$L74:
addu	$4,$9,$2
lbu	$6,0($4)
sll	$2,$2,$6
addu	$4,$6,$5
sll	$6,$3,$6
.set	noreorder
.set	nomacro
bltz	$4,$L60
sw	$2,5168($19)
.set	macro
.set	reorder

lw	$5,5176($19)
lw	$3,5180($19)
sltu	$3,$5,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L361
addiu	$3,$2,-1
.set	macro
.set	reorder

addiu	$3,$5,2
sw	$3,5176($19)
lbu	$3,1($5)
lbu	$5,0($5)
sll	$3,$3,8
or	$3,$3,$5
sll	$5,$3,8
srl	$3,$3,8
or	$3,$5,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$6,$6,$3
$L60:
addiu	$3,$2,-1
$L361:
mul	$3,$3,$16
sra	$3,$3,8
addiu	$3,$3,1
sll	$5,$3,16
sltu	$7,$6,$5
.set	noreorder
.set	nomacro
bne	$7,$0,$L61
sw	$4,5172($19)
.set	macro
.set	reorder

subu	$2,$2,$3
subu	$5,$6,$5
addu	$3,$9,$2
sw	$5,5184($19)
lbu	$3,0($3)
sll	$2,$2,$3
addu	$4,$4,$3
sll	$5,$5,$3
.set	noreorder
.set	nomacro
bltz	$4,$L62
sw	$2,5168($19)
.set	macro
.set	reorder

lw	$6,5176($19)
lw	$3,5180($19)
sltu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L362
addiu	$3,$2,1
.set	macro
.set	reorder

addiu	$3,$6,2
sw	$3,5176($19)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$5,$5,$3
$L62:
addiu	$3,$2,1
$L362:
sw	$4,5172($19)
sra	$3,$3,1
sll	$4,$3,16
sltu	$7,$5,$4
xori	$7,$7,0x1
beq	$7,$0,$L63
subu	$3,$2,$3
subu	$5,$5,$4
$L63:
lui	$2,%hi(vp56_pmbtm_tree)
sw	$3,5168($19)
sw	$5,5184($19)
addiu	$6,$2,%lo(vp56_pmbtm_tree)
$L64:
lb	$2,0($6)
.set	noreorder
.set	nomacro
blez	$2,$L363
subu	$2,$0,$2
.set	macro
.set	reorder

$L67:
lw	$3,5168($19)
lui	$14,%hi(vp56_mb_type_model_model)
lb	$13,1($6)
addiu	$14,$14,%lo(vp56_mb_type_model_model)
lw	$4,5172($19)
addu	$5,$9,$3
lw	$2,5184($19)
addu	$13,$14,$13
lbu	$5,0($5)
lbu	$14,0($13)
sll	$3,$3,$5
addu	$4,$5,$4
sll	$5,$2,$5
.set	noreorder
.set	nomacro
bltz	$4,$L65
sw	$3,5168($19)
.set	macro
.set	reorder

lw	$21,5176($19)
lw	$2,5180($19)
sltu	$2,$21,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L364
addiu	$2,$3,-1
.set	macro
.set	reorder

addiu	$2,$21,2
sw	$2,5176($19)
lbu	$13,1($21)
lbu	$2,0($21)
sll	$13,$13,8
or	$13,$13,$2
sll	$2,$13,8
srl	$13,$13,8
or	$13,$2,$13
andi	$2,$13,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L65:
addiu	$2,$3,-1
$L364:
sw	$4,5172($19)
mul	$2,$2,$14
sra	$2,$2,8
addiu	$2,$2,1
sll	$4,$2,16
sltu	$13,$5,$4
bne	$13,$0,$L66
subu	$2,$3,$2
subu	$5,$5,$4
sw	$2,5168($19)
sw	$5,5184($19)
lb	$2,0($6)
sll	$2,$2,1
addu	$6,$6,$2
lb	$2,0($6)
.set	noreorder
.set	nomacro
bgtz	$2,$L67
subu	$2,$0,$2
.set	macro
.set	reorder

$L363:
beq	$2,$0,$L345
$L68:
lbu	$3,1812($8)
subu	$4,$0,$7
xor	$2,$2,$4
addu	$7,$7,$3
addu	$2,$7,$2
sb	$2,1812($8)
$L213:
.set	noreorder
.set	nomacro
bne	$11,$15,$L72
addiu	$8,$8,1
.set	macro
.set	reorder

addiu	$10,$10,2
beq	$10,$12,$L73
lw	$2,5168($19)
lw	$5,5172($19)
.option	pic0
.set	noreorder
.set	nomacro
j	$L59
.option	pic2
lw	$3,5184($19)
.set	macro
.set	reorder

$L66:
sw	$2,5168($19)
addiu	$6,$6,2
.option	pic0
.set	noreorder
.set	nomacro
j	$L64
.option	pic2
sw	$5,5184($19)
.set	macro
.set	reorder

$L72:
lw	$2,5168($19)
li	$11,1			# 0x1
lw	$5,5172($19)
.option	pic0
.set	noreorder
.set	nomacro
j	$L74
.option	pic2
lw	$3,5184($19)
.set	macro
.set	reorder

$L61:
sw	$3,5168($19)
.option	pic0
.set	noreorder
.set	nomacro
j	$L213
.option	pic2
sw	$6,5184($19)
.set	macro
.set	reorder

$L345:
lw	$13,5172($19)
li	$14,7			# 0x7
lw	$6,5184($19)
lw	$4,5168($19)
move	$5,$13
move	$3,$6
$L71:
addu	$13,$9,$4
sll	$6,$2,1
lbu	$2,0($13)
sll	$13,$4,$2
addu	$5,$2,$5
sll	$3,$3,$2
.set	noreorder
.set	nomacro
bltz	$5,$L69
sw	$13,5168($19)
.set	macro
.set	reorder

lw	$4,5176($19)
lw	$2,5180($19)
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L69
addiu	$2,$4,2
.set	macro
.set	reorder

sw	$2,5176($19)
lbu	$2,1($4)
lbu	$4,0($4)
sll	$2,$2,8
or	$2,$2,$4
sll	$4,$2,8
srl	$2,$2,8
or	$2,$4,$2
andi	$2,$2,0xffff
sll	$2,$2,$5
addiu	$5,$5,-16
or	$3,$3,$2
$L69:
addiu	$4,$13,1
sra	$4,$4,1
sll	$21,$4,16
sltu	$2,$3,$21
xori	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L70
sw	$5,5172($19)
.set	macro
.set	reorder

subu	$4,$13,$4
subu	$3,$3,$21
$L70:
addiu	$14,$14,-1
sw	$4,5168($19)
sw	$3,5184($19)
.set	noreorder
.set	nomacro
bne	$14,$0,$L71
or	$2,$2,$6
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L68
.option	pic2
sll	$2,$2,2
.set	macro
.set	reorder

$L58:
sw	$6,5168($19)
sw	$4,5184($19)
$L73:
addiu	$17,$17,1
.set	noreorder
.set	nomacro
bne	$17,$22,$L75
addiu	$12,$12,20
.set	macro
.set	reorder

addiu	$2,$18,1512
sw	$18,100($sp)
addiu	$3,$18,60
sw	$19,128($sp)
addiu	$4,$sp,48
addiu	$5,$sp,88
sw	$2,116($sp)
sw	$3,120($sp)
sw	$4,124($sp)
sw	$5,108($sp)
$L76:
lw	$7,100($sp)
addiu	$31,$sp,48
lw	$4,116($sp)
move	$12,$7
lbu	$3,1813($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,48($sp)
lbu	$3,1815($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,52($sp)
lbu	$3,1817($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,56($sp)
lbu	$3,1819($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,60($sp)
lbu	$3,1821($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,64($sp)
lbu	$3,1823($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,68($sp)
lbu	$3,1825($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,72($sp)
lbu	$3,1827($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,76($sp)
lbu	$3,1829($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,80($sp)
lbu	$3,1831($7)
sll	$2,$3,2
sll	$3,$3,4
addu	$2,$2,$3
sll	$3,$2,2
addu	$2,$2,$3
sw	$2,84($sp)
$L77:
lbu	$6,1812($12)
addiu	$4,$4,10
lbu	$3,1813($12)
addiu	$31,$31,4
addiu	$12,$12,2
sll	$2,$6,8
nor	$5,$0,$6
subu	$2,$2,$6
subu	$5,$5,$3
sll	$6,$3,2
teq	$5,$0,7
div	$0,$2,$5
sll	$3,$3,4
addu	$3,$6,$3
sll	$5,$3,2
addu	$5,$3,$5
sw	$5,96($sp)
mflo	$2
addiu	$2,$2,-1
sb	$2,-10($4)
sw	$0,-4($31)
lw	$8,48($sp)
lw	$6,60($sp)
lw	$3,68($sp)
lw	$2,80($sp)
sll	$fp,$8,8
lw	$9,56($sp)
sll	$25,$6,8
lw	$14,64($sp)
sll	$24,$3,8
lw	$5,72($sp)
sll	$23,$2,8
lw	$10,84($sp)
addu	$9,$8,$9
lw	$22,52($sp)
addu	$14,$6,$14
lw	$7,76($sp)
addu	$5,$3,$5
addu	$10,$2,$10
addu	$17,$9,$14
addu	$7,$22,$7
addu	$18,$5,$10
addiu	$20,$17,1
addu	$19,$7,$18
sll	$21,$17,8
sll	$16,$9,8
subu	$17,$21,$17
addu	$21,$19,$20
subu	$16,$16,$9
teq	$21,$0,7
div	$0,$17,$21
sll	$15,$7,8
addiu	$19,$19,1
subu	$15,$15,$7
subu	$8,$fp,$8
addiu	$9,$9,1
subu	$6,$25,$6
addiu	$14,$14,1
sll	$13,$22,8
addiu	$7,$7,1
subu	$13,$13,$22
sll	$11,$5,8
addiu	$18,$18,1
subu	$11,$11,$5
subu	$3,$24,$3
addiu	$5,$5,1
subu	$2,$23,$2
addiu	$10,$10,1
mflo	$17
teq	$20,$0,7
div	$0,$16,$20
addiu	$17,$17,1
sb	$17,-9($4)
mflo	$16
teq	$19,$0,7
div	$0,$15,$19
addiu	$16,$16,1
sb	$16,-8($4)
mflo	$15
teq	$9,$0,7
div	$0,$8,$9
addiu	$15,$15,1
sb	$15,-7($4)
mflo	$8
teq	$14,$0,7
div	$0,$6,$14
addiu	$8,$8,1
sb	$8,-6($4)
mflo	$6
teq	$7,$0,7
div	$0,$13,$7
addiu	$6,$6,1
sb	$6,-5($4)
mflo	$7
teq	$18,$0,7
div	$0,$11,$18
addiu	$7,$7,1
sb	$7,-4($4)
mflo	$11
teq	$5,$0,7
div	$0,$3,$5
addiu	$11,$11,1
sb	$11,-3($4)
mflo	$3
teq	$10,$0,7
div	$0,$2,$10
addiu	$3,$3,1
sb	$3,-2($4)
lw	$3,108($sp)
mflo	$2
addiu	$2,$2,1
sb	$2,-1($4)
lw	$2,96($sp)
.set	noreorder
.set	nomacro
bne	$3,$31,$L77
sw	$2,-4($31)
.set	macro
.set	reorder

lw	$4,100($sp)
lw	$5,116($sp)
lw	$6,120($sp)
addiu	$4,$4,20
addiu	$5,$5,100
sw	$4,100($sp)
.set	noreorder
.set	nomacro
bne	$4,$6,$L76
sw	$5,116($sp)
.set	macro
.set	reorder

lw	$19,128($sp)
lw	$25,6548($19)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$19
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L45
.option	pic2
sw	$0,5388($19)
.set	macro
.set	reorder

$L48:
sw	$4,5184($19)
.option	pic0
.set	noreorder
.set	nomacro
j	$L212
.option	pic2
move	$5,$3
.set	macro
.set	reorder

$L324:
li	$2,1			# 0x1
lw	$5,144($sp)
move	$4,$9
lw	$25,256($9)
.set	noreorder
.set	nomacro
jalr	$25
sw	$2,80($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L346
lw	$28,40($sp)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$16,$2,$L365
lw	$3,144($sp)
.set	macro
.set	reorder

lw	$10,164($sp)
lw	$17,136($10)
lw	$7,660($10)
lw	$6,664($10)
lw	$4,5136($17)
addiu	$3,$7,15
lw	$8,6500($17)
addiu	$2,$6,15
addiu	$15,$7,30
addiu	$13,$6,30
lw	$18,16($4)
slt	$16,$3,0
lw	$11,20($4)
slt	$14,$2,0
lw	$10,24($4)
srl	$5,$7,31
lw	$9,28($4)
mul	$12,$18,$8
movn	$3,$15,$16
mul	$11,$8,$11
mul	$10,$8,$10
srl	$4,$6,31
movn	$2,$13,$14
mul	$8,$8,$9
addu	$5,$5,$7
sw	$7,5228($17)
addu	$4,$4,$6
sw	$7,5216($17)
sra	$2,$2,4
sw	$6,5244($17)
sra	$7,$5,1
sw	$6,5232($17)
sra	$4,$4,1
sw	$12,6512($17)
sra	$5,$3,4
sw	$11,6516($17)
sw	$7,5224($17)
slt	$3,$5,1001
sw	$7,5220($17)
sw	$4,5240($17)
sw	$4,5236($17)
sw	$10,6520($17)
sw	$8,6524($17)
sw	$5,5248($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L40
sw	$2,5252($17)
.set	macro
.set	reorder

slt	$2,$2,1001
.set	noreorder
.set	nomacro
beq	$2,$0,$L40
lw	$25,%call16(jz4740_alloc_frame_k0)($28)
.set	macro
.set	reorder

li	$4,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame_k0
1:	jalr	$25
sll	$16,$18,4
.set	macro
.set	reorder

li	$4,16			# 0x10
lw	$3,5248($17)
lw	$28,40($sp)
sw	$2,5288($17)
sll	$2,$3,2
addiu	$2,$2,6
lw	$25,%call16(jz4740_alloc_frame_k0)($28)
sll	$5,$2,2
sll	$2,$2,4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame_k0
1:	jalr	$25
subu	$5,$2,$5
.set	macro
.set	reorder

li	$4,16			# 0x10
lw	$3,5248($17)
lw	$5,5252($17)
lw	$28,40($sp)
sw	$2,5292($17)
mul	$5,$3,$5
lw	$25,%call16(jz4740_alloc_frame_k0)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame_k0
1:	jalr	$25
sll	$5,$5,3
.set	macro
.set	reorder

lw	$28,40($sp)
lw	$4,5160($17)
lw	$25,%call16(av_free)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
sw	$2,5392($17)
.set	macro
.set	reorder

li	$4,16			# 0x10
lw	$28,40($sp)
lw	$25,%call16(jz4740_alloc_frame_k0)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame_k0
1:	jalr	$25
move	$5,$16
.set	macro
.set	reorder

lw	$3,6500($17)
lw	$28,40($sp)
sw	$2,5160($17)
.set	noreorder
.set	nomacro
bgez	$3,$L206
sw	$2,5164($17)
.set	macro
.set	reorder

subu	$16,$16,$18
addu	$2,$2,$16
.option	pic0
.set	noreorder
.set	nomacro
j	$L206
.option	pic2
sw	$2,5164($17)
.set	macro
.set	reorder

$L142:
addiu	$3,$3,1
sra	$3,$3,2
sh	$3,6196($20)
.set	noreorder
.set	nomacro
bgtz	$2,$L347
sh	$3,6192($20)
.set	macro
.set	reorder

$L144:
addiu	$2,$2,1
sra	$2,$2,2
sh	$2,6198($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L348
.option	pic2
sh	$2,6194($20)
.set	macro
.set	reorder

$L238:
li	$10,8			# 0x8
li	$2,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L178
.option	pic2
move	$24,$0
.set	macro
.set	reorder

$L334:
.option	pic0
.set	noreorder
.set	nomacro
j	$L154
.option	pic2
li	$9,2			# 0x2
.set	macro
.set	reorder

$L239:
li	$10,8			# 0x8
li	$2,3			# 0x3
.option	pic0
.set	noreorder
.set	nomacro
j	$L178
.option	pic2
li	$24,8			# 0x8
.set	macro
.set	reorder

$L346:
lui	$6,%hi($LC0)
lw	$25,%call16(av_log)($28)
lw	$4,164($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)
.set	macro
.set	reorder

$L222:
lw	$31,244($sp)
$L350:
li	$2,-1			# 0xffffffffffffffff
lw	$fp,240($sp)
lw	$23,236($sp)
lw	$22,232($sp)
lw	$21,228($sp)
lw	$20,224($sp)
lw	$19,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,248
.set	macro
.set	reorder

$L201:
lw	$2,5152($20)
beq	$3,$2,$L203
lw	$3,5148($20)
beq	$2,$3,$L203
sw	$5,5152($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L204
.option	pic2
move	$5,$2
.set	macro
.set	reorder

$L203:
lw	$2,5156($20)
sw	$5,5156($20)
.option	pic0
.set	noreorder
.set	nomacro
j	$L204
.option	pic2
move	$5,$2
.set	macro
.set	reorder

$L40:
lui	$6,%hi($LC1)
lw	$25,%call16(av_log)($28)
lw	$4,164($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC1)
.set	macro
.set	reorder

lw	$4,164($sp)
lw	$25,260($4)
.set	noreorder
.set	nomacro
jalr	$25
lw	$5,144($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L350
.option	pic2
lw	$31,244($sp)
.set	macro
.set	reorder

.end	ff_vp56_decode_frame
.size	ff_vp56_decode_frame, .-ff_vp56_decode_frame
.section	.text.unlikely,"ax",@progbits
.align	2
.globl	ff_vp56_init
.set	nomips16
.set	nomicromips
.ent	ff_vp56_init
.type	ff_vp56_init, @function
ff_vp56_init:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-48
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$19,40($sp)
li	$2,3			# 0x3
sw	$18,36($sp)
move	$19,$5
sw	$17,32($sp)
move	$18,$6
sw	$16,28($sp)
move	$17,$4
sw	$31,44($sp)
.cprestore	16
lw	$16,136($4)
#APP
# 1336 "vp56.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
li	$2,-1288962048			# 0xffffffffb32c0000
li	$3,48			# 0x30
addiu	$2,$2,4092
$L368:
#APP
# 1345 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 1346 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 1347 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 1348 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
bne	$3,$0,$L368
li	$2,-201326592			# 0xfffffffff4000000
li	$3,48			# 0x30
addiu	$2,$2,4092
$L369:
#APP
# 1354 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 1355 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 1356 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 1357 "vp56.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
bne	$3,$0,$L369
li	$2,35			# 0x23
lw	$3,364($17)
sw	$17,0($16)
movz	$2,$0,$18
.set	noreorder
.set	nomacro
bne	$3,$0,$L371
sw	$2,52($17)
.set	macro
.set	reorder

li	$2,12			# 0xc
sw	$2,364($17)
$L371:
lw	$25,%call16(dsputil_init)($28)
addiu	$4,$16,4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

addiu	$4,$16,4124
lw	$2,132($17)
lw	$28,16($sp)
lw	$25,%call16(ff_vp56dsp_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_vp56dsp_init
1:	jalr	$25
lw	$5,8($2)
.set	macro
.set	reorder

addiu	$4,$16,2708
lw	$28,16($sp)
lw	$25,%call16(ff_init_scantable)($28)
lw	$6,%got(ff_zigzag_direct)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
addiu	$5,$16,4136
.set	macro
.set	reorder

addiu	$3,$16,4704
addiu	$2,$16,4920
sw	$0,5160($16)
addiu	$7,$16,4272
sw	$0,5292($16)
addiu	$5,$16,4488
sw	$3,5144($16)
li	$6,-1			# 0xffffffffffffffff
sw	$2,5148($16)
li	$4,1			# 0x1
sw	$7,5136($16)
sw	$5,5140($16)
sw	$3,5152($16)
sw	$2,5156($16)
sw	$0,5392($16)
sw	$6,5280($16)
sw	$4,6216($16)
sw	$0,6536($16)
.set	noreorder
.set	nomacro
beq	$19,$0,$L372
sw	$18,6496($16)
.set	macro
.set	reorder

li	$2,2			# 0x2
sw	$6,6500($16)
sw	$0,6508($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L367
.option	pic2
sw	$2,6504($16)
.set	macro
.set	reorder

$L372:
li	$2,2			# 0x2
sw	$4,6500($16)
sw	$0,6504($16)
sw	$2,6508($16)
$L367:
lw	$31,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

.end	ff_vp56_init
.size	ff_vp56_init, .-ff_vp56_init
.align	2
.globl	ff_vp56_free
.set	nomips16
.set	nomicromips
.ent	ff_vp56_free
.type	ff_vp56_free, @function
ff_vp56_free:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-40
sw	$17,32($sp)
lw	$17,136($4)
sw	$16,28($sp)
sw	$31,36($sp)
lw	$5,5144($17)
lw	$2,0($5)
beq	$2,$0,$L379
move	$16,$4

lw	$25,260($4)
jalr	$25
nop

$L379:
lw	$5,5148($17)
lw	$2,0($5)
beq	$2,$0,$L380
nop

lw	$25,260($16)
jalr	$25
move	$4,$16

$L380:
lw	$5,5140($17)
lw	$2,0($5)
beq	$2,$0,$L392
lw	$31,36($sp)

lw	$25,260($16)
jalr	$25
move	$4,$16

lw	$31,36($sp)
$L392:
move	$2,$0
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	ff_vp56_free
.size	ff_vp56_free, .-ff_vp56_free
.data
.align	2
.type	whirl_idct, @object
.size	whirl_idct, 24
whirl_idct:
.word	1518499394
.word	1518481660
.word	2106026606
.word	418989853
.word	1785600249
.word	1193115015

.comm	blk_coeff,4,4

.comm	idct_row,6,4
.rdata
.align	2
.type	vp56_candidate_predictor_pos, @object
.size	vp56_candidate_predictor_pos, 24
vp56_candidate_predictor_pos:
.byte	0
.byte	-1
.byte	-1
.byte	0
.byte	-1
.byte	-1
.byte	1
.byte	-1
.byte	0
.byte	-2
.byte	-2
.byte	0
.byte	-2
.byte	-1
.byte	-1
.byte	-2
.byte	1
.byte	-2
.byte	2
.byte	-1
.byte	-2
.byte	-2
.byte	2
.byte	-2
.align	2
.type	vp56_pmbt_tree, @object
.size	vp56_pmbt_tree, 38
vp56_pmbt_tree:
.byte	8
.byte	1
.byte	4
.byte	2
.byte	2
.byte	4
.byte	0
.space	1
.byte	-2
.space	1
.byte	2
.byte	5
.byte	-3
.space	1
.byte	-4
.space	1
.byte	4
.byte	3
.byte	2
.byte	6
.byte	-1
.space	1
.byte	-7
.space	1
.byte	4
.byte	7
.byte	2
.byte	8
.byte	-5
.space	1
.byte	-6
.space	1
.byte	2
.byte	9
.byte	-8
.space	1
.byte	-9
.space	1
.align	2
.type	vp56_pmbtm_tree, @object
.size	vp56_pmbtm_tree, 26
vp56_pmbtm_tree:
.byte	4
.byte	0
.byte	2
.byte	1
.byte	-8
.space	1
.byte	-4
.space	1
.byte	8
.byte	2
.byte	6
.byte	3
.byte	4
.byte	4
.byte	2
.byte	5
.byte	-24
.space	1
.byte	-20
.space	1
.byte	-16
.space	1
.byte	-12
.space	1
.byte	0
.space	1
.align	2
.type	vp56_mb_type_model_model, @object
.size	vp56_mb_type_model_model, 6
vp56_mb_type_model_model:
.byte	-85
.byte	83
.byte	-57
.byte	-116
.byte	125
.byte	104
.align	2
.type	vp56_pre_def_mb_type_stats, @object
.size	vp56_pre_def_mb_type_stats, 960
vp56_pre_def_mb_type_stats:
.byte	9
.byte	15
.byte	32
.byte	25
.byte	7
.byte	19
.byte	9
.byte	21
.byte	1
.byte	12
.byte	14
.byte	12
.byte	3
.byte	18
.byte	14
.byte	23
.byte	3
.byte	10
.byte	0
.byte	4
.byte	41
.byte	22
.byte	1
.byte	0
.byte	1
.byte	31
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	7
.byte	0
.byte	1
.byte	98
.byte	25
.byte	4
.byte	10
.byte	2
.byte	3
.byte	2
.byte	3
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	11
.byte	4
.byte	1
.byte	4
.byte	0
.byte	2
.byte	3
.byte	2
.byte	0
.byte	4
.byte	48
.byte	39
.byte	1
.byte	2
.byte	11
.byte	27
.byte	29
.byte	44
.byte	7
.byte	27
.byte	1
.byte	4
.byte	0
.byte	3
.byte	1
.byte	6
.byte	1
.byte	2
.byte	0
.byte	0
.byte	123
.byte	37
.byte	6
.byte	4
.byte	1
.byte	27
.byte	0
.byte	0
.byte	0
.byte	0
.byte	5
.byte	8
.byte	1
.byte	7
.byte	0
.byte	1
.byte	12
.byte	10
.byte	0
.byte	2
.byte	49
.byte	46
.byte	3
.byte	4
.byte	7
.byte	31
.byte	42
.byte	41
.byte	0
.byte	0
.byte	2
.byte	6
.byte	1
.byte	7
.byte	1
.byte	4
.byte	2
.byte	4
.byte	0
.byte	1
.byte	21
.byte	32
.byte	1
.byte	2
.byte	4
.byte	10
.byte	32
.byte	43
.byte	6
.byte	23
.byte	2
.byte	3
.byte	1
.byte	19
.byte	1
.byte	6
.byte	12
.byte	21
.byte	0
.byte	7
.byte	26
.byte	14
.byte	14
.byte	12
.byte	0
.byte	24
.byte	0
.byte	0
.byte	0
.byte	0
.byte	55
.byte	17
.byte	1
.byte	9
.byte	0
.byte	36
.byte	5
.byte	7
.byte	1
.byte	3
.byte	26
.byte	25
.byte	1
.byte	1
.byte	2
.byte	10
.byte	67
.byte	39
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	14
.byte	0
.byte	2
.byte	31
.byte	26
.byte	1
.byte	6
.byte	69
.byte	83
.byte	0
.byte	0
.byte	0
.byte	2
.byte	10
.byte	29
.byte	3
.byte	12
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	3
.byte	2
.byte	2
.byte	0
.byte	0
.byte	-47
.byte	5
.byte	0
.byte	0
.byte	0
.byte	27
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	103
.byte	46
.byte	1
.byte	2
.byte	2
.byte	10
.byte	33
.byte	42
.byte	0
.byte	0
.byte	1
.byte	4
.byte	0
.byte	3
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	11
.byte	20
.byte	1
.byte	4
.byte	18
.byte	36
.byte	43
.byte	48
.byte	13
.byte	35
.byte	0
.byte	2
.byte	0
.byte	5
.byte	3
.byte	12
.byte	1
.byte	2
.byte	0
.byte	0
.byte	2
.byte	5
.byte	4
.byte	5
.byte	0
.byte	121
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	2
.byte	4
.byte	1
.byte	4
.byte	2
.byte	2
.byte	0
.byte	1
.byte	14
.byte	31
.byte	9
.byte	13
.byte	14
.byte	54
.byte	22
.byte	29
.byte	0
.byte	0
.byte	2
.byte	6
.byte	4
.byte	18
.byte	6
.byte	13
.byte	1
.byte	5
.byte	0
.byte	1
.byte	70
.byte	44
.byte	0
.byte	1
.byte	2
.byte	10
.byte	37
.byte	46
.byte	8
.byte	26
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-81
.byte	5
.byte	0
.byte	1
.byte	0
.byte	48
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	85
.byte	39
.byte	0
.byte	0
.byte	1
.byte	9
.byte	69
.byte	40
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	2
.byte	3
.byte	0
.byte	0
.byte	8
.byte	15
.byte	0
.byte	1
.byte	8
.byte	21
.byte	74
.byte	53
.byte	22
.byte	42
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	3
.byte	1
.byte	2
.byte	0
.byte	0
.byte	83
.byte	5
.byte	2
.byte	3
.byte	0
.byte	102
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	31
.byte	28
.byte	0
.byte	0
.byte	3
.byte	14
.byte	-126
.byte	34
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	3
.byte	3
.byte	0
.byte	1
.byte	-115
.byte	42
.byte	0
.byte	0
.byte	1
.byte	4
.byte	11
.byte	24
.byte	1
.byte	11
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-23
.byte	6
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-85
.byte	25
.byte	0
.byte	0
.byte	1
.byte	5
.byte	25
.byte	21
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	19
.byte	4
.byte	10
.byte	24
.byte	45
.byte	21
.byte	37
.byte	9
.byte	29
.byte	0
.byte	3
.byte	1
.byte	7
.byte	11
.byte	25
.byte	0
.byte	2
.byte	0
.byte	1
.byte	34
.byte	16
.byte	112
.byte	21
.byte	1
.byte	28
.byte	0
.byte	0
.byte	0
.byte	0
.byte	6
.byte	8
.byte	1
.byte	7
.byte	0
.byte	3
.byte	2
.byte	5
.byte	0
.byte	2
.byte	17
.byte	21
.byte	68
.byte	29
.byte	6
.byte	15
.byte	13
.byte	22
.byte	0
.byte	0
.byte	6
.byte	12
.byte	3
.byte	14
.byte	4
.byte	10
.byte	1
.byte	7
.byte	0
.byte	3
.byte	46
.byte	42
.byte	0
.byte	1
.byte	2
.byte	10
.byte	54
.byte	51
.byte	10
.byte	30
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-97
.byte	35
.byte	2
.byte	2
.byte	0
.byte	25
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	6
.byte	0
.byte	5
.byte	0
.byte	1
.byte	4
.byte	4
.byte	0
.byte	1
.byte	51
.byte	39
.byte	0
.byte	1
.byte	2
.byte	12
.byte	91
.byte	44
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	3
.byte	0
.byte	1
.byte	2
.byte	3
.byte	0
.byte	1
.byte	28
.byte	32
.byte	0
.byte	0
.byte	3
.byte	10
.byte	75
.byte	51
.byte	14
.byte	33
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	2
.byte	0
.byte	0
.byte	75
.byte	39
.byte	5
.byte	7
.byte	2
.byte	48
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	11
.byte	2
.byte	16
.byte	1
.byte	4
.byte	7
.byte	10
.byte	0
.byte	2
.byte	81
.byte	25
.byte	0
.byte	0
.byte	2
.byte	9
.byte	106
.byte	26
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	100
.byte	46
.byte	0
.byte	1
.byte	3
.byte	9
.byte	21
.byte	37
.byte	5
.byte	20
.byte	0
.byte	1
.byte	0
.byte	2
.byte	1
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-44
.byte	21
.byte	0
.byte	1
.byte	0
.byte	9
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	2
.byte	2
.byte	0
.byte	0
.byte	-116
.byte	37
.byte	0
.byte	1
.byte	1
.byte	8
.byte	24
.byte	33
.byte	0
.byte	0
.byte	1
.byte	2
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	2
.byte	0
.byte	0
.byte	27
.byte	29
.byte	0
.byte	1
.byte	9
.byte	25
.byte	53
.byte	51
.byte	12
.byte	34
.byte	0
.byte	1
.byte	0
.byte	3
.byte	1
.byte	5
.byte	0
.byte	2
.byte	0
.byte	0
.byte	4
.byte	2
.byte	0
.byte	0
.byte	0
.byte	-84
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	14
.byte	23
.byte	1
.byte	3
.byte	11
.byte	53
.byte	90
.byte	31
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	5
.byte	2
.byte	6
.byte	1
.byte	2
.byte	0
.byte	0
.byte	80
.byte	38
.byte	0
.byte	0
.byte	1
.byte	4
.byte	69
.byte	33
.byte	5
.byte	16
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-69
.byte	22
.byte	1
.byte	1
.byte	0
.byte	17
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	6
.byte	0
.byte	4
.byte	0
.byte	1
.byte	4
.byte	4
.byte	0
.byte	1
.byte	123
.byte	29
.byte	0
.byte	0
.byte	1
.byte	7
.byte	57
.byte	30
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	16
.byte	20
.byte	0
.byte	0
.byte	2
.byte	8
.byte	104
.byte	49
.byte	15
.byte	33
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	-123
.byte	6
.byte	1
.byte	2
.byte	1
.byte	70
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	4
.byte	0
.byte	3
.byte	1
.byte	1
.byte	0
.byte	0
.byte	13
.byte	14
.byte	0
.byte	0
.byte	4
.byte	20
.byte	-81
.byte	20
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	-62
.byte	16
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	9
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-54
.byte	23
.byte	0
.byte	0
.byte	1
.byte	3
.byte	2
.byte	9
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.align	2
.type	vp56_dc_dequant, @object
.size	vp56_dc_dequant, 64
vp56_dc_dequant:
.byte	47
.byte	47
.byte	47
.byte	47
.byte	45
.byte	43
.byte	43
.byte	43
.byte	43
.byte	43
.byte	42
.byte	41
.byte	41
.byte	40
.byte	40
.byte	40
.byte	40
.byte	35
.byte	35
.byte	35
.byte	35
.byte	33
.byte	33
.byte	33
.byte	33
.byte	32
.byte	32
.byte	32
.byte	27
.byte	27
.byte	26
.byte	26
.byte	25
.byte	25
.byte	24
.byte	24
.byte	23
.byte	23
.byte	19
.byte	19
.byte	19
.byte	19
.byte	18
.byte	18
.byte	17
.byte	16
.byte	16
.byte	16
.byte	16
.byte	16
.byte	15
.byte	11
.byte	11
.byte	11
.byte	10
.byte	10
.byte	9
.byte	8
.byte	7
.byte	5
.byte	3
.byte	3
.byte	2
.byte	2
.align	2
.type	vp56_ac_dequant, @object
.size	vp56_ac_dequant, 64
vp56_ac_dequant:
.byte	94
.byte	92
.byte	90
.byte	88
.byte	86
.byte	82
.byte	78
.byte	74
.byte	70
.byte	66
.byte	62
.byte	58
.byte	54
.byte	53
.byte	52
.byte	51
.byte	50
.byte	49
.byte	48
.byte	47
.byte	46
.byte	45
.byte	44
.byte	43
.byte	42
.byte	40
.byte	39
.byte	37
.byte	36
.byte	35
.byte	34
.byte	33
.byte	32
.byte	31
.byte	30
.byte	29
.byte	28
.byte	27
.byte	26
.byte	25
.byte	24
.byte	23
.byte	22
.byte	21
.byte	20
.byte	19
.byte	18
.byte	17
.byte	16
.byte	15
.byte	14
.byte	13
.byte	12
.byte	11
.byte	10
.byte	9
.byte	8
.byte	7
.byte	6
.byte	5
.byte	4
.byte	3
.byte	2
.byte	1
.align	2
.type	vp56_reference_frame, @object
.size	vp56_reference_frame, 40
vp56_reference_frame:
.word	1
.word	0
.word	1
.word	1
.word	1
.word	2
.word	2
.word	1
.word	2
.word	2
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
