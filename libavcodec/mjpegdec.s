	.file	1 "mjpegdec.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.abicalls
	.section	.text.unlikely,"ax",@progbits
	.align	2
	.globl	ff_mjpeg_decode_end
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_end
	.type	ff_mjpeg_decode_end, @function
ff_mjpeg_decode_end:
	.frame	$sp,40,$31		# vars= 0, regs= 4/0, args= 16, gp= 8
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	lui	$28,%hi(__gnu_local_gp)
	sw	$17,28($sp)
	lw	$17,136($4)
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$31,36($sp)
	sw	$18,32($sp)
	sw	$16,24($sp)
	.cprestore	16
	lw	$3,976($17)
	beq	$3,$0,$L10
	lw	$25,%call16(av_free)($28)

	lw	$25,260($4)
	jalr	$25
	addiu	$5,$17,976

	lw	$28,16($sp)
	lw	$25,%call16(av_free)($28)
$L10:
	addiu	$16,$17,1344
	lw	$4,28($17)
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	addiu	$18,$17,1360

	lw	$28,16($sp)
	lw	$25,%call16(av_free)($28)
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,1212($17)

	lw	$28,16($sp)
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addiu	$4,$17,5692

	addiu	$4,$17,544
	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	sw	$0,5696($17)

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,560

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,576

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,592

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,608

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,624

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,640

	lw	$28,16($sp)
	lw	$25,%call16(free_vlc)($28)
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	addiu	$4,$17,656

	lw	$28,16($sp)
$L3:
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	move	$4,$16

	addiu	$4,$16,16
	lw	$28,16($sp)
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addiu	$16,$16,4

	bne	$16,$18,$L3
	lw	$28,16($sp)

	lw	$31,36($sp)
	move	$2,$0
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	ff_mjpeg_decode_end
	.size	ff_mjpeg_decode_end, .-ff_mjpeg_decode_end
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	build_vlc.constprop.6
	.type	build_vlc.constprop.6, @function
build_vlc.constprop.6:
	.frame	$sp,912,$31		# vars= 816, regs= 7/0, args= 56, gp= 8
	.mask	0x803f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-912
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$16,884($sp)
	addiu	$16,$sp,608
	sw	$21,904($sp)
	move	$21,$5
	sw	$20,900($sp)
	move	$5,$0
	sw	$31,908($sp)
	move	$20,$6
	sw	$19,896($sp)
	li	$6,272			# 0x110
	sw	$18,892($sp)
	move	$19,$4
	sw	$17,888($sp)
	move	$4,$16
	.cprestore	56
	move	$18,$7
	lw	$25,%call16(memset)($28)
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	addiu	$17,$sp,64

	move	$4,$16
	lw	$28,56($sp)
	move	$5,$17
	move	$6,$21
	lw	$25,%call16(ff_mjpeg_build_huffman_codes)($28)
	.reloc	1f,R_MIPS_JALR,ff_mjpeg_build_huffman_codes
1:	jalr	$25
	move	$7,$20

	lw	$2,928($sp)
	beq	$2,$0,$L12
	lw	$28,56($sp)

	lw	$25,%call16(memmove)($28)
	addiu	$4,$sp,624
	move	$6,$18
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	move	$5,$16

	sll	$6,$18,1
	lw	$28,56($sp)
	addiu	$4,$sp,96
	move	$5,$17
	lw	$25,%call16(memmove)($28)
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	addiu	$18,$18,16

	move	$5,$0
	lw	$28,56($sp)
	li	$6,32			# 0x20
	sw	$0,608($sp)
	move	$4,$17
	sw	$0,4($16)
	sw	$0,8($16)
	lw	$25,%call16(memset)($28)
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$0,12($16)

	lw	$28,56($sp)
$L12:
	li	$3,1			# 0x1
	lw	$25,%call16(init_vlc_sparse)($28)
	li	$2,2			# 0x2
	sw	$17,24($sp)
	li	$5,9			# 0x9
	sw	$0,36($sp)
	sw	$3,16($sp)
	move	$4,$19
	sw	$3,20($sp)
	move	$6,$18
	move	$7,$16
	sw	$2,28($sp)
	sw	$2,32($sp)
	sw	$0,40($sp)
	sw	$0,44($sp)
	.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
	sw	$0,48($sp)

	lw	$31,908($sp)
	lw	$21,904($sp)
	lw	$20,900($sp)
	lw	$19,896($sp)
	lw	$18,892($sp)
	lw	$17,888($sp)
	lw	$16,884($sp)
	j	$31
	addiu	$sp,$sp,912

	.set	macro
	.set	reorder
	.end	build_vlc.constprop.6
	.size	build_vlc.constprop.6, .-build_vlc.constprop.6
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	build_basic_mjpeg_vlc
	.type	build_basic_mjpeg_vlc, @function
build_basic_mjpeg_vlc:
	.frame	$sp,48,$31		# vars= 0, regs= 3/0, args= 24, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-48
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$16,36($sp)
	li	$7,12			# 0xc
	move	$16,$4
	sw	$0,16($sp)
	sw	$31,44($sp)
	addiu	$4,$4,544
	sw	$17,40($sp)
	.cprestore	24
	lw	$5,%got(ff_mjpeg_bits_dc_luminance)($28)
	lw	$6,%got(ff_mjpeg_val_dc)($28)
	.option	pic0
	jal	build_vlc.constprop.6
	.option	pic2
	li	$17,1			# 0x1

	addiu	$4,$16,560
	lw	$28,24($sp)
	li	$7,12			# 0xc
	lw	$5,%got(ff_mjpeg_bits_dc_chrominance)($28)
	lw	$6,%got(ff_mjpeg_val_dc)($28)
	.option	pic0
	jal	build_vlc.constprop.6
	.option	pic2
	sw	$0,16($sp)

	addiu	$4,$16,608
	lw	$28,24($sp)
	li	$7,251			# 0xfb
	lw	$5,%got(ff_mjpeg_bits_ac_luminance)($28)
	lw	$6,%got(ff_mjpeg_val_ac_luminance)($28)
	.option	pic0
	jal	build_vlc.constprop.6
	.option	pic2
	sw	$17,16($sp)

	addiu	$4,$16,624
	lw	$28,24($sp)
	li	$7,251			# 0xfb
	lw	$5,%got(ff_mjpeg_bits_ac_chrominance)($28)
	lw	$6,%got(ff_mjpeg_val_ac_chrominance)($28)
	.option	pic0
	jal	build_vlc.constprop.6
	.option	pic2
	sw	$17,16($sp)

	lw	$31,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	j	$31
	addiu	$sp,$sp,48

	.set	macro
	.set	reorder
	.end	build_basic_mjpeg_vlc
	.size	build_basic_mjpeg_vlc, .-build_basic_mjpeg_vlc
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align	2
$LC0:
	.ascii	"dqt: 16bit precision\012\000"
	.align	2
$LC1:
	.ascii	"index=%d\012\000"
	.align	2
$LC2:
	.ascii	"qscale[%d]: %d\012\000"
	.text
	.align	2
	.globl	ff_mjpeg_decode_dqt
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_dqt
	.type	ff_mjpeg_decode_dqt, @function
ff_mjpeg_decode_dqt:
	.frame	$sp,64,$31		# vars= 0, regs= 7/0, args= 24, gp= 8
	.mask	0x803f0000,-4
	.fmask	0x00000000,0
	lw	$3,12($4)
	lui	$28,%hi(__gnu_local_gp)
	lw	$6,4($4)
	addiu	$sp,$sp,-64
	addiu	$28,$28,%lo(__gnu_local_gp)
	srl	$2,$3,3
	sw	$31,60($sp)
	li	$7,-4			# 0xfffffffffffffffc
	sw	$21,56($sp)
	addu	$2,$6,$2
	sw	$20,52($sp)
	sw	$19,48($sp)
	sw	$18,44($sp)
	and	$5,$2,$7
	sw	$17,40($sp)
	sw	$16,36($sp)
	.cprestore	24
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$5,$3,0x7
	addu	$2,$2,$5
	li	$5,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$5
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $16
 # 0 "" 2
#NO_APP
	addiu	$16,$16,-2
	slt	$2,$16,65
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L33
	addiu	$5,$3,16
	.set	macro
	.set	reorder

	srl	$2,$5,3
	move	$19,$4
	addu	$2,$6,$2
	and	$4,$2,$7
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$5,0x7
	addu	$2,$2,$4
	li	$4,4			# 0x4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$4
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$3,$3,20
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L23
	sw	$3,12($19)
	.set	macro
	.set	reorder

	lui	$18,%hi($LC1)
	lui	$17,%hi($LC2)
	li	$20,-4			# 0xfffffffffffffffc
	addiu	$18,$18,%lo($LC1)
	addiu	$17,$17,%lo($LC2)
$L31:
	srl	$2,$3,3
	addu	$2,$6,$2
	and	$4,$2,$20
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$3,$3,0x7
	addu	$2,$2,$3
	li	$3,4			# 0x4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$3
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $21
 # 0 "" 2
#NO_APP
	addiu	$5,$5,8
	slt	$2,$21,4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L29
	sw	$5,12($19)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	lw	$4,0($19)
	move	$6,$18
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$21
	.set	macro
	.set	reorder

	addiu	$5,$19,1412
	lw	$3,12($19)
	sll	$6,$21,6
	lw	$28,24($sp)
	li	$8,8			# 0x8
	lw	$9,4($19)
	addiu	$7,$3,512
$L25:
	srl	$2,$3,3
	addu	$2,$9,$2
	and	$4,$2,$20
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$3,0x7
	addu	$2,$2,$4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $4
 # 0 "" 2
#NO_APP
	lbu	$2,0($5)
	addiu	$3,$3,8
	addiu	$5,$5,1
	addu	$2,$6,$2
	addiu	$2,$2,16
	sll	$2,$2,1
	addu	$2,$19,$2
	.set	noreorder
	.set	nomacro
	bne	$3,$7,$L25
	sh	$4,0($2)
	.set	macro
	.set	reorder

	lbu	$4,1413($19)
	addiu	$8,$21,168
	lbu	$2,1420($19)
	li	$5,48			# 0x30
	sw	$3,12($19)
	sll	$8,$8,2
	addu	$4,$6,$4
	lw	$25,%call16(av_log)($28)
	addu	$2,$6,$2
	addiu	$3,$4,16
	lw	$4,0($19)
	addiu	$2,$2,16
	sll	$3,$3,1
	sll	$2,$2,1
	addu	$3,$19,$3
	addu	$2,$19,$2
	addu	$8,$19,$8
	lh	$3,0($3)
	addiu	$16,$16,-65
	lh	$2,0($2)
	move	$6,$17
	move	$7,$21
	slt	$9,$3,$2
	movn	$3,$2,$9
	sra	$3,$3,1
	sw	$3,0($8)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,16($sp)
	.set	macro
	.set	reorder

	slt	$2,$16,65
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L28
	lw	$28,24($sp)
	.set	macro
	.set	reorder

	lw	$5,12($19)
	lw	$6,4($19)
	srl	$3,$5,3
	addu	$3,$6,$3
	and	$2,$3,$20
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$2,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$2,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$2,$5,0x7
	addu	$3,$3,$2
	li	$2,4			# 0x4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$2
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $4
 # 0 "" 2
#NO_APP
	addiu	$3,$5,4
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L31
	sw	$3,12($19)
	.set	macro
	.set	reorder

$L23:
	lui	$6,%hi($LC0)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($19)
	li	$5,16			# 0x10
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC0)
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
	lw	$31,60($sp)
	lw	$21,56($sp)
	lw	$20,52($sp)
	lw	$19,48($sp)
	lw	$18,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

$L33:
	sw	$5,12($4)
$L28:
	lw	$31,60($sp)
	move	$2,$0
	lw	$21,56($sp)
	lw	$20,52($sp)
	lw	$19,48($sp)
	lw	$18,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

$L29:
	lw	$31,60($sp)
	li	$2,-1			# 0xffffffffffffffff
	lw	$21,56($sp)
	lw	$20,52($sp)
	lw	$19,48($sp)
	lw	$18,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

	.end	ff_mjpeg_decode_dqt
	.size	ff_mjpeg_decode_dqt, .-ff_mjpeg_decode_dqt
	.section	.rodata.str1.4
	.align	2
$LC3:
	.ascii	"class=%d index=%d nb_codes=%d\012\000"
	.text
	.align	2
	.globl	ff_mjpeg_decode_dht
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_dht
	.type	ff_mjpeg_decode_dht, @function
ff_mjpeg_decode_dht:
	.frame	$sp,1208,$31		# vars= 1104, regs= 10/0, args= 56, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	lw	$7,12($4)
	lui	$28,%hi(__gnu_local_gp)
	lw	$9,4($4)
	addiu	$sp,$sp,-1208
	addiu	$28,$28,%lo(__gnu_local_gp)
	srl	$2,$7,3
	sw	$31,1204($sp)
	li	$3,-4			# 0xfffffffffffffffc
	sw	$fp,1200($sp)
	addu	$2,$9,$2
	sw	$23,1196($sp)
	sw	$22,1192($sp)
	sw	$21,1188($sp)
	and	$3,$2,$3
	sw	$20,1184($sp)
	sw	$19,1180($sp)
	sw	$18,1176($sp)
	sw	$17,1172($sp)
	sw	$16,1168($sp)
	.cprestore	56
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$3,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$3,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$3,$7,0x7
	addu	$2,$2,$3
	li	$3,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$3
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $18
 # 0 "" 2
#NO_APP
	addiu	$7,$7,16
	addiu	$18,$18,-2
	.set	noreorder
	.set	nomacro
	blez	$18,$L45
	sw	$7,12($4)
	.set	macro
	.set	reorder

	slt	$2,$18,17
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L37
	addiu	$21,$sp,608
	.set	macro
	.set	reorder

	addiu	$16,$sp,1136
	addiu	$fp,$sp,64
	move	$19,$4
$L38:
	srl	$2,$7,3
	li	$3,-4			# 0xfffffffffffffffc
	addu	$2,$9,$2
	and	$4,$2,$3
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$7,0x7
	addu	$2,$2,$4
	li	$5,4			# 0x4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$5
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $20
 # 0 "" 2
#NO_APP
	slt	$2,$20,2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L50
	addiu	$4,$7,4
	.set	macro
	.set	reorder

	srl	$2,$4,3
	addu	$2,$9,$2
	and	$3,$2,$3
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$3,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$3,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$4,0x7
	addu	$2,$2,$4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$5
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $11
 # 0 "" 2
#NO_APP
	addiu	$3,$7,8
	slt	$2,$11,4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L37
	sw	$3,12($19)
	.set	macro
	.set	reorder

	addiu	$4,$sp,1137
	addiu	$7,$7,136
	move	$6,$0
	li	$12,-4			# 0xfffffffffffffffc
	li	$8,8			# 0x8
$L40:
	srl	$2,$3,3
	addu	$2,$9,$2
	and	$5,$2,$12
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$5,$3,0x7
	addu	$2,$2,$5
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$3,$3,8
	sb	$2,0($4)
	andi	$2,$2,0xff
	addu	$6,$6,$2
	.set	noreorder
	.set	nomacro
	bne	$3,$7,$L40
	addiu	$4,$4,1
	.set	macro
	.set	reorder

	addiu	$18,$18,-17
	slt	$2,$18,$6
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L37
	sw	$7,12($19)
	.set	macro
	.set	reorder

	slt	$2,$6,257
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L51
	lw	$31,1204($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L46
	sll	$13,$6,3
	.set	macro
	.set	reorder

	addiu	$23,$sp,880
	addu	$12,$7,$13
	move	$17,$0
	li	$5,-4			# 0xfffffffffffffffc
	li	$4,8			# 0x8
	move	$3,$7
	move	$8,$23
$L42:
	srl	$2,$3,3
	addu	$2,$9,$2
	and	$14,$2,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$14,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$14,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$14,$3,0x7
	addu	$2,$2,$14
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$4
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$3,$3,8
	sb	$2,0($8)
	slt	$14,$17,$2
	movn	$17,$2,$14
	.set	noreorder
	.set	nomacro
	bne	$3,$12,$L42
	addiu	$8,$8,1
	.set	macro
	.set	reorder

	addu	$7,$13,$7
	addiu	$22,$17,1
	sw	$7,12($19)
$L41:
	sll	$3,$20,2
	lw	$25,%call16(free_vlc)($28)
	sw	$11,1160($sp)
	subu	$18,$18,$6
	addu	$3,$3,$11
	addiu	$3,$3,34
	sll	$3,$3,4
	addu	$3,$19,$3
	move	$4,$3
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
	sw	$3,1164($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC3)
	lw	$28,56($sp)
	li	$5,48			# 0x30
	lw	$11,1160($sp)
	addiu	$6,$6,%lo($LC3)
	lw	$4,0($19)
	move	$7,$20
	sw	$22,20($sp)
	lw	$25,%call16(av_log)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$11,16($sp)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,56($sp)
	li	$6,272			# 0x110
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$4,$21
	.set	macro
	.set	reorder

	move	$4,$21
	lw	$28,56($sp)
	move	$5,$fp
	move	$6,$16
	lw	$25,%call16(ff_mjpeg_build_huffman_codes)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,ff_mjpeg_build_huffman_codes
1:	jalr	$25
	move	$7,$23
	.set	macro
	.set	reorder

	li	$2,1			# 0x1
	lw	$28,56($sp)
	.set	noreorder
	.set	nomacro
	bne	$20,$2,$L43
	lw	$3,1164($sp)
	.set	macro
	.set	reorder

	lw	$25,%call16(memmove)($28)
	addiu	$4,$sp,624
	move	$6,$22
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	move	$5,$21
	.set	macro
	.set	reorder

	sll	$6,$22,1
	lw	$28,56($sp)
	addiu	$4,$sp,96
	move	$5,$fp
	lw	$25,%call16(memmove)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	addiu	$22,$17,17
	.set	macro
	.set	reorder

	lw	$28,56($sp)
	sw	$0,0($21)
	sw	$0,4($21)
	sw	$0,8($21)
	sw	$0,12($21)
	sw	$0,64($sp)
	sw	$0,68($sp)
	sw	$0,72($sp)
	sw	$0,76($sp)
	sw	$0,80($sp)
	sw	$0,84($sp)
	sw	$0,88($sp)
	sw	$0,92($sp)
	lw	$3,1164($sp)
$L43:
	li	$6,1			# 0x1
	lw	$25,%call16(init_vlc_sparse)($28)
	li	$2,2			# 0x2
	sw	$fp,24($sp)
	li	$5,9			# 0x9
	sw	$0,36($sp)
	sw	$6,16($sp)
	move	$4,$3
	sw	$6,20($sp)
	move	$7,$21
	move	$6,$22
	sw	$2,28($sp)
	sw	$2,32($sp)
	sw	$0,40($sp)
	sw	$0,44($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
	sw	$0,48($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$2,$L37
	lw	$28,56($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$18,$L45
	slt	$2,$18,17
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L51
	lw	$31,1204($sp)
	.set	macro
	.set	reorder

	lw	$7,12($19)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L38
	.option	pic2
	lw	$9,4($19)
	.set	macro
	.set	reorder

$L50:
	sw	$4,12($19)
$L37:
	lw	$31,1204($sp)
$L51:
	li	$2,-1			# 0xffffffffffffffff
	lw	$fp,1200($sp)
	lw	$23,1196($sp)
	lw	$22,1192($sp)
	lw	$21,1188($sp)
	lw	$20,1184($sp)
	lw	$19,1180($sp)
	lw	$18,1176($sp)
	lw	$17,1172($sp)
	lw	$16,1168($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,1208
	.set	macro
	.set	reorder

$L45:
	lw	$31,1204($sp)
	move	$2,$0
	lw	$fp,1200($sp)
	lw	$23,1196($sp)
	lw	$22,1192($sp)
	lw	$21,1188($sp)
	lw	$20,1184($sp)
	lw	$19,1180($sp)
	lw	$18,1176($sp)
	lw	$17,1172($sp)
	lw	$16,1168($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,1208
	.set	macro
	.set	reorder

$L46:
	li	$22,1			# 0x1
	move	$17,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L41
	.option	pic2
	addiu	$23,$sp,880
	.set	macro
	.set	reorder

	.end	ff_mjpeg_decode_dht
	.size	ff_mjpeg_decode_dht, .-ff_mjpeg_decode_dht
	.section	.rodata.str1.4
	.align	2
$LC4:
	.ascii	"mjpeg: using external huffman table\012\000"
	.align	2
$LC5:
	.ascii	"mjpeg: error using external huffman table, switching bac"
	.ascii	"k to internal\012\000"
	.align	2
$LC6:
	.ascii	"mjpeg bottom field first\012\000"
	.section	.text.unlikely
	.align	2
	.globl	ff_mjpeg_decode_init
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_init
	.type	ff_mjpeg_decode_init, @function
ff_mjpeg_decode_init:
	.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
	.mask	0x80030000,-4
	.fmask	0x00000000,0
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-40
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$17,32($sp)
	li	$2,7			# 0x7
	sw	$31,36($sp)
	move	$17,$4
	sw	$16,28($sp)
	.cprestore	16
#APP
 # 109 "mjpegdec.c" 1
	S32I2M xr16,$2
 # 0 "" 2
#NO_APP
	lw	$16,136($4)
	move	$5,$4
	lw	$25,%call16(dsputil_init)($28)
	addiu	$4,$16,1540
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
	sw	$17,0($16)
	.set	macro
	.set	reorder

	addiu	$4,$16,4244
	lw	$28,16($sp)
	lw	$25,%call16(ff_init_scantable)($28)
	lw	$6,%got(ff_zigzag_direct)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
	addiu	$5,$16,1408
	.set	macro
	.set	reorder

	li	$3,-1			# 0xffffffffffffffff
	lw	$2,664($17)
	move	$4,$16
	sw	$0,24($16)
	sw	$3,20($16)
	li	$3,1			# 0x1
	sw	$0,28($16)
	sw	$2,688($16)
	li	$2,2			# 0x2
	sw	$3,692($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	build_basic_mjpeg_vlc
	.option	pic2
	sw	$2,900($17)
	.set	macro
	.set	reorder

	lw	$2,12($17)
	andi	$2,$2,0x1000
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L54
	lw	$28,16($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC4)
	lw	$25,%call16(av_log)($28)
	li	$5,32			# 0x20
	addiu	$6,$6,%lo($LC4)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$17
	.set	macro
	.set	reorder

	lw	$2,28($17)
	sll	$2,$2,3
	sra	$3,$2,3
	.set	noreorder
	.set	nomacro
	bltz	$3,$L59
	lw	$4,24($17)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L64
	addu	$3,$4,$3
	.set	macro
	.set	reorder

$L59:
	move	$3,$0
	move	$2,$0
	move	$4,$0
	addu	$3,$4,$3
$L64:
	sw	$4,4($16)
	sw	$2,16($16)
	move	$4,$16
	sw	$0,12($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_dht
	.option	pic2
	sw	$3,8($16)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L54
	lw	$28,16($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC5)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC5)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	build_basic_mjpeg_vlc
	.option	pic2
	move	$4,$16
	.set	macro
	.set	reorder

	lw	$28,16($sp)
$L54:
	lw	$2,28($17)
	slt	$2,$2,10
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L57
	li	$2,1818558464			# 0x6c650000
	.set	macro
	.set	reorder

	lw	$3,24($17)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 7($3)  
	lwr $4, 4($3)  
	
 # 0 "" 2
#NO_APP
	addiu	$2,$2,26982
	.set	noreorder
	.set	nomacro
	bne	$4,$2,$L57
	li	$2,6			# 0x6
	.set	macro
	.set	reorder

	lbu	$3,9($3)
	.set	noreorder
	.set	nomacro
	bne	$3,$2,$L57
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC6)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC6)
	sw	$2,5676($16)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$17
	.set	macro
	.set	reorder

$L57:
	lw	$2,132($17)
	lw	$3,8($2)
	li	$2,111			# 0x6f
	.set	noreorder
	.set	nomacro
	bne	$3,$2,$L65
	lw	$31,36($sp)
	.set	macro
	.set	reorder

	li	$2,1			# 0x1
	sw	$2,5688($16)
	lw	$31,36($sp)
$L65:
	move	$2,$0
	lw	$17,32($sp)
	lw	$16,28($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,40
	.set	macro
	.set	reorder

	.end	ff_mjpeg_decode_init
	.size	ff_mjpeg_decode_init, .-ff_mjpeg_decode_init
	.section	.rodata.str1.4
	.align	2
$LC7:
	.ascii	"only 8 bits/component accepted\012\000"
	.align	2
$LC8:
	.ascii	"sof0: picture: %dx%d\012\000"
	.align	2
$LC9:
	.ascii	"only <= 8 bits/component or 16-bit gray accepted for JPE"
	.ascii	"G-LS\012\000"
	.align	2
$LC10:
	.ascii	"component %d %d:%d id: %d quant:%d\012\000"
	.align	2
$LC11:
	.ascii	"Subsampling in JPEG-LS is not supported.\012\000"
	.align	2
$LC12:
	.ascii	"pix fmt id %x\012\000"
	.align	2
$LC13:
	.ascii	"Unhandled pixel format 0x%x\012\000"
	.align	2
$LC14:
	.ascii	"get_buffer() failed\012\000"
	.align	2
$LC15:
	.ascii	"decode_sof0: error, len(%d) mismatch\012\000"
	.text
	.align	2
	.globl	ff_mjpeg_decode_sof
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_sof
	.type	ff_mjpeg_decode_sof, @function
ff_mjpeg_decode_sof:
	.frame	$sp,96,$31		# vars= 16, regs= 10/0, args= 32, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-96
	lui	$28,%hi(__gnu_local_gp)
	sw	$16,56($sp)
	move	$16,$4
	lw	$4,12($4)
	addiu	$28,$28,%lo(__gnu_local_gp)
	lw	$7,4($16)
	li	$3,-4			# 0xfffffffffffffffc
	sw	$31,92($sp)
	srl	$2,$4,3
	sw	$fp,88($sp)
	sw	$23,84($sp)
	addu	$2,$7,$2
	sw	$22,80($sp)
	sw	$21,76($sp)
	sw	$20,72($sp)
	and	$5,$2,$3
	sw	$19,68($sp)
	sw	$18,64($sp)
	sw	$17,60($sp)
	.cprestore	32
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$5,$4,0x7
	addu	$2,$2,$5
	li	$5,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$5
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$5,$4,16
	sw	$2,48($sp)
	srl	$2,$5,3
	addu	$2,$7,$2
	and	$3,$2,$3
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$3,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$3,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$5,$5,0x7
	addu	$2,$2,$5
	li	$3,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$3
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$5,$4,24
	sw	$2,728($16)
	lw	$6,724($16)
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L67
	sw	$5,12($16)
	.set	macro
	.set	reorder

	li	$2,9			# 0x9
	sw	$2,728($16)
$L115:
	lw	$2,704($16)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L148
	srl	$3,$5,3
	.set	macro
	.set	reorder

$L158:
	li	$6,-4			# 0xfffffffffffffffc
	addu	$3,$7,$3
	and	$2,$3,$6
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$2,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$2,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$5,$5,0x7
	addu	$3,$3,$5
	li	$8,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addiu	$5,$4,40
	sw	$3,40($sp)
	srl	$3,$5,3
	addu	$2,$7,$3
	and	$3,$2,$6
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$3,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$3,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$5,$5,0x7
	addu	$2,$2,$5
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $20
 # 0 "" 2
#NO_APP
	addiu	$4,$4,56
	lw	$2,696($16)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L149
	sw	$4,12($16)
	.set	macro
	.set	reorder

$L71:
	lw	$3,40($sp)
$L163:
	lui	$6,%hi($LC8)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	lw	$4,0($16)
	addiu	$6,$6,%lo($LC8)
	move	$7,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,16($sp)
	.set	macro
	.set	reorder

	move	$6,$0
	lw	$28,32($sp)
	move	$4,$20
	lw	$7,0($16)
	lw	$25,%call16(av_image_check_size)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_image_check_size
1:	jalr	$25
	lw	$5,40($sp)
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L73
	sw	$2,44($sp)
	.set	macro
	.set	reorder

	lw	$5,12($16)
	li	$4,-4			# 0xfffffffffffffffc
	lw	$3,4($16)
	srl	$2,$5,3
	addu	$2,$3,$2
	and	$4,$2,$4
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$5,0x7
	addu	$2,$2,$4
	li	$4,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$4
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $17
 # 0 "" 2
#NO_APP
	addiu	$5,$5,8
	addiu	$2,$17,-1
	sltu	$2,$2,4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L73
	sw	$5,12($16)
	.set	macro
	.set	reorder

	lw	$2,708($16)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L150
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

$L164:
	sw	$17,772($16)
	lui	$18,%hi($LC10)
	addiu	$23,$16,792
	li	$6,1			# 0x1
	sw	$2,936($16)
	li	$7,1			# 0x1
	sw	$2,940($16)
	move	$19,$0
	li	$fp,-4			# 0xfffffffffffffffc
	li	$21,8			# 0x8
	li	$22,4			# 0x4
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L78
	.option	pic2
	addiu	$18,$18,%lo($LC10)
	.set	macro
	.set	reorder

$L151:
	lw	$5,12($16)
	lw	$3,4($16)
	lw	$7,936($16)
	lw	$6,940($16)
$L78:
	srl	$2,$5,3
	addu	$2,$3,$2
	and	$4,$2,$fp
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$5,0x7
	addu	$2,$2,$4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$21
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $8
 # 0 "" 2
#NO_APP
	addiu	$12,$5,8
	addiu	$8,$8,-1
	srl	$2,$12,3
	addu	$2,$3,$2
	sw	$8,0($23)
	and	$4,$2,$fp
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$4,$2,3
	andi	$2,$12,0x7
	addu	$2,$4,$2
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$22
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $12
 # 0 "" 2
#NO_APP
	addiu	$13,$5,12
	sw	$12,16($23)
	srl	$2,$13,3
	addu	$2,$3,$2
	and	$4,$2,$fp
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$4,$2,3
	andi	$2,$13,0x7
	addu	$2,$4,$2
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$22
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $13
 # 0 "" 2
#NO_APP
	addiu	$4,$5,16
	slt	$7,$7,$12
	srl	$2,$4,3
	sw	$4,12($16)
	slt	$6,$6,$13
	addu	$3,$3,$2
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L75
	sw	$13,32($23)
	.set	macro
	.set	reorder

	sw	$12,936($16)
$L75:
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L76
	and	$2,$3,$fp
	.set	macro
	.set	reorder

	sw	$13,940($16)
$L76:
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$2,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$2,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$2,$3,3
	andi	$3,$4,0x7
	addu	$3,$2,$3
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$21
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$3,$5,24
	lw	$25,%call16(av_log)($28)
	move	$7,$19
	li	$5,48			# 0x30
	sw	$3,12($16)
	slt	$3,$2,4
	addiu	$19,$19,1
	sw	$2,152($23)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L73
	move	$6,$18
	.set	macro
	.set	reorder

	lw	$4,0($16)
	addiu	$23,$23,4
	sw	$12,16($sp)
	sw	$13,20($sp)
	sw	$8,24($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,28($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$19,$17,$L151
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	lw	$2,708($16)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L79
	li	$3,1			# 0x1
	.set	macro
	.set	reorder

	lw	$2,936($16)
	slt	$2,$2,2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L160
	li	$3,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

	lw	$2,940($16)
	slt	$3,$2,2
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L80
	li	$3,1			# 0x1
	.set	macro
	.set	reorder

	beq	$2,$3,$L152
$L82:
	lw	$2,756($16)
	.set	noreorder
	.set	nomacro
	bne	$2,$20,$L161
	lw	$25,%call16(av_freep)($28)
	.set	macro
	.set	reorder

	lw	$2,760($16)
	lw	$3,40($sp)
	beq	$2,$3,$L84
$L161:
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addiu	$4,$16,1212
	.set	macro
	.set	reorder

	lw	$3,40($sp)
	lw	$2,692($16)
	lw	$28,32($sp)
	sw	$20,756($16)
	sw	$3,760($16)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L153
	sw	$0,696($16)
	.set	macro
	.set	reorder

	lw	$25,%call16(avcodec_set_dimensions)($28)
$L165:
	move	$5,$20
$L166:
	lw	$4,0($16)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,avcodec_set_dimensions
1:	jalr	$25
	lw	$6,40($sp)
	.set	macro
	.set	reorder

	lw	$2,756($16)
	lw	$28,32($sp)
	addiu	$4,$2,15
	addiu	$2,$2,30
	slt	$3,$4,0
	lw	$25,%call16(av_mallocz)($28)
	movn	$4,$2,$3
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
	sra	$4,$4,4
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	sw	$2,1212($16)
	sw	$0,692($16)
$L84:
	lw	$2,696($16)
	beq	$2,$0,$L88
	lw	$2,5676($16)
	lw	$3,700($16)
	sltu	$2,$2,1
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L159
	lw	$31,92($sp)
	.set	macro
	.set	reorder

$L88:
	lw	$4,808($16)
	lui	$6,%hi($LC12)
	lw	$2,824($16)
	li	$5,48			# 0x30
	lw	$3,812($16)
	addiu	$6,$6,%lo($LC12)
	sll	$7,$4,28
	lw	$9,836($16)
	sll	$4,$2,24
	lw	$8,828($16)
	lw	$2,816($16)
	sll	$3,$3,20
	or	$4,$7,$4
	lw	$18,832($16)
	or	$4,$4,$9
	lw	$7,820($16)
	sll	$8,$8,16
	lw	$25,%call16(av_log)($28)
	or	$3,$4,$3
	lw	$4,0($16)
	sll	$2,$2,12
	or	$3,$3,$8
	sll	$18,$18,8
	or	$2,$3,$2
	sll	$3,$7,4
	or	$2,$2,$18
	or	$18,$2,$3
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$18
	.set	macro
	.set	reorder

	li	$2,-791674880			# 0xffffffffd0d00000
	ori	$2,$2,0xd0d0
	and	$2,$18,$2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L89
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	li	$2,-252706816			# 0xfffffffff0f00000
	ori	$2,$2,0xf0f0
	and	$2,$18,$2
	srl	$2,$2,1
	subu	$18,$18,$2
$L89:
	li	$2,218955776			# 0xd0d0000
	addiu	$2,$2,3341
	and	$2,$18,$2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L154
	li	$2,252641280			# 0xf0f0000
	.set	macro
	.set	reorder

$L90:
	li	$2,303104000			# 0x12110000
	addiu	$3,$2,4352
	.set	noreorder
	.set	nomacro
	beq	$18,$3,$L92
	addiu	$2,$2,4353
	.set	macro
	.set	reorder

	slt	$2,$18,$2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L93
	li	$2,285212672			# 0x11000000
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$18,$2,$L94
	li	$2,286326784			# 0x11110000
	.set	macro
	.set	reorder

	addiu	$2,$2,4352
	.set	noreorder
	.set	nomacro
	bne	$18,$2,$L162
	lui	$6,%hi($LC13)
	.set	macro
	.set	reorder

	lw	$2,716($16)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L98
	li	$2,14			# 0xe
	.set	macro
	.set	reorder

	lw	$4,0($16)
	li	$2,30			# 0x1e
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L99
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L73:
	li	$3,-1			# 0xffffffffffffffff
	sw	$3,44($sp)
$L129:
	lw	$31,92($sp)
$L159:
	lw	$2,44($sp)
	lw	$fp,88($sp)
	lw	$23,84($sp)
	lw	$22,80($sp)
	lw	$21,76($sp)
	lw	$20,72($sp)
	lw	$19,68($sp)
	lw	$18,64($sp)
	lw	$17,60($sp)
	lw	$16,56($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,96
	.set	macro
	.set	reorder

$L67:
	li	$6,9			# 0x9
	beq	$2,$6,$L155
	bne	$2,$3,$L115
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L158
	.option	pic2
	srl	$3,$5,3
	.set	macro
	.set	reorder

$L149:
	lw	$2,756($16)
	.set	noreorder
	.set	nomacro
	bne	$2,$20,$L163
	lw	$3,40($sp)
	.set	macro
	.set	reorder

	lw	$4,40($sp)
	lw	$3,760($16)
	addiu	$2,$4,1
	xor	$2,$3,$2
	movz	$4,$3,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L71
	.option	pic2
	sw	$4,40($sp)
	.set	macro
	.set	reorder

$L150:
	lw	$2,728($16)
	slt	$2,$2,9
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L164
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$17,$2,$L164
	lw	$25,%call16(av_log)($28)
	.set	macro
	.set	reorder

	li	$3,-1			# 0xffffffffffffffff
	lui	$6,%hi($LC9)
	lw	$4,0($16)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC9)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,44($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L159
	.option	pic2
	lw	$31,92($sp)
	.set	macro
	.set	reorder

$L79:
	lw	$2,940($16)
	bne	$2,$3,$L82
$L152:
	lw	$3,936($16)
	bne	$3,$2,$L82
	lw	$3,704($16)
	bne	$3,$2,$L82
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L82
	.option	pic2
	sw	$2,716($16)
	.set	macro
	.set	reorder

$L153:
	lw	$3,688($16)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L165
	lw	$25,%call16(avcodec_set_dimensions)($28)
	.set	macro
	.set	reorder

	sll	$2,$3,1
	addu	$2,$2,$3
	addiu	$3,$2,3
	slt	$4,$2,0
	movn	$2,$3,$4
	lw	$3,40($sp)
	sra	$2,$2,2
	slt	$2,$3,$2
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L166
	move	$5,$20
	.set	macro
	.set	reorder

	move	$4,$3
	lw	$3,5676($16)
	sll	$4,$4,1
	li	$2,1			# 0x1
	sw	$4,40($sp)
	sltu	$4,$3,1
	sw	$2,696($16)
	sw	$3,700($16)
	sw	$2,1140($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L165
	.option	pic2
	sw	$4,1144($16)
	.set	macro
	.set	reorder

$L154:
	addiu	$2,$2,3855
	and	$2,$18,$2
	sra	$2,$2,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L90
	.option	pic2
	subu	$18,$18,$2
	.set	macro
	.set	reorder

$L93:
	li	$2,554762240			# 0x21110000
	addiu	$2,$2,4352
	.set	noreorder
	.set	nomacro
	beq	$18,$2,$L96
	li	$2,571539456			# 0x22110000
	.set	macro
	.set	reorder

	addiu	$2,$2,4352
	.set	noreorder
	.set	nomacro
	bne	$18,$2,$L91
	li	$3,12			# 0xc
	.set	macro
	.set	reorder

	lw	$2,5672($16)
	lw	$4,0($16)
	movn	$3,$0,$2
	sw	$3,52($4)
$L99:
	lw	$2,708($16)
	beq	$2,$0,$L104
	lw	$2,772($16)
	slt	$2,$2,2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L105
	li	$2,2			# 0x2
	.set	macro
	.set	reorder

	sw	$2,52($4)
$L104:
	lw	$2,976($16)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L108
	addiu	$18,$16,976
	.set	macro
	.set	reorder

	lw	$25,260($4)
	.set	noreorder
	.set	nomacro
	jalr	$25
	move	$5,$18
	.set	macro
	.set	reorder

	lw	$4,0($16)
$L108:
	lw	$25,256($4)
	move	$5,$18
	.set	noreorder
	.set	nomacro
	jalr	$25
	sw	$0,1056($16)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$2,$L157
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	lw	$6,696($16)
	sll	$2,$17,1
	lw	$3,996($16)
	li	$5,1			# 0x1
	addu	$17,$2,$17
	lw	$4,992($16)
	lw	$2,1000($16)
	sll	$3,$3,$6
	sw	$5,1028($16)
	sll	$4,$4,$6
	sw	$5,1024($16)
	sll	$2,$2,$6
	sw	$5,1192($16)
	sw	$3,1200($16)
	addiu	$17,$17,8
	sw	$4,1196($16)
	sw	$2,1204($16)
	lw	$3,48($sp)
	.set	noreorder
	.set	nomacro
	beq	$17,$3,$L110
	lui	$6,%hi($LC15)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_log)($28)
	lw	$4,0($16)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC15)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$3
	.set	macro
	.set	reorder

	lw	$28,32($sp)
$L110:
	lw	$2,712($16)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L129
	lw	$5,40($sp)
	.set	macro
	.set	reorder

	lw	$2,936($16)
	lw	$3,940($16)
	sll	$2,$2,3
	lw	$4,772($16)
	sll	$3,$3,3
	addu	$20,$2,$20
	addu	$19,$3,$5
	addiu	$20,$20,-1
	addiu	$19,$19,-1
	teq	$2,$0,7
	div	$0,$20,$2
	mflo	$20
	teq	$3,$0,7
	div	$0,$19,$3
	.set	noreorder
	.set	nomacro
	blez	$4,$L114
	mflo	$19
	.set	macro
	.set	reorder

	mul	$19,$20,$19
	addiu	$17,$16,1344
	move	$22,$0
$L113:
	lw	$2,-536($17)
	move	$4,$17
	lw	$18,-520($17)
	addiu	$22,$22,1
	lw	$25,%call16(av_freep)($28)
	mul	$2,$19,$2
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	mul	$18,$2,$18
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	addiu	$4,$17,16
	lw	$25,%call16(av_freep)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addiu	$17,$17,4
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	lw	$25,%call16(av_malloc)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	sll	$4,$18,7
	.set	macro
	.set	reorder

	move	$4,$18
	lw	$28,32($sp)
	lw	$25,%call16(av_mallocz)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
	sw	$2,-4($17)
	.set	macro
	.set	reorder

	lw	$3,-540($17)
	lw	$28,32($sp)
	sw	$2,12($17)
	mul	$3,$3,$20
	sw	$3,-572($17)
	lw	$2,772($16)
	slt	$2,$22,$2
	bne	$2,$0,$L113
$L114:
	lw	$25,%call16(memset)($28)
	addiu	$4,$16,1376
	move	$5,$0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	li	$6,32			# 0x20
	.set	macro
	.set	reorder

	lw	$31,92($sp)
	lw	$2,44($sp)
	lw	$fp,88($sp)
	lw	$23,84($sp)
	lw	$22,80($sp)
	lw	$21,76($sp)
	lw	$20,72($sp)
	lw	$19,68($sp)
	lw	$18,64($sp)
	lw	$17,60($sp)
	lw	$16,56($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,96
	.set	macro
	.set	reorder

$L155:
	li	$2,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L115
	.option	pic2
	sw	$2,720($16)
	.set	macro
	.set	reorder

$L91:
	lui	$6,%hi($LC13)
$L162:
	lw	$25,%call16(av_log)($28)
	lw	$4,0($16)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC13)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$18
	.set	macro
	.set	reorder

	li	$3,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L129
	.option	pic2
	sw	$3,44($sp)
	.set	macro
	.set	reorder

$L94:
	lw	$4,0($16)
	li	$2,8			# 0x8
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L99
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L96:
	lw	$3,5672($16)
	li	$2,13			# 0xd
	li	$5,4			# 0x4
	lw	$4,0($16)
	movn	$2,$5,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L99
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L92:
	lw	$3,5672($16)
	li	$2,34			# 0x22
	li	$5,33			# 0x21
	lw	$4,0($16)
	movn	$2,$5,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L99
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L98:
	lw	$3,5672($16)
	li	$5,5			# 0x5
	lw	$4,0($16)
	movn	$2,$5,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L99
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L80:
	li	$3,-1			# 0xffffffffffffffff
$L160:
	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC11)
	lw	$4,0($16)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC11)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,44($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L159
	.option	pic2
	lw	$31,92($sp)
	.set	macro
	.set	reorder

$L105:
	lw	$2,728($16)
	slt	$2,$2,9
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L106
	li	$2,32			# 0x20
	.set	macro
	.set	reorder

	li	$2,8			# 0x8
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L104
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L106:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L104
	.option	pic2
	sw	$2,52($4)
	.set	macro
	.set	reorder

$L148:
	li	$3,-1			# 0xffffffffffffffff
	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC7)
	lw	$4,0($16)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC7)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,44($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L159
	.option	pic2
	lw	$31,92($sp)
	.set	macro
	.set	reorder

$L157:
	li	$3,-1			# 0xffffffffffffffff
	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC14)
	lw	$4,0($16)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC14)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,44($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L159
	.option	pic2
	lw	$31,92($sp)
	.set	macro
	.set	reorder

	.end	ff_mjpeg_decode_sof
	.size	ff_mjpeg_decode_sof, .-ff_mjpeg_decode_sof
	.align	2
	.globl	jmpeg_simple_idct_put_mxu
	.set	nomips16
	.set	nomicromips
	.ent	jmpeg_simple_idct_put_mxu
	.type	jmpeg_simple_idct_put_mxu, @function
jmpeg_simple_idct_put_mxu:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	li	$2,1518469120			# 0x5a820000
	addiu	$3,$2,30274
#APP
 # 864 "mjpegdec.c" 1
	S32I2M xr5,$3
 # 0 "" 2
#NO_APP
	addiu	$2,$2,12540
#APP
 # 865 "mjpegdec.c" 1
	S32I2M xr6,$2
 # 0 "" 2
#NO_APP
	li	$2,2105999360			# 0x7d870000
	addiu	$2,$2,27246
#APP
 # 866 "mjpegdec.c" 1
	S32I2M xr7,$2
 # 0 "" 2
#NO_APP
	li	$2,418971648			# 0x18f90000
	addiu	$2,$2,18205
#APP
 # 867 "mjpegdec.c" 1
	S32I2M xr8,$2
 # 0 "" 2
#NO_APP
	li	$2,1785593856			# 0x6a6e0000
	addiu	$2,$2,6393
#APP
 # 868 "mjpegdec.c" 1
	S32I2M xr9,$2
 # 0 "" 2
#NO_APP
	li	$2,1193082880			# 0x471d0000
	addiu	$2,$2,32135
#APP
 # 869 "mjpegdec.c" 1
	S32I2M xr10,$2
 # 0 "" 2
#NO_APP
	move	$3,$0
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L171
	addiu	$2,$7,-16
	.set	macro
	.set	reorder

	addiu	$2,$2,16
$L183:
#APP
 # 876 "mjpegdec.c" 1
	S32LDD xr1,$2,0
 # 0 "" 2
 # 877 "mjpegdec.c" 1
	S32LDD xr2,$2,4
 # 0 "" 2
 # 878 "mjpegdec.c" 1
	S32LDD xr3,$2,8
 # 0 "" 2
 # 879 "mjpegdec.c" 1
	S32LDD xr4,$2,12
 # 0 "" 2
 # 881 "mjpegdec.c" 1
	S32SFL xr1,xr1,xr2,xr2,ptn3
 # 0 "" 2
 # 883 "mjpegdec.c" 1
	S32SFL xr3,xr3,xr4,xr4,ptn3
 # 0 "" 2
 # 885 "mjpegdec.c" 1
	D16MUL xr11,xr2,xr5,xr12,WW
 # 0 "" 2
 # 886 "mjpegdec.c" 1
	D16MAC xr11,xr4,xr6,xr12,AA,WW
 # 0 "" 2
 # 887 "mjpegdec.c" 1
	D16MUL xr13,xr2,xr6,xr14,WW
 # 0 "" 2
 # 888 "mjpegdec.c" 1
	D16MAC xr13,xr4,xr5,xr14,SS,WW
 # 0 "" 2
 # 889 "mjpegdec.c" 1
	D16MUL xr2,xr1,xr7,xr4,HW
 # 0 "" 2
 # 890 "mjpegdec.c" 1
	D16MAC xr2,xr1,xr9,xr4,AS,LW
 # 0 "" 2
 # 891 "mjpegdec.c" 1
	D16MAC xr2,xr3,xr10,xr4,AS,HW
 # 0 "" 2
 # 892 "mjpegdec.c" 1
	D16MAC xr2,xr3,xr8,xr4,AS,LW
 # 0 "" 2
 # 894 "mjpegdec.c" 1
	D16MACF xr2,xr0,xr0,xr4,AA,WW
 # 0 "" 2
 # 895 "mjpegdec.c" 1
	D16MACF xr11,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 896 "mjpegdec.c" 1
	D16MACF xr12,xr0,xr0,xr14,AA,WW
 # 0 "" 2
 # 898 "mjpegdec.c" 1
	D16MUL xr4,xr1,xr8,xr15,HW
 # 0 "" 2
 # 899 "mjpegdec.c" 1
	D16MAC xr4,xr1,xr10,xr15,SS,LW
 # 0 "" 2
 # 900 "mjpegdec.c" 1
	D16MAC xr4,xr3,xr9,xr15,AA,HW
 # 0 "" 2
 # 901 "mjpegdec.c" 1
	D16MAC xr4,xr3,xr7,xr15,SA,LW
 # 0 "" 2
 # 902 "mjpegdec.c" 1
	Q16ADD xr11,xr11,xr12,xr12,AS,WW
 # 0 "" 2
 # 904 "mjpegdec.c" 1
	D16MACF xr15,xr0,xr0,xr4,AA,WW
 # 0 "" 2
 # 905 "mjpegdec.c" 1
	Q16ADD xr11,xr11,xr2,xr2,AS,WW
 # 0 "" 2
 # 906 "mjpegdec.c" 1
	Q16ADD xr12,xr12,xr15,xr15,AS,XW
 # 0 "" 2
 # 909 "mjpegdec.c" 1
	S32SFL xr11,xr11,xr12,xr12,ptn3
 # 0 "" 2
 # 911 "mjpegdec.c" 1
	S32SFL xr12,xr12,xr11,xr11,ptn3
 # 0 "" 2
 # 913 "mjpegdec.c" 1
	S32STD xr12,$2,0
 # 0 "" 2
 # 914 "mjpegdec.c" 1
	S32STD xr11,$2,4
 # 0 "" 2
 # 915 "mjpegdec.c" 1
	S32STD xr15,$2,8
 # 0 "" 2
 # 916 "mjpegdec.c" 1
	S32STD xr2,$2,12
 # 0 "" 2
#NO_APP
	addiu	$3,$3,1
	.set	noreorder
	.set	nomacro
	bne	$3,$5,$L183
	addiu	$2,$2,16
	.set	macro
	.set	reorder

	addiu	$2,$2,-16
$L171:
	slt	$5,$5,5
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L182
	addiu	$2,$7,-4
	.set	macro
	.set	reorder

	sll	$3,$6,1
	sll	$8,$6,3
	li	$10,1518469120			# 0x5a820000
	addu	$3,$4,$3
	addiu	$7,$7,12
	sll	$9,$6,2
	subu	$8,$8,$6
	addiu	$10,$10,30274
$L172:
	addiu	$2,$2,4
#APP
 # 927 "mjpegdec.c" 1
	S32LDD xr1,$2,0
 # 0 "" 2
 # 929 "mjpegdec.c" 1
	S32LDD xr3,$2,32
 # 0 "" 2
 # 930 "mjpegdec.c" 1
	S32I2M xr5,$10
 # 0 "" 2
 # 931 "mjpegdec.c" 1
	S32LDD xr11,$2,64
 # 0 "" 2
 # 932 "mjpegdec.c" 1
	S32LDD xr13,$2,96
 # 0 "" 2
 # 934 "mjpegdec.c" 1
	D16MUL xr15,xr5,xr1,xr9,HW
 # 0 "" 2
 # 935 "mjpegdec.c" 1
	D16MAC xr15,xr5,xr11,xr9,AA,HW
 # 0 "" 2
 # 937 "mjpegdec.c" 1
	D16MACF xr15,xr0,xr0,xr9,AA,WW
 # 0 "" 2
 # 939 "mjpegdec.c" 1
	D16MUL xr10,xr5,xr3,xr9,LW
 # 0 "" 2
 # 940 "mjpegdec.c" 1
	D16MAC xr10,xr6,xr13,xr9,AA,LW
 # 0 "" 2
 # 942 "mjpegdec.c" 1
	D16MACF xr10,xr0,xr0,xr9,AA,WW
 # 0 "" 2
 # 944 "mjpegdec.c" 1
	S32LDD xr2,$2,16
 # 0 "" 2
 # 945 "mjpegdec.c" 1
	S32LDD xr4,$2,48
 # 0 "" 2
 # 946 "mjpegdec.c" 1
	Q16ADD xr15,xr15,xr10,xr9,AS,WW
 # 0 "" 2
 # 947 "mjpegdec.c" 1
	D16MUL xr10,xr5,xr1,xr1,HW
 # 0 "" 2
 # 948 "mjpegdec.c" 1
	D16MAC xr10,xr5,xr11,xr1,SS,HW
 # 0 "" 2
 # 950 "mjpegdec.c" 1
	D16MACF xr10,xr0,xr0,xr1,AA,WW
 # 0 "" 2
 # 952 "mjpegdec.c" 1
	D16MUL xr11,xr6,xr3,xr1,LW
 # 0 "" 2
 # 953 "mjpegdec.c" 1
	D16MAC xr11,xr5,xr13,xr1,SS,LW
 # 0 "" 2
 # 955 "mjpegdec.c" 1
	D16MACF xr11,xr0,xr0,xr1,AA,WW
 # 0 "" 2
 # 957 "mjpegdec.c" 1
	S32LDD xr12,$2,80
 # 0 "" 2
 # 958 "mjpegdec.c" 1
	S32LDD xr14,$2,112
 # 0 "" 2
 # 959 "mjpegdec.c" 1
	Q16ADD xr10,xr10,xr11,xr1,AS,WW
 # 0 "" 2
 # 960 "mjpegdec.c" 1
	D16MUL xr11,xr7,xr2,xr13,HW
 # 0 "" 2
 # 961 "mjpegdec.c" 1
	D16MAC xr11,xr7,xr4,xr13,AA,LW
 # 0 "" 2
 # 962 "mjpegdec.c" 1
	D16MAC xr11,xr8,xr12,xr13,AA,LW
 # 0 "" 2
 # 963 "mjpegdec.c" 1
	D16MAC xr11,xr8,xr14,xr13,AA,HW
 # 0 "" 2
 # 965 "mjpegdec.c" 1
	D16MACF xr11,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 967 "mjpegdec.c" 1
	D16MUL xr3,xr7,xr2,xr13,LW
 # 0 "" 2
 # 968 "mjpegdec.c" 1
	D16MAC xr3,xr8,xr4,xr13,SS,HW
 # 0 "" 2
 # 969 "mjpegdec.c" 1
	D16MAC xr3,xr7,xr12,xr13,SS,HW
 # 0 "" 2
 # 970 "mjpegdec.c" 1
	D16MAC xr3,xr8,xr14,xr13,SS,LW
 # 0 "" 2
 # 972 "mjpegdec.c" 1
	D16MACF xr3,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 974 "mjpegdec.c" 1
	D16MUL xr5,xr8,xr2,xr13,LW
 # 0 "" 2
 # 975 "mjpegdec.c" 1
	D16MAC xr5,xr7,xr4,xr13,SS,HW
 # 0 "" 2
 # 976 "mjpegdec.c" 1
	D16MAC xr5,xr8,xr12,xr13,AA,HW
 # 0 "" 2
 # 977 "mjpegdec.c" 1
	D16MAC xr5,xr7,xr14,xr13,AA,LW
 # 0 "" 2
 # 979 "mjpegdec.c" 1
	D16MACF xr5,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 981 "mjpegdec.c" 1
	D16MUL xr2,xr8,xr2,xr13,HW
 # 0 "" 2
 # 982 "mjpegdec.c" 1
	D16MAC xr2,xr8,xr4,xr13,SS,LW
 # 0 "" 2
 # 983 "mjpegdec.c" 1
	D16MAC xr2,xr7,xr12,xr13,AA,LW
 # 0 "" 2
 # 984 "mjpegdec.c" 1
	D16MAC xr2,xr7,xr14,xr13,SS,HW
 # 0 "" 2
 # 986 "mjpegdec.c" 1
	D16MACF xr2,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 988 "mjpegdec.c" 1
	Q16ADD xr15,xr15,xr11,xr11,AS,WW
 # 0 "" 2
 # 989 "mjpegdec.c" 1
	Q16ADD xr10,xr10,xr3,xr3,AS,WW
 # 0 "" 2
 # 990 "mjpegdec.c" 1
	Q16ADD xr1,xr1,xr5,xr5,AS,WW
 # 0 "" 2
 # 991 "mjpegdec.c" 1
	Q16ADD xr9,xr9,xr2,xr2,AS,WW
 # 0 "" 2
 # 993 "mjpegdec.c" 1
	Q16SAT xr15,xr15,xr10
 # 0 "" 2
 # 994 "mjpegdec.c" 1
	Q16SAT xr1,xr1,xr9
 # 0 "" 2
 # 995 "mjpegdec.c" 1
	Q16SAT xr2,xr2,xr5
 # 0 "" 2
 # 996 "mjpegdec.c" 1
	Q16SAT xr3,xr3,xr11
 # 0 "" 2
 # 1001 "mjpegdec.c" 1
	S16STD xr15,$4,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$4,$6
#APP
 # 1003 "mjpegdec.c" 1
	S16STD xr15,$5,0,ptn0
 # 0 "" 2
 # 1005 "mjpegdec.c" 1
	S16STD xr1,$3,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$6,$3
#APP
 # 1007 "mjpegdec.c" 1
	S16STD xr1,$5,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$5,$4,$9
#APP
 # 1009 "mjpegdec.c" 1
	S16STD xr2,$5,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$5,$6
#APP
 # 1011 "mjpegdec.c" 1
	S16STD xr2,$5,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$5,$9,$3
#APP
 # 1013 "mjpegdec.c" 1
	S16STD xr3,$5,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$4,$8
#APP
 # 1015 "mjpegdec.c" 1
	S16STD xr3,$5,0,ptn0
 # 0 "" 2
#NO_APP
	addiu	$4,$4,2
	.set	noreorder
	.set	nomacro
	bne	$2,$7,$L172
	addiu	$3,$3,2
	.set	macro
	.set	reorder

	j	$31
$L182:
	sll	$3,$6,1
	sll	$8,$6,3
	li	$10,1518469120			# 0x5a820000
	addu	$3,$4,$3
	addiu	$7,$7,12
	sll	$9,$6,2
	subu	$8,$8,$6
	addiu	$10,$10,30274
$L173:
	addiu	$2,$2,4
#APP
 # 1024 "mjpegdec.c" 1
	S32LDD xr1,$2,0
 # 0 "" 2
 # 1026 "mjpegdec.c" 1
	S32LDD xr3,$2,32
 # 0 "" 2
 # 1027 "mjpegdec.c" 1
	S32I2M xr5,$10
 # 0 "" 2
 # 1031 "mjpegdec.c" 1
	D16MUL xr15,xr5,xr1,xr9,HW
 # 0 "" 2
 # 1034 "mjpegdec.c" 1
	D16MACF xr15,xr0,xr0,xr9,AA,WW
 # 0 "" 2
 # 1036 "mjpegdec.c" 1
	D16MUL xr10,xr5,xr3,xr9,LW
 # 0 "" 2
 # 1039 "mjpegdec.c" 1
	D16MACF xr10,xr0,xr0,xr9,AA,WW
 # 0 "" 2
 # 1041 "mjpegdec.c" 1
	S32LDD xr2,$2,16
 # 0 "" 2
 # 1042 "mjpegdec.c" 1
	S32LDD xr4,$2,48
 # 0 "" 2
 # 1043 "mjpegdec.c" 1
	Q16ADD xr15,xr15,xr10,xr9,AS,WW
 # 0 "" 2
 # 1044 "mjpegdec.c" 1
	D16MUL xr10,xr5,xr1,xr1,HW
 # 0 "" 2
 # 1047 "mjpegdec.c" 1
	D16MACF xr10,xr0,xr0,xr1,AA,WW
 # 0 "" 2
 # 1049 "mjpegdec.c" 1
	D16MUL xr11,xr6,xr3,xr1,LW
 # 0 "" 2
 # 1052 "mjpegdec.c" 1
	D16MACF xr11,xr0,xr0,xr1,AA,WW
 # 0 "" 2
 # 1056 "mjpegdec.c" 1
	Q16ADD xr10,xr10,xr11,xr1,AS,WW
 # 0 "" 2
 # 1057 "mjpegdec.c" 1
	D16MUL xr11,xr7,xr2,xr13,HW
 # 0 "" 2
 # 1058 "mjpegdec.c" 1
	D16MAC xr11,xr7,xr4,xr13,AA,LW
 # 0 "" 2
 # 1062 "mjpegdec.c" 1
	D16MACF xr11,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 1064 "mjpegdec.c" 1
	D16MUL xr3,xr7,xr2,xr13,LW
 # 0 "" 2
 # 1065 "mjpegdec.c" 1
	D16MAC xr3,xr8,xr4,xr13,SS,HW
 # 0 "" 2
 # 1069 "mjpegdec.c" 1
	D16MACF xr3,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 1071 "mjpegdec.c" 1
	D16MUL xr5,xr8,xr2,xr13,LW
 # 0 "" 2
 # 1072 "mjpegdec.c" 1
	D16MAC xr5,xr7,xr4,xr13,SS,HW
 # 0 "" 2
 # 1076 "mjpegdec.c" 1
	D16MACF xr5,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 1078 "mjpegdec.c" 1
	D16MUL xr2,xr8,xr2,xr13,HW
 # 0 "" 2
 # 1079 "mjpegdec.c" 1
	D16MAC xr2,xr8,xr4,xr13,SS,LW
 # 0 "" 2
 # 1083 "mjpegdec.c" 1
	D16MACF xr2,xr0,xr0,xr13,AA,WW
 # 0 "" 2
 # 1085 "mjpegdec.c" 1
	Q16ADD xr15,xr15,xr11,xr11,AS,WW
 # 0 "" 2
 # 1086 "mjpegdec.c" 1
	Q16ADD xr10,xr10,xr3,xr3,AS,WW
 # 0 "" 2
 # 1087 "mjpegdec.c" 1
	Q16ADD xr1,xr1,xr5,xr5,AS,WW
 # 0 "" 2
 # 1088 "mjpegdec.c" 1
	Q16ADD xr9,xr9,xr2,xr2,AS,WW
 # 0 "" 2
 # 1090 "mjpegdec.c" 1
	Q16SAT xr15,xr15,xr10
 # 0 "" 2
 # 1091 "mjpegdec.c" 1
	Q16SAT xr1,xr1,xr9
 # 0 "" 2
 # 1092 "mjpegdec.c" 1
	Q16SAT xr2,xr2,xr5
 # 0 "" 2
 # 1093 "mjpegdec.c" 1
	Q16SAT xr3,xr3,xr11
 # 0 "" 2
 # 1097 "mjpegdec.c" 1
	S16STD xr15,$4,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$4,$6
#APP
 # 1099 "mjpegdec.c" 1
	S16STD xr15,$5,0,ptn0
 # 0 "" 2
 # 1101 "mjpegdec.c" 1
	S16STD xr1,$3,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$3,$6
#APP
 # 1103 "mjpegdec.c" 1
	S16STD xr1,$5,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$5,$4,$9
#APP
 # 1105 "mjpegdec.c" 1
	S16STD xr2,$5,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$6,$5
#APP
 # 1107 "mjpegdec.c" 1
	S16STD xr2,$5,0,ptn0
 # 0 "" 2
#NO_APP
	addu	$5,$3,$9
#APP
 # 1109 "mjpegdec.c" 1
	S16STD xr3,$5,0,ptn1
 # 0 "" 2
#NO_APP
	addu	$5,$4,$8
#APP
 # 1111 "mjpegdec.c" 1
	S16STD xr3,$5,0,ptn0
 # 0 "" 2
#NO_APP
	addiu	$4,$4,2
	.set	noreorder
	.set	nomacro
	bne	$2,$7,$L173
	addiu	$3,$3,2
	.set	macro
	.set	reorder

	j	$31
	.end	jmpeg_simple_idct_put_mxu
	.size	jmpeg_simple_idct_put_mxu, .-jmpeg_simple_idct_put_mxu
	.section	.rodata.str1.4
	.align	2
$LC16:
	.ascii	"lossless\000"
	.align	2
$LC17:
	.ascii	"sequential DCT\000"
	.align	2
$LC18:
	.ascii	"RGB\000"
	.align	2
$LC19:
	.ascii	"\000"
	.align	2
$LC20:
	.ascii	"PRCT\000"
	.align	2
$LC21:
	.ascii	"RCT\000"
	.align	2
$LC22:
	.ascii	"decode_sos: nb_components (%d) unsupported\012\000"
	.align	2
$LC23:
	.ascii	"decode_sos: invalid len (%d)\012\000"
	.align	2
$LC24:
	.ascii	"component: %d\012\000"
	.align	2
$LC25:
	.ascii	"decode_sos: index(%d) out of components\012\000"
	.align	2
$LC26:
	.ascii	"%s %s p:%d >>:%d ilv:%d bits:%d %s\012\000"
	.align	2
$LC27:
	.ascii	"mjpeg_decode_dc: bad vlc: %d:%d (%p)\012\000"
	.align	2
$LC28:
	.ascii	"error count: %d\012\000"
	.align	2
$LC29:
	.ascii	"Can not flip image with CODEC_FLAG_EMU_EDGE set!\012\000"
	.align	2
$LC30:
	.ascii	"error dc\012\000"
	.align	2
$LC31:
	.ascii	"error y=%d x=%d\012\000"
	.align	2
$LC32:
	.ascii	"decode_sos: ac/dc index out of range\012\000"
	.text
	.align	2
	.globl	ff_mjpeg_decode_sos
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_sos
	.type	ff_mjpeg_decode_sos, @function
ff_mjpeg_decode_sos:
	.frame	$sp,200,$31		# vars= 112, regs= 10/0, args= 40, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	move	$12,$4
	lw	$6,12($4)
	addiu	$sp,$sp,-200
	lw	$2,4($4)
	lw	$3,704($12)
	lui	$28,%hi(__gnu_local_gp)
	sw	$16,160($sp)
	li	$8,8			# 0x8
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$31,196($sp)
	li	$4,-4			# 0xfffffffffffffffc
	sw	$fp,192($sp)
	sw	$3,80($sp)
	srl	$3,$6,3
	sw	$23,188($sp)
	li	$16,1			# 0x1
	sw	$22,184($sp)
	addu	$3,$2,$3
	sw	$21,180($sp)
	sw	$20,176($sp)
	and	$5,$3,$4
	sw	$19,172($sp)
	sw	$18,168($sp)
	sw	$17,164($sp)
	.cprestore	40
	lw	$9,80($sp)
	movz	$16,$8,$9
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$5,$6,0x7
	addu	$3,$3,$5
	li	$5,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$5
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $7
 # 0 "" 2
#NO_APP
	addiu	$5,$6,16
	srl	$3,$5,3
	addu	$3,$2,$3
	and	$4,$3,$4
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$5,$5,0x7
	addu	$3,$3,$5
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $fp
 # 0 "" 2
#NO_APP
	addiu	$6,$6,24
	.set	noreorder
	.set	nomacro
	beq	$fp,$0,$L186
	sw	$6,12($12)
	.set	macro
	.set	reorder

	slt	$3,$fp,5
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L186
	addiu	$3,$fp,3
	.set	macro
	.set	reorder

	sll	$3,$3,1
	.set	noreorder
	.set	nomacro
	bne	$3,$7,$L189
	lw	$25,%call16(av_log)($28)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$fp,$L191
	lw	$4,0($12)
	.set	macro
	.set	reorder

	lui	$17,%hi($LC24)
	sw	$16,80($sp)
	li	$18,1246953472			# 0x4a530000
	addiu	$20,$12,840
	move	$22,$0
	li	$21,-4			# 0xfffffffffffffffc
	li	$19,8			# 0x8
	addiu	$17,$17,%lo($LC24)
	addiu	$18,$18,21581
	move	$23,$12
$L198:
	srl	$3,$6,3
	addu	$2,$2,$3
	and	$3,$2,$21
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$3,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$3,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$3,$6,0x7
	addu	$2,$2,$3
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$19
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $2
 # 0 "" 2
#NO_APP
	addiu	$6,$6,8
	lw	$25,%call16(av_log)($28)
	addiu	$16,$2,-1
	li	$5,48			# 0x30
	sw	$6,12($23)
	move	$7,$16
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$6,$17
	.set	macro
	.set	reorder

	lw	$3,772($23)
	.set	noreorder
	.set	nomacro
	blez	$3,$L192
	lw	$28,40($sp)
	.set	macro
	.set	reorder

	lw	$4,792($23)
	.set	noreorder
	.set	nomacro
	beq	$4,$16,$L426
	li	$4,1			# 0x1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$4,$L420
	move	$12,$23
	.set	macro
	.set	reorder

	lw	$4,796($23)
	.set	noreorder
	.set	nomacro
	beq	$4,$16,$L421
	li	$4,2			# 0x2
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$4,$L408
	li	$7,2			# 0x2
	.set	macro
	.set	reorder

	lw	$4,800($23)
	.set	noreorder
	.set	nomacro
	beq	$4,$16,$L423
	li	$4,3			# 0x3
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$4,$L408
	li	$7,3			# 0x3
	.set	macro
	.set	reorder

	lw	$4,804($23)
	.set	noreorder
	.set	nomacro
	beq	$4,$16,$L425
	li	$7,4			# 0x4
	.set	macro
	.set	reorder

	move	$12,$23
$L408:
	lui	$6,%hi($LC25)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($12)
	li	$5,16			# 0x10
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC25)
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
	sw	$2,80($sp)
$L458:
	lw	$31,196($sp)
	lw	$2,80($sp)
$L603:
	lw	$fp,192($sp)
	lw	$23,188($sp)
	lw	$22,184($sp)
	lw	$21,180($sp)
	lw	$20,176($sp)
	lw	$19,172($sp)
	lw	$18,168($sp)
	lw	$17,164($sp)
	lw	$16,160($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,200
	.set	macro
	.set	reorder

$L192:
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L544
	move	$12,$23
	.set	macro
	.set	reorder

$L426:
	move	$2,$0
$L193:
	lw	$4,0($23)
	lw	$5,228($4)
	.set	noreorder
	.set	nomacro
	beq	$5,$18,$L545
	li	$5,3			# 0x3
	.set	macro
	.set	reorder

$L196:
	sll	$3,$2,2
$L585:
	sw	$2,0($20)
	lw	$2,4($23)
	addu	$3,$23,$3
	lw	$5,808($3)
	lw	$3,824($3)
	mul	$6,$5,$3
	sw	$5,64($20)
	sw	$3,80($20)
	sw	$6,48($20)
	lw	$6,12($23)
	srl	$3,$6,3
	addu	$3,$2,$3
	and	$5,$3,$21
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$5,$6,0x7
	addu	$3,$3,$5
	li	$8,4			# 0x4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $5
 # 0 "" 2
#NO_APP
	addiu	$7,$6,4
	sw	$5,16($20)
	srl	$3,$7,3
	addu	$3,$2,$3
	and	$9,$3,$21
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$9,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$9,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$7,$7,0x7
	addu	$3,$3,$7
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $7
 # 0 "" 2
#NO_APP
	addiu	$6,$6,8
	sltu	$3,$5,4
	sw	$6,12($23)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L197
	sw	$7,32($20)
	.set	macro
	.set	reorder

	sltu	$3,$7,4
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L197
	addiu	$3,$5,34
	.set	macro
	.set	reorder

	sll	$3,$3,4
	addu	$3,$23,$3
	lw	$3,4($3)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L197
	addiu	$3,$7,38
	.set	macro
	.set	reorder

	sll	$3,$3,4
	addu	$3,$23,$3
	lw	$3,4($3)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L197
	addiu	$22,$22,1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$22,$fp,$L198
	addiu	$20,$20,4
	.set	macro
	.set	reorder

	lw	$3,704($23)
	move	$12,$23
	lw	$16,80($sp)
	sw	$3,80($sp)
$L191:
	srl	$3,$6,3
	li	$5,-4			# 0xfffffffffffffffc
	addu	$3,$2,$3
	and	$7,$3,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$7,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$7,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$7,$6,0x7
	addu	$3,$3,$7
	li	$8,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $9
 # 0 "" 2
#NO_APP
	addiu	$7,$6,8
	sw	$9,92($sp)
	srl	$3,$7,3
	addu	$3,$2,$3
	and	$9,$3,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$9,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$9,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$7,$7,0x7
	addu	$3,$3,$7
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$8
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $19
 # 0 "" 2
#NO_APP
	addiu	$7,$6,16
	srl	$3,$7,3
	addu	$3,$2,$3
	and	$8,$3,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$8,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$8,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$7,$7,0x7
	addu	$3,$3,$7
	li	$7,4			# 0x4
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$7
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $10
 # 0 "" 2
#NO_APP
	addiu	$3,$6,20
	sw	$10,100($sp)
	srl	$8,$3,3
	addu	$2,$2,$8
	and	$5,$2,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$3,$3,0x7
	addu	$2,$2,$3
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$7
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $24
 # 0 "" 2
#NO_APP
	addiu	$6,$6,24
	sw	$24,96($sp)
	.set	noreorder
	.set	nomacro
	blez	$fp,$L199
	sw	$6,12($12)
	.set	macro
	.set	reorder

	li	$2,1024			# 0x400
	li	$3,1			# 0x1
	.set	noreorder
	.set	nomacro
	beq	$fp,$3,$L199
	sw	$2,960($12)
	.set	macro
	.set	reorder

	li	$3,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$fp,$3,$L200
	sw	$2,964($12)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$fp,$7,$L200
	sw	$2,968($12)
	.set	macro
	.set	reorder

	sw	$2,972($12)
$L200:
	lw	$3,936($12)
	lw	$5,756($12)
	lw	$7,940($12)
	mul	$6,$16,$3
	lw	$2,760($12)
	mul	$16,$16,$7
	addu	$5,$6,$5
	addiu	$3,$5,-1
	addu	$2,$16,$2
	teq	$6,$0,7
	div	$0,$3,$6
	addiu	$2,$2,-1
	mflo	$3
	teq	$16,$0,7
	div	$0,$2,$16
	sw	$3,764($12)
	mflo	$16
	sw	$16,768($12)
$L201:
	lw	$2,404($4)
	andi	$2,$2,0x1
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L202
	lw	$2,80($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L427
	lui	$7,%hi($LC17)
	.set	macro
	.set	reorder

	lui	$7,%hi($LC16)
	addiu	$7,$7,%lo($LC16)
$L203:
	lw	$2,716($12)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L428
	lui	$3,%hi($LC18)
	.set	macro
	.set	reorder

	lui	$3,%hi($LC19)
	addiu	$3,$3,%lo($LC19)
$L204:
	lw	$2,724($12)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L429
	lw	$8,728($12)
	.set	macro
	.set	reorder

	lw	$2,720($12)
	beq	$2,$0,$L546
	lui	$2,%hi($LC21)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L205
	.option	pic2
	addiu	$2,$2,%lo($LC21)
	.set	macro
	.set	reorder

$L545:
	bne	$fp,$5,$L196
	.set	noreorder
	.set	nomacro
	bne	$3,$fp,$L585
	sll	$3,$2,2
	.set	macro
	.set	reorder

	subu	$3,$fp,$22
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L196
	.option	pic2
	movn	$2,$3,$22
	.set	macro
	.set	reorder

$L197:
	lui	$6,%hi($LC32)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC32)
	.set	macro
	.set	reorder

	li	$8,-1			# 0xffffffffffffffff
	lw	$31,196($sp)
	lw	$fp,192($sp)
	lw	$23,188($sp)
	lw	$22,184($sp)
	lw	$21,180($sp)
	lw	$20,176($sp)
	lw	$19,172($sp)
	lw	$18,168($sp)
	lw	$17,164($sp)
	lw	$16,160($sp)
	sw	$8,80($sp)
	lw	$2,80($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,200
	.set	macro
	.set	reorder

$L420:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	li	$7,1			# 0x1
	.set	macro
	.set	reorder

$L423:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L193
	.option	pic2
	li	$2,2			# 0x2
	.set	macro
	.set	reorder

$L421:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L193
	.option	pic2
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

$L425:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L193
	.option	pic2
	li	$2,3			# 0x3
	.set	macro
	.set	reorder

$L429:
	lui	$2,%hi($LC20)
	addiu	$2,$2,%lo($LC20)
$L205:
	lw	$9,96($sp)
	lui	$6,%hi($LC26)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	sw	$3,16($sp)
	addiu	$6,$6,%lo($LC26)
	sw	$12,156($sp)
	sw	$9,24($sp)
	sw	$19,28($sp)
	sw	$8,32($sp)
	sw	$2,36($sp)
	lw	$3,92($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,20($sp)
	.set	macro
	.set	reorder

	lw	$12,156($sp)
	lw	$28,40($sp)
	lw	$10,704($12)
	sw	$10,80($sp)
$L202:
	lw	$2,5680($12)
	.set	noreorder
	.set	nomacro
	blez	$2,$L586
	lw	$24,80($sp)
	.set	macro
	.set	reorder

	lw	$3,12($12)
$L209:
	addiu	$3,$3,8
	addiu	$2,$2,-1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L209
	sw	$3,12($12)
	.set	macro
	.set	reorder

	lw	$24,80($sp)
$L586:
	beq	$24,$0,$L547
	lw	$2,708($12)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L548
	sw	$2,80($sp)
	.set	macro
	.set	reorder

	lw	$2,716($12)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L549
	addiu	$4,$12,5692
	.set	macro
	.set	reorder

	lw	$6,764($12)
	lw	$10,1196($12)
	addiu	$5,$12,5696
	lw	$25,%call16(av_fast_malloc)($28)
	li	$16,1			# 0x1
	sll	$6,$6,3
	lw	$17,728($12)
	sw	$12,156($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_fast_malloc
1:	jalr	$25
	sw	$10,124($sp)
	.set	macro
	.set	reorder

	sll	$17,$16,$17
	lw	$12,156($sp)
	lw	$8,96($sp)
	addiu	$17,$17,-1
	lw	$28,40($sp)
	lw	$2,728($12)
	lw	$24,5692($12)
	lw	$3,768($12)
	addu	$2,$8,$2
	addiu	$9,$24,2
	addiu	$2,$2,-1
	sw	$24,104($sp)
	sll	$2,$16,$2
	sw	$9,128($sp)
	andi	$2,$2,0xffff
	sh	$2,0($24)
	sh	$2,2($24)
	.set	noreorder
	.set	nomacro
	blez	$3,$L458
	sh	$2,4($24)
	.set	macro
	.set	reorder

	li	$3,65535			# 0xffff
	lw	$10,124($sp)
	sll	$18,$17,16
	lw	$2,764($12)
	sll	$3,$3,$8
	sw	$0,120($sp)
	sra	$10,$10,1
	sw	$0,116($sp)
	andi	$3,$3,0xffff
	sra	$18,$18,16
	li	$9,1			# 0x1
	sw	$10,132($sp)
	sw	$3,108($sp)
	move	$20,$12
	move	$17,$18
$L216:
	lw	$4,976($20)
	lw	$8,120($sp)
	lw	$3,696($20)
	addu	$4,$4,$8
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L218
	sw	$4,112($sp)
	.set	macro
	.set	reorder

	lw	$24,132($sp)
	move	$10,$4
	lw	$4,700($20)
	addu	$3,$10,$24
	movn	$10,$3,$4
	sw	$10,112($sp)
$L218:
	lw	$3,104($sp)
	lhu	$5,0($3)
	lhu	$4,2($3)
	lhu	$3,4($3)
	sw	$5,64($sp)
	sw	$5,48($sp)
	sw	$4,68($sp)
	sw	$4,52($sp)
	sw	$3,72($sp)
	.set	noreorder
	.set	nomacro
	blez	$2,$L242
	sw	$3,56($sp)
	.set	macro
	.set	reorder

	addiu	$10,$sp,64
	lw	$19,104($sp)
	lui	$2,%hi($L223)
	lw	$3,5660($20)
	sll	$12,$9,2
	addiu	$8,$sp,48
	sw	$10,100($sp)
	addiu	$24,$20,856
	addiu	$2,$2,%lo($L223)
	addiu	$10,$20,868
	sw	$8,84($sp)
	move	$14,$0
	sw	$24,88($sp)
	sltu	$18,$9,7
	addu	$12,$2,$12
	move	$21,$19
	move	$22,$14
	move	$19,$10
	move	$9,$18
	move	$10,$17
$L240:
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L587
	lw	$fp,88($sp)
	.set	macro
	.set	reorder

	lw	$2,5664($20)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L588
	addiu	$17,$sp,48
	.set	macro
	.set	reorder

	sw	$3,5664($20)
	lw	$fp,88($sp)
$L587:
	addiu	$17,$sp,48
$L588:
	addiu	$18,$sp,64
	move	$23,$21
	move	$16,$9
$L234:
	lhu	$8,0($23)
	lw	$3,0($17)
	.set	noreorder
	.set	nomacro
	beq	$16,$0,$L221
	sw	$8,0($17)
	.set	macro
	.set	reorder

	lw	$2,0($12)
	j	$2
	.rdata
	.align	2
	.align	2
$L223:
	.word	$L221
	.word	$L222
	.word	$L224
	.word	$L225
	.word	$L226
	.word	$L227
	.word	$L228
	.text
$L186:
	lui	$6,%hi($LC22)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($12)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC22)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$fp
	.set	macro
	.set	reorder

	li	$10,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$10,80($sp)
	.set	macro
	.set	reorder

$L547:
	lw	$2,712($12)
	beq	$2,$0,$L305
	lw	$8,92($sp)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L550
	lw	$24,96($sp)
	.set	macro
	.set	reorder

$L305:
	lw	$2,5688($12)
	beq	$2,$0,$L353
	lw	$4,0($12)
	lw	$2,12($4)
	andi	$2,$2,0x4000
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L551
	lui	$6,%hi($LC29)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$fp,$L415
	addiu	$6,$sp,48
	.set	macro
	.set	reorder

	lw	$2,840($12)
	lw	$4,768($12)
	sll	$7,$2,3
	sll	$2,$2,2
	addu	$7,$12,$7
	addu	$10,$12,$2
	addu	$8,$6,$2
	lw	$9,1376($7)
	sll	$5,$4,3
	lw	$3,1196($10)
	lw	$10,976($10)
	ori	$9,$9,0x1
	subu	$11,$0,$3
	sw	$9,1376($7)
	lw	$2,760($12)
	lw	$7,940($12)
	lw	$9,920($12)
	sw	$11,0($8)
	teq	$7,$0,7
	div	$0,$2,$7
	mflo	$2
	andi	$2,$2,0x7
	subu	$2,$5,$2
	mul	$2,$2,$9
	addiu	$2,$2,-1
	mul	$7,$3,$2
	li	$2,1			# 0x1
	addu	$3,$7,$10
	.set	noreorder
	.set	nomacro
	beq	$fp,$2,$L355
	sw	$3,16($8)
	.set	macro
	.set	reorder

	lw	$2,844($12)
	sll	$8,$2,3
	sll	$2,$2,2
	addu	$8,$12,$8
	addu	$3,$12,$2
	addu	$9,$6,$2
	lw	$2,1376($8)
	lw	$7,1196($3)
	lw	$10,976($3)
	ori	$2,$2,0x1
	subu	$3,$0,$7
	sw	$2,1376($8)
	lw	$2,940($12)
	lw	$8,760($12)
	lw	$11,924($12)
	sw	$3,0($9)
	teq	$2,$0,7
	div	$0,$8,$2
	mflo	$2
	andi	$2,$2,0x7
	subu	$2,$5,$2
	mul	$3,$2,$11
	addiu	$3,$3,-1
	mul	$2,$7,$3
	addu	$3,$2,$10
	li	$2,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$fp,$2,$L355
	sw	$3,16($9)
	.set	macro
	.set	reorder

	lw	$2,848($12)
	sll	$8,$2,3
	sll	$2,$2,2
	addu	$8,$12,$8
	addu	$3,$12,$2
	addu	$2,$6,$2
	lw	$9,1376($8)
	lw	$7,1196($3)
	lw	$10,976($3)
	ori	$9,$9,0x1
	subu	$11,$0,$7
	sw	$9,1376($8)
	lw	$3,760($12)
	lw	$8,940($12)
	lw	$9,928($12)
	sw	$11,0($2)
	teq	$8,$0,7
	div	$0,$3,$8
	mflo	$3
	andi	$3,$3,0x7
	subu	$3,$5,$3
	mul	$3,$3,$9
	addiu	$3,$3,-1
	mul	$8,$7,$3
	addu	$3,$8,$10
	sw	$3,16($2)
	li	$2,4			# 0x4
	bne	$fp,$2,$L355
	lw	$2,852($12)
	sll	$3,$2,3
	sll	$2,$2,2
	addu	$3,$12,$3
	addu	$9,$12,$2
	addu	$6,$6,$2
	lw	$8,1376($3)
	lw	$7,1196($9)
	lw	$9,976($9)
	ori	$8,$8,0x1
	subu	$11,$0,$7
	sw	$8,1376($3)
	lw	$2,760($12)
	lw	$3,940($12)
	lw	$10,932($12)
	sw	$11,0($6)
	teq	$3,$0,7
	div	$0,$2,$3
	mflo	$3
	andi	$3,$3,0x7
	subu	$2,$5,$3
	mul	$2,$2,$10
	addiu	$2,$2,-1
	mul	$3,$7,$2
	addu	$2,$3,$9
	sw	$2,16($6)
$L355:
	.set	noreorder
	.set	nomacro
	blez	$4,$L458
	li	$3,16711680			# 0xff0000
	.set	macro
	.set	reorder

	lw	$2,764($12)
	addiu	$23,$sp,48
	sw	$0,112($sp)
	addiu	$11,$3,255
	sw	$fp,104($sp)
	li	$3,-16777216			# 0xffffffffff000000
	ori	$16,$3,0xff00
$L357:
	.set	noreorder
	.set	nomacro
	blez	$2,$L405
	sw	$0,108($sp)
	.set	macro
	.set	reorder

	move	$17,$12
	sw	$23,124($sp)
$L484:
	lw	$3,5660($17)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L358
	lw	$8,104($sp)
	.set	macro
	.set	reorder

	lw	$2,5664($17)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L552
	lw	$9,104($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgtz	$9,$L589
	lw	$22,124($sp)
	.set	macro
	.set	reorder

$L411:
	addiu	$2,$2,-1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L536
	sw	$2,5664($17)
	.set	macro
	.set	reorder

	lw	$2,12($17)
	subu	$3,$0,$2
	andi	$3,$3,0x7
	beq	$3,$0,$L398
	addu	$2,$3,$2
	sw	$2,12($17)
$L398:
	addiu	$2,$2,16
	lw	$24,104($sp)
	.set	noreorder
	.set	nomacro
	blez	$24,$L536
	sw	$2,12($17)
	.set	macro
	.set	reorder

	li	$3,1024			# 0x400
	li	$2,1			# 0x1
	.set	noreorder
	.set	nomacro
	beq	$24,$2,$L536
	sw	$3,960($17)
	.set	macro
	.set	reorder

	li	$2,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$24,$2,$L536
	sw	$3,964($17)
	.set	macro
	.set	reorder

	li	$2,4			# 0x4
	.set	noreorder
	.set	nomacro
	bne	$24,$2,$L536
	sw	$3,968($17)
	.set	macro
	.set	reorder

	lw	$2,764($17)
	sw	$3,972($17)
$L395:
	lw	$24,108($sp)
$L590:
	addiu	$24,$24,1
	slt	$3,$24,$2
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L484
	sw	$24,108($sp)
	.set	macro
	.set	reorder

	lw	$23,124($sp)
	move	$12,$17
	lw	$4,768($17)
$L405:
	lw	$3,112($sp)
	addiu	$3,$3,1
	sw	$3,112($sp)
	slt	$3,$3,$4
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L357
	lw	$31,196($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L603
	.option	pic2
	lw	$2,80($sp)
	.set	macro
	.set	reorder

$L428:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L204
	.option	pic2
	addiu	$3,$3,%lo($LC18)
	.set	macro
	.set	reorder

$L427:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L203
	.option	pic2
	addiu	$7,$7,%lo($LC17)
	.set	macro
	.set	reorder

$L551:
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	sw	$12,156($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC29)
	.set	macro
	.set	reorder

	lw	$12,156($sp)
	lw	$28,40($sp)
	sw	$0,5688($12)
$L353:
	.set	noreorder
	.set	nomacro
	blez	$fp,$L415
	addiu	$5,$sp,48
	.set	macro
	.set	reorder

	lw	$2,840($12)
	lw	$4,768($12)
	sll	$3,$2,3
	sll	$2,$2,2
	addu	$3,$12,$3
	addu	$7,$12,$2
	addu	$2,$5,$2
	lw	$6,1376($3)
	lw	$8,976($7)
	lw	$7,1196($7)
	ori	$6,$6,0x1
	sw	$8,16($2)
	sw	$7,0($2)
	li	$2,1			# 0x1
	.set	noreorder
	.set	nomacro
	beq	$fp,$2,$L355
	sw	$6,1376($3)
	.set	macro
	.set	reorder

	lw	$3,844($12)
	sll	$2,$3,3
	sll	$3,$3,2
	addu	$2,$12,$2
	addu	$7,$12,$3
	addu	$3,$5,$3
	lw	$6,1376($2)
	lw	$8,976($7)
	lw	$7,1196($7)
	ori	$6,$6,0x1
	sw	$6,1376($2)
	li	$2,2			# 0x2
	sw	$8,16($3)
	.set	noreorder
	.set	nomacro
	beq	$fp,$2,$L355
	sw	$7,0($3)
	.set	macro
	.set	reorder

	lw	$3,848($12)
	sll	$2,$3,3
	sll	$3,$3,2
	addu	$2,$12,$2
	addu	$7,$12,$3
	addu	$3,$5,$3
	lw	$6,1376($2)
	lw	$8,976($7)
	lw	$7,1196($7)
	ori	$6,$6,0x1
	sw	$6,1376($2)
	li	$2,4			# 0x4
	sw	$8,16($3)
	.set	noreorder
	.set	nomacro
	bne	$fp,$2,$L355
	sw	$7,0($3)
	.set	macro
	.set	reorder

	lw	$2,852($12)
	sll	$3,$2,3
	sll	$2,$2,2
	addu	$3,$12,$3
	addu	$6,$12,$2
	addu	$2,$5,$2
	lw	$5,1376($3)
	lw	$7,976($6)
	lw	$6,1196($6)
	ori	$5,$5,0x1
	sw	$7,16($2)
	sw	$6,0($2)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L355
	.option	pic2
	sw	$5,1376($3)
	.set	macro
	.set	reorder

$L199:
	lw	$2,708($12)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L201
	li	$8,1			# 0x1
	.set	macro
	.set	reorder

	lw	$6,920($12)
	lw	$3,940($12)
	lw	$7,904($12)
	lw	$2,936($12)
	teq	$6,$0,7
	div	$0,$3,$6
	lw	$6,760($12)
	lw	$5,756($12)
	sw	$8,888($12)
	sw	$8,904($12)
	sw	$8,920($12)
	mflo	$3
	teq	$7,$0,7
	div	$0,$2,$7
	mflo	$2
	mul	$3,$16,$3
	mul	$16,$16,$2
	addu	$6,$3,$6
	addu	$2,$16,$5
	addiu	$5,$6,-1
	addiu	$2,$2,-1
	teq	$3,$0,7
	div	$0,$5,$3
	mflo	$3
	teq	$16,$0,7
	div	$0,$2,$16
	sw	$3,768($12)
	mflo	$16
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L201
	.option	pic2
	sw	$16,764($12)
	.set	macro
	.set	reorder

$L189:
	lui	$6,%hi($LC23)
	lw	$4,0($12)
	li	$5,16			# 0x10
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC23)
	.set	macro
	.set	reorder

	li	$24,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$24,80($sp)
	.set	macro
	.set	reorder

$L358:
	.set	noreorder
	.set	nomacro
	blez	$8,$L590
	lw	$24,108($sp)
	.set	macro
	.set	reorder

$L410:
	lw	$22,124($sp)
$L589:
	addiu	$23,$17,960
	sw	$0,120($sp)
$L362:
	lw	$9,-56($23)
	lw	$fp,-72($23)
	lw	$2,-120($23)
	lw	$3,-40($23)
	.set	noreorder
	.set	nomacro
	blez	$fp,$L394
	sw	$9,88($sp)
	.set	macro
	.set	reorder

	lw	$4,112($sp)
	sll	$20,$2,2
	lw	$8,108($sp)
	addiu	$2,$2,236
	move	$19,$0
	mul	$4,$3,$4
	lw	$3,88($sp)
	sll	$2,$2,2
	move	$18,$0
	mul	$3,$3,$8
	addu	$2,$17,$2
	move	$21,$0
	sw	$4,84($sp)
	sw	$2,116($sp)
	sw	$3,92($sp)
	addu	$3,$22,$20
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L393
	.option	pic2
	lw	$8,0($3)
	.set	macro
	.set	reorder

$L365:
	addu	$4,$17,$20
	lw	$9,100($sp)
	lw	$2,776($4)
	lw	$6,1344($4)
	mul	$7,$3,$2
	addu	$2,$7,$5
	sll	$2,$2,7
	.set	noreorder
	.set	nomacro
	beq	$9,$0,$L384
	addu	$2,$6,$2
	.set	macro
	.set	reorder

	lw	$5,12($17)
	lw	$3,4($17)
	srl	$6,$5,3
	addu	$3,$3,$6
	li	$6,-4			# 0xfffffffffffffffc
	and	$6,$3,$6
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$6,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$6,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$6,$5,0x7
	addu	$3,$3,$6
	li	$6,1			# 0x1
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$6
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addiu	$5,$5,1
	lhu	$6,0($2)
	lw	$9,96($sp)
	sw	$5,12($17)
	lw	$4,944($4)
	sll	$4,$4,7
	addu	$4,$17,$4
	lh	$5,32($4)
	mul	$3,$3,$5
	sll	$3,$3,$9
	addu	$3,$3,$6
	sh	$3,0($2)
$L383:
	lw	$10,88($sp)
	addiu	$18,$18,1
	beq	$10,$18,$L553
$L392:
	addiu	$19,$19,1
	.set	noreorder
	.set	nomacro
	beq	$19,$fp,$L591
	lw	$10,120($sp)
	.set	macro
	.set	reorder

$L393:
	lw	$9,84($sp)
	addu	$6,$22,$20
	lw	$10,92($sp)
	lw	$4,0($17)
	addu	$3,$21,$9
	lw	$6,16($6)
	addu	$5,$18,$10
	lw	$7,696($17)
	mul	$9,$3,$8
	lw	$4,656($4)
	addu	$2,$9,$5
	sll	$2,$2,3
	sra	$2,$2,$4
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L364
	addu	$4,$6,$2
	.set	macro
	.set	reorder

	lw	$2,700($17)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L364
	sra	$2,$8,1
	.set	macro
	.set	reorder

	addu	$4,$4,$2
$L364:
	lw	$2,712($17)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L365
	addiu	$7,$17,1216
	.set	macro
	.set	reorder

#APP
 # 1160 "mjpegdec.c" 1
	S32STD xr0,$7,0
 # 0 "" 2
 # 1160 "mjpegdec.c" 1
	S32STD xr0,$7,4
 # 0 "" 2
 # 1161 "mjpegdec.c" 1
	S32STD xr0,$7,8
 # 0 "" 2
 # 1161 "mjpegdec.c" 1
	S32STD xr0,$7,12
 # 0 "" 2
 # 1162 "mjpegdec.c" 1
	S32STD xr0,$7,16
 # 0 "" 2
 # 1162 "mjpegdec.c" 1
	S32STD xr0,$7,20
 # 0 "" 2
 # 1163 "mjpegdec.c" 1
	S32STD xr0,$7,24
 # 0 "" 2
 # 1163 "mjpegdec.c" 1
	S32STD xr0,$7,28
 # 0 "" 2
 # 1164 "mjpegdec.c" 1
	S32STD xr0,$7,32
 # 0 "" 2
 # 1164 "mjpegdec.c" 1
	S32STD xr0,$7,36
 # 0 "" 2
 # 1165 "mjpegdec.c" 1
	S32STD xr0,$7,40
 # 0 "" 2
 # 1165 "mjpegdec.c" 1
	S32STD xr0,$7,44
 # 0 "" 2
 # 1166 "mjpegdec.c" 1
	S32STD xr0,$7,48
 # 0 "" 2
 # 1166 "mjpegdec.c" 1
	S32STD xr0,$7,52
 # 0 "" 2
 # 1167 "mjpegdec.c" 1
	S32STD xr0,$7,56
 # 0 "" 2
 # 1167 "mjpegdec.c" 1
	S32STD xr0,$7,60
 # 0 "" 2
 # 1168 "mjpegdec.c" 1
	S32STD xr0,$7,64
 # 0 "" 2
 # 1168 "mjpegdec.c" 1
	S32STD xr0,$7,68
 # 0 "" 2
 # 1169 "mjpegdec.c" 1
	S32STD xr0,$7,72
 # 0 "" 2
 # 1169 "mjpegdec.c" 1
	S32STD xr0,$7,76
 # 0 "" 2
 # 1170 "mjpegdec.c" 1
	S32STD xr0,$7,80
 # 0 "" 2
 # 1170 "mjpegdec.c" 1
	S32STD xr0,$7,84
 # 0 "" 2
 # 1171 "mjpegdec.c" 1
	S32STD xr0,$7,88
 # 0 "" 2
 # 1171 "mjpegdec.c" 1
	S32STD xr0,$7,92
 # 0 "" 2
 # 1172 "mjpegdec.c" 1
	S32STD xr0,$7,96
 # 0 "" 2
 # 1172 "mjpegdec.c" 1
	S32STD xr0,$7,100
 # 0 "" 2
 # 1173 "mjpegdec.c" 1
	S32STD xr0,$7,104
 # 0 "" 2
 # 1173 "mjpegdec.c" 1
	S32STD xr0,$7,108
 # 0 "" 2
 # 1174 "mjpegdec.c" 1
	S32STD xr0,$7,112
 # 0 "" 2
 # 1174 "mjpegdec.c" 1
	S32STD xr0,$7,116
 # 0 "" 2
 # 1175 "mjpegdec.c" 1
	S32STD xr0,$7,120
 # 0 "" 2
 # 1175 "mjpegdec.c" 1
	S32STD xr0,$7,124
 # 0 "" 2
#NO_APP
	lw	$3,12($17)
	lw	$12,28($17)
	lw	$25,-104($23)
	srl	$6,$3,3
	lw	$10,116($sp)
	lw	$5,-88($23)
	addu	$6,$12,$6
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $24, 3($6)  
	lwr $24, 0($6)  
	
 # 0 "" 2
#NO_APP
	srl	$8,$24,8
	lw	$14,0($10)
	sll	$6,$24,8
	and	$8,$8,$11
	and	$6,$6,$16
	or	$6,$8,$6
	addiu	$24,$25,34
	sll	$9,$6,16
	srl	$6,$6,16
	sll	$8,$24,4
	or	$6,$9,$6
	andi	$10,$3,0x7
	sll	$10,$6,$10
	addu	$6,$17,$8
	srl	$8,$10,23
	sll	$14,$14,7
	lw	$13,4($6)
	sll	$8,$8,2
	addiu	$14,$14,32
	addu	$8,$13,$8
	addu	$14,$17,$14
	lh	$6,2($8)
	.set	noreorder
	.set	nomacro
	bltz	$6,$L366
	lh	$9,0($8)
	.set	macro
	.set	reorder

	move	$12,$9
	.set	noreorder
	.set	nomacro
	bltz	$12,$L554
	addu	$9,$6,$3
	.set	macro
	.set	reorder

$L368:
	.set	noreorder
	.set	nomacro
	bne	$12,$0,$L555
	sll	$10,$10,$6
	.set	macro
	.set	reorder

	sw	$9,12($17)
$L371:
	lw	$3,0($23)
#APP
 # 469 "mjpegdec.c" 1
	S32I2M xr2,$3
 # 0 "" 2
#NO_APP
	lhu	$3,0($14)
#APP
 # 470 "mjpegdec.c" 1
	S32MADD xr3,xr2,$2,$3
 # 0 "" 2
 # 471 "mjpegdec.c" 1
	S32CPS xr11,xr0,xr0
 # 0 "" 2
 # 472 "mjpegdec.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addiu	$2,$5,38
	lw	$10,4($17)
	move	$12,$0
	sw	$3,0($23)
	sll	$2,$2,4
	sh	$3,1216($17)
	li	$15,16			# 0x10
	addu	$2,$17,$2
	li	$24,256			# 0x100
	move	$25,$0
	lw	$13,4($2)
	li	$31,-1			# 0xffffffffffffffff
$L373:
	srl	$3,$9,3
	andi	$2,$9,0x7
	addu	$3,$10,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $5, 3($3)  
	lwr $5, 0($3)  
	
 # 0 "" 2
#NO_APP
	move	$3,$5
	sll	$3,$3,8
	srl	$5,$5,8
	and	$3,$3,$16
	and	$5,$5,$11
	or	$3,$5,$3
	sll	$5,$3,16
	srl	$3,$3,16
	or	$3,$5,$3
	sll	$2,$3,$2
	srl	$3,$2,23
	sll	$3,$3,2
	addu	$3,$13,$3
	lh	$5,2($3)
	.set	noreorder
	.set	nomacro
	bltz	$5,$L556
	lh	$3,0($3)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$15,$L376
	addu	$9,$9,$5
	.set	macro
	.set	reorder

$L558:
	srl	$6,$3,4
	.set	noreorder
	.set	nomacro
	beq	$3,$24,$L373
	addu	$12,$6,$12
	.set	macro
	.set	reorder

	andi	$3,$3,0xf
	slt	$6,$3,10
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L592
	sll	$5,$2,$5
	.set	macro
	.set	reorder

	srl	$2,$9,3
	andi	$5,$9,0x7
	addu	$2,$10,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $6, 3($2)  
	lwr $6, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$6,8
	sll	$8,$6,8
	and	$6,$2,$11
	and	$2,$8,$16
	or	$2,$6,$2
	sll	$6,$2,16
	srl	$2,$2,16
	or	$2,$6,$2
	sll	$5,$2,$5
$L592:
#APP
 # 495 "mjpegdec.c" 1
	S32I2M xr4,$5
 # 0 "" 2
 # 496 "mjpegdec.c" 1
	S32EXTRV xr4,xr0,$25,$3
 # 0 "" 2
#NO_APP
	.set	noreorder
	.set	nomacro
	bltz	$5,$L380
	sll	$2,$31,$3
	.set	macro
	.set	reorder

#APP
 # 498 "mjpegdec.c" 1
	S32I2M xr5,$2
 # 0 "" 2
 # 499 "mjpegdec.c" 1
	S32OR xr4,xr4,xr5
 # 0 "" 2
#NO_APP
$L380:
	slt	$2,$12,63
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L557
	addu	$9,$9,$3
	.set	macro
	.set	reorder

	addu	$2,$17,$12
	lbu	$2,1412($2)
#APP
 # 521 "mjpegdec.c" 1
	S32I2M xr10,$2
 # 0 "" 2
#NO_APP
	sll	$2,$2,1
	addu	$3,$14,$2
	lhu	$3,0($3)
#APP
 # 522 "mjpegdec.c" 1
	S32I2M xr12,$3
 # 0 "" 2
 # 523 "mjpegdec.c" 1
	D16MUL xr0,xr4,xr12,xr13,WW
 # 0 "" 2
 # 524 "mjpegdec.c" 1
	S32MAX xr11,xr11,xr10
 # 0 "" 2
 # 525 "mjpegdec.c" 1
	S32M2I xr13, $3
 # 0 "" 2
#NO_APP
	addu	$2,$7,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L373
	.option	pic2
	sh	$3,0($2)
	.set	macro
	.set	reorder

$L556:
	addiu	$9,$9,9
	srl	$6,$9,3
	andi	$2,$9,0x7
	addu	$6,$10,$6
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $8, 3($6)  
	lwr $8, 0($6)  
	
 # 0 "" 2
#NO_APP
	move	$6,$8
	sll	$6,$6,8
	srl	$8,$8,8
	and	$6,$6,$16
	and	$8,$8,$11
	or	$6,$8,$6
	sll	$8,$6,16
	srl	$6,$6,16
	or	$6,$8,$6
	sll	$2,$6,$2
	srl	$5,$2,$5
	addu	$3,$5,$3
	sll	$5,$3,2
	addu	$5,$13,$5
	lh	$3,0($5)
	lh	$5,2($5)
	.set	noreorder
	.set	nomacro
	bne	$3,$15,$L558
	addu	$9,$9,$5
	.set	macro
	.set	reorder

$L376:
	sw	$9,12($17)
#APP
 # 1186 "mjpegdec.c" 1
	S32M2I xr11, $5
 # 0 "" 2
#NO_APP
	addu	$2,$22,$20
	sra	$5,$5,3
	addiu	$18,$18,1
	lw	$8,0($2)
	addiu	$5,$5,1
	sw	$11,148($sp)
	move	$6,$8
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	jmpeg_simple_idct_put_mxu
	.option	pic2
	sw	$8,144($sp)
	.set	macro
	.set	reorder

	lw	$10,88($sp)
	lw	$28,40($sp)
	lw	$8,144($sp)
	.set	noreorder
	.set	nomacro
	bne	$10,$18,$L392
	lw	$11,148($sp)
	.set	macro
	.set	reorder

$L553:
	addiu	$19,$19,1
	addiu	$21,$21,1
	.set	noreorder
	.set	nomacro
	bne	$19,$fp,$L393
	move	$18,$0
	.set	macro
	.set	reorder

$L394:
	lw	$10,120($sp)
$L591:
	addiu	$23,$23,4
	lw	$24,104($sp)
	addiu	$10,$10,1
	slt	$2,$10,$24
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L362
	sw	$10,120($sp)
	.set	macro
	.set	reorder

	lw	$2,5660($17)
	bne	$2,$0,$L360
$L536:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L395
	.option	pic2
	lw	$2,764($17)
	.set	macro
	.set	reorder

$L384:
	lw	$10,944($4)
	move	$4,$2
	lw	$15,-104($23)
	lw	$25,1584($17)
	sll	$10,$10,7
	sw	$2,136($sp)
	sw	$8,144($sp)
	addiu	$10,$10,32
	sw	$11,148($sp)
	sw	$15,140($sp)
	addu	$10,$17,$10
	.set	noreorder
	.set	nomacro
	jalr	$25
	sw	$10,152($sp)
	.set	macro
	.set	reorder

	lw	$5,12($17)
	lw	$6,28($17)
	lw	$11,148($sp)
	srl	$3,$5,3
	lw	$15,140($sp)
	andi	$9,$5,0x7
	lw	$28,40($sp)
	addu	$3,$6,$3
	lw	$2,136($sp)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $24, 3($3)  
	lwr $24, 0($3)  
	
 # 0 "" 2
#NO_APP
	srl	$4,$24,8
	lw	$8,144($sp)
	sll	$3,$24,8
	lw	$10,152($sp)
	and	$4,$4,$11
	and	$3,$3,$16
	or	$3,$4,$3
	addiu	$14,$15,34
	sll	$4,$3,16
	srl	$3,$3,16
	sll	$7,$14,4
	or	$3,$4,$3
	sll	$9,$3,$9
	addu	$7,$17,$7
	srl	$4,$9,23
	lw	$7,4($7)
	sll	$4,$4,2
	addu	$4,$7,$4
	lh	$3,2($4)
	.set	noreorder
	.set	nomacro
	bltz	$3,$L559
	lh	$4,0($4)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$4,$L560
	addu	$5,$3,$5
	.set	macro
	.set	reorder

$L387:
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L561
	sll	$9,$9,$3
	.set	macro
	.set	reorder

	move	$3,$0
	sw	$5,12($17)
$L390:
	lh	$4,0($10)
	lw	$9,96($sp)
	lw	$5,0($23)
	mul	$3,$4,$3
	sll	$3,$3,$9
	addu	$3,$3,$5
	sw	$3,0($23)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L383
	.option	pic2
	sh	$3,0($2)
	.set	macro
	.set	reorder

$L557:
	li	$2,63			# 0x3f
	.set	noreorder
	.set	nomacro
	bne	$12,$2,$L382
	lui	$6,%hi($LC28)
	.set	macro
	.set	reorder

	lbu	$2,1475($17)
#APP
 # 509 "mjpegdec.c" 1
	S32I2M xr10,$2
 # 0 "" 2
#NO_APP
	sll	$2,$2,1
	addu	$14,$14,$2
	lhu	$3,0($14)
#APP
 # 510 "mjpegdec.c" 1
	S32I2M xr12,$3
 # 0 "" 2
 # 511 "mjpegdec.c" 1
	D16MUL xr0,xr4,xr12,xr13,WW
 # 0 "" 2
 # 512 "mjpegdec.c" 1
	S32MAX xr11,xr11,xr10
 # 0 "" 2
 # 513 "mjpegdec.c" 1
	S32M2I xr13, $3
 # 0 "" 2
#NO_APP
	addu	$2,$7,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L376
	.option	pic2
	sh	$3,0($2)
	.set	macro
	.set	reorder

$L555:
	subu	$8,$0,$12
	nor	$2,$0,$10
	sra	$3,$2,31
	addu	$9,$9,$12
	xor	$2,$3,$10
	srl	$2,$2,$8
	xor	$2,$2,$3
	subu	$2,$2,$3
	li	$3,65535			# 0xffff
	.set	noreorder
	.set	nomacro
	bne	$2,$3,$L371
	sw	$9,12($17)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L369
	.option	pic2
	move	$12,$17
	.set	macro
	.set	reorder

$L366:
	addiu	$3,$3,9
	srl	$8,$3,3
	andi	$15,$3,0x7
	addu	$8,$12,$8
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $10, 3($8)  
	lwr $10, 0($8)  
	
 # 0 "" 2
#NO_APP
	move	$8,$10
	sll	$8,$8,8
	srl	$10,$10,8
	and	$8,$8,$16
	and	$10,$10,$11
	or	$8,$10,$8
	sll	$10,$8,16
	srl	$8,$8,16
	or	$8,$10,$8
	sll	$10,$8,$15
	srl	$6,$10,$6
	addu	$6,$6,$9
	sll	$6,$6,2
	addu	$13,$13,$6
	lh	$6,2($13)
	lh	$12,0($13)
	.set	noreorder
	.set	nomacro
	bgez	$12,$L368
	addu	$9,$6,$3
	.set	macro
	.set	reorder

$L554:
	move	$15,$24
	lw	$4,0($17)
	sll	$2,$15,4
	sw	$17,156($sp)
	lui	$6,%hi($LC27)
	addu	$2,$17,$2
	move	$24,$25
	lw	$25,%call16(av_log)($28)
	li	$5,24			# 0x18
	addiu	$6,$6,%lo($LC27)
	sw	$24,16($sp)
	move	$7,$0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,20($sp)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	lw	$12,156($sp)
$L369:
	lui	$6,%hi($LC30)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($12)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC30)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$12,156($sp)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	lw	$12,156($sp)
$L372:
	lw	$2,108($sp)
	lui	$6,%hi($LC31)
	lw	$4,0($12)
	li	$5,16			# 0x10
	lw	$25,%call16(av_log)($28)
	addiu	$6,$6,%lo($LC31)
	lw	$7,112($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,16($sp)
	.set	macro
	.set	reorder

	li	$3,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$3,80($sp)
	.set	macro
	.set	reorder

$L561:
	subu	$3,$0,$4
	nor	$6,$0,$9
	sra	$6,$6,31
	addu	$4,$5,$4
	xor	$9,$6,$9
	srl	$3,$9,$3
	sw	$4,12($17)
	li	$4,65535			# 0xffff
	xor	$3,$3,$6
	subu	$3,$3,$6
	.set	noreorder
	.set	nomacro
	bne	$3,$4,$L390
	move	$12,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L604
	.option	pic2
	lui	$6,%hi($LC30)
	.set	macro
	.set	reorder

$L559:
	addiu	$5,$5,9
	srl	$9,$5,3
	andi	$12,$5,0x7
	addu	$6,$6,$9
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $9, 3($6)  
	lwr $9, 0($6)  
	
 # 0 "" 2
#NO_APP
	move	$6,$9
	sll	$6,$6,8
	srl	$9,$9,8
	and	$6,$6,$16
	and	$9,$9,$11
	or	$6,$9,$6
	sll	$9,$6,16
	srl	$6,$6,16
	or	$6,$9,$6
	sll	$9,$6,$12
	srl	$3,$9,$3
	addu	$3,$3,$4
	sll	$3,$3,2
	addu	$7,$7,$3
	lh	$3,2($7)
	lh	$4,0($7)
	.set	noreorder
	.set	nomacro
	bgez	$4,$L387
	addu	$5,$3,$5
	.set	macro
	.set	reorder

$L560:
	move	$13,$14
	lw	$4,0($17)
	sll	$2,$13,4
	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC27)
	sw	$15,16($sp)
	addu	$2,$17,$2
	sw	$17,156($sp)
	li	$5,24			# 0x18
	addiu	$6,$6,%lo($LC27)
	move	$7,$0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,20($sp)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	lw	$12,156($sp)
	lui	$6,%hi($LC30)
$L604:
	lw	$4,0($12)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC30)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$12,156($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC31)
	lw	$28,40($sp)
	li	$5,16			# 0x10
	lw	$12,156($sp)
	addiu	$6,$6,%lo($LC31)
	lw	$10,108($sp)
	lw	$7,112($sp)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($12)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$10,16($sp)
	.set	macro
	.set	reorder

	li	$3,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$3,80($sp)
	.set	macro
	.set	reorder

$L552:
	.set	noreorder
	.set	nomacro
	bgtz	$8,$L410
	sw	$3,5664($17)
	.set	macro
	.set	reorder

$L360:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L411
	.option	pic2
	lw	$2,5664($17)
	.set	macro
	.set	reorder

$L550:
	lw	$4,840($12)
	sll	$fp,$4,2
	addu	$fp,$12,$fp
	lw	$20,944($fp)
	lw	$9,976($fp)
	lw	$10,1196($fp)
	sll	$20,$20,7
	addiu	$20,$20,32
	sw	$9,84($sp)
	sw	$10,104($sp)
	.set	noreorder
	.set	nomacro
	bne	$24,$0,$L433
	addu	$20,$12,$20
	.set	macro
	.set	reorder

	addiu	$3,$19,1
	li	$2,1			# 0x1
	andi	$7,$3,0x20
	sll	$3,$2,$3
	sll	$2,$2,$8
	andi	$6,$8,0x20
	move	$5,$3
	movn	$3,$0,$7
	sll	$4,$4,3
	movz	$5,$0,$7
	move	$7,$2
	addu	$4,$12,$4
	movn	$2,$0,$6
	movz	$7,$0,$6
	subu	$2,$3,$2
	lw	$6,1380($4)
	subu	$5,$5,$7
	lw	$7,1376($4)
	sltu	$3,$3,$2
	subu	$3,$5,$3
	or	$2,$7,$2
	or	$3,$6,$3
	and	$5,$2,$3
	sw	$2,1376($4)
	addiu	$2,$5,1
	sw	$3,1380($4)
	sltu	$2,$2,1
	sw	$2,112($sp)
$L306:
	lw	$2,696($12)
	beq	$2,$0,$L307
	lw	$2,700($12)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L307
	lw	$3,104($sp)
	.set	macro
	.set	reorder

	lw	$7,84($sp)
	sra	$2,$3,1
	addu	$7,$7,$2
	sw	$7,84($sp)
$L307:
	lw	$4,768($12)
	.set	noreorder
	.set	nomacro
	blez	$4,$L303
	addu	$9,$12,$19
	.set	macro
	.set	reorder

	lw	$8,104($sp)
	move	$23,$0
	lw	$22,96($sp)
	li	$18,16711680			# 0xff0000
	lw	$3,764($12)
	sll	$8,$8,3
	sw	$0,108($sp)
	li	$17,-16777216			# 0xffffffffff000000
	sw	$9,120($sp)
	addiu	$18,$18,255
	sw	$23,80($sp)
	move	$21,$12
	sw	$8,116($sp)
	ori	$17,$17,0xff00
	move	$12,$fp
$L352:
	lw	$2,776($12)
	lw	$5,0($21)
	lw	$7,80($sp)
	lw	$8,108($sp)
	lw	$9,84($sp)
	mul	$2,$7,$2
	lw	$5,656($5)
	lw	$16,1344($12)
	lw	$fp,1360($12)
	sra	$5,$8,$5
	addu	$5,$9,$5
	sll	$6,$2,7
	sw	$5,96($sp)
	addu	$fp,$fp,$2
	.set	noreorder
	.set	nomacro
	blez	$3,$L308
	addu	$16,$16,$6
	.set	macro
	.set	reorder

	sw	$0,88($sp)
$L351:
	lw	$10,100($sp)
	beq	$10,$0,$L309
	lbu	$3,0($fp)
	lw	$2,872($21)
	lw	$10,12($21)
	slt	$14,$19,$3
	movn	$3,$19,$14
	.set	noreorder
	.set	nomacro
	beq	$23,$0,$L310
	move	$14,$3
	.set	macro
	.set	reorder

	lw	$7,92($sp)
	addiu	$23,$23,-1
$L311:
	slt	$2,$14,$7
	bne	$2,$0,$L335
	addiu	$6,$7,1412
	addiu	$5,$14,1413
	addu	$6,$21,$6
	addu	$5,$21,$5
$L337:
	lbu	$3,0($6)
	sll	$3,$3,1
	addu	$8,$16,$3
	lh	$9,0($8)
	.set	noreorder
	.set	nomacro
	beq	$9,$0,$L336
	srl	$4,$10,3
	.set	macro
	.set	reorder

	lw	$2,4($21)
	addu	$3,$20,$3
	sra	$13,$9,31
	addu	$2,$2,$4
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($2)  
	lwr $4, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$4,8
	lhu	$11,0($3)
	sll	$7,$4,8
	and	$2,$2,$18
	and	$7,$7,$17
	or	$7,$2,$7
	sll	$4,$7,16
	xor	$11,$13,$11
	srl	$7,$7,16
	sll	$11,$11,16
	andi	$14,$10,0x7
	or	$3,$4,$7
	sra	$11,$11,16
	sll	$3,$3,$14
	subu	$2,$11,$13
	srl	$3,$3,31
	addiu	$10,$10,1
	mul	$2,$3,$2
	sll	$2,$2,$22
	addu	$2,$2,$9
	sh	$2,0($8)
$L336:
	addiu	$6,$6,1
	bne	$6,$5,$L337
$L335:
	sw	$10,12($21)
$L325:
	lw	$9,112($sp)
$L602:
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L562
	lw	$4,96($sp)
	.set	macro
	.set	reorder

$L407:
	lw	$8,88($sp)
	addiu	$16,$16,128
	lw	$3,764($21)
	addiu	$fp,$fp,1
	addiu	$8,$8,1
	slt	$2,$8,$3
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L351
	sw	$8,88($sp)
	.set	macro
	.set	reorder

	lw	$4,768($21)
$L308:
	lw	$9,80($sp)
	lw	$10,108($sp)
	lw	$24,116($sp)
	addiu	$9,$9,1
	addu	$10,$10,$24
	slt	$2,$9,$4
	sw	$9,80($sp)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L352
	sw	$10,108($sp)
	.set	macro
	.set	reorder

$L303:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$0,80($sp)
	.set	macro
	.set	reorder

$L549:
	lw	$2,768($12)
	.set	noreorder
	.set	nomacro
	blez	$2,$L303
	lw	$9,96($sp)
	.set	macro
	.set	reorder

	li	$5,65535			# 0xffff
	lw	$8,92($sp)
	move	$18,$12
	lw	$3,764($12)
	sll	$5,$5,$9
	sw	$0,88($sp)
	sll	$4,$8,2
	andi	$5,$5,0x00ff
	sw	$5,100($sp)
	li	$5,128			# 0x80
	sll	$5,$5,$9
	sw	$5,108($sp)
	lui	$5,%hi($L284)
	addiu	$5,$5,%lo($L284)
	addu	$5,$5,$4
	sw	$5,120($sp)
	lui	$5,%hi($L263)
	addiu	$5,$5,%lo($L263)
	addu	$5,$5,$4
	sw	$5,116($sp)
$L214:
	.set	noreorder
	.set	nomacro
	blez	$3,$L304
	addiu	$24,$18,888
	.set	macro
	.set	reorder

	lw	$2,5660($18)
	addiu	$3,$18,900
	sw	$0,80($sp)
	sw	$24,112($sp)
	sw	$3,104($sp)
$L302:
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L593
	lw	$24,80($sp)
	.set	macro
	.set	reorder

	lw	$3,5664($18)
	bne	$3,$0,$L593
	sw	$2,5664($18)
	lw	$24,80($sp)
$L593:
	.set	noreorder
	.set	nomacro
	beq	$24,$0,$L594
	lw	$23,112($sp)
	.set	macro
	.set	reorder

	lw	$2,88($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L252
	lw	$13,116($sp)
	.set	macro
	.set	reorder

	lw	$2,696($18)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L252
	lw	$11,112($sp)
	.set	macro
	.set	reorder

	lw	$13,120($sp)
	move	$22,$11
$L281:
	lw	$fp,-48($22)
	lw	$10,0($22)
	lw	$11,16($22)
	sll	$fp,$fp,2
	lw	$9,32($22)
	addu	$fp,$18,$fp
	.set	noreorder
	.set	nomacro
	blez	$10,$L298
	lw	$21,1196($fp)
	.set	macro
	.set	reorder

	lw	$2,80($sp)
	move	$20,$0
	lw	$3,92($sp)
	move	$19,$0
	move	$23,$0
	mul	$12,$11,$2
	lw	$2,88($sp)
	sltu	$3,$3,7
	mul	$9,$2,$9
	sw	$3,84($sp)
	move	$2,$fp
	move	$fp,$10
	move	$10,$2
	addu	$3,$23,$9
$L595:
	lw	$16,976($10)
	addu	$2,$19,$12
	lw	$8,84($sp)
	mul	$4,$21,$3
	addu	$2,$4,$2
	.set	noreorder
	.set	nomacro
	beq	$8,$0,$L282
	addu	$16,$16,$2
	.set	macro
	.set	reorder

	lw	$2,0($13)
	j	$2
	.rdata
	.align	2
	.align	2
$L284:
	.word	$L282
	.word	$L283
	.word	$L285
	.word	$L286
	.word	$L287
	.word	$L288
	.word	$L289
	.text
$L289:
	nor	$3,$0,$21
	lbu	$2,-1($16)
	addu	$3,$16,$3
	subu	$4,$16,$21
	lbu	$5,0($3)
	lbu	$17,0($4)
	subu	$2,$2,$5
	sra	$2,$2,1
	addu	$17,$17,$2
$L290:
	lw	$2,12($18)
	li	$6,16711680			# 0xff0000
	lw	$7,28($18)
	li	$5,-16777216			# 0xffffffffff000000
	lw	$15,-32($22)
	addiu	$6,$6,255
	srl	$3,$2,3
	ori	$5,$5,0xff00
	addu	$3,$7,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($3)  
	lwr $4, 0($3)  
	
 # 0 "" 2
#NO_APP
	sll	$8,$4,8
	srl	$3,$4,8
	and	$4,$3,$6
	and	$3,$8,$5
	or	$3,$4,$3
	addiu	$8,$15,34
	sll	$25,$3,16
	srl	$3,$3,16
	sll	$14,$8,4
	or	$3,$25,$3
	andi	$25,$2,0x7
	sll	$25,$3,$25
	addu	$14,$18,$14
	srl	$3,$25,23
	lw	$14,4($14)
	sll	$3,$3,2
	addu	$3,$14,$3
	lh	$4,2($3)
	.set	noreorder
	.set	nomacro
	bltz	$4,$L563
	lh	$3,0($3)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$3,$L564
	addu	$2,$4,$2
	.set	macro
	.set	reorder

$L293:
	bne	$3,$0,$L565
	move	$3,$0
	sw	$2,12($18)
$L294:
	addu	$17,$3,$17
	addiu	$19,$19,1
	.set	noreorder
	.set	nomacro
	beq	$11,$19,$L566
	sb	$17,0($16)
	.set	macro
	.set	reorder

$L296:
	addiu	$20,$20,1
	.set	noreorder
	.set	nomacro
	bne	$20,$fp,$L595
	addu	$3,$23,$9
	.set	macro
	.set	reorder

$L298:
	lw	$9,104($sp)
	addiu	$22,$22,4
	bne	$9,$22,$L281
	lw	$2,5660($18)
$L581:
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L596
	lw	$9,80($sp)
	.set	macro
	.set	reorder

	lw	$3,5664($18)
	addiu	$3,$3,-1
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L299
	sw	$3,5664($18)
	.set	macro
	.set	reorder

	lw	$3,12($18)
	subu	$4,$0,$3
	andi	$4,$4,0x7
	beq	$4,$0,$L301
	addu	$3,$4,$3
	sw	$3,12($18)
$L301:
	addiu	$3,$3,16
	sw	$3,12($18)
$L299:
	lw	$9,80($sp)
$L596:
	lw	$3,764($18)
	addiu	$9,$9,1
	slt	$4,$9,$3
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L302
	sw	$9,80($sp)
	.set	macro
	.set	reorder

	lw	$2,768($18)
$L304:
	lw	$10,88($sp)
	addiu	$10,$10,1
	slt	$4,$10,$2
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L214
	sw	$10,88($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$0,80($sp)
	.set	macro
	.set	reorder

$L288:
	nor	$3,$0,$21
	lbu	$17,-1($16)
	subu	$2,$16,$21
	addu	$3,$16,$3
	lbu	$2,0($2)
	lbu	$3,0($3)
	subu	$2,$2,$3
	sra	$2,$2,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L290
	.option	pic2
	addu	$17,$17,$2
	.set	macro
	.set	reorder

$L287:
	subu	$4,$16,$21
	lbu	$2,-1($16)
	nor	$3,$0,$21
	addu	$3,$16,$3
	lbu	$17,0($4)
	lbu	$3,0($3)
	addu	$2,$2,$17
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L290
	.option	pic2
	subu	$17,$2,$3
	.set	macro
	.set	reorder

$L283:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L290
	.option	pic2
	lbu	$17,-1($16)
	.set	macro
	.set	reorder

$L286:
	nor	$2,$0,$21
	addu	$2,$16,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L290
	.option	pic2
	lbu	$17,0($2)
	.set	macro
	.set	reorder

$L285:
	subu	$2,$16,$21
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L290
	.option	pic2
	lbu	$17,0($2)
	.set	macro
	.set	reorder

$L282:
	subu	$3,$16,$21
	lbu	$2,-1($16)
	lbu	$17,0($3)
	addu	$17,$2,$17
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L290
	.option	pic2
	sra	$17,$17,1
	.set	macro
	.set	reorder

$L227:
	subu	$2,$8,$3
	lw	$8,0($18)
	sra	$2,$2,1
	addu	$8,$2,$8
$L224:
	lw	$5,12($20)
	li	$11,16711680			# 0xff0000
	lw	$13,28($20)
	li	$7,-16777216			# 0xffffffffff000000
	lw	$6,0($fp)
	addiu	$11,$11,255
	srl	$2,$5,3
	ori	$7,$7,0xff00
	addu	$2,$13,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($2)  
	lwr $3, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sll	$3,$3,8
	and	$2,$2,$11
	and	$3,$3,$7
	or	$3,$2,$3
	addiu	$9,$6,34
	sll	$2,$3,16
	srl	$3,$3,16
	sll	$4,$9,4
	or	$3,$2,$3
	andi	$2,$5,0x7
	sll	$2,$3,$2
	addu	$3,$20,$4
	srl	$4,$2,23
	lw	$14,4($3)
	sll	$4,$4,2
	addu	$4,$14,$4
	lh	$3,2($4)
	.set	noreorder
	.set	nomacro
	bltz	$3,$L229
	lh	$15,0($4)
	.set	macro
	.set	reorder

	move	$4,$15
	.set	noreorder
	.set	nomacro
	bltz	$4,$L567
	addu	$5,$3,$5
	.set	macro
	.set	reorder

$L231:
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L568
	sll	$3,$2,$3
	.set	macro
	.set	reorder

	move	$2,$0
	sw	$5,12($20)
$L232:
	addu	$2,$2,$8
	addiu	$18,$18,4
	and	$2,$10,$2
	andi	$2,$2,0xffff
	addiu	$fp,$fp,4
	sh	$2,0($23)
	addiu	$17,$17,4
	sw	$2,-4($18)
	.set	noreorder
	.set	nomacro
	bne	$19,$fp,$L234
	addiu	$23,$23,2
	.set	macro
	.set	reorder

	lw	$3,5660($20)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L236
	move	$9,$16
	.set	macro
	.set	reorder

	lw	$2,5664($20)
	addiu	$2,$2,-1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L236
	sw	$2,5664($20)
	.set	macro
	.set	reorder

	lw	$5,12($20)
	subu	$4,$0,$5
	andi	$2,$4,0x7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L238
	addu	$2,$2,$5
	.set	macro
	.set	reorder

	move	$2,$5
$L239:
	addiu	$2,$2,16
	sw	$2,12($20)
$L236:
	lw	$2,764($20)
	addiu	$22,$22,1
	slt	$4,$22,$2
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L240
	addiu	$21,$21,8
	.set	macro
	.set	reorder

	lw	$3,720($20)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L241
	move	$17,$10
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$2,$L242
	lw	$10,112($sp)
	.set	macro
	.set	reorder

	move	$7,$0
	lw	$5,128($sp)
	addiu	$6,$10,1
$L243:
	lhu	$3,0($5)
	addiu	$6,$6,4
	lhu	$4,2($5)
	addiu	$5,$5,8
	lbu	$2,-10($5)
	addiu	$7,$7,1
	addu	$3,$3,$4
	addiu	$3,$3,-512
	sra	$3,$3,2
	subu	$2,$2,$3
	andi	$2,$2,0x00ff
	sb	$2,-4($6)
	lbu	$3,-8($5)
	addu	$3,$2,$3
	sb	$3,-5($6)
	lbu	$3,-6($5)
	addu	$2,$2,$3
	sb	$2,-3($6)
	lw	$2,764($20)
	slt	$3,$7,$2
	bne	$3,$0,$L243
$L242:
	lw	$8,116($sp)
	lw	$9,120($sp)
$L605:
	lw	$10,124($sp)
	lw	$3,768($20)
	addiu	$8,$8,1
	addu	$9,$9,$10
	slt	$3,$8,$3
	sw	$8,116($sp)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L458
	sw	$9,120($sp)
	.set	macro
	.set	reorder

	lw	$3,92($sp)
	li	$9,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L216
	.option	pic2
	movn	$9,$3,$8
	.set	macro
	.set	reorder

$L222:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	lw	$8,0($18)
	.set	macro
	.set	reorder

$L228:
	lw	$2,0($18)
	subu	$2,$2,$3
	sra	$2,$2,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	addu	$8,$8,$2
	.set	macro
	.set	reorder

$L226:
	lw	$2,0($18)
	addu	$2,$8,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	subu	$8,$2,$3
	.set	macro
	.set	reorder

$L225:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	move	$8,$3
	.set	macro
	.set	reorder

$L221:
	lw	$2,0($18)
	addu	$2,$8,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	sra	$8,$2,1
	.set	macro
	.set	reorder

$L546:
	lui	$2,%hi($LC19)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L205
	.option	pic2
	addiu	$2,$2,%lo($LC19)
	.set	macro
	.set	reorder

$L565:
	sll	$25,$25,$4
	lw	$8,96($sp)
	subu	$6,$0,$3
	nor	$5,$0,$25
	sra	$5,$5,31
	addu	$4,$2,$3
	xor	$25,$5,$25
	srl	$2,$25,$6
	addiu	$19,$19,1
	sw	$4,12($18)
	xor	$2,$2,$5
	subu	$3,$2,$5
	sll	$3,$3,$8
	andi	$3,$3,0x00ff
	addu	$17,$3,$17
	.set	noreorder
	.set	nomacro
	bne	$11,$19,$L296
	sb	$17,0($16)
	.set	macro
	.set	reorder

$L566:
	addiu	$23,$23,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L296
	.option	pic2
	move	$19,$0
	.set	macro
	.set	reorder

$L563:
	addiu	$2,$2,9
	srl	$24,$2,3
	andi	$25,$2,0x7
	addu	$7,$7,$24
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $24, 3($7)  
	lwr $24, 0($7)  
	
 # 0 "" 2
#NO_APP
	move	$7,$24
	sll	$7,$7,8
	srl	$24,$24,8
	and	$7,$7,$5
	and	$6,$24,$6
	or	$6,$6,$7
	sll	$5,$6,16
	srl	$6,$6,16
	or	$6,$5,$6
	sll	$25,$6,$25
	srl	$4,$25,$4
	addu	$3,$4,$3
	sll	$3,$3,2
	addu	$14,$14,$3
	lh	$4,2($14)
	lh	$3,0($14)
	.set	noreorder
	.set	nomacro
	bgez	$3,$L293
	addu	$2,$4,$2
	.set	macro
	.set	reorder

$L564:
	sll	$8,$8,4
	lw	$4,0($18)
	lui	$2,%hi($LC27)
	lw	$25,%call16(av_log)($28)
	addu	$8,$18,$8
	sw	$9,136($sp)
	li	$5,24			# 0x18
	sw	$10,152($sp)
	addiu	$6,$2,%lo($LC27)
	sw	$11,148($sp)
	move	$7,$0
	sw	$12,156($sp)
	sw	$13,140($sp)
	sw	$15,16($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$8,20($sp)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	lw	$3,100($sp)
	lw	$9,136($sp)
	lw	$10,152($sp)
	lw	$11,148($sp)
	lw	$12,156($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L294
	.option	pic2
	lw	$13,140($sp)
	.set	macro
	.set	reorder

$L594:
	lw	$13,116($sp)
$L252:
	lw	$fp,-48($23)
	lw	$10,0($23)
	lw	$11,16($23)
	sll	$fp,$fp,2
	lw	$9,32($23)
	addu	$fp,$18,$fp
	.set	noreorder
	.set	nomacro
	blez	$10,$L277
	lw	$22,1196($fp)
	.set	macro
	.set	reorder

	lw	$2,80($sp)
	sra	$3,$22,1
	move	$20,$0
	move	$19,$0
	mul	$12,$11,$2
	lw	$2,88($sp)
	sw	$3,84($sp)
	move	$21,$0
	mul	$9,$2,$9
	move	$2,$fp
	move	$fp,$10
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L276
	.option	pic2
	move	$10,$2
	.set	macro
	.set	reorder

$L573:
	lw	$8,88($sp)
	bne	$8,$0,$L257
	.set	noreorder
	.set	nomacro
	bne	$19,$0,$L262
	lw	$24,80($sp)
	.set	macro
	.set	reorder

	bne	$24,$0,$L262
	lw	$17,108($sp)
$L259:
	lw	$2,696($18)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L269
	lw	$8,84($sp)
	.set	macro
	.set	reorder

	lw	$3,700($18)
	addu	$2,$16,$8
	movn	$16,$2,$3
$L269:
	lw	$2,12($18)
	li	$6,16711680			# 0xff0000
	lw	$7,28($18)
	li	$5,-16777216			# 0xffffffffff000000
	lw	$15,-32($23)
	addiu	$6,$6,255
	srl	$3,$2,3
	ori	$5,$5,0xff00
	addu	$3,$7,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $24, 3($3)  
	lwr $24, 0($3)  
	
 # 0 "" 2
#NO_APP
	srl	$25,$24,8
	sll	$3,$24,8
	and	$25,$25,$6
	and	$3,$3,$5
	or	$25,$25,$3
	addiu	$8,$15,34
	sll	$3,$25,16
	srl	$25,$25,16
	sll	$4,$8,4
	or	$25,$3,$25
	andi	$3,$2,0x7
	sll	$25,$25,$3
	addu	$3,$18,$4
	srl	$4,$25,23
	lw	$14,4($3)
	sll	$4,$4,2
	addu	$4,$14,$4
	lh	$3,2($4)
	.set	noreorder
	.set	nomacro
	bltz	$3,$L569
	lh	$4,0($4)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$4,$L570
	addu	$2,$3,$2
	.set	macro
	.set	reorder

$L272:
	bne	$4,$0,$L571
	move	$3,$0
	sw	$2,12($18)
$L273:
	addu	$17,$3,$17
	addiu	$19,$19,1
	.set	noreorder
	.set	nomacro
	beq	$11,$19,$L572
	sb	$17,0($16)
	.set	macro
	.set	reorder

$L275:
	addiu	$20,$20,1
	.set	noreorder
	.set	nomacro
	beq	$20,$fp,$L597
	lw	$3,104($sp)
	.set	macro
	.set	reorder

$L276:
	addu	$3,$21,$9
	lw	$16,976($10)
	addu	$2,$19,$12
	mul	$4,$22,$3
	addu	$2,$4,$2
	.set	noreorder
	.set	nomacro
	beq	$21,$0,$L573
	addu	$16,$16,$2
	.set	macro
	.set	reorder

$L257:
	.set	noreorder
	.set	nomacro
	bne	$19,$0,$L598
	lw	$3,92($sp)
	.set	macro
	.set	reorder

	lw	$2,80($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L599
	subu	$2,$16,$22
	.set	macro
	.set	reorder

	lw	$3,92($sp)
$L598:
	sltu	$2,$3,7
	beq	$2,$0,$L261
	lw	$2,0($13)
	j	$2
	.rdata
	.align	2
	.align	2
$L263:
	.word	$L261
	.word	$L262
	.word	$L264
	.word	$L265
	.word	$L266
	.word	$L267
	.word	$L268
	.text
$L264:
	subu	$2,$16,$22
$L599:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	lbu	$17,0($2)
	.set	macro
	.set	reorder

$L262:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	lbu	$17,-1($16)
	.set	macro
	.set	reorder

$L268:
	nor	$3,$0,$22
	lbu	$2,-1($16)
	addu	$3,$16,$3
	subu	$4,$16,$22
	lbu	$5,0($3)
	lbu	$17,0($4)
	subu	$2,$2,$5
	sra	$2,$2,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	addu	$17,$17,$2
	.set	macro
	.set	reorder

$L267:
	nor	$3,$0,$22
	lbu	$17,-1($16)
	subu	$2,$16,$22
	addu	$3,$16,$3
	lbu	$2,0($2)
	lbu	$3,0($3)
	subu	$2,$2,$3
	sra	$2,$2,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	addu	$17,$17,$2
	.set	macro
	.set	reorder

$L266:
	subu	$4,$16,$22
	lbu	$2,-1($16)
	nor	$3,$0,$22
	addu	$3,$16,$3
	lbu	$17,0($4)
	lbu	$3,0($3)
	addu	$2,$2,$17
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	subu	$17,$2,$3
	.set	macro
	.set	reorder

$L265:
	nor	$2,$0,$22
	addu	$2,$16,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	lbu	$17,0($2)
	.set	macro
	.set	reorder

$L261:
	subu	$3,$16,$22
	lbu	$2,-1($16)
	lbu	$17,0($3)
	addu	$17,$2,$17
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L259
	.option	pic2
	sra	$17,$17,1
	.set	macro
	.set	reorder

$L571:
	sll	$25,$25,$3
	subu	$8,$0,$4
	nor	$3,$0,$25
	sra	$7,$3,31
	addu	$4,$2,$4
	xor	$25,$7,$25
	srl	$3,$25,$8
	lw	$8,96($sp)
	addiu	$19,$19,1
	sw	$4,12($18)
	xor	$25,$3,$7
	subu	$3,$25,$7
	sll	$3,$3,$8
	andi	$3,$3,0x00ff
	addu	$17,$3,$17
	.set	noreorder
	.set	nomacro
	bne	$11,$19,$L275
	sb	$17,0($16)
	.set	macro
	.set	reorder

$L572:
	addiu	$20,$20,1
	addiu	$21,$21,1
	.set	noreorder
	.set	nomacro
	bne	$20,$fp,$L276
	move	$19,$0
	.set	macro
	.set	reorder

$L277:
	lw	$3,104($sp)
$L597:
	addiu	$23,$23,4
	bne	$3,$23,$L252
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L581
	.option	pic2
	lw	$2,5660($18)
	.set	macro
	.set	reorder

$L569:
	addiu	$2,$2,9
	srl	$24,$2,3
	andi	$25,$2,0x7
	addu	$7,$7,$24
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $24, 3($7)  
	lwr $24, 0($7)  
	
 # 0 "" 2
#NO_APP
	move	$7,$24
	sll	$7,$7,8
	srl	$24,$24,8
	and	$7,$7,$5
	and	$6,$24,$6
	or	$6,$6,$7
	sll	$5,$6,16
	srl	$6,$6,16
	or	$6,$5,$6
	sll	$25,$6,$25
	srl	$3,$25,$3
	addu	$3,$3,$4
	sll	$3,$3,2
	addu	$14,$14,$3
	lh	$3,2($14)
	lh	$4,0($14)
	.set	noreorder
	.set	nomacro
	bgez	$4,$L272
	addu	$2,$3,$2
	.set	macro
	.set	reorder

$L570:
	sll	$8,$8,4
	lw	$4,0($18)
	lui	$2,%hi($LC27)
	lw	$25,%call16(av_log)($28)
	addu	$8,$18,$8
	sw	$9,136($sp)
	li	$5,24			# 0x18
	sw	$10,152($sp)
	addiu	$6,$2,%lo($LC27)
	sw	$11,148($sp)
	move	$7,$0
	sw	$12,156($sp)
	sw	$13,140($sp)
	sw	$15,16($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$8,20($sp)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	lw	$3,100($sp)
	lw	$9,136($sp)
	lw	$10,152($sp)
	lw	$11,148($sp)
	lw	$12,156($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L273
	.option	pic2
	lw	$13,140($sp)
	.set	macro
	.set	reorder

$L568:
	lw	$9,96($sp)
	subu	$6,$0,$4
	nor	$2,$0,$3
	sra	$2,$2,31
	addu	$5,$5,$4
	xor	$4,$2,$3
	srl	$3,$4,$6
	sw	$5,12($20)
	xor	$3,$3,$2
	subu	$3,$3,$2
	sll	$3,$3,$9
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L232
	.option	pic2
	andi	$2,$3,0xffff
	.set	macro
	.set	reorder

$L229:
	addiu	$5,$5,9
	srl	$4,$5,3
	andi	$2,$5,0x7
	addu	$13,$13,$4
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($13)  
	lwr $4, 0($13)  
	
 # 0 "" 2
#NO_APP
	move	$13,$4
	sll	$13,$13,8
	srl	$4,$4,8
	and	$7,$13,$7
	and	$11,$4,$11
	or	$7,$11,$7
	sll	$4,$7,16
	srl	$7,$7,16
	or	$4,$4,$7
	sll	$2,$4,$2
	srl	$3,$2,$3
	addu	$3,$3,$15
	sll	$3,$3,2
	addu	$14,$14,$3
	lh	$3,2($14)
	lh	$4,0($14)
	.set	noreorder
	.set	nomacro
	bgez	$4,$L231
	addu	$5,$3,$5
	.set	macro
	.set	reorder

$L567:
	sll	$2,$9,4
	lw	$4,0($20)
	lw	$25,%call16(av_log)($28)
	li	$5,24			# 0x18
	addu	$2,$20,$2
	sw	$6,16($sp)
	lui	$6,%hi($LC27)
	sw	$8,144($sp)
	move	$7,$0
	sw	$10,152($sp)
	addiu	$6,$6,%lo($LC27)
	sw	$2,20($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$12,156($sp)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	lw	$2,108($sp)
	lw	$8,144($sp)
	lw	$10,152($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L232
	.option	pic2
	lw	$12,156($sp)
	.set	macro
	.set	reorder

$L238:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L239
	.option	pic2
	sw	$2,12($20)
	.set	macro
	.set	reorder

$L241:
	lw	$3,724($20)
	bne	$3,$0,$L244
	.set	noreorder
	.set	nomacro
	blez	$2,$L242
	lw	$3,112($sp)
	.set	macro
	.set	reorder

	lw	$2,104($sp)
	move	$5,$0
	addiu	$4,$2,4
$L247:
	lhu	$2,0($4)
	addiu	$3,$3,4
	addiu	$4,$4,8
	addiu	$5,$5,1
	sb	$2,-4($3)
	lhu	$2,-10($4)
	sb	$2,-3($3)
	lhu	$2,-12($4)
	sb	$2,-2($3)
	lw	$2,764($20)
	slt	$6,$5,$2
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L247
	lw	$8,116($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L605
	.option	pic2
	lw	$9,120($sp)
	.set	macro
	.set	reorder

$L309:
	.set	noreorder
	.set	nomacro
	bne	$23,$0,$L574
	lw	$2,872($21)
	.set	macro
	.set	reorder

	addiu	$2,$2,38
	lw	$5,12($21)
	lw	$8,4($21)
	li	$11,15			# 0xf
	sll	$2,$2,4
	lw	$6,92($sp)
	li	$10,32			# 0x20
	addu	$2,$21,$2
	lw	$9,4($2)
$L350:
	srl	$2,$5,3
	andi	$4,$5,0x7
	addu	$2,$8,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($2)  
	lwr $3, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sll	$3,$3,8
	and	$2,$2,$18
	and	$3,$3,$17
	or	$2,$2,$3
	sll	$3,$2,16
	srl	$2,$2,16
	or	$2,$3,$2
	sll	$3,$2,$4
	srl	$2,$3,23
	sll	$2,$2,2
	addu	$2,$9,$2
	lh	$7,2($2)
	.set	noreorder
	.set	nomacro
	bltz	$7,$L575
	lh	$2,0($2)
	.set	macro
	.set	reorder

$L340:
	addiu	$2,$2,-16
	andi	$4,$2,0xf
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L341
	addu	$5,$5,$7
	.set	macro
	.set	reorder

	srl	$2,$2,4
	slt	$13,$4,10
	.set	noreorder
	.set	nomacro
	beq	$13,$0,$L342
	addu	$6,$2,$6
	.set	macro
	.set	reorder

	sll	$3,$3,$7
$L343:
	nor	$2,$0,$3
	sra	$2,$2,31
	subu	$7,$10,$4
	xor	$3,$2,$3
	srl	$3,$3,$7
	slt	$7,$6,$19
	xor	$3,$3,$2
	subu	$3,$3,$2
	.set	noreorder
	.set	nomacro
	beq	$7,$0,$L576
	addu	$5,$5,$4
	.set	macro
	.set	reorder

	addu	$2,$21,$6
	addiu	$6,$6,1
	lbu	$2,1412($2)
	sll	$2,$2,1
	addu	$4,$20,$2
	addu	$2,$16,$2
	lh	$4,0($4)
	mul	$3,$3,$4
	sll	$3,$3,$22
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L350
	.option	pic2
	sh	$3,0($2)
	.set	macro
	.set	reorder

$L341:
	srl	$7,$2,4
	bne	$7,$11,$L348
	addiu	$6,$6,15
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L350
	.option	pic2
	addiu	$6,$6,1
	.set	macro
	.set	reorder

$L575:
	addiu	$5,$5,9
	srl	$4,$5,3
	andi	$3,$5,0x7
	addu	$4,$8,$4
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $13, 3($4)  
	lwr $13, 0($4)  
	
 # 0 "" 2
#NO_APP
	srl	$4,$13,8
	sll	$13,$13,8
	and	$4,$4,$18
	and	$13,$13,$17
	or	$4,$4,$13
	sll	$13,$4,16
	srl	$4,$4,16
	or	$4,$13,$4
	sll	$3,$4,$3
	srl	$4,$3,$7
	addu	$2,$4,$2
	sll	$4,$2,2
	addu	$4,$9,$4
	lh	$2,0($4)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L340
	.option	pic2
	lh	$7,2($4)
	.set	macro
	.set	reorder

$L342:
	srl	$2,$5,3
	andi	$3,$5,0x7
	addu	$2,$8,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $7, 3($2)  
	lwr $7, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$7,8
	sll	$7,$7,8
	and	$2,$2,$18
	and	$7,$7,$17
	or	$7,$2,$7
	sll	$2,$7,16
	srl	$7,$7,16
	or	$7,$2,$7
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L343
	.option	pic2
	sll	$3,$7,$3
	.set	macro
	.set	reorder

$L244:
	.set	noreorder
	.set	nomacro
	blez	$2,$L242
	lw	$24,112($sp)
	.set	macro
	.set	reorder

	move	$7,$0
	lw	$5,128($sp)
	addiu	$6,$24,1
$L246:
	lhu	$3,0($5)
	addiu	$6,$6,4
	lhu	$4,2($5)
	addiu	$5,$5,8
	lbu	$2,-10($5)
	addiu	$7,$7,1
	addu	$3,$3,$4
	sra	$3,$3,2
	subu	$2,$2,$3
	andi	$2,$2,0x00ff
	sb	$2,-4($6)
	lbu	$3,-8($5)
	addu	$3,$2,$3
	sb	$3,-5($6)
	lbu	$3,-6($5)
	addu	$2,$2,$3
	sb	$2,-3($6)
	lw	$2,764($20)
	slt	$3,$7,$2
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L246
	lw	$8,116($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L605
	.option	pic2
	lw	$9,120($sp)
	.set	macro
	.set	reorder

$L562:
	lw	$25,4236($21)
	move	$6,$16
	lw	$5,104($sp)
	.set	noreorder
	.set	nomacro
	jalr	$25
	sw	$12,156($sp)
	.set	macro
	.set	reorder

	lw	$2,0($21)
	lw	$28,40($sp)
	lw	$12,156($sp)
	lw	$3,656($2)
	li	$2,8			# 0x8
	sra	$2,$2,$3
	lw	$3,96($sp)
	addu	$3,$3,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L407
	.option	pic2
	sw	$3,96($sp)
	.set	macro
	.set	reorder

$L310:
	addiu	$2,$2,38
	lw	$15,4($21)
	lw	$7,92($sp)
	li	$25,15			# 0xf
	sll	$2,$2,4
	addu	$2,$21,$2
	lw	$24,4($2)
$L334:
	srl	$3,$10,3
	andi	$2,$10,0x7
	addu	$3,$15,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($3)  
	lwr $4, 0($3)  
	
 # 0 "" 2
#NO_APP
	srl	$3,$4,8
	sll	$4,$4,8
	and	$3,$3,$18
	and	$4,$4,$17
	or	$4,$3,$4
	sll	$3,$4,16
	srl	$4,$4,16
	or	$3,$3,$4
	sll	$2,$3,$2
	srl	$2,$2,23
	sll	$2,$2,2
	addu	$2,$24,$2
	lh	$3,2($2)
	.set	noreorder
	.set	nomacro
	bltz	$3,$L577
	lh	$6,0($2)
	.set	macro
	.set	reorder

$L313:
	addiu	$6,$6,-16
	andi	$2,$6,0xf
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L314
	addu	$10,$10,$3
	.set	macro
	.set	reorder

	srl	$2,$10,3
	andi	$4,$10,0x7
	addu	$2,$15,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($2)  
	lwr $3, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sll	$3,$3,8
	and	$2,$2,$18
	and	$3,$3,$17
	or	$2,$2,$3
	sll	$8,$2,16
	srl	$2,$2,16
	slt	$3,$14,$7
	or	$2,$8,$2
	sll	$8,$2,$4
	srl	$6,$6,4
	srl	$8,$8,31
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L524
	addiu	$10,$10,1
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L583
	.option	pic2
	addu	$7,$7,$6
	.set	macro
	.set	reorder

$L578:
	addu	$3,$20,$3
	sra	$13,$4,31
	lhu	$2,0($3)
	addu	$3,$15,$9
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $9, 3($3)  
	lwr $9, 0($3)  
	
 # 0 "" 2
#NO_APP
	move	$3,$9
	sll	$3,$3,8
	srl	$9,$9,8
	and	$3,$3,$17
	and	$9,$9,$18
	or	$3,$9,$3
	sll	$9,$3,16
	xor	$2,$13,$2
	srl	$3,$3,16
	sll	$2,$2,16
	andi	$11,$10,0x7
	or	$3,$9,$3
	sra	$2,$2,16
	sll	$3,$3,$11
	subu	$2,$2,$13
	srl	$3,$3,31
	addiu	$7,$7,1
	mul	$2,$3,$2
	sll	$2,$2,$22
	addu	$2,$2,$4
	sh	$2,0($5)
	slt	$2,$14,$7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L321
	addiu	$10,$10,1
	.set	macro
	.set	reorder

$L524:
	addu	$2,$21,$7
$L600:
	lbu	$3,1412($2)
	sll	$3,$3,1
	addu	$5,$16,$3
	lh	$4,0($5)
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L578
	srl	$9,$10,3
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L320
	addiu	$2,$6,-1
	.set	macro
	.set	reorder

	addiu	$7,$7,1
	move	$6,$2
	slt	$2,$14,$7
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L600
	addu	$2,$21,$7
	.set	macro
	.set	reorder

$L321:
	addu	$7,$7,$6
$L583:
	slt	$2,$19,$7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L537
	addu	$2,$21,$7
	.set	macro
	.set	reorder

	lbu	$3,1412($2)
	sll	$3,$3,1
	addu	$5,$16,$3
$L320:
	addu	$3,$20,$3
	addiu	$8,$8,-1
	lh	$2,0($3)
	xor	$2,$2,$8
	subu	$2,$2,$8
	sll	$2,$2,$22
	.set	noreorder
	.set	nomacro
	bne	$19,$7,$L323
	sh	$2,0($5)
	.set	macro
	.set	reorder

	lbu	$2,0($fp)
	slt	$2,$2,$19
	beq	$2,$0,$L335
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L335
	.option	pic2
	sb	$19,0($fp)
	.set	macro
	.set	reorder

$L314:
	srl	$6,$6,4
	.set	noreorder
	.set	nomacro
	bne	$6,$25,$L326
	slt	$2,$14,$7
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L525
	li	$8,15			# 0xf
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L584
	.option	pic2
	addu	$7,$7,$8
	.set	macro
	.set	reorder

$L579:
	addu	$2,$20,$2
	addu	$3,$15,$3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($3)  
	lwr $4, 0($3)  
	
 # 0 "" 2
#NO_APP
	srl	$3,$4,8
	lhu	$2,0($2)
	sll	$9,$4,8
	sra	$13,$6,31
	and	$3,$3,$18
	and	$9,$9,$17
	or	$9,$3,$9
	sll	$4,$9,16
	xor	$2,$13,$2
	srl	$9,$9,16
	sll	$2,$2,16
	andi	$11,$10,0x7
	or	$3,$4,$9
	sra	$2,$2,16
	sll	$3,$3,$11
	subu	$2,$2,$13
	srl	$3,$3,31
	addiu	$7,$7,1
	mul	$2,$3,$2
	sll	$2,$2,$22
	addu	$2,$2,$6
	sh	$2,0($5)
	slt	$2,$14,$7
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L330
	addiu	$10,$10,1
	.set	macro
	.set	reorder

$L525:
	addu	$2,$21,$7
$L601:
	lbu	$2,1412($2)
	sll	$2,$2,1
	addu	$5,$16,$2
	lh	$6,0($5)
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L579
	srl	$3,$10,3
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$8,$0,$L323
	addiu	$2,$8,-1
	.set	macro
	.set	reorder

	addiu	$7,$7,1
	move	$8,$2
	slt	$2,$14,$7
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L601
	addu	$2,$21,$7
	.set	macro
	.set	reorder

$L330:
	addu	$7,$7,$8
$L584:
	slt	$2,$19,$7
	bne	$2,$0,$L537
$L323:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L334
	.option	pic2
	addiu	$7,$7,1
	.set	macro
	.set	reorder

$L537:
	lw	$23,80($sp)
$L538:
	lui	$6,%hi($LC28)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($21)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$21,156($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC31)
	lw	$28,40($sp)
	li	$5,16			# 0x10
	lw	$12,156($sp)
	addiu	$6,$6,%lo($LC31)
	lw	$10,88($sp)
	move	$7,$23
	lw	$25,%call16(av_log)($28)
	lw	$4,0($12)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$10,16($sp)
	.set	macro
	.set	reorder

	li	$24,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$24,80($sp)
	.set	macro
	.set	reorder

$L577:
	addiu	$10,$10,9
	srl	$2,$10,3
	andi	$8,$10,0x7
	addu	$2,$15,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($2)  
	lwr $4, 0($2)  
	
 # 0 "" 2
#NO_APP
	move	$2,$4
	sll	$5,$2,8
	srl	$4,$4,8
	and	$5,$5,$17
	and	$4,$4,$18
	or	$5,$4,$5
	sll	$4,$5,16
	srl	$5,$5,16
	or	$2,$4,$5
	sll	$2,$2,$8
	srl	$2,$2,$3
	addu	$2,$2,$6
	sll	$2,$2,2
	addu	$2,$24,$2
	lh	$6,0($2)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L313
	.option	pic2
	lh	$3,2($2)
	.set	macro
	.set	reorder

$L574:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L325
	.option	pic2
	addiu	$23,$23,-1
	.set	macro
	.set	reorder

$L348:
	srl	$2,$5,3
	li	$23,1			# 0x1
	addu	$8,$8,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($8)  
	lwr $3, 0($8)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sll	$3,$3,8
	and	$2,$2,$18
	and	$3,$3,$17
	or	$2,$2,$3
	sll	$4,$2,16
	srl	$2,$2,16
	andi	$3,$5,0x7
	or	$2,$4,$2
	sll	$23,$23,$7
	sll	$3,$2,$3
	subu	$2,$0,$7
	addiu	$4,$23,-1
	srl	$2,$3,$2
	addu	$5,$5,$7
	and	$2,$2,$4
	addu	$23,$2,$23
	addiu	$23,$23,-1
$L346:
	sw	$5,12($21)
	lbu	$2,0($fp)
	slt	$2,$2,$6
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L602
	lw	$9,112($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L325
	.option	pic2
	sb	$6,0($fp)
	.set	macro
	.set	reorder

$L576:
	bne	$19,$6,$L345
	lw	$7,120($sp)
	move	$6,$19
	lbu	$2,1412($7)
	sll	$2,$2,1
	addu	$4,$20,$2
	addu	$2,$16,$2
	lh	$4,0($4)
	mul	$3,$4,$3
	sll	$3,$3,$22
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L346
	.option	pic2
	sh	$3,0($2)
	.set	macro
	.set	reorder

$L326:
	li	$23,1			# 0x1
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L332
	sll	$23,$23,$6
	.set	macro
	.set	reorder

	srl	$2,$10,3
	andi	$3,$10,0x7
	addu	$15,$15,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($15)  
	lwr $4, 0($15)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$4,8
	sll	$4,$4,8
	and	$2,$2,$18
	and	$4,$4,$17
	or	$2,$2,$4
	sll	$4,$2,16
	srl	$2,$2,16
	subu	$5,$0,$6
	or	$2,$4,$2
	sll	$2,$2,$3
	addu	$10,$10,$6
	srl	$2,$2,$5
	addu	$23,$2,$23
$L332:
	lbu	$2,0($fp)
	slt	$2,$2,$7
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L311
	addiu	$23,$23,-1
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L311
	.option	pic2
	sb	$7,0($fp)
	.set	macro
	.set	reorder

$L433:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L306
	.option	pic2
	sw	$0,112($sp)
	.set	macro
	.set	reorder

$L548:
	lw	$25,%call16(ff_jpegls_decode_picture)($28)
	move	$4,$12
	lw	$5,92($sp)
	move	$7,$19
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,ff_jpegls_decode_picture
1:	jalr	$25
	lw	$6,96($sp)
	.set	macro
	.set	reorder

	sra	$2,$2,31
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L458
	.option	pic2
	sw	$2,80($sp)
	.set	macro
	.set	reorder

$L382:
	lw	$25,%call16(av_log)($28)
	lw	$4,0($17)
	move	$31,$12
	li	$5,16			# 0x10
	sw	$17,156($sp)
	move	$7,$31
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC28)
	.set	macro
	.set	reorder

	lw	$28,40($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L372
	.option	pic2
	lw	$12,156($sp)
	.set	macro
	.set	reorder

$L415:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L355
	.option	pic2
	lw	$4,768($12)
	.set	macro
	.set	reorder

$L345:
	lw	$23,80($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L538
	.option	pic2
	move	$7,$6
	.set	macro
	.set	reorder

$L544:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	move	$7,$0
	.set	macro
	.set	reorder

	.end	ff_mjpeg_decode_sos
	.size	ff_mjpeg_decode_sos, .-ff_mjpeg_decode_sos
	.section	.rodata.str1.4
	.align	2
$LC33:
	.ascii	"marker=%x avail_size_in_buf=%td\012\000"
	.align	2
$LC34:
	.ascii	"buffer too small, expanding to %d bytes\012\000"
	.align	2
$LC35:
	.ascii	"escaping removed %td bytes\012\000"
	.align	2
$LC36:
	.ascii	"startcode: %X\012\000"
	.align	2
$LC37:
	.ascii	"restart marker: %d\012\000"
	.align	2
$LC38:
	.ascii	"APPx %8X\012\000"
	.align	2
$LC39:
	.ascii	"AVI1\000"
	.align	2
$LC40:
	.ascii	"JFIF\000"
	.align	2
$LC41:
	.ascii	"mjpeg: JFIF header found (version: %x.%x) SAR=%d/%d\012\000"
	.align	2
$LC42:
	.ascii	"Adob\000"
	.align	2
$LC43:
	.ascii	"mjpeg: Adobe header found\012\000"
	.align	2
$LC44:
	.ascii	"LJIF\000"
	.align	2
$LC45:
	.ascii	"Pegasus lossless jpeg header found\012\000"
	.align	2
$LC46:
	.ascii	"unknown colorspace\012\000"
	.align	2
$LC47:
	.ascii	"mjpg\000"
	.align	2
$LC48:
	.ascii	"mjpeg: Apple MJPEG-A header found\012\000"
	.align	2
$LC49:
	.ascii	"mjpeg: error, decode_app parser read over the end\012\000"
	.align	2
$LC50:
	.ascii	"mjpeg comment: '%s'\012\000"
	.align	2
$LC51:
	.ascii	"AVID\000"
	.align	2
$LC52:
	.ascii	"CS=ITU601\000"
	.align	2
$LC53:
	.ascii	"Intel(R) JPEG Library\000"
	.align	2
$LC54:
	.ascii	"Metasoft MJPEG Codec\000"
	.align	2
$LC55:
	.ascii	"huffman table decode error\012\000"
	.align	2
$LC56:
	.ascii	"Found EOI before any SOF, ignoring\012\000"
	.align	2
$LC57:
	.ascii	"QP: %d\012\000"
	.align	2
$LC58:
	.ascii	"Can not process SOS before SOF, skipping\012\000"
	.align	2
$LC59:
	.ascii	"restart interval: %d\012\000"
	.align	2
$LC60:
	.ascii	"mjpeg: unsupported coding type (%x)\012\000"
	.align	2
$LC61:
	.ascii	"marker parser used %d bytes (%d bits)\012\000"
	.align	2
$LC62:
	.ascii	"EOI missing, emulating\012\000"
	.align	2
$LC63:
	.ascii	"No JPEG data found in image\012\000"
	.align	2
$LC64:
	.ascii	"mjpeg decode frame unused %td bytes\012\000"
	.text
	.align	2
	.globl	ff_mjpeg_decode_frame
	.set	nomips16
	.set	nomicromips
	.ent	ff_mjpeg_decode_frame
	.type	ff_mjpeg_decode_frame, @function
ff_mjpeg_decode_frame:
	.frame	$sp,104,$31		# vars= 24, regs= 10/0, args= 32, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	lw	$3,16($7)
	addiu	$sp,$sp,-104
	lui	$28,%hi(__gnu_local_gp)
	sw	$19,76($sp)
	lui	$2,%hi($LC40)
	sw	$5,108($sp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$3,40($sp)
	lui	$3,%hi($LC39)
	sw	$23,92($sp)
	sw	$22,88($sp)
	lui	$22,%hi($L694)
	sw	$16,64($sp)
	lw	$19,20($7)
	addiu	$7,$3,%lo($LC39)
	lw	$5,40($sp)
	addiu	$22,$22,%lo($L694)
	lw	$23,136($4)
	sw	$20,80($sp)
	move	$20,$4
	addu	$19,$5,$19
	sw	$31,100($sp)
	move	$16,$5
	sw	$fp,96($sp)
	addiu	$4,$2,%lo($LC40)
	sw	$21,84($sp)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $5, 3($7)  
	lwr $5, %lo($LC39)($3)  
	
 # 0 "" 2
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($4)  
	lwr $3, %lo($LC40)($2)  
	
 # 0 "" 2
#NO_APP
	sltu	$2,$16,$19
	sw	$18,72($sp)
	sw	$17,68($sp)
	.cprestore	32
	sw	$6,112($sp)
	sw	$0,1192($23)
	sw	$5,44($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L726
	sw	$3,48($sp)
	.set	macro
	.set	reorder

$L826:
	lbu	$2,0($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L612
	.option	pic2
	li	$3,255			# 0xff
	.set	macro
	.set	reorder

$L608:
	move	$16,$18
	.set	noreorder
	.set	nomacro
	beq	$18,$19,$L820
	move	$2,$17
	.set	macro
	.set	reorder

$L612:
	addiu	$18,$16,1
	.set	noreorder
	.set	nomacro
	bne	$2,$3,$L608
	lbu	$17,1($16)
	.set	macro
	.set	reorder

	addiu	$fp,$17,64
	andi	$2,$fp,0x00ff
	sltu	$2,$2,63
	beq	$2,$0,$L608
	sltu	$2,$18,$19
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L821
	addiu	$16,$16,2
	.set	macro
	.set	reorder

$L609:
	subu	$21,$19,$18
$L717:
	lui	$6,%hi($LC64)
$L860:
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC64)
	move	$4,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$21
	.set	macro
	.set	reorder

	lw	$3,40($sp)
	subu	$2,$18,$3
$L805:
	lw	$31,100($sp)
	lw	$fp,96($sp)
	lw	$23,92($sp)
	lw	$22,88($sp)
	lw	$21,84($sp)
	lw	$20,80($sp)
	lw	$19,76($sp)
	lw	$18,72($sp)
	lw	$17,68($sp)
	lw	$16,64($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,104
	.set	macro
	.set	reorder

$L821:
	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC33)
	subu	$21,$19,$16
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC33)
	sw	$21,16($sp)
	move	$4,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$17
	.set	macro
	.set	reorder

	lw	$2,24($23)
	lw	$28,32($sp)
	slt	$2,$2,$21
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L812
	sw	$16,52($sp)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_free)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	lw	$4,28($23)
	.set	macro
	.set	reorder

	addiu	$4,$21,8
	lw	$28,32($sp)
	lw	$25,%call16(av_malloc)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	sw	$21,24($23)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC34)
	lw	$28,32($sp)
	li	$5,48			# 0x30
	lw	$7,24($23)
	addiu	$6,$6,%lo($LC34)
	sw	$2,28($23)
	lw	$25,%call16(av_log)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	lw	$28,32($sp)
$L812:
	li	$2,218			# 0xda
	.set	noreorder
	.set	nomacro
	beq	$17,$2,$L822
	sll	$3,$21,3
	.set	macro
	.set	reorder

	sra	$2,$3,3
	bltz	$2,$L740
	bltz	$3,$L740
	addu	$2,$16,$2
	move	$4,$16
$L657:
	sw	$4,4($23)
	sw	$3,16($23)
	sw	$2,8($23)
	sw	$0,12($23)
$L656:
	lw	$2,0($23)
	sw	$17,20($23)
	lw	$2,404($2)
	andi	$2,$2,0x100
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L658
	addiu	$2,$17,-208
	.set	macro
	.set	reorder

	sltu	$2,$2,8
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L857
	lui	$6,%hi($LC37)
	.set	macro
	.set	reorder

$L659:
	addiu	$2,$17,-224
$L861:
	sltu	$2,$2,16
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L824
	li	$2,254			# 0xfe
	.set	macro
	.set	reorder

	beq	$17,$2,$L825
$L660:
	andi	$fp,$fp,0x00ff
$L852:
	sltu	$2,$fp,57
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L815
	sll	$fp,$fp,2
	.set	macro
	.set	reorder

	addu	$fp,$22,$fp
	lw	$2,0($fp)
	j	$2
	.rdata
	.align	2
	.align	2
$L694:
	.word	$L693
	.word	$L693
	.word	$L695
	.word	$L696
	.word	$L697
	.word	$L698
	.word	$L698
	.word	$L698
	.word	$L698
	.word	$L698
	.word	$L698
	.word	$L698
	.word	$L815
	.word	$L698
	.word	$L698
	.word	$L698
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L699
	.word	$L700
	.word	$L701
	.word	$L702
	.word	$L815
	.word	$L703
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L815
	.word	$L704
	.word	$L705
	.text
$L705:
	lw	$25,%call16(ff_jpegls_decode_lse)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,ff_jpegls_decode_lse
1:	jalr	$25
	move	$4,$23
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bltz	$2,$L816
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L815:
	lw	$2,12($23)
$L706:
	addiu	$7,$2,7
$L868:
	sw	$2,16($sp)
	addiu	$3,$2,14
	lw	$25,%call16(av_log)($28)
	slt	$2,$7,0
	movn	$7,$3,$2
	lui	$6,%hi($LC61)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC61)
	move	$4,$20
	sra	$2,$7,3
	addu	$16,$16,$2
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$2
	.set	macro
	.set	reorder

	sltu	$2,$16,$19
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L826
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L726:
	lw	$2,1192($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L727
	lw	$25,%call16(av_log)($28)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC62)
	li	$5,24			# 0x18
	addiu	$6,$6,%lo($LC62)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	lw	$28,32($sp)
$L711:
	lw	$2,1192($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L858
	lui	$6,%hi($LC56)
	.set	macro
	.set	reorder

$L712:
	lw	$2,696($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L859
	subu	$21,$19,$16
	.set	macro
	.set	reorder

	lw	$2,700($23)
	lw	$3,5676($23)
	xori	$2,$2,0x1
	sltu	$3,$3,1
	.set	noreorder
	.set	nomacro
	beq	$2,$3,$L815
	sw	$2,700($23)
	.set	macro
	.set	reorder

	subu	$21,$19,$16
$L859:
	move	$18,$16
	lw	$3,108($sp)
$L872:
	addiu	$2,$23,976
	addiu	$4,$23,1184
$L716:
	lw	$8,0($2)
	addiu	$2,$2,16
	addiu	$3,$3,16
	lw	$7,-12($2)
	lw	$6,-8($2)
	lw	$5,-4($2)
	sw	$8,-16($3)
	sw	$7,-12($3)
	sw	$6,-8($3)
	.set	noreorder
	.set	nomacro
	bne	$2,$4,$L716
	sw	$5,-4($3)
	.set	macro
	.set	reorder

	lw	$4,0($2)
	lw	$2,4($2)
	sw	$4,0($3)
	sw	$2,4($3)
	li	$2,216			# 0xd8
	lw	$3,112($sp)
	sw	$2,0($3)
	lw	$2,704($23)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L860
	lui	$6,%hi($LC64)
	.set	macro
	.set	reorder

	lw	$4,676($23)
	lw	$2,680($23)
	lw	$5,756($23)
	lw	$8,672($23)
	slt	$3,$4,$2
	lw	$7,1212($23)
	movz	$2,$4,$3
	addiu	$6,$5,15
	addiu	$5,$5,30
	slt	$3,$6,0
	lw	$25,%call16(memset)($28)
	movn	$6,$5,$3
	move	$4,$7
	slt	$3,$2,$8
	movn	$2,$8,$3
	lw	$3,108($sp)
	sra	$6,$6,4
	move	$5,$2
	sw	$0,88($3)
	sw	$7,84($3)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$2,72($3)
	.set	macro
	.set	reorder

	lw	$2,404($20)
	andi	$2,$2,0x10
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L827
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L720:
	lw	$3,108($sp)
	lw	$4,72($3)
	sll	$3,$4,6
	sll	$2,$4,2
	subu	$2,$3,$2
	lw	$3,108($sp)
	subu	$2,$2,$4
	sll	$2,$2,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L717
	.option	pic2
	sw	$2,72($3)
	.set	macro
	.set	reorder

$L703:
	lw	$4,12($23)
	li	$5,-4			# 0xfffffffffffffffc
	lw	$3,4($23)
	srl	$2,$4,3
	addu	$2,$3,$2
	and	$6,$2,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$6,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$6,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$6,$4,0x7
	addu	$2,$2,$6
	li	$6,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$6
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $8
 # 0 "" 2
#NO_APP
	li	$7,4			# 0x4
	.set	noreorder
	.set	nomacro
	bne	$8,$7,$L828
	addiu	$2,$4,16
	.set	macro
	.set	reorder

	srl	$7,$2,3
	addu	$3,$3,$7
	and	$5,$3,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$2,$2,0x7
	addu	$2,$3,$2
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$6
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $7
 # 0 "" 2
#NO_APP
	lui	$6,%hi($LC59)
	lw	$25,%call16(av_log)($28)
	addiu	$2,$4,32
	lw	$4,0($23)
	li	$5,48			# 0x30
	sw	$7,5660($23)
	addiu	$6,$6,%lo($LC59)
	sw	$0,5664($23)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,12($23)
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	lw	$2,12($23)
	.set	macro
	.set	reorder

$L702:
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_dqt
	.option	pic2
	move	$4,$23
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	lw	$2,12($23)
	.set	macro
	.set	reorder

$L701:
	lw	$2,1192($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L829
	lui	$6,%hi($LC58)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_sos
	.option	pic2
	move	$4,$23
	.set	macro
	.set	reorder

	lw	$2,5668($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L722
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	lw	$2,696($23)
	bne	$2,$0,$L722
	lw	$2,1192($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L729
	lw	$18,52($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L872
	.option	pic2
	lw	$3,108($sp)
	.set	macro
	.set	reorder

$L700:
	lw	$2,5668($23)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L710
	sw	$0,5684($23)
	.set	macro
	.set	reorder

	lw	$2,696($23)
	beq	$2,$0,$L815
$L710:
	lw	$2,5660($23)
	bne	$2,$0,$L815
	lw	$2,1192($23)
	bne	$2,$0,$L712
$L729:
	lui	$6,%hi($LC56)
$L858:
	lw	$25,%call16(av_log)($28)
	li	$5,24			# 0x18
	addiu	$6,$6,%lo($LC56)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	lw	$2,12($23)
	.set	macro
	.set	reorder

$L699:
	lw	$2,12($23)
	sw	$0,5660($23)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	sw	$0,5664($23)
	.set	macro
	.set	reorder

$L698:
	lui	$6,%hi($LC60)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC60)
	move	$4,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L815
	.option	pic2
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L697:
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_dht
	.option	pic2
	move	$4,$23
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L815
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC55)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC55)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L805
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L696:
	li	$2,1			# 0x1
	sw	$0,708($23)
	sw	$0,712($23)
	move	$4,$23
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_sof
	.option	pic2
	sw	$2,704($23)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L815
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L816:
	li	$2,-1			# 0xffffffffffffffff
$L830:
	lw	$31,100($sp)
	lw	$fp,96($sp)
	lw	$23,92($sp)
	lw	$22,88($sp)
	lw	$21,84($sp)
	lw	$20,80($sp)
	lw	$19,76($sp)
	lw	$18,72($sp)
	lw	$17,68($sp)
	lw	$16,64($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,104
	.set	macro
	.set	reorder

$L695:
	li	$2,1			# 0x1
	sw	$0,704($23)
	sw	$0,708($23)
	move	$4,$23
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_sof
	.option	pic2
	sw	$2,712($23)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L815
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L830
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L693:
	sw	$0,704($23)
	move	$4,$23
	sw	$0,708($23)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_sof
	.option	pic2
	sw	$0,712($23)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L815
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L830
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L704:
	li	$2,1			# 0x1
	sw	$0,712($23)
	move	$4,$23
	sw	$2,704($23)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	ff_mjpeg_decode_sof
	.option	pic2
	sw	$2,708($23)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L815
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L830
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L871:
	move	$2,$0
	move	$3,$4
$L615:
	addu	$4,$3,$5
$L625:
	lui	$6,%hi($LC35)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	sw	$2,16($23)
	addiu	$6,$6,%lo($LC35)
	sw	$4,8($23)
	addu	$7,$21,$7
	sw	$3,4($23)
	sw	$0,12($23)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	lw	$2,0($23)
	lw	$28,32($sp)
	sw	$17,20($23)
	lw	$2,404($2)
	andi	$2,$2,0x100
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L861
	addiu	$2,$17,-224
	.set	macro
	.set	reorder

$L658:
	lui	$6,%hi($LC36)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC36)
	move	$4,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$17
	.set	macro
	.set	reorder

	addiu	$2,$17,-208
	sltu	$2,$2,8
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L659
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC37)
$L857:
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC37)
	move	$4,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	andi	$7,$17,0xf
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L660
	.option	pic2
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L740:
	move	$2,$0
	move	$3,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L657
	.option	pic2
	move	$4,$0
	.set	macro
	.set	reorder

$L824:
	lw	$6,12($23)
	li	$4,-4			# 0xfffffffffffffffc
	lw	$3,4($23)
	srl	$2,$6,3
	addu	$2,$3,$2
	and	$4,$2,$4
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$6,0x7
	addu	$2,$2,$4
	li	$4,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$4
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $8
 # 0 "" 2
#NO_APP
	addiu	$10,$6,16
	slt	$2,$8,5
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L660
	sw	$10,12($23)
	.set	macro
	.set	reorder

	sll	$2,$8,3
	lw	$4,16($23)
	addu	$2,$10,$2
	slt	$2,$4,$2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L660
	addiu	$11,$6,32
	.set	macro
	.set	reorder

	lw	$4,0($23)
	srl	$2,$10,3
	li	$9,16711680			# 0xff0000
	li	$7,-16777216			# 0xffffffffff000000
	addu	$2,$3,$2
	srl	$13,$11,3
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $5, 3($2)  
	lwr $5, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$5,8
	sw	$11,12($23)
	sll	$5,$5,8
	addiu	$9,$9,255
	ori	$7,$7,0xff00
	and	$12,$5,$7
	addu	$3,$3,$13
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $5, 3($3)  
	lwr $5, 0($3)  
	
 # 0 "" 2
#NO_APP
	move	$3,$5
	sll	$3,$3,8
	and	$2,$2,$9
	srl	$5,$5,8
	or	$2,$2,$12
	and	$5,$5,$9
	and	$12,$3,$7
	sll	$3,$2,16
	srl	$2,$2,16
	or	$5,$5,$12
	or	$3,$3,$2
	andi	$10,$10,0x7
	sll	$2,$5,16
	srl	$5,$5,16
	sll	$3,$3,$10
	or	$5,$2,$5
	andi	$2,$11,0x7
	sll	$2,$5,$2
	srl	$3,$3,16
	srl	$2,$2,16
	sll	$3,$3,16
	addiu	$6,$6,48
	or	$2,$3,$2
	srl	$3,$2,8
	sll	$2,$2,8
	sw	$6,12($23)
	and	$3,$3,$9
	lw	$5,404($4)
	and	$2,$2,$7
	or	$2,$3,$2
	sll	$6,$2,16
	srl	$3,$2,16
	andi	$5,$5,0x100
	or	$3,$3,$6
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L831
	addiu	$18,$8,-6
	.set	macro
	.set	reorder

$L663:
	lw	$2,44($sp)
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L832
	lw	$4,48($sp)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$4,$L833
	lui	$4,%hi($LC42)
	.set	macro
	.set	reorder

	addiu	$5,$4,%lo($LC42)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $2, 3($5)  
	lwr $2, %lo($LC42)($4)  
	
 # 0 "" 2
#NO_APP
	beq	$3,$2,$L834
$L670:
	lui	$4,%hi($LC44)
	addiu	$5,$4,%lo($LC44)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $2, 3($5)  
	lwr $2, %lo($LC44)($4)  
	
 # 0 "" 2
#NO_APP
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L835
	li	$3,225			# 0xe1
	.set	macro
	.set	reorder

	lw	$4,20($23)
	.set	noreorder
	.set	nomacro
	beq	$4,$3,$L836
	slt	$3,$18,33
	.set	macro
	.set	reorder

$L666:
	.set	noreorder
	.set	nomacro
	bltz	$18,$L862
	lui	$6,%hi($LC49)
	.set	macro
	.set	reorder

$L678:
	addiu	$2,$18,-1
	blez	$2,$L660
	lw	$3,12($23)
$L679:
	addiu	$3,$3,8
	addiu	$2,$2,-1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L679
	sw	$3,12($23)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L852
	.option	pic2
	andi	$fp,$fp,0x00ff
	.set	macro
	.set	reorder

$L822:
	lw	$5,708($23)
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L614
	move	$7,$0
	.set	macro
	.set	reorder

	sltu	$2,$16,$19
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L871
	lw	$4,28($23)
	.set	macro
	.set	reorder

	li	$8,104			# 0x68
	li	$5,255			# 0xff
	move	$2,$16
$L624:
	lbu	$3,0($2)
	addiu	$7,$4,1
	sb	$3,0($4)
	lw	$6,224($20)
	.set	noreorder
	.set	nomacro
	beq	$6,$8,$L733
	addiu	$2,$2,1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$5,$L838
	sltu	$3,$2,$19
	.set	macro
	.set	reorder

$L733:
	move	$4,$7
$L616:
	sltu	$3,$2,$19
	bne	$3,$0,$L624
	lw	$3,28($23)
	subu	$2,$4,$3
	subu	$7,$3,$4
	sll	$2,$2,3
	sra	$5,$2,3
$L618:
	.set	noreorder
	.set	nomacro
	bltz	$5,$L863
	move	$4,$0
	.set	macro
	.set	reorder

	bgez	$2,$L615
$L863:
	move	$2,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L625
	.option	pic2
	move	$3,$0
	.set	macro
	.set	reorder

$L838:
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L864
	addiu	$2,$2,1
	.set	macro
	.set	reorder

	addiu	$2,$2,-1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L853
	.option	pic2
	lw	$3,28($23)
	.set	macro
	.set	reorder

$L619:
	.set	noreorder
	.set	nomacro
	bne	$3,$5,$L865
	addiu	$6,$3,48
	.set	macro
	.set	reorder

	addiu	$2,$2,1
$L864:
	.set	noreorder
	.set	nomacro
	bne	$2,$19,$L619
	lbu	$3,-1($2)
	.set	macro
	.set	reorder

	move	$2,$19
	addiu	$6,$3,48
$L865:
	andi	$6,$6,0x00ff
	sltu	$6,$6,8
	beq	$6,$0,$L839
	sb	$3,1($4)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L616
	.option	pic2
	addiu	$4,$4,2
	.set	macro
	.set	reorder

$L839:
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L616
	move	$4,$7
	.set	macro
	.set	reorder

	lw	$3,28($23)
$L853:
	subu	$2,$7,$3
	subu	$7,$3,$7
	sll	$2,$2,3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L618
	.option	pic2
	sra	$5,$2,3
	.set	macro
	.set	reorder

$L614:
	lw	$3,5684($23)
	lw	$25,28($23)
	move	$2,$16
	li	$4,255			# 0xff
	addiu	$3,$3,1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L628
	.option	pic2
	sw	$3,5684($23)
	.set	macro
	.set	reorder

$L636:
	lbu	$3,-1($2)
	.set	noreorder
	.set	nomacro
	beq	$3,$4,$L840
	addiu	$7,$7,1
	.set	macro
	.set	reorder

$L628:
	sltu	$3,$2,$19
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L636
	addiu	$2,$2,1
	.set	macro
	.set	reorder

	sll	$24,$7,3
	move	$5,$25
$L634:
	li	$14,16711680			# 0xff0000
	move	$2,$0
	li	$13,-16777216			# 0xffffffffff000000
	li	$6,32			# 0x20
	move	$9,$0
	li	$15,8			# 0x8
	addiu	$14,$14,255
	li	$12,255			# 0xff
	li	$31,7			# 0x7
	ori	$13,$13,0xff00
	move	$11,$2
$L638:
	slt	$2,$9,$7
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L866
	slt	$3,$6,32
	.set	macro
	.set	reorder

$L650:
	addiu	$10,$9,1
	slt	$3,$6,9
	addu	$8,$16,$10
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L841
	lbu	$4,-1($8)
	.set	macro
	.set	reorder

	subu	$3,$15,$6
	sll	$2,$11,$6
	srl	$3,$4,$3
	andi	$11,$5,0x3
	.set	noreorder
	.set	nomacro
	beq	$11,$0,$L641
	or	$2,$3,$2
	.set	macro
	.set	reorder

	srl	$3,$2,8
	sll	$2,$2,8
	and	$3,$3,$14
	and	$2,$2,$13
	or	$2,$3,$2
	sll	$3,$2,16
	srl	$2,$2,16
	or	$2,$3,$2
#APP
 # 42 "../libavutil/mips/intreadwrite.h" 1
	swl $2, 3($5)  
	swr $2, 0($5)  
	
 # 0 "" 2
#NO_APP
$L642:
	addiu	$5,$5,4
	addiu	$6,$6,24
	.set	noreorder
	.set	nomacro
	beq	$4,$12,$L842
	move	$11,$4
	.set	macro
	.set	reorder

$L643:
	.set	noreorder
	.set	nomacro
	bne	$10,$7,$L650
	move	$9,$10
	.set	macro
	.set	reorder

	slt	$3,$6,32
$L866:
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L655
	sll	$2,$11,$6
	.set	macro
	.set	reorder

	li	$4,31			# 0x1f
	subu	$4,$4,$6
	srl	$4,$4,3
	addiu	$4,$4,1
	addu	$4,$5,$4
$L654:
	srl	$3,$2,24
	addiu	$5,$5,1
	sll	$2,$2,8
	.set	noreorder
	.set	nomacro
	bne	$5,$4,$L654
	sb	$3,-1($5)
	.set	macro
	.set	reorder

$L655:
	addiu	$2,$24,7
	sra	$2,$2,3
	bltz	$2,$L738
	bltz	$24,$L738
	addu	$2,$25,$2
$L652:
	sw	$25,4($23)
	sw	$24,16($23)
	sw	$2,8($23)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L656
	.option	pic2
	sw	$0,12($23)
	.set	macro
	.set	reorder

$L841:
	sll	$11,$11,8
	addiu	$6,$6,-8
	.set	noreorder
	.set	nomacro
	bne	$4,$12,$L643
	or	$11,$4,$11
	.set	macro
	.set	reorder

$L842:
	slt	$2,$6,8
	lbu	$3,0($8)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L843
	addiu	$9,$9,2
	.set	macro
	.set	reorder

	subu	$2,$31,$6
	sll	$11,$11,$6
	srl	$2,$3,$2
	andi	$4,$5,0x3
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L844
	or	$2,$2,$11
	.set	macro
	.set	reorder

	srl	$4,$2,8
	sll	$2,$2,8
	and	$4,$4,$14
	and	$2,$2,$13
	or	$2,$4,$2
	sll	$4,$2,16
	srl	$2,$2,16
	or	$2,$4,$2
	sw	$2,0($5)
	addiu	$5,$5,4
$L855:
	addiu	$6,$6,25
	move	$11,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L638
	.option	pic2
	addiu	$24,$24,-1
	.set	macro
	.set	reorder

$L641:
	srl	$3,$2,8
	sll	$2,$2,8
	and	$3,$3,$14
	and	$2,$2,$13
	or	$2,$3,$2
	sll	$3,$2,16
	srl	$2,$2,16
	or	$2,$3,$2
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L642
	.option	pic2
	sw	$2,0($5)
	.set	macro
	.set	reorder

$L840:
	addu	$3,$16,$7
	sltu	$2,$3,$19
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L867
	addiu	$7,$7,1
	.set	macro
	.set	reorder

	addiu	$7,$7,-1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L854
	.option	pic2
	addiu	$7,$7,-2
	.set	macro
	.set	reorder

$L630:
	.set	noreorder
	.set	nomacro
	bne	$5,$4,$L633
	addiu	$3,$3,1
	.set	macro
	.set	reorder

	addiu	$7,$7,1
$L867:
	addu	$2,$16,$7
	sltu	$6,$2,$19
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L630
	lbu	$5,0($3)
	.set	macro
	.set	reorder

$L633:
	sll	$5,$5,24
	sra	$5,$5,24
	bgez	$5,$L628
	addiu	$7,$7,-2
$L854:
	li	$2,-1			# 0xffffffffffffffff
	sll	$24,$7,3
	.set	noreorder
	.set	nomacro
	beq	$7,$2,$L634
	move	$5,$0
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L634
	.option	pic2
	move	$5,$25
	.set	macro
	.set	reorder

$L825:
	lw	$2,12($23)
	lw	$3,4($23)
	srl	$4,$2,3
	addu	$3,$3,$4
	li	$4,-4			# 0xfffffffffffffffc
	and	$4,$3,$4
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$4,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$4,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$4,$2,0x7
	addu	$3,$3,$4
	li	$4,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$4
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $17
 # 0 "" 2
#NO_APP
	addiu	$2,$2,16
	slt	$3,$17,2
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L706
	sw	$2,12($23)
	.set	macro
	.set	reorder

	addiu	$21,$17,-2
	lw	$4,16($23)
	sll	$3,$21,3
	addu	$3,$2,$3
	slt	$3,$4,$3
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L868
	addiu	$7,$2,7
	.set	macro
	.set	reorder

	lw	$25,%call16(av_malloc)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	addiu	$4,$17,-1
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L815
	move	$18,$2
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$21,$0,$L743
	addu	$7,$2,$21
	.set	macro
	.set	reorder

	li	$4,-4			# 0xfffffffffffffffc
	li	$2,8			# 0x8
	move	$8,$18
	move	$9,$21
$L683:
	lw	$5,12($23)
	lw	$3,4($23)
	srl	$6,$5,3
	addu	$3,$3,$6
	and	$6,$3,$4
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$6,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$6,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$6,$5,0x7
	addu	$3,$3,$6
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$2
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addiu	$5,$5,8
	addiu	$8,$8,1
	sw	$5,12($23)
	.set	noreorder
	.set	nomacro
	bne	$8,$7,$L683
	sb	$3,-1($8)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$21,$0,$L682
	addiu	$2,$21,-1
	.set	macro
	.set	reorder

	li	$3,10			# 0xa
	addu	$2,$18,$2
	lb	$4,0($2)
	beq	$4,$3,$L845
$L682:
	addu	$9,$18,$9
	sb	$0,0($9)
$L684:
	lw	$4,0($23)
	lw	$2,404($4)
	andi	$2,$2,0x1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L846
	lw	$25,%call16(strcmp)($28)
	.set	macro
	.set	reorder

	lui	$5,%hi($LC51)
	move	$4,$18
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,strcmp
1:	jalr	$25
	addiu	$5,$5,%lo($LC51)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L847
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L686:
	lui	$5,%hi($LC52)
	lw	$25,%call16(strcmp)($28)
	move	$4,$18
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,strcmp
1:	jalr	$25
	addiu	$5,$5,%lo($LC52)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L688
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	li	$2,1			# 0x1
	sw	$2,5672($23)
$L687:
	lw	$25,%call16(av_free)($28)
$L870:
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_free
1:	jalr	$25
	move	$4,$18
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	lw	$2,12($23)
	.set	macro
	.set	reorder

$L738:
	move	$2,$0
	move	$24,$0
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L652
	.option	pic2
	move	$25,$0
	.set	macro
	.set	reorder

$L832:
	lw	$4,12($23)
	li	$6,1			# 0x1
	lw	$3,4($23)
	srl	$5,$4,3
	sw	$6,5668($23)
	addu	$3,$3,$5
	li	$5,-4			# 0xfffffffffffffffc
	and	$5,$3,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$5,$4,0x7
	addu	$3,$3,$5
	li	$5,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$5
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addiu	$4,$4,8
	sw	$4,12($23)
	li	$4,2			# 0x2
	beq	$3,$4,$L848
	bne	$3,$6,$L666
	.set	noreorder
	.set	nomacro
	bgez	$18,$L678
	sw	$0,700($23)
	.set	macro
	.set	reorder

	lui	$6,%hi($LC49)
$L862:
	lw	$25,%call16(av_log)($28)
	lw	$4,0($23)
	li	$5,16			# 0x10
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC49)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L678
	.option	pic2
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L831:
	lui	$6,%hi($LC38)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	sw	$3,56($sp)
	addiu	$6,$6,%lo($LC38)
	sw	$8,60($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$3
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	lw	$8,60($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L663
	.option	pic2
	lw	$3,56($sp)
	.set	macro
	.set	reorder

$L843:
	sll	$11,$11,7
	addiu	$6,$6,-7
	or	$11,$3,$11
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L638
	.option	pic2
	addiu	$24,$24,-1
	.set	macro
	.set	reorder

$L829:
	lw	$25,%call16(av_log)($28)
	li	$5,24			# 0x18
	addiu	$6,$6,%lo($LC58)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	lw	$2,12($23)
	.set	macro
	.set	reorder

$L844:
	srl	$4,$2,8
	sll	$2,$2,8
	and	$4,$4,$14
	and	$2,$2,$13
	or	$2,$4,$2
	sll	$4,$2,16
	srl	$2,$2,16
	or	$2,$4,$2
#APP
 # 42 "../libavutil/mips/intreadwrite.h" 1
	swl $2, 3($5)  
	swr $2, 0($5)  
	
 # 0 "" 2
#NO_APP
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L855
	.option	pic2
	addiu	$5,$5,4
	.set	macro
	.set	reorder

$L836:
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L666
	li	$3,16711680			# 0xff0000
	.set	macro
	.set	reorder

	lw	$7,12($23)
	lw	$4,4($23)
	li	$5,-16777216			# 0xffffffffff000000
	addiu	$6,$3,255
	addiu	$9,$7,16
	srl	$2,$7,3
	srl	$11,$9,3
	addu	$2,$4,$2
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($2)  
	lwr $3, 0($2)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sw	$9,12($23)
	sll	$3,$3,8
	ori	$5,$5,0xff00
	and	$10,$3,$5
	addu	$3,$4,$11
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $4, 3($3)  
	lwr $4, 0($3)  
	
 # 0 "" 2
#NO_APP
	move	$3,$4
	sll	$3,$3,8
	and	$2,$2,$6
	srl	$4,$4,8
	or	$2,$2,$10
	and	$4,$4,$6
	and	$10,$3,$5
	sll	$3,$2,16
	srl	$2,$2,16
	or	$4,$4,$10
	or	$3,$3,$2
	andi	$10,$7,0x7
	sll	$2,$4,16
	srl	$4,$4,16
	sll	$3,$3,$10
	or	$4,$2,$4
	andi	$2,$9,0x7
	sll	$2,$4,$2
	srl	$3,$3,16
	srl	$2,$2,16
	sll	$3,$3,16
	addiu	$7,$7,32
	or	$2,$3,$2
	srl	$3,$2,8
	sll	$2,$2,8
	sw	$7,12($23)
	and	$3,$3,$6
	and	$2,$2,$5
	or	$2,$3,$2
	sll	$3,$2,16
	srl	$2,$2,16
	lui	$4,%hi($LC47)
	or	$3,$3,$2
	addiu	$5,$4,%lo($LC47)
	addiu	$18,$8,-10
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $2, 3($5)  
	lwr $2, %lo($LC47)($4)  
	
 # 0 "" 2
#NO_APP
	bne	$3,$2,$L666
	lw	$4,0($23)
	lw	$3,404($4)
	andi	$3,$3,0x1
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L666
	lui	$6,%hi($LC48)
	.set	macro
	.set	reorder

	lw	$25,%call16(av_log)($28)
	li	$5,32			# 0x20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC48)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L848:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	sw	$6,700($23)
	.set	macro
	.set	reorder

$L722:
	lw	$2,5660($23)
	bne	$2,$0,$L711
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	lw	$2,12($23)
	.set	macro
	.set	reorder

$L833:
	lw	$10,12($23)
	li	$9,-4			# 0xfffffffffffffffc
	lw	$3,4($23)
	addiu	$5,$10,8
	srl	$4,$5,3
	addu	$4,$3,$4
	and	$6,$4,$9
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$6,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$6,4
 # 0 "" 2
#NO_APP
	andi	$4,$4,0x3
	sll	$4,$4,3
	andi	$5,$5,0x7
	addu	$4,$4,$5
	li	$6,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$4,$6
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $7
 # 0 "" 2
#NO_APP
	addiu	$5,$10,16
	srl	$4,$5,3
	addu	$4,$3,$4
	and	$11,$4,$9
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$11,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$11,4
 # 0 "" 2
#NO_APP
	andi	$4,$4,0x3
	sll	$4,$4,3
	andi	$5,$5,0x7
	addu	$4,$4,$5
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$4,$6
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $6
 # 0 "" 2
#NO_APP
	addiu	$11,$10,32
	lw	$4,0($23)
	srl	$5,$11,3
	addu	$5,$3,$5
	and	$12,$5,$9
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$12,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$12,4
 # 0 "" 2
#NO_APP
	andi	$5,$5,0x3
	sll	$5,$5,3
	andi	$11,$11,0x7
	addu	$5,$5,$11
	li	$11,16			# 0x10
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$5,$11
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $5
 # 0 "" 2
#NO_APP
	addiu	$10,$10,48
	sw	$10,12($23)
	sw	$5,392($4)
	lw	$10,12($23)
	srl	$5,$10,3
	addu	$5,$3,$5
	and	$9,$5,$9
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$9,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$9,4
 # 0 "" 2
#NO_APP
	andi	$5,$5,0x3
	sll	$5,$5,3
	andi	$9,$10,0x7
	addu	$5,$5,$9
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$5,$11
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $9
 # 0 "" 2
#NO_APP
	addiu	$10,$10,16
	sw	$10,12($23)
	sw	$9,396($4)
	lw	$5,404($4)
	andi	$5,$5,0x1
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L849
	lw	$25,%call16(av_log)($28)
	.set	macro
	.set	reorder

$L668:
	lw	$5,12($23)
	li	$6,-4			# 0xfffffffffffffffc
	srl	$4,$5,3
	addu	$4,$3,$4
	and	$7,$4,$6
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$7,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$7,4
 # 0 "" 2
#NO_APP
	andi	$4,$4,0x3
	sll	$4,$4,3
	andi	$7,$5,0x7
	addu	$4,$4,$7
	li	$9,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$4,$9
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $4
 # 0 "" 2
#NO_APP
	addiu	$7,$5,8
	srl	$10,$7,3
	addu	$3,$3,$10
	and	$6,$3,$6
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$6,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$6,4
 # 0 "" 2
#NO_APP
	andi	$3,$3,0x3
	sll	$3,$3,3
	andi	$7,$7,0x7
	addu	$3,$3,$7
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$3,$9
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $3
 # 0 "" 2
#NO_APP
	addiu	$5,$5,16
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L669
	sw	$5,12($23)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L669
	mul	$5,$4,$3
	.set	macro
	.set	reorder

	addiu	$4,$8,-16
	sll	$3,$5,1
	addu	$3,$3,$5
	subu	$3,$0,$3
	addu	$4,$4,$3
	addu	$3,$18,$3
	slt	$4,$0,$4
	movn	$18,$3,$4
$L669:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	addiu	$18,$18,-10
	.set	macro
	.set	reorder

$L834:
	lw	$5,12($23)
	lw	$4,4($23)
	srl	$6,$5,3
	addu	$4,$4,$6
	li	$6,-4			# 0xfffffffffffffffc
	and	$6,$4,$6
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$6,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$6,4
 # 0 "" 2
#NO_APP
	andi	$4,$4,0x3
	sll	$4,$4,3
	andi	$6,$5,0x7
	addu	$4,$4,$6
	li	$6,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$4,$6
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $6
 # 0 "" 2
#NO_APP
	addiu	$5,$5,8
	li	$4,101			# 0x65
	.set	noreorder
	.set	nomacro
	bne	$6,$4,$L670
	sw	$5,12($23)
	.set	macro
	.set	reorder

	lw	$4,0($23)
	lw	$2,404($4)
	andi	$2,$2,0x1
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L869
	addiu	$5,$5,56
	.set	macro
	.set	reorder

	lui	$6,%hi($LC43)
	lw	$25,%call16(av_log)($28)
	li	$5,32			# 0x20
	sw	$8,60($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC43)
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	lw	$5,12($23)
	lw	$8,60($sp)
	addiu	$5,$5,56
$L869:
	addiu	$18,$8,-13
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	sw	$5,12($23)
	.set	macro
	.set	reorder

$L835:
	lw	$4,0($23)
	lw	$2,404($4)
	andi	$2,$2,0x1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L850
	lui	$6,%hi($LC45)
	.set	macro
	.set	reorder

$L673:
	lw	$3,12($23)
	lw	$2,4($23)
	addiu	$4,$3,64
	srl	$5,$4,3
	addu	$2,$2,$5
	li	$5,-4			# 0xfffffffffffffffc
	and	$5,$2,$5
#APP
 # 62 "mjpegdec.c" 1
	S32LDDR xr2,$5,0
 # 0 "" 2
 # 63 "mjpegdec.c" 1
	S32LDDR xr1,$5,4
 # 0 "" 2
#NO_APP
	andi	$2,$2,0x3
	sll	$2,$2,3
	andi	$4,$4,0x7
	addu	$2,$2,$4
	li	$4,8			# 0x8
#APP
 # 64 "mjpegdec.c" 1
	S32EXTRV xr2,xr1,$2,$4
 # 0 "" 2
 # 65 "mjpegdec.c" 1
	S32M2I xr2, $4
 # 0 "" 2
#NO_APP
	addiu	$3,$3,72
	li	$2,1			# 0x1
	.set	noreorder
	.set	nomacro
	beq	$4,$2,$L675
	sw	$3,12($23)
	.set	macro
	.set	reorder

	li	$3,2			# 0x2
	.set	noreorder
	.set	nomacro
	bne	$4,$3,$L851
	lui	$6,%hi($LC46)
	.set	macro
	.set	reorder

	sw	$2,716($23)
	addiu	$18,$8,-15
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	sw	$2,724($23)
	.set	macro
	.set	reorder

$L727:
	lui	$6,%hi($LC63)
	li	$5,8			# 0x8
	addiu	$6,$6,%lo($LC63)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$20
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L830
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L828:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L706
	.option	pic2
	sw	$2,12($23)
	.set	macro
	.set	reorder

$L847:
	li	$2,1			# 0x1
$L856:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L687
	.option	pic2
	sw	$2,5668($23)
	.set	macro
	.set	reorder

$L851:
	lw	$25,%call16(av_log)($28)
	lw	$4,0($23)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC46)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$8,60($sp)
	.set	macro
	.set	reorder

	lw	$8,60($sp)
	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	addiu	$18,$8,-15
	.set	macro
	.set	reorder

$L846:
	lui	$6,%hi($LC50)
	lw	$25,%call16(av_log)($28)
	li	$5,32			# 0x20
	addiu	$6,$6,%lo($LC50)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$7,$18
	.set	macro
	.set	reorder

	lui	$5,%hi($LC51)
	lw	$28,32($sp)
	move	$4,$18
	lw	$25,%call16(strcmp)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,strcmp
1:	jalr	$25
	addiu	$5,$5,%lo($LC51)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L686
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L856
	.option	pic2
	li	$2,1			# 0x1
	.set	macro
	.set	reorder

$L845:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L684
	.option	pic2
	sb	$0,0($2)
	.set	macro
	.set	reorder

$L849:
	li	$5,32			# 0x20
	sw	$6,16($sp)
	lui	$6,%hi($LC41)
	lw	$3,392($4)
	addiu	$6,$6,%lo($LC41)
	sw	$8,60($sp)
	sw	$9,24($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,20($sp)
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	lw	$3,4($23)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L668
	.option	pic2
	lw	$8,60($sp)
	.set	macro
	.set	reorder

$L675:
	sw	$4,716($23)
	addiu	$18,$8,-15
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L666
	.option	pic2
	sw	$0,724($23)
	.set	macro
	.set	reorder

$L850:
	lw	$25,%call16(av_log)($28)
	li	$5,32			# 0x20
	sw	$8,60($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC45)
	.set	macro
	.set	reorder

	lw	$28,32($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L673
	.option	pic2
	lw	$8,60($sp)
	.set	macro
	.set	reorder

$L688:
	slt	$2,$17,21
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L689
	li	$2,20			# 0x14
	.set	macro
	.set	reorder

	lui	$5,%hi($LC53)
	lw	$25,%call16(strncmp)($28)
	li	$6,21			# 0x15
	addiu	$5,$5,%lo($LC53)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,strncmp
1:	jalr	$25
	move	$4,$18
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L691
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	li	$2,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L687
	.option	pic2
	sw	$2,5688($23)
	.set	macro
	.set	reorder

$L689:
	.set	noreorder
	.set	nomacro
	bne	$17,$2,$L870
	lw	$25,%call16(av_free)($28)
	.set	macro
	.set	reorder

$L691:
	lui	$5,%hi($LC54)
	lw	$25,%call16(strncmp)($28)
	li	$6,20			# 0x14
	addiu	$5,$5,%lo($LC54)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,strncmp
1:	jalr	$25
	move	$4,$18
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L687
	lw	$28,32($sp)
	.set	macro
	.set	reorder

	li	$2,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L687
	.option	pic2
	sw	$2,5688($23)
	.set	macro
	.set	reorder

$L743:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L682
	.option	pic2
	move	$9,$0
	.set	macro
	.set	reorder

$L827:
	lw	$3,108($sp)
	lui	$6,%hi($LC57)
	lw	$25,%call16(av_log)($28)
	li	$5,48			# 0x30
	addiu	$6,$6,%lo($LC57)
	move	$4,$20
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	lw	$7,72($3)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L720
	.option	pic2
	lw	$28,32($sp)
	.set	macro
	.set	reorder

$L820:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L609
	.option	pic2
	move	$18,$19
	.set	macro
	.set	reorder

	.end	ff_mjpeg_decode_frame
	.size	ff_mjpeg_decode_frame, .-ff_mjpeg_decode_frame
	.globl	thp_decoder
	.section	.rodata.str1.4
	.align	2
$LC65:
	.ascii	"thp\000"
	.align	2
$LC66:
	.ascii	"Nintendo Gamecube THP video\000"
	.section	.data.rel.local,"aw",@progbits
	.align	2
	.type	thp_decoder, @object
	.size	thp_decoder, 72
thp_decoder:
	.word	$LC65
	.word	0
	.word	104
	.word	5712
	.word	ff_mjpeg_decode_init
	.word	0
	.word	ff_mjpeg_decode_end
	.word	ff_mjpeg_decode_frame
	.word	2
	.word	0
	.space	12
	.word	$LC66
	.space	12
	.byte	3
	.space	3
	.globl	mjpeg_decoder
	.section	.rodata.str1.4
	.align	2
$LC67:
	.ascii	"mjpeg\000"
	.align	2
$LC68:
	.ascii	"MJPEG (Motion JPEG)\000"
	.section	.data.rel.local
	.align	2
	.type	mjpeg_decoder, @object
	.size	mjpeg_decoder, 72
mjpeg_decoder:
	.word	$LC67
	.word	0
	.word	8
	.word	5712
	.word	ff_mjpeg_decode_init
	.word	0
	.word	ff_mjpeg_decode_end
	.word	ff_mjpeg_decode_frame
	.word	2
	.word	0
	.space	12
	.word	$LC68
	.space	12
	.byte	8
	.space	3
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
