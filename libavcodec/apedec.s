	.file	1 "apedec.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.abicalls
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	ape_flush
	.type	ape_flush, @function
ape_flush:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lw	$2,136($4)
	j	$31
	sw	$0,4128($2)

	.set	macro
	.set	reorder
	.end	ape_flush
	.size	ape_flush, .-ape_flush
	.section	.text.unlikely,"ax",@progbits
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	ape_decode_close
	.type	ape_decode_close, @function
ape_decode_close:
	.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-32
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$16,24($sp)
	sw	$31,28($sp)
	.cprestore	16
	lw	$16,136($4)
	li	$4,43376			# 0xa970
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addu	$4,$16,$4

	li	$4,43380			# 0xa974
	lw	$28,16($sp)
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addu	$4,$16,$4

	li	$4,43384			# 0xa978
	lw	$28,16($sp)
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addu	$4,$16,$4

	li	$4,43540			# 0xaa14
	lw	$28,16($sp)
	lw	$25,%call16(av_freep)($28)
	.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
	addu	$4,$16,$4

	move	$2,$0
	lw	$31,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,32

	.set	macro
	.set	reorder
	.end	ape_decode_close
	.size	ape_decode_close, .-ape_decode_close
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align	2
$LC0:
	.ascii	"Incorrect extradata\012\000"
	.align	2
$LC1:
	.ascii	"Only 16-bit samples are supported\012\000"
	.align	2
$LC2:
	.ascii	"Only mono and stereo is supported\012\000"
	.align	2
$LC3:
	.ascii	"Compression Level: %d - Flags: %d\012\000"
	.align	2
$LC4:
	.ascii	"Incorrect compression level %d\012\000"
	.section	.text.unlikely
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	ape_decode_init
	.type	ape_decode_init, @function
ape_decode_init:
	.frame	$sp,72,$31		# vars= 0, regs= 9/0, args= 24, gp= 8
	.mask	0x80ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	lw	$3,28($4)
	addiu	$sp,$sp,-72
	addiu	$28,$28,%lo(__gnu_local_gp)
	li	$2,6			# 0x6
	sw	$21,56($sp)
	sw	$20,52($sp)
	move	$21,$4
	sw	$31,68($sp)
	sw	$23,64($sp)
	sw	$22,60($sp)
	sw	$19,48($sp)
	sw	$18,44($sp)
	sw	$17,40($sp)
	sw	$16,36($sp)
	.cprestore	24
	lw	$25,%call16(av_log)($28)
	beq	$3,$2,$L5
	lw	$20,136($4)

	lui	$6,%hi($LC0)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC0)
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	li	$19,-1			# 0xffffffffffffffff

	.option	pic0
	j	$L19
	.option	pic2
	lw	$31,68($sp)

$L5:
	lw	$3,384($4)
	li	$2,16			# 0x10
	beq	$3,$2,$L7
	lui	$6,%hi($LC1)

	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC1)
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	li	$19,-1			# 0xffffffffffffffff

	.option	pic0
	j	$L19
	.option	pic2
	lw	$31,68($sp)

$L7:
	lw	$2,68($4)
	slt	$3,$2,3
	bne	$3,$0,$L8
	lui	$6,%hi($LC3)

	lui	$6,%hi($LC2)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC2)
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	li	$19,-1			# 0xffffffffffffffff

	.option	pic0
	j	$L19
	.option	pic2
	lw	$31,68($sp)

$L8:
	lw	$3,24($4)
	sw	$4,0($20)
	li	$5,48			# 0x30
	sw	$2,4124($20)
	addiu	$6,$6,%lo($LC3)
	li	$19,1000			# 0x3e8
	lbu	$2,1($3)
	lbu	$7,0($3)
	sll	$2,$2,8
	or	$2,$2,$7
	sw	$2,4132($20)
	lbu	$2,3($3)
	lbu	$7,2($3)
	sll	$2,$2,8
	or	$2,$2,$7
	move	$7,$2
	sw	$2,4136($20)
	lbu	$2,5($3)
	lbu	$3,4($3)
	sll	$2,$2,8
	or	$2,$2,$3
	sw	$2,4144($20)
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$2,16($sp)

	lw	$7,4136($20)
	teq	$19,$0,7
	div	$0,$7,$19
	mfhi	$19
	bne	$19,$0,$L9
	lw	$28,24($sp)

	slt	$2,$7,5001
	bne	$2,$0,$L10
	mflo	$3

$L9:
	lui	$6,%hi($LC4)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC4)
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$21

	.option	pic0
	j	$L6
	.option	pic2
	li	$19,-1			# 0xffffffffffffffff

$L10:
	li	$23,43376			# 0xa970
	lui	$16,%hi(ape_filter_orders)
	addu	$23,$20,$23
	move	$22,$0
	addiu	$16,$16,%lo(ape_filter_orders)
	li	$17,12			# 0xc
	addiu	$2,$3,-1
	li	$18,3			# 0x3
	sw	$2,4140($20)
$L13:
	lw	$2,4140($20)
	lw	$25,%call16(av_malloc)($28)
	sll	$3,$2,1
	addu	$3,$3,$2
	addu	$3,$3,$22
	addiu	$22,$22,1
	sll	$3,$3,1
	addu	$3,$3,$16
	lhu	$2,0($3)
	bne	$2,$0,$L11
	mul	$4,$2,$17

	lw	$25,%call16(dsputil_init)($28)
$L20:
	addiu	$4,$20,4
	.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
	move	$5,$21

	li	$2,1			# 0x1
	lw	$3,68($21)
	sw	$2,72($21)
	li	$2,2			# 0x2
	beq	$3,$2,$L15
	li	$2,4			# 0x4

	.option	pic0
	j	$L12
	.option	pic2
	move	$3,$0

$L11:
	addiu	$4,$4,2048
	.reloc	1f,R_MIPS_JALR,av_malloc
1:	jalr	$25
	addiu	$23,$23,4

	lw	$28,24($sp)
	bne	$22,$18,$L13
	sw	$2,-4($23)

	.option	pic0
	j	$L20
	.option	pic2
	lw	$25,%call16(dsputil_init)($28)

$L15:
	li	$2,3			# 0x3
	move	$3,$0
$L12:
	sw	$2,848($21)
	sw	$3,852($21)
$L6:
	lw	$31,68($sp)
$L19:
	move	$2,$19
	lw	$23,64($sp)
	lw	$22,60($sp)
	lw	$21,56($sp)
	lw	$20,52($sp)
	lw	$19,48($sp)
	lw	$18,44($sp)
	lw	$17,40($sp)
	lw	$16,36($sp)
	j	$31
	addiu	$sp,$sp,72

	.set	macro
	.set	reorder
	.end	ape_decode_init
	.size	ape_decode_init, .-ape_decode_init
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	do_apply_filter.isra.1
	.type	do_apply_filter.isra.1, @function
do_apply_filter.isra.1:
	.frame	$sp,88,$31		# vars= 24, regs= 10/0, args= 16, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-88
	lui	$28,%hi(__gnu_local_gp)
	sw	$19,60($sp)
	li	$14,1431633920			# 0x55550000
	sw	$21,68($sp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$23,76($sp)
	li	$10,32768			# 0x8000
	lw	$19,104($sp)
	addiu	$14,$14,21846
	lw	$23,108($sp)
	sw	$22,72($sp)
	li	$22,1			# 0x1
	addiu	$21,$19,256
	sw	$18,56($sp)
	sll	$12,$19,1
	sw	$fp,80($sp)
	sll	$21,$21,2
	sw	$20,64($sp)
	sra	$19,$19,1
	sw	$17,52($sp)
	addiu	$2,$23,-1
	sw	$16,48($sp)
	addiu	$11,$21,-1024
	sw	$31,84($sp)
	move	$18,$7
	.cprestore	16
	move	$16,$5
	move	$17,$6
	subu	$20,$0,$12
	addiu	$19,$19,-1
	sll	$22,$22,$2
	subu	$13,$0,$11
	li	$7,-65536			# 0xffffffffffff0000
	.set	noreorder
	.set	nomacro
	beq	$18,$0,$L55
	slt	$fp,$4,3980
	.set	macro
	.set	reorder

$L42:
	lw	$4,12($16)
	lw	$3,0($16)
	lw	$8,4($16)
	addu	$2,$4,$20
#APP
 # 668 "apedec.c" 1
	S32LDD xr1,$3,0
 # 0 "" 2
 # 669 "apedec.c" 1
	S16LDD xr2,$2,0,ptn0
 # 0 "" 2
 # 670 "apedec.c" 1
	S16LDD xr2,$2,2,ptn1
 # 0 "" 2
#NO_APP
	lw	$6,0($17)
	bne	$6,$0,$L23
#APP
 # 672 "apedec.c" 1
	D16MUL xr4,xr1,xr2,xr5,WW
 # 0 "" 2
#NO_APP
	beq	$19,$0,$L24
	move	$9,$19
$L25:
#APP
 # 674 "apedec.c" 1
	S32LDI xr1,$3,4
 # 0 "" 2
 # 675 "apedec.c" 1
	S16LDI xr2,$2,4,ptn0
 # 0 "" 2
 # 676 "apedec.c" 1
	S16LDD xr2,$2,2,ptn1
 # 0 "" 2
 # 677 "apedec.c" 1
	D16MAC xr4,xr1,xr2,xr5,AA,WW
 # 0 "" 2
#NO_APP
	addiu	$9,$9,-1
	bne	$9,$0,$L25
$L24:
#APP
 # 714 "apedec.c" 1
	D32ADD xr4,xr5,xr4,xr0,AA
 # 0 "" 2
 # 715 "apedec.c" 1
	S32M2I xr4, $2
 # 0 "" 2
#NO_APP
	addu	$2,$2,$22
	addiu	$5,$4,2
	sra	$2,$2,$23
	addiu	$17,$17,4
	addu	$2,$2,$6
	addu	$3,$2,$10
	sw	$2,-4($17)
	and	$3,$3,$7
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L31
	sw	$5,12($16)
	.set	macro
	.set	reorder

	sra	$3,$2,31
	xori	$3,$3,0x7fff
	sll	$3,$3,16
	sra	$3,$3,16
$L32:
	.set	noreorder
	.set	nomacro
	beq	$fp,$0,$L33
	sh	$3,0($4)
	.set	macro
	.set	reorder

	beq	$2,$0,$L43
	sra	$2,$2,28
	andi	$2,$2,0x8
	addiu	$2,$2,-4
	sll	$2,$2,16
	sra	$2,$2,16
$L34:
	lh	$4,-8($8)
	lh	$3,-16($8)
	sh	$2,0($8)
	sra	$4,$4,1
	sra	$2,$3,1
	sh	$4,-8($8)
	sh	$2,-16($8)
$L35:
	lw	$4,8($16)
	addiu	$8,$8,2
	addu	$2,$4,$21
	.set	noreorder
	.set	nomacro
	beq	$5,$2,$L56
	sw	$8,4($16)
	.set	macro
	.set	reorder

	addiu	$18,$18,-1
$L57:
	bne	$18,$0,$L42
$L55:
	lw	$31,84($sp)
	lw	$fp,80($sp)
	lw	$23,76($sp)
	lw	$22,72($sp)
	lw	$21,68($sp)
	lw	$20,64($sp)
	lw	$19,60($sp)
	lw	$18,56($sp)
	lw	$17,52($sp)
	lw	$16,48($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,88
	.set	macro
	.set	reorder

$L56:
	lw	$25,%call16(memmove)($28)
	addu	$5,$5,$13
	move	$6,$11
	sw	$7,40($sp)
	sw	$10,36($sp)
	addiu	$18,$18,-1
	sw	$11,28($sp)
	sw	$12,24($sp)
	sw	$13,32($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	sw	$14,44($sp)
	.set	macro
	.set	reorder

	lw	$11,28($sp)
	lw	$12,24($sp)
	lw	$2,8($16)
	lw	$28,16($sp)
	lw	$14,44($sp)
	addu	$3,$2,$11
	lw	$13,32($sp)
	addu	$2,$2,$12
	lw	$10,36($sp)
	lw	$7,40($sp)
	sw	$3,12($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L57
	.option	pic2
	sw	$2,4($16)
	.set	macro
	.set	reorder

$L33:
	subu	$4,$0,$2
	slt	$3,$2,0
	movz	$4,$2,$3
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L37
	lw	$6,16($16)
	.set	macro
	.set	reorder

	sll	$9,$6,2
	sll	$15,$6,1
	mult	$9,$14
	addu	$15,$15,$6
	sra	$9,$9,31
	slt	$24,$15,$4
	mfhi	$3
	li	$15,25			# 0x19
	li	$25,26			# 0x1a
	movz	$15,$25,$24
	subu	$9,$3,$9
	li	$3,-2147483648			# 0xffffffff80000000
	slt	$9,$9,$4
	and	$2,$2,$3
	li	$3,-1073741824			# 0xffffffffc0000000
	xori	$9,$9,0x1
	addu	$3,$2,$3
	addu	$2,$9,$15
	sra	$2,$3,$2
	sh	$2,0($8)
$L39:
	subu	$2,$4,$6
	lh	$9,-2($8)
	lh	$4,-4($8)
	addiu	$15,$2,15
	lh	$3,-16($8)
	slt	$24,$2,0
	movn	$2,$15,$24
	sra	$9,$9,1
	sra	$4,$4,1
	sra	$3,$3,1
	sra	$2,$2,4
	addu	$2,$2,$6
	sw	$2,16($16)
	sh	$9,-2($8)
	sh	$4,-4($8)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L35
	.option	pic2
	sh	$3,-16($8)
	.set	macro
	.set	reorder

$L31:
	sll	$3,$2,16
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L32
	.option	pic2
	sra	$3,$3,16
	.set	macro
	.set	reorder

$L23:
	.set	noreorder
	.set	nomacro
	bltz	$6,$L58
	addu	$8,$8,$20
	.set	macro
	.set	reorder

#APP
 # 697 "apedec.c" 1
	S16LDD xr3,$8,0,ptn0
 # 0 "" 2
 # 698 "apedec.c" 1
	S16LDD xr3,$8,2,ptn1
 # 0 "" 2
 # 699 "apedec.c" 1
	D16MUL xr4,xr1,xr2,xr5,WW
 # 0 "" 2
 # 700 "apedec.c" 1
	Q16ADD xr6,xr1,xr3,xr7,AS,WW
 # 0 "" 2
 # 701 "apedec.c" 1
	S32STD xr7,$3,0
 # 0 "" 2
#NO_APP
	.set	noreorder
	.set	nomacro
	beq	$19,$0,$L54
	move	$4,$19
	.set	macro
	.set	reorder

$L48:
#APP
 # 703 "apedec.c" 1
	S32LDI xr1,$3,4
 # 0 "" 2
 # 704 "apedec.c" 1
	S16LDI xr2,$2,4,ptn0
 # 0 "" 2
 # 705 "apedec.c" 1
	S16LDD xr2,$2,2,ptn1
 # 0 "" 2
 # 707 "apedec.c" 1
	S16LDI xr3,$8,4,ptn0
 # 0 "" 2
 # 708 "apedec.c" 1
	S16LDD xr3,$8,2,ptn1
 # 0 "" 2
 # 709 "apedec.c" 1
	D16MAC xr4,xr1,xr2,xr5,AA,WW
 # 0 "" 2
 # 710 "apedec.c" 1
	Q16ADD xr6,xr1,xr3,xr7,AS,WW
 # 0 "" 2
 # 711 "apedec.c" 1
	S32STD xr7,$3,0
 # 0 "" 2
#NO_APP
	addiu	$4,$4,-1
	bne	$4,$0,$L48
$L54:
	lw	$6,0($17)
	lw	$4,12($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L24
	.option	pic2
	lw	$8,4($16)
	.set	macro
	.set	reorder

$L43:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L34
	.option	pic2
	move	$2,$0
	.set	macro
	.set	reorder

$L37:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L39
	.option	pic2
	sh	$0,0($8)
	.set	macro
	.set	reorder

$L58:
#APP
 # 680 "apedec.c" 1
	S16LDD xr3,$8,0,ptn0
 # 0 "" 2
 # 681 "apedec.c" 1
	S16LDD xr3,$8,2,ptn1
 # 0 "" 2
 # 682 "apedec.c" 1
	D16MUL xr4,xr1,xr2,xr5,WW
 # 0 "" 2
 # 683 "apedec.c" 1
	Q16ADD xr6,xr1,xr3,xr7,AS,WW
 # 0 "" 2
 # 684 "apedec.c" 1
	S32STD xr6,$3,0
 # 0 "" 2
#NO_APP
	.set	noreorder
	.set	nomacro
	beq	$19,$0,$L54
	move	$4,$19
	.set	macro
	.set	reorder

$L47:
#APP
 # 686 "apedec.c" 1
	S32LDI xr1,$3,4
 # 0 "" 2
 # 687 "apedec.c" 1
	S16LDI xr2,$2,4,ptn0
 # 0 "" 2
 # 688 "apedec.c" 1
	S16LDD xr2,$2,2,ptn1
 # 0 "" 2
 # 690 "apedec.c" 1
	S16LDI xr3,$8,4,ptn0
 # 0 "" 2
 # 691 "apedec.c" 1
	S16LDD xr3,$8,2,ptn1
 # 0 "" 2
 # 692 "apedec.c" 1
	D16MAC xr4,xr1,xr2,xr5,AA,WW
 # 0 "" 2
 # 693 "apedec.c" 1
	Q16ADD xr6,xr1,xr3,xr7,AS,WW
 # 0 "" 2
 # 694 "apedec.c" 1
	S32STD xr6,$3,0
 # 0 "" 2
#NO_APP
	addiu	$4,$4,-1
	bne	$4,$0,$L47
	lw	$6,0($17)
	lw	$4,12($16)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L24
	.option	pic2
	lw	$8,4($16)
	.set	macro
	.set	reorder

	.end	do_apply_filter.isra.1
	.size	do_apply_filter.isra.1, .-do_apply_filter.isra.1
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	entropy_decode.constprop.3
	.type	entropy_decode.constprop.3, @function
entropy_decode.constprop.3:
	.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	lw	$2,4152($4)
	addiu	$sp,$sp,-64
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$17,28($sp)
	andi	$2,$2,0x3
	move	$17,$4
	sw	$18,32($sp)
	sw	$31,60($sp)
	move	$18,$5
	sw	$fp,56($sp)
	addiu	$25,$4,6512
	sw	$23,52($sp)
	sw	$22,48($sp)
	sw	$21,44($sp)
	sw	$20,40($sp)
	sw	$19,36($sp)
	sw	$16,24($sp)
	.cprestore	16
	bne	$2,$0,$L60
	sw	$5,4160($17)

	beq	$5,$0,$L62
	addiu	$16,$5,-1

	li	$18,65536			# 0x10000
	li	$24,-65536			# 0xffffffffffff0000
	li	$8,8388608			# 0x800000
	addu	$7,$4,$18
	addiu	$8,$8,1
	li	$21,65493			# 0xffd5
	addiu	$22,$24,64
	li	$20,63			# 0x3f
	li	$4,1			# 0x1
	li	$19,-1			# 0xffffffffffffffff
$L136:
	lw	$2,4132($17)
	slt	$2,$2,3990
	bne	$2,$0,$L155
	addiu	$25,$25,4

	lw	$12,-22120($7)
	lw	$5,-22144($7)
	srl	$11,$12,5
	sltu	$2,$5,$8
	beq	$2,$0,$L156
	movz	$11,$4,$11

	lw	$14,-21992($7)
	lw	$9,-22136($7)
	lw	$10,-21988($7)
	lw	$2,-22148($7)
$L94:
	sll	$9,$9,8
	sll	$5,$5,8
	sltu	$3,$10,$14
	sll	$2,$2,8
	sw	$9,-22136($7)
	beq	$3,$0,$L93
	sltu	$6,$5,$8

	lbu	$3,0($10)
	addu	$9,$9,$3
	sw	$9,-22136($7)
$L93:
	srl	$3,$9,1
	sw	$5,-22144($7)
	addiu	$10,$10,1
	andi	$3,$3,0xff
	or	$2,$3,$2
	sw	$10,-21988($7)
	bne	$6,$0,$L94
	sw	$2,-22148($7)

$L92:
	srl	$5,$5,16
	teq	$5,$0,7
	divu	$0,$2,$5
	mflo	$3
	sltu	$6,$3,$21
	beq	$6,$0,$L95
	sw	$5,-22140($7)

	sltu	$6,$3,19578
	bne	$6,$0,$L157
	lui	$10,%hi(counts_3980+4)

	move	$14,$0
	addiu	$9,$10,%lo(counts_3980+4)
$L99:
	lhu	$6,0($9)
	addiu	$14,$14,1
	slt	$6,$3,$6
	beq	$6,$0,$L99
	addiu	$9,$9,2

	lui	$9,%hi(counts_diff_3980)
	lui	$10,%hi(counts_3980)
	sll	$3,$14,1
	addiu	$9,$9,%lo(counts_diff_3980)
	addiu	$10,$10,%lo(counts_3980)
	addu	$6,$9,$3
	addu	$3,$10,$3
$L97:
	lhu	$3,0($3)
	lhu	$6,0($6)
	mul	$9,$3,$5
	mul	$5,$5,$6
	subu	$2,$2,$9
	sw	$5,-22144($7)
	beq	$14,$20,$L158
	sw	$2,-22148($7)

	slt	$3,$11,$18
$L168:
	bne	$3,$0,$L159
	sltu	$3,$5,$8

$L108:
	and	$3,$11,$24
	beq	$3,$0,$L160
	move	$3,$11

	move	$10,$0
$L115:
	sra	$3,$3,1
	and	$6,$3,$24
	bne	$6,$0,$L115
	addiu	$10,$10,1

	sll	$15,$4,$10
$L111:
	sltu	$6,$5,$8
	beq	$6,$0,$L116
	addiu	$3,$3,1

	lw	$fp,-21992($7)
	lw	$9,-22136($7)
	lw	$13,-21988($7)
$L118:
	sll	$9,$9,8
	sll	$5,$5,8
	sltu	$6,$13,$fp
	sll	$2,$2,8
	sw	$9,-22136($7)
	beq	$6,$0,$L117
	sltu	$23,$5,$8

	lbu	$6,0($13)
	addu	$9,$9,$6
	sw	$9,-22136($7)
$L117:
	srl	$6,$9,1
	sw	$5,-22144($7)
	addiu	$13,$13,1
	andi	$6,$6,0xff
	or	$2,$6,$2
	sw	$13,-21988($7)
	bne	$23,$0,$L118
	sw	$2,-22148($7)

$L116:
	teq	$3,$0,7
	divu	$0,$5,$3
	mflo	$3
	teq	$3,$0,7
	divu	$0,$2,$3
	sw	$3,-22140($7)
	sltu	$5,$3,$8
	sw	$3,-22144($7)
	mflo	$23
	mul	$6,$23,$3
	subu	$2,$2,$6
	beq	$5,$0,$L119
	sw	$2,-22148($7)

	lw	$5,-21992($7)
	lw	$6,-22136($7)
	lw	$9,-21988($7)
$L121:
	sll	$6,$6,8
	sll	$3,$3,8
	sltu	$13,$9,$5
	sll	$2,$2,8
	sw	$6,-22136($7)
	beq	$13,$0,$L120
	sltu	$fp,$3,$8

	lbu	$13,0($9)
	addu	$6,$6,$13
	sw	$6,-22136($7)
$L120:
	srl	$13,$6,1
	sw	$3,-22144($7)
	addiu	$9,$9,1
	andi	$13,$13,0xff
	or	$2,$13,$2
	sw	$9,-21988($7)
	bne	$fp,$0,$L121
	sw	$2,-22148($7)

$L119:
	teq	$15,$0,7
	divu	$0,$3,$15
	sll	$10,$23,$10
	mflo	$3
	teq	$3,$0,7
	divu	$0,$2,$3
	sw	$3,-22140($7)
	sw	$3,-22144($7)
	mflo	$6
	mul	$5,$6,$3
	addu	$6,$6,$10
	subu	$2,$2,$5
	sw	$2,-22148($7)
$L114:
	mul	$2,$11,$14
	lw	$5,-22124($7)
	addu	$11,$2,$6
$L89:
	addiu	$2,$11,1
	addiu	$6,$12,16
	srl	$3,$2,31
	srl	$6,$6,5
	beq	$5,$0,$L122
	addu	$2,$3,$2

	sra	$2,$2,1
	addiu	$3,$5,4
	subu	$2,$2,$6
	sll	$3,$4,$3
	addu	$12,$2,$12
	sltu	$2,$12,$3
	beq	$2,$0,$L123
	sw	$12,-22120($7)

	addiu	$5,$5,-1
	sw	$5,-22124($7)
$L124:
	andi	$2,$11,0x1
	bne	$2,$0,$L161
	sra	$11,$11,1

$L125:
	subu	$11,$0,$11
$L126:
	addiu	$16,$16,-1
	bne	$16,$19,$L136
	sw	$11,-4($25)

	lw	$2,4160($17)
$L62:
	lw	$3,4156($17)
	beq	$3,$2,$L167
	li	$2,65536			# 0x10000

$L59:
	lw	$31,60($sp)
$L169:
	lw	$fp,56($sp)
	lw	$23,52($sp)
	lw	$22,48($sp)
	lw	$21,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,64

$L122:
	sra	$2,$2,1
	subu	$2,$2,$6
	addu	$12,$2,$12
	sw	$12,-22120($7)
$L123:
	addiu	$2,$5,5
	srl	$12,$12,$2
	beq	$12,$0,$L124
	addiu	$5,$5,1

	andi	$2,$11,0x1
	sra	$11,$11,1
	beq	$2,$0,$L125
	sw	$5,-22124($7)

$L161:
	.option	pic0
	j	$L126
	.option	pic2
	addiu	$11,$11,1

$L95:
	mul	$6,$3,$5
	sw	$5,-22144($7)
	addu	$14,$3,$22
	subu	$2,$2,$6
	sltu	$6,$3,$18
	bne	$6,$0,$L98
	sw	$2,-22148($7)

	sw	$4,-21980($7)
$L98:
	bne	$14,$20,$L168
	slt	$3,$11,$18

$L101:
	lw	$6,-22136($7)
	lw	$9,-21988($7)
	lw	$10,-21992($7)
$L104:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$9,$10
	sw	$6,-22136($7)
	beq	$2,$0,$L103
	sltu	$13,$5,$8

	lbu	$2,0($9)
	addu	$6,$6,$2
	sw	$6,-22136($7)
$L103:
	srl	$2,$6,1
	sw	$5,-22144($7)
	addiu	$9,$9,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$9,-21988($7)
	bne	$13,$0,$L104
	sw	$2,-22148($7)

$L102:
	srl	$5,$5,16
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($7)
	sw	$5,-22144($7)
	mflo	$14
	mul	$3,$14,$5
	sll	$13,$14,16
	subu	$2,$2,$3
	sw	$2,-22148($7)
$L106:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$9,$10
	sw	$6,-22136($7)
	beq	$2,$0,$L105
	sltu	$14,$5,$8

	lbu	$2,0($9)
	addu	$6,$6,$2
	sw	$6,-22136($7)
$L105:
	srl	$2,$6,1
	sw	$5,-22144($7)
	addiu	$9,$9,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$9,-21988($7)
	bne	$14,$0,$L106
	sw	$2,-22148($7)

	srl	$5,$5,16
	li	$3,65536			# 0x10000
	teq	$5,$0,7
	divu	$0,$2,$5
	slt	$3,$11,$3
	sw	$5,-22140($7)
	sw	$5,-22144($7)
	mflo	$14
	mul	$6,$14,$5
	or	$14,$14,$13
	subu	$2,$2,$6
	beq	$3,$0,$L108
	sw	$2,-22148($7)

$L107:
	lw	$13,-21992($7)
	lw	$6,-22136($7)
	lw	$9,-21988($7)
$L113:
	sll	$6,$6,8
	sll	$5,$5,8
	sltu	$3,$9,$13
	sll	$2,$2,8
	sw	$6,-22136($7)
	beq	$3,$0,$L112
	sltu	$10,$5,$8

	lbu	$3,0($9)
	addu	$6,$6,$3
	sw	$6,-22136($7)
$L112:
	srl	$3,$6,1
	sw	$5,-22144($7)
	addiu	$9,$9,1
	andi	$3,$3,0xff
	or	$2,$3,$2
	sw	$9,-21988($7)
	bne	$10,$0,$L113
	sw	$2,-22148($7)

	teq	$11,$0,7
	divu	$0,$5,$11
	mflo	$5
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($7)
	sw	$5,-22144($7)
	mflo	$6
	mul	$3,$6,$5
	subu	$5,$2,$3
	.option	pic0
	j	$L114
	.option	pic2
	sw	$5,-22148($7)

$L155:
	lw	$9,-22144($7)
	sltu	$2,$9,$8
	beq	$2,$0,$L163
	nop

	lw	$5,-22136($7)
	lw	$6,-21988($7)
	lw	$11,-21992($7)
	lw	$2,-22148($7)
$L67:
	sll	$5,$5,8
	sll	$9,$9,8
	sltu	$3,$6,$11
	sll	$2,$2,8
	sw	$5,-22136($7)
	beq	$3,$0,$L66
	sltu	$10,$9,$8

	lbu	$3,0($6)
	addu	$5,$5,$3
	sw	$5,-22136($7)
$L66:
	srl	$3,$5,1
	sw	$9,-22144($7)
	addiu	$6,$6,1
	andi	$3,$3,0xff
	or	$2,$3,$2
	sw	$6,-21988($7)
	bne	$10,$0,$L67
	sw	$2,-22148($7)

$L65:
	srl	$9,$9,16
	teq	$9,$0,7
	divu	$0,$2,$9
	mflo	$10
	sltu	$3,$10,$21
	beq	$3,$0,$L68
	sw	$9,-22140($7)

	sltu	$3,$10,14824
	bne	$3,$0,$L164
	lui	$3,%hi(counts_3970)

	lui	$6,%hi(counts_3970+4)
	addiu	$5,$6,%lo(counts_3970+4)
	move	$6,$0
$L72:
	lhu	$3,0($5)
	addiu	$6,$6,1
	slt	$3,$10,$3
	beq	$3,$0,$L72
	addiu	$5,$5,2

	lui	$10,%hi(counts_diff_3970)
	sll	$3,$6,1
	addiu	$10,$10,%lo(counts_diff_3970)
	addu	$5,$10,$3
	lui	$10,%hi(counts_3970)
	addiu	$10,$10,%lo(counts_3970)
	addu	$3,$10,$3
$L70:
	lhu	$3,0($3)
	lhu	$5,0($5)
	mul	$10,$3,$9
	mul	$9,$9,$5
	subu	$2,$2,$10
	sw	$9,-22144($7)
	beq	$6,$20,$L165
	sw	$2,-22148($7)

$L73:
	lw	$5,-22124($7)
	beq	$5,$0,$L133
	move	$13,$0

	addiu	$13,$5,-1
	sll	$6,$6,$13
$L77:
	slt	$3,$13,17
	bne	$3,$0,$L78
	sltu	$3,$9,$8

	lw	$12,-21992($7)
	lw	$10,-22136($7)
	beq	$3,$0,$L80
	lw	$11,-21988($7)

$L86:
	sll	$10,$10,8
	sll	$9,$9,8
	sltu	$3,$11,$12
	sll	$2,$2,8
	sw	$10,-22136($7)
	beq	$3,$0,$L85
	sltu	$14,$9,$8

	lbu	$3,0($11)
	addu	$10,$10,$3
	sw	$10,-22136($7)
$L85:
	srl	$3,$10,1
	sw	$9,-22144($7)
	addiu	$11,$11,1
	andi	$3,$3,0xff
	or	$2,$3,$2
	sw	$11,-21988($7)
	bne	$14,$0,$L86
	sw	$2,-22148($7)

$L80:
	srl	$9,$9,16
	addiu	$13,$13,-16
	teq	$9,$0,7
	divu	$0,$2,$9
	sw	$9,-22140($7)
	sw	$9,-22144($7)
	mflo	$14
	mul	$3,$14,$9
	subu	$2,$2,$3
	sw	$2,-22148($7)
$L88:
	sll	$10,$10,8
	sll	$9,$9,8
	sltu	$3,$11,$12
	sll	$2,$2,8
	sw	$10,-22136($7)
	beq	$3,$0,$L87
	sltu	$15,$9,$8

	lbu	$3,0($11)
	addu	$10,$10,$3
	sw	$10,-22136($7)
$L87:
	srl	$3,$10,1
	sw	$9,-22144($7)
	addiu	$11,$11,1
	andi	$3,$3,0xff
	or	$2,$3,$2
	sw	$11,-21988($7)
	bne	$15,$0,$L88
	sw	$2,-22148($7)

	srl	$9,$9,$13
	lw	$12,-22120($7)
	teq	$9,$0,7
	divu	$0,$2,$9
	sw	$9,-22140($7)
	sw	$9,-22144($7)
	mflo	$11
	mul	$3,$11,$9
	sll	$11,$11,16
	or	$11,$11,$14
	addu	$11,$11,$6
	subu	$2,$2,$3
	.option	pic0
	j	$L89
	.option	pic2
	sw	$2,-22148($7)

$L159:
	bne	$3,$0,$L107
	nop

	teq	$11,$0,7
	divu	$0,$5,$11
	mflo	$5
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($7)
	sw	$5,-22144($7)
	mflo	$6
	mul	$3,$6,$5
	subu	$5,$2,$3
	.option	pic0
	j	$L114
	.option	pic2
	sw	$5,-22148($7)

$L133:
$L78:
	sltu	$3,$9,$8
	beq	$3,$0,$L81
	nop

	lw	$14,-21992($7)
	lw	$10,-22136($7)
	lw	$11,-21988($7)
$L83:
	sll	$10,$10,8
	sll	$9,$9,8
	sltu	$3,$11,$14
	sll	$2,$2,8
	sw	$10,-22136($7)
	beq	$3,$0,$L82
	sltu	$12,$9,$8

	lbu	$3,0($11)
	addu	$10,$10,$3
	sw	$10,-22136($7)
$L82:
	srl	$3,$10,1
	sw	$9,-22144($7)
	addiu	$11,$11,1
	andi	$3,$3,0xff
	or	$2,$3,$2
	sw	$11,-21988($7)
	bne	$12,$0,$L83
	sw	$2,-22148($7)

$L81:
	srl	$9,$9,$13
	lw	$12,-22120($7)
	teq	$9,$0,7
	divu	$0,$2,$9
	sw	$9,-22140($7)
	sw	$9,-22144($7)
	mflo	$11
	mul	$3,$11,$9
	addu	$11,$11,$6
	subu	$9,$2,$3
	.option	pic0
	j	$L89
	.option	pic2
	sw	$9,-22148($7)

$L68:
	mul	$3,$10,$9
	sw	$9,-22144($7)
	addu	$6,$10,$22
	subu	$2,$2,$3
	sltu	$3,$10,$18
	bne	$3,$0,$L71
	sw	$2,-22148($7)

	sw	$4,-21980($7)
$L71:
	bne	$6,$20,$L73
	nop

$L132:
	lw	$5,-22136($7)
	lw	$6,-21988($7)
	lw	$11,-21992($7)
$L76:
	sll	$5,$5,8
	sll	$9,$9,8
	sll	$3,$2,8
	sltu	$2,$6,$11
	sw	$5,-22136($7)
	beq	$2,$0,$L75
	sltu	$10,$9,$8

	lbu	$2,0($6)
	addu	$5,$5,$2
	sw	$5,-22136($7)
$L75:
	srl	$2,$5,1
	sw	$9,-22144($7)
	addiu	$6,$6,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$6,-21988($7)
	bne	$10,$0,$L76
	sw	$2,-22148($7)

	srl	$9,$9,5
$L166:
	lw	$5,-22124($7)
	move	$6,$0
	teq	$9,$0,7
	divu	$0,$2,$9
	sw	$9,-22140($7)
	sw	$9,-22144($7)
	mflo	$13
	mul	$3,$13,$9
	subu	$2,$2,$3
	.option	pic0
	j	$L77
	.option	pic2
	sw	$2,-22148($7)

$L158:
	sltu	$3,$5,$8
	bne	$3,$0,$L101
	nop

	lw	$10,-21992($7)
	lw	$6,-22136($7)
	.option	pic0
	j	$L102
	.option	pic2
	lw	$9,-21988($7)

$L156:
	.option	pic0
	j	$L92
	.option	pic2
	lw	$2,-22148($7)

$L160:
	li	$15,1			# 0x1
	.option	pic0
	j	$L111
	.option	pic2
	move	$10,$0

$L60:
	lw	$2,%call16(memset)($28)
	sll	$16,$5,2
	move	$5,$0
	move	$4,$25
	move	$25,$2
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$6,$16

	addiu	$4,$17,24944
	lw	$28,16($sp)
	move	$5,$0
	lw	$25,%call16(memset)($28)
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	move	$6,$16

	move	$2,$18
	lw	$3,4156($17)
	bne	$3,$2,$L169
	lw	$31,60($sp)

	li	$2,65536			# 0x10000
$L167:
	li	$9,8388608			# 0x800000
	addu	$17,$17,$2
	addiu	$9,$9,1
	lw	$7,-22144($17)
	sltu	$2,$7,$9
	beq	$2,$0,$L59
	move	$6,$17

	lw	$4,-22136($17)
	lw	$5,-21988($17)
	lw	$10,-21992($17)
	lw	$2,-22148($17)
$L131:
	sll	$4,$4,8
	sll	$7,$7,8
	sll	$3,$2,8
	sltu	$2,$5,$10
	sw	$4,-22136($6)
	beq	$2,$0,$L130
	sltu	$8,$7,$9

	lbu	$2,0($5)
	addu	$4,$4,$2
	sw	$4,-22136($6)
$L130:
	srl	$2,$4,1
	sw	$7,-22144($6)
	addiu	$5,$5,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$5,-21988($6)
	bne	$8,$0,$L131
	sw	$2,-22148($6)

	lw	$31,60($sp)
	lw	$fp,56($sp)
	lw	$23,52($sp)
	lw	$22,48($sp)
	lw	$21,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,64

$L165:
	sltu	$3,$9,$8
	bne	$3,$0,$L132
	nop

	.option	pic0
	j	$L166
	.option	pic2
	srl	$9,$9,5

$L157:
	lui	$3,%hi(counts_3980)
	lui	$6,%hi(counts_diff_3980)
	addiu	$3,$3,%lo(counts_3980)
	addiu	$6,$6,%lo(counts_diff_3980)
	.option	pic0
	j	$L97
	.option	pic2
	move	$14,$0

$L163:
	.option	pic0
	j	$L65
	.option	pic2
	lw	$2,-22148($7)

$L164:
	lui	$5,%hi(counts_diff_3970)
	addiu	$3,$3,%lo(counts_3970)
	addiu	$5,$5,%lo(counts_diff_3970)
	.option	pic0
	j	$L70
	.option	pic2
	move	$6,$0

	.set	macro
	.set	reorder
	.end	entropy_decode.constprop.3
	.size	entropy_decode.constprop.3, .-entropy_decode.constprop.3
	.section	.rodata.str1.4
	.align	2
$LC5:
	.ascii	"Packet size is too big to be handled in lavc! (max is %d"
	.ascii	" where you have %d)\012\000"
	.align	2
$LC6:
	.ascii	"Incorrect offset passed\012\000"
	.align	2
$LC7:
	.ascii	"pure silence mono\012\000"
	.align	2
$LC8:
	.ascii	"pure silence stereo\012\000"
	.align	2
$LC9:
	.ascii	"Error decoding frame\012\000"
	.text
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	ape_decode_frame
	.type	ape_decode_frame, @function
ape_decode_frame:
	.frame	$sp,96,$31		# vars= 24, regs= 10/0, args= 24, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-96
	lui	$28,%hi(__gnu_local_gp)
	sw	$16,56($sp)
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$22,80($sp)
	sw	$20,72($sp)
	sw	$19,68($sp)
	move	$19,$5
	sw	$17,60($sp)
	move	$17,$4
	sw	$31,92($sp)
	sw	$fp,88($sp)
	sw	$23,84($sp)
	sw	$21,76($sp)
	sw	$18,64($sp)
	.cprestore	24
	sw	$6,104($sp)
	lw	$16,20($7)
	lw	$22,16($7)
	.set	noreorder
	.set	nomacro
	bne	$16,$0,$L171
	lw	$20,136($4)
	.set	macro
	.set	reorder

	lw	$2,4128($20)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L418
	lw	$8,104($sp)
	.set	macro
	.set	reorder

	lw	$3,68($4)
	sll	$4,$3,10
	sll	$5,$3,13
	lw	$7,0($8)
	addu	$4,$4,$5
	slt	$4,$7,$4
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L458
	li	$10,65536			# 0x10000
	.set	macro
	.set	reorder

$L347:
	sll	$2,$2,1
	lw	$25,%call16(av_log)($28)
	lui	$6,%hi($LC5)
	mul	$3,$3,$2
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC5)
	move	$4,$17
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	sw	$3,16($sp)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L171:
	lw	$3,68($4)
	lw	$7,0($6)
	sll	$2,$3,10
	sll	$4,$3,13
	addu	$2,$2,$4
	slt	$2,$7,$2
	bne	$2,$0,$L419
	lw	$2,4128($20)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L420
	li	$10,65536			# 0x10000
	.set	macro
	.set	reorder

$L458:
	addu	$10,$20,$10
	lw	$2,-21996($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L459
	lw	$12,104($sp)
	.set	macro
	.set	reorder

$L409:
	lw	$2,4128($20)
	li	$4,4608			# 0x1200
	lw	$3,4124($20)
	move	$18,$4
	slt	$11,$2,4609
	movn	$18,$2,$11
	li	$2,1			# 0x1
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L422
	sw	$0,-21980($10)
	.set	macro
	.set	reorder

	lw	$2,4152($20)
	andi	$3,$2,0x4
	beq	$3,$0,$L185
$L184:
	andi	$2,$2,0x3
	move	$4,$20
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L423
	move	$5,$18
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	entropy_decode.constprop.3
	.option	pic2
	move	$fp,$0
	.set	macro
	.set	reorder

	li	$9,43420			# 0xa99c
	addiu	$25,$20,6512
	lw	$28,24($sp)
	sw	$16,36($sp)
	lui	$21,%hi(ape_filter_orders)
	lui	$22,%hi(ape_filter_fracbits)
	sw	$25,32($sp)
	addu	$9,$20,$9
	move	$2,$fp
	addiu	$21,$21,%lo(ape_filter_orders)
	addiu	$22,$22,%lo(ape_filter_fracbits)
	move	$fp,$20
	move	$23,$25
	move	$16,$9
	move	$20,$2
$L190:
	lw	$3,4140($fp)
	move	$5,$16
	lw	$6,32($sp)
	move	$7,$18
	addiu	$16,$16,40
	sll	$2,$3,1
	addu	$2,$2,$3
	addu	$2,$2,$20
	addiu	$20,$20,1
	sll	$3,$2,1
	addu	$3,$21,$3
	lhu	$3,0($3)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L413
	addu	$2,$2,$22
	.set	macro
	.set	reorder

	lbu	$2,0($2)
	lw	$4,4132($fp)
	sw	$3,16($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	do_apply_filter.isra.1
	.option	pic2
	sw	$2,20($sp)
	.set	macro
	.set	reorder

	li	$2,3			# 0x3
	.set	noreorder
	.set	nomacro
	bne	$20,$2,$L190
	lw	$28,24($sp)
	.set	macro
	.set	reorder

$L413:
	lw	$16,36($sp)
	move	$20,$fp
	.set	noreorder
	.set	nomacro
	beq	$18,$0,$L187
	lw	$2,4168($fp)
	.set	macro
	.set	reorder

	lw	$14,4164($fp)
	addiu	$21,$fp,6312
	addiu	$24,$fp,4264
	addiu	$22,$fp,6504
	move	$15,$18
	move	$8,$2
$L194:
	lw	$3,196($14)
	srl	$5,$8,31
	lw	$9,0($23)
	slt	$2,$0,$8
	lw	$10,192($14)
	subu	$5,$5,$2
	subu	$3,$8,$3
	lw	$6,188($14)
	sw	$8,200($14)
	srl	$7,$9,31
	srl	$4,$3,31
	sw	$3,196($14)
	slt	$12,$0,$3
	lw	$25,4196($20)
	subu	$12,$4,$12
	lw	$11,4192($20)
	slt	$2,$0,$9
	lw	$13,4200($20)
	subu	$7,$7,$2
	lw	$4,4204($20)
	mult	$3,$25
	madd	$8,$11
	sw	$12,68($14)
	madd	$10,$13
	sw	$5,72($14)
	madd	$6,$4
	lw	$2,4192($20)
	addiu	$14,$14,4
	lw	$8,4196($20)
	lw	$6,4200($20)
	mflo	$3
	mul	$4,$5,$7
	sra	$3,$3,10
	addu	$5,$4,$2
	lw	$2,4204($20)
	addu	$4,$9,$3
	sw	$5,4192($20)
	lw	$5,64($14)
	mul	$3,$7,$5
	addu	$5,$3,$8
	sw	$5,4196($20)
	lw	$3,60($14)
	mul	$5,$7,$3
	addu	$3,$5,$6
	sw	$3,4200($20)
	lw	$3,56($14)
	sw	$14,4164($20)
	mul	$5,$7,$3
	addu	$2,$5,$2
	.set	noreorder
	.set	nomacro
	beq	$14,$21,$L424
	sw	$2,4204($20)
	.set	macro
	.set	reorder

$L192:
	lw	$2,4176($20)
	addiu	$23,$23,4
	addiu	$15,$15,-1
	move	$8,$4
	sll	$3,$2,5
	subu	$3,$3,$2
	sra	$3,$3,5
	addu	$3,$4,$3
	sw	$3,4176($20)
	.set	noreorder
	.set	nomacro
	bne	$15,$0,$L194
	sw	$3,-4($23)
	.set	macro
	.set	reorder

	lw	$3,4124($20)
	li	$2,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L425
	sw	$4,4168($20)
	.set	macro
	.set	reorder

$L187:
	li	$2,65536			# 0x10000
$L454:
	addu	$2,$20,$2
	lw	$3,-21980($2)
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L460
	lui	$6,%hi($LC9)
	.set	macro
	.set	reorder

	lw	$6,-21988($2)
	lw	$2,-21992($2)
	sltu	$2,$2,$6
	bne	$2,$0,$L339
	.set	noreorder
	.set	nomacro
	blez	$18,$L415
	lw	$7,4124($20)
	.set	macro
	.set	reorder

	li	$2,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$7,$2,$L342
	sll	$5,$18,1
	.set	macro
	.set	reorder

	addiu	$2,$20,6512
	addu	$4,$19,$5
$L343:
	lw	$3,0($2)
	addiu	$19,$19,2
	addiu	$2,$2,4
	.set	noreorder
	.set	nomacro
	bne	$19,$4,$L343
	sh	$3,-2($19)
	.set	macro
	.set	reorder

$L341:
	mul	$5,$5,$7
	lw	$2,4128($20)
	lw	$3,104($sp)
	subu	$11,$2,$18
	sw	$11,4128($20)
	sw	$5,0($3)
	lw	$2,4128($20)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L461
	li	$3,65536			# 0x10000
	.set	macro
	.set	reorder

	li	$2,65536			# 0x10000
	addu	$2,$20,$2
	lw	$16,-21984($2)
	subu	$16,$6,$16
$L461:
	lw	$31,92($sp)
	move	$2,$16
	lw	$fp,88($sp)
	addu	$20,$20,$3
	lw	$23,84($sp)
	lw	$22,80($sp)
	lw	$21,76($sp)
	lw	$19,68($sp)
	lw	$18,64($sp)
	lw	$17,60($sp)
	lw	$16,56($sp)
	sw	$6,-21984($20)
	lw	$20,72($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,96
	.set	macro
	.set	reorder

$L418:
	sw	$0,0($6)
$L408:
	lw	$31,92($sp)
	lw	$fp,88($sp)
	lw	$23,84($sp)
	lw	$22,80($sp)
	lw	$21,76($sp)
	lw	$20,72($sp)
	lw	$19,68($sp)
	lw	$18,64($sp)
	lw	$17,60($sp)
	lw	$16,56($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,96
	.set	macro
	.set	reorder

$L420:
	li	$21,65536			# 0x10000
	lw	$25,%call16(av_realloc)($28)
	addiu	$2,$16,3
	addu	$21,$20,$21
	li	$5,-4			# 0xfffffffffffffffc
	li	$23,16711680			# 0xff0000
	lw	$4,-21996($21)
	and	$5,$2,$5
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_realloc
1:	jalr	$25
	addiu	$23,$23,255
	.set	macro
	.set	reorder

	sra	$6,$16,2
	lw	$25,2580($20)
	move	$4,$2
	move	$5,$22
	sw	$2,-21996($21)
	.set	noreorder
	.set	nomacro
	jalr	$25
	li	$22,-16777216			# 0xffffffffff000000
	.set	macro
	.set	reorder

	lw	$4,-21996($21)
	ori	$22,$22,0xff00
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($4)  
	lwr $3, 0($4)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	lw	$28,24($sp)
	sll	$3,$3,8
	and	$2,$2,$23
	sw	$4,-21984($21)
	and	$3,$3,$22
	or	$2,$2,$3
	sll	$5,$2,16
	srl	$2,$2,16
	addu	$3,$4,$16
	or	$2,$2,$5
	addiu	$fp,$4,8
	sw	$3,-21992($21)
	sw	$2,4128($20)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 7($4)  
	lwr $3, 4($4)  
	
 # 0 "" 2
#NO_APP
	move	$4,$3
	srl	$3,$3,8
	sll	$4,$4,8
	and	$3,$3,$23
	and	$4,$4,$22
	or	$3,$3,$4
	sll	$4,$3,16
	srl	$3,$3,16
	or	$3,$3,$4
	sltu	$4,$3,4
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L426
	sw	$fp,-21988($21)
	.set	macro
	.set	reorder

	addu	$fp,$fp,$3
	sw	$fp,-21988($21)
	.set	noreorder
	.set	nomacro
	blez	$2,$L427
	sw	$2,4156($20)
	.set	macro
	.set	reorder

	lw	$25,%call16(memset)($28)
	addiu	$4,$20,6512
	move	$5,$0
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	li	$6,18432			# 0x4800
	.set	macro
	.set	reorder

	addiu	$4,$20,24944
	lw	$28,24($sp)
	move	$5,$0
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	li	$6,18432			# 0x4800
	.set	macro
	.set	reorder

	addiu	$8,$fp,4
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 3($fp)  
	lwr $3, 0($fp)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	lw	$4,4132($20)
	sll	$3,$3,8
	lw	$28,24($sp)
	and	$2,$2,$23
	sw	$8,-21988($21)
	and	$3,$3,$22
	sw	$0,4152($20)
	or	$2,$2,$3
	sll	$5,$2,16
	srl	$2,$2,16
	slt	$3,$4,3821
	or	$2,$2,$5
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L180
	sw	$2,4148($20)
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bgez	$2,$L180
	li	$3,2147418112			# 0x7fff0000
	.set	macro
	.set	reorder

	addiu	$4,$fp,8
	ori	$3,$3,0xffff
	and	$2,$2,$3
	move	$8,$4
	sw	$2,4148($20)
#APP
 # 31 "../libavutil/mips/intreadwrite.h" 1
	lwl $3, 7($fp)  
	lwr $3, 4($fp)  
	
 # 0 "" 2
#NO_APP
	srl	$2,$3,8
	sw	$4,-21988($21)
	sll	$3,$3,8
	and	$23,$2,$23
	and	$2,$3,$22
	or	$2,$23,$2
	sll	$3,$2,16
	srl	$2,$2,16
	or	$2,$3,$2
	sw	$2,4152($20)
$L180:
	li	$2,65536			# 0x10000
	sw	$0,4160($20)
	li	$4,10			# 0xa
	lw	$25,%call16(memset)($28)
	li	$3,16384			# 0x4000
	addiu	$7,$8,2
	addu	$2,$20,$2
	addiu	$21,$20,4264
	sw	$4,-22132($2)
	move	$5,$0
	sw	$4,-22124($2)
	li	$6,200			# 0xc8
	sw	$7,-21988($2)
	move	$7,$0
	sw	$3,-22128($2)
	move	$4,$21
	sw	$3,-22120($2)
	lbu	$3,1($8)
	li	$8,128			# 0x80
	sw	$8,-22144($2)
	srl	$8,$3,1
	sw	$3,-22136($2)
	sw	$8,-22148($2)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$7,48($sp)
	.set	macro
	.set	reorder

	lui	$2,%hi(initial_coeffs)
	addiu	$3,$20,4192
	lw	$28,24($sp)
	addiu	$8,$2,%lo(initial_coeffs)
	sw	$21,4164($20)
	lw	$11,%lo(initial_coeffs)($2)
	addiu	$2,$20,4208
	addiu	$4,$20,4224
	lw	$10,4($8)
	move	$5,$0
	lw	$9,8($8)
	li	$6,40			# 0x28
	lw	$8,12($8)
	lui	$21,%hi(ape_filter_orders)
	swl	$11,3($3)
	swr	$11,0($3)
	addiu	$21,$21,%lo(ape_filter_orders)
	swl	$10,7($3)
	swr	$10,4($3)
	swl	$9,11($3)
	swr	$9,8($3)
	swl	$8,15($3)
	swr	$8,12($3)
	swl	$11,3($2)
	swr	$11,0($2)
	swl	$10,7($2)
	swr	$10,4($2)
	swl	$9,11($2)
	swr	$9,8($2)
	swl	$8,15($2)
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	swr	$8,12($2)
	.set	macro
	.set	reorder

	li	$8,43376			# 0xa970
	li	$3,43420			# 0xa99c
	lw	$7,48($sp)
	addu	$8,$20,$8
	lw	$28,24($sp)
	addu	$18,$20,$3
	sw	$16,32($sp)
	sw	$17,36($sp)
	move	$16,$20
	sw	$19,40($sp)
	move	$17,$7
	sw	$0,4180($20)
	move	$19,$8
	sw	$0,4176($20)
	sw	$0,4188($20)
	sw	$0,4184($20)
	sw	$0,4172($20)
	sw	$0,4168($20)
$L182:
	lw	$4,4140($16)
	move	$5,$0
	lw	$25,%call16(memset)($28)
	sll	$2,$4,1
	addu	$2,$2,$4
	addu	$2,$2,$17
	addiu	$17,$17,1
	sll	$2,$2,1
	addu	$2,$21,$2
	lhu	$22,0($2)
	.set	noreorder
	.set	nomacro
	beq	$22,$0,$L411
	sll	$23,$22,1
	.set	macro
	.set	reorder

	lw	$20,0($19)
	sll	$fp,$22,2
	addiu	$18,$18,40
	move	$6,$fp
	addu	$4,$20,$23
	sw	$20,-40($18)
	addiu	$19,$19,4
	addu	$9,$4,$fp
	addu	$2,$4,$23
	sw	$4,-32($18)
	sw	$9,-28($18)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$2,-36($18)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,24($sp)
	move	$6,$23
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	lw	$4,-40($18)
	.set	macro
	.set	reorder

	sll	$2,$22,3
	lw	$28,24($sp)
	move	$6,$fp
	subu	$2,$2,$23
	sw	$0,-24($18)
	move	$5,$0
	addiu	$2,$2,1024
	lw	$25,%call16(memset)($28)
	addu	$2,$20,$2
	addu	$4,$2,$23
	sw	$2,-20($18)
	addu	$fp,$4,$fp
	addu	$2,$4,$23
	sw	$4,-12($18)
	sw	$fp,-8($18)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sw	$2,-16($18)
	.set	macro
	.set	reorder

	move	$5,$0
	lw	$28,24($sp)
	move	$6,$23
	lw	$25,%call16(memset)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	lw	$4,-20($18)
	.set	macro
	.set	reorder

	li	$10,3			# 0x3
	lw	$28,24($sp)
	.set	noreorder
	.set	nomacro
	bne	$17,$10,$L182
	sw	$0,-4($18)
	.set	macro
	.set	reorder

$L411:
	li	$10,65536			# 0x10000
	lw	$17,36($sp)
	move	$20,$16
	lw	$19,40($sp)
	addu	$10,$20,$10
	lw	$2,-21996($10)
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L409
	lw	$16,32($sp)
	.set	macro
	.set	reorder

	lw	$12,104($sp)
$L459:
	move	$2,$16
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	sw	$0,0($12)
	.set	macro
	.set	reorder

$L424:
	move	$3,$21
	move	$2,$24
$L193:
	lwl	$8,3($3)
	lwl	$7,7($3)
	lwl	$6,11($3)
	lwl	$5,15($3)
	lwr	$8,0($3)
	lwr	$7,4($3)
	lwr	$6,8($3)
	lwr	$5,12($3)
	addiu	$3,$3,16
	swl	$8,3($2)
	swr	$8,0($2)
	swl	$7,7($2)
	swr	$7,4($2)
	swl	$6,11($2)
	swr	$6,8($2)
	swl	$5,15($2)
	swr	$5,12($2)
	.set	noreorder
	.set	nomacro
	bne	$3,$22,$L193
	addiu	$2,$2,16
	.set	macro
	.set	reorder

	lwl	$5,3($22)
	move	$14,$24
	lwl	$3,7($22)
	lwr	$5,0($22)
	lwr	$3,4($22)
	swl	$5,3($2)
	swr	$5,0($2)
	swl	$3,7($2)
	swr	$3,4($2)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L192
	.option	pic2
	sw	$24,4164($20)
	.set	macro
	.set	reorder

$L185:
	andi	$2,$2,0x3
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L428
	addiu	$fp,$20,6512
	.set	macro
	.set	reorder

	sw	$18,4160($20)
	addiu	$12,$20,24944
	.set	noreorder
	.set	nomacro
	beq	$18,$0,$L197
	addiu	$13,$18,-1
	.set	macro
	.set	reorder

	li	$4,8388608			# 0x800000
	sw	$18,36($sp)
	li	$22,-65536			# 0xffffffffffff0000
	sw	$16,32($sp)
	li	$11,1			# 0x1
	sw	$fp,40($sp)
	addiu	$4,$4,1
	move	$23,$13
	move	$21,$12
	move	$18,$fp
$L326:
	lw	$2,4132($20)
	slt	$2,$2,3990
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L429
	addiu	$18,$18,4
	.set	macro
	.set	reorder

	lw	$16,-22120($10)
	lw	$3,-22144($10)
	srl	$15,$16,5
	sltu	$2,$3,$4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L430
	movz	$15,$11,$15
	.set	macro
	.set	reorder

	lw	$9,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
	lw	$2,-22148($10)
$L229:
	sll	$6,$6,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L228
	sltu	$8,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L228:
	srl	$2,$6,1
	sw	$3,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L229
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L227:
	srl	$3,$3,16
	li	$6,65493			# 0xffd5
	teq	$3,$0,7
	divu	$0,$2,$3
	mflo	$7
	sltu	$5,$7,$6
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L230
	sw	$3,-22140($10)
	.set	macro
	.set	reorder

	sltu	$5,$7,19578
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L431
	lui	$25,%hi(counts_3980+4)
	.set	macro
	.set	reorder

	move	$14,$0
	addiu	$6,$25,%lo(counts_3980+4)
$L234:
	lhu	$5,0($6)
	addiu	$14,$14,1
	slt	$5,$7,$5
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L234
	addiu	$6,$6,2
	.set	macro
	.set	reorder

	lui	$7,%hi(counts_diff_3980)
	lui	$8,%hi(counts_3980)
	sll	$5,$14,1
	addiu	$7,$7,%lo(counts_diff_3980)
	addiu	$8,$8,%lo(counts_3980)
	addu	$6,$7,$5
	addu	$5,$8,$5
$L232:
	lhu	$5,0($5)
	lhu	$6,0($6)
	mul	$7,$5,$3
	mul	$3,$3,$6
	li	$5,63			# 0x3f
	subu	$2,$2,$7
	sw	$3,-22144($10)
	.set	noreorder
	.set	nomacro
	beq	$14,$5,$L432
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	li	$7,65536			# 0x10000
$L462:
	slt	$5,$15,$7
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L433
	sltu	$5,$3,$4
	.set	macro
	.set	reorder

$L243:
	and	$5,$15,$22
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L434
	move	$9,$15
	.set	macro
	.set	reorder

	move	$8,$0
$L250:
	sra	$9,$9,1
	and	$5,$9,$22
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L250
	addiu	$8,$8,1
	.set	macro
	.set	reorder

	sll	$24,$11,$8
$L246:
	sltu	$5,$3,$4
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L251
	addiu	$9,$9,1
	.set	macro
	.set	reorder

	lw	$fp,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L253:
	sll	$6,$6,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$7,$fp
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L252
	sltu	$25,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L252:
	srl	$2,$6,1
	sw	$3,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$25,$0,$L253
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L251:
	teq	$9,$0,7
	divu	$0,$3,$9
	mflo	$3
	teq	$3,$0,7
	divu	$0,$2,$3
	sw	$3,-22140($10)
	sltu	$5,$3,$4
	sw	$3,-22144($10)
	mflo	$25
	mul	$6,$25,$3
	subu	$2,$2,$6
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L254
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	lw	$fp,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L256:
	sll	$6,$6,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$7,$fp
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L255
	sltu	$9,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L255:
	srl	$2,$6,1
	sw	$3,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L256
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L254:
	teq	$24,$0,7
	divu	$0,$3,$24
	sll	$8,$25,$8
	mflo	$3
	teq	$3,$0,7
	divu	$0,$2,$3
	sw	$3,-22140($10)
	sw	$3,-22144($10)
	mflo	$9
	mul	$5,$9,$3
	addu	$9,$9,$8
	subu	$2,$2,$5
	sw	$2,-22148($10)
$L249:
	mul	$2,$15,$14
	lw	$8,-22124($10)
	addu	$9,$2,$9
$L224:
	addiu	$2,$9,1
	addiu	$5,$16,16
	srl	$3,$2,31
	srl	$5,$5,5
	.set	noreorder
	.set	nomacro
	beq	$8,$0,$L257
	addu	$2,$3,$2
	.set	macro
	.set	reorder

	sra	$2,$2,1
	addiu	$3,$8,4
	subu	$2,$2,$5
	sll	$3,$11,$3
	addu	$16,$2,$16
	sltu	$2,$16,$3
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L258
	sw	$16,-22120($10)
	.set	macro
	.set	reorder

	addiu	$8,$8,-1
	sw	$8,-22124($10)
$L259:
	andi	$2,$9,0x1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L435
	sra	$9,$9,1
	.set	macro
	.set	reorder

$L260:
	subu	$2,$0,$9
	sw	$2,-4($18)
	lw	$2,4132($20)
	slt	$2,$2,3990
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L436
	addiu	$21,$21,4
	.set	macro
	.set	reorder

$L262:
	lw	$16,-22128($10)
	lw	$3,-22144($10)
	srl	$15,$16,5
	sltu	$2,$3,$4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L437
	movz	$15,$11,$15
	.set	macro
	.set	reorder

	lw	$9,-21992($10)
	lw	$7,-22136($10)
	lw	$8,-21988($10)
	lw	$2,-22148($10)
$L293:
	sll	$7,$7,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$8,$9
	sw	$7,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L292
	sltu	$6,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($8)
	addu	$7,$7,$2
	sw	$7,-22136($10)
$L292:
	srl	$2,$7,1
	sw	$3,-22144($10)
	addiu	$8,$8,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$8,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L293
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L291:
	srl	$3,$3,16
	li	$6,65493			# 0xffd5
	teq	$3,$0,7
	divu	$0,$2,$3
	mflo	$7
	sltu	$5,$7,$6
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L294
	sw	$3,-22140($10)
	.set	macro
	.set	reorder

	sltu	$5,$7,19578
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L438
	lui	$25,%hi(counts_3980+4)
	.set	macro
	.set	reorder

	move	$14,$0
	addiu	$6,$25,%lo(counts_3980+4)
$L298:
	lhu	$5,0($6)
	addiu	$14,$14,1
	slt	$5,$7,$5
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L298
	addiu	$6,$6,2
	.set	macro
	.set	reorder

	lui	$7,%hi(counts_diff_3980)
	lui	$8,%hi(counts_3980)
	sll	$5,$14,1
	addiu	$7,$7,%lo(counts_diff_3980)
	addiu	$8,$8,%lo(counts_3980)
	addu	$6,$7,$5
	addu	$5,$8,$5
$L296:
	lhu	$5,0($5)
	lhu	$6,0($6)
	mul	$7,$5,$3
	mul	$3,$3,$6
	li	$5,63			# 0x3f
	subu	$2,$2,$7
	sw	$3,-22144($10)
	.set	noreorder
	.set	nomacro
	beq	$14,$5,$L439
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	li	$7,65536			# 0x10000
$L463:
	slt	$5,$15,$7
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L440
	sltu	$5,$3,$4
	.set	macro
	.set	reorder

$L307:
	and	$5,$15,$22
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L441
	move	$5,$15
	.set	macro
	.set	reorder

	move	$8,$0
$L314:
	sra	$5,$5,1
	and	$6,$5,$22
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L314
	addiu	$8,$8,1
	.set	macro
	.set	reorder

	sll	$25,$11,$8
$L310:
	sltu	$6,$3,$4
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L315
	addiu	$5,$5,1
	.set	macro
	.set	reorder

	lw	$fp,-21992($10)
	lw	$7,-22136($10)
	lw	$9,-21988($10)
$L317:
	sll	$7,$7,8
	sll	$3,$3,8
	sll	$6,$2,8
	sltu	$2,$9,$fp
	sw	$7,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L316
	sltu	$24,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($9)
	addu	$7,$7,$2
	sw	$7,-22136($10)
$L316:
	srl	$2,$7,1
	sw	$3,-22144($10)
	addiu	$9,$9,1
	andi	$2,$2,0xff
	or	$2,$2,$6
	sw	$9,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$24,$0,$L317
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L315:
	teq	$5,$0,7
	divu	$0,$3,$5
	mflo	$5
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sltu	$3,$5,$4
	sw	$5,-22144($10)
	mflo	$24
	mul	$6,$24,$5
	subu	$2,$2,$6
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L318
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	lw	$fp,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L320:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$fp
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L319
	sltu	$9,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L319:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L320
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L318:
	teq	$25,$0,7
	divu	$0,$5,$25
	sll	$8,$24,$8
	mflo	$5
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$9
	mul	$3,$9,$5
	addu	$9,$9,$8
	subu	$2,$2,$3
	sw	$2,-22148($10)
$L313:
	mul	$2,$15,$14
	lw	$8,-22132($10)
	addiu	$5,$16,16
	srl	$5,$5,5
	addu	$9,$2,$9
	addiu	$2,$9,1
	srl	$3,$2,31
	.set	noreorder
	.set	nomacro
	beq	$8,$0,$L321
	addu	$2,$3,$2
	.set	macro
	.set	reorder

$L449:
	sra	$2,$2,1
	addiu	$3,$8,4
	subu	$2,$2,$5
	sll	$3,$11,$3
	addu	$16,$2,$16
	sltu	$2,$16,$3
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L322
	sw	$16,-22128($10)
	.set	macro
	.set	reorder

	addiu	$8,$8,-1
	sw	$8,-22132($10)
$L323:
	andi	$2,$9,0x1
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L442
	sra	$9,$9,1
	.set	macro
	.set	reorder

$L324:
	subu	$2,$0,$9
$L325:
	addiu	$23,$23,-1
	li	$3,-1			# 0xffffffffffffffff
	.set	noreorder
	.set	nomacro
	bne	$23,$3,$L326
	sw	$2,-4($21)
	.set	macro
	.set	reorder

	lw	$16,32($sp)
	lw	$18,36($sp)
	lw	$fp,40($sp)
	lw	$2,4160($20)
$L197:
	lw	$3,4156($20)
	.set	noreorder
	.set	nomacro
	beq	$3,$2,$L327
	li	$2,65536			# 0x10000
	.set	macro
	.set	reorder

$L329:
	li	$8,43420			# 0xa99c
$L457:
	sw	$16,32($sp)
	lui	$21,%hi(ape_filter_orders)
	sw	$17,40($sp)
	addu	$22,$20,$8
	sw	$19,44($sp)
	move	$23,$0
	sw	$13,36($sp)
	addiu	$21,$21,%lo(ape_filter_orders)
	move	$16,$12
	move	$17,$20
	move	$19,$22
$L328:
	lw	$4,4140($17)
	lui	$8,%hi(ape_filter_fracbits)
	move	$5,$19
	addiu	$8,$8,%lo(ape_filter_fracbits)
	sll	$2,$4,1
	move	$6,$fp
	addu	$2,$2,$4
	move	$7,$18
	addu	$2,$2,$23
	addu	$4,$2,$8
	sll	$2,$2,1
	addu	$2,$21,$2
	lhu	$22,0($2)
	.set	noreorder
	.set	nomacro
	beq	$22,$0,$L414
	addiu	$23,$23,1
	.set	macro
	.set	reorder

	lbu	$20,0($4)
	lw	$4,4132($17)
	sw	$22,16($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	do_apply_filter.isra.1
	.option	pic2
	sw	$20,20($sp)
	.set	macro
	.set	reorder

	addiu	$5,$19,20
	lw	$4,4132($17)
	move	$6,$16
	sw	$22,16($sp)
	move	$7,$18
	sw	$20,20($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	jal	do_apply_filter.isra.1
	.option	pic2
	addiu	$19,$19,40
	.set	macro
	.set	reorder

	li	$12,3			# 0x3
	.set	noreorder
	.set	nomacro
	bne	$23,$12,$L328
	lw	$28,24($sp)
	.set	macro
	.set	reorder

$L414:
	move	$20,$17
	lw	$13,36($sp)
	addiu	$10,$20,4264
	lw	$17,40($sp)
	move	$12,$16
	lw	$19,44($sp)
	move	$23,$0
	lw	$16,32($sp)
	move	$22,$12
	sw	$10,36($sp)
	move	$21,$fp
	sw	$12,40($sp)
	.set	noreorder
	.set	nomacro
	beq	$23,$18,$L443
	addiu	$14,$20,6312
	.set	macro
	.set	reorder

$L336:
	lw	$2,4164($20)
	addiu	$21,$21,4
	lw	$8,4168($20)
	addiu	$22,$22,4
	lw	$9,-4($21)
	addiu	$10,$2,4
	lw	$7,196($2)
	srl	$3,$8,31
	slt	$5,$0,$8
	sw	$8,200($2)
	subu	$5,$3,$5
	lw	$6,192($2)
	subu	$7,$8,$7
	lw	$12,188($2)
	srl	$3,$9,31
	srl	$4,$7,31
	sw	$5,72($2)
	slt	$5,$0,$7
	sw	$7,196($2)
	subu	$4,$4,$5
	slt	$5,$0,$9
	subu	$3,$3,$5
	sw	$4,68($2)
	lw	$4,4184($20)
	lw	$24,4196($20)
	lw	$15,4192($20)
	sll	$25,$4,5
	lw	$5,4200($20)
	lw	$11,4180($20)
	mult	$7,$24
	subu	$4,$25,$4
	lw	$25,4204($20)
	madd	$8,$15
	sra	$4,$4,5
	madd	$6,$5
	lw	$5,164($2)
	subu	$4,$11,$4
	madd	$12,$25
	subu	$7,$4,$5
	srl	$24,$4,31
	sw	$4,168($2)
	srl	$11,$7,31
	sw	$7,164($2)
	slt	$4,$0,$4
	slt	$7,$0,$7
	subu	$4,$24,$4
	subu	$7,$11,$7
	mflo	$25
	sw	$4,40($2)
	sw	$7,36($2)
	lw	$4,4180($20)
	lw	$15,4228($20)
	lw	$24,4224($20)
	lw	$11,4232($20)
	lw	$8,4236($20)
	sw	$4,4184($20)
	lw	$7,164($2)
	lw	$4,168($2)
	lw	$6,160($2)
	lw	$5,156($2)
	mult	$15,$7
	madd	$24,$4
	lw	$7,4240($20)
	lw	$4,152($2)
	madd	$11,$6
	madd	$8,$5
	madd	$7,$4
	mflo	$4
	sra	$4,$4,1
	addu	$25,$25,$4
	sra	$25,$25,10
	addu	$9,$9,$25
	sw	$9,4168($20)
	lw	$5,4176($20)
	lw	$25,4192($20)
	lw	$6,4196($20)
	sll	$4,$5,5
	lw	$12,4204($20)
	subu	$4,$4,$5
	sra	$4,$4,5
	addu	$9,$9,$4
	sw	$9,4176($20)
	lw	$4,72($2)
	mul	$5,$3,$4
	addu	$25,$5,$25
	sw	$25,4192($20)
	lw	$4,68($2)
	mul	$5,$3,$4
	addu	$6,$5,$6
	lw	$5,4200($20)
	sw	$6,4196($20)
	lw	$4,64($2)
	mul	$6,$3,$4
	addu	$4,$6,$5
	sw	$4,4200($20)
	lw	$4,60($2)
	mul	$5,$3,$4
	addu	$4,$5,$12
	sw	$4,4204($20)
	lw	$4,40($2)
	mul	$5,$3,$4
	addu	$24,$5,$24
	sw	$24,4224($20)
	lw	$4,36($2)
	mul	$5,$3,$4
	addu	$15,$5,$15
	sw	$15,4228($20)
	lw	$4,32($2)
	mul	$5,$3,$4
	addu	$11,$5,$11
	sw	$11,4232($20)
	lw	$4,28($2)
	mul	$5,$3,$4
	addu	$8,$5,$8
	sw	$8,4236($20)
	lw	$4,24($2)
	mul	$5,$3,$4
	addu	$3,$5,$7
	sw	$3,4240($20)
	sw	$9,-4($21)
	lw	$8,4172($20)
	lw	$4,132($2)
	lw	$6,-4($22)
	srl	$7,$8,31
	subu	$4,$8,$4
	sw	$8,136($2)
	slt	$9,$0,$8
	srl	$3,$4,31
	slt	$5,$0,$4
	sw	$4,132($2)
	subu	$7,$7,$9
	subu	$5,$3,$5
	srl	$9,$6,31
	sw	$7,56($2)
	slt	$3,$0,$6
	sw	$5,52($2)
	subu	$3,$9,$3
	lw	$24,4208($20)
	lw	$5,4212($20)
	lw	$9,4188($20)
	lw	$15,4176($20)
	mult	$4,$5
	lw	$7,100($2)
	sll	$5,$9,5
	lw	$4,128($2)
	lw	$25,4216($20)
	madd	$8,$24
	subu	$5,$5,$9
	lw	$8,124($2)
	lw	$11,4220($20)
	sra	$5,$5,5
	madd	$4,$25
	subu	$15,$15,$5
	madd	$8,$11
	subu	$5,$15,$7
	srl	$8,$15,31
	sw	$15,104($2)
	srl	$7,$5,31
	sw	$5,100($2)
	slt	$15,$0,$15
	slt	$5,$0,$5
	subu	$15,$8,$15
	subu	$5,$7,$5
	mflo	$4
	sw	$15,20($2)
	sw	$5,16($2)
	lw	$5,4176($20)
	lw	$11,4248($20)
	lw	$15,4244($20)
	lw	$9,4252($20)
	lw	$8,4256($20)
	sw	$5,4188($20)
	lw	$5,100($2)
	lw	$7,104($2)
	lw	$24,96($2)
	lw	$25,92($2)
	mult	$11,$5
	madd	$15,$7
	lw	$5,88($2)
	lw	$7,4260($20)
	madd	$9,$24
	madd	$8,$25
	lw	$24,4180($20)
	madd	$7,$5
	sll	$5,$24,5
	subu	$5,$5,$24
	mflo	$12
	sra	$5,$5,5
	sra	$24,$12,1
	addu	$4,$4,$24
	sra	$4,$4,10
	addu	$4,$6,$4
	lw	$6,4208($20)
	addu	$5,$4,$5
	sw	$4,4172($20)
	sw	$5,4180($20)
	lw	$4,56($2)
	mul	$12,$3,$4
	addu	$25,$12,$6
	lw	$6,4212($20)
	sw	$25,4208($20)
	lw	$4,52($2)
	mul	$12,$3,$4
	addu	$4,$12,$6
	sw	$4,4212($20)
	lw	$4,48($2)
	lw	$6,4216($20)
	lw	$24,4220($20)
	mul	$12,$3,$4
	addu	$6,$12,$6
	sw	$6,4216($20)
	lw	$4,44($2)
	mul	$6,$3,$4
	addu	$4,$6,$24
	sw	$4,4220($20)
	lw	$4,20($2)
	mul	$6,$3,$4
	addu	$15,$6,$15
	sw	$15,4244($20)
	lw	$4,16($2)
	mul	$6,$3,$4
	addu	$11,$6,$11
	sw	$11,4248($20)
	lw	$4,12($2)
	mul	$6,$3,$4
	addu	$9,$6,$9
	sw	$9,4252($20)
	lw	$4,8($2)
	mul	$6,$3,$4
	addu	$8,$6,$8
	sw	$8,4256($20)
	lw	$2,4($2)
	mul	$4,$3,$2
	addu	$3,$4,$7
	sw	$3,4260($20)
	sw	$5,-4($22)
	.set	noreorder
	.set	nomacro
	beq	$10,$14,$L444
	sw	$10,4164($20)
	.set	macro
	.set	reorder

	addiu	$23,$23,1
$L445:
	bne	$23,$18,$L336
$L443:
	.set	noreorder
	.set	nomacro
	beq	$18,$0,$L187
	lw	$12,40($sp)
	.set	macro
	.set	reorder

	li	$5,-1			# 0xffffffffffffffff
$L338:
	lw	$2,0($fp)
	addiu	$12,$12,4
	lw	$4,-4($12)
	addiu	$fp,$fp,4
	addiu	$13,$13,-1
	srl	$3,$2,31
	addu	$3,$3,$2
	sra	$3,$3,1
	subu	$3,$4,$3
	addu	$2,$2,$3
	sw	$3,-4($fp)
	.set	noreorder
	.set	nomacro
	bne	$13,$5,$L338
	sw	$2,-4($12)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L454
	.option	pic2
	li	$2,65536			# 0x10000
	.set	macro
	.set	reorder

$L425:
	lw	$25,%call16(memmove)($28)
	addiu	$4,$20,24944
	lw	$5,32($sp)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	sll	$6,$18,2
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L187
	.option	pic2
	lw	$28,24($sp)
	.set	macro
	.set	reorder

$L427:
	lw	$8,104($sp)
	move	$2,$16
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	sw	$0,0($8)
	.set	macro
	.set	reorder

$L444:
	lw	$25,%call16(memmove)($28)
	li	$6,200			# 0xc8
	lw	$4,36($sp)
	move	$5,$14
	sw	$13,48($sp)
	addiu	$23,$23,1
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,memmove
1:	jalr	$25
	sw	$14,52($sp)
	.set	macro
	.set	reorder

	lw	$7,36($sp)
	lw	$28,24($sp)
	lw	$14,52($sp)
	lw	$13,48($sp)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L445
	.option	pic2
	sw	$7,4164($20)
	.set	macro
	.set	reorder

$L257:
	sra	$2,$2,1
	subu	$2,$2,$5
	addu	$16,$2,$16
	sw	$16,-22120($10)
$L258:
	addiu	$2,$8,5
	srl	$16,$16,$2
	.set	noreorder
	.set	nomacro
	beq	$16,$0,$L259
	addiu	$8,$8,1
	.set	macro
	.set	reorder

	andi	$2,$9,0x1
	sra	$9,$9,1
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L260
	sw	$8,-22124($10)
	.set	macro
	.set	reorder

$L435:
	addiu	$2,$9,1
	sw	$2,-4($18)
	lw	$2,4132($20)
	slt	$2,$2,3990
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L262
	addiu	$21,$21,4
	.set	macro
	.set	reorder

$L436:
	lw	$5,-22144($10)
	sltu	$2,$5,$4
	beq	$2,$0,$L446
	lw	$6,-22136($10)
	lw	$7,-21988($10)
	lw	$9,-21992($10)
	lw	$2,-22148($10)
$L266:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L265
	sltu	$8,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L265:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L266
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L264:
	srl	$5,$5,16
	li	$6,65493			# 0xffd5
	teq	$5,$0,7
	divu	$0,$2,$5
	mflo	$7
	sltu	$3,$7,$6
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L267
	sw	$5,-22140($10)
	.set	macro
	.set	reorder

	sltu	$3,$7,14824
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L447
	lui	$25,%hi(counts_3970+4)
	.set	macro
	.set	reorder

	move	$9,$0
	addiu	$6,$25,%lo(counts_3970+4)
$L271:
	lhu	$3,0($6)
	addiu	$9,$9,1
	slt	$3,$7,$3
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L271
	addiu	$6,$6,2
	.set	macro
	.set	reorder

	lui	$7,%hi(counts_diff_3970)
	lui	$8,%hi(counts_3970)
	sll	$3,$9,1
	addiu	$7,$7,%lo(counts_diff_3970)
	addiu	$8,$8,%lo(counts_3970)
	addu	$6,$7,$3
	addu	$3,$8,$3
$L269:
	lhu	$3,0($3)
	lhu	$6,0($6)
	mul	$7,$3,$5
	mul	$5,$5,$6
	li	$3,63			# 0x3f
	subu	$2,$2,$7
	sw	$5,-22144($10)
	.set	noreorder
	.set	nomacro
	beq	$9,$3,$L448
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L272:
	lw	$8,-22132($10)
	.set	noreorder
	.set	nomacro
	beq	$8,$0,$L351
	move	$24,$0
	.set	macro
	.set	reorder

	addiu	$24,$8,-1
	sll	$9,$9,$24
$L276:
	slt	$3,$24,17
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L277
	sltu	$3,$5,$4
	.set	macro
	.set	reorder

	lw	$14,-21992($10)
	lw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L279
	lw	$7,-21988($10)
	.set	macro
	.set	reorder

$L285:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$14
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L284
	sltu	$15,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L284:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$15,$0,$L285
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L279:
	srl	$5,$5,16
	addiu	$24,$24,-16
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$16
	mul	$3,$16,$5
	subu	$2,$2,$3
	sw	$2,-22148($10)
$L287:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$14
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L286
	sltu	$15,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L286:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$15,$0,$L287
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	srl	$5,$5,$24
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$14
	mul	$3,$14,$5
	sll	$14,$14,16
	or	$14,$14,$16
	lw	$16,-22128($10)
	addu	$9,$14,$9
	subu	$2,$2,$3
	sw	$2,-22148($10)
$L453:
	addiu	$2,$9,1
	addiu	$5,$16,16
	srl	$3,$2,31
	srl	$5,$5,5
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L449
	addu	$2,$3,$2
	.set	macro
	.set	reorder

$L321:
	sra	$2,$2,1
	subu	$2,$2,$5
	addu	$16,$2,$16
	sw	$16,-22128($10)
$L322:
	addiu	$2,$8,5
	srl	$16,$16,$2
	.set	noreorder
	.set	nomacro
	beq	$16,$0,$L323
	andi	$2,$9,0x1
	.set	macro
	.set	reorder

	addiu	$8,$8,1
	sra	$9,$9,1
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L324
	sw	$8,-22132($10)
	.set	macro
	.set	reorder

$L442:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L325
	.option	pic2
	addiu	$2,$9,1
	.set	macro
	.set	reorder

$L230:
	mul	$5,$7,$3
	sw	$3,-22144($10)
	li	$6,65536			# 0x10000
	li	$8,-65536			# 0xffffffffffff0000
	ori	$8,$8,0x40
	addu	$14,$7,$8
	subu	$2,$2,$5
	sltu	$5,$7,$6
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L233
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	sw	$11,-21980($10)
$L233:
	li	$5,63			# 0x3f
	.set	noreorder
	.set	nomacro
	bne	$14,$5,$L462
	li	$7,65536			# 0x10000
	.set	macro
	.set	reorder

$L236:
	lw	$5,-22136($10)
	lw	$6,-21988($10)
	lw	$7,-21992($10)
$L239:
	sll	$5,$5,8
	sll	$3,$3,8
	sll	$8,$2,8
	sltu	$2,$6,$7
	sw	$5,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L238
	sltu	$9,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($6)
	addu	$5,$5,$2
	sw	$5,-22136($10)
$L238:
	srl	$2,$5,1
	sw	$3,-22144($10)
	addiu	$6,$6,1
	andi	$2,$2,0xff
	or	$2,$2,$8
	sw	$6,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L239
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L237:
	srl	$3,$3,16
	teq	$3,$0,7
	divu	$0,$2,$3
	sw	$3,-22140($10)
	sw	$3,-22144($10)
	mflo	$14
	mul	$8,$14,$3
	subu	$2,$2,$8
	sll	$8,$14,16
	sw	$2,-22148($10)
$L241:
	sll	$5,$5,8
	sll	$3,$3,8
	sll	$9,$2,8
	sltu	$2,$6,$7
	sw	$5,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L240
	sltu	$14,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($6)
	addu	$5,$5,$2
	sw	$5,-22136($10)
$L240:
	srl	$2,$5,1
	sw	$3,-22144($10)
	addiu	$6,$6,1
	andi	$2,$2,0xff
	or	$2,$2,$9
	sw	$6,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$14,$0,$L241
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	srl	$3,$3,16
	li	$5,65536			# 0x10000
	teq	$3,$0,7
	divu	$0,$2,$3
	slt	$5,$15,$5
	sw	$3,-22140($10)
	sw	$3,-22144($10)
	mflo	$14
	mul	$6,$14,$3
	or	$14,$14,$8
	subu	$2,$2,$6
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L243
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L242:
	lw	$9,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L248:
	sll	$6,$6,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L247
	sltu	$8,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L247:
	srl	$2,$6,1
	sw	$3,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L248
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L244:
	teq	$15,$0,7
	divu	$0,$3,$15
	mflo	$3
	teq	$3,$0,7
	divu	$0,$2,$3
	sw	$3,-22140($10)
	sw	$3,-22144($10)
	mflo	$9
	mul	$5,$9,$3
	subu	$3,$2,$5
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L249
	.option	pic2
	sw	$3,-22148($10)
	.set	macro
	.set	reorder

$L294:
	mul	$5,$7,$3
	sw	$3,-22144($10)
	li	$6,65536			# 0x10000
	li	$8,-65536			# 0xffffffffffff0000
	ori	$8,$8,0x40
	addu	$14,$7,$8
	subu	$2,$2,$5
	sltu	$5,$7,$6
	.set	noreorder
	.set	nomacro
	bne	$5,$0,$L297
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	sw	$11,-21980($10)
$L297:
	li	$7,63			# 0x3f
	.set	noreorder
	.set	nomacro
	bne	$14,$7,$L463
	li	$7,65536			# 0x10000
	.set	macro
	.set	reorder

$L300:
	lw	$6,-22136($10)
	lw	$7,-21988($10)
	lw	$8,-21992($10)
$L303:
	sll	$6,$6,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$7,$8
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L302
	sltu	$9,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L302:
	srl	$2,$6,1
	sw	$3,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L303
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L301:
	srl	$5,$3,16
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$14
	mul	$3,$14,$5
	sll	$9,$14,16
	subu	$2,$2,$3
	sw	$2,-22148($10)
$L305:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$8
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L304
	sltu	$14,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L304:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$14,$0,$L305
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	srl	$3,$5,16
	li	$5,65536			# 0x10000
	teq	$3,$0,7
	divu	$0,$2,$3
	slt	$5,$15,$5
	sw	$3,-22140($10)
	sw	$3,-22144($10)
	mflo	$14
	mul	$6,$14,$3
	or	$14,$14,$9
	subu	$2,$2,$6
	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L307
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L306:
	lw	$9,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L312:
	sll	$6,$6,8
	sll	$3,$3,8
	sll	$5,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L311
	sltu	$8,$3,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L311:
	srl	$2,$6,1
	sw	$3,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$5
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L312
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L308:
	teq	$15,$0,7
	divu	$0,$3,$15
	mflo	$5
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$9
	mul	$3,$9,$5
	subu	$2,$2,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L313
	.option	pic2
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L429:
	lw	$5,-22144($10)
	sltu	$2,$5,$4
	beq	$2,$0,$L450
	lw	$6,-22136($10)
	lw	$7,-21988($10)
	lw	$9,-21992($10)
	lw	$2,-22148($10)
$L202:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L201
	sltu	$8,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L201:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L202
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L200:
	srl	$5,$5,16
	li	$6,65493			# 0xffd5
	teq	$5,$0,7
	divu	$0,$2,$5
	mflo	$7
	sltu	$3,$7,$6
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L203
	sw	$5,-22140($10)
	.set	macro
	.set	reorder

	sltu	$3,$7,14824
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L451
	lui	$25,%hi(counts_3970+4)
	.set	macro
	.set	reorder

	move	$9,$0
	addiu	$6,$25,%lo(counts_3970+4)
$L207:
	lhu	$3,0($6)
	addiu	$9,$9,1
	slt	$3,$7,$3
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L207
	addiu	$6,$6,2
	.set	macro
	.set	reorder

	lui	$7,%hi(counts_diff_3970)
	lui	$8,%hi(counts_3970)
	sll	$3,$9,1
	addiu	$7,$7,%lo(counts_diff_3970)
	addiu	$8,$8,%lo(counts_3970)
	addu	$6,$7,$3
	addu	$3,$8,$3
$L205:
	lhu	$3,0($3)
	lhu	$6,0($6)
	mul	$7,$3,$5
	mul	$5,$5,$6
	li	$3,63			# 0x3f
	subu	$2,$2,$7
	sw	$5,-22144($10)
	.set	noreorder
	.set	nomacro
	beq	$9,$3,$L452
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L208:
	lw	$8,-22124($10)
	.set	noreorder
	.set	nomacro
	beq	$8,$0,$L350
	move	$24,$0
	.set	macro
	.set	reorder

	addiu	$24,$8,-1
	sll	$9,$9,$24
$L212:
	slt	$3,$24,17
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L213
	sltu	$3,$5,$4
	.set	macro
	.set	reorder

	lw	$14,-21992($10)
	lw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L215
	lw	$7,-21988($10)
	.set	macro
	.set	reorder

$L221:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$14
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L220
	sltu	$15,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L220:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$15,$0,$L221
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L215:
	srl	$5,$5,16
	addiu	$24,$24,-16
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$16
	mul	$3,$16,$5
	subu	$2,$2,$3
	sw	$2,-22148($10)
$L223:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$14
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L222
	sltu	$15,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L222:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$15,$0,$L223
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	srl	$5,$5,$24
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$14
	mul	$3,$14,$5
	sll	$14,$14,16
	or	$14,$14,$16
	lw	$16,-22120($10)
	addu	$9,$14,$9
	subu	$2,$2,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L423:
	.option	pic0
	jal	entropy_decode.constprop.3
	.option	pic2
	lui	$6,%hi($LC7)
	lw	$28,24($sp)
	li	$5,48			# 0x30
	lw	$4,0($20)
	lw	$25,%call16(av_log)($28)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC7)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L187
	.option	pic2
	lw	$28,24($sp)
	.set	macro
	.set	reorder

$L342:
	sll	$5,$18,2
	addiu	$2,$20,6512
	addu	$5,$19,$5
$L345:
	lw	$4,0($2)
	addiu	$19,$19,4
	lw	$3,18432($2)
	addiu	$2,$2,4
	sh	$4,-4($19)
	.set	noreorder
	.set	nomacro
	bne	$19,$5,$L345
	sh	$3,-2($19)
	.set	macro
	.set	reorder

$L415:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L341
	.option	pic2
	sll	$5,$18,1
	.set	macro
	.set	reorder

$L433:
	bne	$5,$0,$L242
	.option	pic0
	j	$L244
	.option	pic2
$L440:
	bne	$5,$0,$L306
	.option	pic0
	j	$L308
	.option	pic2
$L350:
$L213:
	sltu	$3,$5,$4
	beq	$3,$0,$L216
	lw	$15,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L218:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$15
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L217
	sltu	$14,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L217:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$14,$0,$L218
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L216:
	srl	$5,$5,$24
	lw	$16,-22120($10)
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$14
	mul	$3,$14,$5
	addu	$9,$14,$9
	subu	$5,$2,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L224
	.option	pic2
	sw	$5,-22148($10)
	.set	macro
	.set	reorder

$L203:
	mul	$3,$7,$5
	sw	$5,-22144($10)
	li	$6,65536			# 0x10000
	li	$8,-65536			# 0xffffffffffff0000
	ori	$8,$8,0x40
	addu	$9,$7,$8
	subu	$2,$2,$3
	sltu	$3,$7,$6
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L206
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	sw	$11,-21980($10)
$L206:
	li	$25,63			# 0x3f
	bne	$9,$25,$L208
$L348:
	lw	$6,-22136($10)
	lw	$7,-21988($10)
	lw	$9,-21992($10)
$L211:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L210
	sltu	$8,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L210:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L211
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	srl	$5,$5,5
$L455:
	lw	$8,-22124($10)
	move	$9,$0
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$24
	mul	$3,$24,$5
	subu	$2,$2,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L212
	.option	pic2
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L351:
$L277:
	sltu	$3,$5,$4
	beq	$3,$0,$L280
	lw	$15,-21992($10)
	lw	$6,-22136($10)
	lw	$7,-21988($10)
$L282:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$15
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L281
	sltu	$14,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L281:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$14,$0,$L282
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L280:
	srl	$5,$5,$24
	lw	$16,-22128($10)
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$14
	mul	$3,$14,$5
	addu	$9,$14,$9
	subu	$5,$2,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L453
	.option	pic2
	sw	$5,-22148($10)
	.set	macro
	.set	reorder

$L267:
	mul	$3,$7,$5
	sw	$5,-22144($10)
	li	$6,65536			# 0x10000
	li	$8,-65536			# 0xffffffffffff0000
	ori	$8,$8,0x40
	addu	$9,$7,$8
	subu	$2,$2,$3
	sltu	$3,$7,$6
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L270
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	sw	$11,-21980($10)
$L270:
	li	$6,63			# 0x3f
	bne	$9,$6,$L272
$L349:
	lw	$6,-22136($10)
	lw	$7,-21988($10)
	lw	$9,-21992($10)
$L275:
	sll	$6,$6,8
	sll	$5,$5,8
	sll	$3,$2,8
	sltu	$2,$7,$9
	sw	$6,-22136($10)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L274
	sltu	$8,$5,$4
	.set	macro
	.set	reorder

	lbu	$2,0($7)
	addu	$6,$6,$2
	sw	$6,-22136($10)
$L274:
	srl	$2,$6,1
	sw	$5,-22144($10)
	addiu	$7,$7,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$7,-21988($10)
	.set	noreorder
	.set	nomacro
	bne	$8,$0,$L275
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

	srl	$5,$5,5
$L456:
	lw	$8,-22132($10)
	move	$9,$0
	teq	$5,$0,7
	divu	$0,$2,$5
	sw	$5,-22140($10)
	sw	$5,-22144($10)
	mflo	$24
	mul	$3,$24,$5
	subu	$2,$2,$3
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L276
	.option	pic2
	sw	$2,-22148($10)
	.set	macro
	.set	reorder

$L422:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L184
	.option	pic2
	lw	$2,4152($20)
	.set	macro
	.set	reorder

$L439:
	sltu	$5,$3,$4
	bne	$5,$0,$L300
	lw	$8,-21992($10)
	lw	$6,-22136($10)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L301
	.option	pic2
	lw	$7,-21988($10)
	.set	macro
	.set	reorder

$L432:
	sltu	$5,$3,$4
	bne	$5,$0,$L236
	lw	$7,-21992($10)
	lw	$5,-22136($10)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L237
	.option	pic2
	lw	$6,-21988($10)
	.set	macro
	.set	reorder

$L428:
	lui	$6,%hi($LC8)
	lw	$25,%call16(av_log)($28)
	lw	$4,0($20)
	li	$5,48			# 0x30
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	addiu	$6,$6,%lo($LC8)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L187
	.option	pic2
	lw	$28,24($sp)
	.set	macro
	.set	reorder

$L437:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L291
	.option	pic2
	lw	$2,-22148($10)
	.set	macro
	.set	reorder

$L430:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L227
	.option	pic2
	lw	$2,-22148($10)
	.set	macro
	.set	reorder

$L339:
	lui	$6,%hi($LC9)
$L460:
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	sw	$0,4128($20)
	addiu	$6,$6,%lo($LC9)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$17
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	li	$2,-1			# 0xffffffffffffffff
	.set	macro
	.set	reorder

$L434:
	li	$24,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L246
	.option	pic2
	move	$8,$0
	.set	macro
	.set	reorder

$L441:
	li	$25,1			# 0x1
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L310
	.option	pic2
	move	$8,$0
	.set	macro
	.set	reorder

$L438:
	lui	$5,%hi(counts_3980)
	lui	$6,%hi(counts_diff_3980)
	addiu	$5,$5,%lo(counts_3980)
	addiu	$6,$6,%lo(counts_diff_3980)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L296
	.option	pic2
	move	$14,$0
	.set	macro
	.set	reorder

$L431:
	lui	$5,%hi(counts_3980)
	lui	$6,%hi(counts_diff_3980)
	addiu	$5,$5,%lo(counts_3980)
	addiu	$6,$6,%lo(counts_diff_3980)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L232
	.option	pic2
	move	$14,$0
	.set	macro
	.set	reorder

$L452:
	sltu	$3,$5,$4
	bne	$3,$0,$L348
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L455
	.option	pic2
	srl	$5,$5,5
	.set	macro
	.set	reorder

$L448:
	sltu	$3,$5,$4
	bne	$3,$0,$L349
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L456
	.option	pic2
	srl	$5,$5,5
	.set	macro
	.set	reorder

$L446:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L264
	.option	pic2
	lw	$2,-22148($10)
	.set	macro
	.set	reorder

$L327:
	li	$8,8388608			# 0x800000
	addu	$2,$20,$2
	addiu	$8,$8,1
	lw	$7,-22144($2)
	sltu	$3,$7,$8
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L329
	move	$6,$2
	.set	macro
	.set	reorder

	lw	$4,-22136($2)
	lw	$5,-21988($2)
	lw	$10,-21992($2)
	lw	$2,-22148($2)
$L331:
	sll	$4,$4,8
	sll	$7,$7,8
	sll	$3,$2,8
	sltu	$2,$5,$10
	sw	$4,-22136($6)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L330
	sltu	$9,$7,$8
	.set	macro
	.set	reorder

	lbu	$2,0($5)
	addu	$4,$4,$2
	sw	$4,-22136($6)
$L330:
	srl	$2,$4,1
	sw	$7,-22144($6)
	addiu	$5,$5,1
	andi	$2,$2,0xff
	or	$2,$2,$3
	sw	$5,-21988($6)
	.set	noreorder
	.set	nomacro
	bne	$9,$0,$L331
	sw	$2,-22148($6)
	.set	macro
	.set	reorder

	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L457
	.option	pic2
	li	$8,43420			# 0xa99c
	.set	macro
	.set	reorder

$L450:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L200
	.option	pic2
	lw	$2,-22148($10)
	.set	macro
	.set	reorder

$L451:
	lui	$3,%hi(counts_3970)
	lui	$6,%hi(counts_diff_3970)
	addiu	$3,$3,%lo(counts_3970)
	addiu	$6,$6,%lo(counts_diff_3970)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L205
	.option	pic2
	move	$9,$0
	.set	macro
	.set	reorder

$L447:
	lui	$3,%hi(counts_3970)
	lui	$6,%hi(counts_diff_3970)
	addiu	$3,$3,%lo(counts_3970)
	addiu	$6,$6,%lo(counts_diff_3970)
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L269
	.option	pic2
	move	$9,$0
	.set	macro
	.set	reorder

$L419:
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L347
	.option	pic2
	lw	$2,4128($20)
	.set	macro
	.set	reorder

$L426:
	lui	$6,%hi($LC6)
	lw	$25,%call16(av_log)($28)
	li	$5,16			# 0x10
	addiu	$6,$6,%lo($LC6)
	.set	noreorder
	.set	nomacro
	.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
	move	$4,$17
	.set	macro
	.set	reorder

	li	$2,-1			# 0xffffffffffffffff
	.option	pic0
	.set	noreorder
	.set	nomacro
	j	$L408
	.option	pic2
	sw	$0,-21996($21)
	.set	macro
	.set	reorder

	.end	ape_decode_frame
	.size	ape_decode_frame, .-ape_decode_frame
	.globl	ape_decoder
	.section	.rodata.str1.4
	.align	2
$LC10:
	.ascii	"ape\000"
	.align	2
$LC11:
	.ascii	"Monkey's Audio\000"
	.section	.data.rel.local,"aw",@progbits
	.align	2
	.type	ape_decoder, @object
	.size	ape_decoder, 72
ape_decoder:
	.word	$LC10
	.word	1
	.word	86051
	.word	43560
	.word	ape_decode_init
	.word	0
	.word	ape_decode_close
	.word	ape_decode_frame
	.word	256
	.space	4
	.word	ape_flush
	.space	8
	.word	$LC11
	.space	16
	.rdata
	.align	2
	.type	initial_coeffs, @object
	.size	initial_coeffs, 16
initial_coeffs:
	.word	360
	.word	317
	.word	-109
	.word	98
	.align	2
	.type	counts_diff_3980, @object
	.size	counts_diff_3980, 42
counts_diff_3980:
	.half	19578
	.half	16582
	.half	12257
	.half	7906
	.half	4576
	.half	2366
	.half	1170
	.half	536
	.half	261
	.half	119
	.half	65
	.half	31
	.half	19
	.half	10
	.half	6
	.half	3
	.half	3
	.half	2
	.half	1
	.half	1
	.half	1
	.align	2
	.type	counts_3980, @object
	.size	counts_3980, 44
counts_3980:
	.half	0
	.half	19578
	.half	-29376
	.half	-17119
	.half	-9213
	.half	-4637
	.half	-2271
	.half	-1101
	.half	-565
	.half	-304
	.half	-185
	.half	-120
	.half	-89
	.half	-70
	.half	-60
	.half	-54
	.half	-51
	.half	-48
	.half	-46
	.half	-45
	.half	-44
	.half	-43
	.align	2
	.type	counts_diff_3970, @object
	.size	counts_diff_3970, 42
counts_diff_3970:
	.half	14824
	.half	13400
	.half	11124
	.half	8507
	.half	6139
	.half	4177
	.half	2755
	.half	1756
	.half	1104
	.half	677
	.half	415
	.half	248
	.half	150
	.half	89
	.half	54
	.half	31
	.half	19
	.half	11
	.half	7
	.half	4
	.half	2
	.align	2
	.type	counts_3970, @object
	.size	counts_3970, 44
counts_3970:
	.half	0
	.half	14824
	.half	28224
	.half	-26188
	.half	-17681
	.half	-11542
	.half	-7365
	.half	-4610
	.half	-2854
	.half	-1750
	.half	-1073
	.half	-658
	.half	-410
	.half	-260
	.half	-171
	.half	-117
	.half	-86
	.half	-67
	.half	-56
	.half	-49
	.half	-45
	.half	-43
	.align	2
	.type	ape_filter_fracbits, @object
	.size	ape_filter_fracbits, 15
ape_filter_fracbits:
	.byte	0
	.byte	0
	.byte	0
	.byte	11
	.byte	0
	.byte	0
	.byte	11
	.byte	0
	.byte	0
	.byte	10
	.byte	13
	.byte	0
	.byte	11
	.byte	13
	.byte	15
	.align	2
	.type	ape_filter_orders, @object
	.size	ape_filter_orders, 30
ape_filter_orders:
	.half	0
	.half	0
	.half	0
	.half	16
	.half	0
	.half	0
	.half	64
	.half	0
	.half	0
	.half	32
	.half	256
	.half	0
	.half	16
	.half	256
	.half	1280
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
