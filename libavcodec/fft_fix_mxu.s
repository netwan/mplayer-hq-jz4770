.file	1 "fft_fix.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.globl	fft_calc_fix_inverse
.set	nomips16
.set	nomicromips
.ent	fft_calc_fix_inverse
.type	fft_calc_fix_inverse, @function
fft_calc_fix_inverse:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
lw	$13,0($4)
li	$2,1			# 0x1
addiu	$sp,$sp,-8
move	$3,$5
sll	$13,$2,$13
sw	$17,4($sp)
sw	$16,0($sp)
move	$16,$5
lw	$17,12($4)
sra	$2,$13,2
$L2:
#APP
# 104 "fft_fix.c" 1
.word	0b01110000011000000000000001010000	#S32LDD XR1,$3,0
# 0 "" 2
# 105 "fft_fix.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 106 "fft_fix.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 107 "fft_fix.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 108 "fft_fix.c" 1
.word	0b01110000011000000001000101010000	#S32LDD XR5,$3,16
# 0 "" 2
# 109 "fft_fix.c" 1
.word	0b01110000011000000001010110010000	#S32LDD XR6,$3,20
# 0 "" 2
# 110 "fft_fix.c" 1
.word	0b01110000011000000001100111010000	#S32LDD XR7,$3,24
# 0 "" 2
# 111 "fft_fix.c" 1
.word	0b01110000011000000001111000010000	#S32LDD XR8,$3,28
# 0 "" 2
# 112 "fft_fix.c" 1
.word	0b01110001000011001100010001011000	#D32ADD XR1,XR1,XR3,XR3,AS
# 0 "" 2
# 113 "fft_fix.c" 1
.word	0b01110001000100010000100010011000	#D32ADD XR2,XR2,XR4,XR4,AS
# 0 "" 2
# 114 "fft_fix.c" 1
.word	0b01110001000111011101010101011000	#D32ADD XR5,XR5,XR7,XR7,AS
# 0 "" 2
# 115 "fft_fix.c" 1
.word	0b01110001001000100001100110011000	#D32ADD XR6,XR6,XR8,XR8,AS
# 0 "" 2
# 116 "fft_fix.c" 1
.word	0b01110001000101010100010001011000	#D32ADD XR1,XR1,XR5,XR5,AS
# 0 "" 2
# 117 "fft_fix.c" 1
.word	0b01110001000110011000100010011000	#D32ADD XR2,XR2,XR6,XR6,AS
# 0 "" 2
# 118 "fft_fix.c" 1
.word	0b01110010001001100000110011011000	#D32ADD XR3,XR3,XR8,XR9,SA
# 0 "" 2
# 119 "fft_fix.c" 1
.word	0b01110001001000011101000100011000	#D32ADD XR4,XR4,XR7,XR8,AS
# 0 "" 2
# 120 "fft_fix.c" 1
.word	0b01110000011000000000000001010001	#S32STD XR1,$3,0
# 0 "" 2
# 121 "fft_fix.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 122 "fft_fix.c" 1
.word	0b01110000011000000000100011010001	#S32STD XR3,$3,8
# 0 "" 2
# 123 "fft_fix.c" 1
.word	0b01110000011000000000110100010001	#S32STD XR4,$3,12
# 0 "" 2
# 124 "fft_fix.c" 1
.word	0b01110000011000000001000101010001	#S32STD XR5,$3,16
# 0 "" 2
# 125 "fft_fix.c" 1
.word	0b01110000011000000001010110010001	#S32STD XR6,$3,20
# 0 "" 2
# 126 "fft_fix.c" 1
.word	0b01110000011000000001101001010001	#S32STD XR9,$3,24
# 0 "" 2
# 127 "fft_fix.c" 1
.word	0b01110000011000000001111000010001	#S32STD XR8,$3,28
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
bne	$2,$0,$L2
addiu	$3,$3,32
.set	macro
.set	reorder

sra	$11,$13,3
li	$5,4			# 0x4
sra	$13,$13,1
$L4:
sll	$15,$5,3
.set	noreorder
.set	nomacro
blez	$11,$L9
addu	$3,$16,$15
.set	macro
.set	reorder

sll	$12,$11,3
move	$14,$0
addu	$25,$17,$12
move	$2,$16
slt	$24,$11,$13
$L8:
#APP
# 151 "fft_fix.c" 1
.word	0b01110000010000000000000001010000	#S32LDD XR1,$2,0
# 0 "" 2
# 152 "fft_fix.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 153 "fft_fix.c" 1
.word	0b01110000011000000000000011010000	#S32LDD XR3,$3,0
# 0 "" 2
# 154 "fft_fix.c" 1
.word	0b01110000011000000000010100010000	#S32LDD XR4,$3,4
# 0 "" 2
# 155 "fft_fix.c" 1
.word	0b01110001000011001100010001011000	#D32ADD XR1,XR1,XR3,XR3,AS
# 0 "" 2
# 156 "fft_fix.c" 1
.word	0b01110001000100010000100010011000	#D32ADD XR2,XR2,XR4,XR4,AS
# 0 "" 2
# 157 "fft_fix.c" 1
.word	0b01110000010000000000000001010001	#S32STD XR1,$2,0
# 0 "" 2
# 158 "fft_fix.c" 1
.word	0b01110000010000000000010010010001	#S32STD XR2,$2,4
# 0 "" 2
# 159 "fft_fix.c" 1
.word	0b01110000011000000000000011010001	#S32STD XR3,$3,0
# 0 "" 2
# 160 "fft_fix.c" 1
.word	0b01110000011000000000010100010001	#S32STD XR4,$3,4
# 0 "" 2
#NO_APP
addiu	$2,$2,8
.set	noreorder
.set	nomacro
beq	$24,$0,$L6
addiu	$3,$3,8
.set	macro
.set	reorder

move	$4,$25
move	$6,$11
$L7:
lw	$10,0($4)
lw	$8,0($3)
lw	$7,4($4)
lw	$9,4($3)
#APP
# 180 "fft_fix.c" 1
.word	0b01110001010010000000100001100110	#S32MUL XR1,XR2,$10,$8
# 0 "" 2
# 181 "fft_fix.c" 1
.word	0b01110001010010010001100101100110	#S32MUL XR5,XR6,$10,$9
# 0 "" 2
# 182 "fft_fix.c" 1
.word	0b01110000010000000000000111010000	#S32LDD XR7,$2,0
# 0 "" 2
# 183 "fft_fix.c" 1
.word	0b01110000111010011000100001000100	#S32MSUB XR1,XR2,$7,$9
# 0 "" 2
# 184 "fft_fix.c" 1
.word	0b01110000111010001001100101000000	#S32MADD XR5,XR6,$7,$8
# 0 "" 2
# 185 "fft_fix.c" 1
.word	0b01110000010000000000011000010000	#S32LDD XR8,$2,4
# 0 "" 2
# 186 "fft_fix.c" 1
.word	0b01110000010101010100010001110000	#D32SLL XR1,XR1,XR5,XR5,1
# 0 "" 2
# 188 "fft_fix.c" 1
.word	0b01110001000001000101110111011000	#D32ADD XR7,XR7,XR1,XR1,AS
# 0 "" 2
# 189 "fft_fix.c" 1
.word	0b01110001000101010110001000011000	#D32ADD XR8,XR8,XR5,XR5,AS
# 0 "" 2
# 190 "fft_fix.c" 1
.word	0b01110000010000000000000111010001	#S32STD XR7,$2,0
# 0 "" 2
# 191 "fft_fix.c" 1
.word	0b01110000010000000000011000010001	#S32STD XR8,$2,4
# 0 "" 2
# 192 "fft_fix.c" 1
.word	0b01110000011000000000000001010001	#S32STD XR1,$3,0
# 0 "" 2
# 193 "fft_fix.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
#NO_APP
addu	$6,$6,$11
addiu	$2,$2,8
slt	$7,$6,$13
addiu	$3,$3,8
.set	noreorder
.set	nomacro
bne	$7,$0,$L7
addu	$4,$4,$12
.set	macro
.set	reorder

$L6:
addiu	$14,$14,1
addu	$2,$2,$15
.set	noreorder
.set	nomacro
bne	$14,$11,$L8
addu	$3,$3,$15
.set	macro
.set	reorder

$L9:
sra	$11,$11,1
.set	noreorder
.set	nomacro
bne	$11,$0,$L4
sll	$5,$5,1
.set	macro
.set	reorder

lw	$17,4($sp)
lw	$16,0($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,8
.set	macro
.set	reorder

.end	fft_calc_fix_inverse
.size	fft_calc_fix_inverse, .-fft_calc_fix_inverse
.align	2
.globl	fft_calc_fix
.set	nomips16
.set	nomicromips
.ent	fft_calc_fix
.type	fft_calc_fix, @function
fft_calc_fix:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
lw	$3,0($4)
li	$11,1			# 0x1
addiu	$sp,$sp,-24
move	$2,$5
sll	$11,$11,$3
sw	$17,8($sp)
sw	$16,4($sp)
move	$16,$5
sra	$13,$11,1
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
move	$5,$13
lw	$17,12($4)
$L21:
lw	$4,0($2)
addiu	$2,$2,16
lw	$3,-12($2)
addiu	$5,$5,-1
lw	$7,-8($2)
lw	$6,-4($2)
addu	$9,$4,$7
addu	$8,$3,$6
subu	$4,$4,$7
subu	$3,$3,$6
sw	$9,-16($2)
sw	$8,-12($2)
sw	$4,-8($2)
.set	noreorder
.set	nomacro
bne	$5,$0,$L21
sw	$3,-4($2)
.set	macro
.set	reorder

sra	$7,$11,2
move	$2,$16
$L22:
lw	$6,0($2)
addiu	$2,$2,32
lw	$5,-28($2)
addiu	$7,$7,-1
lw	$12,-16($2)
lw	$10,-12($2)
lw	$4,-24($2)
lw	$3,-20($2)
addu	$15,$6,$12
lw	$9,-4($2)
addu	$14,$5,$10
lw	$8,-8($2)
subu	$6,$6,$12
subu	$5,$5,$10
sw	$15,-32($2)
addu	$12,$4,$9
sw	$14,-28($2)
subu	$10,$3,$8
sw	$6,-16($2)
subu	$4,$4,$9
sw	$5,-12($2)
addu	$3,$3,$8
sw	$12,-24($2)
sw	$10,-20($2)
sw	$4,-8($2)
.set	noreorder
.set	nomacro
bne	$7,$0,$L22
sw	$3,-4($2)
.set	macro
.set	reorder

sra	$11,$11,3
li	$5,4			# 0x4
$L24:
sll	$15,$5,3
.set	noreorder
.set	nomacro
blez	$11,$L29
addu	$6,$16,$15
.set	macro
.set	reorder

sll	$12,$11,3
move	$14,$0
addu	$25,$17,$12
move	$4,$16
slt	$24,$11,$13
$L28:
lw	$8,0($4)
addiu	$3,$4,8
lw	$7,4($4)
addiu	$2,$6,8
lw	$10,0($6)
lw	$9,4($6)
addu	$19,$8,$10
addu	$18,$7,$9
subu	$8,$8,$10
subu	$7,$7,$9
sw	$19,0($4)
sw	$18,4($4)
sw	$8,0($6)
.set	noreorder
.set	nomacro
beq	$24,$0,$L26
sw	$7,4($6)
.set	macro
.set	reorder

move	$9,$25
move	$10,$11
$L27:
lw	$7,0($9)
lw	$6,0($2)
lw	$4,4($9)
lw	$8,4($2)
#APP
# 268 "fft_fix.c" 1
.set noreorder

# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000111001100000100001100110	#S32MUL XR1,XR2,$7,$6
# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000100010001000100001000100	#S32MSUB XR1,XR2,$4,$8
# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000111010000001100101100110	#S32MUL XR5,XR6,$7,$8
# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000100001101001100101000000	#S32MADD XR5,XR6,$4,$6
# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000010101010100010001110000	#D32SLL XR1,XR1,XR5,XR5,1
# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000000010000000000001101110	#S32M2I XR1, $8
# 0 "" 2
# 268 "fft_fix.c" 1
.word	0b01110000000001110000000101101110	#S32M2I XR5, $7
# 0 "" 2
# 268 "fft_fix.c" 1
.set reorder

# 0 "" 2
#NO_APP
lw	$6,0($3)
addu	$10,$10,$11
lw	$4,4($3)
addiu	$2,$2,8
slt	$18,$10,$13
addu	$20,$8,$6
addu	$19,$7,$4
subu	$6,$6,$8
subu	$4,$4,$7
sw	$20,0($3)
sw	$19,4($3)
addu	$9,$9,$12
sw	$6,-8($2)
addiu	$3,$3,8
.set	noreorder
.set	nomacro
bne	$18,$0,$L27
sw	$4,-4($2)
.set	macro
.set	reorder

$L26:
addiu	$14,$14,1
addu	$4,$3,$15
.set	noreorder
.set	nomacro
bne	$14,$11,$L28
addu	$6,$2,$15
.set	macro
.set	reorder

$L29:
sra	$11,$11,1
.set	noreorder
.set	nomacro
bne	$11,$0,$L24
sll	$5,$5,1
.set	macro
.set	reorder

lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,24
.set	macro
.set	reorder

.end	fft_calc_fix
.size	fft_calc_fix, .-fft_calc_fix
.align	2
.globl	fft_init_fix
.set	nomips16
.set	nomicromips
.ent	fft_init_fix
.type	fft_init_fix, @function
fft_init_fix:
.frame	$sp,88,$31		# vars= 0, regs= 8/8, args= 16, gp= 8
.mask	0x807f0000,-36
.fmask	0x0ff00000,-8
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-88
lui	$28,%hi(__gnu_local_gp)
sw	$16,24($sp)
li	$16,1			# 0x1
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$19,36($sp)
sll	$16,$16,$5
sw	$21,44($sp)
sdc1	$f26,80($sp)
move	$21,$4
srl	$2,$16,31
sw	$22,48($sp)
sw	$20,40($sp)
move	$22,$6
addu	$2,$2,$16
sw	$17,28($sp)
.cprestore	16
move	$17,$5
sra	$19,$2,1
sw	$31,52($sp)
sw	$18,32($sp)
lw	$25,%call16(malloc)($28)
sll	$4,$19,3
sdc1	$f24,72($sp)
sdc1	$f22,64($sp)
sdc1	$f20,56($sp)
.reloc	1f,R_MIPS_JALR,malloc
1:	jalr	$25
sw	$5,0($21)

lw	$28,16($sp)
move	$20,$2
beq	$2,$0,$L41
sw	$2,12($21)

lw	$25,%call16(malloc)($28)
.reloc	1f,R_MIPS_JALR,malloc
1:	jalr	$25
sll	$4,$16,1

lw	$28,16($sp)
move	$18,$2
beq	$2,$0,$L41
sw	$2,8($21)

bne	$22,$0,$L52
sw	$22,4($21)

lui	$2,%hi($LC1)
ldc1	$f26,%lo($LC1)($2)
lui	$2,%hi(fft_calc_fix)
addiu	$2,$2,%lo(fft_calc_fix)
$L42:
sw	$2,16($21)
lw	$2,%got(imdct_calc_fix)($28)
blez	$19,$L47
sw	$2,20($21)

mtc1	$16,$f2
lui	$2,%hi($LC0)
move	$21,$0
ldc1	$f24,%lo($LC0)($2)
lui	$2,%hi($LC3)
ldc1	$f22,%lo($LC3)($2)
cvt.d.w	$f0,$f2
div.d	$f22,$f22,$f0
$L46:
lw	$25,%call16(cos)($28)
addiu	$20,$20,8
mtc1	$21,$f0
addiu	$21,$21,1
cvt.d.w	$f20,$f0
mul.d	$f20,$f22,$f20
.reloc	1f,R_MIPS_JALR,cos
1:	jalr	$25
mov.d	$f12,$f20

mov.d	$f12,$f20
lw	$28,16($sp)
lw	$25,%call16(sin)($28)
.reloc	1f,R_MIPS_JALR,sin
1:	jalr	$25
mov.d	$f20,$f0

mul.d	$f0,$f0,$f26
lw	$28,16($sp)
mul.d	$f20,$f20,$f24
trunc.w.d $f2,$f0
trunc.w.d $f0,$f20
swc1	$f2,-4($20)
bne	$21,$19,$L46
swc1	$f0,-8($20)

$L47:
blez	$16,$L48
move	$2,$0

move	$8,$0
addiu	$5,$17,-1
$L49:
move	$6,$0
blez	$17,$L51
move	$4,$0

$L50:
sra	$3,$8,$4
subu	$7,$5,$4
andi	$3,$3,0x1
sll	$3,$3,$7
addiu	$4,$4,1
bne	$4,$17,$L50
or	$6,$6,$3

andi	$6,$6,0xffff
$L51:
addiu	$8,$8,1
sh	$6,0($18)
bne	$8,$16,$L49
addiu	$18,$18,2

move	$2,$0
$L48:
lw	$31,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
ldc1	$f26,80($sp)
ldc1	$f24,72($sp)
ldc1	$f22,64($sp)
ldc1	$f20,56($sp)
j	$31
addiu	$sp,$sp,88

$L52:
lui	$2,%hi($LC0)
ldc1	$f26,%lo($LC0)($2)
lui	$2,%hi(fft_calc_fix_inverse)
.option	pic0
j	$L42
.option	pic2
addiu	$2,$2,%lo(fft_calc_fix_inverse)

$L41:
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
addiu	$4,$21,8

lw	$28,16($sp)
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
addiu	$4,$21,12

.option	pic0
j	$L48
.option	pic2
li	$2,-1			# 0xffffffffffffffff

.set	macro
.set	reorder
.end	fft_init_fix
.size	fft_init_fix, .-fft_init_fix
.align	2
.globl	fft_permute_fix
.set	nomips16
.set	nomicromips
.ent	fft_permute_fix
.type	fft_permute_fix, @function
fft_permute_fix:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$8,0($4)
li	$2,1			# 0x1
sll	$8,$2,$8
blez	$8,$L69
lw	$7,8($4)

move	$6,$0
move	$4,$5
$L66:
lhu	$3,0($7)
addiu	$7,$7,2
sll	$2,$3,3
slt	$3,$3,$6
beq	$3,$0,$L65
addu	$2,$5,$2

lw	$11,0($4)
lw	$10,4($4)
lw	$9,0($2)
lw	$3,4($2)
sw	$11,0($2)
sw	$10,4($2)
sw	$9,0($4)
sw	$3,4($4)
$L65:
addiu	$6,$6,1
bne	$6,$8,$L66
addiu	$4,$4,8

$L69:
j	$31
nop

.set	macro
.set	reorder
.end	fft_permute_fix
.size	fft_permute_fix, .-fft_permute_fix
.align	2
.globl	fft_end_fix
.set	nomips16
.set	nomicromips
.ent	fft_end_fix
.type	fft_end_fix, @function
fft_end_fix:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,24($sp)
move	$16,$4
sw	$31,28($sp)
.cprestore	16
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
lw	$4,8($4)

lw	$28,16($sp)
lw	$4,12($16)
lw	$31,28($sp)
lw	$16,24($sp)
lw	$25,%call16(free)($28)
.reloc	1f,R_MIPS_JALR,free
1:	jr	$25
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	fft_end_fix
.size	fft_end_fix, .-fft_end_fix
.section	.rodata.cst8,"aM",@progbits,8
.align	3
$LC0:
.word	-4194304
.word	1105199103
.align	3
$LC1:
.word	-4194304
.word	-1042284545
.align	3
$LC3:
.word	1413754136
.word	1075388923
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
