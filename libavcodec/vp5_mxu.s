.file	1 "vp5.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_parse_vector_adjustment
.type	vp5_parse_vector_adjustment, @function
vp5_parse_vector_adjustment:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$10,6560($4)
addiu	$sp,$sp,-16
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,4($sp)
addiu	$14,$10,130
addiu	$24,$10,133
sw	$18,12($sp)
sw	$17,8($sp)
li	$13,1			# 0x1
lw	$12,%got(ff_vp56_norm_shift)($28)
li	$25,1			# 0x1
li	$16,2			# 0x2
addiu	$10,$10,136
$L2:
lw	$8,5168($4)
lw	$2,5172($4)
lw	$7,5184($4)
addu	$3,$12,$8
lbu	$11,0($14)
lbu	$6,0($3)
sll	$8,$8,$6
addu	$2,$6,$2
sll	$7,$7,$6
bltz	$2,$L3
sw	$8,5168($4)

lw	$3,5176($4)
lw	$6,5180($4)
sltu	$6,$3,$6
beq	$6,$0,$L3
addiu	$6,$3,2

sw	$6,5176($4)
lbu	$9,1($3)
lbu	$3,0($3)
sll	$9,$9,8
or	$9,$9,$3
sll	$3,$9,8
srl	$9,$9,8
or	$9,$3,$9
andi	$9,$9,0xffff
sll	$9,$9,$2
addiu	$2,$2,-16
or	$7,$7,$9
$L3:
addiu	$3,$8,-1
mul	$3,$3,$11
sra	$3,$3,8
addiu	$3,$3,1
sll	$6,$3,16
sltu	$9,$7,$6
bne	$9,$0,$L4
sw	$2,5172($4)

subu	$3,$8,$3
subu	$7,$7,$6
addu	$6,$12,$3
sw	$3,5168($4)
sw	$7,5184($4)
lbu	$8,0($6)
lbu	$9,-2($14)
sll	$3,$3,$8
addu	$2,$2,$8
sll	$7,$7,$8
bltz	$2,$L5
sw	$3,5168($4)

lw	$6,5176($4)
lw	$8,5180($4)
sltu	$8,$6,$8
bne	$8,$0,$L28
addiu	$8,$6,2

$L5:
addiu	$6,$3,-1
mul	$6,$6,$9
sra	$6,$6,8
addiu	$6,$6,1
sll	$8,$6,16
sltu	$15,$7,$8
xori	$15,$15,0x1
beq	$15,$0,$L24
sw	$2,5172($4)

subu	$6,$3,$6
subu	$7,$7,$8
$L24:
addu	$3,$12,$6
sw	$6,5168($4)
sw	$7,5184($4)
lbu	$11,-1($24)
lbu	$8,0($3)
sll	$9,$6,$8
addu	$17,$2,$8
sll	$6,$7,$8
bltz	$17,$L7
sw	$9,5168($4)

lw	$3,5176($4)
lw	$2,5180($4)
sltu	$2,$3,$2
bne	$2,$0,$L29
addiu	$2,$3,2

$L7:
addiu	$3,$9,-1
mul	$3,$3,$11
sra	$3,$3,8
addiu	$3,$3,1
sll	$2,$3,16
sltu	$11,$6,$2
xori	$8,$11,0x1
beq	$8,$0,$L25
sw	$17,5172($4)

subu	$3,$9,$3
subu	$6,$6,$2
$L25:
addu	$2,$12,$3
sw	$3,5168($4)
sw	$6,5184($4)
lbu	$9,0($24)
lbu	$7,0($2)
sll	$3,$3,$7
addu	$17,$17,$7
sll	$6,$6,$7
bltz	$17,$L9
sw	$3,5168($4)

lw	$11,5176($4)
lw	$2,5180($4)
sltu	$2,$11,$2
bne	$2,$0,$L30
addiu	$2,$11,2

$L9:
addiu	$2,$3,-1
mul	$2,$2,$9
sra	$2,$2,8
addiu	$2,$2,1
sll	$9,$2,16
sltu	$7,$6,$9
xori	$7,$7,0x1
beq	$7,$0,$L10
sw	$17,5172($4)

subu	$3,$3,$2
subu	$6,$6,$9
sw	$3,5168($4)
$L20:
sll	$11,$7,1
lw	$7,%got(vp56_pva_tree)($28)
sw	$6,5184($4)
or	$11,$11,$8
$L11:
lb	$2,0($7)
blez	$2,$L31
nop

$L14:
lw	$3,5168($4)
lb	$2,1($7)
lw	$6,5172($4)
addu	$8,$12,$3
lw	$17,5184($4)
addu	$2,$10,$2
lbu	$8,0($8)
lbu	$2,0($2)
sll	$3,$3,$8
addu	$6,$8,$6
addiu	$9,$3,-1
sw	$3,5168($4)
sll	$8,$17,$8
bltz	$6,$L12
mul	$2,$9,$2

lw	$17,5176($4)
lw	$9,5180($4)
sltu	$9,$17,$9
beq	$9,$0,$L12
addiu	$18,$17,2

sw	$18,5176($4)
lbu	$9,1($17)
lbu	$17,0($17)
sll	$9,$9,8
or	$9,$9,$17
sll	$17,$9,8
srl	$9,$9,8
or	$9,$17,$9
andi	$9,$9,0xffff
sll	$9,$9,$6
addiu	$6,$6,-16
or	$8,$8,$9
$L12:
sra	$2,$2,8
sw	$6,5172($4)
addiu	$2,$2,1
sll	$6,$2,16
subu	$9,$8,$6
sltu	$6,$8,$6
bne	$6,$0,$L13
subu	$3,$3,$2

sw	$3,5168($4)
sw	$9,5184($4)
lb	$2,0($7)
sll	$2,$2,1
addu	$7,$7,$2
lb	$2,0($7)
bgtz	$2,$L14
nop

$L31:
subu	$2,$0,$2
subu	$3,$0,$15
sll	$2,$2,2
or	$2,$11,$2
xor	$2,$2,$3
addu	$15,$2,$15
$L17:
beq	$13,$25,$L32
nop

beq	$13,$16,$L33
sh	$15,2($5)

$L16:
addiu	$14,$14,1
addiu	$13,$13,1
addiu	$24,$24,2
.option	pic0
j	$L2
.option	pic2
addiu	$10,$10,7

$L13:
sw	$2,5168($4)
addiu	$7,$7,2
.option	pic0
j	$L11
.option	pic2
sw	$8,5184($4)

$L33:
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

$L4:
sw	$3,5168($4)
move	$15,$0
.option	pic0
j	$L17
.option	pic2
sw	$7,5184($4)

$L10:
.option	pic0
j	$L20
.option	pic2
sw	$2,5168($4)

$L30:
sw	$2,5176($4)
lbu	$7,1($11)
lbu	$2,0($11)
sll	$7,$7,8
or	$7,$7,$2
sll	$2,$7,8
srl	$7,$7,8
or	$7,$2,$7
andi	$2,$7,0xffff
sll	$2,$2,$17
addiu	$17,$17,-16
.option	pic0
j	$L9
.option	pic2
or	$6,$6,$2

$L29:
sw	$2,5176($4)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$17
addiu	$17,$17,-16
.option	pic0
j	$L7
.option	pic2
or	$6,$6,$2

$L28:
sw	$8,5176($4)
lbu	$8,1($6)
lbu	$6,0($6)
sll	$8,$8,8
or	$8,$8,$6
sll	$6,$8,8
srl	$8,$8,8
or	$8,$6,$8
andi	$6,$8,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
.option	pic0
j	$L5
.option	pic2
or	$7,$7,$6

$L32:
.option	pic0
j	$L16
.option	pic2
sh	$15,0($5)

.set	macro
.set	reorder
.end	vp5_parse_vector_adjustment
.size	vp5_parse_vector_adjustment, .-vp5_parse_vector_adjustment
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_parse_coeff
.type	vp5_parse_coeff, @function
vp5_parse_coeff:
.frame	$sp,96,$31		# vars= 32, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-96
addiu	$28,$28,%lo(__gnu_local_gp)
addiu	$7,$4,5344
sw	$fp,88($sp)
sw	$23,84($sp)
lw	$2,%got(blk_coeff)($28)
lw	$3,%got(vp56_b6to4)($28)
lw	$23,6560($4)
lw	$fp,%got(vp56_coeff_parse_table)($28)
lw	$2,0($2)
sw	$17,60($sp)
sw	$21,76($sp)
addiu	$21,$4,4140
sw	$16,56($sp)
move	$16,$4
sw	$31,92($sp)
sw	$22,80($sp)
sw	$20,72($sp)
sw	$19,68($sp)
sw	$18,64($sp)
.cprestore	16
sw	$3,32($sp)
sw	$7,36($sp)
sw	$2,24($sp)
sw	$0,28($sp)
sw	$0,40($sp)
sw	$0,48($sp)
sw	$0,44($sp)
lw	$17,%got(ff_vp56_norm_shift)($28)
$L35:
lw	$7,44($sp)
$L105:
lw	$8,48($sp)
lw	$3,32($sp)
lw	$6,5292($16)
subu	$2,$7,$8
lw	$7,36($sp)
lw	$8,40($sp)
lbu	$20,0($3)
sll	$5,$2,4
lw	$3,0($7)
subu	$11,$2,$8
sll	$18,$20,6
subu	$5,$5,$2
sll	$4,$3,2
addu	$7,$16,$18
sll	$3,$3,4
addu	$11,$23,$11
subu	$4,$3,$4
lbu	$3,6236($7)
addiu	$11,$11,166
addu	$4,$6,$4
sll	$2,$3,1
sll	$3,$3,3
lbu	$4,0($4)
subu	$3,$3,$2
addu	$2,$3,$4
sll	$3,$2,2
addu	$2,$3,$2
addu	$5,$2,$5
addu	$5,$23,$5
addiu	$5,$5,1124
#APP
# 189 "vp5.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
lw	$3,44($sp)
sll	$6,$8,1
sll	$19,$8,3
lw	$8,28($sp)
lui	$14,%hi(vp5_coeff_groups+1)
lw	$2,5168($16)
addu	$7,$6,$3
lw	$4,5172($16)
subu	$6,$19,$6
lw	$3,5184($16)
sll	$24,$7,4
sll	$19,$6,5
sll	$25,$8,6
addiu	$14,$14,%lo(vp5_coeff_groups+1)
li	$13,1			# 0x1
move	$12,$0
li	$15,-1			# 0xffffffffffffffff
addu	$19,$6,$19
subu	$24,$24,$7
$L37:
addu	$6,$17,$2
lbu	$7,0($5)
lbu	$8,0($6)
sll	$6,$2,$8
addu	$4,$8,$4
sll	$3,$3,$8
.set	noreorder
.set	nomacro
bltz	$4,$L38
sw	$6,5168($16)
.set	macro
.set	reorder

lw	$8,5176($16)
lw	$2,5180($16)
sltu	$2,$8,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L97
addiu	$2,$6,-1
.set	macro
.set	reorder

addiu	$2,$8,2
sw	$2,5176($16)
lbu	$2,1($8)
lbu	$8,0($8)
sll	$2,$2,8
or	$2,$2,$8
sll	$8,$2,8
srl	$2,$2,8
or	$2,$8,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$3,$3,$2
$L38:
addiu	$2,$6,-1
$L97:
mul	$2,$2,$7
sra	$2,$2,8
addiu	$2,$2,1
sll	$7,$2,16
sltu	$8,$3,$7
.set	noreorder
.set	nomacro
bne	$8,$0,$L39
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$6,$6,$2
subu	$3,$3,$7
addu	$2,$17,$6
sw	$6,5168($16)
sw	$3,5184($16)
lbu	$7,0($2)
lbu	$8,2($5)
sll	$6,$6,$7
addu	$4,$4,$7
sll	$3,$3,$7
.set	noreorder
.set	nomacro
bltz	$4,$L40
sw	$6,5168($16)
.set	macro
.set	reorder

lw	$7,5176($16)
lw	$2,5180($16)
sltu	$2,$7,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L98
addiu	$2,$6,-1
.set	macro
.set	reorder

addiu	$2,$7,2
sw	$2,5176($16)
lbu	$2,1($7)
lbu	$7,0($7)
sll	$2,$2,8
or	$2,$2,$7
sll	$7,$2,8
srl	$2,$2,8
or	$2,$7,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$3,$3,$2
$L40:
addiu	$2,$6,-1
$L98:
mul	$2,$2,$8
sra	$2,$2,8
addiu	$2,$2,1
sll	$7,$2,16
sltu	$8,$3,$7
.set	noreorder
.set	nomacro
bne	$8,$0,$L41
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$2,$6,$2
subu	$3,$3,$7
addu	$6,$17,$2
sw	$2,5168($16)
sw	$3,5184($16)
lbu	$7,0($6)
lbu	$6,3($5)
sll	$2,$2,$7
addu	$4,$4,$7
sll	$3,$3,$7
.set	noreorder
.set	nomacro
bltz	$4,$L42
sw	$2,5168($16)
.set	macro
.set	reorder

lw	$8,5176($16)
lw	$7,5180($16)
sltu	$7,$8,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L42
addiu	$7,$8,2
.set	macro
.set	reorder

sw	$7,5176($16)
lbu	$7,1($8)
lbu	$8,0($8)
sll	$7,$7,8
or	$7,$7,$8
sll	$8,$7,8
srl	$7,$7,8
or	$7,$8,$7
andi	$7,$7,0xffff
sll	$7,$7,$4
addiu	$4,$4,-16
or	$3,$3,$7
$L42:
addiu	$8,$2,-1
mul	$8,$8,$6
sra	$8,$8,8
addiu	$8,$8,1
sll	$6,$8,16
sltu	$7,$3,$6
.set	noreorder
.set	nomacro
bne	$7,$0,$L43
sw	$4,5172($16)
.set	macro
.set	reorder

addu	$18,$16,$18
lw	$7,%got(vp56_pc_tree)($28)
subu	$2,$2,$8
subu	$3,$3,$6
li	$5,4			# 0x4
addu	$18,$18,$12
sw	$2,5168($16)
sw	$3,5184($16)
sb	$5,6236($18)
$L44:
lb	$5,0($7)
.set	noreorder
.set	nomacro
blez	$5,$L94
addu	$8,$17,$2
.set	macro
.set	reorder

$L47:
lbu	$5,0($8)
lb	$6,1($7)
sll	$8,$2,$5
addu	$6,$11,$6
addu	$4,$5,$4
addiu	$2,$8,-1
lbu	$6,0($6)
sll	$5,$3,$5
.set	noreorder
.set	nomacro
bltz	$4,$L45
sw	$8,5168($16)
.set	macro
.set	reorder

lw	$9,5176($16)
lw	$3,5180($16)
sltu	$3,$9,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L45
addiu	$10,$9,2
.set	macro
.set	reorder

sw	$10,5176($16)
lbu	$3,1($9)
lbu	$9,0($9)
sll	$3,$3,8
or	$3,$3,$9
sll	$9,$3,8
srl	$3,$3,8
or	$3,$9,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$5,$5,$3
$L45:
mul	$2,$2,$6
sw	$4,5172($16)
sra	$2,$2,8
addiu	$2,$2,1
sll	$6,$2,16
sltu	$9,$5,$6
.set	noreorder
.set	nomacro
bne	$9,$0,$L46
move	$3,$5
.set	macro
.set	reorder

subu	$2,$8,$2
subu	$3,$5,$6
sw	$2,5168($16)
sw	$3,5184($16)
lb	$5,0($7)
sll	$5,$5,1
addu	$7,$7,$5
lb	$5,0($7)
.set	noreorder
.set	nomacro
bgtz	$5,$L47
addu	$8,$17,$2
.set	macro
.set	reorder

$L94:
lbu	$7,0($8)
subu	$22,$0,$5
sll	$9,$2,$7
addu	$6,$7,$4
sll	$3,$3,$7
.set	noreorder
.set	nomacro
bltz	$6,$L48
sw	$9,5168($16)
.set	macro
.set	reorder

lw	$4,5176($16)
lw	$2,5180($16)
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L99
addiu	$2,$9,1
.set	macro
.set	reorder

addiu	$2,$4,2
sw	$2,5176($16)
lbu	$2,1($4)
lbu	$4,0($4)
sll	$2,$2,8
or	$2,$2,$4
sll	$4,$2,8
srl	$2,$2,8
or	$2,$4,$2
andi	$2,$2,0xffff
sll	$2,$2,$6
addiu	$6,$6,-16
or	$3,$3,$2
$L48:
addiu	$2,$9,1
$L99:
sra	$2,$2,1
sll	$4,$2,16
sltu	$8,$3,$4
xori	$8,$8,0x1
.set	noreorder
.set	nomacro
beq	$8,$0,$L49
sw	$6,5172($16)
.set	macro
.set	reorder

subu	$2,$9,$2
subu	$3,$3,$4
$L49:
li	$4,5			# 0x5
sw	$2,5168($16)
sll	$10,$22,2
sw	$3,5184($16)
subu	$5,$4,$5
lw	$4,%got(vp56_coeff_bit_length)($28)
addu	$7,$4,$22
lw	$4,%got(vp56_coeff_bias)($28)
addu	$5,$4,$5
lbu	$9,0($7)
sll	$4,$22,4
subu	$10,$4,$10
lbu	$7,0($5)
subu	$10,$10,$22
$L52:
addu	$5,$17,$2
addu	$4,$9,$10
lbu	$5,0($5)
addu	$4,$fp,$4
sll	$11,$2,$5
lbu	$2,0($4)
addu	$4,$5,$6
addiu	$6,$11,-1
sw	$11,5168($16)
sll	$5,$3,$5
.set	noreorder
.set	nomacro
bltz	$4,$L50
mul	$2,$6,$2
.set	macro
.set	reorder

lw	$6,5176($16)
lw	$3,5180($16)
sltu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L50
addiu	$13,$6,2
.set	macro
.set	reorder

sw	$13,5176($16)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$5,$5,$3
$L50:
sra	$2,$2,8
sw	$4,5172($16)
addiu	$2,$2,1
sll	$13,$2,16
sltu	$6,$5,$13
xori	$6,$6,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L92
move	$3,$5
.set	macro
.set	reorder

subu	$2,$11,$2
subu	$3,$5,$13
$L92:
sll	$6,$6,$9
sw	$2,5168($16)
addiu	$9,$9,-1
sw	$3,5184($16)
addu	$7,$7,$6
.set	noreorder
.set	nomacro
bne	$9,$15,$L52
move	$6,$4
.set	macro
.set	reorder

lw	$5,32($sp)
li	$13,2			# 0x2
lbu	$20,0($5)
sll	$18,$20,6
$L53:
subu	$5,$0,$8
xor	$7,$5,$7
.set	noreorder
.set	nomacro
beq	$12,$0,$L62
addu	$8,$7,$8
.set	macro
.set	reorder

lhu	$5,5286($16)
mul	$8,$8,$5
$L62:
addu	$5,$21,$22
lbu	$5,0($5)
#APP
# 220 "vp5.c" 1
.word	0b01110000000001010000001010101111	#S32I2M XR10,$5
# 0 "" 2
# 221 "vp5.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
addu	$5,$21,$12
lw	$7,24($sp)
lbu	$5,0($5)
addu	$5,$5,$25
sll	$5,$5,1
addu	$5,$7,$5
sh	$8,0($5)
$L63:
lbu	$6,0($14)
sll	$8,$13,1
sll	$11,$13,6
addiu	$12,$12,1
sll	$7,$6,2
sll	$5,$6,4
addu	$11,$8,$11
subu	$5,$5,$7
addu	$7,$16,$18
subu	$5,$5,$6
addu	$7,$7,$12
addu	$11,$5,$11
slt	$5,$6,3
addu	$11,$11,$19
lbu	$9,6236($7)
addu	$11,$23,$11
.set	noreorder
.set	nomacro
beq	$5,$0,$L84
addiu	$11,$11,188
.set	macro
.set	reorder

sll	$7,$13,3
sll	$5,$9,2
subu	$7,$7,$8
sll	$10,$6,1
sll	$8,$7,4
sll	$6,$6,5
addu	$9,$5,$9
subu	$6,$6,$10
subu	$7,$8,$7
addu	$6,$9,$6
addu	$5,$24,$7
addiu	$14,$14,1
addu	$5,$6,$5
addiu	$5,$5,584
.option	pic0
.set	noreorder
.set	nomacro
j	$L37
.option	pic2
addu	$5,$23,$5
.set	macro
.set	reorder

$L46:
sw	$2,5168($16)
addiu	$7,$7,2
.option	pic0
.set	noreorder
.set	nomacro
j	$L44
.option	pic2
sw	$5,5184($16)
.set	macro
.set	reorder

$L39:
sw	$2,5168($16)
.set	noreorder
.set	nomacro
beq	$13,$0,$L73
sw	$3,5184($16)
.set	macro
.set	reorder

addu	$6,$17,$2
lbu	$7,1($5)
lbu	$6,0($6)
sll	$2,$2,$6
addu	$4,$4,$6
sll	$3,$3,$6
.set	noreorder
.set	nomacro
bltz	$4,$L64
sw	$2,5168($16)
.set	macro
.set	reorder

lw	$6,5176($16)
lw	$5,5180($16)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L100
addiu	$5,$2,-1
.set	macro
.set	reorder

addiu	$5,$6,2
sw	$5,5176($16)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$4
addiu	$4,$4,-16
or	$3,$3,$5
$L64:
addiu	$5,$2,-1
$L100:
mul	$5,$5,$7
sra	$5,$5,8
addiu	$5,$5,1
sll	$6,$5,16
sltu	$7,$3,$6
.set	noreorder
.set	nomacro
bne	$7,$0,$L65
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$2,$2,$5
subu	$3,$3,$6
sw	$2,5168($16)
sw	$3,5184($16)
$L73:
addu	$5,$16,$18
move	$13,$0
addu	$5,$5,$12
.option	pic0
.set	noreorder
.set	nomacro
j	$L63
.option	pic2
sb	$0,6236($5)
.set	macro
.set	reorder

$L84:
move	$5,$11
.option	pic0
.set	noreorder
.set	nomacro
j	$L37
.option	pic2
addiu	$14,$14,1
.set	macro
.set	reorder

$L41:
addu	$6,$17,$2
sw	$3,5184($16)
addu	$5,$16,$18
lbu	$7,0($6)
li	$6,1			# 0x1
addu	$5,$5,$12
sb	$6,6236($5)
sll	$6,$2,$7
addu	$4,$4,$7
sll	$3,$3,$7
.set	noreorder
.set	nomacro
bltz	$4,$L60
sw	$6,5168($16)
.set	macro
.set	reorder

lw	$5,5176($16)
lw	$2,5180($16)
sltu	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L101
addiu	$2,$6,1
.set	macro
.set	reorder

addiu	$2,$5,2
sw	$2,5176($16)
lbu	$2,1($5)
lbu	$5,0($5)
sll	$2,$2,8
or	$2,$2,$5
sll	$5,$2,8
srl	$2,$2,8
or	$2,$5,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$3,$3,$2
$L60:
addiu	$2,$6,1
$L101:
sra	$2,$2,1
sll	$5,$2,16
sltu	$8,$3,$5
xori	$8,$8,0x1
.set	noreorder
.set	nomacro
beq	$8,$0,$L61
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$2,$6,$2
subu	$3,$3,$5
$L61:
li	$13,1			# 0x1
sw	$2,5168($16)
li	$7,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L53
.option	pic2
sw	$3,5184($16)
.set	macro
.set	reorder

$L43:
addu	$2,$17,$8
sw	$8,5168($16)
sw	$3,5184($16)
lbu	$9,4($5)
lbu	$6,0($2)
sll	$2,$8,$6
addu	$4,$4,$6
sll	$3,$3,$6
.set	noreorder
.set	nomacro
bltz	$4,$L54
sw	$2,5168($16)
.set	macro
.set	reorder

lw	$6,5176($16)
lw	$5,5180($16)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L102
addiu	$5,$2,-1
.set	macro
.set	reorder

addiu	$5,$6,2
sw	$5,5176($16)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$4
addiu	$4,$4,-16
or	$3,$3,$5
$L54:
addiu	$5,$2,-1
$L102:
mul	$5,$5,$9
sra	$5,$5,8
addiu	$5,$5,1
sll	$6,$5,16
sltu	$7,$3,$6
.set	noreorder
.set	nomacro
bne	$7,$0,$L55
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$2,$2,$5
subu	$3,$3,$6
addu	$5,$17,$2
sw	$2,5168($16)
sw	$3,5184($16)
lbu	$6,0($5)
lbu	$7,5($11)
sll	$2,$2,$6
addu	$4,$4,$6
sll	$3,$3,$6
.set	noreorder
.set	nomacro
bltz	$4,$L56
sw	$2,5168($16)
.set	macro
.set	reorder

lw	$6,5176($16)
lw	$5,5180($16)
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L103
addiu	$5,$2,-1
.set	macro
.set	reorder

addiu	$5,$6,2
sw	$5,5176($16)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$4
addiu	$4,$4,-16
or	$3,$3,$5
$L56:
addiu	$5,$2,-1
$L103:
mul	$5,$5,$7
sra	$5,$5,8
addiu	$5,$5,1
sll	$6,$5,16
sltu	$7,$3,$6
xori	$7,$7,0x1
.set	noreorder
.set	nomacro
beq	$7,$0,$L57
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$5,$2,$5
subu	$3,$3,$6
$L57:
addu	$2,$16,$18
sw	$3,5184($16)
li	$6,3			# 0x3
addu	$2,$2,$12
addiu	$7,$7,3
sb	$6,6236($2)
$L78:
addu	$2,$17,$5
lbu	$8,0($2)
sll	$5,$5,$8
addu	$4,$8,$4
sll	$6,$3,$8
.set	noreorder
.set	nomacro
bltz	$4,$L58
sw	$5,5168($16)
.set	macro
.set	reorder

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L104
addiu	$2,$5,1
.set	macro
.set	reorder

addiu	$2,$3,2
sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$6,$6,$2
$L58:
addiu	$2,$5,1
$L104:
sra	$2,$2,1
sll	$3,$2,16
sltu	$8,$6,$3
xori	$8,$8,0x1
.set	noreorder
.set	nomacro
beq	$8,$0,$L59
sw	$4,5172($16)
.set	macro
.set	reorder

subu	$2,$5,$2
subu	$6,$6,$3
$L59:
sw	$2,5168($16)
move	$3,$6
sw	$6,5184($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L53
.option	pic2
li	$13,2			# 0x2
.set	macro
.set	reorder

$L65:
sw	$5,5168($16)
sw	$3,5184($16)
#APP
# 236 "vp5.c" 1
.word	0b01110000000000100000001011101110	#S32M2I XR11, $2
# 0 "" 2
#NO_APP
sra	$2,$2,3
lw	$8,%got(idct_row)($28)
lw	$7,28($sp)
addu	$20,$16,$20
addiu	$2,$2,1
addu	$3,$8,$7
sb	$2,0($3)
lbu	$2,6492($20)
sltu	$3,$2,25
.set	noreorder
.set	nomacro
bne	$3,$0,$L79
slt	$3,$12,$2
.set	macro
.set	reorder

slt	$2,$12,24
.set	noreorder
.set	nomacro
beq	$2,$0,$L71
sb	$12,6492($20)
.set	macro
.set	reorder

li	$2,24			# 0x18
$L68:
li	$5,1			# 0x1
lw	$25,%call16(memset)($28)
slt	$6,$2,$12
subu	$3,$5,$12
addu	$4,$18,$12
addu	$2,$3,$2
addiu	$4,$4,6236
movz	$5,$2,$6
addu	$4,$16,$4
move	$6,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$5,5			# 0x5
.set	macro
.set	reorder

lw	$28,16($sp)
$L71:
lw	$3,36($sp)
$L96:
addu	$18,$16,$18
lw	$7,28($sp)
lw	$8,36($sp)
lbu	$4,6236($18)
lw	$2,0($3)
addiu	$7,$7,1
lw	$3,5292($16)
addiu	$8,$8,4
sll	$5,$2,2
sw	$7,28($sp)
sll	$2,$2,4
sw	$8,36($sp)
lw	$7,32($sp)
subu	$2,$2,$5
lw	$8,28($sp)
addu	$2,$3,$2
addiu	$7,$7,1
sb	$4,0($2)
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
beq	$8,$2,$L95
sw	$7,32($sp)
.set	macro
.set	reorder

slt	$2,$8,4
.set	noreorder
.set	nomacro
bne	$2,$0,$L105
lw	$7,44($sp)
.set	macro
.set	reorder

li	$3,1			# 0x1
li	$7,4			# 0x4
li	$8,16			# 0x10
sw	$3,40($sp)
sw	$7,48($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L35
.option	pic2
sw	$8,44($sp)
.set	macro
.set	reorder

$L55:
addu	$2,$16,$18
sw	$3,5184($16)
li	$7,2			# 0x2
addu	$2,$2,$12
sb	$7,6236($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L78
.option	pic2
li	$7,2			# 0x2
.set	macro
.set	reorder

$L79:
.set	noreorder
.set	nomacro
bne	$3,$0,$L68
sb	$12,6492($20)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L96
.option	pic2
lw	$3,36($sp)
.set	macro
.set	reorder

$L95:
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

.end	vp5_parse_coeff
.size	vp5_parse_coeff, .-vp5_parse_coeff
.section	.text.unlikely,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_decode_init
.type	vp5_decode_init, @function
vp5_decode_init:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
li	$5,1			# 0x1
sw	$31,28($sp)
move	$6,$0
sw	$16,24($sp)
.cprestore	16
lw	$25,%call16(ff_vp56_init)($28)
.reloc	1f,R_MIPS_JALR,ff_vp56_init
1:	jalr	$25
lw	$16,136($4)

lui	$2,%hi(vp5_coord_div)
lui	$3,%hi(vp5_parse_coeff_models)
lw	$31,28($sp)
addiu	$2,$2,%lo(vp5_coord_div)
addiu	$3,$3,%lo(vp5_parse_coeff_models)
sw	$2,6528($16)
lui	$2,%hi(vp5_parse_vector_adjustment)
sw	$3,6552($16)
lui	$3,%hi(vp5_parse_header)
addiu	$2,$2,%lo(vp5_parse_vector_adjustment)
addiu	$3,$3,%lo(vp5_parse_header)
sw	$2,6532($16)
lui	$2,%hi(vp5_parse_coeff)
sw	$3,6556($16)
addiu	$2,$2,%lo(vp5_parse_coeff)
sw	$2,6540($16)
lui	$2,%hi(vp5_default_models_init)
addiu	$2,$2,%lo(vp5_default_models_init)
sw	$2,6544($16)
lui	$2,%hi(vp5_parse_vector_models)
addiu	$2,$2,%lo(vp5_parse_vector_models)
sw	$2,6548($16)
move	$2,$0
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	vp5_decode_init
.size	vp5_decode_init, .-vp5_decode_init
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_default_models_init
.type	vp5_default_models_init, @function
vp5_default_models_init:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$4,6560($4)
li	$5,-128			# 0xffffffffffffff80
addiu	$28,$28,%lo(__gnu_local_gp)
li	$6,85			# 0x55
addiu	$2,$4,1812
sb	$5,128($4)
sb	$5,130($4)
sb	$6,132($4)
sb	$5,133($4)
sb	$5,129($4)
sb	$5,131($4)
sb	$6,134($4)
sb	$5,135($4)
lw	$3,%got(vp56_def_mb_types_stats)($28)
addiu	$9,$3,48
$L109:
lwl	$8,3($3)
lwl	$7,7($3)
lwl	$6,11($3)
lwl	$5,15($3)
lwr	$8,0($3)
lwr	$7,4($3)
lwr	$6,8($3)
lwr	$5,12($3)
addiu	$3,$3,16
swl	$8,3($2)
swr	$8,0($2)
swl	$7,7($2)
swr	$7,4($2)
swl	$6,11($2)
swr	$6,8($2)
swl	$5,15($2)
swr	$5,12($2)
bne	$3,$9,$L109
addiu	$2,$2,16

lwl	$9,3($3)
li	$5,-128			# 0xffffffffffffff80
lwl	$8,7($3)
lwl	$7,11($3)
lwr	$9,0($3)
lwr	$8,4($3)
lwr	$7,8($3)
swl	$9,3($2)
swr	$9,0($2)
swl	$8,7($2)
swr	$8,4($2)
swl	$7,11($2)
swr	$7,8($2)
sb	$5,136($4)
sb	$5,137($4)
sb	$5,138($4)
sb	$5,139($4)
sb	$5,140($4)
sb	$5,141($4)
sb	$5,142($4)
sb	$5,143($4)
sb	$5,144($4)
sb	$5,145($4)
sb	$5,146($4)
sb	$5,147($4)
sb	$5,148($4)
j	$31
sb	$5,149($4)

.set	macro
.set	reorder
.end	vp5_default_models_init
.size	vp5_default_models_init, .-vp5_default_models_init
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_parse_coeff_models
.type	vp5_parse_coeff_models, @function
vp5_parse_coeff_models:
.frame	$sp,72,$31		# vars= 24, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$3,-32640			# 0xffffffffffff8080
lw	$6,5172($4)
addiu	$sp,$sp,-72
lui	$28,%hi(__gnu_local_gp)
li	$2,-2139095040			# 0xffffffff80800000
sh	$3,16($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,36($sp)
ori	$2,$2,0x8080
sw	$20,52($sp)
li	$3,-128			# 0xffffffffffffff80
sw	$fp,68($sp)
sw	$2,8($sp)
lui	$14,%hi(vp5_dccv_pct)
sw	$2,12($sp)
move	$13,$0
sb	$3,18($sp)
addiu	$14,$14,%lo(vp5_dccv_pct)
sw	$23,64($sp)
addiu	$20,$sp,8
sw	$22,60($sp)
addiu	$24,$sp,19
sw	$21,56($sp)
li	$15,22			# 0x16
sw	$19,48($sp)
move	$5,$6
sw	$18,44($sp)
sw	$17,40($sp)
.cprestore	0
lw	$2,5184($4)
lw	$11,%got(ff_vp56_norm_shift)($28)
lw	$16,6560($4)
lw	$3,5168($4)
move	$6,$2
addu	$10,$14,$13
$L164:
addu	$12,$16,$13
move	$9,$20
$L120:
addu	$2,$11,$3
lbu	$17,0($10)
lbu	$7,0($2)
sll	$8,$3,$7
addu	$5,$7,$5
sll	$6,$6,$7
bltz	$5,$L114
sw	$8,5168($4)

lw	$3,5176($4)
lw	$2,5180($4)
sltu	$2,$3,$2
beq	$2,$0,$L114
addiu	$2,$3,2

sw	$2,5176($4)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$5
addiu	$5,$5,-16
or	$6,$6,$2
$L114:
addiu	$3,$8,-1
mul	$3,$3,$17
sra	$3,$3,8
addiu	$3,$3,1
sll	$2,$3,16
sltu	$7,$6,$2
bne	$7,$0,$L115
sw	$5,5172($4)

subu	$6,$6,$2
li	$18,7			# 0x7
subu	$2,$8,$3
sw	$6,5184($4)
.option	pic0
j	$L118
.option	pic2
move	$8,$0

$L157:
subu	$6,$6,$19
sw	$2,5168($4)
addiu	$18,$18,-1
or	$8,$8,$17
beq	$18,$0,$L156
sw	$6,5184($4)

$L118:
addu	$3,$11,$2
sll	$17,$8,1
lbu	$8,0($3)
sll	$2,$2,$8
addu	$5,$8,$5
addiu	$7,$2,1
sw	$2,5168($4)
sll	$6,$6,$8
bltz	$5,$L116
sra	$7,$7,1

lw	$8,5176($4)
lw	$3,5180($4)
sltu	$3,$8,$3
beq	$3,$0,$L116
addiu	$19,$8,2

sw	$19,5176($4)
lbu	$3,1($8)
lbu	$8,0($8)
sll	$3,$3,8
or	$3,$3,$8
sll	$8,$3,8
srl	$3,$3,8
or	$3,$8,$3
andi	$3,$3,0xffff
sll	$3,$3,$5
addiu	$5,$5,-16
or	$6,$6,$3
$L116:
sll	$19,$7,16
sw	$5,5172($4)
sltu	$8,$6,$19
xori	$8,$8,0x1
bne	$8,$0,$L157
subu	$2,$2,$7

move	$2,$7
sw	$6,5184($4)
addiu	$18,$18,-1
sw	$2,5168($4)
bne	$18,$0,$L118
or	$8,$8,$17

$L156:
sll	$8,$8,1
addiu	$9,$9,1
sltu	$2,$8,1
addu	$8,$8,$2
addiu	$10,$10,1
andi	$2,$8,0x00ff
addiu	$12,$12,1
sb	$2,-1($9)
sb	$2,165($12)
lw	$3,5168($4)
lw	$5,5172($4)
bne	$24,$9,$L120
lw	$6,5184($4)

addiu	$13,$13,11
$L163:
bne	$13,$15,$L164
addu	$10,$14,$13

lui	$19,%hi(vp5_ract_pct)
move	$fp,$0
addiu	$19,$19,%lo(vp5_ract_pct)
li	$21,66			# 0x42
move	$2,$6
move	$6,$5
$L121:
sll	$5,$fp,1
move	$23,$0
move	$25,$fp
sw	$5,24($sp)
$L135:
addu	$22,$23,$5
move	$17,$0
addiu	$18,$25,188
move	$10,$6
$L132:
addu	$8,$17,$22
addu	$9,$18,$17
addu	$8,$19,$8
addu	$9,$16,$9
move	$7,$20
move	$5,$3
move	$12,$10
$L130:
addu	$3,$11,$5
lbu	$6,0($8)
lbu	$3,0($3)
sll	$5,$5,$3
addu	$12,$3,$12
sll	$2,$2,$3
bltz	$12,$L122
sw	$5,5168($4)

lw	$10,5176($4)
lw	$3,5180($4)
sltu	$3,$10,$3
beq	$3,$0,$L165
addiu	$3,$5,-1

addiu	$3,$10,2
sw	$3,5176($4)
lbu	$3,1($10)
lbu	$10,0($10)
sll	$3,$3,8
or	$3,$3,$10
sll	$10,$3,8
srl	$3,$3,8
or	$3,$10,$3
andi	$3,$3,0xffff
sll	$3,$3,$12
addiu	$12,$12,-16
or	$2,$2,$3
$L122:
addiu	$3,$5,-1
$L165:
mul	$3,$3,$6
sra	$3,$3,8
addiu	$3,$3,1
sll	$6,$3,16
sltu	$10,$2,$6
bne	$10,$0,$L123
sw	$12,5172($4)

subu	$2,$2,$6
subu	$3,$5,$3
li	$13,7			# 0x7
move	$5,$0
.option	pic0
j	$L126
.option	pic2
sw	$2,5184($4)

$L159:
subu	$2,$2,$14
sw	$3,5168($4)
addiu	$13,$13,-1
or	$5,$10,$5
beq	$13,$0,$L158
sw	$2,5184($4)

$L126:
addu	$6,$11,$3
sll	$5,$5,1
lbu	$10,0($6)
sll	$3,$3,$10
addu	$12,$10,$12
addiu	$6,$3,1
sw	$3,5168($4)
sll	$2,$2,$10
bltz	$12,$L124
sra	$6,$6,1

lw	$14,5176($4)
lw	$10,5180($4)
sltu	$10,$14,$10
beq	$10,$0,$L124
addiu	$15,$14,2

sw	$15,5176($4)
lbu	$10,1($14)
lbu	$14,0($14)
sll	$10,$10,8
or	$10,$10,$14
sll	$14,$10,8
srl	$10,$10,8
or	$10,$14,$10
andi	$10,$10,0xffff
sll	$10,$10,$12
addiu	$12,$12,-16
or	$2,$2,$10
$L124:
sll	$14,$6,16
sw	$12,5172($4)
sltu	$10,$2,$14
xori	$10,$10,0x1
bne	$10,$0,$L159
subu	$3,$3,$6

move	$3,$6
sw	$2,5184($4)
addiu	$13,$13,-1
sw	$3,5168($4)
bne	$13,$0,$L126
or	$5,$10,$5

$L158:
sll	$5,$5,1
addiu	$7,$7,1
sltu	$2,$5,1
addu	$5,$5,$2
addiu	$8,$8,1
andi	$5,$5,0x00ff
addiu	$9,$9,1
sb	$5,-1($7)
bne	$24,$7,$L128
sb	$5,-1($9)

$L161:
addiu	$17,$17,11
beq	$17,$21,$L129
nop

lw	$3,5168($4)
lw	$10,5172($4)
.option	pic0
j	$L132
.option	pic2
lw	$2,5184($4)

$L115:
lw	$2,5136($4)
sw	$3,5168($4)
sw	$6,5184($4)
lw	$2,48($2)
beq	$2,$0,$L119
nop

lbu	$2,0($9)
sb	$2,166($12)
lw	$3,5168($4)
lw	$5,5172($4)
lw	$6,5184($4)
$L119:
addiu	$9,$9,1
addiu	$10,$10,1
bne	$24,$9,$L120
addiu	$12,$12,1

.option	pic0
j	$L163
.option	pic2
addiu	$13,$13,11

$L123:
lw	$5,5136($4)
sw	$2,5184($4)
sw	$3,5168($4)
lw	$2,48($5)
beq	$2,$0,$L127
nop

lbu	$2,0($7)
sb	$2,0($9)
$L127:
addiu	$7,$7,1
addiu	$8,$8,1
beq	$24,$7,$L161
addiu	$9,$9,1

$L128:
lw	$5,5168($4)
lw	$12,5172($4)
.option	pic0
j	$L130
.option	pic2
lw	$2,5184($4)

$L129:
addiu	$23,$23,66
li	$2,132			# 0x84
beq	$23,$2,$L131
addiu	$25,$25,198

lw	$3,5168($4)
lw	$6,5172($4)
lw	$2,5184($4)
.option	pic0
j	$L135
.option	pic2
lw	$5,24($sp)

$L131:
addiu	$fp,$fp,66
li	$18,198			# 0xc6
beq	$fp,$18,$L162
lui	$8,%hi(vp5_dccv_lc+144)

lw	$3,5168($4)
lw	$6,5172($4)
.option	pic0
j	$L121
.option	pic2
lw	$2,5184($4)

$L162:
lui	$15,%hi(vp5_dccv_lc)
addiu	$14,$16,166
addiu	$17,$16,1124
addiu	$23,$16,188
addiu	$15,$15,%lo(vp5_dccv_lc)
addiu	$8,$8,%lo(vp5_dccv_lc+144)
li	$7,254			# 0xfe
li	$6,1			# 0x1
$L134:
lbu	$12,0($14)
move	$3,$15
lbu	$11,1($14)
move	$4,$17
lbu	$10,2($14)
lbu	$9,3($14)
lbu	$5,4($14)
$L136:
lh	$2,0($3)
addiu	$3,$3,4
move	$18,$7
addiu	$4,$4,5
mul	$2,$2,$12
lh	$13,-2($3)
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$13
slt	$13,$2,255
movn	$18,$2,$13
slt	$2,$0,$2
move	$13,$18
movz	$13,$6,$2
move	$18,$7
sb	$13,-5($4)
lh	$2,140($3)
lh	$13,142($3)
mul	$2,$2,$11
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$13
slt	$13,$2,255
movn	$18,$2,$13
slt	$2,$0,$2
move	$13,$18
movz	$13,$6,$2
move	$18,$7
sb	$13,-4($4)
lh	$2,284($3)
lh	$13,286($3)
mul	$2,$2,$10
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$13
slt	$13,$2,255
movn	$18,$2,$13
slt	$2,$0,$2
move	$13,$18
movz	$13,$6,$2
move	$18,$7
sb	$13,-3($4)
lh	$2,428($3)
lh	$13,430($3)
mul	$2,$2,$9
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$13
slt	$13,$2,255
movn	$18,$2,$13
slt	$2,$0,$2
move	$13,$18
movz	$13,$6,$2
move	$18,$7
sb	$13,-2($4)
lh	$2,572($3)
lh	$13,574($3)
mul	$2,$2,$5
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$13
slt	$13,$2,255
movn	$18,$2,$13
slt	$2,$0,$2
move	$13,$18
movz	$13,$6,$2
bne	$3,$8,$L136
sb	$13,-1($4)

addiu	$14,$14,11
bne	$14,$23,$L134
addiu	$17,$17,180

lui	$22,%hi(vp5_ract_lc)
addiu	$25,$16,584
addiu	$22,$22,%lo(vp5_ract_lc)
addiu	$16,$16,386
li	$7,254			# 0xfe
li	$6,1			# 0x1
li	$19,3			# 0x3
$L137:
li	$21,2			# 0x2
move	$20,$25
move	$18,$23
$L143:
move	$15,$0
move	$17,$20
move	$14,$22
move	$5,$18
$L141:
lbu	$13,0($5)
addiu	$12,$14,24
lbu	$11,1($5)
move	$4,$17
lbu	$10,2($5)
move	$3,$14
lbu	$9,3($5)
lbu	$8,4($5)
$L138:
lh	$2,0($3)
addiu	$3,$3,4
move	$fp,$7
addiu	$4,$4,5
mul	$2,$2,$13
lh	$24,-2($3)
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$24
slt	$24,$2,255
movn	$fp,$2,$24
slt	$2,$0,$2
move	$24,$fp
movz	$24,$6,$2
move	$fp,$7
sb	$24,-5($4)
lh	$2,20($3)
lh	$24,22($3)
mul	$2,$2,$11
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$24
slt	$24,$2,255
movn	$fp,$2,$24
slt	$2,$0,$2
move	$24,$fp
movz	$24,$6,$2
move	$fp,$7
sb	$24,-4($4)
lh	$2,44($3)
lh	$24,46($3)
mul	$2,$2,$10
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$24
slt	$24,$2,255
movn	$fp,$2,$24
slt	$2,$0,$2
move	$24,$fp
movz	$24,$6,$2
move	$fp,$7
sb	$24,-3($4)
lh	$2,68($3)
lh	$24,70($3)
mul	$2,$2,$9
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$24
slt	$24,$2,255
movn	$fp,$2,$24
slt	$2,$0,$2
move	$24,$fp
movz	$24,$6,$2
move	$fp,$7
sb	$24,-2($4)
lh	$2,92($3)
lh	$24,94($3)
mul	$2,$2,$8
addiu	$2,$2,128
sra	$2,$2,8
addu	$2,$2,$24
slt	$24,$2,255
movn	$fp,$2,$24
slt	$2,$0,$2
move	$24,$fp
movz	$24,$6,$2
bne	$3,$12,$L138
sb	$24,-1($4)

addiu	$15,$15,1
addiu	$5,$5,11
addiu	$14,$14,120
bne	$15,$19,$L141
addiu	$17,$17,30

li	$2,1			# 0x1
addiu	$18,$18,198
beq	$21,$2,$L140
addiu	$20,$20,270

.option	pic0
j	$L143
.option	pic2
li	$21,1			# 0x1

$L140:
addiu	$23,$23,66
addiu	$22,$22,360
bne	$23,$16,$L137
addiu	$25,$25,90

lw	$fp,68($sp)
lw	$23,64($sp)
lw	$22,60($sp)
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
j	$31
addiu	$sp,$sp,72

.set	macro
.set	reorder
.end	vp5_parse_coeff_models
.size	vp5_parse_coeff_models, .-vp5_parse_coeff_models
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_parse_vector_models
.type	vp5_parse_vector_models, @function
vp5_parse_vector_models:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$12,6560($4)
lui	$10,%hi(vp5_vmc_pct)
lw	$14,5168($4)
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$2,5172($4)
addiu	$sp,$sp,-8
lw	$15,5184($4)
addiu	$10,$10,%lo(vp5_vmc_pct)
lw	$9,%got(ff_vp56_norm_shift)($28)
addiu	$7,$12,133
addiu	$8,$12,128
sw	$16,4($sp)
addiu	$11,$12,137
move	$6,$10
$L187:
addu	$3,$9,$14
lbu	$16,0($6)
lbu	$5,0($3)
sll	$3,$14,$5
addu	$2,$5,$2
sll	$15,$15,$5
bltz	$2,$L167
sw	$3,5168($4)

lw	$13,5176($4)
lw	$5,5180($4)
sltu	$5,$13,$5
beq	$5,$0,$L167
addiu	$5,$13,2

sw	$5,5176($4)
lbu	$5,1($13)
lbu	$13,0($13)
sll	$5,$5,8
or	$5,$5,$13
sll	$13,$5,8
srl	$5,$5,8
or	$5,$13,$5
andi	$5,$5,0xffff
sll	$5,$5,$2
addiu	$2,$2,-16
or	$15,$15,$5
$L167:
addiu	$13,$3,-1
mul	$13,$13,$16
sra	$13,$13,8
addiu	$13,$13,1
sll	$5,$13,16
sltu	$14,$15,$5
bne	$14,$0,$L168
sw	$2,5172($4)

subu	$15,$15,$5
subu	$13,$3,$13
li	$24,7			# 0x7
move	$5,$0
.option	pic0
j	$L171
.option	pic2
sw	$15,5184($4)

$L215:
subu	$15,$15,$25
sw	$13,5168($4)
addiu	$24,$24,-1
or	$5,$5,$14
beq	$24,$0,$L214
sw	$15,5184($4)

$L171:
addu	$3,$9,$13
sll	$14,$5,1
lbu	$5,0($3)
sll	$13,$13,$5
addu	$2,$5,$2
addiu	$3,$13,1
sw	$13,5168($4)
sll	$15,$15,$5
bltz	$2,$L169
sra	$3,$3,1

lw	$25,5176($4)
lw	$5,5180($4)
sltu	$5,$25,$5
beq	$5,$0,$L169
addiu	$16,$25,2

sw	$16,5176($4)
lbu	$5,1($25)
lbu	$16,0($25)
sll	$5,$5,8
or	$5,$5,$16
sll	$25,$5,8
srl	$5,$5,8
or	$5,$25,$5
andi	$5,$5,0xffff
sll	$5,$5,$2
addiu	$2,$2,-16
or	$15,$15,$5
$L169:
sll	$25,$3,16
sw	$2,5172($4)
sltu	$5,$15,$25
xori	$5,$5,0x1
bne	$5,$0,$L215
subu	$13,$13,$3

move	$13,$3
sw	$15,5184($4)
addiu	$24,$24,-1
sw	$13,5168($4)
bne	$24,$0,$L171
or	$5,$5,$14

$L214:
sll	$5,$5,1
sltu	$2,$5,1
addu	$5,$5,$2
sb	$5,2($8)
lw	$13,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L197
.option	pic2
lw	$14,5184($4)

$L168:
sw	$13,5168($4)
move	$14,$15
sw	$15,5184($4)
$L197:
addu	$5,$9,$13
lbu	$16,1($6)
lbu	$3,0($5)
sll	$13,$13,$3
addu	$2,$3,$2
sll	$15,$14,$3
bltz	$2,$L172
sw	$13,5168($4)

lw	$5,5176($4)
lw	$3,5180($4)
sltu	$3,$5,$3
beq	$3,$0,$L172
addiu	$3,$5,2

sw	$3,5176($4)
lbu	$3,1($5)
lbu	$5,0($5)
sll	$3,$3,8
or	$3,$3,$5
sll	$14,$3,8
srl	$3,$3,8
or	$3,$14,$3
andi	$3,$3,0xffff
sll	$3,$3,$2
addiu	$2,$2,-16
or	$15,$15,$3
$L172:
addiu	$5,$13,-1
mul	$5,$5,$16
sra	$5,$5,8
addiu	$5,$5,1
sll	$3,$5,16
sltu	$14,$15,$3
bne	$14,$0,$L173
sw	$2,5172($4)

subu	$14,$15,$3
subu	$5,$13,$5
li	$24,7			# 0x7
move	$13,$0
.option	pic0
j	$L176
.option	pic2
sw	$14,5184($4)

$L217:
subu	$14,$14,$25
sw	$5,5168($4)
addiu	$24,$24,-1
or	$13,$13,$15
beq	$24,$0,$L216
sw	$14,5184($4)

$L176:
addu	$3,$9,$5
sll	$15,$13,1
lbu	$13,0($3)
sll	$5,$5,$13
addu	$2,$13,$2
addiu	$3,$5,1
sw	$5,5168($4)
sll	$14,$14,$13
bltz	$2,$L174
sra	$3,$3,1

lw	$25,5176($4)
lw	$13,5180($4)
sltu	$13,$25,$13
beq	$13,$0,$L174
addiu	$16,$25,2

sw	$16,5176($4)
lbu	$13,1($25)
lbu	$16,0($25)
sll	$13,$13,8
or	$13,$13,$16
sll	$25,$13,8
srl	$13,$13,8
or	$13,$25,$13
andi	$13,$13,0xffff
sll	$13,$13,$2
addiu	$2,$2,-16
or	$14,$14,$13
$L174:
sll	$25,$3,16
sw	$2,5172($4)
sltu	$13,$14,$25
xori	$13,$13,0x1
bne	$13,$0,$L217
subu	$5,$5,$3

move	$5,$3
sw	$14,5184($4)
addiu	$24,$24,-1
sw	$5,5168($4)
bne	$24,$0,$L176
or	$13,$13,$15

$L216:
sll	$13,$13,1
sltu	$2,$13,1
addu	$13,$13,$2
sb	$13,0($8)
lw	$5,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L198
.option	pic2
lw	$14,5184($4)

$L173:
sw	$5,5168($4)
move	$14,$15
sw	$15,5184($4)
$L198:
addu	$3,$9,$5
lbu	$16,2($6)
lbu	$13,0($3)
sll	$5,$5,$13
addu	$2,$13,$2
sll	$14,$14,$13
bltz	$2,$L177
sw	$5,5168($4)

lw	$15,5176($4)
lw	$3,5180($4)
sltu	$3,$15,$3
beq	$3,$0,$L225
addiu	$3,$5,-1

addiu	$3,$15,2
sw	$3,5176($4)
lbu	$13,1($15)
lbu	$3,0($15)
sll	$13,$13,8
or	$13,$13,$3
sll	$15,$13,8
srl	$13,$13,8
or	$13,$15,$13
andi	$13,$13,0xffff
sll	$13,$13,$2
addiu	$2,$2,-16
or	$14,$14,$13
$L177:
addiu	$3,$5,-1
$L225:
mul	$3,$3,$16
sra	$3,$3,8
addiu	$3,$3,1
sll	$13,$3,16
sltu	$15,$14,$13
bne	$15,$0,$L178
sw	$2,5172($4)

subu	$13,$14,$13
subu	$3,$5,$3
li	$24,7			# 0x7
move	$14,$0
.option	pic0
j	$L181
.option	pic2
sw	$13,5184($4)

$L219:
subu	$13,$13,$25
sw	$3,5168($4)
addiu	$24,$24,-1
or	$14,$14,$15
beq	$24,$0,$L218
sw	$13,5184($4)

$L181:
addu	$5,$9,$3
sll	$15,$14,1
lbu	$14,0($5)
sll	$3,$3,$14
addu	$2,$14,$2
addiu	$5,$3,1
sw	$3,5168($4)
sll	$13,$13,$14
bltz	$2,$L179
sra	$5,$5,1

lw	$25,5176($4)
lw	$14,5180($4)
sltu	$14,$25,$14
beq	$14,$0,$L179
addiu	$16,$25,2

sw	$16,5176($4)
lbu	$14,1($25)
lbu	$16,0($25)
sll	$14,$14,8
or	$14,$14,$16
sll	$25,$14,8
srl	$14,$14,8
or	$14,$25,$14
andi	$14,$14,0xffff
sll	$14,$14,$2
addiu	$2,$2,-16
or	$13,$13,$14
$L179:
sll	$25,$5,16
sw	$2,5172($4)
sltu	$14,$13,$25
xori	$14,$14,0x1
bne	$14,$0,$L219
subu	$3,$3,$5

move	$3,$5
sw	$13,5184($4)
addiu	$24,$24,-1
sw	$3,5168($4)
bne	$24,$0,$L181
or	$14,$14,$15

$L218:
sll	$14,$14,1
sltu	$2,$14,1
addu	$14,$14,$2
sb	$14,-1($7)
lw	$3,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L199
.option	pic2
lw	$13,5184($4)

$L178:
sw	$3,5168($4)
move	$13,$14
sw	$14,5184($4)
$L199:
addu	$14,$9,$3
lbu	$16,3($6)
lbu	$5,0($14)
sll	$3,$3,$5
addu	$2,$5,$2
sll	$13,$13,$5
bltz	$2,$L182
sw	$3,5168($4)

lw	$14,5176($4)
lw	$5,5180($4)
sltu	$5,$14,$5
beq	$5,$0,$L182
addiu	$5,$14,2

sw	$5,5176($4)
lbu	$5,1($14)
lbu	$14,0($14)
sll	$5,$5,8
or	$5,$5,$14
sll	$15,$5,8
srl	$5,$5,8
or	$5,$15,$5
andi	$5,$5,0xffff
sll	$5,$5,$2
addiu	$2,$2,-16
or	$13,$13,$5
$L182:
addiu	$14,$3,-1
mul	$14,$14,$16
sra	$14,$14,8
addiu	$14,$14,1
sll	$5,$14,16
sltu	$15,$13,$5
bne	$15,$0,$L183
sw	$2,5172($4)

subu	$5,$13,$5
subu	$3,$3,$14
li	$24,7			# 0x7
move	$14,$0
.option	pic0
j	$L186
.option	pic2
sw	$5,5184($4)

$L221:
subu	$5,$5,$25
sw	$3,5168($4)
addiu	$24,$24,-1
or	$14,$14,$15
beq	$24,$0,$L220
sw	$5,5184($4)

$L186:
addu	$13,$9,$3
sll	$15,$14,1
lbu	$14,0($13)
sll	$3,$3,$14
addu	$2,$14,$2
addiu	$13,$3,1
sw	$3,5168($4)
sll	$5,$5,$14
bltz	$2,$L184
sra	$13,$13,1

lw	$25,5176($4)
lw	$14,5180($4)
sltu	$14,$25,$14
beq	$14,$0,$L184
addiu	$16,$25,2

sw	$16,5176($4)
lbu	$14,1($25)
lbu	$16,0($25)
sll	$14,$14,8
or	$14,$14,$16
sll	$25,$14,8
srl	$14,$14,8
or	$14,$25,$14
andi	$14,$14,0xffff
sll	$14,$14,$2
addiu	$2,$2,-16
or	$5,$5,$14
$L184:
sll	$25,$13,16
sw	$2,5172($4)
sltu	$14,$5,$25
xori	$14,$14,0x1
bne	$14,$0,$L221
subu	$3,$3,$13

move	$3,$13
sw	$5,5184($4)
addiu	$24,$24,-1
sw	$3,5168($4)
bne	$24,$0,$L186
or	$14,$14,$15

$L220:
sll	$14,$14,1
sltu	$2,$14,1
addu	$14,$14,$2
sb	$14,0($7)
lw	$14,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L200
.option	pic2
lw	$15,5184($4)

$L183:
sw	$14,5168($4)
move	$15,$13
sw	$13,5184($4)
$L200:
addiu	$7,$7,2
addiu	$6,$6,11
bne	$7,$11,$L187
addiu	$8,$8,1

addiu	$13,$12,14
$L188:
move	$11,$0
$L196:
addu	$6,$9,$14
addu	$3,$10,$11
lbu	$5,0($6)
lbu	$7,4($3)
sll	$14,$14,$5
addu	$2,$5,$2
sll	$15,$15,$5
bltz	$2,$L189
sw	$14,5168($4)

lw	$3,5176($4)
lw	$5,5180($4)
sltu	$5,$3,$5
beq	$5,$0,$L189
addiu	$5,$3,2

sw	$5,5176($4)
lbu	$6,1($3)
lbu	$3,0($3)
sll	$6,$6,8
or	$6,$6,$3
sll	$5,$6,8
srl	$6,$6,8
or	$6,$5,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$15,$15,$6
$L189:
addiu	$3,$14,-1
mul	$3,$3,$7
sra	$3,$3,8
addiu	$3,$3,1
sll	$5,$3,16
sltu	$6,$15,$5
bne	$6,$0,$L190
sw	$2,5172($4)

subu	$5,$15,$5
subu	$3,$14,$3
move	$7,$0
li	$14,7			# 0x7
.option	pic0
j	$L193
.option	pic2
sw	$5,5184($4)

$L223:
subu	$5,$5,$15
sw	$3,5168($4)
addiu	$14,$14,-1
or	$7,$7,$8
beq	$14,$0,$L222
sw	$5,5184($4)

$L193:
addu	$6,$9,$3
sll	$8,$7,1
lbu	$7,0($6)
sll	$3,$3,$7
addu	$2,$7,$2
addiu	$6,$3,1
sw	$3,5168($4)
sll	$5,$5,$7
bltz	$2,$L191
sra	$6,$6,1

lw	$15,5176($4)
lw	$7,5180($4)
sltu	$7,$15,$7
beq	$7,$0,$L191
addiu	$24,$15,2

sw	$24,5176($4)
lbu	$7,1($15)
lbu	$15,0($15)
sll	$7,$7,8
or	$7,$7,$15
sll	$15,$7,8
srl	$7,$7,8
or	$7,$15,$7
andi	$7,$7,0xffff
sll	$7,$7,$2
addiu	$2,$2,-16
or	$5,$5,$7
$L191:
sll	$15,$6,16
sw	$2,5172($4)
sltu	$7,$5,$15
xori	$7,$7,0x1
bne	$7,$0,$L223
subu	$3,$3,$6

move	$3,$6
sw	$5,5184($4)
addiu	$14,$14,-1
sw	$3,5168($4)
bne	$14,$0,$L193
or	$7,$7,$8

$L222:
sll	$7,$7,1
addu	$2,$12,$11
sltu	$3,$7,1
addu	$7,$7,$3
addiu	$11,$11,1
sb	$7,136($2)
li	$2,7			# 0x7
bne	$11,$2,$L194
nop

$L224:
addiu	$12,$12,7
beq	$12,$13,$L166
addiu	$10,$10,11

lw	$14,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L188
.option	pic2
lw	$15,5184($4)

$L190:
addiu	$11,$11,1
sw	$3,5168($4)
li	$2,7			# 0x7
beq	$11,$2,$L224
sw	$15,5184($4)

$L194:
lw	$14,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L196
.option	pic2
lw	$15,5184($4)

$L166:
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	vp5_parse_vector_models
.size	vp5_parse_vector_models, .-vp5_parse_vector_models
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"interlacing not supported\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vp5_parse_header
.type	vp5_parse_header, @function
vp5_parse_header:
.frame	$sp,40,$31		# vars= 0, regs= 3/0, args= 16, gp= 8
.mask	0x80030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-40
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,28($sp)
move	$16,$4
sw	$17,32($sp)
.cprestore	16
sw	$31,36($sp)
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
lw	$17,%got(ff_vp56_norm_shift)($28)
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
addiu	$4,$4,5168

lw	$5,5168($16)
lw	$4,5172($16)
lw	$2,5184($16)
addu	$3,$17,$5
lw	$28,16($sp)
lw	$8,5136($16)
lbu	$3,0($3)
sll	$5,$5,$3
addu	$4,$3,$4
sll	$3,$2,$3
bltz	$4,$L227
sw	$5,5168($16)

lw	$6,5176($16)
lw	$2,5180($16)
sltu	$2,$6,$2
bne	$2,$0,$L296
addiu	$2,$6,2

$L227:
addiu	$2,$5,1
sra	$2,$2,1
sll	$7,$2,16
sltu	$6,$3,$7
xori	$6,$6,0x1
beq	$6,$0,$L228
sw	$4,5172($16)

subu	$2,$5,$2
subu	$3,$3,$7
$L228:
addu	$5,$17,$2
sw	$3,5184($16)
xori	$6,$6,0x1
lbu	$7,0($5)
sw	$6,48($8)
sll	$5,$2,$7
addu	$4,$4,$7
sll	$3,$3,$7
bltz	$4,$L229
sw	$5,5168($16)

lw	$6,5176($16)
lw	$2,5180($16)
sltu	$2,$6,$2
bne	$2,$0,$L297
addiu	$2,$6,2

$L229:
addiu	$2,$5,1
sra	$2,$2,1
sll	$6,$2,16
sltu	$7,$3,$6
bne	$7,$0,$L230
sw	$4,5172($16)

subu	$2,$5,$2
subu	$3,$3,$6
$L230:
li	$8,6			# 0x6
sw	$3,5184($16)
.option	pic0
j	$L233
.option	pic2
move	$5,$0

$L299:
subu	$3,$3,$9
sw	$2,5168($16)
addiu	$8,$8,-1
or	$5,$7,$5
beq	$8,$0,$L298
sw	$3,5184($16)

$L233:
addu	$6,$17,$2
sll	$5,$5,1
lbu	$7,0($6)
sll	$2,$2,$7
addu	$4,$7,$4
addiu	$6,$2,1
sw	$2,5168($16)
sll	$3,$3,$7
bltz	$4,$L231
sra	$6,$6,1

lw	$9,5176($16)
lw	$7,5180($16)
sltu	$7,$9,$7
beq	$7,$0,$L231
addiu	$10,$9,2

sw	$10,5176($16)
lbu	$7,1($9)
lbu	$9,0($9)
sll	$7,$7,8
or	$7,$7,$9
sll	$9,$7,8
srl	$7,$7,8
or	$7,$9,$7
andi	$7,$7,0xffff
sll	$7,$7,$4
addiu	$4,$4,-16
or	$3,$3,$7
$L231:
sll	$9,$6,16
sw	$4,5172($16)
sltu	$7,$3,$9
xori	$7,$7,0x1
bne	$7,$0,$L299
subu	$2,$2,$6

move	$2,$6
sw	$3,5184($16)
addiu	$8,$8,-1
sw	$2,5168($16)
bne	$8,$0,$L233
or	$5,$7,$5

$L298:
lw	$25,%call16(ff_vp56_init_dequant)($28)
.reloc	1f,R_MIPS_JALR,ff_vp56_init_dequant
1:	jalr	$25
move	$4,$16

lw	$2,5136($16)
lw	$2,48($2)
bne	$2,$0,$L300
lw	$28,16($sp)

lw	$2,5392($16)
sltu	$2,$0,$2
$L292:
lw	$31,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,40

$L297:
sw	$2,5176($16)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$2,$2,$6
sll	$6,$2,8
srl	$2,$2,8
or	$2,$6,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
.option	pic0
j	$L229
.option	pic2
or	$3,$3,$2

$L296:
sw	$2,5176($16)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$2,$2,$6
sll	$6,$2,8
srl	$2,$2,8
or	$2,$6,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
.option	pic0
j	$L227
.option	pic2
or	$3,$3,$2

$L300:
lw	$2,5168($16)
li	$7,8			# 0x8
lw	$5,5172($16)
.option	pic0
j	$L237
.option	pic2
lw	$6,5184($16)

$L302:
subu	$3,$3,$6
sw	$2,5168($16)
addiu	$7,$7,-1
move	$5,$4
sw	$3,5184($16)
beq	$7,$0,$L301
move	$6,$3

$L237:
addu	$3,$17,$2
lbu	$3,0($3)
sll	$2,$2,$3
addu	$4,$3,$5
addiu	$5,$2,1
sw	$2,5168($16)
sll	$3,$6,$3
bltz	$4,$L235
sra	$5,$5,1

lw	$8,5176($16)
lw	$6,5180($16)
sltu	$6,$8,$6
beq	$6,$0,$L235
addiu	$9,$8,2

sw	$9,5176($16)
lbu	$6,1($8)
lbu	$8,0($8)
sll	$6,$6,8
or	$6,$6,$8
sll	$8,$6,8
srl	$6,$6,8
or	$6,$8,$6
andi	$6,$6,0xffff
sll	$6,$6,$4
addiu	$4,$4,-16
or	$3,$3,$6
$L235:
sll	$6,$5,16
sw	$4,5172($16)
sltu	$8,$3,$6
beq	$8,$0,$L302
subu	$2,$2,$5

move	$2,$5
sw	$3,5184($16)
addiu	$7,$7,-1
sw	$2,5168($16)
move	$5,$4
bne	$7,$0,$L237
move	$6,$3

$L301:
li	$8,5			# 0x5
.option	pic0
j	$L240
.option	pic2
move	$5,$0

$L304:
subu	$6,$6,$9
sw	$2,5168($16)
addiu	$8,$8,-1
or	$5,$4,$5
sw	$6,5184($16)
move	$4,$7
beq	$8,$0,$L303
move	$3,$6

$L240:
addu	$6,$17,$2
sll	$5,$5,1
lbu	$6,0($6)
sll	$2,$2,$6
addu	$7,$6,$4
addiu	$4,$2,1
sll	$6,$3,$6
sw	$2,5168($16)
bltz	$7,$L238
sra	$3,$4,1

lw	$9,5176($16)
lw	$4,5180($16)
sltu	$4,$9,$4
beq	$4,$0,$L238
addiu	$10,$9,2

sw	$10,5176($16)
lbu	$4,1($9)
lbu	$9,0($9)
sll	$4,$4,8
or	$4,$4,$9
sll	$9,$4,8
srl	$4,$4,8
or	$4,$9,$4
andi	$4,$4,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$6,$6,$4
$L238:
sll	$9,$3,16
sw	$7,5172($16)
sltu	$4,$6,$9
xori	$4,$4,0x1
bne	$4,$0,$L304
subu	$2,$2,$3

move	$2,$3
sw	$6,5184($16)
addiu	$8,$8,-1
or	$5,$4,$5
sw	$2,5168($16)
move	$4,$7
bne	$8,$0,$L240
move	$3,$6

$L303:
slt	$4,$5,6
bne	$4,$0,$L305
lw	$31,36($sp)

move	$2,$0
lw	$17,32($sp)
lw	$16,28($sp)
j	$31
addiu	$sp,$sp,40

$L305:
addu	$3,$17,$2
lbu	$3,0($3)
sll	$2,$2,$3
addu	$4,$3,$7
sll	$6,$6,$3
bltz	$4,$L266
sw	$2,5168($16)

lw	$7,5176($16)
lw	$3,5180($16)
sltu	$3,$7,$3
bne	$3,$0,$L306
addiu	$3,$7,2

$L266:
addiu	$5,$2,1
sra	$5,$5,1
sll	$3,$5,16
sltu	$7,$6,$3
bne	$7,$0,$L267
sw	$4,5172($16)

subu	$5,$2,$5
subu	$6,$6,$3
$L267:
addu	$2,$17,$5
sw	$6,5184($16)
lbu	$2,0($2)
sll	$5,$5,$2
addu	$4,$2,$4
sll	$2,$6,$2
bltz	$4,$L242
sw	$5,5168($16)

lw	$6,5176($16)
lw	$3,5180($16)
sltu	$3,$6,$3
bne	$3,$0,$L307
addiu	$3,$6,2

$L242:
addiu	$3,$5,1
sra	$3,$3,1
sll	$6,$3,16
sltu	$7,$2,$6
bne	$7,$0,$L243
sw	$4,5172($16)

subu	$3,$5,$3
subu	$2,$2,$6
$L243:
addu	$5,$17,$3
sw	$2,5184($16)
lbu	$7,0($5)
sll	$3,$3,$7
addu	$4,$7,$4
sll	$7,$2,$7
bltz	$4,$L244
sw	$3,5168($16)

lw	$5,5176($16)
lw	$2,5180($16)
sltu	$2,$5,$2
bne	$2,$0,$L308
addiu	$2,$5,2

$L244:
addiu	$2,$3,1
sra	$2,$2,1
sll	$5,$2,16
sltu	$6,$7,$5
bne	$6,$0,$L245
sw	$4,5172($16)

subu	$2,$3,$2
lw	$25,%call16(av_log)($28)
subu	$7,$7,$5
lw	$4,0($16)
lui	$6,%hi($LC0)
li	$5,16			# 0x10
sw	$2,5168($16)
addiu	$6,$6,%lo($LC0)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$7,5184($16)

.option	pic0
j	$L292
.option	pic2
move	$2,$0

$L245:
sw	$7,5184($16)
li	$10,8			# 0x8
.option	pic0
j	$L249
.option	pic2
move	$6,$0

$L310:
subu	$3,$3,$7
$L248:
addiu	$10,$10,-1
sw	$2,5168($16)
or	$6,$5,$6
sw	$3,5184($16)
move	$4,$8
beq	$10,$0,$L309
move	$7,$3

$L249:
addu	$3,$17,$2
sll	$6,$6,1
lbu	$3,0($3)
sll	$2,$2,$3
addu	$8,$3,$4
addiu	$4,$2,1
sw	$2,5168($16)
sll	$3,$7,$3
bltz	$8,$L247
sra	$4,$4,1

lw	$9,5176($16)
lw	$7,5180($16)
sltu	$7,$9,$7
beq	$7,$0,$L247
addiu	$5,$9,2

sw	$5,5176($16)
lbu	$7,1($9)
lbu	$5,0($9)
sll	$7,$7,8
or	$7,$7,$5
sll	$9,$7,8
srl	$7,$7,8
or	$7,$9,$7
andi	$7,$7,0xffff
sll	$7,$7,$8
addiu	$8,$8,-16
or	$3,$3,$7
$L247:
sll	$7,$4,16
sw	$8,5172($16)
sltu	$5,$3,$7
xori	$5,$5,0x1
bne	$5,$0,$L310
subu	$2,$2,$4

.option	pic0
j	$L248
.option	pic2
move	$2,$4

$L309:
li	$10,8			# 0x8
move	$5,$0
.option	pic0
j	$L252
.option	pic2
move	$7,$8

$L312:
subu	$4,$4,$9
$L251:
addiu	$10,$10,-1
sw	$2,5168($16)
or	$5,$7,$5
sw	$4,5184($16)
move	$7,$8
beq	$10,$0,$L311
move	$3,$4

$L252:
addu	$4,$17,$2
sll	$5,$5,1
lbu	$4,0($4)
sll	$2,$2,$4
addu	$8,$4,$7
addiu	$7,$2,1
sll	$4,$3,$4
sw	$2,5168($16)
bltz	$8,$L250
sra	$3,$7,1

lw	$11,5176($16)
lw	$9,5180($16)
sltu	$9,$11,$9
beq	$9,$0,$L250
addiu	$7,$11,2

sw	$7,5176($16)
lbu	$9,1($11)
lbu	$7,0($11)
sll	$9,$9,8
or	$9,$9,$7
sll	$11,$9,8
srl	$9,$9,8
or	$9,$11,$9
andi	$9,$9,0xffff
sll	$9,$9,$8
addiu	$8,$8,-16
or	$4,$4,$9
$L250:
sll	$9,$3,16
sw	$8,5172($16)
sltu	$7,$4,$9
xori	$7,$7,0x1
bne	$7,$0,$L312
subu	$2,$2,$3

.option	pic0
j	$L251
.option	pic2
move	$2,$3

$L311:
.option	pic0
j	$L255
.option	pic2
li	$10,8			# 0x8

$L314:
subu	$3,$3,$7
$L254:
addiu	$10,$10,-1
sw	$2,5168($16)
move	$8,$9
sw	$3,5184($16)
beq	$10,$0,$L313
move	$4,$3

$L255:
addu	$3,$17,$2
lbu	$3,0($3)
sll	$2,$2,$3
addu	$9,$3,$8
addiu	$8,$2,1
sll	$3,$4,$3
sw	$2,5168($16)
bltz	$9,$L253
sra	$4,$8,1

lw	$8,5176($16)
lw	$7,5180($16)
sltu	$7,$8,$7
beq	$7,$0,$L253
addiu	$11,$8,2

sw	$11,5176($16)
lbu	$7,1($8)
lbu	$8,0($8)
sll	$7,$7,8
or	$7,$7,$8
sll	$8,$7,8
srl	$7,$7,8
or	$7,$8,$7
andi	$7,$7,0xffff
sll	$7,$7,$9
addiu	$9,$9,-16
or	$3,$3,$7
$L253:
sll	$7,$4,16
sw	$9,5172($16)
sltu	$8,$3,$7
beq	$8,$0,$L314
subu	$2,$2,$4

.option	pic0
j	$L254
.option	pic2
move	$2,$4

$L313:
li	$10,8			# 0x8
.option	pic0
j	$L258
.option	pic2
move	$7,$9

$L316:
subu	$4,$4,$7
$L257:
addiu	$10,$10,-1
sw	$2,5168($16)
move	$7,$8
sw	$4,5184($16)
beq	$10,$0,$L315
move	$3,$4

$L258:
addu	$4,$17,$2
lbu	$4,0($4)
sll	$2,$2,$4
addu	$8,$4,$7
addiu	$7,$2,1
sll	$4,$3,$4
sw	$2,5168($16)
bltz	$8,$L256
sra	$3,$7,1

lw	$9,5176($16)
lw	$7,5180($16)
sltu	$7,$9,$7
beq	$7,$0,$L256
addiu	$11,$9,2

sw	$11,5176($16)
lbu	$7,1($9)
lbu	$9,0($9)
sll	$7,$7,8
or	$7,$7,$9
sll	$9,$7,8
srl	$7,$7,8
or	$7,$9,$7
andi	$7,$7,0xffff
sll	$7,$7,$8
addiu	$8,$8,-16
or	$4,$4,$7
$L256:
sll	$7,$3,16
sw	$8,5172($16)
sltu	$9,$4,$7
beq	$9,$0,$L316
subu	$2,$2,$3

.option	pic0
j	$L257
.option	pic2
move	$2,$3

$L315:
addu	$3,$17,$2
lbu	$7,0($3)
sll	$9,$2,$7
addu	$3,$7,$8
sll	$4,$4,$7
bltz	$3,$L259
sw	$9,5168($16)

lw	$8,5176($16)
lw	$2,5180($16)
sltu	$2,$8,$2
beq	$2,$0,$L318
addiu	$2,$9,1

addiu	$2,$8,2
sw	$2,5176($16)
lbu	$7,1($8)
lbu	$2,0($8)
sll	$7,$7,8
or	$7,$7,$2
sll	$2,$7,8
srl	$7,$7,8
or	$2,$2,$7
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$4,$4,$2
$L259:
addiu	$2,$9,1
$L318:
sra	$2,$2,1
sll	$7,$2,16
sltu	$8,$4,$7
bne	$8,$0,$L260
sw	$3,5172($16)

subu	$2,$9,$2
subu	$4,$4,$7
$L260:
addu	$17,$17,$2
sw	$4,5184($16)
lbu	$7,0($17)
sll	$2,$2,$7
addu	$3,$3,$7
sll	$4,$4,$7
bltz	$3,$L261
sw	$2,5168($16)

lw	$8,5176($16)
lw	$7,5180($16)
sltu	$7,$8,$7
beq	$7,$0,$L319
addiu	$7,$2,1

addiu	$7,$8,2
sw	$7,5176($16)
lbu	$7,1($8)
lbu	$8,0($8)
sll	$7,$7,8
or	$7,$7,$8
sll	$8,$7,8
srl	$7,$7,8
or	$7,$8,$7
andi	$7,$7,0xffff
sll	$7,$7,$3
addiu	$3,$3,-16
or	$4,$4,$7
$L261:
addiu	$7,$2,1
$L319:
sw	$3,5172($16)
sra	$3,$7,1
sll	$7,$3,16
sltu	$8,$4,$7
beq	$8,$0,$L317
nop

$L262:
lw	$2,5392($16)
sll	$5,$5,4
sw	$4,5184($16)
sw	$3,5168($16)
beq	$2,$0,$L294
lw	$4,0($16)

lw	$2,660($4)
beq	$5,$2,$L265
nop

$L294:
sll	$6,$6,4
lw	$25,%call16(avcodec_set_dimensions)($28)
$L320:
.reloc	1f,R_MIPS_JALR,avcodec_set_dimensions
1:	jalr	$25
nop

.option	pic0
j	$L292
.option	pic2
li	$2,2			# 0x2

$L308:
sw	$2,5176($16)
lbu	$2,1($5)
lbu	$5,0($5)
sll	$2,$2,8
or	$2,$2,$5
sll	$5,$2,8
srl	$2,$2,8
or	$2,$5,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
.option	pic0
j	$L244
.option	pic2
or	$7,$7,$2

$L307:
sw	$3,5176($16)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
.option	pic0
j	$L242
.option	pic2
or	$2,$2,$3

$L306:
sw	$3,5176($16)
lbu	$3,1($7)
lbu	$5,0($7)
sll	$3,$3,8
or	$3,$3,$5
sll	$7,$3,8
srl	$3,$3,8
or	$3,$7,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
.option	pic0
j	$L266
.option	pic2
or	$6,$6,$3

$L317:
subu	$3,$2,$3
.option	pic0
j	$L262
.option	pic2
subu	$4,$4,$7

$L265:
lw	$2,664($4)
sll	$6,$6,4
bne	$6,$2,$L320
lw	$25,%call16(avcodec_set_dimensions)($28)

.option	pic0
j	$L292
.option	pic2
li	$2,1			# 0x1

.set	macro
.set	reorder
.end	vp5_parse_header
.size	vp5_parse_header, .-vp5_parse_header
.globl	vp5_decoder
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"vp5\000"
.align	2
$LC2:
.ascii	"On2 VP5\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	vp5_decoder, @object
.size	vp5_decoder, 72
vp5_decoder:
.word	$LC1
.word	0
.word	93
.word	10992
.word	vp5_decode_init
.word	0
.word	ff_vp56_free
.word	ff_vp56_decode_frame
.word	2
.space	16
.word	$LC2
.space	16
.rdata
.align	2
.type	vp5_coord_div, @object
.size	vp5_coord_div, 6
vp5_coord_div:
.byte	2
.byte	2
.byte	2
.byte	2
.byte	4
.byte	4
.align	2
.type	vp5_ract_lc, @object
.size	vp5_ract_lc, 1080
vp5_ract_lc:
.half	276
.half	0
.half	238
.half	0
.half	195
.half	0
.half	156
.half	0
.half	113
.half	0
.half	274
.half	0
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	192
.half	59
.half	182
.half	50
.half	141
.half	48
.half	110
.half	40
.half	92
.half	19
.half	125
.half	128
.half	169
.half	87
.half	169
.half	83
.half	184
.half	62
.half	220
.half	16
.half	184
.half	0
.half	264
.half	0
.half	212
.half	40
.half	212
.half	36
.half	169
.half	49
.half	174
.half	27
.half	8
.half	120
.half	182
.half	71
.half	259
.half	10
.half	197
.half	19
.half	143
.half	22
.half	123
.half	16
.half	110
.half	8
.half	133
.half	88
.half	0
.half	1
.half	256
.half	0
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	207
.half	46
.half	187
.half	50
.half	97
.half	83
.half	23
.half	100
.half	41
.half	56
.half	56
.half	188
.half	166
.half	90
.half	146
.half	108
.half	161
.half	88
.half	136
.half	95
.half	174
.half	0
.half	266
.half	0
.half	264
.half	7
.half	243
.half	18
.half	184
.half	43
.half	-14
.half	154
.half	20
.half	112
.half	20
.half	199
.half	230
.half	26
.half	197
.half	22
.half	159
.half	20
.half	146
.half	12
.half	136
.half	4
.half	54
.half	162
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	0
.half	1
.half	192
.half	59
.half	156
.half	72
.half	84
.half	101
.half	49
.half	101
.half	79
.half	47
.half	79
.half	167
.half	138
.half	115
.half	136
.half	116
.half	166
.half	80
.half	238
.half	0
.half	195
.half	0
.half	261
.half	0
.half	225
.half	33
.half	205
.half	42
.half	159
.half	61
.half	79
.half	96
.half	92
.half	66
.half	28
.half	195
.half	200
.half	37
.half	197
.half	18
.half	159
.half	13
.half	143
.half	7
.half	102
.half	5
.half	123
.half	126
.half	197
.half	3
.half	220
.half	-9
.half	210
.half	-12
.half	187
.half	-6
.half	151
.half	-2
.half	174
.half	80
.half	200
.half	53
.half	187
.half	47
.half	159
.half	40
.half	118
.half	38
.half	100
.half	18
.half	141
.half	111
.half	179
.half	78
.half	166
.half	86
.half	197
.half	50
.half	207
.half	27
.half	187
.half	0
.half	115
.half	139
.half	218
.half	34
.half	220
.half	29
.half	174
.half	46
.half	128
.half	61
.half	54
.half	89
.half	187
.half	65
.half	238
.half	14
.half	197
.half	18
.half	125
.half	26
.half	90
.half	25
.half	82
.half	13
.half	161
.half	86
.half	189
.half	1
.half	205
.half	-2
.half	156
.half	-4
.half	143
.half	-4
.half	146
.half	-4
.half	172
.half	72
.half	230
.half	31
.half	192
.half	45
.half	102
.half	76
.half	38
.half	85
.half	56
.half	41
.half	64
.half	173
.half	166
.half	91
.half	141
.half	111
.half	128
.half	116
.half	118
.half	109
.half	177
.half	0
.half	23
.half	222
.half	253
.half	14
.half	236
.half	21
.half	174
.half	49
.half	33
.half	118
.half	44
.half	93
.half	23
.half	187
.half	218
.half	28
.half	179
.half	28
.half	118
.half	35
.half	95
.half	30
.half	72
.half	24
.half	128
.half	108
.half	187
.half	1
.half	174
.half	-1
.half	125
.half	-1
.half	110
.half	-1
.half	108
.half	-1
.half	202
.half	52
.half	197
.half	53
.half	146
.half	75
.half	46
.half	118
.half	33
.half	103
.half	64
.half	50
.half	118
.half	126
.half	138
.half	114
.half	128
.half	122
.half	161
.half	86
.half	243
.half	-6
.half	195
.half	0
.half	38
.half	210
.half	215
.half	39
.half	179
.half	58
.half	97
.half	101
.half	95
.half	85
.half	87
.half	70
.half	69
.half	152
.half	236
.half	24
.half	205
.half	18
.half	172
.half	12
.half	154
.half	6
.half	125
.half	1
.half	169
.half	75
.half	187
.half	4
.half	230
.half	-2
.half	228
.half	-4
.half	236
.half	-4
.half	241
.half	-2
.half	192
.half	66
.half	200
.half	46
.half	187
.half	42
.half	159
.half	34
.half	136
.half	25
.half	105
.half	10
.half	179
.half	62
.half	207
.half	55
.half	192
.half	63
.half	192
.half	54
.half	195
.half	36
.half	177
.half	1
.half	143
.half	98
.half	225
.half	27
.half	207
.half	34
.half	200
.half	30
.half	131
.half	57
.half	97
.half	60
.half	197
.half	45
.half	271
.half	8
.half	218
.half	13
.half	133
.half	19
.half	90
.half	19
.half	72
.half	7
.half	182
.half	51
.half	179
.half	1
.half	225
.half	-1
.half	154
.half	-2
.half	110
.half	-1
.half	92
.half	0
.half	195
.half	41
.half	241
.half	26
.half	189
.half	40
.half	82
.half	64
.half	33
.half	60
.half	67
.half	17
.half	120
.half	94
.half	192
.half	68
.half	151
.half	94
.half	146
.half	90
.half	143
.half	72
.half	161
.half	0
.half	113
.half	128
.half	256
.half	12
.half	218
.half	29
.half	166
.half	48
.half	44
.half	99
.half	31
.half	87
.half	148
.half	78
.half	238
.half	20
.half	184
.half	22
.half	113
.half	27
.half	90
.half	22
.half	74
.half	9
.half	192
.half	37
.half	184
.half	0
.half	215
.half	-1
.half	141
.half	-1
.half	97
.half	0
.half	49
.half	0
.half	264
.half	13
.half	182
.half	51
.half	138
.half	61
.half	95
.half	63
.half	54
.half	59
.half	64
.half	25
.half	200
.half	45
.half	179
.half	75
.half	156
.half	87
.half	174
.half	65
.half	177
.half	44
.half	174
.half	0
.half	164
.half	85
.half	195
.half	45
.half	148
.half	65
.half	105
.half	79
.half	95
.half	72
.half	87
.half	60
.half	169
.half	63
.align	2
.type	vp5_dccv_lc, @object
.size	vp5_dccv_lc, 720
vp5_dccv_lc:
.half	154
.half	61
.half	141
.half	54
.half	90
.half	45
.half	54
.half	34
.half	54
.half	13
.half	128
.half	109
.half	136
.half	54
.half	148
.half	45
.half	92
.half	41
.half	54
.half	33
.half	51
.half	15
.half	87
.half	113
.half	87
.half	44
.half	97
.half	40
.half	67
.half	36
.half	46
.half	29
.half	41
.half	15
.half	64
.half	80
.half	59
.half	33
.half	61
.half	31
.half	51
.half	28
.half	44
.half	22
.half	33
.half	12
.half	49
.half	63
.half	69
.half	12
.half	59
.half	16
.half	46
.half	14
.half	31
.half	13
.half	26
.half	6
.half	92
.half	26
.half	128
.half	108
.half	77
.half	119
.half	54
.half	84
.half	26
.half	71
.half	87
.half	19
.half	95
.half	155
.half	154
.half	4
.half	182
.half	0
.half	159
.half	-8
.half	128
.half	-5
.half	143
.half	-5
.half	187
.half	55
.half	182
.half	0
.half	228
.half	-3
.half	187
.half	-7
.half	174
.half	-9
.half	189
.half	-11
.half	169
.half	79
.half	161
.half	-9
.half	192
.half	-8
.half	187
.half	-9
.half	169
.half	-10
.half	136
.half	-9
.half	184
.half	40
.half	164
.half	-11
.half	179
.half	-10
.half	174
.half	-10
.half	161
.half	-10
.half	115
.half	-7
.half	197
.half	20
.half	195
.half	-11
.half	195
.half	-11
.half	146
.half	-10
.half	110
.half	-6
.half	95
.half	-4
.half	195
.half	39
.half	182
.half	55
.half	172
.half	77
.half	177
.half	37
.half	169
.half	29
.half	172
.half	52
.half	92
.half	162
.half	174
.half	80
.half	164
.half	80
.half	95
.half	80
.half	46
.half	66
.half	56
.half	24
.half	36
.half	193
.half	164
.half	80
.half	166
.half	77
.half	105
.half	76
.half	49
.half	68
.half	46
.half	31
.half	49
.half	186
.half	97
.half	78
.half	110
.half	74
.half	72
.half	72
.half	44
.half	60
.half	33
.half	30
.half	69
.half	131
.half	61
.half	61
.half	69
.half	63
.half	51
.half	57
.half	31
.half	48
.half	26
.half	27
.half	64
.half	89
.half	67
.half	23
.half	51
.half	32
.half	36
.half	33
.half	26
.half	28
.half	20
.half	12
.half	44
.half	68
.half	26
.half	197
.half	41
.half	189
.half	61
.half	129
.half	28
.half	103
.half	49
.half	52
.half	-12
.half	245
.half	102
.half	141
.half	79
.half	166
.half	72
.half	162
.half	97
.half	125
.half	179
.half	4
.half	307
.half	0
.half	72
.half	168
.half	69
.half	175
.half	84
.half	160
.half	105
.half	127
.half	148
.half	34
.half	310
.half	0
.half	84
.half	151
.half	82
.half	161
.half	87
.half	153
.half	87
.half	135
.half	115
.half	51
.half	317
.half	0
.half	97
.half	125
.half	102
.half	131
.half	105
.half	125
.half	87
.half	122
.half	84
.half	64
.half	54
.half	184
.half	166
.half	18
.half	146
.half	43
.half	125
.half	51
.half	90
.half	64
.half	95
.half	7
.half	38
.half	154
.half	294
.half	0
.half	13
.half	225
.half	10
.half	225
.half	67
.half	168
.half	0
.half	167
.half	161
.half	94
.half	172
.half	76
.half	172
.half	75
.half	136
.half	80
.half	64
.half	98
.half	74
.half	67
.half	315
.half	0
.half	169
.half	76
.half	207
.half	56
.half	164
.half	66
.half	97
.half	80
.half	67
.half	72
.half	328
.half	0
.half	136
.half	80
.half	187
.half	53
.half	154
.half	62
.half	72
.half	85
.half	-2
.half	105
.half	305
.half	0
.half	74
.half	91
.half	128
.half	64
.half	113
.half	64
.half	61
.half	77
.half	41
.half	75
.half	259
.half	0
.half	46
.half	84
.half	51
.half	81
.half	28
.half	89
.half	31
.half	78
.half	23
.half	77
.half	202
.half	0
.half	323
.half	0
.half	323
.half	0
.half	300
.half	0
.half	236
.half	0
.half	195
.half	0
.half	328
.half	0
.align	2
.type	vp5_ract_pct, @object
.size	vp5_ract_pct, 396
vp5_ract_pct:
.byte	-29
.byte	-10
.byte	-26
.byte	-9
.byte	-12
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-54
.byte	-2
.byte	-47
.byte	-25
.byte	-25
.byte	-7
.byte	-7
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-50
.byte	-2
.byte	-31
.byte	-14
.byte	-15
.byte	-5
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-21
.byte	-2
.byte	-15
.byte	-3
.byte	-4
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-22
.byte	-2
.byte	-8
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-16
.byte	-2
.byte	-8
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-18
.byte	-2
.byte	-16
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-12
.byte	-2
.byte	-5
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-50
.byte	-53
.byte	-29
.byte	-17
.byte	-9
.byte	-2
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-49
.byte	-57
.byte	-36
.byte	-20
.byte	-13
.byte	-4
.byte	-4
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-44
.byte	-37
.byte	-26
.byte	-13
.byte	-12
.byte	-3
.byte	-4
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-20
.byte	-19
.byte	-9
.byte	-4
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-16
.byte	-16
.byte	-8
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-26
.byte	-23
.byte	-7
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-18
.byte	-18
.byte	-6
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-8
.byte	-5
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-31
.byte	-17
.byte	-29
.byte	-25
.byte	-12
.byte	-3
.byte	-13
.byte	-2
.byte	-2
.byte	-3
.byte	-2
.byte	-24
.byte	-22
.byte	-32
.byte	-28
.byte	-14
.byte	-7
.byte	-14
.byte	-4
.byte	-5
.byte	-5
.byte	-2
.byte	-21
.byte	-7
.byte	-18
.byte	-16
.byte	-5
.byte	-2
.byte	-7
.byte	-2
.byte	-3
.byte	-3
.byte	-2
.byte	-7
.byte	-3
.byte	-5
.byte	-6
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-5
.byte	-6
.byte	-7
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-13
.byte	-12
.byte	-6
.byte	-6
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-7
.byte	-8
.byte	-6
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.align	2
.type	vp5_dccv_pct, @object
.size	vp5_dccv_pct, 22
vp5_dccv_pct:
.byte	-110
.byte	-59
.byte	-75
.byte	-49
.byte	-24
.byte	-13
.byte	-18
.byte	-5
.byte	-12
.byte	-6
.byte	-7
.byte	-77
.byte	-37
.byte	-42
.byte	-16
.byte	-6
.byte	-2
.byte	-12
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.align	2
.type	vp5_vmc_pct, @object
.size	vp5_vmc_pct, 22
vp5_vmc_pct:
.byte	-13
.byte	-36
.byte	-5
.byte	-3
.byte	-19
.byte	-24
.byte	-15
.byte	-11
.byte	-9
.byte	-5
.byte	-3
.byte	-21
.byte	-45
.byte	-10
.byte	-7
.byte	-22
.byte	-25
.byte	-8
.byte	-7
.byte	-4
.byte	-4
.byte	-2
.align	2
.type	vp5_coeff_groups, @object
.size	vp5_coeff_groups, 64
vp5_coeff_groups:
.byte	-1
.byte	0
.byte	1
.byte	1
.byte	2
.byte	1
.byte	1
.byte	2
.byte	2
.byte	1
.byte	1
.byte	2
.byte	2
.byte	2
.byte	1
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	1
.byte	1
.byte	2
.byte	2
.byte	3
.byte	3
.byte	4
.byte	3
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
