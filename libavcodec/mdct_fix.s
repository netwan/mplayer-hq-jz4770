	.file	1 "mdct_fix.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.abicalls
	.text
	.align	2
	.globl	imdct_half_fix_c
	.set	nomips16
	.set	nomicromips
	.ent	imdct_half_fix_c
	.type	imdct_half_fix_c, @function
imdct_half_fix_c:
	.frame	$sp,48,$31		# vars= 0, regs= 6/0, args= 16, gp= 8
	.mask	0x801f0000,-4
	.fmask	0x00000000,0
	lw	$10,4($4)
	li	$2,1			# 0x1
	addiu	$sp,$sp,-48
	lw	$3,24($4)
	sll	$10,$2,$10
	sw	$20,40($sp)
	sw	$19,36($sp)
	sra	$9,$10,1
	sw	$18,32($sp)
	sra	$20,$10,3
	sw	$31,44($sp)
	sll	$9,$9,2
	sw	$17,28($sp)
	sw	$16,24($sp)
	sra	$10,$10,2
	addiu	$9,$9,-4
	lw	$18,8($4)
	lw	$19,12($4)
	.set	noreorder
	.set	nomacro
	blez	$20,$L2
	addu	$9,$6,$9
	.set	macro
	.set	reorder

	sll	$10,$10,1
	sll	$13,$20,2
	addu	$10,$3,$10
	move	$16,$0
	move	$17,$5
	move	$5,$3
$L3:
	addu	$7,$18,$16
	lw	$12,0($9)
	addu	$3,$19,$16
	lw	$11,0($6)
	lhu	$2,-2($10)
	lw	$7,0($7)
	lw	$8,0($3)
#APP
 # 89 "mdct_fix.c" 1
	S32MUL xr1,xr2,$12,$7
 # 0 "" 2
 # 90 "mdct_fix.c" 1
	S32MUL xr3,xr4,$12,$8
 # 0 "" 2
#NO_APP
	lw	$12,-4($9)
#APP
 # 93 "mdct_fix.c" 1
	S32MUL xr7,xr8,$12,$7
 # 0 "" 2
 # 94 "mdct_fix.c" 1
	S32MUL xr9,xr10,$12,$8
 # 0 "" 2
 # 95 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$11,$8
 # 0 "" 2
 # 96 "mdct_fix.c" 1
	S32MADD xr3,xr4,$11,$7
 # 0 "" 2
#NO_APP
	lw	$11,4($6)
#APP
 # 99 "mdct_fix.c" 1
	D32SLL xr5,xr1,xr3,xr6,1
 # 0 "" 2
 # 100 "mdct_fix.c" 1
	S32MSUB xr7,xr8,$11,$8
 # 0 "" 2
 # 101 "mdct_fix.c" 1
	S32MADD xr9,xr10,$11,$7
 # 0 "" 2
#NO_APP
	lhu	$3,0($5)
	sll	$3,$3,3
	addu	$3,$17,$3
#APP
 # 102 "mdct_fix.c" 1
	S32M2I xr5, $7
 # 0 "" 2
#NO_APP
	sw	$7,0($3)
#APP
 # 103 "mdct_fix.c" 1
	D32SLL xr11,xr7,xr9,xr12,1
 # 0 "" 2
 # 104 "mdct_fix.c" 1
	S32M2I xr6, $7
 # 0 "" 2
#NO_APP
	sll	$2,$2,3
	sw	$7,4($3)
	addiu	$6,$6,8
	addiu	$9,$9,-8
	addu	$2,$17,$2
#APP
 # 107 "mdct_fix.c" 1
	S32M2I xr11, $3
 # 0 "" 2
#NO_APP
	sw	$3,0($2)
#APP
 # 108 "mdct_fix.c" 1
	S32M2I xr12, $7
 # 0 "" 2
#NO_APP
	addiu	$16,$16,4
	sw	$7,4($2)
	addiu	$5,$5,2
	.set	noreorder
	.set	nomacro
	bne	$16,$13,$L3
	addiu	$10,$10,-2
	.set	macro
	.set	reorder

	lw	$25,32($4)
	sll	$20,$20,3
	addiu	$4,$4,16
	.set	noreorder
	.set	nomacro
	jalr	$25
	move	$5,$17
	.set	macro
	.set	reorder

	addiu	$2,$20,-4
	addiu	$20,$20,4
	addu	$13,$19,$16
	addu	$16,$18,$16
	addu	$2,$17,$2
	addu	$3,$17,$20
	move	$4,$0
	addiu	$17,$17,-4
	move	$10,$13
	move	$9,$16
$L5:
	addu	$6,$13,$4
	lw	$8,0($2)
	addu	$7,$16,$4
	lw	$5,-4($2)
	lw	$6,-4($6)
	lw	$7,-4($7)
#APP
 # 118 "mdct_fix.c" 1
	S32MUL xr1,xr2,$8,$6
 # 0 "" 2
 # 118 "mdct_fix.c" 1
	S32MUL xr5,xr6,$8,$7
 # 0 "" 2
 # 118 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$5,$7
 # 0 "" 2
 # 118 "mdct_fix.c" 1
	S32MADD xr5,xr6,$5,$6
 # 0 "" 2
 # 118 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
 # 118 "mdct_fix.c" 1
	S32M2I xr1, $8
 # 0 "" 2
 # 118 "mdct_fix.c" 1
	S32M2I xr5, $5
 # 0 "" 2
#NO_APP
	lw	$12,0($3)
	lw	$7,0($10)
	lw	$6,-4($3)
	lw	$11,0($9)
#APP
 # 119 "mdct_fix.c" 1
	S32MUL xr1,xr2,$12,$7
 # 0 "" 2
 # 119 "mdct_fix.c" 1
	S32MUL xr5,xr6,$12,$11
 # 0 "" 2
 # 119 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$6,$11
 # 0 "" 2
 # 119 "mdct_fix.c" 1
	S32MADD xr5,xr6,$6,$7
 # 0 "" 2
 # 119 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
 # 119 "mdct_fix.c" 1
	S32M2I xr1, $6
 # 0 "" 2
 # 119 "mdct_fix.c" 1
	S32M2I xr5, $7
 # 0 "" 2
#NO_APP
	sw	$8,-4($2)
	addiu	$2,$2,-8
	addiu	$4,$4,-4
	sw	$7,8($2)
	addiu	$10,$10,4
	sw	$6,-4($3)
	sw	$5,0($3)
	addiu	$9,$9,4
	.set	noreorder
	.set	nomacro
	bne	$2,$17,$L5
	addiu	$3,$3,8
	.set	macro
	.set	reorder

	lw	$31,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,48
	.set	macro
	.set	reorder

$L2:
	lw	$25,32($4)
	addiu	$4,$4,16
	lw	$31,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	.set	noreorder
	.set	nomacro
	jr	$25
	addiu	$sp,$sp,48
	.set	macro
	.set	reorder

	.end	imdct_half_fix_c
	.size	imdct_half_fix_c, .-imdct_half_fix_c
	.align	2
	.globl	imdct_calc_fix
	.set	nomips16
	.set	nomicromips
	.ent	imdct_calc_fix
	.type	imdct_calc_fix, @function
imdct_calc_fix:
	.frame	$sp,64,$31		# vars= 0, regs= 9/0, args= 16, gp= 8
	.mask	0x80ff0000,-4
	.fmask	0x00000000,0
	addiu	$sp,$sp,-64
	lw	$2,4($4)
	lw	$8,24($4)
	sw	$19,40($sp)
	li	$19,1			# 0x1
	sw	$17,32($sp)
	sll	$19,$19,$2
	sw	$21,48($sp)
	sw	$23,56($sp)
	sra	$17,$19,1
	sw	$22,52($sp)
	sra	$21,$19,2
	sw	$20,44($sp)
	sll	$17,$17,2
	sw	$18,36($sp)
	sw	$16,28($sp)
	move	$18,$5
	addiu	$17,$17,-4
	sw	$31,60($sp)
	move	$20,$7
	lw	$22,8($4)
	lw	$23,12($4)
	sra	$16,$19,3
	.set	noreorder
	.set	nomacro
	blez	$21,$L15
	addu	$5,$6,$17
	.set	macro
	.set	reorder

	sll	$21,$21,2
	move	$3,$22
	addu	$12,$22,$21
	move	$7,$23
$L16:
	lw	$11,0($5)
	lw	$9,0($3)
	lw	$2,0($6)
	lw	$10,0($7)
#APP
 # 155 "mdct_fix.c" 1
	S32MUL xr1,xr2,$11,$9
 # 0 "" 2
 # 155 "mdct_fix.c" 1
	S32MUL xr5,xr6,$11,$10
 # 0 "" 2
 # 155 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$2,$10
 # 0 "" 2
 # 155 "mdct_fix.c" 1
	S32MADD xr5,xr6,$2,$9
 # 0 "" 2
 # 155 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
#NO_APP
	lhu	$2,0($8)
	sll	$2,$2,3
	addu	$2,$20,$2
#APP
 # 155 "mdct_fix.c" 1
	S32M2I xr1, $9
 # 0 "" 2
#NO_APP
	sw	$9,0($2)
#APP
 # 155 "mdct_fix.c" 1
	S32M2I xr5, $9
 # 0 "" 2
#NO_APP
	addiu	$3,$3,4
	sw	$9,4($2)
	addiu	$6,$6,8
	addiu	$5,$5,-8
	addiu	$8,$8,2
	.set	noreorder
	.set	nomacro
	bne	$3,$12,$L16
	addiu	$7,$7,4
	.set	macro
	.set	reorder

	lw	$25,32($4)
	addiu	$4,$4,16
	.set	noreorder
	.set	nomacro
	jalr	$25
	move	$5,$20
	.set	macro
	.set	reorder

	move	$3,$0
	move	$2,$20
$L18:
	addu	$5,$22,$3
	lw	$7,0($2)
	addu	$6,$23,$3
	lw	$4,4($2)
	lw	$5,0($5)
	lw	$6,0($6)
#APP
 # 163 "mdct_fix.c" 1
	S32MUL xr1,xr2,$7,$5
 # 0 "" 2
 # 163 "mdct_fix.c" 1
	S32MUL xr5,xr6,$7,$6
 # 0 "" 2
 # 163 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$4,$6
 # 0 "" 2
 # 163 "mdct_fix.c" 1
	S32MADD xr5,xr6,$4,$5
 # 0 "" 2
 # 163 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
 # 163 "mdct_fix.c" 1
	S32M2I xr1, $4
 # 0 "" 2
#NO_APP
	sw	$4,0($2)
#APP
 # 163 "mdct_fix.c" 1
	S32M2I xr5, $4
 # 0 "" 2
#NO_APP
	addiu	$3,$3,4
	sw	$4,4($2)
	.set	noreorder
	.set	nomacro
	bne	$21,$3,$L18
	addiu	$2,$2,8
	.set	macro
	.set	reorder

$L22:
	.set	noreorder
	.set	nomacro
	blez	$16,$L14
	sll	$16,$16,3
	.set	macro
	.set	reorder

	addiu	$4,$17,4
	addiu	$3,$16,4
	sll	$6,$19,2
	addiu	$16,$16,-4
	addu	$4,$18,$4
	addu	$3,$20,$3
	addu	$2,$20,$16
	addu	$6,$18,$6
	addiu	$20,$20,-4
	move	$5,$4
$L20:
	lw	$8,0($3)
	addiu	$18,$18,8
	addiu	$3,$3,8
	addiu	$2,$2,-8
	subu	$8,$0,$8
	addiu	$5,$5,-8
	addiu	$4,$4,8
	sw	$8,-8($18)
	addiu	$6,$6,-8
	lw	$8,-8($3)
	sw	$8,4($5)
	lw	$8,4($2)
	sw	$8,-4($18)
	lw	$8,4($2)
	subu	$8,$0,$8
	sw	$8,0($5)
	lw	$8,-12($3)
	subu	$8,$0,$8
	sw	$8,-8($4)
	lw	$8,-12($3)
	subu	$8,$0,$8
	sw	$8,4($6)
	lw	$8,8($2)
	sw	$8,-4($4)
	lw	$8,8($2)
	.set	noreorder
	.set	nomacro
	bne	$2,$20,$L20
	sw	$8,0($6)
	.set	macro
	.set	reorder

$L14:
	lw	$31,60($sp)
	lw	$23,56($sp)
	lw	$22,52($sp)
	lw	$21,48($sp)
	lw	$20,44($sp)
	lw	$19,40($sp)
	lw	$18,36($sp)
	lw	$17,32($sp)
	lw	$16,28($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,64
	.set	macro
	.set	reorder

$L15:
	lw	$25,32($4)
	addiu	$4,$4,16
	.set	noreorder
	.set	nomacro
	jalr	$25
	move	$5,$7
	.set	macro
	.set	reorder

	.option	pic0
	j	$L22
	.option	pic2
	.end	imdct_calc_fix
	.size	imdct_calc_fix, .-imdct_calc_fix
	.align	2
	.globl	mdct_calc_fix
	.set	nomips16
	.set	nomicromips
	.ent	mdct_calc_fix
	.type	mdct_calc_fix, @function
mdct_calc_fix:
	.frame	$sp,72,$31		# vars= 8, regs= 10/0, args= 16, gp= 8
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	lw	$25,4($4)
	li	$2,1			# 0x1
	addiu	$sp,$sp,-72
	lw	$10,24($4)
	sll	$25,$2,$25
	sw	$20,48($sp)
	sw	$23,60($sp)
	sra	$20,$25,2
	sw	$19,44($sp)
	sra	$2,$25,1
	sw	$22,56($sp)
	sll	$19,$20,1
	sw	$21,52($sp)
	sra	$23,$25,3
	sw	$17,36($sp)
	sw	$16,32($sp)
	move	$17,$5
	sw	$31,68($sp)
	move	$16,$7
	sw	$fp,64($sp)
	addu	$19,$19,$20
	sw	$18,40($sp)
	sw	$2,24($sp)
	lw	$21,8($4)
	.set	noreorder
	.set	nomacro
	blez	$23,$L34
	lw	$22,12($4)
	.set	macro
	.set	reorder

	sll	$fp,$19,2
	sll	$13,$23,2
	subu	$19,$0,$19
	addu	$11,$6,$fp
	addu	$31,$21,$13
	sll	$19,$19,2
	sll	$15,$20,2
	sll	$14,$2,2
	sll	$25,$25,2
	sll	$23,$23,1
	move	$7,$21
	move	$5,$22
$L33:
	addu	$2,$6,$fp
	lw	$9,0($7)
	addu	$3,$11,$19
	lw	$18,-4($11)
	addu	$24,$6,$15
	lw	$12,0($5)
	addu	$8,$3,$15
	lw	$2,0($2)
	subu	$9,$0,$9
	lw	$24,0($24)
	lw	$8,-4($8)
	subu	$2,$0,$2
	subu	$2,$2,$18
	subu	$8,$8,$24
#APP
 # 208 "mdct_fix.c" 1
	S32MUL xr1,xr2,$2,$9
 # 0 "" 2
 # 208 "mdct_fix.c" 1
	S32MUL xr5,xr6,$2,$12
 # 0 "" 2
 # 208 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$8,$12
 # 0 "" 2
 # 208 "mdct_fix.c" 1
	S32MADD xr5,xr6,$8,$9
 # 0 "" 2
 # 208 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
#NO_APP
	lhu	$2,0($10)
	sll	$2,$2,3
	addu	$2,$16,$2
#APP
 # 208 "mdct_fix.c" 1
	S32M2I xr1, $8
 # 0 "" 2
#NO_APP
	sw	$8,0($2)
#APP
 # 208 "mdct_fix.c" 1
	S32M2I xr5, $8
 # 0 "" 2
#NO_APP
	addu	$24,$3,$25
	sw	$8,4($2)
	addu	$12,$6,$14
	lw	$8,0($6)
	addu	$3,$3,$14
	addu	$9,$7,$13
	lw	$2,-4($24)
	lw	$12,0($12)
	addu	$24,$5,$13
	lw	$3,-4($3)
	lw	$9,0($9)
	addu	$2,$12,$2
	lw	$12,0($24)
	subu	$8,$8,$3
	subu	$2,$0,$2
	subu	$9,$0,$9
#APP
 # 213 "mdct_fix.c" 1
	S32MUL xr1,xr2,$8,$9
 # 0 "" 2
 # 213 "mdct_fix.c" 1
	S32MUL xr5,xr6,$8,$12
 # 0 "" 2
 # 213 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$2,$12
 # 0 "" 2
 # 213 "mdct_fix.c" 1
	S32MADD xr5,xr6,$2,$9
 # 0 "" 2
 # 213 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
#NO_APP
	addu	$2,$10,$23
	lhu	$2,0($2)
	sll	$2,$2,3
	addu	$2,$16,$2
#APP
 # 213 "mdct_fix.c" 1
	S32M2I xr1, $3
 # 0 "" 2
#NO_APP
	sw	$3,0($2)
#APP
 # 213 "mdct_fix.c" 1
	S32M2I xr5, $3
 # 0 "" 2
#NO_APP
	addiu	$7,$7,4
	sw	$3,4($2)
	addiu	$11,$11,-8
	addiu	$10,$10,2
	addiu	$5,$5,4
	.set	noreorder
	.set	nomacro
	bne	$7,$31,$L33
	addiu	$6,$6,8
	.set	macro
	.set	reorder

$L34:
	lw	$25,32($4)
	addiu	$4,$4,16
	.set	noreorder
	.set	nomacro
	jalr	$25
	move	$5,$16
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	blez	$20,$L29
	sll	$20,$20,2
	.set	macro
	.set	reorder

	lw	$2,24($sp)
	sll	$18,$2,2
	move	$2,$0
	addu	$18,$17,$18
$L35:
	addu	$3,$22,$2
	lw	$6,0($16)
	addu	$4,$21,$2
	lw	$5,4($16)
	lw	$3,0($3)
	lw	$4,0($4)
	subu	$3,$0,$3
	subu	$4,$0,$4
#APP
 # 222 "mdct_fix.c" 1
	S32MUL xr1,xr2,$6,$3
 # 0 "" 2
 # 222 "mdct_fix.c" 1
	S32MUL xr5,xr6,$6,$4
 # 0 "" 2
 # 222 "mdct_fix.c" 1
	S32MSUB xr1,xr2,$5,$4
 # 0 "" 2
 # 222 "mdct_fix.c" 1
	S32MADD xr5,xr6,$5,$3
 # 0 "" 2
 # 222 "mdct_fix.c" 1
	D32SLL xr1,xr1,xr5,xr5,1
 # 0 "" 2
 # 222 "mdct_fix.c" 1
	S32M2I xr1, $3
 # 0 "" 2
 # 222 "mdct_fix.c" 1
	S32M2I xr5, $4
 # 0 "" 2
#NO_APP
	addiu	$2,$2,4
	sw	$4,0($17)
	addiu	$16,$16,8
	sw	$3,-4($18)
	addiu	$17,$17,8
	.set	noreorder
	.set	nomacro
	bne	$2,$20,$L35
	addiu	$18,$18,-8
	.set	macro
	.set	reorder

$L29:
	lw	$31,68($sp)
	lw	$fp,64($sp)
	lw	$23,60($sp)
	lw	$22,56($sp)
	lw	$21,52($sp)
	lw	$20,48($sp)
	lw	$19,44($sp)
	lw	$18,40($sp)
	lw	$17,36($sp)
	lw	$16,32($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,72
	.set	macro
	.set	reorder

	.end	mdct_calc_fix
	.size	mdct_calc_fix, .-mdct_calc_fix
	.align	2
	.globl	mdct_init_fix
	.set	nomips16
	.set	nomicromips
	.ent	mdct_init_fix
	.type	mdct_init_fix, @function
mdct_init_fix:
	.frame	$sp,88,$31		# vars= 0, regs= 8/8, args= 16, gp= 8
	.mask	0x807f0000,-36
	.fmask	0x0ff00000,-8
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-88
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$21,44($sp)
	li	$21,1			# 0x1
	sw	$20,40($sp)
	move	$20,$6
	sw	$19,36($sp)
	li	$6,48			# 0x30
	sw	$31,52($sp)
	move	$19,$5
	sw	$22,48($sp)
	move	$5,$0
	sw	$18,32($sp)
	sll	$21,$21,$19
	sw	$17,28($sp)
	move	$18,$4
	sw	$16,24($sp)
	sra	$17,$21,2
	.cprestore	16
	lw	$25,%call16(memset)($28)
	sll	$22,$17,2
	sdc1	$f26,80($sp)
	sdc1	$f24,72($sp)
	sdc1	$f22,64($sp)
	.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
	sdc1	$f20,56($sp)

	move	$4,$22
	lw	$28,16($sp)
	sw	$19,4($18)
	lw	$25,%call16(malloc)($28)
	.reloc	1f,R_MIPS_JALR,malloc
1:	jalr	$25
	sw	$21,0($18)

	lw	$28,16($sp)
	move	$16,$2
	beq	$2,$0,$L47
	sw	$2,8($18)

	lw	$25,%call16(malloc)($28)
	.reloc	1f,R_MIPS_JALR,malloc
1:	jalr	$25
	move	$4,$22

	lw	$28,16($sp)
	beq	$2,$0,$L48
	sw	$2,12($18)

	blez	$17,$L45
	mtc1	$21,$f2

	move	$22,$2
	lui	$2,%hi($LC3)
	lui	$3,%hi($LC2)
	ldc1	$f20,%lo($LC3)($2)
	lui	$2,%hi($LC1)
	move	$21,$0
	ldc1	$f22,%lo($LC1)($2)
	ldc1	$f24,%lo($LC2)($3)
	cvt.d.w	$f0,$f2
	div.d	$f22,$f22,$f0
$L44:
	lw	$25,%call16(cos)($28)
	addiu	$16,$16,4
	addiu	$22,$22,4
	mtc1	$21,$f0
	addiu	$21,$21,1
	cvt.d.w	$f26,$f0
	add.d	$f26,$f26,$f24
	mul.d	$f26,$f22,$f26
	.reloc	1f,R_MIPS_JALR,cos
1:	jalr	$25
	mov.d	$f12,$f26

	mov.d	$f12,$f26
	lw	$28,16($sp)
	mul.d	$f0,$f0,$f20
	lw	$25,%call16(sin)($28)
	trunc.w.d $f2,$f0
	mfc1	$3,$f2
	subu	$3,$0,$3
	.reloc	1f,R_MIPS_JALR,sin
1:	jalr	$25
	sw	$3,-4($16)

	mul.d	$f0,$f0,$f20
	lw	$28,16($sp)
	trunc.w.d $f2,$f0
	mfc1	$3,$f2
	subu	$3,$0,$3
	bne	$21,$17,$L44
	sw	$3,-4($22)

$L45:
	bne	$20,$0,$L42
	lui	$3,%hi(imdct_half_fix_c)

	lui	$2,%hi(mdct_calc_fix)
	addiu	$2,$2,%lo(mdct_calc_fix)
$L43:
	lw	$25,%call16(fft_init_fix)($28)
	addiu	$4,$18,16
	addiu	$5,$19,-2
	sw	$2,40($18)
	.reloc	1f,R_MIPS_JALR,fft_init_fix
1:	jalr	$25
	move	$6,$20

	move	$3,$0
	bltz	$2,$L52
	lw	$28,16($sp)

$L46:
	lw	$31,52($sp)
	move	$2,$3
	lw	$22,48($sp)
	lw	$21,44($sp)
	lw	$20,40($sp)
	lw	$19,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	ldc1	$f26,80($sp)
	ldc1	$f24,72($sp)
	ldc1	$f22,64($sp)
	ldc1	$f20,56($sp)
	j	$31
	addiu	$sp,$sp,88

$L42:
	lui	$2,%hi(imdct_calc_fix)
	addiu	$3,$3,%lo(imdct_half_fix_c)
	addiu	$2,$2,%lo(imdct_calc_fix)
	.option	pic0
	j	$L43
	.option	pic2
	sw	$3,44($18)

$L52:
	lw	$4,8($18)
$L40:
	lw	$25,%call16(free)($28)
	.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
	nop

	lw	$28,16($sp)
	lw	$25,%call16(free)($28)
	.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
	lw	$4,12($18)

	.option	pic0
	j	$L46
	.option	pic2
	li	$3,-1			# 0xffffffffffffffff

$L48:
	.option	pic0
	j	$L40
	.option	pic2
	move	$4,$16

$L47:
	.option	pic0
	j	$L40
	.option	pic2
	move	$4,$0

	.set	macro
	.set	reorder
	.end	mdct_init_fix
	.size	mdct_init_fix, .-mdct_init_fix
	.align	2
	.globl	mdct_end_fix
	.set	nomips16
	.set	nomicromips
	.ent	mdct_end_fix
	.type	mdct_end_fix, @function
mdct_end_fix:
	.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$28,%hi(__gnu_local_gp)
	addiu	$sp,$sp,-32
	addiu	$28,$28,%lo(__gnu_local_gp)
	sw	$16,24($sp)
	move	$16,$4
	sw	$31,28($sp)
	.cprestore	16
	lw	$25,%call16(free)($28)
	.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
	lw	$4,8($4)

	lw	$28,16($sp)
	lw	$25,%call16(free)($28)
	.reloc	1f,R_MIPS_JALR,free
1:	jalr	$25
	lw	$4,12($16)

	addiu	$4,$16,16
	lw	$28,16($sp)
	lw	$31,28($sp)
	lw	$16,24($sp)
	lw	$25,%call16(fft_end_fix)($28)
	.reloc	1f,R_MIPS_JALR,fft_end_fix
1:	jr	$25
	addiu	$sp,$sp,32

	.set	macro
	.set	reorder
	.end	mdct_end_fix
	.size	mdct_end_fix, .-mdct_end_fix
	.section	.rodata.cst8,"aM",@progbits,8
	.align	3
$LC1:
	.word	1413754136
	.word	1075388923
	.align	3
$LC2:
	.word	0
	.word	1069547520
	.align	3
$LC3:
	.word	-4194304
	.word	1105199103
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
