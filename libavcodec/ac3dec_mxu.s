.file	1 "ac3dec.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_exponents
.type	decode_exponents, @function
decode_exponents:
.frame	$sp,1032,$31		# vars= 1024, regs= 0/0, args= 0, gp= 8
.mask	0x00000000,0
.fmask	0x00000000,0
addiu	$sp,$sp,-1032
xori	$2,$5,0x3
sltu	$2,$2,1
lw	$15,1048($sp)
.set	noreorder
.set	nomacro
blez	$6,$L6
addu	$24,$2,$5
.set	macro
.set	reorder

sll	$10,$6,3
lw	$3,8($4)
lui	$11,%hi(ungroup_3_in_7_bits_tab)
lw	$14,0($4)
subu	$10,$10,$6
addiu	$5,$sp,8
addu	$10,$10,$3
addiu	$11,$11,%lo(ungroup_3_in_7_bits_tab)
li	$13,-4			# 0xfffffffffffffffc
li	$12,7			# 0x7
$L5:
srl	$2,$3,3
addu	$2,$14,$2
and	$8,$2,$13
#APP
# 115 "ac3dec.c" 1
.word	0b01110001000100000000000010010000	#S32LDDR XR2,$8,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001000100000000010001010000	#S32LDDR XR1,$8,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$8,$3,0x7
addu	$2,$2,$8
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010011001100010010100110	#S32EXTRV XR2,XR1,$2,$12
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010000000000010101110	#S32M2I XR2, $8
# 0 "" 2
#NO_APP
sll	$2,$8,1
addiu	$5,$5,12
addu	$2,$2,$8
addiu	$3,$3,7
addu	$2,$11,$2
lbu	$9,0($2)
lbu	$8,1($2)
lbu	$2,2($2)
sw	$9,-12($5)
sw	$8,-8($5)
.set	noreorder
.set	nomacro
bne	$3,$10,$L5
sw	$2,-4($5)
.set	macro
.set	reorder

sw	$3,8($4)
$L6:
sll	$2,$6,1
addu	$6,$2,$6
.set	noreorder
.set	nomacro
blez	$6,$L4
lw	$2,8($sp)
.set	macro
.set	reorder

addiu	$2,$2,-2
addu	$7,$7,$2
sltu	$2,$7,25
.set	noreorder
.set	nomacro
beq	$2,$0,$L8
li	$2,2			# 0x2
.set	macro
.set	reorder

addiu	$5,$sp,12
move	$3,$0
.set	noreorder
.set	nomacro
beq	$24,$2,$L9
move	$4,$0
.set	macro
.set	reorder

li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$24,$2,$L26
li	$2,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$24,$2,$L34
addu	$2,$15,$3
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L42
.option	pic2
addiu	$4,$4,1
.set	macro
.set	reorder

$L37:
lw	$2,0($5)
addiu	$2,$2,-2
addu	$7,$7,$2
sltu	$2,$7,25
.set	noreorder
.set	nomacro
beq	$2,$0,$L8
addiu	$5,$5,4
.set	macro
.set	reorder

$L34:
addiu	$4,$4,1
bne	$4,$6,$L37
$L4:
move	$2,$0
$L41:
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1032
.set	macro
.set	reorder

$L16:
lw	$2,0($5)
addiu	$2,$2,-2
addu	$7,$7,$2
sltu	$2,$7,25
.set	noreorder
.set	nomacro
beq	$2,$0,$L8
addiu	$5,$5,4
.set	macro
.set	reorder

$L9:
sll	$8,$7,24
addiu	$2,$3,1
sra	$8,$8,24
addu	$10,$15,$3
addu	$2,$15,$2
addiu	$4,$4,1
sb	$8,0($10)
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$4,$6,$L16
sb	$8,0($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L41
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L38:
lw	$2,0($5)
addiu	$2,$2,-2
addu	$7,$7,$2
sltu	$2,$7,25
.set	noreorder
.set	nomacro
beq	$2,$0,$L8
addiu	$5,$5,4
.set	macro
.set	reorder

$L26:
sll	$8,$7,24
addu	$2,$15,$3
sra	$8,$8,24
addiu	$9,$3,1
addiu	$4,$4,1
sb	$8,0($2)
addiu	$2,$3,3
addiu	$3,$3,2
addu	$9,$15,$9
addu	$10,$15,$3
addu	$2,$15,$2
sb	$8,0($9)
addiu	$3,$3,2
sb	$8,0($10)
.set	noreorder
.set	nomacro
bne	$4,$6,$L38
sb	$8,0($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L41
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L39:
lw	$2,0($5)
addiu	$2,$2,-2
addu	$7,$7,$2
sltu	$2,$7,25
.set	noreorder
.set	nomacro
beq	$2,$0,$L8
addiu	$5,$5,4
.set	macro
.set	reorder

addu	$2,$15,$3
addiu	$4,$4,1
$L42:
sb	$7,0($2)
.set	noreorder
.set	nomacro
bne	$4,$6,$L39
addiu	$3,$3,1
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L41
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L8:
li	$2,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,1032
.set	macro
.set	reorder

.end	decode_exponents
.size	decode_exponents, .-decode_exponents
.align	2
.set	nomips16
.set	nomicromips
.ent	ac3_decode_transform_coeffs_ch
.type	ac3_decode_transform_coeffs_ch, @function
ac3_decode_transform_coeffs_ch:
.frame	$sp,40,$31		# vars= 0, regs= 9/0, args= 0, gp= 0
.mask	0x40ff0000,-4
.fmask	0x00000000,0
sll	$2,$5,2
li	$9,65536			# 0x10000
addu	$2,$4,$2
sll	$3,$5,10
addu	$2,$2,$9
sll	$7,$5,8
li	$9,47112			# 0xb808
addiu	$8,$5,176
li	$10,58688			# 0xe540
addiu	$sp,$sp,-40
addu	$7,$7,$9
addu	$10,$3,$10
sw	$fp,36($sp)
sll	$8,$8,8
sw	$23,32($sp)
sw	$22,28($sp)
addu	$7,$4,$7
sw	$21,24($sp)
addu	$8,$4,$8
sw	$20,20($sp)
addu	$10,$4,$10
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
lw	$3,-20584($2)
.set	noreorder
.set	nomacro
beq	$5,$0,$L60
lw	$9,-20556($2)
.set	macro
.set	reorder

lw	$18,-11424($2)
sltu	$18,$0,$18
$L44:
slt	$2,$3,$9
.set	noreorder
.set	nomacro
beq	$2,$0,$L43
lui	$17,%hi(b4_mantissas)
.set	macro
.set	reorder

sll	$5,$3,2
lui	$15,%hi(quantization_tab)
addu	$5,$10,$5
lui	$14,%hi(b5_mantissas)
lui	$10,%hi($L48)
lui	$13,%hi(b3_mantissas)
lui	$16,%hi(b2_mantissas)
lui	$25,%hi(b1_mantissas)
li	$12,65536			# 0x10000
li	$24,8323072			# 0x7f0000
addiu	$15,$15,%lo(quantization_tab)
li	$11,-4			# 0xfffffffffffffffc
li	$20,32			# 0x20
addiu	$10,$10,%lo($L48)
li	$19,4			# 0x4
addiu	$14,$14,%lo(b5_mantissas)
addiu	$17,$17,%lo(b4_mantissas)
addiu	$13,$13,%lo(b3_mantissas)
addiu	$16,$16,%lo(b2_mantissas)
addiu	$25,$25,%lo(b1_mantissas)
addu	$12,$4,$12
ori	$24,$24,0xffff
$L58:
addu	$2,$7,$3
lbu	$2,0($2)
sltu	$21,$2,6
beq	$21,$0,$L46
sll	$2,$2,2
addu	$2,$10,$2
lw	$2,0($2)
j	$2
.rdata
.align	2
.align	2
$L48:
.word	$L47
.word	$L49
.word	$L50
.word	$L51
.word	$L52
.word	$L53
.text
$L52:
lw	$2,28($6)
beq	$2,$0,$L57
lw	$2,16($6)
sw	$0,28($6)
$L54:
addu	$21,$8,$3
addiu	$5,$5,4
addiu	$3,$3,1
lb	$21,0($21)
sra	$2,$2,$21
.set	noreorder
.set	nomacro
bne	$3,$9,$L58
sw	$2,-4($5)
.set	macro
.set	reorder

$L43:
lw	$fp,36($sp)
lw	$23,32($sp)
lw	$22,28($sp)
lw	$21,24($sp)
lw	$20,20($sp)
lw	$19,16($sp)
lw	$18,12($sp)
lw	$17,8($sp)
lw	$16,4($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,40
.set	macro
.set	reorder

$L51:
lw	$21,12($4)
lw	$2,4($4)
srl	$22,$21,3
addu	$2,$2,$22
and	$22,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110010110100000000000010010000	#S32LDDR XR2,$22,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010110100000000010001010000	#S32LDDR XR1,$22,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$22,$21,0x7
addu	$2,$2,$22
li	$22,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010101101100010010100110	#S32EXTRV XR2,XR1,$2,$22
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
sll	$2,$2,2
addiu	$21,$21,3
addu	$2,$2,$13
sw	$21,12($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
lw	$2,0($2)
.set	macro
.set	reorder

$L53:
lw	$21,12($4)
lw	$2,4($4)
srl	$22,$21,3
addu	$2,$2,$22
and	$22,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110010110100000000000010010000	#S32LDDR XR2,$22,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010110100000000010001010000	#S32LDDR XR1,$22,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$22,$21,0x7
addu	$2,$2,$22
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010100111100010010100110	#S32EXTRV XR2,XR1,$2,$19
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
sll	$2,$2,2
addiu	$21,$21,4
addu	$2,$2,$14
sw	$21,12($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
lw	$2,0($2)
.set	macro
.set	reorder

$L49:
lw	$2,20($6)
beq	$2,$0,$L55
addiu	$2,$2,-1
sll	$21,$2,2
sw	$2,20($6)
addu	$2,$6,$21
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
lw	$2,0($2)
.set	macro
.set	reorder

$L47:
beq	$18,$0,$L61
lw	$22,-11140($12)
addiu	$21,$22,-55
addiu	$2,$22,-24
andi	$21,$21,0x3f
andi	$2,$2,0x3f
sll	$21,$21,2
sll	$2,$2,2
addu	$21,$12,$21
addu	$2,$12,$2
andi	$fp,$22,0x3f
lw	$23,-11396($21)
sll	$21,$fp,2
lw	$2,-11396($2)
addiu	$22,$22,1
addu	$21,$12,$21
addu	$23,$23,$2
li	$2,-4194304			# 0xffffffffffc00000
and	$fp,$23,$24
sw	$23,-11396($21)
addu	$2,$fp,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
sw	$22,-11140($12)
.set	macro
.set	reorder

$L50:
lw	$2,24($6)
.set	noreorder
.set	nomacro
beq	$2,$0,$L56
addiu	$21,$2,1
.set	macro
.set	reorder

addiu	$2,$2,-1
sll	$21,$21,2
addu	$21,$6,$21
sw	$2,24($6)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
lw	$2,0($21)
.set	macro
.set	reorder

$L46:
lw	$21,12($4)
addu	$2,$2,$15
lw	$fp,4($4)
srl	$23,$21,3
lbu	$22,0($2)
addu	$2,$fp,$23
and	$23,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110010111100000000000010010000	#S32LDDR XR2,$23,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010111100000000010001010000	#S32LDDR XR1,$23,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$23,$21,0x7
addu	$2,$2,$23
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010101101100010010100110	#S32EXTRV XR2,XR1,$2,$22
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
subu	$23,$20,$22
addu	$21,$21,$22
sll	$2,$2,$23
sw	$21,12($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
sra	$2,$2,8
.set	macro
.set	reorder

$L55:
lw	$22,12($4)
lw	$2,4($4)
srl	$21,$22,3
addu	$2,$2,$21
and	$21,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110010101100000000000010010000	#S32LDDR XR2,$21,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010101100000000010001010000	#S32LDDR XR1,$21,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$21,$22,0x7
addu	$2,$2,$21
li	$21,5			# 0x5
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010101011100010010100110	#S32EXTRV XR2,XR1,$2,$21
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000101010000000010101110	#S32M2I XR2, $21
# 0 "" 2
#NO_APP
sll	$2,$21,2
sll	$21,$21,4
addiu	$22,$22,5
subu	$2,$21,$2
li	$21,2			# 0x2
addu	$2,$2,$25
sw	$22,12($4)
sw	$21,20($6)
lw	$22,4($2)
lw	$21,8($2)
lw	$2,0($2)
sw	$22,4($6)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
sw	$21,0($6)
.set	macro
.set	reorder

$L56:
lw	$22,12($4)
lw	$2,4($4)
srl	$21,$22,3
addu	$2,$2,$21
and	$21,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110010101100000000000010010000	#S32LDDR XR2,$21,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010101100000000010001010000	#S32LDDR XR1,$21,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$21,$22,0x7
addu	$2,$2,$21
li	$21,7			# 0x7
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010101011100010010100110	#S32EXTRV XR2,XR1,$2,$21
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000101010000000010101110	#S32M2I XR2, $21
# 0 "" 2
#NO_APP
sll	$2,$21,2
sll	$21,$21,4
addiu	$22,$22,7
subu	$2,$21,$2
li	$21,2			# 0x2
addu	$2,$2,$16
sw	$22,12($4)
sw	$21,24($6)
lw	$22,4($2)
lw	$21,8($2)
lw	$2,0($2)
sw	$22,12($6)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
sw	$21,8($6)
.set	macro
.set	reorder

$L57:
lw	$21,12($4)
lw	$2,4($4)
srl	$22,$21,3
addu	$2,$2,$22
and	$22,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110010110100000000000010010000	#S32LDDR XR2,$22,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010110100000000010001010000	#S32LDDR XR1,$22,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$22,$21,0x7
addu	$2,$2,$22
li	$22,7			# 0x7
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010101101100010010100110	#S32EXTRV XR2,XR1,$2,$22
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
sll	$2,$2,3
addiu	$21,$21,7
addu	$2,$2,$17
li	$22,1			# 0x1
sw	$21,12($4)
lw	$21,4($2)
lw	$2,0($2)
sw	$22,28($6)
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
sw	$21,16($6)
.set	macro
.set	reorder

$L61:
.option	pic0
.set	noreorder
.set	nomacro
j	$L54
.option	pic2
move	$2,$0
.set	macro
.set	reorder

$L60:
.option	pic0
.set	noreorder
.set	nomacro
j	$L44
.option	pic2
li	$18,1			# 0x1
.set	macro
.set	reorder

.end	ac3_decode_transform_coeffs_ch
.size	ac3_decode_transform_coeffs_ch, .-ac3_decode_transform_coeffs_ch
.align	2
.set	nomips16
.set	nomicromips
.ent	ac3_decode_transform_coeffs_ch_skip
.type	ac3_decode_transform_coeffs_ch_skip, @function
ac3_decode_transform_coeffs_ch_skip:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
sll	$2,$5,2
lw	$10,20($6)
li	$3,65536			# 0x10000
lw	$9,24($6)
addu	$2,$4,$2
lw	$11,28($6)
li	$7,47112			# 0xb808
addu	$2,$2,$3
sll	$5,$5,8
lw	$3,-20584($2)
addu	$5,$5,$7
lw	$2,-20556($2)
slt	$7,$3,$2
beq	$7,$0,$L65
addu	$5,$4,$5

lui	$12,%hi(quantization_tab)
lui	$8,%hi($L68)
addu	$3,$5,$3
addiu	$12,$12,%lo(quantization_tab)
addu	$5,$5,$2
addiu	$8,$8,%lo($L68)
$L76:
lbu	$2,0($3)
sltu	$7,$2,6
beq	$7,$0,$L66
nop

sll	$2,$2,2
addu	$2,$8,$2
lw	$2,0($2)
j	$2
nop

.rdata
.align	2
.align	2
$L68:
.word	$L67
.word	$L69
.word	$L70
.word	$L71
.word	$L72
.word	$L73
.text
$L70:
beq	$9,$0,$L75
addiu	$9,$9,-1

$L67:
addiu	$3,$3,1
bne	$3,$5,$L76
nop

$L65:
sw	$10,20($6)
$L79:
sw	$9,24($6)
j	$31
sw	$11,28($6)

$L73:
lw	$2,12($4)
addiu	$3,$3,1
addiu	$2,$2,4
bne	$3,$5,$L76
sw	$2,12($4)

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L72:
bne	$11,$0,$L77
nop

lw	$2,12($4)
addiu	$3,$3,1
li	$11,1			# 0x1
addiu	$2,$2,7
bne	$3,$5,$L76
sw	$2,12($4)

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L71:
lw	$2,12($4)
addiu	$3,$3,1
addiu	$2,$2,3
bne	$3,$5,$L76
sw	$2,12($4)

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L69:
beq	$10,$0,$L74
nop

addiu	$3,$3,1
bne	$3,$5,$L76
addiu	$10,$10,-1

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L66:
addu	$2,$2,$12
lw	$7,12($4)
addiu	$3,$3,1
lbu	$2,0($2)
addu	$2,$2,$7
bne	$3,$5,$L76
sw	$2,12($4)

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L74:
lw	$2,12($4)
addiu	$3,$3,1
li	$10,2			# 0x2
addiu	$2,$2,5
bne	$3,$5,$L76
sw	$2,12($4)

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L77:
addiu	$3,$3,1
bne	$3,$5,$L76
move	$11,$0

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

$L75:
lw	$2,12($4)
addiu	$3,$3,1
li	$9,2			# 0x2
addiu	$2,$2,7
bne	$3,$5,$L76
sw	$2,12($4)

.option	pic0
j	$L79
.option	pic2
sw	$10,20($6)

.set	macro
.set	reorder
.end	ac3_decode_transform_coeffs_ch_skip
.size	ac3_decode_transform_coeffs_ch_skip, .-ac3_decode_transform_coeffs_ch_skip
.section	.text.unlikely,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	ac3_decode_end
.type	ac3_decode_end, @function
ac3_decode_end:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,24($sp)
sw	$31,28($sp)
.cprestore	16
lw	$16,136($4)
li	$4,54428			# 0xd49c
lw	$25,%call16(ff_mdct_end)($28)
.reloc	1f,R_MIPS_JALR,ff_mdct_end
1:	jalr	$25
addu	$4,$16,$4

li	$4,54492			# 0xd4dc
lw	$28,16($sp)
lw	$25,%call16(ff_mdct_end)($28)
.reloc	1f,R_MIPS_JALR,ff_mdct_end
1:	jalr	$25
addu	$4,$16,$4

lw	$28,16($sp)
lw	$25,%call16(av_freep)($28)
.reloc	1f,R_MIPS_JALR,av_freep
1:	jalr	$25
addiu	$4,$16,20

move	$2,$0
lw	$31,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	ac3_decode_end
.size	ac3_decode_end, .-ac3_decode_end
.align	2
.set	nomips16
.set	nomicromips
.ent	ac3_decode_init
.type	ac3_decode_init, @function
ac3_decode_init:
.frame	$sp,64,$31		# vars= 0, regs= 6/2, args= 24, gp= 8
.mask	0x801f0000,-12
.fmask	0x00300000,-8
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-64
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,36($sp)
li	$2,7			# 0x7
sw	$31,52($sp)
move	$17,$4
sw	$20,48($sp)
sw	$19,44($sp)
sw	$18,40($sp)
sw	$16,32($sp)
sdc1	$f20,56($sp)
.cprestore	24
#APP
# 219 "ac3dec.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
lw	$16,136($4)
lw	$25,%call16(ac3_common_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ac3_common_init
1:	jalr	$25
sw	$4,0($16)
.set	macro
.set	reorder

lui	$9,%hi(ungroup_3_in_7_bits_tab)
lw	$28,24($sp)
move	$3,$0
addiu	$9,$9,%lo(ungroup_3_in_7_bits_tab)
li	$6,25			# 0x19
li	$5,5			# 0x5
li	$10,128			# 0x80
move	$2,$9
$L83:
teq	$6,$0,7
div	$0,$3,$6
addiu	$2,$2,3
addiu	$3,$3,1
mfhi	$4
mflo	$8
teq	$5,$0,7
div	$0,$4,$5
sb	$8,-3($2)
mfhi	$7
mflo	$4
sb	$7,-1($2)
.set	noreorder
.set	nomacro
bne	$3,$10,$L83
sb	$4,-2($2)
.set	macro
.set	reorder

lui	$5,%hi(b1_mantissas)
lw	$7,%got(ff_ac3_ungroup_3_in_5_bits_tab)($28)
lui	$8,%hi(b1_mantissas+384)
addiu	$5,$5,%lo(b1_mantissas)
addiu	$8,$8,%lo(b1_mantissas+384)
li	$6,3			# 0x3
$L84:
lbu	$4,0($7)
addiu	$7,$7,3
lbu	$3,-2($7)
addiu	$5,$5,12
addiu	$4,$4,-1
addiu	$3,$3,-1
sll	$4,$4,24
lbu	$2,-1($7)
sll	$3,$3,24
teq	$6,$0,7
div	$0,$4,$6
addiu	$2,$2,-1
sll	$2,$2,24
mflo	$4
teq	$6,$0,7
div	$0,$3,$6
sw	$4,-12($5)
mflo	$3
teq	$6,$0,7
div	$0,$2,$6
sw	$3,-8($5)
mflo	$2
.set	noreorder
.set	nomacro
bne	$5,$8,$L84
sw	$2,-4($5)
.set	macro
.set	reorder

lui	$10,%hi(b2_mantissas)
lui	$12,%hi(b4_mantissas)
addiu	$10,$10,%lo(b2_mantissas)
addiu	$12,$12,%lo(b4_mantissas)
move	$11,$0
li	$8,5			# 0x5
li	$7,11			# 0xb
li	$13,128			# 0x80
$L85:
teq	$7,$0,7
div	$0,$11,$7
lbu	$6,0($9)
lbu	$5,1($9)
addiu	$10,$10,12
lbu	$4,2($9)
addiu	$12,$12,8
addiu	$6,$6,-2
addiu	$5,$5,-2
sll	$6,$6,24
sll	$5,$5,24
addiu	$4,$4,-2
addiu	$11,$11,1
sll	$4,$4,24
addiu	$9,$9,3
mfhi	$2
mflo	$3
teq	$8,$0,7
div	$0,$6,$8
addiu	$2,$2,-5
addiu	$3,$3,-5
sll	$2,$2,24
sll	$3,$3,24
mflo	$6
teq	$8,$0,7
div	$0,$5,$8
sw	$6,-12($10)
mflo	$5
teq	$8,$0,7
div	$0,$4,$8
sw	$5,-8($10)
mflo	$4
teq	$7,$0,7
div	$0,$3,$7
sw	$4,-4($10)
mflo	$3
teq	$7,$0,7
div	$0,$2,$7
sw	$3,-8($12)
mflo	$2
.set	noreorder
.set	nomacro
bne	$11,$13,$L85
sw	$2,-4($12)
.set	macro
.set	reorder

lui	$6,%hi(b3_mantissas)
li	$2,-3			# 0xfffffffffffffffd
addiu	$6,$6,%lo(b3_mantissas)
li	$5,7			# 0x7
li	$7,4			# 0x4
$L86:
sll	$3,$2,24
sll	$4,$2,2
teq	$5,$0,7
div	$0,$3,$5
addu	$4,$6,$4
addiu	$2,$2,1
mflo	$3
.set	noreorder
.set	nomacro
bne	$2,$7,$L86
sw	$3,12($4)
.set	macro
.set	reorder

lui	$6,%hi(b5_mantissas)
li	$2,-7			# 0xfffffffffffffff9
addiu	$6,$6,%lo(b5_mantissas)
li	$5,15			# 0xf
li	$7,8			# 0x8
$L87:
sll	$3,$2,24
sll	$4,$2,2
teq	$5,$0,7
div	$0,$3,$5
addu	$4,$6,$4
addiu	$2,$2,1
mflo	$3
.set	noreorder
.set	nomacro
bne	$2,$7,$L87
sw	$3,28($4)
.set	macro
.set	reorder

lui	$2,%hi($LC3)
lui	$19,%hi(dynamic_range_tab)
lwc1	$f20,%lo($LC3)($2)
move	$18,$0
addiu	$19,$19,%lo(dynamic_range_tab)
li	$20,256			# 0x100
$L88:
sra	$2,$18,7
mov.s	$f12,$f20
lw	$25,%call16(powf)($28)
sra	$3,$18,5
sll	$2,$2,3
subu	$2,$3,$2
addiu	$2,$2,-5
mtc1	$2,$f0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,powf
1:	jalr	$25
cvt.s.w	$f14,$f0
.set	macro
.set	reorder

andi	$2,$18,0x1f
ori	$2,$2,0x20
lw	$28,24($sp)
mtc1	$2,$f2
sll	$2,$18,2
addiu	$18,$18,1
addu	$2,$19,$2
cvt.s.w	$f1,$f2
mul.s	$f0,$f1,$f0
.set	noreorder
.set	nomacro
bne	$18,$20,$L88
swc1	$f0,0($2)
.set	macro
.set	reorder

lui	$2,%hi($LC4)
lw	$25,%call16(aac_mdct_init)($28)
li	$18,54492			# 0xd4dc
ldc1	$f20,%lo($LC4)($2)
li	$5,8			# 0x8
addu	$4,$16,$18
li	$6,1			# 0x1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_mdct_init
1:	jalr	$25
sdc1	$f20,16($sp)
.set	macro
.set	reorder

li	$4,54428			# 0xd49c
lw	$28,24($sp)
li	$5,9			# 0x9
addu	$4,$16,$4
sdc1	$f20,16($sp)
lw	$25,%call16(aac_mdct_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_mdct_init
1:	jalr	$25
li	$6,1			# 0x1
.set	macro
.set	reorder

lui	$2,%hi($LC5)
lw	$28,24($sp)
addiu	$4,$18,25700
li	$6,256			# 0x100
lw	$5,%lo($LC5)($2)
lw	$25,%call16(aac_kbd_window_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_kbd_window_init
1:	jalr	$25
addu	$4,$16,$4
.set	macro
.set	reorder

li	$4,54556			# 0xd51c
lw	$28,24($sp)
move	$5,$17
lw	$25,%call16(dsputil_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,dsputil_init
1:	jalr	$25
addu	$4,$16,$4
.set	macro
.set	reorder

li	$4,54140			# 0xd37c
lw	$28,24($sp)
move	$5,$0
lw	$25,%call16(av_lfg_init)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_lfg_init
1:	jalr	$25
addu	$4,$16,$4
.set	macro
.set	reorder

li	$2,65536			# 0x10000
lw	$28,24($sp)
addu	$2,$16,$2
lw	$3,-8300($2)
lw	$2,%got(ff_float_to_int16_interleave_c)($28)
.set	noreorder
.set	nomacro
beq	$3,$2,$L94
lui	$2,%hi($LC2)
.set	macro
.set	reorder

mtc1	$0,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L89
.option	pic2
lwc1	$f1,%lo($LC2)($2)
.set	macro
.set	reorder

$L94:
lui	$2,%hi($LC0)
lwc1	$f1,%lo($LC0)($2)
lui	$2,%hi($LC1)
lwc1	$f0,%lo($LC1)($2)
$L89:
li	$2,65536			# 0x10000
lw	$3,68($17)
addu	$2,$16,$2
swc1	$f1,-6856($2)
.set	noreorder
.set	nomacro
blez	$3,$L90
swc1	$f0,-6860($2)
.set	macro
.set	reorder

lw	$4,824($17)
addiu	$2,$4,-1
sltu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L90
slt	$3,$4,$3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L102
li	$2,65536			# 0x10000
.set	macro
.set	reorder

sw	$4,68($17)
$L90:
li	$2,65536			# 0x10000
$L102:
lw	$3,252($17)
li	$4,1			# 0x1
addu	$2,$16,$2
.set	noreorder
.set	nomacro
bgtz	$3,$L91
sw	$4,-20604($2)
.set	macro
.set	reorder

$L93:
li	$3,1			# 0x1
move	$2,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L92
.option	pic2
sw	$3,72($17)
.set	macro
.set	reorder

$L91:
lw	$25,%call16(av_mallocz)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_mallocz
1:	jalr	$25
li	$4,32776			# 0x8008
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L93
sw	$2,20($16)
.set	macro
.set	reorder

li	$2,-12			# 0xfffffffffffffff4
$L92:
lw	$31,52($sp)
lw	$20,48($sp)
lw	$19,44($sp)
lw	$18,40($sp)
lw	$17,36($sp)
lw	$16,32($sp)
ldc1	$f20,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

.end	ac3_decode_init
.size	ac3_decode_init, .-ac3_decode_init
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_band_structure.constprop.8
.type	decode_band_structure.constprop.8, @function
decode_band_structure.constprop.8:
.frame	$sp,80,$31		# vars= 48, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-80
lui	$28,%hi(__gnu_local_gp)
sw	$31,76($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$9,96($sp)
lw	$11,104($sp)
lw	$10,108($sp)
.cprestore	16
beq	$6,$0,$L107
subu	$9,$9,$7

lw	$3,8($4)
lw	$6,0($4)
srl	$2,$3,3
andi	$8,$3,0x7
addu	$6,$6,$2
addiu	$3,$3,1
lbu	$2,0($6)
sll	$2,$2,$8
andi	$2,$2,0x00ff
srl	$2,$2,7
beq	$2,$0,$L136
sw	$3,8($4)

$L107:
slt	$2,$9,2
bne	$2,$0,$L106
addiu	$7,$sp,24

lw	$3,8($4)
addiu	$7,$sp,24
lw	$13,0($4)
addiu	$8,$3,-1
move	$12,$3
addu	$8,$8,$9
$L110:
srl	$5,$3,3
andi	$6,$3,0x7
addu	$5,$13,$5
addiu	$3,$3,1
lbu	$2,0($5)
subu	$5,$3,$12
sw	$3,8($4)
addu	$5,$7,$5
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
bne	$3,$8,$L110
sb	$2,-1($5)

$L106:
beq	$11,$0,$L137
move	$4,$7

$L112:
li	$3,12			# 0xc
slt	$2,$9,2
move	$6,$9
bne	$2,$0,$L122
sb	$3,48($sp)

$L121:
move	$3,$0
li	$2,1			# 0x1
addiu	$7,$sp,24
li	$12,12			# 0xc
.option	pic0
j	$L117
.option	pic2
move	$6,$9

$L139:
lbu	$5,24($8)
addiu	$2,$2,1
addiu	$5,$5,12
sb	$5,24($8)
slt	$5,$2,$9
beq	$5,$0,$L138
addiu	$6,$6,-1

$L117:
addu	$5,$4,$2
$L141:
lbu	$5,-1($5)
bne	$5,$0,$L139
addu	$8,$7,$3

addiu	$3,$3,1
addiu	$2,$2,1
addu	$5,$7,$3
sb	$12,24($5)
slt	$5,$2,$9
bne	$5,$0,$L141
addu	$5,$4,$2

$L138:
beq	$11,$0,$L118
nop

$L122:
sw	$6,0($11)
$L118:
beq	$10,$0,$L103
lw	$25,%call16(memcpy)($28)

$L140:
addiu	$5,$sp,48
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$4,$10

$L103:
lw	$31,76($sp)
$L142:
j	$31
addiu	$sp,$sp,80

$L136:
bne	$5,$0,$L142
lw	$31,76($sp)

lw	$2,100($sp)
addiu	$4,$7,1
bne	$11,$0,$L112
addu	$4,$2,$4

$L137:
beq	$10,$0,$L103
li	$3,12			# 0xc

slt	$2,$9,2
move	$6,$9
beq	$2,$0,$L121
sb	$3,48($sp)

.option	pic0
j	$L140
.option	pic2
lw	$25,%call16(memcpy)($28)

.set	macro
.set	reorder
.end	decode_band_structure.constprop.8
.size	decode_band_structure.constprop.8, .-decode_band_structure.constprop.8
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC6:
.ascii	"frame sync error\012\000"
.align	2
$LC7:
.ascii	"invalid bitstream id\012\000"
.align	2
$LC8:
.ascii	"invalid sample rate\012\000"
.align	2
$LC9:
.ascii	"invalid frame size\012\000"
.align	2
$LC10:
.ascii	"unsupported frame type : skipping frame\012\000"
.align	2
$LC11:
.ascii	"invalid frame type\012\000"
.align	2
$LC12:
.ascii	"invalid header\012\000"
.align	2
$LC13:
.ascii	"incomplete frame\012\000"
.align	2
$LC14:
.ascii	"frame CRC mismatch\012\000"
.align	2
$LC17:
.ascii	"invalid spectral extension range (%d >= %d)\012\000"
.align	2
$LC18:
.ascii	"invalid spectral extension copy start bin (%d >= %d)\012"
.ascii	"\000"
.align	2
$LC23:
.ascii	"coupling not allowed in mono or dual-mono\012\000"
.align	2
$LC24:
.ascii	"Enhanced coupling\000"
.align	2
$LC25:
.ascii	"invalid coupling range (%d >= %d)\012\000"
.align	2
$LC26:
.ascii	"new coupling strategy must be present in block 0\012\000"
.align	2
$LC27:
.ascii	"new coupling coordinates must be present in block 0\012\000"
.align	2
$LC28:
.ascii	"Warning: new rematrixing strategy not present in block 0"
.ascii	"\012\000"
.align	2
$LC29:
.ascii	"bandwidth code = %d > 60\012\000"
.align	2
$LC30:
.ascii	"exponent out-of-range\012\000"
.align	2
$LC31:
.ascii	"new bit allocation info must be present in block 0\012\000"
.align	2
$LC32:
.ascii	"new snr offsets must be present in block 0\012\000"
.align	2
$LC33:
.ascii	"new coupling leak info must be present in block 0\012\000"
.align	2
$LC34:
.ascii	"delta bit allocation strategy reserved\012\000"
.align	2
$LC35:
.ascii	"error in bit allocation\012\000"
.align	2
$LC39:
.ascii	"error decoding the audio block\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	ac3_decode_frame
.type	ac3_decode_frame, @function
ac3_decode_frame:
.frame	$sp,288,$31		# vars= 152, regs= 10/10, args= 48, gp= 8
.mask	0xc0ff0000,-44
.fmask	0x3ff00000,-8
addiu	$sp,$sp,-288
lui	$28,%hi(__gnu_local_gp)
sw	$17,212($sp)
sw	$16,208($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$4,288($sp)
lw	$17,136($4)
lw	$16,16($7)
lw	$7,20($7)
sw	$31,244($sp)
sw	$fp,240($sp)
sw	$23,236($sp)
sw	$22,232($sp)
sw	$21,228($sp)
sw	$20,224($sp)
sw	$19,220($sp)
sw	$18,216($sp)
.cprestore	48
sw	$5,292($sp)
sw	$6,296($sp)
sw	$7,184($sp)
lw	$4,20($17)
sdc1	$f28,280($sp)
sdc1	$f26,272($sp)
sdc1	$f24,264($sp)
sdc1	$f22,256($sp)
.set	noreorder
.set	nomacro
beq	$4,$0,$L144
sdc1	$f20,248($sp)
.set	macro
.set	reorder

li	$2,32769			# 0x8001
lw	$25,%call16(memcpy)($28)
li	$6,32768			# 0x8000
slt	$2,$7,$2
movn	$6,$7,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$16
.set	macro
.set	reorder

lw	$8,184($sp)
lw	$28,48($sp)
sll	$3,$8,3
sra	$2,$3,3
.set	noreorder
.set	nomacro
bltz	$2,$L636
lw	$4,20($17)
.set	macro
.set	reorder

bltz	$3,$L636
addu	$2,$4,$2
$L147:
lw	$25,%call16(ff_ac3_parse_header)($28)
addiu	$10,$17,4
sw	$3,16($17)
addiu	$20,$sp,56
sw	$4,4($17)
move	$4,$10
sw	$2,8($17)
move	$5,$20
sw	$0,12($17)
sw	$10,188($sp)
lw	$3,296($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_ac3_parse_header
1:	jalr	$25
sw	$0,0($3)
.set	macro
.set	reorder

lw	$28,48($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L597
sw	$2,180($sp)
.set	macro
.set	reorder

lbu	$6,60($sp)
li	$3,65536			# 0x10000
lw	$5,104($sp)
addu	$4,$17,$3
lhu	$2,90($sp)
lhu	$19,98($sp)
sw	$6,-18520($4)
lbu	$18,62($sp)
sw	$5,52($17)
sw	$18,48($17)
lbu	$5,63($sp)
sw	$5,56($17)
lbu	$6,88($sp)
sw	$6,-18516($4)
sw	$2,40($17)
lw	$2,92($sp)
sw	$2,36($17)
lbu	$6,96($sp)
subu	$2,$6,$5
sw	$6,-20668($4)
addiu	$6,$2,1
sw	$2,-20672($4)
sw	$6,-20664($4)
sw	$19,32($17)
lw	$4,72($sp)
sw	$4,64($17)
lw	$4,76($sp)
sw	$4,68($17)
lw	$4,84($sp)
sw	$4,44($17)
lbu	$4,64($sp)
sw	$4,24($17)
lw	$4,68($sp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L149
sw	$4,28($17)
.set	macro
.set	reorder

sll	$2,$2,2
li	$4,7			# 0x7
addu	$2,$17,$2
addu	$3,$2,$3
sw	$4,-20552($3)
li	$4,2			# 0x2
sw	$0,-20580($3)
sw	$4,-20504($3)
sw	$0,156($2)
$L149:
lbu	$2,61($sp)
sltu	$2,$2,11
.set	noreorder
.set	nomacro
bne	$2,$0,$L954
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$25,%call16(ff_eac3_parse_header)($28)
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_parse_header
1:	jalr	$25
sw	$2,72($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L955
lw	$28,48($sp)
.set	macro
.set	reorder

lw	$19,32($17)
$L596:
lw	$9,184($sp)
$L1038:
slt	$2,$9,$19
.set	noreorder
.set	nomacro
bne	$2,$0,$L1053
lui	$6,%hi($LC13)
.set	macro
.set	reorder

$L172:
lw	$3,288($sp)
lw	$2,252($3)
.set	noreorder
.set	nomacro
blez	$2,$L173
lw	$25,%call16(av_crc_get_table)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_crc_get_table
1:	jalr	$25
li	$4,1			# 0x1
.set	macro
.set	reorder

move	$5,$0
lw	$28,48($sp)
addiu	$6,$16,2
lw	$7,32($17)
move	$4,$2
lw	$25,%call16(av_crc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_crc
1:	jalr	$25
addiu	$7,$7,-2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L957
lw	$28,48($sp)
.set	macro
.set	reorder

$L173:
li	$3,65536			# 0x10000
lw	$4,36($17)
lw	$9,288($sp)
addu	$3,$17,$3
lw	$6,40($17)
lw	$2,48($17)
lw	$5,56($17)
sw	$4,4($9)
sw	$6,64($9)
sw	$2,-20600($3)
lw	$4,-20668($3)
.set	noreorder
.set	nomacro
beq	$5,$0,$L599
sw	$4,-20596($3)
.set	macro
.set	reorder

ori	$5,$2,0x8
sw	$5,-20600($3)
$L599:
lw	$9,288($sp)
lw	$8,824($9)
addiu	$3,$8,-1
sltu	$3,$3,2
bne	$3,$0,$L958
$L927:
lw	$2,52($17)
li	$3,65536			# 0x10000
lw	$8,288($sp)
addu	$3,$17,$3
sw	$4,68($8)
sra	$4,$2,31
sw	$2,848($8)
sw	$4,852($8)
$L918:
lw	$2,-20600($3)
lw	$22,-20596($3)
li	$3,-9			# 0xfffffffffffffff7
and	$2,$2,$3
$L180:
lw	$3,56($17)
sll	$5,$2,2
sll	$2,$2,4
sll	$4,$3,1
sll	$3,$3,3
subu	$2,$2,$5
subu	$3,$3,$4
addu	$2,$3,$2
lw	$3,%got(ff_ac3_dec_channel_map)($28)
.set	noreorder
.set	nomacro
blez	$22,$L198
addu	$3,$3,$2
.set	macro
.set	reorder

lbu	$4,0($3)
li	$2,65536			# 0x10000
addiu	$2,$2,16704
sll	$4,$4,10
addu	$4,$4,$2
addu	$4,$17,$4
sw	$4,112($sp)
li	$4,1			# 0x1
beq	$22,$4,$L198
lbu	$4,1($3)
sll	$4,$4,10
addu	$4,$4,$2
addu	$4,$17,$4
sw	$4,116($sp)
li	$4,2			# 0x2
beq	$22,$4,$L198
lbu	$4,2($3)
sll	$4,$4,10
addu	$4,$4,$2
addu	$4,$17,$4
sw	$4,120($sp)
li	$4,3			# 0x3
beq	$22,$4,$L198
lbu	$4,3($3)
sll	$4,$4,10
addu	$4,$4,$2
addu	$4,$17,$4
sw	$4,124($sp)
li	$4,4			# 0x4
beq	$22,$4,$L198
lbu	$4,4($3)
sll	$4,$4,10
addu	$4,$4,$2
addu	$4,$17,$4
sw	$4,128($sp)
li	$4,5			# 0x5
beq	$22,$4,$L198
lbu	$4,5($3)
sll	$4,$4,10
addu	$4,$4,$2
addu	$4,$17,$4
sw	$4,132($sp)
li	$4,6			# 0x6
beq	$22,$4,$L198
lbu	$3,6($3)
sll	$3,$3,10
addu	$2,$3,$2
addu	$2,$17,$2
sw	$2,136($sp)
$L198:
lw	$10,44($17)
.set	noreorder
.set	nomacro
blez	$10,$L197
lui	$2,%hi($LC38)
.set	macro
.set	reorder

lui	$4,%hi($LC16)
ldc1	$f20,%lo($LC38)($2)
lui	$2,%hi($LC19)
addiu	$3,$17,104
ldc1	$f22,%lo($LC16)($4)
move	$18,$0
move	$16,$0
ldc1	$f26,%lo($LC19)($2)
li	$2,46848			# 0xb700
sw	$3,172($sp)
move	$19,$20
addu	$2,$17,$2
mov.d	$f24,$f20
sw	$2,192($sp)
mov.d	$f28,$f20
$L595:
lw	$8,180($sp)
.set	noreorder
.set	nomacro
beq	$8,$0,$L959
li	$2,65536			# 0x10000
.set	macro
.set	reorder

addu	$2,$17,$2
lw	$fp,-20668($2)
$L607:
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$22,$2,$L588
li	$2,6			# 0x6
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$22,$L589
lw	$8,292($sp)
.set	macro
.set	reorder

sll	$11,$22,2
sll	$6,$22,1
move	$7,$0
addiu	$9,$sp,112
$L590:
addu	$2,$9,$7
move	$3,$8
lw	$2,0($2)
addiu	$5,$2,1024
$L594:
lw	$4,0($2)
addiu	$2,$2,4
sh	$4,0($3)
.set	noreorder
.set	nomacro
bne	$2,$5,$L594
addu	$3,$3,$6
.set	macro
.set	reorder

addiu	$7,$7,4
.set	noreorder
.set	nomacro
bne	$7,$11,$L590
addiu	$8,$8,2
.set	macro
.set	reorder

$L589:
lw	$9,292($sp)
sll	$2,$22,9
lw	$8,172($sp)
addiu	$16,$16,1
addiu	$18,$18,28
addu	$9,$9,$2
addiu	$8,$8,4
slt	$3,$16,$10
sw	$9,292($sp)
lw	$9,192($sp)
sw	$8,172($sp)
addiu	$9,$9,28
.set	noreorder
.set	nomacro
bne	$3,$0,$L595
sw	$9,192($sp)
.set	macro
.set	reorder

$L197:
lw	$3,288($sp)
$L1029:
sll	$10,$10,8
lw	$8,184($sp)
lw	$2,68($3)
lw	$3,296($sp)
mul	$2,$10,$2
sll	$2,$2,1
sw	$2,0($3)
lw	$3,32($17)
slt	$2,$8,$3
movz	$8,$3,$2
move	$2,$8
$L899:
lw	$31,244($sp)
lw	$fp,240($sp)
lw	$23,236($sp)
lw	$22,232($sp)
lw	$21,228($sp)
lw	$20,224($sp)
lw	$19,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
ldc1	$f28,280($sp)
ldc1	$f26,272($sp)
ldc1	$f24,264($sp)
ldc1	$f22,256($sp)
ldc1	$f20,248($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,288
.set	macro
.set	reorder

$L959:
li	$6,65536			# 0x10000
lw	$9,48($17)
lw	$2,80($17)
addiu	$20,$sp,140
addu	$6,$17,$6
sw	$0,140($sp)
sh	$0,4($20)
sw	$9,156($sp)
sb	$0,6($20)
.set	noreorder
.set	nomacro
beq	$2,$0,$L200
lw	$21,-20672($6)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$21,$L201
li	$8,1			# 0x1
.set	macro
.set	reorder

lw	$5,12($17)
lw	$3,4($17)
srl	$7,$5,3
andi	$9,$5,0x7
addu	$7,$3,$7
addiu	$2,$5,1
lbu	$4,0($7)
sw	$2,12($17)
sll	$4,$4,$9
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$21,$8,$L640
sw	$4,-11132($6)
.set	macro
.set	reorder

srl	$9,$2,3
andi	$10,$2,0x7
addu	$9,$3,$9
addiu	$2,$5,2
lbu	$7,0($9)
sw	$2,12($17)
sll	$7,$7,$10
andi	$7,$7,0x00ff
srl	$7,$7,7
xor	$10,$4,$7
sw	$7,-11128($6)
li	$7,2			# 0x2
.set	noreorder
.set	nomacro
beq	$21,$7,$L202
sw	$10,160($sp)
.set	macro
.set	reorder

srl	$9,$2,3
andi	$10,$2,0x7
addu	$9,$3,$9
addiu	$2,$5,3
lbu	$7,0($9)
sw	$2,12($17)
sll	$7,$7,$10
lw	$10,160($sp)
andi	$7,$7,0x00ff
srl	$7,$7,7
xor	$9,$4,$7
movn	$10,$8,$9
sw	$7,-11124($6)
li	$7,3			# 0x3
.set	noreorder
.set	nomacro
beq	$21,$7,$L202
sw	$10,160($sp)
.set	macro
.set	reorder

srl	$9,$2,3
andi	$10,$2,0x7
addu	$9,$3,$9
addiu	$2,$5,4
lbu	$7,0($9)
sw	$2,12($17)
sll	$7,$7,$10
lw	$10,160($sp)
andi	$7,$7,0x00ff
srl	$7,$7,7
xor	$9,$4,$7
movn	$10,$8,$9
sw	$7,-11120($6)
li	$7,4			# 0x4
.set	noreorder
.set	nomacro
beq	$21,$7,$L202
sw	$10,160($sp)
.set	macro
.set	reorder

srl	$9,$2,3
andi	$10,$2,0x7
addu	$9,$3,$9
addiu	$2,$5,5
lbu	$7,0($9)
sw	$2,12($17)
sll	$7,$7,$10
lw	$10,160($sp)
andi	$7,$7,0x00ff
srl	$7,$7,7
xor	$9,$4,$7
movn	$10,$8,$9
sw	$7,-11116($6)
li	$7,5			# 0x5
.set	noreorder
.set	nomacro
beq	$21,$7,$L202
sw	$10,160($sp)
.set	macro
.set	reorder

srl	$9,$2,3
andi	$10,$2,0x7
addu	$9,$3,$9
addiu	$2,$5,6
lbu	$7,0($9)
lw	$9,160($sp)
sw	$2,12($17)
sll	$7,$7,$10
andi	$7,$7,0x00ff
srl	$7,$7,7
xor	$4,$4,$7
movn	$9,$8,$4
li	$4,6			# 0x6
sw	$7,-11112($6)
.set	noreorder
.set	nomacro
beq	$21,$4,$L202
sw	$9,160($sp)
.set	macro
.set	reorder

addiu	$5,$5,7
$L1055:
sw	$5,12($17)
$L200:
lw	$2,84($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1054
lw	$10,156($sp)
.set	macro
.set	reorder

lw	$3,4($17)
lw	$2,12($17)
.set	noreorder
.set	nomacro
blez	$21,$L208
sw	$0,160($sp)
.set	macro
.set	reorder

srl	$4,$2,3
$L1088:
andi	$7,$2,0x7
addu	$4,$3,$4
move	$5,$2
addiu	$2,$2,1
lbu	$6,0($4)
li	$4,65536			# 0x10000
addu	$4,$17,$4
sw	$2,12($17)
sll	$6,$6,$7
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-11420($4)
li	$6,1			# 0x1
.set	noreorder
.set	nomacro
beq	$21,$6,$L208
srl	$7,$2,3
.set	macro
.set	reorder

andi	$8,$2,0x7
addu	$7,$3,$7
addiu	$2,$5,2
lbu	$6,0($7)
sw	$2,12($17)
sll	$6,$6,$8
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-11416($4)
li	$6,2			# 0x2
.set	noreorder
.set	nomacro
beq	$21,$6,$L208
srl	$7,$2,3
.set	macro
.set	reorder

andi	$8,$2,0x7
addu	$7,$3,$7
addiu	$2,$5,3
lbu	$6,0($7)
sw	$2,12($17)
sll	$6,$6,$8
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-11412($4)
li	$6,3			# 0x3
.set	noreorder
.set	nomacro
beq	$21,$6,$L208
srl	$7,$2,3
.set	macro
.set	reorder

andi	$8,$2,0x7
addu	$7,$3,$7
addiu	$2,$5,4
lbu	$6,0($7)
sw	$2,12($17)
sll	$6,$6,$8
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-11408($4)
li	$6,4			# 0x4
.set	noreorder
.set	nomacro
beq	$21,$6,$L208
srl	$7,$2,3
.set	macro
.set	reorder

andi	$8,$2,0x7
addu	$7,$3,$7
addiu	$2,$5,5
lbu	$6,0($7)
sw	$2,12($17)
sll	$6,$6,$8
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-11404($4)
li	$6,5			# 0x5
.set	noreorder
.set	nomacro
beq	$21,$6,$L208
srl	$7,$2,3
.set	macro
.set	reorder

andi	$8,$2,0x7
addu	$7,$3,$7
addiu	$2,$5,6
lbu	$6,0($7)
sw	$2,12($17)
sll	$6,$6,$8
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-11400($4)
li	$4,6			# 0x6
.set	noreorder
.set	nomacro
bne	$21,$4,$L1055
addiu	$5,$5,7
.set	macro
.set	reorder

$L208:
lw	$10,156($sp)
$L1049:
.set	noreorder
.set	nomacro
beq	$16,$0,$L210
sltu	$6,$10,1
.set	macro
.set	reorder

$L962:
lui	$4,%hi($LC16)
lui	$7,%hi(dynamic_range_tab)
ldc1	$f6,%lo($LC16)($4)
lui	$4,%hi($LC4)
li	$9,-4			# 0xfffffffffffffffc
ldc1	$f4,%lo($LC4)($4)
li	$8,8			# 0x8
addiu	$7,$7,%lo(dynamic_range_tab)
srl	$5,$2,3
$L1090:
andi	$10,$2,0x7
addu	$5,$3,$5
addiu	$2,$2,1
lbu	$4,0($5)
sll	$4,$4,$10
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L211
sw	$2,12($17)
.set	macro
.set	reorder

srl	$4,$2,3
addu	$4,$3,$4
and	$5,$4,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110000101100000000000010010000	#S32LDDR XR2,$5,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000101100000000010001010000	#S32LDDR XR1,$5,4
# 0 "" 2
#NO_APP
andi	$4,$4,0x3
sll	$4,$4,3
andi	$5,$2,0x7
addu	$4,$4,$5
#APP
# 117 "ac3dec.c" 1
.word	0b01110000100010001100010010100110	#S32EXTRV XR2,XR1,$4,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
sll	$5,$5,2
lw	$10,0($17)
addiu	$4,$6,11236
addu	$5,$5,$7
lwc1	$f2,828($10)
sll	$4,$4,2
addiu	$2,$2,8
lwc1	$f0,0($5)
addu	$4,$17,$4
sw	$2,12($17)
cvt.d.s	$f2,$f2
cvt.d.s	$f0,$f0
add.d	$f0,$f0,$f6
mul.d	$f0,$f0,$f2
add.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
swc1	$f0,0($4)
$L211:
.set	noreorder
.set	nomacro
beq	$6,$0,$L960
move	$6,$0
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1090
.option	pic2
srl	$5,$2,3
.set	macro
.set	reorder

$L588:
.set	noreorder
.set	nomacro
beq	$fp,$2,$L961
lw	$3,112($sp)
.set	macro
.set	reorder

lw	$2,292($sp)
addiu	$9,$3,1024
lw	$4,116($sp)
$L593:
lw	$8,0($3)
addiu	$4,$4,8
lw	$7,-8($4)
addiu	$3,$3,8
addiu	$2,$2,8
lw	$6,-4($3)
lw	$5,-4($4)
sh	$8,-8($2)
sh	$7,-6($2)
sh	$6,-4($2)
.set	noreorder
.set	nomacro
bne	$3,$9,$L593
sh	$5,-2($2)
.set	macro
.set	reorder

lw	$9,292($sp)
sll	$2,$22,9
lw	$8,172($sp)
addiu	$16,$16,1
addiu	$18,$18,28
addu	$9,$9,$2
addiu	$8,$8,4
slt	$3,$16,$10
sw	$9,292($sp)
lw	$9,192($sp)
sw	$8,172($sp)
addiu	$9,$9,28
.set	noreorder
.set	nomacro
bne	$3,$0,$L595
sw	$9,192($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1029
.option	pic2
lw	$3,288($sp)
.set	macro
.set	reorder

$L201:
lw	$10,156($sp)
$L1054:
lw	$3,4($17)
lw	$2,12($17)
sw	$0,160($sp)
.set	noreorder
.set	nomacro
bne	$16,$0,$L962
sltu	$6,$10,1
.set	macro
.set	reorder

$L210:
srl	$5,$2,3
lui	$4,%hi($LC0)
addu	$5,$3,$5
lwc1	$f6,%lo($LC0)($4)
lui	$4,%hi($LC4)
andi	$10,$2,0x7
ldc1	$f4,%lo($LC4)($4)
addiu	$2,$2,1
lbu	$4,0($5)
lui	$7,%hi(dynamic_range_tab)
li	$9,-4			# 0xfffffffffffffffc
li	$8,8			# 0x8
sw	$2,12($17)
sll	$4,$4,$10
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L963
addiu	$7,$7,%lo(dynamic_range_tab)
.set	macro
.set	reorder

$L214:
addiu	$4,$6,11236
sll	$4,$4,2
addu	$4,$17,$4
.set	noreorder
.set	nomacro
beq	$6,$0,$L964
swc1	$f6,0($4)
.set	macro
.set	reorder

$L642:
srl	$5,$2,3
andi	$10,$2,0x7
addu	$5,$3,$5
addiu	$2,$2,1
move	$6,$0
lbu	$4,0($5)
sll	$4,$4,$10
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L214
sw	$2,12($17)
.set	macro
.set	reorder

$L963:
srl	$4,$2,3
addu	$4,$3,$4
and	$5,$4,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110000101100000000000010010000	#S32LDDR XR2,$5,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000101100000000010001010000	#S32LDDR XR1,$5,4
# 0 "" 2
#NO_APP
andi	$4,$4,0x3
sll	$4,$4,3
andi	$5,$2,0x7
addu	$4,$4,$5
#APP
# 117 "ac3dec.c" 1
.word	0b01110000100010001100010010100110	#S32EXTRV XR2,XR1,$4,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
sll	$5,$5,2
lw	$10,0($17)
addiu	$4,$6,11236
addu	$5,$5,$7
lwc1	$f2,828($10)
sll	$4,$4,2
addiu	$2,$2,8
lwc1	$f0,0($5)
addu	$4,$17,$4
sw	$2,12($17)
cvt.d.s	$f2,$f2
cvt.d.s	$f0,$f0
add.d	$f0,$f0,$f22
mul.d	$f0,$f0,$f2
add.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
.set	noreorder
.set	nomacro
bne	$6,$0,$L642
swc1	$f0,0($4)
.set	macro
.set	reorder

$L964:
lw	$6,72($17)
beq	$6,$0,$L615
$L218:
srl	$5,$2,3
andi	$7,$2,0x7
addu	$5,$3,$5
addiu	$4,$2,1
lbu	$5,0($5)
sw	$4,12($17)
sll	$5,$5,$7
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
bne	$5,$0,$L220
sw	$5,816($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$21,$L920
li	$3,1			# 0x1
.set	macro
.set	reorder

sb	$0,821($17)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$21,$2,$L611
sb	$3,870($17)
.set	macro
.set	reorder

li	$3,2			# 0x2
sb	$0,822($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L611
sb	$2,871($17)
.set	macro
.set	reorder

li	$3,3			# 0x3
sb	$0,823($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L611
sb	$2,872($17)
.set	macro
.set	reorder

li	$3,4			# 0x4
sb	$0,824($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L611
sb	$2,873($17)
.set	macro
.set	reorder

li	$3,5			# 0x5
sb	$0,825($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L611
sb	$2,874($17)
.set	macro
.set	reorder

sb	$0,826($17)
sb	$2,875($17)
$L920:
lw	$6,72($17)
beq	$6,$0,$L245
$L611:
lw	$8,172($sp)
lw	$2,24($8)
.set	noreorder
.set	nomacro
bne	$2,$0,$L965
li	$3,771			# 0x303
.set	macro
.set	reorder

lw	$9,0($8)
move	$3,$9
sw	$9,152($sp)
$L262:
.set	noreorder
.set	nomacro
beq	$3,$0,$L1056
lw	$9,156($sp)
.set	macro
.set	reorder

$L987:
.set	noreorder
.set	nomacro
blez	$21,$L264
addiu	$14,$17,288
.set	macro
.set	reorder

addiu	$25,$17,72
move	$5,$0
li	$24,1			# 0x1
li	$fp,1			# 0x1
li	$9,-4			# 0xfffffffffffffffc
li	$22,2			# 0x2
li	$10,4			# 0x4
li	$13,15			# 0xf
$L275:
lw	$2,-132($14)
beq	$2,$0,$L265
lw	$2,72($17)
beq	$2,$0,$L266
lw	$2,0($14)
beq	$2,$0,$L266
lw	$7,4($17)
lw	$3,12($17)
$L267:
srl	$2,$3,3
sw	$0,0($14)
addu	$2,$7,$2
and	$4,$2,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$3,0x7
addu	$2,$2,$4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010101101100010010100110	#S32EXTRV XR2,XR1,$2,$22
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addiu	$12,$3,2
lw	$15,256($17)
sll	$11,$2,1
sw	$12,12($17)
.set	noreorder
.set	nomacro
blez	$15,$L274
addu	$11,$11,$2
.set	macro
.set	reorder

sll	$15,$15,3
move	$6,$25
addiu	$8,$15,2
move	$5,$12
.option	pic0
.set	noreorder
.set	nomacro
j	$L273
.option	pic2
addu	$8,$3,$8
.set	macro
.set	reorder

$L271:
addiu	$2,$2,16
addu	$3,$11,$3
sll	$2,$2,21
addiu	$6,$6,4
sra	$3,$2,$3
addiu	$5,$5,8
.set	noreorder
.set	nomacro
beq	$5,$8,$L966
sw	$3,308($6)
.set	macro
.set	reorder

$L273:
srl	$2,$5,3
addu	$2,$7,$2
and	$3,$2,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110000011100000000000010010000	#S32LDDR XR2,$3,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000011100000000010001010000	#S32LDDR XR1,$3,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$3,$5,0x7
addu	$2,$2,$3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010010101100010010100110	#S32EXTRV XR2,XR1,$2,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000110000000010101110	#S32M2I XR2, $3
# 0 "" 2
#NO_APP
addiu	$4,$5,4
srl	$2,$4,3
addu	$2,$7,$2
and	$23,$2,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110010111100000000000010010000	#S32LDDR XR2,$23,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010111100000000010001010000	#S32LDDR XR1,$23,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$4,0x7
addu	$2,$2,$4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010010101100010010100110	#S32EXTRV XR2,XR1,$2,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
bne	$3,$13,$L271
sll	$2,$2,22
addu	$3,$11,$3
addiu	$6,$6,4
sra	$3,$2,$3
addiu	$5,$5,8
.set	noreorder
.set	nomacro
bne	$5,$8,$L273
sw	$3,308($6)
.set	macro
.set	reorder

$L966:
addu	$12,$12,$15
sw	$12,12($17)
$L274:
addiu	$24,$24,1
li	$5,1			# 0x1
slt	$2,$21,$24
addiu	$14,$14,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L275
addiu	$25,$25,72
.set	macro
.set	reorder

lw	$8,156($sp)
li	$2,2			# 0x2
beq	$8,$2,$L967
$L277:
li	$10,65536			# 0x10000
$L1072:
lw	$8,152($sp)
addu	$10,$17,$10
sltu	$3,$8,1
lw	$6,-20668($10)
slt	$24,$6,$3
.set	noreorder
.set	nomacro
bne	$24,$0,$L296
move	$22,$3
.set	macro
.set	reorder

lw	$2,72($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L294
li	$12,2			# 0x2
.set	macro
.set	reorder

sll	$2,$16,3
subu	$4,$2,$16
addu	$4,$4,$3
addiu	$4,$4,11712
sll	$4,$4,2
addu	$4,$17,$4
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1057
addiu	$5,$3,1
.set	macro
.set	reorder

li	$5,3			# 0x3
addu	$4,$19,$3
sb	$5,84($4)
addiu	$5,$3,1
$L1057:
slt	$4,$6,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L296
subu	$4,$2,$16
.set	macro
.set	reorder

addu	$4,$4,$5
addiu	$4,$4,11712
sll	$4,$4,2
addu	$4,$17,$4
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$0,$L297
li	$4,3			# 0x3
.set	macro
.set	reorder

addu	$5,$19,$5
sb	$4,84($5)
$L297:
addiu	$5,$3,2
slt	$4,$6,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L296
subu	$4,$2,$16
.set	macro
.set	reorder

addu	$4,$4,$5
addiu	$4,$4,11712
sll	$4,$4,2
addu	$4,$17,$4
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$0,$L298
li	$4,3			# 0x3
.set	macro
.set	reorder

addu	$5,$19,$5
sb	$4,84($5)
$L298:
addiu	$5,$3,3
slt	$4,$6,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L296
subu	$4,$2,$16
.set	macro
.set	reorder

addu	$4,$4,$5
addiu	$4,$4,11712
sll	$4,$4,2
addu	$4,$17,$4
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$0,$L299
li	$4,3			# 0x3
.set	macro
.set	reorder

addu	$5,$19,$5
sb	$4,84($5)
$L299:
addiu	$5,$3,4
slt	$4,$6,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L296
subu	$4,$2,$16
.set	macro
.set	reorder

addu	$4,$4,$5
addiu	$4,$4,11712
sll	$4,$4,2
addu	$4,$17,$4
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$0,$L300
li	$4,3			# 0x3
.set	macro
.set	reorder

addu	$5,$19,$5
sb	$4,84($5)
$L300:
addiu	$5,$3,5
slt	$4,$6,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L296
subu	$4,$2,$16
.set	macro
.set	reorder

addu	$4,$4,$5
addiu	$4,$4,11712
sll	$4,$4,2
addu	$4,$17,$4
lw	$4,0($4)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1058
addiu	$4,$3,6
.set	macro
.set	reorder

li	$4,3			# 0x3
addu	$5,$19,$5
sb	$4,84($5)
addiu	$4,$3,6
$L1058:
slt	$5,$6,$4
.set	noreorder
.set	nomacro
bne	$5,$0,$L296
subu	$2,$2,$16
.set	macro
.set	reorder

addu	$2,$2,$4
addiu	$2,$2,11712
sll	$2,$2,2
addu	$2,$17,$2
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L296
li	$2,3			# 0x3
.set	macro
.set	reorder

addu	$4,$19,$4
sb	$2,84($4)
$L296:
.set	noreorder
.set	nomacro
blez	$21,$L293
li	$4,44984			# 0xafb8
.set	macro
.set	reorder

li	$11,-65536			# 0xffffffffffff0000
li	$14,50528256			# 0x3030000
li	$12,65536			# 0x10000
addu	$4,$17,$4
li	$5,1			# 0x1
addiu	$11,$11,20708
li	$10,3			# 0x3
addiu	$14,$14,771
li	$15,771			# 0x303
li	$25,-4			# 0xfffffffffffffffc
li	$fp,6			# 0x6
.option	pic0
.set	noreorder
.set	nomacro
j	$L315
.option	pic2
addu	$12,$17,$12
.set	macro
.set	reorder

$L968:
lw	$8,-20584($12)
sw	$8,0($4)
$L311:
addiu	$2,$2,-1
sll	$2,$10,$2
addu	$7,$2,$8
addiu	$7,$7,-4
teq	$2,$0,7
div	$0,$7,$2
mflo	$2
.set	noreorder
.set	nomacro
blez	$16,$L309
sw	$2,48($4)
.set	macro
.set	reorder

beq	$9,$8,$L309
sw	$14,0($20)
sh	$15,4($20)
sb	$10,6($20)
$L309:
addiu	$5,$5,1
slt	$2,$21,$5
.set	noreorder
.set	nomacro
bne	$2,$0,$L293
addiu	$4,$4,4
.set	macro
.set	reorder

$L315:
addu	$2,$4,$18
sw	$0,-28($4)
lw	$2,1868($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L309
addu	$7,$4,$11
.set	macro
.set	reorder

lw	$7,0($7)
.set	noreorder
.set	nomacro
bne	$7,$0,$L968
lw	$9,0($4)
.set	macro
.set	reorder

addu	$7,$17,$5
lbu	$7,820($7)
beq	$7,$0,$L312
lw	$8,836($17)
.option	pic0
.set	noreorder
.set	nomacro
j	$L311
.option	pic2
sw	$8,0($4)
.set	macro
.set	reorder

$L265:
sw	$fp,0($14)
addiu	$24,$24,1
$L1060:
addiu	$14,$14,4
slt	$2,$21,$24
.set	noreorder
.set	nomacro
beq	$2,$0,$L275
addiu	$25,$25,72
.set	macro
.set	reorder

lw	$8,156($sp)
li	$2,2			# 0x2
bne	$8,$2,$L277
$L967:
beq	$5,$0,$L603
lw	$6,256($17)
blez	$6,$L603
lw	$2,180($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L279
addiu	$2,$6,46
.set	macro
.set	reorder

lw	$25,%call16(memset)($28)
addiu	$4,$17,184
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sll	$6,$6,2
.set	macro
.set	reorder

lw	$28,48($sp)
$L603:
lw	$2,72($17)
$L1034:
beq	$2,$0,$L282
$L988:
.set	noreorder
.set	nomacro
beq	$16,$0,$L1059
li	$2,65536			# 0x10000
.set	macro
.set	reorder

lw	$3,12($17)
lw	$4,4($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L277
sw	$3,12($17)
.set	macro
.set	reorder

$L286:
li	$2,65536			# 0x10000
$L1059:
lw	$10,152($sp)
li	$3,4			# 0x4
addu	$2,$17,$2
.set	noreorder
.set	nomacro
beq	$10,$0,$L285
sw	$3,-20528($2)
.set	macro
.set	reorder

lw	$3,-20584($2)
slt	$4,$3,62
.set	noreorder
.set	nomacro
beq	$4,$0,$L285
li	$4,3			# 0x3
.set	macro
.set	reorder

li	$5,2			# 0x2
xori	$3,$3,0x25
movz	$4,$5,$3
move	$3,$4
sw	$4,-20528($2)
$L613:
lw	$2,12($17)
li	$5,65536			# 0x10000
lw	$7,4($17)
addu	$5,$17,$5
srl	$9,$2,3
andi	$11,$2,0x7
addu	$9,$7,$9
addiu	$4,$2,1
addiu	$8,$2,2
lbu	$6,0($9)
srl	$9,$4,3
sw	$4,12($17)
andi	$10,$4,0x7
addu	$9,$7,$9
sll	$6,$6,$11
andi	$6,$6,0x00ff
srl	$6,$6,7
sw	$6,-20524($5)
lbu	$4,0($9)
sw	$8,12($17)
sll	$4,$4,$10
andi	$4,$4,0x00ff
srl	$4,$4,7
sw	$4,-20520($5)
li	$4,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$4,$L277
srl	$4,$8,3
.set	macro
.set	reorder

andi	$8,$8,0x7
addu	$4,$7,$4
addiu	$6,$2,3
lbu	$4,0($4)
sw	$6,12($17)
sll	$4,$4,$8
andi	$4,$4,0x00ff
srl	$4,$4,7
sw	$4,-20516($5)
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$4,$L277
srl	$3,$6,3
.set	macro
.set	reorder

addiu	$2,$2,4
addu	$7,$7,$3
andi	$6,$6,0x7
lbu	$3,0($7)
sw	$2,12($17)
sll	$2,$3,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.option	pic0
.set	noreorder
.set	nomacro
j	$L277
.option	pic2
sw	$2,-20512($5)
.set	macro
.set	reorder

$L266:
lw	$3,12($17)
lw	$7,4($17)
srl	$4,$3,3
andi	$6,$3,0x7
addu	$4,$7,$4
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L267
sw	$3,12($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L1060
addiu	$24,$24,1
.set	macro
.set	reorder

lui	$6,%hi($LC27)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC27)
.set	macro
.set	reorder

lw	$28,48($sp)
$L231:
lui	$6,%hi($LC39)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC39)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
li	$8,1			# 0x1
lw	$28,48($sp)
addu	$2,$17,$2
lw	$10,44($17)
sw	$8,180($sp)
lw	$22,-20596($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L607
.option	pic2
lw	$fp,-20668($2)
.set	macro
.set	reorder

$L312:
lw	$8,12($17)
lw	$7,4($17)
srl	$13,$8,3
addu	$7,$7,$13
and	$13,$7,$25
#APP
# 115 "ac3dec.c" 1
.word	0b01110001101100000000000010010000	#S32LDDR XR2,$13,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001101100000000010001010000	#S32LDDR XR1,$13,4
# 0 "" 2
#NO_APP
andi	$7,$7,0x3
sll	$7,$7,3
andi	$13,$8,0x7
addu	$7,$7,$13
#APP
# 117 "ac3dec.c" 1
.word	0b01110000111111101100010010100110	#S32EXTRV XR2 XR1 $7 $30
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001110000000010101110	#S32M2I XR2, $7
# 0 "" 2
#NO_APP
addiu	$8,$8,6
slt	$13,$7,61
.set	noreorder
.set	nomacro
beq	$13,$0,$L969
sw	$8,12($17)
.set	macro
.set	reorder

sll	$8,$7,1
addu	$7,$8,$7
addiu	$8,$7,73
.option	pic0
.set	noreorder
.set	nomacro
j	$L311
.option	pic2
sw	$8,0($4)
.set	macro
.set	reorder

$L293:
lw	$9,152($sp)
.set	noreorder
.set	nomacro
beq	$9,$0,$L307
lw	$8,192($sp)
.set	macro
.set	reorder

lw	$2,0($8)
.set	noreorder
.set	nomacro
beq	$2,$0,$L307
li	$4,65536			# 0x10000
.set	macro
.set	reorder

addiu	$7,$2,-1
addu	$4,$17,$4
li	$5,3			# 0x3
lw	$2,-20556($4)
sll	$5,$5,$7
lw	$7,-20584($4)
subu	$2,$2,$7
teq	$5,$0,7
div	$0,$2,$5
mflo	$2
sw	$2,-20508($4)
$L307:
.set	noreorder
.set	nomacro
bne	$24,$0,$L316
addiu	$11,$3,176
.set	macro
.set	reorder

sw	$16,164($sp)
addiu	$9,$3,11257
sw	$19,168($sp)
li	$12,65536			# 0x10000
sw	$21,176($sp)
sll	$11,$11,8
sw	$3,196($sp)
sll	$9,$9,2
addu	$fp,$17,$12
addu	$11,$17,$11
addu	$9,$17,$9
move	$2,$fp
move	$23,$3
move	$fp,$20
move	$16,$9
move	$19,$11
.option	pic0
.set	noreorder
.set	nomacro
j	$L321
.option	pic2
move	$20,$2
.set	macro
.set	reorder

$L317:
addiu	$23,$23,1
addiu	$19,$19,256
slt	$2,$6,$23
.set	noreorder
.set	nomacro
bne	$2,$0,$L970
addiu	$16,$16,4
.set	macro
.set	reorder

$L321:
addu	$2,$16,$18
lw	$5,1820($2)
.set	noreorder
.set	nomacro
beq	$5,$0,$L317
li	$3,-4			# 0xfffffffffffffffc
.set	macro
.set	reorder

lw	$4,12($17)
lw	$2,4($17)
srl	$6,$4,3
addu	$2,$2,$6
and	$6,$2,$3
#APP
# 115 "ac3dec.c" 1
.word	0b01110000110100000000000010010000	#S32LDDR XR2,$6,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000110100000000010001010000	#S32LDDR XR1,$6,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$6,$4,0x7
addu	$2,$2,$6
li	$6,4			# 0x4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001101100010010100110	#S32EXTRV XR2,XR1,$2,$6
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
sltu	$7,$23,1
sll	$7,$2,$7
addiu	$4,$4,4
sltu	$21,$0,$23
sll	$2,$23,8
sw	$4,12($17)
li	$8,45056			# 0xb000
sb	$7,0($19)
andi	$7,$7,0x00ff
lw	$12,-76($16)
lw	$6,0($16)
lw	$4,188($sp)
addu	$12,$21,$12
addu	$2,$2,$12
addu	$2,$2,$8
addu	$2,$17,$2
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_exponents
.option	pic2
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L971
lw	$28,48($sp)
.set	macro
.set	reorder

beq	$21,$0,$L921
lw	$2,-20664($20)
beq	$23,$2,$L921
lw	$2,12($17)
addiu	$2,$2,2
sw	$2,12($17)
$L921:
lw	$6,-20668($20)
addiu	$23,$23,1
addiu	$19,$19,256
slt	$2,$6,$23
.set	noreorder
.set	nomacro
beq	$2,$0,$L321
addiu	$16,$16,4
.set	macro
.set	reorder

$L970:
lw	$21,176($sp)
move	$20,$fp
lw	$16,164($sp)
lw	$3,196($sp)
lw	$19,168($sp)
$L316:
lw	$2,88($17)
beq	$2,$0,$L331
lw	$4,12($17)
lw	$2,4($17)
srl	$9,$4,3
andi	$10,$4,0x7
addu	$9,$2,$9
addiu	$7,$4,1
lbu	$5,0($9)
sll	$5,$5,$10
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
beq	$5,$0,$L323
sw	$7,12($17)
.set	macro
.set	reorder

srl	$5,$7,3
li	$9,-4			# 0xfffffffffffffffc
addu	$5,$2,$5
and	$10,$5,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110001010100000000000010010000	#S32LDDR XR2,$10,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001010100000000010001010000	#S32LDDR XR1,$10,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$7,$7,0x7
addu	$5,$5,$7
li	$10,2			# 0x2
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010101100010010100110	#S32EXTRV XR2,XR1,$5,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000011010000000010101110	#S32M2I XR2, $13
# 0 "" 2
#NO_APP
lw	$5,%got(ff_ac3_slow_decay_tab)($28)
li	$7,65536			# 0x10000
addiu	$11,$4,3
addu	$7,$17,$7
addu	$13,$5,$13
srl	$5,$11,3
lw	$12,-18516($7)
lbu	$13,0($13)
addu	$5,$2,$5
and	$14,$5,$9
sra	$13,$13,$12
sw	$13,-18508($7)
#APP
# 115 "ac3dec.c" 1
.word	0b01110001110100000000000010010000	#S32LDDR XR2,$14,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001110100000000010001010000	#S32LDDR XR1,$14,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010101100010010100110	#S32EXTRV XR2,XR1,$5,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000011100000000010101110	#S32M2I XR2, $14
# 0 "" 2
#NO_APP
lw	$13,%got(ff_ac3_fast_decay_tab)($28)
addiu	$11,$4,5
srl	$5,$11,3
addu	$13,$13,$14
addu	$5,$2,$5
lbu	$14,0($13)
and	$13,$5,$9
sra	$12,$14,$12
sw	$12,-18504($7)
#APP
# 115 "ac3dec.c" 1
.word	0b01110001101100000000000010010000	#S32LDDR XR2,$13,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001101100000000010001010000	#S32LDDR XR1,$13,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010101100010010100110	#S32EXTRV XR2,XR1,$5,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000011000000000010101110	#S32M2I XR2, $12
# 0 "" 2
#NO_APP
lw	$5,%got(ff_ac3_slow_gain_tab)($28)
sll	$12,$12,1
addiu	$11,$4,7
addu	$12,$5,$12
srl	$5,$11,3
lhu	$13,0($12)
addu	$5,$2,$5
and	$12,$5,$9
sw	$13,-18512($7)
#APP
# 115 "ac3dec.c" 1
.word	0b01110001100100000000000010010000	#S32LDDR XR2,$12,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001100100000000010001010000	#S32LDDR XR1,$12,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010101100010010100110	#S32EXTRV XR2,XR1,$5,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010100000000010101110	#S32M2I XR2, $10
# 0 "" 2
#NO_APP
sll	$11,$10,1
lw	$10,%got(ff_ac3_db_per_bit_tab)($28)
addiu	$5,$4,9
addu	$10,$10,$11
srl	$11,$5,3
lhu	$10,0($10)
addu	$2,$2,$11
and	$9,$2,$9
sw	$10,-18500($7)
#APP
# 115 "ac3dec.c" 1
.word	0b01110001001100000000000010010000	#S32LDDR XR2,$9,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001001100000000010001010000	#S32LDDR XR1,$9,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$5,$5,0x7
addu	$2,$2,$5
li	$5,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001011100010010100110	#S32EXTRV XR2,XR1,$2,$5
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
sll	$9,$5,1
lw	$5,%got(ff_ac3_floor_tab)($28)
addiu	$4,$4,12
slt	$2,$6,$3
addu	$5,$5,$9
sw	$4,12($17)
lh	$4,0($5)
.set	noreorder
.set	nomacro
bne	$2,$0,$L331
sw	$4,-18496($7)
.set	macro
.set	reorder

addu	$5,$19,$3
lbu	$2,84($5)
andi	$4,$2,0x00ff
sltu	$4,$4,2
.set	noreorder
.set	nomacro
beq	$4,$0,$L1061
addiu	$4,$3,1
.set	macro
.set	reorder

li	$2,2			# 0x2
$L1061:
sb	$2,84($5)
slt	$2,$6,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L331
addu	$4,$19,$4
.set	macro
.set	reorder

lbu	$5,84($4)
andi	$2,$5,0x00ff
sltu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L1062
addiu	$2,$3,2
.set	macro
.set	reorder

li	$5,2			# 0x2
$L1062:
sb	$5,84($4)
slt	$4,$6,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L331
addu	$2,$19,$2
.set	macro
.set	reorder

lbu	$5,84($2)
andi	$4,$5,0x00ff
sltu	$4,$4,2
.set	noreorder
.set	nomacro
beq	$4,$0,$L1063
addiu	$4,$3,3
.set	macro
.set	reorder

li	$5,2			# 0x2
$L1063:
sb	$5,84($2)
slt	$2,$6,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L331
addu	$4,$19,$4
.set	macro
.set	reorder

lbu	$5,84($4)
andi	$2,$5,0x00ff
sltu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L1064
addiu	$2,$3,4
.set	macro
.set	reorder

li	$5,2			# 0x2
$L1064:
sb	$5,84($4)
slt	$4,$6,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L331
addu	$2,$19,$2
.set	macro
.set	reorder

lbu	$5,84($2)
andi	$4,$5,0x00ff
sltu	$4,$4,2
.set	noreorder
.set	nomacro
beq	$4,$0,$L1065
addiu	$4,$3,5
.set	macro
.set	reorder

li	$5,2			# 0x2
$L1065:
sb	$5,84($2)
slt	$2,$6,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L331
addu	$4,$19,$4
.set	macro
.set	reorder

lbu	$5,84($4)
andi	$2,$5,0x00ff
sltu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L1066
addiu	$2,$3,6
.set	macro
.set	reorder

li	$5,2			# 0x2
$L1066:
sb	$5,84($4)
slt	$4,$6,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L331
addu	$2,$19,$2
.set	macro
.set	reorder

lbu	$4,84($2)
andi	$5,$4,0x00ff
sltu	$5,$5,2
beq	$5,$0,$L330
li	$4,2			# 0x2
$L330:
sb	$4,84($2)
$L331:
lw	$7,72($17)
beq	$7,$0,$L333
bne	$16,$0,$L334
$L333:
lw	$14,76($17)
beq	$14,$0,$L335
lw	$4,12($17)
lw	$12,4($17)
srl	$9,$4,3
andi	$10,$4,0x7
addu	$9,$12,$9
lbu	$2,0($9)
sll	$2,$2,$10
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L336
addiu	$5,$4,1
.set	macro
.set	reorder

sw	$5,12($17)
$L335:
bne	$7,$0,$L334
.set	noreorder
.set	nomacro
beq	$16,$0,$L972
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

$L334:
lw	$2,92($17)
$L1033:
beq	$2,$0,$L346
lw	$2,12($17)
lw	$10,4($17)
srl	$9,$2,3
andi	$11,$2,0x7
addu	$9,$10,$9
addiu	$4,$2,1
lbu	$5,0($9)
sll	$5,$5,$11
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
beq	$5,$0,$L346
sw	$4,12($17)
.set	macro
.set	reorder

slt	$5,$6,$3
bne	$5,$0,$L348
.set	noreorder
.set	nomacro
bne	$16,$0,$L349
sll	$5,$3,1
.set	macro
.set	reorder

srl	$5,$4,3
li	$12,-4			# 0xfffffffffffffffc
addu	$5,$10,$5
and	$7,$5,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110000111100000000000010010000	#S32LDDR XR2,$7,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000111100000000010001010000	#S32LDDR XR1,$7,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$4,$4,0x7
addu	$4,$5,$4
li	$9,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000100010011100010010100110	#S32EXTRV XR2,XR1,$4,$9
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001000000000010101110	#S32M2I XR2, $4
# 0 "" 2
#NO_APP
lw	$7,%got(ff_ac3_fast_gain_tab)($28)
sll	$4,$4,1
addiu	$11,$3,11770
addiu	$5,$3,1
addu	$4,$7,$4
sll	$11,$11,2
slt	$5,$6,$5
lhu	$13,0($4)
addu	$11,$17,$11
addiu	$4,$2,4
.set	noreorder
.set	nomacro
bne	$5,$0,$L350
sw	$13,4($11)
.set	macro
.set	reorder

srl	$5,$4,3
addu	$5,$10,$5
and	$11,$5,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110001011100000000000010010000	#S32LDDR XR2,$11,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001011100000000010001010000	#S32LDDR XR1,$11,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$4,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010011100010010100110	#S32EXTRV XR2,XR1,$5,$9
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010110000000010101110	#S32M2I XR2, $11
# 0 "" 2
#NO_APP
sll	$11,$11,1
addiu	$5,$3,11771
addu	$11,$7,$11
sll	$5,$5,2
addiu	$13,$3,2
lhu	$14,0($11)
addu	$5,$17,$5
slt	$13,$6,$13
addiu	$11,$2,7
.set	noreorder
.set	nomacro
bne	$13,$0,$L350
sw	$14,4($5)
.set	macro
.set	reorder

srl	$5,$11,3
addu	$5,$10,$5
and	$13,$5,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110001101100000000000010010000	#S32LDDR XR2,$13,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001101100000000010001010000	#S32LDDR XR1,$13,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010011100010010100110	#S32EXTRV XR2,XR1,$5,$9
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010110000000010101110	#S32M2I XR2, $11
# 0 "" 2
#NO_APP
sll	$11,$11,1
addiu	$5,$3,11772
addu	$11,$7,$11
sll	$5,$5,2
addiu	$13,$3,3
lhu	$14,0($11)
addu	$5,$17,$5
slt	$13,$6,$13
addiu	$11,$2,10
.set	noreorder
.set	nomacro
bne	$13,$0,$L350
sw	$14,4($5)
.set	macro
.set	reorder

srl	$5,$11,3
addu	$5,$10,$5
and	$13,$5,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110001101100000000000010010000	#S32LDDR XR2,$13,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001101100000000010001010000	#S32LDDR XR1,$13,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010011100010010100110	#S32EXTRV XR2,XR1,$5,$9
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010110000000010101110	#S32M2I XR2, $11
# 0 "" 2
#NO_APP
sll	$11,$11,1
addiu	$5,$3,11773
addu	$11,$7,$11
sll	$5,$5,2
addiu	$13,$3,4
lhu	$14,0($11)
addu	$5,$17,$5
slt	$13,$6,$13
addiu	$11,$2,13
.set	noreorder
.set	nomacro
bne	$13,$0,$L350
sw	$14,4($5)
.set	macro
.set	reorder

srl	$5,$11,3
addu	$5,$10,$5
and	$13,$5,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110001101100000000000010010000	#S32LDDR XR2,$13,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001101100000000010001010000	#S32LDDR XR1,$13,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010011100010010100110	#S32EXTRV XR2,XR1,$5,$9
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010110000000010101110	#S32M2I XR2, $11
# 0 "" 2
#NO_APP
sll	$11,$11,1
addiu	$5,$3,11774
addu	$11,$7,$11
sll	$5,$5,2
addiu	$13,$3,5
lhu	$14,0($11)
addu	$5,$17,$5
slt	$13,$6,$13
addiu	$11,$2,16
.set	noreorder
.set	nomacro
bne	$13,$0,$L350
sw	$14,4($5)
.set	macro
.set	reorder

srl	$5,$11,3
addu	$5,$10,$5
and	$12,$5,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110001100100000000000010010000	#S32LDDR XR2,$12,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001100100000000010001010000	#S32LDDR XR1,$12,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$11,$11,0x7
addu	$5,$5,$11
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101010011100010010100110	#S32EXTRV XR2,XR1,$5,$9
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010110000000010101110	#S32M2I XR2, $11
# 0 "" 2
#NO_APP
sll	$11,$11,1
addiu	$5,$3,11775
addu	$11,$7,$11
sll	$5,$5,2
addiu	$9,$3,6
lhu	$12,0($11)
addu	$5,$17,$5
slt	$11,$6,$9
addiu	$2,$2,19
.set	noreorder
.set	nomacro
bne	$11,$0,$L350
sw	$12,4($5)
.set	macro
.set	reorder

srl	$5,$2,3
addu	$10,$10,$5
li	$5,-4			# 0xfffffffffffffffc
and	$5,$10,$5
#APP
# 115 "ac3dec.c" 1
.word	0b01110000101100000000000010010000	#S32LDDR XR2,$5,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000101100000000010001010000	#S32LDDR XR1,$5,4
# 0 "" 2
#NO_APP
andi	$10,$10,0x3
sll	$5,$10,3
andi	$2,$2,0x7
addu	$2,$5,$2
li	$5,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001011100010010100110	#S32EXTRV XR2,XR1,$2,$5
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
sll	$5,$5,1
addiu	$2,$9,11770
addu	$7,$7,$5
sll	$2,$2,2
lhu	$5,0($7)
addu	$2,$17,$2
sw	$5,4($2)
$L350:
subu	$6,$6,$3
sll	$2,$6,1
addu	$2,$2,$6
addu	$2,$4,$2
$L351:
sw	$2,12($17)
$L348:
lw	$2,24($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1067
lw	$8,152($sp)
.set	macro
.set	reorder

lw	$4,12($17)
lw	$5,4($17)
srl	$2,$4,3
andi	$6,$4,0x7
addu	$5,$5,$2
addiu	$7,$4,1
lbu	$2,0($5)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L357
sw	$7,12($17)
.set	macro
.set	reorder

addiu	$4,$4,11
sw	$4,12($17)
$L357:
lw	$8,152($sp)
$L1067:
.set	noreorder
.set	nomacro
beq	$8,$0,$L359
li	$2,65536			# 0x10000
.set	macro
.set	reorder

addu	$2,$17,$2
lw	$2,-18484($2)
bne	$2,$0,$L973
lw	$2,12($17)
lw	$6,4($17)
srl	$7,$2,3
andi	$9,$2,0x7
addu	$7,$6,$7
addiu	$5,$2,1
lbu	$4,0($7)
sll	$4,$4,$9
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L361
sw	$5,12($17)
.set	macro
.set	reorder

lw	$2,72($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1068
li	$2,65536			# 0x10000
.set	macro
.set	reorder

beq	$16,$0,$L974
$L1068:
addu	$2,$17,$2
sw	$0,-18484($2)
$L359:
lw	$2,96($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1069
li	$2,65536			# 0x10000
.set	macro
.set	reorder

lw	$4,12($17)
lw	$12,4($17)
srl	$6,$4,3
andi	$7,$4,0x7
addu	$6,$12,$6
addiu	$5,$4,1
lbu	$2,0($6)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L367
sw	$5,12($17)
.set	macro
.set	reorder

slt	$2,$21,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L922
srl	$2,$5,3
.set	macro
.set	reorder

li	$6,-4			# 0xfffffffffffffffc
addu	$2,$12,$2
and	$6,$2,$6
#APP
# 115 "ac3dec.c" 1
.word	0b01110000110100000000000010010000	#S32LDDR XR2,$6,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000110100000000010001010000	#S32LDDR XR1,$6,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$5,$5,0x7
addu	$2,$2,$5
li	$5,2			# 0x2
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001011100010010100110	#S32EXTRV XR2,XR1,$2,$5
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
addiu	$2,$3,13472
addiu	$4,$4,3
sll	$2,$2,2
addu	$2,$17,$2
sw	$4,12($17)
sw	$5,0($2)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$5,$2,$L371
addiu	$5,$3,13473
.set	macro
.set	reorder

addu	$13,$20,$3
sll	$5,$5,2
li	$11,-4			# 0xfffffffffffffffc
addu	$5,$17,$5
li	$10,2			# 0x2
li	$9,3			# 0x3
move	$6,$13
.option	pic0
.set	noreorder
.set	nomacro
j	$L372
.option	pic2
move	$7,$3
.set	macro
.set	reorder

$L374:
addu	$2,$12,$2
and	$8,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110001000100000000000010010000	#S32LDDR XR2,$8,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001000100000000010001010000	#S32LDDR XR1,$8,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$8,$4,0x7
addu	$2,$2,$8
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010010101100010010100110	#S32EXTRV XR2,XR1,$2,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addiu	$4,$4,2
addiu	$5,$5,4
addiu	$6,$6,1
sw	$4,12($17)
.set	noreorder
.set	nomacro
beq	$2,$9,$L371
sw	$2,-4($5)
.set	macro
.set	reorder

$L372:
lbu	$2,0($6)
andi	$8,$2,0x00ff
sltu	$8,$8,2
beq	$8,$0,$L373
li	$2,2			# 0x2
$L373:
addiu	$7,$7,1
sb	$2,0($6)
slt	$2,$21,$7
.set	noreorder
.set	nomacro
beq	$2,$0,$L374
srl	$2,$4,3
.set	macro
.set	reorder

addiu	$9,$3,13479
addiu	$11,$3,6743
sll	$9,$9,2
sll	$11,$11,3
addiu	$21,$21,1
addu	$9,$17,$9
addu	$11,$17,$11
addu	$24,$20,$21
li	$25,1			# 0x1
li	$15,-4			# 0xfffffffffffffffc
li	$8,3			# 0x3
li	$fp,5			# 0x5
$L380:
lw	$2,-28($9)
beq	$2,$25,$L975
$L375:
addiu	$13,$13,1
addiu	$9,$9,4
.set	noreorder
.set	nomacro
bne	$13,$24,$L380
addiu	$11,$11,8
.set	macro
.set	reorder

$L922:
li	$2,65536			# 0x10000
addu	$2,$17,$2
lw	$6,-20668($2)
$L369:
slt	$2,$6,$3
bne	$2,$0,$L384
$L630:
sll	$4,$3,4
sw	$16,164($sp)
sll	$2,$3,2
sw	$18,168($sp)
addiu	$23,$3,11238
sw	$19,176($sp)
addu	$2,$2,$4
sll	$5,$3,9
sll	$fp,$2,2
sll	$4,$3,8
li	$10,48904			# 0xbf08
addiu	$3,$3,6743
li	$9,47112			# 0xb808
addu	$2,$2,$fp
addu	$10,$5,$10
addu	$9,$4,$9
li	$fp,53188			# 0xcfc4
sll	$3,$3,3
sll	$23,$23,2
addu	$fp,$2,$fp
addu	$10,$17,$10
addu	$9,$17,$9
addu	$3,$17,$3
li	$11,65536			# 0x10000
addu	$23,$17,$23
addu	$fp,$17,$fp
addu	$21,$17,$11
move	$16,$3
move	$18,$9
.option	pic0
.set	noreorder
.set	nomacro
j	$L391
.option	pic2
move	$19,$10
.set	macro
.set	reorder

$L385:
beq	$2,$4,$L976
.set	noreorder
.set	nomacro
bne	$2,$0,$L1070
li	$5,-65536			# 0xffffffffffff0000
.set	macro
.set	reorder

addiu	$22,$22,1
addiu	$23,$23,4
slt	$2,$6,$22
addiu	$19,$19,512
addiu	$18,$18,256
addiu	$fp,$fp,100
.set	noreorder
.set	nomacro
bne	$2,$0,$L977
addiu	$16,$16,8
.set	macro
.set	reorder

$L391:
addu	$2,$20,$22
lbu	$2,0($2)
sltu	$4,$2,3
.set	noreorder
.set	nomacro
bne	$4,$0,$L385
li	$4,2			# 0x2
.set	macro
.set	reorder

addiu	$2,$fp,-700
lw	$5,0($23)
lw	$6,28($23)
addiu	$4,$18,-2056
lw	$25,%call16(ff_ac3_bit_alloc_calc_psd)($28)
move	$7,$19
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_ac3_bit_alloc_calc_psd
1:	jalr	$25
sw	$2,204($sp)
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$2,204($sp)
$L386:
lw	$5,2132($23)
addiu	$11,$16,56
lw	$6,0($23)
addiu	$8,$16,112
lw	$7,28($23)
li	$4,47016			# 0xb7a8
lw	$25,%call16(ff_ac3_bit_alloc_calc_mask)($28)
sw	$5,16($sp)
move	$5,$2
lw	$2,-20664($21)
addu	$4,$17,$4
xor	$2,$22,$2
sltu	$2,$2,1
sw	$2,20($sp)
lw	$2,8936($23)
sw	$2,24($sp)
lw	$12,8964($23)
sw	$16,32($sp)
sw	$11,36($sp)
sw	$8,40($sp)
sw	$12,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_ac3_bit_alloc_calc_mask
1:	jalr	$25
sw	$fp,44($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L978
lw	$28,48($sp)
.set	macro
.set	reorder

li	$5,-65536			# 0xffffffffffff0000
$L1070:
lw	$8,2104($23)
lw	$7,%got(ff_ac3_bap_tab)($28)
move	$4,$fp
ori	$5,$5,0x578c
lw	$9,%got(ff_eac3_hebap_tab)($28)
addu	$2,$23,$5
lw	$6,0($23)
lw	$25,%call16(ff_ac3_bit_alloc_calc_bap)($28)
move	$5,$19
addiu	$22,$22,1
lw	$2,0($2)
addiu	$23,$23,4
addiu	$19,$19,512
addiu	$fp,$fp,100
movz	$9,$7,$2
addiu	$16,$16,8
lw	$7,24($23)
sw	$8,16($sp)
lw	$8,-18496($21)
sw	$18,28($sp)
addiu	$18,$18,256
sw	$9,24($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_ac3_bit_alloc_calc_bap
1:	jalr	$25
sw	$8,20($sp)
.set	macro
.set	reorder

lw	$6,-20668($21)
slt	$2,$6,$22
.set	noreorder
.set	nomacro
beq	$2,$0,$L391
lw	$28,48($sp)
.set	macro
.set	reorder

$L977:
lw	$16,164($sp)
lw	$18,168($sp)
lw	$19,176($sp)
$L384:
lw	$2,100($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1071
li	$2,6			# 0x6
.set	macro
.set	reorder

lw	$5,12($17)
lw	$2,4($17)
srl	$4,$5,3
andi	$8,$5,0x7
addu	$4,$2,$4
addiu	$7,$5,1
lbu	$3,0($4)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L392
sw	$7,12($17)
.set	macro
.set	reorder

srl	$3,$7,3
li	$4,-4			# 0xfffffffffffffffc
addu	$2,$2,$3
and	$4,$2,$4
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$3,$7,0x7
addu	$2,$2,$3
li	$3,9			# 0x9
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010000111100010010100110	#S32EXTRV XR2,XR1,$2,$3
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000110000000010101110	#S32M2I XR2, $3
# 0 "" 2
#NO_APP
addiu	$2,$5,10
addiu	$4,$3,-1
.set	noreorder
.set	nomacro
beq	$3,$0,$L392
sw	$2,12($17)
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
$L393:
addiu	$2,$2,8
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$4,$3,$L393
sw	$2,12($17)
.set	macro
.set	reorder

$L392:
li	$2,6			# 0x6
$L1071:
sw	$0,84($sp)
sw	$0,80($sp)
.set	noreorder
.set	nomacro
beq	$6,$2,$L979
sw	$0,76($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$6,$L395
sll	$2,$16,2
.set	macro
.set	reorder

sw	$18,196($sp)
li	$4,44984			# 0xafb8
sw	$19,168($sp)
addiu	$2,$2,1856
li	$8,6144			# 0x1800
li	$9,256			# 0x100
li	$10,1			# 0x1
sw	$2,176($sp)
li	$2,65536			# 0x10000
sw	$8,164($sp)
addu	$7,$17,$4
sw	$9,156($sp)
move	$20,$0
sw	$10,152($sp)
addu	$fp,$17,$2
move	$18,$20
move	$19,$7
$L476:
li	$3,-65536			# 0xffffffffffff0000
ori	$3,$3,0x5770
addu	$2,$19,$3
lw	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L980
lw	$5,152($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L981
lw	$25,%call16(ff_eac3_decode_transform_coeffs_aht_ch)($28)
.set	macro
.set	reorder

$L453:
lw	$2,-28($19)
lw	$7,0($19)
slt	$3,$2,$7
.set	noreorder
.set	nomacro
beq	$3,$0,$L452
lw	$9,156($sp)
.set	macro
.set	reorder

li	$3,45056			# 0xb000
lw	$10,164($sp)
sll	$8,$2,3
sll	$6,$2,5
addu	$4,$9,$3
addiu	$5,$9,14672
lw	$9,176($sp)
subu	$6,$6,$8
addu	$5,$5,$2
addu	$3,$10,$9
addu	$7,$4,$7
addu	$6,$3,$6
addu	$2,$4,$2
sll	$5,$5,2
addu	$6,$17,$6
addu	$4,$17,$2
addu	$5,$17,$5
addu	$7,$17,$7
$L455:
lb	$3,0($4)
addiu	$5,$5,4
lw	$2,0($6)
addiu	$4,$4,1
addiu	$6,$6,24
sra	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$4,$7,$L455
sw	$2,-4($5)
.set	macro
.set	reorder

$L452:
li	$10,-65536			# 0xffffffffffff0000
ori	$10,$10,0x50e4
addu	$2,$19,$10
lw	$2,0($2)
beq	$2,$0,$L457
beq	$18,$0,$L458
lw	$20,-20556($fp)
$L459:
li	$18,1			# 0x1
$L475:
lw	$7,156($sp)
subu	$3,$0,$20
lw	$8,152($sp)
li	$4,58688			# 0xe540
sll	$3,$3,2
lw	$25,%call16(memset)($28)
addu	$2,$20,$7
addiu	$3,$3,1024
sll	$2,$2,2
li	$6,4			# 0x4
slt	$20,$20,256
movn	$6,$3,$20
addu	$4,$2,$4
addiu	$8,$8,1
move	$5,$0
addu	$4,$17,$4
sw	$8,152($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addiu	$19,$19,4
.set	macro
.set	reorder

lw	$9,156($sp)
lw	$10,164($sp)
lw	$6,-20668($fp)
lw	$3,152($sp)
addiu	$9,$9,256
addiu	$10,$10,6144
lw	$28,48($sp)
slt	$2,$6,$3
sw	$9,156($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L476
sw	$10,164($sp)
.set	macro
.set	reorder

lw	$18,196($sp)
lw	$19,168($sp)
$L395:
li	$5,65536			# 0x10000
addu	$5,$17,$5
lw	$2,-20672($5)
.set	noreorder
.set	nomacro
blez	$2,$L479
addiu	$2,$2,1
.set	macro
.set	reorder

addiu	$9,$17,156
sll	$7,$2,8
li	$8,256			# 0x100
li	$11,53960			# 0xd2c8
li	$10,47112			# 0xb808
$L478:
addu	$2,$9,$11
lw	$2,0($2)
bne	$2,$0,$L480
lw	$2,0($9)
beq	$2,$0,$L480
lw	$3,-20584($5)
lw	$4,-20556($5)
slt	$2,$3,$4
.set	noreorder
.set	nomacro
beq	$2,$0,$L480
addiu	$2,$3,14672
.set	macro
.set	reorder

addiu	$12,$4,14672
addu	$2,$8,$2
addu	$12,$12,$8
addu	$3,$3,$10
sll	$2,$2,2
sll	$12,$12,2
addu	$3,$17,$3
addu	$2,$17,$2
addu	$12,$17,$12
$L482:
lbu	$4,0($3)
bne	$4,$0,$L481
sw	$0,0($2)
$L481:
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$12,$L482
addiu	$3,$3,1
.set	macro
.set	reorder

$L480:
addiu	$8,$8,256
.set	noreorder
.set	nomacro
bne	$8,$7,$L478
addiu	$9,$9,4
.set	macro
.set	reorder

$L479:
lw	$5,48($17)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$5,$2,$L982
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L450:
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
beq	$6,$2,$L983
li	$2,65536			# 0x10000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$6,$L496
lui	$2,%hi($LC38)
.set	macro
.set	reorder

mtc1	$0,$f4
lui	$3,%hi($LC36)
ldc1	$f2,%lo($LC38)($2)
lui	$2,%hi($LC37)
li	$7,44948			# 0xaf94
lwc1	$f0,%lo($LC37)($2)
li	$9,65536			# 0x10000
li	$2,59712			# 0xe940
lwc1	$f1,%lo($LC36)($3)
addu	$7,$17,$7
addu	$2,$17,$2
li	$8,1			# 0x1
addu	$9,$17,$9
mov.d	$f10,$f2
mov.s	$f9,$f0
mov.s	$f12,$f1
mov.s	$f8,$f0
mov.s	$f5,$f0
.set	noreorder
.set	nomacro
bne	$5,$0,$L497
mov.d	$f6,$f2
.set	macro
.set	reorder

$L984:
lwc1	$f14,0($7)
mul.s	$f13,$f14,$f12
c.le.s	$fcc3,$f4,$f13
.set	noreorder
.set	nomacro
bc1f	$fcc3,$L907
mul.s	$f13,$f14,$f5
.set	macro
.set	reorder

cvt.d.s	$f14,$f13
add.d	$f14,$f14,$f6
trunc.w.d $f13,$f14
mfc1	$4,$f13
$L500:
addiu	$3,$2,7168
addiu	$6,$2,1024
$L503:
lw	$5,0($2)
addiu	$3,$3,16
addiu	$2,$2,16
mul	$5,$4,$5
sw	$5,-16($3)
lw	$5,-12($2)
mul	$5,$4,$5
sw	$5,-12($3)
lw	$5,-8($2)
mul	$5,$4,$5
sw	$5,-8($3)
lw	$5,-4($2)
mul	$5,$4,$5
.set	noreorder
.set	nomacro
bne	$2,$6,$L503
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$3,-20668($9)
addiu	$8,$8,1
slt	$3,$3,$8
.set	noreorder
.set	nomacro
bne	$3,$0,$L496
addiu	$7,$7,-4
.set	macro
.set	reorder

lw	$5,48($17)
beq	$5,$0,$L984
$L497:
lwc1	$f14,-20592($9)
mul.s	$f13,$f14,$f1
c.le.s	$fcc4,$f4,$f13
.set	noreorder
.set	nomacro
bc1f	$fcc4,$L908
mul.s	$f13,$f14,$f9
.set	macro
.set	reorder

cvt.d.s	$f14,$f13
add.d	$f14,$f14,$f2
trunc.w.d $f13,$f14
.option	pic0
.set	noreorder
.set	nomacro
j	$L500
.option	pic2
mfc1	$4,$f13
.set	macro
.set	reorder

$L346:
beq	$7,$0,$L348
.set	noreorder
.set	nomacro
bne	$16,$0,$L348
slt	$2,$6,$3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L348
sll	$2,$3,2
.set	macro
.set	reorder

lw	$7,%got(ff_ac3_fast_gain_tab)($28)
slt	$5,$3,$6
addu	$2,$17,$2
lhu	$4,8($7)
li	$7,65536			# 0x10000
addu	$2,$2,$7
.set	noreorder
.set	nomacro
beq	$5,$0,$L348
sw	$4,-18452($2)
.set	macro
.set	reorder

addiu	$5,$3,2
slt	$5,$6,$5
.set	noreorder
.set	nomacro
bne	$5,$0,$L348
sw	$4,-18448($2)
.set	macro
.set	reorder

addiu	$5,$3,3
slt	$5,$6,$5
.set	noreorder
.set	nomacro
bne	$5,$0,$L348
sw	$4,-18444($2)
.set	macro
.set	reorder

addiu	$5,$3,4
slt	$5,$6,$5
.set	noreorder
.set	nomacro
bne	$5,$0,$L348
sw	$4,-18440($2)
.set	macro
.set	reorder

addiu	$5,$3,5
slt	$5,$6,$5
.set	noreorder
.set	nomacro
bne	$5,$0,$L348
sw	$4,-18436($2)
.set	macro
.set	reorder

addiu	$5,$3,6
slt	$6,$6,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L348
sw	$4,-18432($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L348
.option	pic2
sw	$4,-18428($2)
.set	macro
.set	reorder

$L976:
.option	pic0
.set	noreorder
.set	nomacro
j	$L386
.option	pic2
addiu	$2,$fp,-700
.set	macro
.set	reorder

$L367:
li	$2,65536			# 0x10000
$L1069:
addu	$2,$17,$2
.set	noreorder
.set	nomacro
bne	$16,$0,$L369
lw	$6,-20668($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$6,$L384
li	$4,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$0,$L369
sw	$4,-11648($2)
.set	macro
.set	reorder

li	$5,1			# 0x1
.set	noreorder
.set	nomacro
beq	$6,$5,$L630
sw	$4,-11644($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$4,$L630
sw	$4,-11640($2)
.set	macro
.set	reorder

li	$5,3			# 0x3
.set	noreorder
.set	nomacro
beq	$6,$5,$L630
sw	$4,-11636($2)
.set	macro
.set	reorder

li	$5,4			# 0x4
.set	noreorder
.set	nomacro
beq	$6,$5,$L630
sw	$4,-11632($2)
.set	macro
.set	reorder

li	$5,5			# 0x5
.set	noreorder
.set	nomacro
beq	$6,$5,$L630
sw	$4,-11628($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L630
.option	pic2
sw	$4,-11624($2)
.set	macro
.set	reorder

$L245:
lw	$2,12($17)
lw	$3,4($17)
srl	$4,$2,3
$L1073:
andi	$6,$2,0x7
addu	$4,$3,$4
addiu	$5,$2,1
lbu	$4,0($4)
sll	$4,$4,$6
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L985
sw	$5,12($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L986
lw	$9,172($sp)
.set	macro
.set	reorder

lw	$10,172($sp)
lw	$9,-4($9)
move	$3,$9
sw	$9,152($sp)
.set	noreorder
.set	nomacro
bne	$3,$0,$L987
sw	$9,0($10)
.set	macro
.set	reorder

lw	$9,156($sp)
$L1056:
li	$2,2			# 0x2
bne	$9,$2,$L277
lw	$2,72($17)
bne	$2,$0,$L988
$L282:
lw	$3,12($17)
lw	$4,4($17)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L286
sw	$3,12($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L1072
li	$10,65536			# 0x10000
.set	macro
.set	reorder

lui	$6,%hi($LC28)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,24			# 0x18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC28)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
lw	$28,48($sp)
addu	$2,$17,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L277
.option	pic2
sw	$0,-20528($2)
.set	macro
.set	reorder

$L615:
lw	$4,816($17)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1073
srl	$4,$2,3
.set	macro
.set	reorder

$L233:
.set	noreorder
.set	nomacro
blez	$21,$L920
lui	$2,%hi($LC0)
.set	macro
.set	reorder

mtc1	$0,$f4
sw	$16,176($sp)
addiu	$25,$17,870
lwc1	$f9,%lo($LC0)($2)
lui	$2,%hi($LC20)
li	$fp,1			# 0x1
lwc1	$f10,%lo($LC20)($2)
addiu	$2,$17,68
li	$12,-4			# 0xfffffffffffffffc
li	$15,2			# 0x2
sw	$2,152($sp)
lui	$2,%hi($LC21)
li	$24,4			# 0x4
lwc1	$f8,%lo($LC21)($2)
lui	$2,%hi($LC22)
mov.s	$f3,$f9
li	$23,15			# 0xf
lwc1	$f7,%lo($LC22)($2)
li	$22,25			# 0x19
$L244:
lbu	$2,-49($25)
.set	noreorder
.set	nomacro
beq	$2,$0,$L235
lw	$5,152($sp)
.set	macro
.set	reorder

lbu	$2,0($25)
beq	$2,$0,$L236
lw	$11,4($17)
lw	$5,12($17)
srl	$2,$5,3
$L1030:
sb	$0,0($25)
addu	$2,$11,$2
and	$3,$2,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110000011100000000000010010000	#S32LDDR XR2,$3,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000011100000000010001010000	#S32LDDR XR1,$3,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$3,$5,0x7
addu	$2,$2,$3
li	$3,5			# 0x5
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010000111100010010100110	#S32EXTRV XR2,XR1,$2,$3
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
mtc1	$2,$f2
.set	noreorder
.set	nomacro
bltz	$2,$L989
cvt.d.w	$f0,$f2
.set	macro
.set	reorder

$L239:
cvt.s.d	$f0,$f0
addiu	$3,$5,5
srl	$2,$3,3
addu	$2,$11,$2
and	$4,$2,$12
mul.s	$f5,$f0,$f10
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$3,$3,0x7
addu	$2,$2,$3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010011111100010010100110	#S32EXTRV XR2,XR1,$2,$15
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addiu	$3,$5,7
lw	$16,848($17)
sll	$14,$2,1
lw	$8,836($17)
sw	$3,164($sp)
addu	$14,$14,$2
.set	noreorder
.set	nomacro
blez	$16,$L238
sw	$3,12($17)
.set	macro
.set	reorder

lwc1	$f0,840($17)
addiu	$13,$16,852
addiu	$7,$17,852
lw	$6,152($sp)
addiu	$5,$5,11
addu	$13,$17,$13
cvt.s.w	$f6,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L243
.option	pic2
div.s	$f6,$f9,$f6
.set	macro
.set	reorder

$L241:
subu	$2,$22,$3
addiu	$6,$6,4
subu	$2,$2,$14
addiu	$7,$7,1
sll	$2,$4,$2
addiu	$5,$5,6
mtc1	$2,$f11
cvt.s.w	$f1,$f11
mul.s	$f1,$f1,$f7
mul.s	$f2,$f1,$f2
mul.s	$f1,$f1,$f0
swc1	$f2,872($6)
.set	noreorder
.set	nomacro
beq	$7,$13,$L990
swc1	$f1,1348($6)
.set	macro
.set	reorder

$L243:
lbu	$9,0($7)
addiu	$3,$5,-4
srl	$2,$3,3
sra	$4,$9,1
addu	$2,$11,$2
addu	$4,$4,$8
and	$10,$2,$12
mtc1	$4,$f1
addu	$8,$8,$9
cvt.s.w	$f0,$f1
mul.s	$f0,$f0,$f6
sub.s	$f0,$f0,$f5
c.le.s	$fcc0,$f0,$f3
movf.s	$f0,$f3,$fcc0
c.le.s	$fcc1,$f4,$f0
movf.s	$f0,$f4,$fcc1
mul.s	$f2,$f0,$f8
sub.s	$f0,$f3,$f0
sqrt.s	$f2,$f2
sqrt.s	$f0,$f0
#APP
# 115 "ac3dec.c" 1
.word	0b01110001010100000000000010010000	#S32LDDR XR2,$10,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001010100000000010001010000	#S32LDDR XR1,$10,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$3,$3,0x7
addu	$2,$2,$3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010110001100010010100110	#S32EXTRV XR2,XR1,$2,$24
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000110000000010101110	#S32M2I XR2, $3
# 0 "" 2
#NO_APP
srl	$2,$5,3
addu	$2,$11,$2
and	$4,$2,$12
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$5,0x7
addu	$2,$2,$4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010011111100010010100110	#S32EXTRV XR2,XR1,$2,$15
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$3,$23,$L241
addiu	$4,$2,4
.set	macro
.set	reorder

sll	$4,$2,1
subu	$2,$22,$3
addiu	$6,$6,4
subu	$2,$2,$14
addiu	$7,$7,1
sll	$2,$4,$2
addiu	$5,$5,6
mtc1	$2,$f11
cvt.s.w	$f1,$f11
mul.s	$f1,$f1,$f7
mul.s	$f2,$f1,$f2
mul.s	$f1,$f1,$f0
swc1	$f2,872($6)
.set	noreorder
.set	nomacro
bne	$7,$13,$L243
swc1	$f1,1348($6)
.set	macro
.set	reorder

$L990:
sll	$2,$16,1
lw	$3,164($sp)
sll	$16,$16,3
subu	$2,$16,$2
addu	$2,$3,$2
sw	$2,12($17)
$L238:
lw	$5,152($sp)
addiu	$fp,$fp,1
addiu	$25,$25,1
slt	$2,$21,$fp
addiu	$5,$5,68
.set	noreorder
.set	nomacro
beq	$2,$0,$L244
sw	$5,152($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L920
.option	pic2
lw	$16,176($sp)
.set	macro
.set	reorder

$L235:
li	$4,1			# 0x1
addiu	$fp,$fp,1
addiu	$25,$25,1
addiu	$5,$5,68
sb	$4,-1($25)
slt	$2,$21,$fp
.set	noreorder
.set	nomacro
beq	$2,$0,$L244
sw	$5,152($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L920
.option	pic2
lw	$16,176($sp)
.set	macro
.set	reorder

$L236:
lw	$5,12($17)
lw	$11,4($17)
srl	$3,$5,3
andi	$4,$5,0x7
addu	$3,$11,$3
addiu	$5,$5,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L238
sw	$5,12($17)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1030
.option	pic2
srl	$2,$5,3
.set	macro
.set	reorder

$L960:
lw	$6,72($17)
.set	noreorder
.set	nomacro
beq	$6,$0,$L615
srl	$5,$2,3
.set	macro
.set	reorder

andi	$7,$2,0x7
addu	$5,$3,$5
addiu	$2,$2,1
lbu	$4,0($5)
sll	$4,$4,$7
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L218
sw	$2,12($17)
.set	macro
.set	reorder

$L919:
lw	$2,816($17)
beq	$2,$0,$L920
.option	pic0
j	$L233
.option	pic2
$L457:
.option	pic0
.set	noreorder
.set	nomacro
j	$L475
.option	pic2
lw	$20,0($19)
.set	macro
.set	reorder

$L294:
sll	$7,$16,3
lw	$14,4($17)
li	$13,1			# 0x1
subu	$7,$7,$16
addiu	$7,$7,11712
li	$11,-4			# 0xfffffffffffffffc
addu	$7,$7,$3
li	$23,3			# 0x3
sll	$7,$7,2
move	$9,$3
addu	$7,$17,$7
$L305:
lw	$4,-20664($10)
move	$8,$12
lw	$5,12($17)
xor	$4,$4,$9
movz	$8,$13,$4
srl	$2,$5,3
addu	$2,$14,$2
move	$4,$8
and	$8,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110001000100000000000010010000	#S32LDDR XR2,$8,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001000100000000010001010000	#S32LDDR XR1,$8,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$8,$5,0x7
addu	$2,$2,$8
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001001100010010100110	#S32EXTRV XR2,XR1,$2,$4
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addu	$4,$5,$4
sw	$4,12($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L304
sw	$2,0($7)
.set	macro
.set	reorder

addu	$2,$20,$9
sb	$23,0($2)
$L304:
addiu	$9,$9,1
slt	$2,$6,$9
.set	noreorder
.set	nomacro
beq	$2,$0,$L305
addiu	$7,$7,4
.set	macro
.set	reorder

.option	pic0
j	$L296
.option	pic2
$L907:
mul.s	$f13,$f14,$f8
cvt.d.s	$f14,$f13
sub.d	$f14,$f14,$f10
trunc.w.d $f13,$f14
.option	pic0
.set	noreorder
.set	nomacro
j	$L500
.option	pic2
mfc1	$4,$f13
.set	macro
.set	reorder

$L983:
mtc1	$0,$f2
lui	$3,%hi($LC36)
addu	$2,$17,$2
lwc1	$f1,%lo($LC36)($3)
lwc1	$f0,-20592($2)
lui	$2,%hi($LC37)
mul.s	$f1,$f0,$f1
c.le.s	$fcc2,$f2,$f1
lwc1	$f1,%lo($LC37)($2)
mul.s	$f0,$f0,$f1
.set	noreorder
.set	nomacro
bc1f	$fcc2,$L906
cvt.d.s	$f0,$f0
.set	macro
.set	reorder

add.d	$f0,$f0,$f24
trunc.w.d $f2,$f0
mfc1	$3,$f2
$L494:
li	$2,60736			# 0xed40
li	$5,61760			# 0xf140
addiu	$4,$2,7168
addu	$5,$17,$5
addu	$2,$17,$2
addu	$4,$17,$4
$L495:
lw	$6,0($2)
addiu	$4,$4,16
addiu	$2,$2,16
mul	$6,$3,$6
sw	$6,-16($4)
lw	$6,-12($2)
mul	$6,$3,$6
sw	$6,-12($4)
lw	$6,-8($2)
mul	$6,$3,$6
sw	$6,-8($4)
lw	$6,-4($2)
mul	$6,$3,$6
.set	noreorder
.set	nomacro
bne	$2,$5,$L495
sw	$6,-4($4)
.set	macro
.set	reorder

$L496:
lw	$2,816($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1074
li	$2,65536			# 0x10000
.set	macro
.set	reorder

lw	$25,%call16(ff_eac3_apply_spectral_extension)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_apply_spectral_extension
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

lw	$28,48($sp)
li	$2,65536			# 0x10000
$L1074:
addu	$2,$17,$2
lw	$fp,-20668($2)
lw	$22,-20596($2)
.set	noreorder
.set	nomacro
beq	$fp,$22,$L1075
lw	$10,160($sp)
.set	macro
.set	reorder

lw	$3,-20600($2)
andi	$3,$3,0x8
.set	noreorder
.set	nomacro
beq	$3,$0,$L1076
lw	$3,160($sp)
.set	macro
.set	reorder

lw	$2,-20672($2)
beq	$22,$2,$L505
$L1076:
.set	noreorder
.set	nomacro
bne	$3,$0,$L660
li	$3,1			# 0x1
.set	macro
.set	reorder

li	$2,65536			# 0x10000
li	$6,44876			# 0xaf4c
addu	$4,$17,$2
addiu	$9,$2,1344
li	$3,2			# 0x2
addu	$9,$17,$9
lw	$8,-20672($4)
.set	noreorder
.set	nomacro
beq	$22,$3,$L992
addu	$6,$17,$6
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$22,$3,$L993
addiu	$2,$2,2368
.set	macro
.set	reorder

lw	$2,-20604($4)
beq	$2,$0,$L994
$L621:
li	$2,65536			# 0x10000
addu	$2,$17,$2
lw	$fp,-20668($2)
$L568:
li	$2,6			# 0x6
$L1080:
.set	noreorder
.set	nomacro
beq	$fp,$2,$L1077
li	$20,65536			# 0x10000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$22,$L539
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L1083:
sw	$16,168($sp)
li	$20,54404			# 0xd484
addiu	$21,$2,15680
addiu	$3,$2,7488
addu	$21,$17,$21
addiu	$12,$2,15168
addiu	$11,$2,16192
addiu	$4,$2,15676
sw	$21,152($sp)
addiu	$2,$2,16188
addu	$21,$17,$3
addu	$4,$17,$4
addu	$2,$17,$2
li	$3,1			# 0x1
addu	$12,$17,$12
sw	$4,156($sp)
addu	$20,$17,$20
sw	$2,164($sp)
addu	$23,$17,$11
sw	$3,160($sp)
move	$16,$17
move	$17,$20
move	$20,$12
$L587:
lw	$2,0($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L580
addiu	$2,$21,-6144
.set	macro
.set	reorder

addiu	$7,$21,-5120
move	$fp,$23
move	$3,$23
$L581:
lw	$4,0($2)
addiu	$3,$3,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$7,$L581
sw	$4,-4($3)
.set	macro
.set	reorder

li	$2,54492			# 0xd4dc
lw	$25,%call16(aac_imdct_half_c)($28)
lw	$5,152($sp)
move	$6,$23
addu	$10,$16,$2
sw	$2,204($sp)
move	$4,$10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
sw	$10,200($sp)
.set	macro
.set	reorder

addiu	$13,$21,9216
lw	$2,204($sp)
li	$12,2			# 0x2
lw	$28,48($sp)
li	$11,31			# 0x1f
lw	$10,200($sp)
li	$15,1020			# 0x3fc
addiu	$6,$2,26720
addiu	$2,$2,27232
addu	$6,$16,$6
addu	$3,$16,$2
li	$14,512			# 0x200
move	$2,$0
$L582:
addu	$4,$21,$2
lw	$5,0($6)
lw	$4,0($4)
#APP
# 778 "ac3dec.c" 1
.word	0b01110000100001010000100001100110	#S32MUL XR1,XR2,$4,$5
# 0 "" 2
#NO_APP
addu	$7,$20,$2
lw	$7,-512($7)
#APP
# 779 "ac3dec.c" 1
.word	0b01110000100001110001000011100110	#S32MUL XR3,XR4,$4,$7
# 0 "" 2
#NO_APP
lw	$4,0($3)
#APP
# 780 "ac3dec.c" 1
.word	0b01110000100001111000100001000100	#S32MSUB XR1,XR2,$4,$7
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110000100001011001000011000000	#S32MADD XR3,XR4,$4,$5
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addu	$4,$13,$2
#APP
# 786 "ac3dec.c" 1
.word	0b01110000100000000000000111010001	#S32STD XR7,$4,0
# 0 "" 2
#NO_APP
subu	$4,$15,$2
addu	$4,$13,$4
#APP
# 787 "ac3dec.c" 1
.word	0b01110000100000000000001000010001	#S32STD XR8,$4,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$6,$6,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L582
addiu	$3,$3,-4
.set	macro
.set	reorder

addiu	$2,$21,-6140
addiu	$7,$21,-5116
$L583:
lw	$3,0($2)
addiu	$fp,$fp,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$7,$L583
sw	$3,-4($fp)
.set	macro
.set	reorder

lw	$25,%call16(aac_imdct_half_c)($28)
move	$4,$10
move	$5,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
move	$6,$23
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$7,160($sp)
$L1035:
addiu	$17,$17,4
addiu	$21,$21,1024
addiu	$7,$7,1
slt	$2,$22,$7
.set	noreorder
.set	nomacro
beq	$2,$0,$L587
sw	$7,160($sp)
.set	macro
.set	reorder

move	$17,$16
lw	$16,168($sp)
li	$2,65536			# 0x10000
$L1047:
addu	$2,$17,$2
lw	$22,-20596($2)
lw	$fp,-20668($2)
$L539:
.option	pic0
.set	noreorder
.set	nomacro
j	$L607
.option	pic2
lw	$10,44($17)
.set	macro
.set	reorder

$L981:
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_decode_transform_coeffs_aht_ch
1:	jalr	$25
lw	$5,152($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L453
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L908:
mul.s	$f13,$f14,$f0
cvt.d.s	$f14,$f13
sub.d	$f14,$f14,$f28
trunc.w.d $f13,$f14
.option	pic0
.set	noreorder
.set	nomacro
j	$L500
.option	pic2
mfc1	$4,$f13
.set	macro
.set	reorder

$L980:
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch
.option	pic2
lw	$6,168($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L452
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L458:
lw	$2,1828($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L995
lw	$6,168($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L996
lw	$25,%call16(ff_eac3_decode_transform_coeffs_aht_ch)($28)
.set	macro
.set	reorder

$L462:
lw	$7,-20584($fp)
lw	$20,-20556($fp)
slt	$2,$7,$20
.set	noreorder
.set	nomacro
beq	$2,$0,$L461
li	$3,45056			# 0xb000
.set	macro
.set	reorder

sll	$5,$7,1
sll	$2,$7,3
subu	$5,$2,$5
addiu	$6,$7,14672
addiu	$5,$5,464
addu	$4,$7,$3
addu	$5,$16,$5
sll	$6,$6,2
sll	$5,$5,2
addu	$3,$20,$3
addu	$5,$17,$5
addu	$4,$17,$4
addu	$6,$17,$6
addu	$8,$17,$3
$L463:
lb	$3,0($4)
addiu	$6,$6,4
lw	$2,0($5)
addiu	$4,$4,1
addiu	$5,$5,24
sra	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$4,$8,$L463
sw	$2,-4($6)
.set	macro
.set	reorder

$L461:
lw	$24,256($17)
.set	noreorder
.set	nomacro
blez	$24,$L459
addu	$24,$17,$24
.set	macro
.set	reorder

lw	$31,-20672($fp)
addiu	$23,$17,384
li	$22,2			# 0x2
addiu	$4,$31,1
move	$21,$17
$L465:
lbu	$18,260($21)
.set	noreorder
.set	nomacro
blez	$31,$L468
addu	$18,$18,$7
.set	macro
.set	reorder

addiu	$15,$7,14672
addiu	$8,$18,14672
sll	$15,$15,2
addiu	$10,$18,15184
addiu	$25,$15,2048
sll	$8,$8,2
sll	$10,$10,2
addiu	$12,$17,156
addu	$15,$17,$15
addu	$8,$17,$8
addu	$25,$17,$25
addu	$10,$17,$10
li	$11,1			# 0x1
move	$13,$23
slt	$14,$7,$18
$L467:
lw	$2,0($12)
beq	$2,$0,$L471
lw	$7,0($13)
.set	noreorder
.set	nomacro
beq	$14,$0,$L473
sll	$7,$7,5
.set	macro
.set	reorder

sll	$9,$11,10
move	$5,$15
$L472:
lw	$2,0($5)
addu	$6,$5,$9
addiu	$5,$5,4
sll	$2,$2,4
mult	$2,$7
mfhi	$3
.set	noreorder
.set	nomacro
bne	$5,$8,$L472
sw	$3,0($6)
.set	macro
.set	reorder

$L473:
beq	$11,$22,$L997
$L471:
addiu	$11,$11,1
$L1031:
addiu	$12,$12,4
.set	noreorder
.set	nomacro
bne	$11,$4,$L467
addiu	$13,$13,72
.set	macro
.set	reorder

$L468:
addiu	$21,$21,1
addiu	$23,$23,4
.set	noreorder
.set	nomacro
bne	$21,$24,$L465
move	$7,$18
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L475
.option	pic2
li	$18,1			# 0x1
.set	macro
.set	reorder

$L997:
lw	$2,-200($23)
beq	$2,$0,$L471
.set	noreorder
.set	nomacro
beq	$14,$0,$L471
move	$2,$25
.set	macro
.set	reorder

$L474:
lw	$3,0($2)
addiu	$2,$2,4
subu	$3,$0,$3
.set	noreorder
.set	nomacro
bne	$2,$10,$L474
sw	$3,-4($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1031
.option	pic2
addiu	$11,$11,1
.set	macro
.set	reorder

$L973:
lw	$6,4($17)
lw	$5,12($17)
$L361:
srl	$4,$5,3
li	$11,-4			# 0xfffffffffffffffc
addu	$4,$6,$4
and	$2,$4,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110000010100000000000010010000	#S32LDDR XR2,$2,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000010100000000010001010000	#S32LDDR XR1,$2,4
# 0 "" 2
#NO_APP
andi	$4,$4,0x3
sll	$4,$4,3
andi	$2,$5,0x7
addu	$4,$4,$2
li	$10,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000100010101100010010100110	#S32EXTRV XR2,XR1,$4,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001110000000010101110	#S32M2I XR2, $7
# 0 "" 2
#NO_APP
addiu	$9,$5,3
srl	$2,$9,3
addu	$2,$6,$2
and	$4,$2,$11
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$9,$9,0x7
addu	$2,$2,$9
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010010101100010010100110	#S32EXTRV XR2,XR1,$2,$10
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001100000000010101110	#S32M2I XR2, $6
# 0 "" 2
#NO_APP
addiu	$2,$5,6
.set	noreorder
.set	nomacro
beq	$16,$0,$L363
sw	$2,12($17)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addu	$2,$17,$2
lw	$4,-18492($2)
beq	$7,$4,$L998
$L364:
lbu	$2,140($sp)
andi	$4,$2,0x00ff
sltu	$4,$4,2
beq	$4,$0,$L365
li	$2,2			# 0x2
$L365:
sb	$2,140($sp)
$L363:
li	$2,65536			# 0x10000
addu	$2,$17,$2
$L1092:
sw	$7,-18492($2)
sw	$6,-18488($2)
li	$2,65536			# 0x10000
addu	$2,$17,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L359
.option	pic2
sw	$0,-18484($2)
.set	macro
.set	reorder

$L955:
sw	$2,180($sp)
$L597:
lw	$8,180($sp)
addiu	$2,$8,5
sltu	$3,$2,5
.set	noreorder
.set	nomacro
beq	$3,$0,$L161
sll	$3,$2,2
.set	macro
.set	reorder

lui	$2,%hi($L163)
addiu	$2,$2,%lo($L163)
addu	$2,$2,$3
lw	$2,0($2)
j	$2
.rdata
.align	2
.align	2
$L163:
.word	$L162
.word	$L164
.word	$L165
.word	$L166
.word	$L167
.text
$L636:
move	$2,$0
move	$3,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L147
.option	pic2
move	$4,$0
.set	macro
.set	reorder

$L961:
lw	$8,292($sp)
addiu	$6,$8,1024
move	$2,$8
$L592:
lh	$5,0($3)
addiu	$2,$2,8
lh	$4,4($3)
addiu	$3,$3,8
sh	$5,-8($2)
sh	$5,-6($2)
sh	$4,-4($2)
.set	noreorder
.set	nomacro
bne	$2,$6,$L592
sh	$4,-2($2)
.set	macro
.set	reorder

lw	$9,292($sp)
sll	$2,$22,9
lw	$8,172($sp)
addiu	$16,$16,1
addiu	$18,$18,28
addu	$9,$9,$2
addiu	$8,$8,4
slt	$3,$16,$10
sw	$9,292($sp)
lw	$9,192($sp)
sw	$8,172($sp)
addiu	$9,$9,28
.set	noreorder
.set	nomacro
bne	$3,$0,$L595
sw	$9,192($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1029
.option	pic2
lw	$3,288($sp)
.set	macro
.set	reorder

$L220:
lw	$9,156($sp)
li	$8,1			# 0x1
beq	$9,$8,$L225
.set	noreorder
.set	nomacro
blez	$21,$L226
srl	$7,$4,3
.set	macro
.set	reorder

andi	$9,$4,0x7
addu	$7,$3,$7
addiu	$4,$2,2
lbu	$5,0($7)
sw	$4,12($17)
sll	$5,$5,$9
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
beq	$21,$8,$L226
sb	$5,821($17)
.set	macro
.set	reorder

srl	$7,$4,3
andi	$8,$4,0x7
addu	$4,$3,$7
addiu	$7,$2,3
lbu	$5,0($4)
sw	$7,12($17)
sll	$5,$5,$8
andi	$5,$5,0x00ff
srl	$5,$5,7
sb	$5,822($17)
li	$5,2			# 0x2
.set	noreorder
.set	nomacro
beq	$21,$5,$L226
move	$4,$7
.set	macro
.set	reorder

srl	$4,$7,3
andi	$8,$7,0x7
addu	$4,$3,$4
addiu	$7,$2,4
lbu	$5,0($4)
sw	$7,12($17)
sll	$5,$5,$8
andi	$5,$5,0x00ff
srl	$5,$5,7
sb	$5,823($17)
li	$5,3			# 0x3
.set	noreorder
.set	nomacro
beq	$21,$5,$L226
move	$4,$7
.set	macro
.set	reorder

srl	$4,$7,3
andi	$8,$7,0x7
addu	$4,$3,$4
addiu	$7,$2,5
lbu	$5,0($4)
sw	$7,12($17)
sll	$5,$5,$8
andi	$5,$5,0x00ff
srl	$5,$5,7
sb	$5,824($17)
li	$5,4			# 0x4
.set	noreorder
.set	nomacro
beq	$21,$5,$L226
move	$4,$7
.set	macro
.set	reorder

srl	$4,$7,3
andi	$8,$7,0x7
addu	$4,$3,$4
addiu	$7,$2,6
lbu	$5,0($4)
sw	$7,12($17)
sll	$5,$5,$8
andi	$5,$5,0x00ff
srl	$5,$5,7
sb	$5,825($17)
li	$5,5			# 0x5
.set	noreorder
.set	nomacro
beq	$21,$5,$L226
move	$4,$7
.set	macro
.set	reorder

srl	$5,$7,3
andi	$7,$7,0x7
addu	$5,$3,$5
addiu	$4,$2,7
lbu	$5,0($5)
sw	$4,12($17)
sll	$5,$5,$7
andi	$5,$5,0x00ff
srl	$5,$5,7
sb	$5,826($17)
li	$5,6			# 0x6
.set	noreorder
.set	nomacro
beq	$21,$5,$L226
addiu	$2,$2,8
.set	macro
.set	reorder

sw	$2,12($17)
$L989:
.option	pic0
.set	noreorder
.set	nomacro
j	$L239
.option	pic2
add.d	$f0,$f0,$f26
.set	macro
.set	reorder

$L225:
sb	$9,821($17)
$L226:
srl	$2,$4,3
li	$7,-4			# 0xfffffffffffffffc
addu	$2,$3,$2
and	$5,$2,$7
#APP
# 115 "ac3dec.c" 1
.word	0b01110000101100000000000010010000	#S32LDDR XR2,$5,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000101100000000010001010000	#S32LDDR XR1,$5,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$5,$4,0x7
addu	$2,$2,$5
li	$5,2			# 0x2
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001011100010010100110	#S32EXTRV XR2,XR1,$2,$5
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
addiu	$8,$4,2
srl	$2,$8,3
addu	$2,$3,$2
and	$7,$2,$7
#APP
# 115 "ac3dec.c" 1
.word	0b01110000111100000000000010010000	#S32LDDR XR2,$7,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000111100000000010001010000	#S32LDDR XR1,$7,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$8,$8,0x7
addu	$2,$2,$8
li	$7,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001111100010010100110	#S32EXTRV XR2,XR1,$2,$7
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001110000000010101110	#S32M2I XR2, $7
# 0 "" 2
#NO_APP
addiu	$7,$7,2
slt	$2,$7,8
.set	noreorder
.set	nomacro
bne	$2,$0,$L228
addiu	$8,$4,5
.set	macro
.set	reorder

sll	$7,$7,1
addiu	$7,$7,-7
$L228:
srl	$2,$8,3
addu	$3,$3,$2
li	$2,-4			# 0xfffffffffffffffc
and	$2,$3,$2
#APP
# 115 "ac3dec.c" 1
.word	0b01110000010100000000000010010000	#S32LDDR XR2,$2,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000010100000000010001010000	#S32LDDR XR1,$2,4
# 0 "" 2
#NO_APP
andi	$2,$3,0x3
sll	$2,$2,3
andi	$8,$8,0x7
addu	$2,$2,$8
li	$3,3			# 0x3
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010000111100010010100110	#S32EXTRV XR2,XR1,$2,$3
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010000000000010101110	#S32M2I XR2, $8
# 0 "" 2
#NO_APP
addiu	$2,$4,8
addiu	$8,$8,5
slt	$3,$8,8
.set	noreorder
.set	nomacro
bne	$3,$0,$L229
sw	$2,12($17)
.set	macro
.set	reorder

sll	$8,$8,1
addiu	$8,$8,-7
$L229:
slt	$2,$7,$8
.set	noreorder
.set	nomacro
beq	$2,$0,$L999
sll	$2,$5,2
.set	macro
.set	reorder

sll	$4,$7,2
sll	$5,$5,4
sll	$3,$7,4
subu	$2,$5,$2
subu	$3,$3,$4
addiu	$2,$2,25
addiu	$3,$3,25
slt	$4,$2,$3
.set	noreorder
.set	nomacro
beq	$4,$0,$L1000
sll	$5,$8,2
.set	macro
.set	reorder

sw	$2,844($17)
sll	$4,$8,4
sw	$3,836($17)
addiu	$9,$17,848
subu	$2,$4,$5
lw	$4,188($sp)
addiu	$3,$17,852
addiu	$2,$2,25
move	$5,$16
sw	$2,840($17)
sw	$8,16($sp)
sw	$9,24($sp)
sw	$3,28($sp)
lw	$2,%got(ff_eac3_default_spx_band_struct)($28)
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_band_structure.constprop.8
.option	pic2
sw	$2,20($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L919
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L978:
lui	$6,%hi($LC35)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC35)
lw	$16,164($sp)
lw	$18,168($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
lw	$19,176($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L660:
sw	$3,152($sp)
li	$3,65536			# 0x10000
addu	$3,$17,$3
lw	$2,-20604($3)
bne	$2,$0,$L1001
$L507:
li	$2,6			# 0x6
$L1045:
.set	noreorder
.set	nomacro
beq	$fp,$2,$L1078
li	$20,65536			# 0x10000
.set	macro
.set	reorder

$L1014:
.set	noreorder
.set	nomacro
blez	$fp,$L519
li	$7,1			# 0x1
.set	macro
.set	reorder

li	$2,65536			# 0x10000
sw	$16,176($sp)
li	$20,54404			# 0xd484
addiu	$12,$2,15168
addiu	$11,$2,16192
addiu	$21,$2,15680
addiu	$3,$2,7488
addiu	$4,$2,15676
addiu	$2,$2,16188
addu	$21,$17,$21
addu	$4,$17,$4
addu	$2,$17,$2
sw	$21,156($sp)
addu	$12,$17,$12
sw	$4,164($sp)
addu	$20,$17,$20
sw	$2,168($sp)
addu	$22,$17,$11
sw	$7,160($sp)
addu	$21,$17,$3
move	$16,$17
move	$17,$20
move	$20,$12
$L533:
lw	$2,0($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L526
addiu	$2,$21,-6144
.set	macro
.set	reorder

addiu	$7,$21,-5120
move	$23,$22
move	$3,$22
$L527:
lw	$4,0($2)
addiu	$3,$3,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$7,$L527
sw	$4,-4($3)
.set	macro
.set	reorder

li	$2,54492			# 0xd4dc
lw	$25,%call16(aac_imdct_half_c)($28)
lw	$5,156($sp)
move	$6,$22
addu	$10,$16,$2
sw	$2,204($sp)
move	$4,$10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
sw	$10,200($sp)
.set	macro
.set	reorder

addiu	$13,$21,9216
lw	$2,204($sp)
li	$12,2			# 0x2
lw	$28,48($sp)
li	$11,31			# 0x1f
lw	$10,200($sp)
li	$15,1020			# 0x3fc
addiu	$6,$2,26720
addiu	$2,$2,27232
addu	$6,$16,$6
addu	$3,$16,$2
li	$14,512			# 0x200
move	$2,$0
$L528:
addu	$4,$21,$2
lw	$5,0($6)
lw	$4,0($4)
#APP
# 778 "ac3dec.c" 1
.word	0b01110000100001010000100001100110	#S32MUL XR1,XR2,$4,$5
# 0 "" 2
#NO_APP
addu	$7,$20,$2
lw	$7,-512($7)
#APP
# 779 "ac3dec.c" 1
.word	0b01110000100001110001000011100110	#S32MUL XR3,XR4,$4,$7
# 0 "" 2
#NO_APP
lw	$4,0($3)
#APP
# 780 "ac3dec.c" 1
.word	0b01110000100001111000100001000100	#S32MSUB XR1,XR2,$4,$7
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110000100001011001000011000000	#S32MADD XR3,XR4,$4,$5
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110001100010111100100001100110	#S32EXTRV XR1,XR2,$12,$11
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110001100010111101000011100110	#S32EXTRV XR3,XR4,$12,$11
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addu	$4,$13,$2
#APP
# 786 "ac3dec.c" 1
.word	0b01110000100000000000000111010001	#S32STD XR7,$4,0
# 0 "" 2
#NO_APP
subu	$4,$15,$2
addu	$4,$13,$4
#APP
# 787 "ac3dec.c" 1
.word	0b01110000100000000000001000010001	#S32STD XR8,$4,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$6,$6,-4
.set	noreorder
.set	nomacro
bne	$2,$14,$L528
addiu	$3,$3,-4
.set	macro
.set	reorder

addiu	$2,$21,-6140
addiu	$7,$21,-5116
$L529:
lw	$3,0($2)
addiu	$23,$23,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$7,$L529
sw	$3,-4($23)
.set	macro
.set	reorder

lw	$25,%call16(aac_imdct_half_c)($28)
move	$4,$10
move	$5,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
move	$6,$22
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$9,160($sp)
$L1032:
addiu	$17,$17,4
addiu	$21,$21,1024
addiu	$9,$9,1
slt	$2,$fp,$9
.set	noreorder
.set	nomacro
beq	$2,$0,$L533
sw	$9,160($sp)
.set	macro
.set	reorder

move	$17,$16
lw	$16,176($sp)
li	$2,65536			# 0x10000
$L1048:
addu	$2,$17,$2
lw	$22,-20596($2)
$L519:
lw	$10,152($sp)
.set	noreorder
.set	nomacro
bne	$10,$0,$L1002
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L534:
addu	$2,$17,$2
lw	$10,44($17)
.option	pic0
.set	noreorder
.set	nomacro
j	$L607
.option	pic2
lw	$fp,-20668($2)
.set	macro
.set	reorder

$L526:
li	$8,54428			# 0xd49c
lw	$25,%call16(aac_imdct_half_c)($28)
lw	$5,156($sp)
addiu	$6,$21,-6144
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
addu	$4,$16,$8
.set	macro
.set	reorder

addiu	$10,$21,9216
lw	$28,48($sp)
move	$2,$0
lw	$4,164($sp)
li	$7,2			# 0x2
lw	$3,168($sp)
li	$6,31			# 0x1f
li	$14,1020			# 0x3fc
li	$13,512			# 0x200
$L531:
addu	$5,$21,$2
lw	$8,0($4)
lw	$5,0($5)
#APP
# 778 "ac3dec.c" 1
.word	0b01110000101010000000100001100110	#S32MUL XR1,XR2,$5,$8
# 0 "" 2
#NO_APP
addu	$9,$20,$2
lw	$9,-512($9)
#APP
# 779 "ac3dec.c" 1
.word	0b01110000101010010001000011100110	#S32MUL XR3,XR4,$5,$9
# 0 "" 2
#NO_APP
lw	$5,0($3)
#APP
# 780 "ac3dec.c" 1
.word	0b01110000101010011000100001000100	#S32MSUB XR1,XR2,$5,$9
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110000101010001001000011000000	#S32MADD XR3,XR4,$5,$8
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110000111001101100100001100110	#S32EXTRV XR1,XR2,$7,$6
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110000111001101101000011100110	#S32EXTRV XR3,XR4,$7,$6
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addu	$5,$10,$2
#APP
# 786 "ac3dec.c" 1
.word	0b01110000101000000000000111010001	#S32STD XR7,$5,0
# 0 "" 2
#NO_APP
subu	$5,$14,$2
addu	$5,$10,$5
#APP
# 787 "ac3dec.c" 1
.word	0b01110000101000000000001000010001	#S32STD XR8,$5,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$4,$4,-4
.set	noreorder
.set	nomacro
bne	$2,$13,$L531
addiu	$3,$3,-4
.set	macro
.set	reorder

addiu	$10,$22,512
move	$3,$22
move	$2,$21
$L532:
lwl	$7,3($3)
lwl	$6,7($3)
lwl	$5,11($3)
lwl	$4,15($3)
lwr	$7,0($3)
lwr	$6,4($3)
lwr	$5,8($3)
lwr	$4,12($3)
addiu	$3,$3,16
swl	$7,3($2)
swr	$7,0($2)
swl	$6,7($2)
swr	$6,4($2)
swl	$5,11($2)
swr	$5,8($2)
swl	$4,15($2)
swr	$4,12($2)
.set	noreorder
.set	nomacro
bne	$3,$10,$L532
addiu	$2,$2,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1032
.option	pic2
lw	$9,160($sp)
.set	macro
.set	reorder

$L336:
srl	$2,$5,3
li	$9,-4			# 0xfffffffffffffffc
addu	$2,$12,$2
and	$9,$2,$9
#APP
# 115 "ac3dec.c" 1
.word	0b01110001001100000000000010010000	#S32LDDR XR2,$9,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001001100000000010001010000	#S32LDDR XR1,$9,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$5,$5,0x7
addu	$2,$2,$5
li	$5,6			# 0x6
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001011100010010100110	#S32EXTRV XR2,XR1,$2,$5
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000110000000000010101110	#S32M2I XR2, $24
# 0 "" 2
#NO_APP
addiu	$4,$4,7
addiu	$24,$24,-15
slt	$2,$6,$3
sw	$4,12($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L334
sll	$24,$24,4
.set	macro
.set	reorder

addiu	$2,$3,11771
lw	$25,%got(ff_ac3_fast_gain_tab)($28)
move	$11,$3
sll	$2,$2,2
addu	$9,$20,$3
move	$5,$0
addu	$2,$17,$2
li	$13,-4			# 0xfffffffffffffffc
li	$fp,4			# 0x4
.set	noreorder
.set	nomacro
beq	$3,$11,$L337
li	$8,3			# 0x3
.set	macro
.set	reorder

li	$10,2			# 0x2
$L1079:
beq	$14,$10,$L337
$L338:
beq	$16,$0,$L339
lw	$4,-28($2)
beq	$4,$5,$L339
lbu	$4,0($9)
andi	$10,$4,0x00ff
bne	$10,$0,$L340
li	$4,1			# 0x1
$L340:
sb	$4,0($9)
$L339:
.set	noreorder
.set	nomacro
bne	$7,$0,$L342
sw	$5,-28($2)
.set	macro
.set	reorder

lw	$10,12($17)
lw	$15,0($2)
srl	$4,$10,3
addu	$4,$12,$4
and	$23,$4,$13
#APP
# 115 "ac3dec.c" 1
.word	0b01110010111100000000000010010000	#S32LDDR XR2,$23,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010111100000000010001010000	#S32LDDR XR1,$23,4
# 0 "" 2
#NO_APP
andi	$4,$4,0x3
sll	$4,$4,3
andi	$23,$10,0x7
addu	$4,$4,$23
#APP
# 117 "ac3dec.c" 1
.word	0b01110000100010001100010010100110	#S32EXTRV XR2,XR1,$4,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001000000000010101110	#S32M2I XR2, $4
# 0 "" 2
#NO_APP
sll	$4,$4,1
addiu	$10,$10,3
addu	$4,$25,$4
sw	$10,12($17)
lhu	$4,0($4)
.set	noreorder
.set	nomacro
beq	$16,$0,$L342
sw	$4,0($2)
.set	macro
.set	reorder

beq	$15,$4,$L342
lbu	$4,0($9)
andi	$10,$4,0x00ff
sltu	$10,$10,2
beq	$10,$0,$L344
li	$4,2			# 0x2
$L344:
sb	$4,0($9)
$L342:
addiu	$11,$11,1
addiu	$9,$9,1
slt	$4,$6,$11
.set	noreorder
.set	nomacro
bne	$4,$0,$L334
addiu	$2,$2,4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$11,$L1079
li	$10,2			# 0x2
.set	macro
.set	reorder

$L337:
lw	$5,12($17)
srl	$4,$5,3
addu	$4,$12,$4
and	$10,$4,$13
#APP
# 115 "ac3dec.c" 1
.word	0b01110001010100000000000010010000	#S32LDDR XR2,$10,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001010100000000010001010000	#S32LDDR XR1,$10,4
# 0 "" 2
#NO_APP
andi	$4,$4,0x3
sll	$4,$4,3
andi	$10,$5,0x7
addu	$4,$4,$10
#APP
# 117 "ac3dec.c" 1
.word	0b01110000100111101100010010100110	#S32EXTRV XR2 XR1 $4 $30
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000010100000000010101110	#S32M2I XR2, $10
# 0 "" 2
#NO_APP
addiu	$4,$5,4
addu	$5,$10,$24
sw	$4,12($17)
.option	pic0
.set	noreorder
.set	nomacro
j	$L338
.option	pic2
sll	$5,$5,2
.set	macro
.set	reorder

$L323:
.set	noreorder
.set	nomacro
beq	$16,$0,$L1004
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

lw	$7,72($17)
beq	$7,$0,$L333
.option	pic0
.set	noreorder
.set	nomacro
j	$L1033
.option	pic2
lw	$2,92($17)
.set	macro
.set	reorder

$L279:
lw	$8,4($17)
addiu	$6,$17,184
sll	$2,$2,2
addu	$2,$17,$2
$L280:
lw	$4,12($17)
addiu	$6,$6,4
srl	$5,$4,3
andi	$7,$4,0x7
addu	$5,$8,$5
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$6,$2,$L280
sw	$3,-4($6)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1034
.option	pic2
lw	$2,72($17)
.set	macro
.set	reorder

$L144:
sll	$3,$7,3
sra	$2,$3,3
bltz	$2,$L636
bltz	$3,$L636
addu	$2,$16,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L147
.option	pic2
move	$4,$16
.set	macro
.set	reorder

$L969:
lui	$6,%hi($LC29)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC29)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L285:
lw	$2,816($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L613
li	$3,4			# 0x4
.set	macro
.set	reorder

lw	$2,836($17)
slt	$2,$2,62
.set	noreorder
.set	nomacro
beq	$2,$0,$L613
li	$2,65536			# 0x10000
.set	macro
.set	reorder

li	$4,3			# 0x3
addu	$2,$17,$2
li	$3,3			# 0x3
.option	pic0
.set	noreorder
.set	nomacro
j	$L613
.option	pic2
sw	$4,-20528($2)
.set	macro
.set	reorder

$L349:
lw	$7,%got(ff_ac3_fast_gain_tab)($28)
addiu	$11,$2,4
addiu	$9,$3,11771
addu	$5,$5,$3
sll	$2,$6,1
subu	$5,$11,$5
sll	$9,$9,2
addu	$2,$2,$6
addu	$9,$17,$9
addu	$12,$20,$3
addu	$2,$5,$2
li	$14,-4			# 0xfffffffffffffffc
li	$13,3			# 0x3
move	$11,$4
$L354:
srl	$5,$11,3
lw	$8,0($9)
addu	$5,$10,$5
and	$15,$5,$14
#APP
# 115 "ac3dec.c" 1
.word	0b01110001111100000000000010010000	#S32LDDR XR2,$15,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110001111100000000010001010000	#S32LDDR XR1,$15,4
# 0 "" 2
#NO_APP
andi	$5,$5,0x3
sll	$5,$5,3
andi	$15,$11,0x7
addu	$5,$5,$15
#APP
# 117 "ac3dec.c" 1
.word	0b01110000101011011100010010100110	#S32EXTRV XR2,XR1,$5,$13
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001010000000010101110	#S32M2I XR2, $5
# 0 "" 2
#NO_APP
sll	$5,$5,1
addu	$5,$7,$5
lhu	$5,0($5)
.set	noreorder
.set	nomacro
beq	$8,$5,$L352
sw	$5,0($9)
.set	macro
.set	reorder

lbu	$5,0($12)
andi	$8,$5,0x00ff
sltu	$8,$8,2
beq	$8,$0,$L353
li	$5,2			# 0x2
$L353:
sb	$5,0($12)
$L352:
addiu	$11,$11,3
addiu	$9,$9,4
.set	noreorder
.set	nomacro
bne	$11,$2,$L354
addiu	$12,$12,1
.set	macro
.set	reorder

subu	$6,$6,$3
addiu	$4,$4,3
sll	$2,$6,1
addu	$2,$2,$6
.option	pic0
.set	noreorder
.set	nomacro
j	$L351
.option	pic2
addu	$2,$4,$2
.set	macro
.set	reorder

$L505:
$L1075:
.set	noreorder
.set	nomacro
beq	$10,$0,$L1080
li	$2,6			# 0x6
.set	macro
.set	reorder

li	$3,65536			# 0x10000
sw	$0,152($sp)
addu	$3,$17,$3
lw	$2,-20604($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1045
li	$2,6			# 0x6
.set	macro
.set	reorder

$L1001:
lw	$2,48($17)
sw	$0,-20604($3)
sltu	$3,$2,8
beq	$3,$0,$L507
sll	$3,$2,2
lui	$2,%hi($L509)
addiu	$2,$2,%lo($L509)
addu	$2,$2,$3
lw	$2,0($2)
j	$2
.rdata
.align	2
.align	2
$L509:
.word	$L508
.word	$L507
.word	$L508
.word	$L510
.word	$L511
.word	$L512
.word	$L513
.word	$L514
.text
$L975:
lw	$6,12($17)
srl	$2,$6,3
addu	$2,$12,$2
and	$4,$2,$15
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$6,0x7
addu	$2,$2,$4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010010001100010010100110	#S32EXTRV XR2,XR1,$2,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001110000000010101110	#S32M2I XR2, $7
# 0 "" 2
#NO_APP
addiu	$5,$6,3
sw	$5,12($17)
.set	noreorder
.set	nomacro
bltz	$7,$L379
sw	$7,0($9)
.set	macro
.set	reorder

sll	$21,$7,2
sll	$7,$7,4
addiu	$6,$6,15
subu	$14,$7,$21
move	$10,$11
addu	$14,$6,$14
$L378:
srl	$2,$5,3
addu	$2,$12,$2
and	$4,$2,$15
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$5,0x7
addu	$2,$2,$4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010111101100010010100110	#S32EXTRV XR2 XR1 $2 $30
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addiu	$4,$5,5
sb	$2,0($10)
srl	$2,$4,3
addu	$2,$12,$2
and	$23,$2,$15
#APP
# 115 "ac3dec.c" 1
.word	0b01110010111100000000000010010000	#S32LDDR XR2,$23,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010111100000000010001010000	#S32LDDR XR1,$23,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$4,0x7
addu	$2,$2,$4
li	$4,4			# 0x4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001001100010010100110	#S32EXTRV XR2,XR1,$2,$4
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addiu	$4,$5,9
sb	$2,56($10)
srl	$2,$4,3
addu	$2,$12,$2
and	$23,$2,$15
#APP
# 115 "ac3dec.c" 1
.word	0b01110010111100000000000010010000	#S32LDDR XR2,$23,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110010111100000000010001010000	#S32LDDR XR1,$23,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$4,$4,0x7
addu	$2,$2,$4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010010001100010010100110	#S32EXTRV XR2,XR1,$2,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000100000000010101110	#S32M2I XR2, $2
# 0 "" 2
#NO_APP
addiu	$5,$5,12
sb	$2,112($10)
.set	noreorder
.set	nomacro
bne	$5,$14,$L378
addiu	$10,$10,1
.set	macro
.set	reorder

subu	$7,$7,$21
addu	$6,$6,$7
sw	$6,12($17)
$L379:
lbu	$2,0($13)
andi	$4,$2,0x00ff
sltu	$4,$4,2
beq	$4,$0,$L377
li	$2,2			# 0x2
$L377:
.option	pic0
.set	noreorder
.set	nomacro
j	$L375
.option	pic2
sb	$2,0($13)
.set	macro
.set	reorder

$L580:
li	$5,54428			# 0xd49c
lw	$25,%call16(aac_imdct_half_c)($28)
addiu	$6,$21,-6144
addu	$4,$16,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
lw	$5,152($sp)
.set	macro
.set	reorder

addiu	$10,$21,9216
lw	$28,48($sp)
move	$2,$0
lw	$4,156($sp)
li	$7,2			# 0x2
lw	$3,164($sp)
li	$6,31			# 0x1f
li	$14,1020			# 0x3fc
li	$13,512			# 0x200
$L585:
addu	$5,$21,$2
lw	$8,0($4)
lw	$5,0($5)
#APP
# 778 "ac3dec.c" 1
.word	0b01110000101010000000100001100110	#S32MUL XR1,XR2,$5,$8
# 0 "" 2
#NO_APP
addu	$9,$20,$2
lw	$9,-512($9)
#APP
# 779 "ac3dec.c" 1
.word	0b01110000101010010001000011100110	#S32MUL XR3,XR4,$5,$9
# 0 "" 2
#NO_APP
lw	$5,0($3)
#APP
# 780 "ac3dec.c" 1
.word	0b01110000101010011000100001000100	#S32MSUB XR1,XR2,$5,$9
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110000101010001001000011000000	#S32MADD XR3,XR4,$5,$8
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110000111001101100100001100110	#S32EXTRV XR1,XR2,$7,$6
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110000111001101101000011100110	#S32EXTRV XR3,XR4,$7,$6
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addu	$5,$10,$2
#APP
# 786 "ac3dec.c" 1
.word	0b01110000101000000000000111010001	#S32STD XR7,$5,0
# 0 "" 2
#NO_APP
subu	$5,$14,$2
addu	$5,$10,$5
#APP
# 787 "ac3dec.c" 1
.word	0b01110000101000000000001000010001	#S32STD XR8,$5,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$4,$4,-4
.set	noreorder
.set	nomacro
bne	$2,$13,$L585
addiu	$3,$3,-4
.set	macro
.set	reorder

addiu	$10,$23,512
move	$3,$23
move	$2,$21
$L586:
lwl	$7,3($3)
lwl	$6,7($3)
lwl	$5,11($3)
lwl	$4,15($3)
lwr	$7,0($3)
lwr	$6,4($3)
lwr	$5,8($3)
lwr	$4,12($3)
addiu	$3,$3,16
swl	$7,3($2)
swr	$7,0($2)
swl	$6,7($2)
swr	$6,4($2)
swl	$5,11($2)
swr	$5,8($2)
swl	$4,15($2)
swr	$4,12($2)
.set	noreorder
.set	nomacro
bne	$3,$10,$L586
addiu	$2,$2,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1035
.option	pic2
lw	$7,160($sp)
.set	macro
.set	reorder

$L979:
li	$3,256			# 0x100
sw	$18,196($sp)
li	$7,6144			# 0x1800
sll	$2,$16,2
addiu	$10,$17,156
sw	$3,152($sp)
addiu	$2,$2,1856
sw	$7,156($sp)
li	$3,65536			# 0x10000
li	$7,1			# 0x1
sw	$10,164($sp)
move	$20,$0
sw	$2,176($sp)
addu	$fp,$17,$3
move	$21,$10
move	$18,$7
$L447:
li	$8,2			# 0x2
beq	$18,$8,$L1005
lw	$2,1676($21)
.set	noreorder
.set	nomacro
bne	$2,$0,$L423
li	$2,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$18,$2,$L1006
move	$4,$17
.set	macro
.set	reorder

move	$5,$18
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch_skip
.option	pic2
move	$6,$19
.set	macro
.set	reorder

lw	$28,48($sp)
$L425:
lw	$2,0($21)
$L1036:
.set	noreorder
.set	nomacro
beq	$2,$0,$L1081
lw	$3,156($sp)
.set	macro
.set	reorder

beq	$20,$0,$L430
$L435:
li	$20,1			# 0x1
$L422:
lw	$3,156($sp)
$L1081:
addiu	$18,$18,1
lw	$7,152($sp)
addiu	$21,$21,4
lw	$6,-20668($fp)
addiu	$3,$3,6144
addiu	$7,$7,256
slt	$2,$6,$18
sw	$3,156($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L447
sw	$7,152($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L395
.option	pic2
lw	$18,196($sp)
.set	macro
.set	reorder

$L430:
lw	$2,1828($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L431
move	$5,$0
.set	macro
.set	reorder

move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch_skip
.option	pic2
move	$6,$19
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$7,-20584($fp)
$L432:
lw	$2,256($17)
$L1039:
.set	noreorder
.set	nomacro
blez	$2,$L435
addu	$2,$17,$2
.set	macro
.set	reorder

lw	$25,-20672($fp)
addiu	$23,$17,384
move	$22,$17
addiu	$14,$25,1
sw	$2,168($sp)
$L437:
lbu	$20,260($22)
.set	noreorder
.set	nomacro
blez	$25,$L440
addu	$20,$20,$7
.set	macro
.set	reorder

addiu	$15,$7,14672
lw	$11,164($sp)
addiu	$8,$20,14672
sll	$15,$15,2
addiu	$9,$20,15184
addiu	$24,$15,2048
sll	$8,$8,2
sll	$9,$9,2
addu	$15,$17,$15
addu	$8,$17,$8
addu	$24,$17,$24
addu	$9,$17,$9
li	$10,1			# 0x1
move	$12,$23
slt	$13,$7,$20
$L439:
lw	$2,0($11)
beq	$2,$0,$L443
lw	$6,0($12)
.set	noreorder
.set	nomacro
beq	$13,$0,$L445
sll	$6,$6,5
.set	macro
.set	reorder

sll	$7,$10,10
move	$4,$15
$L444:
lw	$2,0($4)
addu	$5,$4,$7
addiu	$4,$4,4
sll	$2,$2,4
mult	$2,$6
mfhi	$3
.set	noreorder
.set	nomacro
bne	$4,$8,$L444
sw	$3,0($5)
.set	macro
.set	reorder

$L445:
li	$3,2			# 0x2
beq	$10,$3,$L1007
$L443:
addiu	$10,$10,1
$L1037:
addiu	$11,$11,4
.set	noreorder
.set	nomacro
bne	$10,$14,$L439
addiu	$12,$12,72
.set	macro
.set	reorder

$L440:
lw	$2,168($sp)
addiu	$22,$22,1
addiu	$23,$23,4
.set	noreorder
.set	nomacro
bne	$22,$2,$L437
move	$7,$20
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L422
.option	pic2
li	$20,1			# 0x1
.set	macro
.set	reorder

$L423:
.set	noreorder
.set	nomacro
bne	$16,$0,$L1082
li	$2,44800			# 0xaf00
.set	macro
.set	reorder

lw	$25,%call16(ff_eac3_decode_transform_coeffs_aht_ch)($28)
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_decode_transform_coeffs_aht_ch
1:	jalr	$25
move	$5,$18
.set	macro
.set	reorder

lw	$28,48($sp)
li	$2,44800			# 0xaf00
$L1082:
li	$3,44828			# 0xaf1c
addu	$2,$21,$2
addu	$3,$21,$3
lw	$2,0($2)
lw	$7,0($3)
slt	$3,$2,$7
.set	noreorder
.set	nomacro
beq	$3,$0,$L425
lw	$3,152($sp)
.set	macro
.set	reorder

li	$9,45056			# 0xb000
lw	$10,156($sp)
sll	$8,$2,3
sll	$6,$2,5
addu	$4,$3,$9
lw	$9,176($sp)
addiu	$5,$3,14672
subu	$6,$6,$8
addu	$3,$10,$9
addu	$5,$5,$2
addu	$6,$3,$6
addu	$2,$4,$2
addu	$3,$4,$7
sll	$5,$5,2
addu	$6,$17,$6
addu	$4,$17,$2
addu	$5,$17,$5
addu	$7,$17,$3
$L428:
lb	$3,0($4)
addiu	$5,$5,4
lw	$2,0($6)
addiu	$4,$4,1
addiu	$6,$6,24
sra	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$4,$7,$L428
sw	$2,-4($5)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1036
.option	pic2
lw	$2,0($21)
.set	macro
.set	reorder

$L1005:
lw	$2,1836($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1008
li	$5,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L399
lw	$25,%call16(ff_eac3_decode_transform_coeffs_aht_ch)($28)
.set	macro
.set	reorder

li	$5,2			# 0x2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_decode_transform_coeffs_aht_ch
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

lw	$28,48($sp)
$L399:
lw	$2,-20576($fp)
lw	$4,-20548($fp)
slt	$3,$2,$4
.set	noreorder
.set	nomacro
beq	$3,$0,$L398
sll	$3,$2,3
.set	macro
.set	reorder

sll	$5,$2,1
li	$7,45568			# 0xb200
subu	$5,$3,$5
addiu	$6,$2,15184
addiu	$5,$5,3536
addu	$2,$2,$7
addu	$5,$16,$5
addu	$7,$4,$7
sll	$6,$6,2
sll	$5,$5,2
addu	$4,$17,$2
addu	$5,$17,$5
addu	$6,$17,$6
addu	$7,$17,$7
$L401:
lb	$3,0($4)
addiu	$6,$6,4
lw	$2,0($5)
addiu	$4,$4,1
addiu	$5,$5,24
sra	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$4,$7,$L401
sw	$2,-4($6)
.set	macro
.set	reorder

$L398:
lw	$2,160($17)
beq	$2,$0,$L403
beq	$20,$0,$L404
lw	$11,-20556($fp)
$L405:
li	$20,1			# 0x1
$L421:
subu	$2,$0,$11
lw	$25,%call16(memset)($28)
sll	$3,$11,2
sll	$2,$2,2
li	$4,60736			# 0xed40
addiu	$2,$2,1024
li	$6,4			# 0x4
slt	$11,$11,256
addu	$4,$3,$4
movn	$6,$2,$11
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$17,$4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L422
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L403:
.option	pic0
.set	noreorder
.set	nomacro
j	$L421
.option	pic2
lw	$11,-20548($fp)
.set	macro
.set	reorder

$L1006:
li	$5,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch
.option	pic2
move	$6,$19
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L425
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L1007:
lw	$2,-200($23)
beq	$2,$0,$L443
.set	noreorder
.set	nomacro
beq	$13,$0,$L443
move	$2,$24
.set	macro
.set	reorder

$L446:
lw	$3,0($2)
addiu	$2,$2,4
subu	$3,$0,$3
.set	noreorder
.set	nomacro
bne	$2,$9,$L446
sw	$3,-4($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1037
.option	pic2
addiu	$10,$10,1
.set	macro
.set	reorder

$L371:
lui	$6,%hi($LC34)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC34)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L954:
li	$6,2			# 0x2
lw	$25,%call16(memset)($28)
li	$3,65536			# 0x10000
sw	$0,72($17)
sw	$0,92($17)
addu	$3,$17,$3
sw	$6,76($17)
move	$5,$0
li	$6,28			# 0x1c
sw	$2,80($17)
addiu	$4,$17,1828
sw	$2,84($17)
sw	$2,88($17)
sw	$0,-18484($3)
sw	$2,96($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sw	$2,100($17)
.set	macro
.set	reorder

sltu	$6,$18,1
lw	$28,48($sp)
lw	$2,4($17)
lw	$5,12($17)
addiu	$3,$5,5
$L1091:
addiu	$4,$5,6
srl	$8,$3,3
sw	$3,12($17)
andi	$7,$3,0x7
addu	$3,$2,$8
lbu	$3,0($3)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L151
sw	$4,12($17)
.set	macro
.set	reorder

addiu	$4,$5,14
sw	$4,12($17)
$L151:
srl	$5,$4,3
andi	$7,$4,0x7
addu	$5,$2,$5
addiu	$3,$4,1
lbu	$5,0($5)
sll	$5,$5,$7
andi	$5,$5,0x00ff
srl	$5,$5,7
.set	noreorder
.set	nomacro
beq	$5,$0,$L152
sw	$3,12($17)
.set	macro
.set	reorder

addiu	$3,$4,9
sw	$3,12($17)
$L152:
srl	$4,$3,3
andi	$7,$3,0x7
addu	$4,$2,$4
addiu	$5,$3,1
lbu	$4,0($4)
sll	$4,$4,$7
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L153
sw	$5,12($17)
.set	macro
.set	reorder

addiu	$5,$3,8
sw	$5,12($17)
$L153:
.set	noreorder
.set	nomacro
beq	$6,$0,$L1009
move	$6,$0
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1091
.option	pic2
addiu	$3,$5,5
.set	macro
.set	reorder

$L971:
lui	$6,%hi($LC30)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC30)
lw	$16,164($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
lw	$19,168($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L1009:
addiu	$3,$5,2
addiu	$6,$5,3
srl	$7,$3,3
sw	$3,12($17)
andi	$4,$3,0x7
addu	$3,$2,$7
lbu	$3,0($3)
sll	$3,$3,$4
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L155
sw	$6,12($17)
.set	macro
.set	reorder

addiu	$6,$5,17
sw	$6,12($17)
$L155:
srl	$4,$6,3
andi	$5,$6,0x7
addu	$4,$2,$4
addiu	$3,$6,1
lbu	$4,0($4)
sll	$4,$4,$5
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L156
sw	$3,12($17)
.set	macro
.set	reorder

addiu	$3,$6,15
sw	$3,12($17)
$L156:
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$2,$4
lbu	$4,0($4)
sll	$4,$4,$5
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L157
addiu	$6,$3,1
.set	macro
.set	reorder

srl	$4,$6,3
addu	$2,$2,$4
li	$4,-4			# 0xfffffffffffffffc
and	$4,$2,$4
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$2,$2,0x3
sll	$2,$2,3
andi	$6,$6,0x7
addu	$2,$2,$6
li	$4,6			# 0x6
#APP
# 117 "ac3dec.c" 1
.word	0b01110000010001001100010010100110	#S32EXTRV XR2,XR1,$2,$4
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001000000000010101110	#S32M2I XR2, $4
# 0 "" 2
#NO_APP
addiu	$2,$3,7
li	$3,-1			# 0xffffffffffffffff
sw	$2,12($17)
$L158:
addiu	$2,$2,8
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
beq	$4,$3,$L596
sw	$2,12($17)
.set	macro
.set	reorder

addiu	$2,$2,8
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$4,$3,$L158
sw	$2,12($17)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1038
.option	pic2
lw	$9,184($sp)
.set	macro
.set	reorder

$L431:
.set	noreorder
.set	nomacro
bne	$16,$0,$L433
lw	$25,%call16(ff_eac3_decode_transform_coeffs_aht_ch)($28)
.set	macro
.set	reorder

move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_decode_transform_coeffs_aht_ch
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

lw	$28,48($sp)
$L433:
lw	$7,-20584($fp)
lw	$3,-20556($fp)
slt	$2,$7,$3
.set	noreorder
.set	nomacro
beq	$2,$0,$L432
sll	$4,$7,1
.set	macro
.set	reorder

sll	$2,$7,3
li	$10,45056			# 0xb000
subu	$2,$2,$4
addiu	$6,$7,14672
addiu	$2,$2,464
addu	$5,$7,$10
addu	$2,$16,$2
sll	$6,$6,2
addu	$3,$3,$10
sll	$2,$2,2
addu	$5,$17,$5
addu	$2,$17,$2
addu	$6,$17,$6
addu	$8,$17,$3
$L434:
lb	$4,0($5)
addiu	$6,$6,4
lw	$3,0($2)
addiu	$5,$5,1
addiu	$2,$2,24
sra	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$5,$8,$L434
sw	$3,-4($6)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1039
.option	pic2
lw	$2,256($17)
.set	macro
.set	reorder

$L157:
lw	$9,184($sp)
slt	$2,$9,$19
.set	noreorder
.set	nomacro
beq	$2,$0,$L172
sw	$6,12($17)
.set	macro
.set	reorder

lui	$6,%hi($LC13)
$L1053:
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)
.set	macro
.set	reorder

li	$10,-4			# 0xfffffffffffffffc
lw	$28,48($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L169
.option	pic2
sw	$10,180($sp)
.set	macro
.set	reorder

$L404:
lw	$2,1828($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1010
move	$5,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L408
lw	$25,%call16(ff_eac3_decode_transform_coeffs_aht_ch)($28)
.set	macro
.set	reorder

move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_decode_transform_coeffs_aht_ch
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

lw	$28,48($sp)
$L408:
lw	$3,-20584($fp)
lw	$11,-20556($fp)
slt	$2,$3,$11
.set	noreorder
.set	nomacro
beq	$2,$0,$L407
sll	$5,$3,1
.set	macro
.set	reorder

sll	$2,$3,3
li	$9,45056			# 0xb000
subu	$2,$2,$5
addiu	$4,$3,14672
addiu	$2,$2,464
addu	$5,$3,$9
addu	$2,$16,$2
sll	$4,$4,2
addu	$8,$11,$9
sll	$2,$2,2
addu	$5,$17,$5
addu	$2,$17,$2
addu	$4,$17,$4
addu	$8,$17,$8
$L409:
lb	$7,0($5)
addiu	$4,$4,4
lw	$6,0($2)
addiu	$5,$5,1
addiu	$2,$2,24
sra	$6,$6,$7
.set	noreorder
.set	nomacro
bne	$5,$8,$L409
sw	$6,-4($4)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1040
.option	pic2
lw	$2,256($17)
.set	macro
.set	reorder

$L1010:
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch
.option	pic2
move	$6,$19
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$11,-20556($fp)
lw	$3,-20584($fp)
$L407:
lw	$2,256($17)
$L1040:
.set	noreorder
.set	nomacro
blez	$2,$L405
addu	$2,$17,$2
.set	macro
.set	reorder

lw	$31,-20672($fp)
addiu	$23,$17,384
move	$22,$17
addiu	$24,$31,1
sw	$2,168($sp)
$L411:
lbu	$15,260($22)
.set	noreorder
.set	nomacro
blez	$31,$L414
addu	$15,$15,$3
.set	macro
.set	reorder

addiu	$20,$3,14672
lw	$12,164($sp)
addiu	$6,$15,14672
sll	$20,$20,2
addiu	$9,$15,15184
addiu	$25,$20,2048
sll	$6,$6,2
sll	$9,$9,2
addu	$20,$17,$20
addu	$6,$17,$6
addu	$25,$17,$25
addu	$9,$17,$9
li	$10,1			# 0x1
move	$13,$23
slt	$14,$3,$15
$L413:
lw	$2,0($12)
beq	$2,$0,$L417
lw	$7,0($13)
.set	noreorder
.set	nomacro
beq	$14,$0,$L419
sll	$7,$7,5
.set	macro
.set	reorder

sll	$8,$10,10
move	$4,$20
$L418:
lw	$2,0($4)
addu	$5,$4,$8
addiu	$4,$4,4
sll	$2,$2,4
mult	$2,$7
mfhi	$3
.set	noreorder
.set	nomacro
bne	$4,$6,$L418
sw	$3,0($5)
.set	macro
.set	reorder

$L419:
li	$2,2			# 0x2
beq	$10,$2,$L1011
$L417:
addiu	$10,$10,1
$L1041:
addiu	$12,$12,4
.set	noreorder
.set	nomacro
bne	$10,$24,$L413
addiu	$13,$13,72
.set	macro
.set	reorder

$L414:
lw	$10,168($sp)
addiu	$22,$22,1
addiu	$23,$23,4
.set	noreorder
.set	nomacro
bne	$22,$10,$L411
move	$3,$15
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L421
.option	pic2
li	$20,1			# 0x1
.set	macro
.set	reorder

$L1011:
lw	$2,-200($23)
beq	$2,$0,$L417
.set	noreorder
.set	nomacro
beq	$14,$0,$L417
move	$2,$25
.set	macro
.set	reorder

$L420:
lw	$3,0($2)
addiu	$2,$2,4
subu	$3,$0,$3
.set	noreorder
.set	nomacro
bne	$2,$9,$L420
sw	$3,-4($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1041
.option	pic2
addiu	$10,$10,1
.set	macro
.set	reorder

$L906:
sub.d	$f0,$f0,$f20
trunc.w.d $f3,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L494
.option	pic2
mfc1	$3,$f3
.set	macro
.set	reorder

$L1008:
move	$4,$17
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch
.option	pic2
move	$6,$19
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L398
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L658:
addiu	$10,$10,4
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$10,$9,$L569
swc1	$f0,-4($10)
.set	macro
.set	reorder

li	$22,1			# 0x1
$L928:
li	$2,65536			# 0x10000
$L1042:
li	$3,6			# 0x6
addu	$2,$17,$2
lw	$2,-20668($2)
.set	noreorder
.set	nomacro
bne	$2,$3,$L1083
li	$2,65536			# 0x10000
.set	macro
.set	reorder

li	$20,65536			# 0x10000
$L1077:
addu	$2,$17,$20
lw	$2,-11132($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L574
addiu	$21,$20,16192
.set	macro
.set	reorder

addiu	$2,$20,1344
addiu	$4,$20,2368
addu	$21,$17,$21
addu	$2,$17,$2
addu	$4,$17,$4
move	$22,$21
move	$3,$21
$L575:
lw	$5,0($2)
addiu	$3,$3,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$4,$L575
sw	$5,-4($3)
.set	macro
.set	reorder

li	$20,54492			# 0xd4dc
lw	$25,%call16(aac_imdct_half_c)($28)
move	$6,$21
addiu	$5,$20,26724
addu	$23,$17,$20
addu	$5,$17,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
move	$4,$23
.set	macro
.set	reorder

addiu	$5,$20,18532
addiu	$4,$20,28260
lw	$28,48($sp)
addiu	$7,$20,26212
addiu	$6,$20,19044
addiu	$3,$20,26720
addiu	$20,$20,27232
addu	$5,$17,$5
addu	$4,$17,$4
addu	$7,$17,$7
addu	$6,$17,$6
addu	$3,$17,$3
addu	$20,$17,$20
move	$2,$0
li	$10,2			# 0x2
li	$9,31			# 0x1f
li	$12,508			# 0x1fc
li	$11,512			# 0x200
$L576:
addu	$8,$6,$2
lw	$13,0($3)
lw	$8,-512($8)
#APP
# 778 "ac3dec.c" 1
.word	0b01110001000011010000100001100110	#S32MUL XR1,XR2,$8,$13
# 0 "" 2
#NO_APP
addu	$14,$7,$2
lw	$14,-512($14)
#APP
# 779 "ac3dec.c" 1
.word	0b01110001000011100001000011100110	#S32MUL XR3,XR4,$8,$14
# 0 "" 2
#NO_APP
lw	$8,0($20)
#APP
# 780 "ac3dec.c" 1
.word	0b01110001000011101000100001000100	#S32MSUB XR1,XR2,$8,$14
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110001000011011001000011000000	#S32MADD XR3,XR4,$8,$13
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110001010010011100100001100110	#S32EXTRV XR1,XR2,$10,$9
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110001010010011101000011100110	#S32EXTRV XR3,XR4,$10,$9
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addiu	$8,$2,-512
addu	$8,$4,$8
#APP
# 786 "ac3dec.c" 1
.word	0b01110001000000000000000111010001	#S32STD XR7,$8,0
# 0 "" 2
#NO_APP
subu	$8,$12,$2
addu	$8,$4,$8
#APP
# 787 "ac3dec.c" 1
.word	0b01110001000000000000001000010001	#S32STD XR8,$8,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$3,$3,-4
.set	noreorder
.set	nomacro
bne	$2,$11,$L576
addiu	$20,$20,-4
.set	macro
.set	reorder

li	$3,65536			# 0x10000
addiu	$2,$3,1348
addiu	$3,$3,2372
addu	$2,$17,$2
addu	$3,$17,$3
$L577:
lw	$4,0($2)
addiu	$22,$22,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$3,$L577
sw	$4,-4($22)
.set	macro
.set	reorder

lw	$25,%call16(aac_imdct_half_c)($28)
move	$4,$23
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
move	$6,$21
.set	macro
.set	reorder

li	$2,65536			# 0x10000
lw	$28,48($sp)
addu	$2,$17,$2
lw	$10,44($17)
lw	$22,-20596($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L607
.option	pic2
lw	$fp,-20668($2)
.set	macro
.set	reorder

$L1004:
lui	$6,%hi($LC31)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC31)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L996:
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_eac3_decode_transform_coeffs_aht_ch
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L462
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L995:
move	$5,$0
.option	pic0
.set	noreorder
.set	nomacro
jal	ac3_decode_transform_coeffs_ch
.option	pic2
move	$4,$17
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$20,-20556($fp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L461
.option	pic2
lw	$7,-20584($fp)
.set	macro
.set	reorder

$L992:
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$8,$3,$L547
addiu	$2,$2,2368
.set	macro
.set	reorder

sll	$7,$8,3
addu	$4,$17,$2
addu	$7,$6,$7
move	$10,$9
$L548:
.set	noreorder
.set	nomacro
blez	$8,$L655
mtc1	$0,$f0
.set	macro
.set	reorder

move	$2,$6
move	$3,$10
mtc1	$0,$f1
$L551:
lw	$5,0($3)
addiu	$2,$2,8
cvt.s.w	$f4,$f1
lwc1	$f3,-8($2)
addiu	$3,$3,1024
cvt.s.w	$f2,$f0
lwc1	$f1,-4($2)
mtc1	$5,$f13
cvt.s.w	$f0,$f13
mul.s	$f3,$f0,$f3
mul.s	$f0,$f0,$f1
add.s	$f1,$f3,$f4
add.s	$f0,$f0,$f2
trunc.w.s $f1,$f1
.set	noreorder
.set	nomacro
bne	$2,$7,$L551
trunc.w.s $f0,$f0
.set	macro
.set	reorder

swc1	$f1,0($9)
addiu	$9,$9,4
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$9,$4,$L548
swc1	$f0,1020($9)
.set	macro
.set	reorder

li	$2,65536			# 0x10000
$L1043:
addu	$3,$17,$2
lw	$4,-20604($3)
.set	noreorder
.set	nomacro
bne	$4,$0,$L621
addiu	$2,$2,7488
.set	macro
.set	reorder

lw	$8,-20672($3)
li	$4,1			# 0x1
addu	$2,$17,$2
sw	$4,-20604($3)
$L619:
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$8,$3,$L560
li	$3,65536			# 0x10000
.set	macro
.set	reorder

sll	$5,$8,3
addiu	$7,$3,7488
addiu	$3,$3,8000
addu	$7,$17,$7
addu	$10,$17,$3
.set	noreorder
.set	nomacro
blez	$8,$L657
addu	$5,$6,$5
.set	macro
.set	reorder

mtc1	$0,$f0
$L1084:
move	$3,$6
move	$4,$2
mtc1	$0,$f1
$L564:
lw	$9,0($4)
addiu	$3,$3,8
cvt.s.w	$f4,$f1
lwc1	$f3,-8($3)
addiu	$4,$4,1024
cvt.s.w	$f2,$f0
lwc1	$f1,-4($3)
mtc1	$9,$f5
cvt.s.w	$f0,$f5
mul.s	$f3,$f0,$f3
mul.s	$f0,$f0,$f1
add.s	$f1,$f3,$f4
add.s	$f0,$f0,$f2
trunc.w.s $f1,$f1
.set	noreorder
.set	nomacro
bne	$3,$5,$L564
trunc.w.s $f0,$f0
.set	macro
.set	reorder

swc1	$f1,0($7)
addiu	$7,$7,4
addiu	$2,$2,4
.set	noreorder
.set	nomacro
beq	$7,$10,$L928
swc1	$f0,1020($7)
.set	macro
.set	reorder

$L1013:
.set	noreorder
.set	nomacro
bgtz	$8,$L1084
mtc1	$0,$f0
.set	macro
.set	reorder

$L657:
mtc1	$0,$f1
addiu	$7,$7,4
addiu	$2,$2,4
mtc1	$0,$f0
swc1	$f1,-4($7)
.set	noreorder
.set	nomacro
bne	$7,$10,$L1013
swc1	$f0,1020($7)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1042
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L655:
mtc1	$0,$f1
addiu	$9,$9,4
addiu	$10,$10,4
swc1	$f1,-4($9)
.set	noreorder
.set	nomacro
bne	$9,$4,$L548
swc1	$f0,1020($9)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1043
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L166:
lui	$6,%hi($LC7)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC7)
.set	macro
.set	reorder

lw	$28,48($sp)
$L169:
li	$4,65536			# 0x10000
addu	$4,$17,$4
lw	$22,-20596($4)
.set	noreorder
.set	nomacro
beq	$22,$0,$L600
lw	$10,288($sp)
.set	macro
.set	reorder

$L917:
lw	$2,-20600($4)
li	$3,-9			# 0xfffffffffffffff7
.option	pic0
.set	noreorder
.set	nomacro
j	$L180
.option	pic2
and	$2,$2,$3
.set	macro
.set	reorder

$L600:
lw	$2,-20668($4)
lw	$22,68($10)
slt	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L917
sw	$22,-20596($4)
.set	macro
.set	reorder

li	$5,2			# 0x2
li	$2,1			# 0x1
xori	$3,$22,0x1
movn	$2,$5,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L180
.option	pic2
sw	$2,-20600($4)
.set	macro
.set	reorder

$L165:
lui	$6,%hi($LC8)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC8)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L169
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L164:
lui	$6,%hi($LC9)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC9)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L169
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L162:
lw	$3,24($17)
li	$2,1			# 0x1
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
beq	$3,$2,$L170
li	$5,16			# 0x10
.set	macro
.set	reorder

lw	$2,28($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L171
lw	$4,288($sp)
.set	macro
.set	reorder

$L170:
lui	$6,%hi($LC10)
lw	$4,288($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L899
.option	pic2
lw	$2,32($17)
.set	macro
.set	reorder

$L161:
lui	$6,%hi($LC12)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC12)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L169
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L998:
lw	$2,-18488($2)
.set	noreorder
.set	nomacro
bne	$6,$2,$L364
li	$2,65536			# 0x10000
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1092
.option	pic2
addu	$2,$17,$2
.set	macro
.set	reorder

$L167:
lui	$6,%hi($LC6)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC6)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L899
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L171:
lui	$6,%hi($LC11)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L169
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L514:
li	$4,65536			# 0x10000
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$4,$4,11584
li	$6,1024			# 0x400
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$17,$4
.set	macro
.set	reorder

lw	$28,48($sp)
$L512:
li	$4,65536			# 0x10000
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$4,$4,10560
li	$6,1024			# 0x400
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$17,$4
.set	macro
.set	reorder

lw	$28,48($sp)
$L510:
li	$2,65536			# 0x10000
addiu	$4,$2,8512
addiu	$2,$2,9536
addu	$4,$17,$4
addu	$2,$17,$2
addiu	$9,$4,1024
move	$3,$4
$L516:
lwl	$8,3($3)
lwl	$7,7($3)
lwl	$6,11($3)
lwl	$5,15($3)
lwr	$8,0($3)
lwr	$7,4($3)
lwr	$6,8($3)
lwr	$5,12($3)
addiu	$3,$3,16
swl	$8,3($2)
swr	$8,0($2)
swl	$7,7($2)
swr	$7,4($2)
swl	$6,11($2)
swr	$6,8($2)
swl	$5,15($2)
swr	$5,12($2)
.set	noreorder
.set	nomacro
bne	$3,$9,$L516
addiu	$2,$2,16
.set	macro
.set	reorder

lw	$25,%call16(memset)($28)
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
li	$6,1024			# 0x400
.set	macro
.set	reorder

li	$2,6			# 0x6
.set	noreorder
.set	nomacro
bne	$fp,$2,$L1014
lw	$28,48($sp)
.set	macro
.set	reorder

li	$20,65536			# 0x10000
$L1078:
addu	$2,$17,$20
lw	$2,-11132($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L520
addiu	$21,$20,16192
.set	macro
.set	reorder

addiu	$2,$20,1344
addiu	$4,$20,2368
addu	$21,$17,$21
addu	$2,$17,$2
addu	$4,$17,$4
move	$20,$21
move	$3,$21
$L521:
lw	$5,0($2)
addiu	$3,$3,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$4,$L521
sw	$5,-4($3)
.set	macro
.set	reorder

li	$23,54492			# 0xd4dc
lw	$25,%call16(aac_imdct_half_c)($28)
move	$6,$21
addiu	$5,$23,26724
addu	$22,$17,$23
addu	$5,$17,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
move	$4,$22
.set	macro
.set	reorder

addiu	$5,$23,18532
addiu	$7,$23,28260
lw	$28,48($sp)
addiu	$9,$23,26212
addiu	$8,$23,19044
addiu	$6,$23,26720
addiu	$2,$23,27232
addu	$5,$17,$5
addu	$7,$17,$7
addu	$9,$17,$9
addu	$8,$17,$8
addu	$6,$17,$6
addu	$2,$17,$2
move	$3,$0
li	$10,2			# 0x2
li	$4,31			# 0x1f
li	$15,508			# 0x1fc
li	$14,512			# 0x200
$L522:
addu	$11,$8,$3
lw	$12,0($6)
lw	$11,-512($11)
#APP
# 778 "ac3dec.c" 1
.word	0b01110001011011000000100001100110	#S32MUL XR1,XR2,$11,$12
# 0 "" 2
#NO_APP
addu	$13,$9,$3
lw	$13,-512($13)
#APP
# 779 "ac3dec.c" 1
.word	0b01110001011011010001000011100110	#S32MUL XR3,XR4,$11,$13
# 0 "" 2
#NO_APP
lw	$11,0($2)
#APP
# 780 "ac3dec.c" 1
.word	0b01110001011011011000100001000100	#S32MSUB XR1,XR2,$11,$13
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110001011011001001000011000000	#S32MADD XR3,XR4,$11,$12
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110001010001001100100001100110	#S32EXTRV XR1,XR2,$10,$4
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110001010001001101000011100110	#S32EXTRV XR3,XR4,$10,$4
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addiu	$11,$3,-512
addu	$11,$7,$11
#APP
# 786 "ac3dec.c" 1
.word	0b01110001011000000000000111010001	#S32STD XR7,$11,0
# 0 "" 2
#NO_APP
subu	$11,$15,$3
addu	$11,$7,$11
#APP
# 787 "ac3dec.c" 1
.word	0b01110001011000000000001000010001	#S32STD XR8,$11,0
# 0 "" 2
#NO_APP
addiu	$3,$3,4
addiu	$6,$6,-4
.set	noreorder
.set	nomacro
bne	$3,$14,$L522
addiu	$2,$2,-4
.set	macro
.set	reorder

li	$3,65536			# 0x10000
addiu	$2,$3,1348
addiu	$3,$3,2372
addu	$2,$17,$2
addu	$3,$17,$3
$L523:
lw	$4,0($2)
addiu	$20,$20,4
addiu	$2,$2,8
.set	noreorder
.set	nomacro
bne	$2,$3,$L523
sw	$4,-4($20)
.set	macro
.set	reorder

lw	$25,%call16(aac_imdct_half_c)($28)
move	$4,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
move	$6,$21
.set	macro
.set	reorder

li	$2,65536			# 0x10000
lw	$10,152($sp)
addu	$2,$17,$2
lw	$28,48($sp)
lw	$22,-20596($2)
.set	noreorder
.set	nomacro
beq	$10,$0,$L534
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L1002:
addu	$4,$17,$2
addiu	$9,$2,16704
li	$7,44876			# 0xaf4c
li	$3,2			# 0x2
lw	$8,-20672($4)
addu	$9,$17,$9
.set	noreorder
.set	nomacro
beq	$22,$3,$L1015
addu	$7,$17,$7
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$22,$3,$L542
addiu	$2,$2,17728
.set	macro
.set	reorder

lw	$fp,-20668($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L607
.option	pic2
lw	$10,44($17)
.set	macro
.set	reorder

$L508:
li	$2,65536			# 0x10000
addiu	$3,$2,7488
addiu	$2,$2,8512
addu	$3,$17,$3
addu	$2,$17,$2
addiu	$8,$3,1024
$L515:
lwl	$7,3($3)
lwl	$6,7($3)
lwl	$5,11($3)
lwl	$4,15($3)
lwr	$7,0($3)
lwr	$6,4($3)
lwr	$5,8($3)
lwr	$4,12($3)
addiu	$3,$3,16
swl	$7,3($2)
swr	$7,0($2)
swl	$6,7($2)
swr	$6,4($2)
swl	$5,11($2)
swr	$5,8($2)
swl	$4,15($2)
swr	$4,12($2)
.set	noreorder
.set	nomacro
bne	$3,$8,$L515
addiu	$2,$2,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1045
.option	pic2
li	$2,6			# 0x6
.set	macro
.set	reorder

$L513:
li	$4,65536			# 0x10000
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$4,$4,10560
li	$6,1024			# 0x400
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$17,$4
.set	macro
.set	reorder

lw	$28,48($sp)
$L511:
li	$4,65536			# 0x10000
lw	$25,%call16(memset)($28)
move	$5,$0
addiu	$4,$4,9536
li	$6,1024			# 0x400
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
addu	$4,$17,$4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L507
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L1000:
lui	$6,%hi($LC18)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC18)
sw	$3,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$7,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L999:
lui	$6,%hi($LC17)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$8,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L542:
sll	$6,$8,3
addu	$5,$17,$2
addu	$6,$7,$6
move	$10,$9
$L543:
.set	noreorder
.set	nomacro
blez	$8,$L654
mtc1	$0,$f0
.set	macro
.set	reorder

move	$2,$7
move	$3,$10
$L544:
lw	$4,0($3)
addiu	$2,$2,8
cvt.s.w	$f0,$f0
lwc1	$f1,-8($2)
addiu	$3,$3,1024
mtc1	$4,$f11
cvt.s.w	$f2,$f11
mul.s	$f1,$f2,$f1
add.s	$f0,$f1,$f0
.set	noreorder
.set	nomacro
bne	$2,$6,$L544
trunc.w.s $f0,$f0
.set	macro
.set	reorder

swc1	$f0,0($9)
addiu	$9,$9,4
.set	noreorder
.set	nomacro
bne	$9,$5,$L543
addiu	$10,$10,4
.set	macro
.set	reorder

$L926:
li	$2,65536			# 0x10000
$L1046:
lw	$10,44($17)
addu	$2,$17,$2
.option	pic0
.set	noreorder
.set	nomacro
j	$L607
.option	pic2
lw	$fp,-20668($2)
.set	macro
.set	reorder

$L654:
addiu	$9,$9,4
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$9,$5,$L543
swc1	$f0,-4($9)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1046
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L1015:
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$8,$3,$L536
addiu	$2,$2,17728
.set	macro
.set	reorder

sll	$6,$8,3
move	$10,$9
addu	$4,$17,$2
.set	noreorder
.set	nomacro
blez	$8,$L653
addu	$6,$7,$6
.set	macro
.set	reorder

mtc1	$0,$f0
$L1085:
move	$2,$7
move	$3,$10
mtc1	$0,$f1
$L540:
lw	$5,0($3)
addiu	$2,$2,8
cvt.s.w	$f4,$f1
lwc1	$f3,-8($2)
addiu	$3,$3,1024
cvt.s.w	$f2,$f0
lwc1	$f1,-4($2)
mtc1	$5,$f5
cvt.s.w	$f0,$f5
mul.s	$f3,$f0,$f3
mul.s	$f0,$f0,$f1
add.s	$f1,$f3,$f4
add.s	$f0,$f0,$f2
trunc.w.s $f1,$f1
.set	noreorder
.set	nomacro
bne	$2,$6,$L540
trunc.w.s $f0,$f0
.set	macro
.set	reorder

swc1	$f1,0($9)
addiu	$9,$9,4
addiu	$10,$10,4
.set	noreorder
.set	nomacro
beq	$9,$4,$L926
swc1	$f0,1020($9)
.set	macro
.set	reorder

$L1017:
.set	noreorder
.set	nomacro
bgtz	$8,$L1085
mtc1	$0,$f0
.set	macro
.set	reorder

$L653:
mtc1	$0,$f1
addiu	$9,$9,4
addiu	$10,$10,4
mtc1	$0,$f0
swc1	$f1,-4($9)
.set	noreorder
.set	nomacro
bne	$9,$4,$L1017
swc1	$f0,1020($9)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1046
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L536:
li	$4,4849664			# 0x4a0000
addu	$2,$17,$2
li	$6,8			# 0x8
li	$5,31			# 0x1f
move	$3,$9
ori	$4,$4,0xfb0d
$L538:
lw	$7,1024($3)
#APP
# 855 "ac3dec.c" 1
.word	0b01110000111001000000100001100110	#S32MUL XR1,XR2,$7,$4
# 0 "" 2
#NO_APP
lw	$7,1028($3)
#APP
# 856 "ac3dec.c" 1
.word	0b01110000111001000001000011100110	#S32MUL XR3,XR4,$7,$4
# 0 "" 2
# 857 "ac3dec.c" 1
.word	0b01110000110001011100100001100110	#S32EXTRV XR1,XR2,$6,$5
# 0 "" 2
# 858 "ac3dec.c" 1
.word	0b01110000110001011101000011100110	#S32EXTRV XR3,XR4,$6,$5
# 0 "" 2
# 859 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 860 "ac3dec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
addiu	$7,$3,4
#APP
# 861 "ac3dec.c" 1
.word	0b01110000111000000000000110010001	#S32STD XR6,$7,0
# 0 "" 2
#NO_APP
addiu	$3,$3,8
bne	$3,$2,$L538
.option	pic0
.set	noreorder
.set	nomacro
j	$L1047
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L574:
addiu	$5,$20,15680
lw	$25,%call16(aac_imdct_half_c)($28)
addiu	$6,$20,1344
li	$4,54428			# 0xd49c
addu	$5,$17,$5
addu	$4,$17,$4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
addu	$6,$17,$6
.set	macro
.set	reorder

addiu	$3,$20,7488
addiu	$6,$20,17216
lw	$28,48($sp)
addiu	$8,$20,15168
addiu	$7,$20,8000
addiu	$5,$20,15676
addiu	$2,$20,16188
addu	$3,$17,$3
addu	$6,$17,$6
addu	$8,$17,$8
addu	$7,$17,$7
addu	$5,$17,$5
addu	$2,$17,$2
move	$4,$0
li	$11,2			# 0x2
li	$10,31			# 0x1f
li	$13,508			# 0x1fc
li	$12,512			# 0x200
$L578:
addu	$9,$7,$4
lw	$14,0($5)
lw	$9,-512($9)
#APP
# 778 "ac3dec.c" 1
.word	0b01110001001011100000100001100110	#S32MUL XR1,XR2,$9,$14
# 0 "" 2
#NO_APP
addu	$15,$8,$4
lw	$15,-512($15)
#APP
# 779 "ac3dec.c" 1
.word	0b01110001001011110001000011100110	#S32MUL XR3,XR4,$9,$15
# 0 "" 2
#NO_APP
lw	$9,0($2)
#APP
# 780 "ac3dec.c" 1
.word	0b01110001001011111000100001000100	#S32MSUB XR1,XR2,$9,$15
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110001001011101001000011000000	#S32MADD XR3,XR4,$9,$14
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110001011010101100100001100110	#S32EXTRV XR1,XR2,$11,$10
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110001011010101101000011100110	#S32EXTRV XR3,XR4,$11,$10
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addiu	$9,$4,-512
addu	$9,$6,$9
#APP
# 786 "ac3dec.c" 1
.word	0b01110001001000000000000111010001	#S32STD XR7,$9,0
# 0 "" 2
#NO_APP
subu	$9,$13,$4
addu	$9,$6,$9
#APP
# 787 "ac3dec.c" 1
.word	0b01110001001000000000001000010001	#S32STD XR8,$9,0
# 0 "" 2
#NO_APP
addiu	$4,$4,4
addiu	$5,$5,-4
.set	noreorder
.set	nomacro
bne	$4,$12,$L578
addiu	$2,$2,-4
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addiu	$2,$2,16192
addu	$2,$17,$2
addiu	$8,$2,512
$L579:
lwl	$7,3($2)
lwl	$6,7($2)
lwl	$5,11($2)
lwl	$4,15($2)
lwr	$7,0($2)
lwr	$6,4($2)
lwr	$5,8($2)
lwr	$4,12($2)
addiu	$2,$2,16
swl	$7,3($3)
swr	$7,0($3)
swl	$6,7($3)
swr	$6,4($3)
swl	$5,11($3)
swr	$5,8($3)
swl	$4,15($3)
swr	$4,12($3)
.set	noreorder
.set	nomacro
bne	$2,$8,$L579
addiu	$3,$3,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1047
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L520:
addiu	$5,$20,15680
lw	$25,%call16(aac_imdct_half_c)($28)
addiu	$6,$20,1344
li	$4,54428			# 0xd49c
addu	$5,$17,$5
addu	$4,$17,$4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_imdct_half_c
1:	jalr	$25
addu	$6,$17,$6
.set	macro
.set	reorder

addiu	$3,$20,7488
addiu	$5,$20,17216
lw	$28,48($sp)
addiu	$7,$20,15168
addiu	$6,$20,8000
addiu	$4,$20,15676
addiu	$20,$20,16188
addu	$3,$17,$3
addu	$5,$17,$5
addu	$7,$17,$7
addu	$6,$17,$6
addu	$4,$17,$4
addu	$20,$17,$20
move	$2,$0
li	$10,2			# 0x2
li	$9,31			# 0x1f
li	$12,508			# 0x1fc
li	$11,512			# 0x200
$L524:
addu	$8,$6,$2
lw	$13,0($4)
lw	$8,-512($8)
#APP
# 778 "ac3dec.c" 1
.word	0b01110001000011010000100001100110	#S32MUL XR1,XR2,$8,$13
# 0 "" 2
#NO_APP
addu	$14,$7,$2
lw	$14,-512($14)
#APP
# 779 "ac3dec.c" 1
.word	0b01110001000011100001000011100110	#S32MUL XR3,XR4,$8,$14
# 0 "" 2
#NO_APP
lw	$8,0($20)
#APP
# 780 "ac3dec.c" 1
.word	0b01110001000011101000100001000100	#S32MSUB XR1,XR2,$8,$14
# 0 "" 2
# 781 "ac3dec.c" 1
.word	0b01110001000011011001000011000000	#S32MADD XR3,XR4,$8,$13
# 0 "" 2
# 782 "ac3dec.c" 1
.word	0b01110001010010011100100001100110	#S32EXTRV XR1,XR2,$10,$9
# 0 "" 2
# 783 "ac3dec.c" 1
.word	0b01110001010010011101000011100110	#S32EXTRV XR3,XR4,$10,$9
# 0 "" 2
# 784 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 785 "ac3dec.c" 1
.word	0b01110010101000011001010111110011	#D32SAR XR7,XR5,XR6,XR8,10
# 0 "" 2
#NO_APP
addiu	$8,$2,-512
addu	$8,$5,$8
#APP
# 786 "ac3dec.c" 1
.word	0b01110001000000000000000111010001	#S32STD XR7,$8,0
# 0 "" 2
#NO_APP
subu	$8,$12,$2
addu	$8,$5,$8
#APP
# 787 "ac3dec.c" 1
.word	0b01110001000000000000001000010001	#S32STD XR8,$8,0
# 0 "" 2
#NO_APP
addiu	$2,$2,4
addiu	$4,$4,-4
.set	noreorder
.set	nomacro
bne	$2,$11,$L524
addiu	$20,$20,-4
.set	macro
.set	reorder

li	$2,65536			# 0x10000
addiu	$2,$2,16192
addu	$2,$17,$2
addiu	$8,$2,512
$L525:
lwl	$7,3($2)
lwl	$6,7($2)
lwl	$5,11($2)
lwl	$4,15($2)
lwr	$7,0($2)
lwr	$6,4($2)
lwr	$5,8($2)
lwr	$4,12($2)
addiu	$2,$2,16
swl	$7,3($3)
swr	$7,0($3)
swl	$6,7($3)
swr	$6,4($3)
swl	$5,11($3)
swr	$5,8($3)
swl	$4,15($3)
swr	$4,12($3)
.set	noreorder
.set	nomacro
bne	$2,$8,$L525
addiu	$3,$3,16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1048
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L972:
lui	$6,%hi($LC32)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC32)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L264:
lw	$3,156($sp)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$3,$2,$L1072
li	$10,65536			# 0x10000
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1034
.option	pic2
lw	$2,72($17)
.set	macro
.set	reorder

$L965:
lw	$10,172($sp)
sh	$3,4($20)
li	$3,50528256			# 0x3030000
lw	$2,0($10)
addiu	$3,$3,771
sw	$3,0($20)
li	$3,3			# 0x3
sb	$3,6($20)
$L612:
.set	noreorder
.set	nomacro
bne	$2,$0,$L250
lw	$9,156($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$21,$L252
li	$2,1			# 0x1
.set	macro
.set	reorder

sw	$0,156($17)
.set	noreorder
.set	nomacro
beq	$21,$2,$L252
sw	$2,288($17)
.set	macro
.set	reorder

li	$3,2			# 0x2
sw	$0,160($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L252
sw	$2,292($17)
.set	macro
.set	reorder

li	$3,3			# 0x3
sw	$0,164($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L252
sw	$2,296($17)
.set	macro
.set	reorder

li	$3,4			# 0x4
sw	$0,168($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L252
sw	$2,300($17)
.set	macro
.set	reorder

li	$3,5			# 0x5
sw	$0,172($17)
.set	noreorder
.set	nomacro
beq	$21,$3,$L252
sw	$2,304($17)
.set	macro
.set	reorder

sw	$0,176($17)
sw	$2,308($17)
$L252:
li	$2,65536			# 0x10000
lw	$8,172($sp)
addu	$2,$17,$2
sw	$6,-18484($2)
sw	$0,180($17)
lw	$8,0($8)
move	$3,$8
.option	pic0
.set	noreorder
.set	nomacro
j	$L262
.option	pic2
sw	$8,152($sp)
.set	macro
.set	reorder

$L250:
slt	$2,$9,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L1018
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

beq	$6,$0,$L254
lw	$2,12($17)
lw	$4,4($17)
srl	$5,$2,3
andi	$6,$2,0x7
addu	$5,$4,$5
addiu	$2,$2,1
lbu	$3,0($5)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1019
sw	$2,12($17)
.set	macro
.set	reorder

lw	$5,48($17)
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$5,$3,$L1020
li	$3,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$21,$L1086
lw	$10,156($sp)
.set	macro
.set	reorder

$L1021:
srl	$5,$2,3
andi	$7,$2,0x7
addu	$5,$4,$5
move	$6,$2
addiu	$2,$2,1
lbu	$3,0($5)
sw	$2,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,156($17)
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$21,$3,$L256
srl	$5,$2,3
.set	macro
.set	reorder

andi	$7,$2,0x7
addu	$5,$4,$5
addiu	$2,$6,2
lbu	$3,0($5)
sw	$2,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,160($17)
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$21,$3,$L256
srl	$5,$2,3
.set	macro
.set	reorder

andi	$7,$2,0x7
addu	$5,$4,$5
addiu	$2,$6,3
lbu	$3,0($5)
sw	$2,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,164($17)
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$21,$3,$L256
srl	$5,$2,3
.set	macro
.set	reorder

andi	$7,$2,0x7
addu	$5,$4,$5
addiu	$2,$6,4
lbu	$3,0($5)
sw	$2,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,168($17)
li	$3,4			# 0x4
.set	noreorder
.set	nomacro
beq	$21,$3,$L256
srl	$5,$2,3
.set	macro
.set	reorder

andi	$7,$2,0x7
addu	$5,$4,$5
addiu	$2,$6,5
lbu	$3,0($5)
sw	$2,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,172($17)
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$21,$3,$L256
srl	$5,$2,3
.set	macro
.set	reorder

andi	$7,$2,0x7
addu	$5,$4,$5
addiu	$2,$6,6
lbu	$3,0($5)
sw	$2,12($17)
sll	$3,$3,$7
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,176($17)
li	$3,6			# 0x6
.set	noreorder
.set	nomacro
beq	$21,$3,$L256
addiu	$6,$6,7
.set	macro
.set	reorder

sw	$6,12($17)
$L254:
lw	$4,4($17)
.set	noreorder
.set	nomacro
bgtz	$21,$L1021
lw	$2,12($17)
.set	macro
.set	reorder

$L256:
lw	$10,156($sp)
$L1086:
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$10,$3,$L1022
srl	$3,$2,3
.set	macro
.set	reorder

$L258:
srl	$3,$2,3
li	$6,-4			# 0xfffffffffffffffc
addu	$3,$4,$3
and	$5,$3,$6
#APP
# 115 "ac3dec.c" 1
.word	0b01110000101100000000000010010000	#S32LDDR XR2,$5,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000101100000000010001010000	#S32LDDR XR1,$5,4
# 0 "" 2
#NO_APP
andi	$3,$3,0x3
sll	$3,$3,3
andi	$5,$2,0x7
addu	$3,$3,$5
li	$8,4			# 0x4
#APP
# 117 "ac3dec.c" 1
.word	0b01110000011010001100010010100110	#S32EXTRV XR2,XR1,$3,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000001110000000010101110	#S32M2I XR2, $7
# 0 "" 2
#NO_APP
addiu	$5,$2,4
lw	$3,816($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L259
sw	$5,12($17)
.set	macro
.set	reorder

lw	$3,836($17)
li	$2,715784192			# 0x2aaa0000
ori	$2,$2,0xaaab
addiu	$3,$3,-37
mult	$3,$2
sra	$3,$3,31
mfhi	$2
sra	$2,$2,1
subu	$3,$2,$3
slt	$2,$7,$3
.set	noreorder
.set	nomacro
beq	$2,$0,$L1023
sll	$2,$7,2
.set	macro
.set	reorder

$L1087:
lw	$6,72($17)
sll	$4,$3,4
sll	$5,$7,4
sll	$8,$3,2
subu	$5,$5,$2
subu	$8,$4,$8
lw	$4,188($sp)
li	$9,65536			# 0x10000
addiu	$5,$5,37
addiu	$8,$8,37
addu	$9,$17,$9
addiu	$10,$17,256
sw	$5,-20584($9)
addiu	$2,$17,260
sw	$8,-20556($9)
move	$5,$16
sw	$3,16($sp)
sw	$10,24($sp)
sw	$2,28($sp)
lw	$3,%got(ff_eac3_default_cpl_band_struct)($28)
.option	pic0
.set	noreorder
.set	nomacro
jal	decode_band_structure.constprop.8
.option	pic2
sw	$3,20($sp)
.set	macro
.set	reorder

lw	$3,172($sp)
lw	$28,48($sp)
lw	$3,0($3)
.option	pic0
.set	noreorder
.set	nomacro
j	$L262
.option	pic2
sw	$3,152($sp)
.set	macro
.set	reorder

$L259:
srl	$3,$5,3
addu	$3,$4,$3
and	$4,$3,$6
#APP
# 115 "ac3dec.c" 1
.word	0b01110000100100000000000010010000	#S32LDDR XR2,$4,0
# 0 "" 2
# 116 "ac3dec.c" 1
.word	0b01110000100100000000010001010000	#S32LDDR XR1,$4,4
# 0 "" 2
#NO_APP
andi	$3,$3,0x3
sll	$3,$3,3
andi	$5,$5,0x7
addu	$3,$3,$5
#APP
# 117 "ac3dec.c" 1
.word	0b01110000011010001100010010100110	#S32EXTRV XR2,XR1,$3,$8
# 0 "" 2
# 118 "ac3dec.c" 1
.word	0b01110000000000110000000010101110	#S32M2I XR2, $3
# 0 "" 2
#NO_APP
addiu	$2,$2,8
addiu	$3,$3,3
sw	$2,12($17)
slt	$2,$7,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L1087
sll	$2,$7,2
.set	macro
.set	reorder

$L1023:
lui	$6,%hi($LC25)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC25)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L1022:
andi	$5,$2,0x7
addu	$3,$4,$3
addiu	$2,$2,1
lbu	$3,0($3)
sll	$3,$3,$5
andi	$3,$3,0x00ff
srl	$3,$3,7
.option	pic0
.set	noreorder
.set	nomacro
j	$L258
.option	pic2
sw	$3,180($17)
.set	macro
.set	reorder

$L1020:
sw	$3,156($17)
.option	pic0
.set	noreorder
.set	nomacro
j	$L256
.option	pic2
sw	$3,160($17)
.set	macro
.set	reorder

$L1019:
lui	$5,%hi($LC24)
lw	$25,%call16(av_log_missing_feature)($28)
lw	$4,0($17)
li	$6,1			# 0x1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log_missing_feature
1:	jalr	$25
addiu	$5,$5,%lo($LC24)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L1018:
lui	$6,%hi($LC23)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC23)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L640:
sw	$0,160($sp)
$L202:
lw	$4,84($17)
.set	noreorder
.set	nomacro
bne	$4,$0,$L1088
srl	$4,$2,3
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1049
.option	pic2
lw	$10,156($sp)
.set	macro
.set	reorder

$L958:
slt	$3,$8,$4
.set	noreorder
.set	nomacro
beq	$3,$0,$L927
li	$5,1			# 0x1
.set	macro
.set	reorder

lw	$4,64($17)
li	$6,2			# 0x2
lw	$3,68($17)
xori	$7,$8,0x1
movz	$6,$5,$7
lui	$5,%hi(center_levels)
addiu	$5,$5,%lo(center_levels)
addu	$5,$4,$5
lui	$4,%hi(surround_levels)
sll	$9,$6,1
addiu	$4,$4,%lo(surround_levels)
lbu	$7,0($5)
lui	$5,%hi(gain_levels)
addu	$4,$3,$4
lw	$3,%got(ff_ac3_channel_layout_tab)($28)
addiu	$5,$5,%lo(gain_levels)
sll	$7,$7,2
addu	$3,$3,$9
lbu	$4,0($4)
li	$9,65536			# 0x10000
addu	$7,$7,$5
addu	$9,$17,$9
lhu	$10,0($3)
sll	$4,$4,2
lwc1	$f0,0($7)
sw	$8,-20596($9)
sw	$6,-20600($9)
addu	$4,$4,$5
lw	$3,-20672($9)
lw	$9,288($sp)
sw	$10,52($17)
lwc1	$f2,0($4)
sw	$8,68($9)
sw	$10,848($9)
.set	noreorder
.set	nomacro
blez	$3,$L184
sw	$0,852($9)
.set	macro
.set	reorder

sll	$7,$2,3
sll	$4,$2,1
addu	$4,$4,$7
lui	$7,%hi(ac3_default_coeffs)
addiu	$7,$7,%lo(ac3_default_coeffs)
addu	$4,$4,$7
li	$7,65536			# 0x10000
lbu	$9,0($4)
addu	$7,$17,$7
lbu	$8,1($4)
sll	$9,$9,2
sll	$8,$8,2
addu	$9,$9,$5
addu	$8,$8,$5
lwc1	$f3,0($9)
lwc1	$f1,0($8)
li	$8,1			# 0x1
swc1	$f3,-20660($7)
.set	noreorder
.set	nomacro
beq	$3,$8,$L184
swc1	$f1,-20656($7)
.set	macro
.set	reorder

lbu	$9,2($4)
lbu	$8,3($4)
sll	$9,$9,2
sll	$8,$8,2
addu	$9,$9,$5
addu	$8,$8,$5
lwc1	$f3,0($9)
lwc1	$f1,0($8)
li	$8,2			# 0x2
swc1	$f3,-20652($7)
.set	noreorder
.set	nomacro
beq	$3,$8,$L184
swc1	$f1,-20648($7)
.set	macro
.set	reorder

lbu	$9,4($4)
lbu	$8,5($4)
sll	$9,$9,2
sll	$8,$8,2
addu	$9,$9,$5
addu	$8,$8,$5
lwc1	$f3,0($9)
lwc1	$f1,0($8)
li	$8,3			# 0x3
swc1	$f3,-20644($7)
.set	noreorder
.set	nomacro
beq	$3,$8,$L184
swc1	$f1,-20640($7)
.set	macro
.set	reorder

lbu	$9,6($4)
lbu	$8,7($4)
sll	$9,$9,2
sll	$8,$8,2
addu	$9,$9,$5
addu	$8,$8,$5
lwc1	$f3,0($9)
lwc1	$f1,0($8)
li	$8,4			# 0x4
swc1	$f3,-20636($7)
.set	noreorder
.set	nomacro
beq	$3,$8,$L184
swc1	$f1,-20632($7)
.set	macro
.set	reorder

lbu	$8,8($4)
lbu	$4,9($4)
sll	$8,$8,2
sll	$4,$4,2
addu	$8,$8,$5
addu	$5,$4,$5
lwc1	$f3,0($8)
lwc1	$f1,0($5)
swc1	$f3,-20628($7)
swc1	$f1,-20624($7)
$L184:
slt	$4,$2,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L183
andi	$4,$2,0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L1089
addiu	$4,$2,-4
.set	macro
.set	reorder

li	$4,65536			# 0x10000
addu	$4,$17,$4
swc1	$f0,-20648($4)
swc1	$f0,-20652($4)
$L183:
addiu	$4,$2,-4
$L1089:
sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L1024
addiu	$5,$2,-2
.set	macro
.set	reorder

$L185:
addiu	$4,$2,-6
sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L1025
addiu	$4,$2,-3
.set	macro
.set	reorder

$L186:
.set	noreorder
.set	nomacro
blez	$3,$L187
li	$2,65536			# 0x10000
.set	macro
.set	reorder

li	$4,1			# 0x1
addu	$2,$17,$2
lwc1	$f0,-20660($2)
.set	noreorder
.set	nomacro
beq	$3,$4,$L188
lwc1	$f1,-20656($2)
.set	macro
.set	reorder

lwc1	$f2,-20652($2)
li	$4,2			# 0x2
lwc1	$f3,-20648($2)
add.s	$f2,$f0,$f2
.set	noreorder
.set	nomacro
beq	$3,$4,$L189
add.s	$f1,$f1,$f3
.set	macro
.set	reorder

lwc1	$f4,-20644($2)
li	$4,3			# 0x3
lwc1	$f3,-20640($2)
add.s	$f2,$f2,$f4
.set	noreorder
.set	nomacro
beq	$3,$4,$L189
add.s	$f1,$f1,$f3
.set	macro
.set	reorder

lwc1	$f4,-20636($2)
li	$4,4			# 0x4
lwc1	$f3,-20632($2)
add.s	$f2,$f2,$f4
.set	noreorder
.set	nomacro
beq	$3,$4,$L189
add.s	$f1,$f1,$f3
.set	macro
.set	reorder

lwc1	$f4,-20628($2)
li	$4,5			# 0x5
lwc1	$f3,-20624($2)
add.s	$f2,$f2,$f4
.set	noreorder
.set	nomacro
beq	$3,$4,$L189
add.s	$f1,$f1,$f3
.set	macro
.set	reorder

lwc1	$f4,-20620($2)
li	$4,6			# 0x6
lwc1	$f3,-20616($2)
add.s	$f2,$f2,$f4
.set	noreorder
.set	nomacro
beq	$3,$4,$L189
add.s	$f1,$f1,$f3
.set	macro
.set	reorder

lwc1	$f4,-20612($2)
lwc1	$f3,-20608($2)
add.s	$f2,$f2,$f4
add.s	$f1,$f1,$f3
$L189:
lui	$2,%hi($LC0)
li	$4,2			# 0x2
lwc1	$f3,%lo($LC0)($2)
li	$2,65536			# 0x10000
addu	$2,$17,$2
lwc1	$f5,-20656($2)
lwc1	$f6,-20652($2)
div.s	$f2,$f3,$f2
lwc1	$f4,-20648($2)
div.s	$f3,$f3,$f1
mul.s	$f0,$f2,$f0
mul.s	$f6,$f2,$f6
mul.s	$f5,$f3,$f5
mul.s	$f4,$f3,$f4
swc1	$f0,-20660($2)
swc1	$f6,-20652($2)
swc1	$f5,-20656($2)
.set	noreorder
.set	nomacro
beq	$3,$4,$L190
swc1	$f4,-20648($2)
.set	macro
.set	reorder

lwc1	$f1,-20644($2)
li	$4,3			# 0x3
lwc1	$f0,-20640($2)
mul.s	$f1,$f2,$f1
mul.s	$f0,$f3,$f0
swc1	$f1,-20644($2)
.set	noreorder
.set	nomacro
beq	$3,$4,$L190
swc1	$f0,-20640($2)
.set	macro
.set	reorder

lwc1	$f1,-20636($2)
li	$4,4			# 0x4
lwc1	$f0,-20632($2)
mul.s	$f1,$f2,$f1
mul.s	$f0,$f3,$f0
swc1	$f1,-20636($2)
.set	noreorder
.set	nomacro
beq	$3,$4,$L190
swc1	$f0,-20632($2)
.set	macro
.set	reorder

lwc1	$f1,-20628($2)
li	$4,5			# 0x5
lwc1	$f0,-20624($2)
mul.s	$f1,$f2,$f1
mul.s	$f0,$f3,$f0
swc1	$f1,-20628($2)
.set	noreorder
.set	nomacro
beq	$3,$4,$L190
swc1	$f0,-20624($2)
.set	macro
.set	reorder

lwc1	$f1,-20620($2)
li	$4,6			# 0x6
lwc1	$f0,-20616($2)
mul.s	$f1,$f2,$f1
mul.s	$f0,$f3,$f0
swc1	$f1,-20620($2)
.set	noreorder
.set	nomacro
beq	$3,$4,$L190
swc1	$f0,-20616($2)
.set	macro
.set	reorder

lwc1	$f0,-20612($2)
lwc1	$f1,-20608($2)
mul.s	$f2,$f2,$f0
mul.s	$f1,$f3,$f1
swc1	$f2,-20612($2)
swc1	$f1,-20608($2)
$L190:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$6,$2,$L1026
lui	$4,%hi($LC15)
.set	macro
.set	reorder

$L626:
li	$3,65536			# 0x10000
addu	$3,$17,$3
lw	$2,-20600($3)
lw	$22,-20596($3)
li	$3,-9			# 0xfffffffffffffff7
.option	pic0
.set	noreorder
.set	nomacro
j	$L180
.option	pic2
and	$2,$2,$3
.set	macro
.set	reorder

$L187:
li	$3,65536			# 0x10000
.option	pic0
.set	noreorder
.set	nomacro
j	$L918
.option	pic2
addu	$3,$17,$3
.set	macro
.set	reorder

$L188:
div.s	$f0,$f0,$f0
div.s	$f1,$f1,$f1
swc1	$f0,-20660($2)
.set	noreorder
.set	nomacro
bne	$6,$3,$L626
swc1	$f1,-20656($2)
.set	macro
.set	reorder

add.s	$f1,$f1,$f0
lui	$3,%hi($LC15)
ldc1	$f2,%lo($LC15)($3)
cvt.d.s	$f0,$f1
mul.d	$f0,$f0,$f2
cvt.s.d	$f0,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L626
.option	pic2
swc1	$f0,-20660($2)
.set	macro
.set	reorder

$L1026:
li	$2,65536			# 0x10000
addu	$2,$17,$2
ldc1	$f4,%lo($LC15)($4)
slt	$4,$3,3
lwc1	$f3,-20652($2)
lwc1	$f2,-20660($2)
lwc1	$f1,-20656($2)
lwc1	$f0,-20648($2)
add.s	$f2,$f1,$f2
add.s	$f0,$f0,$f3
cvt.d.s	$f2,$f2
cvt.d.s	$f0,$f0
mul.d	$f2,$f2,$f4
mul.d	$f0,$f0,$f4
cvt.s.d	$f2,$f2
cvt.s.d	$f0,$f0
swc1	$f2,-20660($2)
.set	noreorder
.set	nomacro
bne	$4,$0,$L626
swc1	$f0,-20652($2)
.set	macro
.set	reorder

lwc1	$f1,-20644($2)
li	$4,3			# 0x3
lwc1	$f0,-20640($2)
add.s	$f0,$f0,$f1
cvt.d.s	$f0,$f0
mul.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
.set	noreorder
.set	nomacro
beq	$3,$4,$L626
swc1	$f0,-20644($2)
.set	macro
.set	reorder

lwc1	$f1,-20636($2)
li	$4,4			# 0x4
lwc1	$f0,-20632($2)
add.s	$f0,$f0,$f1
cvt.d.s	$f0,$f0
mul.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
.set	noreorder
.set	nomacro
beq	$3,$4,$L626
swc1	$f0,-20636($2)
.set	macro
.set	reorder

lwc1	$f1,-20628($2)
li	$4,5			# 0x5
lwc1	$f0,-20624($2)
add.s	$f0,$f0,$f1
cvt.d.s	$f0,$f0
mul.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
.set	noreorder
.set	nomacro
beq	$3,$4,$L626
swc1	$f0,-20628($2)
.set	macro
.set	reorder

lwc1	$f1,-20620($2)
li	$4,6			# 0x6
lwc1	$f0,-20616($2)
add.s	$f0,$f0,$f1
cvt.d.s	$f0,$f0
mul.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
.set	noreorder
.set	nomacro
beq	$3,$4,$L626
swc1	$f0,-20620($2)
.set	macro
.set	reorder

lwc1	$f2,-20612($2)
lwc1	$f1,-20608($2)
add.s	$f2,$f1,$f2
cvt.d.s	$f2,$f2
mul.d	$f0,$f2,$f4
cvt.s.d	$f0,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L626
.option	pic2
swc1	$f0,-20612($2)
.set	macro
.set	reorder

$L1025:
addiu	$2,$2,5605
sll	$4,$4,3
sll	$2,$2,3
addu	$4,$17,$4
li	$5,65536			# 0x10000
addu	$2,$17,$2
addu	$4,$5,$4
swc1	$f2,-20656($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L186
.option	pic2
swc1	$f2,4($2)
.set	macro
.set	reorder

$L1024:
cvt.d.s	$f0,$f2
li	$7,65536			# 0x10000
sll	$5,$5,3
addiu	$4,$2,5607
addu	$5,$17,$5
sll	$4,$4,3
addu	$5,$7,$5
lui	$7,%hi($LC15)
addu	$4,$17,$4
ldc1	$f4,%lo($LC15)($7)
mul.d	$f0,$f0,$f4
cvt.s.d	$f0,$f0
swc1	$f0,-20656($5)
.option	pic0
.set	noreorder
.set	nomacro
j	$L185
.option	pic2
swc1	$f0,4($4)
.set	macro
.set	reorder

$L957:
lui	$6,%hi($LC14)
lw	$25,%call16(av_log)($28)
lw	$4,288($sp)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

li	$8,-6			# 0xfffffffffffffffa
lw	$28,48($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L169
.option	pic2
sw	$8,180($sp)
.set	macro
.set	reorder

$L982:
addu	$2,$17,$2
lw	$3,-20552($2)
lw	$4,-20548($2)
lw	$12,-20528($2)
slt	$10,$3,$4
movz	$3,$4,$10
.set	noreorder
.set	nomacro
blez	$12,$L450
move	$10,$3
.set	macro
.set	reorder

li	$7,45012			# 0xafd4
lw	$9,%got(ff_ac3_rematrix_band_tab)($28)
li	$8,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L488
.option	pic2
addu	$7,$17,$7
.set	macro
.set	reorder

$L1027:
move	$14,$8
$L486:
slt	$14,$14,$12
$L1050:
addiu	$7,$7,4
addiu	$8,$8,1
.set	noreorder
.set	nomacro
beq	$14,$0,$L450
addiu	$9,$9,1
.set	macro
.set	reorder

$L488:
lw	$2,0($7)
beq	$2,$0,$L1027
lbu	$4,1($9)
lbu	$2,0($9)
slt	$3,$10,$4
movn	$4,$10,$3
move	$3,$4
slt	$4,$2,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L486
move	$14,$8
.set	macro
.set	reorder

addiu	$2,$2,14928
addiu	$11,$3,14928
sll	$2,$2,2
sll	$11,$11,2
addu	$2,$17,$2
addu	$11,$17,$11
$L487:
lw	$3,0($2)
addiu	$2,$2,4
lw	$4,1020($2)
addu	$13,$3,$4
subu	$3,$3,$4
sw	$13,-4($2)
.set	noreorder
.set	nomacro
bne	$2,$11,$L487
sw	$3,1020($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1050
.option	pic2
slt	$14,$14,$12
.set	macro
.set	reorder

$L986:
lui	$6,%hi($LC26)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC26)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L985:
srl	$4,$5,3
lw	$9,172($sp)
andi	$5,$5,0x7
addu	$3,$3,$4
addiu	$4,$2,2
li	$2,771			# 0x303
lbu	$3,0($3)
move	$6,$0
sw	$4,12($17)
sh	$2,4($20)
sll	$2,$3,$5
li	$3,50528256			# 0x3030000
andi	$2,$2,0x00ff
addiu	$3,$3,771
srl	$2,$2,7
sw	$3,0($20)
li	$3,3			# 0x3
sw	$2,0($9)
.option	pic0
.set	noreorder
.set	nomacro
j	$L612
.option	pic2
sb	$3,6($20)
.set	macro
.set	reorder

$L974:
lui	$6,%hi($LC33)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC33)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lw	$28,48($sp)
.set	macro
.set	reorder

$L994:
sw	$3,-20604($4)
move	$3,$22
$L924:
.option	pic0
.set	noreorder
.set	nomacro
j	$L621
.option	pic2
move	$22,$3
.set	macro
.set	reorder

$L993:
sll	$7,$8,3
addu	$5,$17,$2
addu	$7,$6,$7
move	$10,$9
$L555:
.set	noreorder
.set	nomacro
blez	$8,$L656
mtc1	$0,$f0
.set	macro
.set	reorder

move	$2,$6
move	$3,$10
$L556:
lw	$4,0($3)
addiu	$2,$2,8
cvt.s.w	$f0,$f0
lwc1	$f1,-8($2)
addiu	$3,$3,1024
mtc1	$4,$f3
cvt.s.w	$f2,$f3
mul.s	$f1,$f2,$f1
add.s	$f0,$f1,$f0
.set	noreorder
.set	nomacro
bne	$2,$7,$L556
trunc.w.s $f0,$f0
.set	macro
.set	reorder

swc1	$f0,0($9)
addiu	$9,$9,4
.set	noreorder
.set	nomacro
bne	$9,$5,$L555
addiu	$10,$10,4
.set	macro
.set	reorder

li	$2,65536			# 0x10000
$L1051:
addu	$3,$17,$2
lw	$4,-20604($3)
.set	noreorder
.set	nomacro
bne	$4,$0,$L621
addiu	$2,$2,7488
.set	macro
.set	reorder

lw	$8,-20672($3)
li	$4,1			# 0x1
addu	$2,$17,$2
sw	$4,-20604($3)
li	$3,65536			# 0x10000
$L1052:
sll	$5,$8,3
addiu	$10,$3,7488
addiu	$3,$3,8000
addu	$10,$17,$10
addu	$9,$17,$3
addu	$5,$6,$5
$L569:
.set	noreorder
.set	nomacro
blez	$8,$L658
mtc1	$0,$f0
.set	macro
.set	reorder

move	$3,$6
move	$4,$2
$L570:
lw	$7,0($4)
addiu	$3,$3,8
cvt.s.w	$f0,$f0
lwc1	$f1,-8($3)
addiu	$4,$4,1024
mtc1	$7,$f11
cvt.s.w	$f2,$f11
mul.s	$f1,$f2,$f1
add.s	$f0,$f1,$f0
.set	noreorder
.set	nomacro
bne	$3,$5,$L570
trunc.w.s $f0,$f0
.set	macro
.set	reorder

swc1	$f0,0($10)
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$10,$9,$L569
addiu	$2,$2,4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L928
.option	pic2
li	$22,1			# 0x1
.set	macro
.set	reorder

$L656:
addiu	$9,$9,4
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$9,$5,$L555
swc1	$f0,-4($9)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L1051
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L547:
li	$4,4849664			# 0x4a0000
addu	$2,$17,$2
li	$7,8			# 0x8
li	$5,31			# 0x1f
move	$3,$9
ori	$4,$4,0xfb0d
$L549:
lw	$8,1024($3)
#APP
# 855 "ac3dec.c" 1
.word	0b01110001000001000000100001100110	#S32MUL XR1,XR2,$8,$4
# 0 "" 2
#NO_APP
lw	$8,1028($3)
#APP
# 856 "ac3dec.c" 1
.word	0b01110001000001000001000011100110	#S32MUL XR3,XR4,$8,$4
# 0 "" 2
# 857 "ac3dec.c" 1
.word	0b01110000111001011100100001100110	#S32EXTRV XR1,XR2,$7,$5
# 0 "" 2
# 858 "ac3dec.c" 1
.word	0b01110000111001011101000011100110	#S32EXTRV XR3,XR4,$7,$5
# 0 "" 2
# 859 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 860 "ac3dec.c" 1
.word	0b01110000011000000000000101010001	#S32STD XR5,$3,0
# 0 "" 2
#NO_APP
addiu	$8,$3,4
#APP
# 861 "ac3dec.c" 1
.word	0b01110001000000000000000110010001	#S32STD XR6,$8,0
# 0 "" 2
#NO_APP
addiu	$3,$3,8
bne	$3,$2,$L549
li	$2,65536			# 0x10000
addu	$4,$17,$2
lw	$5,-20604($4)
.set	noreorder
.set	nomacro
bne	$5,$0,$L924
lw	$3,-20596($4)
.set	macro
.set	reorder

li	$5,1			# 0x1
lw	$8,-20672($4)
addiu	$2,$2,7488
sw	$5,-20604($4)
li	$4,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$4,$L619
addu	$2,$17,$2
.set	macro
.set	reorder

bne	$3,$5,$L924
.option	pic0
.set	noreorder
.set	nomacro
j	$L1052
.option	pic2
li	$3,65536			# 0x10000
.set	macro
.set	reorder

$L560:
li	$4,4849664			# 0x4a0000
addiu	$2,$3,7488
addiu	$3,$3,8000
addu	$2,$17,$2
addu	$3,$17,$3
li	$6,8			# 0x8
li	$5,31			# 0x1f
ori	$4,$4,0xfb0d
$L562:
lw	$7,1024($2)
#APP
# 855 "ac3dec.c" 1
.word	0b01110000111001000000100001100110	#S32MUL XR1,XR2,$7,$4
# 0 "" 2
#NO_APP
lw	$7,1028($2)
#APP
# 856 "ac3dec.c" 1
.word	0b01110000111001000001000011100110	#S32MUL XR3,XR4,$7,$4
# 0 "" 2
# 857 "ac3dec.c" 1
.word	0b01110000110001011100100001100110	#S32EXTRV XR1,XR2,$6,$5
# 0 "" 2
# 858 "ac3dec.c" 1
.word	0b01110000110001011101000011100110	#S32EXTRV XR3,XR4,$6,$5
# 0 "" 2
# 859 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 860 "ac3dec.c" 1
.word	0b01110000010000000000000101010001	#S32STD XR5,$2,0
# 0 "" 2
#NO_APP
addiu	$7,$2,4
#APP
# 861 "ac3dec.c" 1
.word	0b01110000111000000000000110010001	#S32STD XR6,$7,0
# 0 "" 2
#NO_APP
addiu	$2,$2,8
bne	$2,$3,$L562
li	$2,65536			# 0x10000
addu	$2,$17,$2
lw	$22,-20596($2)
.option	pic0
.set	noreorder
.set	nomacro
j	$L568
.option	pic2
lw	$fp,-20668($2)
.set	macro
.set	reorder

.end	ac3_decode_frame
.size	ac3_decode_frame, .-ac3_decode_frame
.align	2
.globl	ff_ac3_downmix_c
.set	nomips16
.set	nomicromips
.ent	ff_ac3_downmix_c
.type	ff_ac3_downmix_c, @function
ff_ac3_downmix_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$6,$2,$L1109
lw	$8,16($sp)
.set	macro
.set	reorder

li	$2,1			# 0x1
beq	$6,$2,$L1110
$L1118:
j	$31
$L1110:
blez	$8,$L1117
sll	$6,$7,3
move	$9,$4
addu	$6,$5,$6
.set	noreorder
.set	nomacro
blez	$7,$L1105
move	$10,$0
.set	macro
.set	reorder

mtc1	$0,$f0
$L1115:
move	$3,$5
move	$4,$9
$L1102:
lw	$2,0($4)
addiu	$3,$3,8
cvt.s.w	$f0,$f0
lwc1	$f2,-8($3)
addiu	$4,$4,1024
mtc1	$2,$f3
cvt.s.w	$f1,$f3
mul.s	$f1,$f1,$f2
add.s	$f0,$f1,$f0
.set	noreorder
.set	nomacro
bne	$3,$6,$L1102
trunc.w.s $f0,$f0
.set	macro
.set	reorder

addiu	$10,$10,1
swc1	$f0,0($9)
.set	noreorder
.set	nomacro
beq	$10,$8,$L1118
addiu	$9,$9,4
.set	macro
.set	reorder

$L1112:
.set	noreorder
.set	nomacro
bgtz	$7,$L1115
mtc1	$0,$f0
.set	macro
.set	reorder

$L1105:
mtc1	$0,$f0
addiu	$10,$10,1
addiu	$9,$9,4
.set	noreorder
.set	nomacro
bne	$10,$8,$L1112
swc1	$f0,-4($9)
.set	macro
.set	reorder

.option	pic0
j	$L1117
.option	pic2
$L1109:
li	$2,5			# 0x5
beq	$7,$2,$L1095
blez	$8,$L1117
sll	$9,$7,3
move	$6,$4
addu	$9,$5,$9
.set	noreorder
.set	nomacro
blez	$7,$L1104
move	$10,$0
.set	macro
.set	reorder

mtc1	$0,$f0
$L1116:
move	$3,$5
move	$4,$6
mtc1	$0,$f1
$L1099:
lw	$2,0($4)
addiu	$3,$3,8
cvt.s.w	$f2,$f1
lwc1	$f4,-4($3)
addiu	$4,$4,1024
cvt.s.w	$f3,$f0
lwc1	$f1,-8($3)
mtc1	$2,$f5
cvt.s.w	$f0,$f5
mul.s	$f1,$f0,$f1
mul.s	$f0,$f0,$f4
add.s	$f1,$f1,$f2
add.s	$f0,$f0,$f3
trunc.w.s $f1,$f1
.set	noreorder
.set	nomacro
bne	$3,$9,$L1099
trunc.w.s $f0,$f0
.set	macro
.set	reorder

addiu	$10,$10,1
swc1	$f1,0($6)
addiu	$6,$6,4
.set	noreorder
.set	nomacro
beq	$10,$8,$L1118
swc1	$f0,1020($6)
.set	macro
.set	reorder

$L1114:
.set	noreorder
.set	nomacro
bgtz	$7,$L1116
mtc1	$0,$f0
.set	macro
.set	reorder

$L1104:
mtc1	$0,$f0
addiu	$10,$10,1
addiu	$6,$6,4
mtc1	$0,$f1
swc1	$f0,1020($6)
.set	noreorder
.set	nomacro
bne	$10,$8,$L1114
swc1	$f1,-4($6)
.set	macro
.set	reorder

.option	pic0
j	$L1117
.option	pic2
$L1095:
blez	$8,$L1117
addiu	$8,$8,-1
move	$2,$4
srl	$8,$8,1
li	$4,4849664			# 0x4a0000
addiu	$8,$8,1
move	$3,$0
li	$6,8			# 0x8
sll	$8,$8,1
li	$5,31			# 0x1f
ori	$4,$4,0xfb0d
$L1098:
lw	$7,1024($2)
#APP
# 855 "ac3dec.c" 1
.word	0b01110000111001000000100001100110	#S32MUL XR1,XR2,$7,$4
# 0 "" 2
#NO_APP
lw	$7,1028($2)
#APP
# 856 "ac3dec.c" 1
.word	0b01110000111001000001000011100110	#S32MUL XR3,XR4,$7,$4
# 0 "" 2
# 857 "ac3dec.c" 1
.word	0b01110000110001011100100001100110	#S32EXTRV XR1,XR2,$6,$5
# 0 "" 2
# 858 "ac3dec.c" 1
.word	0b01110000110001011101000011100110	#S32EXTRV XR3,XR4,$6,$5
# 0 "" 2
# 859 "ac3dec.c" 1
.word	0b01110000010110001100010101110000	#D32SLL XR5,XR1,XR3,XR6,1
# 0 "" 2
# 860 "ac3dec.c" 1
.word	0b01110000010000000000000101010001	#S32STD XR5,$2,0
# 0 "" 2
#NO_APP
addiu	$7,$2,4
#APP
# 861 "ac3dec.c" 1
.word	0b01110000111000000000000110010001	#S32STD XR6,$7,0
# 0 "" 2
#NO_APP
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$8,$L1098
addiu	$2,$2,8
.set	macro
.set	reorder

$L1117:
j	$31
.end	ff_ac3_downmix_c
.size	ff_ac3_downmix_c, .-ff_ac3_downmix_c
.globl	eac3_decoder
.section	.rodata.str1.4
.align	2
$LC40:
.ascii	"eac3\000"
.align	2
$LC41:
.ascii	"ATSC A/52B (AC-3, E-AC-3)\000"
.section	.data.rel.local,"aw",@progbits
.align	2
.type	eac3_decoder, @object
.size	eac3_decoder, 72
eac3_decoder:
.word	$LC40
.word	1
.word	86059
.word	89408
.word	ac3_decode_init
.space	4
.word	ac3_decode_end
.word	ac3_decode_frame
.space	20
.word	$LC41
.space	16
.globl	ac3_decoder
.section	.rodata.str1.4
.align	2
$LC42:
.ascii	"ac3\000"
.align	2
$LC43:
.ascii	"ATSC A/52A (AC-3)\000"
.section	.data.rel.local
.align	2
.type	ac3_decoder, @object
.size	ac3_decoder, 72
ac3_decoder:
.word	$LC42
.word	1
.word	86019
.word	89408
.word	ac3_decode_init
.space	4
.word	ac3_decode_end
.word	ac3_decode_frame
.space	20
.word	$LC43
.space	16
.rdata
.align	2
.type	ac3_default_coeffs, @object
.size	ac3_default_coeffs, 80
ac3_default_coeffs:
.byte	2
.byte	7
.byte	7
.byte	2
.space	6
.byte	4
.byte	4
.space	8
.byte	2
.byte	7
.byte	7
.byte	2
.space	6
.byte	2
.byte	7
.byte	5
.byte	5
.byte	7
.byte	2
.space	4
.byte	2
.byte	7
.byte	7
.byte	2
.byte	6
.byte	6
.space	4
.byte	2
.byte	7
.byte	5
.byte	5
.byte	7
.byte	2
.byte	8
.byte	8
.space	2
.byte	2
.byte	7
.byte	7
.byte	2
.byte	6
.byte	7
.byte	7
.byte	6
.space	2
.byte	2
.byte	7
.byte	5
.byte	5
.byte	7
.byte	2
.byte	6
.byte	7
.byte	7
.byte	6
.align	2
.type	surround_levels, @object
.size	surround_levels, 4
surround_levels:
.byte	4
.byte	6
.byte	7
.byte	6
.align	2
.type	center_levels, @object
.size	center_levels, 4
center_levels:
.byte	4
.byte	5
.byte	6
.byte	5
.align	2
.type	gain_levels, @object
.size	gain_levels, 36
gain_levels:
.word	1068827891
.word	1066940400
.word	1065353216
.word	1062683901
.word	1060439283
.word	1058551792
.word	1056964608
.word	0
.word	1052050675
.local	dynamic_range_tab
.comm	dynamic_range_tab,1024,4
.align	2
.type	quantization_tab, @object
.size	quantization_tab, 16
quantization_tab:
.byte	0
.byte	3
.byte	5
.byte	7
.byte	11
.byte	15
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	14
.byte	16
.local	b5_mantissas
.comm	b5_mantissas,64,4
.local	b4_mantissas
.comm	b4_mantissas,1024,4
.local	b3_mantissas
.comm	b3_mantissas,32,4
.local	b2_mantissas
.comm	b2_mantissas,1536,4
.local	b1_mantissas
.comm	b1_mantissas,384,4
.local	ungroup_3_in_7_bits_tab
.comm	ungroup_3_in_7_bits_tab,384,4
.section	.rodata.cst4,"aM",@progbits,4
.align	2
$LC0:
.word	1065353216
.align	2
$LC1:
.word	1136689152
.align	2
$LC2:
.word	1191181824
.align	2
$LC3:
.word	1073741824
.section	.rodata.cst8,"aM",@progbits,8
.align	3
$LC4:
.word	0
.word	1072693248
.section	.rodata.cst4
.align	2
$LC5:
.word	1084227584
.section	.rodata.cst8
.align	3
$LC15:
.word	1719614413
.word	1072079006
.align	3
$LC16:
.word	0
.word	-1074790400
.align	3
$LC19:
.word	0
.word	1106247680
.section	.rodata.cst4
.align	2
$LC20:
.word	1023410176
.align	2
$LC21:
.word	1077936128
.align	2
$LC22:
.word	872415232
.align	2
$LC36:
.word	880803840
.align	2
$LC37:
.word	1082130432
.section	.rodata.cst8
.align	3
$LC38:
.word	0
.word	1071644672
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
