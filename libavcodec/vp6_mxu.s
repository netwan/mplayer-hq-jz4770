.file	1 "vp6.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_default_models_init
.type	vp6_default_models_init, @function
vp6_default_models_init:
.frame	$sp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
.mask	0x00010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$2,6560($4)
li	$7,-94			# 0xffffffffffffffa2
addiu	$28,$28,%lo(__gnu_local_gp)
addiu	$sp,$sp,-8
li	$6,-128			# 0xffffffffffffff80
lw	$5,%got(vp56_def_mb_types_stats)($28)
addiu	$3,$2,1812
sw	$16,4($sp)
sb	$7,130($2)
li	$7,-92			# 0xffffffffffffffa4
addiu	$10,$5,48
sb	$6,128($2)
sb	$6,129($2)
sb	$7,131($2)
$L2:
lwl	$9,3($5)
lwl	$8,7($5)
lwl	$7,11($5)
lwl	$6,15($5)
lwr	$9,0($5)
lwr	$8,4($5)
lwr	$7,8($5)
lwr	$6,12($5)
addiu	$5,$5,16
swl	$9,3($3)
swr	$9,0($3)
swl	$8,7($3)
swr	$8,4($3)
swl	$7,11($3)
swr	$7,8($3)
swl	$6,15($3)
swr	$6,12($3)
bne	$5,$10,$L2
addiu	$3,$3,16

lwl	$11,3($5)
lui	$6,%hi(vp6_def_fdv_vector_model)
lwl	$10,7($5)
lui	$14,%hi(vp6_def_pdv_vector_model)
lwl	$9,11($5)
addiu	$12,$6,%lo(vp6_def_fdv_vector_model)
lw	$16,%lo(vp6_def_fdv_vector_model)($6)
addiu	$8,$14,%lo(vp6_def_pdv_vector_model)
lwr	$11,0($5)
lui	$13,%hi(vp6_def_runv_coeff_model)
lwr	$10,4($5)
lui	$6,%hi(vp6_def_coeff_reorder)
lwr	$9,8($5)
addiu	$7,$13,%lo(vp6_def_runv_coeff_model)
lw	$25,4($12)
addiu	$6,$6,%lo(vp6_def_coeff_reorder)
lw	$24,8($12)
move	$5,$2
swl	$11,3($3)
addiu	$15,$6,64
swr	$11,0($3)
swl	$10,7($3)
swr	$10,4($3)
swl	$9,11($3)
swr	$9,8($3)
swl	$16,153($2)
lw	$3,12($12)
swr	$16,150($2)
swl	$25,157($2)
swr	$25,154($2)
swl	$24,161($2)
swr	$24,158($2)
swl	$3,165($2)
swr	$3,162($2)
lw	$10,%lo(vp6_def_pdv_vector_model)($14)
lw	$9,4($8)
lw	$3,8($8)
swl	$10,139($2)
swr	$10,136($2)
swl	$9,143($2)
swr	$9,140($2)
swl	$3,147($2)
swr	$3,144($2)
lw	$12,%lo(vp6_def_runv_coeff_model)($13)
lbu	$14,12($8)
lbu	$13,13($8)
lw	$11,4($7)
lw	$10,8($7)
lw	$3,12($7)
lw	$9,16($7)
sb	$14,148($2)
sb	$13,149($2)
swl	$12,1487($2)
swr	$12,1484($2)
swl	$11,1491($2)
swr	$11,1488($2)
swl	$10,1495($2)
swr	$10,1492($2)
swl	$3,1499($2)
swr	$3,1496($2)
lw	$8,20($7)
lw	$3,24($7)
swl	$9,1503($2)
swr	$9,1500($2)
swl	$8,1507($2)
swr	$8,1504($2)
swl	$3,1511($2)
swr	$3,1508($2)
$L3:
lw	$8,0($6)
addiu	$6,$6,16
lw	$7,-12($6)
lw	$3,-8($6)
lw	$2,-4($6)
swl	$8,3($5)
swr	$8,0($5)
swl	$7,7($5)
swr	$7,4($5)
swl	$3,11($5)
swr	$3,8($5)
swl	$2,15($5)
swr	$2,12($5)
bne	$6,$15,$L3
addiu	$5,$5,16

lw	$2,6560($4)
li	$8,1			# 0x1
move	$6,$0
li	$7,64			# 0x40
li	$9,16			# 0x10
sb	$0,64($2)
$L4:
.option	pic0
j	$L6
.option	pic2
li	$2,1			# 0x1

$L5:
addiu	$2,$2,1
beq	$2,$7,$L13
nop

$L6:
lw	$3,6560($4)
addu	$5,$3,$2
lbu	$5,0($5)
bne	$5,$6,$L5
addu	$3,$3,$8

sb	$2,64($3)
addiu	$2,$2,1
bne	$2,$7,$L6
addiu	$8,$8,1

$L13:
addiu	$6,$6,1
bne	$6,$9,$L4
lw	$16,4($sp)

j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	vp6_default_models_init
.size	vp6_default_models_init, .-vp6_default_models_init
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_huff_cmp
.type	vp6_huff_cmp, @function
vp6_huff_cmp:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,4($5)
lw	$3,4($4)
lh	$5,0($5)
lh	$4,0($4)
subu	$3,$3,$2
subu	$4,$5,$4
sll	$2,$3,4
j	$31
addu	$2,$4,$2

.set	macro
.set	reorder
.end	vp6_huff_cmp
.size	vp6_huff_cmp, .-vp6_huff_cmp
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_parse_vector_adjustment
.type	vp6_parse_vector_adjustment, @function
vp6_parse_vector_adjustment:
.frame	$sp,32,$31		# vars= 0, regs= 8/0, args= 0, gp= 0
.mask	0x00ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,6208($4)
addiu	$sp,$sp,-32
lui	$28,%hi(__gnu_local_gp)
lw	$12,6560($4)
sw	$23,28($sp)
slt	$2,$2,2
sw	$22,24($sp)
sw	$21,20($sp)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
sh	$0,0($5)
bne	$2,$0,$L48
sh	$0,2($5)

$L16:
lui	$9,%hi(prob_order.6679+7)
lw	$7,%got(ff_vp56_norm_shift)($28)
addiu	$11,$12,130
addiu	$14,$12,153
addiu	$8,$12,136
li	$13,1			# 0x1
move	$10,$0
lui	$24,%hi(prob_order.6679)
addiu	$9,$9,%lo(prob_order.6679+7)
li	$15,2			# 0x2
$L17:
lw	$3,5168($4)
lw	$6,5172($4)
lw	$17,5184($4)
addu	$2,$7,$3
lbu	$18,0($11)
lbu	$16,0($2)
sll	$3,$3,$16
addu	$6,$16,$6
sll	$17,$17,$16
bltz	$6,$L18
sw	$3,5168($4)

lw	$2,5176($4)
lw	$16,5180($4)
sltu	$16,$2,$16
beq	$16,$0,$L18
addiu	$16,$2,2

sw	$16,5176($4)
lbu	$16,1($2)
lbu	$2,0($2)
sll	$16,$16,8
or	$16,$16,$2
sll	$2,$16,8
srl	$16,$16,8
or	$16,$2,$16
andi	$16,$16,0xffff
sll	$16,$16,$6
addiu	$6,$6,-16
or	$17,$17,$16
$L18:
addiu	$2,$3,-1
mul	$2,$2,$18
sra	$2,$2,8
addiu	$2,$2,1
sll	$16,$2,16
sltu	$18,$17,$16
bne	$18,$0,$L19
sw	$6,5172($4)

subu	$2,$3,$2
subu	$17,$17,$16
sll	$21,$10,3
addiu	$18,$24,%lo(prob_order.6679)
sw	$2,5168($4)
move	$20,$0
sw	$17,5184($4)
addu	$21,$12,$21
$L22:
addu	$3,$7,$2
lbu	$22,0($18)
lbu	$3,0($3)
addu	$16,$21,$22
sll	$19,$2,$3
lbu	$2,150($16)
addu	$16,$3,$6
addiu	$6,$19,-1
sw	$19,5168($4)
sll	$3,$17,$3
bltz	$16,$L20
mul	$2,$6,$2

lw	$17,5176($4)
lw	$6,5180($4)
sltu	$6,$17,$6
beq	$6,$0,$L20
addiu	$23,$17,2

sw	$23,5176($4)
lbu	$6,1($17)
lbu	$17,0($17)
sll	$6,$6,8
or	$6,$6,$17
sll	$17,$6,8
srl	$6,$6,8
or	$6,$17,$6
andi	$6,$6,0xffff
sll	$6,$6,$16
addiu	$16,$16,-16
or	$3,$3,$6
$L20:
sra	$2,$2,8
sw	$16,5172($4)
addiu	$2,$2,1
sll	$23,$2,16
sltu	$25,$3,$23
xori	$6,$25,0x1
beq	$6,$0,$L45
move	$17,$3

subu	$2,$19,$2
subu	$17,$3,$23
$L45:
sll	$6,$6,$22
sw	$2,5168($4)
addiu	$18,$18,1
sw	$17,5184($4)
or	$20,$20,$6
bne	$18,$9,$L22
move	$6,$16

andi	$3,$20,0xf0
beq	$3,$0,$L23
addu	$3,$7,$2

lbu	$18,0($14)
lbu	$6,0($3)
sll	$2,$2,$6
addu	$16,$6,$16
sll	$17,$17,$6
bltz	$16,$L24
sw	$2,5168($4)

lw	$6,5176($4)
lw	$3,5180($4)
sltu	$3,$6,$3
beq	$3,$0,$L55
addiu	$3,$2,-1

addiu	$3,$6,2
sw	$3,5176($4)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$6,$3,$6
sll	$3,$6,8
srl	$6,$6,8
or	$6,$3,$6
andi	$3,$6,0xffff
sll	$3,$3,$16
addiu	$16,$16,-16
or	$17,$17,$3
$L24:
addiu	$3,$2,-1
$L55:
mul	$3,$3,$18
sra	$3,$3,8
addiu	$3,$3,1
sll	$18,$3,16
sltu	$6,$17,$18
xori	$6,$6,0x1
bne	$6,$0,$L49
sw	$16,5172($4)

$L46:
sll	$6,$6,3
sw	$3,5168($4)
sw	$17,5184($4)
or	$20,$6,$20
$L26:
addu	$2,$7,$3
lbu	$18,-2($11)
lbu	$6,0($2)
sll	$3,$3,$6
addu	$16,$6,$16
sll	$17,$17,$6
bltz	$16,$L32
sw	$3,5168($4)

lw	$19,5176($4)
lw	$2,5180($4)
sltu	$2,$19,$2
bne	$2,$0,$L50
addiu	$2,$19,2

$L32:
addiu	$2,$3,-1
sw	$16,5172($4)
mul	$2,$2,$18
sra	$2,$2,8
addiu	$2,$2,1
sll	$16,$2,16
sltu	$6,$17,$16
xori	$6,$6,0x1
beq	$6,$0,$L33
subu	$3,$3,$2

subu	$17,$17,$16
sw	$3,5168($4)
$L40:
subu	$2,$0,$6
sw	$17,5184($4)
xor	$20,$2,$20
addu	$20,$20,$6
andi	$20,$20,0xffff
$L31:
beq	$10,$0,$L51
nop

lhu	$2,2($5)
addu	$20,$20,$2
beq	$13,$15,$L52
sh	$20,2($5)

$L35:
addiu	$10,$10,1
addiu	$11,$11,1
addiu	$13,$13,1
addiu	$14,$14,8
.option	pic0
j	$L17
.option	pic2
addiu	$8,$8,7

$L19:
lw	$16,%got(vp56_pva_tree)($28)
sw	$2,5168($4)
sw	$17,5184($4)
$L36:
lb	$20,0($16)
blez	$20,$L53
nop

$L30:
lw	$3,5168($4)
lb	$2,1($16)
lw	$6,5172($4)
addu	$17,$7,$3
lw	$18,5184($4)
addu	$2,$8,$2
lbu	$25,0($17)
lbu	$2,0($2)
sll	$3,$3,$25
addu	$6,$25,$6
addiu	$17,$3,-1
sw	$3,5168($4)
sll	$25,$18,$25
bltz	$6,$L28
mul	$2,$17,$2

lw	$18,5176($4)
lw	$17,5180($4)
sltu	$17,$18,$17
beq	$17,$0,$L28
addiu	$19,$18,2

sw	$19,5176($4)
lbu	$17,1($18)
lbu	$18,0($18)
sll	$17,$17,8
or	$17,$17,$18
sll	$18,$17,8
srl	$17,$17,8
or	$17,$18,$17
andi	$17,$17,0xffff
sll	$17,$17,$6
addiu	$6,$6,-16
or	$25,$25,$17
$L28:
sra	$2,$2,8
sw	$6,5172($4)
addiu	$2,$2,1
sll	$6,$2,16
subu	$17,$25,$6
sltu	$6,$25,$6
bne	$6,$0,$L29
subu	$3,$3,$2

sw	$3,5168($4)
sw	$17,5184($4)
lb	$2,0($16)
sll	$2,$2,1
addu	$16,$16,$2
lb	$20,0($16)
bgtz	$20,$L30
nop

$L53:
subu	$20,$0,$20
bne	$20,$0,$L54
nop

$L41:
.option	pic0
j	$L31
.option	pic2
move	$20,$0

$L29:
sw	$2,5168($4)
addiu	$16,$16,2
.option	pic0
j	$L36
.option	pic2
sw	$25,5184($4)

$L52:
lw	$23,28($sp)
lw	$22,24($sp)
lw	$21,20($sp)
lw	$20,16($sp)
lw	$19,12($sp)
lw	$18,8($sp)
lw	$17,4($sp)
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,32

$L23:
ori	$20,$20,0x8
beq	$20,$0,$L41
nop

$L54:
lw	$16,5172($4)
lw	$17,5184($4)
.option	pic0
j	$L26
.option	pic2
lw	$3,5168($4)

$L33:
.option	pic0
j	$L40
.option	pic2
sw	$2,5168($4)

$L50:
sw	$2,5176($4)
lbu	$6,1($19)
lbu	$2,0($19)
sll	$6,$6,8
or	$6,$6,$2
sll	$2,$6,8
srl	$6,$6,8
or	$6,$2,$6
andi	$2,$6,0xffff
sll	$2,$2,$16
addiu	$16,$16,-16
.option	pic0
j	$L32
.option	pic2
or	$17,$17,$2

$L49:
subu	$3,$2,$3
.option	pic0
j	$L46
.option	pic2
subu	$17,$17,$18

$L48:
lw	$2,6200($4)
.option	pic0
j	$L16
.option	pic2
sw	$2,0($5)

$L51:
lhu	$2,0($5)
addu	$20,$20,$2
.option	pic0
j	$L35
.option	pic2
sh	$20,0($5)

.set	macro
.set	reorder
.end	vp6_parse_vector_adjustment
.size	vp6_parse_vector_adjustment, .-vp6_parse_vector_adjustment
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_parse_coeff
.type	vp6_parse_coeff, @function
vp6_parse_coeff:
.frame	$sp,72,$31		# vars= 24, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
lw	$8,5208($4)
addiu	$sp,$sp,-72
lw	$12,6560($4)
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,40($sp)
lui	$24,%hi(vp6_coeff_groups)
sw	$20,52($sp)
li	$13,-1			# 0xffffffffffffffff
lw	$2,%got(vp56_b6to4)($28)
addiu	$20,$4,5344
lw	$10,%got(ff_vp56_norm_shift)($28)
addiu	$24,$24,%lo(vp6_coeff_groups)
lw	$14,%got(vp56_coeff_parse_table)($28)
sw	$19,48($sp)
move	$19,$0
sw	$2,8($sp)
lw	$2,%got(blk_coeff)($28)
sw	$18,44($sp)
addiu	$18,$4,4140
sw	$16,36($sp)
li	$16,6			# 0x6
sw	$fp,68($sp)
sw	$23,64($sp)
sw	$22,60($sp)
sw	$21,56($sp)
.cprestore	0
sw	$0,12($sp)
sw	$0,20($sp)
sw	$0,24($sp)
lw	$17,0($2)
sw	$17,16($sp)
$L57:
lw	$7,8($sp)
$L128:
lw	$9,24($sp)
lw	$5,20($sp)
lw	$6,5292($4)
lbu	$3,0($7)
subu	$2,$9,$5
lw	$5,0($20)
sll	$9,$3,2
sll	$7,$5,2
sll	$3,$3,4
sll	$5,$5,4
subu	$3,$3,$9
subu	$5,$5,$7
lw	$7,12($sp)
addu	$3,$4,$3
addu	$5,$6,$5
sll	$23,$2,4
subu	$6,$2,$7
lbu	$3,5296($3)
subu	$23,$23,$2
lbu	$2,0($5)
addu	$6,$12,$6
addu	$3,$3,$2
addiu	$6,$6,166
sll	$2,$3,2
addu	$2,$2,$3
addu	$23,$2,$23
addu	$23,$12,$23
addiu	$23,$23,1124
#APP
# 443 "vp6.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
sll	$15,$7,1
lw	$9,0($8)
sll	$2,$7,3
lw	$3,4($8)
move	$11,$0
subu	$2,$2,$15
sll	$21,$19,6
sll	$15,$2,5
li	$7,1			# 0x1
addu	$15,$2,$15
slt	$2,$11,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L59
li	$22,1			# 0x1
.set	macro
.set	reorder

$L117:
.set	noreorder
.set	nomacro
bne	$22,$0,$L120
addu	$5,$10,$9
.set	macro
.set	reorder

lw	$7,16($8)
$L60:
addu	$2,$10,$9
lbu	$fp,2($23)
lbu	$25,0($2)
sll	$9,$9,$25
addu	$3,$25,$3
sll	$7,$7,$25
.set	noreorder
.set	nomacro
bltz	$3,$L63
sw	$9,0($8)
.set	macro
.set	reorder

lw	$5,8($8)
lw	$2,12($8)
sltu	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L121
addiu	$2,$9,-1
.set	macro
.set	reorder

addiu	$2,$5,2
sw	$2,8($8)
lbu	$2,1($5)
lbu	$5,0($5)
sll	$2,$2,8
or	$2,$2,$5
sll	$5,$2,8
srl	$2,$2,8
or	$2,$5,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$7,$7,$2
$L63:
addiu	$2,$9,-1
$L121:
mul	$2,$2,$fp
sra	$2,$2,8
addiu	$2,$2,1
sll	$22,$2,16
sltu	$5,$7,$22
.set	noreorder
.set	nomacro
bne	$5,$0,$L64
sw	$3,4($8)
.set	macro
.set	reorder

subu	$2,$9,$2
subu	$7,$7,$22
addu	$5,$10,$2
sw	$2,0($8)
sw	$7,16($8)
lbu	$22,0($5)
lbu	$9,3($23)
sll	$2,$2,$22
addu	$3,$3,$22
sll	$7,$7,$22
.set	noreorder
.set	nomacro
bltz	$3,$L65
sw	$2,0($8)
.set	macro
.set	reorder

lw	$22,8($8)
lw	$5,12($8)
sltu	$5,$22,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L122
addiu	$5,$2,-1
.set	macro
.set	reorder

addiu	$5,$22,2
sw	$5,8($8)
lbu	$5,1($22)
lbu	$22,0($22)
sll	$5,$5,8
or	$5,$5,$22
sll	$22,$5,8
srl	$5,$5,8
or	$5,$22,$5
andi	$5,$5,0xffff
sll	$5,$5,$3
addiu	$3,$3,-16
or	$7,$7,$5
$L65:
addiu	$5,$2,-1
$L122:
mul	$5,$5,$9
sra	$5,$5,8
addiu	$5,$5,1
sll	$22,$5,16
sltu	$9,$7,$22
.set	noreorder
.set	nomacro
bne	$9,$0,$L66
sw	$3,4($8)
.set	macro
.set	reorder

subu	$2,$2,$5
lw	$9,%got(vp56_pc_tree)($28)
subu	$7,$7,$22
move	$5,$3
sw	$2,0($8)
sw	$7,16($8)
$L67:
lb	$3,0($9)
.set	noreorder
.set	nomacro
blez	$3,$L116
addu	$22,$10,$2
.set	macro
.set	reorder

$L70:
lbu	$3,0($22)
lb	$23,1($9)
sll	$22,$2,$3
addu	$23,$6,$23
addu	$5,$3,$5
addiu	$2,$22,-1
lbu	$23,0($23)
sll	$3,$7,$3
.set	noreorder
.set	nomacro
bltz	$5,$L68
sw	$22,0($8)
.set	macro
.set	reorder

lw	$25,8($8)
lw	$7,12($8)
sltu	$7,$25,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L68
addiu	$fp,$25,2
.set	macro
.set	reorder

sw	$fp,8($8)
lbu	$7,1($25)
lbu	$25,0($25)
sll	$7,$7,8
or	$7,$7,$25
sll	$25,$7,8
srl	$7,$7,8
or	$7,$25,$7
andi	$7,$7,0xffff
sll	$7,$7,$5
addiu	$5,$5,-16
or	$3,$3,$7
$L68:
mul	$2,$2,$23
sw	$5,4($8)
sra	$2,$2,8
addiu	$2,$2,1
sll	$23,$2,16
sltu	$25,$3,$23
.set	noreorder
.set	nomacro
bne	$25,$0,$L69
move	$7,$3
.set	macro
.set	reorder

subu	$2,$22,$2
subu	$7,$3,$23
sw	$2,0($8)
sw	$7,16($8)
lb	$3,0($9)
sll	$3,$3,1
addu	$9,$9,$3
lb	$3,0($9)
.set	noreorder
.set	nomacro
bgtz	$3,$L70
addu	$22,$10,$2
.set	macro
.set	reorder

$L116:
lw	$9,%got(vp56_coeff_bit_length)($28)
subu	$22,$0,$3
li	$6,5			# 0x5
sll	$25,$22,2
subu	$3,$6,$3
addu	$6,$9,$22
lw	$9,%got(vp56_coeff_bias)($28)
sll	$23,$22,4
addu	$3,$9,$3
lbu	$9,0($6)
subu	$23,$23,$25
lbu	$6,0($3)
subu	$23,$23,$22
move	$22,$5
$L73:
addu	$5,$10,$2
addu	$3,$9,$23
lbu	$5,0($5)
addu	$3,$14,$3
sll	$25,$2,$5
lbu	$2,0($3)
addu	$3,$5,$22
addiu	$22,$25,-1
sw	$25,0($8)
sll	$5,$7,$5
.set	noreorder
.set	nomacro
bltz	$3,$L71
mul	$2,$22,$2
.set	macro
.set	reorder

lw	$22,8($8)
lw	$7,12($8)
sltu	$7,$22,$7
.set	noreorder
.set	nomacro
beq	$7,$0,$L71
addiu	$fp,$22,2
.set	macro
.set	reorder

sw	$fp,8($8)
lbu	$7,1($22)
lbu	$22,0($22)
sll	$7,$7,8
or	$7,$7,$22
sll	$22,$7,8
srl	$7,$7,8
or	$7,$22,$7
andi	$7,$7,0xffff
sll	$7,$7,$3
addiu	$3,$3,-16
or	$5,$5,$7
$L71:
sra	$2,$2,8
sw	$3,4($8)
addiu	$2,$2,1
sll	$fp,$2,16
sltu	$22,$5,$fp
xori	$22,$22,0x1
.set	noreorder
.set	nomacro
beq	$22,$0,$L112
move	$7,$5
.set	macro
.set	reorder

subu	$2,$25,$2
subu	$7,$5,$fp
$L112:
sll	$22,$22,$9
sw	$2,0($8)
addiu	$9,$9,-1
sw	$7,16($8)
addu	$6,$6,$22
.set	noreorder
.set	nomacro
bne	$9,$13,$L73
move	$22,$3
.set	macro
.set	reorder

$L113:
li	$22,2			# 0x2
$L74:
addu	$5,$10,$2
lbu	$5,0($5)
sll	$2,$2,$5
addu	$3,$5,$3
sll	$7,$7,$5
.set	noreorder
.set	nomacro
bltz	$3,$L79
sw	$2,0($8)
.set	macro
.set	reorder

lw	$9,8($8)
lw	$5,12($8)
sltu	$5,$9,$5
.set	noreorder
.set	nomacro
beq	$5,$0,$L123
addiu	$5,$2,1
.set	macro
.set	reorder

addiu	$5,$9,2
sw	$5,8($8)
lbu	$5,1($9)
lbu	$9,0($9)
sll	$5,$5,8
or	$5,$5,$9
sll	$9,$5,8
srl	$5,$5,8
or	$5,$9,$5
andi	$5,$5,0xffff
sll	$5,$5,$3
addiu	$3,$3,-16
or	$7,$7,$5
$L79:
addiu	$5,$2,1
$L123:
sw	$3,4($8)
sra	$3,$5,1
sll	$9,$3,16
sltu	$5,$7,$9
xori	$5,$5,0x1
beq	$5,$0,$L80
subu	$3,$2,$3
subu	$7,$7,$9
$L80:
subu	$2,$0,$5
sw	$3,0($8)
sw	$7,16($8)
xor	$6,$2,$6
.set	noreorder
.set	nomacro
beq	$11,$0,$L81
addu	$6,$6,$5
.set	macro
.set	reorder

lhu	$2,5286($4)
mul	$6,$6,$2
$L81:
addu	$2,$12,$11
lbu	$2,64($2)
addu	$2,$18,$2
lbu	$2,0($2)
#APP
# 469 "vp6.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 470 "vp6.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
addu	$2,$2,$21
li	$7,1			# 0x1
sll	$2,$2,1
addu	$2,$17,$2
sh	$6,0($2)
$L82:
addu	$11,$11,$7
sll	$6,$22,1
addu	$3,$11,$24
sll	$2,$22,6
slt	$5,$11,64
lbu	$9,0($3)
move	$3,$6
addu	$6,$3,$2
sll	$2,$9,2
sll	$3,$9,4
subu	$2,$3,$2
subu	$2,$2,$9
addu	$6,$2,$6
addu	$6,$6,$15
addu	$6,$12,$6
.set	noreorder
.set	nomacro
beq	$5,$0,$L98
addiu	$6,$6,188
.set	macro
.set	reorder

slt	$2,$11,2
lw	$9,0($8)
move	$23,$6
.set	noreorder
.set	nomacro
beq	$2,$0,$L117
lw	$3,4($8)
.set	macro
.set	reorder

$L59:
addu	$5,$10,$9
$L120:
lw	$2,16($8)
lbu	$25,0($23)
lbu	$22,0($5)
sll	$9,$9,$22
addu	$3,$22,$3
sll	$22,$2,$22
.set	noreorder
.set	nomacro
bltz	$3,$L61
sw	$9,0($8)
.set	macro
.set	reorder

lw	$5,8($8)
lw	$2,12($8)
sltu	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L124
addiu	$2,$9,-1
.set	macro
.set	reorder

addiu	$2,$5,2
sw	$2,8($8)
lbu	$2,1($5)
lbu	$5,0($5)
sll	$2,$2,8
or	$2,$2,$5
sll	$5,$2,8
srl	$2,$2,8
or	$2,$5,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$22,$22,$2
$L61:
addiu	$2,$9,-1
$L124:
mul	$2,$2,$25
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$25,$22,$5
.set	noreorder
.set	nomacro
bne	$25,$0,$L62
sw	$3,4($8)
.set	macro
.set	reorder

subu	$9,$9,$2
subu	$7,$22,$5
sw	$9,0($8)
.option	pic0
.set	noreorder
.set	nomacro
j	$L60
.option	pic2
sw	$7,16($8)
.set	macro
.set	reorder

$L69:
sw	$2,0($8)
addiu	$9,$9,2
.option	pic0
.set	noreorder
.set	nomacro
j	$L67
.option	pic2
sw	$3,16($8)
.set	macro
.set	reorder

$L64:
sw	$7,16($8)
li	$22,1			# 0x1
.option	pic0
.set	noreorder
.set	nomacro
j	$L74
.option	pic2
li	$6,1			# 0x1
.set	macro
.set	reorder

$L62:
sw	$2,0($8)
.set	noreorder
.set	nomacro
beq	$11,$0,$L103
sw	$22,16($8)
.set	macro
.set	reorder

addu	$5,$10,$2
lbu	$7,1($23)
lbu	$5,0($5)
sll	$2,$2,$5
addu	$6,$3,$5
sll	$5,$22,$5
.set	noreorder
.set	nomacro
bltz	$6,$L83
sw	$2,0($8)
.set	macro
.set	reorder

lw	$9,8($8)
lw	$3,12($8)
sltu	$3,$9,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L125
addiu	$3,$2,-1
.set	macro
.set	reorder

addiu	$3,$9,2
sw	$3,8($8)
lbu	$3,1($9)
lbu	$9,0($9)
sll	$3,$3,8
or	$3,$3,$9
sll	$9,$3,8
srl	$3,$3,8
or	$3,$9,$3
andi	$3,$3,0xffff
sll	$3,$3,$6
addiu	$6,$6,-16
or	$5,$5,$3
$L83:
addiu	$3,$2,-1
$L125:
sw	$6,4($8)
mul	$3,$3,$7
sra	$3,$3,8
addiu	$3,$3,1
sll	$22,$3,16
sltu	$6,$5,$22
.set	noreorder
.set	nomacro
bne	$6,$0,$L84
slt	$6,$11,6
.set	macro
.set	reorder

xori	$6,$6,0x1
sll	$9,$6,1
sll	$6,$6,4
subu	$2,$2,$3
subu	$9,$6,$9
subu	$22,$5,$22
addu	$9,$12,$9
sw	$2,0($8)
lui	$2,%hi(vp6_pcr_tree)
addiu	$9,$9,1484
sw	$22,16($8)
addiu	$7,$2,%lo(vp6_pcr_tree)
$L85:
lb	$2,0($7)
blez	$2,$L118
$L88:
lw	$3,0($8)
lb	$2,1($7)
lw	$5,4($8)
addu	$6,$10,$3
lw	$22,16($8)
addu	$2,$9,$2
lbu	$6,0($6)
lbu	$2,0($2)
sll	$3,$3,$6
addu	$5,$6,$5
addiu	$23,$3,-1
sw	$3,0($8)
sll	$6,$22,$6
.set	noreorder
.set	nomacro
bltz	$5,$L86
mul	$2,$23,$2
.set	macro
.set	reorder

lw	$23,8($8)
lw	$22,12($8)
sltu	$22,$23,$22
.set	noreorder
.set	nomacro
beq	$22,$0,$L86
addiu	$25,$23,2
.set	macro
.set	reorder

sw	$25,8($8)
lbu	$22,1($23)
lbu	$23,0($23)
sll	$22,$22,8
or	$22,$22,$23
sll	$23,$22,8
srl	$22,$22,8
or	$22,$23,$22
andi	$22,$22,0xffff
sll	$22,$22,$5
addiu	$5,$5,-16
or	$6,$6,$22
$L86:
sra	$2,$2,8
sw	$5,4($8)
addiu	$2,$2,1
sll	$5,$2,16
subu	$22,$6,$5
sltu	$5,$6,$5
.set	noreorder
.set	nomacro
bne	$5,$0,$L87
subu	$3,$3,$2
.set	macro
.set	reorder

sw	$3,0($8)
sw	$22,16($8)
lb	$2,0($7)
sll	$2,$2,1
addu	$7,$7,$2
lb	$2,0($7)
bgtz	$2,$L88
$L118:
subu	$7,$0,$2
.set	noreorder
.set	nomacro
bne	$7,$0,$L82
move	$22,$0
.set	macro
.set	reorder

lw	$5,4($8)
li	$7,9			# 0x9
lw	$23,16($8)
lw	$2,0($8)
$L91:
addu	$3,$10,$2
addu	$6,$9,$22
lbu	$3,0($3)
lbu	$6,8($6)
sll	$25,$2,$3
addu	$5,$3,$5
addiu	$2,$25,-1
sw	$25,0($8)
sll	$3,$23,$3
.set	noreorder
.set	nomacro
bltz	$5,$L89
mul	$2,$2,$6
.set	macro
.set	reorder

lw	$23,8($8)
lw	$6,12($8)
sltu	$6,$23,$6
.set	noreorder
.set	nomacro
beq	$6,$0,$L89
addiu	$fp,$23,2
.set	macro
.set	reorder

sw	$fp,8($8)
lbu	$6,1($23)
lbu	$23,0($23)
sll	$6,$6,8
or	$6,$6,$23
sll	$23,$6,8
srl	$6,$6,8
or	$6,$23,$6
andi	$6,$6,0xffff
sll	$6,$6,$5
addiu	$5,$5,-16
or	$3,$3,$6
$L89:
sra	$2,$2,8
sw	$5,4($8)
addiu	$2,$2,1
sll	$fp,$2,16
sltu	$6,$3,$fp
xori	$6,$6,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L114
move	$23,$3
.set	macro
.set	reorder

subu	$2,$25,$2
subu	$23,$3,$fp
$L114:
sll	$6,$6,$22
sw	$2,0($8)
addiu	$22,$22,1
sw	$23,16($8)
.set	noreorder
.set	nomacro
bne	$22,$16,$L91
addu	$7,$7,$6
.set	macro
.set	reorder

$L103:
.option	pic0
.set	noreorder
.set	nomacro
j	$L82
.option	pic2
move	$22,$0
.set	macro
.set	reorder

$L87:
sw	$2,0($8)
addiu	$7,$7,2
.option	pic0
.set	noreorder
.set	nomacro
j	$L85
.option	pic2
sw	$6,16($8)
.set	macro
.set	reorder

$L66:
addu	$2,$10,$5
sw	$5,0($8)
sw	$7,16($8)
lbu	$9,4($23)
lbu	$22,0($2)
sll	$5,$5,$22
addu	$3,$3,$22
sll	$7,$7,$22
.set	noreorder
.set	nomacro
bltz	$3,$L75
sw	$5,0($8)
.set	macro
.set	reorder

lw	$22,8($8)
lw	$2,12($8)
sltu	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L126
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$22,2
sw	$2,8($8)
lbu	$2,1($22)
lbu	$22,0($22)
sll	$2,$2,8
or	$2,$2,$22
sll	$22,$2,8
srl	$2,$2,8
or	$2,$22,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$7,$7,$2
$L75:
addiu	$2,$5,-1
$L126:
mul	$2,$2,$9
sra	$2,$2,8
addiu	$2,$2,1
sll	$9,$2,16
sltu	$22,$7,$9
.set	noreorder
.set	nomacro
bne	$22,$0,$L76
sw	$3,4($8)
.set	macro
.set	reorder

subu	$5,$5,$2
subu	$7,$7,$9
addu	$2,$10,$5
sw	$5,0($8)
sw	$7,16($8)
lbu	$9,0($2)
lbu	$6,5($6)
sll	$5,$5,$9
addu	$3,$3,$9
sll	$7,$7,$9
.set	noreorder
.set	nomacro
bltz	$3,$L77
sw	$5,0($8)
.set	macro
.set	reorder

lw	$9,8($8)
lw	$2,12($8)
sltu	$2,$9,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L127
addiu	$2,$5,-1
.set	macro
.set	reorder

addiu	$2,$9,2
sw	$2,8($8)
lbu	$2,1($9)
lbu	$9,0($9)
sll	$2,$2,8
or	$2,$2,$9
sll	$9,$2,8
srl	$2,$2,8
or	$2,$9,$2
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$7,$7,$2
$L77:
addiu	$2,$5,-1
$L127:
mul	$2,$2,$6
sra	$2,$2,8
addiu	$2,$2,1
sll	$9,$2,16
sltu	$6,$7,$9
xori	$6,$6,0x1
.set	noreorder
.set	nomacro
beq	$6,$0,$L78
sw	$3,4($8)
.set	macro
.set	reorder

subu	$2,$5,$2
subu	$7,$7,$9
$L78:
addiu	$6,$6,3
.option	pic0
.set	noreorder
.set	nomacro
j	$L113
.option	pic2
sw	$7,16($8)
.set	macro
.set	reorder

$L84:
sw	$3,0($8)
sw	$5,16($8)
$L98:
#APP
# 491 "vp6.c" 1
.word	0b01110000000001010000001011101110	#S32M2I XR11, $5
# 0 "" 2
#NO_APP
lw	$3,8($sp)
sra	$5,$5,3
lw	$6,%got(idct_row)($28)
addiu	$20,$20,4
addiu	$5,$5,1
lw	$9,8($sp)
lbu	$2,0($3)
addu	$7,$6,$19
addiu	$9,$9,1
sb	$5,0($7)
addiu	$19,$19,1
lw	$7,16($sp)
sll	$6,$2,2
sll	$3,$2,4
lw	$2,-4($20)
sw	$9,8($sp)
subu	$3,$3,$6
lw	$6,5292($4)
lh	$5,0($7)
sll	$7,$2,2
sll	$2,$2,4
addu	$3,$4,$3
subu	$2,$2,$7
sltu	$5,$0,$5
addu	$2,$6,$2
lw	$6,16($sp)
sb	$5,0($2)
addiu	$6,$6,128
sb	$5,5296($3)
.set	noreorder
.set	nomacro
beq	$19,$16,$L119
sw	$6,16($sp)
.set	macro
.set	reorder

slt	$2,$19,4
.set	noreorder
.set	nomacro
bne	$2,$0,$L128
lw	$7,8($sp)
.set	macro
.set	reorder

li	$3,1			# 0x1
li	$5,4			# 0x4
li	$6,16			# 0x10
sw	$3,12($sp)
sw	$5,20($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L57
.option	pic2
sw	$6,24($sp)
.set	macro
.set	reorder

$L76:
sw	$7,16($8)
li	$22,2			# 0x2
.option	pic0
.set	noreorder
.set	nomacro
j	$L74
.option	pic2
li	$6,2			# 0x2
.set	macro
.set	reorder

$L119:
lw	$fp,68($sp)
lw	$23,64($sp)
lw	$22,60($sp)
lw	$21,56($sp)
lw	$20,52($sp)
lw	$19,48($sp)
lw	$18,44($sp)
lw	$17,40($sp)
lw	$16,36($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

.end	vp6_parse_coeff
.size	vp6_parse_coeff, .-vp6_parse_coeff
.section	.text.unlikely,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_decode_free
.type	vp6_decode_free, @function
vp6_decode_free:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-64
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,28($sp)
sw	$23,52($sp)
move	$23,$0
sw	$21,44($sp)
sw	$20,40($sp)
li	$20,288			# 0x120
sw	$18,32($sp)
li	$18,96			# 0x60
.cprestore	16
sw	$31,60($sp)
sw	$fp,56($sp)
sw	$22,48($sp)
sw	$19,36($sp)
sw	$16,24($sp)
lw	$25,%call16(ff_vp56_free)($28)
.reloc	1f,R_MIPS_JALR,ff_vp56_free
1:	jalr	$25
lw	$17,136($4)

lw	$28,16($sp)
addiu	$21,$17,10328
$L133:
lw	$25,%call16(free_vlc)($28)
move	$4,$21
mul	$22,$23,$20
.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
move	$19,$0

lw	$28,16($sp)
lw	$25,%call16(free_vlc)($28)
.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
addiu	$4,$21,32

lw	$28,16($sp)
addu	$16,$19,$22
$L138:
move	$fp,$0
addiu	$16,$16,10392
$L131:
addu	$4,$16,$fp
lw	$25,%call16(free_vlc)($28)
addiu	$fp,$fp,16
.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
addu	$4,$17,$4

bne	$fp,$18,$L131
lw	$28,16($sp)

addiu	$19,$19,96
bne	$19,$20,$L138
addu	$16,$19,$22

li	$2,1			# 0x1
beq	$23,$2,$L137
addiu	$21,$21,16

.option	pic0
j	$L133
.option	pic2
li	$23,1			# 0x1

$L137:
lw	$31,60($sp)
move	$2,$0
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,64

.set	macro
.set	reorder
.end	vp6_decode_free
.size	vp6_decode_free, .-vp6_decode_free
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_decode_init
.type	vp6_decode_init, @function
vp6_decode_init:
.frame	$sp,32,$31		# vars= 0, regs= 2/0, args= 16, gp= 8
.mask	0x80010000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,132($4)
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-32
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$31,28($sp)
sw	$16,24($sp)
.cprestore	16
lw	$6,8($3)
lw	$25,%call16(ff_vp56_init)($28)
lw	$16,136($4)
xori	$5,$6,0x5e
xori	$6,$6,0x6e
sltu	$5,$5,1
.reloc	1f,R_MIPS_JALR,ff_vp56_init
1:	jalr	$25
sltu	$6,$6,1

lui	$2,%hi(vp6_coord_div)
lui	$3,%hi(vp6_parse_coeff_models)
lw	$31,28($sp)
addiu	$2,$2,%lo(vp6_coord_div)
addiu	$3,$3,%lo(vp6_parse_coeff_models)
sw	$2,6528($16)
lui	$2,%hi(vp6_parse_vector_adjustment)
sw	$3,6552($16)
lui	$3,%hi(vp6_parse_header)
addiu	$2,$2,%lo(vp6_parse_vector_adjustment)
addiu	$3,$3,%lo(vp6_parse_header)
sw	$2,6532($16)
lui	$2,%hi(vp6_filter)
sw	$3,6556($16)
addiu	$2,$2,%lo(vp6_filter)
sw	$2,6536($16)
lui	$2,%hi(vp6_default_models_init)
addiu	$2,$2,%lo(vp6_default_models_init)
sw	$2,6544($16)
lui	$2,%hi(vp6_parse_vector_models)
addiu	$2,$2,%lo(vp6_parse_vector_models)
sw	$2,6548($16)
move	$2,$0
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	vp6_decode_init
.size	vp6_decode_init, .-vp6_decode_init
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	put_h264_chroma_mc8_c.constprop.13
.type	put_h264_chroma_mc8_c.constprop.13, @function
put_h264_chroma_mc8_c.constprop.13:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
andi	$9,$5,0x3
subu	$2,$0,$6
subu	$5,$5,$9
li	$8,4			# 0x4
li	$3,3			# 0x3
addu	$4,$4,$2
subu	$8,$8,$9
subu	$3,$3,$9
addu	$2,$5,$2
#APP
# 643 "vp6.c" 1
.word	0b01110000000001110000000001101111	#S32I2M XR1,$7
# 0 "" 2
#NO_APP
lw	$7,16($sp)
#APP
# 644 "vp6.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
li	$7,134742016			# 0x8080000
addiu	$7,$7,2056
#APP
# 645 "vp6.c" 1
.word	0b01110000000001110000001101101111	#S32I2M XR13,$7
# 0 "" 2
# 646 "vp6.c" 1
.word	0b01110000000101001000010000111101	#S32SFL XR0,XR1,XR2,XR5,PTN0
# 0 "" 2
# 647 "vp6.c" 1
.word	0b01110011000101010101010000111101	#S32SFL XR0,XR5,XR5,XR5,PTN3
# 0 "" 2
# 648 "vp6.c" 1
.word	0b01110010000100010101010011111101	#S32SFL XR3,XR5,XR5,XR4,PTN2
# 0 "" 2
# 649 "vp6.c" 1
.word	0b01110011000111001111010001000110	#Q8ADD XR1,XR13,XR3,SS
# 0 "" 2
# 650 "vp6.c" 1
.word	0b01110011000111010011010010000110	#Q8ADD XR2,XR13,XR4,SS
# 0 "" 2
# 651 "vp6.c" 1
.word	0b01110000000101001000010000111100	#Q8MADL XR0,XR1,XR2,XR5,AA
# 0 "" 2
# 652 "vp6.c" 1
.word	0b01110000000110001000110000111100	#Q8MADL XR0,XR3,XR2,XR6,AA
# 0 "" 2
# 653 "vp6.c" 1
.word	0b01110000000111010000010000111100	#Q8MADL XR0,XR1,XR4,XR7,AA
# 0 "" 2
# 654 "vp6.c" 1
.word	0b01110000001000010000110000111100	#Q8MADL XR0,XR3,XR4,XR8,AA
# 0 "" 2
# 655 "vp6.c" 1
.word	0b01110000100000000011011111110000	#D32SLL XR15,XR13,XR0,XR0,2
# 0 "" 2
#NO_APP
li	$7,8			# 0x8
$L142:
#APP
# 659 "vp6.c" 1
.word	0b01110000010001100000000001010110	#S32LDIV XR1,$2,$6,0
# 0 "" 2
# 660 "vp6.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 661 "vp6.c" 1
.word	0b01110000101001100000001001010110	#S32LDIV XR9,$5,$6,0
# 0 "" 2
# 662 "vp6.c" 1
.word	0b01110000101000000000011010010000	#S32LDD XR10,$5,4
# 0 "" 2
# 664 "vp6.c" 1
.word	0b01110001000001000100100011100111	#S32ALN XR3,XR2,XR1,$8
# 0 "" 2
# 665 "vp6.c" 1
.word	0b01110000011001000100100100100111	#S32ALN XR4,XR2,XR1,$3
# 0 "" 2
# 666 "vp6.c" 1
.word	0b01110001000001100110101011100111	#S32ALN XR11,XR10,XR9,$8
# 0 "" 2
# 667 "vp6.c" 1
.word	0b01110000011001100110101100100111	#S32ALN XR12,XR10,XR9,$3
# 0 "" 2
# 669 "vp6.c" 1
.word	0b01110000000001000011111101011100	#Q8ADDE XR13,XR15,XR0,XR1,AA
# 0 "" 2
# 670 "vp6.c" 1
.word	0b01110000000001010100111101111010	#Q8MAC XR13,XR3,XR5,XR1,AA
# 0 "" 2
# 671 "vp6.c" 1
.word	0b01110000000001011001001101111010	#Q8MAC XR13,XR4,XR6,XR1,AA
# 0 "" 2
# 672 "vp6.c" 1
.word	0b01110000000001011110111101111010	#Q8MAC XR13,XR11,XR7,XR1,AA
# 0 "" 2
# 674 "vp6.c" 1
.word	0b01110000000001100011001101111010	#Q8MAC XR13,XR12,XR8,XR1,AA
# 0 "" 2
# 677 "vp6.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 678 "vp6.c" 1
.word	0b01110000101000000000101001010000	#S32LDD XR9,$5,8
# 0 "" 2
# 679 "vp6.c" 1
.word	0b01110001000001001000111011100111	#S32ALN XR11,XR3,XR2,$8
# 0 "" 2
# 680 "vp6.c" 1
.word	0b01110000011001001000110011100111	#S32ALN XR3,XR3,XR2,$3
# 0 "" 2
# 681 "vp6.c" 1
.word	0b01110001000001101010011100100111	#S32ALN XR12,XR9,XR10,$8
# 0 "" 2
# 682 "vp6.c" 1
.word	0b01110000011001101010011001100111	#S32ALN XR9,XR9,XR10,$3
# 0 "" 2
# 684 "vp6.c" 1
.word	0b01110000000010000011111110011100	#Q8ADDE XR14,XR15,XR0,XR2,AA
# 0 "" 2
# 685 "vp6.c" 1
.word	0b01110000000010010110111110111010	#Q8MAC XR14,XR11,XR5,XR2,AA
# 0 "" 2
# 686 "vp6.c" 1
.word	0b01110000000010011000111110111010	#Q8MAC XR14,XR3,XR6,XR2,AA
# 0 "" 2
# 687 "vp6.c" 1
.word	0b01110000000010011111001110111010	#Q8MAC XR14,XR12,XR7,XR2,AA
# 0 "" 2
# 688 "vp6.c" 1
.word	0b01110000000010100010011110111010	#Q8MAC XR14,XR9,XR8,XR2,AA
# 0 "" 2
# 690 "vp6.c" 1
.word	0b01110001100001000111011101110101	#Q16SLR XR13,XR13,XR1,XR1,6
# 0 "" 2
# 691 "vp6.c" 1
.word	0b01110001100010001011101110110101	#Q16SLR XR14,XR14,XR2,XR2,6
# 0 "" 2
# 692 "vp6.c" 1
.word	0b01110001001101000111010000111101	#S32SFL XR0,XR13,XR1,XR13,PTN1
# 0 "" 2
# 693 "vp6.c" 1
.word	0b01110001001110001011100000111101	#S32SFL XR0,XR14,XR2,XR14,PTN1
# 0 "" 2
# 695 "vp6.c" 1
.word	0b01110000100001100000001101010111	#S32SDIV XR13,$4,$6,0
# 0 "" 2
# 696 "vp6.c" 1
.word	0b01110000100000000000011110010001	#S32STD XR14,$4,4
# 0 "" 2
#NO_APP
addiu	$7,$7,-1
bne	$7,$0,$L142
j	$31
.end	put_h264_chroma_mc8_c.constprop.13
.size	put_h264_chroma_mc8_c.constprop.13, .-put_h264_chroma_mc8_c.constprop.13
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_filter
.type	vp6_filter, @function
vp6_filter:
.frame	$sp,216,$31		# vars= 176, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
addiu	$sp,$sp,-216
move	$9,$5
sw	$31,212($sp)
lw	$2,240($sp)
lw	$10,244($sp)
lw	$5,252($sp)
sll	$3,$2,16
lw	$11,232($sp)
sra	$2,$2,16
lw	$8,236($sp)
sra	$3,$3,16
and	$12,$10,$2
.set	noreorder
.set	nomacro
beq	$5,$0,$L145
and	$13,$10,$3
.set	macro
.set	reorder

lw	$5,6224($4)
li	$10,2			# 0x2
sll	$13,$13,1
.set	noreorder
.set	nomacro
beq	$5,$10,$L207
sll	$12,$12,1
.set	macro
.set	reorder

bne	$12,$0,$L173
slt	$2,$11,$7
.set	noreorder
.set	nomacro
beq	$5,$0,$L155
movn	$7,$11,$2
.set	macro
.set	reorder

$L167:
lw	$3,248($sp)
sll	$2,$3,3
addu	$2,$2,$13
sll	$3,$2,3
lui	$2,%hi(vp6_block_copy_filter)
addiu	$2,$2,%lo(vp6_block_copy_filter)
addu	$2,$2,$3
li	$3,64			# 0x40
#APP
# 595 "vp6.c" 1
.word	0b01110000000000110000001111101111	#S32I2M XR15,$3
# 0 "" 2
# 596 "vp6.c" 1
.word	0b01110000010000000000000001101010	#S16LDD XR1,$2,0,PTN0
# 0 "" 2
# 597 "vp6.c" 1
.word	0b01110000010010000000010001101010	#S16LDD XR1,$2,2,PTN1
# 0 "" 2
# 598 "vp6.c" 1
.word	0b01110000010000000000100010101010	#S16LDD XR2,$2,4,PTN0
# 0 "" 2
# 599 "vp6.c" 1
.word	0b01110000010010000000110010101010	#S16LDD XR2,$2,6,PTN1
# 0 "" 2
#NO_APP
addiu	$5,$7,-3
addiu	$9,$9,-2
addu	$5,$6,$5
li	$4,3			# 0x3
li	$6,8			# 0x8
$L156:
move	$2,$5
#APP
# 605 "vp6.c" 1
.word	0b01110000010000000000101110100100	#S8LDI XR14,$2,2,PTN0
# 0 "" 2
# 606 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 607 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 608 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 609 "vp6.c" 1
.word	0b01110000010000000001001101100010	#S8LDD XR13,$2,4,PTN0
# 0 "" 2
# 611 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 612 "vp6.c" 1
.word	0b01110000100001111011011100100111	#S32ALN XR12,XR13,XR14,$4
# 0 "" 2
# 613 "vp6.c" 1
.word	0b01110000001011110000001010111101	#S32SFL XR10,XR0,XR12,XR11,PTN0
# 0 "" 2
# 614 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 615 "vp6.c" 1
.word	0b01110000001001101100011000001000	#D16MUL XR8,XR1,XR11,XR9,WW
# 0 "" 2
# 616 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 617 "vp6.c" 1
.word	0b01110000001001101000101000001010	#D16MAC XR8,XR2,XR10,XR9,AA,WW
# 0 "" 2
# 618 "vp6.c" 1
.word	0b01110000101001100001100101011001	#D32ASUM XR5,XR6,XR8,XR9,AA
# 0 "" 2
# 619 "vp6.c" 1
.word	0b01110000101001111111110101011001	#D32ASUM XR5,XR15,XR15,XR9,AA
# 0 "" 2
# 620 "vp6.c" 1
.word	0b01110001111001100101010101110011	#D32SAR XR5,XR5,XR9,XR9,7
# 0 "" 2
# 621 "vp6.c" 1
.word	0b01110011001000010110010000111101	#S32SFL XR0,XR9,XR5,XR8,PTN3
# 0 "" 2
# 622 "vp6.c" 1
.word	0b01110000000110100000001000000111	#Q16SAT XR8,XR0,XR8
# 0 "" 2
#NO_APP
move	$3,$9
#APP
# 623 "vp6.c" 1
.word	0b01110000011000000000011000101101	#S16SDI XR8,$3,2,PTN0
# 0 "" 2
# 605 "vp6.c" 1
.word	0b01110000010000000000101110100100	#S8LDI XR14,$2,2,PTN0
# 0 "" 2
# 606 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 607 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 608 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 609 "vp6.c" 1
.word	0b01110000010000000001001101100010	#S8LDD XR13,$2,4,PTN0
# 0 "" 2
# 611 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 612 "vp6.c" 1
.word	0b01110000100001111011011100100111	#S32ALN XR12,XR13,XR14,$4
# 0 "" 2
# 613 "vp6.c" 1
.word	0b01110000001011110000001010111101	#S32SFL XR10,XR0,XR12,XR11,PTN0
# 0 "" 2
# 614 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 615 "vp6.c" 1
.word	0b01110000001001101100011000001000	#D16MUL XR8,XR1,XR11,XR9,WW
# 0 "" 2
# 616 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 617 "vp6.c" 1
.word	0b01110000001001101000101000001010	#D16MAC XR8,XR2,XR10,XR9,AA,WW
# 0 "" 2
# 618 "vp6.c" 1
.word	0b01110000101001100001100101011001	#D32ASUM XR5,XR6,XR8,XR9,AA
# 0 "" 2
# 619 "vp6.c" 1
.word	0b01110000101001111111110101011001	#D32ASUM XR5,XR15,XR15,XR9,AA
# 0 "" 2
# 620 "vp6.c" 1
.word	0b01110001111001100101010101110011	#D32SAR XR5,XR5,XR9,XR9,7
# 0 "" 2
# 621 "vp6.c" 1
.word	0b01110011001000010110010000111101	#S32SFL XR0,XR9,XR5,XR8,PTN3
# 0 "" 2
# 622 "vp6.c" 1
.word	0b01110000000110100000001000000111	#Q16SAT XR8,XR0,XR8
# 0 "" 2
# 623 "vp6.c" 1
.word	0b01110000011000000000011000101101	#S16SDI XR8,$3,2,PTN0
# 0 "" 2
# 605 "vp6.c" 1
.word	0b01110000010000000000101110100100	#S8LDI XR14,$2,2,PTN0
# 0 "" 2
# 606 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 607 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 608 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 609 "vp6.c" 1
.word	0b01110000010000000001001101100010	#S8LDD XR13,$2,4,PTN0
# 0 "" 2
# 611 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 612 "vp6.c" 1
.word	0b01110000100001111011011100100111	#S32ALN XR12,XR13,XR14,$4
# 0 "" 2
# 613 "vp6.c" 1
.word	0b01110000001011110000001010111101	#S32SFL XR10,XR0,XR12,XR11,PTN0
# 0 "" 2
# 614 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 615 "vp6.c" 1
.word	0b01110000001001101100011000001000	#D16MUL XR8,XR1,XR11,XR9,WW
# 0 "" 2
# 616 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 617 "vp6.c" 1
.word	0b01110000001001101000101000001010	#D16MAC XR8,XR2,XR10,XR9,AA,WW
# 0 "" 2
# 618 "vp6.c" 1
.word	0b01110000101001100001100101011001	#D32ASUM XR5,XR6,XR8,XR9,AA
# 0 "" 2
# 619 "vp6.c" 1
.word	0b01110000101001111111110101011001	#D32ASUM XR5,XR15,XR15,XR9,AA
# 0 "" 2
# 620 "vp6.c" 1
.word	0b01110001111001100101010101110011	#D32SAR XR5,XR5,XR9,XR9,7
# 0 "" 2
# 621 "vp6.c" 1
.word	0b01110011001000010110010000111101	#S32SFL XR0,XR9,XR5,XR8,PTN3
# 0 "" 2
# 622 "vp6.c" 1
.word	0b01110000000110100000001000000111	#Q16SAT XR8,XR0,XR8
# 0 "" 2
# 623 "vp6.c" 1
.word	0b01110000011000000000011000101101	#S16SDI XR8,$3,2,PTN0
# 0 "" 2
# 605 "vp6.c" 1
.word	0b01110000010000000000101110100100	#S8LDI XR14,$2,2,PTN0
# 0 "" 2
# 606 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 607 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 608 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 609 "vp6.c" 1
.word	0b01110000010000000001001101100010	#S8LDD XR13,$2,4,PTN0
# 0 "" 2
# 611 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 612 "vp6.c" 1
.word	0b01110000100001111011011100100111	#S32ALN XR12,XR13,XR14,$4
# 0 "" 2
# 613 "vp6.c" 1
.word	0b01110000001011110000001010111101	#S32SFL XR10,XR0,XR12,XR11,PTN0
# 0 "" 2
# 614 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 615 "vp6.c" 1
.word	0b01110000001001101100011000001000	#D16MUL XR8,XR1,XR11,XR9,WW
# 0 "" 2
# 616 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 617 "vp6.c" 1
.word	0b01110000001001101000101000001010	#D16MAC XR8,XR2,XR10,XR9,AA,WW
# 0 "" 2
# 618 "vp6.c" 1
.word	0b01110000101001100001100101011001	#D32ASUM XR5,XR6,XR8,XR9,AA
# 0 "" 2
# 619 "vp6.c" 1
.word	0b01110000101001111111110101011001	#D32ASUM XR5,XR15,XR15,XR9,AA
# 0 "" 2
# 620 "vp6.c" 1
.word	0b01110001111001100101010101110011	#D32SAR XR5,XR5,XR9,XR9,7
# 0 "" 2
# 621 "vp6.c" 1
.word	0b01110011001000010110010000111101	#S32SFL XR0,XR9,XR5,XR8,PTN3
# 0 "" 2
# 622 "vp6.c" 1
.word	0b01110000000110100000001000000111	#Q16SAT XR8,XR0,XR8
# 0 "" 2
# 623 "vp6.c" 1
.word	0b01110000011000000000011000101101	#S16SDI XR8,$3,2,PTN0
# 0 "" 2
#NO_APP
addiu	$6,$6,-1
addu	$5,$5,$8
.set	noreorder
.set	nomacro
bne	$6,$0,$L156
addu	$9,$9,$8
.set	macro
.set	reorder

lw	$31,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L145:
.set	noreorder
.set	nomacro
beq	$12,$0,$L201
move	$5,$0
.set	macro
.set	reorder

$L173:
lw	$14,6500($4)
subu	$10,$11,$7
mul	$10,$10,$14
slt	$10,$10,0
movn	$7,$11,$10
$L153:
beq	$5,$0,$L166
$L165:
.set	noreorder
.set	nomacro
bne	$13,$0,$L158
lw	$15,248($sp)
.set	macro
.set	reorder

lw	$5,248($sp)
addu	$4,$6,$7
li	$15,8			# 0x8
li	$14,-256			# 0xffffffffffffff00
sll	$2,$5,3
lui	$5,%hi(vp6_block_copy_filter)
addu	$2,$2,$12
addiu	$5,$5,%lo(vp6_block_copy_filter)
sll	$2,$2,3
li	$13,8			# 0x8
addu	$5,$5,$2
$L159:
move	$7,$0
addu	$24,$4,$8
$L160:
subu	$10,$4,$8
lbu	$11,0($4)
addu	$12,$7,$8
lh	$6,2($5)
addu	$3,$24,$7
lh	$2,0($5)
lbu	$10,0($10)
addu	$12,$24,$12
mult	$11,$6
lh	$25,4($5)
lbu	$6,0($3)
addu	$11,$9,$7
lbu	$3,0($12)
madd	$10,$2
lh	$2,6($5)
addiu	$7,$7,1
madd	$6,$25
addiu	$4,$4,1
madd	$3,$2
mflo	$2
addiu	$2,$2,64
sra	$2,$2,7
subu	$6,$0,$2
and	$3,$2,$14
sra	$6,$6,31
movn	$2,$6,$3
.set	noreorder
.set	nomacro
bne	$7,$13,$L160
sb	$2,0($11)
.set	macro
.set	reorder

addiu	$15,$15,-1
move	$4,$24
.set	noreorder
.set	nomacro
bne	$15,$0,$L159
addu	$9,$9,$8
.set	macro
.set	reorder

lw	$31,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L207:
lw	$10,6228($4)
.set	noreorder
.set	nomacro
beq	$10,$0,$L147
subu	$14,$0,$3
.set	macro
.set	reorder

slt	$15,$3,0
movz	$14,$3,$15
slt	$14,$10,$14
.set	noreorder
.set	nomacro
beq	$14,$0,$L208
subu	$15,$0,$2
.set	macro
.set	reorder

$L148:
beq	$12,$0,$L201
$L170:
lw	$10,6500($4)
subu	$5,$11,$7
mul	$5,$5,$10
slt	$5,$5,0
movn	$7,$11,$5
$L166:
.set	noreorder
.set	nomacro
beq	$13,$0,$L155
xor	$2,$2,$3
.set	macro
.set	reorder

lw	$5,5164($4)
sra	$2,$2,31
subu	$10,$0,$8
addu	$2,$7,$2
addiu	$5,$5,16
addu	$3,$6,$2
li	$11,4			# 0x4
andi	$6,$3,0x3
subu	$3,$3,$6
li	$2,3			# 0x3
subu	$11,$11,$6
addu	$4,$3,$10
subu	$6,$2,$6
addu	$2,$5,$10
#APP
# 643 "vp6.c" 1
.word	0b01110000000011010000000001101111	#S32I2M XR1,$13
# 0 "" 2
#NO_APP
move	$7,$0
#APP
# 644 "vp6.c" 1
.word	0b01110000000001110000000010101111	#S32I2M XR2,$7
# 0 "" 2
#NO_APP
li	$7,134742016			# 0x8080000
addiu	$7,$7,2056
#APP
# 645 "vp6.c" 1
.word	0b01110000000001110000001101101111	#S32I2M XR13,$7
# 0 "" 2
# 646 "vp6.c" 1
.word	0b01110000000101001000010000111101	#S32SFL XR0,XR1,XR2,XR5,PTN0
# 0 "" 2
# 647 "vp6.c" 1
.word	0b01110011000101010101010000111101	#S32SFL XR0,XR5,XR5,XR5,PTN3
# 0 "" 2
# 648 "vp6.c" 1
.word	0b01110010000100010101010011111101	#S32SFL XR3,XR5,XR5,XR4,PTN2
# 0 "" 2
# 649 "vp6.c" 1
.word	0b01110011000111001111010001000110	#Q8ADD XR1,XR13,XR3,SS
# 0 "" 2
# 650 "vp6.c" 1
.word	0b01110011000111010011010010000110	#Q8ADD XR2,XR13,XR4,SS
# 0 "" 2
# 651 "vp6.c" 1
.word	0b01110000000101001000010000111100	#Q8MADL XR0,XR1,XR2,XR5,AA
# 0 "" 2
# 652 "vp6.c" 1
.word	0b01110000000110001000110000111100	#Q8MADL XR0,XR3,XR2,XR6,AA
# 0 "" 2
# 653 "vp6.c" 1
.word	0b01110000000111010000010000111100	#Q8MADL XR0,XR1,XR4,XR7,AA
# 0 "" 2
# 654 "vp6.c" 1
.word	0b01110000001000010000110000111100	#Q8MADL XR0,XR3,XR4,XR8,AA
# 0 "" 2
# 655 "vp6.c" 1
.word	0b01110000100000000011011111110000	#D32SLL XR15,XR13,XR0,XR0,2
# 0 "" 2
#NO_APP
li	$7,9			# 0x9
$L164:
#APP
# 659 "vp6.c" 1
.word	0b01110000100010000000000001010110	#S32LDIV XR1,$4,$8,0
# 0 "" 2
# 660 "vp6.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 661 "vp6.c" 1
.word	0b01110000011010000000001001010110	#S32LDIV XR9,$3,$8,0
# 0 "" 2
# 662 "vp6.c" 1
.word	0b01110000011000000000011010010000	#S32LDD XR10,$3,4
# 0 "" 2
# 664 "vp6.c" 1
.word	0b01110001011001000100100011100111	#S32ALN XR3,XR2,XR1,$11
# 0 "" 2
# 665 "vp6.c" 1
.word	0b01110000110001000100100100100111	#S32ALN XR4,XR2,XR1,$6
# 0 "" 2
# 666 "vp6.c" 1
.word	0b01110001011001100110101011100111	#S32ALN XR11,XR10,XR9,$11
# 0 "" 2
# 667 "vp6.c" 1
.word	0b01110000110001100110101100100111	#S32ALN XR12,XR10,XR9,$6
# 0 "" 2
# 669 "vp6.c" 1
.word	0b01110000000001000011111101011100	#Q8ADDE XR13,XR15,XR0,XR1,AA
# 0 "" 2
# 670 "vp6.c" 1
.word	0b01110000000001010100111101111010	#Q8MAC XR13,XR3,XR5,XR1,AA
# 0 "" 2
# 671 "vp6.c" 1
.word	0b01110000000001011001001101111010	#Q8MAC XR13,XR4,XR6,XR1,AA
# 0 "" 2
# 672 "vp6.c" 1
.word	0b01110000000001011110111101111010	#Q8MAC XR13,XR11,XR7,XR1,AA
# 0 "" 2
# 674 "vp6.c" 1
.word	0b01110000000001100011001101111010	#Q8MAC XR13,XR12,XR8,XR1,AA
# 0 "" 2
# 677 "vp6.c" 1
.word	0b01110000100000000000100011010000	#S32LDD XR3,$4,8
# 0 "" 2
# 678 "vp6.c" 1
.word	0b01110000011000000000101001010000	#S32LDD XR9,$3,8
# 0 "" 2
# 679 "vp6.c" 1
.word	0b01110001011001001000111011100111	#S32ALN XR11,XR3,XR2,$11
# 0 "" 2
# 680 "vp6.c" 1
.word	0b01110000110001001000110011100111	#S32ALN XR3,XR3,XR2,$6
# 0 "" 2
# 681 "vp6.c" 1
.word	0b01110001011001101010011100100111	#S32ALN XR12,XR9,XR10,$11
# 0 "" 2
# 682 "vp6.c" 1
.word	0b01110000110001101010011001100111	#S32ALN XR9,XR9,XR10,$6
# 0 "" 2
# 684 "vp6.c" 1
.word	0b01110000000010000011111110011100	#Q8ADDE XR14,XR15,XR0,XR2,AA
# 0 "" 2
# 685 "vp6.c" 1
.word	0b01110000000010010110111110111010	#Q8MAC XR14,XR11,XR5,XR2,AA
# 0 "" 2
# 686 "vp6.c" 1
.word	0b01110000000010011000111110111010	#Q8MAC XR14,XR3,XR6,XR2,AA
# 0 "" 2
# 687 "vp6.c" 1
.word	0b01110000000010011111001110111010	#Q8MAC XR14,XR12,XR7,XR2,AA
# 0 "" 2
# 688 "vp6.c" 1
.word	0b01110000000010100010011110111010	#Q8MAC XR14,XR9,XR8,XR2,AA
# 0 "" 2
# 690 "vp6.c" 1
.word	0b01110001100001000111011101110101	#Q16SLR XR13,XR13,XR1,XR1,6
# 0 "" 2
# 691 "vp6.c" 1
.word	0b01110001100010001011101110110101	#Q16SLR XR14,XR14,XR2,XR2,6
# 0 "" 2
# 692 "vp6.c" 1
.word	0b01110001001101000111010000111101	#S32SFL XR0,XR13,XR1,XR13,PTN1
# 0 "" 2
# 693 "vp6.c" 1
.word	0b01110001001110001011100000111101	#S32SFL XR0,XR14,XR2,XR14,PTN1
# 0 "" 2
# 695 "vp6.c" 1
.word	0b01110000010010000000001101010111	#S32SDIV XR13,$2,$8,0
# 0 "" 2
# 696 "vp6.c" 1
.word	0b01110000010000000000011110010001	#S32STD XR14,$2,4
# 0 "" 2
#NO_APP
addiu	$7,$7,-1
bne	$7,$0,$L164
sw	$12,16($sp)
move	$4,$9
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_chroma_mc8_c.constprop.13
.option	pic2
move	$6,$8
.set	macro
.set	reorder

lw	$31,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L201:
slt	$2,$11,$7
$L211:
movn	$7,$11,$2
$L155:
addu	$5,$6,$7
sw	$12,16($sp)
move	$4,$9
move	$6,$8
.option	pic0
.set	noreorder
.set	nomacro
jal	put_h264_chroma_mc8_c.constprop.13
.option	pic2
move	$7,$13
.set	macro
.set	reorder

lw	$31,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L158:
xor	$3,$2,$3
sra	$3,$3,31
lui	$10,%hi(vp6_block_copy_filter)
sll	$4,$15,3
addiu	$10,$10,%lo(vp6_block_copy_filter)
addu	$2,$4,$13
addu	$4,$4,$12
subu	$3,$3,$8
sll	$4,$4,3
sll	$2,$2,3
addu	$3,$3,$7
addu	$2,$10,$2
addu	$10,$10,$4
li	$4,64			# 0x40
#APP
# 708 "vp6.c" 1
.word	0b01110000000001000000001111101111	#S32I2M XR15,$4
# 0 "" 2
# 709 "vp6.c" 1
.word	0b01110000010000000000000001101010	#S16LDD XR1,$2,0,PTN0
# 0 "" 2
# 710 "vp6.c" 1
.word	0b01110000010010000000010001101010	#S16LDD XR1,$2,2,PTN1
# 0 "" 2
# 711 "vp6.c" 1
.word	0b01110000010000000000100010101010	#S16LDD XR2,$2,4,PTN0
# 0 "" 2
# 712 "vp6.c" 1
.word	0b01110000010010000000110010101010	#S16LDD XR2,$2,6,PTN1
# 0 "" 2
#NO_APP
addiu	$3,$3,-2
addiu	$11,$sp,30
li	$5,11			# 0xb
addu	$4,$6,$3
move	$3,$11
$L162:
move	$2,$4
#APP
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
# 718 "vp6.c" 1
.word	0b01110000010000000000011110100100	#S8LDI XR14,$2,1,PTN0
# 0 "" 2
# 719 "vp6.c" 1
.word	0b01110000010001000000011110100010	#S8LDD XR14,$2,1,PTN1
# 0 "" 2
# 720 "vp6.c" 1
.word	0b01110000010010000000101110100010	#S8LDD XR14,$2,2,PTN2
# 0 "" 2
# 721 "vp6.c" 1
.word	0b01110000010011000000111110100010	#S8LDD XR14,$2,3,PTN3
# 0 "" 2
# 722 "vp6.c" 1
.word	0b01110000000011111000000100111101	#S32SFL XR4,XR0,XR14,XR3,PTN0
# 0 "" 2
# 723 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 724 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 725 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 726 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 727 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 728 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 729 "vp6.c" 1
.word	0b01110000011000000000010111101101	#S16SDI XR7,$3,2,PTN0
# 0 "" 2
#NO_APP
addiu	$5,$5,-1
.set	noreorder
.set	nomacro
bne	$5,$0,$L162
addu	$4,$4,$8
.set	macro
.set	reorder

#APP
# 734 "vp6.c" 1
.word	0b01110001010000000000000001101010	#S16LDD XR1,$10,0,PTN0
# 0 "" 2
# 735 "vp6.c" 1
.word	0b01110001010010000000010001101010	#S16LDD XR1,$10,2,PTN1
# 0 "" 2
# 736 "vp6.c" 1
.word	0b01110001010000000000100010101010	#S16LDD XR2,$10,4,PTN0
# 0 "" 2
# 737 "vp6.c" 1
.word	0b01110001010010000000110010101010	#S16LDD XR2,$10,6,PTN1
# 0 "" 2
#NO_APP
li	$4,8			# 0x8
addiu	$9,$9,-1
move	$2,$11
$L163:
#APP
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
#NO_APP
move	$3,$9
#APP
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
# 743 "vp6.c" 1
.word	0b01110000010000000000010011101100	#S16LDI XR3,$2,2,PTN0
# 0 "" 2
# 744 "vp6.c" 1
.word	0b01110000010010000010000011101010	#S16LDD XR3,$2,16,PTN1
# 0 "" 2
# 745 "vp6.c" 1
.word	0b01110000010000000100000100101010	#S16LDD XR4,$2,32,PTN0
# 0 "" 2
# 746 "vp6.c" 1
.word	0b01110000010010000110000100101010	#S16LDD XR4,$2,48,PTN1
# 0 "" 2
# 747 "vp6.c" 1
.word	0b01110000000110001100010101001000	#D16MUL XR5,XR1,XR3,XR6,WW
# 0 "" 2
# 748 "vp6.c" 1
.word	0b01110000000110010000100101001010	#D16MAC XR5,XR2,XR4,XR6,AA,WW
# 0 "" 2
# 749 "vp6.c" 1
.word	0b01110000000111011001010000011000	#D32ADD XR0,XR5,XR6,XR7,AA
# 0 "" 2
# 750 "vp6.c" 1
.word	0b01110000000111000011110000011001	#D32ACC XR0,XR15,XR0,XR7,AA
# 0 "" 2
# 751 "vp6.c" 1
.word	0b01110001110111011100000000110011	#D32SAR XR0,XR0,XR7,XR7,7
# 0 "" 2
# 752 "vp6.c" 1
.word	0b01110000000110011100000111000111	#Q16SAT XR7,XR0,XR7
# 0 "" 2
# 753 "vp6.c" 1
.word	0b01110000011000000000010111100101	#S8SDI XR7,$3,1,PTN0
# 0 "" 2
#NO_APP
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$4,$0,$L163
addu	$9,$9,$8
.set	macro
.set	reorder

lw	$31,212($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,216
.set	macro
.set	reorder

$L208:
slt	$14,$2,0
movz	$15,$2,$14
slt	$10,$10,$15
bne	$10,$0,$L148
$L147:
lw	$14,6232($4)
.set	noreorder
.set	nomacro
bne	$14,$0,$L209
addu	$15,$6,$7
.set	macro
.set	reorder

$L151:
.set	noreorder
.set	nomacro
beq	$12,$0,$L210
subu	$10,$11,$7
.set	macro
.set	reorder

lw	$14,6500($4)
mul	$10,$10,$14
bgez	$10,$L153
.option	pic0
.set	noreorder
.set	nomacro
j	$L165
.option	pic2
move	$7,$11
.set	macro
.set	reorder

$L210:
slt	$2,$11,$7
.option	pic0
.set	noreorder
.set	nomacro
j	$L167
.option	pic2
movn	$7,$11,$2
.set	macro
.set	reorder

$L209:
li	$10,-4			# 0xfffffffffffffffc
andi	$25,$15,0x3
sll	$24,$8,1
and	$10,$15,$10
li	$15,4			# 0x4
subu	$10,$10,$24
subu	$15,$15,$25
#APP
# 522 "vp6.c" 1
.word	0b01110000011111000000001110110000	#D32SLL XR14,XR0,XR0,XR15,1
# 0 "" 2
# 523 "vp6.c" 1
.word	0b01110000011101000000001100110000	#D32SLL XR12,XR0,XR0,XR13,1
# 0 "" 2
# 525 "vp6.c" 1
.word	0b01110001010110000000000001010110	#S32LDIV XR1,$10,$24,0
# 0 "" 2
# 526 "vp6.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 527 "vp6.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 528 "vp6.c" 1
.word	0b01110001010110000000000110010110	#S32LDIV XR6,$10,$24,0
# 0 "" 2
# 529 "vp6.c" 1
.word	0b01110001010000000000010111010000	#S32LDD XR7,$10,4
# 0 "" 2
# 530 "vp6.c" 1
.word	0b01110001010000000000101000010000	#S32LDD XR8,$10,8
# 0 "" 2
# 532 "vp6.c" 1
.word	0b01110001111001000100100001100111	#S32ALN XR1,XR2,XR1,$15
# 0 "" 2
# 533 "vp6.c" 1
.word	0b01110001111001001000110010100111	#S32ALN XR2,XR3,XR2,$15
# 0 "" 2
# 535 "vp6.c" 1
.word	0b01110001111001011001110110100111	#S32ALN XR6,XR7,XR6,$15
# 0 "" 2
# 536 "vp6.c" 1
.word	0b01110001111001011110000111100111	#S32ALN XR7,XR8,XR7,$15
# 0 "" 2
# 538 "vp6.c" 1
.word	0b01110001000001000100100000111101	#S32SFL XR0,XR2,XR1,XR1,PTN1
# 0 "" 2
# 539 "vp6.c" 1
.word	0b01110001000110011001110000111101	#S32SFL XR0,XR7,XR6,XR6,PTN1
# 0 "" 2
# 541 "vp6.c" 1
.word	0b01110000000011000100010010111000	#Q8MUL XR2,XR1,XR1,XR3
# 0 "" 2
# 542 "vp6.c" 1
.word	0b01110000001000011001100111111000	#Q8MUL XR7,XR6,XR6,XR8
# 0 "" 2
# 545 "vp6.c" 1
.word	0b01110000001100000000010000111110	#Q8SAD XR0,XR1,XR0,XR12
# 0 "" 2
# 546 "vp6.c" 1
.word	0b01110000001100000001100000111110	#Q8SAD XR0,XR6,XR0,XR12
# 0 "" 2
# 548 "vp6.c" 1
.word	0b01110011000010001000000100111101	#S32SFL XR4,XR0,XR2,XR2,PTN3
# 0 "" 2
# 550 "vp6.c" 1
.word	0b01110011000011001100000101111101	#S32SFL XR5,XR0,XR3,XR3,PTN3
# 0 "" 2
# 552 "vp6.c" 1
.word	0b01110000101111001100101110011001	#D32ASUM XR14,XR2,XR3,XR15,AA
# 0 "" 2
# 553 "vp6.c" 1
.word	0b01110000101111010101001110011001	#D32ASUM XR14,XR4,XR5,XR15,AA
# 0 "" 2
# 555 "vp6.c" 1
.word	0b01110011000111011100000100111101	#S32SFL XR4,XR0,XR7,XR7,PTN3
# 0 "" 2
# 557 "vp6.c" 1
.word	0b01110011001000100000000101111101	#S32SFL XR5,XR0,XR8,XR8,PTN3
# 0 "" 2
# 559 "vp6.c" 1
.word	0b01110000101111100001111110011001	#D32ASUM XR14,XR7,XR8,XR15,AA
# 0 "" 2
# 560 "vp6.c" 1
.word	0b01110000101111010101001110011001	#D32ASUM XR14,XR4,XR5,XR15,AA
# 0 "" 2
# 525 "vp6.c" 1
.word	0b01110001010110000000000001010110	#S32LDIV XR1,$10,$24,0
# 0 "" 2
# 526 "vp6.c" 1
.word	0b01110001010000000000010010010000	#S32LDD XR2,$10,4
# 0 "" 2
# 527 "vp6.c" 1
.word	0b01110001010000000000100011010000	#S32LDD XR3,$10,8
# 0 "" 2
# 528 "vp6.c" 1
.word	0b01110001010110000000000110010110	#S32LDIV XR6,$10,$24,0
# 0 "" 2
# 529 "vp6.c" 1
.word	0b01110001010000000000010111010000	#S32LDD XR7,$10,4
# 0 "" 2
# 530 "vp6.c" 1
.word	0b01110001010000000000101000010000	#S32LDD XR8,$10,8
# 0 "" 2
# 532 "vp6.c" 1
.word	0b01110001111001000100100001100111	#S32ALN XR1,XR2,XR1,$15
# 0 "" 2
# 533 "vp6.c" 1
.word	0b01110001111001001000110010100111	#S32ALN XR2,XR3,XR2,$15
# 0 "" 2
# 535 "vp6.c" 1
.word	0b01110001111001011001110110100111	#S32ALN XR6,XR7,XR6,$15
# 0 "" 2
# 536 "vp6.c" 1
.word	0b01110001111001011110000111100111	#S32ALN XR7,XR8,XR7,$15
# 0 "" 2
# 538 "vp6.c" 1
.word	0b01110001000001000100100000111101	#S32SFL XR0,XR2,XR1,XR1,PTN1
# 0 "" 2
# 539 "vp6.c" 1
.word	0b01110001000110011001110000111101	#S32SFL XR0,XR7,XR6,XR6,PTN1
# 0 "" 2
# 541 "vp6.c" 1
.word	0b01110000000011000100010010111000	#Q8MUL XR2,XR1,XR1,XR3
# 0 "" 2
# 542 "vp6.c" 1
.word	0b01110000001000011001100111111000	#Q8MUL XR7,XR6,XR6,XR8
# 0 "" 2
# 545 "vp6.c" 1
.word	0b01110000001100000000010000111110	#Q8SAD XR0,XR1,XR0,XR12
# 0 "" 2
# 546 "vp6.c" 1
.word	0b01110000001100000001100000111110	#Q8SAD XR0,XR6,XR0,XR12
# 0 "" 2
# 548 "vp6.c" 1
.word	0b01110011000010001000000100111101	#S32SFL XR4,XR0,XR2,XR2,PTN3
# 0 "" 2
# 550 "vp6.c" 1
.word	0b01110011000011001100000101111101	#S32SFL XR5,XR0,XR3,XR3,PTN3
# 0 "" 2
# 552 "vp6.c" 1
.word	0b01110000101111001100101110011001	#D32ASUM XR14,XR2,XR3,XR15,AA
# 0 "" 2
# 553 "vp6.c" 1
.word	0b01110000101111010101001110011001	#D32ASUM XR14,XR4,XR5,XR15,AA
# 0 "" 2
# 555 "vp6.c" 1
.word	0b01110011000111011100000100111101	#S32SFL XR4,XR0,XR7,XR7,PTN3
# 0 "" 2
# 557 "vp6.c" 1
.word	0b01110011001000100000000101111101	#S32SFL XR5,XR0,XR8,XR8,PTN3
# 0 "" 2
# 559 "vp6.c" 1
.word	0b01110000101111100001111110011001	#D32ASUM XR14,XR7,XR8,XR15,AA
# 0 "" 2
# 560 "vp6.c" 1
.word	0b01110000101111010101001110011001	#D32ASUM XR14,XR4,XR5,XR15,AA
# 0 "" 2
# 563 "vp6.c" 1
.word	0b01110000000011110000001100101110	#S32M2I XR12, $15
# 0 "" 2
# 564 "vp6.c" 1
.word	0b01110000000000111111101010011000	#D32ADD XR10,XR14,XR15,XR0,AA
# 0 "" 2
# 565 "vp6.c" 1
.word	0b01110001000000000010101010110000	#D32SLL XR10,XR10,XR0,XR0,4
# 0 "" 2
# 567 "vp6.c" 1
.word	0b01110000000110000000001010101110	#S32M2I XR10, $24
# 0 "" 2
#NO_APP
mul	$15,$15,$15
subu	$10,$24,$15
sra	$10,$10,8
slt	$14,$10,$14
beq	$14,$0,$L151
bne	$12,$0,$L170
.option	pic0
.set	noreorder
.set	nomacro
j	$L211
.option	pic2
slt	$2,$11,$7
.set	macro
.set	reorder

.end	vp6_filter
.size	vp6_filter, .-vp6_filter
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_get_nb_null
.type	vp6_get_nb_null, @function
vp6_get_nb_null:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$6,10320($4)
li	$8,16711680			# 0xff0000
lw	$10,10312($4)
li	$7,-16777216			# 0xffffffffff000000
addiu	$8,$8,255
srl	$3,$6,3
ori	$7,$7,0xff00
addu	$3,$10,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$8
and	$5,$5,$7
or	$5,$3,$5
sll	$3,$5,16
srl	$5,$5,16
andi	$11,$6,0x7
or	$2,$3,$5
sll	$2,$2,$11
addiu	$9,$6,2
srl	$2,$2,30
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$3,$L215
sw	$9,10320($4)
.set	macro
.set	reorder

li	$3,3			# 0x3
beq	$2,$3,$L216
j	$31
$L216:
srl	$2,$9,3
addiu	$6,$6,3
addu	$2,$10,$2
srl	$5,$6,3
andi	$9,$9,0x7
lbu	$3,0($2)
addu	$10,$10,$5
sw	$6,10320($4)
andi	$2,$6,0x7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($10)  
lwr $5, 0($10)  

# 0 "" 2
#NO_APP
move	$10,$5
sll	$3,$3,$9
srl	$5,$5,8
sll	$10,$10,8
and	$8,$5,$8
and	$7,$10,$7
srl	$3,$3,5
or	$7,$8,$7
sll	$5,$7,16
andi	$3,$3,0x4
srl	$7,$7,16
addiu	$8,$3,2
or	$7,$5,$7
sll	$7,$7,$2
subu	$2,$0,$8
addu	$8,$8,$6
addiu	$3,$3,6
srl	$2,$7,$2
sw	$8,10320($4)
.set	noreorder
.set	nomacro
j	$31
addu	$2,$3,$2
.set	macro
.set	reorder

$L215:
srl	$2,$9,3
andi	$9,$9,0x7
addu	$10,$10,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($10)  
lwr $3, 0($10)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$8,$2,$8
and	$2,$3,$7
or	$2,$8,$2
sll	$5,$2,16
srl	$2,$2,16
addiu	$6,$6,4
or	$2,$5,$2
sll	$2,$2,$9
sw	$6,10320($4)
srl	$2,$2,30
.set	noreorder
.set	nomacro
j	$31
addiu	$2,$2,2
.set	macro
.set	reorder

.end	vp6_get_nb_null
.size	vp6_get_nb_null, .-vp6_get_nb_null
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_parse_coeff_huffman
.type	vp6_parse_coeff_huffman, @function
vp6_parse_coeff_huffman:
.frame	$sp,80,$31		# vars= 16, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
lw	$6,6560($4)
addiu	$sp,$sp,-80
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$21,60($sp)
lui	$21,%hi(vp6_coeff_groups)
sw	$19,52($sp)
li	$19,16711680			# 0xff0000
sw	$fp,72($sp)
move	$8,$0
sw	$22,64($sp)
addiu	$fp,$4,4140
sw	$20,56($sp)
move	$22,$0
sw	$18,48($sp)
move	$20,$0
sw	$31,76($sp)
addiu	$21,$21,%lo(vp6_coeff_groups)
sw	$23,68($sp)
addiu	$19,$19,255
sw	$17,44($sp)
move	$18,$4
sw	$16,40($sp)
.cprestore	16
lw	$7,%got(vp56_coeff_bias)($28)
$L218:
addiu	$2,$22,10328
addu	$2,$18,$2
#APP
# 375 "vp6.c" 1
.word	0b01110000000000000000001011000111	#S32CPS XR11,XR0,XR0
# 0 "" 2
#NO_APP
addiu	$16,$20,2742
sll	$17,$20,5
sll	$3,$20,8
sll	$16,$16,2
sll	$23,$8,6
move	$9,$0
move	$4,$0
addu	$17,$17,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L220
.option	pic2
addu	$16,$18,$16
.set	macro
.set	reorder

$L267:
.set	noreorder
.set	nomacro
beq	$4,$0,$L229
srl	$2,$5,3
.set	macro
.set	reorder

li	$11,-16777216			# 0xffffffffff000000
addu	$2,$9,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($2)  
lwr $12, 0($2)  

# 0 "" 2
#NO_APP
srl	$10,$12,8
sll	$2,$12,8
ori	$11,$11,0xff00
and	$10,$10,$19
and	$2,$2,$11
or	$2,$10,$2
slt	$12,$4,6
sll	$10,$2,16
xori	$12,$12,0x1
srl	$2,$2,16
sll	$12,$12,4
or	$3,$10,$2
andi	$2,$5,0x7
sll	$2,$3,$2
addu	$10,$18,$12
srl	$2,$2,23
lw	$10,10364($10)
sll	$2,$2,2
addu	$2,$10,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L264
lh	$2,0($2)
.set	macro
.set	reorder

$L231:
addu	$5,$3,$5
addiu	$2,$2,1
slt	$3,$2,9
.set	noreorder
.set	nomacro
beq	$3,$0,$L265
sw	$5,10320($18)
.set	macro
.set	reorder

move	$9,$0
$L233:
addu	$4,$4,$2
addu	$2,$4,$21
lbu	$2,0($2)
sltu	$3,$2,4
.set	noreorder
.set	nomacro
bne	$3,$0,$L269
sll	$10,$9,1
.set	macro
.set	reorder

li	$2,3			# 0x3
$L241:
sll	$10,$9,1
$L269:
sll	$5,$9,3
sll	$3,$20,1
subu	$5,$5,$10
addu	$3,$3,$22
slt	$10,$4,64
addu	$3,$5,$3
addu	$2,$3,$2
sll	$2,$2,4
addiu	$2,$2,10392
.set	noreorder
.set	nomacro
beq	$10,$0,$L223
addu	$2,$18,$2
.set	macro
.set	reorder

$L220:
slt	$3,$4,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L270
sll	$3,$4,1
.set	macro
.set	reorder

$L221:
lw	$5,10320($18)
lw	$3,10324($18)
slt	$3,$5,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L217
srl	$3,$5,3
.set	macro
.set	reorder

lw	$9,10312($18)
lw	$11,4($2)
li	$10,-16777216			# 0xffffffffff000000
andi	$12,$5,0x7
addu	$2,$9,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$2,$2,8
srl	$3,$3,8
ori	$10,$10,0xff00
and	$3,$3,$19
and	$2,$2,$10
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$12
srl	$2,$2,23
sll	$2,$2,2
addu	$2,$11,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L266
lh	$2,0($2)
.set	macro
.set	reorder

$L226:
addu	$5,$3,$5
move	$10,$5
.set	noreorder
.set	nomacro
beq	$2,$0,$L267
sw	$5,10320($18)
.set	macro
.set	reorder

li	$3,11			# 0xb
.set	noreorder
.set	nomacro
beq	$2,$3,$L268
addu	$3,$7,$2
.set	macro
.set	reorder

slt	$11,$2,5
.set	noreorder
.set	nomacro
beq	$11,$0,$L236
lbu	$3,0($3)
.set	macro
.set	reorder

move	$2,$3
$L237:
srl	$5,$10,3
andi	$3,$10,0x7
addu	$9,$9,$5
addiu	$10,$10,1
slt	$11,$2,2
lbu	$5,0($9)
li	$9,2			# 0x2
sw	$10,10320($18)
li	$10,1			# 0x1
sll	$3,$5,$3
movn	$9,$10,$11
andi	$3,$3,0x00ff
srl	$3,$3,7
subu	$5,$0,$3
xor	$2,$5,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L240
addu	$3,$2,$3
.set	macro
.set	reorder

lhu	$2,5286($18)
mul	$3,$3,$2
$L240:
addu	$2,$6,$4
lbu	$2,64($2)
addu	$2,$fp,$2
lbu	$2,0($2)
#APP
# 409 "vp6.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
# 410 "vp6.c" 1
.word	0b01110000000000101010111011000011	#S32MAX XR11,XR11,XR10
# 0 "" 2
#NO_APP
lw	$10,%got(blk_coeff)($28)
addu	$5,$23,$2
li	$2,1			# 0x1
sll	$5,$5,1
lw	$10,0($10)
addu	$5,$10,$5
.option	pic0
.set	noreorder
.set	nomacro
j	$L233
.option	pic2
sh	$3,0($5)
.set	macro
.set	reorder

$L260:
sll	$3,$4,1
$L270:
addu	$3,$20,$3
sll	$3,$3,2
addu	$3,$18,$3
lw	$5,10968($3)
.set	noreorder
.set	nomacro
beq	$5,$0,$L221
addiu	$5,$5,-1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$4,$0,$L223
sw	$5,10968($3)
.set	macro
.set	reorder

$L222:
lbu	$2,1($21)
sltu	$3,$2,4
.set	noreorder
.set	nomacro
bne	$3,$0,$L241
li	$4,1			# 0x1
.set	macro
.set	reorder

sll	$3,$9,5
sll	$2,$9,7
subu	$2,$2,$3
addu	$2,$2,$17
addiu	$2,$2,10440
.option	pic0
.set	noreorder
.set	nomacro
j	$L260
.option	pic2
addu	$2,$18,$2
.set	macro
.set	reorder

$L266:
addiu	$5,$5,9
srl	$12,$5,3
andi	$15,$5,0x7
addu	$12,$9,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($12)  
lwr $14, 0($12)  

# 0 "" 2
#NO_APP
srl	$13,$14,8
sll	$14,$14,8
and	$13,$13,$19
and	$14,$14,$10
or	$14,$13,$14
sll	$13,$14,16
srl	$14,$14,16
or	$12,$13,$14
sll	$12,$12,$15
srl	$12,$12,$3
addu	$2,$12,$2
sll	$2,$2,2
addu	$2,$11,$2
lh	$13,2($2)
.set	noreorder
.set	nomacro
bltz	$13,$L227
lh	$2,0($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L226
.option	pic2
move	$3,$13
.set	macro
.set	reorder

$L236:
slt	$10,$2,10
.set	noreorder
.set	nomacro
beq	$10,$0,$L248
li	$11,36			# 0x24
.set	macro
.set	reorder

addiu	$10,$2,-4
subu	$2,$11,$2
$L238:
srl	$11,$5,3
li	$13,-16777216			# 0xffffffffff000000
addu	$11,$9,$11
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($11)  
lwr $12, 0($11)  

# 0 "" 2
#NO_APP
move	$11,$12
sll	$11,$11,8
srl	$12,$12,8
ori	$13,$13,0xff00
and	$12,$12,$19
and	$11,$11,$13
or	$11,$12,$11
sll	$12,$11,16
srl	$11,$11,16
andi	$13,$5,0x7
or	$11,$12,$11
sll	$12,$11,$13
addu	$10,$10,$5
srl	$11,$12,$2
sw	$10,10320($18)
.option	pic0
.set	noreorder
.set	nomacro
j	$L237
.option	pic2
addu	$2,$11,$3
.set	macro
.set	reorder

$L229:
move	$4,$18
sw	$6,24($sp)
sw	$7,32($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	vp6_get_nb_null
.option	pic2
sw	$8,28($sp)
.set	macro
.set	reorder

move	$9,$0
lw	$28,16($sp)
lw	$6,24($sp)
lw	$7,32($sp)
lw	$8,28($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L222
.option	pic2
sw	$2,0($16)
.set	macro
.set	reorder

$L248:
li	$10,11			# 0xb
.option	pic0
.set	noreorder
.set	nomacro
j	$L238
.option	pic2
li	$2,21			# 0x15
.set	macro
.set	reorder

$L265:
srl	$3,$5,3
li	$10,-16777216			# 0xffffffffff000000
addu	$9,$9,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($9)  
lwr $3, 0($9)  

# 0 "" 2
#NO_APP
srl	$9,$3,8
sll	$3,$3,8
ori	$10,$10,0xff00
and	$9,$9,$19
and	$3,$3,$10
or	$3,$9,$3
sll	$9,$3,16
srl	$3,$3,16
andi	$10,$5,0x7
or	$3,$9,$3
sll	$3,$3,$10
addiu	$5,$5,6
srl	$3,$3,26
move	$9,$0
sw	$5,10320($18)
.option	pic0
.set	noreorder
.set	nomacro
j	$L233
.option	pic2
addu	$2,$3,$2
.set	macro
.set	reorder

$L264:
addiu	$5,$5,9
srl	$12,$5,3
andi	$15,$5,0x7
addu	$12,$9,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($12)  
lwr $14, 0($12)  

# 0 "" 2
#NO_APP
srl	$13,$14,8
sll	$14,$14,8
and	$13,$13,$19
and	$14,$14,$11
or	$14,$13,$14
sll	$13,$14,16
srl	$14,$14,16
or	$12,$13,$14
sll	$12,$12,$15
srl	$12,$12,$3
addu	$2,$12,$2
sll	$2,$2,2
addu	$2,$10,$2
lh	$13,2($2)
.set	noreorder
.set	nomacro
bltz	$13,$L232
lh	$2,0($2)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
move	$3,$13
.set	macro
.set	reorder

$L227:
subu	$5,$5,$3
srl	$12,$5,3
andi	$3,$5,0x7
addu	$12,$9,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($12)  
lwr $14, 0($12)  

# 0 "" 2
#NO_APP
srl	$12,$14,8
sll	$14,$14,8
and	$12,$12,$19
and	$10,$14,$10
or	$10,$12,$10
sll	$12,$10,16
srl	$10,$10,16
or	$10,$12,$10
sll	$3,$10,$3
srl	$3,$3,$13
addu	$2,$3,$2
sll	$2,$2,2
addu	$11,$11,$2
lh	$2,0($11)
.option	pic0
.set	noreorder
.set	nomacro
j	$L226
.option	pic2
lh	$3,2($11)
.set	macro
.set	reorder

$L268:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$4,$2,$L223
move	$4,$18
.set	macro
.set	reorder

sw	$6,24($sp)
sw	$7,32($sp)
.option	pic0
.set	noreorder
.set	nomacro
jal	vp6_get_nb_null
.option	pic2
sw	$8,28($sp)
.set	macro
.set	reorder

addiu	$3,$20,2744
lw	$6,24($sp)
sll	$3,$3,2
lw	$7,32($sp)
lw	$8,28($sp)
addu	$3,$18,$3
lw	$28,16($sp)
sw	$2,0($3)
$L223:
#APP
# 418 "vp6.c" 1
.word	0b01110000000000100000001011101110	#S32M2I XR11, $2
# 0 "" 2
#NO_APP
sra	$2,$2,3
lw	$3,%got(idct_row)($28)
addiu	$2,$2,1
addu	$3,$3,$8
addiu	$8,$8,1
sb	$2,0($3)
li	$2,6			# 0x6
.set	noreorder
.set	nomacro
beq	$8,$2,$L217
li	$3,1			# 0x1
.set	macro
.set	reorder

slt	$2,$8,4
movz	$20,$3,$2
li	$3,16			# 0x10
.option	pic0
.set	noreorder
.set	nomacro
j	$L218
.option	pic2
movz	$22,$3,$2
.set	macro
.set	reorder

$L217:
lw	$31,76($sp)
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L232:
subu	$5,$5,$3
srl	$12,$5,3
andi	$3,$5,0x7
addu	$12,$9,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($12)  
lwr $14, 0($12)  

# 0 "" 2
#NO_APP
srl	$12,$14,8
sll	$14,$14,8
and	$12,$12,$19
and	$11,$14,$11
or	$11,$12,$11
sll	$12,$11,16
srl	$11,$11,16
or	$11,$12,$11
sll	$3,$11,$3
srl	$3,$3,$13
addu	$2,$3,$2
sll	$2,$2,2
addu	$10,$10,$2
lh	$2,0($10)
.option	pic0
.set	noreorder
.set	nomacro
j	$L231
.option	pic2
lh	$3,2($10)
.set	macro
.set	reorder

.end	vp6_parse_coeff_huffman
.size	vp6_parse_coeff_huffman, .-vp6_parse_coeff_huffman
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"interlacing not supported\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_parse_header
.type	vp6_parse_header, @function
vp6_parse_header:
.frame	$sp,56,$31		# vars= 0, regs= 8/0, args= 16, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-56
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$21,44($sp)
sw	$20,40($sp)
move	$20,$7
sw	$19,36($sp)
addiu	$19,$4,5168
sw	$18,32($sp)
move	$18,$6
sw	$17,28($sp)
move	$17,$5
sw	$16,24($sp)
move	$16,$4
.cprestore	16
sw	$31,52($sp)
sw	$22,48($sp)
lbu	$3,0($5)
lw	$5,5136($4)
lw	$25,%call16(ff_vp56_init_dequant)($28)
xori	$2,$3,0x80
srl	$2,$2,7
andi	$21,$3,0x1
sw	$2,48($5)
lbu	$5,0($17)
srl	$5,$5,1
.reloc	1f,R_MIPS_JALR,ff_vp56_init_dequant
1:	jalr	$25
andi	$5,$5,0x3f

lw	$2,5136($16)
lw	$2,48($2)
beq	$2,$0,$L272
lw	$28,16($sp)

lbu	$2,1($17)
srl	$22,$2,3
sltu	$3,$22,9
beq	$3,$0,$L364
andi	$2,$2,0x6

sw	$2,6212($16)
lbu	$3,1($17)
andi	$3,$3,0x1
bne	$3,$0,$L365
lui	$6,%hi($LC0)

beq	$21,$0,$L366
nop

$L276:
lbu	$2,3($17)
addiu	$18,$18,-2
lbu	$3,2($17)
addiu	$17,$17,2
sll	$2,$2,8
or	$2,$2,$3
sll	$21,$2,8
srl	$2,$2,8
or	$2,$21,$2
andi	$21,$2,0xffff
addiu	$21,$21,-2
lbu	$5,3($17)
$L387:
lw	$2,5392($16)
lbu	$6,2($17)
lw	$4,0($16)
beq	$2,$0,$L363
sll	$5,$5,4

lw	$2,660($4)
beq	$5,$2,$L280
nop

$L363:
sll	$6,$6,4
lw	$25,%call16(avcodec_set_dimensions)($28)
$L380:
.reloc	1f,R_MIPS_JALR,avcodec_set_dimensions
1:	jalr	$25
nop

li	$3,1			# 0x1
lw	$2,0($16)
lw	$4,28($2)
beq	$4,$3,$L367
lw	$28,16($sp)

li	$20,2			# 0x2
$L281:
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
addiu	$5,$17,6
addiu	$6,$18,-6
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
move	$4,$19

lw	$28,16($sp)
lw	$2,5168($16)
lw	$4,5172($16)
lw	$5,5184($16)
lw	$6,%got(ff_vp56_norm_shift)($28)
addu	$3,$6,$2
lbu	$3,0($3)
sll	$8,$2,$3
addu	$4,$3,$4
sll	$3,$5,$3
bltz	$4,$L282
sw	$8,5168($16)

lw	$2,5176($16)
lw	$5,5180($16)
sltu	$5,$2,$5
beq	$5,$0,$L282
addiu	$5,$2,2

sw	$5,5176($16)
lbu	$5,1($2)
lbu	$2,0($2)
sll	$5,$5,8
or	$5,$5,$2
sll	$7,$5,8
srl	$5,$5,8
or	$5,$7,$5
andi	$5,$5,0xffff
sll	$5,$5,$4
addiu	$4,$4,-16
or	$3,$3,$5
$L282:
addiu	$2,$8,1
sra	$2,$2,1
sll	$5,$2,16
sltu	$7,$3,$5
bne	$7,$0,$L283
sw	$4,5172($16)

subu	$2,$8,$2
subu	$3,$3,$5
$L283:
addu	$5,$6,$2
sw	$3,5184($16)
lbu	$5,0($5)
sll	$7,$2,$5
addu	$4,$4,$5
sll	$5,$3,$5
bltz	$4,$L284
sw	$7,5168($16)

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
beq	$2,$0,$L284
addiu	$2,$3,2

sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L284:
addiu	$3,$7,1
sra	$3,$3,1
sll	$2,$3,16
sltu	$8,$5,$2
bne	$8,$0,$L285
sw	$4,5172($16)

subu	$3,$7,$3
subu	$5,$5,$2
$L285:
lw	$7,6212($16)
move	$2,$5
sw	$5,5184($16)
li	$5,5			# 0x5
xori	$10,$22,0x8
sw	$3,5168($16)
movz	$5,$0,$10
sw	$22,5212($16)
move	$10,$5
$L287:
beq	$7,$0,$L301
addu	$5,$6,$3

lbu	$9,0($5)
sll	$7,$3,$9
addu	$5,$9,$4
sll	$9,$2,$9
bltz	$5,$L302
sw	$7,5168($16)

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
beq	$2,$0,$L302
addiu	$2,$3,2

sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$5
addiu	$5,$5,-16
or	$9,$9,$2
$L302:
addiu	$3,$7,1
sra	$3,$3,1
sll	$2,$3,16
sltu	$4,$9,$2
bne	$4,$0,$L303
sw	$5,5172($16)

subu	$9,$9,$2
li	$2,2			# 0x2
subu	$3,$7,$3
li	$11,5			# 0x5
sw	$9,5184($16)
move	$4,$0
.option	pic0
j	$L306
.option	pic2
sw	$2,6224($16)

$L369:
subu	$7,$7,$9
$L305:
addiu	$11,$11,-1
sw	$3,5168($16)
or	$4,$4,$8
sw	$7,5184($16)
move	$5,$2
beq	$11,$0,$L368
move	$9,$7

$L306:
addu	$2,$6,$3
sll	$8,$4,1
lbu	$7,0($2)
sll	$3,$3,$7
addu	$2,$7,$5
addiu	$5,$3,1
sw	$3,5168($16)
sll	$7,$9,$7
bltz	$2,$L304
sra	$5,$5,1

lw	$9,5176($16)
lw	$4,5180($16)
sltu	$4,$9,$4
beq	$4,$0,$L304
addiu	$12,$9,2

sw	$12,5176($16)
lbu	$4,1($9)
lbu	$9,0($9)
sll	$4,$4,8
or	$9,$4,$9
sll	$4,$9,8
srl	$9,$9,8
or	$9,$4,$9
andi	$9,$9,0xffff
sll	$9,$9,$2
addiu	$2,$2,-16
or	$7,$7,$9
$L304:
sll	$9,$5,16
sw	$2,5172($16)
sltu	$4,$7,$9
xori	$4,$4,0x1
bne	$4,$0,$L369
subu	$3,$3,$5

.option	pic0
j	$L305
.option	pic2
move	$3,$5

$L365:
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)

$L364:
move	$2,$0
$L361:
lw	$31,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L272:
lw	$2,5212($16)
beq	$2,$0,$L361
move	$2,$0

beq	$21,$0,$L370
nop

$L289:
lbu	$2,2($17)
addiu	$18,$18,-2
lbu	$3,1($17)
addiu	$17,$17,2
sll	$2,$2,8
or	$2,$2,$3
sll	$21,$2,8
srl	$2,$2,8
or	$2,$21,$2
andi	$21,$2,0xffff
addiu	$21,$21,-2
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
$L386:
addiu	$5,$17,1
addiu	$6,$18,-1
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
move	$4,$19

lw	$28,16($sp)
lw	$2,5168($16)
lw	$7,5172($16)
lw	$4,5184($16)
lw	$6,%got(ff_vp56_norm_shift)($28)
addu	$3,$6,$2
lbu	$3,0($3)
sll	$5,$2,$3
addu	$7,$3,$7
sll	$4,$4,$3
bltz	$7,$L291
sw	$5,5168($16)

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
beq	$2,$0,$L376
addiu	$2,$5,1

addiu	$2,$3,2
sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$7
addiu	$7,$7,-16
or	$4,$4,$2
$L291:
addiu	$2,$5,1
$L376:
sw	$7,5172($16)
sra	$2,$2,1
sll	$7,$2,16
sltu	$3,$4,$7
xori	$3,$3,0x1
beq	$3,$0,$L292
nop

subu	$2,$5,$2
subu	$4,$4,$7
$L292:
sw	$2,5168($16)
sw	$4,5184($16)
sw	$3,0($20)
lw	$2,6212($16)
lw	$3,5168($16)
beq	$2,$0,$L293
lw	$4,5172($16)

addu	$2,$6,$3
lw	$5,5184($16)
lbu	$2,0($2)
sll	$7,$3,$2
addu	$4,$2,$4
sll	$8,$5,$2
bltz	$4,$L294
sw	$7,5168($16)

lw	$5,5176($16)
lw	$2,5180($16)
sltu	$2,$5,$2
beq	$2,$0,$L377
addiu	$3,$7,1

addiu	$2,$5,2
sw	$2,5176($16)
lbu	$3,1($5)
lbu	$2,0($5)
sll	$3,$3,8
or	$3,$3,$2
sll	$2,$3,8
srl	$3,$3,8
or	$3,$2,$3
andi	$2,$3,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$8,$8,$2
$L294:
addiu	$3,$7,1
$L377:
sra	$3,$3,1
sll	$5,$3,16
sltu	$2,$8,$5
xori	$2,$2,0x1
beq	$2,$0,$L295
sw	$4,5172($16)

subu	$3,$7,$3
sw	$2,6216($16)
subu	$5,$8,$5
addu	$2,$6,$3
sw	$5,5184($16)
lbu	$2,0($2)
sll	$7,$3,$2
addu	$4,$4,$2
sll	$5,$5,$2
bltz	$4,$L296
sw	$7,5168($16)

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
beq	$2,$0,$L296
addiu	$2,$3,2

sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L296:
addiu	$3,$7,1
sra	$3,$3,1
sll	$2,$3,16
sltu	$8,$5,$2
beq	$8,$0,$L371
sw	$4,5172($16)

$L297:
sw	$3,5168($16)
move	$2,$5
sw	$5,5184($16)
$L323:
lw	$5,5212($16)
slt	$5,$5,8
bne	$5,$0,$L301
li	$20,1			# 0x1

addu	$5,$6,$3
lbu	$5,0($5)
sll	$8,$3,$5
addu	$4,$5,$4
sll	$5,$2,$5
bltz	$4,$L299
sw	$8,5168($16)

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
beq	$2,$0,$L299
addiu	$2,$3,2

sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L299:
addiu	$3,$8,1
sra	$3,$3,1
sll	$2,$3,16
sltu	$7,$5,$2
xori	$7,$7,0x1
beq	$7,$0,$L300
sw	$4,5172($16)

subu	$3,$8,$3
subu	$5,$5,$2
$L300:
li	$20,1			# 0x1
sw	$3,5168($16)
move	$10,$0
sw	$5,5184($16)
.option	pic0
j	$L287
.option	pic2
move	$2,$5

$L293:
lw	$2,5184($16)
li	$20,1			# 0x1
$L301:
addu	$6,$6,$3
lbu	$5,0($6)
sll	$8,$3,$5
addu	$4,$5,$4
sll	$5,$2,$5
bltz	$4,$L318
sw	$8,5168($16)

lw	$2,5176($16)
lw	$6,5180($16)
sltu	$6,$2,$6
beq	$6,$0,$L378
addiu	$3,$8,1

addiu	$3,$2,2
sw	$3,5176($16)
lbu	$6,1($2)
lbu	$2,0($2)
sll	$6,$6,8
or	$6,$6,$2
sll	$7,$6,8
srl	$6,$6,8
or	$6,$7,$6
andi	$6,$6,0xffff
sll	$6,$6,$4
addiu	$4,$4,-16
or	$5,$5,$6
$L318:
addiu	$3,$8,1
$L378:
sw	$4,5172($16)
sra	$3,$3,1
sll	$4,$3,16
sltu	$2,$5,$4
xori	$2,$2,0x1
beq	$2,$0,$L319
nop

subu	$3,$8,$3
subu	$5,$5,$4
$L319:
sw	$3,5168($16)
lui	$3,%hi(vp6_parse_coeff)
sw	$5,5184($16)
addiu	$3,$3,%lo(vp6_parse_coeff)
sw	$2,10308($16)
beq	$21,$0,$L320
sw	$3,6540($16)

subu	$6,$18,$21
bltz	$6,$L364
addu	$5,$17,$21

beq	$2,$0,$L321
lui	$3,%hi(vp6_parse_coeff_huffman)

sll	$6,$6,3
addiu	$3,$3,%lo(vp6_parse_coeff_huffman)
sra	$2,$6,3
bltz	$2,$L350
sw	$3,6540($16)

bltz	$6,$L379
move	$3,$0

addu	$3,$5,$2
$L322:
lw	$31,52($sp)
move	$2,$20
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
sw	$5,10312($16)
sw	$6,10324($16)
sw	$3,10316($16)
sw	$0,10320($16)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L370:
lw	$2,6212($16)
beq	$2,$0,$L289
move	$21,$0

.option	pic0
j	$L386
.option	pic2
lw	$25,%call16(ff_vp56_init_range_decoder)($28)

$L366:
beq	$2,$0,$L276
move	$21,$0

.option	pic0
j	$L387
.option	pic2
lbu	$5,3($17)

$L280:
lw	$2,664($4)
sll	$6,$6,4
bne	$6,$2,$L380
lw	$25,%call16(avcodec_set_dimensions)($28)

.option	pic0
j	$L281
.option	pic2
li	$20,1			# 0x1

$L320:
lw	$31,52($sp)
move	$2,$20
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$18,32($sp)
lw	$17,28($sp)
sw	$19,5208($16)
lw	$19,36($sp)
lw	$16,24($sp)
j	$31
addiu	$sp,$sp,56

$L303:
addu	$2,$6,$3
sw	$9,5184($16)
lbu	$2,0($2)
sll	$3,$3,$2
addu	$7,$5,$2
sll	$9,$9,$2
bltz	$7,$L314
sw	$3,5168($16)

lw	$5,5176($16)
lw	$2,5180($16)
sltu	$2,$5,$2
beq	$2,$0,$L381
addiu	$4,$3,1

addiu	$2,$5,2
sw	$2,5176($16)
lbu	$4,1($5)
lbu	$2,0($5)
sll	$4,$4,8
or	$4,$4,$2
sll	$2,$4,8
srl	$4,$4,8
or	$4,$2,$4
andi	$2,$4,0xffff
sll	$2,$2,$7
addiu	$7,$7,-16
or	$9,$9,$2
$L314:
addiu	$4,$3,1
$L381:
sra	$4,$4,1
sll	$5,$4,16
sltu	$2,$9,$5
bne	$2,$0,$L315
sw	$7,5172($16)

subu	$4,$3,$4
subu	$2,$9,$5
li	$3,1			# 0x1
sw	$4,5168($16)
sw	$2,5184($16)
sw	$3,6224($16)
$L313:
lw	$3,5212($16)
slt	$3,$3,8
beq	$3,$0,$L372
li	$5,16			# 0x10

lw	$3,5168($16)
lw	$4,5172($16)
lw	$2,5184($16)
.option	pic0
j	$L301
.option	pic2
sw	$5,6220($16)

$L315:
sw	$4,5168($16)
move	$2,$9
sw	$9,5184($16)
.option	pic0
j	$L313
.option	pic2
sw	$0,6224($16)

$L372:
addu	$3,$6,$4
lbu	$5,0($3)
sll	$3,$4,$5
addu	$4,$5,$7
sll	$2,$2,$5
bltz	$4,$L326
sw	$3,5168($16)

lw	$7,5176($16)
lw	$5,5180($16)
sltu	$5,$7,$5
beq	$5,$0,$L326
addiu	$5,$7,2

sw	$5,5176($16)
lbu	$5,1($7)
lbu	$7,0($7)
sll	$5,$5,8
or	$5,$5,$7
sll	$7,$5,8
srl	$5,$5,8
or	$5,$7,$5
andi	$5,$5,0xffff
sll	$5,$5,$4
addiu	$4,$4,-16
or	$2,$2,$5
$L326:
addiu	$7,$3,1
sra	$7,$7,1
sll	$8,$7,16
sltu	$5,$2,$8
xori	$5,$5,0x1
bne	$5,$0,$L373
sw	$4,5172($16)

move	$3,$7
$L327:
addu	$7,$6,$3
sw	$2,5184($16)
sll	$9,$5,1
lbu	$7,0($7)
sll	$8,$3,$7
addu	$4,$4,$7
sll	$2,$2,$7
bltz	$4,$L328
sw	$8,5168($16)

lw	$5,5176($16)
lw	$3,5180($16)
sltu	$3,$5,$3
beq	$3,$0,$L382
addiu	$3,$8,1

addiu	$3,$5,2
sw	$3,5176($16)
lbu	$3,1($5)
lbu	$5,0($5)
sll	$3,$3,8
or	$3,$3,$5
sll	$5,$3,8
srl	$3,$3,8
or	$3,$5,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$2,$2,$3
$L328:
addiu	$3,$8,1
$L382:
sra	$3,$3,1
sll	$10,$3,16
sltu	$7,$2,$10
xori	$5,$7,0x1
beq	$5,$0,$L329
sw	$4,5172($16)

subu	$3,$8,$3
subu	$2,$2,$10
$L329:
addu	$7,$6,$3
sw	$2,5184($16)
or	$5,$5,$9
sll	$9,$5,1
lbu	$7,0($7)
sll	$8,$3,$7
addu	$4,$7,$4
sll	$2,$2,$7
bltz	$4,$L330
sw	$8,5168($16)

lw	$5,5176($16)
lw	$3,5180($16)
sltu	$3,$5,$3
beq	$3,$0,$L383
addiu	$3,$8,1

addiu	$3,$5,2
sw	$3,5176($16)
lbu	$3,1($5)
lbu	$5,0($5)
sll	$3,$3,8
or	$3,$3,$5
sll	$5,$3,8
srl	$3,$3,8
or	$3,$5,$3
andi	$3,$3,0xffff
sll	$3,$3,$4
addiu	$4,$4,-16
or	$2,$2,$3
$L330:
addiu	$3,$8,1
$L383:
sra	$3,$3,1
sll	$10,$3,16
sltu	$7,$2,$10
xori	$5,$7,0x1
beq	$5,$0,$L331
sw	$4,5172($16)

subu	$3,$8,$3
subu	$2,$2,$10
$L331:
addu	$7,$6,$3
sw	$2,5184($16)
or	$5,$5,$9
sll	$9,$5,1
lbu	$8,0($7)
sll	$10,$3,$8
addu	$4,$8,$4
sll	$5,$2,$8
bltz	$4,$L316
sw	$10,5168($16)

lw	$3,5176($16)
lw	$2,5180($16)
sltu	$2,$3,$2
beq	$2,$0,$L316
addiu	$2,$3,2

sw	$2,5176($16)
lbu	$2,1($3)
lbu	$3,0($3)
sll	$2,$2,8
or	$2,$2,$3
sll	$3,$2,8
srl	$2,$2,8
or	$2,$3,$2
andi	$2,$2,0xffff
sll	$2,$2,$4
addiu	$4,$4,-16
or	$5,$5,$2
$L316:
addiu	$3,$10,1
sra	$3,$3,1
sll	$2,$3,16
sltu	$8,$5,$2
xori	$7,$8,0x1
beq	$7,$0,$L317
sw	$4,5172($16)

subu	$3,$10,$3
subu	$5,$5,$2
$L317:
or	$7,$7,$9
sw	$5,5184($16)
move	$2,$5
.option	pic0
j	$L301
.option	pic2
sw	$7,6220($16)

$L295:
sw	$3,5168($16)
move	$2,$8
sw	$8,5184($16)
.option	pic0
j	$L323
.option	pic2
sw	$0,6216($16)

$L350:
move	$3,$0
$L379:
move	$6,$0
.option	pic0
j	$L322
.option	pic2
move	$5,$0

$L368:
addu	$5,$6,$3
sll	$4,$4,$10
lbu	$5,0($5)
sw	$4,6232($16)
sll	$3,$3,$5
addu	$9,$5,$2
sll	$5,$7,$5
bltz	$9,$L307
sw	$3,5168($16)

lw	$4,5176($16)
lw	$2,5180($16)
sltu	$2,$4,$2
beq	$2,$0,$L307
addiu	$2,$4,2

sw	$2,5176($16)
lbu	$2,1($4)
lbu	$4,0($4)
sll	$2,$2,8
or	$2,$2,$4
sll	$4,$2,8
srl	$2,$2,8
or	$2,$4,$2
andi	$2,$2,0xffff
sll	$2,$2,$9
addiu	$9,$9,-16
or	$5,$5,$2
$L307:
addiu	$4,$3,1
sra	$4,$4,1
sll	$2,$4,16
sltu	$8,$5,$2
xori	$8,$8,0x1
bne	$8,$0,$L374
sw	$9,5172($16)

$L308:
addu	$2,$6,$4
sw	$5,5184($16)
sll	$10,$8,1
lbu	$2,0($2)
sll	$3,$4,$2
addu	$7,$2,$9
sll	$2,$5,$2
bltz	$7,$L309
sw	$3,5168($16)

lw	$5,5176($16)
lw	$4,5180($16)
sltu	$4,$5,$4
beq	$4,$0,$L384
addiu	$4,$3,1

addiu	$4,$5,2
sw	$4,5176($16)
lbu	$4,1($5)
lbu	$5,0($5)
sll	$4,$4,8
or	$4,$4,$5
sll	$5,$4,8
srl	$4,$4,8
or	$4,$5,$4
andi	$4,$4,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$2,$2,$4
$L309:
addiu	$4,$3,1
$L384:
sra	$4,$4,1
sll	$5,$4,16
sltu	$9,$2,$5
xori	$8,$9,0x1
bne	$8,$0,$L375
sw	$7,5172($16)

$L310:
addu	$3,$6,$4
sw	$2,5184($16)
or	$8,$8,$10
sll	$10,$8,1
lbu	$9,0($3)
sll	$3,$4,$9
addu	$7,$9,$7
sll	$2,$2,$9
bltz	$7,$L311
sw	$3,5168($16)

lw	$5,5176($16)
lw	$4,5180($16)
sltu	$4,$5,$4
beq	$4,$0,$L385
addiu	$4,$3,1

addiu	$4,$5,2
sw	$4,5176($16)
lbu	$4,1($5)
lbu	$5,0($5)
sll	$4,$4,8
or	$4,$4,$5
sll	$5,$4,8
srl	$4,$4,8
or	$4,$5,$4
andi	$4,$4,0xffff
sll	$4,$4,$7
addiu	$7,$7,-16
or	$2,$2,$4
$L311:
addiu	$4,$3,1
$L385:
sra	$4,$4,1
sll	$9,$4,16
sltu	$8,$2,$9
xori	$5,$8,0x1
beq	$5,$0,$L312
sw	$7,5172($16)

subu	$4,$3,$4
subu	$2,$2,$9
$L312:
li	$3,2			# 0x2
sw	$4,5168($16)
or	$5,$5,$10
sw	$2,5184($16)
sll	$5,$3,$5
.option	pic0
j	$L313
.option	pic2
sw	$5,6228($16)

$L321:
addiu	$17,$16,5188
lw	$25,%call16(ff_vp56_init_range_decoder)($28)
.reloc	1f,R_MIPS_JALR,ff_vp56_init_range_decoder
1:	jalr	$25
move	$4,$17

move	$2,$20
.option	pic0
j	$L361
.option	pic2
sw	$17,5208($16)

$L367:
lw	$6,24($2)
li	$20,2			# 0x2
lw	$5,40($2)
lw	$4,44($2)
lbu	$3,0($6)
srl	$3,$3,4
subu	$3,$5,$3
sw	$3,40($2)
lbu	$3,0($6)
andi	$3,$3,0xf
subu	$3,$4,$3
.option	pic0
j	$L281
.option	pic2
sw	$3,44($2)

$L373:
subu	$3,$3,$7
.option	pic0
j	$L327
.option	pic2
subu	$2,$2,$8

$L371:
subu	$3,$7,$3
.option	pic0
j	$L297
.option	pic2
subu	$5,$5,$2

$L375:
subu	$4,$3,$4
.option	pic0
j	$L310
.option	pic2
subu	$2,$2,$5

$L374:
subu	$4,$3,$4
.option	pic0
j	$L308
.option	pic2
subu	$5,$5,$2

.set	macro
.set	reorder
.end	vp6_parse_header
.size	vp6_parse_header, .-vp6_parse_header
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_parse_coeff_models
.type	vp6_parse_coeff_models, @function
vp6_parse_coeff_models:
.frame	$sp,352,$31		# vars= 280, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$3,5184($4)
addiu	$sp,$sp,-352
lw	$7,5172($4)
li	$2,-2139095040			# 0xffffffff80800000
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,316($sp)
ori	$2,$2,0x8080
sw	$16,312($sp)
addiu	$17,$sp,224
sw	$31,348($sp)
lui	$14,%hi(vp6_dccv_pct)
sw	$2,224($sp)
sw	$fp,344($sp)
move	$13,$0
sw	$23,340($sp)
addiu	$14,$14,%lo(vp6_dccv_pct)
sw	$22,336($sp)
addiu	$24,$sp,268
sw	$21,332($sp)
li	$15,22			# 0x16
sw	$20,328($sp)
move	$5,$3
sw	$19,324($sp)
move	$3,$7
sw	$18,320($sp)
.cprestore	24
sw	$2,4($17)
sw	$2,8($17)
sw	$2,12($17)
sw	$2,16($17)
sw	$2,20($17)
sw	$2,24($17)
sw	$2,28($17)
sw	$2,32($17)
sw	$2,36($17)
sw	$2,40($17)
lw	$11,%got(ff_vp56_norm_shift)($28)
lw	$16,6560($4)
lw	$2,5168($4)
$L389:
addu	$9,$14,$13
addu	$10,$16,$13
move	$8,$17
$L396:
addu	$6,$11,$2
lbu	$12,0($9)
lbu	$6,0($6)
sll	$7,$2,$6
addu	$3,$6,$3
sll	$6,$5,$6
bltz	$3,$L390
sw	$7,5168($4)

lw	$18,5176($4)
lw	$2,5180($4)
sltu	$2,$18,$2
beq	$2,$0,$L488
addiu	$2,$7,-1

addiu	$2,$18,2
sw	$2,5176($4)
lbu	$5,1($18)
lbu	$2,0($18)
sll	$5,$5,8
or	$5,$5,$2
sll	$2,$5,8
srl	$5,$5,8
or	$2,$2,$5
andi	$2,$2,0xffff
sll	$2,$2,$3
addiu	$3,$3,-16
or	$6,$6,$2
$L390:
addiu	$2,$7,-1
$L488:
mul	$2,$2,$12
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$12,$6,$5
bne	$12,$0,$L391
sw	$3,5172($4)

subu	$5,$6,$5
subu	$2,$7,$2
li	$18,7			# 0x7
move	$7,$0
.option	pic0
j	$L394
.option	pic2
sw	$5,5184($4)

$L473:
subu	$5,$5,$19
sw	$2,5168($4)
addiu	$18,$18,-1
or	$7,$7,$12
beq	$18,$0,$L472
sw	$5,5184($4)

$L394:
addu	$6,$11,$2
sll	$12,$7,1
lbu	$7,0($6)
sll	$2,$2,$7
addu	$3,$7,$3
addiu	$6,$2,1
sw	$2,5168($4)
sll	$5,$5,$7
bltz	$3,$L392
sra	$6,$6,1

lw	$19,5176($4)
lw	$7,5180($4)
sltu	$7,$19,$7
beq	$7,$0,$L392
addiu	$20,$19,2

sw	$20,5176($4)
lbu	$7,1($19)
lbu	$19,0($19)
sll	$7,$7,8
or	$7,$7,$19
sll	$19,$7,8
srl	$7,$7,8
or	$7,$19,$7
andi	$7,$7,0xffff
sll	$7,$7,$3
addiu	$3,$3,-16
or	$5,$5,$7
$L392:
sll	$19,$6,16
sw	$3,5172($4)
sltu	$7,$5,$19
xori	$7,$7,0x1
bne	$7,$0,$L473
subu	$2,$2,$6

move	$2,$6
sw	$5,5184($4)
addiu	$18,$18,-1
sw	$2,5168($4)
bne	$18,$0,$L394
or	$7,$7,$12

$L472:
sll	$7,$7,1
addiu	$8,$8,4
sltu	$2,$7,1
addu	$2,$7,$2
addiu	$9,$9,1
addiu	$10,$10,1
sw	$2,-4($8)
sb	$2,165($10)
lw	$2,5168($4)
lw	$3,5172($4)
bne	$24,$8,$L396
lw	$5,5184($4)

addiu	$13,$13,11
$L486:
bne	$13,$15,$L389
move	$7,$3

move	$3,$5
addu	$5,$11,$2
lbu	$5,0($5)
sll	$2,$2,$5
addu	$7,$5,$7
sll	$5,$3,$5
bltz	$7,$L398
sw	$2,5168($4)

lw	$6,5176($4)
lw	$3,5180($4)
sltu	$3,$6,$3
bne	$3,$0,$L474
addiu	$3,$6,2

$L398:
addiu	$6,$2,1
sra	$6,$6,1
sll	$3,$6,16
sltu	$8,$5,$3
bne	$8,$0,$L399
sw	$7,5172($4)

subu	$2,$2,$6
subu	$3,$5,$3
lui	$8,%hi(vp6_coeff_reorder_pct+1)
lui	$10,%hi(vp6_coeff_reorder_pct+64)
sw	$2,5168($4)
move	$5,$2
sw	$3,5184($4)
addiu	$8,$8,%lo(vp6_coeff_reorder_pct+1)
addiu	$9,$16,1
addiu	$10,$10,%lo(vp6_coeff_reorder_pct+64)
move	$2,$7
.option	pic0
j	$L400
.option	pic2
move	$6,$3

$L476:
subu	$3,$5,$3
subu	$7,$6,$7
addu	$5,$11,$3
sw	$7,5184($4)
lbu	$5,0($5)
sll	$3,$3,$5
addu	$2,$2,$5
sll	$5,$7,$5
bltz	$2,$L404
sw	$3,5168($4)

lw	$7,5176($4)
lw	$6,5180($4)
sltu	$6,$7,$6
beq	$6,$0,$L489
addiu	$6,$3,1

addiu	$6,$7,2
sw	$6,5176($4)
lbu	$6,1($7)
lbu	$7,0($7)
sll	$6,$6,8
or	$6,$6,$7
sll	$7,$6,8
srl	$6,$6,8
or	$6,$7,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$5,$5,$6
$L404:
addiu	$6,$3,1
$L489:
sra	$6,$6,1
sll	$12,$6,16
sltu	$7,$5,$12
xori	$7,$7,0x1
beq	$7,$0,$L451
sw	$2,5172($4)

subu	$3,$3,$6
subu	$5,$5,$12
$L405:
addu	$6,$11,$3
sw	$5,5184($4)
sll	$12,$7,1
lbu	$6,0($6)
sll	$3,$3,$6
addu	$13,$2,$6
sll	$5,$5,$6
bltz	$13,$L406
sw	$3,5168($4)

lw	$6,5176($4)
lw	$2,5180($4)
sltu	$2,$6,$2
beq	$2,$0,$L406
addiu	$2,$6,2

sw	$2,5176($4)
lbu	$2,1($6)
lbu	$6,0($6)
sll	$2,$2,8
or	$2,$2,$6
sll	$6,$2,8
srl	$2,$2,8
or	$2,$6,$2
andi	$2,$2,0xffff
sll	$2,$2,$13
addiu	$13,$13,-16
or	$5,$5,$2
$L406:
addiu	$6,$3,1
sra	$6,$6,1
sll	$14,$6,16
sltu	$7,$5,$14
xori	$2,$7,0x1
beq	$2,$0,$L452
sw	$13,5172($4)

subu	$3,$3,$6
subu	$5,$5,$14
$L407:
addu	$6,$11,$3
sw	$5,5184($4)
or	$2,$2,$12
sll	$12,$2,1
lbu	$2,0($6)
sll	$6,$3,$2
addu	$13,$13,$2
sll	$2,$5,$2
bltz	$13,$L408
sw	$6,5168($4)

lw	$5,5176($4)
lw	$3,5180($4)
sltu	$3,$5,$3
beq	$3,$0,$L490
addiu	$3,$6,1

addiu	$3,$5,2
sw	$3,5176($4)
lbu	$3,1($5)
lbu	$5,0($5)
sll	$3,$3,8
or	$3,$3,$5
sll	$5,$3,8
srl	$3,$3,8
or	$3,$5,$3
andi	$3,$3,0xffff
sll	$3,$3,$13
addiu	$13,$13,-16
or	$2,$2,$3
$L408:
addiu	$3,$6,1
$L490:
sra	$3,$3,1
sll	$14,$3,16
sltu	$7,$2,$14
xori	$5,$7,0x1
beq	$5,$0,$L409
sw	$13,5172($4)

subu	$3,$6,$3
subu	$2,$2,$14
$L409:
addu	$6,$11,$3
sw	$2,5184($4)
or	$5,$5,$12
sll	$12,$5,1
lbu	$5,0($6)
sll	$3,$3,$5
addu	$13,$13,$5
sll	$2,$2,$5
bltz	$13,$L410
sw	$3,5168($4)

lw	$6,5176($4)
lw	$5,5180($4)
sltu	$5,$6,$5
beq	$5,$0,$L491
addiu	$5,$3,1

addiu	$5,$6,2
sw	$5,5176($4)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$13
addiu	$13,$13,-16
or	$2,$2,$5
$L410:
addiu	$5,$3,1
$L491:
sw	$13,5172($4)
sra	$5,$5,1
sll	$13,$5,16
sltu	$7,$2,$13
xori	$6,$7,0x1
beq	$6,$0,$L411
nop

subu	$5,$3,$5
subu	$2,$2,$13
$L411:
or	$6,$6,$12
sw	$5,5168($4)
addiu	$8,$8,1
sw	$2,5184($4)
addiu	$9,$9,1
beq	$8,$10,$L475
sb	$6,-1($9)

$L412:
lw	$5,5168($4)
lw	$2,5172($4)
lw	$6,5184($4)
$L400:
addu	$3,$11,$5
lbu	$12,0($8)
lbu	$7,0($3)
sll	$5,$5,$7
addu	$2,$7,$2
sll	$6,$6,$7
bltz	$2,$L402
sw	$5,5168($4)

lw	$13,5176($4)
lw	$3,5180($4)
sltu	$3,$13,$3
beq	$3,$0,$L492
addiu	$3,$5,-1

addiu	$3,$13,2
sw	$3,5176($4)
lbu	$7,1($13)
lbu	$3,0($13)
sll	$7,$7,8
or	$7,$7,$3
sll	$3,$7,8
srl	$7,$7,8
or	$7,$3,$7
andi	$7,$7,0xffff
sll	$7,$7,$2
addiu	$2,$2,-16
or	$6,$6,$7
$L402:
addiu	$3,$5,-1
$L492:
mul	$3,$3,$12
sra	$3,$3,8
addiu	$3,$3,1
sll	$7,$3,16
sltu	$12,$6,$7
beq	$12,$0,$L476
sw	$2,5172($4)

addiu	$8,$8,1
sw	$3,5168($4)
sw	$6,5184($4)
bne	$8,$10,$L412
addiu	$9,$9,1

$L475:
lw	$2,6560($4)
li	$8,1			# 0x1
move	$6,$0
li	$7,64			# 0x40
li	$9,16			# 0x10
sb	$0,64($2)
.option	pic0
j	$L415
.option	pic2
li	$2,1			# 0x1

$L414:
addiu	$2,$2,1
beq	$2,$7,$L477
nop

$L415:
lw	$3,6560($4)
addu	$5,$3,$2
lbu	$5,0($5)
bne	$5,$6,$L414
addu	$3,$3,$8

sb	$2,64($3)
addiu	$2,$2,1
bne	$2,$7,$L415
addiu	$8,$8,1

$L477:
addiu	$6,$6,1
bne	$6,$9,$L415
li	$2,1			# 0x1

lw	$5,5172($4)
lui	$13,%hi(vp6_runv_pct)
lw	$2,5168($4)
move	$12,$0
lw	$8,5184($4)
addiu	$13,$13,%lo(vp6_runv_pct)
li	$14,14			# 0xe
li	$15,28			# 0x1c
move	$3,$5
$L401:
move	$9,$0
$L422:
addu	$5,$11,$2
addu	$10,$9,$12
lbu	$5,0($5)
addu	$6,$13,$10
sll	$7,$2,$5
lbu	$18,0($6)
addu	$3,$5,$3
sll	$6,$8,$5
bltz	$3,$L417
sw	$7,5168($4)

lw	$8,5176($4)
lw	$2,5180($4)
sltu	$2,$8,$2
beq	$2,$0,$L493
addiu	$2,$7,-1

addiu	$2,$8,2
sw	$2,5176($4)
lbu	$5,1($8)
lbu	$2,0($8)
sll	$5,$5,8
or	$5,$5,$2
sll	$2,$5,8
srl	$5,$5,8
or	$5,$2,$5
andi	$5,$5,0xffff
sll	$5,$5,$3
addiu	$3,$3,-16
or	$6,$6,$5
$L417:
addiu	$2,$7,-1
$L493:
mul	$2,$2,$18
sra	$2,$2,8
addiu	$2,$2,1
sll	$5,$2,16
sltu	$8,$6,$5
bne	$8,$0,$L418
sw	$3,5172($4)

subu	$5,$6,$5
subu	$2,$7,$2
li	$18,7			# 0x7
move	$7,$0
.option	pic0
j	$L421
.option	pic2
sw	$5,5184($4)

$L479:
subu	$5,$5,$19
sw	$2,5168($4)
addiu	$18,$18,-1
or	$7,$7,$8
beq	$18,$0,$L478
sw	$5,5184($4)

$L421:
addu	$6,$11,$2
sll	$8,$7,1
lbu	$7,0($6)
sll	$2,$2,$7
addu	$3,$7,$3
addiu	$6,$2,1
sw	$2,5168($4)
sll	$5,$5,$7
bltz	$3,$L419
sra	$6,$6,1

lw	$19,5176($4)
lw	$7,5180($4)
sltu	$7,$19,$7
beq	$7,$0,$L419
addiu	$20,$19,2

sw	$20,5176($4)
lbu	$7,1($19)
lbu	$19,0($19)
sll	$7,$7,8
or	$7,$7,$19
sll	$19,$7,8
srl	$7,$7,8
or	$7,$19,$7
andi	$7,$7,0xffff
sll	$7,$7,$3
addiu	$3,$3,-16
or	$5,$5,$7
$L419:
sll	$19,$6,16
sw	$3,5172($4)
sltu	$7,$5,$19
xori	$7,$7,0x1
bne	$7,$0,$L479
subu	$2,$2,$6

move	$2,$6
sw	$5,5184($4)
addiu	$18,$18,-1
sw	$2,5168($4)
bne	$18,$0,$L421
or	$7,$7,$8

$L478:
sll	$7,$7,1
addu	$10,$16,$10
sltu	$2,$7,1
addu	$7,$7,$2
addiu	$9,$9,1
sb	$7,1484($10)
lw	$2,5168($4)
lw	$3,5172($4)
bne	$9,$14,$L422
lw	$8,5184($4)

addiu	$12,$12,14
$L487:
bne	$12,$15,$L422
move	$9,$0

lui	$20,%hi(vp6_ract_pct)
move	$5,$3
move	$fp,$0
addiu	$20,$20,%lo(vp6_ract_pct)
li	$21,66			# 0x42
move	$3,$2
move	$6,$5
move	$2,$8
$L423:
sll	$7,$fp,1
move	$23,$0
move	$25,$fp
sw	$7,272($sp)
move	$5,$7
$L436:
addu	$22,$23,$5
move	$18,$0
addiu	$19,$25,188
move	$10,$6
$L434:
addu	$8,$18,$22
addu	$9,$19,$18
addu	$8,$20,$8
addu	$9,$16,$9
move	$7,$17
move	$5,$3
move	$12,$10
$L432:
addu	$3,$11,$5
lbu	$6,0($8)
lbu	$3,0($3)
sll	$5,$5,$3
addu	$12,$3,$12
sll	$2,$2,$3
bltz	$12,$L424
sw	$5,5168($4)

lw	$10,5176($4)
lw	$3,5180($4)
sltu	$3,$10,$3
beq	$3,$0,$L494
addiu	$3,$5,-1

addiu	$3,$10,2
sw	$3,5176($4)
lbu	$3,1($10)
lbu	$10,0($10)
sll	$3,$3,8
or	$3,$3,$10
sll	$10,$3,8
srl	$3,$3,8
or	$3,$10,$3
andi	$3,$3,0xffff
sll	$3,$3,$12
addiu	$12,$12,-16
or	$2,$2,$3
$L424:
addiu	$3,$5,-1
$L494:
mul	$3,$3,$6
sra	$3,$3,8
addiu	$3,$3,1
sll	$6,$3,16
sltu	$10,$2,$6
bne	$10,$0,$L425
sw	$12,5172($4)

subu	$2,$2,$6
subu	$3,$5,$3
li	$13,7			# 0x7
move	$5,$0
.option	pic0
j	$L428
.option	pic2
sw	$2,5184($4)

$L481:
subu	$2,$2,$14
sw	$3,5168($4)
addiu	$13,$13,-1
or	$5,$10,$5
beq	$13,$0,$L480
sw	$2,5184($4)

$L428:
addu	$6,$11,$3
sll	$5,$5,1
lbu	$10,0($6)
sll	$3,$3,$10
addu	$12,$10,$12
addiu	$6,$3,1
sw	$3,5168($4)
sll	$2,$2,$10
bltz	$12,$L426
sra	$6,$6,1

lw	$14,5176($4)
lw	$10,5180($4)
sltu	$10,$14,$10
beq	$10,$0,$L426
addiu	$15,$14,2

sw	$15,5176($4)
lbu	$10,1($14)
lbu	$14,0($14)
sll	$10,$10,8
or	$10,$10,$14
sll	$14,$10,8
srl	$10,$10,8
or	$10,$14,$10
andi	$10,$10,0xffff
sll	$10,$10,$12
addiu	$12,$12,-16
or	$2,$2,$10
$L426:
sll	$14,$6,16
sw	$12,5172($4)
sltu	$10,$2,$14
xori	$10,$10,0x1
bne	$10,$0,$L481
subu	$3,$3,$6

move	$3,$6
sw	$2,5184($4)
addiu	$13,$13,-1
sw	$3,5168($4)
bne	$13,$0,$L428
or	$5,$10,$5

$L480:
sll	$5,$5,1
addiu	$7,$7,4
sltu	$2,$5,1
addu	$5,$5,$2
addiu	$8,$8,1
addiu	$9,$9,1
sw	$5,-4($7)
bne	$24,$7,$L430
sb	$5,-1($9)

$L484:
addiu	$18,$18,11
beq	$18,$21,$L431
nop

lw	$3,5168($4)
lw	$10,5172($4)
.option	pic0
j	$L434
.option	pic2
lw	$2,5184($4)

$L391:
lw	$5,5136($4)
sw	$2,5168($4)
sw	$6,5184($4)
lw	$5,48($5)
bne	$5,$0,$L445
nop

move	$5,$6
$L395:
addiu	$8,$8,4
addiu	$9,$9,1
bne	$24,$8,$L396
addiu	$10,$10,1

.option	pic0
j	$L486
.option	pic2
addiu	$13,$13,11

$L445:
lw	$2,0($8)
sb	$2,166($10)
lw	$2,5168($4)
lw	$3,5172($4)
.option	pic0
j	$L395
.option	pic2
lw	$5,5184($4)

$L452:
.option	pic0
j	$L407
.option	pic2
move	$3,$6

$L451:
.option	pic0
j	$L405
.option	pic2
move	$3,$6

$L399:
lui	$13,%hi(vp6_runv_pct)
sw	$5,5184($4)
move	$8,$5
sw	$6,5168($4)
move	$5,$7
move	$2,$6
move	$12,$0
addiu	$13,$13,%lo(vp6_runv_pct)
li	$14,14			# 0xe
li	$15,28			# 0x1c
.option	pic0
j	$L401
.option	pic2
move	$3,$5

$L418:
addiu	$9,$9,1
sw	$2,5168($4)
move	$8,$6
bne	$9,$14,$L422
sw	$6,5184($4)

.option	pic0
j	$L487
.option	pic2
addiu	$12,$12,14

$L425:
lw	$5,5136($4)
sw	$2,5184($4)
sw	$3,5168($4)
lw	$2,48($5)
beq	$2,$0,$L429
nop

lw	$2,0($7)
sb	$2,0($9)
$L429:
addiu	$7,$7,4
addiu	$8,$8,1
beq	$24,$7,$L484
addiu	$9,$9,1

$L430:
lw	$5,5168($4)
lw	$12,5172($4)
.option	pic0
j	$L432
.option	pic2
lw	$2,5184($4)

$L431:
addiu	$23,$23,66
li	$2,132			# 0x84
beq	$23,$2,$L433
addiu	$25,$25,198

lw	$3,5168($4)
lw	$6,5172($4)
lw	$2,5184($4)
.option	pic0
j	$L436
.option	pic2
lw	$5,272($sp)

$L433:
addiu	$fp,$fp,66
li	$6,198			# 0xc6
beq	$fp,$6,$L435
nop

lw	$3,5168($4)
lw	$6,5172($4)
.option	pic0
j	$L423
.option	pic2
lw	$2,5184($4)

$L435:
lw	$2,10308($4)
bne	$2,$0,$L437
addiu	$fp,$4,10360

li	$8,2			# 0x2
li	$22,255			# 0xff
li	$20,1			# 0x1
move	$24,$16
sw	$8,272($sp)
$L438:
lbu	$13,166($24)
addiu	$24,$24,11
lbu	$2,157($24)
li	$7,139			# 0x8b
li	$fp,203			# 0xcb
sll	$4,$13,2
sll	$5,$13,6
lbu	$3,159($24)
sll	$23,$2,3
lbu	$8,158($24)
sll	$9,$2,5
sll	$21,$13,3
sb	$20,1125($16)
sll	$6,$3,1
sb	$20,1130($16)
sll	$15,$3,4
sb	$20,1135($16)
mul	$12,$8,$7
li	$7,169			# 0xa9
subu	$15,$15,$6
subu	$6,$5,$4
mul	$11,$2,$7
li	$7,214			# 0xd6
addu	$6,$6,$13
mul	$10,$8,$7
li	$7,221			# 0xdd
sll	$6,$6,1
sll	$25,$3,5
sll	$18,$8,2
mul	$14,$2,$7
sll	$17,$3,3
mul	$7,$3,$fp
sll	$19,$8,7
addu	$3,$23,$9
sll	$9,$13,7
addiu	$6,$6,128
subu	$19,$19,$18
addu	$4,$4,$9
subu	$17,$25,$17
subu	$3,$3,$2
sra	$6,$6,8
subu	$2,$19,$8
addu	$4,$4,$13
addiu	$11,$11,128
addu	$5,$21,$5
sll	$18,$15,4
sll	$21,$17,3
addiu	$6,$6,133
sll	$3,$3,1
sll	$2,$2,1
addiu	$4,$4,128
sra	$11,$11,8
subu	$9,$5,$13
addiu	$3,$3,128
subu	$5,$21,$17
subu	$13,$18,$15
move	$17,$22
slt	$15,$6,256
addiu	$11,$11,71
movn	$17,$6,$15
addiu	$8,$2,128
addiu	$12,$12,128
sra	$2,$4,8
sra	$3,$3,8
addiu	$2,$2,51
slt	$fp,$11,256
sra	$12,$12,8
addiu	$3,$3,171
sw	$fp,288($sp)
addiu	$13,$13,128
addiu	$14,$14,128
addiu	$12,$12,117
move	$15,$17
move	$fp,$22
slt	$17,$2,256
movn	$fp,$2,$17
sra	$4,$13,8
move	$21,$22
sra	$13,$14,8
slt	$14,$3,256
movn	$21,$3,$14
slt	$19,$12,256
move	$14,$22
movn	$14,$12,$19
addiu	$10,$10,128
sll	$9,$9,1
sw	$fp,284($sp)
sra	$10,$10,8
lw	$fp,288($sp)
addiu	$9,$9,128
addiu	$10,$10,44
sw	$21,292($sp)
sw	$14,276($sp)
move	$14,$22
movn	$14,$11,$fp
slt	$25,$10,256
addiu	$5,$5,128
sra	$9,$9,8
sra	$5,$5,8
addiu	$9,$9,-16
sw	$14,288($sp)
move	$14,$22
movn	$14,$10,$25
addiu	$5,$5,79
addiu	$4,$4,38
slt	$21,$9,256
slt	$18,$5,256
move	$25,$14
move	$14,$22
movn	$14,$9,$21
sra	$8,$8,8
move	$19,$22
movn	$19,$5,$18
slt	$23,$4,256
move	$fp,$22
addiu	$8,$8,-3
movn	$fp,$4,$23
addiu	$13,$13,-30
slt	$18,$8,256
move	$21,$14
sw	$19,280($sp)
move	$14,$22
movn	$14,$8,$18
addiu	$7,$7,128
slt	$19,$13,256
move	$23,$fp
move	$fp,$22
movn	$fp,$13,$19
sra	$7,$7,8
move	$18,$14
addiu	$7,$7,17
lw	$14,292($sp)
slt	$3,$0,$3
slt	$17,$7,256
move	$19,$fp
movz	$14,$20,$3
move	$fp,$22
movn	$fp,$7,$17
slt	$12,$0,$12
slt	$5,$0,$5
move	$3,$14
lw	$14,280($sp)
slt	$7,$0,$7
move	$17,$fp
lw	$fp,276($sp)
movz	$14,$20,$5
slt	$6,$0,$6
slt	$2,$0,$2
movz	$fp,$20,$12
slt	$11,$0,$11
slt	$10,$0,$10
move	$5,$14
movz	$17,$20,$7
slt	$4,$0,$4
move	$12,$fp
lw	$14,288($sp)
lw	$fp,284($sp)
slt	$9,$0,$9
slt	$13,$0,$13
movz	$15,$20,$6
slt	$8,$0,$8
addiu	$16,$16,180
movz	$fp,$20,$2
movz	$14,$20,$11
movz	$25,$20,$10
movz	$23,$20,$4
movz	$21,$20,$9
movz	$19,$20,$13
movz	$18,$20,$8
sb	$17,958($16)
sb	$15,944($16)
sb	$3,946($16)
sb	$12,947($16)
sb	$5,948($16)
sb	$fp,949($16)
sb	$14,951($16)
sb	$25,952($16)
sb	$23,953($16)
sb	$21,954($16)
sb	$19,956($16)
sb	$18,957($16)
lw	$17,272($sp)
beq	$17,$20,$L485
li	$5,1			# 0x1

.option	pic0
j	$L438
.option	pic2
sw	$5,272($sp)

$L485:
lw	$31,348($sp)
lw	$fp,344($sp)
lw	$23,340($sp)
lw	$22,336($sp)
lw	$21,332($sp)
lw	$20,328($sp)
lw	$19,324($sp)
lw	$18,320($sp)
lw	$17,316($sp)
lw	$16,312($sp)
j	$31
addiu	$sp,$sp,352

$L437:
addiu	$21,$16,1484
addiu	$18,$16,188
addiu	$19,$4,10328
sw	$fp,304($sp)
addiu	$14,$4,10488
sw	$21,276($sp)
addiu	$16,$16,166
sw	$18,300($sp)
addiu	$3,$sp,132
sw	$19,284($sp)
addiu	$fp,$sp,32
sw	$14,296($sp)
addiu	$22,$sp,220
sw	$16,292($sp)
li	$21,256			# 0x100
sw	$3,272($sp)
li	$23,255			# 0xff
li	$20,1			# 0x1
move	$19,$fp
move	$17,$4
move	$fp,$22
$L444:
lui	$7,%hi(vp6_huff_coeff_map)
lw	$11,292($sp)
addiu	$10,$sp,132
sw	$21,132($sp)
addiu	$7,$7,%lo(vp6_huff_coeff_map)
$L439:
addiu	$10,$10,8
lbu	$2,0($11)
lw	$6,-8($10)
addiu	$7,$7,2
lbu	$5,-2($7)
addiu	$11,$11,1
subu	$3,$23,$2
mul	$2,$2,$6
mul	$3,$3,$6
lbu	$4,-1($7)
sll	$5,$5,3
addu	$5,$19,$5
sll	$4,$4,3
srl	$2,$2,8
srl	$3,$3,8
sltu	$8,$2,1
sltu	$6,$3,1
addu	$2,$2,$8
addu	$3,$3,$6
addu	$4,$19,$4
sw	$2,4($5)
bne	$fp,$10,$L439
sw	$3,4($4)

lw	$3,300($sp)
lw	$25,%call16(free_vlc)($28)
lw	$4,284($sp)
sw	$3,288($sp)
.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
addiu	$22,$4,32

lw	$28,24($sp)
lui	$8,%hi(vp6_huff_cmp)
lw	$4,0($17)
li	$6,12			# 0xc
addiu	$8,$8,%lo(vp6_huff_cmp)
lw	$5,284($sp)
move	$7,$19
lw	$16,296($sp)
lw	$25,%call16(ff_huff_build_tree)($28)
sw	$8,16($sp)
.reloc	1f,R_MIPS_JALR,ff_huff_build_tree
1:	jalr	$25
sw	$20,20($sp)

li	$2,255			# 0xff
lw	$18,276($sp)
li	$8,255			# 0xff
sw	$21,108($sp)
li	$14,3			# 0x3
lw	$28,24($sp)
move	$4,$22
lbu	$7,0($18)
sw	$14,280($sp)
lw	$25,%call16(free_vlc)($28)
subu	$6,$2,$7
sltu	$2,$7,1
sll	$6,$6,8
addu	$7,$2,$7
srl	$2,$6,8
sltu	$3,$2,1
sw	$7,116($sp)
addu	$6,$2,$3
li	$3,255			# 0xff
sw	$6,140($sp)
lbu	$2,1($18)
subu	$10,$3,$2
mul	$3,$7,$2
mul	$2,$7,$10
srl	$3,$3,8
srl	$2,$2,8
sltu	$10,$3,1
sltu	$7,$2,1
addu	$3,$3,$10
addu	$2,$2,$7
sw	$3,124($sp)
sw	$2,132($sp)
lbu	$7,2($18)
subu	$10,$8,$7
mul	$7,$3,$7
mul	$3,$3,$10
srl	$7,$7,8
srl	$3,$3,8
sltu	$11,$7,1
sltu	$10,$3,1
addu	$7,$7,$11
addu	$3,$3,$10
sw	$7,36($sp)
sw	$3,44($sp)
lbu	$3,3($18)
subu	$7,$8,$3
mul	$3,$2,$3
mul	$2,$2,$7
srl	$3,$3,8
srl	$2,$2,8
sltu	$10,$3,1
sltu	$7,$2,1
addu	$3,$3,$10
addu	$2,$2,$7
sw	$3,52($sp)
sw	$2,60($sp)
lbu	$2,4($18)
subu	$3,$8,$2
mul	$2,$6,$2
mul	$3,$6,$3
srl	$2,$2,8
srl	$3,$3,8
sltu	$6,$2,1
sltu	$7,$3,1
addu	$6,$2,$6
addu	$3,$3,$7
sw	$6,148($sp)
sw	$3,100($sp)
lbu	$2,5($18)
subu	$7,$8,$2
mul	$3,$6,$2
mul	$2,$6,$7
srl	$3,$3,8
srl	$2,$2,8
sltu	$7,$3,1
sltu	$6,$2,1
addu	$3,$3,$7
addu	$2,$2,$6
sw	$3,156($sp)
sw	$2,164($sp)
lbu	$6,6($18)
subu	$7,$8,$6
mul	$6,$3,$6
mul	$3,$3,$7
srl	$6,$6,8
srl	$3,$3,8
sltu	$10,$6,1
sltu	$7,$3,1
addu	$6,$6,$10
addu	$3,$3,$7
sw	$6,68($sp)
sw	$3,76($sp)
lbu	$3,7($18)
sw	$22,308($sp)
subu	$6,$8,$3
mul	$3,$2,$3
mul	$2,$2,$6
srl	$3,$3,8
srl	$2,$2,8
sltu	$7,$3,1
sltu	$6,$2,1
addu	$3,$3,$7
addu	$2,$2,$6
sw	$3,84($sp)
.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
sw	$2,92($sp)

lui	$2,%hi(vp6_huff_cmp)
lw	$28,24($sp)
li	$6,9			# 0x9
addiu	$2,$2,%lo(vp6_huff_cmp)
lw	$4,0($17)
lw	$5,308($sp)
move	$7,$19
sw	$20,20($sp)
lw	$25,%call16(ff_huff_build_tree)($28)
.reloc	1f,R_MIPS_JALR,ff_huff_build_tree
1:	jalr	$25
sw	$2,16($sp)

move	$2,$19
lw	$28,24($sp)
move	$19,$fp
move	$fp,$2
$L440:
addiu	$18,$16,-96
lw	$22,288($sp)
move	$2,$18
move	$18,$22
move	$22,$2
$L442:
lui	$5,%hi(vp6_huff_coeff_map)
sw	$21,132($sp)
addiu	$7,$sp,132
addiu	$5,$5,%lo(vp6_huff_coeff_map)
move	$10,$18
$L441:
addiu	$7,$7,8
lbu	$2,0($10)
lw	$8,-8($7)
addiu	$5,$5,2
lbu	$6,-2($5)
addiu	$10,$10,1
subu	$3,$23,$2
mul	$2,$2,$8
mul	$3,$3,$8
lbu	$4,-1($5)
sll	$6,$6,3
addu	$6,$fp,$6
sll	$4,$4,3
srl	$2,$2,8
srl	$3,$3,8
sltu	$9,$2,1
sltu	$8,$3,1
addu	$2,$2,$9
addu	$3,$3,$8
addu	$4,$fp,$4
sw	$2,4($6)
bne	$19,$7,$L441
sw	$3,4($4)

lw	$25,%call16(free_vlc)($28)
move	$4,$22
.reloc	1f,R_MIPS_JALR,free_vlc
1:	jalr	$25
addiu	$18,$18,11

lui	$2,%hi(vp6_huff_cmp)
lw	$28,24($sp)
move	$5,$22
addiu	$2,$2,%lo(vp6_huff_cmp)
lw	$4,0($17)
li	$6,12			# 0xc
sw	$20,20($sp)
addiu	$22,$22,16
sw	$2,16($sp)
lw	$25,%call16(ff_huff_build_tree)($28)
.reloc	1f,R_MIPS_JALR,ff_huff_build_tree
1:	jalr	$25
move	$7,$fp

bne	$16,$22,$L442
lw	$28,24($sp)

lw	$3,280($sp)
addiu	$16,$16,96
lw	$8,288($sp)
addiu	$3,$3,-1
addiu	$8,$8,66
sw	$3,280($sp)
bne	$3,$0,$L440
sw	$8,288($sp)

lw	$14,284($sp)
move	$2,$fp
lw	$18,296($sp)
move	$fp,$19
lw	$3,300($sp)
move	$19,$2
addiu	$14,$14,16
lw	$8,276($sp)
addiu	$18,$18,288
addiu	$3,$3,198
sw	$14,284($sp)
addiu	$8,$8,14
sw	$18,296($sp)
sw	$3,300($sp)
lw	$14,292($sp)
lw	$18,284($sp)
lw	$3,304($sp)
addiu	$14,$14,11
sw	$8,276($sp)
bne	$18,$3,$L444
sw	$14,292($sp)

lw	$31,348($sp)
addiu	$4,$17,10968
lw	$fp,344($sp)
move	$5,$0
lw	$23,340($sp)
li	$6,16			# 0x10
lw	$22,336($sp)
lw	$21,332($sp)
lw	$20,328($sp)
lw	$19,324($sp)
lw	$18,320($sp)
lw	$17,316($sp)
lw	$16,312($sp)
lw	$25,%call16(memset)($28)
.reloc	1f,R_MIPS_JALR,memset
1:	jr	$25
addiu	$sp,$sp,352

$L474:
sw	$3,5176($4)
lbu	$3,1($6)
lbu	$6,0($6)
sll	$3,$3,8
or	$3,$3,$6
sll	$6,$3,8
srl	$3,$3,8
or	$3,$6,$3
andi	$3,$3,0xffff
sll	$3,$3,$7
addiu	$7,$7,-16
.option	pic0
j	$L398
.option	pic2
or	$5,$5,$3

.set	macro
.set	reorder
.end	vp6_parse_coeff_models
.size	vp6_parse_coeff_models, .-vp6_parse_coeff_models
.align	2
.set	nomips16
.set	nomicromips
.ent	vp6_parse_vector_models
.type	vp6_parse_vector_models, @function
vp6_parse_vector_models:
.frame	$sp,8,$31		# vars= 0, regs= 2/0, args= 0, gp= 0
.mask	0x00030000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$28,%hi(__gnu_local_gp)
lw	$8,6560($4)
addiu	$sp,$sp,-8
lw	$3,5168($4)
addiu	$28,$28,%lo(__gnu_local_gp)
lw	$2,5172($4)
lui	$9,%hi(vp6_sig_dct_pct)
sw	$16,0($sp)
lui	$11,%hi(vp6_sig_dct_pct+4)
sw	$17,4($sp)
lw	$7,%got(ff_vp56_norm_shift)($28)
addiu	$9,$9,%lo(vp6_sig_dct_pct)
lw	$16,5184($4)
addiu	$11,$11,%lo(vp6_sig_dct_pct+4)
addiu	$10,$8,128
$L506:
addu	$5,$7,$3
lbu	$13,0($9)
lbu	$6,0($5)
sll	$3,$3,$6
addu	$2,$6,$2
sll	$16,$16,$6
bltz	$2,$L496
sw	$3,5168($4)

lw	$5,5176($4)
lw	$6,5180($4)
sltu	$6,$5,$6
beq	$6,$0,$L496
addiu	$6,$5,2

sw	$6,5176($4)
lbu	$6,1($5)
lbu	$5,0($5)
sll	$6,$6,8
or	$6,$6,$5
sll	$12,$6,8
srl	$6,$6,8
or	$6,$12,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$16,$16,$6
$L496:
addiu	$5,$3,-1
mul	$5,$5,$13
sra	$5,$5,8
addiu	$5,$5,1
sll	$6,$5,16
sltu	$12,$16,$6
bne	$12,$0,$L497
sw	$2,5172($4)

subu	$16,$16,$6
subu	$5,$3,$5
li	$13,7			# 0x7
move	$6,$0
.option	pic0
j	$L500
.option	pic2
sw	$16,5184($4)

$L543:
subu	$16,$16,$14
sw	$5,5168($4)
addiu	$13,$13,-1
or	$6,$6,$12
beq	$13,$0,$L542
sw	$16,5184($4)

$L500:
addu	$3,$7,$5
sll	$12,$6,1
lbu	$6,0($3)
sll	$5,$5,$6
addu	$2,$6,$2
addiu	$3,$5,1
sw	$5,5168($4)
sll	$16,$16,$6
bltz	$2,$L498
sra	$3,$3,1

lw	$14,5176($4)
lw	$6,5180($4)
sltu	$6,$14,$6
beq	$6,$0,$L498
addiu	$15,$14,2

sw	$15,5176($4)
lbu	$6,1($14)
lbu	$14,0($14)
sll	$6,$6,8
or	$6,$6,$14
sll	$14,$6,8
srl	$6,$6,8
or	$6,$14,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$16,$16,$6
$L498:
sll	$14,$3,16
sw	$2,5172($4)
sltu	$6,$16,$14
xori	$6,$6,0x1
bne	$6,$0,$L543
subu	$5,$5,$3

move	$5,$3
sw	$16,5184($4)
addiu	$13,$13,-1
sw	$5,5168($4)
bne	$13,$0,$L500
or	$6,$6,$12

$L542:
sll	$6,$6,1
sltu	$2,$6,1
addu	$6,$6,$2
sb	$6,2($10)
lw	$5,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L524
.option	pic2
lw	$12,5184($4)

$L497:
sw	$5,5168($4)
move	$12,$16
sw	$16,5184($4)
$L524:
addu	$3,$7,$5
lbu	$14,1($9)
lbu	$6,0($3)
sll	$5,$5,$6
addu	$2,$6,$2
sll	$12,$12,$6
bltz	$2,$L501
sw	$5,5168($4)

lw	$3,5176($4)
lw	$6,5180($4)
sltu	$6,$3,$6
beq	$6,$0,$L501
addiu	$6,$3,2

sw	$6,5176($4)
lbu	$6,1($3)
lbu	$3,0($3)
sll	$6,$6,8
or	$6,$6,$3
sll	$13,$6,8
srl	$6,$6,8
or	$6,$13,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$12,$12,$6
$L501:
addiu	$3,$5,-1
mul	$3,$3,$14
sra	$3,$3,8
addiu	$3,$3,1
sll	$6,$3,16
sltu	$13,$12,$6
bne	$13,$0,$L502
sw	$2,5172($4)

subu	$6,$12,$6
subu	$3,$5,$3
li	$14,7			# 0x7
move	$12,$0
.option	pic0
j	$L505
.option	pic2
sw	$6,5184($4)

$L545:
subu	$6,$6,$15
sw	$3,5168($4)
addiu	$14,$14,-1
or	$12,$12,$13
beq	$14,$0,$L544
sw	$6,5184($4)

$L505:
addu	$5,$7,$3
sll	$13,$12,1
lbu	$12,0($5)
sll	$3,$3,$12
addu	$2,$12,$2
addiu	$5,$3,1
sw	$3,5168($4)
sll	$6,$6,$12
bltz	$2,$L503
sra	$5,$5,1

lw	$15,5176($4)
lw	$12,5180($4)
sltu	$12,$15,$12
beq	$12,$0,$L503
addiu	$16,$15,2

sw	$16,5176($4)
lbu	$12,1($15)
lbu	$15,0($15)
sll	$12,$12,8
or	$12,$12,$15
sll	$15,$12,8
srl	$12,$12,8
or	$12,$15,$12
andi	$12,$12,0xffff
sll	$12,$12,$2
addiu	$2,$2,-16
or	$6,$6,$12
$L503:
sll	$15,$5,16
sw	$2,5172($4)
sltu	$12,$6,$15
xori	$12,$12,0x1
bne	$12,$0,$L545
subu	$3,$3,$5

move	$3,$5
sw	$6,5184($4)
addiu	$14,$14,-1
sw	$3,5168($4)
bne	$14,$0,$L505
or	$12,$12,$13

$L544:
sll	$12,$12,1
sltu	$2,$12,1
addu	$12,$12,$2
sb	$12,0($10)
lw	$3,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L525
.option	pic2
lw	$16,5184($4)

$L502:
sw	$3,5168($4)
move	$16,$12
sw	$12,5184($4)
$L525:
addiu	$9,$9,2
bne	$9,$11,$L506
addiu	$10,$10,1

lui	$13,%hi(vp6_pdv_pct)
move	$12,$0
addiu	$13,$13,%lo(vp6_pdv_pct)
li	$14,14			# 0xe
move	$11,$0
$L513:
addu	$5,$7,$3
addu	$15,$11,$12
lbu	$9,0($5)
addu	$5,$13,$15
sll	$6,$3,$9
lbu	$17,0($5)
addu	$2,$9,$2
sll	$9,$16,$9
bltz	$2,$L508
sw	$6,5168($4)

lw	$10,5176($4)
lw	$3,5180($4)
sltu	$3,$10,$3
beq	$3,$0,$L553
addiu	$3,$6,-1

addiu	$3,$10,2
sw	$3,5176($4)
lbu	$5,1($10)
lbu	$3,0($10)
sll	$5,$5,8
or	$5,$5,$3
sll	$10,$5,8
srl	$5,$5,8
or	$5,$10,$5
andi	$5,$5,0xffff
sll	$5,$5,$2
addiu	$2,$2,-16
or	$9,$9,$5
$L508:
addiu	$3,$6,-1
$L553:
mul	$3,$3,$17
sra	$3,$3,8
addiu	$3,$3,1
sll	$5,$3,16
sltu	$10,$9,$5
bne	$10,$0,$L509
sw	$2,5172($4)

subu	$5,$9,$5
subu	$3,$6,$3
li	$16,7			# 0x7
move	$6,$0
.option	pic0
j	$L512
.option	pic2
sw	$5,5184($4)

$L547:
subu	$5,$5,$24
sw	$3,5168($4)
addiu	$16,$16,-1
or	$6,$6,$10
beq	$16,$0,$L546
sw	$5,5184($4)

$L512:
addu	$9,$7,$3
sll	$10,$6,1
lbu	$6,0($9)
sll	$3,$3,$6
addu	$2,$6,$2
addiu	$9,$3,1
sw	$3,5168($4)
sll	$5,$5,$6
bltz	$2,$L510
sra	$9,$9,1

lw	$24,5176($4)
lw	$6,5180($4)
sltu	$6,$24,$6
beq	$6,$0,$L510
addiu	$17,$24,2

sw	$17,5176($4)
lbu	$6,1($24)
lbu	$17,0($24)
sll	$6,$6,8
or	$6,$6,$17
sll	$24,$6,8
srl	$6,$6,8
or	$6,$24,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$5,$5,$6
$L510:
sll	$24,$9,16
sw	$2,5172($4)
sltu	$6,$5,$24
xori	$6,$6,0x1
bne	$6,$0,$L547
subu	$3,$3,$9

move	$3,$9
sw	$5,5184($4)
addiu	$16,$16,-1
sw	$3,5168($4)
bne	$16,$0,$L512
or	$6,$6,$10

$L546:
sll	$6,$6,1
addu	$15,$8,$15
sltu	$2,$6,1
addu	$6,$6,$2
addiu	$11,$11,1
li	$5,7			# 0x7
sb	$6,136($15)
lw	$3,5168($4)
lw	$2,5172($4)
bne	$11,$5,$L513
lw	$16,5184($4)

addiu	$12,$12,7
$L552:
bne	$12,$14,$L513
move	$11,$0

lui	$12,%hi(vp6_fdv_pct)
addiu	$12,$12,%lo(vp6_fdv_pct)
li	$13,8			# 0x8
li	$14,16			# 0x10
$L515:
move	$10,$0
$L523:
addu	$5,$7,$3
addu	$15,$10,$11
lbu	$5,0($5)
addu	$6,$12,$15
sll	$3,$3,$5
lbu	$9,0($6)
addu	$2,$5,$2
sll	$16,$16,$5
bltz	$2,$L516
sw	$3,5168($4)

lw	$6,5176($4)
lw	$5,5180($4)
sltu	$5,$6,$5
beq	$5,$0,$L516
addiu	$5,$6,2

sw	$5,5176($4)
lbu	$5,1($6)
lbu	$6,0($6)
sll	$5,$5,8
or	$5,$5,$6
sll	$6,$5,8
srl	$5,$5,8
or	$5,$6,$5
andi	$5,$5,0xffff
sll	$5,$5,$2
addiu	$2,$2,-16
or	$16,$16,$5
$L516:
addiu	$6,$3,-1
mul	$6,$6,$9
sra	$6,$6,8
addiu	$6,$6,1
sll	$5,$6,16
sltu	$9,$16,$5
bne	$9,$0,$L517
sw	$2,5172($4)

subu	$5,$16,$5
subu	$3,$3,$6
li	$25,7			# 0x7
move	$6,$0
.option	pic0
j	$L520
.option	pic2
sw	$5,5184($4)

$L549:
subu	$5,$5,$16
sw	$3,5168($4)
addiu	$25,$25,-1
or	$6,$6,$24
beq	$25,$0,$L548
sw	$5,5184($4)

$L520:
addu	$9,$7,$3
sll	$24,$6,1
lbu	$9,0($9)
sll	$3,$3,$9
addu	$2,$9,$2
addiu	$6,$3,1
sll	$5,$5,$9
sw	$3,5168($4)
bltz	$2,$L518
sra	$9,$6,1

lw	$16,5176($4)
lw	$6,5180($4)
sltu	$6,$16,$6
beq	$6,$0,$L518
addiu	$17,$16,2

sw	$17,5176($4)
lbu	$6,1($16)
lbu	$16,0($16)
sll	$6,$6,8
or	$6,$6,$16
sll	$16,$6,8
srl	$6,$6,8
or	$6,$16,$6
andi	$6,$6,0xffff
sll	$6,$6,$2
addiu	$2,$2,-16
or	$5,$5,$6
$L518:
sll	$16,$9,16
sw	$2,5172($4)
sltu	$6,$5,$16
xori	$6,$6,0x1
bne	$6,$0,$L549
subu	$3,$3,$9

move	$3,$9
sw	$5,5184($4)
addiu	$25,$25,-1
sw	$3,5168($4)
bne	$25,$0,$L520
or	$6,$6,$24

$L548:
sll	$6,$6,1
addu	$15,$8,$15
sltu	$2,$6,1
addu	$6,$6,$2
addiu	$10,$10,1
bne	$10,$13,$L521
sb	$6,150($15)

$L551:
addiu	$11,$11,8
beq	$11,$14,$L495
lw	$17,4($sp)

lw	$3,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L515
.option	pic2
lw	$16,5184($4)

$L509:
addiu	$11,$11,1
sw	$3,5168($4)
li	$5,7			# 0x7
sw	$9,5184($4)
bne	$11,$5,$L513
move	$16,$9

.option	pic0
j	$L552
.option	pic2
addiu	$12,$12,7

$L517:
addiu	$10,$10,1
sw	$6,5168($4)
beq	$10,$13,$L551
sw	$16,5184($4)

$L521:
lw	$3,5168($4)
lw	$2,5172($4)
.option	pic0
j	$L523
.option	pic2
lw	$16,5184($4)

$L495:
lw	$16,0($sp)
j	$31
addiu	$sp,$sp,8

.set	macro
.set	reorder
.end	vp6_parse_vector_models
.size	vp6_parse_vector_models, .-vp6_parse_vector_models
.rdata
.align	2
.type	prob_order.6679, @object
.size	prob_order.6679, 7
prob_order.6679:
.byte	0
.byte	1
.byte	2
.byte	7
.byte	6
.byte	5
.byte	4
.globl	vp6a_decoder
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"vp6a\000"
.align	2
$LC2:
.ascii	"On2 VP6 (Flash version, with alpha channel)\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	vp6a_decoder, @object
.size	vp6a_decoder, 72
vp6a_decoder:
.word	$LC1
.word	0
.word	110
.word	10992
.word	vp6_decode_init
.word	0
.word	vp6_decode_free
.word	ff_vp56_decode_frame
.word	2
.space	16
.word	$LC2
.space	16
.globl	vp6f_decoder
.section	.rodata.str1.4
.align	2
$LC3:
.ascii	"vp6f\000"
.align	2
$LC4:
.ascii	"On2 VP6 (Flash version)\000"
.section	.data.rel
.align	2
.type	vp6f_decoder, @object
.size	vp6f_decoder, 72
vp6f_decoder:
.word	$LC3
.word	0
.word	95
.word	10992
.word	vp6_decode_init
.word	0
.word	vp6_decode_free
.word	ff_vp56_decode_frame
.word	2
.space	16
.word	$LC4
.space	16
.globl	vp6_decoder
.section	.rodata.str1.4
.align	2
$LC5:
.ascii	"vp6\000"
.align	2
$LC6:
.ascii	"On2 VP6\000"
.section	.data.rel
.align	2
.type	vp6_decoder, @object
.size	vp6_decoder, 72
vp6_decoder:
.word	$LC5
.word	0
.word	94
.word	10992
.word	vp6_decode_init
.word	0
.word	vp6_decode_free
.word	ff_vp56_decode_frame
.word	2
.space	16
.word	$LC6
.space	16
.rdata
.align	2
.type	vp6_huff_coeff_map, @object
.size	vp6_huff_coeff_map, 22
vp6_huff_coeff_map:
.byte	13
.byte	14
.byte	11
.byte	0
.byte	1
.byte	15
.byte	16
.byte	18
.byte	2
.byte	17
.byte	3
.byte	4
.byte	19
.byte	20
.byte	5
.byte	6
.byte	21
.byte	22
.byte	7
.byte	8
.byte	9
.byte	10
.align	2
.type	vp6_coord_div, @object
.size	vp6_coord_div, 6
vp6_coord_div:
.byte	4
.byte	4
.byte	4
.byte	4
.byte	8
.byte	8
.align	2
.type	vp6_pcr_tree, @object
.size	vp6_pcr_tree, 34
vp6_pcr_tree:
.byte	8
.byte	0
.byte	4
.byte	1
.byte	2
.byte	2
.byte	-1
.space	1
.byte	-2
.space	1
.byte	2
.byte	3
.byte	-3
.space	1
.byte	-4
.space	1
.byte	8
.byte	4
.byte	4
.byte	5
.byte	2
.byte	6
.byte	-5
.space	1
.byte	-6
.space	1
.byte	2
.byte	7
.byte	-7
.space	1
.byte	-8
.space	1
.byte	0
.space	1
.align	2
.type	vp6_block_copy_filter, @object
.size	vp6_block_copy_filter, 1088
vp6_block_copy_filter:
.half	0
.half	128
.half	0
.half	0
.half	-3
.half	122
.half	9
.half	0
.half	-4
.half	109
.half	24
.half	-1
.half	-5
.half	91
.half	45
.half	-3
.half	-4
.half	68
.half	68
.half	-4
.half	-3
.half	45
.half	91
.half	-5
.half	-1
.half	24
.half	109
.half	-4
.half	0
.half	9
.half	122
.half	-3
.half	0
.half	128
.half	0
.half	0
.half	-4
.half	124
.half	9
.half	-1
.half	-5
.half	110
.half	25
.half	-2
.half	-6
.half	91
.half	46
.half	-3
.half	-5
.half	69
.half	69
.half	-5
.half	-3
.half	46
.half	91
.half	-6
.half	-2
.half	25
.half	110
.half	-5
.half	-1
.half	9
.half	124
.half	-4
.half	0
.half	128
.half	0
.half	0
.half	-4
.half	123
.half	10
.half	-1
.half	-6
.half	110
.half	26
.half	-2
.half	-7
.half	92
.half	47
.half	-4
.half	-6
.half	70
.half	70
.half	-6
.half	-4
.half	47
.half	92
.half	-7
.half	-2
.half	26
.half	110
.half	-6
.half	-1
.half	10
.half	123
.half	-4
.half	0
.half	128
.half	0
.half	0
.half	-5
.half	124
.half	10
.half	-1
.half	-7
.half	110
.half	27
.half	-2
.half	-7
.half	91
.half	48
.half	-4
.half	-6
.half	70
.half	70
.half	-6
.half	-4
.half	48
.half	92
.half	-8
.half	-2
.half	27
.half	110
.half	-7
.half	-1
.half	10
.half	124
.half	-5
.half	0
.half	128
.half	0
.half	0
.half	-6
.half	124
.half	11
.half	-1
.half	-8
.half	111
.half	28
.half	-3
.half	-8
.half	92
.half	49
.half	-5
.half	-7
.half	71
.half	71
.half	-7
.half	-5
.half	49
.half	92
.half	-8
.half	-3
.half	28
.half	111
.half	-8
.half	-1
.half	11
.half	124
.half	-6
.half	0
.half	128
.half	0
.half	0
.half	-6
.half	123
.half	12
.half	-1
.half	-9
.half	111
.half	29
.half	-3
.half	-9
.half	93
.half	50
.half	-6
.half	-8
.half	72
.half	72
.half	-8
.half	-6
.half	50
.half	93
.half	-9
.half	-3
.half	29
.half	111
.half	-9
.half	-1
.half	12
.half	123
.half	-6
.half	0
.half	128
.half	0
.half	0
.half	-7
.half	124
.half	12
.half	-1
.half	-10
.half	111
.half	30
.half	-3
.half	-10
.half	93
.half	51
.half	-6
.half	-9
.half	73
.half	73
.half	-9
.half	-6
.half	51
.half	93
.half	-10
.half	-3
.half	30
.half	111
.half	-10
.half	-1
.half	12
.half	124
.half	-7
.half	0
.half	128
.half	0
.half	0
.half	-7
.half	123
.half	13
.half	-1
.half	-11
.half	112
.half	31
.half	-4
.half	-11
.half	94
.half	52
.half	-7
.half	-10
.half	74
.half	74
.half	-10
.half	-7
.half	52
.half	94
.half	-11
.half	-4
.half	31
.half	112
.half	-11
.half	-1
.half	13
.half	123
.half	-7
.half	0
.half	128
.half	0
.half	0
.half	-8
.half	124
.half	13
.half	-1
.half	-12
.half	112
.half	32
.half	-4
.half	-12
.half	94
.half	53
.half	-7
.half	-10
.half	74
.half	74
.half	-10
.half	-7
.half	53
.half	94
.half	-12
.half	-4
.half	32
.half	112
.half	-12
.half	-1
.half	13
.half	124
.half	-8
.half	0
.half	128
.half	0
.half	0
.half	-9
.half	124
.half	14
.half	-1
.half	-13
.half	112
.half	33
.half	-4
.half	-13
.half	95
.half	54
.half	-8
.half	-11
.half	75
.half	75
.half	-11
.half	-8
.half	54
.half	95
.half	-13
.half	-4
.half	33
.half	112
.half	-13
.half	-1
.half	14
.half	124
.half	-9
.half	0
.half	128
.half	0
.half	0
.half	-9
.half	123
.half	15
.half	-1
.half	-14
.half	113
.half	34
.half	-5
.half	-14
.half	95
.half	55
.half	-8
.half	-12
.half	76
.half	76
.half	-12
.half	-8
.half	55
.half	95
.half	-14
.half	-5
.half	34
.half	112
.half	-13
.half	-1
.half	15
.half	123
.half	-9
.half	0
.half	128
.half	0
.half	0
.half	-10
.half	124
.half	15
.half	-1
.half	-14
.half	113
.half	34
.half	-5
.half	-15
.half	96
.half	56
.half	-9
.half	-13
.half	77
.half	77
.half	-13
.half	-9
.half	56
.half	96
.half	-15
.half	-5
.half	34
.half	113
.half	-14
.half	-1
.half	15
.half	124
.half	-10
.half	0
.half	128
.half	0
.half	0
.half	-10
.half	123
.half	16
.half	-1
.half	-15
.half	113
.half	35
.half	-5
.half	-16
.half	98
.half	56
.half	-10
.half	-14
.half	78
.half	78
.half	-14
.half	-10
.half	56
.half	98
.half	-16
.half	-5
.half	35
.half	113
.half	-15
.half	-1
.half	16
.half	123
.half	-10
.half	0
.half	128
.half	0
.half	0
.half	-11
.half	124
.half	17
.half	-2
.half	-16
.half	113
.half	36
.half	-5
.half	-17
.half	98
.half	57
.half	-10
.half	-14
.half	78
.half	78
.half	-14
.half	-10
.half	57
.half	98
.half	-17
.half	-5
.half	36
.half	113
.half	-16
.half	-2
.half	17
.half	124
.half	-11
.half	0
.half	128
.half	0
.half	0
.half	-12
.half	125
.half	17
.half	-2
.half	-17
.half	114
.half	37
.half	-6
.half	-18
.half	99
.half	58
.half	-11
.half	-15
.half	79
.half	79
.half	-15
.half	-11
.half	58
.half	99
.half	-18
.half	-6
.half	37
.half	114
.half	-17
.half	-2
.half	17
.half	125
.half	-12
.half	0
.half	128
.half	0
.half	0
.half	-12
.half	124
.half	18
.half	-2
.half	-18
.half	114
.half	38
.half	-6
.half	-19
.half	99
.half	59
.half	-11
.half	-16
.half	80
.half	80
.half	-16
.half	-11
.half	59
.half	99
.half	-19
.half	-6
.half	38
.half	114
.half	-18
.half	-2
.half	18
.half	124
.half	-12
.half	0
.half	128
.half	0
.half	0
.half	-4
.half	118
.half	16
.half	-2
.half	-7
.half	106
.half	34
.half	-5
.half	-8
.half	90
.half	53
.half	-7
.half	-8
.half	72
.half	72
.half	-8
.half	-7
.half	53
.half	90
.half	-8
.half	-5
.half	34
.half	106
.half	-7
.half	-2
.half	16
.half	118
.half	-4
.align	2
.type	vp6_coeff_groups, @object
.size	vp6_coeff_groups, 64
vp6_coeff_groups:
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.byte	5
.align	2
.type	vp6_ract_pct, @object
.size	vp6_ract_pct, 396
vp6_ract_pct:
.byte	-29
.byte	-10
.byte	-26
.byte	-9
.byte	-12
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-47
.byte	-25
.byte	-25
.byte	-7
.byte	-7
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-31
.byte	-14
.byte	-15
.byte	-5
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-15
.byte	-3
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-16
.byte	-1
.byte	-8
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-16
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-50
.byte	-53
.byte	-29
.byte	-17
.byte	-9
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-49
.byte	-57
.byte	-36
.byte	-20
.byte	-13
.byte	-4
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-44
.byte	-37
.byte	-26
.byte	-13
.byte	-12
.byte	-3
.byte	-4
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-20
.byte	-19
.byte	-9
.byte	-4
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-16
.byte	-16
.byte	-8
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-26
.byte	-23
.byte	-7
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-18
.byte	-18
.byte	-6
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-8
.byte	-5
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-31
.byte	-17
.byte	-29
.byte	-25
.byte	-12
.byte	-3
.byte	-13
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-24
.byte	-22
.byte	-32
.byte	-28
.byte	-14
.byte	-7
.byte	-14
.byte	-4
.byte	-5
.byte	-5
.byte	-1
.byte	-21
.byte	-7
.byte	-18
.byte	-16
.byte	-5
.byte	-1
.byte	-7
.byte	-1
.byte	-3
.byte	-3
.byte	-1
.byte	-7
.byte	-3
.byte	-5
.byte	-6
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-5
.byte	-6
.byte	-7
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-13
.byte	-12
.byte	-6
.byte	-6
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-7
.byte	-8
.byte	-6
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.align	2
.type	vp6_runv_pct, @object
.size	vp6_runv_pct, 28
vp6_runv_pct:
.byte	-37
.byte	-10
.byte	-18
.byte	-7
.byte	-24
.byte	-17
.byte	-7
.byte	-1
.byte	-8
.byte	-3
.byte	-17
.byte	-12
.byte	-15
.byte	-8
.byte	-58
.byte	-24
.byte	-5
.byte	-3
.byte	-37
.byte	-15
.byte	-3
.byte	-1
.byte	-8
.byte	-7
.byte	-12
.byte	-18
.byte	-5
.byte	-1
.align	2
.type	vp6_coeff_reorder_pct, @object
.size	vp6_coeff_reorder_pct, 64
vp6_coeff_reorder_pct:
.byte	-1
.byte	-124
.byte	-124
.byte	-97
.byte	-103
.byte	-105
.byte	-95
.byte	-86
.byte	-92
.byte	-94
.byte	-120
.byte	110
.byte	103
.byte	114
.byte	-127
.byte	118
.byte	124
.byte	125
.byte	-124
.byte	-120
.byte	114
.byte	110
.byte	-114
.byte	-121
.byte	-122
.byte	123
.byte	-113
.byte	126
.byte	-103
.byte	-73
.byte	-90
.byte	-95
.byte	-85
.byte	-76
.byte	-77
.byte	-92
.byte	-53
.byte	-38
.byte	-31
.byte	-39
.byte	-41
.byte	-50
.byte	-53
.byte	-39
.byte	-27
.byte	-15
.byte	-8
.byte	-13
.byte	-3
.byte	-1
.byte	-3
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.align	2
.type	vp6_dccv_pct, @object
.size	vp6_dccv_pct, 22
vp6_dccv_pct:
.byte	-110
.byte	-1
.byte	-75
.byte	-49
.byte	-24
.byte	-13
.byte	-18
.byte	-5
.byte	-12
.byte	-6
.byte	-7
.byte	-77
.byte	-1
.byte	-42
.byte	-16
.byte	-6
.byte	-1
.byte	-12
.byte	-1
.byte	-1
.byte	-1
.byte	-1
.align	2
.type	vp6_fdv_pct, @object
.size	vp6_fdv_pct, 16
vp6_fdv_pct:
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-6
.byte	-6
.byte	-4
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-5
.byte	-5
.byte	-2
.align	2
.type	vp6_pdv_pct, @object
.size	vp6_pdv_pct, 14
vp6_pdv_pct:
.byte	-3
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-11
.byte	-3
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.byte	-2
.align	2
.type	vp6_sig_dct_pct, @object
.size	vp6_sig_dct_pct, 4
vp6_sig_dct_pct:
.byte	-19
.byte	-10
.byte	-25
.byte	-13
.align	2
.type	vp6_def_runv_coeff_model, @object
.size	vp6_def_runv_coeff_model, 28
vp6_def_runv_coeff_model:
.byte	-58
.byte	-59
.byte	-60
.byte	-110
.byte	-58
.byte	-52
.byte	-87
.byte	-114
.byte	-126
.byte	-120
.byte	-107
.byte	-107
.byte	-65
.byte	-7
.byte	-121
.byte	-55
.byte	-75
.byte	-102
.byte	98
.byte	117
.byte	-124
.byte	126
.byte	-110
.byte	-87
.byte	-72
.byte	-16
.byte	-10
.byte	-2
.align	2
.type	vp6_def_coeff_reorder, @object
.size	vp6_def_coeff_reorder, 64
vp6_def_coeff_reorder:
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	2
.byte	3
.byte	3
.byte	4
.byte	4
.byte	4
.byte	5
.byte	5
.byte	5
.byte	5
.byte	6
.byte	6
.byte	7
.byte	7
.byte	7
.byte	7
.byte	7
.byte	8
.byte	8
.byte	9
.byte	9
.byte	9
.byte	9
.byte	9
.byte	9
.byte	10
.byte	10
.byte	11
.byte	11
.byte	11
.byte	11
.byte	11
.byte	11
.byte	12
.byte	12
.byte	12
.byte	12
.byte	12
.byte	12
.byte	13
.byte	13
.byte	13
.byte	13
.byte	13
.byte	14
.byte	14
.byte	14
.byte	14
.byte	15
.byte	15
.byte	15
.byte	15
.byte	15
.byte	15
.align	2
.type	vp6_def_pdv_vector_model, @object
.size	vp6_def_pdv_vector_model, 14
vp6_def_pdv_vector_model:
.byte	-31
.byte	-110
.byte	-84
.byte	-109
.byte	-42
.byte	39
.byte	-100
.byte	-52
.byte	-86
.byte	119
.byte	-21
.byte	-116
.byte	-26
.byte	-28
.align	2
.type	vp6_def_fdv_vector_model, @object
.size	vp6_def_fdv_vector_model, 16
vp6_def_fdv_vector_model:
.byte	-9
.byte	-46
.byte	-121
.byte	68
.byte	-118
.byte	-36
.byte	-17
.byte	-10
.byte	-12
.byte	-72
.byte	-55
.byte	44
.byte	-83
.byte	-35
.byte	-17
.byte	-3
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
