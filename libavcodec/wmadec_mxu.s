.file	1 "wmadec.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	wma_vector_fmul_add_c
.type	wma_vector_fmul_add_c, @function
wma_vector_fmul_add_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lw	$2,16($sp)
addiu	$7,$7,-2
.set	noreorder
.set	nomacro
blez	$2,$L7
addiu	$4,$4,-2
.set	macro
.set	reorder

addiu	$9,$2,-1
li	$8,18			# 0x12
srl	$9,$9,2
addiu	$2,$5,4
sll	$9,$9,4
li	$3,16			# 0x10
addiu	$9,$9,20
addu	$5,$5,$9
$L3:
lw	$9,-4($2)
lw	$10,0($6)
#APP
# 476 "wmadec.c" 1
.word	0b01110001001010100000100001100110	#S32MUL XR1,XR2,$9,$10
# 0 "" 2
#NO_APP
lw	$9,0($2)
lw	$10,4($6)
#APP
# 477 "wmadec.c" 1
.word	0b01110001001010100001000011100110	#S32MUL XR3,XR4,$9,$10
# 0 "" 2
#NO_APP
lw	$9,4($2)
lw	$10,8($6)
#APP
# 478 "wmadec.c" 1
.word	0b01110001001010100001100101100110	#S32MUL XR5,XR6,$9,$10
# 0 "" 2
#NO_APP
lw	$9,8($2)
lw	$10,12($6)
#APP
# 479 "wmadec.c" 1
.word	0b01110001001010100010000111100110	#S32MUL XR7,XR8,$9,$10
# 0 "" 2
# 480 "wmadec.c" 1
.word	0b01110001000000111100100001100110	#S32EXTRV XR1,XR2,$8,$3
# 0 "" 2
# 481 "wmadec.c" 1
.word	0b01110001000000111101000011100110	#S32EXTRV XR3,XR4,$8,$3
# 0 "" 2
# 482 "wmadec.c" 1
.word	0b01110001000000111101100101100110	#S32EXTRV XR5,XR6,$8,$3
# 0 "" 2
# 483 "wmadec.c" 1
.word	0b01110001000000111110000111100110	#S32EXTRV XR7,XR8,$8,$3
# 0 "" 2
# 484 "wmadec.c" 1
.word	0b01110000111000000000011001101100	#S16LDI XR9,$7,2,PTN0
# 0 "" 2
# 485 "wmadec.c" 1
.word	0b01110000111000000000011010101100	#S16LDI XR10,$7,2,PTN0
# 0 "" 2
# 486 "wmadec.c" 1
.word	0b01110000111000000000011011101100	#S16LDI XR11,$7,2,PTN0
# 0 "" 2
# 487 "wmadec.c" 1
.word	0b01110000111000000000011100101100	#S16LDI XR12,$7,2,PTN0
# 0 "" 2
# 488 "wmadec.c" 1
.word	0b01110000000000100100010010011000	#D32ADD XR2,XR1,XR9,XR0,AA
# 0 "" 2
# 489 "wmadec.c" 1
.word	0b01110000000000101000110100011000	#D32ADD XR4,XR3,XR10,XR0,AA
# 0 "" 2
# 490 "wmadec.c" 1
.word	0b01110000000000101101010110011000	#D32ADD XR6,XR5,XR11,XR0,AA
# 0 "" 2
# 491 "wmadec.c" 1
.word	0b01110000000000110001111000011000	#D32ADD XR8,XR7,XR12,XR0,AA
# 0 "" 2
# 492 "wmadec.c" 1
.word	0b01110000100000000000010010101101	#S16SDI XR2,$4,2,PTN0
# 0 "" 2
# 493 "wmadec.c" 1
.word	0b01110000100000000000010100101101	#S16SDI XR4,$4,2,PTN0
# 0 "" 2
# 494 "wmadec.c" 1
.word	0b01110000100000000000010110101101	#S16SDI XR6,$4,2,PTN0
# 0 "" 2
# 495 "wmadec.c" 1
.word	0b01110000100000000000011000101101	#S16SDI XR8,$4,2,PTN0
# 0 "" 2
#NO_APP
addiu	$2,$2,16
.set	noreorder
.set	nomacro
bne	$2,$5,$L3
addiu	$6,$6,16
.set	macro
.set	reorder

$L7:
j	$31
.end	wma_vector_fmul_add_c
.size	wma_vector_fmul_add_c, .-wma_vector_fmul_add_c
.align	2
.set	nomips16
.set	nomicromips
.ent	wma_vector_fmul_reverse_c
.type	wma_vector_fmul_reverse_c, @function
wma_vector_fmul_reverse_c:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
sll	$2,$7,2
addiu	$4,$4,-2
addiu	$2,$2,-4
.set	noreorder
.set	nomacro
blez	$7,$L13
addu	$6,$6,$2
.set	macro
.set	reorder

addiu	$7,$7,-1
li	$3,18			# 0x12
srl	$7,$7,2
li	$2,16			# 0x10
nor	$7,$0,$7
sll	$7,$7,4
addu	$7,$6,$7
$L10:
lw	$8,0($5)
lw	$9,0($6)
#APP
# 531 "wmadec.c" 1
.word	0b01110001000010010000100001100110	#S32MUL XR1,XR2,$8,$9
# 0 "" 2
#NO_APP
lw	$8,4($5)
lw	$9,-4($6)
#APP
# 532 "wmadec.c" 1
.word	0b01110001000010010001000011100110	#S32MUL XR3,XR4,$8,$9
# 0 "" 2
#NO_APP
lw	$8,8($5)
lw	$9,-8($6)
#APP
# 533 "wmadec.c" 1
.word	0b01110001000010010001100101100110	#S32MUL XR5,XR6,$8,$9
# 0 "" 2
#NO_APP
lw	$8,12($5)
lw	$9,-12($6)
#APP
# 534 "wmadec.c" 1
.word	0b01110001000010010010000111100110	#S32MUL XR7,XR8,$8,$9
# 0 "" 2
# 535 "wmadec.c" 1
.word	0b01110000011000101100100001100110	#S32EXTRV XR1,XR2,$3,$2
# 0 "" 2
# 536 "wmadec.c" 1
.word	0b01110000011000101101000011100110	#S32EXTRV XR3,XR4,$3,$2
# 0 "" 2
# 537 "wmadec.c" 1
.word	0b01110000011000101101100101100110	#S32EXTRV XR5,XR6,$3,$2
# 0 "" 2
# 538 "wmadec.c" 1
.word	0b01110000011000101110000111100110	#S32EXTRV XR7,XR8,$3,$2
# 0 "" 2
# 539 "wmadec.c" 1
.word	0b01110000100000000000010001101101	#S16SDI XR1,$4,2,PTN0
# 0 "" 2
# 540 "wmadec.c" 1
.word	0b01110000100000000000010011101101	#S16SDI XR3,$4,2,PTN0
# 0 "" 2
# 541 "wmadec.c" 1
.word	0b01110000100000000000010101101101	#S16SDI XR5,$4,2,PTN0
# 0 "" 2
# 542 "wmadec.c" 1
.word	0b01110000100000000000010111101101	#S16SDI XR7,$4,2,PTN0
# 0 "" 2
#NO_APP
addiu	$6,$6,-16
.set	noreorder
.set	nomacro
bne	$6,$7,$L10
addiu	$5,$5,16
.set	macro
.set	reorder

$L13:
j	$31
.end	wma_vector_fmul_reverse_c
.size	wma_vector_fmul_reverse_c, .-wma_vector_fmul_reverse_c
.section	.text.unlikely,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	flush
.type	flush, @function
flush:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$2,136($4)
li	$3,65536			# 0x10000
addu	$2,$2,$3
sw	$0,26104($2)
j	$31
sw	$0,26100($2)

.set	macro
.set	reorder
.end	flush
.size	flush, .-flush
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	wma_decode_init
.type	wma_decode_init, @function
wma_decode_init:
.frame	$sp,104,$31		# vars= 0, regs= 6/4, args= 56, gp= 8
.mask	0x801f0000,-20
.fmask	0x00f00000,-8
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-104
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,68($sp)
li	$2,7			# 0x7
sw	$31,84($sp)
move	$17,$4
sw	$20,80($sp)
sw	$19,76($sp)
sw	$18,72($sp)
sw	$16,64($sp)
sdc1	$f22,96($sp)
.cprestore	56
sdc1	$f20,88($sp)
#APP
# 116 "wmadec.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
lw	$3,132($4)
li	$2,65536			# 0x10000
lw	$16,136($4)
addiu	$4,$2,20487
lw	$5,24($17)
lw	$3,8($3)
.set	noreorder
.set	nomacro
beq	$3,$4,$L40
sw	$17,0($16)
.set	macro
.set	reorder

addiu	$2,$2,20488
beq	$3,$2,$L41
$L33:
move	$2,$0
move	$3,$0
move	$6,$0
move	$5,$0
$L17:
lw	$25,%call16(ff_wma_init)($28)
move	$4,$17
sw	$6,72($16)
sw	$3,64($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_wma_init
1:	jalr	$25
sw	$2,68($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L34
lw	$28,56($sp)
.set	macro
.set	reorder

lw	$2,1100($16)
.set	noreorder
.set	nomacro
blez	$2,$L23
lui	$2,%hi($LC0)
.set	macro
.set	reorder

li	$19,58512			# 0xe490
ldc1	$f20,%lo($LC0)($2)
move	$18,$0
addu	$19,$16,$19
$L22:
lw	$5,1096($16)
li	$6,1			# 0x1
lw	$25,%call16(aac_mdct_init)($28)
move	$4,$19
sdc1	$f20,16($sp)
addiu	$19,$19,64
subu	$5,$5,$18
addiu	$18,$18,1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,aac_mdct_init
1:	jalr	$25
addiu	$5,$5,1
.set	macro
.set	reorder

lw	$2,1100($16)
slt	$2,$18,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L22
lw	$28,56($sp)
.set	macro
.set	reorder

$L23:
lw	$2,76($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L42
li	$3,1			# 0x1
.set	macro
.set	reorder

$L21:
lw	$2,72($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L43
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$19,1092($16)
lui	$2,%hi($LC4)
mtc1	$19,$f0
cvt.d.w	$f2,$f0
ldc1	$f0,%lo($LC4)($2)
div.d	$f0,$f0,$f2
.set	noreorder
.set	nomacro
blez	$19,$L26
cvt.s.d	$f20,$f0
.set	macro
.set	reorder

li	$20,65536			# 0x10000
lui	$2,%hi($LC0)
ori	$20,$20,0xe604
ldc1	$f0,%lo($LC0)($2)
move	$18,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L27
.option	pic2
addu	$20,$16,$20
.set	macro
.set	reorder

$L44:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,cos
1:	jalr	$25
cvt.d.s	$f12,$f12
.set	macro
.set	reorder

lw	$28,56($sp)
$L27:
addiu	$18,$18,1
add.d	$f0,$f0,$f0
lw	$25,%call16(cos)($28)
addiu	$20,$20,4
mtc1	$18,$f2
cvt.s.d	$f0,$f0
cvt.s.w	$f12,$f2
swc1	$f0,-4($20)
.set	noreorder
.set	nomacro
bne	$18,$19,$L44
mul.s	$f12,$f20,$f12
.set	macro
.set	reorder

$L26:
li	$2,131072			# 0x20000
lui	$3,%hi($LC1)
addiu	$18,$2,1540
addiu	$2,$2,2564
ldc1	$f0,%lo($LC1)($3)
lui	$3,%hi($LC5)
addu	$19,$16,$2
lui	$2,%hi($LC6)
ldc1	$f22,%lo($LC5)($3)
addu	$18,$16,$18
li	$20,-125			# 0xffffffffffffff83
.option	pic0
.set	noreorder
.set	nomacro
j	$L30
.option	pic2
ldc1	$f20,%lo($LC6)($2)
.set	macro
.set	reorder

$L28:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,pow
1:	jalr	$25
mul.d	$f14,$f14,$f20
.set	macro
.set	reorder

lw	$28,56($sp)
$L30:
mtc1	$20,$f3
addiu	$18,$18,4
lw	$25,%call16(pow)($28)
addiu	$20,$20,1
cvt.s.d	$f0,$f0
mov.d	$f12,$f22
cvt.d.w	$f14,$f3
.set	noreorder
.set	nomacro
bne	$18,$19,$L28
swc1	$f0,-4($18)
.set	macro
.set	reorder

lui	$2,%hi($LC2)
li	$19,254			# 0xfe
ldc1	$f0,%lo($LC2)($2)
lui	$2,%hi($LC3)
li	$18,127			# 0x7f
lwc1	$f2,%lo($LC3)($2)
lui	$2,%hi($LC7)
lwc1	$f21,%lo($LC7)($2)
lui	$2,%hi($LC6)
ldc1	$f22,%lo($LC6)($2)
li	$2,131072			# 0x20000
addiu	$2,$2,3584
.option	pic0
.set	noreorder
.set	nomacro
j	$L29
.option	pic2
addu	$16,$16,$2
.set	macro
.set	reorder

$L45:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,pow
1:	jalr	$25
addiu	$19,$19,-1
.set	macro
.set	reorder

mov.s	$f2,$f20
lw	$28,56($sp)
$L29:
cvt.s.d	$f20,$f0
addiu	$16,$16,-4
lw	$25,%call16(pow)($28)
mtc1	$19,$f3
mov.d	$f14,$f22
add.s	$f1,$f20,$f20
cvt.s.w	$f12,$f3
sub.s	$f3,$f2,$f20
sub.s	$f0,$f1,$f2
mul.s	$f12,$f12,$f21
swc1	$f3,4($16)
swc1	$f0,-508($16)
.set	noreorder
.set	nomacro
bne	$19,$18,$L45
cvt.d.s	$f12,$f12
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L46
.option	pic2
li	$3,1			# 0x1
.set	macro
.set	reorder

$L43:
lw	$25,%call16(init_vlc_sparse)($28)
li	$2,4			# 0x4
lw	$7,%got(ff_aac_scalefactor_bits)($28)
addiu	$4,$16,84
sw	$0,36($sp)
sw	$3,16($sp)
li	$5,8			# 0x8
sw	$3,20($sp)
li	$6,121			# 0x79
sw	$2,28($sp)
sw	$2,32($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$0,48($sp)
lw	$3,%got(ff_aac_scalefactor_code)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,24($sp)
.set	macro
.set	reorder

li	$3,1			# 0x1
$L46:
move	$2,$0
sw	$3,72($17)
$L36:
lw	$31,84($sp)
lw	$20,80($sp)
lw	$19,76($sp)
lw	$18,72($sp)
lw	$17,68($sp)
lw	$16,64($sp)
ldc1	$f22,96($sp)
ldc1	$f20,88($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,104
.set	macro
.set	reorder

$L41:
lw	$2,28($17)
slt	$2,$2,6
bne	$2,$0,$L33
lbu	$2,5($5)
.option	pic0
.set	noreorder
.set	nomacro
j	$L38
.option	pic2
lbu	$5,4($5)
.set	macro
.set	reorder

$L42:
lw	$25,%call16(init_vlc_sparse)($28)
li	$2,2			# 0x2
lw	$7,%got(ff_wma_hgain_huffbits)($28)
addiu	$4,$16,756
sw	$0,36($sp)
sw	$3,16($sp)
li	$5,9			# 0x9
sw	$3,20($sp)
li	$6,37			# 0x25
sw	$2,28($sp)
sw	$2,32($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$0,48($sp)
lw	$3,%got(ff_wma_hgain_huffcodes)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,24($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L21
.option	pic2
lw	$28,56($sp)
.set	macro
.set	reorder

$L40:
lw	$2,28($17)
slt	$2,$2,4
bne	$2,$0,$L33
lbu	$2,3($5)
lbu	$5,2($5)
$L38:
sll	$2,$2,8
or	$5,$2,$5
andi	$3,$5,0x2
andi	$2,$5,0x4
andi	$6,$5,0x1
andi	$3,$3,0xffff
.option	pic0
.set	noreorder
.set	nomacro
j	$L17
.option	pic2
andi	$2,$2,0xffff
.set	macro
.set	reorder

$L34:
.option	pic0
.set	noreorder
.set	nomacro
j	$L36
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	wma_decode_init
.size	wma_decode_init, .-wma_decode_init
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC8:
.ascii	"prev_block_len_bits %d out of range\012\000"
.align	2
$LC9:
.ascii	"block_len_bits %d out of range\012\000"
.align	2
$LC10:
.ascii	"next_block_len_bits %d out of range\012\000"
.align	2
$LC11:
.ascii	"frame_len overflow\012\000"
.align	2
$LC12:
.ascii	"hgain vlc invalid\012\000"
.align	2
$LC13:
.ascii	"Exponent vlc invalid\012\000"
.align	2
$LC14:
.ascii	"Exponent out of range: %d\012\000"
.globl	__divdi3
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	wma_decode_block
.type	wma_decode_block, @function
wma_decode_block:
.frame	$sp,464,$31		# vars= 344, regs= 10/6, args= 48, gp= 8
.mask	0xc0ff0000,-28
.fmask	0x03f00000,-8
lui	$28,%hi(__gnu_local_gp)
lw	$2,68($4)
addiu	$sp,$sp,-464
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$16,400($sp)
move	$16,$4
sw	$31,436($sp)
sw	$fp,432($sp)
sw	$23,428($sp)
sw	$22,424($sp)
sw	$21,420($sp)
sw	$20,416($sp)
sw	$19,412($sp)
sw	$18,408($sp)
sw	$17,404($sp)
sdc1	$f24,456($sp)
.cprestore	48
sdc1	$f22,448($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L48
sdc1	$f20,440($sp)
.set	macro
.set	reorder

lw	$9,1100($4)
li	$5,32			# 0x20
lw	$3,1104($4)
addiu	$2,$9,-1
ori	$2,$2,0x1
clz	$6,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L247
subu	$8,$5,$6
.set	macro
.set	reorder

lw	$4,1108($4)
lw	$3,1112($16)
lw	$5,12($16)
lw	$2,4($16)
lw	$7,1096($16)
sw	$4,1116($16)
sw	$3,1108($16)
$L53:
srl	$3,$5,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$3,$4,8
addiu	$2,$2,255
sll	$4,$4,8
and	$3,$3,$2
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$4,$4,$2
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
andi	$2,$5,0x7
or	$3,$3,$4
sll	$2,$3,$2
addu	$5,$5,$8
srl	$2,$2,$6
slt	$3,$2,$9
.set	noreorder
.set	nomacro
beq	$3,$0,$L248
sw	$5,12($16)
.set	macro
.set	reorder

subu	$2,$7,$2
lw	$17,1108($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L55
.option	pic2
sw	$2,1112($16)
.set	macro
.set	reorder

$L48:
lw	$17,1096($4)
move	$7,$17
sw	$17,1112($4)
sw	$17,1116($4)
sw	$17,1108($4)
$L55:
li	$20,1			# 0x1
lw	$2,1128($16)
lw	$3,1092($16)
sll	$20,$20,$17
addu	$2,$20,$2
slt	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L249
sw	$20,1120($16)
.set	macro
.set	reorder

lw	$18,48($16)
li	$2,2			# 0x2
beq	$18,$2,$L250
blez	$18,$L59
lw	$8,4($16)
lw	$4,12($16)
$L58:
srl	$3,$4,3
andi	$2,$4,0x7
addu	$3,$8,$3
addiu	$6,$4,1
slt	$5,$18,2
lbu	$3,0($3)
sw	$6,12($16)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$5,$0,$L60
sb	$2,1133($16)
.set	macro
.set	reorder

srl	$5,$6,3
andi	$9,$6,0x7
addu	$5,$8,$5
addiu	$6,$4,2
lbu	$3,0($5)
sw	$6,12($16)
sll	$3,$3,$9
andi	$3,$3,0x00ff
srl	$3,$3,7
or	$2,$3,$2
sb	$3,1134($16)
$L60:
.set	noreorder
.set	nomacro
beq	$2,$0,$L61
subu	$17,$7,$17
.set	macro
.set	reorder

li	$9,16711680			# 0xff0000
li	$7,-16777216			# 0xffffffffff000000
li	$19,1			# 0x1
addiu	$9,$9,255
li	$10,127			# 0x7f
ori	$7,$7,0xff00
$L62:
srl	$3,$6,3
andi	$5,$6,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$9
and	$4,$4,$7
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
addiu	$6,$6,7
or	$2,$3,$4
sll	$2,$2,$5
sw	$6,12($16)
srl	$2,$2,25
.set	noreorder
.set	nomacro
beq	$2,$10,$L62
addu	$19,$19,$2
.set	macro
.set	reorder

lw	$25,%call16(ff_wma_total_gain_to_bits)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_wma_total_gain_to_bits
1:	jalr	$25
move	$4,$19
.set	macro
.set	reorder

sll	$3,$17,2
lw	$4,392($16)
addu	$24,$16,$3
lw	$18,48($16)
lw	$28,48($sp)
sw	$2,352($sp)
lw	$2,396($24)
.set	noreorder
.set	nomacro
blez	$18,$L251
subu	$4,$2,$4
.set	macro
.set	reorder

addiu	$6,$sp,56
sll	$3,$18,2
move	$2,$6
addu	$3,$6,$3
$L66:
sw	$4,0($2)
addiu	$2,$2,4
bne	$2,$3,$L66
lw	$2,76($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L65
addu	$20,$16,$18
.set	macro
.set	reorder

sll	$10,$17,6
move	$7,$0
li	$12,772			# 0x304
move	$14,$16
move	$11,$16
$L71:
lbu	$2,1133($11)
beq	$2,$0,$L73
lw	$9,416($24)
blez	$9,$L73
lw	$3,12($16)
subu	$4,$12,$7
lw	$13,4($16)
addu	$4,$16,$4
addu	$9,$9,$3
srl	$5,$3,3
$L272:
addu	$2,$4,$7
addu	$5,$13,$5
addu	$8,$2,$10
andi	$15,$3,0x7
lbu	$2,0($5)
addiu	$3,$3,1
addiu	$4,$4,4
sll	$2,$2,$15
sw	$3,12($16)
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L74
sw	$2,-4($4)
.set	macro
.set	reorder

lw	$2,0($6)
lw	$5,-336($8)
subu	$2,$2,$5
sw	$2,0($6)
$L74:
.set	noreorder
.set	nomacro
bne	$9,$3,$L272
srl	$5,$3,3
.set	macro
.set	reorder

$L73:
addiu	$11,$11,1
addiu	$6,$6,4
.set	noreorder
.set	nomacro
bne	$11,$20,$L71
addiu	$7,$7,-64
.set	macro
.set	reorder

li	$12,16711680			# 0xff0000
li	$11,-16777216			# 0xffffffffff000000
addiu	$15,$16,772
li	$13,-2147483648			# 0xffffffff80000000
addiu	$12,$12,255
ori	$11,$11,0xff00
$L77:
lbu	$2,1133($14)
beq	$2,$0,$L79
lw	$21,416($24)
blez	$21,$L79
li	$7,-2147483648			# 0xffffffff80000000
move	$6,$0
move	$5,$15
$L86:
lw	$2,0($5)
beq	$2,$0,$L80
beq	$7,$13,$L252
lw	$8,12($16)
lw	$10,4($16)
lw	$9,760($16)
srl	$2,$8,3
andi	$22,$8,0x7
addu	$2,$10,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sll	$4,$2,8
srl	$3,$3,8
and	$4,$4,$11
and	$3,$3,$12
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$22
srl	$2,$2,23
sll	$2,$2,2
addu	$2,$9,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L253
lh	$2,0($2)
.set	macro
.set	reorder

$L84:
addu	$8,$3,$8
addiu	$4,$2,-18
sw	$8,12($16)
.set	noreorder
.set	nomacro
bltz	$2,$L254
addu	$7,$7,$4
.set	macro
.set	reorder

$L82:
sw	$7,128($5)
$L80:
addiu	$6,$6,1
.set	noreorder
.set	nomacro
bne	$6,$21,$L86
addiu	$5,$5,4
.set	macro
.set	reorder

$L79:
addiu	$14,$14,1
.set	noreorder
.set	nomacro
bne	$20,$14,$L77
addiu	$15,$15,64
.set	macro
.set	reorder

lw	$3,1108($16)
lw	$2,1096($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L273
lui	$2,%hi($LC15)
.set	macro
.set	reorder

$L69:
lw	$3,12($16)
lw	$4,4($16)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L255
sw	$3,12($16)
.set	macro
.set	reorder

$L68:
.set	noreorder
.set	nomacro
blez	$18,$L176
lui	$2,%hi($LC15)
.set	macro
.set	reorder

$L273:
lw	$20,1120($16)
li	$12,16711680			# 0xff0000
lwc1	$f6,%lo($LC15)($2)
lui	$2,%hi($LC16)
li	$11,-16777216			# 0xffffffffff000000
lwc1	$f5,%lo($LC16)($2)
lui	$2,%hi($LC17)
li	$3,8323072			# 0x7f0000
lwc1	$f7,%lo($LC17)($2)
lui	$2,%hi($LC18)
addiu	$15,$16,17536
ldc1	$f8,%lo($LC18)($2)
li	$2,65536			# 0x10000
addiu	$14,$16,1152
ori	$2,$2,0xe604
addu	$2,$16,$2
move	$13,$0
addiu	$12,$12,255
sw	$2,356($sp)
lui	$2,%hi(pow_tab+240)
ori	$11,$11,0xff00
addiu	$23,$2,%lo(pow_tab+240)
ori	$3,$3,0xffff
$L120:
addu	$2,$16,$13
lbu	$2,1133($2)
beq	$2,$0,$L90
lw	$21,72($16)
.set	noreorder
.set	nomacro
beq	$21,$0,$L91
addiu	$7,$sp,312
.set	macro
.set	reorder

lw	$4,1096($16)
sll	$2,$20,2
lw	$5,1108($16)
move	$9,$14
lw	$7,56($16)
addu	$2,$14,$2
subu	$5,$4,$5
sll	$4,$5,1
sll	$5,$5,3
addu	$4,$4,$5
sll	$10,$4,2
addu	$10,$4,$10
li	$4,1			# 0x1
addiu	$10,$10,120
.set	noreorder
.set	nomacro
beq	$7,$4,$L92
addu	$10,$16,$10
.set	macro
.set	reorder

move	$18,$0
li	$fp,36			# 0x24
$L99:
sltu	$4,$9,$2
beq	$4,$0,$L94
lw	$21,88($16)
li	$22,2			# 0x2
lw	$20,4($16)
$L109:
lw	$5,12($16)
srl	$4,$5,3
andi	$8,$5,0x7
addu	$4,$20,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$6,8
sll	$7,$6,8
and	$4,$4,$12
and	$7,$7,$11
or	$7,$4,$7
sll	$6,$7,16
srl	$7,$7,16
or	$4,$6,$7
sll	$4,$4,$8
srl	$4,$4,24
sll	$4,$4,2
addu	$4,$21,$4
lh	$6,2($4)
.set	noreorder
.set	nomacro
bltz	$6,$L256
lh	$4,0($4)
.set	macro
.set	reorder

$L100:
addu	$5,$6,$5
.set	noreorder
.set	nomacro
bltz	$4,$L257
sw	$5,12($16)
.set	macro
.set	reorder

addiu	$4,$4,-60
addu	$fp,$fp,$4
addiu	$4,$fp,60
sltu	$4,$4,157
.set	noreorder
.set	nomacro
beq	$4,$0,$L258
lui	$6,%hi($LC14)
.set	macro
.set	reorder

addiu	$10,$10,2
lhu	$5,-2($10)
sll	$4,$fp,2
addu	$4,$23,$4
andi	$6,$5,0x3
.set	noreorder
.set	nomacro
beq	$6,$22,$L106
lw	$4,0($4)
.set	macro
.set	reorder

li	$7,3			# 0x3
.set	noreorder
.set	nomacro
beq	$6,$7,$L107
li	$7,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$7,$L108
move	$6,$9
.set	macro
.set	reorder

$L105:
sw	$4,0($9)
addiu	$9,$9,4
$L107:
sw	$4,0($9)
addiu	$9,$9,4
$L106:
addiu	$6,$9,4
sw	$4,0($9)
$L108:
addiu	$5,$5,-4
sw	$4,0($6)
.set	noreorder
.set	nomacro
bgtz	$5,$L105
addiu	$9,$6,4
.set	macro
.set	reorder

slt	$6,$18,$4
sltu	$5,$9,$2
.set	noreorder
.set	nomacro
bne	$5,$0,$L109
movn	$18,$4,$6
.set	macro
.set	reorder

$L94:
sw	$18,0($15)
lw	$20,1120($16)
lw	$18,48($16)
sw	$17,-16400($15)
$L90:
addiu	$13,$13,1
addiu	$15,$15,4
slt	$2,$13,$18
.set	noreorder
.set	nomacro
bne	$2,$0,$L120
addiu	$14,$14,8192
.set	macro
.set	reorder

$L121:
.set	noreorder
.set	nomacro
blez	$18,$L259
addiu	$22,$16,4
.set	macro
.set	reorder

addiu	$fp,$16,17544
move	$23,$0
li	$8,1			# 0x1
addiu	$21,$sp,56
move	$6,$20
move	$20,$24
addu	$2,$16,$23
$L274:
lbu	$2,1133($2)
beq	$2,$0,$L122
.set	noreorder
.set	nomacro
beq	$23,$8,$L260
move	$18,$0
.set	macro
.set	reorder

$L123:
lw	$25,%call16(memset)($28)
sll	$6,$6,1
move	$5,$0
sw	$8,392($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

sll	$3,$18,2
sll	$2,$23,2
lw	$28,48($sp)
addu	$3,$16,$3
lw	$10,1120($16)
addiu	$6,$18,64
lw	$9,1096($16)
addu	$2,$21,$2
lw	$4,0($16)
sll	$6,$6,4
lw	$11,1060($3)
lw	$7,1068($3)
move	$5,$22
lw	$2,0($2)
addu	$6,$16,$6
lw	$3,352($sp)
lw	$25,%call16(ff_wma_run_level_decode)($28)
addiu	$6,$6,4
sw	$fp,24($sp)
sw	$0,20($sp)
sw	$11,16($sp)
sw	$0,28($sp)
sw	$2,32($sp)
sw	$10,36($sp)
sw	$9,40($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_wma_run_level_decode
1:	jalr	$25
sw	$3,44($sp)
.set	macro
.set	reorder

lw	$28,48($sp)
lw	$18,48($16)
lw	$6,1120($16)
lw	$8,392($sp)
$L122:
lw	$3,56($16)
addiu	$23,$23,1
addiu	$fp,$fp,4096
.set	noreorder
.set	nomacro
beq	$3,$8,$L261
slt	$5,$23,$18
.set	macro
.set	reorder

$L124:
.set	noreorder
.set	nomacro
bne	$5,$0,$L274
addu	$2,$16,$23
.set	macro
.set	reorder

move	$24,$20
move	$20,$6
$L88:
srl	$22,$20,31
addu	$2,$22,$20
sra	$2,$2,1
mtc1	$2,$f2
lui	$2,%hi($LC3)
lwc1	$f0,%lo($LC3)($2)
li	$2,1			# 0x1
cvt.s.w	$f1,$f2
.set	noreorder
.set	nomacro
beq	$3,$2,$L262
div.s	$f0,$f0,$f1
.set	macro
.set	reorder

$L126:
lui	$2,%hi($LC17)
mtc1	$0,$f1
c.le.s	$fcc3,$f1,$f0
lwc1	$f1,%lo($LC17)($2)
lui	$2,%hi($LC18)
ldc1	$f2,%lo($LC18)($2)
mul.s	$f0,$f0,$f1
.set	noreorder
.set	nomacro
bc1f	$fcc3,$L239
cvt.d.s	$f0,$f0
.set	macro
.set	reorder

add.d	$f0,$f0,$f2
trunc.w.d $f2,$f0
swc1	$f2,356($sp)
$L129:
.set	noreorder
.set	nomacro
blez	$18,$L130
addiu	$2,$16,17544
.set	macro
.set	reorder

sw	$16,360($sp)
sll	$3,$19,2
sw	$24,364($sp)
addiu	$4,$sp,56
sw	$2,352($sp)
lui	$2,%hi(pow_table)
addiu	$21,$16,17536
addiu	$2,$2,%lo(pow_table)
sw	$4,368($sp)
move	$fp,$0
addu	$2,$3,$2
addiu	$19,$16,1152
move	$23,$21
sw	$2,372($sp)
move	$21,$16
move	$16,$fp
$L150:
addu	$2,$21,$16
lbu	$2,1133($2)
beq	$2,$0,$L131
lw	$6,0($23)
lw	$13,352($sp)
.set	noreorder
.set	nomacro
beq	$6,$0,$L180
lw	$22,-16400($23)
.set	macro
.set	reorder

lw	$7,372($sp)
lw	$25,%call16(__divdi3)($28)
sw	$13,388($sp)
lw	$4,0($7)
sra	$7,$6,31
sra	$2,$4,31
srl	$5,$4,24
sll	$2,$2,8
sll	$4,$4,8
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
or	$5,$5,$2
.set	macro
.set	reorder

lw	$3,356($sp)
lw	$28,48($sp)
lw	$13,388($sp)
mul	$2,$3,$2
sra	$fp,$2,16
$L132:
lw	$2,76($21)
.set	noreorder
.set	nomacro
bne	$2,$0,$L133
addiu	$8,$19,24592
.set	macro
.set	reorder

lw	$2,392($21)
.set	noreorder
.set	nomacro
blez	$2,$L135
move	$3,$0
.set	macro
.set	reorder

addiu	$8,$8,4
$L275:
sw	$0,-4($8)
lw	$2,392($21)
addiu	$3,$3,1
slt	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L275
addiu	$8,$8,4
.set	macro
.set	reorder

addiu	$8,$8,-4
$L135:
sll	$2,$16,2
addiu	$3,$sp,56
addu	$2,$3,$2
lw	$7,0($2)
.set	noreorder
.set	nomacro
blez	$7,$L147
lw	$6,352($sp)
.set	macro
.set	reorder

move	$4,$0
move	$5,$8
$L148:
sll	$2,$4,$17
lh	$3,0($6)
addiu	$5,$5,4
sra	$2,$2,$22
addiu	$4,$4,1
sll	$2,$2,2
addiu	$6,$6,2
addu	$2,$19,$2
lw	$2,0($2)
mul	$2,$fp,$2
sra	$2,$2,8
mul	$2,$3,$2
sra	$2,$2,9
.set	noreorder
.set	nomacro
bne	$4,$7,$L148
sw	$2,-4($5)
.set	macro
.set	reorder

sll	$4,$4,2
addu	$8,$8,$4
$L147:
lw	$3,364($sp)
lw	$2,1120($21)
lw	$6,396($3)
subu	$6,$2,$6
blez	$6,$L241
lw	$25,%call16(memset)($28)
move	$5,$0
sll	$6,$6,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$8
.set	macro
.set	reorder

lw	$28,48($sp)
$L241:
lw	$18,48($21)
$L131:
lw	$4,352($sp)
addiu	$16,$16,1
lw	$7,360($sp)
addiu	$19,$19,8192
slt	$2,$16,$18
addiu	$4,$4,4096
addiu	$7,$7,64
addiu	$23,$23,4
sw	$4,352($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L150
sw	$7,360($sp)
.set	macro
.set	reorder

lbu	$2,1132($21)
.set	noreorder
.set	nomacro
beq	$2,$0,$L242
move	$16,$21
.set	macro
.set	reorder

lbu	$2,1134($21)
.set	noreorder
.set	nomacro
beq	$2,$0,$L61
lw	$20,1120($21)
.set	macro
.set	reorder

srl	$22,$20,31
$L152:
lbu	$2,1133($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L263
move	$5,$0
.set	macro
.set	reorder

$L154:
addu	$5,$22,$20
li	$2,33932			# 0x848c
sra	$5,$5,1
addiu	$3,$16,25740
.set	noreorder
.set	nomacro
blez	$5,$L61
addu	$2,$16,$2
.set	macro
.set	reorder

move	$4,$0
$L155:
#APP
# 648 "wmadec.c" 1
.word	0b01110000011000000000010001010100	#S32LDI XR1,$3,4
# 0 "" 2
# 649 "wmadec.c" 1
.word	0b01110000010000000000010010010100	#S32LDI XR2,$2,4
# 0 "" 2
# 650 "wmadec.c" 1
.word	0b01110000011000000000010011010100	#S32LDI XR3,$3,4
# 0 "" 2
# 651 "wmadec.c" 1
.word	0b01110000010000000000010100010100	#S32LDI XR4,$2,4
# 0 "" 2
# 652 "wmadec.c" 1
.word	0b01110010000110001000010101011000	#D32ADD XR5,XR1,XR2,XR6,SA
# 0 "" 2
# 653 "wmadec.c" 1
.word	0b01110010001000010000110111011000	#D32ADD XR7,XR3,XR4,XR8,SA
# 0 "" 2
# 654 "wmadec.c" 1
.word	0b01110000011011111111110110010001	#S32STD XR6,$3,-4
# 0 "" 2
# 655 "wmadec.c" 1
.word	0b01110000010011111111110101010001	#S32STD XR5,$2,-4
# 0 "" 2
# 656 "wmadec.c" 1
.word	0b01110000011000000000001000010001	#S32STD XR8,$3,0
# 0 "" 2
# 657 "wmadec.c" 1
.word	0b01110000010000000000000111010001	#S32STD XR7,$2,0
# 0 "" 2
#NO_APP
addiu	$4,$4,1
bne	$4,$5,$L155
lw	$18,48($16)
$L242:
lw	$20,1120($16)
$L61:
.set	noreorder
.set	nomacro
blez	$18,$L59
sll	$17,$17,6
.set	macro
.set	reorder

li	$21,58512			# 0xe490
addiu	$18,$16,25744
addu	$21,$17,$21
li	$17,42128			# 0xa490
addu	$21,$16,$21
addu	$17,$16,$17
move	$23,$0
li	$19,1			# 0x1
sw	$21,352($sp)
$L173:
addu	$3,$16,$23
srl	$2,$20,31
lbu	$3,1133($3)
addu	$2,$2,$20
.set	noreorder
.set	nomacro
bne	$3,$0,$L264
sra	$22,$2,1
.set	macro
.set	reorder

lbu	$2,1132($16)
beq	$2,$0,$L158
beq	$23,$19,$L157
$L158:
lw	$25,%call16(memset)($28)
move	$5,$0
li	$6,16384			# 0x4000
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

$L157:
lw	$4,1092($16)
sll	$21,$23,12
lw	$2,1128($16)
lw	$6,1108($16)
srl	$3,$4,31
lw	$5,1116($16)
addu	$3,$3,$4
slt	$4,$5,$6
sra	$3,$3,1
addu	$2,$3,$2
subu	$2,$2,$22
addu	$21,$21,$2
lw	$2,1096($16)
addiu	$21,$21,29432
sll	$21,$21,1
.set	noreorder
.set	nomacro
bne	$4,$0,$L159
addu	$21,$16,$21
.set	macro
.set	reorder

subu	$2,$2,$6
sw	$20,16($sp)
move	$4,$21
addiu	$2,$2,14708
move	$5,$17
sll	$2,$2,2
move	$7,$21
addu	$2,$16,$2
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_vector_fmul_add_c
.option	pic2
lw	$6,0($2)
.set	macro
.set	reorder

move	$3,$17
lw	$7,1120($16)
lw	$6,1108($16)
lw	$4,1112($16)
sll	$2,$7,1
sll	$5,$7,2
slt	$8,$4,$6
addu	$22,$21,$2
.set	noreorder
.set	nomacro
beq	$8,$0,$L265
addu	$5,$3,$5
.set	macro
.set	reorder

$L164:
sll	$20,$19,$4
lw	$2,1096($16)
subu	$21,$7,$20
subu	$4,$2,$4
srl	$3,$21,31
addu	$21,$3,$21
sra	$21,$21,1
.set	noreorder
.set	nomacro
blez	$21,$L170
sll	$8,$21,2
.set	macro
.set	reorder

addu	$2,$5,$8
move	$3,$5
move	$6,$22
$L169:
lw	$7,0($3)
addiu	$6,$6,2
addiu	$3,$3,4
.set	noreorder
.set	nomacro
bne	$3,$2,$L169
sh	$7,-2($6)
.set	macro
.set	reorder

$L170:
addiu	$2,$4,14708
sll	$4,$21,1
sll	$2,$2,2
addu	$5,$5,$8
addu	$2,$16,$2
move	$7,$20
addu	$4,$22,$4
lw	$6,0($2)
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_vector_fmul_reverse_c
.option	pic2
addu	$20,$21,$20
.set	macro
.set	reorder

sll	$2,$20,1
sra	$5,$21,4
lw	$28,48($sp)
addu	$2,$22,$2
move	$3,$0
.set	noreorder
.set	nomacro
blez	$5,$L165
addiu	$2,$2,-4
.set	macro
.set	reorder

$L202:
#APP
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
# 628 "wmadec.c" 1
.word	0b01110000010000000000010000010101	#S32SDI XR0,$2,4
# 0 "" 2
#NO_APP
addiu	$3,$3,1
bne	$3,$5,$L202
$L165:
lw	$2,48($16)
addiu	$23,$23,1
addiu	$18,$18,8192
slt	$2,$23,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L173
lw	$20,1120($16)
.set	macro
.set	reorder

$L59:
lw	$4,1124($16)
$L271:
lw	$3,1128($16)
lw	$2,1092($16)
addiu	$4,$4,1
addu	$20,$20,$3
slt	$2,$20,$2
sw	$4,1124($16)
sw	$20,1128($16)
xori	$2,$2,0x1
$L234:
lw	$31,436($sp)
lw	$fp,432($sp)
lw	$23,428($sp)
lw	$22,424($sp)
lw	$21,420($sp)
lw	$20,416($sp)
lw	$19,412($sp)
lw	$18,408($sp)
lw	$17,404($sp)
lw	$16,400($sp)
ldc1	$f24,456($sp)
ldc1	$f22,448($sp)
ldc1	$f20,440($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,464
.set	macro
.set	reorder

$L251:
lw	$2,76($16)
beq	$2,$0,$L65
lw	$3,1108($16)
lw	$2,1096($16)
bne	$3,$2,$L69
$L176:
lw	$3,56($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L88
.option	pic2
lw	$20,1120($16)
.set	macro
.set	reorder

$L65:
lw	$3,1108($16)
lw	$2,1096($16)
beq	$3,$2,$L68
lw	$3,12($16)
lw	$4,4($16)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$4,$4,$2
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L68
sw	$3,12($16)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L121
.option	pic2
lw	$20,1120($16)
.set	macro
.set	reorder

$L159:
sll	$22,$19,$5
subu	$2,$2,$5
subu	$fp,$20,$22
addiu	$2,$2,14708
sw	$22,16($sp)
srl	$3,$fp,31
sll	$2,$2,2
addu	$fp,$3,$fp
addu	$2,$16,$2
sra	$fp,$fp,1
sll	$4,$fp,1
lw	$6,0($2)
sll	$20,$fp,2
addu	$4,$21,$4
addu	$5,$17,$20
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_vector_fmul_add_c
.option	pic2
move	$7,$4
.set	macro
.set	reorder

addu	$6,$22,$fp
sll	$8,$6,2
sll	$6,$6,1
addu	$7,$17,$8
.set	noreorder
.set	nomacro
blez	$fp,$L163
addu	$21,$21,$6
.set	macro
.set	reorder

addu	$5,$7,$20
move	$2,$7
move	$3,$21
$L162:
lw	$4,0($2)
addiu	$3,$3,2
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$5,$L162
sh	$4,-2($3)
.set	macro
.set	reorder

$L163:
subu	$3,$7,$8
lw	$7,1120($16)
subu	$21,$21,$6
lw	$4,1112($16)
lw	$6,1108($16)
sll	$2,$7,1
sll	$5,$7,2
slt	$8,$4,$6
addu	$22,$21,$2
.set	noreorder
.set	nomacro
bne	$8,$0,$L164
addu	$5,$3,$5
.set	macro
.set	reorder

$L265:
lw	$2,1096($16)
move	$4,$22
addiu	$23,$23,1
addiu	$18,$18,8192
subu	$2,$2,$6
addiu	$2,$2,14708
sll	$2,$2,2
addu	$2,$16,$2
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_vector_fmul_reverse_c
.option	pic2
lw	$6,0($2)
.set	macro
.set	reorder

lw	$2,48($16)
lw	$28,48($sp)
slt	$2,$23,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L173
lw	$20,1120($16)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L271
.option	pic2
lw	$4,1124($16)
.set	macro
.set	reorder

$L264:
lw	$25,%call16(wma_imdct_calc_c)($28)
move	$5,$17
lw	$4,352($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,wma_imdct_calc_c
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L157
.option	pic2
lw	$20,1120($16)
.set	macro
.set	reorder

$L250:
lw	$4,12($16)
lw	$8,4($16)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$4,$4,1
lbu	$2,0($3)
sw	$4,12($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.option	pic0
.set	noreorder
.set	nomacro
j	$L58
.option	pic2
sb	$2,1132($16)
.set	macro
.set	reorder

$L254:
lui	$6,%hi($LC12)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC12)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,436($sp)
lw	$fp,432($sp)
lw	$23,428($sp)
lw	$22,424($sp)
lw	$21,420($sp)
lw	$20,416($sp)
lw	$19,412($sp)
lw	$18,408($sp)
lw	$17,404($sp)
lw	$16,400($sp)
ldc1	$f24,456($sp)
ldc1	$f22,448($sp)
ldc1	$f20,440($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,464
.set	macro
.set	reorder

$L247:
lw	$5,12($4)
li	$10,-16777216			# 0xffffffffff000000
lw	$2,4($4)
sw	$0,1104($4)
li	$4,16711680			# 0xff0000
srl	$3,$5,3
addiu	$13,$4,255
addu	$3,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($3)  
lwr $7, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$7,8
sll	$7,$7,8
ori	$12,$10,0xff00
and	$3,$3,$13
and	$7,$7,$12
or	$7,$3,$7
sll	$3,$7,16
srl	$7,$7,16
andi	$10,$5,0x7
or	$3,$3,$7
lw	$7,1096($16)
sll	$10,$3,$10
addu	$5,$8,$5
srl	$10,$10,$6
slt	$3,$10,$9
.set	noreorder
.set	nomacro
beq	$3,$0,$L266
sw	$5,12($16)
.set	macro
.set	reorder

subu	$10,$7,$10
srl	$4,$5,3
andi	$14,$5,0x7
addu	$4,$2,$4
sw	$10,1116($16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($4)  
lwr $11, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$11,8
sll	$11,$11,8
and	$4,$4,$13
and	$10,$11,$12
or	$10,$4,$10
sll	$4,$10,16
srl	$10,$10,16
addu	$5,$5,$8
or	$3,$4,$10
sll	$3,$3,$14
srl	$3,$3,$6
slt	$4,$3,$9
.set	noreorder
.set	nomacro
beq	$4,$0,$L267
sw	$5,12($16)
.set	macro
.set	reorder

subu	$3,$7,$3
.option	pic0
.set	noreorder
.set	nomacro
j	$L53
.option	pic2
sw	$3,1108($16)
.set	macro
.set	reorder

$L255:
.option	pic0
.set	noreorder
.set	nomacro
j	$L121
.option	pic2
lw	$20,1120($16)
.set	macro
.set	reorder

$L261:
slt	$2,$18,2
bne	$2,$0,$L124
lw	$2,12($16)
subu	$4,$0,$2
andi	$4,$4,0x7
.set	noreorder
.set	nomacro
beq	$4,$0,$L124
addu	$2,$4,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L124
.option	pic2
sw	$2,12($16)
.set	macro
.set	reorder

$L253:
addiu	$8,$8,9
srl	$4,$8,3
andi	$23,$8,0x7
addu	$10,$10,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($10)  
lwr $4, 0($10)  

# 0 "" 2
#NO_APP
move	$10,$4
sll	$22,$10,8
srl	$4,$4,8
and	$22,$22,$11
and	$4,$4,$12
or	$22,$4,$22
sll	$10,$22,16
srl	$4,$22,16
or	$4,$10,$4
sll	$4,$4,$23
srl	$3,$4,$3
addu	$2,$3,$2
sll	$2,$2,2
addu	$9,$9,$2
lh	$2,0($9)
.option	pic0
.set	noreorder
.set	nomacro
j	$L84
.option	pic2
lh	$3,2($9)
.set	macro
.set	reorder

$L252:
lw	$4,12($16)
lw	$2,4($16)
srl	$3,$4,3
andi	$7,$4,0x7
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$12
and	$3,$3,$11
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$4,$4,7
or	$2,$3,$2
sll	$7,$2,$7
sw	$4,12($16)
srl	$7,$7,25
.option	pic0
.set	noreorder
.set	nomacro
j	$L82
.option	pic2
addiu	$7,$7,-19
.set	macro
.set	reorder

$L91:
lw	$8,4($16)
lw	$6,12($16)
move	$5,$0
lw	$10,%got(ff_wma_lsp_codebook)($28)
.option	pic0
.set	noreorder
.set	nomacro
j	$L114
.option	pic2
li	$9,10			# 0xa
.set	macro
.set	reorder

$L112:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 3($2)  
lwr $22, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$22,8
sll	$25,$22,8
and	$2,$2,$12
and	$25,$25,$11
or	$25,$2,$25
sll	$22,$25,16
srl	$2,$25,16
addiu	$6,$6,4
or	$2,$22,$2
sll	$2,$2,$4
addiu	$7,$7,4
sw	$6,12($16)
srl	$4,$2,28
sll	$2,$5,4
addiu	$5,$5,1
addu	$2,$2,$4
sll	$2,$2,2
addu	$2,$10,$2
lwc1	$f0,0($2)
.set	noreorder
.set	nomacro
beq	$5,$9,$L268
swc1	$f0,-4($7)
.set	macro
.set	reorder

$L114:
.set	noreorder
.set	nomacro
beq	$5,$0,$L111
srl	$2,$6,3
.set	macro
.set	reorder

slt	$22,$5,8
andi	$4,$6,0x7
.set	noreorder
.set	nomacro
bne	$22,$0,$L112
addu	$2,$8,$2
.set	macro
.set	reorder

$L111:
srl	$2,$6,3
andi	$4,$6,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 3($2)  
lwr $22, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$22,8
sll	$25,$22,8
and	$2,$2,$12
and	$25,$25,$11
or	$25,$2,$25
sll	$22,$25,16
srl	$2,$25,16
addiu	$6,$6,3
or	$2,$22,$2
sll	$2,$2,$4
addiu	$7,$7,4
sw	$6,12($16)
srl	$4,$2,29
sll	$2,$5,4
addiu	$5,$5,1
addu	$2,$2,$4
sll	$2,$2,2
addu	$2,$10,$2
lwc1	$f0,0($2)
.set	noreorder
.set	nomacro
bne	$5,$9,$L114
swc1	$f0,-4($7)
.set	macro
.set	reorder

$L268:
blez	$20,$L119
mtc1	$0,$f4
lwc1	$f20,316($sp)
addiu	$7,$20,31105
lw	$5,356($sp)
li	$10,1065353216			# 0x3f800000
lwc1	$f19,312($sp)
sll	$7,$7,2
li	$9,131072			# 0x20000
lwc1	$f18,324($sp)
addu	$7,$16,$7
li	$8,33152			# 0x8180
lwc1	$f17,320($sp)
move	$6,$14
lwc1	$f16,328($sp)
mov.s	$f15,$f4
lwc1	$f14,332($sp)
lwc1	$f13,336($sp)
lwc1	$f12,340($sp)
lwc1	$f11,344($sp)
lwc1	$f10,348($sp)
$L116:
lwc1	$f0,0($5)
addiu	$6,$6,4
addiu	$5,$5,4
sub.s	$f3,$f0,$f17
sub.s	$f2,$f0,$f20
sub.s	$f23,$f0,$f19
sub.s	$f1,$f0,$f18
sub.s	$f21,$f0,$f14
sub.s	$f25,$f0,$f16
mul.s	$f23,$f23,$f3
mul.s	$f1,$f2,$f1
sub.s	$f3,$f0,$f12
sub.s	$f24,$f0,$f13
mul.s	$f23,$f23,$f6
mul.s	$f1,$f1,$f6
sub.s	$f2,$f0,$f10
sub.s	$f22,$f0,$f11
mul.s	$f23,$f23,$f25
mul.s	$f1,$f1,$f21
add.s	$f21,$f0,$f5
sub.s	$f0,$f5,$f0
mul.s	$f23,$f23,$f24
mul.s	$f1,$f1,$f3
mul.s	$f3,$f23,$f22
mul.s	$f1,$f1,$f2
mul.s	$f3,$f3,$f3
mul.s	$f1,$f1,$f1
mul.s	$f3,$f3,$f21
mul.s	$f1,$f1,$f0
add.s	$f1,$f3,$f1
mfc1	$2,$f1
srl	$4,$2,14
sll	$18,$2,7
andi	$4,$4,0x1fc
addu	$4,$16,$4
and	$18,$18,$3
addu	$4,$4,$9
or	$18,$18,$10
lwc1	$f2,3076($4)
mtc1	$18,$f0
srl	$2,$2,23
lwc1	$f1,2564($4)
addu	$2,$2,$8
sll	$2,$2,2
addu	$2,$16,$2
mul.s	$f2,$f2,$f0
lwc1	$f0,4($2)
add.s	$f2,$f2,$f1
mul.s	$f2,$f2,$f0
mul.s	$f0,$f2,$f7
c.le.s	$fcc0,$f2,$f4
c.le.s	$fcc1,$f15,$f2
cvt.d.s	$f0,$f0
movf.s	$f4,$f2,$fcc0
add.d	$f2,$f0,$f8
sub.d	$f0,$f0,$f8
trunc.w.d $f21,$f2
trunc.w.d $f2,$f0
mfc1	$2,$f21
mfc1	$4,$f2
movf	$2,$4,$fcc1
.set	noreorder
.set	nomacro
bne	$5,$7,$L116
sw	$2,-4($6)
.set	macro
.set	reorder

mtc1	$0,$f0
lw	$20,1120($16)
lw	$18,48($16)
c.le.s	$fcc2,$f0,$f4
mul.s	$f0,$f4,$f7
.set	noreorder
.set	nomacro
bc1f	$fcc2,$L238
cvt.d.s	$f0,$f0
.set	macro
.set	reorder

add.d	$f0,$f0,$f8
trunc.w.d $f21,$f0
mfc1	$21,$f21
$L119:
sw	$21,0($15)
$L270:
.option	pic0
.set	noreorder
.set	nomacro
j	$L90
.option	pic2
sw	$17,-16400($15)
.set	macro
.set	reorder

$L256:
addiu	$5,$5,8
srl	$7,$5,3
andi	$25,$5,0x7
addu	$7,$20,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $8, 3($7)  
lwr $8, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$8,8
sll	$8,$8,8
and	$7,$7,$12
and	$8,$8,$11
or	$7,$7,$8
sll	$8,$7,16
srl	$7,$7,16
or	$7,$8,$7
sll	$7,$7,$25
srl	$7,$7,$6
addu	$4,$7,$4
sll	$4,$4,2
addu	$4,$21,$4
lh	$8,2($4)
.set	noreorder
.set	nomacro
bltz	$8,$L101
lh	$4,0($4)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L100
.option	pic2
move	$6,$8
.set	macro
.set	reorder

$L133:
lw	$20,392($21)
lw	$4,364($sp)
sll	$20,$20,$17
sra	$20,$20,$22
lw	$24,416($4)
sll	$20,$20,2
.set	noreorder
.set	nomacro
bltz	$24,$L131
addu	$20,$19,$20
.set	macro
.set	reorder

lw	$11,360($sp)
li	$10,-1			# 0xffffffffffffffff
move	$7,$fp
$L145:
.set	noreorder
.set	nomacro
bltz	$10,$L269
lw	$2,364($sp)
.set	macro
.set	reorder

lw	$2,1096($21)
lw	$4,1108($21)
lw	$3,768($11)
subu	$2,$2,$4
sll	$2,$2,4
addu	$2,$2,$10
addiu	$2,$2,108
sll	$2,$2,2
addu	$2,$21,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L137
lw	$18,4($2)
.set	macro
.set	reorder

lw	$6,0($23)
sll	$4,$7,8
lw	$25,%call16(__divdi3)($28)
sra	$5,$4,31
sw	$8,392($sp)
sra	$7,$6,31
sw	$10,380($sp)
sw	$11,384($sp)
sw	$13,388($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
sw	$24,376($sp)
.set	macro
.set	reorder

lw	$3,356($sp)
lw	$28,48($sp)
lw	$8,392($sp)
mul	$2,$3,$2
lw	$10,380($sp)
lw	$11,384($sp)
lw	$13,388($sp)
lw	$24,376($sp)
.set	noreorder
.set	nomacro
blez	$18,$L140
sra	$7,$2,16
.set	macro
.set	reorder

move	$3,$0
move	$4,$8
$L142:
sll	$2,$3,$17
addiu	$4,$4,4
sra	$2,$2,$22
addiu	$3,$3,1
sll	$2,$2,2
addu	$2,$20,$2
lw	$2,0($2)
mul	$2,$7,$2
sra	$2,$2,17
.set	noreorder
.set	nomacro
bne	$3,$18,$L142
sw	$2,-4($4)
.set	macro
.set	reorder

sll	$2,$18,2
.option	pic0
.set	noreorder
.set	nomacro
j	$L140
.option	pic2
addu	$8,$8,$2
.set	macro
.set	reorder

$L269:
lw	$18,372($2)
lw	$2,392($21)
subu	$18,$18,$2
$L137:
.set	noreorder
.set	nomacro
blez	$18,$L276
sll	$2,$18,$17
.set	macro
.set	reorder

move	$4,$0
move	$6,$8
move	$5,$13
$L144:
sll	$2,$4,$17
lh	$3,0($5)
addiu	$6,$6,4
sra	$2,$2,$22
addiu	$4,$4,1
sll	$2,$2,2
addiu	$5,$5,2
addu	$2,$20,$2
lw	$2,0($2)
mul	$2,$fp,$2
sra	$2,$2,8
mul	$3,$3,$2
sra	$3,$3,9
.set	noreorder
.set	nomacro
bne	$4,$18,$L144
sw	$3,-4($6)
.set	macro
.set	reorder

sll	$3,$18,2
sll	$2,$18,1
addu	$8,$8,$3
addu	$13,$13,$2
$L140:
sll	$2,$18,$17
$L276:
addiu	$10,$10,1
sra	$2,$2,$22
addiu	$11,$11,4
sll	$2,$2,2
.set	noreorder
.set	nomacro
bne	$10,$24,$L145
addu	$20,$20,$2
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L131
.option	pic2
lw	$18,48($21)
.set	macro
.set	reorder

$L180:
.option	pic0
.set	noreorder
.set	nomacro
j	$L132
.option	pic2
move	$fp,$0
.set	macro
.set	reorder

$L238:
sub.d	$f0,$f0,$f8
trunc.w.d $f2,$f0
mfc1	$21,$f2
.option	pic0
.set	noreorder
.set	nomacro
j	$L270
.option	pic2
sw	$21,0($15)
.set	macro
.set	reorder

$L130:
lbu	$2,1132($16)
beq	$2,$0,$L59
lbu	$2,1134($16)
bne	$2,$0,$L152
.option	pic0
.set	noreorder
.set	nomacro
j	$L271
.option	pic2
lw	$4,1124($16)
.set	macro
.set	reorder

$L262:
cvt.d.w	$f2,$f2
cvt.d.s	$f0,$f0
sqrt.d	$f2,$f2
mul.d	$f0,$f2,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L126
.option	pic2
cvt.s.d	$f0,$f0
.set	macro
.set	reorder

$L239:
sub.d	$f0,$f0,$f2
trunc.w.d $f21,$f0
.option	pic0
.set	noreorder
.set	nomacro
j	$L129
.option	pic2
swc1	$f21,356($sp)
.set	macro
.set	reorder

$L260:
lbu	$18,1132($16)
.option	pic0
.set	noreorder
.set	nomacro
j	$L123
.option	pic2
sltu	$18,$0,$18
.set	macro
.set	reorder

$L263:
lw	$25,%call16(memset)($28)
addiu	$4,$16,25744
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memset
1:	jalr	$25
sll	$6,$20,2
.set	macro
.set	reorder

li	$2,1			# 0x1
lw	$28,48($sp)
.option	pic0
.set	noreorder
.set	nomacro
j	$L154
.option	pic2
sb	$2,1133($16)
.set	macro
.set	reorder

$L259:
.option	pic0
.set	noreorder
.set	nomacro
j	$L88
.option	pic2
lw	$3,56($16)
.set	macro
.set	reorder

$L101:
subu	$5,$5,$6
srl	$7,$5,3
andi	$6,$5,0x7
addu	$7,$20,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($7)  
lwr $25, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$25,8
sll	$25,$25,8
and	$7,$7,$12
and	$25,$25,$11
or	$25,$7,$25
sll	$7,$25,16
srl	$25,$25,16
or	$7,$7,$25
sll	$6,$7,$6
srl	$6,$6,$8
addu	$6,$6,$4
sll	$6,$6,2
addu	$6,$21,$6
lh	$4,0($6)
.option	pic0
.set	noreorder
.set	nomacro
j	$L100
.option	pic2
lh	$6,2($6)
.set	macro
.set	reorder

$L92:
lw	$8,12($16)
lw	$4,4($16)
lhu	$6,0($10)
srl	$5,$8,3
andi	$fp,$8,0x7
addu	$4,$4,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$12
and	$5,$5,$11
or	$5,$4,$5
sll	$4,$5,16
srl	$5,$5,16
addiu	$8,$8,5
or	$4,$4,$5
sll	$fp,$4,$fp
sw	$8,12($16)
andi	$4,$6,0x3
srl	$fp,$fp,27
addiu	$fp,$fp,10
sll	$5,$fp,2
addu	$5,$23,$5
lw	$18,0($5)
li	$5,2			# 0x2
.set	noreorder
.set	nomacro
beq	$4,$5,$L96
li	$5,3			# 0x3
.set	macro
.set	reorder

beq	$4,$5,$L97
beq	$4,$7,$L98
$L95:
sw	$18,0($9)
addiu	$9,$9,4
$L97:
sw	$18,0($9)
addiu	$9,$9,4
$L96:
sw	$18,0($9)
addiu	$9,$9,4
$L98:
addiu	$6,$6,-4
sw	$18,0($9)
.set	noreorder
.set	nomacro
bgtz	$6,$L95
addiu	$9,$9,4
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L99
.option	pic2
addiu	$10,$10,2
.set	macro
.set	reorder

$L249:
lui	$6,%hi($LC11)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC11)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L234
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L248:
lui	$6,%hi($LC10)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
subu	$7,$7,$2
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L234
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L266:
lui	$6,%hi($LC8)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC8)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
subu	$7,$7,$10
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L234
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L258:
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$7,$fp
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L234
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L257:
lui	$6,%hi($LC13)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC13)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L234
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L267:
lui	$6,%hi($LC9)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
subu	$7,$7,$3
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L234
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	wma_decode_block
.size	wma_decode_block, .-wma_decode_block
.align	2
.set	nomips16
.set	nomicromips
.ent	wma_decode_frame
.type	wma_decode_frame, @function
wma_decode_frame:
.frame	$sp,48,$31		# vars= 0, regs= 5/0, args= 16, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
addiu	$sp,$sp,-48
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$17,32($sp)
move	$17,$5
sw	$16,28($sp)
move	$16,$4
sw	$31,44($sp)
sw	$19,40($sp)
sw	$18,36($sp)
.cprestore	16
sw	$0,1124($4)
.option	pic0
.set	noreorder
.set	nomacro
j	$L279
.option	pic2
sw	$0,1128($4)
.set	macro
.set	reorder

$L306:
.set	noreorder
.set	nomacro
bne	$2,$0,$L305
li	$2,2			# 0x2
.set	macro
.set	reorder

$L279:
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_decode_block
.option	pic2
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L306
lw	$28,16($sp)
.set	macro
.set	reorder

lw	$31,44($sp)
li	$2,-1			# 0xffffffffffffffff
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L305:
lw	$18,48($16)
.set	noreorder
.set	nomacro
bne	$18,$2,$L280
lw	$19,1092($16)
.set	macro
.set	reorder

lbu	$5,1134($16)
li	$4,58860			# 0xe5ec
sra	$6,$19,1
addu	$3,$16,$4
.set	noreorder
.set	nomacro
bne	$5,$0,$L307
addiu	$2,$17,-4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$6,$L295
move	$4,$0
.set	macro
.set	reorder

$L284:
#APP
# 1067 "wmadec.c" 1
.word	0b01110000011000000000010001010100	#S32LDI XR1,$3,4
# 0 "" 2
# 1068 "wmadec.c" 1
.word	0b01110000000100000100000010111001	#S32MOVZ XR2,XR0,XR1
# 0 "" 2
# 1069 "wmadec.c" 1
.word	0b01110011000001000100100010111101	#S32SFL XR2,XR2,XR1,XR1,PTN3
# 0 "" 2
# 1070 "wmadec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 1071 "wmadec.c" 1
.word	0b01110000010000000000010010010101	#S32SDI XR2,$2,4
# 0 "" 2
#NO_APP
addiu	$4,$4,1
bne	$4,$6,$L284
.option	pic0
.set	noreorder
.set	nomacro
j	$L293
.option	pic2
lw	$3,1092($16)
.set	macro
.set	reorder

$L280:
li	$5,58864			# 0xe5f0
lw	$25,%call16(memcpy)($28)
sll	$6,$19,1
addu	$5,$16,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$18,$L289
move	$6,$19
.set	macro
.set	reorder

lw	$3,1092($16)
$L293:
li	$7,58860			# 0xe5ec
move	$4,$0
addu	$7,$16,$7
$L292:
sll	$2,$4,12
move	$5,$7
addu	$2,$2,$3
move	$3,$0
addiu	$2,$2,29432
sll	$2,$2,1
addu	$2,$16,$2
.set	noreorder
.set	nomacro
blez	$6,$L291
addiu	$2,$2,-4
.set	macro
.set	reorder

$L297:
#APP
# 1090 "wmadec.c" 1
.word	0b01110000010000000000010101010100	#S32LDI XR5,$2,4
# 0 "" 2
# 1091 "wmadec.c" 1
.word	0b01110000101000000000010101010101	#S32SDI XR5,$5,4
# 0 "" 2
#NO_APP
addiu	$3,$3,1
bne	$3,$6,$L297
$L291:
addiu	$4,$4,1
slt	$2,$4,$18
.set	noreorder
.set	nomacro
beq	$2,$0,$L289
addiu	$7,$7,8192
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L292
.option	pic2
lw	$3,1092($16)
.set	macro
.set	reorder

$L289:
lw	$31,44($sp)
move	$2,$0
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,48
.set	macro
.set	reorder

$L307:
.set	noreorder
.set	nomacro
blez	$6,$L295
addiu	$5,$4,8192
.set	macro
.set	reorder

move	$4,$0
addu	$5,$16,$5
$L285:
#APP
# 1076 "wmadec.c" 1
.word	0b01110000011000000000010001010100	#S32LDI XR1,$3,4
# 0 "" 2
# 1077 "wmadec.c" 1
.word	0b01110000101000000000010010010100	#S32LDI XR2,$5,4
# 0 "" 2
# 1078 "wmadec.c" 1
.word	0b01110011000001000100100010111101	#S32SFL XR2,XR2,XR1,XR1,PTN3
# 0 "" 2
# 1079 "wmadec.c" 1
.word	0b01110000010000000000010001010101	#S32SDI XR1,$2,4
# 0 "" 2
# 1080 "wmadec.c" 1
.word	0b01110000010000000000010010010101	#S32SDI XR2,$2,4
# 0 "" 2
#NO_APP
addiu	$4,$4,1
bne	$4,$6,$L285
.option	pic0
.set	noreorder
.set	nomacro
j	$L293
.option	pic2
lw	$3,1092($16)
.set	macro
.set	reorder

$L295:
.option	pic0
.set	noreorder
.set	nomacro
j	$L293
.option	pic2
move	$3,$19
.set	macro
.set	reorder

.end	wma_decode_frame
.size	wma_decode_frame, .-wma_decode_frame
.section	.rodata.str1.4
.align	2
$LC19:
.ascii	"Insufficient output space\012\000"
.align	2
$LC20:
.ascii	"len %d invalid\012\000"
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	wma_decode_superframe
.type	wma_decode_superframe, @function
wma_decode_superframe:
.frame	$sp,64,$31		# vars= 0, regs= 10/0, args= 16, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
lui	$28,%hi(__gnu_local_gp)
lw	$2,20($7)
addiu	$sp,$sp,-64
addiu	$28,$28,%lo(__gnu_local_gp)
sw	$21,44($sp)
sw	$16,24($sp)
sw	$31,60($sp)
sw	$fp,56($sp)
sw	$23,52($sp)
sw	$22,48($sp)
sw	$20,40($sp)
sw	$19,36($sp)
sw	$18,32($sp)
sw	$17,28($sp)
.cprestore	16
lw	$16,136($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L343
lw	$21,16($7)
.set	macro
.set	reorder

lw	$20,60($16)
slt	$2,$2,$20
.set	noreorder
.set	nomacro
bne	$2,$0,$L329
sll	$4,$20,3
.set	macro
.set	reorder

sra	$2,$4,3
bltz	$2,$L331
.set	noreorder
.set	nomacro
bltz	$4,$L331
addu	$2,$21,$2
.set	macro
.set	reorder

sw	$4,16($16)
move	$17,$5
sw	$0,12($16)
lw	$5,64($16)
move	$3,$21
move	$19,$6
sw	$2,8($16)
.set	noreorder
.set	nomacro
bne	$5,$0,$L345
sw	$3,4($16)
.set	macro
.set	reorder

$L312:
lw	$3,48($16)
lw	$2,1092($16)
lw	$4,0($6)
mul	$2,$3,$2
sll	$2,$2,1
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L342
move	$4,$16
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
jal	wma_decode_frame
.option	pic2
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L343
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,48($16)
lw	$22,1092($16)
mul	$22,$2,$22
sll	$22,$22,1
addu	$22,$17,$22
subu	$17,$22,$17
$L351:
lw	$31,60($sp)
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$18,32($sp)
sw	$17,0($19)
lw	$2,60($16)
lw	$19,36($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L329:
lw	$31,60($sp)
move	$2,$0
lw	$fp,56($sp)
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L349:
lui	$6,%hi($LC20)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC20)
.set	macro
.set	reorder

$L314:
li	$2,-1			# 0xffffffffffffffff
$L343:
li	$3,65536			# 0x10000
lw	$31,60($sp)
lw	$fp,56($sp)
addu	$16,$16,$3
lw	$23,52($sp)
lw	$22,48($sp)
lw	$21,44($sp)
lw	$20,40($sp)
lw	$19,36($sp)
lw	$18,32($sp)
lw	$17,28($sp)
sw	$0,26104($16)
lw	$16,24($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,64
.set	macro
.set	reorder

$L331:
move	$2,$0
sw	$0,12($16)
move	$4,$0
move	$3,$0
move	$17,$5
sw	$2,8($16)
sw	$4,16($16)
sw	$3,4($16)
lw	$5,64($16)
.set	noreorder
.set	nomacro
beq	$5,$0,$L312
move	$19,$6
.set	macro
.set	reorder

$L345:
li	$2,4			# 0x4
lw	$4,48($16)
li	$7,16711680			# 0xff0000
lw	$8,1092($16)
li	$6,-16777216			# 0xffffffffff000000
sw	$2,12($16)
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$18,$5,8
sll	$5,$5,8
ori	$6,$6,0xff00
and	$2,$18,$7
and	$18,$5,$6
or	$18,$2,$18
srl	$18,$18,8
li	$2,8			# 0x8
andi	$18,$18,0xf
mul	$4,$18,$4
sw	$2,12($16)
lw	$5,0($19)
mul	$2,$4,$8
sll	$2,$2,1
sltu	$2,$5,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L342
addiu	$18,$18,-1
.set	macro
.set	reorder

lw	$2,80($16)
li	$9,65536			# 0x10000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 4($3)  
lwr $4, 1($3)  

# 0 "" 2
#NO_APP
srl	$8,$4,8
addiu	$11,$2,11
sll	$4,$4,8
addu	$5,$16,$9
and	$8,$8,$7
sw	$11,12($16)
and	$4,$4,$6
or	$4,$8,$4
lw	$5,26104($5)
li	$23,29			# 0x1d
sll	$10,$4,16
srl	$4,$4,16
subu	$8,$23,$2
or	$23,$10,$4
.set	noreorder
.set	nomacro
blez	$5,$L332
srl	$23,$23,$8
.set	macro
.set	reorder

addiu	$2,$23,7
sra	$2,$2,3
addu	$2,$5,$2
slt	$2,$2,16385
.set	noreorder
.set	nomacro
beq	$2,$0,$L314
addiu	$9,$9,9712
.set	macro
.set	reorder

slt	$2,$23,8
addu	$5,$5,$9
.set	noreorder
.set	nomacro
bne	$2,$0,$L333
addu	$5,$16,$5
.set	macro
.set	reorder

addiu	$9,$23,-8
srl	$10,$9,3
addiu	$8,$10,1
.option	pic0
.set	noreorder
.set	nomacro
j	$L319
.option	pic2
addu	$8,$5,$8
.set	macro
.set	reorder

$L346:
lw	$11,12($16)
lw	$3,4($16)
$L319:
srl	$2,$11,3
andi	$12,$11,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$7
and	$4,$4,$6
or	$2,$3,$4
sll	$3,$2,16
srl	$2,$2,16
addiu	$11,$11,8
or	$2,$3,$2
sll	$2,$2,$12
addiu	$5,$5,1
sw	$11,12($16)
srl	$2,$2,24
.set	noreorder
.set	nomacro
bne	$5,$8,$L346
sb	$2,-1($5)
.set	macro
.set	reorder

sll	$10,$10,3
subu	$9,$9,$10
.set	noreorder
.set	nomacro
blez	$9,$L352
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L350:
lw	$6,12($16)
lw	$2,4($16)
srl	$3,$6,3
addu	$2,$2,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$4,$3,8
addiu	$2,$2,255
sll	$5,$3,8
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$4,$2,16
srl	$2,$2,16
andi	$3,$6,0x7
or	$2,$4,$2
sll	$2,$2,$3
subu	$3,$0,$9
srl	$3,$2,$3
li	$2,8			# 0x8
subu	$2,$2,$9
addu	$9,$9,$6
sll	$2,$3,$2
sw	$9,12($16)
sb	$2,0($8)
li	$2,65536			# 0x10000
$L352:
sw	$0,12($16)
li	$5,131072			# 0x20000
addu	$4,$16,$2
addiu	$3,$2,9712
addiu	$2,$2,26096
sw	$5,16($16)
lw	$4,26100($4)
addu	$3,$16,$3
addu	$2,$16,$2
sw	$3,4($16)
.set	noreorder
.set	nomacro
blez	$4,$L321
sw	$2,8($16)
.set	macro
.set	reorder

sw	$4,12($16)
$L321:
move	$4,$16
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_decode_frame
.option	pic2
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L314
lw	$28,16($sp)
.set	macro
.set	reorder

lw	$3,48($16)
lw	$22,1092($16)
lw	$2,80($16)
mul	$22,$3,$22
sll	$22,$22,1
addu	$22,$17,$22
$L315:
addiu	$23,$23,8
li	$3,16384			# 0x4000
addu	$2,$23,$2
addiu	$2,$2,3
sra	$4,$2,3
subu	$3,$3,$4
sll	$3,$3,3
sra	$5,$3,3
bltz	$5,$L335
bltz	$3,$L335
addu	$4,$21,$4
addu	$5,$4,$5
$L322:
andi	$6,$2,0x7
sw	$4,4($16)
sw	$3,16($16)
sw	$5,8($16)
.set	noreorder
.set	nomacro
beq	$6,$0,$L323
sw	$0,12($16)
.set	macro
.set	reorder

sw	$6,12($16)
$L323:
li	$3,1			# 0x1
.set	noreorder
.set	nomacro
blez	$18,$L336
sw	$3,1104($16)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L325
.option	pic2
move	$fp,$0
.set	macro
.set	reorder

$L348:
lw	$2,48($16)
lw	$3,1092($16)
mul	$2,$2,$3
sll	$2,$2,1
.set	noreorder
.set	nomacro
beq	$18,$fp,$L347
addu	$22,$22,$2
.set	macro
.set	reorder

$L325:
move	$5,$22
.option	pic0
.set	noreorder
.set	nomacro
jal	wma_decode_frame
.option	pic2
move	$4,$16
.set	macro
.set	reorder

addiu	$fp,$fp,1
.set	noreorder
.set	nomacro
bgez	$2,$L348
lw	$28,16($sp)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L343
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L335:
move	$5,$0
move	$3,$0
.option	pic0
.set	noreorder
.set	nomacro
j	$L322
.option	pic2
move	$4,$0
.set	macro
.set	reorder

$L347:
lw	$2,80($16)
lw	$6,12($16)
addu	$23,$23,$2
addiu	$23,$23,3
$L324:
li	$2,-8			# 0xfffffffffffffff8
li	$4,65536			# 0x10000
and	$2,$23,$2
addu	$2,$2,$6
addu	$3,$16,$4
sra	$5,$2,3
andi	$2,$2,0x7
subu	$7,$20,$5
sw	$2,26100($3)
sltu	$2,$7,16385
.set	noreorder
.set	nomacro
beq	$2,$0,$L349
addiu	$4,$4,9712
.set	macro
.set	reorder

lw	$25,%call16(memcpy)($28)
addu	$5,$21,$5
sw	$7,26104($3)
addu	$4,$16,$4
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$6,$7
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L351
.option	pic2
subu	$17,$22,$17
.set	macro
.set	reorder

$L332:
.option	pic0
.set	noreorder
.set	nomacro
j	$L315
.option	pic2
move	$22,$17
.set	macro
.set	reorder

$L342:
lui	$6,%hi($LC19)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
li	$5,16			# 0x10
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC19)
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L343
.option	pic2
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L333:
move	$9,$23
.set	noreorder
.set	nomacro
bgtz	$9,$L350
move	$8,$5
.set	macro
.set	reorder

.option	pic0
.set	noreorder
.set	nomacro
j	$L352
.option	pic2
li	$2,65536			# 0x10000
.set	macro
.set	reorder

$L336:
.option	pic0
.set	noreorder
.set	nomacro
j	$L324
.option	pic2
move	$23,$2
.set	macro
.set	reorder

.end	wma_decode_superframe
.size	wma_decode_superframe, .-wma_decode_superframe
.globl	wmav2_decoder
.section	.rodata.str1.4
.align	2
$LC21:
.ascii	"wmav2\000"
.align	2
$LC22:
.ascii	"Windows Media Audio 2\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	wmav2_decoder, @object
.size	wmav2_decoder, 72
wmav2_decoder:
.word	$LC21
.word	1
.word	86024
.word	138784
.word	wma_decode_init
.word	0
.word	ff_wma_end
.word	wma_decode_superframe
.space	8
.word	flush
.space	8
.word	$LC22
.space	16
.globl	wmav1_decoder
.section	.rodata.str1.4
.align	2
$LC23:
.ascii	"wmav1\000"
.align	2
$LC24:
.ascii	"Windows Media Audio 1\000"
.section	.data.rel
.align	2
.type	wmav1_decoder, @object
.size	wmav1_decoder, 72
wmav1_decoder:
.word	$LC23
.word	1
.word	86023
.word	138784
.word	wma_decode_init
.word	0
.word	ff_wma_end
.word	wma_decode_superframe
.space	8
.word	flush
.space	8
.word	$LC24
.space	16
.rdata
.align	2
.type	pow_tab, @object
.size	pow_tab, 624
pow_tab:
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	2
.word	2
.word	2
.word	3
.word	3
.word	3
.word	4
.word	5
.word	5
.word	6
.word	7
.word	8
.word	9
.word	11
.word	12
.word	14
.word	17
.word	19
.word	22
.word	26
.word	30
.word	34
.word	39
.word	46
.word	53
.word	61
.word	70
.word	81
.word	93
.word	108
.word	125
.word	144
.word	166
.word	192
.word	222
.word	256
.word	296
.word	341
.word	394
.word	455
.word	526
.word	607
.word	701
.word	810
.word	935
.word	1080
.word	1247
.word	1440
.word	1662
.word	1920
.word	2217
.word	2560
.word	2956
.word	3414
.word	3942
.word	4552
.word	5257
.word	6071
.word	7010
.word	8095
.word	9348
.word	10795
.word	12466
.word	14396
.word	16624
.word	19197
.word	22169
.word	25600
.word	29562
.word	34138
.word	39422
.word	45524
.word	52570
.word	60707
.word	70104
.word	80954
.word	93485
.word	107954
.word	124664
.word	143959
.word	166242
.word	191973
.word	221687
.word	256000
.word	295624
.word	341382
.word	394221
.word	455240
.word	525702
.word	607072
.word	701035
.word	809543
.word	934846
.word	1079543
.word	1246637
.word	1439594
.word	1662417
.word	1919729
.word	2216869
.word	2560000
.word	2956242
.word	3413815
.word	3942212
.word	4552396
.word	5257024
.word	6070717
.word	7010355
.word	8095431
.word	9348458
.word	10795430
.word	12466369
.word	14395938
.word	16624170
.word	19197292
.word	22168686
.word	25600000
.word	29562418
.word	34138148
.word	39422120
.word	45523952
.word	52570240
.word	60707168
.word	70103544
.word	80954312
.word	93484576
.word	107954304
.word	124663688
.word	143959376
.word	166241696
.word	191972912
.word	221686864
.align	2
.type	pow_table, @object
.size	pow_table, 556
pow_table:
.word	256
.word	287
.word	322
.word	361
.word	405
.word	455
.word	510
.word	573
.word	643
.word	721
.word	809
.word	908
.word	1019
.word	1143
.word	1283
.word	1439
.word	1615
.word	1812
.word	2033
.word	2281
.word	2560
.word	2872
.word	3222
.word	3616
.word	4057
.word	4552
.word	5107
.word	5731
.word	6430
.word	7215
.word	8095
.word	9083
.word	10191
.word	11435
.word	12830
.word	14395
.word	16152
.word	18123
.word	20334
.word	22816
.word	25600
.word	28723
.word	32228
.word	36160
.word	40573
.word	45523
.word	51078
.word	57311
.word	64304
.word	72150
.word	80954
.word	90832
.word	101915
.word	114350
.word	128303
.word	143959
.word	161525
.word	181234
.word	203348
.word	228160
.word	256000
.word	287236
.word	322284
.word	361609
.word	405732
.word	455239
.word	510787
.word	573112
.word	643042
.word	721506
.word	809543
.word	908322
.word	1019154
.word	1143509
.word	1283039
.word	1439593
.word	1615250
.word	1812341
.word	2033480
.word	2281602
.word	2560000
.word	2872367
.word	3222849
.word	3616096
.word	4057326
.word	4552395
.word	5107871
.word	5731126
.word	6430429
.word	7215060
.word	8095430
.word	9083222
.word	10191543
.word	11435099
.word	12830393
.word	14395937
.word	16152508
.word	18123412
.word	20334802
.word	22816024
.word	25600000
.word	28723672
.word	32228490
.word	36160961
.word	40573265
.word	45523952
.word	51078715
.word	57311261
.word	64304292
.word	72150603
.word	80954308
.word	90832227
.word	101915435
.word	114350999
.word	128303931
.word	143959379
.word	161525080
.word	181234120
.word	203348028
.word	228160240
.word	256000000
.word	287236724
.word	322284905
.word	361609611
.word	405732657
.word	455239528
.word	510787152
.word	573112611
.word	643042926
.word	721506030
.word	809543081
.word	908322276
.word	1019154356
.word	1143509995
.word	1283039318
.word	1439593792
.word	1615250801
.word	1812341208
.word	2033480280
.section	.rodata.cst8,"aM",@progbits,8
.align	3
$LC0:
.word	0
.word	1072693248
.align	3
$LC1:
.word	1719614413
.word	1105633438
.align	3
$LC2:
.word	-2115926195
.word	1072694274
.section	.rodata.cst4,"aM",@progbits,4
.align	2
$LC3:
.word	1065353216
.section	.rodata.cst8
.align	3
$LC4:
.word	1413754136
.word	1074340347
.align	3
$LC5:
.word	0
.word	1073741824
.align	3
$LC6:
.word	0
.word	-1076887552
.section	.rodata.cst4
.align	2
$LC7:
.word	998244352
.align	2
$LC15:
.word	1056964608
.align	2
$LC16:
.word	1073741824
.align	2
$LC17:
.word	1199570944
.section	.rodata.cst8
.align	3
$LC18:
.word	0
.word	1071644672
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
