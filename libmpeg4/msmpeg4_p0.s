.file	1 "msmpeg4_p0.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.unlikely.JZC_msmpeg4_decode_init,"ax",@progbits
.align	2
.globl	JZC_msmpeg4_decode_init
.set	nomips16
.set	nomicromips
.ent	JZC_msmpeg4_decode_init
.type	JZC_msmpeg4_decode_init, @function
JZC_msmpeg4_decode_init:
.frame	$sp,104,$31		# vars= 0, regs= 9/0, args= 56, gp= 8
.mask	0x80ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-104
lw	$25,%call16(mpeg4_decode_init)($28)
sw	$18,76($sp)
.cprestore	56
sw	$31,100($sp)
sw	$23,96($sp)
sw	$22,92($sp)
sw	$21,88($sp)
sw	$20,84($sp)
sw	$19,80($sp)
sw	$17,72($sp)
sw	$16,68($sp)
.reloc	1f,R_MIPS_JALR,mpeg4_decode_init
1:	jalr	$25
lw	$18,136($4)

lw	$4,10304($18)
addiu	$2,$4,-1
sltu	$3,$2,6
beq	$3,$0,$L2
lw	$28,56($sp)

sll	$3,$2,2
lw	$2,%got($L4)($28)
addiu	$2,$2,%lo($L4)
addu	$2,$2,$3
lw	$2,0($2)
addu	$2,$2,$28
j	$2
nop

.rdata
.align	2
.align	2
$L4:
.gpword	$L3
.gpword	$L3
.gpword	$L5
.gpword	$L6
.gpword	$L6
.gpword	$L7
.section	.text.unlikely.JZC_msmpeg4_decode_init
$L3:
lw	$2,%got(ff_mpeg1_dc_scale_table)($28)
sw	$2,2792($18)
b	$L2
sw	$2,2788($18)

$L5:
lw	$2,88($18)
beq	$2,$0,$L8
lw	$2,%got(ff_mpeg4_y_dc_scale_table)($28)

lw	$2,%got(old_ff_y_dc_scale_table)($28)
sw	$2,2788($18)
lw	$2,%got(wmv1_c_dc_scale_table)($28)
b	$L11
sw	$2,2792($18)

$L8:
sw	$2,2788($18)
lw	$2,%got(ff_mpeg4_c_dc_scale_table)($28)
b	$L11
sw	$2,2792($18)

$L6:
lw	$2,%got(wmv1_y_dc_scale_table)($28)
sw	$2,2788($18)
lw	$2,%got(wmv1_c_dc_scale_table)($28)
b	$L38
sw	$2,2792($18)

$L7:
lw	$2,%got(wmv3_dc_scale_table)($28)
sw	$2,2788($18)
b	$L38
sw	$2,2792($18)

$L2:
slt	$4,$4,4
bne	$4,$0,$L59
lw	$2,%got(initialized.6479)($28)

$L38:
lw	$17,%got(wmv1_scantable)($28)
addiu	$16,$18,5664
lw	$25,%call16(ff_init_scantable)($28)
addiu	$5,$18,8728
move	$4,$16
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
addiu	$6,$17,64

addiu	$5,$18,8860
lw	$28,56($sp)
addiu	$6,$17,128
lw	$25,%call16(ff_init_scantable)($28)
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$16

addiu	$5,$18,8992
lw	$28,56($sp)
addiu	$6,$17,192
lw	$25,%call16(ff_init_scantable)($28)
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$16

addiu	$5,$18,9124
lw	$28,56($sp)
move	$4,$16
lw	$25,%call16(ff_init_scantable)($28)
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$6,$17

lw	$28,56($sp)
$L11:
lw	$2,%got(initialized.6479)($28)
$L59:
lw	$3,%lo(initialized.6479)($2)
beq	$3,$0,$L12
lw	$22,%got(v2_dc_chroma_table)($28)

lw	$2,%got(done.6491)($28)
$L58:
lw	$3,%lo(done.6491)($2)
bne	$3,$0,$L14
li	$3,1			# 0x1

lw	$19,%got(static_rl_table_store)($28)
lw	$22,%got(rl_table)($28)
move	$17,$0
li	$21,188			# 0xbc
li	$20,390			# 0x186
sw	$3,%lo(done.6491)($2)
addiu	$19,$19,%lo(static_rl_table_store)
b	$L22
li	$23,6			# 0x6

$L12:
li	$3,1			# 0x1
lw	$21,%got(v2_dc_lum_table)($28)
li	$20,-256			# 0xffffffffffffff00
lw	$17,%got(ff_mpeg4_DCtab_lum)($28)
li	$23,1			# 0x1
lw	$16,%got(ff_mpeg4_DCtab_chrom)($28)
addiu	$22,$22,%lo(v2_dc_chroma_table)
addiu	$21,$21,%lo(v2_dc_lum_table)
sw	$3,%lo(initialized.6479)($2)
li	$19,256			# 0x100
$L20:
lw	$25,%call16(abs)($28)
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
move	$4,$20

move	$3,$0
lw	$28,56($sp)
$L15:
beq	$2,$0,$L57
sra	$2,$2,1

b	$L15
addiu	$3,$3,1

$L57:
bgez	$20,$L39
subu	$6,$0,$20

sll	$2,$23,$3
addiu	$2,$2,-1
b	$L17
xor	$6,$2,$6

$L39:
move	$6,$20
$L17:
sll	$4,$3,1
addu	$2,$17,$4
lbu	$5,1($2)
lbu	$7,0($2)
sll	$2,$23,$5
addiu	$2,$2,-1
beq	$3,$0,$L18
xor	$2,$2,$7

sll	$2,$2,$3
slt	$7,$3,9
or	$2,$6,$2
bne	$7,$0,$L18
addu	$5,$5,$3

sll	$2,$2,1
addiu	$5,$5,1
ori	$2,$2,0x1
$L18:
addu	$4,$16,$4
sw	$5,4($21)
sw	$2,0($21)
lbu	$5,1($4)
lbu	$4,0($4)
sll	$2,$23,$5
addiu	$2,$2,-1
beq	$3,$0,$L19
xor	$2,$2,$4

sll	$2,$2,$3
slt	$4,$3,9
or	$2,$6,$2
bne	$4,$0,$L19
addu	$5,$5,$3

sll	$2,$2,1
addiu	$5,$5,1
ori	$2,$2,0x1
$L19:
addiu	$20,$20,1
sw	$2,0($22)
sw	$5,4($22)
addiu	$21,$21,8
bne	$20,$19,$L20
addiu	$22,$22,8

b	$L58
lw	$2,%got(done.6491)($28)

$L22:
mul	$2,$17,$21
lw	$25,%call16(init_rl)($28)
lw	$16,%got(rl_table)($28)
addu	$4,$2,$22
mul	$2,$17,$20
addiu	$17,$17,1
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
addu	$5,$2,$19

bne	$17,$23,$L22
lw	$28,56($sp)

li	$2,4			# 0x4
lw	$9,8($16)
li	$3,642			# 0x282
lw	$6,0($16)
li	$8,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,16($sp)
addiu	$4,$16,44
sw	$3,56($16)
li	$5,9			# 0x9
lw	$3,%got(table.6500)($28)
addiu	$6,$6,1
addiu	$7,$9,2
addiu	$3,$3,%lo(table.6500)
sw	$3,48($16)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$2,28($sp)
sw	$8,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

lw	$2,60($16)
bne	$2,$0,$L23
lw	$28,56($sp)

lw	$5,%got(rl_vlc_table.6499)($28)
li	$7,642			# 0x282
li	$6,128			# 0x80
addiu	$5,$5,%lo(rl_vlc_table.6499)
$L24:
mul	$3,$2,$7
addu	$4,$3,$5
addu	$3,$16,$2
addiu	$2,$2,4
bne	$2,$6,$L24
sw	$4,60($3)

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(rl_table)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

lw	$28,56($sp)
$L23:
li	$2,4			# 0x4
lw	$9,196($16)
li	$3,1104			# 0x450
lw	$6,188($16)
li	$8,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,16($sp)
addiu	$4,$16,232
sw	$3,244($16)
li	$5,9			# 0x9
lw	$3,%got(table.6506)($28)
addiu	$6,$6,1
addiu	$7,$9,2
addiu	$3,$3,%lo(table.6506)
sw	$3,236($16)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$2,28($sp)
sw	$8,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

lw	$2,248($16)
bne	$2,$0,$L25
lw	$28,56($sp)

lw	$5,%got(rl_vlc_table.6505)($28)
li	$7,1104			# 0x450
li	$6,128			# 0x80
addiu	$5,$5,%lo(rl_vlc_table.6505)
$L26:
mul	$3,$2,$7
addu	$4,$3,$5
addu	$3,$16,$2
addiu	$2,$2,4
bne	$2,$6,$L26
sw	$4,248($3)

lw	$4,%got(rl_table)($28)
lw	$25,%call16(init_vlc_rl)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
addiu	$4,$4,188

lw	$28,56($sp)
$L25:
li	$2,4			# 0x4
lw	$9,384($16)
li	$3,554			# 0x22a
lw	$6,376($16)
li	$8,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,16($sp)
addiu	$4,$16,420
sw	$3,432($16)
li	$5,9			# 0x9
lw	$3,%got(table.6512)($28)
addiu	$6,$6,1
addiu	$7,$9,2
addiu	$3,$3,%lo(table.6512)
sw	$3,424($16)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$2,28($sp)
sw	$8,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

lw	$2,436($16)
bne	$2,$0,$L27
lw	$28,56($sp)

lw	$5,%got(rl_vlc_table.6511)($28)
li	$7,554			# 0x22a
li	$6,128			# 0x80
addiu	$5,$5,%lo(rl_vlc_table.6511)
$L28:
mul	$3,$2,$7
addu	$4,$3,$5
addu	$3,$16,$2
addiu	$2,$2,4
bne	$2,$6,$L28
sw	$4,436($3)

lw	$4,%got(rl_table)($28)
lw	$25,%call16(init_vlc_rl)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
addiu	$4,$4,376

lw	$28,56($sp)
$L27:
li	$2,4			# 0x4
lw	$9,572($16)
li	$3,940			# 0x3ac
lw	$6,564($16)
li	$8,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,16($sp)
addiu	$4,$16,608
sw	$3,620($16)
li	$5,9			# 0x9
lw	$3,%got(table.6518)($28)
addiu	$6,$6,1
addiu	$7,$9,2
addiu	$3,$3,%lo(table.6518)
sw	$3,612($16)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$2,28($sp)
sw	$8,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

lw	$2,624($16)
bne	$2,$0,$L29
lw	$28,56($sp)

lw	$5,%got(rl_vlc_table.6517)($28)
li	$7,940			# 0x3ac
li	$6,128			# 0x80
addiu	$5,$5,%lo(rl_vlc_table.6517)
$L30:
mul	$3,$2,$7
addu	$4,$3,$5
addu	$3,$16,$2
addiu	$2,$2,4
bne	$2,$6,$L30
sw	$4,624($3)

lw	$4,%got(rl_table)($28)
lw	$25,%call16(init_vlc_rl)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
addiu	$4,$4,564

lw	$28,56($sp)
$L29:
li	$2,4			# 0x4
lw	$9,760($16)
li	$3,962			# 0x3c2
lw	$6,752($16)
li	$8,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,16($sp)
addiu	$4,$16,796
sw	$3,808($16)
li	$5,9			# 0x9
lw	$3,%got(table.6524)($28)
addiu	$6,$6,1
addiu	$7,$9,2
addiu	$3,$3,%lo(table.6524)
sw	$3,800($16)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$2,28($sp)
sw	$8,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

lw	$2,812($16)
bne	$2,$0,$L31
lw	$28,56($sp)

lw	$5,%got(rl_vlc_table.6523)($28)
li	$7,962			# 0x3c2
li	$6,128			# 0x80
addiu	$5,$5,%lo(rl_vlc_table.6523)
$L32:
mul	$3,$2,$7
addu	$4,$3,$5
addu	$3,$16,$2
addiu	$2,$2,4
bne	$2,$6,$L32
sw	$4,812($3)

lw	$4,%got(rl_table)($28)
lw	$25,%call16(init_vlc_rl)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
addiu	$4,$4,752

lw	$28,56($sp)
$L31:
li	$2,4			# 0x4
lw	$9,948($16)
li	$3,554			# 0x22a
lw	$6,940($16)
li	$8,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$2,16($sp)
addiu	$4,$16,984
sw	$3,996($16)
li	$5,9			# 0x9
lw	$3,%got(table.6530)($28)
addiu	$6,$6,1
addiu	$7,$9,2
addiu	$3,$3,%lo(table.6530)
sw	$3,988($16)
sw	$8,20($sp)
sw	$9,24($sp)
sw	$2,28($sp)
sw	$8,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,48($sp)

lw	$2,1000($16)
bne	$2,$0,$L33
lw	$28,56($sp)

lw	$5,%got(rl_vlc_table.6529)($28)
li	$7,554			# 0x22a
li	$6,128			# 0x80
addiu	$5,$5,%lo(rl_vlc_table.6529)
$L34:
mul	$3,$2,$7
addu	$4,$3,$5
addu	$3,$16,$2
addiu	$2,$2,4
bne	$2,$6,$L34
sw	$4,1000($3)

lw	$4,%got(rl_table)($28)
lw	$25,%call16(init_vlc_rl)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
addiu	$4,$4,940

lw	$28,56($sp)
$L33:
li	$17,1			# 0x1
lw	$21,%got(mv_tables)($28)
li	$2,3714			# 0xe82
sw	$0,36($sp)
li	$16,4			# 0x4
lw	$25,%call16(init_vlc_sparse)($28)
sw	$17,16($sp)
li	$20,2			# 0x2
sw	$17,20($sp)
addiu	$4,$21,24
sw	$2,36($21)
li	$5,9			# 0x9
lw	$2,%got(table.6534)($28)
li	$19,8			# 0x8
lw	$23,%got(wmv2_inter_table)($28)
addiu	$2,$2,%lo(table.6534)
sw	$2,28($21)
sw	$20,28($sp)
sw	$20,32($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$16,48($sp)
lw	$6,0($21)
lw	$2,4($21)
lw	$7,8($21)
addiu	$6,$6,1
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,24($sp)

li	$2,2694			# 0xa86
lw	$28,56($sp)
addiu	$4,$21,64
lw	$6,40($21)
li	$5,9			# 0x9
lw	$3,44($21)
lw	$7,48($21)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$6,$6,1
sw	$2,76($21)
lw	$2,%got(table.6535)($28)
addiu	$2,$2,%lo(table.6535)
sw	$2,68($21)
sw	$3,24($sp)
sw	$17,16($sp)
sw	$17,20($sp)
sw	$20,28($sp)
sw	$20,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,120			# 0x78
sw	$19,16($sp)
sw	$16,20($sp)
lw	$2,%got(table.6536)($28)
lw	$22,%got(ff_msmp4_dc_luma_vlc_hw)($28)
lw	$3,%got(ff_table0_dc_lum)($28)
addiu	$2,$2,%lo(table.6536)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$22,$22,%lo(ff_msmp4_dc_luma_vlc_hw)
addiu	$7,$3,4
sw	$2,4($22)
li	$2,1158			# 0x486
move	$4,$22
sw	$2,12($22)
sw	$3,24($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,120			# 0x78
sw	$19,16($sp)
sw	$16,20($sp)
lw	$2,%got(table.6537)($28)
lw	$21,%got(ff_msmp4_dc_chroma_vlc_hw)($28)
lw	$3,%got(ff_table0_dc_chroma)($28)
addiu	$2,$2,%lo(table.6537)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$21,$21,%lo(ff_msmp4_dc_chroma_vlc_hw)
addiu	$7,$3,4
sw	$2,4($21)
li	$2,1118			# 0x45e
move	$4,$21
sw	$2,12($21)
sw	$3,24($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$2,1476			# 0x5c4
lw	$28,56($sp)
li	$5,9			# 0x9
li	$6,120			# 0x78
sw	$2,28($22)
sw	$19,16($sp)
lw	$2,%got(table.6538)($28)
lw	$3,%got(ff_table1_dc_lum)($28)
lw	$4,%got(ff_msmp4_dc_luma_vlc_hw+16)($28)
addiu	$2,$2,%lo(table.6538)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,4
addiu	$4,$4,%lo(ff_msmp4_dc_luma_vlc_hw+16)
sw	$2,20($22)
sw	$3,24($sp)
sw	$16,20($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$2,1216			# 0x4c0
lw	$28,56($sp)
li	$5,9			# 0x9
li	$6,120			# 0x78
sw	$2,28($21)
sw	$19,16($sp)
lw	$2,%got(table.6539)($28)
lw	$3,%got(ff_table1_dc_chroma)($28)
lw	$4,%got(ff_msmp4_dc_chroma_vlc_hw+16)($28)
addiu	$2,$2,%lo(table.6539)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,4
addiu	$4,$4,%lo(ff_msmp4_dc_chroma_vlc_hw+16)
sw	$2,20($21)
sw	$3,24($sp)
sw	$16,20($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,512			# 0x200
sw	$19,16($sp)
sw	$16,20($sp)
sw	$19,28($sp)
lw	$3,%got(v2_dc_lum_table)($28)
lw	$2,%got(v2_dc_lum_vlc_hw)($28)
lw	$7,%got(v2_dc_lum_table+4)($28)
addiu	$3,$3,%lo(v2_dc_lum_table)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(v2_dc_lum_vlc_hw)
sw	$16,32($sp)
sw	$0,36($sp)
addiu	$7,$7,%lo(v2_dc_lum_table+4)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.6540)($28)
sw	$0,40($sp)
sw	$0,44($sp)
addiu	$3,$3,%lo(table.6540)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,1472			# 0x5c0
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,512			# 0x200
sw	$19,16($sp)
sw	$16,20($sp)
sw	$19,28($sp)
lw	$3,%got(v2_dc_chroma_table)($28)
lw	$2,%got(v2_dc_chroma_vlc_hw)($28)
lw	$7,%got(v2_dc_chroma_table+4)($28)
addiu	$3,$3,%lo(v2_dc_chroma_table)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(v2_dc_chroma_vlc_hw)
sw	$16,32($sp)
sw	$0,36($sp)
addiu	$7,$7,%lo(v2_dc_chroma_table+4)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.6541)($28)
sw	$0,40($sp)
sw	$0,44($sp)
addiu	$3,$3,%lo(table.6541)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,1506			# 0x5e2
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,3			# 0x3
lw	$28,56($sp)
li	$6,4			# 0x4
sw	$20,16($sp)
sw	$17,20($sp)
sw	$20,28($sp)
lw	$3,%got(v2_intra_cbpc)($28)
lw	$2,%got(v2_intra_cbpc_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$17,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(v2_intra_cbpc_vlc_hw)
lw	$3,%got(table.6542)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6542)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$19,12($2)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,4($2)

li	$5,7			# 0x7
lw	$28,56($sp)
li	$6,8			# 0x8
sw	$20,16($sp)
sw	$17,20($sp)
sw	$20,28($sp)
lw	$3,%got(v2_mb_type)($28)
lw	$2,%got(v2_mb_type_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$17,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(v2_mb_type_vlc_hw)
lw	$3,%got(table.6543)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6543)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,128			# 0x80
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,33			# 0x21
sw	$20,16($sp)
sw	$17,20($sp)
sw	$20,28($sp)
lw	$3,%got(mvtab)($28)
lw	$2,%got(v2_mv_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$17,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(v2_mv_vlc_hw)
lw	$3,%got(table.6544)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6544)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,538			# 0x21a
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,128			# 0x80
lw	$3,0($23)
sw	$19,16($sp)
sw	$16,20($sp)
lw	$2,%got(table.6545)($28)
addiu	$7,$3,4
lw	$21,%got(ff_mb_non_intra_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(table.6545)
addiu	$21,$21,%lo(ff_mb_non_intra_vlc_hw)
sw	$2,4($21)
li	$2,1636			# 0x664
move	$4,$21
sw	$2,12($21)
sw	$3,24($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$2,2648			# 0xa58
lw	$28,56($sp)
li	$5,9			# 0x9
lw	$3,4($23)
li	$6,128			# 0x80
sw	$2,28($21)
sw	$19,16($sp)
lw	$2,%got(table.6546)($28)
addiu	$7,$3,4
lw	$4,%got(ff_mb_non_intra_vlc_hw+16)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(table.6546)
addiu	$4,$4,%lo(ff_mb_non_intra_vlc_hw+16)
sw	$2,20($21)
sw	$3,24($sp)
sw	$16,20($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$2,1532			# 0x5fc
lw	$28,56($sp)
li	$5,9			# 0x9
lw	$3,8($23)
li	$6,128			# 0x80
sw	$2,44($21)
sw	$19,16($sp)
lw	$2,%got(table.6547)($28)
addiu	$7,$3,4
lw	$4,%got(ff_mb_non_intra_vlc_hw+32)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(table.6547)
addiu	$4,$4,%lo(ff_mb_non_intra_vlc_hw+32)
sw	$2,36($21)
sw	$3,24($sp)
sw	$16,20($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$2,2488			# 0x9b8
lw	$28,56($sp)
li	$5,9			# 0x9
lw	$3,12($23)
li	$6,128			# 0x80
sw	$2,60($21)
sw	$19,16($sp)
lw	$2,%got(table.6548)($28)
addiu	$7,$3,4
lw	$4,%got(ff_mb_non_intra_vlc_hw+48)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(table.6548)
addiu	$4,$4,%lo(ff_mb_non_intra_vlc_hw+48)
sw	$2,52($21)
sw	$3,24($sp)
sw	$16,20($sp)
sw	$19,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$16,48($sp)

li	$5,9			# 0x9
lw	$28,56($sp)
li	$6,64			# 0x40
sw	$16,16($sp)
sw	$20,20($sp)
sw	$16,28($sp)
lw	$3,%got(ff_msmp4_mb_i_table)($28)
lw	$2,%got(ff_msmp4_mb_i_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,2
sw	$20,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(ff_msmp4_mb_i_vlc_hw)
lw	$3,%got(table.6549)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6549)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,536			# 0x218
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,3			# 0x3
lw	$28,56($sp)
li	$6,4			# 0x4
sw	$20,16($sp)
sw	$17,20($sp)
sw	$20,28($sp)
lw	$3,%got(table_inter_intra)($28)
lw	$2,%got(ff_inter_intra_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$17,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(ff_inter_intra_vlc_hw)
lw	$3,%got(table.6550)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6550)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$19,12($2)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,4($2)

li	$5,6			# 0x6
lw	$28,56($sp)
li	$6,16			# 0x10
sw	$20,16($sp)
sw	$17,20($sp)
sw	$20,28($sp)
lw	$3,%got(ff_h263_cbpy_tab)($28)
lw	$2,%got(ff_ms_h263_cbpy_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$17,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(ff_ms_h263_cbpy_vlc_hw)
lw	$3,%got(table.6551)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6551)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,64			# 0x40
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,6			# 0x6
lw	$28,56($sp)
li	$6,9			# 0x9
sw	$17,16($sp)
sw	$17,20($sp)
sw	$17,28($sp)
lw	$3,%got(ff_h263_intra_MCBPC_code)($28)
lw	$2,%got(ms_h263_intra_MCBPC_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
lw	$7,%got(ff_h263_intra_MCBPC_bits)($28)
sw	$3,24($sp)
addiu	$2,$2,%lo(ms_h263_intra_MCBPC_vlc_hw)
lw	$3,%got(table.6552)($28)
sw	$17,32($sp)
move	$4,$2
sw	$0,36($sp)
addiu	$3,$3,%lo(table.6552)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,72			# 0x48
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,7			# 0x7
lw	$28,56($sp)
li	$6,28			# 0x1c
sw	$17,16($sp)
sw	$17,20($sp)
sw	$17,28($sp)
lw	$3,%got(ff_h263_inter_MCBPC_code)($28)
lw	$2,%got(ms_h263_inter_MCBPC_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
lw	$7,%got(ff_h263_inter_MCBPC_bits)($28)
sw	$3,24($sp)
addiu	$2,$2,%lo(ms_h263_inter_MCBPC_vlc_hw)
lw	$3,%got(table.6553)($28)
sw	$17,32($sp)
move	$4,$2
sw	$0,36($sp)
addiu	$3,$3,%lo(table.6553)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$16,48($sp)
sw	$3,4($2)
li	$3,198			# 0xc6
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

lw	$28,56($sp)
$L14:
lw	$2,10304($18)
blez	$2,$L35
slt	$3,$2,3

bne	$3,$0,$L36
slt	$2,$2,5

beq	$2,$0,$L35
lw	$2,%got(msmpeg4v34_decode_mb)($28)

addiu	$2,$2,%lo(msmpeg4v34_decode_mb)
b	$L35
sw	$2,10528($18)

$L36:
lw	$2,%got(msmpeg4v12_decode_mb)($28)
addiu	$2,$2,%lo(msmpeg4v12_decode_mb)
sw	$2,10528($18)
$L35:
lw	$3,164($18)
move	$2,$0
lw	$31,100($sp)
lw	$23,96($sp)
lw	$22,92($sp)
lw	$21,88($sp)
lw	$20,84($sp)
lw	$19,80($sp)
lw	$17,72($sp)
lw	$16,68($sp)
sw	$3,10292($18)
lw	$18,76($sp)
j	$31
addiu	$sp,$sp,104

.set	macro
.set	reorder
.end	JZC_msmpeg4_decode_init
.size	JZC_msmpeg4_decode_init, .-JZC_msmpeg4_decode_init
.section	.text.msmpeg4v2_decode_motion.constprop.4,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	msmpeg4v2_decode_motion.constprop.4
.type	msmpeg4v2_decode_motion.constprop.4, @function
msmpeg4v2_decode_motion.constprop.4:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,%got(v2_mv_vlc_hw+4)($28)
li	$11,16711680			# 0xff0000
lw	$8,10340($4)
li	$9,-16777216			# 0xffffffffff000000
lw	$10,10332($4)
addiu	$11,$11,255
ori	$9,$9,0xff00
lw	$12,%lo(v2_mv_vlc_hw+4)($2)
srl	$2,$8,3
andi	$3,$8,0x7
addu	$2,$10,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
srl	$7,$6,8
sll	$2,$6,8
and	$7,$7,$11
and	$2,$2,$9
or	$2,$7,$2
sll	$6,$2,16
srl	$2,$2,16
or	$6,$6,$2
sll	$3,$6,$3
srl	$3,$3,23
sll	$3,$3,2
addu	$3,$12,$3
lh	$6,2($3)
.set	noreorder
.set	nomacro
bltz	$6,$L68
lh	$3,0($3)
.set	macro
.set	reorder

$L62:
addu	$6,$6,$8
.set	noreorder
.set	nomacro
bltz	$3,$L65
sw	$6,10340($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L66
andi	$7,$6,0x7
.set	macro
.set	reorder

srl	$2,$6,3
addu	$10,$10,$2
addiu	$6,$6,1
lbu	$2,0($10)
sw	$6,10340($4)
sll	$4,$2,$7
andi	$4,$4,0x00ff
srl	$4,$4,7
subu	$2,$0,$4
xor	$2,$2,$3
addu	$2,$2,$4
addu	$2,$2,$5
slt	$3,$2,-63
.set	noreorder
.set	nomacro
bne	$3,$0,$L69
slt	$3,$2,64
.set	macro
.set	reorder

bne	$3,$0,$L70
.set	noreorder
.set	nomacro
j	$31
addiu	$2,$2,-64
.set	macro
.set	reorder

$L66:
move	$2,$5
$L70:
j	$31
$L65:
.set	noreorder
.set	nomacro
j	$31
li	$2,65535			# 0xffff
.set	macro
.set	reorder

$L68:
addiu	$8,$8,9
srl	$7,$8,3
andi	$2,$8,0x7
addu	$7,$10,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $13, 3($7)  
lwr $13, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$13,8
sll	$13,$13,8
and	$11,$7,$11
and	$9,$13,$9
or	$9,$11,$9
sll	$7,$9,16
srl	$9,$9,16
or	$9,$7,$9
sll	$2,$9,$2
srl	$6,$2,$6
addu	$3,$6,$3
sll	$3,$3,2
addu	$12,$12,$3
lh	$3,0($12)
.set	noreorder
.set	nomacro
b	$L62
lh	$6,2($12)
.set	macro
.set	reorder

$L69:
.set	noreorder
.set	nomacro
j	$31
addiu	$2,$2,64
.set	macro
.set	reorder

.end	msmpeg4v2_decode_motion.constprop.4
.size	msmpeg4v2_decode_motion.constprop.4, .-msmpeg4v2_decode_motion.constprop.4
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"illegal dc vlc\012\000"
.align	2
$LC1:
.ascii	"dc overflow- block: %d qscale: %d//\012\000"
.align	2
$LC2:
.ascii	"dc overflow+ L qscale: %d//\012\000"
.align	2
$LC3:
.ascii	"dc overflow+ C qscale: %d//\012\000"
.align	2
$LC4:
.ascii	"ignoring overflow at %d %d\012\000"
.align	2
$LC5:
.ascii	"ac-tex damaged at %d %d\012\000"
.section	.text.JZC_msmpeg4_decode_block.constprop.5,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	JZC_msmpeg4_decode_block.constprop.5
.type	JZC_msmpeg4_decode_block.constprop.5, @function
JZC_msmpeg4_decode_block.constprop.5:
.frame	$sp,88,$31		# vars= 16, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-88
lw	$2,8004($4)
.cprestore	24
sw	$21,68($sp)
move	$21,$6
sw	$20,64($sp)
move	$20,$5
sw	$19,60($sp)
move	$19,$4
sw	$18,56($sp)
move	$18,$7
sw	$31,84($sp)
sw	$fp,80($sp)
sw	$23,76($sp)
sw	$22,72($sp)
sw	$17,52($sp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L252
sw	$16,48($sp)
.set	macro
.set	reorder

lw	$4,10304($4)
addiu	$3,$4,-1
sltu	$3,$3,2
bne	$3,$0,$L253
.set	noreorder
.set	nomacro
beq	$7,$0,$L190
lw	$3,10276($19)
.set	macro
.set	reorder

li	$22,188			# 0xbc
lw	$2,%got(rl_table)($28)
addiu	$3,$3,3
addiu	$14,$19,9128
mul	$4,$3,$22
move	$23,$0
li	$24,1			# 0x1
li	$17,1			# 0x1
li	$9,-1			# 0xffffffffffffffff
addu	$22,$4,$2
lw	$12,60($22)
$L136:
lw	$fp,10340($19)
li	$11,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
lw	$13,10332($19)
addiu	$11,$11,255
li	$25,1			# 0x1
li	$7,32			# 0x20
li	$6,1073741824			# 0x40000000
move	$16,$fp
.set	noreorder
.set	nomacro
b	$L168
ori	$10,$10,0xff00
.set	macro
.set	reorder

$L257:
lw	$5,10304($19)
beq	$5,$25,$L142
bltz	$2,$L143
and	$4,$2,$6
.set	noreorder
.set	nomacro
beq	$4,$0,$L254
sll	$2,$2,2
.set	macro
.set	reorder

srl	$4,$2,23
sll	$4,$4,2
addu	$4,$12,$4
lb	$18,2($4)
.set	noreorder
.set	nomacro
bltz	$18,$L158
lh	$8,0($4)
.set	macro
.set	reorder

addiu	$3,$3,2
$L159:
teq	$24,$0,7
div	$0,$8,$24
lbu	$15,3($4)
sll	$2,$2,$18
addiu	$3,$3,1
sra	$5,$15,7
sra	$16,$2,31
addiu	$5,$5,8
xor	$8,$16,$8
sll	$5,$5,2
addu	$16,$3,$18
addu	$5,$22,$5
srl	$2,$2,31
lw	$5,4($5)
addu	$2,$8,$2
mflo	$3
addu	$4,$5,$3
lb	$3,0($4)
addu	$3,$15,$3
addu	$3,$3,$17
addu	$9,$9,$3
$L157:
slt	$3,$9,63
beq	$3,$0,$L255
$L162:
addu	$3,$14,$9
lbu	$3,0($3)
sll	$3,$3,1
addu	$3,$20,$3
sh	$2,0($3)
$L168:
srl	$3,$16,3
andi	$2,$16,0x7
addu	$3,$13,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sll	$3,$3,8
srl	$4,$4,8
and	$3,$3,$10
and	$4,$4,$11
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
srl	$5,$2,23
sll	$5,$5,2
addu	$5,$12,$5
lb	$3,2($5)
.set	noreorder
.set	nomacro
bltz	$3,$L256
lh	$4,0($5)
.set	macro
.set	reorder

$L140:
sll	$2,$2,$3
lbu	$15,3($5)
.set	noreorder
.set	nomacro
beq	$4,$0,$L257
addu	$3,$3,$16
.set	macro
.set	reorder

sra	$8,$2,31
addu	$9,$9,$15
srl	$2,$2,31
xor	$4,$8,$4
addiu	$16,$3,1
slt	$3,$9,63
.set	noreorder
.set	nomacro
bne	$3,$0,$L162
addu	$2,$4,$2
.set	macro
.set	reorder

$L255:
addiu	$17,$9,-192
li	$3,-64			# 0xffffffffffffffc0
and	$3,$17,$3
beq	$3,$0,$L163
lw	$3,10344($19)
subu	$fp,$3,$fp
li	$3,-128			# 0xffffffffffffff80
.set	noreorder
.set	nomacro
beq	$17,$3,$L258
li	$3,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L164:
lw	$2,9816($19)
slt	$2,$2,2
beq	$2,$0,$L166
$L165:
.set	noreorder
.set	nomacro
bltz	$fp,$L166
li	$5,16			# 0x10
.set	macro
.set	reorder

lw	$2,7996($19)
lw	$6,%got($LC4)($28)
lw	$4,0($19)
lw	$7,7992($19)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC4)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

$L167:
sw	$16,10340($19)
$L132:
lw	$2,8004($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L169
sll	$3,$21,2
.set	macro
.set	reorder

lw	$2,10304($19)
$L273:
slt	$2,$2,4
bne	$2,$0,$L171
blez	$17,$L171
$L192:
li	$17,63			# 0x3f
$L171:
addiu	$21,$21,2170
lw	$31,84($sp)
lw	$fp,80($sp)
move	$2,$0
sll	$21,$21,2
lw	$23,76($sp)
lw	$22,72($sp)
addu	$19,$19,$21
lw	$20,64($sp)
lw	$21,68($sp)
lw	$18,56($sp)
lw	$16,48($sp)
sw	$17,0($19)
lw	$19,60($sp)
lw	$17,52($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L190:
addiu	$21,$21,2170
li	$3,-1			# 0xffffffffffffffff
sll	$21,$21,2
addu	$19,$19,$21
sw	$3,0($19)
$L236:
lw	$31,84($sp)
lw	$fp,80($sp)
lw	$23,76($sp)
lw	$22,72($sp)
lw	$21,68($sp)
lw	$20,64($sp)
lw	$19,60($sp)
lw	$18,56($sp)
lw	$17,52($sp)
lw	$16,48($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,88
.set	macro
.set	reorder

$L252:
lw	$2,10304($4)
slt	$23,$6,4
slt	$3,$2,3
.set	noreorder
.set	nomacro
bne	$3,$0,$L259
lw	$5,10340($4)
.set	macro
.set	reorder

lw	$8,10332($4)
li	$7,16711680			# 0xff0000
srl	$3,$5,3
lw	$9,10284($4)
li	$6,-16777216			# 0xffffffffff000000
addu	$3,$8,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
ori	$6,$6,0xff00
and	$3,$3,$7
and	$4,$4,$6
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
sll	$9,$9,4
or	$3,$3,$4
.set	noreorder
.set	nomacro
beq	$23,$0,$L85
andi	$10,$5,0x7
.set	macro
.set	reorder

lw	$4,%got(ff_msmp4_dc_luma_vlc_hw)($28)
addiu	$4,$4,%lo(ff_msmp4_dc_luma_vlc_hw)
$L250:
addu	$4,$9,$4
sll	$3,$3,$10
srl	$3,$3,23
lw	$10,4($4)
sll	$3,$3,2
addu	$3,$10,$3
lh	$9,2($3)
.set	noreorder
.set	nomacro
bltz	$9,$L90
lh	$16,0($3)
.set	macro
.set	reorder

move	$17,$16
$L91:
addu	$5,$9,$5
.set	noreorder
.set	nomacro
bltz	$17,$L260
sw	$5,10340($19)
.set	macro
.set	reorder

li	$3,119			# 0x77
beq	$17,$3,$L261
.set	noreorder
.set	nomacro
beq	$17,$0,$L276
sll	$5,$21,2
.set	macro
.set	reorder

lw	$4,10340($19)
lw	$5,10332($19)
srl	$3,$4,3
andi	$6,$4,0x7
addu	$5,$5,$3
addiu	$4,$4,1
lbu	$3,0($5)
sw	$4,10340($19)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
subu	$16,$0,$3
xor	$16,$17,$16
addu	$17,$16,$3
$L84:
sll	$5,$21,2
$L276:
lw	$fp,2728($19)
lw	$6,2780($19)
addu	$5,$19,$5
lw	$22,2784($19)
lw	$10,10296($19)
move	$3,$6
lw	$8,8012($5)
lw	$4,8036($5)
movz	$3,$22,$23
sll	$8,$8,1
nor	$7,$0,$4
addu	$fp,$fp,$8
sll	$7,$7,1
sll	$4,$4,1
addu	$7,$fp,$7
lh	$9,-2($fp)
subu	$4,$fp,$4
lh	$8,0($7)
.set	noreorder
.set	nomacro
beq	$10,$0,$L239
lh	$4,0($4)
.set	macro
.set	reorder

andi	$7,$21,0x2
.set	noreorder
.set	nomacro
beq	$7,$0,$L235
li	$7,1024			# 0x400
.set	macro
.set	reorder

$L239:
slt	$2,$2,4
$L99:
li	$7,8			# 0x8
.set	noreorder
.set	nomacro
beq	$3,$7,$L262
sra	$7,$3,1
.set	macro
.set	reorder

addu	$9,$9,$7
addu	$8,$8,$7
teq	$3,$0,7
div	$0,$9,$3
addu	$7,$4,$7
mflo	$11
teq	$3,$0,7
div	$0,$8,$3
mflo	$10
teq	$3,$0,7
div	$0,$7,$3
mflo	$16
$L104:
.set	noreorder
.set	nomacro
bne	$2,$0,$L105
lw	$25,%call16(abs)($28)
.set	macro
.set	reorder

lw	$2,10324($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L106
subu	$4,$11,$10
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$21,$2,$L107
li	$2,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$21,$2,$L196
li	$2,3			# 0x3
.set	macro
.set	reorder

beq	$21,$2,$L263
lw	$11,192($19)
lw	$2,196($19)
.set	noreorder
.set	nomacro
beq	$23,$0,$L110
lw	$13,7996($19)
.set	macro
.set	reorder

sll	$9,$13,1
lw	$7,7992($19)
sra	$2,$21,1
lw	$8,2088($19)
andi	$4,$21,0x1
addu	$2,$2,$9
sll	$5,$7,1
sll	$2,$2,3
addu	$4,$4,$5
mul	$5,$11,$2
sll	$4,$4,3
addu	$2,$5,$4
addu	$9,$8,$2
$L111:
.set	noreorder
.set	nomacro
bne	$7,$0,$L112
sll	$16,$3,3
.set	macro
.set	reorder

sra	$16,$3,1
addiu	$16,$16,1024
teq	$3,$0,7
div	$0,$16,$3
mflo	$16
$L113:
.set	noreorder
.set	nomacro
bne	$13,$0,$L117
sll	$8,$11,3
.set	macro
.set	reorder

sra	$2,$3,1
addiu	$2,$2,1024
teq	$3,$0,7
div	$0,$2,$3
mflo	$3
$L118:
lw	$2,9884($19)
beq	$2,$0,$L198
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$4,$L264
li	$4,2			# 0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$4,$L265
li	$2,1			# 0x1
.set	macro
.set	reorder

move	$16,$3
.set	noreorder
.set	nomacro
b	$L122
sw	$2,32($sp)
.set	macro
.set	reorder

$L259:
lw	$9,10332($4)
li	$7,16711680			# 0xff0000
srl	$3,$5,3
li	$6,-16777216			# 0xffffffffff000000
addu	$3,$9,$3
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
ori	$6,$6,0xff00
.set	noreorder
.set	nomacro
beq	$23,$0,$L74
srl	$3,$4,8
.set	macro
.set	reorder

lw	$8,%got(v2_dc_lum_vlc_hw+4)($28)
lw	$8,%lo(v2_dc_lum_vlc_hw+4)($8)
$L248:
sll	$4,$4,8
and	$3,$3,$7
and	$4,$4,$6
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
andi	$10,$5,0x7
or	$3,$4,$3
sll	$3,$3,$10
srl	$3,$3,23
sll	$3,$3,2
addu	$3,$8,$3
lh	$10,2($3)
.set	noreorder
.set	nomacro
bltz	$10,$L266
lh	$16,0($3)
.set	macro
.set	reorder

$L80:
addu	$5,$10,$5
.set	noreorder
.set	nomacro
bltz	$16,$L189
sw	$5,10340($19)
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
bne	$2,$3,$L84
addiu	$17,$16,-256
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$23,$0,$L96
move	$2,$0
.set	macro
.set	reorder

addiu	$2,$21,-3
$L96:
sll	$2,$2,2
addu	$2,$19,$2
lw	$16,2712($2)
addu	$16,$17,$16
.set	noreorder
.set	nomacro
bltz	$16,$L189
sw	$16,2712($2)
.set	macro
.set	reorder

$L127:
bne	$23,$0,$L129
$L193:
lw	$22,2784($19)
$L126:
lw	$3,10280($19)
sll	$22,$22,8
slt	$2,$22,$16
addiu	$22,$3,3
sll	$3,$22,4
sll	$4,$22,6
subu	$3,$4,$3
subu	$22,$3,$22
sll	$3,$22,2
lw	$22,%got(rl_table)($28)
.set	noreorder
.set	nomacro
bne	$2,$0,$L267
addu	$22,$22,$3
.set	macro
.set	reorder

$L130:
lw	$17,10304($19)
.set	noreorder
.set	nomacro
beq	$18,$0,$L205
sh	$16,0($20)
.set	macro
.set	reorder

lw	$2,2824($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L133
addiu	$14,$19,8732
.set	macro
.set	reorder

lw	$2,32($sp)
bne	$2,$0,$L134
.set	noreorder
.set	nomacro
b	$L135
addiu	$14,$19,8996
.set	macro
.set	reorder

$L129:
lw	$22,10276($19)
lw	$4,2780($19)
sll	$3,$22,6
sll	$2,$22,4
sll	$4,$4,8
subu	$2,$3,$2
slt	$3,$4,$16
subu	$22,$2,$22
sll	$2,$22,2
lw	$22,%got(rl_table)($28)
.set	noreorder
.set	nomacro
beq	$3,$0,$L130
addu	$22,$22,$2
.set	macro
.set	reorder

lw	$6,%got($LC2)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
lw	$7,2872($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC2)
.set	macro
.set	reorder

lw	$2,10324($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L130
lw	$28,24($sp)
.set	macro
.set	reorder

$L240:
.set	noreorder
.set	nomacro
b	$L236
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L253:
lw	$24,2872($19)
.set	noreorder
.set	nomacro
beq	$7,$0,$L190
lw	$3,10276($19)
.set	macro
.set	reorder

addiu	$3,$3,3
lw	$5,%got(rl_table)($28)
li	$2,47			# 0x2f
li	$22,188			# 0xbc
mul	$6,$3,$2
addiu	$23,$24,-1
xori	$4,$4,0x2
ori	$23,$23,0x1
sltu	$17,$0,$4
addiu	$14,$19,9128
li	$9,-1			# 0xffffffffffffffff
addu	$2,$6,$24
mul	$6,$3,$22
addiu	$2,$2,14
sll	$24,$24,1
sll	$2,$2,2
addu	$2,$5,$2
addu	$22,$6,$5
.set	noreorder
.set	nomacro
b	$L136
lw	$12,4($2)
.set	macro
.set	reorder

$L256:
sll	$2,$2,9
addiu	$16,$16,9
srl	$3,$2,$3
addu	$5,$3,$4
sll	$5,$5,2
addu	$5,$12,$5
lh	$4,0($5)
.set	noreorder
.set	nomacro
b	$L140
lb	$3,2($5)
.set	macro
.set	reorder

$L254:
addiu	$3,$3,2
$L142:
srl	$2,$3,3
andi	$18,$3,0x7
addu	$2,$13,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$4
sll	$2,$2,8
srl	$4,$4,8
and	$2,$2,$10
and	$4,$4,$11
or	$2,$4,$2
sll	$4,$2,16
srl	$2,$2,16
slt	$5,$5,4
or	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$5,$0,$L145
sll	$18,$2,$18
.set	macro
.set	reorder

sll	$2,$18,7
sll	$5,$18,1
sra	$2,$2,24
srl	$15,$18,31
srl	$5,$5,26
.set	noreorder
.set	nomacro
blez	$2,$L155
addiu	$16,$3,15
.set	macro
.set	reorder

mul	$3,$2,$24
$L277:
addu	$2,$3,$23
$L156:
addiu	$5,$5,1
addu	$9,$9,$5
addiu	$3,$9,192
.set	noreorder
.set	nomacro
b	$L157
movn	$9,$3,$15
.set	macro
.set	reorder

$L145:
lw	$8,10312($19)
srl	$15,$18,31
sll	$5,$18,1
.set	noreorder
.set	nomacro
beq	$8,$0,$L147
addiu	$2,$3,1
.set	macro
.set	reorder

lw	$18,10316($19)
subu	$16,$7,$8
$L148:
sll	$4,$5,$18
addu	$2,$2,$8
srl	$8,$4,31
sll	$4,$4,1
subu	$3,$0,$8
srl	$4,$4,$16
addiu	$2,$2,1
subu	$16,$7,$18
xor	$4,$3,$4
srl	$5,$5,$16
addu	$16,$2,$18
addu	$2,$4,$8
.set	noreorder
.set	nomacro
bgtz	$2,$L277
mul	$3,$2,$24
.set	macro
.set	reorder

$L155:
mul	$2,$2,$24
.set	noreorder
.set	nomacro
b	$L156
subu	$2,$2,$23
.set	macro
.set	reorder

$L143:
sll	$2,$2,1
srl	$5,$2,23
sll	$5,$5,2
addu	$5,$12,$5
lb	$18,2($5)
.set	noreorder
.set	nomacro
bltz	$18,$L160
lh	$4,0($5)
.set	macro
.set	reorder

addiu	$3,$3,1
$L161:
lbu	$16,3($5)
addiu	$3,$3,1
sll	$2,$2,$18
sra	$15,$16,7
addiu	$5,$16,-1
addiu	$15,$15,6
addu	$9,$9,$16
sll	$15,$15,2
addu	$16,$3,$18
addu	$15,$22,$15
andi	$5,$5,0x3f
sra	$8,$2,31
lw	$3,4($15)
srl	$2,$2,31
addu	$5,$3,$5
lb	$3,0($5)
mul	$5,$24,$3
addu	$4,$5,$4
xor	$8,$8,$4
.set	noreorder
.set	nomacro
b	$L157
addu	$2,$8,$2
.set	macro
.set	reorder

$L147:
lw	$4,2872($19)
slt	$4,$4,8
.set	noreorder
.set	nomacro
bne	$4,$0,$L149
srl	$8,$5,29
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$5,$L206
li	$8,2			# 0x2
.set	macro
.set	reorder

addiu	$2,$3,2
sll	$5,$18,2
addiu	$3,$3,7
li	$4,3			# 0x3
$L151:
.set	noreorder
.set	nomacro
bltz	$5,$L269
subu	$16,$7,$4
.set	macro
.set	reorder

addiu	$2,$2,1
addiu	$4,$4,1
.set	noreorder
.set	nomacro
bne	$2,$3,$L151
sll	$5,$5,1
.set	macro
.set	reorder

li	$8,8			# 0x8
li	$16,24			# 0x18
li	$4,8			# 0x8
$L153:
srl	$5,$5,30
sw	$4,10312($19)
addiu	$2,$3,2
addiu	$18,$5,3
srl	$3,$2,3
andi	$5,$2,0x7
addu	$3,$13,$3
sw	$18,10316($19)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$4
sll	$3,$3,8
srl	$4,$4,8
and	$3,$3,$10
and	$4,$4,$11
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
.set	noreorder
.set	nomacro
b	$L148
sll	$5,$3,$5
.set	macro
.set	reorder

$L163:
addu	$14,$14,$17
lbu	$3,0($14)
sll	$3,$3,1
addu	$3,$20,$3
.set	noreorder
.set	nomacro
b	$L167
sh	$2,0($3)
.set	macro
.set	reorder

$L158:
addiu	$3,$3,11
srl	$2,$3,3
andi	$15,$3,0x7
addu	$2,$13,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$4
sll	$2,$2,8
srl	$4,$4,8
and	$5,$4,$11
and	$4,$2,$10
or	$4,$5,$4
sll	$2,$4,16
srl	$4,$4,16
or	$2,$2,$4
sll	$2,$2,$15
srl	$4,$2,$18
addu	$8,$4,$8
sll	$4,$8,2
addu	$4,$12,$4
lh	$8,0($4)
.set	noreorder
.set	nomacro
b	$L159
lb	$18,2($4)
.set	macro
.set	reorder

$L149:
.set	noreorder
.set	nomacro
bne	$8,$0,$L152
sll	$5,$18,4
.set	macro
.set	reorder

srl	$5,$5,31
addiu	$3,$3,5
addiu	$8,$5,8
sll	$5,$18,5
move	$4,$8
.set	noreorder
.set	nomacro
b	$L153
subu	$16,$7,$8
.set	macro
.set	reorder

$L169:
lw	$5,2812($19)
lw	$4,2824($19)
addu	$3,$19,$3
lw	$6,2172($19)
lw	$2,8012($3)
sll	$2,$2,5
.set	noreorder
.set	nomacro
beq	$4,$0,$L173
addu	$2,$5,$2
.set	macro
.set	reorder

lw	$4,32($sp)
bne	$4,$0,$L174
lw	$3,7992($19)
addiu	$4,$2,-32
lw	$5,7996($19)
.set	noreorder
.set	nomacro
beq	$3,$0,$L175
lw	$9,168($19)
.set	macro
.set	reorder

mul	$7,$5,$9
lw	$10,2872($19)
addiu	$3,$3,-1
addu	$3,$7,$3
addu	$9,$6,$3
lb	$5,0($9)
.set	noreorder
.set	nomacro
beq	$10,$5,$L175
li	$3,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

li	$6,1			# 0x1
and	$3,$21,$3
.set	noreorder
.set	nomacro
beq	$3,$6,$L175
sra	$11,$10,1
.set	macro
.set	reorder

addiu	$8,$2,-30
li	$6,1			# 0x1
.set	noreorder
.set	nomacro
b	$L180
li	$12,8			# 0x8
.set	macro
.set	reorder

$L178:
teq	$10,$0,7
div	$0,$4,$10
addiu	$6,$6,1
addiu	$8,$8,2
mflo	$4
addu	$4,$7,$4
.set	noreorder
.set	nomacro
beq	$6,$12,$L173
sh	$4,0($3)
.set	macro
.set	reorder

lb	$5,0($9)
$L180:
sll	$3,$6,3
lh	$4,0($8)
addu	$3,$19,$3
mul	$5,$4,$5
lbu	$3,5664($3)
sll	$3,$3,1
addu	$3,$20,$3
addu	$4,$5,$11
.set	noreorder
.set	nomacro
bgtz	$5,$L178
lhu	$7,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L178
subu	$4,$5,$11
.set	macro
.set	reorder

$L175:
lhu	$3,-30($2)
lhu	$5,16($20)
addu	$3,$3,$5
sh	$3,16($20)
#APP
# 369 "msmpeg4_p0.c" 1
.word	0b01110010100000000100000110101010	#S16LDD XR6,$20,32,PTN0
# 0 "" 2
# 370 "msmpeg4_p0.c" 1
.word	0b01110010100000001000000111101010	#S16LDD XR7,$20,64,PTN0
# 0 "" 2
# 371 "msmpeg4_p0.c" 1
.word	0b01110010100010000110000110101010	#S16LDD XR6,$20,48,PTN1
# 0 "" 2
# 372 "msmpeg4_p0.c" 1
.word	0b01110010100000001100001000101010	#S16LDD XR8,$20,96,PTN0
# 0 "" 2
# 373 "msmpeg4_p0.c" 1
.word	0b01110010100010001010000111101010	#S16LDD XR7,$20,80,PTN1
# 0 "" 2
# 374 "msmpeg4_p0.c" 1
.word	0b01110010100010001110001000101010	#S16LDD XR8,$20,112,PTN1
# 0 "" 2
# 376 "msmpeg4_p0.c" 1
.word	0b01110000100000000000010001010000	#S32LDD XR1,$4,4
# 0 "" 2
# 377 "msmpeg4_p0.c" 1
.word	0b01110000100000000000100010010000	#S32LDD XR2,$4,8
# 0 "" 2
# 378 "msmpeg4_p0.c" 1
.word	0b01110000100000000000110011010000	#S32LDD XR3,$4,12
# 0 "" 2
# 380 "msmpeg4_p0.c" 1
.word	0b01110000000000000101101001001110	#Q16ADD XR9,XR6,XR1,XR0,AA,WW
# 0 "" 2
# 381 "msmpeg4_p0.c" 1
.word	0b01110000000000001001111010001110	#Q16ADD XR10,XR7,XR2,XR0,AA,WW
# 0 "" 2
# 382 "msmpeg4_p0.c" 1
.word	0b01110000000000001110000100001110	#Q16ADD XR4,XR8,XR3,XR0,AA,WW
# 0 "" 2
# 384 "msmpeg4_p0.c" 1
.word	0b01110010100000000100001001101011	#S16STD XR9,$20,32,PTN0
# 0 "" 2
# 385 "msmpeg4_p0.c" 1
.word	0b01110010100000001000001010101011	#S16STD XR10,$20,64,PTN0
# 0 "" 2
# 386 "msmpeg4_p0.c" 1
.word	0b01110010100010000110001001101011	#S16STD XR9,$20,48,PTN1
# 0 "" 2
# 387 "msmpeg4_p0.c" 1
.word	0b01110010100000001100000100101011	#S16STD XR4,$20,96,PTN0
# 0 "" 2
# 388 "msmpeg4_p0.c" 1
.word	0b01110010100010001010001010101011	#S16STD XR10,$20,80,PTN1
# 0 "" 2
# 389 "msmpeg4_p0.c" 1
.word	0b01110010100010001110000100101011	#S16STD XR4,$20,112,PTN1
# 0 "" 2
#NO_APP
$L173:
lh	$3,16($20)
$L275:
sh	$3,2($2)
lh	$3,2($20)
sh	$3,18($2)
#APP
# 478 "msmpeg4_p0.c" 1
.word	0b01110010100000000100000110101010	#S16LDD XR6,$20,32,PTN0
# 0 "" 2
# 479 "msmpeg4_p0.c" 1
.word	0b01110010100000001000000111101010	#S16LDD XR7,$20,64,PTN0
# 0 "" 2
# 480 "msmpeg4_p0.c" 1
.word	0b01110010100010000110000110101010	#S16LDD XR6,$20,48,PTN1
# 0 "" 2
# 481 "msmpeg4_p0.c" 1
.word	0b01110010100000001100001000101010	#S16LDD XR8,$20,96,PTN0
# 0 "" 2
# 482 "msmpeg4_p0.c" 1
.word	0b01110010100010001010000111101010	#S16LDD XR7,$20,80,PTN1
# 0 "" 2
# 483 "msmpeg4_p0.c" 1
.word	0b01110010100010001110001000101010	#S16LDD XR8,$20,112,PTN1
# 0 "" 2
# 485 "msmpeg4_p0.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 486 "msmpeg4_p0.c" 1
.word	0b01110000010000000000100111010001	#S32STD XR7,$2,8
# 0 "" 2
# 487 "msmpeg4_p0.c" 1
.word	0b01110000010000000000111000010001	#S32STD XR8,$2,12
# 0 "" 2
# 489 "msmpeg4_p0.c" 1
.word	0b01110010100000000000010010010000	#S32LDD XR2,$20,4
# 0 "" 2
# 490 "msmpeg4_p0.c" 1
.word	0b01110010100000000000100011010000	#S32LDD XR3,$20,8
# 0 "" 2
# 491 "msmpeg4_p0.c" 1
.word	0b01110010100000000000110100010000	#S32LDD XR4,$20,12
# 0 "" 2
# 493 "msmpeg4_p0.c" 1
.word	0b01110000010000000001010010010001	#S32STD XR2,$2,20
# 0 "" 2
# 494 "msmpeg4_p0.c" 1
.word	0b01110000010000000001100011010001	#S32STD XR3,$2,24
# 0 "" 2
# 495 "msmpeg4_p0.c" 1
.word	0b01110000010000000001110100010001	#S32STD XR4,$2,28
# 0 "" 2
#NO_APP
lw	$2,2824($19)
bne	$2,$0,$L192
.set	noreorder
.set	nomacro
b	$L273
lw	$2,10304($19)
.set	macro
.set	reorder

$L160:
addiu	$3,$3,10
srl	$2,$3,3
andi	$15,$3,0x7
addu	$2,$13,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$5
sll	$2,$2,8
srl	$5,$5,8
and	$8,$5,$11
and	$5,$2,$10
or	$5,$8,$5
sll	$2,$5,16
srl	$5,$5,16
or	$2,$2,$5
sll	$2,$2,$15
srl	$16,$2,$18
addu	$5,$16,$4
sll	$5,$5,2
addu	$5,$12,$5
lh	$4,0($5)
.set	noreorder
.set	nomacro
b	$L161
lb	$18,2($5)
.set	macro
.set	reorder

$L205:
.set	noreorder
.set	nomacro
b	$L132
move	$17,$0
.set	macro
.set	reorder

$L152:
move	$4,$8
addiu	$3,$3,4
.set	noreorder
.set	nomacro
b	$L153
subu	$16,$7,$8
.set	macro
.set	reorder

$L258:
teq	$24,$0,7
div	$0,$2,$24
mflo	$2
bne	$2,$3,$L164
b	$L165
$L105:
subu	$4,$11,$10
sw	$6,40($sp)
sw	$11,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
sw	$10,36($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$10,36($sp)
sw	$2,36($sp)
lw	$25,%call16(abs)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
subu	$4,$10,$16
.set	macro
.set	reorder

lw	$3,36($sp)
lw	$11,44($sp)
lw	$28,24($sp)
slt	$2,$2,$3
lw	$6,40($sp)
movn	$16,$11,$2
sltu	$2,$2,1
sw	$2,32($sp)
$L122:
.set	noreorder
.set	nomacro
bne	$23,$0,$L188
addu	$16,$17,$16
.set	macro
.set	reorder

mul	$2,$16,$22
.set	noreorder
.set	nomacro
bgez	$16,$L126
sh	$2,0($fp)
.set	macro
.set	reorder

lw	$2,2872($19)
li	$5,16			# 0x10
lw	$6,%got($LC1)($28)
move	$7,$21
lw	$25,%call16(av_log)($28)
move	$16,$0
lw	$4,0($19)
addiu	$6,$6,%lo($LC1)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$2,10324($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L193
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L236
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L107:
addu	$16,$17,$11
sw	$0,32($sp)
$L188:
mul	$6,$16,$6
sh	$6,0($fp)
$L271:
bgez	$16,$L127
.set	noreorder
.set	nomacro
b	$L274
lw	$2,2872($19)
.set	macro
.set	reorder

$L74:
lw	$8,%got(v2_dc_chroma_vlc_hw+4)($28)
.set	noreorder
.set	nomacro
b	$L248
lw	$8,%lo(v2_dc_chroma_vlc_hw+4)($8)
.set	macro
.set	reorder

$L85:
lw	$4,%got(ff_msmp4_dc_chroma_vlc_hw)($28)
.set	noreorder
.set	nomacro
b	$L250
addiu	$4,$4,%lo(ff_msmp4_dc_chroma_vlc_hw)
.set	macro
.set	reorder

$L133:
$L135:
slt	$17,$17,4
lw	$12,60($22)
xori	$17,$17,0x1
move	$23,$0
li	$24,1			# 0x1
.set	noreorder
.set	nomacro
b	$L136
move	$9,$0
.set	macro
.set	reorder

$L174:
lw	$4,8036($3)
lw	$3,7996($19)
lw	$5,7992($19)
sll	$4,$4,5
lw	$8,168($19)
.set	noreorder
.set	nomacro
beq	$3,$0,$L181
subu	$4,$2,$4
.set	macro
.set	reorder

mul	$7,$3,$8
lw	$10,2872($19)
addu	$3,$7,$5
subu	$3,$3,$8
addu	$8,$6,$3
lb	$7,0($8)
beq	$10,$7,$L181
addiu	$3,$21,-2
sltu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L181
sra	$11,$10,1
.set	macro
.set	reorder

addiu	$9,$19,5665
addiu	$6,$4,18
.set	noreorder
.set	nomacro
b	$L185
addiu	$5,$4,32
.set	macro
.set	reorder

$L270:
addu	$3,$3,$11
$L184:
teq	$10,$0,7
div	$0,$3,$10
addiu	$6,$6,2
addiu	$9,$9,1
mflo	$3
addu	$3,$7,$3
.set	noreorder
.set	nomacro
beq	$6,$5,$L173
sh	$3,0($4)
.set	macro
.set	reorder

lb	$7,0($8)
$L185:
lh	$3,0($6)
lbu	$4,0($9)
mul	$3,$3,$7
sll	$4,$4,1
addu	$4,$20,$4
.set	noreorder
.set	nomacro
bgtz	$3,$L270
lhu	$7,0($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L184
subu	$3,$3,$11
.set	macro
.set	reorder

$L235:
slt	$2,$2,4
movn	$4,$7,$2
.set	noreorder
.set	nomacro
b	$L99
movn	$8,$7,$2
.set	macro
.set	reorder

$L106:
sw	$6,40($sp)
sw	$11,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
sw	$10,36($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$10,36($sp)
sw	$2,36($sp)
lw	$25,%call16(abs)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
subu	$4,$10,$16
.set	macro
.set	reorder

lw	$3,36($sp)
lw	$11,44($sp)
lw	$28,24($sp)
slt	$2,$3,$2
lw	$6,40($sp)
movz	$16,$11,$2
sltu	$2,$0,$2
.set	noreorder
.set	nomacro
b	$L122
sw	$2,32($sp)
.set	macro
.set	reorder

$L134:
.set	noreorder
.set	nomacro
b	$L135
addiu	$14,$19,8864
.set	macro
.set	reorder

$L260:
lw	$6,%got($LC0)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)
.set	macro
.set	reorder

lw	$28,24($sp)
$L189:
lw	$2,2872($19)
$L274:
li	$5,16			# 0x10
lw	$6,%got($LC1)($28)
move	$7,$21
lw	$25,%call16(av_log)($28)
move	$16,$0
lw	$4,0($19)
addiu	$6,$6,%lo($LC1)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$2,10324($19)
.set	noreorder
.set	nomacro
beq	$2,$0,$L240
lw	$28,24($sp)
.set	macro
.set	reorder

bne	$23,$0,$L129
.set	noreorder
.set	nomacro
b	$L126
lw	$22,2784($19)
.set	macro
.set	reorder

$L166:
lw	$2,7996($19)
li	$5,16			# 0x10
lw	$6,%got($LC5)($28)
lw	$4,0($19)
lw	$7,7992($19)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC5)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L236
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L181:
lhu	$3,18($4)
lhu	$5,2($20)
addu	$3,$3,$5
sh	$3,2($20)
#APP
# 426 "msmpeg4_p0.c" 1
.word	0b01110000100000000001010110010000	#S32LDD XR6,$4,20
# 0 "" 2
# 427 "msmpeg4_p0.c" 1
.word	0b01110000100000000001100111010000	#S32LDD XR7,$4,24
# 0 "" 2
# 428 "msmpeg4_p0.c" 1
.word	0b01110000100000000001111000010000	#S32LDD XR8,$4,28
# 0 "" 2
# 430 "msmpeg4_p0.c" 1
.word	0b01110010100000000000010010010000	#S32LDD XR2,$20,4
# 0 "" 2
# 431 "msmpeg4_p0.c" 1
.word	0b01110010100000000000100011010000	#S32LDD XR3,$20,8
# 0 "" 2
# 432 "msmpeg4_p0.c" 1
.word	0b01110010100000000000110100010000	#S32LDD XR4,$20,12
# 0 "" 2
# 434 "msmpeg4_p0.c" 1
.word	0b01110000000000011000100010001110	#Q16ADD XR2,XR2,XR6,XR0,AA,WW
# 0 "" 2
# 435 "msmpeg4_p0.c" 1
.word	0b01110000000000011100110011001110	#Q16ADD XR3,XR3,XR7,XR0,AA,WW
# 0 "" 2
# 436 "msmpeg4_p0.c" 1
.word	0b01110000000000100001000100001110	#Q16ADD XR4,XR4,XR8,XR0,AA,WW
# 0 "" 2
# 438 "msmpeg4_p0.c" 1
.word	0b01110010100000000000010010010001	#S32STD XR2,$20,4
# 0 "" 2
# 439 "msmpeg4_p0.c" 1
.word	0b01110010100000000000100011010001	#S32STD XR3,$20,8
# 0 "" 2
# 440 "msmpeg4_p0.c" 1
.word	0b01110010100000000000110100010001	#S32STD XR4,$20,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L275
lh	$3,16($20)
.set	macro
.set	reorder

$L267:
lw	$6,%got($LC3)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($19)
lw	$7,2872($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

lw	$2,10324($19)
.set	noreorder
.set	nomacro
bne	$2,$0,$L130
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L236
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L262:
addiu	$11,$9,4
addiu	$10,$8,4
addiu	$7,$4,4
addiu	$9,$9,11
addiu	$8,$8,11
addiu	$4,$4,11
slt	$13,$11,0
movn	$11,$9,$13
slt	$12,$10,0
slt	$9,$7,0
movn	$10,$8,$12
movn	$7,$4,$9
sra	$11,$11,3
sra	$10,$10,3
.set	noreorder
.set	nomacro
b	$L104
sra	$16,$7,3
.set	macro
.set	reorder

$L266:
addiu	$5,$5,9
srl	$4,$5,3
andi	$12,$5,0x7
addu	$4,$9,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($4)  
lwr $11, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$11,8
sll	$11,$11,8
and	$4,$4,$7
and	$11,$11,$6
or	$11,$4,$11
sll	$4,$11,16
srl	$11,$11,16
or	$3,$4,$11
sll	$3,$3,$12
srl	$3,$3,$10
addu	$3,$3,$16
sll	$3,$3,2
addu	$3,$8,$3
lh	$11,2($3)
.set	noreorder
.set	nomacro
bltz	$11,$L81
lh	$16,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L80
move	$10,$11
.set	macro
.set	reorder

$L90:
addiu	$5,$5,9
srl	$4,$5,3
andi	$3,$5,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($4)  
lwr $11, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$11,8
sll	$11,$11,8
and	$4,$4,$7
and	$11,$11,$6
or	$11,$4,$11
sll	$4,$11,16
srl	$11,$11,16
or	$4,$4,$11
sll	$3,$4,$3
srl	$3,$3,$9
addu	$3,$3,$16
sll	$3,$3,2
addu	$3,$10,$3
lh	$11,2($3)
.set	noreorder
.set	nomacro
bltz	$11,$L92
lh	$16,0($3)
.set	macro
.set	reorder

move	$17,$16
.set	noreorder
.set	nomacro
b	$L91
move	$9,$11
.set	macro
.set	reorder

$L269:
move	$8,$4
sll	$5,$5,1
.set	noreorder
.set	nomacro
b	$L153
addiu	$3,$2,1
.set	macro
.set	reorder

$L261:
lw	$5,10340($19)
lw	$3,10332($19)
addiu	$7,$5,8
srl	$4,$5,3
srl	$8,$7,3
addu	$4,$3,$4
addu	$8,$3,$8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
sw	$7,10340($19)
srl	$4,$3,8
sll	$6,$3,8
lbu	$3,0($8)
li	$8,16711680			# 0xff0000
andi	$16,$7,0x7
addiu	$8,$8,255
sll	$16,$3,$16
and	$4,$4,$8
li	$8,-16777216			# 0xffffffffff000000
andi	$16,$16,0x00ff
ori	$8,$8,0xff00
and	$6,$6,$8
or	$4,$4,$6
sll	$3,$4,16
srl	$4,$4,16
srl	$16,$16,7
or	$3,$3,$4
andi	$4,$5,0x7
sll	$3,$3,$4
subu	$17,$0,$16
addiu	$5,$5,9
srl	$3,$3,24
xor	$3,$3,$17
sw	$5,10340($19)
.set	noreorder
.set	nomacro
b	$L84
addu	$17,$3,$16
.set	macro
.set	reorder

$L92:
subu	$5,$5,$9
srl	$4,$5,3
andi	$3,$5,0x7
addu	$8,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $9, 3($8)  
lwr $9, 0($8)  

# 0 "" 2
#NO_APP
srl	$8,$9,8
sll	$9,$9,8
and	$7,$8,$7
and	$6,$9,$6
or	$7,$7,$6
sll	$6,$7,16
srl	$4,$7,16
or	$4,$6,$4
sll	$4,$4,$3
srl	$3,$4,$11
addu	$3,$3,$16
sll	$3,$3,2
addu	$10,$10,$3
lh	$17,0($10)
.set	noreorder
.set	nomacro
b	$L91
lh	$9,2($10)
.set	macro
.set	reorder

$L81:
subu	$5,$5,$10
srl	$4,$5,3
andi	$3,$5,0x7
addu	$9,$9,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($9)  
lwr $4, 0($9)  

# 0 "" 2
#NO_APP
move	$9,$4
sll	$9,$9,8
srl	$4,$4,8
and	$6,$9,$6
and	$7,$4,$7
or	$6,$7,$6
sll	$4,$6,16
srl	$6,$6,16
or	$4,$4,$6
sll	$3,$4,$3
srl	$3,$3,$11
addu	$16,$3,$16
sll	$3,$16,2
addu	$8,$8,$3
lh	$16,0($8)
.set	noreorder
.set	nomacro
b	$L80
lh	$10,2($8)
.set	macro
.set	reorder

$L206:
li	$16,30			# 0x1e
li	$4,2			# 0x2
sll	$5,$5,1
.set	noreorder
.set	nomacro
b	$L153
addiu	$3,$2,1
.set	macro
.set	reorder

$L198:
.set	noreorder
.set	nomacro
b	$L122
sw	$0,32($sp)
.set	macro
.set	reorder

$L117:
sll	$3,$3,3
subu	$8,$9,$8
move	$4,$0
li	$9,8			# 0x8
$L119:
addiu	$7,$8,8
move	$2,$8
$L120:
lbu	$5,0($2)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$7,$L120
addu	$4,$4,$5
.set	macro
.set	reorder

addiu	$9,$9,-1
.set	noreorder
.set	nomacro
bne	$9,$0,$L119
addu	$8,$8,$11
.set	macro
.set	reorder

sra	$2,$3,1
addu	$2,$2,$4
teq	$3,$0,7
div	$0,$2,$3
.set	noreorder
.set	nomacro
b	$L118
mflo	$3
.set	macro
.set	reorder

$L112:
addiu	$10,$9,-8
li	$12,8			# 0x8
move	$4,$0
$L114:
addiu	$8,$10,8
move	$2,$10
$L115:
lbu	$5,0($2)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$8,$2,$L115
addu	$4,$4,$5
.set	macro
.set	reorder

addiu	$12,$12,-1
.set	noreorder
.set	nomacro
bne	$12,$0,$L114
addu	$10,$10,$11
.set	macro
.set	reorder

sra	$2,$16,1
addu	$4,$2,$4
teq	$16,$0,7
div	$0,$4,$16
.set	noreorder
.set	nomacro
b	$L113
mflo	$16
.set	macro
.set	reorder

$L110:
sll	$9,$13,3
lw	$8,2076($5)
lw	$7,7992($19)
move	$11,$2
mul	$5,$2,$9
sll	$4,$7,3
addu	$2,$5,$4
.set	noreorder
.set	nomacro
b	$L111
addu	$9,$8,$2
.set	macro
.set	reorder

$L263:
sw	$6,40($sp)
sw	$11,44($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
sw	$10,36($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
move	$22,$2
lw	$10,36($sp)
lw	$25,%call16(abs)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,abs
1:	jalr	$25
subu	$4,$10,$16
.set	macro
.set	reorder

lw	$11,44($sp)
slt	$2,$22,$2
lw	$28,24($sp)
lw	$6,40($sp)
movz	$16,$11,$2
sltu	$2,$0,$2
sw	$2,32($sp)
addu	$16,$17,$16
$L272:
mul	$6,$16,$6
.set	noreorder
.set	nomacro
b	$L271
sh	$6,0($fp)
.set	macro
.set	reorder

$L196:
li	$2,1			# 0x1
addu	$16,$17,$16
.set	noreorder
.set	nomacro
b	$L272
sw	$2,32($sp)
.set	macro
.set	reorder

$L264:
bne	$21,$0,$L198
move	$16,$3
li	$3,1			# 0x1
addu	$16,$17,$16
.set	noreorder
.set	nomacro
b	$L272
sw	$3,32($sp)
.set	macro
.set	reorder

$L265:
beq	$21,$0,$L201
move	$16,$3
.set	noreorder
.set	nomacro
b	$L122
sw	$2,32($sp)
.set	macro
.set	reorder

$L201:
sw	$0,32($sp)
.set	noreorder
.set	nomacro
b	$L272
addu	$16,$17,$16
.set	macro
.set	reorder

.end	JZC_msmpeg4_decode_block.constprop.5
.size	JZC_msmpeg4_decode_block.constprop.5, .-JZC_msmpeg4_decode_block.constprop.5
.section	.rodata.str1.4
.align	2
$LC6:
.ascii	"cbpc %d invalid at %d %d\012\000"
.align	2
$LC7:
.ascii	"cbpy %d invalid at %d %d\012\000"
.align	2
$LC8:
.ascii	"\012error while decoding block: %d x %d (%d)\012\000"
.section	.text.msmpeg4v12_decode_mb,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	msmpeg4v12_decode_mb
.type	msmpeg4v12_decode_mb, @function
msmpeg4v12_decode_mb:
.frame	$sp,72,$31		# vars= 8, regs= 8/0, args= 24, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-72
lw	$3,2904($4)
li	$2,2			# 0x2
.cprestore	24
sw	$18,48($sp)
move	$18,$5
sw	$17,44($sp)
move	$17,$4
sw	$31,68($sp)
sw	$22,64($sp)
sw	$21,60($sp)
sw	$20,56($sp)
sw	$19,52($sp)
.set	noreorder
.set	nomacro
beq	$3,$2,$L311
sw	$16,40($sp)
.set	macro
.set	reorder

lw	$3,10304($4)
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L312
sw	$4,8004($17)
.set	macro
.set	reorder

lw	$5,10340($17)
li	$8,16711680			# 0xff0000
lw	$4,%got(ms_h263_intra_MCBPC_vlc_hw+4)($28)
li	$6,-16777216			# 0xffffffffff000000
lw	$9,10332($17)
addiu	$8,$8,255
srl	$2,$5,3
ori	$6,$6,0xff00
addu	$2,$9,$2
lw	$10,%lo(ms_h263_intra_MCBPC_vlc_hw+4)($4)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$4,$3,8
srl	$2,$3,8
and	$3,$2,$8
and	$2,$4,$6
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$4,$5,0x7
or	$2,$3,$2
sll	$2,$2,$4
srl	$2,$2,26
sll	$2,$2,2
addu	$2,$10,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L313
lh	$7,0($2)
.set	macro
.set	reorder

$L293:
addu	$3,$3,$5
sw	$3,10340($17)
$L291:
sltu	$2,$7,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L321
lw	$6,%got($LC6)($28)
.set	macro
.set	reorder

lw	$2,10332($17)
$L289:
lw	$3,10304($17)
li	$10,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$10,$L314
lw	$8,10340($17)
.set	macro
.set	reorder

srl	$3,$8,3
sw	$0,2824($17)
lw	$11,2904($17)
addu	$2,$2,$3
lw	$19,%got(dMB)($28)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
lw	$2,%got(ff_ms_h263_cbpy_vlc_hw+4)($28)
sll	$6,$3,8
li	$3,16711680			# 0xff0000
addiu	$3,$3,255
lw	$9,%lo(ff_ms_h263_cbpy_vlc_hw+4)($2)
andi	$2,$8,0x7
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
srl	$2,$2,26
sll	$2,$2,2
addu	$2,$9,$2
lh	$3,2($2)
lh	$22,0($2)
addu	$8,$8,$3
sll	$22,$22,2
sw	$8,10340($17)
.set	noreorder
.set	nomacro
beq	$11,$10,$L315
or	$22,$7,$22
.set	macro
.set	reorder

$L298:
lw	$25,3008($17)
move	$4,$18
move	$16,$0
.set	noreorder
.set	nomacro
jalr	$25
li	$20,5			# 0x5
.set	macro
.set	reorder

andi	$5,$22,0x1
andi	$3,$22,0x2
lw	$28,24($sp)
sll	$5,$5,20
lw	$6,0($19)
sll	$3,$3,15
andi	$2,$22,0x4
or	$4,$5,$3
lw	$19,%got(JZC_msmpeg4_decode_block.constprop.5)($28)
sll	$5,$2,10
andi	$3,$22,0x10
andi	$2,$22,0x8
or	$3,$4,$3
sll	$2,$2,5
andi	$4,$22,0x20
or	$3,$3,$5
sra	$4,$4,5
or	$2,$3,$2
or	$2,$2,$4
addiu	$19,$19,%lo(JZC_msmpeg4_decode_block.constprop.5)
li	$21,6			# 0x6
sw	$2,72($6)
$L301:
subu	$7,$20,$16
move	$5,$18
sra	$7,$22,$7
move	$6,$16
move	$4,$17
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,JZC_msmpeg4_decode_block.constprop.5
1:	jalr	$25
andi	$7,$7,0x1
.set	macro
.set	reorder

addiu	$18,$18,128
.set	noreorder
.set	nomacro
bltz	$2,$L316
lw	$28,24($sp)
.set	macro
.set	reorder

addiu	$16,$16,1
.set	noreorder
.set	nomacro
bne	$16,$21,$L301
move	$2,$0
.set	macro
.set	reorder

$L282:
lw	$31,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

$L311:
lw	$2,10288($4)
.set	noreorder
.set	nomacro
bne	$2,$0,$L317
lw	$3,10340($4)
.set	macro
.set	reorder

lw	$2,10332($4)
$L281:
lw	$5,10304($17)
li	$4,2			# 0x2
.set	noreorder
.set	nomacro
beq	$5,$4,$L318
lw	$5,%got(ms_h263_inter_MCBPC_vlc_hw+4)($28)
.set	macro
.set	reorder

li	$6,16711680			# 0xff0000
srl	$4,$3,3
li	$8,-16777216			# 0xffffffffff000000
addu	$4,$2,$4
lw	$9,%lo(ms_h263_inter_MCBPC_vlc_hw+4)($5)
addiu	$10,$6,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$6,$5,8
ori	$8,$8,0xff00
and	$5,$4,$10
and	$4,$6,$8
or	$4,$5,$4
sll	$5,$4,16
srl	$4,$4,16
andi	$7,$3,0x7
or	$4,$5,$4
sll	$4,$4,$7
srl	$4,$4,25
sll	$4,$4,2
addu	$4,$9,$4
lh	$5,2($4)
.set	noreorder
.set	nomacro
bltz	$5,$L319
lh	$7,0($4)
.set	macro
.set	reorder

$L286:
addu	$3,$5,$3
sw	$3,10340($17)
$L284:
sltu	$3,$7,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L309
sra	$3,$7,2
.set	macro
.set	reorder

andi	$7,$7,0x3
.set	noreorder
.set	nomacro
bne	$3,$0,$L289
sw	$3,8004($17)
.set	macro
.set	reorder

lw	$8,10340($17)
srl	$3,$8,3
addu	$2,$2,$3
lw	$3,%got(ff_ms_h263_cbpy_vlc_hw+4)($28)
lw	$9,%lo(ff_ms_h263_cbpy_vlc_hw+4)($3)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
sll	$6,$3,8
li	$3,16711680			# 0xff0000
andi	$2,$8,0x7
addiu	$3,$3,255
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
srl	$2,$2,26
sll	$2,$2,2
addu	$2,$9,$2
lh	$3,2($2)
lh	$22,0($2)
addu	$8,$8,$3
.set	noreorder
.set	nomacro
bltz	$22,$L320
sw	$8,10340($17)
.set	macro
.set	reorder

lw	$3,10304($17)
sll	$22,$22,2
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$2,$L296
or	$22,$7,$22
.set	macro
.set	reorder

li	$2,3			# 0x3
andi	$3,$22,0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L322
addiu	$2,$sp,32
.set	macro
.set	reorder

$L296:
xori	$22,$22,0x3c
addiu	$2,$sp,32
$L322:
lw	$25,%call16(h263_pred_motion)($28)
move	$6,$0
lw	$19,%got(dMB)($28)
addiu	$7,$sp,36
move	$5,$0
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,h263_pred_motion
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

move	$4,$17
lw	$28,24($sp)
lw	$16,%got(msmpeg4v2_decode_motion.constprop.4)($28)
addiu	$16,$16,%lo(msmpeg4v2_decode_motion.constprop.4)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,msmpeg4v2_decode_motion.constprop.4
1:	jalr	$25
lw	$5,36($sp)
.set	macro
.set	reorder

move	$4,$17
lw	$5,32($sp)
move	$25,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,msmpeg4v2_decode_motion.constprop.4
1:	jalr	$25
sw	$2,36($sp)
.set	macro
.set	reorder

li	$5,1			# 0x1
lw	$4,36($sp)
lw	$3,0($19)
sw	$5,7260($17)
sw	$0,7264($17)
sw	$4,8($3)
sw	$4,7268($17)
sw	$2,12($3)
.set	noreorder
.set	nomacro
b	$L298
sw	$2,7272($17)
.set	macro
.set	reorder

$L312:
lw	$8,10340($17)
lw	$3,10332($17)
srl	$2,$8,3
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sll	$6,$3,8
li	$3,16711680			# 0xff0000
srl	$5,$2,8
lw	$2,%got(v2_intra_cbpc_vlc_hw+4)($28)
addiu	$3,$3,255
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
lw	$7,%lo(v2_intra_cbpc_vlc_hw+4)($2)
andi	$2,$8,0x7
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
srl	$2,$2,29
sll	$2,$2,2
addu	$3,$7,$2
lh	$2,2($3)
lh	$7,0($3)
addu	$2,$8,$2
.set	noreorder
.set	nomacro
b	$L291
sw	$2,10340($17)
.set	macro
.set	reorder

$L317:
lw	$2,10332($4)
srl	$5,$3,3
andi	$6,$3,0x7
addiu	$3,$3,1
addu	$5,$2,$5
lbu	$4,0($5)
sll	$4,$4,$6
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
beq	$4,$0,$L281
sw	$3,10340($17)
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
sw	$0,8004($17)
li	$4,1			# 0x1
sw	$0,7264($17)
move	$2,$0
sw	$3,8680($17)
sw	$3,8684($17)
sw	$3,8688($17)
sw	$3,8692($17)
sw	$3,8696($17)
sw	$3,8700($17)
lw	$3,%got(dMB)($28)
sw	$4,7260($17)
lw	$3,0($3)
sw	$0,8($3)
sw	$0,7268($17)
sw	$0,12($3)
sw	$0,7272($17)
sw	$4,2832($17)
.set	noreorder
.set	nomacro
b	$L282
sw	$0,72($3)
.set	macro
.set	reorder

$L313:
addiu	$5,$5,6
srl	$4,$5,3
andi	$2,$5,0x7
addu	$9,$9,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($9)  
lwr $11, 0($9)  

# 0 "" 2
#NO_APP
srl	$9,$11,8
sll	$11,$11,8
and	$8,$9,$8
and	$6,$11,$6
or	$6,$8,$6
sll	$9,$6,16
srl	$4,$6,16
or	$4,$9,$4
sll	$4,$4,$2
srl	$3,$4,$3
addu	$2,$3,$7
sll	$2,$2,2
addu	$2,$10,$2
lh	$7,0($2)
.set	noreorder
.set	nomacro
b	$L293
lh	$3,2($2)
.set	macro
.set	reorder

$L314:
srl	$4,$8,3
lw	$3,%got(ff_ms_h263_cbpy_vlc_hw+4)($28)
andi	$5,$8,0x7
lw	$19,%got(dMB)($28)
addu	$4,$2,$4
addiu	$8,$8,1
lw	$9,%lo(ff_ms_h263_cbpy_vlc_hw+4)($3)
lbu	$3,0($4)
srl	$4,$8,3
sw	$8,10340($17)
andi	$10,$8,0x7
addu	$4,$2,$4
sll	$3,$3,$5
andi	$3,$3,0x00ff
srl	$3,$3,7
sw	$3,2824($17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($4)  
lwr $3, 0($4)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
sll	$6,$3,8
li	$3,16711680			# 0xff0000
addiu	$3,$3,255
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$10
srl	$2,$2,26
sll	$2,$2,2
addu	$3,$9,$2
lh	$2,2($3)
lh	$22,0($3)
addu	$2,$8,$2
sll	$22,$22,2
sw	$2,10340($17)
.set	noreorder
.set	nomacro
b	$L298
or	$22,$7,$22
.set	macro
.set	reorder

$L316:
lw	$2,7996($17)
li	$5,16			# 0x10
lw	$6,%got($LC8)($28)
lw	$4,0($17)
lw	$7,7992($17)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC8)
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$16,20($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

$L315:
.set	noreorder
.set	nomacro
b	$L298
xori	$22,$22,0x3c
.set	macro
.set	reorder

$L318:
lw	$5,%got(v2_mb_type_vlc_hw+4)($28)
srl	$4,$3,3
li	$7,16711680			# 0xff0000
addu	$4,$2,$4
addiu	$7,$7,255
lw	$6,%lo(v2_mb_type_vlc_hw+4)($5)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$5,$5,$7
or	$4,$4,$5
sll	$5,$4,16
srl	$4,$4,16
andi	$7,$3,0x7
or	$4,$5,$4
sll	$4,$4,$7
srl	$4,$4,25
sll	$4,$4,2
addu	$4,$6,$4
lh	$5,2($4)
lh	$7,0($4)
addu	$3,$3,$5
.set	noreorder
.set	nomacro
b	$L284
sw	$3,10340($17)
.set	macro
.set	reorder

$L319:
addiu	$3,$3,7
srl	$6,$3,3
andi	$12,$3,0x7
addu	$6,$2,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $11, 3($6)  
lwr $11, 0($6)  

# 0 "" 2
#NO_APP
srl	$6,$11,8
sll	$11,$11,8
and	$6,$6,$10
and	$8,$11,$8
or	$8,$6,$8
sll	$6,$8,16
srl	$8,$8,16
or	$4,$6,$8
sll	$4,$4,$12
srl	$4,$4,$5
addu	$7,$4,$7
sll	$4,$7,2
addu	$4,$9,$4
lh	$7,0($4)
.set	noreorder
.set	nomacro
b	$L286
lh	$5,2($4)
.set	macro
.set	reorder

$L309:
lw	$6,%got($LC6)($28)
$L321:
li	$5,16			# 0x10
lw	$3,7992($17)
lw	$2,7996($17)
lw	$4,0($17)
addiu	$6,$6,%lo($LC6)
lw	$25,%call16(av_log)($28)
$L310:
sw	$2,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L282
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L320:
lw	$6,%got($LC7)($28)
li	$5,16			# 0x10
lw	$3,7992($17)
lw	$2,7996($17)
lw	$4,0($17)
addiu	$6,$6,%lo($LC7)
.set	noreorder
.set	nomacro
b	$L310
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

.end	msmpeg4v12_decode_mb
.size	msmpeg4v12_decode_mb, .-msmpeg4v12_decode_mb
.section	.rodata.str1.4
.align	2
$LC9:
.ascii	"illegal MV code at %d %d\012\000"
.section	.text.msmpeg4v34_decode_mb,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	msmpeg4v34_decode_mb
.type	msmpeg4v34_decode_mb, @function
msmpeg4v34_decode_mb:
.frame	$sp,72,$31		# vars= 8, regs= 8/0, args= 24, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,7996($4)
addiu	$sp,$sp,-72
lw	$7,168($4)
lw	$6,7992($4)
lw	$3,2904($4)
mul	$8,$2,$7
sw	$16,40($sp)
.cprestore	24
lw	$16,2192($4)
sw	$31,68($sp)
sw	$22,64($sp)
sw	$21,60($sp)
sw	$20,56($sp)
addu	$2,$8,$6
sw	$19,52($sp)
sw	$18,48($sp)
sll	$2,$2,2
addu	$16,$16,$2
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L383
sw	$17,44($sp)
.set	macro
.set	reorder

lw	$7,10340($4)
li	$3,1			# 0x1
lw	$6,%got(ff_msmp4_mb_i_vlc_hw+4)($28)
li	$9,16711680			# 0xff0000
lw	$11,10332($4)
li	$8,-16777216			# 0xffffffffff000000
srl	$2,$7,3
sw	$3,8004($4)
addiu	$9,$9,255
addu	$2,$11,$2
lw	$10,%lo(ff_msmp4_mb_i_vlc_hw+4)($6)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$6,$3,8
srl	$2,$3,8
ori	$8,$8,0xff00
and	$3,$2,$9
and	$2,$6,$8
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$7,0x7
or	$2,$3,$2
sll	$2,$2,$6
srl	$2,$2,23
sll	$2,$2,2
addu	$2,$10,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L384
lh	$13,0($2)
.set	macro
.set	reorder

$L334:
addu	$3,$3,$7
.set	noreorder
.set	nomacro
bltz	$13,$L382
sw	$3,10340($4)
.set	macro
.set	reorder

addiu	$10,$4,8012
move	$6,$0
move	$18,$0
li	$15,5			# 0x5
li	$14,6			# 0x6
$L338:
subu	$9,$15,$6
slt	$7,$6,4
sra	$3,$13,$9
addiu	$6,$6,1
.set	noreorder
.set	nomacro
beq	$7,$0,$L336
andi	$2,$3,0x1
.set	macro
.set	reorder

lw	$11,0($10)
lw	$12,172($4)
lw	$3,2804($4)
addiu	$7,$11,-1
subu	$8,$11,$12
subu	$7,$7,$12
addu	$8,$3,$8
addu	$7,$3,$7
addu	$3,$3,$11
lbu	$8,0($8)
lbu	$7,0($7)
lbu	$11,-1($3)
xor	$7,$7,$8
movz	$8,$11,$7
xor	$2,$2,$8
sb	$2,0($3)
$L336:
sll	$2,$2,$9
addiu	$10,$10,4
.set	noreorder
.set	nomacro
bne	$6,$14,$L338
or	$18,$18,$2
.set	macro
.set	reorder

lw	$2,8004($4)
$L332:
move	$22,$5
.set	noreorder
.set	nomacro
beq	$2,$0,$L385
move	$17,$4
.set	macro
.set	reorder

lw	$3,10340($4)
lw	$8,10332($4)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,10340($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,2824($17)
li	$2,1			# 0x1
sw	$2,0($16)
lw	$2,10324($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L352
lw	$3,%got(ff_inter_intra_vlc_hw+4)($28)
.set	macro
.set	reorder

lw	$7,10340($17)
srl	$2,$7,3
lw	$9,%lo(ff_inter_intra_vlc_hw+4)($3)
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$5,$3,8
sll	$6,$3,8
li	$3,16711680			# 0xff0000
andi	$2,$7,0x7
addiu	$3,$3,255
and	$4,$5,$3
li	$3,-16777216			# 0xffffffffff000000
ori	$3,$3,0xff00
and	$3,$6,$3
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
srl	$2,$2,29
sll	$2,$2,2
addu	$2,$9,$2
lh	$3,2($2)
lh	$2,0($2)
addu	$7,$7,$3
sw	$2,9884($17)
sw	$7,10340($17)
$L352:
lw	$2,10308($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L351
lw	$19,%got(dMB)($28)
.set	macro
.set	reorder

bne	$18,$0,$L379
.set	noreorder
.set	nomacro
b	$L392
lw	$25,3008($17)
.set	macro
.set	reorder

$L385:
lw	$3,10308($4)
beq	$3,$0,$L340
bne	$18,$0,$L386
$L340:
addiu	$2,$sp,32
lw	$25,%call16(h263_pred_motion)($28)
move	$5,$0
move	$6,$0
addiu	$7,$sp,36
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,h263_pred_motion
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

li	$10,16711680			# 0xff0000
lw	$7,10340($17)
li	$9,-16777216			# 0xffffffffff000000
lw	$8,10332($17)
addiu	$10,$10,255
lw	$6,10272($17)
ori	$9,$9,0xff00
srl	$4,$7,3
lw	$28,24($sp)
andi	$13,$7,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($4)  
lwr $25, 0($4)  

# 0 "" 2
#NO_APP
srl	$3,$25,8
lw	$5,%got(mv_tables)($28)
sll	$4,$25,8
and	$3,$3,$10
and	$4,$4,$9
or	$4,$3,$4
sll	$11,$6,3
sll	$6,$6,5
sll	$3,$4,16
srl	$4,$4,16
addu	$12,$11,$6
or	$2,$3,$4
sll	$2,$2,$13
addu	$3,$5,$12
srl	$2,$2,23
lw	$13,28($3)
sll	$2,$2,2
addu	$2,$13,$2
lh	$4,2($2)
.set	noreorder
.set	nomacro
bltz	$4,$L387
lh	$2,0($2)
.set	macro
.set	reorder

$L343:
addu	$4,$4,$7
.set	noreorder
.set	nomacro
bltz	$2,$L388
sw	$4,10340($17)
.set	macro
.set	reorder

addu	$3,$11,$6
addu	$3,$5,$3
lw	$5,0($3)
beq	$2,$5,$L389
lw	$4,12($3)
lw	$3,16($3)
addu	$4,$4,$2
addu	$2,$3,$2
lbu	$5,0($4)
lbu	$4,0($2)
$L346:
lw	$3,36($sp)
lw	$2,32($sp)
addiu	$3,$3,-32
addiu	$2,$2,-32
addu	$3,$3,$5
addu	$2,$2,$4
slt	$4,$3,-63
.set	noreorder
.set	nomacro
beq	$4,$0,$L347
addiu	$4,$3,-64
.set	macro
.set	reorder

addiu	$3,$3,64
$L348:
slt	$4,$2,-63
.set	noreorder
.set	nomacro
beq	$4,$0,$L349
addiu	$4,$2,-64
.set	macro
.set	reorder

addiu	$2,$2,64
$L350:
lw	$19,%got(dMB)($28)
li	$4,1			# 0x1
sw	$0,7264($17)
sw	$2,32($sp)
sw	$4,7260($17)
lw	$4,0($19)
sw	$3,36($sp)
sw	$3,8($4)
sw	$3,7268($17)
sw	$2,12($4)
sw	$2,7272($17)
li	$2,12296			# 0x3008
sw	$2,0($16)
$L351:
lw	$25,3008($17)
$L392:
move	$4,$22
move	$16,$0
.set	noreorder
.set	nomacro
jalr	$25
li	$20,5			# 0x5
.set	macro
.set	reorder

andi	$5,$18,0x1
andi	$3,$18,0x2
lw	$28,24($sp)
sll	$5,$5,20
lw	$6,0($19)
sll	$3,$3,15
andi	$2,$18,0x4
or	$4,$5,$3
lw	$19,%got(JZC_msmpeg4_decode_block.constprop.5)($28)
sll	$5,$2,10
andi	$3,$18,0x10
andi	$2,$18,0x8
or	$3,$4,$3
sll	$2,$2,5
andi	$4,$18,0x20
or	$3,$3,$5
sra	$4,$4,5
or	$2,$3,$2
or	$2,$2,$4
addiu	$19,$19,%lo(JZC_msmpeg4_decode_block.constprop.5)
li	$21,6			# 0x6
sw	$2,72($6)
$L355:
subu	$7,$20,$16
move	$5,$22
sra	$7,$18,$7
move	$6,$16
move	$4,$17
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,JZC_msmpeg4_decode_block.constprop.5
1:	jalr	$25
andi	$7,$7,0x1
.set	macro
.set	reorder

addiu	$22,$22,128
.set	noreorder
.set	nomacro
bltz	$2,$L390
lw	$28,24($sp)
.set	macro
.set	reorder

addiu	$16,$16,1
.set	noreorder
.set	nomacro
bne	$16,$21,$L355
move	$2,$0
.set	macro
.set	reorder

$L327:
lw	$31,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

$L383:
lw	$2,10288($4)
lw	$7,10340($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L326
lw	$8,10332($4)
.set	macro
.set	reorder

srl	$3,$7,3
andi	$6,$7,0x7
addu	$3,$8,$3
addiu	$7,$7,1
lbu	$2,0($3)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L326
sw	$7,10340($4)
.set	macro
.set	reorder

li	$3,-1			# 0xffffffffffffffff
sw	$0,8004($4)
li	$5,1			# 0x1
sw	$0,7264($4)
move	$2,$0
sw	$3,8680($4)
sw	$3,8684($4)
sw	$3,8688($4)
sw	$3,8692($4)
sw	$3,8696($4)
sw	$3,8700($4)
lw	$3,%got(dMB)($28)
sw	$5,7260($4)
lw	$3,0($3)
sw	$0,8($3)
sw	$0,7268($4)
sw	$0,12($3)
sw	$0,7272($4)
sw	$5,2832($4)
li	$4,14344			# 0x3808
sw	$4,0($16)
.set	noreorder
.set	nomacro
b	$L327
sw	$0,72($3)
.set	macro
.set	reorder

$L326:
lw	$3,%got(ff_mb_non_intra_vlc_hw+52)($28)
srl	$2,$7,3
li	$10,16711680			# 0xff0000
li	$9,-16777216			# 0xffffffffff000000
addu	$2,$8,$2
lw	$11,%lo(ff_mb_non_intra_vlc_hw+52)($3)
addiu	$10,$10,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
sll	$6,$3,8
srl	$2,$3,8
ori	$9,$9,0xff00
and	$3,$2,$10
and	$2,$6,$9
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$7,0x7
or	$2,$3,$2
sll	$2,$2,$6
srl	$2,$2,23
sll	$2,$2,2
addu	$2,$11,$2
lh	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L391
lh	$18,0($2)
.set	macro
.set	reorder

$L329:
addu	$7,$3,$7
.set	noreorder
.set	nomacro
bltz	$18,$L382
sw	$7,10340($4)
.set	macro
.set	reorder

nor	$2,$0,$18
andi	$2,$2,0x40
sra	$2,$2,6
andi	$18,$18,0x3f
.set	noreorder
.set	nomacro
b	$L332
sw	$2,8004($4)
.set	macro
.set	reorder

$L349:
slt	$5,$2,64
.set	noreorder
.set	nomacro
b	$L350
movz	$2,$4,$5
.set	macro
.set	reorder

$L347:
slt	$5,$3,64
.set	noreorder
.set	nomacro
b	$L348
movz	$3,$4,$5
.set	macro
.set	reorder

$L386:
lw	$4,10340($4)
lw	$7,10332($17)
srl	$6,$4,3
andi	$8,$4,0x7
addu	$6,$7,$6
addiu	$5,$4,1
lbu	$3,0($6)
sll	$3,$3,$8
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L341
sw	$5,10340($17)
.set	macro
.set	reorder

srl	$2,$5,3
andi	$5,$5,0x7
addu	$7,$7,$2
addiu	$4,$4,2
lbu	$2,0($7)
sw	$4,10340($17)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,1
$L341:
sw	$2,10276($17)
.set	noreorder
.set	nomacro
b	$L340
sw	$2,10280($17)
.set	macro
.set	reorder

$L379:
lw	$3,10340($17)
srl	$5,$3,3
andi	$6,$3,0x7
addu	$5,$8,$5
addiu	$4,$3,1
lbu	$2,0($5)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L356
sw	$4,10340($17)
.set	macro
.set	reorder

srl	$2,$4,3
andi	$4,$4,0x7
addu	$8,$8,$2
addiu	$3,$3,2
lbu	$2,0($8)
sw	$3,10340($17)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
addiu	$2,$2,1
$L353:
lw	$19,%got(dMB)($28)
sw	$2,10276($17)
.set	noreorder
.set	nomacro
b	$L351
sw	$2,10280($17)
.set	macro
.set	reorder

$L391:
addiu	$7,$7,9
srl	$6,$7,3
andi	$13,$7,0x7
addu	$6,$8,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($6)  
lwr $12, 0($6)  

# 0 "" 2
#NO_APP
srl	$6,$12,8
sll	$12,$12,8
and	$6,$6,$10
and	$12,$12,$9
or	$12,$6,$12
sll	$6,$12,16
srl	$12,$12,16
or	$2,$6,$12
sll	$2,$2,$13
srl	$2,$2,$3
addu	$2,$2,$18
sll	$2,$2,2
addu	$2,$11,$2
lh	$6,2($2)
.set	noreorder
.set	nomacro
bltz	$6,$L330
lh	$18,0($2)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L329
move	$3,$6
.set	macro
.set	reorder

$L384:
addiu	$7,$7,9
srl	$6,$7,3
andi	$2,$7,0x7
addu	$11,$11,$6
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($11)  
lwr $12, 0($11)  

# 0 "" 2
#NO_APP
srl	$11,$12,8
sll	$12,$12,8
and	$9,$11,$9
and	$8,$12,$8
or	$8,$9,$8
sll	$11,$8,16
srl	$6,$8,16
or	$6,$11,$6
sll	$6,$6,$2
srl	$3,$6,$3
addu	$2,$3,$13
sll	$2,$2,2
addu	$2,$10,$2
lh	$13,0($2)
.set	noreorder
.set	nomacro
b	$L334
lh	$3,2($2)
.set	macro
.set	reorder

$L390:
lw	$2,7996($17)
li	$5,16			# 0x10
lw	$6,%got($LC8)($28)
lw	$4,0($17)
lw	$7,7992($17)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC8)
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$16,20($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,72
.set	macro
.set	reorder

$L387:
addiu	$7,$7,9
srl	$12,$7,3
andi	$15,$7,0x7
addu	$12,$8,$12
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $14, 3($12)  
lwr $14, 0($12)  

# 0 "" 2
#NO_APP
srl	$12,$14,8
sll	$14,$14,8
and	$10,$12,$10
and	$9,$14,$9
or	$9,$10,$9
sll	$12,$9,16
srl	$3,$9,16
or	$3,$12,$3
sll	$3,$3,$15
srl	$4,$3,$4
addu	$2,$4,$2
sll	$2,$2,2
addu	$3,$13,$2
lh	$2,0($3)
.set	noreorder
.set	nomacro
b	$L343
lh	$4,2($3)
.set	macro
.set	reorder

$L389:
addiu	$9,$4,6
srl	$3,$4,3
srl	$5,$9,3
li	$2,16711680			# 0xff0000
addiu	$10,$2,255
addu	$2,$8,$3
addu	$8,$8,$5
li	$5,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($2)  
lwr $7, 0($2)  

# 0 "" 2
#NO_APP
sw	$9,10340($17)
srl	$3,$7,8
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($8)  
lwr $6, 0($8)  

# 0 "" 2
#NO_APP
sll	$7,$7,8
srl	$2,$6,8
sll	$6,$6,8
ori	$5,$5,0xff00
and	$3,$3,$10
and	$7,$7,$5
and	$2,$2,$10
and	$5,$6,$5
or	$5,$2,$5
or	$6,$3,$7
sll	$3,$6,16
sll	$2,$5,16
srl	$6,$6,16
srl	$5,$5,16
or	$3,$3,$6
or	$2,$2,$5
andi	$6,$4,0x7
andi	$9,$9,0x7
addiu	$4,$4,12
sll	$3,$3,$6
sll	$2,$2,$9
sw	$4,10340($17)
srl	$5,$3,26
.set	noreorder
.set	nomacro
b	$L346
srl	$4,$2,26
.set	macro
.set	reorder

$L356:
.set	noreorder
.set	nomacro
b	$L353
move	$2,$0
.set	macro
.set	reorder

$L330:
subu	$7,$7,$3
srl	$3,$7,3
andi	$2,$7,0x7
addu	$8,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($8)  
lwr $3, 0($8)  

# 0 "" 2
#NO_APP
srl	$8,$3,8
sll	$3,$3,8
and	$10,$8,$10
and	$8,$3,$9
or	$8,$10,$8
sll	$3,$8,16
srl	$8,$8,16
or	$3,$3,$8
sll	$2,$3,$2
srl	$2,$2,$6
addu	$18,$2,$18
sll	$2,$18,2
addu	$11,$11,$2
lh	$18,0($11)
.set	noreorder
.set	nomacro
b	$L329
lh	$3,2($11)
.set	macro
.set	reorder

$L388:
lw	$2,7996($17)
li	$5,16			# 0x10
lw	$6,%got($LC9)($28)
lw	$4,0($17)
lw	$7,7992($17)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

$L382:
.set	noreorder
.set	nomacro
b	$L327
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.end	msmpeg4v34_decode_mb
.size	msmpeg4v34_decode_mb, .-msmpeg4v34_decode_mb
.local	initialized.6479
.comm	initialized.6479,4,4
.local	table.6553
.comm	table.6553,792,4
.local	table.6552
.comm	table.6552,288,4
.local	table.6551
.comm	table.6551,256,4
.local	table.6550
.comm	table.6550,32,4
.local	table.6549
.comm	table.6549,2144,4
.local	table.6548
.comm	table.6548,9952,4
.local	table.6547
.comm	table.6547,6128,4
.local	table.6546
.comm	table.6546,10592,4
.local	table.6545
.comm	table.6545,6544,4
.local	table.6544
.comm	table.6544,2152,4
.local	table.6543
.comm	table.6543,512,4
.local	table.6542
.comm	table.6542,32,4
.local	table.6541
.comm	table.6541,6024,4
.local	table.6540
.comm	table.6540,5888,4
.local	table.6539
.comm	table.6539,4864,4
.local	table.6538
.comm	table.6538,5904,4
.local	table.6537
.comm	table.6537,4472,4
.local	table.6536
.comm	table.6536,4632,4
.local	table.6535
.comm	table.6535,10776,4
.local	table.6534
.comm	table.6534,14856,4
.local	rl_vlc_table.6529
.comm	rl_vlc_table.6529,70912,4
.local	table.6530
.comm	table.6530,2216,4
.local	rl_vlc_table.6523
.comm	rl_vlc_table.6523,123136,4
.local	table.6524
.comm	table.6524,3848,4
.local	rl_vlc_table.6517
.comm	rl_vlc_table.6517,120320,4
.local	table.6518
.comm	table.6518,3760,4
.local	rl_vlc_table.6511
.comm	rl_vlc_table.6511,70912,4
.local	table.6512
.comm	table.6512,2216,4
.local	rl_vlc_table.6505
.comm	rl_vlc_table.6505,141312,4
.local	table.6506
.comm	table.6506,4416,4
.local	rl_vlc_table.6499
.comm	rl_vlc_table.6499,82176,4
.local	table.6500
.comm	table.6500,2568,4
.local	done.6491
.comm	done.6491,4,4
.globl	msmpeg4v3_decoder
.section	.rodata.str1.4
.align	2
$LC10:
.ascii	"msmpeg4\000"
.align	2
$LC11:
.ascii	"MPEG-4 part 2 Microsoft variant version 3\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	msmpeg4v3_decoder, @object
.size	msmpeg4v3_decoder, 72
msmpeg4v3_decoder:
.word	$LC10
.word	0
.word	17
.word	10584
.word	JZC_msmpeg4_decode_init
.word	0
.word	mpeg4_decode_end
.word	mpeg4_decode_frame
.word	3
.space	12
.word	ff_pixfmt_list_420
.word	$LC11
.space	12
.byte	3
.space	3
.globl	msmpeg4v2_decoder
.section	.rodata.str1.4
.align	2
$LC12:
.ascii	"msmpeg4v2\000"
.align	2
$LC13:
.ascii	"MPEG-4 part 2 Microsoft variant version 2\000"
.section	.data.rel
.align	2
.type	msmpeg4v2_decoder, @object
.size	msmpeg4v2_decoder, 72
msmpeg4v2_decoder:
.word	$LC12
.word	0
.word	16
.word	10584
.word	JZC_msmpeg4_decode_init
.word	0
.word	mpeg4_decode_end
.word	mpeg4_decode_frame
.word	3
.space	12
.word	ff_pixfmt_list_420
.word	$LC13
.space	12
.byte	3
.space	3
.globl	msmpeg4v1_decoder
.section	.rodata.str1.4
.align	2
$LC14:
.ascii	"msmpeg4v1\000"
.align	2
$LC15:
.ascii	"MPEG-4 part 2 Microsoft variant version 1\000"
.section	.data.rel
.align	2
.type	msmpeg4v1_decoder, @object
.size	msmpeg4v1_decoder, 72
msmpeg4v1_decoder:
.word	$LC14
.word	0
.word	15
.word	10584
.word	JZC_msmpeg4_decode_init
.word	0
.word	mpeg4_decode_end
.word	mpeg4_decode_frame
.word	3
.space	12
.word	ff_pixfmt_list_420
.word	$LC15
.space	12
.byte	3
.space	3
.local	ms_h263_inter_MCBPC_vlc_hw
.comm	ms_h263_inter_MCBPC_vlc_hw,16,4
.local	ms_h263_intra_MCBPC_vlc_hw
.comm	ms_h263_intra_MCBPC_vlc_hw,16,4
.local	ff_ms_h263_cbpy_vlc_hw
.comm	ff_ms_h263_cbpy_vlc_hw,16,4
.local	ff_inter_intra_vlc_hw
.comm	ff_inter_intra_vlc_hw,16,4
.local	ff_msmp4_mb_i_vlc_hw
.comm	ff_msmp4_mb_i_vlc_hw,16,4
.local	ff_mb_non_intra_vlc_hw
.comm	ff_mb_non_intra_vlc_hw,64,4
.local	ff_msmp4_dc_chroma_vlc_hw
.comm	ff_msmp4_dc_chroma_vlc_hw,32,4
.local	ff_msmp4_dc_luma_vlc_hw
.comm	ff_msmp4_dc_luma_vlc_hw,32,4
.local	v2_intra_cbpc_vlc_hw
.comm	v2_intra_cbpc_vlc_hw,16,4
.local	v2_mb_type_vlc_hw
.comm	v2_mb_type_vlc_hw,16,4
.local	v2_mv_vlc_hw
.comm	v2_mv_vlc_hw,16,4
.local	v2_dc_chroma_vlc_hw
.comm	v2_dc_chroma_vlc_hw,16,4
.local	v2_dc_lum_vlc_hw
.comm	v2_dc_lum_vlc_hw,16,4
.local	static_rl_table_store
.comm	static_rl_table_store,2340,4
.local	v2_dc_chroma_table
.comm	v2_dc_chroma_table,4096,4
.local	v2_dc_lum_table
.comm	v2_dc_lum_table,4096,4

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
