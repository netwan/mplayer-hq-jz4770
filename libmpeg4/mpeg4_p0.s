.file	1 "mpeg4_p0.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.unlikely.decode_init,"ax",@progbits
.align	2
.set	nomips16
.set	nomicromips
.ent	decode_init
.type	decode_init, @function
decode_init:
.frame	$sp,96,$31		# vars= 0, regs= 7/0, args= 56, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-96
lw	$25,%call16(mpeg4_decode_init)($28)
li	$2,-1			# 0xffffffffffffffff
sw	$16,68($sp)
.cprestore	56
sw	$17,72($sp)
move	$17,$4
sw	$31,92($sp)
sw	$21,88($sp)
sw	$20,84($sp)
sw	$19,80($sp)
sw	$18,76($sp)
lw	$16,136($4)
sw	$2,10216($16)
sw	$2,10212($16)
sw	$2,10192($16)
.reloc	1f,R_MIPS_JALR,mpeg4_decode_init
1:	jalr	$25
sw	$2,10188($16)

bltz	$2,$L2
lw	$28,56($sp)

lw	$2,%got(done.6928)($28)
lw	$3,%lo(done.6928)($2)
bne	$3,$0,$L19
lw	$4,%got(mpeg4_decode_mb_p0)($28)

lw	$18,%got(ff_mpeg4_rl_intra_hw)($28)
li	$3,1			# 0x1
lw	$25,%call16(init_rl)($28)
lw	$5,%got(ff_mpeg4_static_rl_table_store)($28)
sw	$3,%lo(done.6928)($2)
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
move	$4,$18

li	$2,4			# 0x4
lw	$28,56($sp)
li	$3,2			# 0x2
lw	$8,8($18)
li	$5,8			# 0x8
lw	$6,0($18)
addiu	$4,$18,44
sw	$2,16($sp)
sw	$2,28($sp)
addiu	$7,$8,2
sw	$2,48($sp)
addiu	$6,$6,1
lw	$2,%got(table.6931)($28)
lw	$25,%call16(init_vlc_sparse)($28)
sw	$3,20($sp)
addiu	$2,$2,%lo(table.6931)
sw	$8,24($sp)
sw	$3,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$2,48($18)
li	$2,554			# 0x22a
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,56($18)

move	$5,$18
lw	$2,60($18)
bne	$2,$0,$L4
lw	$28,56($sp)

lw	$6,%got(rl_vlc_table.6930)($28)
li	$8,554			# 0x22a
li	$7,128			# 0x80
addiu	$6,$6,%lo(rl_vlc_table.6930)
$L5:
mul	$3,$2,$8
addu	$4,$3,$6
addu	$3,$5,$2
addiu	$2,$2,4
bne	$2,$7,$L5
sw	$4,60($3)

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(ff_mpeg4_rl_intra_hw)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

lw	$28,56($sp)
$L4:
lw	$4,%got(ff_mpeg4_rl_intra)($28)
lw	$5,%got(ff_mpeg4_static_rl_table_store)($28)
lw	$25,%call16(init_rl)($28)
move	$18,$4
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
move	$19,$5

lw	$28,56($sp)
lw	$25,%call16(init_rl)($28)
lw	$4,%got(rvlc_rl_inter)($28)
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
addiu	$5,$19,390

lw	$28,56($sp)
lw	$25,%call16(init_rl)($28)
lw	$4,%got(rvlc_rl_intra)($28)
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
addiu	$5,$19,780

li	$2,4			# 0x4
lw	$28,56($sp)
li	$3,2			# 0x2
lw	$8,8($18)
li	$5,9			# 0x9
lw	$6,0($18)
addiu	$4,$18,44
sw	$2,16($sp)
sw	$2,28($sp)
addiu	$7,$8,2
sw	$2,48($sp)
addiu	$6,$6,1
lw	$2,%got(table.6937)($28)
lw	$25,%call16(init_vlc_sparse)($28)
sw	$3,20($sp)
addiu	$2,$2,%lo(table.6937)
sw	$8,24($sp)
sw	$3,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$2,48($18)
li	$2,554			# 0x22a
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,56($18)

move	$5,$18
lw	$2,60($18)
bne	$2,$0,$L6
lw	$28,56($sp)

lw	$6,%got(rl_vlc_table.6936)($28)
li	$8,554			# 0x22a
li	$7,128			# 0x80
addiu	$6,$6,%lo(rl_vlc_table.6936)
$L7:
mul	$3,$2,$8
addu	$4,$3,$6
addu	$3,$5,$2
addiu	$2,$2,4
bne	$2,$7,$L7
sw	$4,60($3)

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(ff_mpeg4_rl_intra)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

lw	$28,56($sp)
$L6:
li	$2,4			# 0x4
lw	$18,%got(rvlc_rl_inter)($28)
li	$3,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$0,36($sp)
li	$5,9			# 0x9
sw	$2,16($sp)
addiu	$4,$18,44
sw	$2,28($sp)
sw	$2,48($sp)
sw	$3,20($sp)
sw	$3,32($sp)
sw	$0,40($sp)
sw	$0,44($sp)
lw	$2,%got(table.6943)($28)
lw	$3,8($18)
lw	$6,0($18)
addiu	$2,$2,%lo(table.6943)
addiu	$7,$3,2
addiu	$6,$6,1
sw	$2,48($18)
li	$2,1072			# 0x430
sw	$2,56($18)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,24($sp)

move	$5,$18
lw	$2,60($18)
bne	$2,$0,$L8
lw	$28,56($sp)

lw	$6,%got(rl_vlc_table.6942)($28)
li	$8,1072			# 0x430
li	$7,128			# 0x80
addiu	$6,$6,%lo(rl_vlc_table.6942)
$L9:
mul	$3,$2,$8
addu	$4,$3,$6
addu	$3,$5,$2
addiu	$2,$2,4
bne	$2,$7,$L9
sw	$4,60($3)

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(rvlc_rl_inter)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

lw	$28,56($sp)
$L8:
li	$2,4			# 0x4
lw	$18,%got(rvlc_rl_intra)($28)
li	$3,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$0,36($sp)
li	$5,9			# 0x9
sw	$2,16($sp)
addiu	$4,$18,44
sw	$2,28($sp)
sw	$2,48($sp)
sw	$3,20($sp)
sw	$3,32($sp)
sw	$0,40($sp)
sw	$0,44($sp)
lw	$2,%got(table.6949)($28)
lw	$3,8($18)
lw	$6,0($18)
addiu	$2,$2,%lo(table.6949)
addiu	$7,$3,2
addiu	$6,$6,1
sw	$2,48($18)
li	$2,1072			# 0x430
sw	$2,56($18)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,24($sp)

move	$5,$18
lw	$2,60($18)
bne	$2,$0,$L10
lw	$28,56($sp)

lw	$6,%got(rl_vlc_table.6948)($28)
li	$8,1072			# 0x430
li	$7,128			# 0x80
addiu	$6,$6,%lo(rl_vlc_table.6948)
$L11:
mul	$3,$2,$8
addu	$4,$3,$6
addu	$3,$5,$2
addiu	$2,$2,4
bne	$2,$7,$L11
sw	$4,60($3)

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(rvlc_rl_intra)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

lw	$28,56($sp)
$L10:
lw	$3,%got(table.6953)($28)
li	$18,2			# 0x2
lw	$7,%got(ff_mpeg4_DCtab_lum)($28)
li	$20,1			# 0x1
lw	$2,%got(dc_lum)($28)
li	$19,4			# 0x4
addiu	$3,$3,%lo(table.6953)
lw	$25,%call16(init_vlc_sparse)($28)
li	$21,512			# 0x200
sw	$18,16($sp)
addiu	$2,$2,%lo(dc_lum)
sw	$7,24($sp)
li	$5,8			# 0x8
sw	$20,20($sp)
li	$6,10			# 0xa
sw	$18,28($sp)
addiu	$7,$7,1
sw	$20,32($sp)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
sw	$0,44($sp)
sw	$19,48($sp)
sw	$3,4($2)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$21,12($2)

li	$5,8			# 0x8
lw	$28,56($sp)
li	$6,10			# 0xa
sw	$18,16($sp)
sw	$20,20($sp)
sw	$18,28($sp)
lw	$3,%got(ff_mpeg4_DCtab_chrom)($28)
lw	$2,%got(dc_chrom)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$20,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(dc_chrom)
lw	$3,%got(table.6954)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6954)
sw	$0,44($sp)
sw	$19,48($sp)
sw	$21,12($2)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,4($2)

li	$5,6			# 0x6
lw	$28,56($sp)
li	$6,15			# 0xf
sw	$19,16($sp)
sw	$18,20($sp)
sw	$19,28($sp)
lw	$3,%got(sprite_trajectory_tab)($28)
lw	$2,%got(sprite_trajectory)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,2
sw	$18,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(sprite_trajectory)
lw	$3,%got(table.6955)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6955)
sw	$0,44($sp)
sw	$19,48($sp)
sw	$3,4($2)
li	$3,128			# 0x80
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,4			# 0x4
lw	$28,56($sp)
li	$6,4			# 0x4
sw	$18,16($sp)
sw	$20,20($sp)
sw	$18,28($sp)
lw	$3,%got(mb_type_b_tab)($28)
lw	$2,%got(mb_type_b_vlc)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$20,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(mb_type_b_vlc)
lw	$3,%got(table.6956)($28)
sw	$0,36($sp)
move	$4,$2
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6956)
sw	$0,44($sp)
sw	$19,48($sp)
sw	$3,4($2)
li	$3,16			# 0x10
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

lw	$28,56($sp)
lw	$4,%got(mpeg4_decode_mb_p0)($28)
$L19:
li	$3,1			# 0x1
move	$2,$0
sw	$0,10096($16)
sw	$3,32($16)
sw	$4,10528($16)
li	$4,4			# 0x4
sw	$4,9908($16)
sw	$3,900($17)
$L2:
lw	$31,92($sp)
lw	$21,88($sp)
lw	$20,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,96

.set	macro
.set	reorder
.end	decode_init
.size	decode_init, .-decode_init
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"dc<0 at %dx%d\012\000"
.section	.text.mpeg4_pred_dc.isra.13.part.14,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	mpeg4_pred_dc.isra.13.part.14
.type	mpeg4_pred_dc.isra.13.part.14, @function
mpeg4_pred_dc.isra.13.part.14:
.frame	$sp,40,$31		# vars= 0, regs= 1/0, args= 24, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
move	$2,$4
lw	$3,7996($4)
lw	$6,%got($LC0)($28)
addiu	$sp,$sp,-40
lw	$7,7992($2)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($4)
sw	$31,36($sp)
.cprestore	24
sw	$3,16($sp)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC0)

li	$2,-1			# 0xffffffffffffffff
lw	$31,36($sp)
j	$31
addiu	$sp,$sp,40

.set	macro
.set	reorder
.end	mpeg4_pred_dc.isra.13.part.14
.size	mpeg4_pred_dc.isra.13.part.14, .-mpeg4_pred_dc.isra.13.part.14
.section	.text.mpeg4_decode_sprite_trajectory,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_sprite_trajectory
.type	mpeg4_decode_sprite_trajectory, @function
mpeg4_decode_sprite_trajectory:
.frame	$sp,80,$31		# vars= 32, regs= 9/0, args= 0, gp= 8
.mask	0x40ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,10072($4)
li	$24,2			# 0x2
li	$25,16			# 0x10
lw	$7,9972($4)
addiu	$sp,$sp,-80
lw	$8,8($4)
sll	$24,$24,$2
lw	$9,12($4)
sw	$17,48($sp)
li	$17,3			# 0x3
teq	$24,$0,7
div	$0,$25,$24
.cprestore	0
sw	$fp,76($sp)
subu	$17,$17,$2
sw	$23,72($sp)
sw	$22,68($sp)
sw	$21,64($sp)
sw	$20,60($sp)
sw	$19,56($sp)
sw	$18,52($sp)
sw	$16,44($sp)
sw	$0,8($sp)
sw	$0,12($sp)
sw	$0,16($sp)
sw	$0,20($sp)
sw	$0,24($sp)
sw	$0,28($sp)
.set	noreorder
.set	nomacro
blez	$7,$L63
mflo	$25
.set	macro
.set	reorder

lw	$3,%got(sprite_trajectory+4)($28)
li	$13,16711680			# 0xff0000
li	$12,-16777216			# 0xffffffffff000000
lw	$14,0($5)
lw	$2,8($5)
addiu	$11,$sp,8
addiu	$10,$4,9980
lw	$15,%lo(sprite_trajectory+4)($3)
move	$6,$0
addiu	$13,$13,255
li	$18,32			# 0x20
li	$16,500			# 0x1f4
li	$19,413			# 0x19d
ori	$12,$12,0xff00
$L34:
srl	$3,$2,3
andi	$21,$2,0x7
addu	$3,$14,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($3)  
lwr $20, 0($3)  

# 0 "" 2
#NO_APP
srl	$7,$20,8
sll	$20,$20,8
and	$7,$7,$13
and	$20,$20,$12
or	$20,$7,$20
sll	$7,$20,16
srl	$20,$20,16
or	$3,$7,$20
sll	$3,$3,$21
srl	$3,$3,26
sll	$3,$3,2
addu	$3,$15,$3
lh	$7,2($3)
.set	noreorder
.set	nomacro
bltz	$7,$L75
lh	$3,0($3)
.set	macro
.set	reorder

$L25:
addu	$2,$7,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L76
sw	$2,8($5)
.set	macro
.set	reorder

move	$20,$0
move	$7,$0
$L27:
lw	$3,10188($4)
beq	$3,$16,$L77
$L28:
addiu	$2,$2,1
sw	$2,8($5)
srl	$3,$2,3
$L90:
andi	$23,$2,0x7
addu	$3,$14,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 3($3)  
lwr $22, 0($3)  

# 0 "" 2
#NO_APP
srl	$21,$22,8
sll	$22,$22,8
and	$21,$21,$13
and	$22,$22,$12
or	$22,$21,$22
sll	$21,$22,16
srl	$22,$22,16
or	$3,$21,$22
sll	$3,$3,$23
srl	$3,$3,26
sll	$3,$3,2
addu	$3,$15,$3
lh	$21,2($3)
.set	noreorder
.set	nomacro
bltz	$21,$L78
lh	$3,0($3)
.set	macro
.set	reorder

$L31:
addu	$2,$21,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L79
sw	$2,8($5)
.set	macro
.set	reorder

move	$21,$0
move	$3,$0
$L33:
addiu	$2,$2,1
sw	$7,0($11)
addiu	$6,$6,1
sw	$3,4($11)
addiu	$10,$10,4
sw	$2,8($5)
sh	$20,-4($10)
sh	$21,-2($10)
lw	$7,9972($4)
slt	$3,$6,$7
.set	noreorder
.set	nomacro
bne	$3,$0,$L34
addiu	$11,$11,8
.set	macro
.set	reorder

slt	$2,$6,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L80
lw	$3,12($sp)
.set	macro
.set	reorder

lw	$16,8($sp)
lw	$14,12($sp)
lw	$13,20($sp)
lw	$12,24($sp)
lw	$20,16($sp)
lw	$15,28($sp)
addu	$13,$14,$13
addu	$12,$16,$12
$L23:
sll	$2,$6,2
addiu	$2,$2,9982
addu	$2,$4,$2
$L37:
addiu	$6,$6,1
sh	$0,0($2)
sh	$0,-2($2)
slt	$3,$6,4
.set	noreorder
.set	nomacro
bne	$3,$0,$L37
addiu	$2,$2,4
.set	macro
.set	reorder

$L36:
slt	$2,$8,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L66
li	$5,1			# 0x1
.set	macro
.set	reorder

move	$2,$0
addiu	$2,$2,1
$L92:
sll	$10,$5,$2
slt	$3,$10,$8
.set	noreorder
.set	nomacro
bne	$3,$0,$L92
addiu	$2,$2,1
.set	macro
.set	reorder

addiu	$2,$2,-1
sll	$19,$10,4
$L38:
slt	$3,$9,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L67
li	$11,1			# 0x1
.set	macro
.set	reorder

move	$3,$0
addiu	$3,$3,1
$L93:
sll	$5,$11,$3
slt	$6,$5,$9
.set	noreorder
.set	nomacro
bne	$6,$0,$L93
addiu	$3,$3,1
.set	macro
.set	reorder

addiu	$3,$3,-1
sll	$18,$5,4
$L40:
lw	$11,10188($4)
li	$6,500			# 0x1f4
.set	noreorder
.set	nomacro
beq	$11,$6,$L81
li	$6,413			# 0x19d
.set	macro
.set	reorder

sll	$6,$8,1
$L94:
sll	$11,$9,1
addu	$6,$6,$16
addu	$11,$11,$14
sra	$21,$24,1
addu	$20,$6,$20
addu	$11,$11,$15
mul	$16,$21,$16
mul	$14,$21,$14
mul	$22,$21,$20
mul	$13,$13,$21
mul	$12,$12,$21
mul	$11,$21,$11
$L43:
mul	$21,$25,$22
subu	$6,$0,$8
subu	$15,$8,$10
sll	$6,$6,4
mul	$20,$25,$16
addu	$6,$21,$6
mult	$10,$6
madd	$15,$20
mflo	$6
.set	noreorder
.set	nomacro
blez	$6,$L44
sra	$21,$8,1
.set	macro
.set	reorder

addu	$6,$6,$21
teq	$8,$0,7
div	$0,$6,$8
mflo	$6
mul	$13,$25,$13
mul	$22,$25,$14
mult	$10,$13
madd	$15,$22
mflo	$15
.set	noreorder
.set	nomacro
blez	$15,$L46
addu	$6,$6,$19
.set	macro
.set	reorder

$L84:
addu	$15,$15,$21
mul	$12,$25,$12
teq	$8,$0,7
div	$0,$15,$8
subu	$19,$9,$5
mflo	$8
mult	$5,$12
madd	$20,$19
mflo	$12
.set	noreorder
.set	nomacro
blez	$12,$L48
sra	$15,$9,1
.set	macro
.set	reorder

$L85:
mul	$20,$25,$11
subu	$13,$0,$9
sll	$13,$13,4
addu	$13,$20,$13
mult	$5,$13
madd	$22,$19
mflo	$11
.set	noreorder
.set	nomacro
blez	$11,$L50
addu	$12,$12,$15
.set	macro
.set	reorder

$L86:
li	$19,1			# 0x1
.set	noreorder
.set	nomacro
beq	$7,$19,$L53
addu	$11,$11,$15
.set	macro
.set	reorder

$L87:
slt	$13,$7,2
.set	noreorder
.set	nomacro
bne	$13,$0,$L82
li	$13,2			# 0x2
.set	macro
.set	reorder

beq	$7,$13,$L56
li	$13,3			# 0x3
.set	noreorder
.set	nomacro
bne	$7,$13,$L52
move	$15,$2
.set	macro
.set	reorder

teq	$9,$0,7
div	$0,$11,$9
slt	$13,$2,$3
movz	$15,$3,$13
subu	$21,$0,$25
addu	$2,$2,$3
subu	$20,$0,$10
addu	$17,$2,$17
move	$13,$15
sra	$5,$5,$13
sll	$15,$10,1
subu	$17,$17,$13
sra	$10,$10,$13
sll	$3,$20,4
addiu	$2,$17,-1
sw	$17,10028($4)
sll	$13,$14,$17
sll	$2,$19,$2
sll	$23,$16,$17
addiu	$fp,$17,1
addu	$23,$23,$2
addu	$2,$2,$13
sll	$fp,$19,$fp
addiu	$13,$17,2
sw	$23,9996($4)
sw	$2,10000($4)
sw	$13,10032($4)
mflo	$11
teq	$9,$0,7
div	$0,$12,$9
addu	$11,$11,$18
mflo	$9
mul	$15,$15,$5
mul	$12,$16,$21
mul	$21,$14,$21
mul	$3,$3,$5
mul	$15,$15,$25
addu	$6,$6,$12
addu	$9,$9,$12
mul	$6,$6,$5
mul	$9,$9,$10
addu	$8,$8,$21
addu	$11,$11,$21
mul	$5,$8,$5
mul	$8,$16,$15
mul	$10,$11,$10
sw	$6,10012($4)
addu	$2,$6,$9
sw	$9,10016($4)
sw	$5,10020($4)
addu	$2,$8,$2
mul	$8,$14,$15
sw	$10,10024($4)
addu	$25,$5,$10
addu	$2,$2,$3
addu	$2,$2,$fp
addu	$14,$8,$25
sw	$2,10004($4)
addu	$14,$3,$14
addu	$19,$fp,$14
sw	$19,10008($4)
$L58:
sll	$3,$24,$17
beq	$3,$6,$L83
$L60:
li	$3,16			# 0x10
$L95:
lw	$11,10008($4)
lw	$10,10016($4)
subu	$13,$3,$13
lw	$9,10024($4)
lw	$12,10000($4)
subu	$17,$3,$17
sll	$2,$2,$13
sw	$3,10028($4)
sll	$23,$23,$17
sw	$3,10032($4)
sll	$6,$6,$17
sw	$7,9976($4)
sll	$5,$5,$17
sw	$2,10004($4)
sll	$3,$12,$17
sw	$23,9996($4)
sll	$2,$10,$17
sw	$6,10012($4)
sll	$13,$11,$13
sw	$5,10020($4)
sll	$17,$9,$17
sw	$3,10000($4)
sw	$2,10016($4)
sw	$13,10008($4)
sw	$17,10024($4)
$L22:
lw	$fp,76($sp)
lw	$23,72($sp)
lw	$22,68($sp)
lw	$21,64($sp)
lw	$20,60($sp)
lw	$19,56($sp)
lw	$18,52($sp)
lw	$17,48($sp)
lw	$16,44($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L77:
lw	$3,10192($4)
bne	$3,$19,$L28
.set	noreorder
.set	nomacro
b	$L90
srl	$3,$2,3
.set	macro
.set	reorder

$L76:
srl	$7,$2,3
andi	$20,$2,0x7
addu	$7,$14,$7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($7)  
lwr $21, 0($7)  

# 0 "" 2
#NO_APP
srl	$7,$21,8
sll	$21,$21,8
and	$7,$7,$13
and	$21,$21,$12
or	$21,$7,$21
sll	$22,$21,16
srl	$21,$21,16
subu	$23,$18,$3
or	$21,$22,$21
sll	$20,$21,$20
addu	$2,$2,$3
nor	$7,$0,$20
sra	$7,$7,31
sw	$2,8($5)
xor	$20,$7,$20
srl	$20,$20,$23
xor	$20,$20,$7
subu	$7,$20,$7
.set	noreorder
.set	nomacro
b	$L27
andi	$20,$7,0xffff
.set	macro
.set	reorder

$L79:
srl	$21,$2,3
andi	$fp,$2,0x7
addu	$21,$14,$21
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 3($21)  
lwr $22, 0($21)  

# 0 "" 2
#NO_APP
srl	$23,$22,8
sll	$21,$22,8
and	$23,$23,$13
and	$21,$21,$12
or	$21,$23,$21
sll	$22,$21,16
srl	$21,$21,16
subu	$23,$18,$3
or	$22,$22,$21
sll	$21,$22,$fp
addu	$2,$2,$3
nor	$3,$0,$21
sra	$3,$3,31
sw	$2,8($5)
xor	$21,$3,$21
srl	$21,$21,$23
xor	$21,$21,$3
subu	$3,$21,$3
.set	noreorder
.set	nomacro
b	$L33
andi	$21,$3,0xffff
.set	macro
.set	reorder

$L75:
addiu	$2,$2,6
srl	$20,$2,3
andi	$23,$2,0x7
addu	$20,$14,$20
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 3($20)  
lwr $22, 0($20)  

# 0 "" 2
#NO_APP
srl	$21,$22,8
sll	$22,$22,8
and	$21,$21,$13
and	$22,$22,$12
or	$22,$21,$22
sll	$21,$22,16
srl	$22,$22,16
or	$20,$21,$22
sll	$20,$20,$23
srl	$20,$20,$7
addu	$3,$20,$3
sll	$3,$3,2
addu	$3,$15,$3
lh	$22,2($3)
.set	noreorder
.set	nomacro
bltz	$22,$L26
lh	$3,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L25
move	$7,$22
.set	macro
.set	reorder

$L78:
addiu	$2,$2,6
srl	$22,$2,3
andi	$fp,$2,0x7
addu	$22,$14,$22
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $23, 3($22)  
lwr $23, 0($22)  

# 0 "" 2
#NO_APP
move	$22,$23
sll	$22,$22,8
srl	$23,$23,8
and	$22,$22,$12
and	$23,$23,$13
or	$22,$23,$22
sll	$23,$22,16
srl	$22,$22,16
or	$22,$23,$22
sll	$22,$22,$fp
srl	$22,$22,$21
addu	$3,$22,$3
sll	$3,$3,2
addu	$3,$15,$3
lh	$23,2($3)
.set	noreorder
.set	nomacro
bltz	$23,$L32
lh	$3,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L31
move	$21,$23
.set	macro
.set	reorder

$L56:
subu	$5,$0,$25
sll	$3,$10,1
addu	$17,$2,$17
mul	$25,$3,$25
mul	$3,$16,$5
addiu	$13,$17,2
sw	$17,10028($4)
sw	$13,10032($4)
addu	$6,$3,$6
mul	$3,$14,$5
sw	$6,10012($4)
sw	$6,10024($4)
addu	$5,$3,$8
subu	$8,$22,$8
subu	$3,$0,$10
addu	$2,$8,$6
sw	$5,10020($4)
sw	$8,10016($4)
mul	$8,$16,$25
addu	$9,$6,$5
sll	$10,$3,4
addiu	$3,$17,1
sll	$16,$16,$17
addu	$2,$8,$2
mul	$8,$14,$25
addu	$2,$2,$10
sll	$14,$14,$17
addu	$25,$8,$9
addiu	$8,$17,-1
addu	$10,$10,$25
sll	$8,$19,$8
sll	$19,$19,$3
addu	$23,$16,$8
addu	$2,$2,$19
addu	$8,$8,$14
addu	$19,$19,$10
sw	$23,9996($4)
sw	$2,10004($4)
sw	$8,10000($4)
.set	noreorder
.set	nomacro
b	$L58
sw	$19,10008($4)
.set	macro
.set	reorder

$L82:
bne	$7,$0,$L52
sw	$0,10016($4)
move	$2,$0
sw	$0,9996($4)
sw	$0,10000($4)
sw	$0,10004($4)
sw	$0,10008($4)
sw	$24,10012($4)
sw	$0,10020($4)
sw	$24,10024($4)
sw	$0,10028($4)
sw	$0,10032($4)
lw	$13,10016($4)
.set	noreorder
.set	nomacro
beq	$13,$0,$L69
move	$23,$0
.set	macro
.set	reorder

$L88:
move	$6,$24
move	$13,$0
move	$5,$0
.set	noreorder
.set	nomacro
b	$L60
move	$17,$0
.set	macro
.set	reorder

$L32:
subu	$2,$2,$21
srl	$21,$2,3
andi	$22,$2,0x7
addu	$21,$14,$21
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($21)  
lwr $fp, 0($21)  

# 0 "" 2
#NO_APP
move	$21,$fp
sll	$21,$21,8
srl	$fp,$fp,8
and	$21,$21,$12
and	$fp,$fp,$13
or	$21,$fp,$21
sll	$fp,$21,16
srl	$21,$21,16
or	$21,$fp,$21
sll	$22,$21,$22
srl	$21,$22,$23
addu	$21,$21,$3
sll	$21,$21,2
addu	$21,$15,$21
lh	$3,0($21)
.set	noreorder
.set	nomacro
b	$L31
lh	$21,2($21)
.set	macro
.set	reorder

$L26:
subu	$2,$2,$7
srl	$20,$2,3
andi	$7,$2,0x7
addu	$20,$14,$20
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($20)  
lwr $21, 0($20)  

# 0 "" 2
#NO_APP
srl	$20,$21,8
sll	$23,$21,8
and	$20,$20,$13
and	$23,$23,$12
or	$23,$20,$23
sll	$21,$23,16
srl	$20,$23,16
or	$20,$21,$20
sll	$20,$20,$7
srl	$7,$20,$22
addu	$7,$7,$3
sll	$7,$7,2
addu	$7,$15,$7
lh	$3,0($7)
.set	noreorder
.set	nomacro
b	$L25
lh	$7,2($7)
.set	macro
.set	reorder

$L83:
lw	$8,10016($4)
bne	$8,$0,$L60
bne	$5,$0,$L60
.set	noreorder
.set	nomacro
b	$L91
lw	$5,10024($4)
.set	macro
.set	reorder

$L81:
lw	$11,10192($4)
.set	noreorder
.set	nomacro
bne	$11,$6,$L94
sll	$6,$8,1
.set	macro
.set	reorder

mul	$11,$24,$8
mul	$21,$24,$9
addu	$6,$11,$16
addu	$11,$21,$14
addu	$22,$6,$20
.set	noreorder
.set	nomacro
b	$L43
addu	$11,$11,$15
.set	macro
.set	reorder

$L44:
subu	$6,$6,$21
teq	$8,$0,7
div	$0,$6,$8
mflo	$6
mul	$13,$25,$13
mul	$22,$25,$14
mult	$10,$13
madd	$15,$22
mflo	$15
.set	noreorder
.set	nomacro
bgtz	$15,$L84
addu	$6,$6,$19
.set	macro
.set	reorder

$L46:
subu	$15,$15,$21
mul	$12,$25,$12
teq	$8,$0,7
div	$0,$15,$8
subu	$19,$9,$5
mflo	$8
mult	$5,$12
madd	$20,$19
mflo	$12
.set	noreorder
.set	nomacro
bgtz	$12,$L85
sra	$15,$9,1
.set	macro
.set	reorder

$L48:
mul	$20,$25,$11
subu	$13,$0,$9
sll	$13,$13,4
addu	$13,$20,$13
mult	$5,$13
madd	$22,$19
mflo	$11
.set	noreorder
.set	nomacro
bgtz	$11,$L86
subu	$12,$12,$15
.set	macro
.set	reorder

$L50:
li	$19,1			# 0x1
.set	noreorder
.set	nomacro
bne	$7,$19,$L87
subu	$11,$11,$15
.set	macro
.set	reorder

$L53:
sra	$3,$14,1
sw	$0,10016($4)
sra	$2,$16,1
sw	$14,10000($4)
andi	$5,$16,0x1
sw	$16,9996($4)
andi	$14,$14,0x1
sw	$24,10012($4)
sw	$0,10020($4)
or	$2,$5,$2
sw	$24,10024($4)
or	$14,$14,$3
sw	$2,10004($4)
sw	$14,10008($4)
sw	$0,10028($4)
sw	$0,10032($4)
lw	$13,10016($4)
.set	noreorder
.set	nomacro
bne	$13,$0,$L88
move	$23,$16
.set	macro
.set	reorder

$L69:
move	$17,$0
move	$3,$24
move	$6,$24
lw	$5,10024($4)
$L91:
.set	noreorder
.set	nomacro
beq	$3,$5,$L89
move	$5,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L95
li	$3,16			# 0x10
.set	macro
.set	reorder

$L52:
lw	$6,10012($4)
lw	$17,10028($4)
lw	$5,10020($4)
lw	$23,9996($4)
lw	$2,10004($4)
.set	noreorder
.set	nomacro
b	$L58
lw	$13,10032($4)
.set	macro
.set	reorder

$L80:
lw	$2,8($sp)
lw	$13,20($sp)
lw	$12,24($sp)
move	$16,$2
lw	$20,16($sp)
move	$14,$3
lw	$15,28($sp)
addu	$13,$3,$13
.set	noreorder
.set	nomacro
b	$L36
addu	$12,$2,$12
.set	macro
.set	reorder

$L63:
move	$15,$0
move	$12,$0
move	$13,$0
move	$20,$0
move	$14,$0
move	$16,$0
.set	noreorder
.set	nomacro
b	$L23
move	$6,$0
.set	macro
.set	reorder

$L67:
li	$18,16			# 0x10
li	$5,1			# 0x1
.set	noreorder
.set	nomacro
b	$L40
move	$3,$0
.set	macro
.set	reorder

$L66:
li	$19,16			# 0x10
li	$10,1			# 0x1
.set	noreorder
.set	nomacro
b	$L38
move	$2,$0
.set	macro
.set	reorder

$L89:
sra	$2,$2,$13
lw	$5,10000($4)
lw	$3,10008($4)
sra	$23,$23,$17
sw	$24,10012($4)
sra	$17,$5,$17
sw	$2,10004($4)
sra	$13,$3,$13
sw	$23,9996($4)
li	$2,1			# 0x1
sw	$0,10016($4)
sw	$17,10000($4)
sw	$13,10008($4)
sw	$0,10020($4)
sw	$24,10024($4)
sw	$0,10028($4)
sw	$0,10032($4)
.set	noreorder
.set	nomacro
b	$L22
sw	$2,9976($4)
.set	macro
.set	reorder

.end	mpeg4_decode_sprite_trajectory
.size	mpeg4_decode_sprite_trajectory, .-mpeg4_decode_sprite_trajectory
.section	.rodata.str1.4
.align	2
$LC1:
.ascii	"DivX%dBuild%d%c\000"
.align	2
$LC2:
.ascii	"DivX%db%d%c\000"
.align	2
$LC3:
.ascii	"Invalid and inefficient vfw-avi packed B frames detected"
.ascii	"\012\000"
.align	2
$LC4:
.ascii	"FFmpe%*[^b]b%d\000"
.align	2
$LC5:
.ascii	"FFmpeg v%d.%d.%d / libavcodec build: %d\000"
.align	2
$LC6:
.ascii	"Lavc%d.%d.%d\000"
.align	2
$LC7:
.ascii	"ffmpeg\000"
.align	2
$LC8:
.ascii	"XviD%d\000"
.section	.text.decode_user_data,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_user_data
.type	decode_user_data, @function
decode_user_data:
.frame	$sp,344,$31		# vars= 280, regs= 8/0, args= 24, gp= 8
.mask	0x807f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-344
lw	$7,8($5)
li	$12,16711680			# 0xff0000
lw	$13,12($5)
li	$11,-16777216			# 0xffffffffff000000
.cprestore	24
sw	$17,316($sp)
move	$8,$0
sw	$16,312($sp)
addiu	$17,$sp,32
sw	$31,340($sp)
move	$16,$4
sw	$22,336($sp)
addiu	$12,$12,255
sw	$21,332($sp)
ori	$11,$11,0xff00
sw	$20,328($sp)
li	$14,255			# 0xff
sw	$19,324($sp)
sw	$18,320($sp)
sw	$0,300($sp)
sw	$0,296($sp)
sw	$0,292($sp)
.set	noreorder
.set	nomacro
b	$L97
sw	$0,288($sp)
.set	macro
.set	reorder

$L99:
lw	$3,0($5)
move	$7,$9
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$6,8
sll	$6,$6,8
and	$3,$3,$12
and	$6,$6,$11
or	$6,$3,$6
sll	$3,$6,16
srl	$2,$6,16
or	$2,$3,$2
sll	$2,$2,$4
srl	$3,$2,24
srl	$2,$2,9
beq	$2,$0,$L98
addiu	$8,$8,1
sw	$9,8($5)
.set	noreorder
.set	nomacro
beq	$8,$14,$L98
sb	$3,0($10)
.set	macro
.set	reorder

$L97:
srl	$2,$7,3
andi	$4,$7,0x7
addiu	$9,$7,8
slt	$7,$7,$13
.set	noreorder
.set	nomacro
bne	$7,$0,$L99
addu	$10,$17,$8
.set	macro
.set	reorder

$L98:
lw	$5,%got($LC1)($28)
addiu	$20,$sp,304
lw	$25,%call16(sscanf)($28)
addu	$2,$17,$8
addiu	$19,$sp,300
addiu	$18,$sp,296
sw	$20,16($sp)
addiu	$5,$5,%lo($LC1)
sb	$0,0($2)
move	$4,$17
move	$6,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
move	$7,$18
.set	macro
.set	reorder

slt	$3,$2,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L121
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$3,300($sp)
$L123:
sw	$3,10188($16)
lw	$3,296($sp)
sw	$3,10192($16)
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L122
lb	$3,304($sp)
.set	macro
.set	reorder

$L102:
sw	$0,10196($16)
$L101:
lw	$5,%got($LC4)($28)
move	$4,$17
lw	$25,%call16(sscanf)($28)
move	$6,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
addiu	$5,$5,%lo($LC4)
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
beq	$2,$3,$L104
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$5,%got($LC5)($28)
addiu	$22,$sp,288
lw	$25,%call16(sscanf)($28)
addiu	$21,$sp,292
sw	$18,20($sp)
move	$4,$17
addiu	$5,$5,%lo($LC5)
sw	$22,16($sp)
move	$6,$19
move	$7,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
li	$20,4			# 0x4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$20,$L104
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$5,%got($LC6)($28)
move	$4,$17
lw	$25,%call16(sscanf)($28)
move	$6,$19
sw	$22,16($sp)
move	$7,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
addiu	$5,$5,%lo($LC6)
.set	macro
.set	reorder

addiu	$2,$2,1
slt	$3,$2,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L108
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$3,300($sp)
sll	$4,$3,16
lw	$3,292($sp)
sll	$3,$3,8
addu	$3,$4,$3
lw	$4,288($sp)
addu	$3,$3,$4
.set	noreorder
.set	nomacro
beq	$2,$20,$L109
sw	$3,296($sp)
.set	macro
.set	reorder

$L108:
lw	$5,%got($LC7)($28)
move	$4,$17
lw	$25,%call16(strcmp)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,strcmp
1:	jalr	$25
addiu	$5,$5,%lo($LC7)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L106
lw	$28,24($sp)
.set	macro
.set	reorder

$L113:
lw	$5,%got($LC8)($28)
move	$4,$17
lw	$25,%call16(sscanf)($28)
move	$6,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
addiu	$5,$5,%lo($LC8)
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
bne	$2,$3,$L124
lw	$31,340($sp)
.set	macro
.set	reorder

lw	$2,296($sp)
sw	$2,10212($16)
lw	$31,340($sp)
$L124:
move	$2,$0
lw	$22,336($sp)
lw	$21,332($sp)
lw	$20,328($sp)
lw	$19,324($sp)
lw	$18,320($sp)
lw	$17,316($sp)
lw	$16,312($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,344
.set	macro
.set	reorder

$L106:
li	$2,4600			# 0x11f8
.set	noreorder
.set	nomacro
b	$L113
sw	$2,10216($16)
.set	macro
.set	reorder

$L122:
li	$2,112			# 0x70
bne	$3,$2,$L102
li	$20,1			# 0x1
lw	$2,9872($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L101
sw	$20,10196($16)
.set	macro
.set	reorder

lw	$6,%got($LC3)($28)
li	$5,24			# 0x18
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L101
sw	$20,9872($16)
.set	macro
.set	reorder

$L104:
lw	$3,296($sp)
$L109:
.set	noreorder
.set	nomacro
b	$L113
sw	$3,10216($16)
.set	macro
.set	reorder

$L121:
lw	$5,%got($LC2)($28)
move	$4,$17
lw	$25,%call16(sscanf)($28)
move	$6,$19
sw	$20,16($sp)
move	$7,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,sscanf
1:	jalr	$25
addiu	$5,$5,%lo($LC2)
.set	macro
.set	reorder

slt	$3,$2,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L101
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L123
lw	$3,300($sp)
.set	macro
.set	reorder

.end	decode_user_data
.size	decode_user_data, .-decode_user_data
.section	.rodata.str1.4
.align	2
$LC9:
.ascii	"dc overflow at %dx%d\012\000"
.align	2
$LC10:
.ascii	"|level| overflow in 3. esc, qp=%d\012\000"
.align	2
$LC11:
.ascii	"ac-tex damaged at %d %d\012\000"
.align	2
$LC12:
.ascii	"texture corrupted at %d %d %d\012\000"
.section	.text.mpeg4_decode_partitioned_mb,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_partitioned_mb
.type	mpeg4_decode_partitioned_mb, @function
mpeg4_decode_partitioned_mb:
.frame	$sp,96,$31		# vars= 24, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-96
lw	$3,168($4)
lw	$2,7992($4)
sw	$19,68($sp)
move	$19,$5
lw	$5,7996($4)
lw	$6,2192($4)
lw	$7,2844($4)
mul	$9,$5,$3
lw	$3,10108($4)
sw	$21,76($sp)
sw	$16,56($sp)
sw	$20,72($sp)
sw	$17,60($sp)
.cprestore	24
sw	$31,92($sp)
addu	$8,$9,$2
sw	$fp,88($sp)
sw	$23,84($sp)
sw	$22,80($sp)
sw	$8,32($sp)
lw	$21,32($sp)
lw	$8,2872($4)
lw	$2,2172($4)
sll	$16,$21,2
sw	$18,64($sp)
addu	$7,$7,$21
addu	$6,$6,$16
slt	$3,$8,$3
addu	$2,$2,$21
lbu	$18,0($7)
lw	$20,0($6)
sw	$3,10112($4)
lb	$5,0($2)
.set	noreorder
.set	nomacro
beq	$8,$5,$L126
move	$17,$4
.set	macro
.set	reorder

lw	$25,%call16(ff_set_qscale)($28)
.reloc	1f,R_MIPS_JALR,ff_set_qscale
1:	jalr	$25
lw	$28,24($sp)
$L126:
lw	$6,2904($17)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$6,$2,$L127
li	$2,4			# 0x4
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$2,$L127
andi	$2,$20,0x800
.set	macro
.set	reorder

lw	$3,2192($17)
addu	$16,$3,$16
li	$3,1			# 0x1
sw	$3,8004($17)
lw	$3,0($16)
andi	$3,$3,0x200
.set	noreorder
.set	nomacro
beq	$2,$0,$L345
sw	$3,2824($17)
.set	macro
.set	reorder

$L269:
lw	$2,9808($17)
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
blez	$2,$L346
sw	$2,9808($17)
.set	macro
.set	reorder

$L245:
#APP
# 610 "mpeg4_p0.c" 1
mfc0	$3,$21,3
# 0 "" 2
#NO_APP
srl	$3,$3,5
#APP
# 611 "mpeg4_p0.c" 1
mfc0	$2,$21,2
# 0 "" 2
#NO_APP
andi	$5,$3,0x1f
lw	$4,88($17)
andi	$7,$3,0x7
lw	$3,%got(mpeg4_hw_bs_buffer)($28)
srl	$5,$5,3
lw	$9,10332($17)
andi	$4,$4,0x10
lw	$6,0($3)
subu	$2,$2,$6
addu	$2,$2,$5
sll	$2,$2,3
addu	$7,$2,$7
srl	$2,$7,3
sw	$7,10340($17)
addu	$2,$9,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$4,$0,$L266
sll	$2,$3,8
.set	macro
.set	reorder

li	$11,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
addiu	$11,$11,255
srl	$3,$3,8
ori	$10,$10,0xff00
and	$2,$2,$10
and	$3,$3,$11
or	$3,$2,$3
srl	$4,$3,16
sll	$2,$3,16
andi	$3,$7,0x7
or	$2,$2,$4
sll	$3,$2,$3
srl	$3,$3,16
sltu	$4,$3,256
.set	noreorder
.set	nomacro
beq	$4,$0,$L256
move	$2,$3
.set	macro
.set	reorder

lw	$8,2904($17)
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$8,$4,$L256
li	$4,1			# 0x1
.set	macro
.set	reorder

li	$12,8			# 0x8
subu	$12,$12,$8
sra	$3,$3,$12
bne	$3,$4,$L256
lw	$3,10084($17)
bne	$3,$0,$L256
addiu	$8,$8,8
move	$6,$7
.set	noreorder
.set	nomacro
b	$L257
li	$13,1			# 0x1
.set	macro
.set	reorder

$L258:
bne	$4,$13,$L256
$L257:
addu	$6,$8,$6
addu	$7,$7,$8
srl	$3,$6,3
sw	$6,10340($17)
andi	$5,$6,0x7
addu	$3,$9,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$11
and	$4,$4,$10
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$5
srl	$2,$2,16
sltu	$3,$2,256
.set	noreorder
.set	nomacro
bne	$3,$0,$L258
sra	$4,$2,$12
.set	macro
.set	reorder

$L256:
lw	$4,10344($17)
addiu	$3,$7,8
slt	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L259
andi	$3,$7,0x7
.set	macro
.set	reorder

nor	$3,$0,$7
li	$4,127			# 0x7f
andi	$3,$3,0x7
sra	$2,$2,8
sra	$3,$4,$3
or	$2,$3,$2
beq	$2,$4,$L260
$L266:
move	$2,$0
$L326:
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L127:
addiu	$21,$17,8012
lw	$8,2184($17)
addiu	$3,$17,7268
addiu	$7,$17,7300
move	$4,$21
$L129:
lw	$2,0($4)
addiu	$3,$3,8
addiu	$4,$4,4
sll	$2,$2,2
addu	$2,$8,$2
lh	$5,0($2)
lh	$2,2($2)
sw	$5,-8($3)
.set	noreorder
.set	nomacro
bne	$3,$7,$L129
sw	$2,-4($3)
.set	macro
.set	reorder

andi	$3,$20,0x7
andi	$2,$20,0x800
.set	noreorder
.set	nomacro
beq	$2,$0,$L130
sw	$3,8004($17)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
sw	$0,7264($17)
li	$3,1			# 0x1
sw	$2,8680($17)
sw	$2,8684($17)
sw	$2,8688($17)
sw	$2,8692($17)
sw	$2,8696($17)
sw	$2,8700($17)
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$6,$2,$L347
sw	$3,7260($17)
.set	macro
.set	reorder

li	$2,1			# 0x1
$L389:
sw	$0,10036($17)
sw	$2,2832($17)
lw	$2,9808($17)
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
bgtz	$2,$L245
sw	$2,9808($17)
.set	macro
.set	reorder

$L346:
#APP
# 610 "mpeg4_p0.c" 1
mfc0	$3,$21,3
# 0 "" 2
#NO_APP
srl	$3,$3,5
#APP
# 611 "mpeg4_p0.c" 1
mfc0	$2,$21,2
# 0 "" 2
#NO_APP
andi	$5,$3,0x1f
lw	$4,88($17)
andi	$7,$3,0x7
lw	$3,%got(mpeg4_hw_bs_buffer)($28)
srl	$5,$5,3
lw	$9,10332($17)
andi	$4,$4,0x10
lw	$6,0($3)
subu	$2,$2,$6
addu	$2,$2,$5
sll	$2,$2,3
addu	$7,$2,$7
srl	$2,$7,3
sw	$7,10340($17)
addu	$2,$9,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$4,$0,$L246
sll	$2,$3,8
.set	macro
.set	reorder

li	$13,16711680			# 0xff0000
li	$10,-16777216			# 0xffffffffff000000
addiu	$13,$13,255
srl	$3,$3,8
ori	$10,$10,0xff00
and	$2,$2,$10
and	$3,$3,$13
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
andi	$4,$7,0x7
or	$2,$2,$3
sll	$3,$2,$4
srl	$3,$3,16
sltu	$4,$3,256
.set	noreorder
.set	nomacro
beq	$4,$0,$L247
move	$2,$3
.set	macro
.set	reorder

lw	$8,2904($17)
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$8,$4,$L247
li	$4,1			# 0x1
.set	macro
.set	reorder

li	$11,8			# 0x8
subu	$11,$11,$8
sra	$3,$3,$11
bne	$3,$4,$L247
lw	$3,10084($17)
bne	$3,$0,$L247
addiu	$8,$8,8
move	$6,$7
.set	noreorder
.set	nomacro
b	$L248
li	$12,1			# 0x1
.set	macro
.set	reorder

$L249:
bne	$4,$12,$L247
$L248:
addu	$6,$8,$6
addu	$7,$7,$8
srl	$3,$6,3
sw	$6,10340($17)
andi	$5,$6,0x7
addu	$3,$9,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$13
and	$4,$4,$10
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$5
srl	$2,$2,16
sltu	$3,$2,256
.set	noreorder
.set	nomacro
bne	$3,$0,$L249
sra	$4,$2,$11
.set	macro
.set	reorder

$L247:
lw	$4,10344($17)
addiu	$3,$7,8
slt	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L250
andi	$3,$7,0x7
.set	macro
.set	reorder

nor	$3,$0,$7
li	$4,127			# 0x7f
andi	$3,$3,0x7
sra	$2,$2,8
sra	$3,$4,$3
or	$2,$3,$2
.set	noreorder
.set	nomacro
beq	$2,$4,$L326
li	$2,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

$L246:
li	$2,-3			# 0xfffffffffffffffd
$L376:
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L130:
.set	noreorder
.set	nomacro
beq	$3,$0,$L133
andi	$2,$20,0x40
.set	macro
.set	reorder

lw	$2,2192($17)
addu	$16,$2,$16
lw	$2,0($16)
andi	$2,$2,0x200
sw	$2,2824($17)
$L270:
li	$fp,-4			# 0xfffffffffffffffc
lw	$25,3008($17)
lw	$4,10520($17)
move	$22,$0
sw	$18,36($sp)
.set	noreorder
.set	nomacro
jalr	$25
sw	$fp,40($sp)
.set	macro
.set	reorder

li	$2,524288			# 0x80000
lw	$3,36($sp)
lw	$18,8004($17)
addiu	$23,$2,121
lw	$28,24($sp)
.set	noreorder
.set	nomacro
beq	$18,$0,$L137
andi	$20,$3,0x20
.set	macro
.set	reorder

$L354:
lw	$2,10112($17)
beq	$2,$0,$L138
lw	$2,10084($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L139
slt	$2,$22,4
.set	macro
.set	reorder

lw	$4,0($21)
lw	$3,2728($17)
sll	$4,$4,1
addu	$3,$3,$4
.set	noreorder
.set	nomacro
beq	$2,$0,$L140
lh	$3,0($3)
.set	macro
.set	reorder

lw	$2,2780($17)
$L336:
sra	$4,$2,1
lw	$5,2848($17)
lw	$6,168($17)
move	$16,$0
addu	$3,$3,$4
lw	$4,7996($17)
teq	$2,$0,7
div	$0,$3,$2
lw	$3,7992($17)
addu	$3,$5,$3
mflo	$2
mul	$5,$4,$6
addu	$3,$5,$3
lbu	$3,0($3)
sh	$2,0($19)
sll	$3,$3,$22
andi	$3,$3,0x20
sw	$3,44($sp)
$L164:
beq	$20,$0,$L176
lw	$3,%got(ff_mpeg4_rl_intra_hw)($28)
lw	$2,2824($17)
.set	noreorder
.set	nomacro
bne	$2,$0,$L177
lw	$9,60($3)
.set	macro
.set	reorder

lw	$4,%got(ff_mpeg4_rl_intra)($28)
addiu	$8,$17,8732
move	$20,$0
li	$14,1			# 0x1
$L178:
lw	$13,%got(JZC_mpeg4_intra_level)($28)
li	$12,127			# 0x7f
lw	$11,%got(JZC_mpeg4_intra_run)($28)
li	$15,125			# 0x7d
li	$10,102			# 0x66
sw	$21,48($sp)
li	$7,13			# 0xd
li	$25,109			# 0x6d
.set	noreorder
.set	nomacro
beq	$18,$0,$L183
li	$24,93			# 0x5d
.set	macro
.set	reorder

$L350:
#APP
# 342 "vlc_bs.c" 1
mtc0	$23,$21,0
# 0 "" 2
# 343 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 344 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
slt	$2,$3,103
movz	$3,$10,$2
move	$2,$3
sll	$3,$3,2
addu	$2,$13,$2
addu	$3,$11,$3
lb	$6,0($2)
lw	$2,0($3)
.set	noreorder
.set	nomacro
bne	$6,$0,$L188
addiu	$2,$2,1
.set	macro
.set	reorder

$L352:
#APP
# 112 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L189
#APP
# 112 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L190
#APP
# 112 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
andi	$5,$3,0x40
andi	$3,$3,0x3f
#APP
# 112 "vlc_bs.c" 1
mtc0	$24,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
# 112 "vlc_bs.c" 1
mtc0	$15,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$fp,$21,1
# 0 "" 2
#NO_APP
sll	$6,$6,8
addu	$2,$6,$fp
andi	$6,$2,0x2000
beq	$6,$0,$L195
andi	$6,$2,0x1000
.set	noreorder
.set	nomacro
beq	$6,$0,$L191
sra	$6,$2,1
.set	macro
.set	reorder

li	$21,-4096			# 0xfffffffffffff000
or	$6,$6,$21
$L192:
andi	$2,$2,0x1
beq	$2,$0,$L195
#APP
# 1701 "mpeg4_p0.c" 1
.word	0b01110000000001100000000001101111	#S32I2M XR1,$6
# 0 "" 2
# 1702 "mpeg4_p0.c" 1
.word	0b01110000000101000000000010101111	#S32I2M XR2,$20
# 0 "" 2
# 1703 "mpeg4_p0.c" 1
.word	0b01110000000011100000000011101111	#S32I2M XR3,$14
# 0 "" 2
# 1704 "mpeg4_p0.c" 1
.word	0b01110000000000000100100100000111	#S32CPS XR4,XR2,XR1
# 0 "" 2
# 1705 "mpeg4_p0.c" 1
.word	0b01110000010101001100010100001101	#S16MAD XR4,XR1,XR3,XR5,A,1
# 0 "" 2
# 1706 "mpeg4_p0.c" 1
.word	0b01110000000001100000000101101110	#S32M2I XR5, $6
# 0 "" 2
#NO_APP
addiu	$fp,$6,2048
sltu	$fp,$fp,4096
.set	noreorder
.set	nomacro
bne	$fp,$0,$L193
move	$2,$6
.set	macro
.set	reorder

lw	$2,9816($17)
slt	$2,$2,3
.set	noreorder
.set	nomacro
bne	$2,$0,$L387
slt	$2,$6,0
.set	macro
.set	reorder

addiu	$2,$6,2560
sltu	$2,$2,5121
beq	$2,$0,$L348
slt	$2,$6,0
$L387:
li	$fp,2047			# 0x7ff
li	$6,-2048			# 0xfffffffffffff800
movn	$fp,$6,$2
move	$2,$fp
$L193:
addiu	$3,$3,1
addu	$16,$16,$3
addiu	$3,$16,192
movn	$16,$3,$5
$L196:
slt	$3,$16,63
beq	$3,$0,$L349
$L207:
addu	$3,$8,$16
lbu	$3,0($3)
sll	$3,$3,1
addu	$3,$19,$3
.set	noreorder
.set	nomacro
bne	$18,$0,$L350
sh	$2,0($3)
.set	macro
.set	reorder

$L183:
#APP
# 154 "vlc_bs.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,2
addu	$3,$9,$3
lb	$5,2($3)
.set	noreorder
.set	nomacro
bltz	$5,$L351
lh	$6,0($3)
.set	macro
.set	reorder

$L186:
addiu	$5,$5,-1
lbu	$2,3($3)
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
beq	$6,$0,$L352
$L188:
addu	$16,$16,$2
#APP
# 112 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$5,$0,$2
subu	$2,$0,$5
slt	$3,$16,63
xor	$2,$2,$6
.set	noreorder
.set	nomacro
bne	$3,$0,$L207
addu	$2,$2,$5
.set	macro
.set	reorder

$L349:
addiu	$16,$16,-192
li	$3,-64			# 0xffffffffffffffc0
and	$3,$16,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L353
lw	$21,48($sp)
.set	macro
.set	reorder

addu	$8,$8,$16
lbu	$3,0($8)
sll	$3,$3,1
addu	$3,$19,$3
.set	noreorder
.set	nomacro
bne	$18,$0,$L176
sh	$2,0($3)
.set	macro
.set	reorder

$L209:
sw	$16,668($21)
$L181:
lw	$3,36($sp)
addiu	$22,$22,1
lw	$8,40($sp)
li	$2,6			# 0x6
addiu	$19,$19,128
sll	$3,$3,1
addiu	$8,$8,1
addiu	$21,$21,4
sw	$3,36($sp)
.set	noreorder
.set	nomacro
beq	$22,$2,$L269
sw	$8,40($sp)
.set	macro
.set	reorder

lw	$3,36($sp)
lw	$18,8004($17)
.set	noreorder
.set	nomacro
bne	$18,$0,$L354
andi	$20,$3,0x20
.set	macro
.set	reorder

$L137:
.set	noreorder
.set	nomacro
beq	$20,$0,$L355
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,10164($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L182
addiu	$8,$17,8732
.set	macro
.set	reorder

lw	$2,%got(ff_h263_rl_inter_hw)($28)
move	$20,$0
li	$14,1			# 0x1
lw	$4,%got(ff_h263_rl_inter)($28)
li	$16,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L178
lw	$9,60($2)
.set	macro
.set	reorder

$L259:
lw	$4,%got(ff_mpeg4_resync_prefix)($28)
sll	$3,$3,1
addu	$3,$4,$3
lhu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$2,$3,$L326
move	$2,$0
.set	macro
.set	reorder

lw	$6,10340($17)
addiu	$4,$6,1
subu	$2,$0,$4
andi	$2,$2,0x7
.set	noreorder
.set	nomacro
bne	$2,$0,$L356
sw	$4,10340($17)
.set	macro
.set	reorder

$L261:
move	$16,$0
.set	noreorder
.set	nomacro
b	$L263
li	$5,32			# 0x20
.set	macro
.set	reorder

$L357:
addiu	$16,$16,1
.set	noreorder
.set	nomacro
beq	$16,$5,$L388
lw	$25,%call16(ff_mpeg4_get_video_packet_prefix_length)($28)
.set	macro
.set	reorder

$L263:
srl	$3,$4,3
andi	$7,$4,0x7
addu	$3,$9,$3
addiu	$4,$4,1
lbu	$2,0($3)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L357
sw	$4,10340($17)
.set	macro
.set	reorder

lw	$25,%call16(ff_mpeg4_get_video_packet_prefix_length)($28)
$L388:
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_get_video_packet_prefix_length
1:	jalr	$25
sw	$6,10340($17)
.set	macro
.set	reorder

slt	$2,$16,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L326
move	$2,$0
.set	macro
.set	reorder

$L260:
lw	$2,7992($17)
li	$5,2			# 0x2
lw	$3,160($17)
lw	$4,2844($17)
addiu	$2,$2,1
lw	$9,32($sp)
xor	$3,$2,$3
li	$2,1			# 0x1
movz	$2,$5,$3
addu	$2,$4,$2
addu	$2,$2,$9
lbu	$2,0($2)
beq	$2,$0,$L266
$L251:
.set	noreorder
.set	nomacro
b	$L326
li	$2,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

$L133:
li	$3,1			# 0x1
sltu	$2,$0,$2
sw	$3,7260($17)
.set	noreorder
.set	nomacro
b	$L270
sw	$2,7264($17)
.set	macro
.set	reorder

$L345:
.set	noreorder
.set	nomacro
b	$L270
addiu	$21,$17,8012
.set	macro
.set	reorder

$L347:
lw	$4,9948($17)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$4,$2,$L389
li	$2,1			# 0x1
.set	macro
.set	reorder

sw	$3,10036($17)
.set	noreorder
.set	nomacro
b	$L269
sw	$0,2832($17)
.set	macro
.set	reorder

$L189:
beq	$18,$0,$L202
#APP
# 342 "vlc_bs.c" 1
mtc0	$23,$21,0
# 0 "" 2
# 343 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 344 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
slt	$2,$3,103
movz	$3,$10,$2
move	$2,$3
sll	$3,$3,2
addu	$2,$13,$2
addu	$3,$11,$3
lb	$fp,0($2)
lw	$3,0($3)
addiu	$3,$3,1
addu	$16,$16,$3
$L378:
#APP
# 112 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
sra	$2,$3,7
addiu	$3,$3,-1
addiu	$2,$2,6
andi	$3,$3,0x3f
sll	$6,$2,2
sltu	$5,$0,$5
addu	$2,$4,$6
subu	$6,$0,$5
lw	$2,4($2)
addu	$3,$2,$3
lb	$3,0($3)
mul	$21,$3,$14
addu	$2,$21,$fp
xor	$2,$2,$6
.set	noreorder
.set	nomacro
b	$L196
addu	$2,$2,$5
.set	macro
.set	reorder

$L351:
#APP
# 79 "vlc_bs.c" 1
mtc0	$15,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$5,$0,$5
sll	$fp,$5,4
ori	$2,$fp,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
#NO_APP
addu	$3,$5,$6
sll	$3,$3,2
addu	$3,$9,$3
lb	$5,2($3)
.set	noreorder
.set	nomacro
bgez	$5,$L186
lh	$6,0($3)
.set	macro
.set	reorder

ori	$fp,$fp,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$fp,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$5
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$6,$3,$6
sll	$3,$6,2
addu	$3,$9,$3
lh	$6,0($3)
.set	noreorder
.set	nomacro
b	$L186
lb	$5,2($3)
.set	macro
.set	reorder

$L190:
beq	$18,$0,$L197
#APP
# 342 "vlc_bs.c" 1
mtc0	$23,$21,0
# 0 "" 2
# 343 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 344 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
slt	$2,$3,103
movz	$3,$10,$2
move	$2,$3
sll	$3,$3,2
addu	$2,$13,$2
addu	$3,$11,$3
lb	$6,0($2)
lw	$5,0($3)
addiu	$5,$5,1
$L198:
teq	$14,$0,7
div	$0,$6,$14
sra	$3,$5,7
addiu	$3,$3,8
sll	$3,$3,2
addu	$3,$4,$3
lw	$3,4($3)
mflo	$2
addu	$2,$3,$2
lb	$2,0($2)
addu	$5,$5,$2
addiu	$5,$5,1
addu	$16,$16,$5
#APP
# 112 "vlc_bs.c" 1
mtc0	$7,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$3,$0,$2
subu	$2,$0,$3
xor	$2,$2,$6
.set	noreorder
.set	nomacro
b	$L196
addu	$2,$2,$3
.set	macro
.set	reorder

$L202:
#APP
# 154 "vlc_bs.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,2
addu	$3,$9,$3
lb	$5,2($3)
.set	noreorder
.set	nomacro
bltz	$5,$L358
lh	$2,0($3)
.set	macro
.set	reorder

$L339:
move	$fp,$2
$L205:
addiu	$5,$5,-1
lbu	$3,3($3)
sll	$5,$5,4
ori	$5,$5,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L378
addu	$16,$16,$3
.set	macro
.set	reorder

$L358:
#APP
# 79 "vlc_bs.c" 1
mtc0	$15,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
nor	$5,$0,$5
sll	$6,$5,4
ori	$3,$6,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$3,$3,$2
sll	$3,$3,2
addu	$3,$9,$3
lb	$5,2($3)
.set	noreorder
.set	nomacro
bgez	$5,$L339
lh	$2,0($3)
.set	macro
.set	reorder

ori	$6,$6,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$5
sll	$3,$3,4
ori	$3,$3,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$3,$3,$2
sll	$3,$3,2
addu	$3,$9,$3
lh	$fp,0($3)
.set	noreorder
.set	nomacro
b	$L205
lb	$5,2($3)
.set	macro
.set	reorder

$L197:
#APP
# 154 "vlc_bs.c" 1
mtc0	$12,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,2
addu	$3,$9,$3
lb	$5,2($3)
.set	noreorder
.set	nomacro
bltz	$5,$L359
lh	$6,0($3)
.set	macro
.set	reorder

$L200:
addiu	$2,$5,-1
lbu	$5,3($3)
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
b	$L198
$L191:
.set	noreorder
.set	nomacro
b	$L192
andi	$6,$6,0xfff
.set	macro
.set	reorder

$L176:
lw	$2,10112($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L360
lw	$3,0($21)
.set	macro
.set	reorder

$L212:
lw	$2,2812($17)
sll	$3,$3,5
lw	$4,2824($17)
lw	$6,2172($17)
.set	noreorder
.set	nomacro
beq	$4,$0,$L232
addu	$2,$2,$3
.set	macro
.set	reorder

lw	$3,44($sp)
bne	$3,$0,$L226
lw	$3,7992($17)
lw	$4,7996($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L229
lw	$7,168($17)
.set	macro
.set	reorder

mul	$5,$4,$7
lw	$10,2872($17)
addiu	$3,$3,-1
addu	$3,$5,$3
addu	$7,$6,$3
lb	$3,0($7)
.set	noreorder
.set	nomacro
beq	$10,$3,$L229
li	$4,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

li	$5,1			# 0x1
and	$4,$22,$4
.set	noreorder
.set	nomacro
beq	$4,$5,$L229
sra	$11,$10,1
.set	macro
.set	reorder

addiu	$9,$2,-30
li	$8,1			# 0x1
.set	noreorder
.set	nomacro
b	$L235
li	$12,8			# 0x8
.set	macro
.set	reorder

$L234:
teq	$10,$0,7
div	$0,$4,$10
addiu	$8,$8,1
addiu	$9,$9,2
mflo	$4
addu	$4,$6,$4
.set	noreorder
.set	nomacro
beq	$8,$12,$L232
sh	$4,0($3)
.set	macro
.set	reorder

lb	$3,0($7)
$L235:
sll	$4,$8,3
lh	$5,0($9)
addu	$4,$17,$4
mul	$5,$5,$3
lbu	$3,5664($4)
sll	$3,$3,1
addu	$3,$19,$3
addu	$4,$5,$11
.set	noreorder
.set	nomacro
bgtz	$5,$L234
lhu	$6,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L234
subu	$4,$5,$11
.set	macro
.set	reorder

$L229:
addiu	$7,$2,-30
li	$6,1			# 0x1
li	$8,8			# 0x8
$L228:
lh	$4,0($7)
sll	$3,$6,4
addiu	$6,$6,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L231
addu	$3,$19,$3
.set	macro
.set	reorder

lhu	$5,0($3)
addu	$4,$4,$5
sh	$4,0($3)
$L231:
.set	noreorder
.set	nomacro
bne	$6,$8,$L228
addiu	$7,$7,2
.set	macro
.set	reorder

$L232:
lh	$3,16($19)
$L379:
sh	$3,2($2)
lh	$3,2($19)
sh	$3,18($2)
#APP
# 581 "mpeg4_p0.c" 1
.word	0b01110010011000000100000110101010	#S16LDD XR6,$19,32,PTN0
# 0 "" 2
# 582 "mpeg4_p0.c" 1
.word	0b01110010011000001000000111101010	#S16LDD XR7,$19,64,PTN0
# 0 "" 2
# 583 "mpeg4_p0.c" 1
.word	0b01110010011010000110000110101010	#S16LDD XR6,$19,48,PTN1
# 0 "" 2
# 584 "mpeg4_p0.c" 1
.word	0b01110010011000001100001000101010	#S16LDD XR8,$19,96,PTN0
# 0 "" 2
# 585 "mpeg4_p0.c" 1
.word	0b01110010011010001010000111101010	#S16LDD XR7,$19,80,PTN1
# 0 "" 2
# 586 "mpeg4_p0.c" 1
.word	0b01110010011010001110001000101010	#S16LDD XR8,$19,112,PTN1
# 0 "" 2
# 588 "mpeg4_p0.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 589 "mpeg4_p0.c" 1
.word	0b01110000010000000000100111010001	#S32STD XR7,$2,8
# 0 "" 2
# 590 "mpeg4_p0.c" 1
.word	0b01110000010000000000111000010001	#S32STD XR8,$2,12
# 0 "" 2
# 592 "mpeg4_p0.c" 1
.word	0b01110010011000000000010010010000	#S32LDD XR2,$19,4
# 0 "" 2
# 593 "mpeg4_p0.c" 1
.word	0b01110010011000000000100011010000	#S32LDD XR3,$19,8
# 0 "" 2
# 594 "mpeg4_p0.c" 1
.word	0b01110010011000000000110100010000	#S32LDD XR4,$19,12
# 0 "" 2
# 596 "mpeg4_p0.c" 1
.word	0b01110000010000000001010010010001	#S32STD XR2,$2,20
# 0 "" 2
# 597 "mpeg4_p0.c" 1
.word	0b01110000010000000001100011010001	#S32STD XR3,$2,24
# 0 "" 2
# 598 "mpeg4_p0.c" 1
.word	0b01110000010000000001110100010001	#S32STD XR4,$2,28
# 0 "" 2
#NO_APP
lw	$3,2824($17)
li	$2,63			# 0x3f
.set	noreorder
.set	nomacro
b	$L209
movn	$16,$2,$3
.set	macro
.set	reorder

$L359:
#APP
# 79 "vlc_bs.c" 1
mtc0	$15,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$5,$0,$5
sll	$fp,$5,4
ori	$2,$fp,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$6,$3,$6
sll	$3,$6,2
addu	$3,$9,$3
lb	$5,2($3)
.set	noreorder
.set	nomacro
bgez	$5,$L200
lh	$6,0($3)
.set	macro
.set	reorder

ori	$fp,$fp,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$fp,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$5
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$3,$3,$6
sll	$3,$3,2
addu	$3,$9,$3
lh	$6,0($3)
.set	noreorder
.set	nomacro
b	$L200
lb	$5,2($3)
.set	macro
.set	reorder

$L355:
.set	noreorder
.set	nomacro
b	$L181
sw	$2,668($21)
.set	macro
.set	reorder

$L182:
lw	$14,2872($17)
li	$16,-1			# 0xffffffffffffffff
lw	$4,%got(ff_h263_rl_inter)($28)
addiu	$2,$14,14
addiu	$20,$14,-1
sll	$3,$2,2
lw	$2,%got(ff_h263_rl_inter_hw)($28)
sll	$14,$14,1
ori	$20,$20,0x1
addu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L178
lw	$9,4($2)
.set	macro
.set	reorder

$L138:
lw	$2,0($21)
slt	$3,$22,4
lw	$6,2780($17)
lw	$4,2784($17)
lw	$5,2728($17)
sll	$2,$2,1
movn	$4,$6,$3
addu	$5,$5,$2
#APP
# 248 "mpeg4_p0.c" 1
.word	0b01110000101101111111110001101010	#S16LDD XR1,$5,-2,PTN2
# 0 "" 2
#NO_APP
lw	$2,24($21)
nor	$2,$0,$2
sll	$2,$2,1
addu	$3,$5,$2
#APP
# 249 "mpeg4_p0.c" 1
.word	0b01110000011100000000000010101010	#S16LDD XR2,$3,0,PTN2
# 0 "" 2
#NO_APP
addiu	$2,$2,2
addu	$2,$5,$2
#APP
# 250 "mpeg4_p0.c" 1
.word	0b01110000010100000000000011101010	#S16LDD XR3,$2,0,PTN2
# 0 "" 2
#NO_APP
lw	$2,10296($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L166
andi	$2,$22,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L167
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 254 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
# 255 "mpeg4_p0.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
#NO_APP
$L167:
lw	$7,7992($17)
lw	$2,9784($17)
.set	noreorder
.set	nomacro
beq	$7,$2,$L361
li	$3,1			# 0x1
.set	macro
.set	reorder

$L168:
#APP
# 270 "mpeg4_p0.c" 1
.word	0b01110011000000001000110100011000	#D32ADD XR4,XR3,XR2,XR0,SS
# 0 "" 2
# 271 "mpeg4_p0.c" 1
.word	0b01110011000000001000010101011000	#D32ADD XR5,XR1,XR2,XR0,SS
# 0 "" 2
# 272 "mpeg4_p0.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 273 "mpeg4_p0.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 274 "mpeg4_p0.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 275 "mpeg4_p0.c" 1
.word	0b01110000000101001101010001111001	#S32MOVN XR1,XR5,XR3
# 0 "" 2
# 276 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101110	#S32M2I XR1, $2
# 0 "" 2
# 277 "mpeg4_p0.c" 1
.word	0b01110000000000110000000101101110	#S32M2I XR5, $3
# 0 "" 2
#NO_APP
sltu	$3,$0,$3
lw	$6,9816($17)
sra	$8,$4,1
sw	$3,44($sp)
move	$3,$2
addu	$2,$3,$8
slt	$3,$6,3
teq	$4,$0,7
div	$0,$2,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L171
mflo	$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L362
mul	$2,$4,$2
.set	macro
.set	reorder

addiu	$3,$4,2048
slt	$3,$3,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L363
lw	$6,%got($LC9)($28)
.set	macro
.set	reorder

$L173:
li	$3,-2048			# 0xfffffffffffff800
and	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L338
li	$16,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

bltz	$2,$L274
lw	$3,88($17)
andi	$3,$3,0x1000
.set	noreorder
.set	nomacro
bne	$3,$0,$L390
sll	$2,$2,16
.set	macro
.set	reorder

li	$2,2047			# 0x7ff
li	$16,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L164
sh	$2,0($5)
.set	macro
.set	reorder

$L338:
sll	$2,$2,16
$L390:
sra	$2,$2,16
.set	noreorder
.set	nomacro
b	$L164
sh	$2,0($5)
.set	macro
.set	reorder

$L348:
lw	$6,%got($LC10)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
lw	$7,2872($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

lw	$28,24($sp)
$L195:
lw	$18,8004($17)
$L264:
lw	$2,7996($17)
li	$5,16			# 0x10
lw	$6,%got($LC12)($28)
lw	$4,0($17)
lw	$7,7992($17)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC12)
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$18,20($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L326
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L140:
.set	noreorder
.set	nomacro
b	$L336
lw	$2,2784($17)
.set	macro
.set	reorder

$L177:
lw	$3,44($sp)
bne	$3,$0,$L179
addiu	$8,$17,8996
lw	$4,%got(ff_mpeg4_rl_intra)($28)
move	$20,$0
.set	noreorder
.set	nomacro
b	$L178
li	$14,1			# 0x1
.set	macro
.set	reorder

$L139:
slt	$3,$22,4
.set	noreorder
.set	nomacro
beq	$3,$0,$L143
li	$2,393216			# 0x60000
.set	macro
.set	reorder

li	$2,262144			# 0x40000
addiu	$2,$2,121
#APP
# 302 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 303 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 305 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$4,$2,10
$L380:
beq	$4,$0,$L264
.set	noreorder
.set	nomacro
beq	$2,$0,$L146
move	$8,$0
.set	macro
.set	reorder

li	$4,9			# 0x9
.set	noreorder
.set	nomacro
beq	$2,$4,$L364
addiu	$4,$2,-1
.set	macro
.set	reorder

sll	$5,$4,4
ori	$5,$5,0xd
#APP
# 112 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$5,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
sra	$4,$8,$4
.set	noreorder
.set	nomacro
beq	$4,$0,$L365
li	$4,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L146:
lw	$2,0($21)
$L386:
lw	$4,2780($17)
lw	$5,2784($17)
lw	$6,2728($17)
sll	$2,$2,1
movn	$5,$4,$3
addu	$6,$6,$2
#APP
# 248 "mpeg4_p0.c" 1
.word	0b01110000110101111111110001101010	#S16LDD XR1,$6,-2,PTN2
# 0 "" 2
#NO_APP
lw	$3,24($21)
nor	$3,$0,$3
sll	$3,$3,1
addu	$2,$6,$3
#APP
# 249 "mpeg4_p0.c" 1
.word	0b01110000010100000000000010101010	#S16LDD XR2,$2,0,PTN2
# 0 "" 2
#NO_APP
addiu	$3,$3,2
addu	$3,$6,$3
#APP
# 250 "mpeg4_p0.c" 1
.word	0b01110000011100000000000011101010	#S16LDD XR3,$3,0,PTN2
# 0 "" 2
#NO_APP
lw	$2,10296($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L152
andi	$2,$22,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L366
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$7,7992($17)
$L381:
lw	$2,9784($17)
.set	noreorder
.set	nomacro
beq	$7,$2,$L367
li	$3,1			# 0x1
.set	macro
.set	reorder

$L154:
#APP
# 270 "mpeg4_p0.c" 1
.word	0b01110011000000001000110100011000	#D32ADD XR4,XR3,XR2,XR0,SS
# 0 "" 2
# 271 "mpeg4_p0.c" 1
.word	0b01110011000000001000010101011000	#D32ADD XR5,XR1,XR2,XR0,SS
# 0 "" 2
# 272 "mpeg4_p0.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 273 "mpeg4_p0.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 274 "mpeg4_p0.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 275 "mpeg4_p0.c" 1
.word	0b01110000000101001101010001111001	#S32MOVN XR1,XR5,XR3
# 0 "" 2
# 276 "mpeg4_p0.c" 1
.word	0b01110000000000110000000001101110	#S32M2I XR1, $3
# 0 "" 2
# 277 "mpeg4_p0.c" 1
.word	0b01110000000001000000000101101110	#S32M2I XR5, $4
# 0 "" 2
#NO_APP
sltu	$4,$0,$4
lw	$2,9816($17)
sra	$9,$5,1
sw	$4,44($sp)
move	$4,$3
addu	$3,$4,$9
slt	$4,$2,3
teq	$5,$0,7
div	$0,$3,$5
mflo	$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L157
addu	$2,$8,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L368
mul	$3,$5,$2
.set	macro
.set	reorder

addiu	$4,$5,2048
slt	$4,$4,$3
.set	noreorder
.set	nomacro
bne	$4,$0,$L369
li	$5,16			# 0x10
.set	macro
.set	reorder

$L160:
li	$4,-2048			# 0xfffffffffffff800
and	$4,$3,$4
beq	$4,$0,$L337
bltz	$3,$L272
lw	$4,88($17)
andi	$4,$4,0x1000
.set	noreorder
.set	nomacro
bne	$4,$0,$L391
sll	$3,$3,16
.set	macro
.set	reorder

li	$3,2047			# 0x7ff
sh	$3,0($6)
$L159:
bltz	$2,$L195
$L377:
sh	$2,0($19)
.set	noreorder
.set	nomacro
b	$L164
move	$16,$0
.set	macro
.set	reorder

$L337:
sll	$3,$3,16
$L391:
sra	$3,$3,16
.set	noreorder
.set	nomacro
b	$L159
sh	$3,0($6)
.set	macro
.set	reorder

$L226:
lw	$5,24($21)
lw	$3,7996($17)
lw	$4,7992($17)
sll	$5,$5,5
lw	$7,168($17)
.set	noreorder
.set	nomacro
beq	$3,$0,$L238
subu	$5,$2,$5
.set	macro
.set	reorder

mul	$8,$3,$7
lw	$10,2872($17)
addu	$3,$8,$4
subu	$7,$3,$7
addu	$7,$6,$7
lb	$3,0($7)
.set	noreorder
.set	nomacro
beq	$10,$3,$L238
addiu	$4,$22,-2
.set	macro
.set	reorder

sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L238
addiu	$8,$5,18
.set	macro
.set	reorder

sra	$11,$10,1
addiu	$9,$17,5665
.set	noreorder
.set	nomacro
b	$L243
addiu	$5,$5,32
.set	macro
.set	reorder

$L370:
addu	$3,$3,$11
$L242:
teq	$10,$0,7
div	$0,$3,$10
addiu	$8,$8,2
addiu	$9,$9,1
mflo	$3
addu	$3,$6,$3
.set	noreorder
.set	nomacro
beq	$8,$5,$L232
sh	$3,0($4)
.set	macro
.set	reorder

lb	$3,0($7)
$L243:
lh	$4,0($8)
lbu	$6,0($9)
mul	$3,$4,$3
sll	$4,$6,1
addu	$4,$19,$4
.set	noreorder
.set	nomacro
bgtz	$3,$L370
lhu	$6,0($4)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L242
subu	$3,$3,$11
.set	macro
.set	reorder

$L238:
addiu	$5,$5,18
addiu	$6,$19,2
addiu	$7,$19,16
$L237:
lh	$3,0($5)
.set	noreorder
.set	nomacro
beq	$3,$0,$L240
addiu	$5,$5,2
.set	macro
.set	reorder

lhu	$4,0($6)
addu	$3,$3,$4
sh	$3,0($6)
$L240:
addiu	$6,$6,2
bne	$6,$7,$L237
.set	noreorder
.set	nomacro
b	$L379
lh	$3,16($19)
.set	macro
.set	reorder

$L360:
lw	$7,2780($17)
sll	$4,$3,1
lw	$5,2784($17)
slt	$2,$22,4
lw	$8,2728($17)
lh	$6,0($19)
movn	$5,$7,$2
addu	$8,$8,$4
#APP
# 248 "mpeg4_p0.c" 1
.word	0b01110001000101111111110001101010	#S16LDD XR1,$8,-2,PTN2
# 0 "" 2
#NO_APP
lw	$2,24($21)
nor	$2,$0,$2
sll	$2,$2,1
addu	$4,$8,$2
#APP
# 249 "mpeg4_p0.c" 1
.word	0b01110000100100000000000010101010	#S16LDD XR2,$4,0,PTN2
# 0 "" 2
#NO_APP
addiu	$2,$2,2
addu	$2,$8,$2
#APP
# 250 "mpeg4_p0.c" 1
.word	0b01110000010100000000000011101010	#S16LDD XR3,$2,0,PTN2
# 0 "" 2
#NO_APP
lw	$2,10296($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L214
andi	$2,$22,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L371
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$7,7992($17)
$L382:
lw	$2,9784($17)
.set	noreorder
.set	nomacro
beq	$7,$2,$L372
li	$4,1			# 0x1
.set	macro
.set	reorder

$L216:
#APP
# 270 "mpeg4_p0.c" 1
.word	0b01110011000000001000110100011000	#D32ADD XR4,XR3,XR2,XR0,SS
# 0 "" 2
# 271 "mpeg4_p0.c" 1
.word	0b01110011000000001000010101011000	#D32ADD XR5,XR1,XR2,XR0,SS
# 0 "" 2
# 272 "mpeg4_p0.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 273 "mpeg4_p0.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 274 "mpeg4_p0.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 275 "mpeg4_p0.c" 1
.word	0b01110000000101001101010001111001	#S32MOVN XR1,XR5,XR3
# 0 "" 2
# 276 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101110	#S32M2I XR1, $2
# 0 "" 2
# 277 "mpeg4_p0.c" 1
.word	0b01110000000001000000000101101110	#S32M2I XR5, $4
# 0 "" 2
#NO_APP
sltu	$4,$0,$4
lw	$9,9816($17)
sra	$10,$5,1
sw	$4,44($sp)
move	$4,$2
addu	$2,$4,$10
slt	$4,$9,3
teq	$5,$0,7
div	$0,$2,$5
mflo	$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L219
addu	$2,$6,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L373
mul	$4,$5,$2
.set	macro
.set	reorder

addiu	$5,$5,2048
slt	$5,$5,$4
.set	noreorder
.set	nomacro
bne	$5,$0,$L374
li	$5,16			# 0x10
.set	macro
.set	reorder

$L222:
li	$5,-2048			# 0xfffffffffffff800
and	$5,$4,$5
beq	$5,$0,$L340
bltz	$4,$L277
lw	$5,88($17)
andi	$5,$5,0x1000
beq	$5,$0,$L278
$L340:
sll	$4,$4,16
sra	$4,$4,16
$L224:
sll	$2,$2,16
sh	$4,0($8)
sra	$2,$2,16
$L221:
srl	$4,$16,31
sh	$2,0($19)
.set	noreorder
.set	nomacro
b	$L212
addu	$16,$16,$4
.set	macro
.set	reorder

$L171:
.set	noreorder
.set	nomacro
b	$L173
mul	$2,$4,$2
.set	macro
.set	reorder

$L166:
lw	$7,7992($17)
lw	$2,9784($17)
bne	$7,$2,$L168
$L169:
lw	$2,9788($17)
$L383:
lw	$3,7996($17)
addiu	$2,$2,1
bne	$3,$2,$L168
.set	noreorder
.set	nomacro
beq	$22,$0,$L392
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$3,40($sp)
sltu	$2,$3,2
beq	$2,$0,$L168
li	$2,1024			# 0x400
$L392:
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L168
$L179:
addiu	$8,$17,8864
lw	$4,%got(ff_mpeg4_rl_intra)($28)
move	$20,$0
.set	noreorder
.set	nomacro
b	$L178
li	$14,1			# 0x1
.set	macro
.set	reorder

$L250:
lw	$4,%got(ff_mpeg4_resync_prefix)($28)
sll	$3,$3,1
addu	$3,$4,$3
lhu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$2,$3,$L376
li	$2,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

lw	$6,10340($17)
addiu	$4,$6,1
subu	$2,$0,$4
andi	$2,$2,0x7
.set	noreorder
.set	nomacro
beq	$2,$0,$L252
sw	$4,10340($17)
.set	macro
.set	reorder

addu	$4,$2,$4
sw	$4,10340($17)
$L252:
move	$16,$0
.set	noreorder
.set	nomacro
b	$L254
li	$5,32			# 0x20
.set	macro
.set	reorder

$L375:
addiu	$16,$16,1
.set	noreorder
.set	nomacro
beq	$16,$5,$L393
lw	$25,%call16(ff_mpeg4_get_video_packet_prefix_length)($28)
.set	macro
.set	reorder

$L254:
srl	$3,$4,3
andi	$7,$4,0x7
addu	$3,$9,$3
addiu	$4,$4,1
lbu	$2,0($3)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L375
sw	$4,10340($17)
.set	macro
.set	reorder

lw	$25,%call16(ff_mpeg4_get_video_packet_prefix_length)($28)
$L393:
move	$4,$17
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_get_video_packet_prefix_length
1:	jalr	$25
sw	$6,10340($17)
.set	macro
.set	reorder

slt	$2,$16,$2
beq	$2,$0,$L251
.set	noreorder
.set	nomacro
b	$L376
li	$2,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

$L143:
addiu	$2,$2,121
#APP
# 323 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 325 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 326 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L380
sltu	$4,$2,10
.set	macro
.set	reorder

$L219:
.set	noreorder
.set	nomacro
b	$L222
mul	$4,$5,$2
.set	macro
.set	reorder

$L214:
lw	$7,7992($17)
lw	$2,9784($17)
bne	$7,$2,$L216
$L217:
lw	$2,9788($17)
$L384:
lw	$4,7996($17)
addiu	$2,$2,1
bne	$4,$2,$L216
.set	noreorder
.set	nomacro
beq	$22,$0,$L394
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$fp,40($sp)
sltu	$2,$fp,2
beq	$2,$0,$L216
li	$2,1024			# 0x400
$L394:
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L216
$L157:
.set	noreorder
.set	nomacro
b	$L160
mul	$3,$5,$2
.set	macro
.set	reorder

$L152:
lw	$7,7992($17)
lw	$2,9784($17)
bne	$7,$2,$L154
$L155:
lw	$2,9788($17)
$L385:
lw	$3,7996($17)
addiu	$2,$2,1
bne	$3,$2,$L154
.set	noreorder
.set	nomacro
beq	$22,$0,$L395
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$9,40($sp)
sltu	$2,$9,2
beq	$2,$0,$L154
li	$2,1024			# 0x400
$L395:
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L154
$L356:
addu	$4,$2,$4
.set	noreorder
.set	nomacro
b	$L261
sw	$4,10340($17)
.set	macro
.set	reorder

$L366:
#APP
# 254 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
# 255 "mpeg4_p0.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L381
lw	$7,7992($17)
.set	macro
.set	reorder

$L371:
#APP
# 254 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
# 255 "mpeg4_p0.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L382
lw	$7,7992($17)
.set	macro
.set	reorder

$L365:
sll	$2,$4,$2
or	$2,$8,$2
.set	noreorder
.set	nomacro
b	$L146
addiu	$8,$2,1
.set	macro
.set	reorder

$L278:
.set	noreorder
.set	nomacro
b	$L224
li	$4,2047			# 0x7ff
.set	macro
.set	reorder

$L274:
move	$2,$0
.set	noreorder
.set	nomacro
b	$L164
sh	$2,0($5)
.set	macro
.set	reorder

$L361:
li	$2,-3			# 0xfffffffffffffffd
and	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$3,$L169
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 259 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
# 260 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L383
lw	$2,9788($17)
.set	macro
.set	reorder

$L277:
.set	noreorder
.set	nomacro
b	$L224
move	$4,$0
.set	macro
.set	reorder

$L372:
li	$2,-3			# 0xfffffffffffffffd
and	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$4,$L217
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 259 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
# 260 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L384
lw	$2,9788($17)
.set	macro
.set	reorder

$L272:
move	$3,$0
.set	noreorder
.set	nomacro
b	$L159
sh	$3,0($6)
.set	macro
.set	reorder

$L367:
li	$2,-3			# 0xfffffffffffffffd
and	$2,$22,$2
.set	noreorder
.set	nomacro
beq	$2,$3,$L155
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 259 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
# 260 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L385
lw	$2,9788($17)
.set	macro
.set	reorder

$L353:
lw	$6,%got($LC11)($28)
li	$5,16			# 0x10
lw	$2,7996($17)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
addiu	$6,$6,%lo($LC11)
lw	$7,7992($17)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L195
lw	$28,24($sp)
.set	macro
.set	reorder

$L364:
li	$2,125			# 0x7d
#APP
# 98 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 99 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 100 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$4,13			# 0xd
#APP
# 104 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 105 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 106 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,1
or	$8,$2,$8
sra	$2,$8,8
bne	$2,$0,$L148
li	$2,-512			# 0xfffffffffffffe00
or	$2,$8,$2
addiu	$8,$2,1
$L148:
li	$2,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L386
lw	$2,0($21)
.set	macro
.set	reorder

$L363:
li	$5,16			# 0x10
lw	$2,7996($17)
li	$16,-1			# 0xffffffffffffffff
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L164
lw	$28,24($sp)
.set	macro
.set	reorder

$L362:
lw	$2,%got(mpeg4_pred_dc.isra.13.part.14)($28)
move	$4,$17
addiu	$25,$2,%lo(mpeg4_pred_dc.isra.13.part.14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_dc.isra.13.part.14
1:	jalr	$25
li	$16,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L164
lw	$28,24($sp)
.set	macro
.set	reorder

$L369:
lw	$6,%got($LC9)($28)
lw	$2,7996($17)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L264
lw	$18,8004($17)
.set	macro
.set	reorder

$L368:
lw	$2,%got(mpeg4_pred_dc.isra.13.part.14)($28)
addiu	$25,$2,%lo(mpeg4_pred_dc.isra.13.part.14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_dc.isra.13.part.14
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L377
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L264
lw	$18,8004($17)
.set	macro
.set	reorder

$L374:
lw	$2,7996($17)
lw	$6,%got($LC9)($28)
lw	$25,%call16(av_log)($28)
lw	$4,0($17)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L221
lw	$3,0($21)
.set	macro
.set	reorder

$L373:
lw	$2,%got(mpeg4_pred_dc.isra.13.part.14)($28)
addiu	$25,$2,%lo(mpeg4_pred_dc.isra.13.part.14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_dc.isra.13.part.14
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

sll	$2,$2,16
lw	$28,24($sp)
lw	$3,0($21)
.set	noreorder
.set	nomacro
b	$L221
sra	$2,$2,16
.set	macro
.set	reorder

.end	mpeg4_decode_partitioned_mb
.size	mpeg4_decode_partitioned_mb, .-mpeg4_decode_partitioned_mb
.section	.text.mpeg4_decode_init_vlc,"ax",@progbits
.align	2
.align	5
.globl	mpeg4_decode_init_vlc
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_init_vlc
.type	mpeg4_decode_init_vlc, @function
mpeg4_decode_init_vlc:
.frame	$sp,88,$31		# vars= 0, regs= 5/0, args= 56, gp= 8
.mask	0x800f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(done.6415)($28)
addiu	$sp,$sp,-88
.cprestore	56
sw	$31,84($sp)
sw	$19,80($sp)
sw	$18,76($sp)
sw	$17,72($sp)
sw	$16,68($sp)
lw	$3,%lo(done.6415)($2)
beq	$3,$0,$L408
lw	$31,84($sp)

lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,88

$L408:
li	$16,1			# 0x1
lw	$8,%got(ff_h263_intra_MCBPC_code)($28)
li	$18,4			# 0x4
lw	$3,%got(ff_h263_intra_MCBPC_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
li	$5,6			# 0x6
sw	$16,%lo(done.6415)($2)
li	$6,9			# 0x9
lw	$2,%got(table.6416)($28)
move	$4,$3
lw	$7,%got(ff_h263_intra_MCBPC_bits)($28)
li	$19,2			# 0x2
sw	$8,24($sp)
addiu	$2,$2,%lo(table.6416)
sw	$16,16($sp)
sw	$16,20($sp)
sw	$16,28($sp)
sw	$16,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$2,4($3)
li	$2,72			# 0x48
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,12($3)

li	$5,7			# 0x7
lw	$28,56($sp)
li	$6,28			# 0x1c
sw	$16,16($sp)
sw	$16,20($sp)
sw	$16,28($sp)
lw	$3,%got(ff_h263_inter_MCBPC_code)($28)
lw	$2,%got(ff_h263_inter_MCBPC_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
lw	$7,%got(ff_h263_inter_MCBPC_bits)($28)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.6417)($28)
sw	$16,32($sp)
sw	$0,36($sp)
addiu	$3,$3,%lo(table.6417)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,198			# 0xc6
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,6			# 0x6
lw	$28,56($sp)
li	$6,16			# 0x10
sw	$19,16($sp)
sw	$16,20($sp)
sw	$19,28($sp)
lw	$3,%got(ff_h263_cbpy_tab)($28)
lw	$2,%got(ff_h263_cbpy_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$16,32($sp)
sw	$3,24($sp)
move	$4,$2
lw	$3,%got(table.6418)($28)
sw	$0,36($sp)
sw	$0,40($sp)
addiu	$3,$3,%lo(table.6418)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,64			# 0x40
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$6,33			# 0x21
lw	$28,56($sp)
li	$5,8			# 0x8
sw	$19,16($sp)
sw	$16,20($sp)
sw	$19,28($sp)
lw	$3,%got(mvtab)($28)
lw	$2,%got(mv_vlc_hw)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
lw	$17,%got(ff_h263_rl_inter_hw)($28)
sw	$3,24($sp)
addiu	$2,$2,%lo(mv_vlc_hw)
lw	$3,%got(table.6419)($28)
sw	$16,32($sp)
move	$4,$2
sw	$0,36($sp)
addiu	$3,$3,%lo(table.6419)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$3,4($2)
li	$3,538			# 0x21a
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

lw	$28,56($sp)
lw	$25,%call16(init_rl)($28)
lw	$5,%got(ff_h263_static_rl_table_store)($28)
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
move	$4,$17

addiu	$4,$17,44
lw	$2,8($17)
li	$5,8			# 0x8
lw	$28,56($sp)
lw	$6,0($17)
addiu	$7,$2,2
sw	$18,16($sp)
sw	$2,24($sp)
lw	$2,%got(table.6422)($28)
addiu	$6,$6,1
lw	$25,%call16(init_vlc_sparse)($28)
sw	$19,20($sp)
addiu	$2,$2,%lo(table.6422)
sw	$18,28($sp)
sw	$19,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$18,48($sp)
sw	$2,48($17)
li	$2,554			# 0x22a
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,56($17)

lw	$2,60($17)
beq	$2,$0,$L409
lw	$28,56($sp)

$L398:
lw	$4,%got(ff_h263_rl_inter)($28)
lw	$25,%call16(init_rl)($28)
lw	$5,%got(ff_h263_static_rl_table_store)($28)
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
move	$16,$4

lw	$28,56($sp)
lw	$5,%got(ff_h263_static_rl_table_store)($28)
lw	$25,%call16(init_rl)($28)
lw	$4,%got(rl_intra_aic)($28)
.reloc	1f,R_MIPS_JALR,init_rl
1:	jalr	$25
addiu	$5,$5,390

li	$2,4			# 0x4
lw	$28,56($sp)
li	$3,2			# 0x2
lw	$8,8($16)
addiu	$4,$16,44
lw	$6,0($16)
li	$5,9			# 0x9
sw	$2,16($sp)
sw	$2,28($sp)
addiu	$7,$8,2
sw	$2,48($sp)
addiu	$6,$6,1
lw	$2,%got(table.6428)($28)
lw	$25,%call16(init_vlc_sparse)($28)
sw	$3,20($sp)
addiu	$2,$2,%lo(table.6428)
sw	$8,24($sp)
sw	$3,32($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$2,48($16)
li	$2,554			# 0x22a
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$2,56($16)

lw	$2,60($16)
beq	$2,$0,$L410
lw	$28,56($sp)

$L400:
li	$2,4			# 0x4
lw	$16,%got(rl_intra_aic)($28)
li	$3,2			# 0x2
lw	$25,%call16(init_vlc_sparse)($28)
sw	$0,36($sp)
li	$5,9			# 0x9
sw	$2,16($sp)
addiu	$4,$16,44
sw	$2,28($sp)
sw	$2,48($sp)
sw	$3,20($sp)
sw	$3,32($sp)
sw	$0,40($sp)
sw	$0,44($sp)
lw	$2,%got(table.6434)($28)
lw	$3,8($16)
lw	$6,0($16)
addiu	$2,$2,%lo(table.6434)
addiu	$7,$3,2
addiu	$6,$6,1
sw	$2,48($16)
li	$2,554			# 0x22a
sw	$2,56($16)
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,24($sp)

lw	$2,60($16)
beq	$2,$0,$L411
lw	$28,56($sp)

$L402:
lw	$3,%got(table.6438)($28)
li	$17,2			# 0x2
lw	$2,%got(h263_mbtype_b_vlc)($28)
li	$16,1			# 0x1
lw	$7,%got(h263_mbtype_b_tab)($28)
li	$18,4			# 0x4
addiu	$3,$3,%lo(table.6438)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$2,$2,%lo(h263_mbtype_b_vlc)
sw	$17,16($sp)
sw	$16,20($sp)
li	$5,6			# 0x6
sw	$7,24($sp)
li	$6,15			# 0xf
sw	$17,28($sp)
addiu	$7,$7,1
sw	$16,32($sp)
move	$4,$2
sw	$18,48($sp)
sw	$0,36($sp)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$3,4($2)
li	$3,80			# 0x50
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

li	$5,3			# 0x3
lw	$28,56($sp)
li	$6,4			# 0x4
sw	$17,16($sp)
sw	$16,20($sp)
sw	$17,28($sp)
lw	$3,%got(cbpc_b_tab)($28)
lw	$2,%got(cbpc_b_vlc)($28)
lw	$25,%call16(init_vlc_sparse)($28)
addiu	$7,$3,1
sw	$16,32($sp)
sw	$3,24($sp)
addiu	$2,$2,%lo(cbpc_b_vlc)
lw	$3,%got(table.6439)($28)
sw	$18,48($sp)
move	$4,$2
sw	$0,36($sp)
addiu	$3,$3,%lo(table.6439)
sw	$0,40($sp)
sw	$0,44($sp)
sw	$3,4($2)
li	$3,8			# 0x8
.reloc	1f,R_MIPS_JALR,init_vlc_sparse
1:	jalr	$25
sw	$3,12($2)

lw	$31,84($sp)
lw	$19,80($sp)
lw	$18,76($sp)
lw	$17,72($sp)
lw	$16,68($sp)
j	$31
addiu	$sp,$sp,88

$L409:
lw	$3,%got(rl_vlc_table.6421)($28)
addiu	$2,$17,60
addiu	$17,$17,188
addiu	$3,$3,%lo(rl_vlc_table.6421)
$L399:
sw	$3,0($2)
addiu	$2,$2,4
bne	$2,$17,$L399
addiu	$3,$3,2216

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(ff_h263_rl_inter_hw)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

b	$L398
lw	$28,56($sp)

$L411:
lw	$3,%got(rl_vlc_table.6433)($28)
addiu	$2,$16,60
addiu	$16,$16,188
addiu	$3,$3,%lo(rl_vlc_table.6433)
$L403:
sw	$3,0($2)
addiu	$2,$2,4
bne	$2,$16,$L403
addiu	$3,$3,2216

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(rl_intra_aic)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

b	$L402
lw	$28,56($sp)

$L410:
lw	$3,%got(rl_vlc_table.6427)($28)
addiu	$2,$16,60
addiu	$16,$16,188
addiu	$3,$3,%lo(rl_vlc_table.6427)
$L401:
sw	$3,0($2)
addiu	$2,$2,4
bne	$2,$16,$L401
addiu	$3,$3,2216

lw	$25,%call16(init_vlc_rl)($28)
lw	$4,%got(ff_h263_rl_inter)($28)
.reloc	1f,R_MIPS_JALR,init_vlc_rl
1:	jalr	$25
nop

b	$L400
lw	$28,56($sp)

.set	macro
.set	reorder
.end	mpeg4_decode_init_vlc
.size	mpeg4_decode_init_vlc, .-mpeg4_decode_init_vlc
.section	.text.mpeg4_decode_motion,"ax",@progbits
.align	2
.align	5
.globl	mpeg4_decode_motion
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_motion
.type	mpeg4_decode_motion, @function
mpeg4_decode_motion:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,%got(mv_vlc_hw+4)($28)
lw	$9,%lo(mv_vlc_hw+4)($2)
li	$2,127			# 0x7f
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
sll	$8,$8,2
addu	$8,$9,$8
lh	$3,2($8)
.set	noreorder
.set	nomacro
bltz	$3,$L430
lh	$7,0($8)
.set	macro
.set	reorder

$L414:
addiu	$3,$3,-1
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$7,$0,$L422
bltz	$7,$L423
li	$2,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$6,-1
.set	noreorder
.set	nomacro
beq	$2,$0,$L416
addiu	$8,$7,-1
.set	macro
.set	reorder

slt	$9,$2,9
.set	noreorder
.set	nomacro
beq	$9,$0,$L431
sll	$7,$8,$2
.set	macro
.set	reorder

addiu	$2,$6,-2
andi	$2,$2,0x7
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 112 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
or	$8,$9,$7
addiu	$7,$8,1
$L416:
sltu	$2,$0,$3
lw	$4,2952($4)
subu	$3,$0,$2
xor	$3,$7,$3
addu	$3,$3,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L432
addu	$7,$5,$3
.set	macro
.set	reorder

slt	$2,$5,-31
.set	noreorder
.set	nomacro
beq	$2,$0,$L420
slt	$5,$5,33
.set	macro
.set	reorder

addiu	$5,$7,64
slt	$2,$7,-63
movn	$7,$5,$2
.set	noreorder
.set	nomacro
j	$31
move	$2,$7
.set	macro
.set	reorder

$L423:
.set	noreorder
.set	nomacro
j	$31
li	$2,65535			# 0xffff
.set	macro
.set	reorder

$L422:
.set	noreorder
.set	nomacro
j	$31
move	$2,$5
.set	macro
.set	reorder

$L430:
li	$2,125			# 0x7d
#APP
# 79 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$3,$3,4
ori	$3,$3,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$7,$2,$7
sll	$7,$7,2
addu	$9,$9,$7
lh	$7,0($9)
.set	noreorder
.set	nomacro
b	$L414
lh	$3,2($9)
.set	macro
.set	reorder

$L432:
li	$2,27			# 0x1b
subu	$5,$2,$6
sll	$7,$7,$5
.set	noreorder
.set	nomacro
j	$31
sra	$2,$7,$5
.set	macro
.set	reorder

$L420:
bne	$5,$0,$L425
slt	$2,$7,64
bne	$2,$0,$L425
.set	noreorder
.set	nomacro
j	$31
addiu	$2,$7,-64
.set	macro
.set	reorder

$L425:
.set	noreorder
.set	nomacro
j	$31
move	$2,$7
.set	macro
.set	reorder

$L431:
li	$2,125			# 0x7d
#APP
# 98 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 99 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 100 "vlc_bs.c" 1
mfc0	$10,$21,1
# 0 "" 2
#NO_APP
addiu	$2,$6,-2
andi	$2,$2,0x7
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 104 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 105 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 106 "vlc_bs.c" 1
mfc0	$8,$21,1
# 0 "" 2
#NO_APP
addiu	$9,$6,-9
sll	$10,$10,$9
or	$9,$10,$8
or	$8,$9,$7
.set	noreorder
.set	nomacro
b	$L416
addiu	$7,$8,1
.set	macro
.set	reorder

.end	mpeg4_decode_motion
.size	mpeg4_decode_motion, .-mpeg4_decode_motion
.section	.text.mpeg4_pred_motion,"ax",@progbits
.align	2
.align	5
.globl	mpeg4_pred_motion
.set	nomips16
.set	nomicromips
.ent	mpeg4_pred_motion
.type	mpeg4_pred_motion, @function
mpeg4_pred_motion:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$2,$5,2002
lw	$3,10296($4)
addiu	$6,$6,546
lw	$8,172($4)
sll	$2,$2,2
sll	$6,$6,2
addu	$2,$4,$2
addu	$6,$4,$6
lw	$2,4($2)
lw	$6,0($6)
sll	$2,$2,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L434
addu	$2,$6,$2
.set	macro
.set	reorder

slt	$3,$5,3
bne	$3,$0,$L456
$L434:
lw	$6,%got(off.6462)($28)
sll	$5,$5,2
sll	$4,$8,2
lh	$9,-4($2)
addiu	$6,$6,%lo(off.6462)
subu	$4,$2,$4
addu	$5,$5,$6
lw	$3,0($5)
subu	$3,$3,$8
sll	$3,$3,2
addu	$3,$2,$3
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000010010000000001101111	#S32I2M XR1,$9
# 0 "" 2
#NO_APP
lh	$5,0($4)
#APP
# 135 "mpeg4_p0.c" 1
.word	0b01110000000001010000000010101111	#S32I2M XR2,$5
# 0 "" 2
#NO_APP
lh	$5,0($3)
#APP
# 136 "mpeg4_p0.c" 1
.word	0b01110000000001010000000011101111	#S32I2M XR3,$5
# 0 "" 2
# 138 "mpeg4_p0.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 139 "mpeg4_p0.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 140 "mpeg4_p0.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 141 "mpeg4_p0.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 143 "mpeg4_p0.c" 1
.word	0b01110000000001100000000111101110	#S32M2I XR7, $6
# 0 "" 2
#NO_APP
lh	$5,-2($2)
sw	$6,0($7)
$L461:
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000001010000000001101111	#S32I2M XR1,$5
# 0 "" 2
#NO_APP
lh	$4,2($4)
$L453:
#APP
# 135 "mpeg4_p0.c" 1
.word	0b01110000000001000000000010101111	#S32I2M XR2,$4
# 0 "" 2
#NO_APP
lh	$3,2($3)
$L462:
#APP
# 136 "mpeg4_p0.c" 1
.word	0b01110000000000110000000011101111	#S32I2M XR3,$3
# 0 "" 2
# 138 "mpeg4_p0.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 139 "mpeg4_p0.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 140 "mpeg4_p0.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 141 "mpeg4_p0.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 143 "mpeg4_p0.c" 1
.word	0b01110000000000110000000111101110	#S32M2I XR7, $3
# 0 "" 2
#NO_APP
lw	$4,16($sp)
.set	noreorder
.set	nomacro
j	$31
sw	$3,0($4)
.set	macro
.set	reorder

$L456:
.set	noreorder
.set	nomacro
beq	$5,$0,$L457
li	$3,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$5,$3,$L458
sll	$3,$8,2
.set	macro
.set	reorder

lw	$9,7992($4)
lw	$6,9784($4)
sll	$5,$5,2
subu	$4,$2,$3
lw	$3,%got(off.6462)($28)
addiu	$3,$3,%lo(off.6462)
addu	$5,$5,$3
lw	$3,0($5)
subu	$3,$3,$8
sll	$3,$3,2
.set	noreorder
.set	nomacro
beq	$9,$6,$L442
addu	$3,$2,$3
.set	macro
.set	reorder

lh	$6,-4($2)
lh	$5,-2($2)
$L443:
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000001100000000001101111	#S32I2M XR1,$6
# 0 "" 2
#NO_APP
lh	$6,0($4)
#APP
# 135 "mpeg4_p0.c" 1
.word	0b01110000000001100000000010101111	#S32I2M XR2,$6
# 0 "" 2
#NO_APP
lh	$6,0($3)
#APP
# 136 "mpeg4_p0.c" 1
.word	0b01110000000001100000000011101111	#S32I2M XR3,$6
# 0 "" 2
# 138 "mpeg4_p0.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 139 "mpeg4_p0.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 140 "mpeg4_p0.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 141 "mpeg4_p0.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 143 "mpeg4_p0.c" 1
.word	0b01110000000001100000000111101110	#S32M2I XR7, $6
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L461
sw	$6,0($7)
.set	macro
.set	reorder

$L457:
lw	$6,7992($4)
lw	$3,9784($4)
.set	noreorder
.set	nomacro
beq	$6,$3,$L459
addiu	$9,$6,1
.set	macro
.set	reorder

beq	$3,$9,$L460
$L441:
lh	$4,-4($2)
lh	$3,-2($2)
sw	$4,0($7)
lw	$4,16($sp)
.set	noreorder
.set	nomacro
j	$31
sw	$3,0($4)
.set	macro
.set	reorder

$L460:
lw	$3,32($4)
.set	noreorder
.set	nomacro
beq	$3,$0,$L441
li	$3,2			# 0x2
.set	macro
.set	reorder

subu	$3,$3,$8
sll	$3,$3,2
.set	noreorder
.set	nomacro
bne	$6,$0,$L439
addu	$3,$2,$3
.set	macro
.set	reorder

lh	$4,0($3)
lh	$3,2($3)
sw	$4,0($7)
lw	$4,16($sp)
.set	noreorder
.set	nomacro
j	$31
sw	$3,0($4)
.set	macro
.set	reorder

$L458:
lw	$3,7992($4)
lw	$6,9784($4)
addiu	$3,$3,1
bne	$3,$6,$L441
lw	$3,32($4)
.set	noreorder
.set	nomacro
beq	$3,$0,$L441
subu	$3,$5,$8
.set	macro
.set	reorder

lh	$4,-4($2)
sll	$3,$3,2
addu	$3,$2,$3
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000001000000000001101111	#S32I2M XR1,$4
# 0 "" 2
#NO_APP
move	$4,$0
#APP
# 135 "mpeg4_p0.c" 1
.word	0b01110000000001000000000010101111	#S32I2M XR2,$4
# 0 "" 2
#NO_APP
lh	$5,0($3)
#APP
# 136 "mpeg4_p0.c" 1
.word	0b01110000000001010000000011101111	#S32I2M XR3,$5
# 0 "" 2
# 138 "mpeg4_p0.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 139 "mpeg4_p0.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 140 "mpeg4_p0.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 141 "mpeg4_p0.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 143 "mpeg4_p0.c" 1
.word	0b01110000000001100000000111101110	#S32M2I XR7, $6
# 0 "" 2
#NO_APP
lh	$5,-2($2)
sw	$6,0($7)
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000001010000000001101111	#S32I2M XR1,$5
# 0 "" 2
#NO_APP
b	$L453
$L442:
move	$5,$0
sh	$0,-2($2)
move	$6,$0
.set	noreorder
.set	nomacro
b	$L443
sh	$0,-4($2)
.set	macro
.set	reorder

$L459:
lw	$3,16($sp)
sw	$0,0($3)
.set	noreorder
.set	nomacro
j	$31
sw	$0,0($7)
.set	macro
.set	reorder

$L439:
lh	$4,-4($2)
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000001000000000001101111	#S32I2M XR1,$4
# 0 "" 2
# 135 "mpeg4_p0.c" 1
.word	0b01110000000001010000000010101111	#S32I2M XR2,$5
# 0 "" 2
#NO_APP
lh	$4,0($3)
#APP
# 136 "mpeg4_p0.c" 1
.word	0b01110000000001000000000011101111	#S32I2M XR3,$4
# 0 "" 2
# 138 "mpeg4_p0.c" 1
.word	0b01110000000001001000010100000011	#S32MIN XR4,XR1,XR2
# 0 "" 2
# 139 "mpeg4_p0.c" 1
.word	0b01110000000000001000010101000011	#S32MAX XR5,XR1,XR2
# 0 "" 2
# 140 "mpeg4_p0.c" 1
.word	0b01110000000000001101000110000011	#S32MAX XR6,XR4,XR3
# 0 "" 2
# 141 "mpeg4_p0.c" 1
.word	0b01110000000001010101100111000011	#S32MIN XR7,XR6,XR5
# 0 "" 2
# 143 "mpeg4_p0.c" 1
.word	0b01110000000001100000000111101110	#S32M2I XR7, $6
# 0 "" 2
#NO_APP
lh	$4,-2($2)
sw	$6,0($7)
#APP
# 134 "mpeg4_p0.c" 1
.word	0b01110000000001000000000001101111	#S32I2M XR1,$4
# 0 "" 2
# 135 "mpeg4_p0.c" 1
.word	0b01110000000001010000000010101111	#S32I2M XR2,$5
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L462
lh	$3,2($3)
.set	macro
.set	reorder

.end	mpeg4_pred_motion
.size	mpeg4_pred_motion, .-mpeg4_pred_motion
.section	.rodata.str1.4
.align	2
$LC13:
.ascii	"cbpc damaged at %d %d\012\000"
.align	2
$LC14:
.ascii	"illegal MB_type\012\000"
.align	2
$LC15:
.ascii	"I cbpc damaged at %d %d\012\000"
.align	2
$LC16:
.ascii	"I cbpy damaged at %d %d\012\000"
.section	.text.mpeg4_decode_mb_p0,"ax",@progbits
.align	2
.align	5
.globl	mpeg4_decode_mb_p0
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_mb_p0
.type	mpeg4_decode_mb_p0, @function
mpeg4_decode_mb_p0:
.frame	$sp,96,$31		# vars= 24, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$2,168($4)
addiu	$sp,$sp,-96
lw	$9,7996($4)
lw	$7,7992($4)
lw	$10,2904($4)
sw	$18,64($sp)
move	$18,$4
mul	$4,$9,$2
.cprestore	24
li	$2,2			# 0x2
sw	$17,60($sp)
sw	$31,92($sp)
move	$17,$5
sw	$fp,88($sp)
sw	$23,84($sp)
sw	$22,80($sp)
sw	$21,76($sp)
addu	$3,$4,$7
sw	$20,72($sp)
sw	$19,68($sp)
sw	$16,56($sp)
.set	noreorder
.set	nomacro
beq	$10,$2,$L464
sw	$3,44($sp)
.set	macro
.set	reorder

li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$10,$2,$L464
li	$2,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$10,$2,$L882
li	$3,89			# 0x59
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L804
li	$4,8			# 0x8
.set	macro
.set	reorder

$L593:
.set	noreorder
.set	nomacro
bne	$2,$4,$L883
andi	$6,$2,0x4
.set	macro
.set	reorder

$L804:
#APP
# 264 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 265 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 266 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bgez	$2,$L593
lw	$6,%got($LC15)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$4,0($18)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC15)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$9,16($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
$L850:
lw	$31,92($sp)
$L956:
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L464:
lw	$2,%got(ff_h263_inter_MCBPC_vlc_hw)($28)
li	$5,13			# 0xd
li	$6,111			# 0x6f
li	$11,109			# 0x6d
li	$8,20			# 0x14
.set	noreorder
.set	nomacro
b	$L493
lw	$4,4($2)
.set	macro
.set	reorder

$L490:
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bltz	$3,$L884
$L491:
.set	noreorder
.set	nomacro
bne	$3,$8,$L885
move	$2,$3
.set	macro
.set	reorder

$L493:
#APP
# 120 "vlc_bs.c" 1
mtc0	$5,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$2,$0,$L886
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

#APP
# 154 "vlc_bs.c" 1
mtc0	$6,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,2
addu	$3,$4,$3
lh	$2,2($3)
.set	noreorder
.set	nomacro
bgez	$2,$L490
lh	$3,0($3)
.set	macro
.set	reorder

#APP
# 79 "vlc_bs.c" 1
mtc0	$11,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$12,$21,1
# 0 "" 2
#NO_APP
nor	$2,$0,$2
sll	$2,$2,4
ori	$2,$2,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
addu	$3,$2,$3
sll	$3,$3,2
addu	$3,$4,$3
lh	$2,2($3)
lh	$3,0($3)
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
bgez	$3,$L491
$L884:
lw	$6,%got($LC13)($28)
li	$5,16			# 0x10
lw	$4,0($18)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC13)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$9,16($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L882:
sw	$0,8004($18)
.set	noreorder
.set	nomacro
bne	$7,$0,$L532
sw	$0,10036($18)
.set	macro
.set	reorder

sw	$0,7360($18)
sw	$0,7356($18)
sw	$0,7352($18)
sw	$0,7348($18)
sw	$0,7376($18)
sw	$0,7372($18)
sw	$0,7368($18)
sw	$0,7364($18)
$L532:
lw	$2,980($18)
lw	$3,44($sp)
addu	$2,$2,$3
lbu	$2,0($2)
.set	noreorder
.set	nomacro
beq	$2,$0,$L533
sw	$2,2832($18)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$4,2192($18)
sll	$3,$3,2
sw	$0,7264($18)
sw	$2,8680($18)
addu	$3,$4,$3
sw	$2,8684($18)
li	$4,14344			# 0x3808
sw	$2,8688($18)
sw	$2,8692($18)
sw	$2,8696($18)
sw	$2,8700($18)
li	$2,1			# 0x1
sw	$2,7260($18)
lw	$2,%got(dMB)($28)
lw	$2,0($2)
sw	$0,8($2)
sw	$0,7268($18)
sw	$0,12($2)
sw	$0,7272($18)
sw	$0,40($2)
sw	$0,7300($18)
sw	$0,44($2)
sw	$0,7304($18)
sw	$4,0($3)
sw	$0,72($2)
$L488:
lw	$3,52($18)
li	$2,13			# 0xd
beq	$3,$2,$L887
$L707:
move	$2,$0
$L919:
lw	$31,92($sp)
$L957:
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L887:
#APP
# 610 "mpeg4_p0.c" 1
mfc0	$3,$21,3
# 0 "" 2
#NO_APP
srl	$3,$3,5
#APP
# 611 "mpeg4_p0.c" 1
mfc0	$4,$21,2
# 0 "" 2
#NO_APP
andi	$2,$3,0x1f
lw	$5,88($18)
srl	$6,$2,3
lw	$2,%got(mpeg4_hw_bs_buffer)($28)
andi	$3,$3,0x7
lw	$9,10332($18)
andi	$5,$5,0x10
lw	$2,0($2)
subu	$2,$4,$2
addu	$2,$2,$6
sll	$2,$2,3
addu	$7,$2,$3
srl	$2,$7,3
sw	$7,10340($18)
addu	$2,$9,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$5,$0,$L707
li	$11,16711680			# 0xff0000
.set	macro
.set	reorder

li	$10,-16777216			# 0xffffffffff000000
sll	$2,$3,8
addiu	$11,$11,255
srl	$3,$3,8
ori	$10,$10,0xff00
and	$2,$2,$10
and	$3,$3,$11
or	$3,$2,$3
sll	$2,$3,16
srl	$3,$3,16
andi	$4,$7,0x7
or	$2,$2,$3
sll	$3,$2,$4
srl	$3,$3,16
sltu	$4,$3,256
.set	noreorder
.set	nomacro
beq	$4,$0,$L708
move	$2,$3
.set	macro
.set	reorder

lw	$8,2904($18)
li	$4,3			# 0x3
.set	noreorder
.set	nomacro
beq	$8,$4,$L708
li	$4,1			# 0x1
.set	macro
.set	reorder

li	$12,8			# 0x8
subu	$12,$12,$8
sra	$3,$3,$12
bne	$3,$4,$L708
lw	$3,10084($18)
bne	$3,$0,$L708
addiu	$8,$8,8
move	$6,$7
.set	noreorder
.set	nomacro
b	$L709
li	$13,1			# 0x1
.set	macro
.set	reorder

$L710:
bne	$4,$13,$L708
$L709:
addu	$6,$6,$8
addu	$7,$7,$8
srl	$3,$6,3
sw	$6,10340($18)
andi	$5,$6,0x7
addu	$3,$9,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$11
and	$4,$4,$10
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
or	$2,$3,$4
sll	$2,$2,$5
srl	$2,$2,16
sltu	$3,$2,256
.set	noreorder
.set	nomacro
bne	$3,$0,$L710
sra	$4,$2,$12
.set	macro
.set	reorder

$L708:
lw	$4,10344($18)
addiu	$3,$7,8
slt	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L711
andi	$3,$7,0x7
.set	macro
.set	reorder

nor	$3,$0,$7
li	$4,127			# 0x7f
andi	$3,$3,0x7
sra	$2,$2,8
sra	$3,$4,$3
or	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$2,$4,$L919
move	$2,$0
.set	macro
.set	reorder

$L712:
lw	$2,7992($18)
lw	$3,160($18)
lw	$5,2904($18)
addiu	$2,$2,1
xor	$4,$2,$3
li	$2,1			# 0x1
li	$3,2			# 0x2
movn	$3,$2,$4
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
bne	$5,$2,$L850
li	$2,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

lw	$2,980($18)
addu	$2,$2,$3
lw	$3,44($sp)
addu	$2,$2,$3
lbu	$2,0($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L707
li	$2,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L956
lw	$31,92($sp)
.set	macro
.set	reorder

$L883:
li	$3,1			# 0x1
sw	$3,8004($18)
$L494:
li	$3,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
lw	$7,44($sp)
li	$8,513			# 0x201
lw	$5,2192($18)
sw	$3,2824($18)
sll	$4,$7,2
li	$7,1			# 0x1
addu	$4,$5,$4
movn	$7,$8,$3
li	$3,131072			# 0x20000
addiu	$3,$3,89
sw	$7,0($4)
#APP
# 282 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 283 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 284 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bltz	$3,$L888
sll	$3,$3,2
.set	macro
.set	reorder

lw	$5,2872($18)
lw	$4,10108($18)
andi	$2,$2,0x3
or	$fp,$3,$2
slt	$2,$5,$4
.set	noreorder
.set	nomacro
bne	$6,$0,$L889
sw	$2,10112($18)
.set	macro
.set	reorder

$L596:
lw	$2,10364($18)
.set	noreorder
.set	nomacro
bne	$2,$0,$L941
addiu	$3,$17,768
.set	macro
.set	reorder

li	$2,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sw	$2,10448($18)
addiu	$3,$17,768
$L941:
move	$2,$17
$L598:
#APP
# 2033 "mpeg4_p0.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 2034 "mpeg4_p0.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 2035 "mpeg4_p0.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 2037 "mpeg4_p0.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 2038 "mpeg4_p0.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 2039 "mpeg4_p0.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 2040 "mpeg4_p0.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 2041 "mpeg4_p0.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
addiu	$2,$2,32
.set	noreorder
.set	nomacro
bne	$2,$3,$L598
addiu	$22,$18,8012
.set	macro
.set	reorder

li	$3,-4			# 0xfffffffffffffffc
lw	$2,10112($18)
move	$23,$0
sw	$3,40($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L599
andi	$21,$fp,0x20
.set	macro
.set	reorder

$L897:
slt	$20,$23,4
.set	noreorder
.set	nomacro
beq	$20,$0,$L600
li	$4,393216			# 0x60000
.set	macro
.set	reorder

li	$2,262144			# 0x40000
addiu	$2,$2,121
#APP
# 302 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 303 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 305 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sltu	$2,$3,10
$L932:
.set	noreorder
.set	nomacro
beq	$2,$0,$L927
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$0,$L603
move	$9,$0
.set	macro
.set	reorder

li	$2,9			# 0x9
.set	noreorder
.set	nomacro
beq	$3,$2,$L890
addiu	$2,$3,-1
.set	macro
.set	reorder

sll	$4,$2,4
ori	$4,$4,0xd
#APP
# 112 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
sra	$2,$9,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L603
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

sll	$2,$2,$3
or	$2,$9,$2
addiu	$9,$2,1
$L603:
lw	$2,0($22)
$L939:
lw	$3,2780($18)
lw	$5,2784($18)
lw	$6,2728($18)
sll	$2,$2,1
movn	$5,$3,$20
addu	$6,$6,$2
#APP
# 248 "mpeg4_p0.c" 1
.word	0b01110000110101111111110001101010	#S16LDD XR1,$6,-2,PTN2
# 0 "" 2
#NO_APP
lw	$3,24($22)
nor	$3,$0,$3
sll	$3,$3,1
addu	$2,$6,$3
#APP
# 249 "mpeg4_p0.c" 1
.word	0b01110000010100000000000010101010	#S16LDD XR2,$2,0,PTN2
# 0 "" 2
#NO_APP
addiu	$3,$3,2
addu	$3,$6,$3
#APP
# 250 "mpeg4_p0.c" 1
.word	0b01110000011100000000000011101010	#S16LDD XR3,$3,0,PTN2
# 0 "" 2
#NO_APP
lw	$2,10296($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L609
andi	$2,$23,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L610
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 254 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
# 255 "mpeg4_p0.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
#NO_APP
$L610:
lw	$7,7992($18)
lw	$2,9784($18)
.set	noreorder
.set	nomacro
beq	$7,$2,$L891
li	$3,1			# 0x1
.set	macro
.set	reorder

$L611:
#APP
# 270 "mpeg4_p0.c" 1
.word	0b01110011000000001000110100011000	#D32ADD XR4,XR3,XR2,XR0,SS
# 0 "" 2
# 271 "mpeg4_p0.c" 1
.word	0b01110011000000001000010101011000	#D32ADD XR5,XR1,XR2,XR0,SS
# 0 "" 2
# 272 "mpeg4_p0.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 273 "mpeg4_p0.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 274 "mpeg4_p0.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 275 "mpeg4_p0.c" 1
.word	0b01110000000101001101010001111001	#S32MOVN XR1,XR5,XR3
# 0 "" 2
# 276 "mpeg4_p0.c" 1
.word	0b01110000000000110000000001101110	#S32M2I XR1, $3
# 0 "" 2
# 277 "mpeg4_p0.c" 1
.word	0b01110000000100110000000101101110	#S32M2I XR5, $19
# 0 "" 2
#NO_APP
sra	$2,$5,1
lw	$8,9816($18)
move	$4,$3
addu	$3,$4,$2
slt	$4,$8,3
teq	$5,$0,7
div	$0,$3,$5
sltu	$19,$0,$19
mflo	$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L614
addu	$2,$9,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L892
mul	$3,$5,$2
.set	macro
.set	reorder

addiu	$4,$5,2048
slt	$4,$4,$3
.set	noreorder
.set	nomacro
bne	$4,$0,$L893
li	$5,16			# 0x10
.set	macro
.set	reorder

$L617:
li	$4,-2048			# 0xfffffffffffff800
and	$4,$3,$4
beq	$4,$0,$L868
bltz	$3,$L731
lw	$4,88($18)
andi	$4,$4,0x1000
.set	noreorder
.set	nomacro
bne	$4,$0,$L942
sll	$3,$3,16
.set	macro
.set	reorder

li	$3,2047			# 0x7ff
sh	$3,0($6)
$L616:
.set	noreorder
.set	nomacro
bltz	$2,$L618
move	$16,$0
.set	macro
.set	reorder

sh	$2,0($17)
$L621:
beq	$21,$0,$L633
lw	$2,2824($18)
.set	noreorder
.set	nomacro
bne	$2,$0,$L634
addiu	$4,$18,8732
.set	macro
.set	reorder

$L635:
lw	$12,%got(JZC_mpeg4_intra_level)($28)
li	$9,524288			# 0x80000
lw	$11,%got(JZC_mpeg4_intra_run)($28)
li	$10,102			# 0x66
lw	$15,%got(ff_mpeg4_rl_intra)($28)
addiu	$9,$9,121
li	$8,13			# 0xd
li	$25,109			# 0x6d
li	$24,93			# 0x5d
li	$21,125			# 0x7d
move	$14,$12
.set	noreorder
.set	nomacro
b	$L647
move	$13,$11
.set	macro
.set	reorder

$L895:
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L638
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L639
#APP
# 112 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
andi	$7,$6,0x40
andi	$6,$6,0x3f
#APP
# 112 "vlc_bs.c" 1
mtc0	$24,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 112 "vlc_bs.c" 1
mtc0	$21,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,8
addu	$5,$2,$3
andi	$2,$5,0x2000
.set	noreorder
.set	nomacro
beq	$2,$0,$L618
andi	$2,$5,0x1000
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L640
sra	$2,$5,1
.set	macro
.set	reorder

li	$3,-4096			# 0xfffffffffffff000
or	$2,$2,$3
$L641:
andi	$5,$5,0x1
.set	noreorder
.set	nomacro
beq	$5,$0,$L618
addiu	$3,$2,2048
.set	macro
.set	reorder

sltu	$3,$3,4096
bne	$3,$0,$L642
lw	$3,9816($18)
slt	$3,$3,3
.set	noreorder
.set	nomacro
bne	$3,$0,$L943
li	$5,2047			# 0x7ff
.set	macro
.set	reorder

addiu	$3,$2,2560
sltu	$3,$3,5121
beq	$3,$0,$L872
$L943:
li	$3,-2048			# 0xfffffffffffff800
slt	$2,$2,0
movn	$5,$3,$2
move	$2,$5
$L642:
addiu	$6,$6,1
addu	$16,$16,$6
addiu	$3,$16,192
movn	$16,$3,$7
$L644:
slt	$3,$16,63
.set	noreorder
.set	nomacro
beq	$3,$0,$L894
addu	$3,$4,$16
.set	macro
.set	reorder

$L944:
lbu	$3,0($3)
sll	$3,$3,1
addu	$3,$17,$3
sh	$2,0($3)
$L647:
#APP
# 342 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 343 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 344 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
slt	$2,$3,103
movz	$3,$10,$2
move	$2,$3
addu	$3,$12,$3
sll	$2,$2,2
lb	$3,0($3)
addu	$2,$11,$2
.set	noreorder
.set	nomacro
beq	$3,$0,$L895
lw	$2,0($2)
.set	macro
.set	reorder

#APP
# 1308 "mpeg4_p0.c" 1
mtc0	$8,$21,0
# 0 "" 2
#NO_APP
addiu	$2,$2,1
addu	$16,$16,$2
#APP
# 1311 "mpeg4_p0.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1312 "mpeg4_p0.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$5,$0,$2
subu	$2,$0,$5
xor	$3,$3,$2
addu	$2,$3,$5
slt	$3,$16,63
.set	noreorder
.set	nomacro
bne	$3,$0,$L944
addu	$3,$4,$16
.set	macro
.set	reorder

$L894:
addiu	$16,$16,-192
li	$3,-64			# 0xffffffffffffffc0
and	$3,$16,$3
bne	$3,$0,$L873
addu	$4,$4,$16
lbu	$3,0($4)
sll	$3,$3,1
addu	$3,$17,$3
sh	$2,0($3)
$L633:
lw	$2,10112($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L896
lw	$3,0($22)
.set	macro
.set	reorder

$L649:
lw	$2,2812($18)
sll	$3,$3,5
lw	$4,2824($18)
lw	$5,2172($18)
.set	noreorder
.set	nomacro
beq	$4,$0,$L670
addu	$2,$2,$3
.set	macro
.set	reorder

bne	$19,$0,$L664
lw	$3,7992($18)
lw	$4,7996($18)
.set	noreorder
.set	nomacro
beq	$3,$0,$L667
lw	$7,168($18)
.set	macro
.set	reorder

mul	$6,$4,$7
lw	$10,2872($18)
addiu	$3,$3,-1
addu	$3,$6,$3
addu	$7,$5,$3
lb	$3,0($7)
.set	noreorder
.set	nomacro
beq	$10,$3,$L667
li	$4,-3			# 0xfffffffffffffffd
.set	macro
.set	reorder

li	$5,1			# 0x1
and	$4,$23,$4
.set	noreorder
.set	nomacro
beq	$4,$5,$L667
sra	$11,$10,1
.set	macro
.set	reorder

addiu	$9,$2,-30
li	$8,1			# 0x1
.set	noreorder
.set	nomacro
b	$L673
li	$12,8			# 0x8
.set	macro
.set	reorder

$L672:
teq	$10,$0,7
div	$0,$4,$10
addiu	$8,$8,1
addiu	$9,$9,2
mflo	$4
addu	$4,$6,$4
.set	noreorder
.set	nomacro
beq	$8,$12,$L670
sh	$4,0($3)
.set	macro
.set	reorder

lb	$3,0($7)
$L673:
sll	$4,$8,3
lh	$5,0($9)
addu	$4,$18,$4
mul	$5,$5,$3
lbu	$3,5664($4)
sll	$3,$3,1
addu	$3,$17,$3
addu	$4,$5,$11
.set	noreorder
.set	nomacro
bgtz	$5,$L672
lhu	$6,0($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L672
subu	$4,$5,$11
.set	macro
.set	reorder

$L638:
#APP
# 342 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 343 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 344 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
slt	$2,$3,103
movz	$3,$10,$2
move	$2,$3
sll	$3,$3,2
addu	$2,$14,$2
addu	$3,$13,$3
lb	$5,0($2)
lw	$7,0($3)
addiu	$6,$7,1
addu	$16,$16,$6
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sra	$6,$6,7
andi	$7,$7,0x3f
addiu	$6,$6,6
sltu	$3,$0,$2
sll	$6,$6,2
subu	$2,$0,$3
addu	$6,$15,$6
lw	$6,4($6)
addu	$7,$6,$7
lb	$6,0($7)
addu	$5,$5,$6
xor	$2,$5,$2
.set	noreorder
.set	nomacro
b	$L644
addu	$2,$2,$3
.set	macro
.set	reorder

$L639:
#APP
# 342 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 343 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 344 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
slt	$2,$3,103
movz	$3,$10,$2
move	$2,$3
sll	$3,$3,2
addu	$2,$14,$2
addu	$3,$13,$3
lb	$7,0($2)
lw	$5,0($3)
addiu	$5,$5,1
sra	$6,$5,7
addiu	$6,$6,8
sll	$6,$6,2
addu	$6,$15,$6
lw	$2,4($6)
addu	$2,$2,$7
lb	$2,0($2)
addu	$5,$5,$2
addiu	$5,$5,1
addu	$16,$16,$5
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sltu	$5,$0,$2
subu	$2,$0,$5
xor	$2,$7,$2
.set	noreorder
.set	nomacro
b	$L644
addu	$2,$2,$5
.set	macro
.set	reorder

$L640:
.set	noreorder
.set	nomacro
b	$L641
andi	$2,$2,0xfff
.set	macro
.set	reorder

$L667:
addiu	$7,$2,-30
li	$6,1			# 0x1
li	$8,8			# 0x8
$L666:
lh	$4,0($7)
sll	$3,$6,4
addiu	$6,$6,1
.set	noreorder
.set	nomacro
beq	$4,$0,$L669
addu	$3,$17,$3
.set	macro
.set	reorder

lhu	$5,0($3)
addu	$4,$4,$5
sh	$4,0($3)
$L669:
.set	noreorder
.set	nomacro
bne	$6,$8,$L666
addiu	$7,$7,2
.set	macro
.set	reorder

$L670:
lh	$3,16($17)
$L933:
sh	$3,2($2)
lh	$3,2($17)
sh	$3,18($2)
#APP
# 581 "mpeg4_p0.c" 1
.word	0b01110010001000000100000110101010	#S16LDD XR6,$17,32,PTN0
# 0 "" 2
# 582 "mpeg4_p0.c" 1
.word	0b01110010001000001000000111101010	#S16LDD XR7,$17,64,PTN0
# 0 "" 2
# 583 "mpeg4_p0.c" 1
.word	0b01110010001010000110000110101010	#S16LDD XR6,$17,48,PTN1
# 0 "" 2
# 584 "mpeg4_p0.c" 1
.word	0b01110010001000001100001000101010	#S16LDD XR8,$17,96,PTN0
# 0 "" 2
# 585 "mpeg4_p0.c" 1
.word	0b01110010001010001010000111101010	#S16LDD XR7,$17,80,PTN1
# 0 "" 2
# 586 "mpeg4_p0.c" 1
.word	0b01110010001010001110001000101010	#S16LDD XR8,$17,112,PTN1
# 0 "" 2
# 588 "mpeg4_p0.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 589 "mpeg4_p0.c" 1
.word	0b01110000010000000000100111010001	#S32STD XR7,$2,8
# 0 "" 2
# 590 "mpeg4_p0.c" 1
.word	0b01110000010000000000111000010001	#S32STD XR8,$2,12
# 0 "" 2
# 592 "mpeg4_p0.c" 1
.word	0b01110010001000000000010010010000	#S32LDD XR2,$17,4
# 0 "" 2
# 593 "mpeg4_p0.c" 1
.word	0b01110010001000000000100011010000	#S32LDD XR3,$17,8
# 0 "" 2
# 594 "mpeg4_p0.c" 1
.word	0b01110010001000000000110100010000	#S32LDD XR4,$17,12
# 0 "" 2
# 596 "mpeg4_p0.c" 1
.word	0b01110000010000000001010010010001	#S32STD XR2,$2,20
# 0 "" 2
# 597 "mpeg4_p0.c" 1
.word	0b01110000010000000001100011010001	#S32STD XR3,$2,24
# 0 "" 2
# 598 "mpeg4_p0.c" 1
.word	0b01110000010000000001110100010001	#S32STD XR4,$2,28
# 0 "" 2
#NO_APP
lw	$3,2824($18)
li	$2,63			# 0x3f
addiu	$22,$22,4
addiu	$23,$23,1
movn	$16,$2,$3
li	$2,6			# 0x6
sll	$fp,$fp,1
lw	$3,40($sp)
addiu	$17,$17,128
addiu	$3,$3,1
sw	$16,664($22)
.set	noreorder
.set	nomacro
beq	$23,$2,$L488
sw	$3,40($sp)
.set	macro
.set	reorder

lw	$2,10112($18)
.set	noreorder
.set	nomacro
bne	$2,$0,$L897
andi	$21,$fp,0x20
.set	macro
.set	reorder

$L599:
lw	$2,0($22)
slt	$20,$23,4
lw	$3,2780($18)
lw	$4,2784($18)
lw	$5,2728($18)
sll	$2,$2,1
movn	$4,$3,$20
addu	$5,$5,$2
#APP
# 248 "mpeg4_p0.c" 1
.word	0b01110000101101111111110001101010	#S16LDD XR1,$5,-2,PTN2
# 0 "" 2
#NO_APP
lw	$2,24($22)
nor	$2,$0,$2
sll	$2,$2,1
addu	$3,$5,$2
#APP
# 249 "mpeg4_p0.c" 1
.word	0b01110000011100000000000010101010	#S16LDD XR2,$3,0,PTN2
# 0 "" 2
#NO_APP
addiu	$2,$2,2
addu	$2,$5,$2
#APP
# 250 "mpeg4_p0.c" 1
.word	0b01110000010100000000000011101010	#S16LDD XR3,$2,0,PTN2
# 0 "" 2
#NO_APP
lw	$2,10296($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L623
andi	$2,$23,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L624
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 254 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
# 255 "mpeg4_p0.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
#NO_APP
$L624:
lw	$7,7992($18)
lw	$2,9784($18)
.set	noreorder
.set	nomacro
beq	$7,$2,$L898
li	$3,1			# 0x1
.set	macro
.set	reorder

$L625:
#APP
# 270 "mpeg4_p0.c" 1
.word	0b01110011000000001000110100011000	#D32ADD XR4,XR3,XR2,XR0,SS
# 0 "" 2
# 271 "mpeg4_p0.c" 1
.word	0b01110011000000001000010101011000	#D32ADD XR5,XR1,XR2,XR0,SS
# 0 "" 2
# 272 "mpeg4_p0.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 273 "mpeg4_p0.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 274 "mpeg4_p0.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 275 "mpeg4_p0.c" 1
.word	0b01110000000101001101010001111001	#S32MOVN XR1,XR5,XR3
# 0 "" 2
# 276 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101110	#S32M2I XR1, $2
# 0 "" 2
# 277 "mpeg4_p0.c" 1
.word	0b01110000000100110000000101101110	#S32M2I XR5, $19
# 0 "" 2
#NO_APP
sra	$8,$4,1
lw	$6,9816($18)
move	$3,$2
addu	$2,$3,$8
slt	$3,$6,3
teq	$4,$0,7
div	$0,$2,$4
sltu	$19,$0,$19
.set	noreorder
.set	nomacro
bne	$3,$0,$L628
mflo	$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L899
mul	$2,$4,$2
.set	macro
.set	reorder

addiu	$3,$4,2048
slt	$3,$3,$2
.set	noreorder
.set	nomacro
bne	$3,$0,$L900
lw	$6,%got($LC9)($28)
.set	macro
.set	reorder

$L630:
li	$3,-2048			# 0xfffffffffffff800
and	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L869
li	$16,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

bltz	$2,$L733
lw	$3,88($18)
andi	$3,$3,0x1000
.set	noreorder
.set	nomacro
bne	$3,$0,$L945
sll	$2,$2,16
.set	macro
.set	reorder

li	$2,2047			# 0x7ff
li	$16,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L621
sh	$2,0($5)
.set	macro
.set	reorder

$L869:
sll	$2,$2,16
$L945:
sra	$2,$2,16
.set	noreorder
.set	nomacro
b	$L621
sh	$2,0($5)
.set	macro
.set	reorder

$L868:
sll	$3,$3,16
$L942:
sra	$3,$3,16
.set	noreorder
.set	nomacro
b	$L616
sh	$3,0($6)
.set	macro
.set	reorder

$L664:
lw	$3,24($22)
lw	$4,7996($18)
lw	$6,7992($18)
sll	$3,$3,5
lw	$8,168($18)
.set	noreorder
.set	nomacro
beq	$4,$0,$L676
subu	$3,$2,$3
.set	macro
.set	reorder

mul	$7,$4,$8
lw	$11,2872($18)
addu	$4,$7,$6
subu	$4,$4,$8
addu	$8,$5,$4
lb	$4,0($8)
.set	noreorder
.set	nomacro
beq	$11,$4,$L676
addiu	$5,$23,-2
.set	macro
.set	reorder

sltu	$5,$5,2
.set	noreorder
.set	nomacro
bne	$5,$0,$L676
addiu	$9,$3,18
.set	macro
.set	reorder

sra	$12,$11,1
addiu	$10,$18,5665
.set	noreorder
.set	nomacro
b	$L681
addiu	$3,$3,32
.set	macro
.set	reorder

$L901:
addu	$4,$4,$12
$L680:
teq	$11,$0,7
div	$0,$4,$11
addiu	$9,$9,2
addiu	$10,$10,1
mflo	$4
addu	$4,$6,$4
.set	noreorder
.set	nomacro
beq	$9,$3,$L670
sh	$4,0($5)
.set	macro
.set	reorder

lb	$4,0($8)
$L681:
lh	$5,0($9)
lbu	$6,0($10)
mul	$4,$5,$4
sll	$5,$6,1
addu	$5,$17,$5
.set	noreorder
.set	nomacro
bgtz	$4,$L901
lhu	$6,0($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L680
subu	$4,$4,$12
.set	macro
.set	reorder

$L896:
lw	$5,2780($18)
sll	$2,$3,1
lw	$4,2784($18)
lw	$8,2728($18)
lh	$6,0($17)
movn	$4,$5,$20
addu	$8,$8,$2
#APP
# 248 "mpeg4_p0.c" 1
.word	0b01110001000101111111110001101010	#S16LDD XR1,$8,-2,PTN2
# 0 "" 2
#NO_APP
lw	$2,24($22)
nor	$2,$0,$2
sll	$2,$2,1
addu	$5,$8,$2
#APP
# 249 "mpeg4_p0.c" 1
.word	0b01110000101100000000000010101010	#S16LDD XR2,$5,0,PTN2
# 0 "" 2
#NO_APP
addiu	$2,$2,2
addu	$2,$8,$2
#APP
# 250 "mpeg4_p0.c" 1
.word	0b01110000010100000000000011101010	#S16LDD XR3,$2,0,PTN2
# 0 "" 2
#NO_APP
lw	$2,10296($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L651
andi	$2,$23,0x2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L652
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 254 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
# 255 "mpeg4_p0.c" 1
.word	0b01110000000000100000000011101111	#S32I2M XR3,$2
# 0 "" 2
#NO_APP
$L652:
lw	$7,7992($18)
lw	$2,9784($18)
.set	noreorder
.set	nomacro
beq	$7,$2,$L902
li	$5,1			# 0x1
.set	macro
.set	reorder

$L653:
#APP
# 270 "mpeg4_p0.c" 1
.word	0b01110011000000001000110100011000	#D32ADD XR4,XR3,XR2,XR0,SS
# 0 "" 2
# 271 "mpeg4_p0.c" 1
.word	0b01110011000000001000010101011000	#D32ADD XR5,XR1,XR2,XR0,SS
# 0 "" 2
# 272 "mpeg4_p0.c" 1
.word	0b01110000000000010001000100000111	#S32CPS XR4,XR4,XR4
# 0 "" 2
# 273 "mpeg4_p0.c" 1
.word	0b01110000000000010101010101000111	#S32CPS XR5,XR5,XR5
# 0 "" 2
# 274 "mpeg4_p0.c" 1
.word	0b01110000000000010001010101000110	#S32SLT XR5,XR5,XR4
# 0 "" 2
# 275 "mpeg4_p0.c" 1
.word	0b01110000000101001101010001111001	#S32MOVN XR1,XR5,XR3
# 0 "" 2
# 276 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101110	#S32M2I XR1, $2
# 0 "" 2
# 277 "mpeg4_p0.c" 1
.word	0b01110000000100110000000101101110	#S32M2I XR5, $19
# 0 "" 2
#NO_APP
sra	$10,$4,1
lw	$9,9816($18)
move	$5,$2
addu	$2,$5,$10
slt	$5,$9,3
teq	$4,$0,7
div	$0,$2,$4
sltu	$19,$0,$19
mflo	$2
.set	noreorder
.set	nomacro
bne	$5,$0,$L656
addu	$2,$6,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L903
mul	$5,$4,$2
.set	macro
.set	reorder

addiu	$4,$4,2048
slt	$4,$4,$5
.set	noreorder
.set	nomacro
bne	$4,$0,$L904
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

$L659:
li	$4,-2048			# 0xfffffffffffff800
and	$4,$5,$4
beq	$4,$0,$L870
bltz	$5,$L736
lw	$4,88($18)
andi	$4,$4,0x1000
.set	noreorder
.set	nomacro
bne	$4,$0,$L946
sll	$5,$5,16
.set	macro
.set	reorder

li	$5,2047			# 0x7ff
$L661:
sll	$2,$2,16
sh	$5,0($8)
sra	$2,$2,16
$L658:
srl	$4,$16,31
sh	$2,0($17)
.set	noreorder
.set	nomacro
b	$L649
addu	$16,$16,$4
.set	macro
.set	reorder

$L600:
ori	$4,$4,0x79
#APP
# 323 "vlc_bs.c" 1
mtc0	$4,$21,0
# 0 "" 2
# 325 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 326 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L932
sltu	$2,$3,10
.set	macro
.set	reorder

$L623:
lw	$7,7992($18)
lw	$2,9784($18)
bne	$7,$2,$L625
$L626:
lw	$2,9788($18)
$L934:
lw	$3,7996($18)
addiu	$2,$2,1
bne	$3,$2,$L625
.set	noreorder
.set	nomacro
bne	$23,$0,$L905
lw	$3,40($sp)
.set	macro
.set	reorder

li	$2,1024			# 0x400
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L625
$L628:
.set	noreorder
.set	nomacro
b	$L630
mul	$2,$4,$2
.set	macro
.set	reorder

$L634:
bne	$19,$0,$L636
.set	noreorder
.set	nomacro
b	$L635
addiu	$4,$18,8996
.set	macro
.set	reorder

$L614:
.set	noreorder
.set	nomacro
b	$L617
mul	$3,$5,$2
.set	macro
.set	reorder

$L609:
lw	$7,7992($18)
lw	$2,9784($18)
bne	$7,$2,$L611
$L612:
lw	$2,9788($18)
$L935:
lw	$3,7996($18)
addiu	$2,$2,1
bne	$3,$2,$L611
.set	noreorder
.set	nomacro
beq	$23,$0,$L947
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$3,40($sp)
sltu	$2,$3,2
beq	$2,$0,$L611
li	$2,1024			# 0x400
$L947:
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L611
$L872:
lw	$6,%got($LC10)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
lw	$7,2872($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

$L618:
li	$2,-1			# 0xffffffffffffffff
$L927:
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L676:
addiu	$3,$3,18
addiu	$6,$17,2
addiu	$7,$17,16
$L675:
lh	$4,0($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L678
addiu	$3,$3,2
.set	macro
.set	reorder

lhu	$5,0($6)
addu	$4,$4,$5
sh	$4,0($6)
$L678:
addiu	$6,$6,2
bne	$7,$6,$L675
.set	noreorder
.set	nomacro
b	$L933
lh	$3,16($17)
.set	macro
.set	reorder

$L870:
sll	$5,$5,16
$L946:
.set	noreorder
.set	nomacro
b	$L661
sra	$5,$5,16
.set	macro
.set	reorder

$L886:
sw	$0,8004($18)
li	$3,1			# 0x1
sw	$0,7264($18)
sw	$2,8680($18)
sw	$2,8684($18)
sw	$2,8688($18)
sw	$2,8692($18)
sw	$2,8696($18)
sw	$2,8700($18)
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$10,$2,$L906
sw	$3,7260($18)
.set	macro
.set	reorder

$L467:
lw	$3,44($sp)
lw	$4,%got(dMB)($28)
sll	$2,$3,2
lw	$3,2192($18)
lw	$9,0($4)
addu	$2,$3,$2
li	$3,14344			# 0x3808
sw	$3,0($2)
li	$2,1			# 0x1
sw	$0,10036($18)
sw	$0,8($9)
sw	$0,7268($18)
sw	$0,12($9)
sw	$0,7272($18)
sw	$2,2832($18)
.set	noreorder
.set	nomacro
b	$L488
sw	$0,72($9)
.set	macro
.set	reorder

$L656:
.set	noreorder
.set	nomacro
b	$L659
mul	$5,$4,$2
.set	macro
.set	reorder

$L885:
srl	$4,$3,2
andi	$4,$4,0x1
andi	$6,$3,0x8
.set	noreorder
.set	nomacro
bne	$4,$0,$L494
sw	$4,8004($18)
.set	macro
.set	reorder

addiu	$4,$17,768
move	$2,$17
$L495:
#APP
# 2033 "mpeg4_p0.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 2034 "mpeg4_p0.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 2035 "mpeg4_p0.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 2037 "mpeg4_p0.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 2038 "mpeg4_p0.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 2039 "mpeg4_p0.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 2040 "mpeg4_p0.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 2041 "mpeg4_p0.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$4,$L495
lw	$4,2904($18)
li	$2,4			# 0x4
.set	noreorder
.set	nomacro
beq	$4,$2,$L907
li	$2,2			# 0x2
.set	macro
.set	reorder

andi	$19,$3,0x10
$L498:
sw	$0,10036($18)
$L499:
li	$2,95			# 0x5f
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
lw	$2,%got(ff_h263_cbpy_vlc_hw)($28)
sll	$4,$4,2
lw	$2,4($2)
addu	$4,$2,$4
lh	$2,2($4)
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lh	$16,0($4)
andi	$3,$3,0x3
xori	$16,$16,0xf
sll	$16,$16,2
.set	noreorder
.set	nomacro
bne	$6,$0,$L908
or	$16,$3,$16
.set	macro
.set	reorder

$L500:
lw	$2,10364($18)
.set	noreorder
.set	nomacro
bne	$2,$0,$L948
li	$4,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$16,$0,$L949
li	$3,13			# 0xd
.set	macro
.set	reorder

lw	$3,88($18)
andi	$3,$3,0x4
.set	noreorder
.set	nomacro
beq	$3,$0,$L948
li	$3,13			# 0xd
.set	macro
.set	reorder

$L949:
#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sw	$3,10448($18)
li	$4,1			# 0x1
$L948:
.set	noreorder
.set	nomacro
bne	$19,$0,$L503
sw	$4,7260($18)
.set	macro
.set	reorder

lw	$3,10036($18)
.set	noreorder
.set	nomacro
beq	$3,$0,$L504
lw	$5,44($sp)
.set	macro
.set	reorder

lw	$2,2192($18)
sll	$3,$5,2
addu	$2,$2,$3
li	$3,13320			# 0x3408
sw	$3,0($2)
lw	$9,7080($18)
lw	$13,88($18)
lw	$8,10072($18)
addiu	$9,$9,4
lw	$10,10044($18)
andi	$13,$13,0x20
sw	$0,7264($18)
.set	noreorder
.set	nomacro
bne	$13,$0,$L505
sll	$9,$4,$9
.set	macro
.set	reorder

move	$14,$9
$L506:
lw	$11,9976($18)
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$11,$4,$L909
li	$15,16			# 0x10
.set	macro
.set	reorder

lw	$6,10028($18)
lw	$20,10012($18)
move	$7,$0
lw	$2,7992($18)
addu	$5,$8,$6
lw	$19,10016($18)
lw	$3,7996($18)
addiu	$5,$5,1
lw	$12,9996($18)
sll	$4,$4,$5
mul	$3,$19,$3
subu	$5,$20,$4
mul	$2,$5,$2
sll	$3,$3,4
sll	$2,$2,4
addu	$2,$2,$12
addu	$12,$2,$3
$L512:
li	$3,16			# 0x10
move	$2,$12
$L511:
sra	$4,$2,$6
addiu	$3,$3,-1
addu	$7,$7,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L511
addu	$2,$2,$5
.set	macro
.set	reorder

addiu	$15,$15,-1
.set	noreorder
.set	nomacro
bne	$15,$0,$L512
addu	$12,$12,$19
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$7,$L513
li	$2,1			# 0x1
.set	macro
.set	reorder

addiu	$3,$8,8
subu	$3,$3,$10
sll	$2,$2,$3
sra	$2,$2,1
addu	$7,$2,$7
sra	$3,$7,$3
$L509:
subu	$12,$0,$14
slt	$2,$3,$12
.set	noreorder
.set	nomacro
bne	$2,$0,$L514
slt	$2,$3,$14
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L514
move	$12,$3
.set	macro
.set	reorder

addiu	$12,$14,-1
$L514:
sra	$2,$9,$10
li	$5,1			# 0x1
.set	noreorder
.set	nomacro
beq	$11,$5,$L910
movn	$9,$2,$13
.set	macro
.set	reorder

lw	$7,10028($18)
li	$13,16			# 0x10
lw	$6,10020($18)
move	$4,$0
lw	$3,10024($18)
addu	$14,$8,$7
lw	$2,7992($18)
lw	$11,7996($18)
addiu	$14,$14,1
lw	$15,10000($18)
mul	$2,$6,$2
sll	$5,$5,$14
subu	$14,$3,$5
mul	$11,$14,$11
sll	$2,$2,4
addu	$2,$2,$15
sll	$11,$11,4
addu	$11,$2,$11
$L521:
li	$3,16			# 0x10
move	$2,$11
$L520:
sra	$5,$2,$7
addiu	$3,$3,-1
addu	$4,$4,$5
.set	noreorder
.set	nomacro
bne	$3,$0,$L520
addu	$2,$2,$6
.set	macro
.set	reorder

addiu	$13,$13,-1
.set	noreorder
.set	nomacro
bne	$13,$0,$L521
addu	$11,$11,$14
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$4,$L522
addiu	$8,$8,8
.set	macro
.set	reorder

subu	$8,$8,$10
li	$2,1			# 0x1
sll	$2,$2,$8
sra	$2,$2,1
addu	$4,$2,$4
sra	$8,$4,$8
$L518:
subu	$7,$0,$9
slt	$2,$8,$7
.set	noreorder
.set	nomacro
bne	$2,$0,$L523
slt	$2,$8,$9
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L523
move	$7,$8
.set	macro
.set	reorder

addiu	$7,$9,-1
$L523:
lw	$5,%got(dMB)($28)
andi	$2,$16,0x2
andi	$4,$16,0x1
sll	$4,$4,20
sll	$2,$2,15
lw	$6,0($5)
andi	$3,$16,0x4
andi	$5,$16,0x8
or	$2,$2,$4
sll	$3,$3,10
sw	$12,8($6)
andi	$4,$16,0x10
sw	$12,7268($18)
sll	$5,$5,5
sw	$7,12($6)
or	$4,$2,$4
sw	$7,7272($18)
$L871:
andi	$2,$16,0x20
or	$3,$4,$3
sra	$19,$2,5
or	$3,$3,$5
or	$19,$3,$19
$L524:
lw	$11,%got(ff_h263_rl_inter_hw)($28)
addiu	$12,$18,8680
sw	$19,72($6)
addiu	$21,$18,8704
addiu	$5,$18,8732
li	$9,127			# 0x7f
lw	$7,60($11)
li	$10,125			# 0x7d
li	$8,13			# 0xd
li	$14,109			# 0x6d
li	$13,93			# 0x5d
li	$20,-2048			# 0xfffffffffffff800
li	$19,2047			# 0x7ff
li	$15,-4096			# 0xfffffffffffff000
.set	noreorder
.set	nomacro
beq	$2,$0,$L911
li	$22,-64			# 0xffffffffffffffc0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L705
li	$4,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L914:
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L690
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
beq	$2,$0,$L691
#APP
# 112 "vlc_bs.c" 1
mtc0	$14,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$23,$21,1
# 0 "" 2
#NO_APP
andi	$24,$23,0x40
andi	$23,$23,0x3f
#APP
# 112 "vlc_bs.c" 1
mtc0	$13,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 112 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$3,$3,8
addu	$2,$3,$2
andi	$6,$2,0x2000
beq	$6,$0,$L618
andi	$6,$2,0x1000
.set	noreorder
.set	nomacro
beq	$6,$0,$L692
sra	$6,$2,1
.set	macro
.set	reorder

or	$6,$6,$15
$L693:
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L618
addiu	$2,$6,2048
.set	macro
.set	reorder

sltu	$2,$2,4096
bne	$2,$0,$L694
lw	$2,9816($18)
slt	$2,$2,3
.set	noreorder
.set	nomacro
bne	$2,$0,$L695
addiu	$2,$6,2560
.set	macro
.set	reorder

sltu	$2,$2,5121
beq	$2,$0,$L872
$L695:
slt	$6,$6,0
move	$2,$19
movn	$2,$20,$6
move	$6,$2
$L694:
addiu	$23,$23,1
addu	$4,$4,$23
addiu	$2,$4,192
movn	$4,$2,$24
$L696:
slt	$2,$4,63
beq	$2,$0,$L912
$L703:
addu	$2,$5,$4
lbu	$2,0($2)
sll	$2,$2,1
addu	$2,$17,$2
sh	$6,0($2)
$L705:
#APP
# 154 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
addu	$2,$7,$2
lb	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L913
lh	$6,0($2)
.set	macro
.set	reorder

$L687:
addiu	$3,$3,-1
lbu	$2,3($2)
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
beq	$6,$0,$L914
#APP
# 1469 "mpeg4_p0.c" 1
mtc0	$8,$21,0
# 0 "" 2
#NO_APP
addu	$4,$4,$2
#APP
# 1472 "mpeg4_p0.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 1473 "mpeg4_p0.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sltu	$3,$0,$3
subu	$2,$0,$3
xor	$6,$6,$2
slt	$2,$4,63
.set	noreorder
.set	nomacro
bne	$2,$0,$L703
addu	$6,$6,$3
.set	macro
.set	reorder

$L912:
addiu	$4,$4,-192
and	$2,$4,$22
bne	$2,$0,$L873
addu	$2,$5,$4
lbu	$2,0($2)
sll	$2,$2,1
addu	$2,$17,$2
sh	$6,0($2)
sw	$4,0($12)
$L685:
addiu	$12,$12,4
sll	$16,$16,1
.set	noreorder
.set	nomacro
beq	$12,$21,$L488
addiu	$17,$17,128
.set	macro
.set	reorder

andi	$2,$16,0x20
.set	noreorder
.set	nomacro
bne	$2,$0,$L705
li	$4,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L911:
li	$2,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L685
sw	$2,0($12)
.set	macro
.set	reorder

$L651:
lw	$7,7992($18)
lw	$2,9784($18)
bne	$7,$2,$L653
$L654:
lw	$2,9788($18)
$L937:
lw	$5,7996($18)
addiu	$2,$2,1
bne	$5,$2,$L653
.set	noreorder
.set	nomacro
beq	$23,$0,$L950
li	$2,1024			# 0x400
.set	macro
.set	reorder

lw	$5,40($sp)
sltu	$2,$5,2
beq	$2,$0,$L653
li	$2,1024			# 0x400
$L950:
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L653
$L636:
.set	noreorder
.set	nomacro
b	$L635
addiu	$4,$18,8864
.set	macro
.set	reorder

$L898:
li	$2,-3			# 0xfffffffffffffffd
and	$2,$23,$2
.set	noreorder
.set	nomacro
beq	$2,$3,$L626
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 259 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
# 260 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L934
lw	$2,9788($18)
.set	macro
.set	reorder

$L891:
li	$2,-3			# 0xfffffffffffffffd
and	$2,$23,$2
.set	noreorder
.set	nomacro
beq	$2,$3,$L612
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 259 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
# 260 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L935
lw	$2,9788($18)
.set	macro
.set	reorder

$L733:
move	$2,$0
.set	noreorder
.set	nomacro
b	$L621
sh	$2,0($5)
.set	macro
.set	reorder

$L731:
move	$3,$0
.set	noreorder
.set	nomacro
b	$L616
sh	$3,0($6)
.set	macro
.set	reorder

$L889:
li	$2,29			# 0x1d
#APP
# 112 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$3,%got(quant_tab.6803)($28)
move	$4,$18
lw	$25,%call16(ff_set_qscale)($28)
addiu	$3,$3,%lo(quant_tab.6803)
addu	$2,$2,$3
lb	$2,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_set_qscale
1:	jalr	$25
addu	$5,$5,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L596
lw	$28,24($sp)
.set	macro
.set	reorder

$L533:
li	$2,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$0,$L915
lw	$22,%got(dMB)($28)
.set	macro
.set	reorder

move	$19,$0
lw	$3,44($sp)
li	$20,63744			# 0xf900
move	$16,$0
move	$2,$0
move	$21,$0
$L534:
lw	$4,992($18)
sll	$3,$3,2
addu	$3,$4,$3
li	$4,7			# 0x7
sw	$4,7260($18)
lw	$3,0($3)
andi	$3,$3,0x40
.set	noreorder
.set	nomacro
beq	$3,$0,$L564
li	$3,1			# 0x1
.set	macro
.set	reorder

lhu	$8,9936($18)
lhu	$10,9938($18)
addiu	$9,$18,8012
lw	$6,0($22)
addiu	$5,$18,7272
lw	$12,984($18)
addiu	$11,$18,7304
subu	$13,$10,$8
sw	$3,7264($18)
addiu	$7,$6,12
$L577:
lw	$14,0($9)
sll	$14,$14,2
addu	$14,$12,$14
lh	$3,0($14)
addiu	$4,$3,32
sltu	$4,$4,64
.set	noreorder
.set	nomacro
beq	$4,$0,$L565
mul	$4,$3,$10
.set	macro
.set	reorder

sll	$15,$3,1
addu	$15,$18,$15
lh	$4,7448($15)
addu	$4,$21,$4
.set	noreorder
.set	nomacro
bne	$21,$0,$L874
sw	$4,-4($5)
.set	macro
.set	reorder

lh	$4,7576($15)
$L570:
lh	$3,2($14)
lw	$14,-4($5)
sw	$4,28($5)
sw	$4,28($7)
addiu	$4,$3,32
sltu	$4,$4,64
.set	noreorder
.set	nomacro
beq	$4,$0,$L571
sw	$14,-4($7)
.set	macro
.set	reorder

sll	$4,$3,1
addu	$4,$18,$4
lh	$14,7448($4)
addu	$14,$2,$14
.set	noreorder
.set	nomacro
beq	$2,$0,$L572
sw	$14,0($5)
.set	macro
.set	reorder

subu	$3,$14,$3
$L576:
lw	$4,0($5)
addiu	$7,$7,8
sw	$3,32($5)
addiu	$5,$5,8
sw	$3,24($7)
addiu	$9,$9,4
.set	noreorder
.set	nomacro
bne	$5,$11,$L577
sw	$4,-8($7)
.set	macro
.set	reorder

li	$2,61760			# 0xf140
$L578:
or	$20,$20,$2
$L563:
lw	$2,44($sp)
$L936:
lw	$4,2192($18)
sll	$3,$2,2
andi	$2,$16,0x20
addu	$3,$4,$3
.set	noreorder
.set	nomacro
b	$L524
sw	$20,0($3)
.set	macro
.set	reorder

$L503:
lw	$7,44($sp)
li	$5,12352			# 0x3040
lw	$2,2192($18)
addiu	$fp,$18,7268
li	$19,1			# 0x1
sw	$16,40($sp)
sll	$3,$7,2
sw	$17,48($sp)
move	$23,$0
addu	$2,$2,$3
addiu	$3,$sp,32
addiu	$22,$sp,36
sw	$5,0($2)
li	$21,65535			# 0xffff
sw	$4,7264($18)
move	$17,$fp
move	$16,$3
$L530:
lw	$25,%call16(mpeg4_pred_motion)($28)
move	$6,$0
move	$5,$23
sw	$16,16($sp)
move	$7,$22
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_motion
1:	jalr	$25
move	$4,$18
.set	macro
.set	reorder

move	$4,$18
lw	$28,24($sp)
move	$20,$2
lw	$5,36($sp)
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7080($18)
.set	macro
.set	reorder

move	$4,$18
lw	$28,24($sp)
move	$fp,$2
slt	$2,$2,$21
.set	noreorder
.set	nomacro
beq	$2,$0,$L618
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	macro
.set	reorder

lw	$5,32($sp)
addiu	$19,$19,2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7080($18)
.set	macro
.set	reorder

sll	$5,$23,3
slt	$6,$2,$21
lw	$28,24($sp)
sll	$4,$19,2
.set	noreorder
.set	nomacro
beq	$6,$0,$L618
addiu	$23,$23,1
.set	macro
.set	reorder

lw	$3,%got(dMB)($28)
addiu	$17,$17,8
lw	$6,0($3)
addu	$4,$6,$4
addu	$5,$6,$5
sw	$fp,8($5)
sw	$fp,-8($17)
sw	$2,0($4)
li	$4,4			# 0x4
sw	$2,-4($17)
sh	$fp,0($20)
.set	noreorder
.set	nomacro
bne	$23,$4,$L530
sh	$2,2($20)
.set	macro
.set	reorder

lw	$16,40($sp)
lw	$17,48($sp)
$L851:
andi	$2,$16,0x2
andi	$4,$16,0x1
sll	$4,$4,20
sll	$2,$2,15
andi	$3,$16,0x4
or	$2,$2,$4
andi	$5,$16,0x8
andi	$4,$16,0x10
sll	$3,$3,10
or	$4,$2,$4
.set	noreorder
.set	nomacro
b	$L871
sll	$5,$5,5
.set	macro
.set	reorder

$L915:
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
li	$2,63			# 0x3f
#APP
# 154 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
lw	$2,%got(mb_type_b_vlc+4)($28)
sll	$3,$3,2
lw	$2,%lo(mb_type_b_vlc+4)($2)
addu	$3,$2,$3
lh	$2,2($3)
lh	$3,0($3)
addiu	$2,$2,-1
sll	$2,$2,4
ori	$2,$2,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bltz	$3,$L916
sll	$3,$3,2
.set	macro
.set	reorder

lw	$2,%got(mb_type_b_map)($28)
addiu	$2,$2,%lo(mb_type_b_map)
addu	$2,$3,$2
.set	noreorder
.set	nomacro
bne	$4,$0,$L536
lw	$20,0($2)
.set	macro
.set	reorder

addiu	$3,$17,768
move	$2,$17
$L537:
#APP
# 2033 "mpeg4_p0.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 2034 "mpeg4_p0.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 2035 "mpeg4_p0.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 2037 "mpeg4_p0.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 2038 "mpeg4_p0.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 2039 "mpeg4_p0.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 2040 "mpeg4_p0.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 2041 "mpeg4_p0.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L537
li	$2,93			# 0x5d
#APP
# 112 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$16,$21,1
# 0 "" 2
#NO_APP
andi	$22,$20,0x100
move	$19,$16
.set	noreorder
.set	nomacro
bne	$22,$0,$L538
move	$21,$22
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$16,$0,$L539
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
bne	$3,$0,$L917
$L540:
lw	$21,10364($18)
.set	noreorder
.set	nomacro
beq	$21,$0,$L951
li	$2,13			# 0xd
.set	macro
.set	reorder

andi	$2,$19,0x4
sw	$0,7260($18)
andi	$4,$19,0x1
sll	$4,$4,20
sll	$2,$2,10
andi	$3,$19,0x2
or	$2,$2,$4
andi	$5,$19,0x10
andi	$4,$19,0x8
sll	$3,$3,15
or	$2,$2,$5
andi	$19,$19,0x20
sll	$4,$4,5
or	$3,$2,$3
sra	$2,$19,5
andi	$21,$20,0x180
or	$19,$3,$4
.set	noreorder
.set	nomacro
beq	$21,$0,$L542
or	$19,$19,$2
.set	macro
.set	reorder

li	$3,3			# 0x3
$L954:
lw	$22,%got(dMB)($28)
andi	$2,$20,0x3000
.set	noreorder
.set	nomacro
beq	$2,$0,$L560
sw	$3,7264($18)
.set	macro
.set	reorder

li	$2,1			# 0x1
addiu	$23,$18,7348
move	$21,$0
li	$7,4			# 0x4
sw	$2,7260($18)
$L559:
lw	$5,0($23)
addiu	$23,$23,8
lw	$25,%call16(mpeg4_decode_motion)($28)
move	$4,$18
lw	$6,7080($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sw	$7,52($sp)
.set	macro
.set	reorder

move	$4,$18
lw	$5,-4($23)
move	$fp,$2
lw	$28,24($sp)
lw	$6,7080($18)
srl	$2,$5,31
addu	$5,$2,$5
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sra	$5,$5,1
.set	macro
.set	reorder

addiu	$5,$21,2
addiu	$21,$21,3
lw	$3,0($22)
sll	$4,$5,2
lw	$7,52($sp)
sll	$21,$21,2
lw	$28,24($sp)
sll	$6,$2,1
addu	$4,$3,$4
addu	$3,$3,$21
sw	$fp,0($4)
move	$21,$5
sw	$fp,-88($23)
sw	$fp,-8($23)
sw	$2,0($3)
sw	$2,-84($23)
.set	noreorder
.set	nomacro
bne	$5,$7,$L559
sw	$6,-4($23)
.set	macro
.set	reorder

$L560:
andi	$2,$20,0xc000
.set	noreorder
.set	nomacro
beq	$2,$0,$L854
addiu	$23,$18,7364
.set	macro
.set	reorder

lw	$2,7260($18)
li	$21,8			# 0x8
li	$7,12			# 0xc
ori	$2,$2,0x2
sw	$2,7260($18)
$L561:
lw	$5,0($23)
addiu	$23,$23,8
lw	$25,%call16(mpeg4_decode_motion)($28)
move	$4,$18
lw	$6,7084($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sw	$7,52($sp)
.set	macro
.set	reorder

move	$4,$18
lw	$5,-4($23)
move	$fp,$2
lw	$28,24($sp)
lw	$6,7084($18)
srl	$2,$5,31
addu	$5,$2,$5
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sra	$5,$5,1
.set	macro
.set	reorder

addiu	$4,$21,2
addiu	$21,$21,3
lw	$6,0($22)
sll	$3,$4,2
lw	$7,52($sp)
sll	$21,$21,2
lw	$28,24($sp)
sll	$5,$2,1
addu	$8,$6,$21
addu	$3,$6,$3
move	$21,$4
sw	$fp,0($3)
sw	$fp,-72($23)
sw	$fp,-8($23)
sw	$2,0($8)
sw	$2,-68($23)
.set	noreorder
.set	nomacro
bne	$4,$7,$L561
sw	$5,-4($23)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L936
lw	$2,44($sp)
.set	macro
.set	reorder

$L711:
lw	$4,%got(ff_mpeg4_resync_prefix)($28)
sll	$3,$3,1
addu	$3,$4,$3
lhu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$2,$3,$L919
move	$2,$0
.set	macro
.set	reorder

lw	$6,10340($18)
addiu	$4,$6,1
subu	$2,$0,$4
andi	$2,$2,0x7
.set	noreorder
.set	nomacro
beq	$2,$0,$L713
sw	$4,10340($18)
.set	macro
.set	reorder

addu	$4,$2,$4
sw	$4,10340($18)
$L713:
move	$16,$0
.set	noreorder
.set	nomacro
b	$L715
li	$5,32			# 0x20
.set	macro
.set	reorder

$L918:
addiu	$16,$16,1
.set	noreorder
.set	nomacro
beq	$16,$5,$L952
lw	$25,%call16(ff_mpeg4_get_video_packet_prefix_length)($28)
.set	macro
.set	reorder

$L715:
srl	$3,$4,3
andi	$7,$4,0x7
addu	$3,$9,$3
addiu	$4,$4,1
lbu	$2,0($3)
sll	$2,$2,$7
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L918
sw	$4,10340($18)
.set	macro
.set	reorder

lw	$25,%call16(ff_mpeg4_get_video_packet_prefix_length)($28)
$L952:
move	$4,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_get_video_packet_prefix_length
1:	jalr	$25
sw	$6,10340($18)
.set	macro
.set	reorder

slt	$2,$16,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L712
move	$2,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L957
lw	$31,92($sp)
.set	macro
.set	reorder

$L905:
sltu	$2,$3,2
beq	$2,$0,$L625
li	$2,1024			# 0x400
#APP
# 267 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
b	$L625
$L913:
#APP
# 79 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$23,$3,4
ori	$24,$23,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$24,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$6
sll	$2,$2,2
addu	$2,$7,$2
lb	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L687
lh	$6,0($2)
.set	macro
.set	reorder

ori	$23,$23,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$23,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$3,$3,4
ori	$3,$3,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$6
sll	$2,$2,2
addu	$2,$7,$2
lh	$6,0($2)
.set	noreorder
.set	nomacro
b	$L687
lb	$3,2($2)
.set	macro
.set	reorder

$L690:
#APP
# 154 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
addu	$2,$7,$2
lb	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L920
lh	$23,0($2)
.set	macro
.set	reorder

$L701:
addiu	$3,$3,-1
lbu	$2,3($2)
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$4,$4,$2
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
sra	$3,$2,7
addiu	$2,$2,-1
addiu	$3,$3,6
andi	$2,$2,0x3f
sll	$3,$3,2
sltu	$25,$0,$6
addu	$3,$11,$3
subu	$24,$0,$25
lw	$3,4($3)
addu	$2,$3,$2
lb	$6,0($2)
addu	$23,$6,$23
xor	$23,$24,$23
.set	noreorder
.set	nomacro
b	$L696
addu	$6,$23,$25
.set	macro
.set	reorder

$L691:
#APP
# 154 "vlc_bs.c" 1
mtc0	$9,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,2
addu	$2,$7,$2
lb	$3,2($2)
.set	noreorder
.set	nomacro
bltz	$3,$L921
lh	$23,0($2)
.set	macro
.set	reorder

$L698:
addiu	$3,$3,-1
lbu	$2,3($2)
sll	$3,$3,4
ori	$3,$3,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sra	$3,$2,7
addiu	$3,$3,8
sll	$3,$3,2
addu	$3,$11,$3
lw	$3,4($3)
addu	$3,$3,$23
lb	$3,0($3)
addu	$2,$2,$3
addiu	$2,$2,1
addu	$4,$4,$2
#APP
# 120 "vlc_bs.c" 1
mtc0	$8,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$6,$21,1
# 0 "" 2
#NO_APP
sltu	$3,$0,$6
subu	$6,$0,$3
xor	$6,$6,$23
.set	noreorder
.set	nomacro
b	$L696
addu	$6,$6,$3
.set	macro
.set	reorder

$L920:
#APP
# 79 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$24,$3,4
ori	$25,$24,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$23
sll	$2,$2,2
addu	$2,$7,$2
lb	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L701
lh	$23,0($2)
.set	macro
.set	reorder

ori	$24,$24,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$24,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$3,$3,4
ori	$3,$3,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$23
sll	$2,$2,2
addu	$2,$7,$2
lh	$23,0($2)
.set	noreorder
.set	nomacro
b	$L701
lb	$3,2($2)
.set	macro
.set	reorder

$L692:
.set	noreorder
.set	nomacro
b	$L693
andi	$6,$6,0xfff
.set	macro
.set	reorder

$L921:
#APP
# 79 "vlc_bs.c" 1
mtc0	$10,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$24,$3,4
ori	$25,$24,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$25,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$23
sll	$2,$2,2
addu	$2,$7,$2
lb	$3,2($2)
.set	noreorder
.set	nomacro
bgez	$3,$L698
lh	$23,0($2)
.set	macro
.set	reorder

ori	$24,$24,0xd
#APP
# 79 "vlc_bs.c" 1
mtc0	$24,$21,0
# 0 "" 2
# 80 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
nor	$3,$0,$3
sll	$3,$3,4
ori	$3,$3,0xf
#APP
# 154 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 155 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 156 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
addu	$2,$3,$23
sll	$2,$2,2
addu	$2,$7,$2
lh	$23,0($2)
.set	noreorder
.set	nomacro
b	$L698
lb	$3,2($2)
.set	macro
.set	reorder

$L571:
mul	$4,$10,$3
teq	$8,$0,7
div	$0,$4,$8
mflo	$4
addu	$4,$2,$4
.set	noreorder
.set	nomacro
beq	$2,$0,$L575
sw	$4,0($5)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L576
subu	$3,$4,$3
.set	macro
.set	reorder

$L874:
.set	noreorder
.set	nomacro
b	$L570
subu	$4,$4,$3
.set	macro
.set	reorder

$L565:
teq	$8,$0,7
div	$0,$4,$8
mflo	$4
addu	$4,$21,$4
.set	noreorder
.set	nomacro
bne	$21,$0,$L874
sw	$4,-4($5)
.set	macro
.set	reorder

mul	$3,$3,$13
teq	$8,$0,7
div	$0,$3,$8
.set	noreorder
.set	nomacro
b	$L570
mflo	$4
.set	macro
.set	reorder

$L906:
lw	$4,9948($18)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
bne	$4,$2,$L467
lw	$5,44($sp)
.set	macro
.set	reorder

lw	$2,2192($18)
sll	$4,$5,2
addu	$2,$2,$4
li	$4,15368			# 0x3c08
sw	$4,0($2)
lw	$10,7080($18)
lw	$13,88($18)
lw	$2,%got(dMB)($28)
addiu	$10,$10,4
lw	$8,10072($18)
andi	$13,$13,0x20
lw	$11,10044($18)
sw	$3,10036($18)
sll	$10,$3,$10
.set	noreorder
.set	nomacro
bne	$13,$0,$L468
lw	$9,0($2)
.set	macro
.set	reorder

move	$14,$10
$L469:
lw	$12,9976($18)
li	$5,1			# 0x1
beq	$12,$5,$L922
lw	$6,10028($18)
li	$16,16			# 0x10
lw	$2,10012($18)
move	$7,$0
lw	$3,7992($18)
addu	$19,$8,$6
lw	$17,10016($18)
lw	$4,7996($18)
addiu	$19,$19,1
lw	$15,9996($18)
sll	$5,$5,$19
mul	$4,$17,$4
subu	$5,$2,$5
mul	$2,$5,$3
sll	$3,$4,4
sll	$2,$2,4
addu	$2,$2,$15
addu	$15,$2,$3
$L475:
li	$3,16			# 0x10
move	$2,$15
$L474:
sra	$4,$2,$6
addiu	$3,$3,-1
addu	$7,$7,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L474
addu	$2,$2,$5
.set	macro
.set	reorder

addiu	$16,$16,-1
.set	noreorder
.set	nomacro
bne	$16,$0,$L475
addu	$15,$15,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$7,$L476
li	$2,1			# 0x1
.set	macro
.set	reorder

addiu	$3,$8,8
subu	$3,$3,$11
sll	$2,$2,$3
sra	$2,$2,1
addu	$7,$2,$7
sra	$3,$7,$3
$L472:
subu	$2,$0,$14
slt	$4,$3,$2
bne	$4,$0,$L477
slt	$2,$3,$14
.set	noreorder
.set	nomacro
bne	$2,$0,$L477
move	$2,$3
.set	macro
.set	reorder

addiu	$2,$14,-1
$L477:
sra	$3,$10,$11
sw	$2,8($9)
li	$5,1			# 0x1
sw	$2,7268($18)
.set	noreorder
.set	nomacro
beq	$12,$5,$L923
movn	$10,$3,$13
.set	macro
.set	reorder

lw	$7,10028($18)
li	$13,16			# 0x10
lw	$6,10020($18)
move	$4,$0
lw	$3,10024($18)
addu	$14,$8,$7
lw	$2,7992($18)
lw	$12,7996($18)
addiu	$14,$14,1
lw	$15,10000($18)
mul	$2,$6,$2
sll	$5,$5,$14
subu	$14,$3,$5
mul	$12,$14,$12
sll	$2,$2,4
addu	$2,$2,$15
sll	$12,$12,4
addu	$12,$2,$12
$L484:
li	$3,16			# 0x10
move	$2,$12
$L483:
sra	$5,$2,$7
addiu	$3,$3,-1
addu	$4,$4,$5
.set	noreorder
.set	nomacro
bne	$3,$0,$L483
addu	$2,$2,$6
.set	macro
.set	reorder

addiu	$13,$13,-1
.set	noreorder
.set	nomacro
bne	$13,$0,$L484
addu	$12,$12,$14
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
blez	$4,$L485
addiu	$8,$8,8
.set	macro
.set	reorder

subu	$8,$8,$11
li	$2,1			# 0x1
sll	$2,$2,$8
sra	$2,$2,1
addu	$4,$2,$4
sra	$8,$4,$8
$L481:
subu	$2,$0,$10
slt	$3,$8,$2
bne	$3,$0,$L486
slt	$2,$8,$10
.set	noreorder
.set	nomacro
bne	$2,$0,$L486
move	$2,$8
.set	macro
.set	reorder

addiu	$2,$10,-1
$L486:
sw	$2,12($9)
sw	$2,7272($18)
sw	$0,2832($18)
.set	noreorder
.set	nomacro
b	$L488
sw	$0,72($9)
.set	macro
.set	reorder

$L902:
li	$2,-3			# 0xfffffffffffffffd
and	$2,$23,$2
.set	noreorder
.set	nomacro
beq	$2,$5,$L654
li	$2,1024			# 0x400
.set	macro
.set	reorder

#APP
# 259 "mpeg4_p0.c" 1
.word	0b01110000000000100000000001101111	#S32I2M XR1,$2
# 0 "" 2
# 260 "mpeg4_p0.c" 1
.word	0b01110000000000100000000010101111	#S32I2M XR2,$2
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L937
lw	$2,9788($18)
.set	macro
.set	reorder

$L736:
.set	noreorder
.set	nomacro
b	$L661
move	$5,$0
.set	macro
.set	reorder

$L538:
lw	$2,10364($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L541
andi	$2,$16,0x4
.set	macro
.set	reorder

sw	$0,7260($18)
andi	$3,$16,0x1
sll	$2,$2,10
sll	$3,$3,20
andi	$4,$16,0x2
or	$3,$2,$3
andi	$5,$16,0x10
sll	$2,$4,15
andi	$19,$16,0x8
or	$3,$3,$5
sll	$19,$19,5
andi	$4,$16,0x20
or	$2,$3,$2
sra	$3,$4,5
or	$2,$2,$19
andi	$4,$20,0x180
.set	noreorder
.set	nomacro
bne	$4,$0,$L867
or	$19,$2,$3
.set	macro
.set	reorder

$L542:
andi	$2,$20,0x3000
.set	noreorder
.set	nomacro
bne	$2,$0,$L852
sw	$0,7264($18)
.set	macro
.set	reorder

lw	$22,%got(dMB)($28)
$L551:
andi	$2,$20,0xc000
.set	noreorder
.set	nomacro
bne	$2,$0,$L924
move	$4,$18
.set	macro
.set	reorder

bne	$21,$0,$L543
$L854:
.set	noreorder
.set	nomacro
b	$L563
lw	$6,0($22)
.set	macro
.set	reorder

$L924:
lw	$2,7260($18)
lw	$25,%call16(mpeg4_decode_motion)($28)
lw	$5,7364($18)
lw	$6,7084($18)
ori	$2,$2,0x2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sw	$2,7260($18)
.set	macro
.set	reorder

move	$4,$18
lw	$28,24($sp)
move	$23,$2
lw	$5,7368($18)
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7084($18)
.set	macro
.set	reorder

lw	$3,0($22)
lw	$28,24($sp)
sw	$23,40($3)
sw	$23,7300($18)
sw	$23,7364($18)
sw	$23,7372($18)
sw	$2,44($3)
sw	$2,7304($18)
sw	$2,7368($18)
.set	noreorder
.set	nomacro
beq	$21,$0,$L854
sw	$2,7376($18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L938
andi	$2,$20,0x800
.set	macro
.set	reorder

$L890:
li	$2,125			# 0x7d
#APP
# 98 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 99 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 100 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
li	$3,13			# 0xd
#APP
# 104 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 105 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 106 "vlc_bs.c" 1
mfc0	$9,$21,1
# 0 "" 2
#NO_APP
sll	$2,$2,1
or	$9,$2,$9
sra	$2,$9,8
bne	$2,$0,$L605
li	$2,-512			# 0xfffffffffffffe00
or	$2,$9,$2
addiu	$9,$2,1
$L605:
li	$2,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L939
lw	$2,0($22)
.set	macro
.set	reorder

$L572:
.set	noreorder
.set	nomacro
b	$L576
lh	$3,7576($4)
.set	macro
.set	reorder

$L575:
mul	$3,$13,$3
teq	$8,$0,7
div	$0,$3,$8
.set	noreorder
.set	nomacro
b	$L576
mflo	$3
.set	macro
.set	reorder

$L908:
li	$2,29			# 0x1d
#APP
# 112 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 113 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 114 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$3,%got(quant_tab.6803)($28)
move	$4,$18
lw	$5,2872($18)
lw	$25,%call16(ff_set_qscale)($28)
addiu	$3,$3,%lo(quant_tab.6803)
addu	$2,$2,$3
lb	$2,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_set_qscale
1:	jalr	$25
addu	$5,$2,$5
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L500
lw	$28,24($sp)
.set	macro
.set	reorder

$L536:
lw	$2,10364($18)
andi	$22,$20,0x100
.set	noreorder
.set	nomacro
bne	$2,$0,$L718
move	$21,$22
.set	macro
.set	reorder

move	$19,$0
move	$16,$0
$L717:
.set	noreorder
.set	nomacro
bne	$21,$0,$L546
move	$21,$22
.set	macro
.set	reorder

$L545:
li	$3,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$2,$0,$L546
move	$21,$22
.set	macro
.set	reorder

li	$2,-9			# 0xfffffffffffffff7
andi	$4,$20,0x3000
and	$2,$20,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L548
ori	$2,$2,0x90
.set	macro
.set	reorder

#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sw	$4,7332($18)
#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sw	$3,7336($18)
$L548:
andi	$20,$20,0xc000
.set	noreorder
.set	nomacro
beq	$20,$0,$L953
move	$21,$22
.set	macro
.set	reorder

li	$3,13			# 0xd
#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$4,$21,1
# 0 "" 2
#NO_APP
sw	$4,7340($18)
#APP
# 120 "vlc_bs.c" 1
mtc0	$3,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sw	$3,7344($18)
move	$21,$22
$L953:
move	$20,$2
$L546:
andi	$2,$20,0x180
.set	noreorder
.set	nomacro
beq	$2,$0,$L542
sw	$0,7260($18)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$22,$0,$L954
li	$3,3			# 0x3
.set	macro
.set	reorder

$L867:
lw	$22,%got(dMB)($28)
$L543:
andi	$2,$20,0x800
$L938:
.set	noreorder
.set	nomacro
bne	$2,$0,$L925
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	macro
.set	reorder

move	$5,$0
li	$6,1			# 0x1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
move	$4,$18
.set	macro
.set	reorder

move	$5,$0
lw	$28,24($sp)
li	$6,1			# 0x1
move	$4,$18
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
move	$21,$2
.set	macro
.set	reorder

lw	$4,7996($18)
lw	$5,168($18)
lw	$3,7992($18)
lw	$28,24($sp)
mul	$6,$4,$5
.set	noreorder
.set	nomacro
b	$L534
addu	$3,$6,$3
.set	macro
.set	reorder

$L907:
lw	$4,9948($18)
.set	noreorder
.set	nomacro
bne	$4,$2,$L498
andi	$19,$3,0x10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$19,$0,$L498
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
b	$L499
sw	$2,10036($18)
.set	macro
.set	reorder

$L541:
.set	noreorder
.set	nomacro
beq	$16,$0,$L728
li	$2,13			# 0xd
.set	macro
.set	reorder

$L951:
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
andi	$2,$19,0x4
sw	$3,10448($18)
andi	$3,$19,0x1
sll	$2,$2,10
sll	$3,$3,20
andi	$4,$19,0x2
or	$3,$2,$3
andi	$5,$19,0x10
sll	$2,$4,15
andi	$4,$19,0x8
or	$3,$3,$5
andi	$19,$19,0x20
sll	$4,$4,5
or	$2,$3,$2
sra	$3,$19,5
or	$2,$2,$4
.set	noreorder
.set	nomacro
b	$L717
or	$19,$2,$3
.set	macro
.set	reorder

$L504:
.set	noreorder
.set	nomacro
bne	$2,$0,$L525
li	$2,13			# 0xd
.set	macro
.set	reorder

#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$3,$0,$L926
lw	$5,44($sp)
.set	macro
.set	reorder

$L525:
lw	$6,44($sp)
move	$5,$0
lw	$2,2192($18)
addiu	$7,$sp,36
lw	$25,%call16(mpeg4_pred_motion)($28)
move	$4,$18
sll	$3,$6,2
move	$6,$0
addu	$2,$2,$3
li	$3,12296			# 0x3008
li	$19,65535			# 0xffff
sw	$3,0($2)
addiu	$2,$sp,32
sw	$0,7264($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_motion
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

move	$4,$18
lw	$28,24($sp)
lw	$5,36($sp)
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7080($18)
.set	macro
.set	reorder

move	$20,$2
slt	$2,$2,$19
.set	noreorder
.set	nomacro
beq	$2,$0,$L618
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$25,%call16(mpeg4_decode_motion)($28)
move	$4,$18
lw	$5,32($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7080($18)
.set	macro
.set	reorder

slt	$19,$2,$19
.set	noreorder
.set	nomacro
beq	$19,$0,$L618
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$6,%got(dMB)($28)
andi	$3,$16,0x2
andi	$4,$16,0x1
sll	$3,$3,15
sll	$4,$4,20
lw	$6,0($6)
andi	$5,$16,0x4
or	$4,$3,$4
andi	$7,$16,0x10
sll	$3,$5,10
sw	$20,8($6)
andi	$5,$16,0x8
sw	$20,7268($18)
or	$4,$4,$7
sw	$2,12($6)
sll	$5,$5,5
.set	noreorder
.set	nomacro
b	$L871
sw	$2,7272($18)
.set	macro
.set	reorder

$L718:
$L955:
move	$19,$0
.set	noreorder
.set	nomacro
b	$L546
move	$16,$0
.set	macro
.set	reorder

$L728:
move	$19,$0
.set	noreorder
.set	nomacro
b	$L546
move	$21,$22
.set	macro
.set	reorder

$L873:
lw	$2,7996($18)
li	$5,16			# 0x10
lw	$6,%got($LC11)($28)
lw	$4,0($18)
lw	$7,7992($18)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC11)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L927
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L564:
lw	$7,8012($18)
lw	$3,984($18)
lhu	$6,9936($18)
sll	$7,$7,2
addu	$7,$3,$7
lh	$4,0($7)
addiu	$3,$4,32
sltu	$3,$3,64
.set	noreorder
.set	nomacro
beq	$3,$0,$L579
lhu	$8,9938($18)
.set	macro
.set	reorder

sll	$3,$4,1
addu	$3,$18,$3
lh	$5,7448($3)
addu	$5,$21,$5
.set	noreorder
.set	nomacro
bne	$21,$0,$L875
sw	$5,7268($18)
.set	macro
.set	reorder

lh	$4,7576($3)
$L584:
lh	$3,2($7)
lw	$9,0($22)
sw	$4,7300($18)
addiu	$7,$3,32
sw	$5,8($9)
sltu	$5,$7,64
.set	noreorder
.set	nomacro
beq	$5,$0,$L585
sw	$4,40($9)
.set	macro
.set	reorder

sll	$4,$3,1
addu	$4,$18,$4
lh	$7,7448($4)
addu	$7,$2,$7
.set	noreorder
.set	nomacro
bne	$2,$0,$L876
sw	$7,7272($18)
.set	macro
.set	reorder

lh	$2,7576($4)
$L590:
lw	$8,7268($18)
addiu	$4,$9,8
lw	$3,7300($18)
addiu	$5,$18,7268
lw	$25,%call16(memcpy)($28)
li	$6,64			# 0x40
sw	$2,7304($18)
sw	$7,12($9)
sw	$2,44($9)
sw	$8,7292($18)
sw	$8,7284($18)
sw	$8,7276($18)
sw	$7,7296($18)
sw	$7,7288($18)
sw	$7,7280($18)
sw	$3,7324($18)
sw	$3,7316($18)
sw	$3,7308($18)
sw	$2,7328($18)
sw	$2,7320($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$2,7312($18)
.set	macro
.set	reorder

lw	$2,0($18)
lw	$2,232($2)
andi	$2,$2,0x200
.set	noreorder
.set	nomacro
bne	$2,$0,$L591
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$2,10044($18)
.set	noreorder
.set	nomacro
beq	$2,$0,$L591
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$6,0($22)
li	$2,61704			# 0xf108
.set	noreorder
.set	nomacro
b	$L578
sw	$3,7264($18)
.set	macro
.set	reorder

$L505:
.set	noreorder
.set	nomacro
b	$L506
sra	$14,$9,$10
.set	macro
.set	reorder

$L539:
lw	$2,10364($18)
.set	noreorder
.set	nomacro
bne	$2,$0,$L955
move	$21,$22
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L545
move	$19,$0
.set	macro
.set	reorder

$L909:
lw	$3,10188($18)
li	$2,500			# 0x1f4
beq	$3,$2,$L928
$L508:
lw	$3,9996($18)
li	$2,1			# 0x1
sll	$2,$2,$8
sll	$3,$3,$10
.set	noreorder
.set	nomacro
blez	$3,$L510
sra	$2,$2,1
.set	macro
.set	reorder

addu	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L509
sra	$3,$3,$8
.set	macro
.set	reorder

$L591:
lw	$6,0($22)
li	$2,61704			# 0xf108
.set	noreorder
.set	nomacro
b	$L578
sw	$0,7264($18)
.set	macro
.set	reorder

$L468:
.set	noreorder
.set	nomacro
b	$L469
sra	$14,$10,$11
.set	macro
.set	reorder

$L910:
lw	$3,10188($18)
li	$2,500			# 0x1f4
.set	noreorder
.set	nomacro
beq	$3,$2,$L929
li	$2,413			# 0x19d
.set	macro
.set	reorder

$L517:
lw	$2,10000($18)
li	$3,1			# 0x1
sll	$3,$3,$8
sll	$2,$2,$10
.set	noreorder
.set	nomacro
blez	$2,$L519
sra	$3,$3,1
.set	macro
.set	reorder

addu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L518
sra	$8,$2,$8
.set	macro
.set	reorder

$L926:
lw	$3,2192($18)
sll	$4,$5,2
addu	$3,$3,$4
li	$4,12432			# 0x3090
sw	$4,0($3)
li	$3,3			# 0x3
sw	$3,7264($18)
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$3,$21,1
# 0 "" 2
#NO_APP
sw	$3,7332($18)
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
sw	$2,7336($18)
addiu	$2,$sp,32
lw	$25,%call16(mpeg4_pred_motion)($28)
move	$5,$0
move	$6,$0
lw	$23,%got(dMB)($28)
addiu	$7,$sp,36
sw	$2,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_motion
1:	jalr	$25
move	$4,$18
.set	macro
.set	reorder

addiu	$20,$18,7268
lw	$28,24($sp)
move	$19,$0
li	$21,65535			# 0xffff
li	$22,4			# 0x4
lw	$25,%call16(mpeg4_decode_motion)($28)
$L940:
move	$4,$18
lw	$5,36($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7080($18)
.set	macro
.set	reorder

move	$4,$18
lw	$28,24($sp)
move	$fp,$2
slt	$2,$2,$21
.set	noreorder
.set	nomacro
beq	$2,$0,$L618
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	macro
.set	reorder

lw	$5,32($sp)
lw	$6,7080($18)
srl	$2,$5,31
addu	$5,$2,$5
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sra	$5,$5,1
.set	macro
.set	reorder

addiu	$5,$19,2
addiu	$3,$19,3
lw	$28,24($sp)
slt	$6,$2,$21
sll	$4,$5,2
sll	$3,$3,2
.set	noreorder
.set	nomacro
beq	$6,$0,$L618
move	$19,$5
.set	macro
.set	reorder

lw	$6,0($23)
addiu	$20,$20,8
addu	$4,$6,$4
addu	$5,$6,$3
sw	$fp,0($4)
sw	$fp,-8($20)
sw	$2,0($5)
.set	noreorder
.set	nomacro
beq	$19,$22,$L851
sw	$2,-4($20)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L940
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	macro
.set	reorder

$L852:
li	$2,1			# 0x1
lw	$25,%call16(mpeg4_decode_motion)($28)
lw	$5,7348($18)
move	$4,$18
lw	$6,7080($18)
lw	$22,%got(dMB)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
sw	$2,7260($18)
.set	macro
.set	reorder

move	$4,$18
lw	$28,24($sp)
move	$23,$2
lw	$5,7352($18)
lw	$25,%call16(mpeg4_decode_motion)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_motion
1:	jalr	$25
lw	$6,7080($18)
.set	macro
.set	reorder

lw	$3,0($22)
lw	$28,24($sp)
sw	$23,8($3)
sw	$23,7268($18)
sw	$23,7348($18)
sw	$23,7356($18)
sw	$2,12($3)
sw	$2,7272($18)
sw	$2,7352($18)
.set	noreorder
.set	nomacro
b	$L551
sw	$2,7360($18)
.set	macro
.set	reorder

$L876:
.set	noreorder
.set	nomacro
b	$L590
subu	$2,$7,$3
.set	macro
.set	reorder

$L585:
mul	$4,$3,$8
teq	$6,$0,7
div	$0,$4,$6
mflo	$7
addu	$7,$2,$7
.set	noreorder
.set	nomacro
bne	$2,$0,$L876
sw	$7,7272($18)
.set	macro
.set	reorder

subu	$2,$8,$6
mul	$2,$3,$2
teq	$6,$0,7
div	$0,$2,$6
.set	noreorder
.set	nomacro
b	$L590
mflo	$2
.set	macro
.set	reorder

$L875:
.set	noreorder
.set	nomacro
b	$L584
subu	$4,$5,$4
.set	macro
.set	reorder

$L579:
mul	$3,$4,$8
teq	$6,$0,7
div	$0,$3,$6
mflo	$5
addu	$5,$21,$5
.set	noreorder
.set	nomacro
bne	$21,$0,$L875
sw	$5,7268($18)
.set	macro
.set	reorder

subu	$3,$8,$6
mul	$4,$4,$3
teq	$6,$0,7
div	$0,$4,$6
.set	noreorder
.set	nomacro
b	$L584
mflo	$4
.set	macro
.set	reorder

$L900:
li	$5,16			# 0x10
lw	$2,7996($18)
li	$16,-1			# 0xffffffffffffffff
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L621
lw	$28,24($sp)
.set	macro
.set	reorder

$L513:
addiu	$2,$8,8
li	$3,1			# 0x1
subu	$2,$2,$10
sll	$3,$3,$2
sra	$3,$3,1
addu	$3,$3,$7
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
b	$L509
sra	$3,$3,$2
.set	macro
.set	reorder

$L522:
subu	$2,$8,$10
li	$8,1			# 0x1
sll	$8,$8,$2
sra	$8,$8,1
addu	$8,$8,$4
addiu	$8,$8,-1
.set	noreorder
.set	nomacro
b	$L518
sra	$8,$8,$2
.set	macro
.set	reorder

$L893:
lw	$2,7996($18)
lw	$6,%got($LC9)($28)
lw	$4,0($18)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L927
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L922:
lw	$3,10188($18)
li	$2,500			# 0x1f4
beq	$3,$2,$L930
$L471:
lw	$3,9996($18)
li	$2,1			# 0x1
sll	$2,$2,$8
sll	$3,$3,$11
.set	noreorder
.set	nomacro
blez	$3,$L473
sra	$2,$2,1
.set	macro
.set	reorder

addu	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L472
sra	$3,$3,$8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L486
move	$2,$8
.set	macro
.set	reorder

$L923:
lw	$3,10188($18)
li	$2,500			# 0x1f4
.set	noreorder
.set	nomacro
beq	$3,$2,$L931
li	$2,413			# 0x19d
.set	macro
.set	reorder

$L480:
lw	$2,10000($18)
li	$3,1			# 0x1
sll	$3,$3,$8
sll	$2,$2,$11
.set	noreorder
.set	nomacro
blez	$2,$L482
sra	$3,$3,1
.set	macro
.set	reorder

addu	$2,$2,$3
.set	noreorder
.set	nomacro
b	$L481
sra	$8,$2,$8
.set	macro
.set	reorder

$L485:
subu	$2,$8,$11
li	$8,1			# 0x1
sll	$8,$8,$2
sra	$8,$8,1
addu	$8,$8,$4
addiu	$8,$8,-1
.set	noreorder
.set	nomacro
b	$L481
sra	$8,$8,$2
.set	macro
.set	reorder

$L476:
addiu	$2,$8,8
li	$3,1			# 0x1
subu	$2,$2,$11
sll	$3,$3,$2
sra	$3,$3,1
addu	$3,$3,$7
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
b	$L472
sra	$3,$3,$2
.set	macro
.set	reorder

$L888:
lw	$2,7996($18)
li	$5,16			# 0x10
lw	$6,%got($LC16)($28)
lw	$4,0($18)
lw	$7,7992($18)
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L850
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L519:
addu	$2,$2,$3
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
b	$L518
sra	$8,$2,$8
.set	macro
.set	reorder

$L929:
lw	$3,10192($18)
bne	$3,$2,$L517
subu	$8,$8,$10
lw	$2,10000($18)
sll	$8,$11,$8
teq	$8,$0,7
div	$0,$2,$8
.set	noreorder
.set	nomacro
b	$L518
mflo	$8
.set	macro
.set	reorder

$L510:
addu	$3,$3,$2
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
b	$L509
sra	$3,$3,$8
.set	macro
.set	reorder

$L928:
lw	$3,10192($18)
li	$2,413			# 0x19d
bne	$3,$2,$L508
subu	$2,$8,$10
lw	$3,9996($18)
sll	$2,$11,$2
teq	$2,$0,7
div	$0,$3,$2
.set	noreorder
.set	nomacro
b	$L509
mflo	$3
.set	macro
.set	reorder

$L899:
lw	$2,%got(mpeg4_pred_dc.isra.13.part.14)($28)
move	$4,$18
addiu	$25,$2,%lo(mpeg4_pred_dc.isra.13.part.14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_dc.isra.13.part.14
1:	jalr	$25
li	$16,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L621
lw	$28,24($sp)
.set	macro
.set	reorder

$L904:
lw	$2,7996($18)
li	$5,16			# 0x10
lw	$6,%got($LC9)($28)
lw	$4,0($18)
addiu	$6,$6,%lo($LC9)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L658
lw	$3,0($22)
.set	macro
.set	reorder

$L892:
lw	$2,%got(mpeg4_pred_dc.isra.13.part.14)($28)
addiu	$25,$2,%lo(mpeg4_pred_dc.isra.13.part.14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_dc.isra.13.part.14
1:	jalr	$25
move	$4,$18
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L616
lw	$28,24($sp)
.set	macro
.set	reorder

$L917:
#APP
# 120 "vlc_bs.c" 1
mtc0	$2,$21,0
# 0 "" 2
# 121 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
# 123 "vlc_bs.c" 1
mfc0	$2,$21,1
# 0 "" 2
#NO_APP
lw	$5,2872($18)
sll	$2,$2,2
lw	$25,%call16(ff_set_qscale)($28)
move	$4,$18
addiu	$5,$5,-2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_set_qscale
1:	jalr	$25
addu	$5,$5,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L540
lw	$28,24($sp)
.set	macro
.set	reorder

$L903:
lw	$2,%got(mpeg4_pred_dc.isra.13.part.14)($28)
addiu	$25,$2,%lo(mpeg4_pred_dc.isra.13.part.14)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_pred_dc.isra.13.part.14
1:	jalr	$25
move	$4,$18
.set	macro
.set	reorder

sll	$2,$2,16
lw	$28,24($sp)
lw	$3,0($22)
.set	noreorder
.set	nomacro
b	$L658
sra	$2,$2,16
.set	macro
.set	reorder

$L916:
lw	$6,%got($LC14)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC14)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L850
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L925:
lw	$4,7996($18)
move	$2,$0
lw	$5,168($18)
move	$21,$0
lw	$3,7992($18)
mul	$6,$4,$5
.set	noreorder
.set	nomacro
b	$L534
addu	$3,$6,$3
.set	macro
.set	reorder

$L473:
addu	$3,$3,$2
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
b	$L472
sra	$3,$3,$8
.set	macro
.set	reorder

$L930:
lw	$3,10192($18)
li	$2,413			# 0x19d
bne	$3,$2,$L471
subu	$2,$8,$11
lw	$3,9996($18)
sll	$2,$12,$2
teq	$2,$0,7
div	$0,$3,$2
.set	noreorder
.set	nomacro
b	$L472
mflo	$3
.set	macro
.set	reorder

$L482:
addu	$2,$2,$3
addiu	$2,$2,-1
.set	noreorder
.set	nomacro
b	$L481
sra	$8,$2,$8
.set	macro
.set	reorder

$L931:
lw	$3,10192($18)
bne	$3,$2,$L480
subu	$8,$8,$11
lw	$2,10000($18)
sll	$8,$12,$8
teq	$8,$0,7
div	$0,$2,$8
.set	noreorder
.set	nomacro
b	$L481
mflo	$8
.set	macro
.set	reorder

.end	mpeg4_decode_mb_p0
.size	mpeg4_decode_mb_p0, .-mpeg4_decode_mb_p0
.section	.rodata.str1.4
.align	2
$LC17:
.ascii	"I\000"
.align	2
$LC18:
.ascii	"S\000"
.align	2
$LC19:
.ascii	"P\000"
.align	2
$LC20:
.ascii	"B\000"
.align	2
$LC21:
.ascii	"q\000"
.align	2
$LC22:
.ascii	"h\000"
.align	2
$LC23:
.ascii	" VOLC\000"
.align	2
$LC24:
.ascii	" \000"
.align	2
$LC25:
.ascii	"WV1F\000"
.align	2
$LC26:
.ascii	"frame skip %d\012\000"
.align	2
$LC27:
.ascii	"illegal chroma format\012\000"
.align	2
$LC28:
.ascii	"only rectangular vol supported\012\000"
.align	2
$LC29:
.ascii	"Gray shape not supported\012\000"
.align	2
$LC30:
.ascii	"Marker bit missing %s\012\000"
.align	2
$LC31:
.ascii	"before time_increment_resolution\000"
.align	2
$LC32:
.ascii	"time_base.den==0\012\000"
.align	2
$LC33:
.ascii	"before fixed_vop_rate\000"
.align	2
$LC34:
.ascii	"MP4S\000"
.align	2
$LC35:
.ascii	"MPEG4 OBMC not supported (very likely buggy encoder)\012"
.ascii	"\000"
.align	2
$LC36:
.ascii	"Static Sprites not supported\012\000"
.align	2
$LC37:
.ascii	"%d sprite_warping_points\012\000"
.align	2
$LC38:
.ascii	"N-bit not supported\012\000"
.align	2
$LC39:
.ascii	"quant precision %d\012\000"
.align	2
$LC40:
.ascii	"in complexity estimation part 1\000"
.align	2
$LC41:
.ascii	"in complexity estimation part 2\000"
.align	2
$LC42:
.ascii	"Invalid Complexity estimation method %d\012\000"
.align	2
$LC43:
.ascii	"new pred not supported\012\000"
.align	2
$LC44:
.ascii	"reduced resolution VOP not supported\012\000"
.align	2
$LC45:
.ascii	"scalability not supported\012\000"
.align	2
$LC46:
.ascii	"low_delay flag incorrectly, clearing it\012\000"
.align	2
$LC47:
.ascii	"before time_increment\000"
.align	2
$LC48:
.ascii	"hmm, seems the headers are not complete, trying to guess"
.ascii	" time_increment_bits\012\000"
.align	2
$LC49:
.ascii	"my guess is %d bits ;)\012\000"
.globl	__divdi3
.align	2
$LC50:
.ascii	"MPEG4 PTS: %lld\012\000"
.align	2
$LC51:
.ascii	"before vop_coded\000"
.align	2
$LC52:
.ascii	"vop not coded\012\000"
.align	2
$LC53:
.ascii	"sprite_brightness_change not supported\012\000"
.align	2
$LC54:
.ascii	"static sprite not supported\012\000"
.align	2
$LC55:
.ascii	"Error, header damaged or not MPEG4 header (qscale=0)\012"
.ascii	"\000"
.align	2
$LC56:
.ascii	"Error, header damaged or not MPEG4 header (f_code=0)\012"
.ascii	"\000"
.align	2
$LC57:
.ascii	"qp:%d fc:%d,%d %s size:%d pro:%d alt:%d top:%d %spel par"
.ascii	"t:%d resync:%d w:%d a:%d rnd:%d vot:%d%s dc:%d ce:%d/%d/"
.ascii	"%d\012\000"
.align	2
$LC58:
.ascii	"load backward shape isn't supported\012\000"
.align	2
$LC59:
.ascii	"looks like this file was encoded with (divx4/(old)xvid/o"
.ascii	"pendivx) -> forcing low_delay flag\012\000"
.section	.text.JZC_mpeg4_decode_picture_header,"ax",@progbits
.align	2
.align	5
.globl	JZC_mpeg4_decode_picture_header
.set	nomips16
.set	nomicromips
.ent	JZC_mpeg4_decode_picture_header
.type	JZC_mpeg4_decode_picture_header, @function
JZC_mpeg4_decode_picture_header:
.frame	$sp,152,$31		# vars= 8, regs= 10/0, args= 96, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-152
sw	$21,132($sp)
move	$21,$5
lw	$5,8($5)
.cprestore	96
sw	$16,112($sp)
move	$16,$4
subu	$2,$0,$5
sw	$31,148($sp)
sw	$fp,144($sp)
andi	$2,$2,0x7
sw	$23,140($sp)
sw	$22,136($sp)
sw	$20,128($sp)
sw	$19,124($sp)
sw	$18,120($sp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L959
sw	$17,116($sp)
.set	macro
.set	reorder

addu	$5,$2,$5
sw	$5,8($21)
$L959:
lw	$2,%got($LC25)($28)
lw	$3,92($16)
addiu	$4,$2,%lo($LC25)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, %lo($LC25)($2)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$6,$L1240
srl	$2,$5,3
.set	macro
.set	reorder

lw	$18,%got($LC45)($28)
$L1288:
lw	$17,%got($LC44)($28)
lw	$2,%got($LC34)($28)
lw	$13,12($21)
addiu	$18,$18,%lo($LC45)
lw	$19,%got(decode_user_data)($28)
addiu	$17,$17,%lo($LC44)
addiu	$3,$2,%lo($LC34)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $20, 3($3)  
lwr $20, %lo($LC34)($2)  

# 0 "" 2
#NO_APP
li	$6,255			# 0xff
$L1287:
li	$10,16711680			# 0xff0000
li	$9,-16777216			# 0xffffffffff000000
slt	$3,$5,$13
addiu	$10,$10,255
ori	$9,$9,0xff00
li	$12,-256			# 0xffffffffffffff00
li	$11,256			# 0x100
srl	$2,$5,3
andi	$14,$5,0x7
sll	$6,$6,8
.set	noreorder
.set	nomacro
beq	$3,$0,$L1241
addiu	$7,$5,8
.set	macro
.set	reorder

$L964:
lw	$8,0($21)
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$10
and	$4,$4,$9
or	$2,$3,$4
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$14
srl	$2,$2,24
or	$6,$2,$6
and	$2,$6,$12
.set	noreorder
.set	nomacro
beq	$2,$11,$L1242
sw	$7,8($21)
.set	macro
.set	reorder

move	$5,$7
slt	$3,$5,$13
srl	$2,$5,3
andi	$14,$5,0x7
sll	$6,$6,8
.set	noreorder
.set	nomacro
bne	$3,$0,$L964
addiu	$7,$5,8
.set	macro
.set	reorder

$L1241:
li	$2,8			# 0x8
.set	noreorder
.set	nomacro
bne	$13,$2,$L1274
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$2,10188($16)
.set	noreorder
.set	nomacro
bltz	$2,$L1243
lw	$6,%got($LC26)($28)
.set	macro
.set	reorder

$L1293:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$7,8			# 0x8
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC26)
.set	macro
.set	reorder

li	$2,100			# 0x64
$L1168:
lw	$31,148($sp)
lw	$fp,144($sp)
lw	$23,140($sp)
lw	$22,136($sp)
lw	$21,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

$L1242:
addiu	$2,$6,-288
sltu	$2,$2,16
.set	noreorder
.set	nomacro
bne	$2,$0,$L1244
li	$2,434			# 0x1b2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$2,$L1245
li	$2,435			# 0x1b3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$2,$L1246
li	$2,438			# 0x1b6
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$6,$2,$L963
move	$5,$7
.set	macro
.set	reorder

$L1034:
subu	$2,$0,$5
andi	$2,$2,0x7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1287
li	$6,255			# 0xff
.set	macro
.set	reorder

addu	$2,$2,$5
move	$5,$2
.set	noreorder
.set	nomacro
b	$L1287
sw	$2,8($21)
.set	macro
.set	reorder

$L1244:
addiu	$2,$5,9
addiu	$5,$5,17
srl	$3,$2,3
sw	$2,8($21)
andi	$6,$2,0x7
addu	$2,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$5,8($21)
sll	$4,$3,8
and	$3,$2,$10
and	$4,$4,$9
or	$2,$3,$4
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$6
srl	$2,$2,24
sw	$2,10100($16)
lw	$5,8($21)
srl	$3,$5,3
andi	$6,$5,0x7
addu	$3,$8,$3
addiu	$4,$5,1
lbu	$2,0($3)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1247
sw	$4,8($21)
.set	macro
.set	reorder

li	$22,1			# 0x1
$L969:
srl	$2,$4,3
li	$6,16711680			# 0xff0000
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$5,$3,8
li	$3,-16777216			# 0xffffffffff000000
addiu	$6,$6,255
ori	$7,$3,0xff00
and	$3,$2,$6
and	$2,$5,$7
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
andi	$5,$4,0x7
or	$2,$3,$2
sll	$2,$2,$5
addiu	$4,$4,4
srl	$2,$2,28
li	$3,15			# 0xf
sw	$4,8($21)
.set	noreorder
.set	nomacro
beq	$2,$3,$L1248
sw	$2,10068($16)
.set	macro
.set	reorder

lw	$3,%got(ff_h263_pixel_aspect)($28)
sll	$2,$2,3
lw	$4,0($16)
addu	$2,$3,$2
lw	$3,0($2)
lw	$2,4($2)
sw	$3,392($4)
sw	$2,396($4)
$L971:
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1249
sw	$2,10104($16)
.set	macro
.set	reorder

lw	$2,136($16)
bne	$2,$0,$L1230
sw	$0,10096($16)
$L1230:
lw	$6,8($21)
$L974:
srl	$2,$6,3
addu	$8,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($8)  
lwr $2, 0($8)  

# 0 "" 2
#NO_APP
srl	$4,$2,8
sll	$5,$2,8
li	$2,16711680			# 0xff0000
addiu	$2,$2,255
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$4,$6,0x7
ori	$2,$2,0xff00
and	$2,$5,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$4
addiu	$3,$6,2
srl	$2,$2,30
sw	$3,8($21)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1250
sw	$2,9944($16)
.set	macro
.set	reorder

$L976:
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L977
li	$2,1			# 0x1
.set	macro
.set	reorder

$L979:
lw	$7,0($21)
lw	$5,8($21)
$L978:
srl	$3,$5,3
andi	$4,$5,0x7
addu	$3,$7,$3
addiu	$5,$5,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L980
sw	$5,8($21)
.set	macro
.set	reorder

lw	$6,%got($LC30)($28)
li	$5,32			# 0x20
lw	$7,%got($LC31)($28)
move	$4,$0
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC30)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC31)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$7,0($21)
lw	$5,8($21)
$L980:
srl	$2,$5,3
lw	$4,0($16)
addu	$2,$7,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$6,$3,8
addiu	$2,$2,255
sll	$8,$3,8
and	$3,$6,$2
li	$2,-16777216			# 0xffffffffff000000
andi	$6,$5,0x7
ori	$2,$2,0xff00
and	$2,$8,$2
or	$2,$3,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$5,$5,16
or	$2,$3,$2
sll	$2,$2,$6
sw	$5,8($21)
srl	$2,$2,16
.set	noreorder
.set	nomacro
beq	$2,$0,$L1251
sw	$2,36($4)
.set	macro
.set	reorder

addiu	$2,$2,-1
li	$3,32			# 0x20
ori	$2,$2,0x1
clz	$2,$2
subu	$2,$3,$2
sw	$2,9908($16)
lw	$2,8($21)
srl	$5,$2,3
andi	$6,$2,0x7
addu	$5,$7,$5
addiu	$2,$2,1
lbu	$3,0($5)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L983
sw	$2,8($21)
.set	macro
.set	reorder

lw	$6,%got($LC30)($28)
move	$4,$0
lw	$7,%got($LC33)($28)
li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC30)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC33)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$7,0($21)
lw	$2,8($21)
lw	$4,0($16)
$L983:
srl	$5,$2,3
andi	$6,$2,0x7
addu	$5,$7,$5
addiu	$2,$2,1
lbu	$3,0($5)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1252
sw	$2,8($21)
.set	macro
.set	reorder

li	$2,1			# 0x1
sw	$2,32($4)
$L985:
lw	$2,9944($16)
li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$3,$L1233
sw	$0,10168($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L987
li	$8,-16777216			# 0xffffffffff000000
.set	macro
.set	reorder

lw	$6,0($21)
lw	$3,8($21)
$L988:
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sw	$0,10448($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$2,$2,0x1
sw	$2,10436($16)
sw	$2,10364($16)
lw	$5,8($21)
srl	$3,$5,3
andi	$4,$5,0x7
addu	$3,$6,$3
addiu	$5,$5,1
lbu	$2,0($3)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L990
sw	$5,8($21)
.set	macro
.set	reorder

lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L1253
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

$L990:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$22,$2,$L991
srl	$2,$5,3
.set	macro
.set	reorder

andi	$23,$5,0x7
addu	$2,$6,$2
addiu	$5,$5,1
lbu	$2,0($2)
sw	$5,8($21)
sll	$23,$2,$23
andi	$23,$23,0x00ff
srl	$23,$23,7
sw	$23,9948($16)
$L992:
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
beq	$23,$2,$L1254
lw	$6,%got($LC36)($28)
.set	macro
.set	reorder

addiu	$23,$23,-1
sltu	$23,$23,2
bne	$23,$0,$L1120
$L1231:
lw	$8,0($21)
$L995:
lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$6,$4,1
lbu	$2,0($3)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1255
sw	$6,8($21)
.set	macro
.set	reorder

$L999:
li	$2,5			# 0x5
sw	$2,10040($16)
$L1002:
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1011
sw	$2,10164($16)
.set	macro
.set	reorder

lw	$3,%got(ff_mpeg4_default_intra_matrix)($28)
addiu	$5,$16,5664
lw	$4,%got(ff_mpeg4_default_non_intra_matrix)($28)
addiu	$7,$3,128
$L1006:
lbu	$2,0($5)
addiu	$3,$3,2
lhu	$6,-2($3)
addiu	$4,$4,2
addiu	$5,$5,1
sll	$2,$2,1
addu	$2,$16,$2
sh	$6,8076($2)
sh	$6,8204($2)
lhu	$6,-2($4)
sh	$6,8332($2)
.set	noreorder
.set	nomacro
bne	$3,$7,$L1006
sh	$6,8460($2)
.set	macro
.set	reorder

lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$4,$4,1
lbu	$2,0($3)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1007
sw	$4,8($21)
.set	macro
.set	reorder

li	$12,16711680			# 0xff0000
lw	$2,%got(ff_zigzag_direct)($28)
li	$11,-16777216			# 0xffffffffff000000
move	$9,$0
move	$3,$0
addiu	$12,$12,255
li	$13,64			# 0x40
.set	noreorder
.set	nomacro
b	$L1009
ori	$11,$11,0xff00
.set	macro
.set	reorder

$L1256:
lbu	$6,0($10)
move	$9,$5
addiu	$3,$3,1
addu	$5,$16,$6
lbu	$5,5664($5)
sll	$5,$5,1
addu	$5,$16,$5
sh	$7,8076($5)
.set	noreorder
.set	nomacro
beq	$3,$13,$L1007
sh	$7,8204($5)
.set	macro
.set	reorder

$L1009:
srl	$5,$4,3
andi	$14,$4,0x7
addu	$5,$8,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
srl	$6,$7,8
sll	$7,$7,8
and	$6,$6,$12
and	$7,$7,$11
or	$7,$6,$7
sll	$6,$7,16
srl	$7,$7,16
addiu	$4,$4,8
or	$5,$6,$7
sll	$5,$5,$14
addu	$10,$2,$3
sw	$4,8($21)
srl	$5,$5,24
.set	noreorder
.set	nomacro
bne	$5,$0,$L1256
andi	$7,$5,0xffff
.set	macro
.set	reorder

addu	$3,$2,$3
addiu	$6,$2,64
andi	$5,$9,0xffff
$L1010:
lbu	$2,0($3)
addiu	$3,$3,1
addu	$2,$16,$2
lbu	$2,5664($2)
sll	$2,$2,1
addu	$2,$16,$2
sh	$5,8076($2)
.set	noreorder
.set	nomacro
bne	$3,$6,$L1010
sh	$5,8204($2)
.set	macro
.set	reorder

$L1007:
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$9,$4,1
lbu	$2,0($3)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1257
sw	$9,8($21)
.set	macro
.set	reorder

$L1011:
li	$2,1			# 0x1
beq	$22,$2,$L1258
$L1004:
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10044($16)
lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$23,$4,1
lbu	$2,0($3)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1259
sw	$23,8($21)
.set	macro
.set	reorder

$L1016:
sw	$0,10184($16)
sw	$0,10180($16)
sw	$0,10176($16)
lw	$3,8($21)
$L1023:
srl	$4,$3,3
$L1292:
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
xori	$2,$2,0x1
sw	$2,10092($16)
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1024
sw	$2,10080($16)
.set	macro
.set	reorder

lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10088($16)
$L1024:
li	$2,1			# 0x1
beq	$22,$2,$L1025
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1026
sw	$2,10060($16)
.set	macro
.set	reorder

lw	$3,8($21)
$L1027:
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1260
sw	$2,10064($16)
.set	macro
.set	reorder

$L1028:
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1261
sw	$2,10048($16)
.set	macro
.set	reorder

$L1233:
lw	$5,8($21)
.set	noreorder
.set	nomacro
b	$L1034
lw	$13,12($21)
.set	macro
.set	reorder

$L977:
.set	noreorder
.set	nomacro
beq	$22,$2,$L979
lw	$6,%got($LC29)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC29)
.set	macro
.set	reorder

lw	$5,8($21)
lw	$28,96($sp)
lw	$7,0($21)
addiu	$5,$5,4
.set	noreorder
.set	nomacro
b	$L978
sw	$5,8($21)
.set	macro
.set	reorder

$L1261:
lw	$6,8($21)
li	$7,16711680			# 0xff0000
lw	$5,0($21)
li	$11,-16777216			# 0xffffffffff000000
lw	$10,4($21)
addiu	$7,$7,255
srl	$3,$6,3
lw	$9,12($21)
andi	$4,$6,0x7
addu	$8,$8,$3
addiu	$3,$6,1
ori	$11,$11,0xff00
lbu	$2,0($8)
sw	$3,8($21)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10052($16)
lw	$2,8($21)
addiu	$25,$2,25
addiu	$24,$2,5
addiu	$8,$2,10
addiu	$13,$2,15
addiu	$14,$2,20
sw	$24,8($21)
srl	$23,$25,3
srl	$3,$24,3
addu	$23,$5,$23
srl	$12,$8,3
srl	$15,$13,3
srl	$22,$14,3
addu	$3,$5,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sw	$8,8($21)
sll	$4,$4,8
addu	$12,$5,$12
addu	$15,$5,$15
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($12)  
lwr $fp, 0($12)  

# 0 "" 2
#NO_APP
addu	$22,$5,$22
sw	$13,8($21)
move	$12,$fp
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($15)  
lwr $fp, 0($15)  

# 0 "" 2
#NO_APP
sw	$14,8($21)
move	$15,$fp
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($22)  
lwr $fp, 0($22)  

# 0 "" 2
#NO_APP
sw	$25,8($21)
and	$3,$3,$7
move	$22,$fp
and	$fp,$4,$11
lbu	$4,0($23)
or	$23,$3,$fp
sll	$3,$23,16
andi	$25,$25,0x7
srl	$23,$23,16
sll	$4,$4,$25
or	$3,$3,$23
andi	$24,$24,0x7
andi	$4,$4,0x00ff
sll	$3,$3,$24
addiu	$2,$2,26
srl	$4,$4,7
srl	$3,$3,27
sw	$2,8($21)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1031
sw	$4,10056($16)
.set	macro
.set	reorder

sll	$2,$12,8
srl	$3,$12,8
and	$2,$2,$11
and	$3,$3,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$8,$8,0x7
or	$2,$3,$2
sll	$8,$2,$8
srl	$2,$8,27
.set	noreorder
.set	nomacro
beq	$2,$0,$L1031
sll	$2,$15,8
.set	macro
.set	reorder

srl	$3,$15,8
and	$2,$2,$11
and	$3,$3,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$13,$13,0x7
or	$2,$3,$2
sll	$13,$2,$13
srl	$2,$13,27
.set	noreorder
.set	nomacro
beq	$2,$0,$L1031
sll	$2,$22,8
.set	macro
.set	reorder

srl	$22,$22,8
and	$11,$2,$11
and	$7,$22,$7
or	$7,$11,$7
sll	$2,$7,16
srl	$7,$7,16
andi	$14,$14,0x7
or	$7,$2,$7
sll	$14,$7,$14
srl	$2,$14,27
.set	noreorder
.set	nomacro
beq	$2,$0,$L1031
lw	$25,%call16(av_log)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$6,$18
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$5,8($21)
.set	noreorder
.set	nomacro
b	$L1034
lw	$13,12($21)
.set	macro
.set	reorder

$L1254:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC36)
.set	macro
.set	reorder

lw	$2,9948($16)
addiu	$3,$2,-1
sltu	$3,$3,2
.set	noreorder
.set	nomacro
beq	$3,$0,$L1231
lw	$28,96($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$23,$L1262
li	$2,16711680			# 0xff0000
.set	macro
.set	reorder

$L1120:
lw	$8,0($21)
lw	$4,8($21)
$L997:
srl	$3,$4,3
li	$2,16711680			# 0xff0000
li	$6,-16777216			# 0xffffffffff000000
addu	$3,$8,$3
addiu	$9,$2,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
ori	$6,$6,0xff00
and	$3,$3,$9
and	$5,$5,$6
or	$3,$3,$5
sll	$2,$3,16
srl	$3,$3,16
andi	$7,$4,0x7
or	$2,$2,$3
sll	$7,$2,$7
addiu	$4,$4,6
srl	$7,$7,26
sltu	$2,$7,4
sw	$4,8($21)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1263
sw	$7,9972($16)
.set	macro
.set	reorder

lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($3)  
lwr $fp, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$fp,8
sll	$3,$fp,8
and	$2,$2,$9
and	$3,$3,$6
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$4,$4,2
or	$2,$3,$2
sll	$2,$2,$5
sw	$4,8($21)
srl	$2,$2,30
sw	$2,10072($16)
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
lw	$3,9948($16)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,9968($16)
li	$2,1			# 0x1
bne	$3,$2,$L995
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$3,$3,1
lbu	$2,0($4)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10076($16)
lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$6,$4,1
lbu	$2,0($3)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L999
sw	$6,8($21)
.set	macro
.set	reorder

$L1255:
srl	$2,$6,3
li	$10,16711680			# 0xff0000
addu	$2,$8,$2
li	$9,-16777216			# 0xffffffffff000000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
addiu	$10,$10,255
srl	$3,$3,8
sll	$2,$2,8
ori	$9,$9,0xff00
and	$2,$2,$9
and	$3,$3,$10
or	$3,$3,$2
sll	$7,$3,16
srl	$5,$3,16
andi	$3,$6,0x7
or	$5,$7,$5
sll	$3,$5,$3
addiu	$2,$4,5
srl	$3,$3,28
sw	$2,8($21)
sw	$3,10040($16)
lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$8,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($8)  
lwr $3, 0($8)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$2,$2,$10
and	$3,$3,$9
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$4,$4,4
or	$2,$3,$2
sll	$2,$2,$5
li	$3,8			# 0x8
srl	$2,$2,28
.set	noreorder
.set	nomacro
beq	$2,$3,$L1000
sw	$4,8($21)
.set	macro
.set	reorder

lw	$6,%got($LC38)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC38)
.set	macro
.set	reorder

lw	$28,96($sp)
$L1000:
lw	$7,10040($16)
li	$2,5			# 0x5
.set	noreorder
.set	nomacro
beq	$7,$2,$L1232
li	$5,16			# 0x10
.set	macro
.set	reorder

lw	$6,%got($LC39)($28)
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC39)
.set	macro
.set	reorder

lw	$28,96($sp)
$L1232:
.set	noreorder
.set	nomacro
b	$L1002
lw	$8,0($21)
.set	macro
.set	reorder

$L1031:
sw	$0,10048($16)
move	$13,$9
sw	$5,0($21)
move	$5,$6
sw	$10,4($21)
sw	$6,8($21)
.set	noreorder
.set	nomacro
b	$L1034
sw	$9,12($21)
.set	macro
.set	reorder

$L991:
andi	$23,$5,0x7
addu	$2,$6,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($2)  
lwr $6, 0($2)  

# 0 "" 2
#NO_APP
li	$2,16711680			# 0xff0000
srl	$4,$6,8
addiu	$2,$2,255
sll	$6,$6,8
and	$3,$4,$2
li	$2,-16777216			# 0xffffffffff000000
addiu	$5,$5,2
ori	$2,$2,0xff00
and	$2,$6,$2
or	$2,$3,$2
sw	$5,8($21)
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$23,$2,$23
srl	$23,$23,30
.set	noreorder
.set	nomacro
b	$L992
sw	$23,9948($16)
.set	macro
.set	reorder

$L1245:
addiu	$25,$19,%lo(decode_user_data)
move	$5,$21
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_user_data
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$5,8($21)
.set	noreorder
.set	nomacro
b	$L1034
lw	$13,12($21)
.set	macro
.set	reorder

$L1246:
srl	$2,$7,3
addiu	$11,$5,13
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$11,8($21)
sll	$3,$3,8
and	$2,$2,$10
srl	$4,$11,3
and	$3,$3,$9
or	$3,$2,$3
addu	$4,$8,$4
sll	$2,$3,16
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($4)  
lwr $6, 0($4)  

# 0 "" 2
#NO_APP
srl	$3,$3,16
srl	$4,$6,8
sll	$12,$6,8
or	$2,$2,$3
addiu	$6,$5,20
and	$4,$4,$10
andi	$3,$7,0x7
and	$12,$12,$9
sw	$6,8($21)
or	$4,$4,$12
sll	$3,$2,$3
srl	$7,$6,3
sll	$2,$4,16
srl	$4,$4,16
srl	$3,$3,27
addu	$8,$8,$7
or	$2,$2,$4
andi	$11,$11,0x7
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($8)  
lwr $4, 0($8)  

# 0 "" 2
#NO_APP
srl	$7,$4,8
sll	$4,$4,8
sll	$8,$3,2
sll	$2,$2,$11
sll	$3,$3,6
and	$10,$7,$10
and	$7,$4,$9
or	$7,$10,$7
subu	$3,$3,$8
srl	$2,$2,26
sll	$4,$7,16
srl	$7,$7,16
addu	$2,$2,$3
andi	$6,$6,0x7
or	$3,$4,$7
sll	$3,$3,$6
sll	$4,$2,2
sll	$2,$2,6
srl	$3,$3,26
subu	$2,$2,$4
addiu	$5,$5,26
addu	$2,$3,$2
sw	$5,8($21)
sw	$2,9916($16)
lw	$2,8($21)
lw	$13,12($21)
addiu	$2,$2,2
move	$5,$2
.set	noreorder
.set	nomacro
b	$L1034
sw	$2,8($21)
.set	macro
.set	reorder

$L1250:
lw	$6,%got($LC28)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC28)
.set	macro
.set	reorder

lw	$28,96($sp)
.set	noreorder
.set	nomacro
b	$L976
lw	$2,9944($16)
.set	macro
.set	reorder

$L1249:
lw	$2,8($21)
li	$5,16711680			# 0xff0000
addiu	$5,$5,255
srl	$3,$2,3
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$4,$4,8
and	$3,$3,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$4,$4,$5
or	$3,$3,$4
sll	$4,$3,16
srl	$3,$3,16
andi	$5,$2,0x7
or	$3,$4,$3
sll	$3,$3,$5
addiu	$2,$2,2
srl	$3,$3,30
li	$4,1			# 0x1
.set	noreorder
.set	nomacro
beq	$3,$4,$L973
sw	$2,8($21)
.set	macro
.set	reorder

lw	$6,%got($LC27)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC27)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$8,0($21)
lw	$2,8($21)
$L973:
srl	$3,$2,3
andi	$4,$2,0x7
addu	$3,$8,$3
addiu	$2,$2,1
lbu	$3,0($3)
sw	$2,8($21)
sll	$2,$3,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10096($16)
lw	$3,8($21)
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$8,$4
addiu	$6,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L974
sw	$6,8($21)
.set	macro
.set	reorder

addiu	$6,$3,80
.set	noreorder
.set	nomacro
b	$L974
sw	$6,8($21)
.set	macro
.set	reorder

$L1247:
srl	$2,$4,3
andi	$22,$4,0x7
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$10,$2,$10
and	$2,$3,$9
or	$2,$10,$2
sll	$3,$2,16
srl	$2,$2,16
addiu	$4,$5,8
or	$2,$3,$2
sll	$22,$2,$22
sw	$4,8($21)
.set	noreorder
.set	nomacro
b	$L969
srl	$22,$22,28
.set	macro
.set	reorder

$L1252:
srl	$3,$2,3
lw	$8,9908($16)
li	$6,16711680			# 0xff0000
addu	$7,$7,$3
addiu	$6,$6,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($7)  
lwr $5, 0($7)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$5,$5,$6
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$6,$2,0x7
or	$3,$5,$3
sll	$3,$3,$6
subu	$5,$0,$8
addu	$2,$2,$8
srl	$3,$3,$5
sw	$2,8($21)
.set	noreorder
.set	nomacro
b	$L985
sw	$3,32($4)
.set	macro
.set	reorder

$L1257:
li	$12,16711680			# 0xff0000
lw	$2,%got(ff_zigzag_direct)($28)
li	$11,-16777216			# 0xffffffffff000000
move	$4,$0
move	$3,$0
addiu	$12,$12,255
li	$13,64			# 0x40
.set	noreorder
.set	nomacro
b	$L1013
ori	$11,$11,0xff00
.set	macro
.set	reorder

$L1264:
lbu	$6,0($10)
move	$4,$5
addiu	$3,$3,1
addu	$5,$16,$6
lbu	$5,5664($5)
sll	$5,$5,1
addu	$5,$16,$5
sh	$7,8332($5)
.set	noreorder
.set	nomacro
beq	$3,$13,$L1011
sh	$7,8460($5)
.set	macro
.set	reorder

$L1013:
srl	$5,$9,3
andi	$14,$9,0x7
addu	$5,$8,$5
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $7, 3($5)  
lwr $7, 0($5)  

# 0 "" 2
#NO_APP
srl	$6,$7,8
sll	$7,$7,8
and	$6,$6,$12
and	$7,$7,$11
or	$7,$6,$7
sll	$6,$7,16
srl	$7,$7,16
addiu	$9,$9,8
or	$5,$6,$7
sll	$5,$5,$14
addu	$10,$2,$3
sw	$9,8($21)
srl	$5,$5,24
.set	noreorder
.set	nomacro
bne	$5,$0,$L1264
andi	$7,$5,0xffff
.set	macro
.set	reorder

addu	$3,$2,$3
addiu	$5,$2,64
andi	$4,$4,0xffff
$L1014:
lbu	$2,0($3)
addiu	$3,$3,1
addu	$2,$16,$2
lbu	$2,5664($2)
sll	$2,$2,1
addu	$2,$16,$2
sh	$4,8332($2)
.set	noreorder
.set	nomacro
bne	$3,$5,$L1014
sh	$4,8460($2)
.set	macro
.set	reorder

li	$2,1			# 0x1
bne	$22,$2,$L1004
$L1258:
sw	$0,10044($16)
lw	$4,8($21)
srl	$3,$4,3
andi	$5,$4,0x7
addu	$3,$8,$3
addiu	$23,$4,1
lbu	$2,0($3)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1016
sw	$23,8($21)
.set	macro
.set	reorder

$L1259:
srl	$2,$23,3
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($2)  
lwr $5, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$2,$5,8
li	$5,16711680			# 0xff0000
addiu	$5,$5,255
and	$3,$3,$5
li	$5,-16777216			# 0xffffffffff000000
ori	$5,$5,0xff00
and	$2,$2,$5
or	$3,$3,$2
sll	$5,$3,16
srl	$3,$3,16
andi	$2,$23,0x7
or	$3,$5,$3
addiu	$5,$4,3
sll	$2,$3,$2
sw	$5,8($21)
.set	noreorder
.set	nomacro
bgez	$2,$L1265
srl	$7,$2,30
.set	macro
.set	reorder

lw	$6,%got($LC42)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC42)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$8,0($21)
.set	noreorder
.set	nomacro
b	$L1023
lw	$3,8($21)
.set	macro
.set	reorder

$L987:
lw	$3,8($21)
li	$7,16711680			# 0xff0000
lw	$6,0($21)
addiu	$7,$7,255
addiu	$2,$3,1
ori	$8,$8,0xff00
srl	$4,$2,3
sw	$2,8($21)
andi	$5,$2,0x7
addu	$2,$6,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($2)  
lwr $4, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$4,8
sll	$4,$4,8
and	$2,$2,$7
and	$4,$4,$8
or	$2,$2,$4
sll	$4,$2,16
srl	$2,$2,16
addiu	$9,$3,15
or	$2,$4,$2
sll	$2,$2,$5
addiu	$3,$3,29
sw	$9,8($21)
srl	$4,$9,3
srl	$2,$2,19
addu	$4,$6,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$2,$0,$L988
sw	$3,8($21)
.set	macro
.set	reorder

sll	$4,$5,8
srl	$5,$5,8
and	$8,$4,$8
and	$4,$5,$7
or	$4,$8,$4
sll	$5,$4,16
srl	$4,$4,16
andi	$9,$9,0x7
or	$4,$5,$4
sll	$9,$4,$9
srl	$4,$9,19
beq	$4,$0,$L988
lw	$5,8($16)
beq	$5,$0,$L989
lw	$5,92($16)
beq	$5,$20,$L988
$L989:
sw	$2,8($16)
sw	$4,12($16)
.set	noreorder
.set	nomacro
b	$L988
lw	$3,8($21)
.set	macro
.set	reorder

$L1025:
sw	$0,10060($16)
.set	noreorder
.set	nomacro
b	$L1028
sw	$0,10064($16)
.set	macro
.set	reorder

$L1260:
lw	$25,%call16(av_log)($28)
li	$5,16			# 0x10
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$6,$17
.set	macro
.set	reorder

lw	$28,96($sp)
.set	noreorder
.set	nomacro
b	$L1028
lw	$8,0($21)
.set	macro
.set	reorder

$L1026:
lw	$6,%got($LC43)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC43)
.set	macro
.set	reorder

lw	$3,8($21)
lw	$28,96($sp)
lw	$8,0($21)
addiu	$3,$3,3
.set	noreorder
.set	nomacro
b	$L1027
sw	$3,8($21)
.set	macro
.set	reorder

$L1248:
lw	$5,8($21)
lw	$4,0($16)
srl	$3,$5,3
andi	$9,$5,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($3)  
lwr $fp, 0($3)  

# 0 "" 2
#NO_APP
srl	$2,$fp,8
sll	$3,$fp,8
and	$2,$2,$6
and	$3,$3,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$5,$5,8
or	$2,$3,$2
sll	$2,$2,$9
sw	$5,8($21)
srl	$2,$2,24
sw	$2,392($4)
lw	$5,8($21)
srl	$3,$5,3
andi	$9,$5,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sll	$3,$3,8
srl	$2,$2,8
and	$3,$3,$7
and	$6,$2,$6
or	$2,$6,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$5,$5,8
or	$2,$3,$2
sll	$2,$2,$9
sw	$5,8($21)
srl	$2,$2,24
.set	noreorder
.set	nomacro
b	$L971
sw	$2,396($4)
.set	macro
.set	reorder

$L1240:
lw	$8,0($21)
li	$7,16711680			# 0xff0000
li	$4,-16777216			# 0xffffffffff000000
addu	$2,$8,$2
addiu	$7,$7,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
ori	$4,$4,0xff00
and	$3,$3,$4
and	$2,$2,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
andi	$6,$5,0x7
or	$2,$3,$2
sll	$2,$2,$6
li	$3,5701632			# 0x570000
srl	$2,$2,8
addiu	$3,$3,22064
.set	noreorder
.set	nomacro
bne	$2,$3,$L1288
lw	$18,%got($LC45)($28)
.set	macro
.set	reorder

addiu	$2,$5,24
addiu	$5,$5,32
srl	$3,$2,3
sw	$2,8($21)
andi	$6,$2,0x7
addu	$2,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sll	$3,$3,8
and	$7,$2,$7
and	$4,$3,$4
or	$2,$7,$4
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$6
li	$3,240			# 0xf0
srl	$2,$2,24
.set	noreorder
.set	nomacro
bne	$2,$3,$L1288
sw	$5,8($21)
.set	macro
.set	reorder

$L963:
lw	$2,64($16)
li	$3,524288			# 0x80000
and	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L1037
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,10096($16)
sltu	$2,$2,1
$L1038:
lw	$4,0($16)
li	$7,16711680			# 0xff0000
addiu	$7,$7,255
sw	$2,264($4)
lw	$6,8($21)
srl	$2,$6,3
andi	$9,$6,0x7
addu	$8,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($8)  
lwr $5, 0($8)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$7
li	$7,-16777216			# 0xffffffffff000000
addiu	$6,$6,2
ori	$7,$7,0xff00
and	$5,$5,$7
or	$5,$3,$5
sw	$6,8($21)
sll	$3,$5,16
srl	$5,$5,16
or	$2,$3,$5
sll	$2,$2,$9
li	$3,3			# 0x3
srl	$2,$2,30
addiu	$2,$2,1
.set	noreorder
.set	nomacro
beq	$2,$3,$L1266
sw	$2,2904($16)
.set	macro
.set	reorder

$L1039:
lw	$2,10080($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1289
lw	$2,%got(mpeg4_decode_mb_p0)($28)
.set	macro
.set	reorder

lw	$3,2904($16)
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L1040
li	$2,1			# 0x1
.set	macro
.set	reorder

sw	$2,10084($16)
lw	$2,%got(mpeg4_decode_partitioned_mb)($28)
addiu	$2,$2,%lo(mpeg4_decode_partitioned_mb)
sw	$2,10528($16)
$L1119:
lw	$7,8($21)
lw	$6,0($21)
srl	$4,$7,3
andi	$5,$7,0x7
addu	$4,$6,$4
addiu	$3,$7,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1130
sw	$3,8($21)
.set	macro
.set	reorder

$L1043:
srl	$4,$3,3
andi	$5,$3,0x7
addu	$4,$6,$4
subu	$17,$3,$7
addiu	$3,$3,1
lbu	$2,0($4)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1043
sw	$3,8($21)
.set	macro
.set	reorder

$L1042:
srl	$2,$3,3
andi	$4,$3,0x7
addu	$6,$6,$2
addiu	$3,$3,1
lbu	$2,0($6)
sll	$2,$2,$4
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1044
sw	$3,8($21)
.set	macro
.set	reorder

lw	$6,%got($LC30)($28)
move	$4,$0
lw	$7,%got($LC47)($28)
li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC30)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC47)
.set	macro
.set	reorder

lw	$28,96($sp)
$L1044:
lw	$3,9908($16)
.set	noreorder
.set	nomacro
beq	$3,$0,$L1290
lw	$6,%got($LC48)($28)
.set	macro
.set	reorder

lw	$4,8($21)
li	$7,16711680			# 0xff0000
lw	$5,0($21)
addiu	$7,$7,255
srl	$2,$4,3
addu	$5,$5,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
srl	$2,$6,8
sll	$6,$6,8
and	$2,$2,$7
li	$7,-16777216			# 0xffffffffff000000
ori	$7,$7,0xff00
and	$6,$6,$7
or	$2,$2,$6
sll	$6,$2,16
srl	$2,$2,16
andi	$7,$4,0x7
or	$6,$6,$2
sll	$6,$6,$7
nor	$2,$0,$3
srl	$6,$6,$2
andi	$6,$6,0x1
.set	noreorder
.set	nomacro
bne	$6,$0,$L1046
addiu	$2,$5,3
.set	macro
.set	reorder

lw	$6,%got($LC48)($28)
$L1290:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
li	$18,26			# 0x1a
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC48)
.set	macro
.set	reorder

li	$2,1			# 0x1
li	$10,16711680			# 0xff0000
lw	$28,96($sp)
li	$9,-16777216			# 0xffffffffff000000
lw	$6,2904($16)
li	$7,1			# 0x1
sw	$2,9908($16)
li	$8,2			# 0x2
addiu	$10,$10,255
li	$15,48			# 0x30
li	$13,4			# 0x4
li	$11,27			# 0x1b
li	$14,24			# 0x18
li	$12,16			# 0x10
.set	noreorder
.set	nomacro
b	$L1051
ori	$9,$9,0xff00
.set	macro
.set	reorder

$L1268:
beq	$6,$13,$L1267
$L1048:
lw	$2,8($21)
lw	$3,0($21)
srl	$4,$2,3
andi	$2,$2,0x7
addu	$3,$3,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $4, 3($3)  
lwr $4, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$4,8
sll	$5,$4,8
and	$4,$3,$10
and	$3,$5,$9
or	$3,$4,$3
sll	$4,$3,16
srl	$3,$3,16
or	$3,$4,$3
sll	$2,$3,$2
srl	$2,$2,$19
andi	$2,$2,0x1f
beq	$2,$14,$L1049
addiu	$7,$7,1
$L1291:
.set	noreorder
.set	nomacro
beq	$7,$12,$L1049
sw	$7,9908($16)
.set	macro
.set	reorder

$L1051:
.set	noreorder
.set	nomacro
bne	$6,$8,$L1268
subu	$19,$11,$7
.set	macro
.set	reorder

lw	$19,8($21)
$L1286:
subu	$2,$18,$7
lw	$4,0($21)
srl	$3,$19,3
andi	$19,$19,0x7
addu	$3,$4,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$10
and	$5,$5,$9
or	$5,$4,$5
sll	$4,$5,16
srl	$3,$5,16
or	$3,$4,$3
sll	$3,$3,$19
srl	$2,$3,$2
andi	$2,$2,0x37
.set	noreorder
.set	nomacro
bne	$2,$15,$L1291
addiu	$7,$7,1
.set	macro
.set	reorder

addiu	$7,$7,-1
$L1049:
lw	$6,%got($LC49)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC49)
.set	macro
.set	reorder

lw	$4,8($21)
lw	$5,0($21)
lw	$28,96($sp)
srl	$2,$4,3
lw	$3,9908($16)
andi	$7,$4,0x7
addu	$5,$5,$2
addiu	$2,$5,3
$L1046:
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 0($2)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
sll	$5,$6,8
srl	$2,$6,8
li	$6,16711680			# 0xff0000
addiu	$6,$6,255
and	$2,$2,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$6,$5,$6
or	$2,$2,$6
addu	$5,$3,$4
sll	$4,$2,16
srl	$2,$2,16
sw	$5,8($21)
subu	$3,$0,$3
or	$2,$4,$2
lw	$5,2904($16)
sll	$2,$2,$7
srl	$3,$2,$3
li	$2,3			# 0x3
beq	$5,$2,$L1052
lw	$18,0($16)
lw	$5,9916($16)
lw	$4,88($16)
lw	$2,36($18)
addu	$17,$17,$5
sw	$5,9912($16)
andi	$4,$4,0x8
mul	$5,$17,$2
sw	$17,9916($16)
addu	$2,$5,$3
sra	$3,$2,31
sw	$2,9920($16)
.set	noreorder
.set	nomacro
bne	$4,$0,$L1053
sw	$3,9924($16)
.set	macro
.set	reorder

lw	$4,9928($16)
$L1054:
subu	$4,$2,$4
sw	$2,9928($16)
sw	$3,9932($16)
sh	$4,9936($16)
$L1056:
lw	$6,32($18)
.set	noreorder
.set	nomacro
beq	$6,$0,$L1076
move	$4,$0
.set	macro
.set	reorder

srl	$2,$6,31
lw	$4,9920($16)
lw	$5,9924($16)
sra	$7,$6,31
addu	$2,$2,$6
lw	$25,%call16(__divdi3)($28)
lw	$17,2696($16)
sra	$3,$2,1
sra	$2,$2,31
addu	$4,$3,$4
addu	$5,$2,$5
sltu	$3,$4,$3
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
addu	$5,$3,$5
.set	macro
.set	reorder

lw	$28,96($sp)
sw	$2,56($17)
sw	$3,60($17)
$L1077:
lw	$2,404($18)
andi	$2,$2,0x200
.set	noreorder
.set	nomacro
bne	$2,$0,$L1269
li	$5,48			# 0x30
.set	macro
.set	reorder

$L1078:
lw	$2,8($21)
lw	$4,0($21)
srl	$5,$2,3
andi	$6,$2,0x7
addu	$5,$4,$5
addiu	$2,$2,1
lbu	$3,0($5)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1079
sw	$2,8($21)
.set	macro
.set	reorder

lw	$6,%got($LC30)($28)
move	$4,$0
lw	$7,%got($LC51)($28)
li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC30)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC51)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$4,0($21)
lw	$2,8($21)
$L1079:
srl	$3,$2,3
andi	$6,$2,0x7
addu	$3,$4,$3
addiu	$5,$2,1
lbu	$3,0($3)
sll	$3,$3,$6
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1080
sw	$5,8($21)
.set	macro
.set	reorder

lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
bne	$2,$0,$L1270
lw	$6,%got($LC52)($28)
.set	macro
.set	reorder

$L1236:
li	$2,100			# 0x64
$L1273:
lw	$31,148($sp)
$L1301:
lw	$fp,144($sp)
lw	$23,140($sp)
lw	$22,136($sp)
lw	$21,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

$L1253:
lw	$6,%got($LC35)($28)
li	$5,32			# 0x20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC35)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$6,0($21)
.set	noreorder
.set	nomacro
b	$L990
lw	$5,8($21)
.set	macro
.set	reorder

$L1265:
srl	$2,$5,3
andi	$5,$5,0x7
addu	$2,$8,$2
addiu	$3,$4,4
lbu	$2,0($2)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1018
sw	$3,8($21)
.set	macro
.set	reorder

srl	$5,$3,3
addiu	$2,$4,5
addu	$4,$8,$5
andi	$3,$3,0x7
lbu	$4,0($4)
sw	$2,8($21)
lw	$5,10176($16)
sll	$2,$4,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$5,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
andi	$3,$2,0x7
addu	$4,$8,$4
addiu	$5,$2,1
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$3,8($21)
$L1018:
srl	$5,$3,3
andi	$4,$3,0x7
addu	$5,$8,$5
addiu	$2,$3,1
lbu	$5,0($5)
sll	$4,$5,$4
andi	$4,$4,0x00ff
srl	$4,$4,7
.set	noreorder
.set	nomacro
bne	$4,$0,$L1019
sw	$2,8($21)
.set	macro
.set	reorder

srl	$4,$2,3
addiu	$3,$3,2
addu	$4,$8,$4
andi	$2,$2,0x7
lbu	$4,0($4)
sw	$3,8($21)
lw	$3,10176($16)
sll	$2,$4,$2
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$3,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
$L1019:
srl	$4,$2,3
andi	$3,$2,0x7
addu	$4,$8,$4
addiu	$5,$2,1
lbu	$4,0($4)
sll	$3,$4,$3
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
beq	$3,$0,$L1271
sw	$5,8($21)
.set	macro
.set	reorder

srl	$3,$5,3
andi	$5,$5,0x7
addu	$3,$8,$3
addiu	$4,$2,2
lbu	$3,0($3)
sll	$3,$3,$5
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1122
sw	$4,8($21)
.set	macro
.set	reorder

srl	$3,$4,3
addiu	$5,$2,3
addu	$3,$8,$3
andi	$2,$4,0x7
lbu	$3,0($3)
sw	$5,8($21)
lw	$5,10176($16)
sll	$2,$3,$2
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$5,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,5
andi	$2,$2,0x4
addu	$2,$4,$2
sw	$2,10176($16)
lw	$4,8($21)
$L1122:
srl	$3,$4,3
andi	$2,$4,0x7
addu	$3,$8,$3
addiu	$5,$4,1
lbu	$3,0($3)
sll	$2,$3,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
bne	$2,$0,$L1021
sw	$5,8($21)
.set	macro
.set	reorder

srl	$3,$5,3
addiu	$4,$4,2
addu	$3,$8,$3
andi	$5,$5,0x7
lbu	$2,0($3)
sw	$4,8($21)
lw	$3,10180($16)
sll	$2,$2,$5
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$3,$2
sw	$2,10180($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10184($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10184($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
lw	$2,8($21)
srl	$4,$2,3
addiu	$5,$2,1
addu	$4,$8,$4
andi	$3,$2,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
lw	$5,8($21)
$L1021:
srl	$4,$5,3
andi	$2,$5,0x7
addu	$4,$8,$4
addiu	$3,$5,1
lbu	$4,0($4)
sll	$2,$4,$2
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1272
sw	$3,8($21)
.set	macro
.set	reorder

li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$7,$2,$L1292
srl	$4,$3,3
.set	macro
.set	reorder

addiu	$5,$5,2
addu	$4,$8,$4
andi	$3,$3,0x7
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10176($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10176($16)
lw	$2,8($21)
srl	$4,$2,3
andi	$3,$2,0x7
addu	$4,$8,$4
addiu	$5,$2,1
lbu	$2,0($4)
sw	$5,8($21)
lw	$4,10180($16)
sll	$2,$2,$3
srl	$2,$2,4
andi	$2,$2,0x8
addu	$2,$4,$2
sw	$2,10180($16)
.set	noreorder
.set	nomacro
b	$L1023
lw	$3,8($21)
.set	macro
.set	reorder

$L1243:
lw	$2,10212($16)
bgez	$2,$L1293
li	$2,-1			# 0xffffffffffffffff
$L1274:
lw	$31,148($sp)
lw	$fp,144($sp)
lw	$23,140($sp)
lw	$22,136($sp)
lw	$21,132($sp)
lw	$20,128($sp)
lw	$19,124($sp)
lw	$18,120($sp)
lw	$17,116($sp)
lw	$16,112($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,152
.set	macro
.set	reorder

$L1271:
lw	$6,%got($LC30)($28)
move	$4,$0
lw	$7,%got($LC40)($28)
li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC30)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC40)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$8,0($21)
.set	noreorder
.set	nomacro
b	$L1016
sw	$23,8($21)
.set	macro
.set	reorder

$L1262:
lw	$7,8($21)
lw	$8,0($21)
li	$4,-16777216			# 0xffffffffff000000
addiu	$5,$2,255
srl	$2,$7,3
ori	$4,$4,0xff00
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($2)  
lwr $fp, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$fp,8
sll	$2,$fp,8
and	$3,$3,$5
and	$2,$2,$4
or	$3,$3,$2
sll	$6,$3,16
srl	$3,$3,16
andi	$2,$7,0x7
or	$3,$6,$3
sll	$2,$3,$2
addiu	$7,$7,13
srl	$2,$2,19
sw	$7,8($21)
sw	$2,9952($16)
lw	$3,8($21)
addiu	$2,$3,1
addiu	$7,$3,14
srl	$3,$2,3
sw	$2,8($21)
andi	$2,$2,0x7
addu	$3,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($3)  
lwr $6, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$6,8
sw	$7,8($21)
sll	$6,$6,8
and	$3,$3,$5
and	$6,$6,$4
or	$3,$3,$6
sll	$6,$3,16
srl	$3,$3,16
or	$3,$6,$3
sll	$2,$3,$2
srl	$2,$2,19
sw	$2,9956($16)
lw	$3,8($21)
addiu	$2,$3,1
addiu	$7,$3,14
srl	$3,$2,3
sw	$2,8($21)
andi	$6,$2,0x7
addu	$2,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
move	$2,$3
sw	$7,8($21)
srl	$3,$3,8
sll	$2,$2,8
and	$3,$3,$5
and	$2,$2,$4
or	$3,$3,$2
sll	$2,$3,16
srl	$3,$3,16
or	$3,$2,$3
sll	$3,$3,$6
srl	$3,$3,19
sw	$3,9960($16)
lw	$3,8($21)
addiu	$2,$3,1
addiu	$7,$3,14
srl	$3,$2,3
sw	$2,8($21)
andi	$6,$2,0x7
addu	$2,$8,$3
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $3, 3($2)  
lwr $3, 0($2)  

# 0 "" 2
#NO_APP
srl	$2,$3,8
sw	$7,8($21)
sll	$3,$3,8
and	$2,$2,$5
and	$4,$3,$4
or	$2,$2,$4
sll	$3,$2,16
srl	$2,$2,16
or	$2,$3,$2
sll	$2,$2,$6
srl	$2,$2,19
sw	$2,9964($16)
lw	$4,8($21)
addiu	$4,$4,1
.set	noreorder
.set	nomacro
b	$L997
sw	$4,8($21)
.set	macro
.set	reorder

$L1267:
lw	$2,9948($16)
bne	$2,$8,$L1048
.set	noreorder
.set	nomacro
b	$L1286
lw	$19,8($21)
.set	macro
.set	reorder

$L1040:
lw	$2,%got(mpeg4_decode_mb_p0)($28)
$L1289:
sw	$0,10084($16)
.set	noreorder
.set	nomacro
b	$L1119
sw	$2,10528($16)
.set	macro
.set	reorder

$L1037:
move	$2,$0
.set	noreorder
.set	nomacro
b	$L1038
sw	$3,10096($16)
.set	macro
.set	reorder

$L1076:
lw	$2,2696($16)
li	$5,-2147483648			# 0xffffffff80000000
sw	$4,56($2)
.set	noreorder
.set	nomacro
b	$L1077
sw	$5,60($2)
.set	macro
.set	reorder

$L1053:
lw	$5,9932($16)
slt	$6,$3,$5
.set	noreorder
.set	nomacro
bne	$6,$0,$L1137
lw	$4,9928($16)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$5,$3,$L1054
sltu	$5,$2,$4
.set	macro
.set	reorder

beq	$5,$0,$L1054
$L1137:
lw	$5,36($18)
addiu	$17,$17,1
sra	$6,$5,31
sw	$17,9916($16)
addu	$5,$2,$5
addu	$3,$3,$6
sltu	$2,$5,$2
addu	$3,$2,$3
sw	$5,9920($16)
move	$2,$5
.set	noreorder
.set	nomacro
b	$L1054
sw	$3,9924($16)
.set	macro
.set	reorder

$L1270:
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC52)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1273
li	$2,100			# 0x64
.set	macro
.set	reorder

$L1263:
lw	$6,%got($LC37)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC37)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L1274
sw	$0,9972($16)
.set	macro
.set	reorder

$L1272:
lw	$6,%got($LC30)($28)
move	$4,$0
lw	$7,%got($LC41)($28)
li	$5,32			# 0x20
lw	$25,%call16(av_log)($28)
addiu	$6,$6,%lo($LC30)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$7,$7,%lo($LC41)
.set	macro
.set	reorder

lw	$28,96($sp)
lw	$8,0($21)
.set	noreorder
.set	nomacro
b	$L1016
sw	$23,8($21)
.set	macro
.set	reorder

$L1251:
lw	$6,%got($LC32)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC32)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1274
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1052:
lw	$2,0($16)
lw	$6,9912($16)
lhu	$5,9936($16)
lhu	$4,9928($16)
lw	$2,36($2)
addu	$17,$17,$6
subu	$4,$5,$4
mul	$6,$17,$2
addu	$2,$6,$3
addu	$3,$4,$2
sw	$2,9920($16)
sra	$2,$2,31
andi	$3,$3,0xffff
sltu	$4,$3,$5
sw	$2,9924($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L1236
sh	$3,9938($16)
.set	macro
.set	reorder

subu	$3,$5,$3
slt	$3,$3,$5
.set	noreorder
.set	nomacro
beq	$3,$0,$L1273
li	$2,100			# 0x64
.set	macro
.set	reorder

lw	$25,%call16(ff_mpeg4_init_direct_mv)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_init_direct_mv
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$20,10168($16)
.set	noreorder
.set	nomacro
beq	$20,$0,$L1059
lw	$28,96($sp)
.set	macro
.set	reorder

sra	$23,$20,31
sra	$19,$20,1
move	$fp,$23
$L1060:
lw	$17,9932($16)
.set	noreorder
.set	nomacro
blez	$17,$L1275
lw	$8,9928($16)
.set	macro
.set	reorder

$L1138:
addu	$4,$8,$19
addu	$5,$17,$23
sltu	$2,$4,$8
addu	$5,$2,$5
$L1064:
lw	$25,%call16(__divdi3)($28)
move	$6,$20
sw	$8,104($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
move	$7,$fp
.set	macro
.set	reorder

lhu	$3,9936($16)
move	$18,$2
lw	$8,104($sp)
subu	$22,$8,$3
sltu	$3,$8,$22
subu	$17,$17,$3
.set	noreorder
.set	nomacro
blez	$17,$L1276
lw	$28,96($sp)
.set	macro
.set	reorder

$L1139:
addu	$4,$22,$19
addu	$5,$17,$23
sltu	$3,$4,$22
addu	$5,$3,$5
$L1067:
lw	$25,%call16(__divdi3)($28)
move	$6,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
move	$7,$fp
.set	macro
.set	reorder

subu	$2,$18,$2
lw	$3,9924($16)
lw	$28,96($sp)
sll	$18,$2,1
lw	$4,9920($16)
andi	$18,$18,0xffff
.set	noreorder
.set	nomacro
blez	$3,$L1277
sh	$18,9940($16)
.set	macro
.set	reorder

addu	$2,$4,$19
$L1298:
addu	$3,$3,$23
sltu	$5,$2,$4
addu	$5,$5,$3
move	$4,$2
$L1070:
lw	$25,%call16(__divdi3)($28)
move	$6,$20
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
move	$7,$fp
.set	macro
.set	reorder

lw	$28,96($sp)
.set	noreorder
.set	nomacro
blez	$17,$L1278
move	$8,$2
.set	macro
.set	reorder

$L1141:
addu	$4,$22,$19
addu	$17,$17,$23
sltu	$3,$4,$22
addu	$5,$3,$17
$L1073:
lw	$25,%call16(__divdi3)($28)
move	$6,$20
sw	$8,104($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,__divdi3
1:	jalr	$25
move	$7,$fp
.set	macro
.set	reorder

lw	$8,104($sp)
lw	$3,10364($16)
lw	$28,96($sp)
subu	$2,$8,$2
sll	$2,$2,1
andi	$2,$2,0xffff
.set	noreorder
.set	nomacro
beq	$3,$0,$L1074
sh	$2,9942($16)
.set	macro
.set	reorder

$L1075:
.set	noreorder
.set	nomacro
b	$L1056
lw	$18,0($16)
.set	macro
.set	reorder

$L1266:
lw	$2,10096($16)
beq	$2,$0,$L1039
lw	$2,10104($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1039
li	$3,524288			# 0x80000
.set	macro
.set	reorder

lw	$2,64($16)
and	$2,$2,$3
.set	noreorder
.set	nomacro
bne	$2,$0,$L1039
lw	$6,%got($LC46)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC46)
.set	macro
.set	reorder

lw	$28,96($sp)
.set	noreorder
.set	nomacro
b	$L1039
sw	$0,10096($16)
.set	macro
.set	reorder

$L1080:
lw	$6,9944($16)
li	$3,2			# 0x2
beq	$6,$3,$L1081
lw	$7,2904($16)
.set	noreorder
.set	nomacro
beq	$7,$3,$L1082
li	$8,4			# 0x4
.set	macro
.set	reorder

beq	$7,$8,$L1279
$L1081:
sw	$0,7984($16)
$L1083:
.set	noreorder
.set	nomacro
beq	$6,$0,$L1090
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,9948($16)
bne	$2,$3,$L1087
lw	$3,2904($16)
beq	$3,$2,$L1280
$L1087:
lw	$3,8($21)
addiu	$3,$3,55
sw	$3,8($21)
$L1088:
addiu	$2,$3,1
addiu	$6,$3,2
srl	$7,$2,3
sw	$2,8($21)
andi	$5,$2,0x7
addu	$2,$4,$7
lbu	$2,0($2)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
beq	$2,$0,$L1089
sw	$6,8($21)
.set	macro
.set	reorder

addiu	$3,$3,10
sw	$3,8($21)
$L1089:
lw	$3,9944($16)
li	$2,2			# 0x2
bne	$3,$2,$L1090
lw	$2,10412($16)
$L1093:
.set	noreorder
.set	nomacro
beq	$2,$0,$L1094
lw	$25,%call16(ff_init_scantable)($28)
.set	macro
.set	reorder

addiu	$17,$16,5664
lw	$6,%got(ff_alternate_vertical_scan)($28)
addiu	$5,$16,9124
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

addiu	$5,$16,8728
lw	$28,96($sp)
lw	$6,%got(ff_alternate_vertical_scan)($28)
lw	$25,%call16(ff_init_scantable)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

addiu	$5,$16,8860
lw	$28,96($sp)
lw	$6,%got(ff_alternate_vertical_scan)($28)
$L1234:
lw	$25,%call16(ff_init_scantable)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

addiu	$5,$16,8992
lw	$28,96($sp)
lw	$25,%call16(ff_init_scantable)($28)
lw	$6,%got(ff_alternate_vertical_scan)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

li	$2,4			# 0x4
lw	$3,2904($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L1281
lw	$28,96($sp)
.set	macro
.set	reorder

$L1097:
lw	$3,9944($16)
li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$3,$2,$L1101
li	$2,16711680			# 0xff0000
.set	macro
.set	reorder

lw	$5,8($21)
lw	$8,0($21)
li	$3,-16777216			# 0xffffffffff000000
addiu	$7,$2,255
lw	$9,10040($16)
srl	$2,$5,3
ori	$6,$3,0xff00
addu	$2,$8,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $fp, 3($2)  
lwr $fp, 0($2)  

# 0 "" 2
#NO_APP
srl	$3,$fp,8
sll	$4,$fp,8
and	$3,$3,$7
and	$4,$4,$6
or	$4,$3,$4
sll	$3,$4,16
srl	$4,$4,16
andi	$10,$5,0x7
or	$2,$3,$4
sll	$2,$2,$10
subu	$3,$0,$9
addu	$5,$5,$9
srl	$2,$2,$3
sw	$5,8($21)
sw	$2,2872($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1282
sw	$2,2876($16)
.set	macro
.set	reorder

lw	$2,2904($16)
li	$3,1			# 0x1
beq	$2,$3,$L1103
lw	$2,8($21)
srl	$4,$2,3
andi	$9,$2,0x7
addu	$4,$8,$4
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$7
and	$5,$5,$6
or	$5,$4,$5
sll	$4,$5,16
srl	$5,$5,16
addiu	$2,$2,3
or	$3,$4,$5
sll	$3,$3,$9
sw	$2,8($21)
srl	$3,$3,29
.set	noreorder
.set	nomacro
beq	$3,$0,$L1104
sw	$3,7080($16)
.set	macro
.set	reorder

lw	$3,2904($16)
li	$2,3			# 0x3
beq	$3,$2,$L1283
$L1106:
li	$2,1			# 0x1
sw	$2,7084($16)
$L1107:
lw	$4,0($16)
lw	$2,404($4)
andi	$2,$2,0x1
.set	noreorder
.set	nomacro
beq	$2,$0,$L1108
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,2904($16)
lw	$7,2872($16)
lw	$6,7080($16)
.set	noreorder
.set	nomacro
beq	$2,$3,$L1132
lw	$24,7084($16)
.set	macro
.set	reorder

li	$3,2			# 0x2
.set	noreorder
.set	nomacro
beq	$2,$3,$L1133
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L1134
lw	$2,%got($LC20)($28)
.set	macro
.set	reorder

lw	$2,%got($LC18)($28)
addiu	$2,$2,%lo($LC18)
$L1109:
lw	$3,10044($16)
lw	$23,12($21)
lw	$22,10364($16)
lw	$20,10412($16)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1135
lw	$19,10396($16)
.set	macro
.set	reorder

lw	$8,%got($LC22)($28)
addiu	$8,$8,%lo($LC22)
$L1110:
lw	$5,7984($16)
li	$9,1			# 0x1
lw	$3,10104($16)
lw	$18,10080($16)
lw	$17,10092($16)
subu	$9,$9,$5
lw	$15,9972($16)
lw	$14,10072($16)
.set	noreorder
.set	nomacro
bne	$3,$0,$L1136
lw	$13,10100($16)
.set	macro
.set	reorder

lw	$3,%got($LC24)($28)
addiu	$3,$3,%lo($LC24)
$L1111:
lw	$12,10108($16)
li	$5,48			# 0x30
lw	$11,10176($16)
lw	$fp,10180($16)
lw	$10,10184($16)
lw	$25,%call16(av_log)($28)
sw	$6,16($sp)
sw	$24,20($sp)
sw	$2,24($sp)
sw	$23,28($sp)
sw	$22,32($sp)
sw	$20,36($sp)
sw	$19,40($sp)
sw	$8,44($sp)
sw	$18,48($sp)
sw	$17,52($sp)
sw	$15,56($sp)
sw	$14,60($sp)
sw	$9,64($sp)
sw	$13,68($sp)
sw	$3,72($sp)
sw	$12,76($sp)
sw	$11,80($sp)
sw	$fp,84($sp)
sw	$10,88($sp)
lw	$6,%got($LC57)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC57)
.set	macro
.set	reorder

lw	$28,96($sp)
$L1108:
lw	$2,10048($16)
bne	$2,$0,$L1112
lw	$2,9944($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1101
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$3,2904($16)
beq	$3,$2,$L1101
lw	$2,8($21)
addiu	$2,$2,1
sw	$2,8($21)
$L1101:
lw	$2,10100($16)
bne	$2,$0,$L1118
lw	$2,10104($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1118
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lw	$3,10188($16)
beq	$3,$2,$L1284
$L1118:
lw	$2,136($16)
$L1117:
addiu	$3,$2,1
$L1296:
lw	$2,88($16)
sw	$3,136($16)
andi	$2,$2,0x400
lw	$3,%got(ff_mpeg4_y_dc_scale_table)($28)
sw	$3,2788($16)
lw	$3,%got(ff_mpeg4_c_dc_scale_table)($28)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1168
sw	$3,2792($16)
.set	macro
.set	reorder

lw	$4,8($16)
move	$2,$0
lw	$3,12($16)
sw	$4,180($16)
.set	noreorder
.set	nomacro
b	$L1168
sw	$3,184($16)
.set	macro
.set	reorder

$L1279:
lw	$7,9948($16)
bne	$7,$3,$L1081
$L1082:
srl	$3,$5,3
addiu	$2,$2,2
addu	$3,$4,$3
andi	$5,$5,0x7
lbu	$3,0($3)
sw	$2,8($21)
lw	$6,9944($16)
sll	$2,$3,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
b	$L1083
sw	$2,7984($16)
.set	macro
.set	reorder

$L1269:
lw	$2,2696($16)
lw	$6,%got($LC50)($28)
move	$4,$18
lw	$25,%call16(av_log)($28)
lw	$3,60($2)
addiu	$6,$6,%lo($LC50)
lw	$2,56($2)
sw	$3,20($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1078
lw	$28,96($sp)
.set	macro
.set	reorder

$L1090:
lw	$3,10176($16)
lw	$2,8($21)
addu	$2,$3,$2
li	$3,1			# 0x1
sw	$2,8($21)
lw	$5,2904($16)
.set	noreorder
.set	nomacro
beq	$5,$3,$L1294
srl	$3,$2,3
.set	macro
.set	reorder

lw	$3,10180($16)
addu	$2,$2,$3
li	$3,3			# 0x3
sw	$2,8($21)
lw	$5,2904($16)
.set	noreorder
.set	nomacro
bne	$5,$3,$L1294
srl	$3,$2,3
.set	macro
.set	reorder

lw	$3,10184($16)
addu	$2,$2,$3
sw	$2,8($21)
srl	$3,$2,3
$L1294:
li	$6,16711680			# 0xff0000
addu	$3,$4,$3
addiu	$6,$6,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$5,$5,$6
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
andi	$6,$2,0x7
or	$3,$5,$3
lw	$5,%got(mpeg4_dc_threshold)($28)
sll	$3,$3,$6
addiu	$2,$2,3
srl	$3,$3,29
addu	$3,$5,$3
sw	$2,8($21)
lw	$2,10364($16)
lbu	$3,0($3)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1092
sw	$3,10108($16)
.set	macro
.set	reorder

lw	$3,8($21)
srl	$5,$3,3
andi	$6,$3,0x7
addu	$5,$4,$5
addiu	$3,$3,1
lbu	$2,0($5)
sw	$3,8($21)
sll	$2,$2,$6
andi	$2,$2,0x00ff
srl	$2,$2,7
sw	$2,10396($16)
lw	$3,8($21)
srl	$2,$3,3
andi	$5,$3,0x7
addu	$2,$4,$2
addiu	$3,$3,1
lbu	$2,0($2)
sw	$3,8($21)
sll	$2,$2,$5
andi	$2,$2,0x00ff
srl	$2,$2,7
.set	noreorder
.set	nomacro
b	$L1093
sw	$2,10412($16)
.set	macro
.set	reorder

$L1092:
sw	$0,10412($16)
$L1094:
lw	$25,%call16(ff_init_scantable)($28)
addiu	$17,$16,5664
lw	$6,%got(ff_zigzag_direct)($28)
addiu	$5,$16,9124
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

addiu	$5,$16,8728
lw	$28,96($sp)
lw	$6,%got(ff_zigzag_direct)($28)
lw	$25,%call16(ff_init_scantable)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_init_scantable
1:	jalr	$25
move	$4,$17
.set	macro
.set	reorder

addiu	$5,$16,8860
lw	$28,96($sp)
.set	noreorder
.set	nomacro
b	$L1234
lw	$6,%got(ff_alternate_horizontal_scan)($28)
.set	macro
.set	reorder

$L1130:
.set	noreorder
.set	nomacro
b	$L1042
move	$17,$0
.set	macro
.set	reorder

$L1281:
lw	$2,9948($16)
addiu	$2,$2,-1
sltu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L1097
lw	$25,%got(mpeg4_decode_sprite_trajectory)($28)
.set	macro
.set	reorder

move	$4,$16
addiu	$25,$25,%lo(mpeg4_decode_sprite_trajectory)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_sprite_trajectory
1:	jalr	$25
move	$5,$21
.set	macro
.set	reorder

lw	$2,9968($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L1099
lw	$28,96($sp)
.set	macro
.set	reorder

lw	$6,%got($LC53)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC53)
.set	macro
.set	reorder

lw	$28,96($sp)
$L1099:
lw	$3,9948($16)
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
bne	$3,$2,$L1097
lw	$6,%got($LC54)($28)
.set	macro
.set	reorder

li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC54)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1097
lw	$28,96($sp)
.set	macro
.set	reorder

$L1112:
lw	$2,10056($16)
beq	$2,$0,$L1235
lw	$2,8($21)
lw	$4,0($21)
srl	$3,$2,3
andi	$5,$2,0x7
addu	$4,$4,$3
addiu	$2,$2,1
lbu	$3,0($4)
sll	$3,$3,$5
andi	$3,$3,0x00ff
srl	$3,$3,7
.set	noreorder
.set	nomacro
bne	$3,$0,$L1285
sw	$2,8($21)
.set	macro
.set	reorder

addiu	$2,$2,2
.set	noreorder
.set	nomacro
b	$L1101
sw	$2,8($21)
.set	macro
.set	reorder

$L1285:
lw	$6,%got($LC58)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC58)
.set	macro
.set	reorder

lw	$28,96($sp)
$L1235:
lw	$2,8($21)
addiu	$2,$2,2
.set	noreorder
.set	nomacro
b	$L1101
sw	$2,8($21)
.set	macro
.set	reorder

$L1103:
.set	noreorder
.set	nomacro
b	$L1106
sw	$2,7080($16)
.set	macro
.set	reorder

$L1136:
lw	$3,%got($LC23)($28)
.set	noreorder
.set	nomacro
b	$L1111
addiu	$3,$3,%lo($LC23)
.set	macro
.set	reorder

$L1135:
lw	$8,%got($LC21)($28)
.set	noreorder
.set	nomacro
b	$L1110
addiu	$8,$8,%lo($LC21)
.set	macro
.set	reorder

$L1134:
.set	noreorder
.set	nomacro
b	$L1109
addiu	$2,$2,%lo($LC20)
.set	macro
.set	reorder

$L1133:
lw	$2,%got($LC19)($28)
.set	noreorder
.set	nomacro
b	$L1109
addiu	$2,$2,%lo($LC19)
.set	macro
.set	reorder

$L1132:
lw	$2,%got($LC17)($28)
.set	noreorder
.set	nomacro
b	$L1109
addiu	$2,$2,%lo($LC17)
.set	macro
.set	reorder

$L1104:
lw	$6,%got($LC56)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC56)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1168
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1283:
lw	$4,8($21)
lw	$3,0($21)
srl	$2,$4,3
andi	$5,$4,0x7
addu	$3,$3,$2
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $2, 3($3)  
lwr $2, 0($3)  

# 0 "" 2
#NO_APP
move	$3,$2
sll	$3,$3,8
srl	$2,$2,8
and	$3,$3,$6
and	$2,$2,$7
or	$2,$2,$3
sll	$3,$2,16
srl	$2,$2,16
addiu	$4,$4,3
or	$2,$3,$2
sll	$2,$2,$5
sw	$4,8($21)
srl	$2,$2,29
.set	noreorder
.set	nomacro
b	$L1107
sw	$2,7084($16)
.set	macro
.set	reorder

$L1282:
lw	$6,%got($LC55)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC55)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1168
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L1074:
sltu	$18,$2,$18
.set	noreorder
.set	nomacro
beq	$18,$0,$L1236
sltu	$2,$2,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$0,$L1075
li	$2,100			# 0x64
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L1301
lw	$31,148($sp)
.set	macro
.set	reorder

$L1278:
.set	noreorder
.set	nomacro
bne	$17,$0,$L1295
subu	$4,$22,$19
.set	macro
.set	reorder

bne	$22,$0,$L1141
$L1295:
subu	$17,$17,$23
sltu	$3,$22,$4
.set	noreorder
.set	nomacro
b	$L1073
subu	$5,$17,$3
.set	macro
.set	reorder

$L1284:
lw	$2,136($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L1296
addiu	$3,$2,1
.set	macro
.set	reorder

lw	$6,%got($LC59)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC59)
.set	macro
.set	reorder

li	$3,1			# 0x1
lw	$28,96($sp)
lw	$2,136($16)
.set	noreorder
.set	nomacro
b	$L1117
sw	$3,10096($16)
.set	macro
.set	reorder

$L1280:
.set	noreorder
.set	nomacro
b	$L1088
lw	$3,8($21)
.set	macro
.set	reorder

$L1277:
.set	noreorder
.set	nomacro
bne	$3,$0,$L1297
subu	$2,$4,$19
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$4,$0,$L1298
addu	$2,$4,$19
.set	macro
.set	reorder

subu	$2,$4,$19
$L1297:
subu	$3,$3,$23
sltu	$4,$4,$2
subu	$5,$3,$4
.set	noreorder
.set	nomacro
b	$L1070
move	$4,$2
.set	macro
.set	reorder

$L1276:
.set	noreorder
.set	nomacro
bne	$17,$0,$L1299
subu	$4,$22,$19
.set	macro
.set	reorder

bne	$22,$0,$L1139
$L1299:
subu	$3,$17,$23
sltu	$5,$22,$4
.set	noreorder
.set	nomacro
b	$L1067
subu	$5,$3,$5
.set	macro
.set	reorder

$L1275:
.set	noreorder
.set	nomacro
bne	$17,$0,$L1300
subu	$4,$8,$19
.set	macro
.set	reorder

bne	$8,$0,$L1138
$L1300:
subu	$2,$17,$23
sltu	$5,$8,$4
.set	noreorder
.set	nomacro
b	$L1064
subu	$5,$2,$5
.set	macro
.set	reorder

$L1059:
lhu	$20,9938($16)
.set	noreorder
.set	nomacro
beq	$20,$0,$L1061
sw	$20,10168($16)
.set	macro
.set	reorder

sra	$19,$20,1
sra	$23,$20,31
.set	noreorder
.set	nomacro
b	$L1060
move	$fp,$0
.set	macro
.set	reorder

$L1061:
li	$2,1			# 0x1
li	$20,1			# 0x1
move	$fp,$0
move	$19,$0
sw	$2,10168($16)
.set	noreorder
.set	nomacro
b	$L1060
move	$23,$0
.set	macro
.set	reorder

.end	JZC_mpeg4_decode_picture_header
.size	JZC_mpeg4_decode_picture_header, .-JZC_mpeg4_decode_picture_header
.local	table.6956
.comm	table.6956,64,4
.local	table.6955
.comm	table.6955,512,4
.local	table.6954
.comm	table.6954,2048,4
.local	table.6953
.comm	table.6953,2048,4
.local	rl_vlc_table.6948
.comm	rl_vlc_table.6948,137216,4
.local	table.6949
.comm	table.6949,4288,4
.local	rl_vlc_table.6942
.comm	rl_vlc_table.6942,137216,4
.local	table.6943
.comm	table.6943,4288,4
.local	rl_vlc_table.6936
.comm	rl_vlc_table.6936,70912,4
.local	table.6937
.comm	table.6937,2216,4
.local	rl_vlc_table.6930
.comm	rl_vlc_table.6930,70912,4
.local	table.6931
.comm	table.6931,2216,4
.local	done.6928
.comm	done.6928,4,4
.rdata
.align	2
.type	quant_tab.6803, @object
.size	quant_tab.6803, 4
quant_tab.6803:
.byte	-1
.byte	-2
.byte	1
.byte	2
.align	2
.type	off.6462, @object
.size	off.6462, 16
off.6462:
.word	2
.word	1
.word	1
.word	-1
.local	table.6439
.comm	table.6439,32,4
.local	table.6438
.comm	table.6438,320,4
.local	rl_vlc_table.6433
.comm	rl_vlc_table.6433,70912,4
.local	table.6434
.comm	table.6434,2216,4
.local	rl_vlc_table.6427
.comm	rl_vlc_table.6427,70912,4
.local	table.6428
.comm	table.6428,2216,4
.local	rl_vlc_table.6421
.comm	rl_vlc_table.6421,70912,4
.local	table.6422
.comm	table.6422,2216,4
.local	table.6419
.comm	table.6419,2152,4
.local	table.6418
.comm	table.6418,256,4
.local	table.6417
.comm	table.6417,792,4
.local	table.6416
.comm	table.6416,288,4
.local	done.6415
.comm	done.6415,4,4
.globl	mpeg4_decoder
.section	.rodata.str1.4
.align	2
$LC60:
.ascii	"mpeg4\000"
.align	2
$LC61:
.ascii	"MPEG-4 part 2\000"
.section	.data.rel,"aw",@progbits
.align	2
.type	mpeg4_decoder, @object
.size	mpeg4_decoder, 72
mpeg4_decoder:
.word	$LC60
.word	0
.word	13
.word	10584
.word	decode_init
.word	0
.word	mpeg4_decode_end
.word	mpeg4_decode_frame
.word	43
.space	4
.word	ff_mpeg_flush
.space	4
.word	ff_hwaccel_pixfmt_list_420
.word	$LC61
.space	12
.byte	3
.space	3
.local	cbpc_b_vlc
.comm	cbpc_b_vlc,16,4
.local	h263_mbtype_b_vlc
.comm	h263_mbtype_b_vlc,16,4
.local	mv_vlc_hw
.comm	mv_vlc_hw,16,4

.comm	ff_h263_cbpy_vlc_hw,16,4

.comm	ff_h263_inter_MCBPC_vlc_hw,16,4

.comm	ff_h263_intra_MCBPC_vlc_hw,16,4
.rdata
.align	2
.type	mb_type_b_map, @object
.size	mb_type_b_map, 16
mb_type_b_map:
.word	61696
.word	61448
.word	49160
.word	12296
.globl	JZC_mpeg4_intra_run
.align	2
.type	JZC_mpeg4_intra_run, @object
.size	JZC_mpeg4_intra_run, 412
JZC_mpeg4_intra_run:
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	0
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	1
.word	2
.word	2
.word	2
.word	2
.word	2
.word	3
.word	3
.word	3
.word	3
.word	4
.word	4
.word	4
.word	5
.word	5
.word	5
.word	6
.word	6
.word	6
.word	7
.word	7
.word	7
.word	8
.word	8
.word	9
.word	9
.word	10
.word	11
.word	12
.word	13
.word	14
.word	192
.word	192
.word	192
.word	192
.word	192
.word	192
.word	192
.word	192
.word	193
.word	193
.word	193
.word	194
.word	194
.word	195
.word	195
.word	196
.word	196
.word	197
.word	197
.word	198
.word	198
.word	199
.word	200
.word	201
.word	202
.word	203
.word	204
.word	205
.word	206
.word	207
.word	208
.word	209
.word	210
.word	211
.word	212
.word	65
.globl	JZC_mpeg4_intra_level
.align	2
.type	JZC_mpeg4_intra_level, @object
.size	JZC_mpeg4_intra_level, 103
JZC_mpeg4_intra_level:
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	11
.byte	12
.byte	13
.byte	14
.byte	15
.byte	16
.byte	17
.byte	18
.byte	19
.byte	20
.byte	21
.byte	22
.byte	23
.byte	24
.byte	25
.byte	26
.byte	27
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	9
.byte	10
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	1
.byte	2
.byte	3
.byte	4
.byte	1
.byte	2
.byte	3
.byte	1
.byte	2
.byte	3
.byte	1
.byte	2
.byte	3
.byte	1
.byte	2
.byte	3
.byte	1
.byte	2
.byte	1
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	2
.byte	3
.byte	4
.byte	5
.byte	6
.byte	7
.byte	8
.byte	1
.byte	2
.byte	3
.byte	1
.byte	2
.byte	1
.byte	2
.byte	1
.byte	2
.byte	1
.byte	2
.byte	1
.byte	2
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.local	mb_type_b_vlc
.comm	mb_type_b_vlc,16,4
.local	sprite_trajectory
.comm	sprite_trajectory,16,4
.local	dc_chrom
.comm	dc_chrom,16,4
.local	dc_lum
.comm	dc_lum,16,4

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
