.file	1 "mpeg4.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.abicalls
.section	.text.get_consumed_bytes,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_consumed_bytes
.type	get_consumed_bytes, @function
get_consumed_bytes:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lw	$3,10196($4)
bne	$3,$0,$L6
lw	$2,10340($4)

lw	$3,0($4)
lw	$3,872($3)
beq	$3,$0,$L10
li	$6,65536			# 0x10000

$L6:
j	$31
move	$2,$5

$L10:
lw	$3,64($4)
addiu	$2,$2,7
and	$3,$3,$6
beq	$3,$0,$L3
sra	$2,$2,3

lw	$3,9832($4)
subu	$2,$2,$3
slt	$3,$2,0
j	$31
movn	$2,$0,$3

$L3:
beq	$2,$0,$L7
li	$3,10			# 0xa

addiu	$3,$2,9
$L4:
slt	$3,$3,$5
j	$31
movz	$2,$5,$3

$L7:
b	$L4
li	$2,1			# 0x1

.set	macro
.set	reorder
.end	get_consumed_bytes
.size	get_consumed_bytes, .-get_consumed_bytes
.section	.text.get_MBARGs,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	get_MBARGs
.type	get_MBARGs, @function
get_MBARGs:
.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
lw	$2,%got(dMB)($28)
move	$3,$4
addiu	$5,$4,8680
lw	$4,10448($4)
addiu	$sp,$sp,-32
lw	$25,%call16(memcpy)($28)
li	$6,24			# 0x18
lw	$2,0($2)
.cprestore	16
sw	$31,28($sp)
sb	$4,1($2)
addiu	$4,$2,80
lw	$7,8004($3)
sb	$7,2($2)
lw	$7,7260($3)
sb	$7,3($2)
lw	$7,7264($3)
sb	$7,4($2)
lw	$7,7992($3)
sb	$7,76($2)
lw	$7,7996($3)
sb	$7,77($2)
lw	$7,2872($3)
sb	$7,78($2)
lw	$7,2876($3)
sb	$7,79($2)
lw	$7,2780($3)
sb	$7,5($2)
lw	$7,2784($3)
sb	$7,104($2)
lw	$7,2824($3)
sb	$7,105($2)
lw	$3,0($3)
lw	$3,704($3)
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sb	$3,106($2)

move	$2,$0
lw	$31,28($sp)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	get_MBARGs
.size	get_MBARGs, .-get_MBARGs
.section	.rodata.str1.4,"aMS",@progbits,1
.align	2
$LC0:
.ascii	"Slice mismatch at MB: %d\012\000"
.align	2
$LC1:
.ascii	"Error at MB: %d\012\000"
.align	2
$LC2:
.ascii	"discarding %d junk bits at end, next would be %X\012\000"
.align	2
$LC3:
.ascii	"overreading %d bits\012\000"
.align	2
$LC4:
.ascii	"slice end not reached but screenspace end (%d left %06X,"
.ascii	" score= %d)\012\000"
.section	.text.decode_slice,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	decode_slice
.type	decode_slice, @function
decode_slice:
.frame	$sp,96,$31		# vars= 24, regs= 10/0, args= 24, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
lw	$5,%got(tcsm1_base)($28)
addiu	$sp,$sp,-96
lw	$7,0($4)
li	$2,-201326592			# 0xfffffffff4000000
sw	$fp,88($sp)
move	$fp,$4
.cprestore	24
addiu	$8,$2,4
sw	$22,80($sp)
addiu	$3,$fp,9192
sw	$19,68($sp)
li	$11,16			# 0x10
sw	$18,64($sp)
addiu	$4,$2,4096
sw	$17,60($sp)
addiu	$25,$fp,8076
sw	$16,56($sp)
sw	$31,92($sp)
sw	$23,84($sp)
sw	$21,76($sp)
sw	$20,72($sp)
lw	$5,0($5)
lw	$10,656($7)
move	$7,$0
lw	$6,10084($fp)
addiu	$9,$5,16384
lw	$22,%got(tcsm1_fifo_wp)($28)
lw	$17,%got(task_fifo_wp)($28)
sra	$10,$11,$10
lw	$18,%got(task_fifo_wp_d1)($28)
sw	$0,16384($5)
sw	$3,48($sp)
addiu	$3,$fp,8332
lw	$5,%got(tcsm0_fifo_rp)($28)
lw	$16,%got(dc_chrom_table)($28)
sw	$4,0($17)
sw	$8,0($5)
sw	$4,0($18)
addiu	$16,$16,%lo(dc_chrom_table)
sw	$25,36($sp)
sw	$3,40($sp)
sw	$10,32($sp)
sw	$9,0($22)
lw	$8,%got(task_fifo_wp_d2)($28)
lw	$19,%got(dFRM)($28)
sw	$4,0($8)
li	$4,18			# 0x12
sw	$0,4($2)
li	$2,127			# 0x7f
lw	$5,10304($fp)
movn	$2,$4,$6
sw	$2,44($sp)
lw	$6,10344($fp)
$L250:
addiu	$2,$5,-1
lw	$10,10332($fp)
lw	$9,10336($fp)
sltu	$2,$2,2
lw	$8,10340($fp)
lw	$5,7992($fp)
lw	$4,7996($fp)
sw	$6,9804($fp)
li	$6,1			# 0x1
sw	$10,9792($fp)
sw	$9,9796($fp)
sw	$8,9800($fp)
sw	$6,10296($fp)
sw	$5,9784($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L213
sw	$4,9788($fp)
.set	macro
.set	reorder

lw	$2,%got(mc_base)($28)
lw	$20,0($2)
li	$2,131072			# 0x20000
ori	$2,$2,0xf889
sw	$2,0($20)
lw	$4,288($fp)
$L237:
lw	$25,%call16(get_phy_addr)($28)
sw	$0,768($20)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$7,52($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$4,888($fp)
sw	$2,772($20)
lw	$3,%got(mc_base)($28)
lw	$25,%call16(get_phy_addr)($28)
lw	$23,0($3)
sw	$0,896($23)
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$28,24($sp)
lw	$4,%got(mc_base)($28)
lw	$25,%call16(get_phy_addr)($28)
lw	$21,0($4)
lw	$4,292($fp)
sw	$2,900($23)
sw	$0,2816($21)
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$28,24($sp)
lw	$4,892($fp)
sw	$2,2820($21)
lw	$5,%got(mc_base)($28)
lw	$25,%call16(get_phy_addr)($28)
lw	$20,0($5)
sw	$0,2944($20)
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
li	$8,100663296			# 0x6000000
lw	$28,24($sp)
li	$6,134217728			# 0x8000000
sw	$2,2948($20)
move	$4,$fp
lw	$25,%got(mc_base)($28)
lw	$5,0($25)
lw	$25,%call16(ff_set_qscale)($28)
sw	$8,32($5)
sw	$8,2080($5)
sw	$6,36($5)
sw	$0,44($5)
sw	$6,2084($5)
sw	$0,2088($5)
sw	$0,2092($5)
lw	$2,192($fp)
addiu	$6,$2,15
slt	$8,$2,0
movn	$2,$6,$8
sra	$2,$2,4
andi	$2,$2,0xfff
sll	$2,$2,16
ori	$2,$2,0x10
sw	$2,76($5)
lw	$2,164($fp)
lw	$6,160($fp)
sll	$2,$2,4
sll	$6,$6,4
andi	$2,$2,0xff0
sll	$2,$2,16
andi	$6,$6,0xff0
or	$2,$2,$6
sw	$2,80($5)
lw	$2,192($fp)
addiu	$6,$2,15
slt	$8,$2,0
movn	$2,$6,$8
sra	$2,$2,4
andi	$2,$2,0xfff
sll	$2,$2,16
ori	$2,$2,0x8
sw	$2,2124($5)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_set_qscale
1:	jalr	$25
lw	$5,2872($fp)
.set	macro
.set	reorder

addiu	$4,$19,172
lw	$24,2904($fp)
li	$6,64			# 0x40
lw	$15,164($fp)
lw	$23,10428($fp)
lw	$21,10432($fp)
lw	$20,160($fp)
lw	$14,7984($fp)
lw	$13,10044($fp)
lw	$12,52($fp)
lw	$28,24($sp)
lw	$11,192($fp)
lw	$10,196($fp)
lw	$9,10328($fp)
lw	$2,7988($fp)
lw	$8,9880($fp)
sb	$24,43($19)
sb	$15,11($19)
li	$15,1			# 0x1
sb	$23,8($19)
sb	$21,9($19)
sb	$20,10($19)
sb	$14,4($19)
sb	$13,5($19)
sb	$12,7($19)
sw	$15,0($19)
lw	$14,8($fp)
lw	$13,12($fp)
lw	$12,28($fp)
lw	$5,48($sp)
lw	$25,%call16(memcpy)($28)
sw	$14,12($19)
sw	$13,16($19)
sb	$12,6($19)
sw	$11,20($19)
sw	$10,24($19)
sb	$9,28($19)
sb	$8,29($19)
sb	$2,30($19)
sb	$0,42($19)
lw	$9,44($fp)
lw	$8,10412($fp)
lw	$2,10164($fp)
sb	$9,31($19)
sb	$8,40($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sb	$2,41($19)
.set	macro
.set	reorder

addiu	$4,$19,44
lw	$28,24($sp)
li	$6,128			# 0x80
lw	$15,88($fp)
lw	$13,292($fp)
lw	$12,296($fp)
lw	$11,300($fp)
lw	$14,288($fp)
lw	$10,888($fp)
lw	$9,892($fp)
lw	$8,896($fp)
lw	$2,900($fp)
lw	$25,%call16(memcpy)($28)
lw	$5,36($sp)
sw	$13,368($19)
sw	$12,372($19)
sw	$11,376($19)
sw	$10,380($19)
sw	$9,384($19)
sw	$15,32($19)
sw	$14,364($19)
sw	$8,388($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
sw	$2,392($19)
.set	macro
.set	reorder

li	$6,128			# 0x80
lw	$28,24($sp)
addiu	$4,$19,236
lw	$25,%call16(memcpy)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
lw	$5,40($sp)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
lw	$4,2096($fp)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$4,2100($fp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,404($19)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$4,2088($fp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,408($19)
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$4,2092($fp)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,396($19)
.set	macro
.set	reorder

li	$6,412			# 0x19c
lw	$28,24($sp)
sw	$2,400($19)
lw	$3,%got(t1_dFRM)($28)
lw	$25,%call16(memcpy)($28)
lw	$5,%got(dFRM)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
lw	$4,0($3)
.set	macro
.set	reorder

li	$10,32768			# 0x8000
lw	$28,24($sp)
li	$13,32832			# 0x8040
lw	$9,36($sp)
lw	$7,52($sp)
lw	$4,%got(vmau_base)($28)
lw	$12,0($4)
addu	$10,$12,$10
addu	$13,$12,$13
move	$11,$10
$L20:
lhu	$3,6($9)
addiu	$9,$9,8
lhu	$2,-6($9)
addiu	$11,$11,4
sll	$3,$3,24
sll	$2,$2,8
lbu	$5,-8($9)
lbu	$4,-4($9)
andi	$2,$2,0xffff
or	$3,$3,$5
sll	$4,$4,16
or	$2,$3,$2
or	$2,$2,$4
sw	$2,-4($11)
.set	noreorder
.set	nomacro
bne	$11,$13,$L20
li	$2,32896			# 0x8080
.set	macro
.set	reorder

lw	$9,40($sp)
addiu	$10,$10,64
addu	$12,$12,$2
$L21:
lhu	$3,6($9)
addiu	$9,$9,8
lhu	$2,-6($9)
addiu	$10,$10,4
sll	$3,$3,24
sll	$2,$2,8
lbu	$5,-8($9)
lbu	$4,-4($9)
andi	$2,$2,0xffff
or	$3,$3,$5
sll	$4,$4,16
or	$2,$3,$2
or	$2,$2,$4
sw	$2,-4($10)
bne	$10,$12,$L21
bne	$7,$0,$L134
lw	$5,%got(t1_dFRM)($28)
lw	$2,0($5)
addiu	$4,$2,416
$L23:
#APP
# 1122 "mpeg4.c" 1
cache 25,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$4,$L23
li	$2,-2147483648			# 0xffffffff80000000
addiu	$4,$2,16384
$L24:
#APP
# 1125 "mpeg4.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$4,$L24
#APP
# 1125 "mpeg4.c" 1
sync
# 0 "" 2
#NO_APP
lw	$2,%got(aux_base)($28)
li	$4,1			# 0x1
li	$5,-201326592			# 0xfffffffff4000000
lw	$9,10332($fp)
lw	$2,0($2)
sw	$4,0($2)
sw	$0,0($5)
sw	$4,0($2)
li	$4,2			# 0x2
sw	$4,0($2)
lw	$6,10344($fp)
lw	$2,%got(mpeg4_sw_bs_buffer)($28)
sra	$6,$6,3
lw	$8,0($2)
slt	$2,$6,-63
.set	noreorder
.set	nomacro
bne	$2,$0,$L30
move	$2,$0
.set	macro
.set	reorder

addiu	$6,$6,64
$L29:
addu	$4,$9,$2
addu	$3,$8,$2
addiu	$2,$2,1
lbu	$4,0($4)
.set	noreorder
.set	nomacro
bne	$2,$6,$L29
sb	$4,0($3)
.set	macro
.set	reorder

$L30:
lw	$4,%got(sde_base)($28)
move	$2,$0
li	$8,144			# 0x90
lw	$4,0($4)
$L26:
lw	$25,%got(c_table)($28)
addu	$3,$4,$2
addiu	$25,$25,%lo(c_table)
addu	$5,$25,$2
addiu	$2,$2,4
lw	$5,0($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L26
sw	$5,8192($3)
.set	macro
.set	reorder

move	$2,$0
li	$8,128			# 0x80
$L31:
lw	$3,%got(cbpy_table)($28)
addiu	$3,$3,%lo(cbpy_table)
addu	$5,$3,$2
addu	$3,$4,$2
addiu	$2,$2,4
lw	$5,0($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L31
sw	$5,8704($3)
.set	macro
.set	reorder

lw	$5,%got(dc_lum_table)($28)
move	$2,$0
li	$8,528			# 0x210
addiu	$9,$5,%lo(dc_lum_table)
$L32:
addu	$5,$9,$2
addu	$3,$4,$2
addiu	$2,$2,4
lw	$5,0($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L32
sw	$5,9216($3)
.set	macro
.set	reorder

move	$2,$0
li	$8,544			# 0x220
$L33:
addu	$5,$16,$2
addu	$3,$4,$2
addiu	$2,$2,4
lw	$5,0($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L33
sw	$5,9728($3)
.set	macro
.set	reorder

lw	$8,%got(rl_intra_table)($28)
move	$2,$0
addiu	$9,$8,%lo(rl_intra_table)
li	$8,800			# 0x320
$L34:
addu	$5,$9,$2
addu	$3,$4,$2
addiu	$2,$2,4
lw	$5,0($5)
.set	noreorder
.set	nomacro
bne	$2,$8,$L34
sw	$5,10240($3)
.set	macro
.set	reorder

li	$2,-2147483648			# 0xffffffff80000000
addiu	$4,$2,16384
$L35:
#APP
# 1182 "mpeg4.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$4,$L35
#APP
# 1182 "mpeg4.c" 1
sync
# 0 "" 2
#NO_APP
lw	$4,%got(mpeg4_hw_bs_buffer)($28)
lw	$2,10340($fp)
lw	$4,0($4)
srl	$5,$2,3
addu	$4,$4,$5
li	$5,-4			# 0xfffffffffffffffc
and	$4,$4,$5
#APP
# 1196 "mpeg4.c" 1
mtc0	$4,$21,2
# 0 "" 2
#NO_APP
andi	$2,$2,0x1f
li	$4,65536			# 0x10000
sll	$2,$2,5
or	$2,$2,$4
#APP
# 1199 "mpeg4.c" 1
mtc0	$2,$21,3
# 0 "" 2
#NO_APP
$L36:
#APP
# 1203 "mpeg4.c" 1
mfc0	$2,$21,3
# 0 "" 2
#NO_APP
and	$2,$2,$4
bne	$2,$0,$L36
$L22:
lw	$2,7996($fp)
lw	$4,164($fp)
slt	$4,$2,$4
beq	$4,$0,$L28
li	$21,-201326592			# 0xfffffffff4000000
li	$23,1			# 0x1
addiu	$21,$21,4096
move	$20,$7
$L56:
lw	$3,10304($fp)
beq	$3,$0,$L37
bne	$20,$0,$L38
lw	$6,9788($fp)
lw	$4,10292($fp)
addu	$4,$6,$4
.set	noreorder
.set	nomacro
beq	$2,$4,$L214
move	$4,$fp
.set	macro
.set	reorder

$L38:
.set	noreorder
.set	nomacro
bne	$3,$23,$L37
li	$2,128			# 0x80
.set	macro
.set	reorder

sw	$2,2720($fp)
sw	$2,2716($fp)
sw	$2,2712($fp)
$L37:
lw	$25,%call16(jz_init_block_index)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz_init_block_index
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$2,7992($fp)
lw	$3,160($fp)
slt	$3,$2,$3
.set	noreorder
.set	nomacro
beq	$3,$0,$L102
lw	$28,24($sp)
.set	macro
.set	reorder

$L158:
lw	$8,8012($fp)
lw	$7,8016($fp)
lw	$6,8020($fp)
lw	$5,8024($fp)
addiu	$8,$8,2
lw	$4,8028($fp)
addiu	$7,$7,2
lw	$3,8032($fp)
addiu	$6,$6,2
addiu	$5,$5,2
lw	$9,9784($fp)
addiu	$4,$4,1
sw	$8,8012($fp)
addiu	$3,$3,1
sw	$7,8016($fp)
sw	$6,8020($fp)
sw	$5,8024($fp)
sw	$4,8028($fp)
.set	noreorder
.set	nomacro
beq	$2,$9,$L215
sw	$3,8032($fp)
.set	macro
.set	reorder

$L57:
lw	$7,0($17)
lw	$2,%got(tcsm0_fifo_rp)($28)
sw	$23,7260($fp)
andi	$4,$7,0xffff
sw	$0,7264($fp)
addiu	$5,$4,876
.set	noreorder
.set	nomacro
b	$L59
lw	$6,0($2)
.set	macro
.set	reorder

$L216:
.set	noreorder
.set	nomacro
bne	$2,$0,$L239
lw	$3,%got(dMB)($28)
.set	macro
.set	reorder

$L59:
lw	$2,0($6)
andi	$2,$2,0xffff
sltu	$3,$2,$5
.set	noreorder
.set	nomacro
bne	$3,$0,$L216
sltu	$2,$2,$4
.set	macro
.set	reorder

lw	$3,%got(dMB)($28)
$L239:
li	$2,1			# 0x1
addiu	$5,$7,108
sw	$7,0($3)
sb	$2,0($7)
lw	$25,10528($fp)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$3,2904($fp)
move	$20,$2
li	$2,3			# 0x3
.set	noreorder
.set	nomacro
beq	$3,$2,$L61
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$3,7996($fp)
lw	$4,168($fp)
lw	$2,2180($fp)
lw	$6,7992($fp)
mul	$7,$3,$4
lw	$5,2832($fp)
lw	$4,172($fp)
lw	$3,8012($fp)
addu	$2,$7,$2
addu	$2,$2,$6
sb	$5,0($2)
lw	$2,7264($fp)
bne	$2,$0,$L61
lw	$2,8004($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L135
move	$7,$0
.set	macro
.set	reorder

lh	$8,7268($fp)
lh	$7,7272($fp)
$L63:
addiu	$6,$3,1
lw	$2,2184($fp)
sll	$5,$3,2
addu	$3,$4,$3
addu	$4,$4,$6
sll	$3,$3,2
addiu	$6,$5,4
sll	$4,$4,2
addu	$5,$2,$5
addu	$6,$2,$6
addu	$3,$2,$3
sh	$8,0($5)
addu	$2,$2,$4
sh	$7,2($5)
sh	$8,0($6)
sh	$7,2($6)
sh	$8,0($3)
sh	$7,2($3)
sh	$8,0($2)
sh	$7,2($2)
$L61:
lw	$4,7996($fp)
lw	$6,168($fp)
lw	$3,7992($fp)
lw	$5,2172($fp)
mul	$7,$4,$6
lw	$2,2872($fp)
lw	$4,2836($fp)
addu	$3,$7,$3
addu	$5,$5,$3
sb	$2,0($5)
lw	$2,8004($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L64
addu	$4,$4,$3
.set	macro
.set	reorder

lw	$2,32($fp)
bne	$2,$0,$L65
lw	$2,9880($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L65
li	$2,128			# 0x80
.set	macro
.set	reorder

lw	$3,10388($fp)
sll	$2,$2,$3
sw	$2,2720($fp)
sw	$2,2716($fp)
sw	$2,2712($fp)
$L67:
lw	$2,2832($fp)
beq	$2,$0,$L70
sw	$0,2832($fp)
$L211:
lbu	$2,0($4)
addiu	$2,$2,1
andi	$2,$2,0x00ff
sltu	$3,$2,100
bne	$3,$0,$L207
li	$2,99			# 0x63
$L207:
sb	$2,0($4)
$L72:
lw	$25,%got(get_MBARGs)($28)
addiu	$25,$25,%lo(get_MBARGs)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_MBARGs
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
lw	$3,0($17)
ori	$2,$2,0x4000
addiu	$4,$3,1752
sltu	$4,$4,$2
.set	noreorder
.set	nomacro
beq	$4,$0,$L76
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$9,0($22)
addiu	$3,$3,876
lw	$4,%got(task_fifo_wp)($28)
lw	$2,0($18)
lw	$7,%got(task_fifo_wp_d2)($28)
sw	$3,0($4)
lw	$4,0($9)
sw	$3,0($18)
sw	$2,0($7)
addiu	$4,$4,1
sw	$4,0($9)
.set	noreorder
.set	nomacro
bltz	$20,$L240
li	$4,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

$L78:
lw	$2,9900($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L218
lw	$25,%call16(ff_h263_loop_filter)($28)
.set	macro
.set	reorder

$L100:
lw	$2,7992($fp)
lw	$3,160($fp)
addiu	$2,$2,1
slt	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L158
sw	$2,7992($fp)
.set	macro
.set	reorder

$L102:
lw	$5,7996($fp)
move	$4,$fp
lw	$6,32($sp)
move	$20,$0
lw	$25,%call16(ff_draw_horiz_band)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_draw_horiz_band
1:	jalr	$25
mul	$5,$6,$5
.set	macro
.set	reorder

lw	$2,7996($fp)
lw	$3,164($fp)
lw	$28,24($sp)
addiu	$2,$2,1
sw	$0,7992($fp)
slt	$3,$2,$3
.set	noreorder
.set	nomacro
bne	$3,$0,$L56
sw	$2,7996($fp)
.set	macro
.set	reorder

$L28:
li	$16,-201326592			# 0xfffffffff4000000
lw	$8,0($17)
lw	$20,%got(dMB)($28)
move	$19,$0
lw	$21,%got(task_fifo_wp)($28)
addiu	$23,$16,16384
addiu	$16,$16,4096
lw	$4,%got(tcsm0_fifo_rp)($28)
$L242:
andi	$5,$8,0xffff
addiu	$6,$5,876
.set	noreorder
.set	nomacro
b	$L104
lw	$7,0($4)
.set	macro
.set	reorder

$L219:
.set	noreorder
.set	nomacro
bne	$2,$0,$L241
lw	$25,%got(get_MBARGs)($28)
.set	macro
.set	reorder

$L104:
lw	$2,0($7)
andi	$2,$2,0xffff
sltu	$3,$2,$6
.set	noreorder
.set	nomacro
bne	$3,$0,$L219
sltu	$2,$2,$5
.set	macro
.set	reorder

lw	$25,%got(get_MBARGs)($28)
$L241:
move	$4,$fp
lw	$5,%got(dMB)($28)
addiu	$25,$25,%lo(get_MBARGs)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_MBARGs
1:	jalr	$25
sw	$8,0($5)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$19,$2,$L220
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$19,$0,$L221
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,0($20)
sb	$3,0($2)
$L106:
lw	$8,0($17)
addiu	$2,$8,1752
sltu	$2,$2,$23
.set	noreorder
.set	nomacro
bne	$2,$0,$L222
addiu	$8,$8,876
.set	macro
.set	reorder

sw	$16,0($21)
move	$8,$16
$L109:
lw	$3,0($22)
addiu	$19,$19,1
lw	$4,0($18)
lw	$5,%got(task_fifo_wp_d2)($28)
sw	$8,0($18)
lw	$2,0($3)
sw	$4,0($5)
addiu	$2,$2,1
sw	$2,0($3)
li	$2,5			# 0x5
.set	noreorder
.set	nomacro
bne	$19,$2,$L242
lw	$4,%got(tcsm0_fifo_rp)($28)
.set	macro
.set	reorder

li	$4,-201326592			# 0xfffffffff4000000
$L184:
lw	$2,0($4)
.set	noreorder
.set	nomacro
beq	$2,$0,$L184
lw	$2,%got(aux_base)($28)
.set	macro
.set	reorder

li	$4,1			# 0x1
lw	$5,52($fp)
lw	$2,0($2)
sw	$4,0($2)
li	$2,13			# 0xd
.set	noreorder
.set	nomacro
beq	$5,$2,$L112
lw	$6,88($fp)
.set	macro
.set	reorder

lw	$2,10340($fp)
andi	$4,$6,0x1
lw	$7,10344($fp)
.set	noreorder
.set	nomacro
beq	$4,$0,$L114
subu	$7,$7,$2
.set	macro
.set	reorder

$L113:
lw	$4,10172($fp)
slt	$4,$4,-1
.set	noreorder
.set	nomacro
bne	$4,$0,$L243
li	$4,-17			# 0xffffffffffffffef
.set	macro
.set	reorder

lw	$4,10080($fp)
.set	noreorder
.set	nomacro
bne	$4,$0,$L243
li	$4,-17			# 0xffffffffffffffef
.set	macro
.set	reorder

$L132:
ori	$6,$6,0x10
sw	$6,88($fp)
$L114:
lw	$4,10304($fp)
bne	$4,$0,$L123
andi	$6,$6,0x10
.set	noreorder
.set	nomacro
beq	$6,$0,$L198
li	$4,7			# 0x7
.set	macro
.set	reorder

$L124:
lw	$5,9816($fp)
slt	$5,$5,3
.set	noreorder
.set	nomacro
bne	$5,$0,$L127
li	$5,1073741824			# 0x40000000
.set	macro
.set	reorder

addiu	$4,$4,48
$L126:
slt	$4,$4,$7
bne	$4,$0,$L223
$L128:
.set	noreorder
.set	nomacro
bltz	$7,$L224
move	$4,$fp
.set	macro
.set	reorder

lw	$2,7996($fp)
lw	$7,7992($fp)
lw	$5,9784($fp)
lw	$6,9788($fp)
lw	$25,%call16(ff_er_add_slice)($28)
addiu	$7,$7,-1
sw	$2,16($sp)
li	$2,112			# 0x70
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

move	$2,$0
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L76:
lw	$9,0($22)
move	$3,$21
lw	$2,0($18)
lw	$5,%got(task_fifo_wp)($28)
lw	$7,%got(task_fifo_wp_d2)($28)
lw	$4,0($9)
sw	$21,0($5)
sw	$3,0($18)
addiu	$4,$4,1
sw	$2,0($7)
sw	$4,0($9)
.set	noreorder
.set	nomacro
bgez	$20,$L78
li	$4,-2			# 0xfffffffffffffffe
.set	macro
.set	reorder

$L240:
lw	$7,7992($fp)
move	$2,$3
lw	$10,7996($fp)
.set	noreorder
.set	nomacro
bne	$20,$4,$L79
lw	$5,168($fp)
.set	macro
.set	reorder

lw	$2,9900($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L225
lw	$25,%call16(ff_h263_loop_filter)($28)
.set	macro
.set	reorder

$L80:
lw	$8,44($sp)
move	$4,$fp
lw	$5,9784($fp)
lw	$6,9788($fp)
andi	$2,$8,0x70
lw	$25,%call16(ff_er_add_slice)($28)
sw	$10,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

lw	$5,10172($fp)
lw	$2,7992($fp)
lw	$4,160($fp)
addiu	$5,$5,-1
lw	$28,24($sp)
addiu	$2,$2,1
slt	$4,$2,$4
sw	$5,10172($fp)
.set	noreorder
.set	nomacro
beq	$4,$0,$L226
sw	$2,7992($fp)
.set	macro
.set	reorder

lw	$2,7996($fp)
lw	$4,164($fp)
slt	$4,$2,$4
beq	$4,$0,$L86
$L90:
lw	$5,10304($fp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L83
lw	$25,%call16(ff_h263_resync)($28)
.set	macro
.set	reorder

lw	$4,10292($fp)
beq	$4,$0,$L86
lw	$6,7992($fp)
bne	$6,$0,$L86
teq	$4,$0,7
div	$0,$2,$4
mfhi	$2
bne	$2,$0,$L86
lw	$2,10340($fp)
lw	$4,10344($fp)
slt	$2,$4,$2
bne	$2,$0,$L86
$L87:
slt	$2,$5,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L244
lw	$4,%got(mpeg4_hw_bs_buffer)($28)
.set	macro
.set	reorder

lw	$2,32($fp)
.set	noreorder
.set	nomacro
bne	$2,$0,$L227
lw	$25,%call16(ff_mpeg4_clean_buffers)($28)
.set	macro
.set	reorder

$L88:
lw	$4,%got(mpeg4_hw_bs_buffer)($28)
$L244:
lw	$2,10340($fp)
lw	$4,0($4)
srl	$6,$2,3
addu	$4,$4,$6
li	$6,-4			# 0xfffffffffffffffc
and	$4,$4,$6
#APP
# 1446 "mpeg4.c" 1
mtc0	$4,$21,2
# 0 "" 2
#NO_APP
andi	$2,$2,0x1f
li	$4,65536			# 0x10000
sll	$2,$2,5
or	$2,$2,$4
#APP
# 1449 "mpeg4.c" 1
mtc0	$2,$21,3
# 0 "" 2
#NO_APP
$L89:
#APP
# 1453 "mpeg4.c" 1
mfc0	$2,$21,3
# 0 "" 2
#NO_APP
and	$2,$2,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L89
li	$7,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L250
lw	$6,10344($fp)
.set	macro
.set	reorder

$L70:
lw	$2,2168($fp)
beq	$2,$0,$L211
.set	noreorder
.set	nomacro
b	$L72
sb	$0,0($4)
.set	macro
.set	reorder

$L64:
lw	$2,32($fp)
bne	$2,$0,$L69
lw	$2,9880($fp)
beq	$2,$0,$L67
$L69:
lw	$2,2840($fp)
addu	$3,$2,$3
li	$2,1			# 0x1
.set	noreorder
.set	nomacro
b	$L67
sb	$2,0($3)
.set	macro
.set	reorder

$L65:
lw	$2,2840($fp)
addu	$3,$2,$3
lbu	$2,0($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L67
li	$11,1024			# 0x400
.set	macro
.set	reorder

lw	$6,8012($fp)
lw	$3,172($fp)
lw	$5,2728($fp)
addiu	$8,$6,1
lw	$2,2812($fp)
addu	$7,$3,$6
addu	$3,$3,$8
sll	$9,$7,1
sll	$10,$6,1
sll	$12,$3,1
addu	$10,$5,$10
addu	$12,$5,$12
addu	$5,$5,$9
sll	$9,$6,5
sh	$11,0($12)
sh	$11,0($5)
addu	$2,$2,$9
sh	$11,2($10)
sh	$11,0($10)
#APP
# 728 "mpeg4.c" 1
pref 30,0($2)
# 0 "" 2
# 729 "mpeg4.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 730 "mpeg4.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 731 "mpeg4.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 732 "mpeg4.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 733 "mpeg4.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 734 "mpeg4.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 735 "mpeg4.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 736 "mpeg4.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
lw	$2,2812($fp)
addiu	$9,$9,32
addu	$2,$2,$9
#APP
# 738 "mpeg4.c" 1
pref 30,0($2)
# 0 "" 2
# 739 "mpeg4.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 740 "mpeg4.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 741 "mpeg4.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 742 "mpeg4.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 743 "mpeg4.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 744 "mpeg4.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 745 "mpeg4.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 746 "mpeg4.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
lw	$2,2812($fp)
sll	$5,$7,5
addu	$2,$2,$5
#APP
# 749 "mpeg4.c" 1
pref 30,0($2)
# 0 "" 2
# 750 "mpeg4.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 751 "mpeg4.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 752 "mpeg4.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 753 "mpeg4.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 754 "mpeg4.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 755 "mpeg4.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 756 "mpeg4.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 757 "mpeg4.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
lw	$2,2812($fp)
addiu	$5,$5,32
addu	$2,$2,$5
#APP
# 759 "mpeg4.c" 1
pref 30,0($2)
# 0 "" 2
# 760 "mpeg4.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 761 "mpeg4.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 762 "mpeg4.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 763 "mpeg4.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 764 "mpeg4.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 765 "mpeg4.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 766 "mpeg4.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 767 "mpeg4.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
lw	$2,10304($fp)
slt	$2,$2,3
bne	$2,$0,$L68
lw	$2,2804($fp)
addu	$3,$2,$3
addu	$7,$2,$7
addu	$8,$2,$8
sb	$0,0($3)
addu	$2,$2,$6
sb	$0,0($7)
sb	$0,0($8)
sb	$0,0($2)
$L68:
lw	$2,168($fp)
li	$9,1024			# 0x400
lw	$5,7996($fp)
lw	$3,7992($fp)
lw	$6,2732($fp)
mul	$8,$2,$5
lw	$7,2736($fp)
addu	$5,$8,$3
lw	$3,2816($fp)
sll	$2,$5,1
sll	$8,$5,5
addu	$6,$6,$2
addu	$7,$7,$2
addu	$3,$3,$8
sh	$9,0($7)
sh	$9,0($6)
#APP
# 786 "mpeg4.c" 1
pref 30,0($3)
# 0 "" 2
# 787 "mpeg4.c" 1
.word	0b01110000011000000000000000010001	#S32STD XR0,$3,0
# 0 "" 2
# 788 "mpeg4.c" 1
.word	0b01110000011000000000010000010001	#S32STD XR0,$3,4
# 0 "" 2
# 789 "mpeg4.c" 1
.word	0b01110000011000000000100000010001	#S32STD XR0,$3,8
# 0 "" 2
# 790 "mpeg4.c" 1
.word	0b01110000011000000000110000010001	#S32STD XR0,$3,12
# 0 "" 2
# 791 "mpeg4.c" 1
.word	0b01110000011000000001000000010001	#S32STD XR0,$3,16
# 0 "" 2
# 792 "mpeg4.c" 1
.word	0b01110000011000000001010000010001	#S32STD XR0,$3,20
# 0 "" 2
# 793 "mpeg4.c" 1
.word	0b01110000011000000001100000010001	#S32STD XR0,$3,24
# 0 "" 2
# 794 "mpeg4.c" 1
.word	0b01110000011000000001110000010001	#S32STD XR0,$3,28
# 0 "" 2
#NO_APP
lw	$2,2820($fp)
addu	$2,$2,$8
#APP
# 797 "mpeg4.c" 1
pref 30,0($2)
# 0 "" 2
# 798 "mpeg4.c" 1
.word	0b01110000010000000000000000010001	#S32STD XR0,$2,0
# 0 "" 2
# 799 "mpeg4.c" 1
.word	0b01110000010000000000010000010001	#S32STD XR0,$2,4
# 0 "" 2
# 800 "mpeg4.c" 1
.word	0b01110000010000000000100000010001	#S32STD XR0,$2,8
# 0 "" 2
# 801 "mpeg4.c" 1
.word	0b01110000010000000000110000010001	#S32STD XR0,$2,12
# 0 "" 2
# 802 "mpeg4.c" 1
.word	0b01110000010000000001000000010001	#S32STD XR0,$2,16
# 0 "" 2
# 803 "mpeg4.c" 1
.word	0b01110000010000000001010000010001	#S32STD XR0,$2,20
# 0 "" 2
# 804 "mpeg4.c" 1
.word	0b01110000010000000001100000010001	#S32STD XR0,$2,24
# 0 "" 2
# 805 "mpeg4.c" 1
.word	0b01110000010000000001110000010001	#S32STD XR0,$2,28
# 0 "" 2
#NO_APP
lw	$2,2840($fp)
addu	$5,$2,$5
.set	noreorder
.set	nomacro
b	$L67
sb	$0,0($5)
.set	macro
.set	reorder

$L215:
lw	$2,9788($fp)
lw	$3,7996($fp)
addiu	$2,$2,1
bne	$2,$3,$L57
.set	noreorder
.set	nomacro
b	$L57
sw	$0,10296($fp)
.set	macro
.set	reorder

$L218:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h263_loop_filter
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L100
lw	$28,24($sp)
.set	macro
.set	reorder

$L135:
.set	noreorder
.set	nomacro
b	$L63
move	$8,$0
.set	macro
.set	reorder

$L134:
.set	noreorder
.set	nomacro
b	$L22
li	$7,1			# 0x1
.set	macro
.set	reorder

$L226:
lw	$5,7996($fp)
move	$4,$fp
lw	$6,32($sp)
lw	$25,%call16(ff_draw_horiz_band)($28)
sw	$0,7992($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_draw_horiz_band
1:	jalr	$25
mul	$5,$6,$5
.set	macro
.set	reorder

lw	$2,7996($fp)
lw	$4,164($fp)
lw	$28,24($sp)
addiu	$2,$2,1
slt	$4,$2,$4
.set	noreorder
.set	nomacro
bne	$4,$0,$L90
sw	$2,7996($fp)
.set	macro
.set	reorder

$L86:
li	$16,-201326592			# 0xfffffffff4000000
lw	$8,0($17)
lw	$20,%got(dMB)($28)
move	$19,$0
lw	$21,%got(task_fifo_wp)($28)
addiu	$23,$16,16384
addiu	$16,$16,4096
lw	$4,%got(tcsm0_fifo_rp)($28)
$L246:
andi	$5,$8,0xffff
addiu	$6,$5,876
.set	noreorder
.set	nomacro
b	$L92
lw	$7,0($4)
.set	macro
.set	reorder

$L228:
.set	noreorder
.set	nomacro
bne	$2,$0,$L245
lw	$25,%got(get_MBARGs)($28)
.set	macro
.set	reorder

$L92:
lw	$2,0($7)
andi	$2,$2,0xffff
sltu	$3,$2,$6
.set	noreorder
.set	nomacro
bne	$3,$0,$L228
sltu	$2,$2,$5
.set	macro
.set	reorder

lw	$25,%got(get_MBARGs)($28)
$L245:
move	$4,$fp
lw	$5,%got(dMB)($28)
addiu	$25,$25,%lo(get_MBARGs)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_MBARGs
1:	jalr	$25
sw	$8,0($5)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$19,$2,$L229
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$19,$0,$L230
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,0($20)
sb	$3,0($2)
$L94:
lw	$8,0($17)
addiu	$2,$8,1752
sltu	$2,$2,$23
beq	$2,$0,$L96
addiu	$8,$8,876
sw	$8,0($21)
$L97:
lw	$3,0($22)
addiu	$19,$19,1
lw	$4,0($18)
lw	$5,%got(task_fifo_wp_d2)($28)
sw	$8,0($18)
lw	$2,0($3)
sw	$4,0($5)
addiu	$2,$2,1
sw	$2,0($3)
li	$2,5			# 0x5
.set	noreorder
.set	nomacro
bne	$19,$2,$L246
lw	$4,%got(tcsm0_fifo_rp)($28)
.set	macro
.set	reorder

li	$3,-201326592			# 0xfffffffff4000000
$L183:
lw	$2,0($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L183
li	$4,1			# 0x1
.set	macro
.set	reorder

lw	$3,%got(aux_base)($28)
$L251:
lw	$31,92($sp)
move	$2,$0
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$3,0($3)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
sw	$4,0($3)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L83:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h263_resync
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L86
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L87
lw	$5,10304($fp)
.set	macro
.set	reorder

$L225:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h263_loop_filter
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$7,7992($fp)
.set	noreorder
.set	nomacro
b	$L80
lw	$10,7996($fp)
.set	macro
.set	reorder

$L213:
lw	$25,%got(mc_base)($28)
li	$2,131072			# 0x20000
ori	$2,$2,0xf809
lw	$20,0($25)
sw	$2,0($20)
.set	noreorder
.set	nomacro
b	$L237
lw	$4,288($fp)
.set	macro
.set	reorder

$L227:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_clean_buffers
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L88
lw	$5,10304($fp)
.set	macro
.set	reorder

$L96:
sw	$16,0($21)
.set	noreorder
.set	nomacro
b	$L97
move	$8,$16
.set	macro
.set	reorder

$L214:
lw	$7,7992($fp)
lw	$5,9784($fp)
lw	$25,%call16(ff_er_add_slice)($28)
sw	$2,16($sp)
li	$2,112			# 0x70
addiu	$7,$7,-1
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

lw	$5,10304($fp)
.set	noreorder
.set	nomacro
beq	$5,$0,$L39
lw	$28,24($sp)
.set	macro
.set	reorder

lw	$2,10292($fp)
beq	$2,$0,$L42
lw	$4,7992($fp)
bne	$4,$0,$L42
lw	$4,7996($fp)
teq	$2,$0,7
div	$0,$4,$2
mfhi	$4
bne	$4,$0,$L42
lw	$2,10340($fp)
lw	$4,10344($fp)
slt	$2,$4,$2
bne	$2,$0,$L42
$L43:
slt	$2,$5,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L247
lw	$4,%got(mpeg4_hw_bs_buffer)($28)
.set	macro
.set	reorder

lw	$2,32($fp)
.set	noreorder
.set	nomacro
beq	$2,$0,$L247
lw	$25,%call16(ff_mpeg4_clean_buffers)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_clean_buffers
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

lw	$28,24($sp)
lw	$5,10304($fp)
lw	$4,%got(mpeg4_hw_bs_buffer)($28)
$L247:
lw	$2,10340($fp)
lw	$4,0($4)
srl	$6,$2,3
addu	$4,$4,$6
li	$6,-4			# 0xfffffffffffffffc
and	$4,$4,$6
#APP
# 1241 "mpeg4.c" 1
mtc0	$4,$21,2
# 0 "" 2
#NO_APP
andi	$2,$2,0x1f
li	$4,65536			# 0x10000
sll	$2,$2,5
or	$2,$2,$4
#APP
# 1244 "mpeg4.c" 1
mtc0	$2,$21,3
# 0 "" 2
#NO_APP
$L45:
#APP
# 1248 "mpeg4.c" 1
mfc0	$2,$21,3
# 0 "" 2
#NO_APP
and	$2,$2,$4
.set	noreorder
.set	nomacro
bne	$2,$0,$L45
li	$7,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L250
lw	$6,10344($fp)
.set	macro
.set	reorder

$L222:
.set	noreorder
.set	nomacro
b	$L109
sw	$8,0($21)
.set	macro
.set	reorder

$L123:
lw	$4,2904($fp)
li	$8,24			# 0x18
andi	$6,$6,0x10
xori	$5,$4,0x1
li	$4,7			# 0x7
.set	noreorder
.set	nomacro
bne	$6,$0,$L124
movz	$4,$8,$5
.set	macro
.set	reorder

slt	$4,$4,$7
beq	$4,$0,$L128
$L223:
lw	$5,10332($fp)
srl	$6,$2,3
andi	$2,$2,0x7
lw	$4,0($fp)
lw	$25,%call16(av_log)($28)
addu	$3,$5,$6
li	$6,16711680			# 0xff0000
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($3)  
lwr $5, 0($3)  

# 0 "" 2
#NO_APP
addiu	$6,$6,255
srl	$3,$5,8
sll	$5,$5,8
and	$3,$3,$6
li	$6,-16777216			# 0xffffffffff000000
ori	$6,$6,0xff00
and	$5,$5,$6
lw	$6,%got($LC2)($28)
or	$3,$3,$5
sll	$5,$3,16
srl	$3,$3,16
addiu	$6,$6,%lo($LC2)
or	$3,$5,$3
sll	$2,$3,$2
li	$5,16			# 0x10
srl	$2,$2,8
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

move	$2,$0
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L79:
mul	$3,$10,$5
li	$4,-3			# 0xfffffffffffffffd
.set	noreorder
.set	nomacro
beq	$20,$4,$L231
addu	$7,$3,$7
.set	macro
.set	reorder

lw	$25,%got(dMB)($28)
li	$4,-1			# 0xffffffffffffffff
lw	$6,%got($LC1)($28)
li	$5,16			# 0x10
sw	$2,0($25)
sb	$4,0($2)
addiu	$6,$6,%lo($LC1)
lw	$2,0($9)
lw	$4,0($fp)
lw	$25,%call16(av_log)($28)
addiu	$2,$2,1
sw	$2,0($9)
lw	$2,0($9)
addiu	$2,$2,1
sw	$2,0($9)
lw	$2,0($9)
addiu	$2,$2,1
sw	$2,0($9)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
lw	$3,44($sp)
lw	$28,24($sp)
andi	$2,$3,0xe
$L210:
lw	$8,7996($fp)
move	$4,$fp
lw	$5,9784($fp)
lw	$6,9788($fp)
lw	$7,7992($fp)
$L209:
lw	$25,%call16(ff_er_add_slice)($28)
sw	$8,16($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_add_slice
1:	jalr	$25
sw	$2,20($sp)
.set	macro
.set	reorder

li	$2,-1			# 0xffffffffffffffff
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L119:
lw	$4,10172($fp)
slt	$4,$4,-1
beq	$4,$0,$L132
$L122:
li	$4,-17			# 0xffffffffffffffef
$L243:
and	$6,$6,$4
.set	noreorder
.set	nomacro
b	$L114
sw	$6,88($fp)
.set	macro
.set	reorder

$L112:
andi	$2,$6,0x1
beq	$2,$0,$L232
lw	$2,10340($fp)
lw	$7,10344($fp)
subu	$7,$7,$2
sltu	$5,$7,48
beq	$5,$0,$L113
lw	$5,10080($fp)
bne	$5,$0,$L122
bne	$7,$0,$L117
lw	$4,10172($fp)
addiu	$4,$4,16
.set	noreorder
.set	nomacro
b	$L113
sw	$4,10172($fp)
.set	macro
.set	reorder

$L224:
lw	$6,%got($LC3)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
subu	$7,$0,$7
lw	$4,0($fp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC3)
.set	macro
.set	reorder

move	$2,$0
lw	$31,92($sp)
lw	$fp,88($sp)
lw	$23,84($sp)
lw	$22,80($sp)
lw	$21,76($sp)
lw	$20,72($sp)
lw	$19,68($sp)
lw	$18,64($sp)
lw	$17,60($sp)
lw	$16,56($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,96
.set	macro
.set	reorder

$L232:
lw	$2,10340($fp)
lw	$7,10344($fp)
.set	noreorder
.set	nomacro
b	$L114
subu	$7,$7,$2
.set	macro
.set	reorder

$L127:
.set	noreorder
.set	nomacro
b	$L126
addu	$4,$4,$5
.set	macro
.set	reorder

$L220:
lw	$2,0($20)
li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L106
sb	$3,0($2)
.set	macro
.set	reorder

$L229:
lw	$2,0($20)
li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L94
sb	$3,0($2)
.set	macro
.set	reorder

$L230:
lw	$3,0($20)
.set	noreorder
.set	nomacro
b	$L94
sb	$2,0($3)
.set	macro
.set	reorder

$L231:
lw	$4,%got(dMB)($28)
li	$5,16			# 0x10
lw	$6,%got($LC0)($28)
lw	$25,%call16(av_log)($28)
sw	$2,0($4)
li	$4,-1			# 0xffffffffffffffff
addiu	$6,$6,%lo($LC0)
sb	$4,0($2)
lw	$2,0($9)
lw	$4,0($fp)
addiu	$2,$2,1
sw	$2,0($9)
lw	$2,0($9)
addiu	$2,$2,1
sw	$2,0($9)
lw	$2,0($9)
addiu	$2,$2,1
sw	$2,0($9)
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
move	$4,$fp
lw	$8,44($sp)
lw	$7,7992($fp)
lw	$28,24($sp)
andi	$2,$8,0x70
lw	$5,9784($fp)
lw	$8,7996($fp)
addiu	$7,$7,1
.set	noreorder
.set	nomacro
b	$L209
lw	$6,9788($fp)
.set	macro
.set	reorder

$L198:
lw	$8,10172($fp)
srl	$5,$2,3
lw	$4,10332($fp)
andi	$2,$2,0x7
lw	$25,%call16(av_log)($28)
addu	$5,$4,$5
lw	$4,0($fp)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, 0($5)  

# 0 "" 2
#NO_APP
sw	$8,20($sp)
li	$8,16711680			# 0xff0000
srl	$5,$6,8
addiu	$8,$8,255
sll	$6,$6,8
and	$5,$5,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$6,$6,$8
or	$5,$5,$6
sll	$6,$5,16
srl	$5,$5,16
or	$5,$6,$5
lw	$6,%got($LC4)($28)
sll	$2,$5,$2
li	$5,16			# 0x10
srl	$2,$2,8
addiu	$6,$6,%lo($LC4)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$2,16($sp)
.set	macro
.set	reorder

lw	$8,44($sp)
lw	$28,24($sp)
.set	noreorder
.set	nomacro
b	$L210
andi	$2,$8,0x70
.set	macro
.set	reorder

$L117:
.set	noreorder
.set	nomacro
beq	$7,$4,$L119
srl	$5,$2,3
.set	macro
.set	reorder

lw	$4,10332($fp)
li	$8,16711680			# 0xff0000
addu	$4,$4,$5
addiu	$8,$8,255
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $5, 3($4)  
lwr $5, 0($4)  

# 0 "" 2
#NO_APP
srl	$4,$5,8
sll	$5,$5,8
and	$4,$4,$8
li	$8,-16777216			# 0xffffffffff000000
ori	$8,$8,0xff00
and	$5,$5,$8
or	$5,$4,$5
sll	$4,$5,16
srl	$5,$5,16
andi	$8,$2,0x7
or	$4,$4,$5
sll	$4,$4,$8
nor	$5,$0,$2
li	$8,127			# 0x7f
andi	$5,$5,0x7
srl	$4,$4,24
sra	$5,$8,$5
or	$4,$4,$5
.set	noreorder
.set	nomacro
beq	$4,$8,$L233
slt	$4,$7,9
.set	macro
.set	reorder

$L120:
lw	$4,10172($fp)
addiu	$4,$4,1
.set	noreorder
.set	nomacro
b	$L113
sw	$4,10172($fp)
.set	macro
.set	reorder

$L42:
li	$16,-201326592			# 0xfffffffff4000000
lw	$2,0($17)
lw	$20,%got(dMB)($28)
move	$19,$0
lw	$23,%got(task_fifo_wp)($28)
addiu	$21,$16,16384
addiu	$16,$16,4096
lw	$25,%got(tcsm0_fifo_rp)($28)
$L249:
andi	$5,$2,0xffff
addiu	$4,$5,876
.set	noreorder
.set	nomacro
b	$L48
lw	$6,0($25)
.set	macro
.set	reorder

$L234:
.set	noreorder
.set	nomacro
bne	$3,$0,$L248
lw	$25,%got(get_MBARGs)($28)
.set	macro
.set	reorder

$L48:
lw	$3,0($6)
andi	$3,$3,0xffff
sltu	$7,$3,$4
.set	noreorder
.set	nomacro
bne	$7,$0,$L234
sltu	$3,$3,$5
.set	macro
.set	reorder

lw	$25,%got(get_MBARGs)($28)
$L248:
move	$4,$fp
lw	$3,%got(dMB)($28)
addiu	$25,$25,%lo(get_MBARGs)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_MBARGs
1:	jalr	$25
sw	$2,0($3)
.set	macro
.set	reorder

li	$2,2			# 0x2
.set	noreorder
.set	nomacro
beq	$19,$2,$L235
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$19,$0,$L236
li	$3,1			# 0x1
.set	macro
.set	reorder

lw	$2,0($20)
sb	$3,0($2)
$L50:
lw	$3,0($17)
addiu	$2,$3,876
addiu	$3,$3,1752
sltu	$3,$3,$21
.set	noreorder
.set	nomacro
bne	$3,$0,$L52
sw	$2,0($17)
.set	macro
.set	reorder

sw	$16,0($23)
move	$2,$16
$L52:
lw	$5,0($22)
addiu	$19,$19,1
lw	$6,0($18)
lw	$4,%got(task_fifo_wp_d2)($28)
sw	$2,0($18)
lw	$3,0($5)
sw	$6,0($4)
addiu	$3,$3,1
sw	$3,0($5)
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
bne	$19,$3,$L249
lw	$25,%got(tcsm0_fifo_rp)($28)
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
$L182:
lw	$3,0($2)
.set	noreorder
.set	nomacro
beq	$3,$0,$L182
lw	$3,%got(aux_base)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L251
li	$4,1			# 0x1
.set	macro
.set	reorder

$L39:
lw	$25,%call16(ff_h263_resync)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h263_resync
1:	jalr	$25
move	$4,$fp
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L42
lw	$28,24($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L43
lw	$5,10304($fp)
.set	macro
.set	reorder

$L233:
.set	noreorder
.set	nomacro
beq	$4,$0,$L121
addiu	$4,$2,8
.set	macro
.set	reorder

lw	$4,10172($fp)
addiu	$4,$4,-1
.set	noreorder
.set	nomacro
b	$L113
sw	$4,10172($fp)
.set	macro
.set	reorder

$L121:
andi	$4,$4,0x8
.set	noreorder
.set	nomacro
beq	$4,$0,$L120
slt	$4,$7,17
.set	macro
.set	reorder

beq	$4,$0,$L120
lw	$4,10172($fp)
addiu	$4,$4,4
.set	noreorder
.set	nomacro
b	$L113
sw	$4,10172($fp)
.set	macro
.set	reorder

$L221:
lw	$3,0($20)
.set	noreorder
.set	nomacro
b	$L106
sb	$2,0($3)
.set	macro
.set	reorder

$L236:
lw	$3,0($20)
.set	noreorder
.set	nomacro
b	$L50
sb	$2,0($3)
.set	macro
.set	reorder

$L235:
lw	$2,0($20)
li	$3,-1			# 0xffffffffffffffff
.set	noreorder
.set	nomacro
b	$L50
sb	$3,0($2)
.set	macro
.set	reorder

.end	decode_slice
.size	decode_slice, .-decode_slice
.section	.text.mpeg4_motion_set.constprop.4,"ax",@progbits
.align	2
.align	5
.set	nomips16
.set	nomicromips
.ent	mpeg4_motion_set.constprop.4
.type	mpeg4_motion_set.constprop.4, @function
mpeg4_motion_set.constprop.4:
.frame	$sp,16,$31		# vars= 0, regs= 3/0, args= 0, gp= 0
.mask	0x00070000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
sll	$2,$4,4
sll	$4,$4,6
addiu	$sp,$sp,-16
addu	$4,$2,$4
lw	$2,%got(mc_base)($28)
move	$9,$0
sw	$18,12($sp)
li	$11,128			# 0x80
sw	$17,8($sp)
sw	$16,4($sp)
lw	$10,0($2)
lw	$2,%got(IntpFMT)($28)
addiu	$3,$2,%lo(IntpFMT)
sll	$2,$4,3
move	$8,$3
subu	$2,$2,$4
addu	$2,$3,$2
$L253:
lbu	$6,5($2)
addiu	$13,$9,1280
lbu	$17,0($2)
addiu	$15,$9,1284
lbu	$12,1($2)
addu	$13,$10,$13
lbu	$7,33($2)
sll	$6,$6,31
lbu	$16,7($2)
andi	$17,$17,0x3
lbu	$14,27($2)
sll	$17,$17,28
lbu	$4,25($2)
andi	$7,$7,0x1
andi	$5,$12,0x1
lbu	$12,29($2)
or	$6,$7,$6
lbu	$7,31($2)
sll	$5,$5,27
andi	$16,$16,0x1
or	$6,$6,$17
sll	$16,$16,24
or	$5,$6,$5
sll	$4,$4,16
andi	$6,$14,0xf
or	$5,$5,$16
sll	$6,$6,8
andi	$3,$12,0x1
or	$4,$5,$4
sll	$3,$3,2
andi	$5,$7,0x1
or	$4,$4,$6
sll	$5,$5,1
or	$3,$4,$3
or	$3,$3,$5
addu	$15,$10,$15
sw	$3,0($13)
addiu	$14,$9,1028
lbu	$5,6($2)
addiu	$13,$9,1024
lbu	$6,2($2)
addu	$14,$10,$14
lbu	$7,34($2)
addu	$13,$10,$13
lbu	$18,3($2)
sll	$5,$5,31
lbu	$17,4($2)
andi	$6,$6,0x1
lbu	$16,8($2)
andi	$7,$7,0x1
sll	$6,$6,27
lbu	$4,26($2)
andi	$18,$18,0x1
lbu	$25,28($2)
or	$7,$7,$5
lbu	$24,30($2)
sll	$18,$18,26
lbu	$12,32($2)
andi	$5,$17,0x1
or	$6,$7,$6
sll	$5,$5,25
andi	$7,$16,0x1
or	$6,$6,$18
sll	$7,$7,24
or	$5,$6,$5
sll	$4,$4,16
andi	$6,$25,0xf
or	$5,$5,$7
sll	$6,$6,8
andi	$3,$24,0x1
or	$4,$5,$4
sll	$3,$3,2
andi	$5,$12,0x1
or	$4,$4,$6
sll	$5,$5,1
or	$3,$4,$3
or	$3,$3,$5
addiu	$5,$9,1156
sw	$3,0($15)
addiu	$2,$2,35
lb	$4,-19($2)
addu	$15,$10,$5
lbu	$3,-20($2)
addiu	$6,$9,1152
lbu	$5,-22($2)
addiu	$8,$8,35
lbu	$7,-21($2)
sll	$4,$4,24
sll	$3,$3,16
or	$4,$5,$4
sll	$5,$7,8
or	$3,$4,$3
or	$3,$3,$5
addu	$6,$10,$6
sw	$3,0($14)
addiu	$12,$9,3328
lb	$4,-23($2)
addiu	$7,$9,3332
lbu	$3,-24($2)
addu	$12,$10,$12
lbu	$5,-26($2)
addu	$7,$10,$7
lbu	$14,-25($2)
sll	$4,$4,24
sll	$3,$3,16
or	$4,$5,$4
sll	$5,$14,8
or	$3,$4,$3
or	$3,$3,$5
addiu	$9,$9,8
sw	$3,0($13)
lb	$4,-11($2)
lbu	$5,-14($2)
lbu	$3,-12($2)
lbu	$13,-13($2)
sll	$4,$4,24
sll	$3,$3,16
or	$4,$5,$4
sll	$5,$13,8
or	$3,$4,$3
or	$3,$3,$5
sw	$3,0($15)
lb	$4,-15($2)
lbu	$3,-16($2)
lbu	$5,-18($2)
lbu	$13,-17($2)
sll	$4,$4,24
sll	$3,$3,16
or	$4,$5,$4
sll	$5,$13,8
or	$3,$4,$3
or	$3,$3,$5
sw	$3,0($6)
lbu	$6,-28($8)
lbu	$5,-8($8)
lbu	$14,-30($8)
lbu	$3,-26($8)
sll	$6,$6,15
andi	$5,$5,0x7
lbu	$13,-25($8)
lbu	$4,-10($8)
sll	$5,$5,12
andi	$6,$6,0xffff
sll	$14,$14,31
or	$5,$6,$5
andi	$6,$3,0x7
or	$5,$5,$14
sll	$3,$6,9
andi	$4,$4,0x3f
andi	$6,$13,0x7
or	$4,$5,$4
sll	$5,$6,6
or	$3,$4,$3
or	$3,$3,$5
sw	$3,0($12)
lbu	$6,-27($8)
lbu	$5,-7($8)
lbu	$13,-29($8)
lbu	$3,-18($8)
sll	$6,$6,15
andi	$5,$5,0x7
lbu	$4,-9($8)
lbu	$12,-17($8)
sll	$5,$5,12
andi	$6,$6,0xffff
sll	$13,$13,31
or	$5,$6,$5
andi	$6,$3,0x7
or	$5,$5,$13
sll	$3,$6,9
andi	$4,$4,0x3f
andi	$6,$12,0x7
or	$4,$5,$4
sll	$5,$6,6
or	$3,$4,$3
or	$3,$3,$5
sw	$3,0($7)
bne	$9,$11,$L253
lw	$18,12($sp)

lw	$17,8($sp)
lw	$16,4($sp)
j	$31
addiu	$sp,$sp,16

.set	macro
.set	reorder
.end	mpeg4_motion_set.constprop.4
.size	mpeg4_motion_set.constprop.4, .-mpeg4_motion_set.constprop.4
.section	.text.mpeg4_p1_jumpto_main,"ax",@progbits
.align	2
.align	5
.globl	mpeg4_p1_jumpto_main
.set	nomips16
.set	nomicromips
.ent	mpeg4_p1_jumpto_main
.type	mpeg4_p1_jumpto_main, @function
mpeg4_p1_jumpto_main:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,-201326592			# 0xfffffffff4000000
#APP
# 435 "mpeg4.c" 1
jr	$2	#i_jr
# 0 "" 2
#NO_APP
j	$31
.end	mpeg4_p1_jumpto_main
.size	mpeg4_p1_jumpto_main, .-mpeg4_p1_jumpto_main
.section	.text.unlikely.mpeg4_decode_end,"ax",@progbits
.align	2
.globl	mpeg4_decode_end
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_end
.type	mpeg4_decode_end, @function
mpeg4_decode_end:
.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
.mask	0x80000000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	nomacro
addiu	$sp,$sp,-32
lw	$25,%call16(MPV_common_end)($28)
lw	$4,136($4)
sw	$31,28($sp)
.cprestore	16
.reloc	1f,R_MIPS_JALR,MPV_common_end
1:	jalr	$25
nop

move	$2,$0
lw	$28,16($sp)
lw	$31,28($sp)
lw	$3,%got(use_jz_buf)($28)
sw	$0,0($3)
j	$31
addiu	$sp,$sp,32

.set	macro
.set	reorder
.end	mpeg4_decode_end
.size	mpeg4_decode_end, .-mpeg4_decode_end
.section	.rodata.str1.4
.align	2
$LC5:
.ascii	"msmpeg4 len of aux task = %d\012\000"
.align	2
$LC6:
.ascii	"reserved_mem addr = 0x%08x, *reserved_mem = 0x%08x\012\000"
.align	2
$LC7:
.ascii	"mpeg4 len of aux task = %d\012\000"
.section	.text.unlikely.mpeg4_decode_init,"ax",@progbits
.align	2
.globl	mpeg4_decode_init
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_init
.type	mpeg4_decode_init, @function
mpeg4_decode_init:
.frame	$sp,56,$31		# vars= 0, regs= 7/0, args= 16, gp= 8
.mask	0x803f0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-56
lw	$2,%got(rota_crc)($28)
.cprestore	16
sw	$17,32($sp)
move	$17,$4
sw	$16,28($sp)
sw	$31,52($sp)
sw	$21,48($sp)
sw	$20,44($sp)
sw	$19,40($sp)
sw	$18,36($sp)
sh	$0,0($2)
li	$2,3			# 0x3
lw	$16,136($4)
#APP
# 540 "mpeg4.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
lw	$2,%got(mc_base)($28)
move	$12,$0
li	$14,16			# 0x10
lw	$13,0($2)
lw	$2,%got(IntpFMT)($28)
move	$9,$13
addiu	$2,$2,%lo(IntpFMT)
$L260:
lbu	$11,565($2)
addiu	$2,$2,35
lbu	$6,525($2)
addiu	$9,$9,8
lbu	$7,558($2)
addiu	$12,$12,1
sll	$11,$11,31
andi	$6,$6,0x3
andi	$7,$7,0x1
lbu	$24,526($2)
sll	$6,$6,28
lbu	$15,532($2)
or	$7,$7,$11
lb	$10,550($2)
or	$6,$7,$6
lbu	$8,552($2)
andi	$24,$24,0x1
lbu	$19,554($2)
sll	$24,$24,27
andi	$4,$15,0x1
lbu	$18,556($2)
andi	$5,$10,0xff
sll	$4,$4,24
or	$6,$6,$24
sll	$5,$5,16
andi	$7,$8,0xf
or	$4,$6,$4
sll	$7,$7,8
andi	$3,$19,0x1
andi	$6,$18,0x1
or	$4,$4,$5
sll	$3,$3,2
or	$5,$4,$7
sll	$4,$6,1
or	$3,$5,$3
or	$3,$3,$4
sll	$15,$15,15
sw	$3,1272($9)
andi	$4,$8,0x7
lbu	$3,531($2)
sll	$4,$4,12
lbu	$7,527($2)
andi	$15,$15,0xffff
lbu	$8,559($2)
or	$15,$15,$4
lbu	$6,528($2)
sll	$25,$3,31
lbu	$5,529($2)
andi	$7,$7,0x1
lbu	$4,533($2)
andi	$8,$8,0x1
lb	$24,551($2)
sll	$7,$7,27
andi	$6,$6,0x1
lbu	$18,553($2)
or	$8,$8,$25
lbu	$3,555($2)
sll	$6,$6,26
lbu	$19,557($2)
andi	$5,$5,0x1
or	$7,$8,$7
andi	$20,$4,0x1
sll	$8,$5,25
or	$7,$7,$6
andi	$5,$24,0xff
sll	$20,$20,24
or	$6,$7,$8
sll	$5,$5,16
andi	$7,$18,0xf
or	$6,$6,$20
sll	$7,$7,8
andi	$3,$3,0x1
or	$5,$6,$5
sll	$3,$3,2
andi	$6,$19,0x1
or	$5,$5,$7
sll	$6,$6,1
or	$3,$5,$3
or	$3,$3,$6
sll	$4,$4,15
sw	$3,1276($9)
andi	$3,$18,0x7
lb	$5,541($2)
sll	$3,$3,12
lbu	$8,540($2)
andi	$4,$4,0xffff
lbu	$6,538($2)
or	$4,$4,$3
lbu	$7,539($2)
sll	$5,$5,24
sll	$3,$8,16
or	$5,$6,$5
sll	$6,$7,8
or	$3,$5,$3
or	$3,$3,$6
andi	$24,$24,0x3f
sw	$3,1020($9)
or	$3,$4,$25
lb	$5,537($2)
or	$24,$3,$24
lb	$8,535($2)
or	$11,$15,$11
lb	$25,534($2)
andi	$10,$10,0x3f
lbu	$6,536($2)
sll	$5,$5,24
sll	$4,$8,8
andi	$7,$25,0xff
sll	$6,$6,16
or	$5,$7,$5
or	$5,$5,$6
andi	$4,$4,0xffff
or	$4,$5,$4
andi	$25,$25,0x7
sw	$4,1016($9)
or	$10,$11,$10
lb	$5,549($2)
sll	$25,$25,9
lbu	$7,546($2)
andi	$8,$8,0x7
lbu	$4,548($2)
sll	$8,$8,6
lbu	$3,547($2)
sll	$5,$5,24
or	$25,$10,$25
sll	$4,$4,16
or	$5,$7,$5
sll	$3,$3,8
or	$4,$5,$4
or	$4,$4,$3
or	$6,$25,$8
sw	$4,1148($9)
lb	$3,542($2)
lb	$4,545($2)
lb	$7,543($2)
lbu	$11,544($2)
andi	$10,$3,0x7
sll	$4,$4,24
sll	$8,$7,8
andi	$5,$3,0xff
sll	$11,$11,16
sll	$10,$10,9
andi	$7,$7,0x7
or	$3,$5,$4
sll	$7,$7,6
or	$4,$3,$11
or	$24,$24,$10
andi	$3,$8,0xffff
or	$3,$4,$3
or	$7,$24,$7
sw	$3,1144($9)
sw	$6,3320($9)
sw	$7,3324($9)
.set	noreorder
.set	nomacro
bne	$12,$14,$L260
li	$3,7			# 0x7
.set	macro
.set	reorder

lw	$2,%got(aux_base)($28)
li	$4,65161			# 0xfe89
sw	$3,4($13)
sw	$3,2052($13)
lw	$5,0($2)
li	$2,-201326592			# 0xfffffffff4000000
sw	$4,0($13)
li	$4,-2147483648			# 0xffffffff80000000
addiu	$3,$2,16384
addiu	$4,$4,16384
sw	$4,48($13)
li	$4,1			# 0x1
sw	$0,2096($13)
sw	$4,0($5)
$L261:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L261
lw	$2,%got(tcsm1_base)($28)
li	$3,49152			# 0xc000
move	$18,$2
lw	$2,0($2)
addu	$3,$2,$3
$L262:
sw	$0,0($2)
addiu	$2,$2,4
bne	$2,$3,$L262
lw	$2,%got(sram_base)($28)
lw	$2,0($2)
addiu	$3,$2,28672
$L263:
sw	$0,0($2)
addiu	$2,$2,4
.set	noreorder
.set	nomacro
bne	$2,$3,$L263
li	$21,2			# 0x2
.set	macro
.set	reorder

lw	$5,660($17)
lw	$3,664($17)
move	$4,$16
lw	$2,232($17)
li	$19,1			# 0x1
lw	$25,%call16(MPV_decode_defaults)($28)
li	$20,5			# 0x5
sw	$5,8($16)
sw	$3,12($16)
sw	$2,88($16)
sw	$17,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_decode_defaults
1:	jalr	$25
sw	$21,28($16)
.set	macro
.set	reorder

move	$4,$17
lw	$28,16($sp)
lw	$2,132($17)
lw	$25,496($17)
sw	$20,10040($16)
sw	$19,10096($16)
lw	$3,%got(ff_h263_decode_mb)($28)
lw	$5,48($2)
.set	noreorder
.set	nomacro
jalr	$25
sw	$3,10528($16)
.set	macro
.set	reorder

li	$4,18			# 0x12
lw	$3,132($17)
lw	$28,16($sp)
sw	$2,52($17)
sw	$19,2948($16)
lw	$3,8($3)
.set	noreorder
.set	nomacro
beq	$3,$4,$L265
sltu	$4,$3,19
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L266
li	$4,15			# 0xf
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$4,$L267
sltu	$4,$3,16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L268
li	$4,16			# 0x10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$20,$L269
li	$4,13			# 0xd
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$4,$L295
lw	$25,%call16(ff_find_hwaccel)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L264
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L268:
.set	noreorder
.set	nomacro
beq	$3,$4,$L271
li	$4,17			# 0x11
.set	macro
.set	reorder

bne	$3,$4,$L286
li	$4,3			# 0x3
sw	$19,44($16)
sw	$19,32($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$4,10304($16)
.set	macro
.set	reorder

$L266:
li	$4,22			# 0x16
.set	noreorder
.set	nomacro
beq	$3,$4,$L273
sltu	$4,$3,23
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$4,$0,$L274
li	$4,19			# 0x13
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$4,$L275
li	$4,21			# 0x15
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$3,$4,$L295
lw	$25,%call16(ff_find_hwaccel)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L264
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L274:
addiu	$4,$3,-73
sltu	$4,$4,2
beq	$4,$0,$L286
li	$4,6			# 0x6
sw	$19,44($16)
sw	$19,32($16)
sw	$4,10304($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$19,900($17)
.set	macro
.set	reorder

$L269:
sw	$0,2948($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$21,900($17)
.set	macro
.set	reorder

$L267:
sw	$19,44($16)
sw	$19,32($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$19,10304($16)
.set	macro
.set	reorder

$L271:
sw	$19,44($16)
sw	$19,32($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$21,10304($16)
.set	macro
.set	reorder

$L265:
li	$4,4			# 0x4
sw	$19,44($16)
sw	$19,32($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$4,10304($16)
.set	macro
.set	reorder

$L275:
sw	$19,44($16)
sw	$19,32($16)
.set	noreorder
.set	nomacro
b	$L270
sw	$20,10304($16)
.set	macro
.set	reorder

$L273:
sw	$19,48($16)
$L270:
lw	$25,%call16(ff_find_hwaccel)($28)
$L295:
move	$4,$3
move	$5,$2
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_find_hwaccel
1:	jalr	$25
sw	$3,52($16)
.set	macro
.set	reorder

li	$4,32			# 0x20
lw	$28,16($sp)
li	$5,524288			# 0x80000
lw	$3,0($18)
sw	$2,872($17)
addiu	$3,$3,16416
lw	$2,%got(t1_dFRM)($28)
lw	$25,%call16(jz4740_alloc_frame)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
sw	$3,0($2)
.set	macro
.set	reorder

li	$2,-201326592			# 0xfffffffff4000000
lw	$28,16($sp)
sw	$0,0($2)
lw	$2,10304($16)
lw	$18,0($18)
addiu	$2,$2,-1
sltu	$2,$2,2
.set	noreorder
.set	nomacro
beq	$2,$0,$L277
lw	$25,%call16(printf)($28)
.set	macro
.set	reorder

lw	$19,%got(msmpeg4_auxcodes_len)($28)
lw	$4,%got($LC5)($28)
lw	$5,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC5)
.set	macro
.set	reorder

move	$4,$0
lw	$28,16($sp)
move	$6,$19
lw	$7,%got(msmpeg4_aux_task_codes)($28)
$L278:
lw	$2,0($6)
sll	$3,$4,2
addu	$5,$7,$3
srl	$2,$2,2
addu	$3,$18,$3
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L294
addiu	$4,$4,1
.set	macro
.set	reorder

lw	$2,0($5)
.set	noreorder
.set	nomacro
b	$L278
sw	$2,0($3)
.set	macro
.set	reorder

$L277:
lw	$19,%got(mpeg4_auxcodes_len)($28)
lw	$4,%got($LC7)($28)
lw	$5,0($19)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC7)
.set	macro
.set	reorder

move	$4,$0
lw	$28,16($sp)
move	$5,$19
lw	$7,%got(mpeg4_aux_task_codes)($28)
$L281:
lw	$2,0($5)
sll	$3,$4,2
addu	$6,$7,$3
srl	$2,$2,2
addu	$3,$18,$3
sltu	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L294
addiu	$4,$4,1
.set	macro
.set	reorder

lw	$2,0($6)
.set	noreorder
.set	nomacro
b	$L281
sw	$2,0($3)
.set	macro
.set	reorder

$L294:
lw	$4,%got($LC6)($28)
move	$5,$18
lw	$25,%call16(printf)($28)
lw	$6,0($18)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
addiu	$4,$4,%lo($LC6)
.set	macro
.set	reorder

li	$2,-2147483648			# 0xffffffff80000000
lw	$28,16($sp)
addiu	$3,$2,16384
$L283:
#APP
# 679 "mpeg4.c" 1
cache 1,0($2)
# 0 "" 2
#NO_APP
addiu	$2,$2,32
bne	$2,$3,$L283
#APP
# 679 "mpeg4.c" 1
sync
# 0 "" 2
# 680 "mpeg4.c" 1
sync
# 0 "" 2
#NO_APP
li	$3,1			# 0x1
lw	$25,%call16(jz4740_alloc_frame)($28)
li	$2,-201326592			# 0xfffffffff4000000
li	$5,1048576			# 0x100000
sw	$3,12($2)
move	$4,$0
sw	$3,16($2)
li	$3,257			# 0x101
lw	$2,%got(sde_base)($28)
lw	$2,0($2)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,jz4740_alloc_frame
1:	jalr	$25
sw	$3,48($2)
.set	macro
.set	reorder

lw	$28,16($sp)
move	$4,$2
lw	$3,%got(mpeg4_sw_bs_buffer)($28)
lw	$25,%call16(get_phy_addr)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_phy_addr
1:	jalr	$25
sw	$2,0($3)
.set	macro
.set	reorder

li	$3,-4194304			# 0xffffffffffc00000
lw	$28,16($sp)
srl	$4,$2,22
lw	$5,132($17)
and	$3,$2,$3
sll	$4,$4,12
ori	$3,$3,0x3
or	$3,$3,$4
lw	$4,%got(mpeg4_hw_bs_buffer)($28)
lw	$6,8($5)
lw	$5,%got(vpu_base)($28)
sw	$2,0($4)
li	$2,-9			# 0xfffffffffffffff7
and	$2,$2,$6
lw	$5,0($5)
sw	$3,16($5)
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
bne	$2,$3,$L284
lw	$25,%call16(MPV_common_init)($28)
.set	macro
.set	reorder

$L285:
lw	$25,%call16(mpeg4_decode_init_vlc)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_decode_init_vlc
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$4,1			# 0x1
lw	$28,16($sp)
move	$2,$0
lw	$3,%got(use_jz_buf)($28)
.set	noreorder
.set	nomacro
b	$L264
sw	$4,0($3)
.set	macro
.set	reorder

$L284:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_common_init
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L285
lw	$28,16($sp)
.set	macro
.set	reorder

$L286:
li	$2,-1			# 0xffffffffffffffff
$L264:
lw	$31,52($sp)
lw	$21,48($sp)
lw	$20,44($sp)
lw	$19,40($sp)
lw	$18,36($sp)
lw	$17,32($sp)
lw	$16,28($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,56
.set	macro
.set	reorder

.end	mpeg4_decode_init
.size	mpeg4_decode_init, .-mpeg4_decode_init
.section	.rodata.str1.4
.align	2
$LC8:
.ascii	"p\000"
.align	2
$LC9:
.ascii	"\000"
.align	2
$LC10:
.ascii	"this codec does not support truncated bitstreams\012\000"
.align	2
$LC11:
.ascii	"XVID\000"
.align	2
$LC12:
.ascii	"XVIX\000"
.align	2
$LC13:
.ascii	"RMP4\000"
.align	2
$LC14:
.ascii	"SIPP\000"
.align	2
$LC15:
.ascii	"DIVX\000"
.align	2
$LC16:
.ascii	"UMP4\000"
.align	2
$LC17:
.ascii	"bugs: %X lavc_build:%d xvid_build:%d divx_version:%d div"
.ascii	"x_build:%d %s\012\000"
.align	2
$LC18:
.ascii	"mul slice %d %d\012\000"
.section	.text.mpeg4_decode_frame,"ax",@progbits
.align	2
.align	5
.globl	mpeg4_decode_frame
.set	nomips16
.set	nomicromips
.ent	mpeg4_decode_frame
.type	mpeg4_decode_frame, @function
mpeg4_decode_frame:
.frame	$sp,136,$31		# vars= 48, regs= 10/0, args= 40, gp= 8
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.cpload	$25
.set	reorder
addiu	$sp,$sp,-136
lw	$8,16($7)
lw	$2,20($7)
lw	$3,12($4)
lw	$7,596($4)
sw	$16,96($sp)
.cprestore	40
sw	$31,132($sp)
sw	$fp,128($sp)
sw	$23,124($sp)
sw	$22,120($sp)
sw	$21,116($sp)
sw	$20,112($sp)
sw	$19,108($sp)
sw	$18,104($sp)
sw	$17,100($sp)
sw	$8,92($sp)
sw	$2,88($sp)
lw	$16,136($4)
sw	$3,64($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L297
sw	$7,68($16)
.set	macro
.set	reorder

lw	$3,10096($16)
.set	noreorder
.set	nomacro
bne	$3,$0,$L595
lw	$31,132($sp)
.set	macro
.set	reorder

lw	$3,2692($16)
beq	$3,$0,$L595
move	$2,$3
addiu	$3,$3,208
$L299:
lw	$9,0($2)
addiu	$2,$2,16
addiu	$5,$5,16
lw	$8,-12($2)
lw	$7,-8($2)
lw	$4,-4($2)
sw	$9,-16($5)
sw	$8,-12($5)
sw	$7,-8($5)
.set	noreorder
.set	nomacro
bne	$2,$3,$L299
sw	$4,-4($5)
.set	macro
.set	reorder

lw	$3,4($2)
lw	$4,0($2)
move	$2,$0
sw	$3,4($5)
li	$3,216			# 0xd8
sw	$4,0($5)
sw	$0,2692($16)
sw	$3,0($6)
$L492:
lw	$31,132($sp)
$L595:
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

$L297:
move	$17,$4
li	$4,65536			# 0x10000
move	$20,$6
and	$3,$3,$4
.set	noreorder
.set	nomacro
bne	$3,$0,$L300
move	$18,$5
.set	macro
.set	reorder

$L305:
lw	$3,%got($LC15)($28)
lw	$2,%got($LC11)($28)
lw	$19,%got($LC17)($28)
addiu	$5,$3,%lo($LC15)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $22, 3($5)  
lwr $22, %lo($LC15)($3)  

# 0 "" 2
#NO_APP
lw	$3,10204($16)
addiu	$4,$2,%lo($LC11)
addiu	$19,$19,%lo($LC17)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $21, 3($4)  
lwr $21, %lo($LC11)($2)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$0,$L308
lw	$2,88($sp)
.set	macro
.set	reorder

$L569:
lw	$2,10196($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L307
lw	$2,88($sp)
.set	macro
.set	reorder

slt	$4,$2,20
.set	noreorder
.set	nomacro
beq	$4,$0,$L596
sll	$2,$2,3
.set	macro
.set	reorder

$L307:
sll	$3,$3,3
sra	$2,$3,3
.set	noreorder
.set	nomacro
bltz	$2,$L466
lw	$4,10200($16)
.set	macro
.set	reorder

bltz	$3,$L466
addu	$2,$4,$2
$L309:
sw	$2,10336($16)
sw	$4,10332($16)
sw	$3,10344($16)
sw	$0,10340($16)
sw	$0,10204($16)
lw	$2,124($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L597
lw	$25,%call16(MPV_common_init)($28)
.set	macro
.set	reorder

$L315:
lw	$2,2696($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L598
lw	$25,%call16(ff_find_unused_picture)($28)
.set	macro
.set	reorder

lw	$2,0($2)
beq	$2,$0,$L316
$L598:
move	$5,$0
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_find_unused_picture
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

sll	$3,$2,3
lw	$4,200($16)
sll	$2,$2,5
lw	$28,40($sp)
addu	$2,$3,$2
sll	$3,$2,4
subu	$2,$3,$2
addu	$2,$4,$2
sw	$2,2696($16)
$L316:
lw	$2,10304($16)
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$2,$3,$L562
lw	$25,%call16(ff_wmv2_decode_picture_header)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$2,$0,$L563
lw	$25,%call16(msmpeg4_decode_picture_header)($28)
.set	macro
.set	reorder

lw	$2,32($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L320
li	$2,21			# 0x15
.set	macro
.set	reorder

lw	$3,0($16)
lw	$2,28($3)
.set	noreorder
.set	nomacro
beq	$2,$0,$L599
lw	$25,%call16(JZC_mpeg4_decode_picture_header)($28)
.set	macro
.set	reorder

lw	$4,136($16)
.set	noreorder
.set	nomacro
bne	$4,$0,$L600
addiu	$5,$16,10332
.set	macro
.set	reorder

sll	$2,$2,3
lw	$6,24($3)
sra	$3,$2,3
bltz	$3,$L470
bltz	$2,$L470
addu	$3,$6,$3
$L322:
lw	$25,%call16(JZC_mpeg4_decode_picture_header)($28)
addiu	$5,$sp,48
move	$4,$16
sw	$6,48($sp)
sw	$2,60($sp)
sw	$3,52($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,JZC_mpeg4_decode_picture_header
1:	jalr	$25
sw	$0,56($sp)
.set	macro
.set	reorder

lw	$28,40($sp)
lw	$25,%call16(JZC_mpeg4_decode_picture_header)($28)
$L599:
addiu	$5,$16,10332
$L600:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,JZC_mpeg4_decode_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,40($sp)
$L318:
li	$3,100			# 0x64
.set	noreorder
.set	nomacro
beq	$2,$3,$L589
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L601
lw	$31,132($sp)
.set	macro
.set	reorder

lw	$3,10096($16)
li	$4,-1			# 0xffffffffffffffff
lw	$2,10212($16)
sltu	$3,$3,1
.set	noreorder
.set	nomacro
beq	$2,$4,$L564
sw	$3,264($17)
.set	macro
.set	reorder

bltz	$2,$L331
lw	$3,10188($16)
bltz	$3,$L331
sw	$4,10192($16)
sw	$4,10188($16)
$L331:
lw	$7,88($16)
andi	$3,$7,0x1
.set	noreorder
.set	nomacro
beq	$3,$0,$L602
andi	$3,$7,0x80
.set	macro
.set	reorder

lw	$4,%got($LC12)($28)
lw	$3,92($16)
addiu	$5,$4,%lo($LC12)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, %lo($LC12)($4)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$6,$L565
lw	$4,%got($LC16)($28)
.set	macro
.set	reorder

addiu	$5,$4,%lo($LC16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($5)  
lwr $12, %lo($LC16)($4)  

# 0 "" 2
#NO_APP
beq	$3,$12,$L566
$L334:
lw	$4,10188($16)
slt	$5,$4,500
.set	noreorder
.set	nomacro
bne	$5,$0,$L603
sltu	$3,$2,4
.set	macro
.set	reorder

lw	$3,10192($16)
slt	$3,$3,1814
.set	noreorder
.set	nomacro
beq	$3,$0,$L603
sltu	$3,$2,4
.set	macro
.set	reorder

ori	$6,$7,0x40
slt	$3,$4,503
.set	noreorder
.set	nomacro
bne	$3,$0,$L471
sw	$6,88($16)
.set	macro
.set	reorder

ori	$7,$7,0x140
sw	$7,88($16)
$L335:
sltu	$3,$2,4
$L603:
.set	noreorder
.set	nomacro
beq	$3,$0,$L336
sltu	$3,$2,13
.set	macro
.set	reorder

sltu	$3,$2,2
ori	$6,$7,0x40
li	$8,1073741824			# 0x40000000
movn	$7,$6,$3
sw	$8,10172($16)
$L337:
ori	$7,$7,0x400
$L339:
lw	$3,10216($16)
ori	$7,$7,0x1000
sltu	$6,$3,4653
.set	noreorder
.set	nomacro
beq	$6,$0,$L341
sw	$7,88($16)
.set	macro
.set	reorder

$L573:
ori	$7,$7,0x80
$L342:
ori	$7,$7,0x200
$L344:
ori	$7,$7,0x400
ori	$7,$7,0x1000
sw	$7,88($16)
$L347:
.set	noreorder
.set	nomacro
bltz	$4,$L332
ori	$3,$7,0x200
.set	macro
.set	reorder

li	$6,501			# 0x1f5
.set	noreorder
.set	nomacro
beq	$4,$6,$L567
sw	$3,88($16)
.set	macro
.set	reorder

ori	$7,$7,0x600
movn	$3,$7,$5
ori	$7,$3,0x800
$L605:
sw	$7,88($16)
$L332:
andi	$3,$7,0x80
$L602:
.set	noreorder
.set	nomacro
beq	$3,$0,$L350
lw	$3,%got(ff_put_qpel16_mc11_old_c)($28)
.set	macro
.set	reorder

sw	$3,3876($16)
lw	$3,%got(ff_put_no_rnd_qpel16_mc11_old_c)($28)
sw	$3,4132($16)
lw	$3,%got(ff_avg_qpel16_mc11_old_c)($28)
sw	$3,4004($16)
lw	$3,%got(ff_put_qpel16_mc31_old_c)($28)
sw	$3,3884($16)
lw	$3,%got(ff_put_no_rnd_qpel16_mc31_old_c)($28)
sw	$3,4140($16)
lw	$3,%got(ff_avg_qpel16_mc31_old_c)($28)
sw	$3,4012($16)
lw	$3,%got(ff_put_qpel16_mc12_old_c)($28)
sw	$3,3892($16)
lw	$3,%got(ff_put_no_rnd_qpel16_mc12_old_c)($28)
sw	$3,4148($16)
lw	$3,%got(ff_avg_qpel16_mc12_old_c)($28)
sw	$3,4020($16)
lw	$3,%got(ff_put_qpel16_mc32_old_c)($28)
sw	$3,3900($16)
lw	$3,%got(ff_put_no_rnd_qpel16_mc32_old_c)($28)
sw	$3,4156($16)
lw	$3,%got(ff_avg_qpel16_mc32_old_c)($28)
sw	$3,4028($16)
lw	$3,%got(ff_put_qpel16_mc13_old_c)($28)
sw	$3,3908($16)
lw	$3,%got(ff_put_no_rnd_qpel16_mc13_old_c)($28)
sw	$3,4164($16)
lw	$3,%got(ff_avg_qpel16_mc13_old_c)($28)
sw	$3,4036($16)
lw	$3,%got(ff_put_qpel16_mc33_old_c)($28)
sw	$3,3916($16)
lw	$3,%got(ff_put_no_rnd_qpel16_mc33_old_c)($28)
sw	$3,4172($16)
lw	$3,%got(ff_avg_qpel16_mc33_old_c)($28)
sw	$3,4044($16)
lw	$3,%got(ff_put_qpel8_mc11_old_c)($28)
sw	$3,3940($16)
lw	$3,%got(ff_put_no_rnd_qpel8_mc11_old_c)($28)
sw	$3,4196($16)
lw	$3,%got(ff_avg_qpel8_mc11_old_c)($28)
sw	$3,4068($16)
lw	$3,%got(ff_put_qpel8_mc31_old_c)($28)
sw	$3,3948($16)
lw	$3,%got(ff_put_no_rnd_qpel8_mc31_old_c)($28)
sw	$3,4204($16)
lw	$3,%got(ff_avg_qpel8_mc31_old_c)($28)
sw	$3,4076($16)
lw	$3,%got(ff_put_qpel8_mc12_old_c)($28)
sw	$3,3956($16)
lw	$3,%got(ff_put_no_rnd_qpel8_mc12_old_c)($28)
sw	$3,4212($16)
lw	$3,%got(ff_avg_qpel8_mc12_old_c)($28)
sw	$3,4084($16)
lw	$3,%got(ff_put_qpel8_mc32_old_c)($28)
sw	$3,3964($16)
lw	$3,%got(ff_put_no_rnd_qpel8_mc32_old_c)($28)
sw	$3,4220($16)
lw	$3,%got(ff_avg_qpel8_mc32_old_c)($28)
sw	$3,4092($16)
lw	$3,%got(ff_put_qpel8_mc13_old_c)($28)
sw	$3,3972($16)
lw	$3,%got(ff_put_no_rnd_qpel8_mc13_old_c)($28)
sw	$3,4228($16)
lw	$3,%got(ff_avg_qpel8_mc13_old_c)($28)
sw	$3,4100($16)
lw	$3,%got(ff_put_qpel8_mc33_old_c)($28)
sw	$3,3980($16)
lw	$3,%got(ff_put_no_rnd_qpel8_mc33_old_c)($28)
sw	$3,4236($16)
lw	$3,%got(ff_avg_qpel8_mc33_old_c)($28)
sw	$3,4108($16)
$L350:
lw	$3,404($17)
andi	$3,$3,0x1000
beq	$3,$0,$L351
lw	$3,10196($16)
lw	$4,0($16)
lw	$6,10216($16)
lw	$9,10188($16)
.set	noreorder
.set	nomacro
bne	$3,$0,$L472
lw	$8,10192($16)
.set	macro
.set	reorder

lw	$3,%got($LC9)($28)
addiu	$3,$3,%lo($LC9)
$L352:
lw	$25,%call16(av_log)($28)
li	$5,48			# 0x30
sw	$6,16($sp)
move	$6,$19
sw	$2,20($sp)
sw	$9,24($sp)
sw	$8,28($sp)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
sw	$3,32($sp)
.set	macro
.set	reorder

lw	$28,40($sp)
$L351:
lw	$4,8($16)
lw	$2,660($17)
.set	noreorder
.set	nomacro
beq	$4,$2,$L568
addiu	$fp,$16,9824
.set	macro
.set	reorder

$L604:
addiu	$23,$sp,48
addiu	$5,$16,9856
move	$2,$fp
move	$3,$23
$L355:
lw	$9,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$8,-12($2)
lw	$7,-8($2)
lw	$6,-4($2)
sw	$9,-16($3)
sw	$8,-12($3)
sw	$7,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$5,$L355
sw	$6,-4($3)
.set	macro
.set	reorder

lw	$5,0($2)
slt	$4,$4,1921
lw	$2,4($2)
sw	$5,0($3)
sw	$2,4($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L356
sw	$0,9824($16)
.set	macro
.set	reorder

lw	$2,12($16)
slt	$2,$2,1081
.set	noreorder
.set	nomacro
beq	$2,$0,$L601
lw	$31,132($sp)
.set	macro
.set	reorder

lw	$25,%call16(MPV_common_end)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_common_end
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$sp,80
lw	$28,40($sp)
move	$2,$23
move	$3,$fp
$L358:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L358
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
lw	$2,124($16)
$L588:
.set	noreorder
.set	nomacro
bne	$2,$0,$L359
lw	$25,%call16(avcodec_set_dimensions)($28)
.set	macro
.set	reorder

move	$4,$17
lw	$5,8($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,avcodec_set_dimensions
1:	jalr	$25
lw	$6,12($16)
.set	macro
.set	reorder

lw	$3,10204($16)
.set	noreorder
.set	nomacro
bne	$3,$0,$L569
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$2,88($sp)
$L308:
sll	$2,$2,3
$L596:
sra	$3,$2,3
.set	noreorder
.set	nomacro
bltz	$3,$L468
lw	$4,92($sp)
.set	macro
.set	reorder

bltz	$2,$L468
addu	$3,$4,$3
$L311:
sw	$2,10344($16)
sw	$4,10332($16)
sw	$3,10336($16)
sw	$0,10340($16)
sw	$0,10204($16)
lw	$2,124($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L315
lw	$25,%call16(MPV_common_init)($28)
.set	macro
.set	reorder

$L597:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_common_init
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L315
lw	$28,40($sp)
.set	macro
.set	reorder

$L356:
lw	$31,132($sp)
$L601:
li	$2,-1			# 0xffffffffffffffff
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

$L300:
lw	$3,52($16)
li	$4,13			# 0xd
.set	noreorder
.set	nomacro
beq	$3,$4,$L570
li	$4,5			# 0x5
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$3,$4,$L304
lw	$25,%call16(ff_h263_find_frame_end)($28)
.set	macro
.set	reorder

$L552:
addiu	$19,$16,9824
move	$5,$8
move	$4,$19
.set	noreorder
.set	nomacro
jalr	$25
move	$6,$2
.set	macro
.set	reorder

addiu	$6,$sp,92
lw	$28,40($sp)
addiu	$7,$sp,88
move	$4,$19
lw	$25,%call16(ff_combine_frame)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_combine_frame
1:	jalr	$25
move	$5,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L305
lw	$28,40($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L492
lw	$2,88($sp)
.set	macro
.set	reorder

$L320:
lw	$3,52($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L571
lw	$25,%call16(ff_intel_h263_decode_picture_header)($28)
.set	macro
.set	reorder

lw	$2,48($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L324
lw	$25,%call16(h263_decode_picture_header)($28)
.set	macro
.set	reorder

lw	$25,%call16(ff_flv_decode_picture_header)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_flv_decode_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L318
lw	$28,40($sp)
.set	macro
.set	reorder

$L468:
move	$3,$0
move	$2,$0
.set	noreorder
.set	nomacro
b	$L311
move	$4,$0
.set	macro
.set	reorder

$L564:
lw	$3,10188($16)
bne	$3,$2,$L331
lw	$3,10216($16)
beq	$3,$2,$L572
$L329:
lw	$4,10216($16)
li	$3,-1			# 0xffffffffffffffff
bne	$4,$3,$L331
lw	$3,92($16)
bne	$3,$22,$L331
lw	$3,10100($16)
bne	$3,$0,$L331
lw	$3,10104($16)
.set	noreorder
.set	nomacro
bne	$3,$0,$L331
li	$3,400			# 0x190
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L331
sw	$3,10188($16)
.set	macro
.set	reorder

$L568:
lw	$3,12($16)
lw	$2,664($17)
bne	$3,$2,$L604
.set	noreorder
.set	nomacro
b	$L588
lw	$2,124($16)
.set	macro
.set	reorder

$L563:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,msmpeg4_decode_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L318
lw	$28,40($sp)
.set	macro
.set	reorder

$L472:
lw	$3,%got($LC8)($28)
.set	noreorder
.set	nomacro
b	$L352
addiu	$3,$3,%lo($LC8)
.set	macro
.set	reorder

$L336:
.set	noreorder
.set	nomacro
bne	$3,$0,$L337
sltu	$3,$2,33
.set	macro
.set	reorder

bne	$3,$0,$L339
lw	$3,10216($16)
sltu	$6,$3,4653
bne	$6,$0,$L573
$L341:
sltu	$6,$3,4655
.set	noreorder
.set	nomacro
bne	$6,$0,$L342
sltu	$6,$3,4670
.set	macro
.set	reorder

bne	$6,$0,$L344
sltu	$3,$3,4713
beq	$3,$0,$L347
ori	$7,$7,0x1000
.set	noreorder
.set	nomacro
b	$L347
sw	$7,88($16)
.set	macro
.set	reorder

$L466:
move	$2,$0
move	$3,$0
.set	noreorder
.set	nomacro
b	$L309
move	$4,$0
.set	macro
.set	reorder

$L562:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_wmv2_decode_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L318
lw	$28,40($sp)
.set	macro
.set	reorder

$L565:
ori	$7,$7,0x4
sw	$7,88($16)
addiu	$5,$4,%lo($LC16)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($5)  
lwr $12, %lo($LC16)($4)  

# 0 "" 2
#NO_APP
bne	$3,$12,$L334
$L566:
ori	$7,$7,0x8
.set	noreorder
.set	nomacro
b	$L334
sw	$7,88($16)
.set	macro
.set	reorder

$L567:
li	$4,19988480			# 0x1310000
lw	$5,10192($16)
addiu	$4,$4,31936
.set	noreorder
.set	nomacro
bne	$5,$4,$L605
ori	$7,$3,0x800
.set	macro
.set	reorder

li	$4,1073741824			# 0x40000000
.set	noreorder
.set	nomacro
b	$L605
sw	$4,10172($16)
.set	macro
.set	reorder

$L324:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,h263_decode_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L318
lw	$28,40($sp)
.set	macro
.set	reorder

$L470:
move	$3,$0
move	$2,$0
.set	noreorder
.set	nomacro
b	$L322
move	$6,$0
.set	macro
.set	reorder

$L571:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_intel_h263_decode_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L318
lw	$28,40($sp)
.set	macro
.set	reorder

$L384:
lw	$2,44($16)
beq	$2,$0,$L392
lw	$2,10304($16)
slt	$2,$2,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L392
li	$2,1			# 0x1
.set	macro
.set	reorder

lw	$3,2904($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L574
lw	$25,%call16(msmpeg4_decode_ext_header)($28)
.set	macro
.set	reorder

$L392:
lw	$3,52($16)
li	$2,13			# 0xd
beq	$3,$2,$L575
$L385:
lw	$2,872($17)
$L594:
.set	noreorder
.set	nomacro
beq	$2,$0,$L606
lw	$25,%call16(MPV_frame_end)($28)
.set	macro
.set	reorder

lw	$25,32($2)
.set	noreorder
.set	nomacro
jalr	$25
move	$4,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L356
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$25,%call16(MPV_frame_end)($28)
$L606:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_frame_end
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$2,184($16)
andi	$2,$2,0xf
.set	noreorder
.set	nomacro
bne	$2,$0,$L401
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$2,180($16)
andi	$2,$2,0xf
.set	noreorder
.set	nomacro
beq	$2,$0,$L607
lw	$2,%got(dFRM)($28)
.set	macro
.set	reorder

$L401:
lw	$2,12($16)
andi	$3,$2,0xf
.set	noreorder
.set	nomacro
beq	$3,$0,$L554
slt	$4,$2,0
.set	macro
.set	reorder

addiu	$5,$2,15
lw	$3,8($16)
lw	$19,2088($16)
movz	$5,$2,$4
sra	$4,$5,4
li	$5,-2147483648			# 0xffffffff80000000
sll	$4,$4,4
addiu	$5,$5,15
and	$2,$2,$5
.set	noreorder
.set	nomacro
bltz	$2,$L576
mul	$4,$4,$3
.set	macro
.set	reorder

$L408:
lw	$5,160($16)
li	$23,16			# 0x10
.set	noreorder
.set	nomacro
blez	$5,$L406
subu	$23,$23,$2
.set	macro
.set	reorder

addiu	$fp,$2,-1
li	$3,17			# 0x11
sll	$fp,$fp,4
subu	$2,$3,$2
addiu	$23,$23,1
addu	$3,$fp,$4
sll	$2,$2,4
addu	$19,$19,$3
addu	$fp,$2,$fp
sll	$23,$23,4
move	$22,$0
$L410:
addiu	$17,$19,16
addu	$21,$23,$19
$L409:
lw	$25,%call16(memcpy)($28)
move	$4,$17
li	$6,16			# 0x10
addiu	$17,$17,16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$19
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$17,$21,$L409
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$5,160($16)
addiu	$22,$22,1
slt	$2,$22,$5
.set	noreorder
.set	nomacro
bne	$2,$0,$L410
addu	$19,$19,$fp
.set	macro
.set	reorder

lw	$2,12($16)
andi	$3,$2,0xf
.set	noreorder
.set	nomacro
beq	$3,$0,$L554
addiu	$6,$2,15
.set	macro
.set	reorder

lw	$3,8($16)
slt	$4,$2,0
lw	$19,2092($16)
movz	$6,$2,$4
sra	$4,$6,4
li	$6,-2147483648			# 0xffffffff80000000
sll	$4,$4,4
addiu	$6,$6,15
mul	$4,$3,$4
and	$2,$2,$6
.set	noreorder
.set	nomacro
bltz	$2,$L577
sra	$4,$4,1
.set	macro
.set	reorder

$L413:
srl	$6,$2,31
li	$23,8			# 0x8
addu	$2,$6,$2
sra	$2,$2,1
.set	noreorder
.set	nomacro
blez	$5,$L406
subu	$23,$23,$2
.set	macro
.set	reorder

addiu	$fp,$2,-1
li	$3,9			# 0x9
sll	$fp,$fp,4
subu	$3,$3,$2
addiu	$23,$23,1
addu	$2,$fp,$4
sll	$3,$3,4
addu	$19,$19,$2
addu	$fp,$3,$fp
sll	$23,$23,4
move	$22,$0
$L415:
addiu	$17,$19,16
addu	$21,$23,$19
$L414:
lw	$25,%call16(memcpy)($28)
move	$4,$17
li	$6,16			# 0x10
addiu	$17,$17,16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
move	$5,$19
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$17,$21,$L414
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$2,160($16)
addiu	$22,$22,1
slt	$2,$22,$2
.set	noreorder
.set	nomacro
bne	$2,$0,$L415
addu	$19,$19,$fp
.set	macro
.set	reorder

$L554:
lw	$3,8($16)
$L406:
andi	$2,$3,0xf
.set	noreorder
.set	nomacro
beq	$2,$0,$L607
lw	$2,%got(dFRM)($28)
.set	macro
.set	reorder

addiu	$5,$3,15
lw	$2,2088($16)
slt	$4,$3,0
movz	$5,$3,$4
sra	$4,$5,4
li	$5,-2147483648			# 0xffffffff80000000
sll	$4,$4,8
addiu	$5,$5,15
and	$3,$3,$5
.set	noreorder
.set	nomacro
bltz	$3,$L578
addu	$4,$2,$4
.set	macro
.set	reorder

$L418:
lw	$2,164($16)
li	$7,16			# 0x10
.set	noreorder
.set	nomacro
blez	$2,$L416
subu	$7,$7,$3
.set	macro
.set	reorder

addiu	$8,$3,-1
move	$9,$0
$L423:
addu	$4,$4,$8
addiu	$2,$4,1
addiu	$6,$4,256
addu	$5,$2,$7
$L420:
lbu	$3,0($4)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$5,$L420
sb	$3,-1($2)
.set	macro
.set	reorder

addiu	$4,$4,16
.set	noreorder
.set	nomacro
bne	$4,$6,$L420
addu	$5,$2,$7
.set	macro
.set	reorder

lw	$5,8($16)
addiu	$9,$9,1
lw	$4,160($16)
lw	$6,164($16)
addiu	$10,$5,15
lw	$2,2088($16)
slt	$3,$5,0
movz	$10,$5,$3
sll	$4,$4,8
mul	$12,$4,$9
move	$3,$10
sra	$3,$3,4
slt	$10,$9,$6
sll	$11,$3,8
addu	$4,$12,$11
.set	noreorder
.set	nomacro
bne	$10,$0,$L423
addu	$4,$2,$4
.set	macro
.set	reorder

andi	$2,$5,0xf
.set	noreorder
.set	nomacro
beq	$2,$0,$L607
lw	$2,%got(dFRM)($28)
.set	macro
.set	reorder

lw	$2,2092($16)
sll	$7,$3,7
addu	$7,$2,$7
li	$2,-2147483648			# 0xffffffff80000000
addiu	$2,$2,15
and	$5,$5,$2
.set	noreorder
.set	nomacro
bltz	$5,$L579
li	$2,-16			# 0xfffffffffffffff0
.set	macro
.set	reorder

$L424:
srl	$10,$5,31
li	$8,8			# 0x8
addu	$5,$10,$5
sra	$10,$5,1
.set	noreorder
.set	nomacro
blez	$6,$L416
subu	$8,$8,$10
.set	macro
.set	reorder

addiu	$10,$10,-1
move	$9,$0
addiu	$11,$8,9
$L430:
addu	$7,$7,$10
addiu	$2,$7,1
addu	$6,$7,$11
addiu	$4,$7,9
addiu	$7,$7,137
$L425:
addu	$5,$2,$8
$L426:
lbu	$3,-9($4)
addiu	$2,$2,1
.set	noreorder
.set	nomacro
bne	$2,$5,$L426
sb	$3,-1($2)
.set	macro
.set	reorder

move	$2,$6
move	$3,$4
$L427:
lbu	$5,-1($4)
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$3,$6,$L427
sb	$5,-1($3)
.set	macro
.set	reorder

addiu	$4,$4,16
.set	noreorder
.set	nomacro
bne	$4,$7,$L425
addiu	$6,$3,16
.set	macro
.set	reorder

lw	$3,8($16)
addiu	$9,$9,1
lw	$7,160($16)
lw	$4,164($16)
addiu	$5,$3,15
lw	$2,2092($16)
sll	$7,$7,7
slt	$6,$3,0
movn	$3,$5,$6
mul	$5,$7,$9
slt	$4,$9,$4
sra	$3,$3,4
sll	$3,$3,7
addu	$7,$5,$3
.set	noreorder
.set	nomacro
bne	$4,$0,$L430
addu	$7,$2,$7
.set	macro
.set	reorder

$L416:
lw	$2,%got(dFRM)($28)
$L607:
lbu	$2,42($2)
.set	noreorder
.set	nomacro
bne	$2,$0,$L580
li	$6,2			# 0x2
.set	macro
.set	reorder

$L404:
lw	$3,2904($16)
li	$2,3			# 0x3
beq	$3,$2,$L431
lw	$2,10096($16)
bne	$2,$0,$L431
lw	$2,2688($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L450
addiu	$4,$2,208
.set	macro
.set	reorder

move	$3,$18
$L451:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L451
sw	$5,-4($3)
.set	macro
.set	reorder

lw	$4,0($2)
$L593:
lw	$2,4($2)
sw	$4,0($3)
sw	$2,4($3)
lw	$2,2688($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L452
li	$2,216			# 0xd8
.set	macro
.set	reorder

$L613:
lw	$25,%call16(ff_print_debug_info)($28)
move	$4,$16
move	$5,$18
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_print_debug_info
1:	jalr	$25
sw	$2,0($20)
.set	macro
.set	reorder

lw	$28,40($sp)
$L450:
lw	$25,%got(get_consumed_bytes)($28)
$L589:
move	$4,$16
$L615:
addiu	$25,$25,%lo(get_consumed_bytes)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,get_consumed_bytes
1:	jalr	$25
lw	$5,88($sp)
.set	macro
.set	reorder

lw	$31,132($sp)
lw	$fp,128($sp)
lw	$23,124($sp)
lw	$22,120($sp)
lw	$21,116($sp)
lw	$20,112($sp)
lw	$19,108($sp)
lw	$18,104($sp)
lw	$17,100($sp)
lw	$16,96($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,136
.set	macro
.set	reorder

$L572:
lw	$3,96($16)
beq	$3,$21,$L330
lw	$3,92($16)
.set	noreorder
.set	nomacro
beq	$3,$21,$L330
lw	$4,%got($LC12)($28)
.set	macro
.set	reorder

addiu	$5,$4,%lo($LC12)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $6, 3($5)  
lwr $6, %lo($LC12)($4)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$6,$L330
lw	$4,%got($LC13)($28)
.set	macro
.set	reorder

addiu	$5,$4,%lo($LC13)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $12, 3($5)  
lwr $12, %lo($LC13)($4)  

# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
beq	$3,$12,$L330
lw	$4,%got($LC14)($28)
.set	macro
.set	reorder

addiu	$5,$4,%lo($LC14)
#APP
# 31 "../libavutil/mips/intreadwrite.h" 1
lwl $25, 3($5)  
lwr $25, %lo($LC14)($4)  

# 0 "" 2
#NO_APP
bne	$3,$25,$L329
$L330:
sw	$0,10212($16)
.set	noreorder
.set	nomacro
b	$L331
move	$2,$0
.set	macro
.set	reorder

$L359:
lw	$2,52($16)
li	$3,5			# 0x5
.set	noreorder
.set	nomacro
beq	$2,$3,$L360
addiu	$2,$2,-20
.set	macro
.set	reorder

sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L608
lw	$25,%call16(ff_h263_get_gob_height)($28)
.set	macro
.set	reorder

$L361:
lw	$2,2904($16)
lw	$4,2688($16)
xori	$3,$2,0x1
sltu	$3,$3,1
sw	$2,2140($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L581
sw	$3,2136($16)
.set	macro
.set	reorder

lw	$4,128($17)
.set	noreorder
.set	nomacro
bne	$4,$0,$L582
li	$3,3			# 0x3
.set	macro
.set	reorder

lw	$3,708($17)
slt	$5,$3,8
.set	noreorder
.set	nomacro
beq	$5,$0,$L460
li	$5,3			# 0x3
.set	macro
.set	reorder

$L370:
lw	$3,9812($16)
$L590:
.set	noreorder
.set	nomacro
beq	$3,$0,$L371
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L589
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

lw	$2,7984($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L373
sw	$0,9812($16)
.set	macro
.set	reorder

$L374:
addiu	$3,$16,4112
addiu	$2,$16,3984
li	$4,2			# 0x2
$L553:
lw	$25,%got(mpeg4_motion_set.constprop.4)($28)
sw	$3,7964($16)
addiu	$25,$25,%lo(mpeg4_motion_set.constprop.4)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,mpeg4_motion_set.constprop.4
1:	jalr	$25
sw	$2,7968($16)
.set	macro
.set	reorder

lw	$28,40($sp)
$L377:
lw	$25,%call16(MPV_frame_start)($28)
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,MPV_frame_start
1:	jalr	$25
move	$5,$17
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L356
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$2,872($17)
.set	noreorder
.set	nomacro
beq	$2,$0,$L382
move	$4,$17
.set	macro
.set	reorder

lw	$6,10336($16)
lw	$5,10332($16)
lw	$25,24($2)
.set	noreorder
.set	nomacro
jalr	$25
subu	$6,$6,$5
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L356
lw	$28,40($sp)
.set	macro
.set	reorder

$L382:
lw	$25,%call16(ff_er_frame_start)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_er_frame_start
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

li	$2,5			# 0x5
lw	$3,10304($16)
.set	noreorder
.set	nomacro
beq	$3,$2,$L583
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$19,%got(decode_slice)($28)
$L612:
move	$4,$16
sw	$0,7992($16)
addiu	$19,$19,%lo(decode_slice)
move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_slice
1:	jalr	$25
sw	$0,7996($16)
.set	macro
.set	reorder

lw	$4,7996($16)
lw	$2,164($16)
slt	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L384
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$21,%got($LC18)($28)
.set	noreorder
.set	nomacro
b	$L508
addiu	$21,$21,%lo($LC18)
.set	macro
.set	reorder

$L585:
lw	$3,10292($16)
beq	$3,$0,$L384
lw	$5,7992($16)
bne	$5,$0,$L384
teq	$3,$0,7
div	$0,$4,$3
mfhi	$4
bne	$4,$0,$L384
lw	$3,10340($16)
lw	$4,10344($16)
slt	$3,$4,$3
bne	$3,$0,$L384
$L389:
slt	$2,$2,4
.set	noreorder
.set	nomacro
beq	$2,$0,$L609
lw	$25,%call16(printf)($28)
.set	macro
.set	reorder

lw	$2,32($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L584
lw	$25,%call16(ff_mpeg4_clean_buffers)($28)
.set	macro
.set	reorder

$L390:
lw	$25,%call16(printf)($28)
$L609:
move	$4,$21
lw	$5,7992($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,printf
1:	jalr	$25
lw	$6,7996($16)
.set	macro
.set	reorder

move	$25,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,decode_slice
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$4,7996($16)
lw	$2,164($16)
slt	$2,$4,$2
.set	noreorder
.set	nomacro
beq	$2,$0,$L384
lw	$28,40($sp)
.set	macro
.set	reorder

$L508:
lw	$2,10304($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L585
lw	$25,%call16(ff_h263_resync)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h263_resync
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L384
lw	$28,40($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L389
lw	$2,10304($16)
.set	macro
.set	reorder

$L582:
.set	noreorder
.set	nomacro
beq	$2,$3,$L589
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

lw	$3,708($17)
slt	$5,$3,8
bne	$5,$0,$L369
$L459:
slt	$5,$3,32
.set	noreorder
.set	nomacro
bne	$5,$0,$L369
li	$5,1			# 0x1
.set	macro
.set	reorder

$L610:
.set	noreorder
.set	nomacro
bne	$2,$5,$L589
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

slt	$3,$3,48
beq	$3,$0,$L589
$L369:
slt	$4,$4,5
$L592:
.set	noreorder
.set	nomacro
bne	$4,$0,$L370
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L615
move	$4,$16
.set	macro
.set	reorder

$L304:
lw	$6,%got($LC10)($28)
li	$5,16			# 0x10
lw	$25,%call16(av_log)($28)
lw	$4,0($16)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_log
1:	jalr	$25
addiu	$6,$6,%lo($LC10)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L492
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

$L360:
lw	$25,%call16(ff_h263_get_gob_height)($28)
$L608:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_h263_get_gob_height
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

lw	$28,40($sp)
.set	noreorder
.set	nomacro
b	$L361
sw	$2,9864($16)
.set	macro
.set	reorder

$L570:
.set	noreorder
.set	nomacro
b	$L552
lw	$25,%call16(ff_mpeg4_find_frame_end)($28)
.set	macro
.set	reorder

$L460:
.set	noreorder
.set	nomacro
beq	$2,$5,$L450
slt	$5,$3,32
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$5,$0,$L610
li	$5,1			# 0x1
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L590
lw	$3,9812($16)
.set	macro
.set	reorder

$L584:
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_mpeg4_clean_buffers
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L390
lw	$28,40($sp)
.set	macro
.set	reorder

$L371:
lw	$3,0($16)
lw	$3,596($3)
andi	$3,$3,0x1
.set	noreorder
.set	nomacro
beq	$3,$0,$L375
li	$3,3			# 0x3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
beq	$2,$3,$L586
addiu	$3,$16,4960
.set	macro
.set	reorder

lw	$2,7984($16)
bne	$2,$0,$L374
$L373:
addiu	$3,$16,3856
$L591:
addiu	$2,$16,3984
$L616:
.set	noreorder
.set	nomacro
b	$L553
li	$4,1			# 0x1
.set	macro
.set	reorder

$L375:
lw	$3,7984($16)
.set	noreorder
.set	nomacro
beq	$3,$0,$L591
addiu	$3,$16,3856
.set	macro
.set	reorder

li	$3,3			# 0x3
.set	noreorder
.set	nomacro
bne	$2,$3,$L374
addiu	$3,$16,3856
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L616
addiu	$2,$16,3984
.set	macro
.set	reorder

$L581:
li	$3,3			# 0x3
.set	noreorder
.set	nomacro
beq	$2,$3,$L589
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

lw	$3,2916($16)
bne	$3,$0,$L589
lw	$3,708($17)
slt	$5,$3,8
.set	noreorder
.set	nomacro
beq	$5,$0,$L459
lw	$4,128($17)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L592
slt	$4,$4,5
.set	macro
.set	reorder

$L431:
lw	$2,2696($16)
move	$3,$18
addiu	$4,$2,208
$L448:
lw	$8,0($2)
addiu	$2,$2,16
addiu	$3,$3,16
lw	$7,-12($2)
lw	$6,-8($2)
lw	$5,-4($2)
sw	$8,-16($3)
sw	$7,-12($3)
sw	$6,-8($3)
.set	noreorder
.set	nomacro
bne	$2,$4,$L448
sw	$5,-4($3)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L593
lw	$4,0($2)
.set	macro
.set	reorder

$L580:
lw	$3,192($16)
lw	$4,2092($16)
li	$9,1			# 0x1
lw	$5,2088($16)
subu	$2,$0,$3
subu	$3,$4,$3
sll	$2,$2,1
addiu	$3,$3,-272
addiu	$2,$2,-544
move	$8,$5
addu	$2,$5,$2
move	$7,$4
$L439:
#APP
# 1950 "mpeg4.c" 1
.word	0b01110001000111000000000101100010	#S8LDD XR5,$8,0,PTN7
# 0 "" 2
# 1951 "mpeg4.c" 1
.word	0b01110000111111000000000110100010	#S8LDD XR6,$7,0,PTN7
# 0 "" 2
# 1952 "mpeg4.c" 1
.word	0b01110000111111000010000111100010	#S8LDD XR7,$7,8,PTN7
# 0 "" 2
# 1954 "mpeg4.c" 1
.word	0b01110001000000000000001000010000	#S32LDD XR8,$8,0
# 0 "" 2
# 1955 "mpeg4.c" 1
.word	0b01110001000000000000011001010000	#S32LDD XR9,$8,4
# 0 "" 2
# 1956 "mpeg4.c" 1
.word	0b01110001000000000000101010010000	#S32LDD XR10,$8,8
# 0 "" 2
# 1957 "mpeg4.c" 1
.word	0b01110001000000000000111011010000	#S32LDD XR11,$8,12
# 0 "" 2
# 1958 "mpeg4.c" 1
.word	0b01110000111000000000001100010000	#S32LDD XR12,$7,0
# 0 "" 2
# 1959 "mpeg4.c" 1
.word	0b01110000111000000000011101010000	#S32LDD XR13,$7,4
# 0 "" 2
# 1960 "mpeg4.c" 1
.word	0b01110000111000000000101110010000	#S32LDD XR14,$7,8
# 0 "" 2
# 1961 "mpeg4.c" 1
.word	0b01110000111000000000111111010000	#S32LDD XR15,$7,12
# 0 "" 2
#NO_APP
li	$10,2			# 0x2
$L433:
li	$7,16			# 0x10
$L434:
#APP
# 1964 "mpeg4.c" 1
.word	0b01110000010000000010000101010101	#S32SDI XR5,$2,32
# 0 "" 2
# 1965 "mpeg4.c" 1
.word	0b01110000010000000000010101010001	#S32STD XR5,$2,4
# 0 "" 2
# 1966 "mpeg4.c" 1
.word	0b01110000010000000000100101010001	#S32STD XR5,$2,8
# 0 "" 2
# 1967 "mpeg4.c" 1
.word	0b01110000010000000000110101010001	#S32STD XR5,$2,12
# 0 "" 2
# 1968 "mpeg4.c" 1
.word	0b01110000010000000001000101010001	#S32STD XR5,$2,16
# 0 "" 2
# 1969 "mpeg4.c" 1
.word	0b01110000010000000001010101010001	#S32STD XR5,$2,20
# 0 "" 2
# 1970 "mpeg4.c" 1
.word	0b01110000010000000001100101010001	#S32STD XR5,$2,24
# 0 "" 2
# 1971 "mpeg4.c" 1
.word	0b01110000010000000001110101010001	#S32STD XR5,$2,28
# 0 "" 2
#NO_APP
addiu	$7,$7,-1
.set	noreorder
.set	nomacro
bne	$7,$0,$L434
li	$8,8			# 0x8
.set	macro
.set	reorder

$L435:
move	$7,$2
#APP
# 1974 "mpeg4.c" 1
.word	0b01110000111000000010001000010101	#S32SDI XR8,$7,32
# 0 "" 2
#NO_APP
move	$2,$7
#APP
# 1975 "mpeg4.c" 1
.word	0b01110000111000000000011001010001	#S32STD XR9,$7,4
# 0 "" 2
# 1976 "mpeg4.c" 1
.word	0b01110000111000000000101010010001	#S32STD XR10,$7,8
# 0 "" 2
# 1977 "mpeg4.c" 1
.word	0b01110000111000000000111011010001	#S32STD XR11,$7,12
# 0 "" 2
# 1978 "mpeg4.c" 1
.word	0b01110000111000000001001000010001	#S32STD XR8,$7,16
# 0 "" 2
# 1979 "mpeg4.c" 1
.word	0b01110000111000000001011001010001	#S32STD XR9,$7,20
# 0 "" 2
# 1980 "mpeg4.c" 1
.word	0b01110000111000000001101010010001	#S32STD XR10,$7,24
# 0 "" 2
# 1981 "mpeg4.c" 1
.word	0b01110000111000000001111011010001	#S32STD XR11,$7,28
# 0 "" 2
#NO_APP
addiu	$8,$8,-1
bne	$8,$0,$L435
li	$2,16			# 0x10
$L436:
#APP
# 1984 "mpeg4.c" 1
.word	0b01110000011000000001000110010101	#S32SDI XR6,$3,16
# 0 "" 2
# 1985 "mpeg4.c" 1
.word	0b01110000011000000000010110010001	#S32STD XR6,$3,4
# 0 "" 2
# 1986 "mpeg4.c" 1
.word	0b01110000011000000000100111010001	#S32STD XR7,$3,8
# 0 "" 2
# 1987 "mpeg4.c" 1
.word	0b01110000011000000000110111010001	#S32STD XR7,$3,12
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$0,$L436
li	$2,8			# 0x8
$L437:
#APP
# 1990 "mpeg4.c" 1
.word	0b01110000011000000001001100010101	#S32SDI XR12,$3,16
# 0 "" 2
# 1991 "mpeg4.c" 1
.word	0b01110000011000000000011101010001	#S32STD XR13,$3,4
# 0 "" 2
# 1992 "mpeg4.c" 1
.word	0b01110000011000000000101110010001	#S32STD XR14,$3,8
# 0 "" 2
# 1993 "mpeg4.c" 1
.word	0b01110000011000000000111111010001	#S32STD XR15,$3,12
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$0,$L437
lw	$11,192($16)
lw	$8,196($16)
addiu	$2,$11,-768
addiu	$12,$8,-384
addu	$2,$7,$2
.set	noreorder
.set	nomacro
beq	$10,$9,$L438
addu	$3,$3,$12
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L433
li	$10,1			# 0x1
.set	macro
.set	reorder

$L438:
lw	$3,164($16)
addiu	$3,$3,-1
mul	$7,$3,$11
mul	$3,$3,$8
addu	$2,$7,$11
addu	$12,$3,$8
addiu	$2,$2,-544
addiu	$8,$7,240
addiu	$7,$3,112
addiu	$3,$12,-272
addu	$8,$5,$8
addu	$7,$4,$7
addu	$2,$5,$2
.set	noreorder
.set	nomacro
bne	$6,$10,$L475
addu	$3,$4,$3
.set	macro
.set	reorder

lw	$3,160($16)
sll	$7,$11,1
li	$9,2			# 0x2
li	$8,1			# 0x1
addiu	$3,$3,-1
sll	$10,$3,7
sll	$12,$3,8
addiu	$2,$10,-16
addiu	$3,$12,-32
subu	$2,$2,$11
subu	$7,$3,$7
addu	$11,$5,$12
addu	$7,$5,$7
addu	$3,$4,$10
addu	$2,$4,$2
$L446:
#APP
# 2008 "mpeg4.c" 1
.word	0b01110001011111000011110101100010	#S8LDD XR5,$11,15,PTN7
# 0 "" 2
# 2009 "mpeg4.c" 1
.word	0b01110000011111000001110110100010	#S8LDD XR6,$3,7,PTN7
# 0 "" 2
# 2010 "mpeg4.c" 1
.word	0b01110000011111000011110111100010	#S8LDD XR7,$3,15,PTN7
# 0 "" 2
# 2012 "mpeg4.c" 1
.word	0b01110001011000000000001000010000	#S32LDD XR8,$11,0
# 0 "" 2
# 2013 "mpeg4.c" 1
.word	0b01110001011000000000011001010000	#S32LDD XR9,$11,4
# 0 "" 2
# 2014 "mpeg4.c" 1
.word	0b01110001011000000000101010010000	#S32LDD XR10,$11,8
# 0 "" 2
# 2015 "mpeg4.c" 1
.word	0b01110001011000000000111011010000	#S32LDD XR11,$11,12
# 0 "" 2
# 2016 "mpeg4.c" 1
.word	0b01110000011000000000001100010000	#S32LDD XR12,$3,0
# 0 "" 2
# 2017 "mpeg4.c" 1
.word	0b01110000011000000000011101010000	#S32LDD XR13,$3,4
# 0 "" 2
# 2018 "mpeg4.c" 1
.word	0b01110000011000000000101110010000	#S32LDD XR14,$3,8
# 0 "" 2
# 2019 "mpeg4.c" 1
.word	0b01110000011000000000111111010000	#S32LDD XR15,$3,12
# 0 "" 2
#NO_APP
li	$11,2			# 0x2
$L440:
li	$3,8			# 0x8
$L441:
#APP
# 2022 "mpeg4.c" 1
.word	0b01110000111000000010001000010101	#S32SDI XR8,$7,32
# 0 "" 2
# 2023 "mpeg4.c" 1
.word	0b01110000111000000000011001010001	#S32STD XR9,$7,4
# 0 "" 2
# 2024 "mpeg4.c" 1
.word	0b01110000111000000000101010010001	#S32STD XR10,$7,8
# 0 "" 2
# 2025 "mpeg4.c" 1
.word	0b01110000111000000000111011010001	#S32STD XR11,$7,12
# 0 "" 2
# 2026 "mpeg4.c" 1
.word	0b01110000111000000001001000010001	#S32STD XR8,$7,16
# 0 "" 2
# 2027 "mpeg4.c" 1
.word	0b01110000111000000001011001010001	#S32STD XR9,$7,20
# 0 "" 2
# 2028 "mpeg4.c" 1
.word	0b01110000111000000001101010010001	#S32STD XR10,$7,24
# 0 "" 2
# 2029 "mpeg4.c" 1
.word	0b01110000111000000001111011010001	#S32STD XR11,$7,28
# 0 "" 2
#NO_APP
addiu	$3,$3,-1
.set	noreorder
.set	nomacro
bne	$3,$0,$L441
li	$10,16			# 0x10
.set	macro
.set	reorder

$L442:
move	$3,$7
#APP
# 2032 "mpeg4.c" 1
.word	0b01110000011000000010000101010101	#S32SDI XR5,$3,32
# 0 "" 2
#NO_APP
move	$7,$3
#APP
# 2033 "mpeg4.c" 1
.word	0b01110000011000000000010101010001	#S32STD XR5,$3,4
# 0 "" 2
# 2034 "mpeg4.c" 1
.word	0b01110000011000000000100101010001	#S32STD XR5,$3,8
# 0 "" 2
# 2035 "mpeg4.c" 1
.word	0b01110000011000000000110101010001	#S32STD XR5,$3,12
# 0 "" 2
# 2036 "mpeg4.c" 1
.word	0b01110000011000000001000101010001	#S32STD XR5,$3,16
# 0 "" 2
# 2037 "mpeg4.c" 1
.word	0b01110000011000000001010101010001	#S32STD XR5,$3,20
# 0 "" 2
# 2038 "mpeg4.c" 1
.word	0b01110000011000000001100101010001	#S32STD XR5,$3,24
# 0 "" 2
# 2039 "mpeg4.c" 1
.word	0b01110000011000000001110101010001	#S32STD XR5,$3,28
# 0 "" 2
#NO_APP
addiu	$10,$10,-1
bne	$10,$0,$L442
li	$7,8			# 0x8
$L443:
#APP
# 2042 "mpeg4.c" 1
.word	0b01110000010000000001001100010101	#S32SDI XR12,$2,16
# 0 "" 2
# 2043 "mpeg4.c" 1
.word	0b01110000010000000000011101010001	#S32STD XR13,$2,4
# 0 "" 2
# 2044 "mpeg4.c" 1
.word	0b01110000010000000000101110010001	#S32STD XR14,$2,8
# 0 "" 2
# 2045 "mpeg4.c" 1
.word	0b01110000010000000000111111010001	#S32STD XR15,$2,12
# 0 "" 2
#NO_APP
addiu	$7,$7,-1
bne	$7,$0,$L443
li	$7,16			# 0x10
$L444:
#APP
# 2048 "mpeg4.c" 1
.word	0b01110000010000000001000110010101	#S32SDI XR6,$2,16
# 0 "" 2
# 2049 "mpeg4.c" 1
.word	0b01110000010000000000010110010001	#S32STD XR6,$2,4
# 0 "" 2
# 2050 "mpeg4.c" 1
.word	0b01110000010000000000100111010001	#S32STD XR7,$2,8
# 0 "" 2
# 2051 "mpeg4.c" 1
.word	0b01110000010000000000110111010001	#S32STD XR7,$2,12
# 0 "" 2
#NO_APP
addiu	$7,$7,-1
bne	$7,$0,$L444
lw	$14,192($16)
lw	$13,196($16)
addiu	$7,$14,-768
addiu	$10,$13,-384
addu	$7,$3,$7
.set	noreorder
.set	nomacro
beq	$11,$8,$L445
addu	$2,$2,$10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L440
move	$11,$6
.set	macro
.set	reorder

$L445:
lw	$10,164($16)
lw	$2,160($16)
addiu	$10,$10,-1
addiu	$2,$2,-1
mul	$15,$10,$14
mul	$12,$10,$13
sll	$3,$2,8
sll	$2,$2,7
addiu	$11,$3,240
addiu	$10,$2,112
addiu	$3,$3,-32
addu	$7,$15,$14
addiu	$2,$2,-16
addu	$13,$12,$13
addu	$7,$3,$7
addu	$11,$11,$15
addu	$10,$10,$12
addu	$2,$2,$13
addu	$11,$5,$11
addu	$3,$4,$10
addu	$7,$5,$7
.set	noreorder
.set	nomacro
beq	$9,$8,$L404
addu	$2,$4,$2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L446
li	$9,1			# 0x1
.set	macro
.set	reorder

$L475:
.set	noreorder
.set	nomacro
b	$L439
li	$6,1			# 0x1
.set	macro
.set	reorder

$L575:
lw	$2,10196($16)
.set	noreorder
.set	nomacro
beq	$2,$0,$L385
lw	$3,88($sp)
.set	macro
.set	reorder

lw	$19,10340($16)
sra	$19,$19,3
subu	$6,$3,$19
slt	$2,$6,6
.set	noreorder
.set	nomacro
bne	$2,$0,$L394
addiu	$5,$3,-3
.set	macro
.set	reorder

slt	$2,$19,$5
.set	noreorder
.set	nomacro
beq	$2,$0,$L394
lw	$4,92($sp)
.set	macro
.set	reorder

li	$8,1			# 0x1
li	$7,182			# 0xb6
addu	$2,$4,$19
.set	noreorder
.set	nomacro
b	$L397
addu	$4,$4,$5
.set	macro
.set	reorder

$L395:
addiu	$2,$2,1
$L611:
beq	$2,$4,$L394
$L397:
lbu	$5,0($2)
bne	$5,$0,$L395
lbu	$5,1($2)
bne	$5,$0,$L395
lbu	$5,2($2)
bne	$5,$8,$L395
lbu	$5,3($2)
.set	noreorder
.set	nomacro
bne	$5,$7,$L611
addiu	$2,$2,1
.set	macro
.set	reorder

lw	$4,10332($16)
lw	$2,10200($16)
.set	noreorder
.set	nomacro
beq	$4,$2,$L587
slt	$2,$3,8
.set	macro
.set	reorder

$L398:
lw	$25,%call16(av_fast_malloc)($28)
$L614:
addiu	$4,$16,10200
addiu	$5,$16,10208
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,av_fast_malloc
1:	jalr	$25
addiu	$6,$6,8
.set	macro
.set	reorder

lw	$4,10200($16)
.set	noreorder
.set	nomacro
beq	$4,$0,$L474
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$5,92($sp)
lw	$6,88($sp)
lw	$25,%call16(memcpy)($28)
addu	$5,$5,$19
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,memcpy
1:	jalr	$25
subu	$6,$6,$19
.set	macro
.set	reorder

lw	$2,88($sp)
lw	$28,40($sp)
subu	$19,$2,$19
.set	noreorder
.set	nomacro
b	$L385
sw	$19,10204($16)
.set	macro
.set	reorder

$L583:
lw	$25,%call16(ff_wmv2_decode_secondary_picture_header)($28)
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,ff_wmv2_decode_secondary_picture_header
1:	jalr	$25
move	$4,$16
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bltz	$2,$L492
lw	$28,40($sp)
.set	macro
.set	reorder

li	$3,1			# 0x1
.set	noreorder
.set	nomacro
bne	$2,$3,$L612
lw	$19,%got(decode_slice)($28)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L594
lw	$2,872($17)
.set	macro
.set	reorder

$L452:
lw	$2,10096($16)
.set	noreorder
.set	nomacro
bne	$2,$0,$L613
li	$2,216			# 0xd8
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L589
lw	$25,%got(get_consumed_bytes)($28)
.set	macro
.set	reorder

$L394:
lw	$4,10332($16)
lw	$2,10200($16)
.set	noreorder
.set	nomacro
bne	$4,$2,$L385
slt	$2,$3,8
.set	macro
.set	reorder

bne	$2,$0,$L385
lw	$2,10212($16)
.set	noreorder
.set	nomacro
bltz	$2,$L385
move	$19,$0
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
b	$L398
move	$6,$3
.set	macro
.set	reorder

$L586:
addiu	$2,$16,5216
sw	$3,7964($16)
.set	noreorder
.set	nomacro
b	$L377
sw	$2,7968($16)
.set	macro
.set	reorder

$L579:
addiu	$5,$5,-1
or	$5,$5,$2
.set	noreorder
.set	nomacro
b	$L424
addiu	$5,$5,1
.set	macro
.set	reorder

$L578:
addiu	$3,$3,-1
li	$2,-16			# 0xfffffffffffffff0
or	$3,$3,$2
.set	noreorder
.set	nomacro
b	$L418
addiu	$3,$3,1
.set	macro
.set	reorder

$L576:
addiu	$2,$2,-1
li	$5,-16			# 0xfffffffffffffff0
or	$2,$2,$5
.set	noreorder
.set	nomacro
b	$L408
addiu	$2,$2,1
.set	macro
.set	reorder

$L577:
addiu	$2,$2,-1
li	$6,-16			# 0xfffffffffffffff0
or	$2,$2,$6
.set	noreorder
.set	nomacro
b	$L413
addiu	$2,$2,1
.set	macro
.set	reorder

$L574:
move	$4,$16
.set	noreorder
.set	nomacro
.reloc	1f,R_MIPS_JALR,msmpeg4_decode_ext_header
1:	jalr	$25
lw	$5,88($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bgez	$2,$L392
lw	$28,40($sp)
.set	macro
.set	reorder

lw	$3,188($16)
lw	$2,9780($16)
addu	$2,$2,$3
li	$3,14			# 0xe
.set	noreorder
.set	nomacro
b	$L392
sb	$3,-1($2)
.set	macro
.set	reorder

$L471:
.set	noreorder
.set	nomacro
b	$L335
move	$7,$6
.set	macro
.set	reorder

$L474:
.set	noreorder
.set	nomacro
b	$L492
li	$2,-12			# 0xfffffffffffffff4
.set	macro
.set	reorder

$L587:
.set	noreorder
.set	nomacro
bne	$2,$0,$L614
lw	$25,%call16(av_fast_malloc)($28)
.set	macro
.set	reorder

lw	$2,10212($16)
slt	$2,$2,0
movz	$6,$3,$2
.set	noreorder
.set	nomacro
b	$L614
movz	$19,$0,$2
.set	macro
.set	reorder

.end	mpeg4_decode_frame
.size	mpeg4_decode_frame, .-mpeg4_decode_frame

.comm	rota_crc,2,2
.data
.align	2
.type	dc_chrom_table, @object
.size	dc_chrom_table, 544
dc_chrom_table:
.half	-26368
.half	14344
.half	12295
.half	12295
.half	10246
.half	10246
.half	10246
.half	10246
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	8197
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	6148
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	18431
.half	6156
.half	4107
.half	4107
.half	2058
.half	2058
.half	2058
.half	2058
.half	9
.half	9
.half	9
.half	9
.half	9
.half	9
.half	9
.half	9
.align	2
.type	dc_lum_table, @object
.size	dc_lum_table, 528
dc_lum_table:
.half	-28416
.half	14345
.half	12296
.half	12296
.half	10247
.half	10247
.half	10247
.half	10247
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	8198
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	6149
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4100
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	4096
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2050
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	2049
.half	18431
.half	4108
.half	2059
.half	2059
.half	10
.half	10
.half	10
.half	10
.align	2
.type	rl_intra_table, @object
.size	rl_intra_table, 800
rl_intra_table:
.half	-28400
.half	-28392
.half	-28384
.half	-28376
.half	-28368
.half	-26368
.half	12390
.half	12390
.half	-28360
.half	-28352
.half	-28344
.half	-28336
.half	-28328
.half	-28320
.half	-28312
.half	-28304
.half	-28296
.half	-28288
.half	-28280
.half	14425
.half	14424
.half	14422
.half	14405
.half	14398
.half	14396
.half	14394
.half	14426
.half	14379
.half	14366
.half	14347
.half	14346
.half	14345
.half	12370
.half	12370
.half	12368
.half	12368
.half	12340
.half	12340
.half	12372
.half	12372
.half	12343
.half	12343
.half	12326
.half	12326
.half	12317
.half	12317
.half	12296
.half	12296
.half	10308
.half	10308
.half	10308
.half	10308
.half	10289
.half	10289
.half	10289
.half	10289
.half	10318
.half	10318
.half	10318
.half	10318
.half	10315
.half	10315
.half	10315
.half	10315
.half	10286
.half	10286
.half	10286
.half	10286
.half	10282
.half	10282
.half	10282
.half	10282
.half	10247
.half	10247
.half	10247
.half	10247
.half	10246
.half	10246
.half	10246
.half	10246
.half	10268
.half	10268
.half	10268
.half	10268
.half	10245
.half	10245
.half	10245
.half	10245
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8229
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8196
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	8195
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	6211
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	2048
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6171
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6146
.half	6168
.half	6169
.half	6170
.half	6179
.half	6198
.half	6180
.half	6185
.half	6201
.half	6210
.half	6218
.half	6229
.half	6231
.half	6242
.half	6243
.half	6244
.half	6245
.half	18431
.half	18431
.half	18431
.half	18431
.half	4169
.half	4168
.half	4117
.half	4116
.half	2127
.half	2127
.half	2125
.half	2125
.half	2119
.half	2119
.half	2113
.half	2113
.half	2099
.half	2099
.half	2107
.half	2107
.half	2096
.half	2096
.half	2093
.half	2093
.half	2088
.half	2088
.half	2081
.half	2081
.half	2067
.half	2067
.half	2066
.half	2066
.half	4118
.half	4119
.half	4130
.half	4157
.half	4177
.half	4179
.half	4192
.half	4193
.half	2065
.half	2065
.half	2064
.half	2064
.half	95
.half	95
.half	95
.half	95
.half	94
.half	94
.half	94
.half	94
.half	93
.half	93
.half	93
.half	93
.half	92
.half	92
.half	92
.half	92
.half	91
.half	91
.half	91
.half	91
.half	76
.half	76
.half	76
.half	76
.half	70
.half	70
.half	70
.half	70
.half	64
.half	64
.half	64
.half	64
.half	63
.half	63
.half	63
.half	63
.half	56
.half	56
.half	56
.half	56
.half	53
.half	53
.half	53
.half	53
.half	50
.half	50
.half	50
.half	50
.half	44
.half	44
.half	44
.half	44
.half	39
.half	39
.half	39
.half	39
.half	32
.half	32
.half	32
.half	32
.half	31
.half	31
.half	31
.half	31
.half	15
.half	15
.half	15
.half	15
.half	47
.half	47
.half	47
.half	47
.half	14
.half	14
.half	14
.half	14
.half	13
.half	13
.half	13
.half	13
.half	12
.half	12
.half	12
.half	12
.align	2
.type	cbpy_table, @object
.size	cbpy_table, 128
cbpy_table:
.half	18431
.half	18431
.half	10246
.half	10249
.half	8200
.half	8200
.half	8196
.half	8196
.half	8194
.half	8194
.half	8193
.half	8193
.half	6144
.half	6144
.half	6144
.half	6144
.half	6156
.half	6156
.half	6156
.half	6156
.half	6154
.half	6154
.half	6154
.half	6154
.half	6158
.half	6158
.half	6158
.half	6158
.half	6149
.half	6149
.half	6149
.half	6149
.half	6157
.half	6157
.half	6157
.half	6157
.half	6147
.half	6147
.half	6147
.half	6147
.half	6155
.half	6155
.half	6155
.half	6155
.half	6151
.half	6151
.half	6151
.half	6151
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.half	2063
.align	2
.type	c_table, @object
.size	c_table, 144
c_table:
.half	-28608
.half	10245
.half	10246
.half	10247
.half	6148
.half	6148
.half	6148
.half	6148
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4097
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4098
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	4099
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	0
.half	18431
.half	4104
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431
.half	18431

.comm	mpeg4_hw_bs_buffer,4,4

.comm	mpeg4_sw_bs_buffer,4,4

.comm	tcsm0_fifo_rp,4,4

.comm	tcsm1_fifo_wp,4,4

.comm	task_fifo_wp_d2,4,4

.comm	task_fifo_wp_d1,4,4

.comm	task_fifo_wp,4,4

.comm	emu_edge_buf_aux,4,4

.comm	current_picture_ptr,12,4

.comm	t1_dFRM,4,4

.comm	dFRM,412,4

.comm	dMB,4,4
.rdata
.align	2
.type	IntpFMT, @object
.size	IntpFMT, 7280
IntpFMT:
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	16
.byte	16
.byte	5
.byte	5
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	0
.space	7
.byte	15
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	-1
.byte	3
.byte	-6
.byte	20
.byte	20
.byte	-6
.byte	3
.byte	-1
.byte	15
.byte	15
.byte	5
.byte	5
.byte	1
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	0
.byte	5
.byte	10
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	7
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.space	7
.byte	0
.space	7
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	12
.byte	6
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	6
.byte	12
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	8
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	16
.byte	0
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	16
.byte	5
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	16
.byte	6
.byte	5
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	52
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	32
.byte	32
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-5
.byte	20
.byte	20
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-5
.byte	20
.byte	52
.byte	-5
.byte	1
.byte	0
.byte	0
.byte	16
.byte	32
.byte	5
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	2
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	8
.byte	4
.byte	4
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	0
.space	34
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	8
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	7
.byte	0
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	7
.byte	4
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	7
.byte	6
.byte	4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	31
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	53
.byte	18
.byte	-3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	9
.byte	9
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	8
.byte	31
.byte	4
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-3
.byte	18
.byte	53
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	31
.byte	6
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	4
.byte	0
.byte	3
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	-2
.byte	96
.byte	42
.byte	-7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-7
.byte	42
.byte	96
.byte	-2
.byte	-1
.byte	0
.byte	0
.byte	64
.byte	0
.byte	0
.byte	10
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	5
.byte	5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	5
.byte	-5
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	32
.byte	0
.byte	6
.byte	0
.byte	1
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	109
.byte	24
.byte	-1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-4
.byte	68
.byte	68
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	-1
.byte	24
.byte	109
.byte	-4
.byte	0
.byte	0
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.space	1
.byte	0
.space	1
.byte	0
.space	1
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	0
.byte	1
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	0
.space	7
.byte	64
.byte	0
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	2
.byte	-11
.byte	108
.byte	36
.byte	-8
.byte	1
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	3
.byte	-16
.byte	77
.byte	77
.byte	-16
.byte	3
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	2
.byte	1
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	0
.byte	1
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	1
.byte	-8
.byte	36
.byte	108
.byte	-11
.byte	2
.byte	0
.byte	0
.byte	64
.byte	64
.byte	7
.byte	7
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0

.comm	mc_base,4,4

.comm	sram_base,4,4

.comm	tcsm1_base,4,4

.comm	sde_frm_mv_cnt,4,4

.comm	sde_frm_buffer,4,4
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
