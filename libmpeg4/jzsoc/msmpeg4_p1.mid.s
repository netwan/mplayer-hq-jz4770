.file	1 "msmpeg4_p1.c"
.section .mdebug.abi32
.previous
.gnu_attribute 4, 1
.text
.align	2
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct_add_mxu.constprop.3
.type	ff_simple_idct_add_mxu.constprop.3, @function
ff_simple_idct_add_mxu.constprop.3:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lui	$2,%hi(whirl_idct)
addiu	$2,$2,%lo(whirl_idct)
#APP
# 337 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000000101010000	#S32LDD XR5,$2,0
# 0 "" 2
# 338 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000010110010000	#S32LDD XR6,$2,4
# 0 "" 2
# 339 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000100111010000	#S32LDD XR7,$2,8
# 0 "" 2
# 340 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000111000010000	#S32LDD XR8,$2,12
# 0 "" 2
# 341 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000001001001010000	#S32LDD XR9,$2,16
# 0 "" 2
# 342 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000001011010010000	#S32LDD XR10,$2,20
# 0 "" 2
#NO_APP
addiu	$3,$5,-16
li	$6,8			# 0x8
move	$2,$3
$L2:
#APP
# 346 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000001000001010100	#S32LDI XR1,$2,16
# 0 "" 2
# 347 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 348 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 349 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000110100010000	#S32LDD XR4,$2,12
# 0 "" 2
# 351 "mpeg4_aux_idct.c" 1
.word	0b01110011000010001000010001111101	#S32SFL XR1,XR1,XR2,XR2,PTN3
# 0 "" 2
# 352 "mpeg4_aux_idct.c" 1
.word	0b01110011000100010000110011111101	#S32SFL XR3,XR3,XR4,XR4,PTN3
# 0 "" 2
# 354 "mpeg4_aux_idct.c" 1
.word	0b01110000001100010100101011001000	#D16MUL XR11,XR2,XR5,XR12,WW
# 0 "" 2
# 355 "mpeg4_aux_idct.c" 1
.word	0b01110000001100011001001011001010	#D16MAC XR11,XR4,XR6,XR12,AA,WW
# 0 "" 2
# 357 "mpeg4_aux_idct.c" 1
.word	0b01110000001110011000101101001000	#D16MUL XR13,XR2,XR6,XR14,WW
# 0 "" 2
# 358 "mpeg4_aux_idct.c" 1
.word	0b01110011001110010101001101001010	#D16MAC XR13,XR4,XR5,XR14,SS,WW
# 0 "" 2
# 360 "mpeg4_aux_idct.c" 1
.word	0b01110000100100011100010010001000	#D16MUL XR2,XR1,XR7,XR4,HW
# 0 "" 2
# 361 "mpeg4_aux_idct.c" 1
.word	0b01110001010100100100010010001010	#D16MAC XR2,XR1,XR9,XR4,AS,LW
# 0 "" 2
# 362 "mpeg4_aux_idct.c" 1
.word	0b01110001100100101000110010001010	#D16MAC XR2,XR3,XR10,XR4,AS,HW
# 0 "" 2
# 363 "mpeg4_aux_idct.c" 1
.word	0b01110001010100100000110010001010	#D16MAC XR2,XR3,XR8,XR4,AS,LW
# 0 "" 2
# 364 "mpeg4_aux_idct.c" 1
.word	0b01110000000100000000000010001011	#D16MACF XR2,XR0,XR0,XR4,AA,WW
# 0 "" 2
# 365 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 366 "mpeg4_aux_idct.c" 1
.word	0b01110000001110000000001100001011	#D16MACF XR12,XR0,XR0,XR14,AA,WW
# 0 "" 2
# 368 "mpeg4_aux_idct.c" 1
.word	0b01110000101111100000010100001000	#D16MUL XR4,XR1,XR8,XR15,HW
# 0 "" 2
# 369 "mpeg4_aux_idct.c" 1
.word	0b01110011011111101000010100001010	#D16MAC XR4,XR1,XR10,XR15,SS,LW
# 0 "" 2
# 370 "mpeg4_aux_idct.c" 1
.word	0b01110000101111100100110100001010	#D16MAC XR4,XR3,XR9,XR15,AA,HW
# 0 "" 2
# 371 "mpeg4_aux_idct.c" 1
.word	0b01110010011111011100110100001010	#D16MAC XR4,XR3,XR7,XR15,SA,LW
# 0 "" 2
# 372 "mpeg4_aux_idct.c" 1
.word	0b01110001001100110010111011001110	#Q16ADD XR11,XR11,XR12,XR12,AS,WW
# 0 "" 2
# 373 "mpeg4_aux_idct.c" 1
.word	0b01110000000100000000001111001011	#D16MACF XR15,XR0,XR0,XR4,AA,WW
# 0 "" 2
# 375 "mpeg4_aux_idct.c" 1
.word	0b01110001000010001010111011001110	#Q16ADD XR11,XR11,XR2,XR2,AS,WW
# 0 "" 2
# 376 "mpeg4_aux_idct.c" 1
.word	0b01110001111111111111001100001110	#Q16ADD XR12,XR12,XR15,XR15,AS,XW
# 0 "" 2
# 378 "mpeg4_aux_idct.c" 1
.word	0b01110011001100110010111011111101	#S32SFL XR11,XR11,XR12,XR12,PTN3
# 0 "" 2
# 379 "mpeg4_aux_idct.c" 1
.word	0b01110011001011101111001100111101	#S32SFL XR12,XR12,XR11,XR11,PTN3
# 0 "" 2
# 381 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000001100010001	#S32STD XR12,$2,0
# 0 "" 2
# 382 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000011011010001	#S32STD XR11,$2,4
# 0 "" 2
# 383 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000101111010001	#S32STD XR15,$2,8
# 0 "" 2
# 384 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000110010010001	#S32STD XR2,$2,12
# 0 "" 2
#NO_APP
addiu	$6,$6,-1
bne	$6,$0,$L2
li	$6,1518469120			# 0x5a820000
addiu	$5,$5,-4
li	$2,4			# 0x4
addiu	$6,$6,30274
$L3:
#APP
# 390 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000000010001010100	#S32LDI XR1,$5,4
# 0 "" 2
# 391 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000010000011010000	#S32LDD XR3,$5,32
# 0 "" 2
# 392 "mpeg4_aux_idct.c" 1
.word	0b01110000000001100000000101101111	#S32I2M XR5,$6
# 0 "" 2
# 393 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000100001011010000	#S32LDD XR11,$5,64
# 0 "" 2
# 394 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000110001101010000	#S32LDD XR13,$5,96
# 0 "" 2
# 396 "mpeg4_aux_idct.c" 1
.word	0b01110000101001000101011111001000	#D16MUL XR15,XR5,XR1,XR9,HW
# 0 "" 2
# 397 "mpeg4_aux_idct.c" 1
.word	0b01110000101001101101011111001010	#D16MAC XR15,XR5,XR11,XR9,AA,HW
# 0 "" 2
# 398 "mpeg4_aux_idct.c" 1
.word	0b01110000001001000000001111001011	#D16MACF XR15,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 399 "mpeg4_aux_idct.c" 1
.word	0b01110000011001001101011010001000	#D16MUL XR10,XR5,XR3,XR9,LW
# 0 "" 2
# 400 "mpeg4_aux_idct.c" 1
.word	0b01110000011001110101101010001010	#D16MAC XR10,XR6,XR13,XR9,AA,LW
# 0 "" 2
# 401 "mpeg4_aux_idct.c" 1
.word	0b01110000001001000000001010001011	#D16MACF XR10,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 402 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000001000010010000	#S32LDD XR2,$5,16
# 0 "" 2
# 403 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000011000100010000	#S32LDD XR4,$5,48
# 0 "" 2
# 404 "mpeg4_aux_idct.c" 1
.word	0b01110001001001101011111111001110	#Q16ADD XR15,XR15,XR10,XR9,AS,WW
# 0 "" 2
# 406 "mpeg4_aux_idct.c" 1
.word	0b01110000100001000101011010001000	#D16MUL XR10,XR5,XR1,XR1,HW
# 0 "" 2
# 407 "mpeg4_aux_idct.c" 1
.word	0b01110011100001101101011010001010	#D16MAC XR10,XR5,XR11,XR1,SS,HW
# 0 "" 2
# 408 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000000001010001011	#D16MACF XR10,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 409 "mpeg4_aux_idct.c" 1
.word	0b01110000010001001101101011001000	#D16MUL XR11,XR6,XR3,XR1,LW
# 0 "" 2
# 410 "mpeg4_aux_idct.c" 1
.word	0b01110011010001110101011011001010	#D16MAC XR11,XR5,XR13,XR1,SS,LW
# 0 "" 2
# 411 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000000001011001011	#D16MACF XR11,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 412 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000101001100010000	#S32LDD XR12,$5,80
# 0 "" 2
# 413 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000111001110010000	#S32LDD XR14,$5,112
# 0 "" 2
# 414 "mpeg4_aux_idct.c" 1
.word	0b01110001000001101110101010001110	#Q16ADD XR10,XR10,XR11,XR1,AS,WW
# 0 "" 2
# 416 "mpeg4_aux_idct.c" 1
.word	0b01110000101101001001111011001000	#D16MUL XR11,XR7,XR2,XR13,HW
# 0 "" 2
# 417 "mpeg4_aux_idct.c" 1
.word	0b01110000011101010001111011001010	#D16MAC XR11,XR7,XR4,XR13,AA,LW
# 0 "" 2
# 418 "mpeg4_aux_idct.c" 1
.word	0b01110000011101110010001011001010	#D16MAC XR11,XR8,XR12,XR13,AA,LW
# 0 "" 2
# 419 "mpeg4_aux_idct.c" 1
.word	0b01110000101101111010001011001010	#D16MAC XR11,XR8,XR14,XR13,AA,HW
# 0 "" 2
# 420 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 422 "mpeg4_aux_idct.c" 1
.word	0b01110000011101001001110011001000	#D16MUL XR3,XR7,XR2,XR13,LW
# 0 "" 2
# 423 "mpeg4_aux_idct.c" 1
.word	0b01110011101101010010000011001010	#D16MAC XR3,XR8,XR4,XR13,SS,HW
# 0 "" 2
# 424 "mpeg4_aux_idct.c" 1
.word	0b01110011101101110001110011001010	#D16MAC XR3,XR7,XR12,XR13,SS,HW
# 0 "" 2
# 425 "mpeg4_aux_idct.c" 1
.word	0b01110011011101111010000011001010	#D16MAC XR3,XR8,XR14,XR13,SS,LW
# 0 "" 2
# 426 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000011001011	#D16MACF XR3,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 428 "mpeg4_aux_idct.c" 1
.word	0b01110000011101001010000101001000	#D16MUL XR5,XR8,XR2,XR13,LW
# 0 "" 2
# 429 "mpeg4_aux_idct.c" 1
.word	0b01110011101101010001110101001010	#D16MAC XR5,XR7,XR4,XR13,SS,HW
# 0 "" 2
# 430 "mpeg4_aux_idct.c" 1
.word	0b01110000101101110010000101001010	#D16MAC XR5,XR8,XR12,XR13,AA,HW
# 0 "" 2
# 431 "mpeg4_aux_idct.c" 1
.word	0b01110000011101111001110101001010	#D16MAC XR5,XR7,XR14,XR13,AA,LW
# 0 "" 2
# 432 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000101001011	#D16MACF XR5,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 434 "mpeg4_aux_idct.c" 1
.word	0b01110000101101001010000010001000	#D16MUL XR2,XR8,XR2,XR13,HW
# 0 "" 2
# 435 "mpeg4_aux_idct.c" 1
.word	0b01110011011101010010000010001010	#D16MAC XR2,XR8,XR4,XR13,SS,LW
# 0 "" 2
# 436 "mpeg4_aux_idct.c" 1
.word	0b01110000011101110001110010001010	#D16MAC XR2,XR7,XR12,XR13,AA,LW
# 0 "" 2
# 437 "mpeg4_aux_idct.c" 1
.word	0b01110011101101111001110010001010	#D16MAC XR2,XR7,XR14,XR13,SS,HW
# 0 "" 2
# 438 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000010001011	#D16MACF XR2,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 440 "mpeg4_aux_idct.c" 1
.word	0b01110001001011101111111111001110	#Q16ADD XR15,XR15,XR11,XR11,AS,WW
# 0 "" 2
# 441 "mpeg4_aux_idct.c" 1
.word	0b01110001000011001110101010001110	#Q16ADD XR10,XR10,XR3,XR3,AS,WW
# 0 "" 2
# 442 "mpeg4_aux_idct.c" 1
.word	0b01110001000101010100010001001110	#Q16ADD XR1,XR1,XR5,XR5,AS,WW
# 0 "" 2
# 443 "mpeg4_aux_idct.c" 1
.word	0b01110001000010001010011001001110	#Q16ADD XR9,XR9,XR2,XR2,AS,WW
# 0 "" 2
# 445 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000000001111010001	#S32STD XR15,$5,0
# 0 "" 2
# 446 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000001001010010001	#S32STD XR10,$5,16
# 0 "" 2
# 447 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000010000001010001	#S32STD XR1,$5,32
# 0 "" 2
# 448 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000011001001010001	#S32STD XR9,$5,48
# 0 "" 2
# 449 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000100000010010001	#S32STD XR2,$5,64
# 0 "" 2
# 450 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000101000101010001	#S32STD XR5,$5,80
# 0 "" 2
# 451 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000110000011010001	#S32STD XR3,$5,96
# 0 "" 2
# 452 "mpeg4_aux_idct.c" 1
.word	0b01110000101000000111001011010001	#S32STD XR11,$5,112
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$0,$L3
addiu	$4,$4,-16
li	$2,8			# 0x8
li	$5,16			# 0x10
$L4:
#APP
# 457 "mpeg4_aux_idct.c" 1
.word	0b01110000100001010000000101010110	#S32LDIV XR5,$4,$5,0
# 0 "" 2
# 458 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000001000001010100	#S32LDI XR1,$3,16
# 0 "" 2
# 459 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000000010010010000	#S32LDD XR2,$3,4
# 0 "" 2
# 460 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000001010010011101	#Q8ACCE XR2,XR5,XR0,XR1,AA
# 0 "" 2
# 462 "mpeg4_aux_idct.c" 1
.word	0b01110000100000000000010110010000	#S32LDD XR6,$4,4
# 0 "" 2
# 463 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000000100011010000	#S32LDD XR3,$3,8
# 0 "" 2
# 464 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000000110100010000	#S32LDD XR4,$3,12
# 0 "" 2
# 465 "mpeg4_aux_idct.c" 1
.word	0b01110000000011000001100100011101	#Q8ACCE XR4,XR6,XR0,XR3,AA
# 0 "" 2
# 467 "mpeg4_aux_idct.c" 1
.word	0b01110000000110000100100101000111	#Q16SAT XR5,XR2,XR1
# 0 "" 2
# 468 "mpeg4_aux_idct.c" 1
.word	0b01110000100000000000000101010001	#S32STD XR5,$4,0
# 0 "" 2
# 469 "mpeg4_aux_idct.c" 1
.word	0b01110000000110001101000110000111	#Q16SAT XR6,XR4,XR3
# 0 "" 2
# 470 "mpeg4_aux_idct.c" 1
.word	0b01110000100000000000010110010001	#S32STD XR6,$4,4
# 0 "" 2
#NO_APP
addiu	$2,$2,-1
bne	$2,$0,$L4
j	$31
.end	ff_simple_idct_add_mxu.constprop.3
.size	ff_simple_idct_add_mxu.constprop.3, .-ff_simple_idct_add_mxu.constprop.3
.align	2
.set	nomips16
.set	nomicromips
.ent	add_dequant_dct_opt.constprop.2
.type	add_dequant_dct_opt.constprop.2, @function
add_dequant_dct_opt.constprop.2:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
lui	$2,%hi(dMB_L)
addiu	$3,$5,20
lw	$2,%lo(dMB_L)($2)
sll	$3,$3,2
addu	$2,$2,$3
lw	$8,0($2)
.set	noreorder
.set	nomacro
bltz	$8,$L33
move	$5,$4
.set	macro
.set	reorder

lui	$2,%hi(dFRM)
lw	$3,%lo(dFRM)($2)
lb	$2,41($3)
.set	noreorder
.set	nomacro
bne	$2,$0,$L11
li	$2,2			# 0x2
.set	macro
.set	reorder

lb	$4,7($3)
.set	noreorder
.set	nomacro
beq	$4,$2,$L32
li	$2,-1			# 0xffffffffffffffff
.set	macro
.set	reorder

lbu	$2,6($3)
addiu	$2,$2,-1
andi	$2,$2,0x00ff
sltu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$0,$L30
addu	$3,$3,$8
.set	macro
.set	reorder

$L15:
.set	noreorder
.set	nomacro
j	ff_simple_idct_add_mxu.constprop.3
move	$4,$6
.set	macro
.set	reorder

$L11:
li	$2,-1			# 0xffffffffffffffff
$L32:
#APP
# 770 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000001111101111	#S32I2M XR15,$2
# 0 "" 2
#NO_APP
li	$2,1			# 0x1
#APP
# 771 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000000101101111	#S32I2M XR5,$2
# 0 "" 2
#NO_APP
addiu	$3,$3,236
addiu	$9,$5,128
move	$2,$5
$L14:
lh	$8,0($2)
beq	$8,$0,$L13
#APP
# 775 "mpeg4_aux_idct.c" 1
.word	0b01110000000010000000000001101111	#S32I2M XR1,$8
# 0 "" 2
# 776 "mpeg4_aux_idct.c" 1
.word	0b01110000000000000100010010000111	#S32CPS XR2,XR1,XR1
# 0 "" 2
#NO_APP
lhu	$8,0($3)
#APP
# 777 "mpeg4_aux_idct.c" 1
.word	0b01110000111010000000110000100110	#S32MUL XR0,XR3,$7,$8
# 0 "" 2
# 778 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000100100110000	#D32SLL XR4,XR2,XR0,XR0,1
# 0 "" 2
# 779 "mpeg4_aux_idct.c" 1
.word	0b01110000000110010101000110100111	#S32OR XR6,XR4,XR5
# 0 "" 2
# 780 "mpeg4_aux_idct.c" 1
.word	0b01110000000111001101100000001000	#D16MUL XR0,XR6,XR3,XR7,WW
# 0 "" 2
# 781 "mpeg4_aux_idct.c" 1
.word	0b01110001000000000001111001110001	#D32SLR XR9,XR7,XR0,XR0,4
# 0 "" 2
# 782 "mpeg4_aux_idct.c" 1
.word	0b01110000000000000110011000000111	#S32CPS XR8,XR9,XR1
# 0 "" 2
# 783 "mpeg4_aux_idct.c" 1
.word	0b01110000000010000000001000101110	#S32M2I XR8, $8
# 0 "" 2
#NO_APP
sh	$8,0($2)
#APP
# 784 "mpeg4_aux_idct.c" 1
.word	0b01110000100000000010001111011001	#D32ASUM XR15,XR8,XR0,XR0,AA
# 0 "" 2
#NO_APP
$L13:
addiu	$2,$2,2
.set	noreorder
.set	nomacro
bne	$2,$9,$L14
addiu	$3,$3,2
.set	macro
.set	reorder

#APP
# 787 "mpeg4_aux_idct.c" 1
.word	0b01110000000101010111111101100111	#S32AND XR13,XR15,XR5
# 0 "" 2
#NO_APP
lhu	$2,126($5)
#APP
# 788 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000001110101111	#S32I2M XR14,$2
# 0 "" 2
# 789 "mpeg4_aux_idct.c" 1
.word	0b01110000000111110111101110100111	#S32XOR XR14,XR14,XR13
# 0 "" 2
# 790 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000001110101110	#S32M2I XR14, $2
# 0 "" 2
#NO_APP
sh	$2,126($5)
.set	noreorder
.set	nomacro
j	ff_simple_idct_add_mxu.constprop.3
move	$4,$6
.set	macro
.set	reorder

$L33:
j	$31
$L30:
addiu	$9,$7,-1
sll	$7,$7,1
lbu	$4,172($3)
ori	$9,$9,0x1
move	$2,$5
addiu	$4,$4,1
sll	$4,$4,1
addu	$4,$5,$4
$L20:
lh	$3,0($2)
beq	$3,$0,$L17
mul	$10,$7,$3
.set	noreorder
.set	nomacro
bltz	$3,$L31
addu	$8,$10,$9
.set	macro
.set	reorder

$L19:
sh	$8,0($2)
$L17:
addiu	$2,$2,2
bne	$2,$4,$L20
j	$L15
$L31:
mul	$3,$7,$3
.set	noreorder
.set	nomacro
j	$L19
subu	$8,$3,$9
.set	macro
.set	reorder

.end	add_dequant_dct_opt.constprop.2
.size	add_dequant_dct_opt.constprop.2, .-add_dequant_dct_opt.constprop.2
.align	2
.globl	mpeg4_hpel_4mv
.set	nomips16
.set	nomicromips
.ent	mpeg4_hpel_4mv
.type	mpeg4_hpel_4mv, @function
mpeg4_hpel_4mv:
.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
.mask	0x003f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
addiu	$sp,$sp,-24
li	$2,131072			# 0x20000
li	$3,196608			# 0x30000
sw	$21,20($sp)
lw	$9,44($sp)
lui	$8,%hi(motion_dha)
lw	$11,48($sp)
sll	$5,$5,8
lw	$10,52($sp)
andi	$4,$4,0xff
movn	$3,$2,$9
lui	$2,%hi(motion_dsa)
lw	$12,56($sp)
lw	$14,%lo(motion_dsa)($2)
lw	$2,%lo(motion_dha)($8)
lw	$13,60($sp)
sw	$20,16($sp)
sw	$19,12($sp)
sw	$18,8($sp)
sw	$17,4($sp)
sw	$16,0($sp)
sb	$0,0($14)
li	$14,321191936			# 0x13250000
sw	$3,48($14)
li	$3,3			# 0x3
beq	$6,$3,$L36
li	$3,-1048248320			# 0xffffffffc1850000

andi	$5,$5,0xffff
or	$4,$4,$3
or	$4,$4,$5
andi	$24,$6,0x1
sw	$4,0($2)
bne	$24,$0,$L37
li	$4,8			# 0x8

li	$3,4			# 0x4
li	$25,1			# 0x1
andi	$6,$6,0x2
$L38:
beq	$6,$0,$L43
li	$5,786432			# 0xc0000

lw	$15,16($13)
lhu	$17,16($12)
li	$16,524288			# 0x80000
addu	$3,$2,$3
sll	$15,$15,16
movz	$16,$5,$9
sll	$14,$7,31
or	$5,$15,$17
li	$7,1073741824			# 0x40000000
sw	$5,0($3)
addu	$5,$2,$4
lw	$3,16($11)
or	$19,$14,$7
addiu	$4,$25,2
andi	$3,$3,0xf
ori	$3,$3,0xa0
or	$3,$3,$19
or	$3,$3,$16
sll	$4,$4,2
sw	$3,0($5)
lw	$3,20($13)
addu	$5,$2,$4
lhu	$17,20($12)
addiu	$15,$4,4
addiu	$7,$4,8
sll	$3,$3,16
addu	$15,$2,$15
or	$3,$3,$17
addu	$7,$2,$7
sw	$3,0($5)
addiu	$18,$4,12
lw	$3,20($11)
addiu	$5,$4,16
addu	$18,$2,$18
addu	$5,$2,$5
andi	$3,$3,0xf
ori	$3,$3,0x2a0
or	$3,$3,$19
or	$3,$3,$16
addiu	$17,$4,20
sw	$3,0($15)
addiu	$3,$4,24
lw	$20,24($13)
addu	$17,$2,$17
lhu	$21,24($12)
addiu	$15,$25,8
addiu	$4,$4,28
sll	$20,$20,16
or	$20,$20,$21
sw	$20,0($7)
lw	$7,24($11)
andi	$7,$7,0xf
ori	$7,$7,0x8a0
or	$7,$7,$19
or	$7,$7,$16
sw	$7,0($18)
lw	$7,28($13)
lhu	$12,28($12)
sll	$7,$7,16
or	$7,$7,$12
sw	$7,0($5)
li	$5,1124073472			# 0x43000000
lw	$11,28($11)
addiu	$5,$5,2720
andi	$11,$11,0xf
or	$11,$14,$11
or	$5,$11,$5
or	$5,$5,$16
sw	$5,0($17)
beq	$24,$0,$L58
addu	$11,$2,$3

lw	$15,68($sp)
lw	$3,64($sp)
addu	$13,$2,$4
move	$12,$0
lw	$5,0($15)
addiu	$15,$25,10
lhu	$7,0($3)
sll	$3,$5,16
or	$3,$3,$7
sw	$3,0($11)
lw	$4,0($10)
$L76:
li	$5,196608			# 0x30000
li	$7,131072			# 0x20000
sll	$3,$15,2
movz	$7,$5,$9
andi	$4,$4,0xf
ori	$4,$4,0xf0
or	$4,$4,$14
or	$4,$4,$12
or	$5,$4,$7
addiu	$4,$3,4
sw	$5,0($13)
beq	$6,$0,$L77
li	$6,-1048576000			# 0xffffffffc1800000

$L58:
lw	$6,68($sp)
addu	$3,$2,$3
lw	$7,64($sp)
lw	$5,4($6)
li	$6,131072			# 0x20000
lhu	$11,4($7)
li	$7,196608			# 0x30000
sll	$5,$5,16
movn	$7,$6,$9
addu	$6,$2,$4
or	$5,$5,$11
sw	$5,0($3)
move	$9,$7
lw	$5,4($10)
li	$7,1107296256			# 0x42000000
addiu	$3,$15,2
addiu	$7,$7,240
andi	$5,$5,0xf
or	$4,$5,$7
or	$7,$4,$14
sll	$3,$3,2
or	$7,$7,$9
addiu	$4,$3,4
sw	$7,0($6)
$L51:
li	$6,-1048576000			# 0xffffffffc1800000
$L77:
lhu	$5,%lo(motion_dha)($8)
addu	$3,$2,$3
lw	$21,20($sp)
ori	$6,$6,0xffff
lw	$20,16($sp)
addu	$2,$2,$4
lw	$19,12($sp)
sw	$6,0($3)
li	$3,321650688			# 0x132c0000
lw	$18,8($sp)
addiu	$3,$3,1
lw	$17,4($sp)
lw	$16,0($sp)
or	$3,$5,$3
li	$5,-1610612736			# 0xffffffffa0000000
ori	$5,$5,0xffff
sw	$5,0($2)
li	$2,321191936			# 0x13250000
sw	$3,84($2)
j	$31
addiu	$sp,$sp,24

$L43:
beq	$24,$0,$L51
lw	$11,68($sp)

addu	$13,$2,$4
lw	$12,64($sp)
addu	$3,$2,$3
addiu	$15,$25,2
sll	$14,$7,31
lw	$5,0($11)
lhu	$11,0($12)
li	$12,33554432			# 0x2000000
sll	$4,$5,16
or	$4,$4,$11
sw	$4,0($3)
j	$L76
lw	$4,0($10)

$L36:
li	$3,-1047920640			# 0xffffffffc18a0000
andi	$5,$5,0xffff
or	$4,$4,$3
or	$4,$4,$5
sw	$4,0($2)
$L37:
lw	$3,0($13)
sll	$15,$7,31
lhu	$4,0($12)
sll	$3,$3,16
or	$4,$3,$4
sw	$4,4($2)
bne	$9,$0,$L39
li	$3,524288			# 0x80000

lw	$5,0($11)
li	$3,786432			# 0xc0000
li	$14,786432			# 0xc0000
or	$16,$15,$3
andi	$5,$5,0xf
ori	$5,$5,0xa0
or	$5,$5,$16
sw	$5,8($2)
lw	$4,4($13)
lhu	$3,4($12)
sll	$4,$4,16
or	$4,$4,$3
sw	$4,12($2)
lw	$3,4($11)
andi	$3,$3,0xf
ori	$3,$3,0x2a0
or	$3,$3,$16
$L73:
sw	$3,16($2)
andi	$6,$6,0x2
lw	$3,8($13)
lhu	$4,8($12)
sll	$3,$3,16
or	$3,$3,$4
sw	$3,20($2)
lw	$4,8($11)
andi	$4,$4,0xf
or	$4,$15,$4
ori	$4,$4,0x8a0
or	$4,$4,$14
sw	$4,24($2)
lw	$4,12($13)
lhu	$3,12($12)
sll	$4,$4,16
or	$4,$4,$3
sw	$4,28($2)
bne	$6,$0,$L75
nop

li	$4,33554432			# 0x2000000
li	$3,16777216			# 0x1000000
or	$15,$15,$4
$L54:
lw	$14,12($11)
li	$5,786432			# 0xc0000
li	$16,524288			# 0x80000
li	$4,40			# 0x28
movn	$5,$16,$9
andi	$14,$14,0xf
ori	$14,$14,0xaa0
or	$3,$14,$3
or	$3,$3,$15
or	$5,$3,$5
li	$24,1			# 0x1
li	$3,36			# 0x24
sw	$5,32($2)
j	$L38
li	$25,9			# 0x9

$L39:
lw	$4,0($11)
li	$14,524288			# 0x80000
or	$5,$15,$3
andi	$4,$4,0xf
ori	$4,$4,0xa0
or	$4,$4,$5
sw	$4,8($2)
lw	$4,4($13)
lhu	$3,4($12)
sll	$4,$4,16
or	$4,$4,$3
sw	$4,12($2)
lw	$3,4($11)
andi	$3,$3,0xf
ori	$3,$3,0x2a0
j	$L73
or	$3,$3,$5

$L75:
j	$L54
move	$3,$0

.set	macro
.set	reorder
.end	mpeg4_hpel_4mv
.size	mpeg4_hpel_4mv, .-mpeg4_hpel_4mv
.align	2
.globl	mpeg4_qpel
.set	nomips16
.set	nomicromips
.ent	mpeg4_qpel
.type	mpeg4_qpel, @function
mpeg4_qpel:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(motion_dsa)
lw	$9,20($sp)
lui	$8,%hi(motion_dha)
lw	$11,24($sp)
lw	$10,28($sp)
sll	$5,$5,8
lw	$2,%lo(motion_dsa)($2)
andi	$4,$4,0xff
lw	$3,%lo(motion_dha)($8)
lw	$12,32($sp)
lw	$13,36($sp)
sb	$0,0($2)
li	$2,3			# 0x3
beq	$6,$2,$L79
li	$2,-914096128			# 0xffffffffc9840000

li	$2,-914227200			# 0xffffffffc9820000
andi	$5,$5,0xffff
or	$4,$4,$2
or	$4,$4,$5
andi	$15,$6,0x1
sw	$4,0($3)
bne	$15,$0,$L80
li	$4,8			# 0x8

li	$2,4			# 0x4
li	$14,1			# 0x1
andi	$6,$6,0x2
$L81:
beq	$6,$0,$L83
nop

lw	$5,4($13)
addu	$2,$3,$2
lhu	$24,4($12)
sll	$12,$7,31
addiu	$13,$14,2
sll	$5,$5,16
addu	$7,$3,$4
or	$5,$5,$24
sw	$5,0($2)
sll	$2,$13,2
lw	$5,4($11)
addiu	$4,$2,4
andi	$5,$5,0xf
or	$11,$12,$5
li	$5,1124073472			# 0x43000000
addiu	$5,$5,240
or	$5,$11,$5
sw	$5,0($7)
bne	$15,$0,$L84
lw	$11,44($sp)

lw	$6,44($sp)
$L102:
addu	$2,$3,$2
lw	$7,40($sp)
lw	$5,4($6)
li	$6,131072			# 0x20000
lhu	$11,4($7)
li	$7,196608			# 0x30000
sll	$5,$5,16
movn	$7,$6,$9
addu	$6,$3,$4
or	$5,$5,$11
sw	$5,0($2)
move	$9,$7
lw	$5,4($10)
li	$7,1107296256			# 0x42000000
addiu	$2,$13,2
addiu	$7,$7,240
andi	$5,$5,0xf
or	$4,$5,$7
or	$7,$4,$12
sll	$2,$2,2
or	$7,$7,$9
addiu	$4,$2,4
sw	$7,0($6)
$L87:
li	$6,-1048576000			# 0xffffffffc1800000
lhu	$5,%lo(motion_dha)($8)
addu	$2,$3,$2
ori	$6,$6,0xffff
addu	$3,$3,$4
sw	$6,0($2)
li	$2,321650688			# 0x132c0000
addiu	$2,$2,1
or	$2,$5,$2
li	$5,-1610612736			# 0xffffffffa0000000
ori	$5,$5,0xffff
sw	$5,0($3)
li	$3,321191936			# 0x13250000
sw	$2,84($3)
j	$31
nop

$L83:
beq	$15,$0,$L87
lw	$12,44($sp)

addiu	$13,$14,2
addu	$14,$3,$4
addu	$2,$3,$2
lw	$5,0($12)
lw	$12,40($sp)
sll	$4,$5,16
lhu	$11,0($12)
sll	$12,$7,31
or	$4,$4,$11
li	$11,33554432			# 0x2000000
sw	$4,0($2)
lw	$4,0($10)
$L101:
li	$5,196608			# 0x30000
li	$7,131072			# 0x20000
sll	$2,$13,2
movz	$7,$5,$9
andi	$4,$4,0xf
ori	$4,$4,0xf0
or	$4,$4,$12
or	$4,$4,$11
or	$5,$4,$7
addiu	$4,$2,4
sw	$5,0($14)
bne	$6,$0,$L102
lw	$6,44($sp)

li	$6,-1048576000			# 0xffffffffc1800000
lhu	$5,%lo(motion_dha)($8)
addu	$2,$3,$2
ori	$6,$6,0xffff
addu	$3,$3,$4
sw	$6,0($2)
li	$2,321650688			# 0x132c0000
addiu	$2,$2,1
or	$2,$5,$2
li	$5,-1610612736			# 0xffffffffa0000000
ori	$5,$5,0xffff
sw	$5,0($3)
li	$3,321191936			# 0x13250000
sw	$2,84($3)
j	$31
nop

$L79:
andi	$5,$5,0xffff
or	$4,$4,$2
or	$4,$4,$5
sll	$24,$7,31
sw	$4,0($3)
li	$6,2			# 0x2
lw	$2,0($13)
move	$5,$0
lhu	$4,0($12)
sll	$2,$2,16
or	$2,$2,$4
sw	$2,4($3)
$L89:
lw	$14,0($11)
li	$4,16			# 0x10
li	$2,12			# 0xc
li	$15,1			# 0x1
andi	$14,$14,0xf
ori	$14,$14,0xf0
or	$5,$14,$5
or	$5,$5,$24
li	$14,3			# 0x3
sw	$5,8($3)
j	$L81
nop

$L80:
lw	$2,0($13)
andi	$6,$6,0x2
lhu	$4,0($12)
sll	$24,$7,31
sll	$2,$2,16
or	$2,$2,$4
sw	$2,4($3)
bne	$6,$0,$L100
nop

li	$2,33554432			# 0x2000000
li	$5,16777216			# 0x1000000
j	$L89
or	$24,$24,$2

$L84:
addiu	$13,$14,4
addu	$14,$3,$4
addu	$2,$3,$2
lw	$5,0($11)
lw	$11,40($sp)
sll	$4,$5,16
lhu	$7,0($11)
move	$11,$0
or	$4,$4,$7
sw	$4,0($2)
j	$L101
lw	$4,0($10)

$L100:
j	$L89
move	$5,$0

.set	macro
.set	reorder
.end	mpeg4_qpel
.size	mpeg4_qpel, .-mpeg4_qpel
.align	2
.globl	mpeg4_qpel_4mv
.set	nomips16
.set	nomicromips
.ent	mpeg4_qpel_4mv
.type	mpeg4_qpel_4mv, @function
mpeg4_qpel_4mv:
.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
.mask	0x001f0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
lui	$2,%hi(motion_dsa)
lui	$8,%hi(motion_dha)
addiu	$sp,$sp,-24
lw	$3,%lo(motion_dsa)($2)
sll	$5,$5,8
lw	$2,%lo(motion_dha)($8)
andi	$4,$4,0xff
lw	$9,44($sp)
lw	$11,48($sp)
lw	$10,52($sp)
lw	$12,56($sp)
lw	$13,60($sp)
sw	$20,20($sp)
sw	$19,16($sp)
sw	$18,12($sp)
sw	$17,8($sp)
sw	$16,4($sp)
sb	$0,0($3)
li	$3,3			# 0x3
beq	$6,$3,$L104
li	$3,-914030592			# 0xffffffffc9850000

andi	$5,$5,0xffff
or	$4,$4,$3
or	$4,$4,$5
andi	$15,$6,0x1
sw	$4,0($2)
bne	$15,$0,$L105
li	$4,8			# 0x8

li	$3,4			# 0x4
li	$25,1			# 0x1
andi	$6,$6,0x2
$L106:
beq	$6,$0,$L108
sll	$14,$7,31

lw	$16,16($13)
lhu	$5,16($12)
addu	$3,$2,$3
li	$18,1073741824			# 0x40000000
sll	$7,$16,16
addiu	$17,$18,160
or	$7,$7,$5
addu	$16,$2,$4
sw	$7,0($3)
addiu	$4,$25,2
lw	$5,16($11)
addiu	$24,$18,672
sll	$4,$4,2
addiu	$18,$18,2208
andi	$5,$5,0xf
or	$5,$14,$5
or	$5,$5,$17
addu	$7,$2,$4
sw	$5,0($16)
addiu	$20,$4,4
lw	$3,20($13)
addiu	$19,$4,8
lhu	$5,20($12)
addu	$20,$2,$20
addu	$19,$2,$19
sll	$3,$3,16
addiu	$17,$4,12
or	$5,$3,$5
addu	$17,$2,$17
sw	$5,0($7)
addiu	$16,$4,16
lw	$3,20($11)
addiu	$7,$4,20
addu	$16,$2,$16
addu	$7,$2,$7
andi	$3,$3,0xf
or	$3,$14,$3
or	$3,$3,$24
addiu	$24,$25,8
sw	$3,0($20)
addiu	$3,$4,24
lw	$5,24($13)
addiu	$4,$4,28
lhu	$20,24($12)
sll	$5,$5,16
or	$5,$5,$20
sw	$5,0($19)
lw	$5,24($11)
andi	$5,$5,0xf
or	$5,$14,$5
or	$5,$5,$18
sw	$5,0($17)
lw	$5,28($13)
lhu	$12,28($12)
sll	$5,$5,16
or	$5,$5,$12
sw	$5,0($16)
lw	$5,28($11)
andi	$5,$5,0xf
or	$11,$14,$5
li	$5,1124073472			# 0x43000000
addiu	$5,$5,2720
or	$5,$11,$5
sw	$5,0($7)
bne	$15,$0,$L109
lw	$11,68($sp)

$L117:
lw	$6,68($sp)
addu	$3,$2,$3
lw	$7,64($sp)
lw	$5,4($6)
li	$6,131072			# 0x20000
lhu	$11,4($7)
li	$7,196608			# 0x30000
sll	$5,$5,16
movn	$7,$6,$9
addu	$6,$2,$4
or	$5,$5,$11
sw	$5,0($3)
move	$9,$7
lw	$5,4($10)
li	$7,1107296256			# 0x42000000
addiu	$3,$24,2
addiu	$7,$7,240
andi	$5,$5,0xf
or	$4,$5,$7
or	$7,$4,$14
sll	$3,$3,2
or	$7,$7,$9
addiu	$4,$3,4
sw	$7,0($6)
$L112:
li	$6,-1048576000			# 0xffffffffc1800000
lhu	$5,%lo(motion_dha)($8)
$L127:
addu	$3,$2,$3
lw	$20,20($sp)
ori	$6,$6,0xffff
lw	$19,16($sp)
addu	$2,$2,$4
lw	$18,12($sp)
sw	$6,0($3)
li	$3,321650688			# 0x132c0000
lw	$17,8($sp)
addiu	$3,$3,1
lw	$16,4($sp)
or	$3,$5,$3
li	$5,-1610612736			# 0xffffffffa0000000
ori	$5,$5,0xffff
sw	$5,0($2)
li	$2,321191936			# 0x13250000
sw	$3,84($2)
j	$31
addiu	$sp,$sp,24

$L108:
beq	$15,$0,$L112
lw	$14,68($sp)

addu	$3,$2,$3
lw	$12,64($sp)
addiu	$24,$25,2
lw	$5,0($14)
sll	$14,$7,31
lhu	$11,0($12)
addu	$12,$2,$4
sll	$4,$5,16
or	$4,$4,$11
li	$11,33554432			# 0x2000000
sw	$4,0($3)
lw	$4,0($10)
$L126:
li	$5,196608			# 0x30000
li	$7,131072			# 0x20000
sll	$3,$24,2
movz	$7,$5,$9
andi	$4,$4,0xf
ori	$4,$4,0xf0
or	$4,$4,$14
or	$4,$4,$11
or	$5,$4,$7
addiu	$4,$3,4
sw	$5,0($12)
bne	$6,$0,$L117
li	$6,-1048576000			# 0xffffffffc1800000

j	$L127
lhu	$5,%lo(motion_dha)($8)

$L104:
li	$3,-913702912			# 0xffffffffc98a0000
andi	$5,$5,0xffff
or	$4,$4,$3
or	$4,$4,$5
sw	$4,0($2)
$L105:
lw	$3,0($13)
sll	$16,$7,31
lhu	$4,0($12)
andi	$6,$6,0x2
sll	$3,$3,16
or	$3,$3,$4
sw	$3,4($2)
lw	$3,0($11)
andi	$3,$3,0xf
or	$3,$16,$3
ori	$3,$3,0xa0
sw	$3,8($2)
lw	$3,4($13)
lhu	$4,4($12)
sll	$3,$3,16
or	$3,$3,$4
sw	$3,12($2)
lw	$3,4($11)
andi	$3,$3,0xf
or	$3,$16,$3
ori	$3,$3,0x2a0
sw	$3,16($2)
lw	$3,8($13)
lhu	$4,8($12)
sll	$3,$3,16
or	$3,$3,$4
sw	$3,20($2)
lw	$3,8($11)
andi	$3,$3,0xf
or	$3,$16,$3
ori	$3,$3,0x8a0
sw	$3,24($2)
lw	$3,12($13)
lhu	$4,12($12)
sll	$3,$3,16
or	$3,$3,$4
sw	$3,28($2)
beq	$6,$0,$L107
li	$5,16777216			# 0x1000000

move	$5,$0
$L114:
lw	$14,12($11)
li	$4,40			# 0x28
li	$3,36			# 0x24
li	$15,1			# 0x1
andi	$14,$14,0xf
ori	$14,$14,0xaa0
or	$5,$14,$5
or	$5,$5,$16
li	$25,9			# 0x9
sw	$5,32($2)
j	$L106
nop

$L107:
li	$3,33554432			# 0x2000000
j	$L114
or	$16,$16,$3

$L109:
addu	$3,$2,$3
lw	$12,64($sp)
addiu	$24,$25,10
lw	$5,0($11)
move	$11,$0
lhu	$7,0($12)
addu	$12,$2,$4
sll	$4,$5,16
or	$4,$4,$7
sw	$4,0($3)
j	$L126
lw	$4,0($10)

.set	macro
.set	reorder
.end	mpeg4_qpel_4mv
.size	mpeg4_qpel_4mv, .-mpeg4_qpel_4mv
.align	2
.set	nomips16
.set	nomicromips
.ent	MPV_motion_p1
.type	MPV_motion_p1, @function
MPV_motion_p1:
.frame	$sp,248,$31		# vars= 160, regs= 10/0, args= 48, gp= 0
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
.set	noreorder
.set	nomacro
li	$9,2			# 0x2
addiu	$sp,$sp,-248
li	$2,-201326592			# 0xfffffffff4000000
sw	$9,148($sp)
li	$9,8			# 0x8
addiu	$3,$2,21032
sw	$0,144($sp)
lui	$14,%hi(motion_dha)
sw	$31,244($sp)
sw	$9,152($sp)
li	$9,10			# 0xa
addiu	$8,$2,21128
sw	$3,%lo(motion_dha)($14)
sw	$fp,240($sp)
andi	$13,$6,0x1
sw	$9,156($sp)
lui	$9,%hi(motion_buf)
sw	$23,236($sp)
sw	$3,%lo(motion_buf)($9)
lui	$3,%hi(motion_dsa)
sw	$22,232($sp)
sw	$8,%lo(motion_dsa)($3)
sw	$21,228($sp)
sw	$20,224($sp)
sw	$19,220($sp)
sw	$18,216($sp)
sw	$17,212($sp)
sw	$16,208($sp)
lw	$3,21028($2)
lhu	$11,264($sp)
beq	$3,$0,$L129
lhu	$12,268($sp)

addiu	$2,$2,20004
lui	$3,%hi(motion_douty)
li	$8,321650688			# 0x132c0000
sw	$2,%lo(motion_douty)($3)
li	$9,-201326592			# 0xfffffffff4000000
li	$2,321650688			# 0x132c0000
addiu	$8,$8,20004
addiu	$10,$2,21128
addiu	$3,$2,21708
addiu	$9,$9,21708
li	$2,321191936			# 0x13250000
lui	$15,%hi(motion_doutc)
sw	$9,%lo(motion_doutc)($15)
sw	$8,28($2)
sw	$3,2076($2)
sw	$10,88($2)
sw	$10,2136($2)
beq	$4,$0,$L132
li	$2,1			# 0x1

$L233:
bne	$4,$2,$L232
lw	$31,244($sp)

sw	$0,176($sp)
sw	$0,180($sp)
sw	$0,168($sp)
beq	$5,$0,$L161
sw	$0,172($sp)

bne	$13,$0,$L162
addiu	$4,$sp,48

addiu	$10,$sp,112
addiu	$14,$sp,80
sw	$10,192($sp)
sw	$14,188($sp)
$L163:
andi	$2,$6,0x2
beq	$2,$0,$L169
lw	$14,272($sp)

subu	$20,$0,$11
subu	$19,$0,$12
lw	$24,192($sp)
move	$25,$4
lw	$15,188($sp)
sw	$6,184($sp)
sll	$fp,$11,1
sw	$4,196($sp)
sll	$23,$12,1
lw	$6,280($sp)
sll	$20,$20,1
lw	$4,276($sp)
sll	$19,$19,1
addiu	$2,$14,32
move	$17,$0
move	$16,$0
move	$5,$0
li	$31,-4			# 0xfffffffffffffffc
li	$22,-16			# 0xfffffffffffffff0
li	$21,4			# 0x4
$L174:
lw	$10,0($2)
andi	$14,$5,0x1
lw	$9,4($2)
addu	$3,$14,$fp
sra	$18,$10,2
andi	$8,$9,0x3
sw	$10,16($24)
sll	$3,$3,3
sw	$9,16($15)
sll	$8,$8,2
addu	$3,$3,$18
andi	$18,$10,0x3
or	$8,$8,$18
slt	$18,$3,-16
sw	$8,16($25)
beq	$18,$0,$L170
slt	$3,$3,$4

sw	$22,16($24)
$L171:
sra	$8,$5,1
sra	$3,$9,2
addu	$14,$8,$23
sll	$14,$14,3
addu	$3,$14,$3
slt	$14,$3,-16
beq	$14,$0,$L172
slt	$3,$3,$6

$L228:
sw	$22,16($15)
$L173:
srl	$8,$10,31
srl	$3,$9,31
addu	$10,$8,$10
addu	$9,$3,$9
sra	$10,$10,1
sra	$9,$9,1
addiu	$5,$5,1
addu	$16,$16,$10
addu	$17,$17,$9
addiu	$2,$2,8
addiu	$24,$24,4
addiu	$15,$15,4
bne	$5,$21,$L174
addiu	$25,$25,4

lui	$2,%hi(JZC_chroma_roundtab.2405)
lw	$4,196($sp)
andi	$5,$17,0xf
lw	$6,184($sp)
addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
andi	$3,$16,0xf
addu	$5,$5,$2
addu	$2,$3,$2
sra	$17,$17,3
lbu	$3,0($5)
sra	$16,$16,3
lbu	$2,0($2)
addu	$17,$3,$17
addu	$16,$2,$16
andi	$2,$17,0x1
sll	$2,$2,1
sw	$17,172($sp)
andi	$3,$16,0x1
sw	$16,180($sp)
or	$2,$2,$3
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,96($2)
sw	$2,164($sp)
$L169:
addiu	$2,$sp,160
lw	$3,188($sp)
sw	$7,20($sp)
move	$5,$12
sw	$4,24($sp)
andi	$6,$6,0x3
sw	$2,28($sp)
move	$4,$11
lw	$2,192($sp)
move	$7,$13
sw	$0,16($sp)
sw	$3,36($sp)
sw	$2,32($sp)
addiu	$2,$sp,176
sw	$2,40($sp)
addiu	$2,$sp,168
jal	mpeg4_qpel_4mv
sw	$2,44($sp)

lw	$31,244($sp)
$L232:
lw	$fp,240($sp)
lw	$23,236($sp)
lw	$22,232($sp)
lw	$21,228($sp)
lw	$20,224($sp)
lw	$19,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
j	$31
addiu	$sp,$sp,248

$L129:
addiu	$2,$2,19492
lui	$3,%hi(motion_douty)
li	$8,321650688			# 0x132c0000
sw	$2,%lo(motion_douty)($3)
li	$9,-201326592			# 0xfffffffff4000000
li	$2,321650688			# 0x132c0000
addiu	$8,$8,19492
addiu	$10,$2,21128
addiu	$3,$2,21708
addiu	$9,$9,21708
li	$2,321191936			# 0x13250000
lui	$15,%hi(motion_doutc)
sw	$9,%lo(motion_doutc)($15)
sw	$8,28($2)
sw	$3,2076($2)
sw	$10,88($2)
sw	$10,2136($2)
bne	$4,$0,$L233
li	$2,1			# 0x1

$L132:
beq	$5,$0,$L134
nop

bne	$13,$0,$L224
lw	$2,272($sp)

addiu	$4,$sp,48
$L135:
andi	$2,$6,0x2
beq	$2,$0,$L139
lw	$14,272($sp)

lw	$5,284($sp)
lw	$3,36($14)
andi	$2,$5,0x100
lw	$8,32($14)
andi	$5,$3,0x3
sll	$5,$5,2
sw	$3,84($sp)
andi	$9,$8,0x3
sw	$8,116($sp)
or	$5,$5,$9
beq	$2,$0,$L140
sw	$5,52($sp)

lui	$5,%hi(rtab.2443)
andi	$9,$8,0x7
andi	$2,$3,0x7
addiu	$5,$5,%lo(rtab.2443)
sll	$2,$2,2
sll	$9,$9,2
sra	$8,$8,1
addu	$9,$9,$5
addu	$5,$2,$5
sra	$2,$3,1
lw	$9,0($9)
lw	$3,0($5)
addu	$8,$8,$9
addu	$3,$2,$3
$L141:
sra	$2,$3,1
andi	$3,$3,0x1
or	$2,$3,$2
sra	$3,$8,1
sw	$2,172($sp)
andi	$8,$8,0x1
andi	$2,$2,0x1
or	$3,$8,$3
sll	$2,$2,1
andi	$5,$3,0x1
sw	$3,180($sp)
or	$2,$2,$5
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,96($2)
sw	$2,164($sp)
$L139:
addiu	$2,$sp,160
sw	$7,20($sp)
sw	$4,24($sp)
move	$5,$12
sw	$0,16($sp)
move	$4,$11
sw	$2,28($sp)
addiu	$2,$sp,112
andi	$6,$6,0x3
move	$7,$13
sw	$2,32($sp)
addiu	$2,$sp,80
sw	$2,36($sp)
addiu	$2,$sp,176
sw	$2,40($sp)
addiu	$2,$sp,168
jal	mpeg4_qpel
sw	$2,44($sp)

lw	$31,244($sp)
lw	$fp,240($sp)
lw	$23,236($sp)
lw	$22,232($sp)
lw	$21,228($sp)
lw	$20,224($sp)
lw	$19,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
j	$31
addiu	$sp,$sp,248

$L134:
beq	$13,$0,$L234
andi	$4,$6,0x2

lw	$2,272($sp)
lw	$5,0($2)
lw	$4,4($2)
andi	$3,$5,0x2
andi	$2,$4,0x1
sw	$5,112($sp)
sll	$2,$2,1
sw	$4,80($sp)
sra	$3,$3,1
sw	$5,176($sp)
andi	$8,$4,0x2
sw	$4,168($sp)
andi	$9,$5,0x1
or	$2,$2,$9
or	$3,$3,$8
addiu	$8,$5,1
sw	$2,48($sp)
or	$3,$3,$2
sltu	$8,$8,3
bne	$8,$0,$L144
sw	$3,160($sp)

sra	$5,$5,1
sw	$5,176($sp)
$L144:
addiu	$5,$4,1
sltu	$5,$5,3
beq	$5,$0,$L227
sra	$4,$4,1

$L145:
addiu	$4,$sp,48
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$4,$2
addu	$3,$4,$3
lw	$4,96($2)
lw	$2,96($3)
sw	$4,48($sp)
sw	$2,160($sp)
andi	$4,$6,0x2
$L234:
andi	$4,$4,0x00ff
beq	$4,$0,$L146
lw	$5,272($sp)

lw	$8,32($5)
lw	$5,36($5)
andi	$3,$8,0x2
andi	$2,$5,0x1
sw	$8,116($sp)
sll	$2,$2,1
sw	$5,84($sp)
sra	$3,$3,1
sw	$8,180($sp)
andi	$9,$5,0x2
sw	$5,172($sp)
andi	$10,$8,0x1
or	$2,$2,$10
or	$3,$3,$9
addiu	$9,$8,1
sw	$2,52($sp)
or	$3,$3,$2
sltu	$9,$9,3
bne	$9,$0,$L147
sw	$3,164($sp)

sra	$8,$8,1
sw	$8,180($sp)
$L147:
addiu	$8,$5,1
sltu	$8,$8,3
bne	$8,$0,$L235
addiu	$8,$sp,48

sra	$5,$5,1
sw	$5,172($sp)
$L235:
sll	$2,$2,2
sll	$3,$3,2
addu	$2,$8,$2
addu	$3,$8,$3
lw	$5,96($2)
lw	$2,96($3)
sw	$5,52($sp)
sw	$2,164($sp)
$L146:
andi	$6,$6,0x3
xori	$2,$6,0x3
li	$3,-1048444928			# 0xffffffffc1820000
li	$6,-1048313856			# 0xffffffffc1840000
li	$5,196608			# 0x30000
movn	$6,$3,$2
li	$3,131072			# 0x20000
sll	$12,$12,8
movz	$3,$5,$7
andi	$2,$11,0x00ff
andi	$11,$12,0xffff
or	$11,$2,$11
or	$11,$11,$6
li	$2,-201326592			# 0xfffffffff4000000
li	$5,321191936			# 0x13250000
sb	$0,21128($2)
sw	$3,48($5)
sw	$11,21032($2)
beq	$13,$0,$L195
addiu	$9,$2,21040

lw	$5,80($sp)
sll	$9,$13,31
lhu	$6,112($sp)
sll	$5,$5,16
or	$5,$6,$5
sw	$5,21036($2)
beq	$4,$0,$L152
li	$3,33554432			# 0x2000000

move	$2,$0
$L188:
li	$3,786432			# 0xc0000
li	$5,524288			# 0x80000
li	$8,-201326592			# 0xfffffffff4000000
movz	$5,$3,$7
li	$16,3			# 0x3
lw	$3,48($sp)
andi	$3,$3,0xf
ori	$3,$3,0xf0
or	$3,$3,$9
or	$2,$3,$2
or	$2,$2,$5
addiu	$9,$8,21048
sw	$2,21040($8)
addiu	$8,$8,21044
$L151:
beq	$4,$0,$L154
li	$3,786432			# 0xc0000

lw	$5,52($sp)
li	$2,524288			# 0x80000
lw	$15,84($sp)
move	$12,$3
lhu	$11,116($sp)
li	$3,1124073472			# 0x43000000
movn	$12,$2,$7
addiu	$6,$16,2
addiu	$3,$3,240
andi	$5,$5,0xf
sll	$10,$13,31
sll	$17,$6,2
or	$3,$5,$3
sll	$15,$15,16
li	$2,-201326592			# 0xfffffffff4000000
or	$3,$3,$10
addiu	$2,$2,21032
or	$5,$11,$15
or	$3,$3,$12
addiu	$11,$17,4
sw	$5,0($8)
sw	$3,0($9)
addu	$8,$17,$2
bne	$13,$0,$L156
addu	$9,$11,$2

$L192:
li	$2,131072			# 0x20000
lw	$4,172($sp)
li	$3,196608			# 0x30000
lhu	$5,180($sp)
addiu	$6,$6,2
movn	$3,$2,$7
li	$2,1107296256			# 0x42000000
sll	$6,$6,2
addiu	$2,$2,240
sll	$4,$4,16
move	$7,$3
lw	$3,164($sp)
or	$4,$5,$4
andi	$3,$3,0xf
sw	$4,0($8)
or	$2,$3,$2
li	$3,-201326592			# 0xfffffffff4000000
or	$2,$2,$10
addiu	$3,$3,21032
or	$7,$2,$7
addiu	$2,$6,4
sw	$7,0($9)
addu	$8,$6,$3
addu	$9,$2,$3
li	$2,321650688			# 0x132c0000
$L230:
lhu	$3,%lo(motion_dha)($14)
lw	$31,244($sp)
addiu	$2,$2,1
lw	$fp,240($sp)
lw	$23,236($sp)
or	$2,$3,$2
lw	$22,232($sp)
li	$3,-1048576000			# 0xffffffffc1800000
lw	$21,228($sp)
lw	$20,224($sp)
ori	$3,$3,0xffff
lw	$19,220($sp)
lw	$18,216($sp)
sw	$3,0($8)
li	$3,-1610612736			# 0xffffffffa0000000
lw	$17,212($sp)
ori	$3,$3,0xffff
lw	$16,208($sp)
sw	$3,0($9)
li	$3,321191936			# 0x13250000
sw	$2,84($3)
j	$31
addiu	$sp,$sp,248

$L161:
bne	$13,$0,$L175
addiu	$14,$sp,80

addiu	$5,$sp,112
addiu	$8,$sp,80
addiu	$4,$sp,48
sw	$5,192($sp)
sw	$8,188($sp)
$L176:
andi	$2,$6,0x2
beq	$2,$0,$L236
addiu	$2,$sp,160

lw	$8,272($sp)
subu	$21,$0,$11
subu	$20,$0,$12
lw	$17,192($sp)
lw	$16,188($sp)
sll	$24,$11,1
sw	$6,184($sp)
addiu	$5,$8,32
sw	$7,196($sp)
sll	$23,$12,1
lw	$6,276($sp)
sll	$21,$21,1
lw	$7,280($sp)
sll	$20,$20,1
move	$19,$0
move	$18,$0
move	$8,$0
li	$31,-2			# 0xfffffffffffffffe
li	$25,-16			# 0xfffffffffffffff0
li	$22,4			# 0x4
move	$15,$4
$L187:
lw	$10,0($5)
andi	$14,$8,0x1
lw	$9,4($5)
addu	$3,$14,$24
sra	$fp,$10,1
andi	$2,$9,0x1
sw	$10,16($17)
sll	$3,$3,3
sw	$9,16($16)
sll	$2,$2,1
addu	$3,$3,$fp
andi	$fp,$10,0x1
or	$2,$2,$fp
slt	$fp,$3,-16
sw	$2,16($15)
beq	$fp,$0,$L183
slt	$3,$3,$6

sw	$25,16($17)
$L184:
sra	$14,$8,1
sra	$3,$9,1
addu	$fp,$14,$23
sll	$fp,$fp,3
addu	$3,$fp,$3
slt	$fp,$3,-16
beq	$fp,$0,$L185
slt	$3,$3,$7

sw	$25,16($16)
$L186:
sll	$2,$2,2
addiu	$15,$15,4
addu	$2,$4,$2
addiu	$8,$8,1
addu	$18,$18,$10
lw	$2,96($2)
addu	$19,$19,$9
addiu	$5,$5,8
addiu	$17,$17,4
addiu	$16,$16,4
bne	$8,$22,$L187
sw	$2,12($15)

lui	$2,%hi(JZC_chroma_roundtab.2405)
lw	$6,184($sp)
andi	$5,$19,0xf
lw	$7,196($sp)
addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
andi	$3,$18,0xf
addu	$5,$5,$2
addu	$2,$3,$2
sra	$19,$19,3
lbu	$3,0($5)
sra	$18,$18,3
lbu	$2,0($2)
addu	$19,$3,$19
addu	$18,$2,$18
andi	$2,$19,0x1
sll	$2,$2,1
sw	$19,172($sp)
andi	$3,$18,0x1
sw	$18,180($sp)
or	$2,$2,$3
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,96($2)
sw	$2,164($sp)
addiu	$2,$sp,160
$L236:
lw	$5,192($sp)
lw	$8,188($sp)
andi	$6,$6,0x3
sw	$7,20($sp)
move	$7,$13
sw	$2,28($sp)
addiu	$2,$sp,176
sw	$4,24($sp)
move	$4,$11
sw	$5,32($sp)
move	$5,$12
sw	$2,40($sp)
addiu	$2,$sp,168
sw	$0,16($sp)
sw	$8,36($sp)
jal	mpeg4_hpel_4mv
sw	$2,44($sp)

lw	$31,244($sp)
lw	$fp,240($sp)
lw	$23,236($sp)
lw	$22,232($sp)
lw	$21,228($sp)
lw	$20,224($sp)
lw	$19,220($sp)
lw	$18,216($sp)
lw	$17,212($sp)
lw	$16,208($sp)
j	$31
addiu	$sp,$sp,248

$L170:
bne	$3,$0,$L171
subu	$14,$20,$14

sll	$14,$14,3
and	$8,$8,$31
addu	$14,$14,$4
sw	$8,16($25)
sra	$8,$5,1
sll	$14,$14,2
sra	$3,$9,2
sw	$14,16($24)
addu	$14,$8,$23
sll	$14,$14,3
addu	$3,$14,$3
slt	$14,$3,-16
bne	$14,$0,$L228
slt	$3,$3,$6

$L172:
bne	$3,$0,$L173
subu	$8,$19,$8

sll	$8,$8,3
lw	$3,16($25)
li	$14,-13			# 0xfffffffffffffff3
addu	$8,$8,$6
and	$3,$3,$14
sll	$8,$8,2
sw	$3,16($25)
j	$L173
sw	$8,16($15)

$L185:
bne	$3,$0,$L186
subu	$14,$20,$14

sll	$14,$14,3
li	$3,-3			# 0xfffffffffffffffd
addu	$14,$14,$7
and	$2,$2,$3
sll	$14,$14,1
j	$L186
sw	$14,16($16)

$L183:
bne	$3,$0,$L184
subu	$14,$21,$14

sll	$14,$14,3
and	$2,$2,$31
addu	$14,$14,$6
sw	$2,16($15)
sll	$14,$14,1
j	$L184
sw	$14,16($17)

$L175:
lw	$15,272($sp)
addiu	$10,$sp,112
sw	$6,196($sp)
subu	$21,$0,$11
sw	$7,200($sp)
subu	$20,$0,$12
sw	$14,188($sp)
addiu	$4,$sp,48
sw	$10,192($sp)
lw	$6,276($sp)
sll	$24,$11,1
lw	$7,280($sp)
sll	$23,$12,1
sll	$21,$21,1
sll	$20,$20,1
move	$19,$0
move	$18,$0
move	$5,$0
li	$31,-2			# 0xfffffffffffffffe
li	$25,-16			# 0xfffffffffffffff0
li	$22,4			# 0x4
move	$16,$14
move	$17,$10
move	$14,$4
$L181:
lw	$9,0($15)
andi	$10,$5,0x1
lw	$8,4($15)
addu	$3,$10,$24
sra	$fp,$9,1
andi	$2,$8,0x1
sw	$9,0($17)
sll	$3,$3,3
sw	$8,0($16)
sll	$2,$2,1
addu	$3,$3,$fp
andi	$fp,$9,0x1
or	$2,$2,$fp
slt	$fp,$3,-16
sw	$2,0($14)
beq	$fp,$0,$L177
slt	$3,$3,$6

sw	$25,0($17)
$L178:
sra	$10,$5,1
sra	$3,$8,1
addu	$fp,$10,$23
sll	$fp,$fp,3
addu	$3,$fp,$3
slt	$fp,$3,-16
beq	$fp,$0,$L179
slt	$3,$3,$7

sw	$25,0($16)
$L180:
sll	$2,$2,2
addiu	$14,$14,4
addu	$2,$4,$2
addiu	$5,$5,1
addu	$18,$18,$9
lw	$2,96($2)
addu	$19,$19,$8
addiu	$15,$15,8
addiu	$17,$17,4
addiu	$16,$16,4
bne	$5,$22,$L181
sw	$2,-4($14)

lui	$2,%hi(JZC_chroma_roundtab.2405)
lw	$6,196($sp)
andi	$5,$19,0xf
lw	$7,200($sp)
addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
andi	$3,$18,0xf
addu	$5,$5,$2
addu	$2,$3,$2
sra	$19,$19,3
lbu	$3,0($5)
sra	$18,$18,3
lbu	$2,0($2)
addu	$19,$3,$19
addu	$18,$2,$18
andi	$2,$19,0x1
sll	$2,$2,1
sw	$19,168($sp)
andi	$3,$18,0x1
sw	$18,176($sp)
or	$2,$2,$3
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,96($2)
j	$L176
sw	$2,160($sp)

$L179:
bne	$3,$0,$L180
subu	$10,$20,$10

sll	$10,$10,3
li	$3,-3			# 0xfffffffffffffffd
addu	$10,$10,$7
and	$2,$2,$3
sll	$10,$10,1
j	$L180
sw	$10,0($16)

$L177:
bne	$3,$0,$L178
subu	$10,$21,$10

sll	$10,$10,3
and	$2,$2,$31
addu	$10,$10,$6
sw	$2,0($14)
sll	$10,$10,1
j	$L178
sw	$10,0($17)

$L162:
lw	$14,272($sp)
addiu	$2,$sp,112
sw	$6,196($sp)
addiu	$3,$sp,80
lw	$6,280($sp)
subu	$22,$0,$11
sw	$4,200($sp)
subu	$21,$0,$12
sw	$2,192($sp)
move	$15,$4
sw	$3,188($sp)
lw	$4,276($sp)
sll	$25,$11,1
sll	$24,$12,1
sll	$22,$22,1
sll	$21,$21,1
move	$19,$0
move	$18,$0
move	$9,$0
li	$31,-4			# 0xfffffffffffffffc
li	$fp,-16			# 0xfffffffffffffff0
li	$23,4			# 0x4
move	$17,$2
move	$16,$3
$L168:
lw	$8,0($14)
andi	$10,$9,0x1
lw	$5,4($14)
addu	$2,$10,$25
sra	$20,$8,2
andi	$3,$5,0x3
sw	$8,0($17)
sll	$2,$2,3
sw	$5,0($16)
sll	$3,$3,2
addu	$2,$2,$20
andi	$20,$8,0x3
or	$3,$3,$20
slt	$20,$2,-16
sw	$3,0($15)
beq	$20,$0,$L164
slt	$2,$2,$4

sw	$fp,0($17)
$L165:
sra	$3,$9,1
sra	$2,$5,2
addu	$10,$3,$24
sll	$10,$10,3
addu	$2,$10,$2
slt	$10,$2,-16
beq	$10,$0,$L166
slt	$2,$2,$6

sw	$fp,0($16)
$L167:
srl	$3,$8,31
srl	$2,$5,31
addu	$8,$3,$8
addu	$5,$2,$5
sra	$8,$8,1
sra	$5,$5,1
addiu	$9,$9,1
addu	$18,$18,$8
addu	$19,$19,$5
addiu	$14,$14,8
addiu	$17,$17,4
addiu	$16,$16,4
bne	$9,$23,$L168
addiu	$15,$15,4

lui	$2,%hi(JZC_chroma_roundtab.2405)
lw	$4,200($sp)
andi	$5,$19,0xf
lw	$6,196($sp)
addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
andi	$3,$18,0xf
addu	$5,$5,$2
addu	$2,$3,$2
sra	$19,$19,3
lbu	$3,0($5)
sra	$18,$18,3
lbu	$2,0($2)
addu	$19,$3,$19
addu	$18,$2,$18
andi	$2,$19,0x1
sll	$2,$2,1
sw	$19,168($sp)
andi	$3,$18,0x1
sw	$18,176($sp)
or	$2,$2,$3
sll	$2,$2,2
addu	$2,$4,$2
lw	$2,96($2)
j	$L163
sw	$2,160($sp)

$L166:
bne	$2,$0,$L167
subu	$3,$21,$3

sll	$3,$3,3
lw	$2,0($15)
li	$10,-13			# 0xfffffffffffffff3
addu	$3,$3,$6
and	$2,$2,$10
sll	$3,$3,2
sw	$2,0($15)
j	$L167
sw	$3,0($16)

$L164:
bne	$2,$0,$L165
subu	$10,$22,$10

sll	$10,$10,3
and	$3,$3,$31
addu	$10,$10,$4
sw	$3,0($15)
sll	$10,$10,2
j	$L165
sw	$10,0($17)

$L195:
addiu	$8,$2,21036
j	$L151
li	$16,1			# 0x1

$L154:
beq	$13,$0,$L230
li	$2,321650688			# 0x132c0000

lw	$5,168($sp)
addiu	$6,$16,2
lhu	$2,176($sp)
sll	$10,$13,31
li	$3,33554432			# 0x2000000
sll	$5,$5,16
or	$2,$2,$5
sw	$2,0($8)
li	$5,131072			# 0x20000
$L231:
li	$8,196608			# 0x30000
sll	$2,$6,2
movn	$8,$5,$7
addiu	$11,$2,4
lw	$5,160($sp)
andi	$5,$5,0xf
ori	$5,$5,0xf0
or	$5,$5,$10
or	$3,$5,$3
li	$5,-201326592			# 0xfffffffff4000000
or	$3,$3,$8
addiu	$5,$5,21032
sw	$3,0($9)
addu	$8,$2,$5
bne	$4,$0,$L192
addu	$9,$11,$5

j	$L230
li	$2,321650688			# 0x132c0000

$L224:
lw	$8,272($sp)
lw	$4,284($sp)
lw	$3,4($2)
lw	$5,0($8)
andi	$2,$4,0x100
andi	$4,$3,0x3
sll	$4,$4,2
sw	$3,80($sp)
andi	$8,$5,0x3
sw	$5,112($sp)
or	$4,$4,$8
bne	$2,$0,$L229
sw	$4,48($sp)

lw	$10,284($sp)
andi	$2,$10,0x40
beq	$2,$0,$L138
srl	$2,$5,31

sra	$4,$5,1
sra	$2,$3,1
andi	$5,$5,0x1
andi	$3,$3,0x1
or	$5,$5,$4
or	$3,$3,$2
$L137:
sra	$2,$3,1
andi	$3,$3,0x1
or	$2,$3,$2
sra	$3,$5,1
sw	$2,168($sp)
andi	$5,$5,0x1
andi	$2,$2,0x1
or	$3,$5,$3
sll	$2,$2,1
andi	$4,$3,0x1
sw	$3,176($sp)
or	$2,$2,$4
sll	$2,$2,2
addiu	$4,$sp,48
addu	$2,$4,$2
lw	$2,96($2)
j	$L135
sw	$2,160($sp)

$L140:
lw	$10,284($sp)
andi	$5,$10,0x40
beq	$5,$0,$L142
srl	$2,$8,31

sra	$5,$8,1
sra	$2,$3,1
andi	$8,$8,0x1
andi	$3,$3,0x1
or	$8,$8,$5
j	$L141
or	$3,$3,$2

$L229:
lui	$4,%hi(rtab.2442)
andi	$8,$5,0x7
andi	$2,$3,0x7
addiu	$4,$4,%lo(rtab.2442)
sll	$2,$2,2
sll	$8,$8,2
sra	$5,$5,1
addu	$8,$8,$4
addu	$4,$2,$4
sra	$2,$3,1
lw	$8,0($8)
lw	$3,0($4)
addu	$5,$5,$8
j	$L137
addu	$3,$2,$3

$L152:
li	$2,16777216			# 0x1000000
j	$L188
or	$9,$9,$3

$L227:
j	$L145
sw	$4,168($sp)

$L156:
lw	$5,168($sp)
addiu	$6,$16,4
lhu	$2,176($sp)
move	$3,$0
sll	$5,$5,16
or	$2,$2,$5
sw	$2,0($8)
j	$L231
li	$5,131072			# 0x20000

$L142:
srl	$5,$3,31
addu	$8,$2,$8
addu	$3,$5,$3
sra	$8,$8,1
j	$L141
sra	$3,$3,1

$L138:
srl	$4,$3,31
addu	$5,$2,$5
addu	$3,$4,$3
sra	$5,$5,1
j	$L137
sra	$3,$3,1

.set	macro
.set	reorder
.end	MPV_motion_p1
.size	MPV_motion_p1, .-MPV_motion_p1
.align	2
.globl	ff_simple_idct_put_mxu
.set	nomips16
.set	nomicromips
.ent	ff_simple_idct_put_mxu
.type	ff_simple_idct_put_mxu, @function
ff_simple_idct_put_mxu:
.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
.mask	0x00000000,0
.fmask	0x00000000,0
li	$2,1518469120			# 0x5a820000
addiu	$3,$2,30274
#APP
# 480 "mpeg4_aux_idct.c" 1
.word	0b01110000000000110000000101101111	#S32I2M XR5,$3
# 0 "" 2
#NO_APP
addiu	$2,$2,12540
#APP
# 481 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000000110101111	#S32I2M XR6,$2
# 0 "" 2
#NO_APP
li	$2,2105999360			# 0x7d870000
addiu	$2,$2,27246
#APP
# 482 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000000111101111	#S32I2M XR7,$2
# 0 "" 2
#NO_APP
li	$2,418971648			# 0x18f90000
addiu	$2,$2,18205
#APP
# 483 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000001000101111	#S32I2M XR8,$2
# 0 "" 2
#NO_APP
li	$2,1785593856			# 0x6a6e0000
addiu	$2,$2,6393
#APP
# 484 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000001001101111	#S32I2M XR9,$2
# 0 "" 2
#NO_APP
li	$2,1193082880			# 0x471d0000
addiu	$2,$2,32135
#APP
# 485 "mpeg4_aux_idct.c" 1
.word	0b01110000000000100000001010101111	#S32I2M XR10,$2
# 0 "" 2
#NO_APP
move	$3,$0
.set	noreorder
.set	nomacro
beq	$5,$0,$L241
addiu	$2,$7,-16
.set	macro
.set	reorder

addiu	$2,$2,16
$L253:
#APP
# 492 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000000001010000	#S32LDD XR1,$2,0
# 0 "" 2
# 493 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000010010010000	#S32LDD XR2,$2,4
# 0 "" 2
# 494 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000100011010000	#S32LDD XR3,$2,8
# 0 "" 2
# 495 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000110100010000	#S32LDD XR4,$2,12
# 0 "" 2
# 497 "mpeg4_aux_idct.c" 1
.word	0b01110011000010001000010001111101	#S32SFL XR1,XR1,XR2,XR2,PTN3
# 0 "" 2
# 499 "mpeg4_aux_idct.c" 1
.word	0b01110011000100010000110011111101	#S32SFL XR3,XR3,XR4,XR4,PTN3
# 0 "" 2
# 501 "mpeg4_aux_idct.c" 1
.word	0b01110000001100010100101011001000	#D16MUL XR11,XR2,XR5,XR12,WW
# 0 "" 2
# 502 "mpeg4_aux_idct.c" 1
.word	0b01110000001100011001001011001010	#D16MAC XR11,XR4,XR6,XR12,AA,WW
# 0 "" 2
# 503 "mpeg4_aux_idct.c" 1
.word	0b01110000001110011000101101001000	#D16MUL XR13,XR2,XR6,XR14,WW
# 0 "" 2
# 504 "mpeg4_aux_idct.c" 1
.word	0b01110011001110010101001101001010	#D16MAC XR13,XR4,XR5,XR14,SS,WW
# 0 "" 2
# 505 "mpeg4_aux_idct.c" 1
.word	0b01110000100100011100010010001000	#D16MUL XR2,XR1,XR7,XR4,HW
# 0 "" 2
# 506 "mpeg4_aux_idct.c" 1
.word	0b01110001010100100100010010001010	#D16MAC XR2,XR1,XR9,XR4,AS,LW
# 0 "" 2
# 507 "mpeg4_aux_idct.c" 1
.word	0b01110001100100101000110010001010	#D16MAC XR2,XR3,XR10,XR4,AS,HW
# 0 "" 2
# 508 "mpeg4_aux_idct.c" 1
.word	0b01110001010100100000110010001010	#D16MAC XR2,XR3,XR8,XR4,AS,LW
# 0 "" 2
# 510 "mpeg4_aux_idct.c" 1
.word	0b01110000000100000000000010001011	#D16MACF XR2,XR0,XR0,XR4,AA,WW
# 0 "" 2
# 511 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 512 "mpeg4_aux_idct.c" 1
.word	0b01110000001110000000001100001011	#D16MACF XR12,XR0,XR0,XR14,AA,WW
# 0 "" 2
# 514 "mpeg4_aux_idct.c" 1
.word	0b01110000101111100000010100001000	#D16MUL XR4,XR1,XR8,XR15,HW
# 0 "" 2
# 515 "mpeg4_aux_idct.c" 1
.word	0b01110011011111101000010100001010	#D16MAC XR4,XR1,XR10,XR15,SS,LW
# 0 "" 2
# 516 "mpeg4_aux_idct.c" 1
.word	0b01110000101111100100110100001010	#D16MAC XR4,XR3,XR9,XR15,AA,HW
# 0 "" 2
# 517 "mpeg4_aux_idct.c" 1
.word	0b01110010011111011100110100001010	#D16MAC XR4,XR3,XR7,XR15,SA,LW
# 0 "" 2
# 518 "mpeg4_aux_idct.c" 1
.word	0b01110001001100110010111011001110	#Q16ADD XR11,XR11,XR12,XR12,AS,WW
# 0 "" 2
# 520 "mpeg4_aux_idct.c" 1
.word	0b01110000000100000000001111001011	#D16MACF XR15,XR0,XR0,XR4,AA,WW
# 0 "" 2
# 521 "mpeg4_aux_idct.c" 1
.word	0b01110001000010001010111011001110	#Q16ADD XR11,XR11,XR2,XR2,AS,WW
# 0 "" 2
# 522 "mpeg4_aux_idct.c" 1
.word	0b01110001111111111111001100001110	#Q16ADD XR12,XR12,XR15,XR15,AS,XW
# 0 "" 2
# 525 "mpeg4_aux_idct.c" 1
.word	0b01110011001100110010111011111101	#S32SFL XR11,XR11,XR12,XR12,PTN3
# 0 "" 2
# 527 "mpeg4_aux_idct.c" 1
.word	0b01110011001011101111001100111101	#S32SFL XR12,XR12,XR11,XR11,PTN3
# 0 "" 2
# 529 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000001100010001	#S32STD XR12,$2,0
# 0 "" 2
# 530 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000011011010001	#S32STD XR11,$2,4
# 0 "" 2
# 531 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000101111010001	#S32STD XR15,$2,8
# 0 "" 2
# 532 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000000110010010001	#S32STD XR2,$2,12
# 0 "" 2
#NO_APP
addiu	$3,$3,1
.set	noreorder
.set	nomacro
bne	$3,$5,$L253
addiu	$2,$2,16
.set	macro
.set	reorder

$L241:
slt	$5,$5,5
.set	noreorder
.set	nomacro
bne	$5,$0,$L252
addiu	$3,$7,-4
.set	macro
.set	reorder

li	$5,1518469120			# 0x5a820000
addiu	$7,$7,12
addiu	$5,$5,30274
$L242:
addiu	$3,$3,4
#APP
# 543 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000000000001010000	#S32LDD XR1,$3,0
# 0 "" 2
# 545 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000010000011010000	#S32LDD XR3,$3,32
# 0 "" 2
# 546 "mpeg4_aux_idct.c" 1
.word	0b01110000000001010000000101101111	#S32I2M XR5,$5
# 0 "" 2
# 547 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000100001011010000	#S32LDD XR11,$3,64
# 0 "" 2
# 548 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000110001101010000	#S32LDD XR13,$3,96
# 0 "" 2
# 550 "mpeg4_aux_idct.c" 1
.word	0b01110000101001000101011111001000	#D16MUL XR15,XR5,XR1,XR9,HW
# 0 "" 2
# 551 "mpeg4_aux_idct.c" 1
.word	0b01110000101001101101011111001010	#D16MAC XR15,XR5,XR11,XR9,AA,HW
# 0 "" 2
# 553 "mpeg4_aux_idct.c" 1
.word	0b01110000001001000000001111001011	#D16MACF XR15,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 555 "mpeg4_aux_idct.c" 1
.word	0b01110000011001001101011010001000	#D16MUL XR10,XR5,XR3,XR9,LW
# 0 "" 2
# 556 "mpeg4_aux_idct.c" 1
.word	0b01110000011001110101101010001010	#D16MAC XR10,XR6,XR13,XR9,AA,LW
# 0 "" 2
# 558 "mpeg4_aux_idct.c" 1
.word	0b01110000001001000000001010001011	#D16MACF XR10,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 560 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000001000010010000	#S32LDD XR2,$3,16
# 0 "" 2
# 561 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000011000100010000	#S32LDD XR4,$3,48
# 0 "" 2
# 562 "mpeg4_aux_idct.c" 1
.word	0b01110001001001101011111111001110	#Q16ADD XR15,XR15,XR10,XR9,AS,WW
# 0 "" 2
# 563 "mpeg4_aux_idct.c" 1
.word	0b01110000100001000101011010001000	#D16MUL XR10,XR5,XR1,XR1,HW
# 0 "" 2
# 564 "mpeg4_aux_idct.c" 1
.word	0b01110011100001101101011010001010	#D16MAC XR10,XR5,XR11,XR1,SS,HW
# 0 "" 2
# 566 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000000001010001011	#D16MACF XR10,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 568 "mpeg4_aux_idct.c" 1
.word	0b01110000010001001101101011001000	#D16MUL XR11,XR6,XR3,XR1,LW
# 0 "" 2
# 569 "mpeg4_aux_idct.c" 1
.word	0b01110011010001110101011011001010	#D16MAC XR11,XR5,XR13,XR1,SS,LW
# 0 "" 2
# 571 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000000001011001011	#D16MACF XR11,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 573 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000101001100010000	#S32LDD XR12,$3,80
# 0 "" 2
# 574 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000111001110010000	#S32LDD XR14,$3,112
# 0 "" 2
# 575 "mpeg4_aux_idct.c" 1
.word	0b01110001000001101110101010001110	#Q16ADD XR10,XR10,XR11,XR1,AS,WW
# 0 "" 2
# 576 "mpeg4_aux_idct.c" 1
.word	0b01110000101101001001111011001000	#D16MUL XR11,XR7,XR2,XR13,HW
# 0 "" 2
# 577 "mpeg4_aux_idct.c" 1
.word	0b01110000011101010001111011001010	#D16MAC XR11,XR7,XR4,XR13,AA,LW
# 0 "" 2
# 578 "mpeg4_aux_idct.c" 1
.word	0b01110000011101110010001011001010	#D16MAC XR11,XR8,XR12,XR13,AA,LW
# 0 "" 2
# 579 "mpeg4_aux_idct.c" 1
.word	0b01110000101101111010001011001010	#D16MAC XR11,XR8,XR14,XR13,AA,HW
# 0 "" 2
# 581 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 583 "mpeg4_aux_idct.c" 1
.word	0b01110000011101001001110011001000	#D16MUL XR3,XR7,XR2,XR13,LW
# 0 "" 2
# 584 "mpeg4_aux_idct.c" 1
.word	0b01110011101101010010000011001010	#D16MAC XR3,XR8,XR4,XR13,SS,HW
# 0 "" 2
# 585 "mpeg4_aux_idct.c" 1
.word	0b01110011101101110001110011001010	#D16MAC XR3,XR7,XR12,XR13,SS,HW
# 0 "" 2
# 586 "mpeg4_aux_idct.c" 1
.word	0b01110011011101111010000011001010	#D16MAC XR3,XR8,XR14,XR13,SS,LW
# 0 "" 2
# 588 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000011001011	#D16MACF XR3,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 590 "mpeg4_aux_idct.c" 1
.word	0b01110000011101001010000101001000	#D16MUL XR5,XR8,XR2,XR13,LW
# 0 "" 2
# 591 "mpeg4_aux_idct.c" 1
.word	0b01110011101101010001110101001010	#D16MAC XR5,XR7,XR4,XR13,SS,HW
# 0 "" 2
# 592 "mpeg4_aux_idct.c" 1
.word	0b01110000101101110010000101001010	#D16MAC XR5,XR8,XR12,XR13,AA,HW
# 0 "" 2
# 593 "mpeg4_aux_idct.c" 1
.word	0b01110000011101111001110101001010	#D16MAC XR5,XR7,XR14,XR13,AA,LW
# 0 "" 2
# 595 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000101001011	#D16MACF XR5,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 597 "mpeg4_aux_idct.c" 1
.word	0b01110000101101001010000010001000	#D16MUL XR2,XR8,XR2,XR13,HW
# 0 "" 2
# 598 "mpeg4_aux_idct.c" 1
.word	0b01110011011101010010000010001010	#D16MAC XR2,XR8,XR4,XR13,SS,LW
# 0 "" 2
# 599 "mpeg4_aux_idct.c" 1
.word	0b01110000011101110001110010001010	#D16MAC XR2,XR7,XR12,XR13,AA,LW
# 0 "" 2
# 600 "mpeg4_aux_idct.c" 1
.word	0b01110011101101111001110010001010	#D16MAC XR2,XR7,XR14,XR13,SS,HW
# 0 "" 2
# 602 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000010001011	#D16MACF XR2,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 604 "mpeg4_aux_idct.c" 1
.word	0b01110001001011101111111111001110	#Q16ADD XR15,XR15,XR11,XR11,AS,WW
# 0 "" 2
# 605 "mpeg4_aux_idct.c" 1
.word	0b01110001000011001110101010001110	#Q16ADD XR10,XR10,XR3,XR3,AS,WW
# 0 "" 2
# 606 "mpeg4_aux_idct.c" 1
.word	0b01110001000101010100010001001110	#Q16ADD XR1,XR1,XR5,XR5,AS,WW
# 0 "" 2
# 607 "mpeg4_aux_idct.c" 1
.word	0b01110001000010001010011001001110	#Q16ADD XR9,XR9,XR2,XR2,AS,WW
# 0 "" 2
# 609 "mpeg4_aux_idct.c" 1
.word	0b01110000000110101011111111000111	#Q16SAT XR15,XR15,XR10
# 0 "" 2
# 610 "mpeg4_aux_idct.c" 1
.word	0b01110000000110100100010001000111	#Q16SAT XR1,XR1,XR9
# 0 "" 2
# 611 "mpeg4_aux_idct.c" 1
.word	0b01110000000110010100100010000111	#Q16SAT XR2,XR2,XR5
# 0 "" 2
# 612 "mpeg4_aux_idct.c" 1
.word	0b01110000000110101100110011000111	#Q16SAT XR3,XR3,XR11
# 0 "" 2
# 617 "mpeg4_aux_idct.c" 1
.word	0b01110000100010000000001111101011	#S16STD XR15,$4,0,PTN1
# 0 "" 2
#NO_APP
move	$2,$4
#APP
# 618 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010001111101101	#S16SDI XR15,$2,16,PTN0
# 0 "" 2
# 619 "mpeg4_aux_idct.c" 1
.word	0b01110000010010000010000001101101	#S16SDI XR1,$2,16,PTN1
# 0 "" 2
# 620 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010000001101101	#S16SDI XR1,$2,16,PTN0
# 0 "" 2
# 621 "mpeg4_aux_idct.c" 1
.word	0b01110000010010000010000010101101	#S16SDI XR2,$2,16,PTN1
# 0 "" 2
# 622 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010000010101101	#S16SDI XR2,$2,16,PTN0
# 0 "" 2
# 623 "mpeg4_aux_idct.c" 1
.word	0b01110000010010000010000011101101	#S16SDI XR3,$2,16,PTN1
# 0 "" 2
# 624 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010000011101011	#S16STD XR3,$2,16,PTN0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$3,$7,$L242
addiu	$4,$4,2
.set	macro
.set	reorder

j	$31
$L252:
li	$5,1518469120			# 0x5a820000
addiu	$7,$7,12
addiu	$5,$5,30274
$L243:
addiu	$3,$3,4
#APP
# 633 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000000000001010000	#S32LDD XR1,$3,0
# 0 "" 2
# 635 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000010000011010000	#S32LDD XR3,$3,32
# 0 "" 2
# 636 "mpeg4_aux_idct.c" 1
.word	0b01110000000001010000000101101111	#S32I2M XR5,$5
# 0 "" 2
# 640 "mpeg4_aux_idct.c" 1
.word	0b01110000101001000101011111001000	#D16MUL XR15,XR5,XR1,XR9,HW
# 0 "" 2
# 643 "mpeg4_aux_idct.c" 1
.word	0b01110000001001000000001111001011	#D16MACF XR15,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 645 "mpeg4_aux_idct.c" 1
.word	0b01110000011001001101011010001000	#D16MUL XR10,XR5,XR3,XR9,LW
# 0 "" 2
# 648 "mpeg4_aux_idct.c" 1
.word	0b01110000001001000000001010001011	#D16MACF XR10,XR0,XR0,XR9,AA,WW
# 0 "" 2
# 650 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000001000010010000	#S32LDD XR2,$3,16
# 0 "" 2
# 651 "mpeg4_aux_idct.c" 1
.word	0b01110000011000000011000100010000	#S32LDD XR4,$3,48
# 0 "" 2
# 652 "mpeg4_aux_idct.c" 1
.word	0b01110001001001101011111111001110	#Q16ADD XR15,XR15,XR10,XR9,AS,WW
# 0 "" 2
# 653 "mpeg4_aux_idct.c" 1
.word	0b01110000100001000101011010001000	#D16MUL XR10,XR5,XR1,XR1,HW
# 0 "" 2
# 656 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000000001010001011	#D16MACF XR10,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 658 "mpeg4_aux_idct.c" 1
.word	0b01110000010001001101101011001000	#D16MUL XR11,XR6,XR3,XR1,LW
# 0 "" 2
# 661 "mpeg4_aux_idct.c" 1
.word	0b01110000000001000000001011001011	#D16MACF XR11,XR0,XR0,XR1,AA,WW
# 0 "" 2
# 665 "mpeg4_aux_idct.c" 1
.word	0b01110001000001101110101010001110	#Q16ADD XR10,XR10,XR11,XR1,AS,WW
# 0 "" 2
# 666 "mpeg4_aux_idct.c" 1
.word	0b01110000101101001001111011001000	#D16MUL XR11,XR7,XR2,XR13,HW
# 0 "" 2
# 667 "mpeg4_aux_idct.c" 1
.word	0b01110000011101010001111011001010	#D16MAC XR11,XR7,XR4,XR13,AA,LW
# 0 "" 2
# 671 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000001011001011	#D16MACF XR11,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 673 "mpeg4_aux_idct.c" 1
.word	0b01110000011101001001110011001000	#D16MUL XR3,XR7,XR2,XR13,LW
# 0 "" 2
# 674 "mpeg4_aux_idct.c" 1
.word	0b01110011101101010010000011001010	#D16MAC XR3,XR8,XR4,XR13,SS,HW
# 0 "" 2
# 678 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000011001011	#D16MACF XR3,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 680 "mpeg4_aux_idct.c" 1
.word	0b01110000011101001010000101001000	#D16MUL XR5,XR8,XR2,XR13,LW
# 0 "" 2
# 681 "mpeg4_aux_idct.c" 1
.word	0b01110011101101010001110101001010	#D16MAC XR5,XR7,XR4,XR13,SS,HW
# 0 "" 2
# 685 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000101001011	#D16MACF XR5,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 687 "mpeg4_aux_idct.c" 1
.word	0b01110000101101001010000010001000	#D16MUL XR2,XR8,XR2,XR13,HW
# 0 "" 2
# 688 "mpeg4_aux_idct.c" 1
.word	0b01110011011101010010000010001010	#D16MAC XR2,XR8,XR4,XR13,SS,LW
# 0 "" 2
# 692 "mpeg4_aux_idct.c" 1
.word	0b01110000001101000000000010001011	#D16MACF XR2,XR0,XR0,XR13,AA,WW
# 0 "" 2
# 694 "mpeg4_aux_idct.c" 1
.word	0b01110001001011101111111111001110	#Q16ADD XR15,XR15,XR11,XR11,AS,WW
# 0 "" 2
# 695 "mpeg4_aux_idct.c" 1
.word	0b01110001000011001110101010001110	#Q16ADD XR10,XR10,XR3,XR3,AS,WW
# 0 "" 2
# 696 "mpeg4_aux_idct.c" 1
.word	0b01110001000101010100010001001110	#Q16ADD XR1,XR1,XR5,XR5,AS,WW
# 0 "" 2
# 697 "mpeg4_aux_idct.c" 1
.word	0b01110001000010001010011001001110	#Q16ADD XR9,XR9,XR2,XR2,AS,WW
# 0 "" 2
# 699 "mpeg4_aux_idct.c" 1
.word	0b01110000000110101011111111000111	#Q16SAT XR15,XR15,XR10
# 0 "" 2
# 700 "mpeg4_aux_idct.c" 1
.word	0b01110000000110100100010001000111	#Q16SAT XR1,XR1,XR9
# 0 "" 2
# 701 "mpeg4_aux_idct.c" 1
.word	0b01110000000110010100100010000111	#Q16SAT XR2,XR2,XR5
# 0 "" 2
# 702 "mpeg4_aux_idct.c" 1
.word	0b01110000000110101100110011000111	#Q16SAT XR3,XR3,XR11
# 0 "" 2
# 706 "mpeg4_aux_idct.c" 1
.word	0b01110000100010000000001111101011	#S16STD XR15,$4,0,PTN1
# 0 "" 2
#NO_APP
move	$2,$4
#APP
# 707 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010001111101101	#S16SDI XR15,$2,16,PTN0
# 0 "" 2
# 708 "mpeg4_aux_idct.c" 1
.word	0b01110000010010000010000001101101	#S16SDI XR1,$2,16,PTN1
# 0 "" 2
# 709 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010000001101101	#S16SDI XR1,$2,16,PTN0
# 0 "" 2
# 710 "mpeg4_aux_idct.c" 1
.word	0b01110000010010000010000010101101	#S16SDI XR2,$2,16,PTN1
# 0 "" 2
# 711 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010000010101101	#S16SDI XR2,$2,16,PTN0
# 0 "" 2
# 712 "mpeg4_aux_idct.c" 1
.word	0b01110000010010000010000011101101	#S16SDI XR3,$2,16,PTN1
# 0 "" 2
# 713 "mpeg4_aux_idct.c" 1
.word	0b01110000010000000010000011101011	#S16STD XR3,$2,16,PTN0
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
bne	$3,$7,$L243
addiu	$4,$4,2
.set	macro
.set	reorder

j	$31
.end	ff_simple_idct_put_mxu
.size	ff_simple_idct_put_mxu, .-ff_simple_idct_put_mxu
.section	.p1_main,"ax",@progbits
.align	2
.globl	main
.set	nomips16
.set	nomicromips
.ent	main
.type	main, @function
main:
.frame	$sp,80,$31		# vars= 0, regs= 10/0, args= 40, gp= 0
.mask	0xc0ff0000,-4
.fmask	0x00000000,0
la	$sp, 0xF400BFFC
addiu	$sp,$sp,-80
li	$2,3			# 0x3
sw	$31,76($sp)
sw	$fp,72($sp)
sw	$23,68($sp)
sw	$22,64($sp)
sw	$21,60($sp)
sw	$20,56($sp)
sw	$19,52($sp)
sw	$18,48($sp)
sw	$17,44($sp)
sw	$16,40($sp)
#APP
# 54 "msmpeg4_p1.c" 1
.word	0b01110000000000100000010000101111	#S32I2M XR16,$2
# 0 "" 2
#NO_APP
li	$2,-201326592			# 0xfffffffff4000000
li	$4,321585152			# 0x132b0000
addiu	$3,$2,16388
sw	$0,16388($2)
addiu	$4,$4,4096
addiu	$3,$3,18472
lui	$23,%hi(dMB_X)
sw	$4,16392($2)
addiu	$5,$2,16864
sw	$3,%lo(dMB_X)($23)
li	$3,4194304			# 0x400000
lui	$22,%hi(dMB_L)
addiu	$7,$2,16416
addiu	$6,$2,17740
sw	$5,%lo(dMB_L)($22)
addiu	$4,$2,18616
addiu	$3,$3,64
lui	$21,%hi(dFRM)
lui	$18,%hi(dMB)
lui	$5,%hi(dMB_N)
sw	$7,%lo(dFRM)($21)
sw	$6,%lo(dMB)($18)
sw	$4,%lo(dMB_N)($5)
sw	$3,21140($2)
li	$2,321650688			# 0x132c0000
li	$3,320929792			# 0x13210000
addiu	$2,$2,21132
sw	$2,0($3)
li	$4,-201326592			# 0xfffffffff4000000
$L520:
lw	$3,16384($4)
lw	$2,16388($4)
addiu	$2,$2,2
slt	$2,$2,$3
beq	$2,$0,$L520
lw	$2,16392($4)
li	$3,320929792			# 0x13210000
sw	$2,21132($4)
li	$2,321650688			# 0x132c0000
addiu	$2,$2,17740
sw	$2,21136($4)
li	$2,-2143289344			# 0xffffffff80400000
addiu	$2,$2,876
sw	$2,21144($4)
li	$4,1			# 0x1
li	$2,320929792			# 0x13210000
sw	$4,4($2)
$L256:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L256
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

li	$5,321585152			# 0x132b0000
lw	$4,16388($2)
addiu	$6,$5,15509
addiu	$4,$4,1
sw	$4,16388($2)
lw	$4,16392($2)
addiu	$4,$4,876
sw	$4,16392($2)
lw	$3,16392($2)
sltu	$3,$3,$6
.set	noreorder
.set	nomacro
bne	$3,$0,$L257
addiu	$5,$5,4096
.set	macro
.set	reorder

sw	$5,16392($2)
$L257:
li	$2,-201326592			# 0xfffffffff4000000
li	$3,321585152			# 0x132b0000
li	$6,1			# 0x1
lw	$4,16392($2)
li	$5,320929792			# 0x13210000
sw	$4,4($3)
li	$4,-2143289344			# 0xffffffff80400000
li	$3,321650688			# 0x132c0000
addiu	$4,$4,876
addiu	$3,$3,18616
sw	$4,21144($2)
sw	$3,21136($2)
li	$3,320929792			# 0x13210000
lw	$4,16392($2)
sw	$6,4($5)
sw	$4,21132($2)
$L258:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L258
li	$2,-201326592			# 0xfffffffff4000000
.set	macro
.set	reorder

li	$3,1			# 0x1
li	$5,321585152			# 0x132b0000
sw	$0,21964($2)
sw	$3,21968($2)
addiu	$6,$5,15509
lw	$4,16388($2)
addiu	$4,$4,1
sw	$4,16388($2)
lw	$4,16392($2)
addiu	$4,$4,876
sw	$4,16392($2)
lw	$3,16392($2)
sltu	$3,$3,$6
.set	noreorder
.set	nomacro
bne	$3,$0,$L259
addiu	$5,$5,4096
.set	macro
.set	reorder

sw	$5,16392($2)
$L259:
li	$2,-201326592			# 0xfffffffff4000000
li	$4,1048576			# 0x100000
li	$5,321585152			# 0x132b0000
lw	$6,16392($2)
li	$3,262144			# 0x40000
lw	$20,16812($2)
addiu	$9,$4,256
addiu	$3,$3,4
lw	$19,16816($2)
lb	$7,17740($2)
addiu	$5,$5,16
sw	$6,-12($5)
addiu	$6,$4,16
addiu	$4,$4,128
sw	$3,21204($2)
sw	$3,21208($2)
addiu	$20,$20,-512
sw	$3,21220($2)
addiu	$19,$19,-256
sw	$4,21192($2)
li	$4,321650688			# 0x132c0000
sw	$3,21224($2)
sw	$3,21300($2)
li	$3,-2147221504			# 0xffffffff80040000
addiu	$4,$4,21968
sw	$6,21172($2)
addiu	$3,$3,4
sw	$6,21188($2)
sw	$9,21176($2)
addiu	$6,$2,17740
sw	$5,21292($2)
sw	$4,21296($2)
.set	noreorder
.set	nomacro
blez	$7,$L370
sw	$3,21304($2)
.set	macro
.set	reorder

li	$fp,-201326592			# 0xfffffffff4000000
move	$17,$0
$L369:
lbu	$9,76($6)
addiu	$20,$20,256
lbu	$10,77($6)
.set	noreorder
.set	nomacro
bne	$9,$0,$L262
addiu	$19,$19,128
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
bne	$10,$0,$L521
lw	$3,%lo(dMB_L)($22)
.set	macro
.set	reorder

lbu	$3,2($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L482
lw	$3,%lo(dFRM)($21)
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L365:
lw	$6,16384($fp)
lw	$3,16388($fp)
addiu	$3,$3,2
slt	$3,$3,$6
beq	$3,$0,$L365
.set	noreorder
.set	nomacro
beq	$17,$0,$L522
lw	$4,%lo(dMB)($18)
.set	macro
.set	reorder

$L448:
lw	$3,21964($fp)
beq	$3,$0,$L448
sw	$0,21964($fp)
li	$7,321585152			# 0x132b0000
lw	$4,16392($fp)
addiu	$9,$7,15508
addiu	$4,$4,876
sw	$4,16392($fp)
lw	$3,16392($fp)
sltu	$3,$3,$9
.set	noreorder
.set	nomacro
bne	$3,$0,$L368
addiu	$7,$7,4096
.set	macro
.set	reorder

sw	$7,16392($fp)
$L368:
lw	$6,16392($fp)
li	$3,321585152			# 0x132b0000
sw	$6,4($3)
lw	$3,16388($fp)
addiu	$3,$3,1
sw	$3,16388($fp)
$L366 = .
lw	$4,%lo(dMB)($18)
$L522:
lui	$2,%hi(dMB_N)
li	$7,321650688			# 0x132c0000
lw	$3,%lo(dMB_X)($23)
sw	$5,%lo(dMB_X)($23)
addiu	$17,$17,1
lw	$6,%lo(dMB_N)($2)
addiu	$5,$7,21964
sw	$4,%lo(dMB_L)($22)
li	$4,4194304			# 0x400000
sw	$3,%lo(dMB_N)($2)
andi	$3,$3,0xffff
addiu	$4,$4,876
lw	$9,16392($fp)
sw	$6,%lo(dMB)($18)
or	$3,$3,$7
sw	$5,21152($fp)
li	$5,262144			# 0x40000
sw	$4,21144($fp)
li	$4,321585152			# 0x132b0000
addiu	$5,$5,4
sw	$3,21136($fp)
addiu	$4,$4,12
sw	$9,21132($fp)
li	$3,320929792			# 0x13210000
sw	$5,21156($fp)
li	$5,-2147221504			# 0xffffffff80040000
sw	$4,21148($fp)
li	$4,1			# 0x1
addiu	$5,$5,4
sw	$5,21160($fp)
sw	$4,4($3)
lb	$3,0($6)
bgtz	$3,$L369
$L370:
li	$3,320995328			# 0x13220000
$L261:
lw	$2,4($3)
andi	$2,$2,0x4
.set	noreorder
.set	nomacro
beq	$2,$0,$L261
li	$2,321585152			# 0x132b0000
.set	macro
.set	reorder

li	$3,1			# 0x1
sw	$3,0($2)
#APP
# 295 "msmpeg4_p1.c" 1
nop	#i_nop
# 0 "" 2
# 296 "msmpeg4_p1.c" 1
nop	#i_nop
# 0 "" 2
# 297 "msmpeg4_p1.c" 1
nop	#i_nop
# 0 "" 2
# 298 "msmpeg4_p1.c" 1
nop	#i_nop
# 0 "" 2
# 299 "msmpeg4_p1.c" 1
wait	#i_wait
# 0 "" 2
#NO_APP
lw	$31,76($sp)
move	$2,$0
lw	$fp,72($sp)
lw	$23,68($sp)
lw	$22,64($sp)
lw	$21,60($sp)
lw	$20,56($sp)
lw	$19,52($sp)
lw	$18,48($sp)
lw	$17,44($sp)
lw	$16,40($sp)
.set	noreorder
.set	nomacro
j	$31
addiu	$sp,$sp,80
.set	macro
.set	reorder

$L482:
addiu	$9,$6,8
lbu	$4,4($6)
lbu	$6,3($6)
lbu	$5,5($3)
lbu	$7,4($3)
sw	$0,16($sp)
sw	$0,20($sp)
sw	$9,24($sp)
lw	$9,12($3)
sw	$9,28($sp)
lw	$9,16($3)
sw	$9,32($sp)
lw	$3,32($3)
.set	noreorder
.set	nomacro
jal	MPV_motion_p1
sw	$3,36($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L365
lw	$5,%lo(dMB_L)($22)
.set	macro
.set	reorder

$L262:
lw	$3,%lo(dMB_L)($22)
$L521:
lbu	$3,2($3)
.set	noreorder
.set	nomacro
beq	$3,$0,$L483
lui	$2,%hi(motion_dsa)
.set	macro
.set	reorder

lw	$3,21028($fp)
$L512:
.set	noreorder
.set	nomacro
bne	$3,$0,$L268
addiu	$16,$fp,21028
.set	macro
.set	reorder

li	$3,1			# 0x1
sw	$3,21028($fp)
lw	$3,%lo(dMB)($18)
lbu	$4,2($3)
.set	noreorder
.set	nomacro
beq	$4,$0,$L484
addiu	$16,$16,-1536
.set	macro
.set	reorder

$L270:
lw	$5,%lo(dMB_L)($22)
$L511:
lbu	$3,2($5)
.set	noreorder
.set	nomacro
bne	$3,$0,$L271
lw	$6,%lo(dFRM)($21)
.set	macro
.set	reorder

lw	$3,%lo(dFRM)($21)
lb	$4,31($3)
bne	$4,$0,$L272
lb	$6,7($3)
addiu	$4,$6,-1
andi	$4,$4,0x00ff
sltu	$4,$4,2
.set	noreorder
.set	nomacro
bne	$4,$0,$L273
li	$4,13			# 0xd
.set	macro
.set	reorder

beq	$6,$4,$L485
lbu	$7,78($5)
$L514:
addiu	$4,$5,108
move	$5,$0
.set	noreorder
.set	nomacro
jal	add_dequant_dct_opt.constprop.2
move	$6,$16
.set	macro
.set	reorder

li	$5,1			# 0x1
lw	$3,%lo(dMB_L)($22)
addiu	$6,$16,8
addiu	$4,$3,236
.set	noreorder
.set	nomacro
jal	add_dequant_dct_opt.constprop.2
lbu	$7,78($3)
.set	macro
.set	reorder

li	$5,2			# 0x2
lw	$3,%lo(dMB_L)($22)
addiu	$6,$16,128
addiu	$4,$3,364
.set	noreorder
.set	nomacro
jal	add_dequant_dct_opt.constprop.2
lbu	$7,78($3)
.set	macro
.set	reorder

li	$5,3			# 0x3
lw	$3,%lo(dMB_L)($22)
addiu	$6,$16,136
addiu	$4,$3,492
.set	noreorder
.set	nomacro
jal	add_dequant_dct_opt.constprop.2
lbu	$7,78($3)
.set	macro
.set	reorder

li	$5,4			# 0x4
lw	$3,%lo(dMB_L)($22)
addiu	$6,$16,256
addiu	$4,$3,620
.set	noreorder
.set	nomacro
jal	add_dequant_dct_opt.constprop.2
lbu	$7,79($3)
.set	macro
.set	reorder

li	$5,5			# 0x5
lw	$3,%lo(dMB_L)($22)
addiu	$6,$16,264
addiu	$4,$3,748
.set	noreorder
.set	nomacro
jal	add_dequant_dct_opt.constprop.2
lbu	$7,79($3)
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L275:
beq	$17,$0,$L365
$L447:
lw	$6,21968($fp)
.set	noreorder
.set	nomacro
beq	$6,$0,$L447
li	$4,321585152			# 0x132b0000
.set	macro
.set	reorder

sw	$0,21968($fp)
li	$7,321650688			# 0x132c0000
sw	$20,21168($fp)
addiu	$4,$4,16
sw	$19,21184($fp)
addiu	$9,$7,23020
addiu	$6,$16,256
sw	$4,21196($fp)
andi	$16,$16,0xffff
sw	$4,21212($fp)
andi	$6,$6,0xffff
sw	$4,21228($fp)
li	$4,262144			# 0x40000
addiu	$11,$7,21968
sw	$9,21200($fp)
addiu	$4,$4,4
sw	$9,21216($fp)
addiu	$10,$7,21164
li	$2,-201326592			# 0xfffffffff4000000
sw	$11,21232($fp)
sw	$4,21236($fp)
li	$4,-2147221504			# 0xffffffff80040000
li	$3,1			# 0x1
addiu	$4,$4,4
li	$9,320995328			# 0x13220000
or	$16,$16,$7
sw	$4,21240($fp)
or	$4,$6,$7
ori	$2,$2,0x52ac
sw	$10,0($9)
sw	$16,0($2)
sw	$4,21180($fp)
sw	$3,4($9)
j	$L365
$L271:
lbu	$3,7($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$0,$L281
lw	$7,%lo(dMB)($18)
.set	macro
.set	reorder

lb	$3,41($6)
addiu	$7,$5,108
.set	noreorder
.set	nomacro
bne	$3,$0,$L486
lbu	$10,78($5)
.set	macro
.set	reorder

lbu	$3,6($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
bne	$3,$0,$L487
$L289:
li	$5,8			# 0x8
$L519:
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$16,8
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,236
.set	noreorder
.set	nomacro
bne	$3,$0,$L287
lbu	$11,78($5)
.set	macro
.set	reorder

$L475:
lb	$9,7($6)
li	$3,2			# 0x2
beq	$9,$3,$L287
lbu	$3,6($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
bne	$3,$0,$L488
$L303:
li	$5,8			# 0x8
$L518:
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,128
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,364
.set	noreorder
.set	nomacro
bne	$3,$0,$L301
lbu	$11,78($5)
.set	macro
.set	reorder

$L476:
lb	$9,7($6)
li	$3,2			# 0x2
beq	$9,$3,$L301
lbu	$3,6($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
bne	$3,$0,$L489
$L317:
li	$5,8			# 0x8
$L517:
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,136
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,492
.set	noreorder
.set	nomacro
bne	$3,$0,$L315
lbu	$11,78($5)
.set	macro
.set	reorder

$L477:
lb	$9,7($6)
li	$3,2			# 0x2
beq	$9,$3,$L315
lbu	$3,6($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
bne	$3,$0,$L490
$L331:
li	$5,8			# 0x8
$L516:
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,256
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,620
.set	noreorder
.set	nomacro
bne	$3,$0,$L329
lbu	$11,79($5)
.set	macro
.set	reorder

$L478:
lb	$9,7($6)
li	$3,2			# 0x2
beq	$9,$3,$L329
lbu	$3,6($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
bne	$3,$0,$L491
$L345:
li	$5,8			# 0x8
$L515:
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,264
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,748
.set	noreorder
.set	nomacro
bne	$3,$0,$L343
lbu	$11,79($5)
.set	macro
.set	reorder

$L479:
lb	$9,7($6)
li	$3,2			# 0x2
beq	$9,$3,$L343
lbu	$3,6($6)
addiu	$3,$3,-1
andi	$3,$3,0x00ff
sltu	$3,$3,2
bne	$3,$0,$L492
$L357:
li	$5,8			# 0x8
$L513:
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L275
lw	$5,%lo(dMB_L)($22)
.set	macro
.set	reorder

$L268:
lw	$3,%lo(dMB)($18)
sw	$0,21028($fp)
lbu	$4,2($3)
.set	noreorder
.set	nomacro
bne	$4,$0,$L270
addiu	$16,$16,-1024
.set	macro
.set	reorder

$L484:
lw	$11,%lo(dFRM)($21)
addiu	$12,$3,8
lbu	$4,4($3)
lbu	$6,3($3)
lbu	$5,5($11)
lbu	$7,4($11)
sw	$9,16($sp)
sw	$10,20($sp)
sw	$12,24($sp)
lw	$3,12($11)
sw	$3,28($sp)
lw	$3,16($11)
sw	$3,32($sp)
lw	$3,32($11)
.set	noreorder
.set	nomacro
jal	MPV_motion_p1
sw	$3,36($sp)
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L511
lw	$5,%lo(dMB_L)($22)
.set	macro
.set	reorder

$L483:
li	$4,-2147483648			# 0xffffffff80000000
lw	$5,%lo(motion_dsa)($2)
ori	$4,$4,0xffff
$L266:
lw	$3,0($5)
bne	$3,$4,$L266
lw	$4,21028($fp)
.set	noreorder
.set	nomacro
bne	$4,$0,$L371
addiu	$3,$fp,21028
.set	macro
.set	reorder

addiu	$3,$3,-1296
$L267:
addiu	$4,$fp,21700
#APP
# 63 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100001010100	#S32LDI XR1,$4,8
# 0 "" 2
# 64 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 65 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000011010000	#S32LDD XR3,$4,64
# 0 "" 2
# 66 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100010100010000	#S32LDD XR4,$4,68
# 0 "" 2
# 67 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100101010100	#S32LDI XR5,$4,8
# 0 "" 2
# 68 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010110010000	#S32LDD XR6,$4,4
# 0 "" 2
# 69 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000111010000	#S32LDD XR7,$4,64
# 0 "" 2
# 70 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100011000010000	#S32LDD XR8,$4,68
# 0 "" 2
# 72 "mpeg4_p1_mc.c" 1
pref 30,0($3)
# 0 "" 2
# 73 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000001010101	#S32SDI XR1,$3,16
# 0 "" 2
# 74 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 75 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100011010001	#S32STD XR3,$3,8
# 0 "" 2
# 76 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000110100010001	#S32STD XR4,$3,12
# 0 "" 2
# 77 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000101010101	#S32SDI XR5,$3,16
# 0 "" 2
# 78 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010110010001	#S32STD XR6,$3,4
# 0 "" 2
# 79 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100111010001	#S32STD XR7,$3,8
# 0 "" 2
# 80 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000111000010001	#S32STD XR8,$3,12
# 0 "" 2
# 82 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100001010100	#S32LDI XR1,$4,8
# 0 "" 2
# 83 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 84 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000011010000	#S32LDD XR3,$4,64
# 0 "" 2
# 85 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100010100010000	#S32LDD XR4,$4,68
# 0 "" 2
# 86 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100101010100	#S32LDI XR5,$4,8
# 0 "" 2
# 87 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010110010000	#S32LDD XR6,$4,4
# 0 "" 2
# 88 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000111010000	#S32LDD XR7,$4,64
# 0 "" 2
# 89 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100011000010000	#S32LDD XR8,$4,68
# 0 "" 2
# 91 "mpeg4_p1_mc.c" 1
pref 30,0($3)
# 0 "" 2
# 92 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000001010101	#S32SDI XR1,$3,16
# 0 "" 2
# 93 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 94 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100011010001	#S32STD XR3,$3,8
# 0 "" 2
# 95 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000110100010001	#S32STD XR4,$3,12
# 0 "" 2
# 96 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000101010101	#S32SDI XR5,$3,16
# 0 "" 2
# 97 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010110010001	#S32STD XR6,$3,4
# 0 "" 2
# 98 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100111010001	#S32STD XR7,$3,8
# 0 "" 2
# 99 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000111000010001	#S32STD XR8,$3,12
# 0 "" 2
# 101 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100001010100	#S32LDI XR1,$4,8
# 0 "" 2
# 102 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 103 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000011010000	#S32LDD XR3,$4,64
# 0 "" 2
# 104 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100010100010000	#S32LDD XR4,$4,68
# 0 "" 2
# 105 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100101010100	#S32LDI XR5,$4,8
# 0 "" 2
# 106 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010110010000	#S32LDD XR6,$4,4
# 0 "" 2
# 107 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000111010000	#S32LDD XR7,$4,64
# 0 "" 2
# 108 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100011000010000	#S32LDD XR8,$4,68
# 0 "" 2
# 110 "mpeg4_p1_mc.c" 1
pref 30,0($3)
# 0 "" 2
# 111 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000001010101	#S32SDI XR1,$3,16
# 0 "" 2
# 112 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 113 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100011010001	#S32STD XR3,$3,8
# 0 "" 2
# 114 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000110100010001	#S32STD XR4,$3,12
# 0 "" 2
# 115 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000101010101	#S32SDI XR5,$3,16
# 0 "" 2
# 116 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010110010001	#S32STD XR6,$3,4
# 0 "" 2
# 117 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100111010001	#S32STD XR7,$3,8
# 0 "" 2
# 118 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000111000010001	#S32STD XR8,$3,12
# 0 "" 2
# 120 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100001010100	#S32LDI XR1,$4,8
# 0 "" 2
# 121 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010010010000	#S32LDD XR2,$4,4
# 0 "" 2
# 122 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000011010000	#S32LDD XR3,$4,64
# 0 "" 2
# 123 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100010100010000	#S32LDD XR4,$4,68
# 0 "" 2
# 124 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000100101010100	#S32LDI XR5,$4,8
# 0 "" 2
# 125 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000000010110010000	#S32LDD XR6,$4,4
# 0 "" 2
# 126 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100000111010000	#S32LDD XR7,$4,64
# 0 "" 2
# 127 "mpeg4_p1_mc.c" 1
.word	0b01110000100000000100011000010000	#S32LDD XR8,$4,68
# 0 "" 2
# 129 "mpeg4_p1_mc.c" 1
pref 30,0($3)
# 0 "" 2
# 130 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000001010101	#S32SDI XR1,$3,16
# 0 "" 2
# 131 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010010010001	#S32STD XR2,$3,4
# 0 "" 2
# 132 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100011010001	#S32STD XR3,$3,8
# 0 "" 2
# 133 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000110100010001	#S32STD XR4,$3,12
# 0 "" 2
# 134 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000001000101010101	#S32SDI XR5,$3,16
# 0 "" 2
# 135 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000010110010001	#S32STD XR6,$3,4
# 0 "" 2
# 136 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000100111010001	#S32STD XR7,$3,8
# 0 "" 2
# 137 "mpeg4_p1_mc.c" 1
.word	0b01110000011000000000111000010001	#S32STD XR8,$3,12
# 0 "" 2
#NO_APP
.set	noreorder
.set	nomacro
j	$L512
lw	$3,21028($fp)
.set	macro
.set	reorder

$L272:
lb	$4,7($3)
li	$3,19			# 0x13
beq	$4,$3,$L275
$L273:
lw	$3,80($5)
bltz	$3,$L276
addiu	$5,$5,108
.set	noreorder
.set	nomacro
jal	ff_simple_idct_add_mxu.constprop.3
move	$4,$16
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L276:
lw	$3,84($5)
bltz	$3,$L277
addiu	$5,$5,236
.set	noreorder
.set	nomacro
jal	ff_simple_idct_add_mxu.constprop.3
addiu	$4,$16,8
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L277:
lw	$3,88($5)
bltz	$3,$L278
addiu	$5,$5,364
.set	noreorder
.set	nomacro
jal	ff_simple_idct_add_mxu.constprop.3
addiu	$4,$16,128
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L278:
lw	$3,92($5)
bltz	$3,$L279
addiu	$5,$5,492
.set	noreorder
.set	nomacro
jal	ff_simple_idct_add_mxu.constprop.3
addiu	$4,$16,136
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L279:
lw	$3,96($5)
bltz	$3,$L280
addiu	$5,$5,620
.set	noreorder
.set	nomacro
jal	ff_simple_idct_add_mxu.constprop.3
addiu	$4,$16,256
.set	macro
.set	reorder

lw	$5,%lo(dMB_L)($22)
$L280:
lw	$3,100($5)
bltz	$3,$L275
addiu	$5,$5,748
.set	noreorder
.set	nomacro
jal	ff_simple_idct_add_mxu.constprop.3
addiu	$4,$16,264
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L275
lw	$5,%lo(dMB_L)($22)
.set	macro
.set	reorder

$L281:
li	$5,8			# 0x8
li	$6,16			# 0x10
move	$4,$16
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
addiu	$7,$7,108
.set	macro
.set	reorder

addiu	$4,$16,8
lw	$7,%lo(dMB)($18)
li	$5,8			# 0x8
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
addiu	$7,$7,236
.set	macro
.set	reorder

addiu	$4,$16,128
lw	$7,%lo(dMB)($18)
li	$5,8			# 0x8
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
addiu	$7,$7,364
.set	macro
.set	reorder

addiu	$4,$16,136
lw	$7,%lo(dMB)($18)
li	$5,8			# 0x8
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
addiu	$7,$7,492
.set	macro
.set	reorder

addiu	$4,$16,256
lw	$7,%lo(dMB)($18)
li	$5,8			# 0x8
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
addiu	$7,$7,620
.set	macro
.set	reorder

li	$5,8			# 0x8
lw	$7,%lo(dMB)($18)
addiu	$4,$16,264
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
addiu	$7,$7,748
.set	macro
.set	reorder

lui	$2,%hi(dMB_L)
.set	noreorder
.set	nomacro
j	$L275
lw	$5,%lo(dMB_L)($2)
.set	macro
.set	reorder

$L486:
lhu	$4,108($5)
addiu	$3,$5,110
lbu	$11,5($5)
addiu	$6,$6,46
addiu	$9,$5,236
mul	$4,$4,$11
sh	$4,108($5)
$L286:
lh	$4,0($3)
beq	$4,$0,$L283
.set	noreorder
.set	nomacro
bltz	$4,$L493
mul	$5,$10,$4
.set	macro
.set	reorder

lhu	$4,0($6)
mul	$4,$5,$4
sra	$4,$4,3
$L285:
sh	$4,0($3)
$L283:
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$9,$L286
addiu	$6,$6,2
.set	macro
.set	reorder

li	$5,8			# 0x8
li	$6,16			# 0x10
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
move	$4,$16
.set	macro
.set	reorder

addiu	$4,$16,8
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,236
.set	noreorder
.set	nomacro
beq	$3,$0,$L475
lbu	$11,78($5)
.set	macro
.set	reorder

$L287:
lhu	$9,236($5)
addiu	$3,$5,238
lbu	$12,5($5)
addiu	$6,$6,46
addiu	$10,$5,364
mul	$9,$9,$12
sh	$9,236($5)
$L300:
lh	$5,0($3)
beq	$5,$0,$L297
.set	noreorder
.set	nomacro
bltz	$5,$L494
mul	$9,$11,$5
.set	macro
.set	reorder

lhu	$5,0($6)
mul	$5,$9,$5
sra	$5,$5,3
$L299:
sh	$5,0($3)
$L297:
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$10,$L300
addiu	$6,$6,2
.set	macro
.set	reorder

li	$5,8			# 0x8
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,128
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,364
.set	noreorder
.set	nomacro
beq	$3,$0,$L476
lbu	$11,78($5)
.set	macro
.set	reorder

$L301:
lhu	$9,364($5)
addiu	$3,$5,366
lbu	$12,5($5)
addiu	$6,$6,46
addiu	$10,$5,492
mul	$9,$9,$12
sh	$9,364($5)
$L314:
lh	$5,0($3)
beq	$5,$0,$L311
.set	noreorder
.set	nomacro
bltz	$5,$L495
mul	$9,$11,$5
.set	macro
.set	reorder

lhu	$5,0($6)
mul	$5,$9,$5
sra	$5,$5,3
$L313:
sh	$5,0($3)
$L311:
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$10,$L314
addiu	$6,$6,2
.set	macro
.set	reorder

li	$5,8			# 0x8
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,136
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,492
.set	noreorder
.set	nomacro
beq	$3,$0,$L477
lbu	$11,78($5)
.set	macro
.set	reorder

$L315:
lhu	$9,492($5)
addiu	$3,$5,494
lbu	$12,5($5)
addiu	$6,$6,46
addiu	$10,$5,620
mul	$9,$9,$12
sh	$9,492($5)
$L328:
lh	$5,0($3)
beq	$5,$0,$L325
.set	noreorder
.set	nomacro
bltz	$5,$L496
mul	$9,$11,$5
.set	macro
.set	reorder

lhu	$5,0($6)
mul	$5,$9,$5
sra	$5,$5,3
$L327:
sh	$5,0($3)
$L325:
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$10,$L328
addiu	$6,$6,2
.set	macro
.set	reorder

li	$5,8			# 0x8
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,256
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,620
.set	noreorder
.set	nomacro
beq	$3,$0,$L478
lbu	$11,79($5)
.set	macro
.set	reorder

$L329:
lhu	$9,620($5)
addiu	$3,$5,622
lbu	$12,104($5)
addiu	$6,$6,46
addiu	$10,$5,748
mul	$9,$9,$12
sh	$9,620($5)
$L342:
lh	$5,0($3)
beq	$5,$0,$L339
.set	noreorder
.set	nomacro
bltz	$5,$L497
mul	$9,$11,$5
.set	macro
.set	reorder

lhu	$5,0($6)
mul	$5,$9,$5
sra	$5,$5,3
$L341:
sh	$5,0($3)
$L339:
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$10,$L342
addiu	$6,$6,2
.set	macro
.set	reorder

li	$5,8			# 0x8
.set	noreorder
.set	nomacro
jal	ff_simple_idct_put_mxu
li	$6,16			# 0x10
.set	macro
.set	reorder

addiu	$4,$16,264
lw	$6,%lo(dFRM)($21)
lw	$5,%lo(dMB_L)($22)
lb	$3,41($6)
addiu	$7,$5,748
.set	noreorder
.set	nomacro
beq	$3,$0,$L479
lbu	$11,79($5)
.set	macro
.set	reorder

$L343:
lhu	$9,748($5)
addiu	$3,$5,750
lbu	$12,104($5)
addiu	$6,$6,46
addiu	$10,$5,876
mul	$9,$9,$12
sh	$9,748($5)
$L356:
lh	$5,0($3)
beq	$5,$0,$L353
.set	noreorder
.set	nomacro
bltz	$5,$L498
mul	$9,$11,$5
.set	macro
.set	reorder

lhu	$5,0($6)
mul	$5,$9,$5
sra	$5,$5,3
$L355:
sh	$5,0($3)
$L353:
addiu	$3,$3,2
.set	noreorder
.set	nomacro
bne	$3,$10,$L356
addiu	$6,$6,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L513
li	$5,8			# 0x8
.set	macro
.set	reorder

$L493:
subu	$4,$0,$4
lhu	$5,0($6)
mul	$4,$10,$4
mul	$4,$4,$5
sra	$4,$4,3
.set	noreorder
.set	nomacro
j	$L285
subu	$4,$0,$4
.set	macro
.set	reorder

$L495:
subu	$5,$0,$5
lhu	$9,0($6)
mul	$5,$11,$5
mul	$5,$5,$9
sra	$5,$5,3
.set	noreorder
.set	nomacro
j	$L313
subu	$5,$0,$5
.set	macro
.set	reorder

$L494:
subu	$5,$0,$5
lhu	$9,0($6)
mul	$5,$11,$5
mul	$5,$5,$9
sra	$5,$5,3
.set	noreorder
.set	nomacro
j	$L299
subu	$5,$0,$5
.set	macro
.set	reorder

$L498:
subu	$5,$0,$5
lhu	$9,0($6)
mul	$5,$11,$5
mul	$5,$5,$9
sra	$5,$5,3
.set	noreorder
.set	nomacro
j	$L355
subu	$5,$0,$5
.set	macro
.set	reorder

$L497:
subu	$5,$0,$5
lhu	$9,0($6)
mul	$5,$11,$5
mul	$5,$5,$9
sra	$5,$5,3
.set	noreorder
.set	nomacro
j	$L341
subu	$5,$0,$5
.set	macro
.set	reorder

$L496:
subu	$5,$0,$5
lhu	$9,0($6)
mul	$5,$11,$5
mul	$5,$5,$9
sra	$5,$5,3
.set	noreorder
.set	nomacro
j	$L327
subu	$5,$0,$5
.set	macro
.set	reorder

$L371:
.set	noreorder
.set	nomacro
j	$L267
addiu	$3,$3,-784
.set	macro
.set	reorder

$L485:
lb	$3,41($3)
beq	$3,$0,$L273
.set	noreorder
.set	nomacro
j	$L514
lbu	$7,78($5)
.set	macro
.set	reorder

$L492:
lb	$3,29($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L499
sll	$12,$11,1
.set	macro
.set	reorder

move	$3,$0
$L358:
lbu	$9,105($5)
.set	noreorder
.set	nomacro
bne	$9,$0,$L359
li	$11,63			# 0x3f
.set	macro
.set	reorder

lw	$9,100($5)
addu	$6,$6,$9
lbu	$11,172($6)
beq	$11,$0,$L357
$L359:
addiu	$5,$5,750
li	$9,1			# 0x1
$L363:
lh	$6,0($5)
beq	$6,$0,$L360
mul	$2,$12,$6
.set	noreorder
.set	nomacro
bltz	$6,$L500
addu	$10,$2,$3
.set	macro
.set	reorder

$L362:
sh	$10,0($5)
$L360:
addiu	$9,$9,1
slt	$6,$11,$9
.set	noreorder
.set	nomacro
beq	$6,$0,$L363
addiu	$5,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L513
li	$5,8			# 0x8
.set	macro
.set	reorder

$L491:
lb	$3,29($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L501
sll	$12,$11,1
.set	macro
.set	reorder

move	$3,$0
$L346:
lbu	$9,105($5)
.set	noreorder
.set	nomacro
bne	$9,$0,$L347
li	$11,63			# 0x3f
.set	macro
.set	reorder

lw	$9,96($5)
addu	$6,$6,$9
lbu	$11,172($6)
beq	$11,$0,$L345
$L347:
addiu	$5,$5,622
li	$9,1			# 0x1
$L351:
lh	$6,0($5)
beq	$6,$0,$L348
mul	$2,$12,$6
.set	noreorder
.set	nomacro
bltz	$6,$L502
addu	$10,$2,$3
.set	macro
.set	reorder

$L350:
sh	$10,0($5)
$L348:
addiu	$9,$9,1
slt	$6,$11,$9
.set	noreorder
.set	nomacro
beq	$6,$0,$L351
addiu	$5,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L515
li	$5,8			# 0x8
.set	macro
.set	reorder

$L490:
lb	$3,29($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L503
sll	$12,$11,1
.set	macro
.set	reorder

move	$3,$0
$L332:
lbu	$9,105($5)
.set	noreorder
.set	nomacro
bne	$9,$0,$L333
li	$11,63			# 0x3f
.set	macro
.set	reorder

lw	$9,92($5)
addu	$6,$6,$9
lbu	$11,172($6)
beq	$11,$0,$L331
$L333:
addiu	$5,$5,494
li	$9,1			# 0x1
$L337:
lh	$6,0($5)
beq	$6,$0,$L334
mul	$2,$12,$6
.set	noreorder
.set	nomacro
bltz	$6,$L504
addu	$10,$2,$3
.set	macro
.set	reorder

$L336:
sh	$10,0($5)
$L334:
addiu	$9,$9,1
slt	$6,$11,$9
.set	noreorder
.set	nomacro
beq	$6,$0,$L337
addiu	$5,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L516
li	$5,8			# 0x8
.set	macro
.set	reorder

$L489:
lb	$3,29($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L505
sll	$12,$11,1
.set	macro
.set	reorder

move	$3,$0
$L318:
lbu	$9,105($5)
.set	noreorder
.set	nomacro
bne	$9,$0,$L319
li	$11,63			# 0x3f
.set	macro
.set	reorder

lw	$9,88($5)
addu	$6,$6,$9
lbu	$11,172($6)
beq	$11,$0,$L317
$L319:
addiu	$5,$5,366
li	$9,1			# 0x1
$L323:
lh	$6,0($5)
beq	$6,$0,$L320
mul	$2,$12,$6
.set	noreorder
.set	nomacro
bltz	$6,$L506
addu	$10,$2,$3
.set	macro
.set	reorder

$L322:
sh	$10,0($5)
$L320:
addiu	$9,$9,1
slt	$6,$11,$9
.set	noreorder
.set	nomacro
beq	$6,$0,$L323
addiu	$5,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L517
li	$5,8			# 0x8
.set	macro
.set	reorder

$L488:
lb	$3,29($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L507
sll	$12,$11,1
.set	macro
.set	reorder

move	$3,$0
$L304:
lbu	$9,105($5)
.set	noreorder
.set	nomacro
bne	$9,$0,$L305
li	$11,63			# 0x3f
.set	macro
.set	reorder

lw	$9,84($5)
addu	$6,$6,$9
lbu	$11,172($6)
beq	$11,$0,$L303
$L305:
addiu	$5,$5,238
li	$9,1			# 0x1
$L309:
lh	$6,0($5)
beq	$6,$0,$L306
mul	$2,$12,$6
.set	noreorder
.set	nomacro
bltz	$6,$L508
addu	$10,$2,$3
.set	macro
.set	reorder

$L308:
sh	$10,0($5)
$L306:
addiu	$9,$9,1
slt	$6,$11,$9
.set	noreorder
.set	nomacro
beq	$6,$0,$L309
addiu	$5,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L518
li	$5,8			# 0x8
.set	macro
.set	reorder

$L487:
lb	$3,29($6)
.set	noreorder
.set	nomacro
beq	$3,$0,$L509
sll	$11,$10,1
.set	macro
.set	reorder

move	$3,$0
$L290:
lbu	$4,105($5)
.set	noreorder
.set	nomacro
bne	$4,$0,$L291
li	$10,63			# 0x3f
.set	macro
.set	reorder

lw	$4,80($5)
addu	$6,$6,$4
lbu	$10,172($6)
beq	$10,$0,$L289
$L291:
addiu	$5,$5,110
li	$6,1			# 0x1
$L295:
lh	$4,0($5)
beq	$4,$0,$L292
mul	$2,$11,$4
.set	noreorder
.set	nomacro
bltz	$4,$L510
addu	$9,$2,$3
.set	macro
.set	reorder

$L294:
sh	$9,0($5)
$L292:
addiu	$6,$6,1
slt	$4,$10,$6
.set	noreorder
.set	nomacro
beq	$4,$0,$L295
addiu	$5,$5,2
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L519
li	$5,8			# 0x8
.set	macro
.set	reorder

$L509:
lhu	$4,108($5)
addiu	$3,$10,-1
lbu	$9,5($5)
ori	$3,$3,0x1
mul	$4,$4,$9
.set	noreorder
.set	nomacro
j	$L290
sh	$4,108($5)
.set	macro
.set	reorder

$L507:
lhu	$9,236($5)
addiu	$3,$11,-1
lbu	$10,5($5)
ori	$3,$3,0x1
mul	$9,$9,$10
.set	noreorder
.set	nomacro
j	$L304
sh	$9,236($5)
.set	macro
.set	reorder

$L505:
lhu	$9,364($5)
addiu	$3,$11,-1
lbu	$10,5($5)
ori	$3,$3,0x1
mul	$9,$9,$10
.set	noreorder
.set	nomacro
j	$L318
sh	$9,364($5)
.set	macro
.set	reorder

$L503:
lhu	$9,492($5)
addiu	$3,$11,-1
lbu	$10,5($5)
ori	$3,$3,0x1
mul	$9,$9,$10
.set	noreorder
.set	nomacro
j	$L332
sh	$9,492($5)
.set	macro
.set	reorder

$L501:
lhu	$9,620($5)
addiu	$3,$11,-1
lbu	$10,104($5)
ori	$3,$3,0x1
mul	$9,$9,$10
.set	noreorder
.set	nomacro
j	$L346
sh	$9,620($5)
.set	macro
.set	reorder

$L499:
lhu	$9,748($5)
addiu	$3,$11,-1
lbu	$10,104($5)
ori	$3,$3,0x1
mul	$9,$9,$10
.set	noreorder
.set	nomacro
j	$L358
sh	$9,748($5)
.set	macro
.set	reorder

$L500:
mul	$6,$12,$6
.set	noreorder
.set	nomacro
j	$L362
subu	$10,$6,$3
.set	macro
.set	reorder

$L502:
mul	$6,$12,$6
.set	noreorder
.set	nomacro
j	$L350
subu	$10,$6,$3
.set	macro
.set	reorder

$L504:
mul	$6,$12,$6
.set	noreorder
.set	nomacro
j	$L336
subu	$10,$6,$3
.set	macro
.set	reorder

$L506:
mul	$6,$12,$6
.set	noreorder
.set	nomacro
j	$L322
subu	$10,$6,$3
.set	macro
.set	reorder

$L508:
mul	$6,$12,$6
.set	noreorder
.set	nomacro
j	$L308
subu	$10,$6,$3
.set	macro
.set	reorder

$L510:
mul	$4,$11,$4
.set	noreorder
.set	nomacro
j	$L294
subu	$9,$4,$3
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L291
li	$10,63			# 0x3f
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L319
li	$11,63			# 0x3f
.set	macro
.set	reorder

.set	noreorder
.set	nomacro
j	$L347
li	$11,63			# 0x3f
.set	macro
.set	reorder

.end	main
.size	main, .-main
.rdata
.align	2
.type	JZC_chroma_roundtab.2405, @object
.size	JZC_chroma_roundtab.2405, 16
JZC_chroma_roundtab.2405:
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.byte	1
.byte	1
.byte	1
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	0
.byte	1
.byte	1
.align	2
.type	rtab.2443, @object
.size	rtab.2443, 32
rtab.2443:
.word	0
.word	0
.word	1
.word	1
.word	0
.word	0
.word	0
.word	1
.align	2
.type	rtab.2442, @object
.size	rtab.2442, 32
rtab.2442:
.word	0
.word	0
.word	1
.word	1
.word	0
.word	0
.word	0
.word	1

.comm	dMB_X,4,4

.comm	dMB_N,4,4

.comm	dMB_L,4,4

.comm	dMB,4,4

.comm	dFRM,4,4

.comm	current_picture_ptr,12,4
.data
.align	2
.type	whirl_idct, @object
.size	whirl_idct, 24
whirl_idct:
.word	1518499394
.word	1518481660
.word	2106026606
.word	418989853
.word	1785600249
.word	1193115015

.comm	motion_iwta,4,4

.comm	motion_doutc,4,4

.comm	motion_douty,4,4

.comm	motion_dsa,4,4

.comm	motion_dha,4,4

.comm	motion_buf,4,4

.comm	mc_base,4,4
.globl	OFFTAB
.align	2
.type	OFFTAB, @object
.size	OFFTAB, 16
OFFTAB:
.word	0
.word	4
.word	32
.word	36
.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
