	.file	1 "mpeg4_p1.c"
	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 1
	.text
	.align	2
	.globl	mpeg4_hpel_4mv
	.set	nomips16
	.set	nomicromips
	.ent	mpeg4_hpel_4mv
	.type	mpeg4_hpel_4mv, @function
mpeg4_hpel_4mv:
	.frame	$sp,24,$31		# vars= 0, regs= 6/0, args= 0, gp= 0
	.mask	0x003f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	li	$2,131072			# 0x20000
	li	$3,196608			# 0x30000
	sw	$21,20($sp)
	lw	$9,44($sp)
	lui	$8,%hi(motion_dha)
	lw	$11,48($sp)
	sll	$5,$5,8
	lw	$10,52($sp)
	andi	$4,$4,0xff
	movn	$3,$2,$9
	lui	$2,%hi(motion_dsa)
	lw	$12,56($sp)
	lw	$14,%lo(motion_dsa)($2)
	lw	$2,%lo(motion_dha)($8)
	lw	$13,60($sp)
	sw	$20,16($sp)
	sw	$19,12($sp)
	sw	$18,8($sp)
	sw	$17,4($sp)
	sw	$16,0($sp)
	sb	$0,0($14)
	li	$14,321191936			# 0x13250000
	sw	$3,48($14)
	li	$3,3			# 0x3
	beq	$6,$3,$L3
	li	$3,-1048248320			# 0xffffffffc1850000

	andi	$5,$5,0xffff
	or	$4,$4,$3
	or	$4,$4,$5
	andi	$24,$6,0x1
	sw	$4,0($2)
	bne	$24,$0,$L4
	li	$4,8			# 0x8

	li	$3,4			# 0x4
	li	$25,1			# 0x1
	andi	$6,$6,0x2
$L5:
	beq	$6,$0,$L10
	li	$5,786432			# 0xc0000

	lw	$15,16($13)
	lhu	$17,16($12)
	li	$16,524288			# 0x80000
	addu	$3,$2,$3
	sll	$15,$15,16
	movz	$16,$5,$9
	sll	$14,$7,31
	or	$5,$15,$17
	li	$7,1073741824			# 0x40000000
	sw	$5,0($3)
	addu	$5,$2,$4
	lw	$3,16($11)
	or	$19,$14,$7
	addiu	$4,$25,2
	andi	$3,$3,0xf
	ori	$3,$3,0xa0
	or	$3,$3,$19
	or	$3,$3,$16
	sll	$4,$4,2
	sw	$3,0($5)
	lw	$3,20($13)
	addu	$5,$2,$4
	lhu	$17,20($12)
	addiu	$15,$4,4
	addiu	$7,$4,8
	sll	$3,$3,16
	addu	$15,$2,$15
	or	$3,$3,$17
	addu	$7,$2,$7
	sw	$3,0($5)
	addiu	$18,$4,12
	lw	$3,20($11)
	addiu	$5,$4,16
	addu	$18,$2,$18
	addu	$5,$2,$5
	andi	$3,$3,0xf
	ori	$3,$3,0x2a0
	or	$3,$3,$19
	or	$3,$3,$16
	addiu	$17,$4,20
	sw	$3,0($15)
	addiu	$3,$4,24
	lw	$20,24($13)
	addu	$17,$2,$17
	lhu	$21,24($12)
	addiu	$15,$25,8
	addiu	$4,$4,28
	sll	$20,$20,16
	or	$20,$20,$21
	sw	$20,0($7)
	lw	$7,24($11)
	andi	$7,$7,0xf
	ori	$7,$7,0x8a0
	or	$7,$7,$19
	or	$7,$7,$16
	sw	$7,0($18)
	lw	$7,28($13)
	lhu	$12,28($12)
	sll	$7,$7,16
	or	$7,$7,$12
	sw	$7,0($5)
	li	$5,1124073472			# 0x43000000
	lw	$11,28($11)
	addiu	$5,$5,2720
	andi	$11,$11,0xf
	or	$11,$14,$11
	or	$5,$11,$5
	or	$5,$5,$16
	sw	$5,0($17)
	beq	$24,$0,$L25
	addu	$11,$2,$3

	lw	$15,68($sp)
	lw	$3,64($sp)
	addu	$13,$2,$4
	move	$12,$0
	lw	$5,0($15)
	addiu	$15,$25,10
	lhu	$7,0($3)
	sll	$3,$5,16
	or	$3,$3,$7
	sw	$3,0($11)
	lw	$4,0($10)
$L44:
	li	$5,196608			# 0x30000
	li	$7,131072			# 0x20000
	sll	$3,$15,2
	movz	$7,$5,$9
	andi	$4,$4,0xf
	ori	$4,$4,0xf0
	or	$4,$4,$14
	or	$4,$4,$12
	or	$5,$4,$7
	addiu	$4,$3,4
	sw	$5,0($13)
	beq	$6,$0,$L45
	li	$6,-1048576000			# 0xffffffffc1800000

$L25:
	lw	$6,68($sp)
	addu	$3,$2,$3
	lw	$7,64($sp)
	lw	$5,4($6)
	li	$6,131072			# 0x20000
	lhu	$11,4($7)
	li	$7,196608			# 0x30000
	sll	$5,$5,16
	movn	$7,$6,$9
	addu	$6,$2,$4
	or	$5,$5,$11
	sw	$5,0($3)
	move	$9,$7
	lw	$5,4($10)
	li	$7,1107296256			# 0x42000000
	addiu	$3,$15,2
	addiu	$7,$7,240
	andi	$5,$5,0xf
	or	$4,$5,$7
	or	$7,$4,$14
	sll	$3,$3,2
	or	$7,$7,$9
	addiu	$4,$3,4
	sw	$7,0($6)
$L18:
	li	$6,-1048576000			# 0xffffffffc1800000
$L45:
	lhu	$5,%lo(motion_dha)($8)
	addu	$3,$2,$3
	lw	$21,20($sp)
	ori	$6,$6,0xffff
	lw	$20,16($sp)
	addu	$2,$2,$4
	lw	$19,12($sp)
	sw	$6,0($3)
	li	$3,321650688			# 0x132c0000
	lw	$18,8($sp)
	addiu	$3,$3,1
	lw	$17,4($sp)
	lw	$16,0($sp)
	or	$3,$5,$3
	li	$5,-1610612736			# 0xffffffffa0000000
	ori	$5,$5,0xffff
	sw	$5,0($2)
	li	$2,321191936			# 0x13250000
	sw	$3,84($2)
	j	$31
	addiu	$sp,$sp,24

$L10:
	beq	$24,$0,$L18
	lw	$11,68($sp)

	addu	$13,$2,$4
	lw	$12,64($sp)
	addu	$3,$2,$3
	addiu	$15,$25,2
	sll	$14,$7,31
	lw	$5,0($11)
	lhu	$11,0($12)
	li	$12,33554432			# 0x2000000
	sll	$4,$5,16
	or	$4,$4,$11
	sw	$4,0($3)
	j	$L44
	lw	$4,0($10)

$L3:
	li	$3,-1047920640			# 0xffffffffc18a0000
	andi	$5,$5,0xffff
	or	$4,$4,$3
	or	$4,$4,$5
	sw	$4,0($2)
$L4:
	lw	$3,0($13)
	sll	$15,$7,31
	lhu	$4,0($12)
	sll	$3,$3,16
	or	$4,$3,$4
	sw	$4,4($2)
	bne	$9,$0,$L6
	li	$3,524288			# 0x80000

	lw	$5,0($11)
	li	$3,786432			# 0xc0000
	li	$14,786432			# 0xc0000
	or	$16,$15,$3
	andi	$5,$5,0xf
	ori	$5,$5,0xa0
	or	$5,$5,$16
	sw	$5,8($2)
	lw	$4,4($13)
	lhu	$3,4($12)
	sll	$4,$4,16
	or	$4,$4,$3
	sw	$4,12($2)
	lw	$3,4($11)
	andi	$3,$3,0xf
	ori	$3,$3,0x2a0
	or	$3,$3,$16
$L40:
	sw	$3,16($2)
	andi	$6,$6,0x2
	lw	$3,8($13)
	lhu	$4,8($12)
	sll	$3,$3,16
	or	$3,$3,$4
	sw	$3,20($2)
	lw	$4,8($11)
	andi	$4,$4,0xf
	or	$4,$15,$4
	ori	$4,$4,0x8a0
	or	$4,$4,$14
	sw	$4,24($2)
	lw	$4,12($13)
	lhu	$3,12($12)
	sll	$4,$4,16
	or	$4,$4,$3
	sw	$4,28($2)
	bne	$6,$0,$L43
	nop

	li	$4,33554432			# 0x2000000
	li	$3,16777216			# 0x1000000
	or	$15,$15,$4
$L21:
	lw	$14,12($11)
	li	$5,786432			# 0xc0000
	li	$16,524288			# 0x80000
	li	$4,40			# 0x28
	movn	$5,$16,$9
	andi	$14,$14,0xf
	ori	$14,$14,0xaa0
	or	$3,$14,$3
	or	$3,$3,$15
	or	$5,$3,$5
	li	$24,1			# 0x1
	li	$3,36			# 0x24
	sw	$5,32($2)
	j	$L5
	li	$25,9			# 0x9

$L6:
	lw	$4,0($11)
	li	$14,524288			# 0x80000
	or	$5,$15,$3
	andi	$4,$4,0xf
	ori	$4,$4,0xa0
	or	$4,$4,$5
	sw	$4,8($2)
	lw	$4,4($13)
	lhu	$3,4($12)
	sll	$4,$4,16
	or	$4,$4,$3
	sw	$4,12($2)
	lw	$3,4($11)
	andi	$3,$3,0xf
	ori	$3,$3,0x2a0
	j	$L40
	or	$3,$3,$5

$L43:
	j	$L21
	move	$3,$0

	.set	macro
	.set	reorder
	.end	mpeg4_hpel_4mv
	.size	mpeg4_hpel_4mv, .-mpeg4_hpel_4mv
	.align	2
	.globl	mpeg4_qpel
	.set	nomips16
	.set	nomicromips
	.ent	mpeg4_qpel
	.type	mpeg4_qpel, @function
mpeg4_qpel:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(motion_dsa)
	lw	$9,20($sp)
	lui	$8,%hi(motion_dha)
	lw	$11,24($sp)
	lw	$10,28($sp)
	sll	$5,$5,8
	lw	$2,%lo(motion_dsa)($2)
	andi	$4,$4,0xff
	lw	$3,%lo(motion_dha)($8)
	lw	$12,32($sp)
	lw	$13,36($sp)
	sb	$0,0($2)
	li	$2,3			# 0x3
	beq	$6,$2,$L47
	li	$2,-914096128			# 0xffffffffc9840000

	li	$2,-914227200			# 0xffffffffc9820000
	andi	$5,$5,0xffff
	or	$4,$4,$2
	or	$4,$4,$5
	andi	$15,$6,0x1
	sw	$4,0($3)
	bne	$15,$0,$L48
	li	$4,8			# 0x8

	li	$2,4			# 0x4
	li	$14,1			# 0x1
	andi	$6,$6,0x2
$L49:
	beq	$6,$0,$L51
	nop

	lw	$5,4($13)
	addu	$2,$3,$2
	lhu	$24,4($12)
	sll	$12,$7,31
	addiu	$13,$14,2
	sll	$5,$5,16
	addu	$7,$3,$4
	or	$5,$5,$24
	sw	$5,0($2)
	sll	$2,$13,2
	lw	$5,4($11)
	addiu	$4,$2,4
	andi	$5,$5,0xf
	or	$11,$12,$5
	li	$5,1124073472			# 0x43000000
	addiu	$5,$5,240
	or	$5,$11,$5
	sw	$5,0($7)
	bne	$15,$0,$L52
	lw	$11,44($sp)

	lw	$6,44($sp)
$L70:
	addu	$2,$3,$2
	lw	$7,40($sp)
	lw	$5,4($6)
	li	$6,131072			# 0x20000
	lhu	$11,4($7)
	li	$7,196608			# 0x30000
	sll	$5,$5,16
	movn	$7,$6,$9
	addu	$6,$3,$4
	or	$5,$5,$11
	sw	$5,0($2)
	move	$9,$7
	lw	$5,4($10)
	li	$7,1107296256			# 0x42000000
	addiu	$2,$13,2
	addiu	$7,$7,240
	andi	$5,$5,0xf
	or	$4,$5,$7
	or	$7,$4,$12
	sll	$2,$2,2
	or	$7,$7,$9
	addiu	$4,$2,4
	sw	$7,0($6)
$L55:
	li	$6,-1048576000			# 0xffffffffc1800000
	lhu	$5,%lo(motion_dha)($8)
	addu	$2,$3,$2
	ori	$6,$6,0xffff
	addu	$3,$3,$4
	sw	$6,0($2)
	li	$2,321650688			# 0x132c0000
	addiu	$2,$2,1
	or	$2,$5,$2
	li	$5,-1610612736			# 0xffffffffa0000000
	ori	$5,$5,0xffff
	sw	$5,0($3)
	li	$3,321191936			# 0x13250000
	sw	$2,84($3)
	j	$31
	nop

$L51:
	beq	$15,$0,$L55
	lw	$12,44($sp)

	addiu	$13,$14,2
	addu	$14,$3,$4
	addu	$2,$3,$2
	lw	$5,0($12)
	lw	$12,40($sp)
	sll	$4,$5,16
	lhu	$11,0($12)
	sll	$12,$7,31
	or	$4,$4,$11
	li	$11,33554432			# 0x2000000
	sw	$4,0($2)
	lw	$4,0($10)
$L69:
	li	$5,196608			# 0x30000
	li	$7,131072			# 0x20000
	sll	$2,$13,2
	movz	$7,$5,$9
	andi	$4,$4,0xf
	ori	$4,$4,0xf0
	or	$4,$4,$12
	or	$4,$4,$11
	or	$5,$4,$7
	addiu	$4,$2,4
	sw	$5,0($14)
	bne	$6,$0,$L70
	lw	$6,44($sp)

	li	$6,-1048576000			# 0xffffffffc1800000
	lhu	$5,%lo(motion_dha)($8)
	addu	$2,$3,$2
	ori	$6,$6,0xffff
	addu	$3,$3,$4
	sw	$6,0($2)
	li	$2,321650688			# 0x132c0000
	addiu	$2,$2,1
	or	$2,$5,$2
	li	$5,-1610612736			# 0xffffffffa0000000
	ori	$5,$5,0xffff
	sw	$5,0($3)
	li	$3,321191936			# 0x13250000
	sw	$2,84($3)
	j	$31
	nop

$L47:
	andi	$5,$5,0xffff
	or	$4,$4,$2
	or	$4,$4,$5
	sll	$24,$7,31
	sw	$4,0($3)
	li	$6,2			# 0x2
	lw	$2,0($13)
	move	$5,$0
	lhu	$4,0($12)
	sll	$2,$2,16
	or	$2,$2,$4
	sw	$2,4($3)
$L57:
	lw	$14,0($11)
	li	$4,16			# 0x10
	li	$2,12			# 0xc
	li	$15,1			# 0x1
	andi	$14,$14,0xf
	ori	$14,$14,0xf0
	or	$5,$14,$5
	or	$5,$5,$24
	li	$14,3			# 0x3
	sw	$5,8($3)
	j	$L49
	nop

$L48:
	lw	$2,0($13)
	andi	$6,$6,0x2
	lhu	$4,0($12)
	sll	$24,$7,31
	sll	$2,$2,16
	or	$2,$2,$4
	sw	$2,4($3)
	bne	$6,$0,$L68
	nop

	li	$2,33554432			# 0x2000000
	li	$5,16777216			# 0x1000000
	j	$L57
	or	$24,$24,$2

$L52:
	addiu	$13,$14,4
	addu	$14,$3,$4
	addu	$2,$3,$2
	lw	$5,0($11)
	lw	$11,40($sp)
	sll	$4,$5,16
	lhu	$7,0($11)
	move	$11,$0
	or	$4,$4,$7
	sw	$4,0($2)
	j	$L69
	lw	$4,0($10)

$L68:
	j	$L57
	move	$5,$0

	.set	macro
	.set	reorder
	.end	mpeg4_qpel
	.size	mpeg4_qpel, .-mpeg4_qpel
	.align	2
	.globl	mpeg4_qpel_4mv
	.set	nomips16
	.set	nomicromips
	.ent	mpeg4_qpel_4mv
	.type	mpeg4_qpel_4mv, @function
mpeg4_qpel_4mv:
	.frame	$sp,24,$31		# vars= 0, regs= 5/0, args= 0, gp= 0
	.mask	0x001f0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(motion_dsa)
	lui	$8,%hi(motion_dha)
	addiu	$sp,$sp,-24
	lw	$3,%lo(motion_dsa)($2)
	sll	$5,$5,8
	lw	$2,%lo(motion_dha)($8)
	andi	$4,$4,0xff
	lw	$9,44($sp)
	lw	$11,48($sp)
	lw	$10,52($sp)
	lw	$12,56($sp)
	lw	$13,60($sp)
	sw	$20,20($sp)
	sw	$19,16($sp)
	sw	$18,12($sp)
	sw	$17,8($sp)
	sw	$16,4($sp)
	sb	$0,0($3)
	li	$3,3			# 0x3
	beq	$6,$3,$L72
	li	$3,-914030592			# 0xffffffffc9850000

	andi	$5,$5,0xffff
	or	$4,$4,$3
	or	$4,$4,$5
	andi	$15,$6,0x1
	sw	$4,0($2)
	bne	$15,$0,$L73
	li	$4,8			# 0x8

	li	$3,4			# 0x4
	li	$25,1			# 0x1
	andi	$6,$6,0x2
$L74:
	beq	$6,$0,$L76
	sll	$14,$7,31

	lw	$16,16($13)
	lhu	$5,16($12)
	addu	$3,$2,$3
	li	$18,1073741824			# 0x40000000
	sll	$7,$16,16
	addiu	$17,$18,160
	or	$7,$7,$5
	addu	$16,$2,$4
	sw	$7,0($3)
	addiu	$4,$25,2
	lw	$5,16($11)
	addiu	$24,$18,672
	sll	$4,$4,2
	addiu	$18,$18,2208
	andi	$5,$5,0xf
	or	$5,$14,$5
	or	$5,$5,$17
	addu	$7,$2,$4
	sw	$5,0($16)
	addiu	$20,$4,4
	lw	$3,20($13)
	addiu	$19,$4,8
	lhu	$5,20($12)
	addu	$20,$2,$20
	addu	$19,$2,$19
	sll	$3,$3,16
	addiu	$17,$4,12
	or	$5,$3,$5
	addu	$17,$2,$17
	sw	$5,0($7)
	addiu	$16,$4,16
	lw	$3,20($11)
	addiu	$7,$4,20
	addu	$16,$2,$16
	addu	$7,$2,$7
	andi	$3,$3,0xf
	or	$3,$14,$3
	or	$3,$3,$24
	addiu	$24,$25,8
	sw	$3,0($20)
	addiu	$3,$4,24
	lw	$5,24($13)
	addiu	$4,$4,28
	lhu	$20,24($12)
	sll	$5,$5,16
	or	$5,$5,$20
	sw	$5,0($19)
	lw	$5,24($11)
	andi	$5,$5,0xf
	or	$5,$14,$5
	or	$5,$5,$18
	sw	$5,0($17)
	lw	$5,28($13)
	lhu	$12,28($12)
	sll	$5,$5,16
	or	$5,$5,$12
	sw	$5,0($16)
	lw	$5,28($11)
	andi	$5,$5,0xf
	or	$11,$14,$5
	li	$5,1124073472			# 0x43000000
	addiu	$5,$5,2720
	or	$5,$11,$5
	sw	$5,0($7)
	bne	$15,$0,$L77
	lw	$11,68($sp)

$L85:
	lw	$6,68($sp)
	addu	$3,$2,$3
	lw	$7,64($sp)
	lw	$5,4($6)
	li	$6,131072			# 0x20000
	lhu	$11,4($7)
	li	$7,196608			# 0x30000
	sll	$5,$5,16
	movn	$7,$6,$9
	addu	$6,$2,$4
	or	$5,$5,$11
	sw	$5,0($3)
	move	$9,$7
	lw	$5,4($10)
	li	$7,1107296256			# 0x42000000
	addiu	$3,$24,2
	addiu	$7,$7,240
	andi	$5,$5,0xf
	or	$4,$5,$7
	or	$7,$4,$14
	sll	$3,$3,2
	or	$7,$7,$9
	addiu	$4,$3,4
	sw	$7,0($6)
$L80:
	li	$6,-1048576000			# 0xffffffffc1800000
	lhu	$5,%lo(motion_dha)($8)
$L95:
	addu	$3,$2,$3
	lw	$20,20($sp)
	ori	$6,$6,0xffff
	lw	$19,16($sp)
	addu	$2,$2,$4
	lw	$18,12($sp)
	sw	$6,0($3)
	li	$3,321650688			# 0x132c0000
	lw	$17,8($sp)
	addiu	$3,$3,1
	lw	$16,4($sp)
	or	$3,$5,$3
	li	$5,-1610612736			# 0xffffffffa0000000
	ori	$5,$5,0xffff
	sw	$5,0($2)
	li	$2,321191936			# 0x13250000
	sw	$3,84($2)
	j	$31
	addiu	$sp,$sp,24

$L76:
	beq	$15,$0,$L80
	lw	$14,68($sp)

	addu	$3,$2,$3
	lw	$12,64($sp)
	addiu	$24,$25,2
	lw	$5,0($14)
	sll	$14,$7,31
	lhu	$11,0($12)
	addu	$12,$2,$4
	sll	$4,$5,16
	or	$4,$4,$11
	li	$11,33554432			# 0x2000000
	sw	$4,0($3)
	lw	$4,0($10)
$L94:
	li	$5,196608			# 0x30000
	li	$7,131072			# 0x20000
	sll	$3,$24,2
	movz	$7,$5,$9
	andi	$4,$4,0xf
	ori	$4,$4,0xf0
	or	$4,$4,$14
	or	$4,$4,$11
	or	$5,$4,$7
	addiu	$4,$3,4
	sw	$5,0($12)
	bne	$6,$0,$L85
	li	$6,-1048576000			# 0xffffffffc1800000

	j	$L95
	lhu	$5,%lo(motion_dha)($8)

$L72:
	li	$3,-913702912			# 0xffffffffc98a0000
	andi	$5,$5,0xffff
	or	$4,$4,$3
	or	$4,$4,$5
	sw	$4,0($2)
$L73:
	lw	$3,0($13)
	sll	$16,$7,31
	lhu	$4,0($12)
	andi	$6,$6,0x2
	sll	$3,$3,16
	or	$3,$3,$4
	sw	$3,4($2)
	lw	$3,0($11)
	andi	$3,$3,0xf
	or	$3,$16,$3
	ori	$3,$3,0xa0
	sw	$3,8($2)
	lw	$3,4($13)
	lhu	$4,4($12)
	sll	$3,$3,16
	or	$3,$3,$4
	sw	$3,12($2)
	lw	$3,4($11)
	andi	$3,$3,0xf
	or	$3,$16,$3
	ori	$3,$3,0x2a0
	sw	$3,16($2)
	lw	$3,8($13)
	lhu	$4,8($12)
	sll	$3,$3,16
	or	$3,$3,$4
	sw	$3,20($2)
	lw	$3,8($11)
	andi	$3,$3,0xf
	or	$3,$16,$3
	ori	$3,$3,0x8a0
	sw	$3,24($2)
	lw	$3,12($13)
	lhu	$4,12($12)
	sll	$3,$3,16
	or	$3,$3,$4
	sw	$3,28($2)
	beq	$6,$0,$L75
	li	$5,16777216			# 0x1000000

	move	$5,$0
$L82:
	lw	$14,12($11)
	li	$4,40			# 0x28
	li	$3,36			# 0x24
	li	$15,1			# 0x1
	andi	$14,$14,0xf
	ori	$14,$14,0xaa0
	or	$5,$14,$5
	or	$5,$5,$16
	li	$25,9			# 0x9
	sw	$5,32($2)
	j	$L74
	nop

$L75:
	li	$3,33554432			# 0x2000000
	j	$L82
	or	$16,$16,$3

$L77:
	addu	$3,$2,$3
	lw	$12,64($sp)
	addiu	$24,$25,10
	lw	$5,0($11)
	move	$11,$0
	lhu	$7,0($12)
	addu	$12,$2,$4
	sll	$4,$5,16
	or	$4,$4,$7
	sw	$4,0($3)
	j	$L94
	lw	$4,0($10)

	.set	macro
	.set	reorder
	.end	mpeg4_qpel_4mv
	.size	mpeg4_qpel_4mv, .-mpeg4_qpel_4mv
	.align	2
	.set	nomips16
	.set	nomicromips
	.ent	MPV_motion_p1
	.type	MPV_motion_p1, @function
MPV_motion_p1:
	.frame	$sp,248,$31		# vars= 160, regs= 10/0, args= 48, gp= 0
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$9,2			# 0x2
	addiu	$sp,$sp,-248
	li	$2,-201326592			# 0xfffffffff4000000
	sw	$9,148($sp)
	li	$9,8			# 0x8
	addiu	$3,$2,21032
	sw	$0,144($sp)
	lui	$14,%hi(motion_dha)
	sw	$31,244($sp)
	sw	$9,152($sp)
	li	$9,10			# 0xa
	addiu	$8,$2,21128
	sw	$3,%lo(motion_dha)($14)
	sw	$fp,240($sp)
	andi	$13,$6,0x1
	sw	$9,156($sp)
	lui	$9,%hi(motion_buf)
	sw	$23,236($sp)
	sw	$3,%lo(motion_buf)($9)
	lui	$3,%hi(motion_dsa)
	sw	$22,232($sp)
	sw	$8,%lo(motion_dsa)($3)
	sw	$21,228($sp)
	sw	$20,224($sp)
	sw	$19,220($sp)
	sw	$18,216($sp)
	sw	$17,212($sp)
	sw	$16,208($sp)
	lw	$3,21028($2)
	lhu	$11,264($sp)
	beq	$3,$0,$L97
	lhu	$12,268($sp)

	addiu	$2,$2,20004
	lui	$3,%hi(motion_douty)
	li	$8,321650688			# 0x132c0000
	sw	$2,%lo(motion_douty)($3)
	li	$9,-201326592			# 0xfffffffff4000000
	li	$2,321650688			# 0x132c0000
	addiu	$8,$8,20004
	addiu	$10,$2,21128
	addiu	$3,$2,21708
	addiu	$9,$9,21708
	li	$2,321191936			# 0x13250000
	lui	$15,%hi(motion_doutc)
	sw	$9,%lo(motion_doutc)($15)
	sw	$8,28($2)
	sw	$3,2076($2)
	sw	$10,88($2)
	sw	$10,2136($2)
	beq	$4,$0,$L100
	li	$2,1			# 0x1

$L201:
	bne	$4,$2,$L200
	lw	$31,244($sp)

	sw	$0,176($sp)
	sw	$0,180($sp)
	sw	$0,168($sp)
	beq	$5,$0,$L129
	sw	$0,172($sp)

	bne	$13,$0,$L130
	addiu	$4,$sp,48

	addiu	$10,$sp,112
	addiu	$14,$sp,80
	sw	$10,192($sp)
	sw	$14,188($sp)
$L131:
	andi	$2,$6,0x2
	beq	$2,$0,$L137
	lw	$14,272($sp)

	subu	$20,$0,$11
	subu	$19,$0,$12
	lw	$24,192($sp)
	move	$25,$4
	lw	$15,188($sp)
	sw	$6,184($sp)
	sll	$fp,$11,1
	sw	$4,196($sp)
	sll	$23,$12,1
	lw	$6,280($sp)
	sll	$20,$20,1
	lw	$4,276($sp)
	sll	$19,$19,1
	addiu	$2,$14,32
	move	$17,$0
	move	$16,$0
	move	$5,$0
	li	$31,-4			# 0xfffffffffffffffc
	li	$22,-16			# 0xfffffffffffffff0
	li	$21,4			# 0x4
$L142:
	lw	$10,0($2)
	andi	$14,$5,0x1
	lw	$9,4($2)
	addu	$3,$14,$fp
	sra	$18,$10,2
	andi	$8,$9,0x3
	sw	$10,16($24)
	sll	$3,$3,3
	sw	$9,16($15)
	sll	$8,$8,2
	addu	$3,$3,$18
	andi	$18,$10,0x3
	or	$8,$8,$18
	slt	$18,$3,-16
	sw	$8,16($25)
	beq	$18,$0,$L138
	slt	$3,$3,$4

	sw	$22,16($24)
$L139:
	sra	$8,$5,1
	sra	$3,$9,2
	addu	$14,$8,$23
	sll	$14,$14,3
	addu	$3,$14,$3
	slt	$14,$3,-16
	beq	$14,$0,$L140
	slt	$3,$3,$6

$L196:
	sw	$22,16($15)
$L141:
	srl	$8,$10,31
	srl	$3,$9,31
	addu	$10,$8,$10
	addu	$9,$3,$9
	sra	$10,$10,1
	sra	$9,$9,1
	addiu	$5,$5,1
	addu	$16,$16,$10
	addu	$17,$17,$9
	addiu	$2,$2,8
	addiu	$24,$24,4
	addiu	$15,$15,4
	bne	$5,$21,$L142
	addiu	$25,$25,4

	lui	$2,%hi(JZC_chroma_roundtab.2405)
	lw	$4,196($sp)
	andi	$5,$17,0xf
	lw	$6,184($sp)
	addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
	andi	$3,$16,0xf
	addu	$5,$5,$2
	addu	$2,$3,$2
	sra	$17,$17,3
	lbu	$3,0($5)
	sra	$16,$16,3
	lbu	$2,0($2)
	addu	$17,$3,$17
	addu	$16,$2,$16
	andi	$2,$17,0x1
	sll	$2,$2,1
	sw	$17,172($sp)
	andi	$3,$16,0x1
	sw	$16,180($sp)
	or	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,96($2)
	sw	$2,164($sp)
$L137:
	addiu	$2,$sp,160
	lw	$3,188($sp)
	sw	$7,20($sp)
	move	$5,$12
	sw	$4,24($sp)
	andi	$6,$6,0x3
	sw	$2,28($sp)
	move	$4,$11
	lw	$2,192($sp)
	move	$7,$13
	sw	$0,16($sp)
	sw	$3,36($sp)
	sw	$2,32($sp)
	addiu	$2,$sp,176
	sw	$2,40($sp)
	addiu	$2,$sp,168
	jal	mpeg4_qpel_4mv
	sw	$2,44($sp)

	lw	$31,244($sp)
$L200:
	lw	$fp,240($sp)
	lw	$23,236($sp)
	lw	$22,232($sp)
	lw	$21,228($sp)
	lw	$20,224($sp)
	lw	$19,220($sp)
	lw	$18,216($sp)
	lw	$17,212($sp)
	lw	$16,208($sp)
	j	$31
	addiu	$sp,$sp,248

$L97:
	addiu	$2,$2,19492
	lui	$3,%hi(motion_douty)
	li	$8,321650688			# 0x132c0000
	sw	$2,%lo(motion_douty)($3)
	li	$9,-201326592			# 0xfffffffff4000000
	li	$2,321650688			# 0x132c0000
	addiu	$8,$8,19492
	addiu	$10,$2,21128
	addiu	$3,$2,21708
	addiu	$9,$9,21708
	li	$2,321191936			# 0x13250000
	lui	$15,%hi(motion_doutc)
	sw	$9,%lo(motion_doutc)($15)
	sw	$8,28($2)
	sw	$3,2076($2)
	sw	$10,88($2)
	sw	$10,2136($2)
	bne	$4,$0,$L201
	li	$2,1			# 0x1

$L100:
	beq	$5,$0,$L102
	nop

	bne	$13,$0,$L192
	lw	$2,272($sp)

	addiu	$4,$sp,48
$L103:
	andi	$2,$6,0x2
	beq	$2,$0,$L107
	lw	$14,272($sp)

	lw	$5,284($sp)
	lw	$3,36($14)
	andi	$2,$5,0x100
	lw	$8,32($14)
	andi	$5,$3,0x3
	sll	$5,$5,2
	sw	$3,84($sp)
	andi	$9,$8,0x3
	sw	$8,116($sp)
	or	$5,$5,$9
	beq	$2,$0,$L108
	sw	$5,52($sp)

	lui	$5,%hi(rtab.2443)
	andi	$9,$8,0x7
	andi	$2,$3,0x7
	addiu	$5,$5,%lo(rtab.2443)
	sll	$2,$2,2
	sll	$9,$9,2
	sra	$8,$8,1
	addu	$9,$9,$5
	addu	$5,$2,$5
	sra	$2,$3,1
	lw	$9,0($9)
	lw	$3,0($5)
	addu	$8,$8,$9
	addu	$3,$2,$3
$L109:
	sra	$2,$3,1
	andi	$3,$3,0x1
	or	$2,$3,$2
	sra	$3,$8,1
	sw	$2,172($sp)
	andi	$8,$8,0x1
	andi	$2,$2,0x1
	or	$3,$8,$3
	sll	$2,$2,1
	andi	$5,$3,0x1
	sw	$3,180($sp)
	or	$2,$2,$5
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,96($2)
	sw	$2,164($sp)
$L107:
	addiu	$2,$sp,160
	sw	$7,20($sp)
	sw	$4,24($sp)
	move	$5,$12
	sw	$0,16($sp)
	move	$4,$11
	sw	$2,28($sp)
	addiu	$2,$sp,112
	andi	$6,$6,0x3
	move	$7,$13
	sw	$2,32($sp)
	addiu	$2,$sp,80
	sw	$2,36($sp)
	addiu	$2,$sp,176
	sw	$2,40($sp)
	addiu	$2,$sp,168
	jal	mpeg4_qpel
	sw	$2,44($sp)

	lw	$31,244($sp)
	lw	$fp,240($sp)
	lw	$23,236($sp)
	lw	$22,232($sp)
	lw	$21,228($sp)
	lw	$20,224($sp)
	lw	$19,220($sp)
	lw	$18,216($sp)
	lw	$17,212($sp)
	lw	$16,208($sp)
	j	$31
	addiu	$sp,$sp,248

$L102:
	beq	$13,$0,$L202
	andi	$4,$6,0x2

	lw	$2,272($sp)
	lw	$5,0($2)
	lw	$4,4($2)
	andi	$3,$5,0x2
	andi	$2,$4,0x1
	sw	$5,112($sp)
	sll	$2,$2,1
	sw	$4,80($sp)
	sra	$3,$3,1
	sw	$5,176($sp)
	andi	$8,$4,0x2
	sw	$4,168($sp)
	andi	$9,$5,0x1
	or	$2,$2,$9
	or	$3,$3,$8
	addiu	$8,$5,1
	sw	$2,48($sp)
	or	$3,$3,$2
	sltu	$8,$8,3
	bne	$8,$0,$L112
	sw	$3,160($sp)

	sra	$5,$5,1
	sw	$5,176($sp)
$L112:
	addiu	$5,$4,1
	sltu	$5,$5,3
	beq	$5,$0,$L195
	sra	$4,$4,1

$L113:
	addiu	$4,$sp,48
	sll	$2,$2,2
	sll	$3,$3,2
	addu	$2,$4,$2
	addu	$3,$4,$3
	lw	$4,96($2)
	lw	$2,96($3)
	sw	$4,48($sp)
	sw	$2,160($sp)
	andi	$4,$6,0x2
$L202:
	andi	$4,$4,0x00ff
	beq	$4,$0,$L114
	lw	$5,272($sp)

	lw	$8,32($5)
	lw	$5,36($5)
	andi	$3,$8,0x2
	andi	$2,$5,0x1
	sw	$8,116($sp)
	sll	$2,$2,1
	sw	$5,84($sp)
	sra	$3,$3,1
	sw	$8,180($sp)
	andi	$9,$5,0x2
	sw	$5,172($sp)
	andi	$10,$8,0x1
	or	$2,$2,$10
	or	$3,$3,$9
	addiu	$9,$8,1
	sw	$2,52($sp)
	or	$3,$3,$2
	sltu	$9,$9,3
	bne	$9,$0,$L115
	sw	$3,164($sp)

	sra	$8,$8,1
	sw	$8,180($sp)
$L115:
	addiu	$8,$5,1
	sltu	$8,$8,3
	bne	$8,$0,$L203
	addiu	$8,$sp,48

	sra	$5,$5,1
	sw	$5,172($sp)
$L203:
	sll	$2,$2,2
	sll	$3,$3,2
	addu	$2,$8,$2
	addu	$3,$8,$3
	lw	$5,96($2)
	lw	$2,96($3)
	sw	$5,52($sp)
	sw	$2,164($sp)
$L114:
	andi	$6,$6,0x3
	xori	$2,$6,0x3
	li	$3,-1048444928			# 0xffffffffc1820000
	li	$6,-1048313856			# 0xffffffffc1840000
	li	$5,196608			# 0x30000
	movn	$6,$3,$2
	li	$3,131072			# 0x20000
	sll	$12,$12,8
	movz	$3,$5,$7
	andi	$2,$11,0x00ff
	andi	$11,$12,0xffff
	or	$11,$2,$11
	or	$11,$11,$6
	li	$2,-201326592			# 0xfffffffff4000000
	li	$5,321191936			# 0x13250000
	sb	$0,21128($2)
	sw	$3,48($5)
	sw	$11,21032($2)
	beq	$13,$0,$L163
	addiu	$9,$2,21040

	lw	$5,80($sp)
	sll	$9,$13,31
	lhu	$6,112($sp)
	sll	$5,$5,16
	or	$5,$6,$5
	sw	$5,21036($2)
	beq	$4,$0,$L120
	li	$3,33554432			# 0x2000000

	move	$2,$0
$L156:
	li	$3,786432			# 0xc0000
	li	$5,524288			# 0x80000
	li	$8,-201326592			# 0xfffffffff4000000
	movz	$5,$3,$7
	li	$16,3			# 0x3
	lw	$3,48($sp)
	andi	$3,$3,0xf
	ori	$3,$3,0xf0
	or	$3,$3,$9
	or	$2,$3,$2
	or	$2,$2,$5
	addiu	$9,$8,21048
	sw	$2,21040($8)
	addiu	$8,$8,21044
$L119:
	beq	$4,$0,$L122
	li	$3,786432			# 0xc0000

	lw	$5,52($sp)
	li	$2,524288			# 0x80000
	lw	$15,84($sp)
	move	$12,$3
	lhu	$11,116($sp)
	li	$3,1124073472			# 0x43000000
	movn	$12,$2,$7
	addiu	$6,$16,2
	addiu	$3,$3,240
	andi	$5,$5,0xf
	sll	$10,$13,31
	sll	$17,$6,2
	or	$3,$5,$3
	sll	$15,$15,16
	li	$2,-201326592			# 0xfffffffff4000000
	or	$3,$3,$10
	addiu	$2,$2,21032
	or	$5,$11,$15
	or	$3,$3,$12
	addiu	$11,$17,4
	sw	$5,0($8)
	sw	$3,0($9)
	addu	$8,$17,$2
	bne	$13,$0,$L124
	addu	$9,$11,$2

$L160:
	li	$2,131072			# 0x20000
	lw	$4,172($sp)
	li	$3,196608			# 0x30000
	lhu	$5,180($sp)
	addiu	$6,$6,2
	movn	$3,$2,$7
	li	$2,1107296256			# 0x42000000
	sll	$6,$6,2
	addiu	$2,$2,240
	sll	$4,$4,16
	move	$7,$3
	lw	$3,164($sp)
	or	$4,$5,$4
	andi	$3,$3,0xf
	sw	$4,0($8)
	or	$2,$3,$2
	li	$3,-201326592			# 0xfffffffff4000000
	or	$2,$2,$10
	addiu	$3,$3,21032
	or	$7,$2,$7
	addiu	$2,$6,4
	sw	$7,0($9)
	addu	$8,$6,$3
	addu	$9,$2,$3
	li	$2,321650688			# 0x132c0000
$L198:
	lhu	$3,%lo(motion_dha)($14)
	lw	$31,244($sp)
	addiu	$2,$2,1
	lw	$fp,240($sp)
	lw	$23,236($sp)
	or	$2,$3,$2
	lw	$22,232($sp)
	li	$3,-1048576000			# 0xffffffffc1800000
	lw	$21,228($sp)
	lw	$20,224($sp)
	ori	$3,$3,0xffff
	lw	$19,220($sp)
	lw	$18,216($sp)
	sw	$3,0($8)
	li	$3,-1610612736			# 0xffffffffa0000000
	lw	$17,212($sp)
	ori	$3,$3,0xffff
	lw	$16,208($sp)
	sw	$3,0($9)
	li	$3,321191936			# 0x13250000
	sw	$2,84($3)
	j	$31
	addiu	$sp,$sp,248

$L129:
	bne	$13,$0,$L143
	addiu	$14,$sp,80

	addiu	$5,$sp,112
	addiu	$8,$sp,80
	addiu	$4,$sp,48
	sw	$5,192($sp)
	sw	$8,188($sp)
$L144:
	andi	$2,$6,0x2
	beq	$2,$0,$L204
	addiu	$2,$sp,160

	lw	$8,272($sp)
	subu	$21,$0,$11
	subu	$20,$0,$12
	lw	$17,192($sp)
	lw	$16,188($sp)
	sll	$24,$11,1
	sw	$6,184($sp)
	addiu	$5,$8,32
	sw	$7,196($sp)
	sll	$23,$12,1
	lw	$6,276($sp)
	sll	$21,$21,1
	lw	$7,280($sp)
	sll	$20,$20,1
	move	$19,$0
	move	$18,$0
	move	$8,$0
	li	$31,-2			# 0xfffffffffffffffe
	li	$25,-16			# 0xfffffffffffffff0
	li	$22,4			# 0x4
	move	$15,$4
$L155:
	lw	$10,0($5)
	andi	$14,$8,0x1
	lw	$9,4($5)
	addu	$3,$14,$24
	sra	$fp,$10,1
	andi	$2,$9,0x1
	sw	$10,16($17)
	sll	$3,$3,3
	sw	$9,16($16)
	sll	$2,$2,1
	addu	$3,$3,$fp
	andi	$fp,$10,0x1
	or	$2,$2,$fp
	slt	$fp,$3,-16
	sw	$2,16($15)
	beq	$fp,$0,$L151
	slt	$3,$3,$6

	sw	$25,16($17)
$L152:
	sra	$14,$8,1
	sra	$3,$9,1
	addu	$fp,$14,$23
	sll	$fp,$fp,3
	addu	$3,$fp,$3
	slt	$fp,$3,-16
	beq	$fp,$0,$L153
	slt	$3,$3,$7

	sw	$25,16($16)
$L154:
	sll	$2,$2,2
	addiu	$15,$15,4
	addu	$2,$4,$2
	addiu	$8,$8,1
	addu	$18,$18,$10
	lw	$2,96($2)
	addu	$19,$19,$9
	addiu	$5,$5,8
	addiu	$17,$17,4
	addiu	$16,$16,4
	bne	$8,$22,$L155
	sw	$2,12($15)

	lui	$2,%hi(JZC_chroma_roundtab.2405)
	lw	$6,184($sp)
	andi	$5,$19,0xf
	lw	$7,196($sp)
	addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
	andi	$3,$18,0xf
	addu	$5,$5,$2
	addu	$2,$3,$2
	sra	$19,$19,3
	lbu	$3,0($5)
	sra	$18,$18,3
	lbu	$2,0($2)
	addu	$19,$3,$19
	addu	$18,$2,$18
	andi	$2,$19,0x1
	sll	$2,$2,1
	sw	$19,172($sp)
	andi	$3,$18,0x1
	sw	$18,180($sp)
	or	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,96($2)
	sw	$2,164($sp)
	addiu	$2,$sp,160
$L204:
	lw	$5,192($sp)
	lw	$8,188($sp)
	andi	$6,$6,0x3
	sw	$7,20($sp)
	move	$7,$13
	sw	$2,28($sp)
	addiu	$2,$sp,176
	sw	$4,24($sp)
	move	$4,$11
	sw	$5,32($sp)
	move	$5,$12
	sw	$2,40($sp)
	addiu	$2,$sp,168
	sw	$0,16($sp)
	sw	$8,36($sp)
	jal	mpeg4_hpel_4mv
	sw	$2,44($sp)

	lw	$31,244($sp)
	lw	$fp,240($sp)
	lw	$23,236($sp)
	lw	$22,232($sp)
	lw	$21,228($sp)
	lw	$20,224($sp)
	lw	$19,220($sp)
	lw	$18,216($sp)
	lw	$17,212($sp)
	lw	$16,208($sp)
	j	$31
	addiu	$sp,$sp,248

$L138:
	bne	$3,$0,$L139
	subu	$14,$20,$14

	sll	$14,$14,3
	and	$8,$8,$31
	addu	$14,$14,$4
	sw	$8,16($25)
	sra	$8,$5,1
	sll	$14,$14,2
	sra	$3,$9,2
	sw	$14,16($24)
	addu	$14,$8,$23
	sll	$14,$14,3
	addu	$3,$14,$3
	slt	$14,$3,-16
	bne	$14,$0,$L196
	slt	$3,$3,$6

$L140:
	bne	$3,$0,$L141
	subu	$8,$19,$8

	sll	$8,$8,3
	lw	$3,16($25)
	li	$14,-13			# 0xfffffffffffffff3
	addu	$8,$8,$6
	and	$3,$3,$14
	sll	$8,$8,2
	sw	$3,16($25)
	j	$L141
	sw	$8,16($15)

$L153:
	bne	$3,$0,$L154
	subu	$14,$20,$14

	sll	$14,$14,3
	li	$3,-3			# 0xfffffffffffffffd
	addu	$14,$14,$7
	and	$2,$2,$3
	sll	$14,$14,1
	j	$L154
	sw	$14,16($16)

$L151:
	bne	$3,$0,$L152
	subu	$14,$21,$14

	sll	$14,$14,3
	and	$2,$2,$31
	addu	$14,$14,$6
	sw	$2,16($15)
	sll	$14,$14,1
	j	$L152
	sw	$14,16($17)

$L143:
	lw	$15,272($sp)
	addiu	$10,$sp,112
	sw	$6,196($sp)
	subu	$21,$0,$11
	sw	$7,200($sp)
	subu	$20,$0,$12
	sw	$14,188($sp)
	addiu	$4,$sp,48
	sw	$10,192($sp)
	lw	$6,276($sp)
	sll	$24,$11,1
	lw	$7,280($sp)
	sll	$23,$12,1
	sll	$21,$21,1
	sll	$20,$20,1
	move	$19,$0
	move	$18,$0
	move	$5,$0
	li	$31,-2			# 0xfffffffffffffffe
	li	$25,-16			# 0xfffffffffffffff0
	li	$22,4			# 0x4
	move	$16,$14
	move	$17,$10
	move	$14,$4
$L149:
	lw	$9,0($15)
	andi	$10,$5,0x1
	lw	$8,4($15)
	addu	$3,$10,$24
	sra	$fp,$9,1
	andi	$2,$8,0x1
	sw	$9,0($17)
	sll	$3,$3,3
	sw	$8,0($16)
	sll	$2,$2,1
	addu	$3,$3,$fp
	andi	$fp,$9,0x1
	or	$2,$2,$fp
	slt	$fp,$3,-16
	sw	$2,0($14)
	beq	$fp,$0,$L145
	slt	$3,$3,$6

	sw	$25,0($17)
$L146:
	sra	$10,$5,1
	sra	$3,$8,1
	addu	$fp,$10,$23
	sll	$fp,$fp,3
	addu	$3,$fp,$3
	slt	$fp,$3,-16
	beq	$fp,$0,$L147
	slt	$3,$3,$7

	sw	$25,0($16)
$L148:
	sll	$2,$2,2
	addiu	$14,$14,4
	addu	$2,$4,$2
	addiu	$5,$5,1
	addu	$18,$18,$9
	lw	$2,96($2)
	addu	$19,$19,$8
	addiu	$15,$15,8
	addiu	$17,$17,4
	addiu	$16,$16,4
	bne	$5,$22,$L149
	sw	$2,-4($14)

	lui	$2,%hi(JZC_chroma_roundtab.2405)
	lw	$6,196($sp)
	andi	$5,$19,0xf
	lw	$7,200($sp)
	addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
	andi	$3,$18,0xf
	addu	$5,$5,$2
	addu	$2,$3,$2
	sra	$19,$19,3
	lbu	$3,0($5)
	sra	$18,$18,3
	lbu	$2,0($2)
	addu	$19,$3,$19
	addu	$18,$2,$18
	andi	$2,$19,0x1
	sll	$2,$2,1
	sw	$19,168($sp)
	andi	$3,$18,0x1
	sw	$18,176($sp)
	or	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,96($2)
	j	$L144
	sw	$2,160($sp)

$L147:
	bne	$3,$0,$L148
	subu	$10,$20,$10

	sll	$10,$10,3
	li	$3,-3			# 0xfffffffffffffffd
	addu	$10,$10,$7
	and	$2,$2,$3
	sll	$10,$10,1
	j	$L148
	sw	$10,0($16)

$L145:
	bne	$3,$0,$L146
	subu	$10,$21,$10

	sll	$10,$10,3
	and	$2,$2,$31
	addu	$10,$10,$6
	sw	$2,0($14)
	sll	$10,$10,1
	j	$L146
	sw	$10,0($17)

$L130:
	lw	$14,272($sp)
	addiu	$2,$sp,112
	sw	$6,196($sp)
	addiu	$3,$sp,80
	lw	$6,280($sp)
	subu	$22,$0,$11
	sw	$4,200($sp)
	subu	$21,$0,$12
	sw	$2,192($sp)
	move	$15,$4
	sw	$3,188($sp)
	lw	$4,276($sp)
	sll	$25,$11,1
	sll	$24,$12,1
	sll	$22,$22,1
	sll	$21,$21,1
	move	$19,$0
	move	$18,$0
	move	$9,$0
	li	$31,-4			# 0xfffffffffffffffc
	li	$fp,-16			# 0xfffffffffffffff0
	li	$23,4			# 0x4
	move	$17,$2
	move	$16,$3
$L136:
	lw	$8,0($14)
	andi	$10,$9,0x1
	lw	$5,4($14)
	addu	$2,$10,$25
	sra	$20,$8,2
	andi	$3,$5,0x3
	sw	$8,0($17)
	sll	$2,$2,3
	sw	$5,0($16)
	sll	$3,$3,2
	addu	$2,$2,$20
	andi	$20,$8,0x3
	or	$3,$3,$20
	slt	$20,$2,-16
	sw	$3,0($15)
	beq	$20,$0,$L132
	slt	$2,$2,$4

	sw	$fp,0($17)
$L133:
	sra	$3,$9,1
	sra	$2,$5,2
	addu	$10,$3,$24
	sll	$10,$10,3
	addu	$2,$10,$2
	slt	$10,$2,-16
	beq	$10,$0,$L134
	slt	$2,$2,$6

	sw	$fp,0($16)
$L135:
	srl	$3,$8,31
	srl	$2,$5,31
	addu	$8,$3,$8
	addu	$5,$2,$5
	sra	$8,$8,1
	sra	$5,$5,1
	addiu	$9,$9,1
	addu	$18,$18,$8
	addu	$19,$19,$5
	addiu	$14,$14,8
	addiu	$17,$17,4
	addiu	$16,$16,4
	bne	$9,$23,$L136
	addiu	$15,$15,4

	lui	$2,%hi(JZC_chroma_roundtab.2405)
	lw	$4,200($sp)
	andi	$5,$19,0xf
	lw	$6,196($sp)
	addiu	$2,$2,%lo(JZC_chroma_roundtab.2405)
	andi	$3,$18,0xf
	addu	$5,$5,$2
	addu	$2,$3,$2
	sra	$19,$19,3
	lbu	$3,0($5)
	sra	$18,$18,3
	lbu	$2,0($2)
	addu	$19,$3,$19
	addu	$18,$2,$18
	andi	$2,$19,0x1
	sll	$2,$2,1
	sw	$19,168($sp)
	andi	$3,$18,0x1
	sw	$18,176($sp)
	or	$2,$2,$3
	sll	$2,$2,2
	addu	$2,$4,$2
	lw	$2,96($2)
	j	$L131
	sw	$2,160($sp)

$L134:
	bne	$2,$0,$L135
	subu	$3,$21,$3

	sll	$3,$3,3
	lw	$2,0($15)
	li	$10,-13			# 0xfffffffffffffff3
	addu	$3,$3,$6
	and	$2,$2,$10
	sll	$3,$3,2
	sw	$2,0($15)
	j	$L135
	sw	$3,0($16)

$L132:
	bne	$2,$0,$L133
	subu	$10,$22,$10

	sll	$10,$10,3
	and	$3,$3,$31
	addu	$10,$10,$4
	sw	$3,0($15)
	sll	$10,$10,2
	j	$L133
	sw	$10,0($17)

$L163:
	addiu	$8,$2,21036
	j	$L119
	li	$16,1			# 0x1

$L122:
	beq	$13,$0,$L198
	li	$2,321650688			# 0x132c0000

	lw	$5,168($sp)
	addiu	$6,$16,2
	lhu	$2,176($sp)
	sll	$10,$13,31
	li	$3,33554432			# 0x2000000
	sll	$5,$5,16
	or	$2,$2,$5
	sw	$2,0($8)
	li	$5,131072			# 0x20000
$L199:
	li	$8,196608			# 0x30000
	sll	$2,$6,2
	movn	$8,$5,$7
	addiu	$11,$2,4
	lw	$5,160($sp)
	andi	$5,$5,0xf
	ori	$5,$5,0xf0
	or	$5,$5,$10
	or	$3,$5,$3
	li	$5,-201326592			# 0xfffffffff4000000
	or	$3,$3,$8
	addiu	$5,$5,21032
	sw	$3,0($9)
	addu	$8,$2,$5
	bne	$4,$0,$L160
	addu	$9,$11,$5

	j	$L198
	li	$2,321650688			# 0x132c0000

$L192:
	lw	$8,272($sp)
	lw	$4,284($sp)
	lw	$3,4($2)
	lw	$5,0($8)
	andi	$2,$4,0x100
	andi	$4,$3,0x3
	sll	$4,$4,2
	sw	$3,80($sp)
	andi	$8,$5,0x3
	sw	$5,112($sp)
	or	$4,$4,$8
	bne	$2,$0,$L197
	sw	$4,48($sp)

	lw	$10,284($sp)
	andi	$2,$10,0x40
	beq	$2,$0,$L106
	srl	$2,$5,31

	sra	$4,$5,1
	sra	$2,$3,1
	andi	$5,$5,0x1
	andi	$3,$3,0x1
	or	$5,$5,$4
	or	$3,$3,$2
$L105:
	sra	$2,$3,1
	andi	$3,$3,0x1
	or	$2,$3,$2
	sra	$3,$5,1
	sw	$2,168($sp)
	andi	$5,$5,0x1
	andi	$2,$2,0x1
	or	$3,$5,$3
	sll	$2,$2,1
	andi	$4,$3,0x1
	sw	$3,176($sp)
	or	$2,$2,$4
	sll	$2,$2,2
	addiu	$4,$sp,48
	addu	$2,$4,$2
	lw	$2,96($2)
	j	$L103
	sw	$2,160($sp)

$L108:
	lw	$10,284($sp)
	andi	$5,$10,0x40
	beq	$5,$0,$L110
	srl	$2,$8,31

	sra	$5,$8,1
	sra	$2,$3,1
	andi	$8,$8,0x1
	andi	$3,$3,0x1
	or	$8,$8,$5
	j	$L109
	or	$3,$3,$2

$L197:
	lui	$4,%hi(rtab.2442)
	andi	$8,$5,0x7
	andi	$2,$3,0x7
	addiu	$4,$4,%lo(rtab.2442)
	sll	$2,$2,2
	sll	$8,$8,2
	sra	$5,$5,1
	addu	$8,$8,$4
	addu	$4,$2,$4
	sra	$2,$3,1
	lw	$8,0($8)
	lw	$3,0($4)
	addu	$5,$5,$8
	j	$L105
	addu	$3,$2,$3

$L120:
	li	$2,16777216			# 0x1000000
	j	$L156
	or	$9,$9,$3

$L195:
	j	$L113
	sw	$4,168($sp)

$L124:
	lw	$5,168($sp)
	addiu	$6,$16,4
	lhu	$2,176($sp)
	move	$3,$0
	sll	$5,$5,16
	or	$2,$2,$5
	sw	$2,0($8)
	j	$L199
	li	$5,131072			# 0x20000

$L110:
	srl	$5,$3,31
	addu	$8,$2,$8
	addu	$3,$5,$3
	sra	$8,$8,1
	j	$L109
	sra	$3,$3,1

$L106:
	srl	$4,$3,31
	addu	$5,$2,$5
	addu	$3,$4,$3
	sra	$5,$5,1
	j	$L105
	sra	$3,$3,1

	.set	macro
	.set	reorder
	.end	MPV_motion_p1
	.size	MPV_motion_p1, .-MPV_motion_p1
	.section	.p1_main,"ax",@progbits
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,120,$31		# vars= 40, regs= 10/0, args= 40, gp= 0
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	li	$2,3			# 0x3
#APP
 # 82 "mpeg4_p1.c" 1
	S32I2M xr16,$2
 # 0 "" 2
#NO_APP
	li	$2,-201326592			# 0xfffffffff4000000
	ori	$2,$2,0x8000
	addiu	$4,$2,200
$L206:
	sw	$0,0($2)
	addiu	$2,$2,4
	.set	noreorder
	.set	nomacro
	bne	$2,$4,$L206
	li	$3,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

	li	$2,321585152			# 0x132b0000
	addiu	$sp,$sp,-120
	addiu	$2,$2,4096
	addiu	$4,$3,16416
	sw	$18,88($sp)
	sw	$31,116($sp)
	lui	$11,%hi(dMB_X)
	sw	$fp,112($sp)
	addiu	$7,$3,17740
	sw	$23,108($sp)
	addiu	$6,$3,16864
	sw	$22,104($sp)
	addiu	$5,$3,18616
	sw	$21,100($sp)
	lui	$14,%hi(dFRM)
	sw	$20,96($sp)
	lui	$8,%hi(dMB)
	sw	$19,92($sp)
	lui	$18,%hi(dMB_L)
	sw	$17,84($sp)
	lui	$10,%hi(dMB_N)
	sw	$16,80($sp)
	sw	$0,16388($3)
	sw	$2,16392($3)
	addiu	$2,$4,18444
	sw	$4,%lo(dFRM)($14)
	sw	$7,%lo(dMB)($8)
	sw	$2,%lo(dMB_X)($11)
	li	$2,4194304			# 0x400000
	sw	$6,%lo(dMB_L)($18)
	addiu	$2,$2,64
	sw	$5,%lo(dMB_N)($10)
	sw	$2,21140($3)
	li	$2,321650688			# 0x132c0000
	li	$3,320929792			# 0x13210000
	addiu	$2,$2,21132
	sw	$2,0($3)
	li	$4,-201326592			# 0xfffffffff4000000
$L340:
	lw	$3,16384($4)
	lw	$2,16388($4)
	addiu	$2,$2,2
	slt	$2,$2,$3
	beq	$2,$0,$L340
	lw	$2,16392($4)
	li	$3,320929792			# 0x13210000
	sw	$2,21132($4)
	li	$2,321650688			# 0x132c0000
	addiu	$2,$2,17740
	sw	$2,21136($4)
	li	$2,-2143289344			# 0xffffffff80400000
	addiu	$2,$2,876
	sw	$2,21144($4)
	li	$4,1			# 0x1
	li	$2,320929792			# 0x13210000
	sw	$4,4($2)
$L208:
	lw	$2,4($3)
	andi	$2,$2,0x4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L208
	li	$2,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

	li	$5,321585152			# 0x132b0000
	lw	$4,16388($2)
	addiu	$6,$5,15509
	addiu	$4,$4,1
	sw	$4,16388($2)
	lw	$4,16392($2)
	addiu	$4,$4,876
	sw	$4,16392($2)
	lw	$3,16392($2)
	sltu	$3,$3,$6
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L209
	addiu	$5,$5,4096
	.set	macro
	.set	reorder

	sw	$5,16392($2)
$L209:
	li	$2,-201326592			# 0xfffffffff4000000
	li	$3,321585152			# 0x132b0000
	li	$6,1			# 0x1
	lw	$4,16392($2)
	li	$5,320929792			# 0x13210000
	sw	$4,4($3)
	li	$4,-2143289344			# 0xffffffff80400000
	li	$3,321650688			# 0x132c0000
	addiu	$4,$4,876
	addiu	$3,$3,18616
	sw	$4,21144($2)
	sw	$3,21136($2)
	li	$3,320929792			# 0x13210000
	lw	$4,16392($2)
	sw	$6,4($5)
	sw	$4,21132($2)
$L210:
	lw	$2,4($3)
	andi	$2,$2,0x4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L210
	li	$2,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

	li	$3,1			# 0x1
	li	$5,321585152			# 0x132b0000
	sw	$0,21964($2)
	sw	$3,21968($2)
	addiu	$6,$5,15509
	lw	$4,16388($2)
	addiu	$4,$4,1
	sw	$4,16388($2)
	lw	$4,16392($2)
	addiu	$4,$4,876
	sw	$4,16392($2)
	lw	$3,16392($2)
	sltu	$3,$3,$6
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L211
	addiu	$5,$5,4096
	.set	macro
	.set	reorder

	sw	$5,16392($2)
$L211:
	li	$2,-201326592			# 0xfffffffff4000000
	li	$9,321585152			# 0x132b0000
	li	$7,1048576			# 0x100000
	lw	$3,16392($2)
	li	$6,321650688			# 0x132c0000
	lw	$23,16812($2)
	li	$5,16777216			# 0x1000000
	lw	$22,16816($2)
	li	$4,8388608			# 0x800000
	lhu	$19,17816($2)
	addiu	$5,$5,256
	sw	$3,4($9)
	li	$3,262144			# 0x40000
	addiu	$4,$4,128
	addiu	$3,$3,4
	sw	$5,21236($2)
	addiu	$17,$7,16
	sw	$5,21240($2)
	addiu	$16,$6,21324
	sw	$5,21252($2)
	sw	$3,21204($2)
	addiu	$15,$6,21580
	sw	$3,21208($2)
	addiu	$20,$7,256
	sw	$3,21220($2)
	addiu	$7,$7,128
	sw	$3,21224($2)
	addiu	$9,$9,16
	sw	$3,21300($2)
	li	$3,-2147221504			# 0xffffffff80040000
	addiu	$6,$6,21968
	sw	$17,21172($2)
	addiu	$3,$3,4
	sw	$17,21188($2)
	sw	$20,21176($2)
	addiu	$23,$23,-768
	sw	$7,21192($2)
	addiu	$22,$22,-384
	sw	$16,21228($2)
	sw	$16,21244($2)
	sw	$5,21256($2)
	sw	$15,21260($2)
	sw	$4,21268($2)
	sw	$4,21272($2)
	sw	$15,21276($2)
	sw	$4,21284($2)
	sw	$4,21288($2)
	sw	$9,21292($2)
	sw	$6,21296($2)
	.set	noreorder
	.set	nomacro
	beq	$19,$0,$L265
	sw	$3,21304($2)
	.set	macro
	.set	reorder

	lbu	$5,17817($2)
	lbu	$6,16426($2)
	lbu	$3,17816($2)
	sll	$2,$5,2
	mul	$4,$5,$6
	sltu	$7,$3,2
	addu	$20,$4,$3
	addu	$2,$20,$2
	sll	$4,$2,8
	sll	$2,$2,7
	addiu	$4,$4,768
	addiu	$2,$2,384
	addu	$23,$23,$4
	.set	noreorder
	.set	nomacro
	bne	$7,$0,$L328
	addu	$22,$22,$2
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L329
	addiu	$6,$6,-1
	.set	macro
	.set	reorder

$L266:
	li	$15,1			# 0x1
	.set	noreorder
	.set	nomacro
	j	$L212
	li	$24,1			# 0x1
	.set	macro
	.set	reorder

$L265:
	move	$15,$0
	li	$24,1			# 0x1
	move	$20,$0
$L212:
	li	$3,-201326592			# 0xfffffffff4000000
	li	$5,4			# 0x4
	addiu	$19,$3,16416
	li	$2,321388544			# 0x13280000
	lb	$9,17740($3)
	li	$4,321650688			# 0x132c0000
	lbu	$6,10($19)
	sw	$5,64($2)
	addiu	$5,$3,17740
	li	$3,5			# 0x5
	ori	$4,$4,0x8800
	sll	$6,$6,4
	sw	$4,18412($19)
	sw	$3,80($2)
	li	$3,768			# 0x300
	sh	$3,18416($19)
	li	$3,1048576			# 0x100000
	sw	$4,12($2)
	addiu	$3,$3,16
	sw	$0,96($2)
	sw	$3,116($2)
	li	$3,1			# 0x1
	sw	$0,68($2)
	sw	$6,84($2)
	sw	$3,68($2)
	.set	noreorder
	.set	nomacro
	blez	$9,$L261
	addiu	$12,$19,4100
	.set	macro
	.set	reorder

	lbu	$25,77($5)
	addiu	$13,$19,20088
	move	$16,$12
	lbu	$12,76($5)
	addiu	$fp,$19,19320
	addiu	$21,$19,3588
	li	$9,321585152			# 0x132b0000
	li	$17,321388544			# 0x13280000
	addiu	$19,$19,3076
	addiu	$23,$23,256
	.set	noreorder
	.set	nomacro
	bne	$12,$0,$L216
	addiu	$22,$22,128
	.set	macro
	.set	reorder

$L332:
	bne	$25,$0,$L216
$L217:
	lbu	$2,2($5)
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L330
	lw	$2,%lo(dFRM)($14)
	.set	macro
	.set	reorder

	bne	$15,$0,$L331
$L221:
	li	$4,-201326592			# 0xfffffffff4000000
$L257:
	lw	$3,16384($4)
	lw	$2,16388($4)
	addiu	$2,$2,2
	slt	$2,$2,$3
	beq	$2,$0,$L257
	.set	noreorder
	.set	nomacro
	beq	$20,$0,$L220
	li	$2,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

$L341:
	lw	$3,21964($2)
	beq	$3,$0,$L341
	sw	$0,21964($2)
	li	$4,321585152			# 0x132b0000
	lw	$3,16392($2)
	ori	$4,$4,0x3c94
	addiu	$3,$3,876
	sw	$3,16392($2)
	lw	$3,16392($2)
	sltu	$3,$3,$4
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L259
	addiu	$3,$9,4096
	.set	macro
	.set	reorder

	sw	$3,16392($2)
$L259:
	li	$2,-201326592			# 0xfffffffff4000000
	lw	$3,16392($2)
	sw	$3,4($9)
	lw	$3,16388($2)
	addiu	$3,$3,1
	sw	$3,16388($2)
$L220:
	lw	$4,%lo(dMB)($8)
	li	$6,321650688			# 0x132c0000
	lw	$3,%lo(dMB_X)($11)
	li	$2,-201326592			# 0xfffffffff4000000
	lw	$5,%lo(dMB_N)($10)
	addiu	$25,$9,12
	lw	$7,%lo(dMB_L)($18)
	addiu	$12,$6,21964
	sw	$4,%lo(dMB_L)($18)
	li	$4,4194304			# 0x400000
	sw	$3,%lo(dMB_N)($10)
	andi	$3,$3,0xffff
	addiu	$4,$4,876
	sw	$5,%lo(dMB)($8)
	or	$3,$3,$6
	sw	$7,%lo(dMB_X)($11)
	sw	$25,21148($2)
	addiu	$20,$20,1
	sw	$3,21136($2)
	li	$3,-2147221504			# 0xffffffff80040000
	sw	$4,21144($2)
	li	$4,262144			# 0x40000
	sw	$12,21152($2)
	addiu	$3,$3,4
	lw	$7,16392($2)
	addiu	$4,$4,4
	sw	$3,21160($2)
	li	$3,1			# 0x1
	sw	$4,21156($2)
	move	$4,$21
	sw	$7,21132($2)
	li	$2,320929792			# 0x13210000
	move	$21,$16
	sw	$3,4($2)
	move	$16,$19
	lb	$2,0($5)
	move	$3,$13
	.set	noreorder
	.set	nomacro
	blez	$2,$L261
	move	$13,$fp
	.set	macro
	.set	reorder

	lbu	$12,76($5)
	move	$fp,$3
	move	$19,$4
	lbu	$25,77($5)
	addiu	$23,$23,256
	.set	noreorder
	.set	nomacro
	beq	$12,$0,$L332
	addiu	$22,$22,128
	.set	macro
	.set	reorder

$L216:
	.set	noreorder
	.set	nomacro
	bne	$15,$0,$L217
	lw	$4,%lo(dMB_L)($18)
	.set	macro
	.set	reorder

	lbu	$3,2($4)
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L222
	addiu	$2,$fp,-16
	.set	macro
	.set	reorder

	addiu	$3,$4,92
	lw	$4,72($4)
	li	$5,1			# 0x1
	and	$4,$5,$4
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L223
	li	$6,6			# 0x6
	.set	macro
	.set	reorder

	li	$4,8			# 0x8
$L224:
#APP
 # 343 "mpeg4_p1.c" 1
	S32LDI xr1,$3,16
 # 0 "" 2
 # 344 "mpeg4_p1.c" 1
	S32LDD xr2,$3,4
 # 0 "" 2
 # 345 "mpeg4_p1.c" 1
	S32LDD xr3,$3,8
 # 0 "" 2
 # 346 "mpeg4_p1.c" 1
	S32LDD xr4,$3,12
 # 0 "" 2
 # 347 "mpeg4_p1.c" 1
	S32SDI xr1,$2,16
 # 0 "" 2
 # 348 "mpeg4_p1.c" 1
	S32STD xr2,$2,4
 # 0 "" 2
 # 349 "mpeg4_p1.c" 1
	S32STD xr3,$2,8
 # 0 "" 2
 # 350 "mpeg4_p1.c" 1
	S32STD xr4,$2,12
 # 0 "" 2
#NO_APP
	addiu	$4,$4,-1
	bne	$4,$0,$L224
	addiu	$6,$6,-1
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L226
	sll	$5,$5,4
	.set	macro
	.set	reorder

$L334:
	lw	$4,%lo(dMB_L)($18)
	lw	$4,72($4)
	and	$4,$5,$4
	.set	noreorder
	.set	nomacro
	bne	$4,$0,$L224
	li	$4,8			# 0x8
	.set	macro
	.set	reorder

$L223:
	addiu	$6,$6,-1
	addiu	$3,$3,128
	.set	noreorder
	.set	nomacro
	bne	$6,$0,$L334
	sll	$5,$5,4
	.set	macro
	.set	reorder

$L226:
	lui	$7,%hi(motion_dsa)
	li	$4,-2147483648			# 0xffffffff80000000
	lw	$5,%lo(motion_dsa)($7)
	ori	$4,$4,0xffff
$L228:
	lw	$3,0($5)
	bne	$3,$4,$L228
$L222:
	lw	$3,%lo(dMB)($8)
	lbu	$4,2($3)
	.set	noreorder
	.set	nomacro
	beq	$4,$0,$L335
	lw	$31,%lo(dFRM)($14)
	.set	macro
	.set	reorder

$L229:
	lw	$4,%lo(dMB_L)($18)
	lbu	$3,2($4)
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L230
	lw	$5,%lo(dFRM)($14)
	.set	macro
	.set	reorder

	lb	$3,31($5)
	bne	$3,$0,$L231
	lb	$6,7($5)
	addiu	$3,$6,-1
	andi	$3,$3,0x00ff
	sltu	$3,$3,2
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L231
	li	$3,13			# 0xd
	.set	macro
	.set	reorder

	beq	$6,$3,$L336
$L301:
	lw	$3,72($17)
	andi	$3,$3,0x1
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L301
	addiu	$7,$21,256
	.set	macro
	.set	reorder

	li	$12,321650688			# 0x132c0000
	andi	$7,$7,0xffff
	or	$7,$7,$12
	andi	$25,$21,0xffff
	sw	$7,76($sp)
	li	$7,321388544			# 0x13280000
	or	$25,$25,$12
	ori	$7,$7,0x68
	addiu	$3,$21,264
	sw	$25,0($7)
	lw	$25,76($sp)
	subu	$2,$2,$fp
	andi	$3,$3,0xffff
	or	$3,$3,$12
	addiu	$7,$2,16
	sw	$25,108($17)
	andi	$2,$fp,0xffff
	sw	$3,112($17)
	li	$3,603979776			# 0x24000000
	lw	$25,72($4)
	or	$12,$2,$12
	li	$2,-201326592			# 0xfffffffff4000000
	or	$3,$25,$3
	ori	$2,$2,0x8800
	sh	$7,16($2)
	sw	$3,0($2)
	sw	$12,8($2)
	lb	$3,41($5)
	li	$5,1			# 0x1
	lbu	$4,78($4)
	sw	$5,64($17)
	li	$5,8388608			# 0x800000
	andi	$4,$4,0x3f
	movn	$5,$0,$3
	or	$3,$5,$4
	sw	$3,4($2)
$L234:
	slt	$2,$20,2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L257
	li	$4,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

	li	$2,-201326592			# 0xfffffffff4000000
$L342:
	lw	$3,21968($2)
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L342
	addiu	$3,$19,256
	.set	macro
	.set	reorder

	sw	$0,21968($2)
	li	$5,321650688			# 0x132c0000
	sw	$23,21168($2)
	andi	$3,$3,0xffff
	sw	$22,21184($2)
	or	$3,$3,$5
	addiu	$6,$5,23020
	addiu	$4,$9,16
	sw	$3,21180($2)
	andi	$7,$19,0xffff
	lw	$3,%lo(dFRM)($14)
	or	$7,$7,$5
	sw	$6,21200($2)
	sw	$4,21196($2)
	sw	$7,21164($2)
	sw	$4,21212($2)
	sw	$6,21216($2)
	lbu	$6,42($3)
	.set	noreorder
	.set	nomacro
	beq	$6,$0,$L241
	move	$7,$19
	.set	macro
	.set	reorder

	sltu	$2,$24,6
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L242
	sll	$2,$24,2
	.set	macro
	.set	reorder

	lui	$4,%hi($L244)
	addiu	$4,$4,%lo($L244)
	addu	$2,$4,$2
	lw	$2,0($2)
	j	$2
	.rdata
	.align	2
	.align	2
$L244:
	.word	$L242
	.word	$L243
	.word	$L245
	.word	$L246
	.word	$L247
	.word	$L245
	.section	.p1_main
$L330:
	addiu	$3,$5,8
	lbu	$4,4($5)
	lbu	$6,3($5)
	lbu	$7,4($2)
	lbu	$5,5($2)
	sw	$12,16($sp)
	sw	$25,20($sp)
	sw	$3,24($sp)
	lw	$3,12($2)
	sw	$3,28($sp)
	lw	$3,16($2)
	sw	$3,32($sp)
	lw	$2,32($2)
	sw	$8,44($sp)
	sw	$9,60($sp)
	sw	$10,48($sp)
	sw	$2,36($sp)
	sw	$11,52($sp)
	sw	$13,72($sp)
	sw	$14,56($sp)
	sw	$15,68($sp)
	.set	noreorder
	.set	nomacro
	jal	MPV_motion_p1
	sw	$24,64($sp)
	.set	macro
	.set	reorder

	lw	$15,68($sp)
	lw	$24,64($sp)
	lw	$14,56($sp)
	lw	$13,72($sp)
	lw	$11,52($sp)
	lw	$10,48($sp)
	lw	$9,60($sp)
	.set	noreorder
	.set	nomacro
	beq	$15,$0,$L221
	lw	$8,44($sp)
	.set	macro
	.set	reorder

$L331:
	.set	noreorder
	.set	nomacro
	j	$L220
	move	$15,$0
	.set	macro
	.set	reorder

$L246:
	addiu	$6,$19,-1
	li	$3,1			# 0x1
$L263:
	li	$2,-201326592			# 0xfffffffff4000000
	subu	$3,$19,$3
	li	$4,16			# 0x10
	addiu	$5,$3,256
	addiu	$3,$2,21572
	addiu	$2,$2,21308
$L249:
#APP
 # 676 "mpeg4_p1.c" 1
	S8LDI xr5,$6,16,ptn7
 # 0 "" 2
 # 677 "mpeg4_p1.c" 1
	S32SDI xr5,$2,16
 # 0 "" 2
 # 678 "mpeg4_p1.c" 1
	S32STD xr5,$2,4
 # 0 "" 2
 # 679 "mpeg4_p1.c" 1
	S32STD xr5,$2,8
 # 0 "" 2
 # 680 "mpeg4_p1.c" 1
	S32STD xr5,$2,12
 # 0 "" 2
 # 681 "mpeg4_p1.c" 1
	S8LDI xr6,$5,8,ptn7
 # 0 "" 2
 # 682 "mpeg4_p1.c" 1
	S32SDI xr6,$3,8
 # 0 "" 2
 # 683 "mpeg4_p1.c" 1
	S32STD xr6,$3,4
 # 0 "" 2
#NO_APP
	addiu	$4,$4,-1
	bne	$4,$0,$L249
	li	$3,321650688			# 0x132c0000
	li	$2,-201326592			# 0xfffffffff4000000
	addiu	$3,$3,21324
	sw	$3,21228($2)
	li	$3,3			# 0x3
	.set	noreorder
	.set	nomacro
	beq	$24,$3,$L250
	li	$3,16777216			# 0x1000000
	.set	macro
	.set	reorder

	addiu	$4,$23,-512
	addiu	$6,$23,-256
	addiu	$3,$3,256
	sw	$4,21232($2)
	addiu	$5,$22,-256
	addiu	$4,$22,-128
	sw	$6,21248($2)
	sw	$3,21236($2)
	sw	$3,21240($2)
	sw	$5,21264($2)
	sw	$4,21280($2)
$L242:
	li	$2,321650688			# 0x132c0000
$L343:
$L344:
	li	$3,320995328			# 0x13220000
	addiu	$2,$2,21164
	sw	$2,0($3)
	li	$2,1			# 0x1
	sw	$2,4($3)
	.set	noreorder
	.set	nomacro
	j	$L257
	li	$4,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

$L230:
	lbu	$2,7($5)
	addiu	$2,$2,-1
	andi	$2,$2,0x00ff
	sltu	$2,$2,2
	bne	$2,$0,$L304
$L303:
	lw	$2,72($17)
	andi	$2,$2,0x1
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L303
	addiu	$7,$4,108
	.set	macro
	.set	reorder

	addiu	$6,$21,256
	andi	$7,$7,0xffff
	addiu	$2,$21,264
	li	$12,321650688			# 0x132c0000
	sw	$7,76($sp)
	andi	$25,$21,0xffff
	andi	$6,$6,0xffff
	andi	$2,$2,0xffff
	li	$7,321388544			# 0x13280000
	or	$25,$25,$12
	or	$6,$6,$12
	or	$2,$2,$12
	ori	$7,$7,0x68
	sw	$25,0($7)
	li	$3,8388608			# 0x800000
	lw	$25,76($sp)
	sw	$6,108($17)
	li	$6,-201326592			# 0xfffffffff4000000
	sw	$2,112($17)
	li	$2,768			# 0x300
	ori	$6,$6,0x8800
	or	$7,$25,$12
	sh	$2,16($6)
	li	$2,17891328			# 0x1110000
	sw	$7,8($6)
	addiu	$2,$2,4369
	sw	$2,0($6)
	lb	$5,41($5)
	lbu	$2,78($4)
	li	$4,1			# 0x1
	movn	$3,$0,$5
	andi	$2,$2,0x3f
	sw	$4,64($17)
	or	$2,$3,$2
	.set	noreorder
	.set	nomacro
	j	$L234
	sw	$2,4($6)
	.set	macro
	.set	reorder

$L304:
	lw	$2,72($17)
	andi	$2,$2,0x1
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L304
	addiu	$7,$4,108
	.set	macro
	.set	reorder

	addiu	$6,$21,256
	andi	$7,$7,0xffff
	addiu	$2,$21,264
	li	$12,321650688			# 0x132c0000
	sw	$7,76($sp)
	andi	$25,$21,0xffff
	andi	$6,$6,0xffff
	andi	$2,$2,0xffff
	li	$7,321388544			# 0x13280000
	or	$25,$25,$12
	or	$6,$6,$12
	or	$2,$2,$12
	ori	$7,$7,0x68
	sw	$25,0($7)
	li	$3,8388608			# 0x800000
	lw	$25,76($sp)
	sw	$6,108($17)
	li	$6,-201326592			# 0xfffffffff4000000
	sw	$2,112($17)
	li	$2,17891328			# 0x1110000
	or	$7,$25,$12
	addiu	$2,$2,4369
	ori	$6,$6,0x8800
	sw	$7,8($6)
	sw	$2,0($6)
	lb	$2,41($5)
	li	$5,1			# 0x1
	lbu	$4,78($4)
	movn	$3,$0,$2
	sw	$5,64($17)
	move	$2,$3
	andi	$3,$4,0x3f
	or	$2,$2,$3
	.set	noreorder
	.set	nomacro
	j	$L234
	sw	$2,4($6)
	.set	macro
	.set	reorder

$L336:
	lb	$3,41($5)
	bne	$3,$0,$L301
$L231:
	lb	$5,7($5)
	li	$3,19			# 0x13
	beq	$5,$3,$L234
$L302:
	lw	$3,72($17)
	andi	$3,$3,0x1
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L302
	addiu	$7,$21,256
	.set	macro
	.set	reorder

	addiu	$6,$21,264
	li	$3,321650688			# 0x132c0000
	andi	$12,$21,0xffff
	andi	$7,$7,0xffff
	andi	$6,$6,0xffff
	li	$25,321388544			# 0x13280000
	or	$12,$12,$3
	or	$7,$7,$3
	or	$6,$6,$3
	ori	$25,$25,0x68
	sw	$12,0($25)
	subu	$2,$2,$fp
	sw	$7,108($17)
	li	$7,603979776			# 0x24000000
	sw	$6,112($17)
	andi	$6,$fp,0xffff
	lw	$12,72($4)
	addiu	$2,$2,16
	or	$6,$6,$3
	li	$3,-201326592			# 0xfffffffff4000000
	or	$7,$12,$7
	ori	$3,$3,0x8800
	sh	$2,16($3)
	sw	$7,0($3)
	sw	$6,8($3)
	lbu	$2,78($4)
	li	$4,1			# 0x1
	sw	$4,64($17)
	li	$4,8388608			# 0x800000
	andi	$2,$2,0x3f
	or	$2,$2,$4
	.set	noreorder
	.set	nomacro
	j	$L234
	sw	$2,4($3)
	.set	macro
	.set	reorder

$L241:
	li	$3,262144			# 0x40000
	sw	$4,21228($2)
	addiu	$5,$5,21968
	addiu	$3,$3,4
	sw	$5,21232($2)
	sw	$3,21236($2)
	li	$3,-2147221504			# 0xffffffff80040000
	addiu	$3,$3,4
	sw	$3,21240($2)
	li	$2,321650688			# 0x132c0000
	li	$3,320995328			# 0x13220000
	addiu	$2,$2,21164
	sw	$2,0($3)
	li	$2,1			# 0x1
	sw	$2,4($3)
	.set	noreorder
	.set	nomacro
	j	$L257
	li	$4,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

$L335:
	addiu	$7,$3,8
	lbu	$4,4($3)
	lbu	$6,3($3)
	lbu	$3,4($31)
	lbu	$5,5($31)
	sw	$7,24($sp)
	sw	$12,16($sp)
	move	$7,$3
	sw	$25,20($sp)
	lw	$3,12($31)
	sw	$3,28($sp)
	lw	$3,16($31)
	sw	$3,32($sp)
	lw	$3,32($31)
	sw	$2,40($sp)
	sw	$8,44($sp)
	sw	$9,60($sp)
	sw	$3,36($sp)
	sw	$10,48($sp)
	sw	$11,52($sp)
	sw	$13,72($sp)
	sw	$14,56($sp)
	sw	$15,68($sp)
	.set	noreorder
	.set	nomacro
	jal	MPV_motion_p1
	sw	$24,64($sp)
	.set	macro
	.set	reorder

	lw	$24,64($sp)
	lw	$15,68($sp)
	lw	$14,56($sp)
	lw	$13,72($sp)
	lw	$11,52($sp)
	lw	$10,48($sp)
	lw	$9,60($sp)
	lw	$8,44($sp)
	.set	noreorder
	.set	nomacro
	j	$L229
	lw	$2,40($sp)
	.set	macro
	.set	reorder

$L261:
	li	$3,321388544			# 0x13280000
$L215:
	lw	$2,72($3)
	andi	$2,$2,0x1
	beq	$2,$0,$L215
	li	$3,320995328			# 0x13220000
$L307:
	lw	$2,4($3)
	andi	$2,$2,0x4
	.set	noreorder
	.set	nomacro
	beq	$2,$0,$L307
	li	$2,321585152			# 0x132b0000
	.set	macro
	.set	reorder

	li	$3,1			# 0x1
	sw	$3,0($2)
#APP
 # 873 "mpeg4_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 874 "mpeg4_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 875 "mpeg4_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 876 "mpeg4_p1.c" 1
	nop	#i_nop
 # 0 "" 2
 # 877 "mpeg4_p1.c" 1
	wait	#i_wait
 # 0 "" 2
#NO_APP
	lw	$31,116($sp)
	move	$2,$0
	lw	$fp,112($sp)
	lw	$23,108($sp)
	lw	$22,104($sp)
	lw	$21,100($sp)
	lw	$20,96($sp)
	lw	$19,92($sp)
	lw	$18,88($sp)
	lw	$17,84($sp)
	lw	$16,80($sp)
	.set	noreorder
	.set	nomacro
	j	$31
	addiu	$sp,$sp,120
	.set	macro
	.set	reorder

$L247:
	addiu	$4,$9,16
	li	$2,-201326592			# 0xfffffffff4000000
	sw	$4,21228($2)
	li	$4,321650688			# 0x132c0000
	addiu	$4,$4,21968
	sw	$4,21232($2)
	li	$4,262144			# 0x40000
	addiu	$4,$4,4
	sw	$4,21236($2)
	li	$4,-2147221504			# 0xffffffff80040000
	addiu	$4,$4,4
	sw	$4,21240($2)
	lbu	$2,10($3)
	teq	$2,$0,7
	div	$0,$20,$2
	mfhi	$2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L343
	li	$2,321650688			# 0x132c0000
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L343
	li	$24,3			# 0x3
	.set	macro
	.set	reorder

$L245:
	li	$2,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$24,$2,$L264
	move	$2,$0
	.set	macro
	.set	reorder

	addiu	$7,$19,240
	li	$2,112			# 0x70
$L264:
	addiu	$2,$2,256
	addu	$2,$19,$2
#APP
 # 701 "mpeg4_p1.c" 1
	S32LDD xr5,$7,0
 # 0 "" 2
 # 702 "mpeg4_p1.c" 1
	S32LDD xr6,$7,4
 # 0 "" 2
 # 703 "mpeg4_p1.c" 1
	S32LDD xr7,$7,8
 # 0 "" 2
 # 704 "mpeg4_p1.c" 1
	S32LDD xr8,$7,12
 # 0 "" 2
 # 705 "mpeg4_p1.c" 1
	S32LDD xr9,$2,0
 # 0 "" 2
 # 706 "mpeg4_p1.c" 1
	S32LDD xr10,$2,4
 # 0 "" 2
 # 707 "mpeg4_p1.c" 1
	S32LDD xr11,$2,8
 # 0 "" 2
 # 708 "mpeg4_p1.c" 1
	S32LDD xr12,$2,12
 # 0 "" 2
#NO_APP
	li	$2,-201326592			# 0xfffffffff4000000
	li	$3,16			# 0x10
	addiu	$2,$2,21308
$L252:
#APP
 # 710 "mpeg4_p1.c" 1
	S32SDI xr5,$2,16
 # 0 "" 2
 # 711 "mpeg4_p1.c" 1
	S32STD xr6,$2,4
 # 0 "" 2
 # 712 "mpeg4_p1.c" 1
	S32STD xr7,$2,8
 # 0 "" 2
 # 713 "mpeg4_p1.c" 1
	S32STD xr8,$2,12
 # 0 "" 2
#NO_APP
	addiu	$3,$3,-1
	bne	$3,$0,$L252
	li	$2,-201326592			# 0xfffffffff4000000
	li	$3,8			# 0x8
	addiu	$2,$2,21564
$L253:
#APP
 # 716 "mpeg4_p1.c" 1
	S32SDI xr9,$2,16
 # 0 "" 2
 # 717 "mpeg4_p1.c" 1
	S32STD xr10,$2,4
 # 0 "" 2
 # 718 "mpeg4_p1.c" 1
	S32STD xr11,$2,8
 # 0 "" 2
 # 719 "mpeg4_p1.c" 1
	S32STD xr12,$2,12
 # 0 "" 2
#NO_APP
	addiu	$3,$3,-1
	.set	noreorder
	.set	nomacro
	bne	$3,$0,$L253
	lw	$4,%lo(dFRM)($14)
	.set	macro
	.set	reorder

	li	$3,321650688			# 0x132c0000
	li	$2,-201326592			# 0xfffffffff4000000
	addiu	$3,$3,21324
	li	$5,16777216			# 0x1000000
	sw	$3,21228($2)
	li	$3,2			# 0x2
	.set	noreorder
	.set	nomacro
	beq	$24,$3,$L338
	addiu	$5,$5,256
	.set	macro
	.set	reorder

	lw	$3,20($4)
	sw	$5,21236($2)
	sw	$5,21240($2)
	sll	$3,$3,1
	addu	$3,$23,$3
	sw	$3,21232($2)
	lw	$3,20($4)
	addu	$3,$23,$3
	sw	$3,21248($2)
	lw	$3,20($4)
	addu	$3,$22,$3
	sw	$3,21264($2)
	lw	$2,24($4)
$L255:
	addu	$2,$22,$2
	li	$3,-201326592			# 0xfffffffff4000000
	sw	$2,21280($3)
	lw	$2,%lo(dFRM)($14)
	lbu	$2,10($2)
	teq	$2,$0,7
	div	$0,$20,$2
	mfhi	$2
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L343
	li	$2,321650688			# 0x132c0000
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L344
	li	$24,3			# 0x3
	.set	macro
	.set	reorder

$L243:
	lbu	$2,11($3)
	lbu	$3,10($3)
	addiu	$2,$2,-1
	mul	$2,$3,$2
	slt	$2,$2,$20
	.set	noreorder
	.set	nomacro
	bne	$2,$0,$L274
	li	$2,2			# 0x2
	.set	macro
	.set	reorder

	li	$24,4			# 0x4
	slt	$3,$3,$20
	movz	$24,$2,$3
	addiu	$6,$19,-16
	.set	noreorder
	.set	nomacro
	j	$L263
	li	$3,8			# 0x8
	.set	macro
	.set	reorder

$L329:
$L339:
	.set	noreorder
	.set	nomacro
	beq	$3,$6,$L267
	li	$15,1			# 0x1
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	beq	$5,$0,$L268
	li	$2,-201326592			# 0xfffffffff4000000
	.set	macro
	.set	reorder

	li	$15,1			# 0x1
	lbu	$24,16427($2)
	li	$2,4			# 0x4
	addiu	$24,$24,-1
	xor	$5,$5,$24
	li	$24,5			# 0x5
	.set	noreorder
	.set	nomacro
	j	$L212
	movn	$24,$2,$5
	.set	macro
	.set	reorder

$L250:
	addiu	$4,$23,512
	addiu	$6,$23,256
	addiu	$3,$3,256
	sw	$4,21232($2)
	addiu	$5,$22,256
	addiu	$4,$22,128
	sw	$6,21248($2)
	sw	$3,21236($2)
	li	$24,1			# 0x1
	sw	$3,21240($2)
	sw	$5,21264($2)
	.set	noreorder
	.set	nomacro
	j	$L242
	sw	$4,21280($2)
	.set	macro
	.set	reorder

$L274:
	li	$24,5			# 0x5
	addiu	$6,$19,-16
	.set	noreorder
	.set	nomacro
	j	$L263
	li	$3,8			# 0x8
	.set	macro
	.set	reorder

$L328:
	addiu	$23,$23,-1024
	.set	noreorder
	.set	nomacro
	beq	$3,$0,$L266
	addiu	$22,$22,-512
	.set	macro
	.set	reorder

	.set	noreorder
	.set	nomacro
	j	$L339
	addiu	$6,$6,-1
	.set	macro
	.set	reorder

$L267:
	.set	noreorder
	.set	nomacro
	j	$L212
	li	$24,3			# 0x3
	.set	macro
	.set	reorder

$L338:
	lw	$3,20($4)
	sw	$5,21236($2)
	sw	$5,21240($2)
	sll	$3,$3,1
	subu	$3,$23,$3
	sw	$3,21232($2)
	lw	$3,20($4)
	subu	$3,$23,$3
	sw	$3,21248($2)
	lw	$3,20($4)
	subu	$3,$22,$3
	sw	$3,21264($2)
	lw	$2,24($4)
	.set	noreorder
	.set	nomacro
	j	$L255
	subu	$2,$0,$2
	.set	macro
	.set	reorder

$L268:
	.set	noreorder
	.set	nomacro
	j	$L212
	li	$24,2			# 0x2
	.set	macro
	.set	reorder

	.end	main
	.size	main, .-main
	.rdata
	.align	2
	.type	JZC_chroma_roundtab.2405, @object
	.size	JZC_chroma_roundtab.2405, 16
JZC_chroma_roundtab.2405:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.align	2
	.type	rtab.2443, @object
	.size	rtab.2443, 32
rtab.2443:
	.word	0
	.word	0
	.word	1
	.word	1
	.word	0
	.word	0
	.word	0
	.word	1
	.align	2
	.type	rtab.2442, @object
	.size	rtab.2442, 32
rtab.2442:
	.word	0
	.word	0
	.word	1
	.word	1
	.word	0
	.word	0
	.word	0
	.word	1

	.comm	dMB_X,4,4

	.comm	dMB_N,4,4

	.comm	dMB_L,4,4

	.comm	dMB,4,4

	.comm	dFRM,4,4

	.comm	current_picture_ptr,12,4

	.comm	motion_iwta,4,4

	.comm	motion_doutc,4,4

	.comm	motion_douty,4,4

	.comm	motion_dsa,4,4

	.comm	motion_dha,4,4

	.comm	motion_buf,4,4

	.comm	mc_base,4,4
	.globl	OFFTAB
	.data
	.align	2
	.type	OFFTAB, @object
	.size	OFFTAB, 16
OFFTAB:
	.word	0
	.word	4
	.word	32
	.word	36
	.ident	"GCC: (Buildroot 2014.05-gbb847d4) 4.9.1"
